plugins {
    id("java")
    id("com.openexchange.build.gradle-git") version "5.0.1"
    id("com.openexchange.build.osgi") version "4.0.0"
    id("com.openexchange.build.packaging") version "8.0.0" apply false
    id("com.openexchange.build.plugin-applier") version "4.0.0"
    id("com.openexchange.build.project-type-scanner") version "4.1.0"
    id("org.gradle.test-retry") version "1.5.8"
}

val testRuntimeOnlyForSubProjects by configurations.creating {
    isCanBeConsumed = true
}

dependencies {
    testRuntimeOnlyForSubProjects("junit:junit:4.13.2")
    //allows JUnit 3 and JUnit 4 tests to run
    testRuntimeOnlyForSubProjects("org.junit.vintage:junit-vintage-engine:5.10.2")
}

allprojects {
    apply {
        plugin("com.openexchange.build.plugin-applier")
        plugin("org.gradle.test-retry")
    }
    repositories {
        maven {
            url = uri("https://artifactory.production.cloud.oxoe.io/artifactory/libs-release")
        }
    }
    tasks.withType<Test>() {
        ignoreFailures = true

        useJUnitPlatform()
        retry {
            maxRetries.set(3)
            maxFailures.set(100)
        }
        reports {
            junitXml.apply {
                mergeReruns.set(true)
            }
        }
        jvmArgs = listOf(
            "--add-opens=java.base/java.lang=ALL-UNNAMED",
            "--add-opens=java.base/java.util=ALL-UNNAMED",
            "--add-opens=java.base/java.util.concurrent=ALL-UNNAMED",
            "--add-opens=java.base/java.util.regex=ALL-UNNAMED",
            "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",
            "--add-opens=java.base/java.net=ALL-UNNAMED",
            "--add-opens=java.base/java.util.concurrent.atomic=ALL-UNNAMED",
            "--add-opens=java.base/javax.net.ssl=ALL-UNNAMED",
            "--add-opens=java.base/java.util.stream=ALL-UNNAMED",
            "--add-opens=java.base/java.nio.charset=ALL-UNNAMED",
            "--add-opens=java.base/java.nio.file=ALL-UNNAMED",
            "--add-opens=java.base/java.text=ALL-UNNAMED",
            "--add-opens=java.base/java.security=ALL-UNNAMED",
            "--add-opens=java.base/sun.security.action=ALL-UNNAMED",
            "--add-opens=java.base/sun.security.jca=ALL-UNNAMED",
            "--add-opens=java.base/jdk.internal.access=ALL-UNNAMED",
            "--add-opens=java.xml/jdk.xml.internal=ALL-UNNAMED",
            "--add-opens=java.base/java.io=ALL-UNNAMED"
        )

        maxHeapSize = "256M"
        testLogging {
            events(org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED)
            exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
            showExceptions = true
            showCauses = true
            showStackTraces = true
        }
    }
}
