pluginManagement {
    repositories {
        maven {
            url = uri("https://artifactory.production.cloud.oxoe.io/artifactory/libs-release")
        }
    }
}

buildscript {
    repositories {
        mavenLocal()
        maven {
            setUrl("https://artifactory.production.cloud.oxoe.io/artifactory/libs-release/")
        }
    }
    dependencies {
        classpath("com.openexchange.build:projectset:3.0.0")
    }
}

apply {
    plugin("com.openexchange.build.projectset")
}

rootProject.name = "unittest"

include("com.openexchange.test")
project(":com.openexchange.test").projectDir = File("backend/com.openexchange.test")

include("com.openexchange.office.document")
project(":com.openexchange.office.document").projectDir = File("office/com.openexchange.office.document")

include("com.openexchange.office.filter.odf.test")
project(":com.openexchange.office.filter.odf.test").projectDir = File("office/com.openexchange.office.filter.odf.test")

include("com.openexchange.office.message")
project(":com.openexchange.office.message").projectDir = File("office/com.openexchange.office.message")

include("com.openexchange.office.ot.test")
project(":com.openexchange.office.ot.test").projectDir = File("office/com.openexchange.office.ot.test")

include("com.openexchange.office.rest")
project(":com.openexchange.office.rest").projectDir = File("office/com.openexchange.office.rest")

include("com.openexchange.office.rt2.core")
project(":com.openexchange.office.rt2.core").projectDir = File("office/com.openexchange.office.rt2.core")

include("com.openexchange.office.rt2.protocol")
project(":com.openexchange.office.rt2.protocol").projectDir = File("office/com.openexchange.office.rt2.protocol")

include("com.openexchange.office.tools.common")
project(":com.openexchange.office.tools.common").projectDir = File("office/com.openexchange.office.tools.common")

include("com.openexchange.office.tools.service")
project(":com.openexchange.office.tools.service").projectDir = File("office/com.openexchange.office.tools.service")

// include("com.openexchange.office.filter.ooxml.test")
// project(":com.openexchange.office.filter.ooxml.test").projectDir = File("office/com.openexchange.office.filter.ooxml.test")

// include("com.openexchange.office.filter.test")
// project(":com.openexchange.office.filter.test").projectDir = File("office/com.openexchange.office.filter.test")
