/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.files.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.FileStorageFolderType;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.file.storage.TypeAware;
import com.openexchange.file.storage.composition.FileID;
import com.openexchange.file.storage.composition.FolderID;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.folderstorage.ContentType;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.folderstorage.database.contentType.InfostoreContentType;
import com.openexchange.group.Group;
import com.openexchange.group.GroupService;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FolderHelperServiceImpl implements FolderHelperService {
	private static final String ROOT_FOLDER_ID = "0";

    private static final Logger LOG = LoggerFactory.getLogger(FolderHelperServiceImpl.class);

    private Session session;

    @Autowired
    private FolderService folderService;
    
    @Autowired
    private IDBasedFolderAccessFactory idBasedFolderAccessFactory;
    
    
    @Autowired
    private ConfigurationHelper configurationHelper;
    
    @Autowired
    private GroupService groupService;
    
    public FolderHelperServiceImpl(Session session) {
    	this.session = session;
    }

    /**
     * Checks if a folder exists or not.
     *
     * @param folderId the folder id to check
     * @return TRUE if the folder currently exists, FALSE otherwise
     * @throws OXException
     */
    @Override
    public boolean exists(String folderId) throws OXException {
    	final IDBasedFolderAccess folderAccess = getFolderAccess();
    	return folderAccess.exists(folderId);
    }

    /**
     * Retrieves the sub folders of a the provided folder.
     *
     * @param folderId the folder id where all sub folders should be retrieved.
     * @return an array of FileStorageFolder instances which can be empty
     * @throws OXException If either parent folder does not exist or its
     * subfolders cannot be delivered
     */
	@Override
    public FileStorageFolder[] getSubfolders(String folderId, boolean recursive) throws OXException {
    	final IDBasedFolderAccess folderAccess = getFolderAccess();
    	return folderAccess.getSubfolders(folderId, recursive);
	}

	/**
	 * Creates a new file storage folder with attributes taken from given file
	 * storage folder description.
	 *
	 * @param folderToCreate a initialized FolderStorageFolder instance to be 
	 * created in the storage.
	 * @return the folder id created
	 */
	@Override
    public String createFolder(FileStorageFolder folderToCreate) throws OXException {
    	final IDBasedFolderAccess folderAccess = getFolderAccess();
    	return folderAccess.createFolder(folderToCreate);
	}

    /**
     * Determines if a folder is a trash folder or not.
     * If folder is in trash, it is also defined as trash folder
     *
     * @param folder
     *  The folder to be checked.
     *
     * @return
     *  TRUE if the folder is a trash folder or in trash folder, otherwise FALSE.
     */
    @Override
    public boolean isFolderTrashOrInTrash(String folderId) throws OXException {
    	final FileStorageFolder folder = getFileStorageFolder(folderId);
        return ((null != folder) && (TypeAware.class.isInstance(folder) && FileStorageFolderType.TRASH_FOLDER.equals(((TypeAware)folder).getType())));
    }

    /**
     * Determines if a folder is a root folder or not.
     *
     *  The folder to be checked.
     *
     * @return
     *  TRUE if the folder is a trash folder, otherwise FALSE.
     */
    @Override
    public boolean isRootFolder(String folderId) throws OXException {
    	final FileStorageFolder folder = getFileStorageFolder(folderId);

        // Uses a workaround in case isRootFolder is false but the id = 0
        // Treat this folder as root folder regarding the result of
        // isRootFolder.
        return (folder.isRootFolder() || ROOT_FOLDER_ID.equals(folderId));
    }

    /**
     * Retrieve user read permission for a certain folder.
     *
     * @param fileAccess
     *  The file access to be used to retrieve the write
     *  permissions of the user.
     *
     * @param folderId
     *  The folder id where want to know the write permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has read permission in the provided folder,
     *  other FALSE.
     */
    @Override
    public boolean folderHasReadAccess(String folderId) throws OXException {
        boolean ret = false;

        int[] result = getFolderPermissions(folderId);
        if (null != result && result.length >= 3) {
            int readPermission = result[FOLDER_PERMISSIONS_READ];
            ret = (readPermission > FileStoragePermission.NO_PERMISSIONS);
        }
        return ret;
    }

    /**
     * Retrieve user write permission for a certain folder.
     *
     * @param fileAccess
     *  The file access to be used to retrieve the write
     *  permissions of the user.
     *
     * @param folderId
     *  The folder id where want to know the write permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has write permission in the provided folder,
     *  other FALSE.
     */
    @Override
    public boolean folderHasWriteAccess(String folderId) throws OXException {
        boolean ret = false;

        int[] result = getFolderPermissions(folderId);
        if (null != result && result.length >= 3) {
            int writePermission = result[FOLDER_PERMISSIONS_WRITE];
            ret = (writePermission > FileStoragePermission.NO_PERMISSIONS);
        }
        return ret;
    }

    /**
     * Retrieve user create permission for a certain folder.
     *
     * @param fileAccess
     *  The file access to be used to retrieve the create
     *  permissions of the user.
     *
     * @param folderId
     *  The folder id where want to know the create permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has the create permission in the provided folder,
     *  other FALSE.
     */
    @Override
    public boolean folderHasCreateAccess(String folderId) throws OXException {
        boolean ret = false;

        int[] result = getFolderPermissions(folderId);
        if (null != result && result.length >= 3) {
            int createPermission = result[FOLDER_PERMISSIONS_FOLDER];
            ret = (createPermission >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER);
        }
        return ret;
    }

    /**
     * Retrieve the user permissions for the provided folder id.
     * @param folderAccess
     *  The folder access used to retrieve the user permissions.
     * @param folderId
     *  The folder id to retrieve the user permissions.
     *
     * @return
     *  An array with three entries for read, write and folder permissions or
     *  null if the method is unable to retrieve the permissions.
     */
    @Override
    public int[] getFolderPermissions(String folderId) throws OXException {
        int[] result = null;


        try {
            // retrieve the folder instance from the folder id
        	final FileStorageFolder folder = getFileStorageFolder(folderId);

            if (folder != null) {
                final FileStoragePermission permission = folder.getOwnPermission();

                int readPermission = permission.getReadPermission();
                int writePermission = permission.getWritePermission();
                int folderPermission = permission.getFolderPermission();

                // create array with user permissions for the folder
                result = new int[FOLDER_PERMISSIONS_SIZE];
                result[FOLDER_PERMISSIONS_READ] = readPermission;
                result[FOLDER_PERMISSIONS_WRITE] = writePermission;
                result[FOLDER_PERMISSIONS_FOLDER] = folderPermission;
            }
        } catch (final Exception e) {
            LOG.warn("FolderHelperService - retrieving user permissions for folder failed with exception", e);
        }

        return result;
    }

    /**
     * Checks that a user permissions for a folder id are
     * sufficient for provided rights (read, write & create files).
     *
     * @param folderAccess the folder access to retrieve the user permissions.
     * @param folderId the folder id to retrieve the user permissions.
     * @param read true to ask for read permissions
     * @param write true to ask for write permissions
     * @param create true to ask for create permissions
     * @return true if the folder provides the asked permissions, otherwise false
     */
    @Override
    public boolean providesFolderPermissions(String folderId, boolean read, boolean write, boolean create) {
        boolean result = false;

        try {
            // retrieve the folder instance from the folder id
        	final FileStorageFolder folder = getFileStorageFolder(folderId);

            if (folder != null) {
                final FileStoragePermission permission = folder.getOwnPermission();

                result = true;
                result &= read ? (permission.getReadPermission() > FileStoragePermission.NO_PERMISSIONS) : result;
                result &= write ? (permission.getWritePermission() > FileStoragePermission.NO_PERMISSIONS) : result;
                result &= create ? (permission.getFolderPermission() >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER) : result;
            }
        } catch (final Exception e) {
            LOG.warn("FolderHelperService - retrieving user permissions for folder failed with exception", e);
        }

        return result;
    }

    /**
     * Retrieves the folder id from the provided file id.
     * </br><b>ATTENTION: This folder id is user-dependent and should never be used
     * in a different user-context.</b>
     *
     * @param folderAccess the folder access to be used to retrieve the folder id.
     * @param id the file id where one wants to retrieve the parent folder
     * @return the folder id or null if retrieving the id failed
     */
    @Override
    public String getFolderId(final String id) {
        String result = null;

        try {
            final FileID fileID = new FileID(id);
            final FolderID folderID = new FolderID(fileID.getService(), fileID.getAccountId(), fileID.getFolderId());

            result = folderID.toUniqueID();
        } catch (final Exception e) {
            LOG.error("FolderHelperService - retrieving the folder id from a file_id failed with exception:", e);
        }

        return result;
    }

    /**
     * Provides the user's default folder id from the session.
     *
     * @param session The ServerSession of the user.
     * @return The folder id of the user or null if the folder id couldn't be
     *         retrieved.
     */
	@Override
	public String getUserFolderId(ServerSession session) {
        String folderId = null;

        if ((null != session) && (session instanceof ServerSession)) {
            final String treeId = com.openexchange.folderstorage.FolderStorage.REAL_TREE_ID;
            final ContentType contentType = InfostoreContentType.getInstance();

            try {
                final UserizedFolder defaultFolder = folderService.getDefaultFolder(session.getUser(), treeId, contentType, session, null);

                if (null != defaultFolder) {
                    folderId = defaultFolder.getID();
                }
            } catch (OXException e) {
                LOG.warn("FolderHelperService - retrieving default folder of user failed with exception", e);
            }
        }

        return folderId;
	}

    /**
     * Provides the user's documents folder id from the session.
     *
     * @param session The server session of the user.
     * @return The folder id of the users documents. or null if the folder id
     *         cannot be retrieved.
     */
    @Override
    public String getUserDocumentsFolderId(ServerSession session) {
        String folderId = configurationHelper.getCascadeConfigurationValue(session, "io.ox/files", "folder/documents");
        if (StringUtils.isEmpty(folderId)) {
            // fall-back if documents folder is not set - can be configured by administrator
            // instead use the user's default folder
            return getUserFolderId(session);
        }
        return folderId;
    }

    /**
     * Provides the user's templates folder id from the session.
     *
     * @param session The server session of the user.
     * @return The folder id of the users templates or null if the folder id
     *         cannot be retrieved.
     */
    @Override
    public String getUserTemplatesFolderId(ServerSession session) {
        String folderId = configurationHelper.getCascadeConfigurationValue(session, "io.ox/files", "folder/templates");
        if (StringUtils.isEmpty(folderId)) {
            // fall-back if templates folder is not set - can be configured by administrator
            // instead use the user's default folder
            return getUserFolderId(session);
        }
        return folderId;
    }

    @Override
    public Set<Integer> canWriteToFolder(String folderId, Context ctx, List<Integer> userIds) throws OXException {
        Set<Integer> userWithWriteAccess = new HashSet<>();

        IDBasedFolderAccess folderAccess = idBasedFolderAccessFactory.createAccess(session);
        FileStorageFolder folder = folderAccess.getFolder(folderId);

        final List<FileStoragePermission> perms = folder.getPermissions();

        if (null != perms) {

            for (FileStoragePermission perm : perms) {

                if (perm.getWritePermission() >= FileStoragePermission.WRITE_ALL_OBJECTS) {
                    if (perm.isGroup()) {
                        Group group = groupService.getGroup(ctx, perm.getEntity());
                        for (int user : group.getMember()) {
                            if (userIds.contains(user)) {
                                userWithWriteAccess.add(user);
                            }
                        }
                    } else if (userIds.contains(perm.getEntity())) {
                        userWithWriteAccess.add(perm.getEntity());
                    }

                }
            }
        }
        return userWithWriteAccess;    	
    }
    
    /**
     * Determines if the folder id is the pre-defined sharing
     * folder id.
     *
     * @param the folder id to check
     * @return TRUE, if the folder id is the pre-defined sharing
     * folder or FALSE if not.
     */
    @Override
    public boolean isSystemUserInfostoreFolder(String folderId) {
        boolean result = false;
        try {
            int value = Integer.parseInt(folderId);
            return value == FolderObject.SYSTEM_USER_INFOSTORE_FOLDER_ID;
        } catch (NumberFormatException e) {
            // nothing to do - can be possible if this is an external
            // storage folder id - result will be FALSE
        }
        return result;
    }

    /**
     * Provides an IDBasedFolderAccess instance. 
     *
     * @return an IDBasedFolderAccess instance
     * @throws OXException if folder access cannot be retrieved
     */
    private IDBasedFolderAccess getFolderAccess() throws OXException {    	
    	return idBasedFolderAccessFactory.createAccess(session);
    }

    /**
     * Provides a FileStorageFolder instance for a folder id.
     * 
     * @param folderId the folder id that we want to access
     * @return a FileStorageFolder instance
     * @throws OXException if a FileStorageFolder instance cannot be provided
     */
    private FileStorageFolder getFileStorageFolder(String folderId) throws OXException {
    	final IDBasedFolderAccess folderAccess = getFolderAccess();
    	return folderAccess.getFolder(folderId);
    }

}
