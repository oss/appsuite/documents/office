/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.files;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.tools.common.error.ErrorCode;

public interface FileHelperService {

    boolean canWriteToFile(String fileId);
    boolean canWriteToFile(File metaData, int userId);
    Set<Integer> canWriteToFile(String fileId, Context ctx, List<Integer> userIds) throws OXException;
    boolean canCreateFileAndWrite(String sFolderId);
    boolean canReadFile(String folderId, String fileId, int userId);
    File getFileMetaData(String fileID, String version) throws OXException;
    File getFileMetaData(String fileId);
    ErrorCode createFileWithStream(String folder_id, String fileName, String mimeType, InputStream inputStream);
    ErrorCode createFileWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, String mimeType, InputStream inputStream);
    ErrorCode createFileAndWriteStream(File file, InputStream inputStream);
    ErrorCode createFileAndWriteStream(IDBasedFileAccess fileAccess, File file, InputStream inputStream);
    ErrorCode createFileCopyWithStream(String folder_id, String fileName, File sourceFile, InputStream inputStream, boolean copyMetaData);
    ErrorCode createFileCopyWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, File sourceFile, InputStream inputStream, boolean copyMetaData);
    ErrorCode writeStreamToFile(File targetFile, InputStream inputStream, File copyMetaData);
    ErrorCode writeStreamToFile(IDBasedFileAccess fileAccess, File targetFile, InputStream inputStream, File copyMetaData);
    File getMetaDataFromFileName(String folder_id, String fileName) throws OXException;
    File getMetaDataFromFileName(String folder_id, String fileName, List<Field> fields) throws OXException;
    boolean fileNameExists(String folder_id, String fileName) throws OXException;
    ErrorCode deleteFile(final String fileId, boolean hardDelete);
}
