/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.files.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageFileAccess.SortDirection;
import com.openexchange.file.storage.FileStorageObjectPermission;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.group.Group;
import com.openexchange.group.GroupService;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.session.Session;
import com.openexchange.tools.iterator.SearchIterator;
import com.openexchange.tx.TransactionAwares;


/**
 * A services which provides higher-level file functions for OX Documents
 *
 * @author Carsten Driesner
 * @since 7.10.4
 *
 */
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FileHelperServiceImpl implements FileHelperService {

	private static final Logger LOG = LoggerFactory.getLogger(FileHelperServiceImpl.class);

	private Session session;

	@Autowired
	private IDBasedFileAccessFactory fileAccessFactory;

	@Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;

    @Autowired
    private GroupService groupService;

	private FolderHelperService folderHelperService;

    public FileHelperServiceImpl(Session session) {
    	this.session = session;
    };

    /**
     * Determines, if the user associated with the provided session can
     * write to the file, checking folder/file attributes.
     *
     * @param userId the user id of the user
     * @param folderId the folder id where the file is located
     * @param fileId the file to be checked for write permissions
     * @return TRUE, if the file can be written, FALSE otherwise
     */
    @Override
    public boolean canWriteToFile(final String fileId) {
        boolean writeAccess = false;

        try {
            final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
            final File metaData = fileAccess.getFileMetadata(fileId, FileStorageFileAccess.CURRENT_VERSION);

            writeAccess = canWriteToFile(metaData, session.getUserId());
        } catch (Exception e) {
            LOG.error("FileHelperServiceImpl: Exception caught while trying to determine file/folder access rights for user", e);
        }

        return writeAccess;
    }

    @Override
    public Set<Integer> canWriteToFile(String fileId, Context ctx, List<Integer> userIds) throws OXException {
        Set<Integer> userWithWriteAccess = new HashSet<>();

        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        final File metaData = fileAccess.getFileMetadata(fileId, FileStorageFileAccess.CURRENT_VERSION);

        final List<FileStorageObjectPermission> perms = metaData.getObjectPermissions();

        if (null != perms) {

            for (FileStorageObjectPermission perm : perms) {

                if (perm.getPermissions() >= FileStorageObjectPermission.WRITE) {
                    if (perm.isGroup()) {
                        Group group = groupService.getGroup(ctx, perm.getEntity());
                        for (int user : group.getMember()) {
                            if (userIds.contains(user)) {
                                userWithWriteAccess.add(user);
                            }
                        }
                    } else if (userIds.contains(perm.getEntity())) {
                        userWithWriteAccess.add(perm.getEntity());
                    }

                }
            }
        }
        return userWithWriteAccess;
    }

    /**
     * Determines, if the user associated with the provided session can
     * write to the file, checking folder/file attributes.
     *
     * @param services the service lookup instance to be used
     * @param metaData the meta data of the file to be checked
     * @param userId the user id of the user
     * @return TRUE, if the file can be written, FALSE otherwise
     */
    @Override
    public boolean canWriteToFile(final File metaData, final int userId) {
        Validate.notNull(metaData);

        boolean writeAccess = false;

        try {
        	final FolderHelperService folderHelperService = getFolderHelperService();
            final int[] permissions = folderHelperService.getFolderPermissions(metaData.getFolderId());

            if (null != permissions) {
                int write  = permissions[FolderHelperService.FOLDER_PERMISSIONS_WRITE];

                if (write >= FileStoragePermission.WRITE_ALL_OBJECTS) {
                    // permissions are sufficient to write the document
                    writeAccess = true;
                } else {
                    // We have again dig deeper and now check the creator to determine, if we
                    // have write permissions or not. This is necessary to check the WRITE_OWN_OBJECTS
                    // permission correctly.
                    writeAccess = ((write >= FileStoragePermission.WRITE_OWN_OBJECTS) &&
                                    metaData.getCreatedBy() == userId);
                }

                if (!writeAccess) {
                    // #40873 check guest permissions, which can provide write access via object
                    final List<FileStorageObjectPermission> perms = metaData.getObjectPermissions();

                    // #42905 object permissions can be null - must be checked
                    if (null != perms) {
                        // we have to check all permissions to find out that at least one has the needed permission
                        for(FileStorageObjectPermission perm : perms) {
                            if (!perm.isGroup() && (perm.getEntity() == userId) && (perm.getPermissions() >= FileStorageObjectPermission.WRITE)) {
                                // write access via a user permission
                                writeAccess = true;
                                break;
                            } else if (perm.isGroup() && (perm.getPermissions() >= FileStorageObjectPermission.WRITE)) {
                                // #52180 write access via a group permission
                                writeAccess = true;
                                break;
                            }
                        }
                    }
                }

                // check the locking attributes which can also result in no write permissions
                final Date now = new Date();
                final Date lockedUntil = metaData.getLockedUntil();
                int lockedByUserId = -1;

                if ((null != lockedUntil) && lockedUntil.after(now)) {
                    lockedByUserId = metaData.getModifiedBy();
                }
                writeAccess = ((writeAccess && (lockedByUserId == -1 )) || (writeAccess && (lockedByUserId != -1) && (userId == lockedByUserId)));
            }
        } catch (Exception e) {
            LOG.error("FileHelperServiceImpl: Exception caught while trying to determine file/folder access rights for user", e);
        }

        return writeAccess;
    }

    /**
     * Determines, if the user associated with the provided session can
     * create a new file and write to it.
     *
     * @param sFolderId the folder id which should be checked
     * @return TRUE, if the file can be created & written, FALSE otherwise
     */
    @Override
    public boolean canCreateFileAndWrite(String folderId) {
        Validate.notNull(folderId);

        boolean bAccess = false;

        try {
        	final FolderHelperService folderHelperService = getFolderHelperService();
            final int[] permissions = folderHelperService.getFolderPermissions(folderId);

            if (null != permissions) {
                int write  = permissions[FolderHelperService.FOLDER_PERMISSIONS_WRITE];
                int create = permissions[FolderHelperService.FOLDER_PERMISSIONS_FOLDER];

                bAccess = ((write >= FileStoragePermission.WRITE_ALL_OBJECTS) &&
                           (create >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER));
            }
        } catch (Exception e) {
            LOG.error("FileHelperServiceImpl: Exception caught while trying to determine file/folder access rights for user", e);
        }

        return bAccess;
    }

    /**
     * Determines, if the user associated with the provided session can
     * read the file, checking folder/file attributes.
     *
     * @param services the service lookup instance to be used
     * @param folderId the folder id where the file resides
     * @param fileId the file id that should be checked for read access rights
     * @param userId the user id of the user
     * @return TRUE, if the file can be read, FALSE otherwise
     */
    @Override
    public boolean canReadFile(String folderId, String fileId, final int userId) {
        Validate.notNull(folderId);
        Validate.notNull(fileId);

        boolean readAccess = false;

        try {
        	final FolderHelperService folderHelperService = getFolderHelperService();
            final int[] permissions = folderHelperService.getFolderPermissions(folderId);
            final File metaData = getFileMetaData(fileId);

            // We must have access to the meta data and be able to retrieve the folder permissions
            // otherwise this is a clear indication that no read access is available. We have to
            // check special access rights, too.
            if ((null != metaData) && (null != permissions)) {
                int read = permissions[FolderHelperService.FOLDER_PERMISSIONS_READ];

                if (read >= FileStoragePermission.READ_ALL_OBJECTS) {
                    // permissions are sufficient to read the document
                    readAccess = true;
                } else {
                    // We have again dig deeper and now check the creator to determine, if we
                    // have read permissions or not. This is necessary to check the READ_OWN_OBJECTS
                    // permission correctly.
                    readAccess = ((read >= FileStoragePermission.READ_OWN_OBJECTS) &&
                                  ((metaData != null) && metaData.getCreatedBy() == userId));
                }

                if (!readAccess && (metaData != null)) {
                    // #40873 check guest permissions, which can provide read access via object
                    final List<FileStorageObjectPermission> perms = metaData.getObjectPermissions();

                    // #42905 object permissions can be null - must be checked
                    if (null != perms) {
                        // we have to check all permissions to find out that at least one has the needimpl_canUserAccessFileed permission
                        for(FileStorageObjectPermission perm : perms){
                            if (!perm.isGroup() && (perm.getEntity() == userId) && (perm.getPermissions() >= FileStorageObjectPermission.READ)) {
                                // read access via a user permission
                                readAccess = true;
                                break;
                            } else if (perm.isGroup() && (perm.getPermissions() >= FileStorageObjectPermission.READ)) {
                                // read access via a group permission
                                readAccess = true;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("RT connection: Exception caught while trying to determine file/folder access rights for user", e);
        }

        return readAccess;
    }

    /**
     *
     * @param aSession
     * @param sFileID
     * @param sVersion
     * @return
     * @throws Exception
     */
    @Override
    public File getFileMetaData(final String sFileID, final String sVersion) throws OXException {
    	Validate.notNull(sFileID);

        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
    	return fileAccess.getFileMetadata(sFileID, sVersion);
    }

    /**
     * Retrieves the meta data of a file specified by the id.
     *
     * @param session
     * @param fileId
     * @return the meta data or NULL if the meta data cannot be retrieved
     *  which is normally related to missing access rights
     */
    @Override
    public File getFileMetaData(String fileId) {
        Validate.notNull(fileId);

        File result = null;

        try {
            final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
            result = fileAccess.getFileMetadata(fileId, FileStorageFileAccess.CURRENT_VERSION);
        } catch (OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR, false);
            if (errorCode.getCode() != ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR.getCode()) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("RT connection: Unexpected exception caught trying to retrieve meta data for file : " + fileId, e);
                MDC.remove(MDCEntries.ERROR);
            } else {
                LOG.debug("No read permissions detected for file : " + fileId, e);
            }
        }

        return result;
    }

    /**
     * Creates a OX Drive file and writes the provided input stream to the file.
     *
     * @param folder_id
     *  The folder id where the file should be created.
     *
     * @param fileName
     *  The file name of the new file.
     *
     * @param mimeType
     *  The mime type of the new file.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileWithStream(final String folder_id, final String fileName, final String mimeType, final InputStream inputStream) {
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        return createFileWithStream(fileAccess, folder_id, fileName, mimeType, inputStream);
    }

    /**
     * Creates a OX Drive file and writes the provided input stream to the file.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param folder_id
     *  The folder id where the file should be created.
     *
     * @param fileName
     *  The file name of the new file.
     *
     * @param mimeType
     *  The mime type of the new file.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, String mimeType, InputStream inputStream) {
        // create default file for creating new file
        final DefaultFile metaData = new DefaultFile();

        // set new file attributes - using old file as template
        metaData.setId(FileStorageFileAccess.NEW);
        metaData.setFolderId(folder_id);
        metaData.setFileName(fileName);
        metaData.setFileMIMEType(mimeType);

        return writeFileStream(fileAccess, metaData, inputStream);
    }

    /**
     * Creates a OX Drive file and writes the provided input stream to it.
     *
     * @param file
     *  The new file instance, which is correctly initialized.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileAndWriteStream(final File file, final InputStream inputStream) {
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        return writeFileStream(fileAccess, file, inputStream);
    }

    /**
     * Creates a OX Drive file and writes the provided input stream to it.
     *
     * @param fileAccess
     *   The FileAccess instance writing the file.

     * @param file
     *  The new file instance, which is correctly initialized.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileAndWriteStream(IDBasedFileAccess fileAccess, File file, InputStream inputStream) {
        return writeFileStream(fileAccess, file, inputStream);
    }

    /**
     * Creates a new file and writes the provided input stream, while optional
     * taking over some meta data from a present file. This method creates a
     * default FileAccess writing the file.
     *
     * @param fileName
     *  The file name of the new file.
     *
     * @param sourceFile
     *  The source file which is used to take over the meta data from. The following
     *  meta data is copied: createdBy, modifiedBy, created, lastModified, mimeType.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileCopyWithStream(final String folder_id, final String fileName, final File sourceFile, final InputStream inputStream, boolean copyMetaData) {
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        return createFileCopyWithStream(fileAccess, folder_id, fileName, sourceFile, inputStream, copyMetaData);
    }

    /**
     * Creates a new file and writes the provided input stream, while optional
     * taking over some meta data from a present file.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param fileName
     *  The file name of the new file.
     *
     * @param sourceFile
     *  The source file which is used to take over the meta data from. The following
     *  meta data is copied: createdBy, modifiedBy, created, lastModified, mimeType.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode createFileCopyWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, File sourceFile, InputStream inputStream, boolean copyMetaData) {
        // create default file for creating new file
        final DefaultFile metaData = new DefaultFile();

        // set new file attributes - using old file as template
        metaData.setId(FileStorageFileAccess.NEW);
        metaData.setFolderId(folder_id);
        metaData.setFileName(fileName);
        metaData.setFileMIMEType(sourceFile.getFileMIMEType());
        // take over modification, creator and dates from the source file
        if (copyMetaData) {
            metaData.setCreatedBy(sourceFile.getCreatedBy());
            metaData.setModifiedBy(sourceFile.getModifiedBy());
            metaData.setCreated(sourceFile.getCreated());
            metaData.setLastModified(sourceFile.getLastModified());
            metaData.setVersionComment(sourceFile.getVersionComment());
        }

        return writeFileStream(fileAccess, metaData, inputStream);
    }

    /**
     * Writes the provided stream to the target file and optional copies
     * given meta data to the file.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param file
     *  Specifies the file that should be written to.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode writeStreamToFile(final File targetFile, final InputStream inputStream, final File copyMetaData) {
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        return writeStreamToFile(fileAccess, targetFile, inputStream, copyMetaData);
    }

    /**
     * Writes the provided stream to the target file and optional copies
     * given meta data to the file.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param file
     *  Specifies the file that should be written to.
     *
     * @param inputStream
     *  The input stream of the document to be written. Attention: The stream
     *  is not closed by the method. So the caller is responsible to close
     *  the stream.
     *
     * @return
     *  The error code of the operation which is ErrorCode.NO_ERROR on success
     *  or any other code in case of an error.
     */
    @Override
    public ErrorCode writeStreamToFile(IDBasedFileAccess fileAccess, File targetFile, InputStream inputStream, File copyMetaData) {
        final DefaultFile metaData = new DefaultFile();

        // set necessary meta data to overwrite an existing file
        metaData.setId(targetFile.getId());
        metaData.setFolderId(targetFile.getFolderId());
        metaData.setFileName(targetFile.getFileName());
        metaData.setFileMIMEType(targetFile.getFileMIMEType());

        // take over modification, creator and dates from the source file
        if (null != copyMetaData) {
            metaData.setCreatedBy(copyMetaData.getCreatedBy());
            metaData.setModifiedBy(copyMetaData.getModifiedBy());
            metaData.setCreated(copyMetaData.getCreated());
            metaData.setLastModified(copyMetaData.getLastModified());
            metaData.setVersionComment(copyMetaData.getVersionComment());
        }

        return writeFileStream(fileAccess, metaData, inputStream);
    }

    /**
     * Retrieves the meta data from a file specified by the folder and
     * file name.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param folder_id
     *  The folder where the file is located retrieving the meta data from. Must NOT be null.
     *
     * @param fileName
     *  The file name of the file retrieving the meta data from. Must NOT be null.
     *
     * @return
     *  A File reference or null if no file could be found.
     *
     * @throws OXException
     */
    @Override
    public File getMetaDataFromFileName(final String folder_id, final String fileName) throws OXException {
        final List<Field> fields = new ArrayList<Field>();
        fields.add(Field.ID);
        fields.add(Field.FOLDER_ID);
        fields.add(Field.FILENAME);
        fields.add(Field.FILE_MIMETYPE);
        fields.add(Field.VERSION);

        return getMetaDataFromFileName(folder_id, fileName, fields);
    }

    /**
     * Retrieves the meta data from a file specified by the folder and
     * file name.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param folder_id
     *  The folder where the file is located retrieving the meta data from. Must NOT be null.
     *
     * @param fileName
     *  The file name of the file retrieving the meta data from. Must NOT be null.
     *
     * @param fields
     *  A list of fields which should be retrieved from the file.
     *
     * @return
     *  A File reference or null if no file could be found.
     *
     * @throws OXException
     */
    @Override
    public File getMetaDataFromFileName(final String folder_id, final String fileName, final List<Field> fields) throws OXException {
        File result = null;

        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);

        final long time1 = System.currentTimeMillis();
        final SearchIterator<File> files = fileAccess.search(fileName, fields, folder_id, null, SortDirection.DEFAULT, FileStorageFileAccess.NOT_SET, FileStorageFileAccess.NOT_SET);

        while (files.hasNext()) {
            final File file = files.next();
            if (FileHelper.hasSameFileName(file, fileName)) {
                result = file;
                break;
            }
        }

        LOG.trace("RT connection: TIME to get meta data from file = " + (System.currentTimeMillis() - time1) + "ms");

        return result;
    }

    /**
     * Checks, if the given file exists or not.
     *
     * @param fileAccess
     *  The file access instance to be used by this method. Must NOT be null.
     *
     * @param folder_id
     * @param fileName
     * @return
     * @throws OXException
     */
    @Override
    public boolean fileNameExists(final String folder_id, final String fileName) throws OXException {
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);

        final List<Field> fields = new ArrayList<Field>();
        fields.add(Field.ID);
        fields.add(Field.FILENAME);

        final long time1 = System.currentTimeMillis();
        SearchIterator<File> iter = fileAccess.search(fileName, fields, folder_id, null, SortDirection.DEFAULT, -1, -1);
        LOG.trace("RT connection: TIME for file name exists = " + (System.currentTimeMillis() - time1) + "ms");
        return (iter != null) && (!iter.hasNext());
    }

    /**
     * Tries to delete a file, depending on the parameter the file is moved
     * to the trash or completely deleted.
     *
     * @param fileAccess the file access instance to be used for the delete operation
     * @param fileId the file id of the file to be delete
     * @param hardDelete if true the file will be deleted without putting it into the trash
     *
     * @return
     *  The error code of the operation. In case of success, ErrorCode.NO_ERROR.
     */
    @Override
    public ErrorCode deleteFile(final String fileId, boolean hardDelete) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);

        if (null != fileAccess) {
            try {
                final List<String> ids = new ArrayList<String>();

                ids.add(fileId);
                fileAccess.removeDocument(ids, FileStorageFileAccess.DISTANT_FUTURE, hardDelete);
            } catch (OXException e) {
                errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                errorCode = ExceptionToErrorCode.map(e, errorCode, false);
            }
        }

        return errorCode;
    }

    /**
     * Writes the stream to the provided file.
     *
     * @param fileAccess
     *   The FileAccess instance writing the file.
     *
     * @param newFile
     *   The new meta file instance. Must be filled with the necessary
     *   properties.
     *
     * @param inputStream
     *   The data to be written.
     *
     * @return
     */
    private ErrorCode writeFileStream(final IDBasedFileAccess fileAccess, final File fileMetaData, final InputStream inputStream) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        boolean rollback = true;

        final long time1 = System.currentTimeMillis();
        try {
            LOG.trace("RT connection: [writeFileStream] saveDocument using meta data: " + ((null != fileMetaData) ? fileMetaData.toString() : "null"));
            fileAccess.startTransaction();
            rollback = true;
            fileAccess.saveDocument(fileMetaData, inputStream, FileStorageFileAccess.DISTANT_FUTURE, new ArrayList<Field>());
            fileAccess.commit();
            rollback = false;
        } catch (final Exception e) {
            errorCode = ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR;
            if (e instanceof OXException) {
                errorCode = ExceptionToErrorCode.map((OXException)e, errorCode, false);
            }
            MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
            LOG.error(errorCode.getDescription(), e);
            MDC.remove(MDCEntries.ERROR);
        } finally {
            // Roll-back (if needed) and finish
            if (rollback) {
                TransactionAwares.rollbackSafe(fileAccess);
            }

            TransactionAwares.finishSafe(fileAccess);
        }
        LOG.trace("RT connection: TIME to write file stream = " + (System.currentTimeMillis() - time1) + "ms");

        return errorCode;
    }

    private FolderHelperService getFolderHelperService() {
    	if (folderHelperService == null) {
    		folderHelperService = folderHelperServiceFactory.create(session);
    	}
    	return folderHelperService;
    }

}
