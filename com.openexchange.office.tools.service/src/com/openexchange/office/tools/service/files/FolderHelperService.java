/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.files;

import java.util.List;
import java.util.Set;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.tools.session.ServerSession;

public interface FolderHelperService {

    int FOLDER_PERMISSIONS_READ = 0;
    int FOLDER_PERMISSIONS_WRITE = 1;
    int FOLDER_PERMISSIONS_FOLDER = 2;
    int FOLDER_PERMISSIONS_SIZE = 3;

    /**
     * Checks if a folder exists or not.
     *
     * @param folderId the folder id to check
     * @return TRUE if the folder currently exists, FALSE otherwise
     * @throws OXException
     */
    boolean exists(String folderId) throws OXException;

    /**
     * Retrieves the sub folders of a the provided folder.
     *
     * @param folderId the folder id where all sub folders should be retrieved.
     * @param all sub folders are provided if TRUE, otherwise only the folders
     * within the provided folder
     * @return an array of FileStorageFolder instances which can be empty
     * @throws OXException If either parent folder does not exist or its
     * subfolders cannot be delivered
     */
	FileStorageFolder[] getSubfolders(String folderId, boolean recursive) throws OXException;

	/**
	 * Creates a new file storage folder with attributes taken from given file
	 * storage folder description.
	 *
	 * @param folderToCreate a initialized FolderStorageFolder instance to be 
	 * created in the storage.
	 * @return the folder id created
	 */
	String createFolder(FileStorageFolder folderToCreate) throws OXException;

    /**
     * Determines if a folder is the trash folder or resides
     * in the trash folder.
     *
     * @param folder The folder to be checked.
     * @return
     *  TRUE if the folder is the trash folder or resides in the trash folder, 
     *  otherwise FALSE.
     */
    boolean isFolderTrashOrInTrash(String folderId) throws OXException;

    /**
     * Determines if a folder is the root folder or not.
     *
     * @param folderId The folder to be checked.
     * @return TRUE if the folder is a trash folder, otherwise FALSE.
     */
    boolean isRootFolder(String folderId) throws OXException;

    /**
     * Retrieve user read permission for a certain folder.
     *
     * @param folderId
     *  The folder id where want to know the write permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has read permission in the provided folder,
     *  other FALSE.
     */
    boolean folderHasReadAccess(String folderId) throws OXException;

    /**
     * Retrieve user write permission for a certain folder.
     *
     * @param folderId
     *  The folder id where want to know the write permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has write permission in the provided folder,
     *  other FALSE.
     */
    boolean folderHasWriteAccess(String folderId) throws OXException;

    /**
     * Retrieve user create permission for a certain folder.
     *
     * @param folderId
     *  The folder id where want to know the create permissions
     *  of the user.
     *
     * @return
     *  TRUE if the user has the create permission in the provided folder,
     *  other FALSE.
     */
    boolean folderHasCreateAccess(String folderId) throws OXException;

    /**
     *  The folder access used to retrieve the user permissions.
     * @param folderId
     *  The folder id to retrieve the user permissions.
     *
     * @return
     *  An array with three entries for read, write and folder permissions or
     *  null if the method is unable to retrieve the permissions.
     */
    int[] getFolderPermissions(String folderId) throws OXException;

    /**
     * Checks that a user permissions for a folder id are
     * sufficient for provided rights (read, write & create files).
     *
     * @param folderId the folder id to retrieve the user permissions.
     * @param read true to ask for read permissions
     * @param write true to ask for write permissions
     * @param create true to ask for create permissions
     * @return true if the folder provides the asked permissions, otherwise false
     */
    boolean providesFolderPermissions(String folderId, boolean read, boolean write, boolean create);

    /**
     * Retrieves the folder id from the provided file id.
     * </br><b>ATTENTION: A folder id is always user-dependent and should 
     * never be used in a different user-context.</b>
     *
     * @param id the file id where one wants to retrieve the parent folder
     * @return the folder id or null if retrieving the id failed
     */
    String getFolderId(String id);

    /**
     * Provides the user's default folder id from the session.
     *
     * @param session The ServerSession of the user.
     * @return The folder id of the user or null if the folder id couldn't be
     *         retrieved.
     */
    String getUserFolderId(ServerSession session);

    /**
     * Provides the user's documents folder id from the session.
     *
     * @param session The server session of the user.
     * @return The folder id of the users documents. or null if the folder id
     *         cannot be retrieved.
     */
   String getUserDocumentsFolderId(ServerSession session);

   /**
    * Provides the user's templates folder id from the session.
    *
    * @param session The server session of the user.
    * @return The folder id of the users templates or null if the folder id
    *         cannot be retrieved.
    */
   String getUserTemplatesFolderId(ServerSession session);
   
   Set<Integer> canWriteToFolder(String folderId, Context ctx, List<Integer> userIds) throws OXException;

   /**
    * Checks if the provided folder id is the system user
    * infostore folder used for file sharing.
    * 
    * @param folderId the folder id to be checked
    * @return TRUE if the provided folderId is the sharing folder id
    *  or FALSE if not.
    */
   boolean isSystemUserInfostoreFolder(String folderId);

}
