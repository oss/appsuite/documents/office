/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.storage.impl;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageCapability;
import com.openexchange.file.storage.composition.FolderID;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StorageHelperServiceImpl implements StorageHelperService {

    // capabilities cached locally
    boolean efficientRetrieval = false;
    boolean fileVersions = false;
    boolean setCapabilitiesCalled = false;
    boolean supportsPersistenIDs = false;
    boolean ignorableVersion = false;
    boolean supportsExtendedMetaData = false;
    String storageAccountId = null;

    /**
     * Initializes a new {@link StorageHelperService}.
     * @param services
     * @param session
     * @param folderId
     */
    public StorageHelperServiceImpl(IDBasedFileAccessFactory fileFactory, UserConfigurationFactory userConfigHelper, final Session session, final String folderId) {
        final IDBasedFileAccess fileAccess = (null != session) ? fileFactory.createAccess(session) : null;
        final UserConfigurationHelper confHelper = userConfigHelper.create(session, "io.ox/office", Mode.WRITE_THROUGH);
        impl_setCapabilities(fileAccess, folderId, confHelper);
    }

    /**
     * Returns the capability for an efficient stream and meta-data retrieval.
     * @return
     */
    @Override
    public boolean supportsEfficientRetrieval() {
        return efficientRetrieval;
    }

    /**
     * Returns the capability to support file versions.
     * @return
     */
    @Override
    public boolean supportsFileVersions() {
        return fileVersions;
    }

    /**
     * Returns the capability to persistent file and folder ids.
     * @return
     */
    @Override
    public boolean supportsPersistentIDs() {
        return supportsPersistenIDs;
    }

    /**
     * Returns the capability to ignore versions, e.g. overwrite an existing
     * version.
     * @return
     */
    @Override
    public boolean supportsIgnorableVersion() {
        return ignorableVersion;
    }

    /**
     * Returns the capability to store extended meta data.
     *
     * @return <code>TRUE</code> if supported otherwise <code>FALSE</code>.
     */
    @Override
    public boolean supportsExtendedMetaData() {
        return supportsExtendedMetaData;
    }

    /**
     * Returns the storage account id associated with the folder id.
     *
     * @return the account id associated with the folder id
     */
    @Override
    public String accountId() {
        return storageAccountId;
    }

    /**
     * Determines, using the folder id and file access, if the storage supports
     * file versions or not.
     *
     * @param fileAccess
     *  A valid file access .
     *
     * @param folderId
     *  A valid folder id to be checked for rename support or not.
     *
     * @return
     *  TRUE if the storage supports versions, otherwise not.
     */
    private boolean impl_setCapabilities(final IDBasedFileAccess fileAccess, final String folderId, final UserConfigurationHelper conf) {
        boolean result = false;

        try {
            final FolderID folderIdentifier = new FolderID(folderId);
            final String folderAccountId = folderIdentifier.getAccountId();
            final String folderService = folderIdentifier.getService();

            storageAccountId = folderAccountId;

            efficientRetrieval = fileAccess.supports(folderService, folderAccountId, FileStorageCapability.EFFICIENT_RETRIEVAL);
            fileVersions = fileAccess.supports(folderService, folderAccountId, FileStorageCapability.FILE_VERSIONS);
            supportsPersistenIDs = fileAccess.supports(folderService, folderAccountId, FileStorageCapability.PERSISTENT_IDS);
            ignorableVersion = fileAccess.supports(folderService, folderAccountId, FileStorageCapability.IGNORABLE_VERSION);
            supportsExtendedMetaData = fileAccess.supports(folderService, folderAccountId, FileStorageCapability.EXTENDED_METADATA);

            // overwrite values from configuration for QA purposes only
            final Boolean storageVersion = conf.getBoolean("module/storageFileVersions", null);
            if (null != storageVersion) {
                fileVersions = storageVersion;
            }
        } catch (NumberFormatException e) {
            // nothing to do
        } catch (OXException e) {
            // nothing to do
        }

        return result;
    }

}
