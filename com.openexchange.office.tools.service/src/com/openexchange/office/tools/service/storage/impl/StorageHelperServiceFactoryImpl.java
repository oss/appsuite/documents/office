/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.storage.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;

@Service
@RegisteredService(registeredClass=StorageHelperServiceFactory.class)
public class StorageHelperServiceFactoryImpl implements StorageHelperServiceFactory {

	@Autowired
	private IDBasedFileAccessFactory idBasedFileAccessFactory;
	
	@Autowired
	private UserConfigurationFactory userConfigurationFactory;
	
	@Override
	public StorageHelperService create(Session session, String folderId) {
		return new StorageHelperServiceImpl(idBasedFileAccessFactory, userConfigurationFactory, session, folderId);
	}

}
