/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.storage;

/**
 * Helper class to ease the access to storage specific capabilities.
 * {@link StorageHelperService}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public interface StorageHelperService {

    /**
     * Returns the capability for an efficient stream and meta-data retrieval.
     * @return
     */
    public boolean supportsEfficientRetrieval();

    /**
     * Returns the capability to support file versions.
     * @return
     */
    public boolean supportsFileVersions();

    /**
     * Returns the capability to persistent file and folder ids.
     * @return
     */
    public boolean supportsPersistentIDs();

    /**
     * Returns the capability to ignore versions, e.g. overwrite an existing
     * version.
     * @return
     */
    public boolean supportsIgnorableVersion();

    /**
     * Returns the capability to store extended meta data.
     *
     * @return <code>TRUE</code> if supported otherwise <code>FALSE</code>.
     */
    public boolean supportsExtendedMetaData();

    /**
     *  Returns the storage account id associated with the folder id.
     *
     * @return the account id associated with the folder id
     */
    public String accountId();
}
