/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.logging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.EvictingQueue;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

public class CachedAppender extends AppenderBase<ILoggingEvent> {

	private static final Logger log = LoggerFactory.getLogger(CachedAppender.class);
	
	private static final int DEFAULT_COUNT_LOGS_STORED = 100;
	
	private ConcurrentHashMap<String, Collection<ILoggingEvent>> msgs = new ConcurrentHashMap<>();
	
	@Override
	public void start() {
		super.start();		
	}	
	
    @Override
    public void stop() {
    	msgs = null;
        super.stop();
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        if (!isStarted()) {
            return;
        }
        String docUid = eventObject.getMDCPropertyMap().get(MDCEntries.DOC_UID);
        if (docUid != null) {
        	Collection<ILoggingEvent> loggingEvents = Collections.synchronizedCollection(EvictingQueue.create(DEFAULT_COUNT_LOGS_STORED));
        	Collection<ILoggingEvent> tmpCol = msgs.putIfAbsent(docUid, loggingEvents);
        	if (tmpCol != null) {
        		loggingEvents = tmpCol;
        	}
        	loggingEvents.add(eventObject);
        } else {
        	log.warn("No com.openexchange.rt2.document.uid found for log statement {}", eventObject);
        }
    }

    public void reset() {
    	msgs.clear();
    }

    public int size() {
        return msgs.size();
    }

    public Set<String> getDocUidsWithLoggingEvents() {
        return new HashSet<>(msgs.keySet());
    }
    
    public boolean hasLoggingEventsForDocUid(String docUid) {
    	return msgs.containsKey(docUid);
    }
    
    public Collection<ILoggingEvent> getLoggingEventsForDocUid(String docUid) {
    	Collection<ILoggingEvent> res = msgs.get(docUid);
    	if (res != null) {
    		return new ArrayList<ILoggingEvent>(res);
    	}
    	return Collections.emptyList();
    }    
    
	public void removeEntriesForKey(String key) {
		msgs.remove(key);
	}
}
