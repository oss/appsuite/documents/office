/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.logging;

import java.util.List;
import java.util.Map;

import org.slf4j.Marker;
import org.slf4j.event.KeyValuePair;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.LoggerContextVO;

public class LoggingEventProxy implements ILoggingEvent {

	private final ILoggingEvent orig;
	private final StackTraceElement stackTraceElement;

	public LoggingEventProxy(ILoggingEvent orig, StackTraceElement stackTraceElement) {
		this.orig = orig;
		this.stackTraceElement = stackTraceElement;
	}

	@Override
    public String getThreadName() {
		return orig.getThreadName();
	}

	@Override
    public Level getLevel() {
		return orig.getLevel();
	}

	@Override
    public String getMessage() {
		return orig.getMessage();
	}

	@Override
    public Object[] getArgumentArray() {
		return orig.getArgumentArray();
	}

	@Override
    public String getFormattedMessage() {
		return orig.getFormattedMessage();
	}

	@Override
    public String getLoggerName() {
		return stackTraceElement.getClassName();
	}

	@Override
    public LoggerContextVO getLoggerContextVO() {
		return orig.getLoggerContextVO();
	}

	@Override
    public IThrowableProxy getThrowableProxy() {
		return orig.getThrowableProxy();
	}

	@Override
    public StackTraceElement[] getCallerData() {
		StackTraceElement [] callerData = new StackTraceElement[1];
		callerData[0] = stackTraceElement;
		return callerData;
	}

	@Override
    public boolean hasCallerData() {
		return orig.hasCallerData();
	}

	@Override
    public Marker getMarker() {
		return orig.getMarker();
	}

	@Override
    public Map<String, String> getMDCPropertyMap() {
		return orig.getMDCPropertyMap();
	}

	@Override
    public Map<String, String> getMdc() {
		return orig.getMdc();
	}

	@Override
    public long getTimeStamp() {
		return orig.getTimeStamp();
	}

	@Override
    public void prepareForDeferredProcessing() {
		orig.prepareForDeferredProcessing();
	}

    @Override
    public List<Marker> getMarkerList() {
        return orig.getMarkerList();
    }

    @Override
    public int getNanoseconds() {
        return orig.getNanoseconds();
    }

    @Override
    public long getSequenceNumber() {
        return orig.getSequenceNumber();
    }

    @Override
    public List<KeyValuePair> getKeyValuePairs() {
        return orig.getKeyValuePairs();
    }
}
