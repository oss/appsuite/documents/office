/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.logging;

public interface MDCEntries {
	String LOGGER_NAME = "com.openexchange.office.logger.name";
	String LOGGER_LINE = "com.openexchange.office.logger.line";
	String LOGGER_METHOD = "com.openexchange.office.logger.method";
	String CLIENT_UID = "com.openexchange.rt2.client.uid";
	String DOC_UID = "com.openexchange.rt2.document.uid";
	String BACKEND_UID = "com.openexchange.rt2.backend.uid";
	String BACKEND_PART = "com.openexchange.rt2.backend.type";
	String RECIPIENTS = "com.openexchange.rt2.backend.recipients";
	String REQUEST_TYPE = "com.openexchange.rt2.request.type";
	String ADMIN_MSG_TYPE = "com.openexchange.rt2.admin.type";
	String FILENAME = "com.openexchange.rt2.filename";
	String LEVEL = "com.openexchange.rt2.level";
	String ERROR = "com.openexchange.rt2.error";
	String CHANNEL_UID = "com.openexchange.rt2.channel.uid";
	String MSG_UID = "com.openexchange.rt2.msg.uid";
}
