/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.logging;

import java.util.Collection;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.log.SpecialLoggerInfo;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.jms.LoggingJmsDestination;
import com.openexchange.threadpool.ThreadPools;
import ch.qos.logback.classic.spi.ILoggingEvent;

@RegisteredService
public class SpecialLogService {

	private static final Logger specialLogger = LoggerFactory.getLogger(SpecialLoggerInfo.SPECIAL_LOGGER_NAME); 
	
	private final CachedAppender cachedAppender;

	@Autowired
	private JmsTemplateWithoutTtl jmsTemplate;

	public SpecialLogService(CachedAppender cachedAppender) {
		this.cachedAppender = cachedAppender;
	}

	public long getEntriesCount() {
	    return cachedAppender.size();
	}

	public Set<String> getDocUidsWithEntries() {
	    return cachedAppender.getDocUidsWithLoggingEvents();
	}

	public void removeEntriesForKey(String key) {
		cachedAppender.removeEntriesForKey(key);
	}
	
	public void log(Level level, Logger loggerOfClass, String docUid, Throwable t, String logLine, Object...args) {
		boolean removeDocUidFromMDC = false;
		try {			 
			if (MDC.get(MDCEntries.DOC_UID) == null) {
				removeDocUidFromMDC = true;
				MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid);
			}
			MDCHelper.safeMDCPut(MDCEntries.LOGGER_NAME, loggerOfClass.getName());
			StackTraceElement stackTraceElement = t.getStackTrace()[0];
			MDCHelper.safeMDCPut(MDCEntries.LOGGER_LINE, Integer.toString(stackTraceElement.getLineNumber()));
			MDCHelper.safeMDCPut(MDCEntries.LOGGER_METHOD, stackTraceElement.getMethodName());
			MDCHelper.safeMDCPut(MDCEntries.LEVEL, level.name());
			switch (level) {
				case TRACE: specialLogger.trace(logLine, args);
							break;						
				case DEBUG: specialLogger.debug(logLine, args);
							break;			
				case INFO:  specialLogger.info(logLine, args);
							break;
				case WARN:  specialLogger.warn(logLine, args);
							break;
				case ERROR: specialLogger.error(logLine, args);
			}
		} finally {
			if (removeDocUidFromMDC) {
				MDC.remove(MDCEntries.DOC_UID);
			}
			MDC.remove(MDCEntries.LOGGER_NAME);
			MDC.remove(MDCEntries.LOGGER_LINE);
			MDC.remove(MDCEntries.LOGGER_METHOD);
			MDC.remove(MDCEntries.LEVEL);
		}
		switch (level) {
			case TRACE: loggerOfClass.trace(logLine, args);
						break;						
			case DEBUG: loggerOfClass.debug(logLine, args);
						break;			
			case INFO:  loggerOfClass.info(logLine, args);
						break;
			case WARN:  loggerOfClass.warn(logLine, args);
						break;
			case ERROR: loggerOfClass.error(logLine, args);
		}
	}
	
	public void onErrorEvent(ILoggingEvent eventObject) {
		String docUid = eventObject.getMDCPropertyMap().get(MDCEntries.DOC_UID);
		if (docUid != null) {
			// DOCS-2767:
			// Ensure asynchronous processing of the following code to release
			// the global appender mutex and prevent possible deadlocks or longer
			// delays of certain threads, because the following code needs more
			// time to be executed. It must also be noticed that ActiveMQ internally
			// uses a global mutex for session creation which is always called when
			// trying to send a message. Therefore at least two global mutexes are
			// involved which increases the risk of deadlocks or delays.
            ThreadPools.submitElseExecute(ThreadPools.task(() -> {
                jmsTemplate.convertAndSend(LoggingJmsDestination.loggingErrorOccuredTopic, docUid);
            }));
		}
	}
	
	public void logForDocUid(String docUid) {
		Collection<ILoggingEvent> loggingEvents = cachedAppender.getLoggingEventsForDocUid(docUid);
		loggingEvents.forEach(e -> {
			String loggerName = e.getMDCPropertyMap().get(MDCEntries.LOGGER_NAME);
			if (loggerName == null) {
				loggerName = e.getLoggerName();
			}
			if (loggerName == null) {
				loggerName = "unknown";
			}
			int loggerLine = -1;
			try {
				loggerLine = Integer.parseInt(e.getMDCPropertyMap().get(MDCEntries.LOGGER_LINE));
			} catch (NumberFormatException ex) {
				loggerLine = -1;
			}
			String methodName = e.getMDCPropertyMap().get(MDCEntries.LOGGER_METHOD);
			if (methodName == null) {
				methodName = "unknown";
			}
			String fileName = "unknown";
			if (e.getCallerData().length > 0) {
				fileName = e.getCallerData()[0].getFileName();
			}
			ILoggingEvent loggingEvent = new LoggingEventProxy(e, new StackTraceElement(loggerName, methodName, fileName, loggerLine));				
			ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(loggerName);
			if (logbackLogger.getEffectiveLevel().toInt() > loggingEvent.getLevel().toInt()) {
				logbackLogger.callAppenders(loggingEvent);
			}
		});		
		// Log only once
		removeEntriesForKey(docUid);
	}
	
    public Collection<ILoggingEvent> getLoggingEventsForDocUid(String docUid) {
    	return cachedAppender.getLoggingEventsForDocUid(docUid);
    }

}
