/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.logging;

import java.util.concurrent.atomic.AtomicBoolean;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

public class LogErrorPublisher extends AppenderBase<ILoggingEvent> {

	private final SpecialLogService specialLogService;
	
	private AtomicBoolean currentyProcessingOnErrorEvent = new AtomicBoolean(false);
	
	public LogErrorPublisher(SpecialLogService specialLogService) {
		this.specialLogService = specialLogService;
	}

	@Override
	protected void append(ILoggingEvent eventObject) {
		if (eventObject.getLevel().equals(Level.ERROR)) {
			if (currentyProcessingOnErrorEvent.compareAndSet(false, true)) {
				specialLogService.onErrorEvent(eventObject);
				currentyProcessingOnErrorEvent.set(false);
			}
		}
	}
}
