/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.session;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tools.session.ServerSessionAdapter;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

//=============================================================================
/** helper to handle seesions and it's values more comfortable.
 */
@Service
@RegisteredService
public class SessionService {
	private static final int MAX_NUM_OF_SESSION_INVALID_TO_STORE = 20;

	private static final DateTimeFormatter isoDateFormatter = DateTimeFormatter.ISO_DATE_TIME;
	
	private class SessionInvalidEntry {
		private String sessionId;
		private LocalDateTime timeStamp;

		public SessionInvalidEntry(String sessionId) {
			this.sessionId = sessionId;
			this.timeStamp = LocalDateTime.now();
		}

		public String getSessionId() {
			return this.sessionId;
		}

		public LocalDateTime getTimeStamp() {
			return this.timeStamp;
		}
	}

	@Autowired
	private SessiondService sessiondService;
	
	@Autowired
	private UserService userService;
	
	@Autowired(required=false)
	private ContextService contextService;
	
	private AtomicLong sessionInvalidNotification = new AtomicLong(0);
	private Collection<SessionInvalidEntry> lastSessionsInvalidated = Collections.synchronizedCollection(new CircularFifoQueue<SessionInvalidEntry>(MAX_NUM_OF_SESSION_INVALID_TO_STORE));

	//-------------------------------------------------------------------------
	/**
	 * Increases the counter of session invalid notifications.
	 *
	 * @param sessionId the session that was notified as invalid
	 */
	public void incSessionInvalidNotification(String sessionId) {
		sessionInvalidNotification.incrementAndGet();
		lastSessionsInvalidated.add(new SessionInvalidEntry(sessionId));
	}

	//-------------------------------------------------------------------------
	/**
	 * Provides the number of session invalid notifications received.
	 */
	public long getCountOfSessionInvalidNotifications() {
		return sessionInvalidNotification.get();
	}

	//-------------------------------------------------------------------------
	/**
	 * Provides a list of session id and their time of invalidation.
	 *
	 * @return
	 *  A list of strings that contain the session id and the date/time of their
	 *  corresponding invalidation.
	 */
	public List<String> getLatestStoredSessionInvalidated() {
		final List<String> sessionInvalidated = new ArrayList<>();
		lastSessionsInvalidated.stream().forEach(s -> {
			StringBuffer buf = new StringBuffer(256);
			buf.append("session-id=").append(s.getSessionId())
			   .append(" - date=").append(s.getTimeStamp().format(isoDateFormatter));
			sessionInvalidated.add(buf.toString());
		});
		return sessionInvalidated;
	}

	//-------------------------------------------------------------------------
	/** @return a session object bound to the given session ID.
	 *          Will return null if session ID is invalid, unknown, wrong, unexpected ...
	 *
	 *	@param	sSessionId [IN]
	 *			the session ID where such sessions has to be returned for.
	 * @throws OXException
	 */
	@SuppressWarnings("unchecked")
	public < T extends Session > T getSession4Id (final String sSessionId) throws OXException
	{
		if (StringUtils.isEmpty(sSessionId))
			return null;

        Session aSession  = sessiondService.getSession(sSessionId);

		final ServerSession aSessionEx = ServerSessionAdapter.valueOf(aSession);
		return (T) aSessionEx;
	}

	//-------------------------------------------------------------------------
	/** @return TRUE if given session is valid and not expired; FALSE otherwise.
	 *
	 *	@param	aSession [IN]
	 *			the session to be validated here.
	 * @throws OXException
	 */
	public < T extends Session > boolean isSessionValid (final T session) throws OXException
	{
		if (session == null)
			return false;

		final Context aContext = getContextInfo(session);
        if (
        	(   aContext == null    ) ||
        	( ! aContext.isEnabled())
           )
        	return false;

        final User aUser = userService.getUser(session.getUserId(), session.getContextId());
        if (
        	(   aUser == null        ) ||
        	( ! aUser.isMailEnabled())
           )
        	return false;

        return true;
	}

	//-------------------------------------------------------------------------
	@SuppressWarnings("unused")
	public < T extends Session > void refreshSession (final T aSession) throws OXException
	{
		Validate.notNull(aSession, "Invalid argument 'session'.");
		final String  sSessionId        = aSession.getSessionID();
		final Session aRefreshedSession = getSession4Id (sSessionId);
	}

    //-------------------------------------------------------------------------
	/** analyze the given session and return an Context object which
	 *  describe the user context more in it's details.
	 *
	 *  @param	aSession [IN]
	 *  		the session where those context info has to be retrieved from.
	 *
	 *  @return the context info object.
	 * @throws OXException
	 *
	 *  @throws an exception in case session is not valid ...
	 *  		or user context could not be retrieved successfully.
	 */
	public < T extends Session > Context getContextInfo (final T session) throws OXException {
		final Context aContext   = contextService.getContext(session.getContextId());

		return aContext;
	}

	//-------------------------------------------------------------------------
	/** extract users name from given session to be used as visible information
	 *  e.g. at the UI.
	 *
	 *  @param	aSession [IN]
	 *  		the session where those user name has to be retrieved from.
	 *
	 *  @return the user name.
	 * @throws OXException
	 *
	 *  @throws an exception in case session is not valid ...
	 *  		or user name could not be retrieved successfully.
	 */
	public < T extends Session > String getUserDisplayName (final T session) throws OXException
	{
		final User   aUser = userService.getUser(session.getUserId(), session.getContextId());
		final String sName = aUser.getDisplayName();
		return sName;
	}

	//-------------------------------------------------------------------------
    /** dump given session to string for debug purposes.
    *
    *   @param	aSession [IN]
    *  			the session to be analyzed here
    *
    *   @return the string representation of those session.
    */
    public < T extends Session > String dumpSessionToString (final T aSession)
		// no exception here please .-)
    {
        final StringBuffer sDump = new StringBuffer(256);

        sDump.append("#### =================================================\n");
        sDump.append("#### session dump :                                   \n");
        try {
            sDump.append("#### session      : " + aSession + "\n");
        } catch (final Throwable ignore) {
        } // catch errors line by line ! we want to get info's as much as possible :-)
        try {
            sDump.append("#### login        : " + aSession.getLogin() + "\n");
        } catch (final Throwable ignore) {
        }
        try {
            sDump.append("#### login name   : " + aSession.getLoginName() + "\n");
        } catch (final Throwable ignore) {
        }
        try {
            sDump.append("#### user login   : " + aSession.getUserlogin() + "\n");
        } catch (final Throwable ignore) {
        }
        try {
            sDump.append("#### user id      : " + aSession.getUserId() + "\n");
        } catch (final Throwable ignore) {
        }
        try {
            sDump.append("#### context id   : " + aSession.getContextId() + "\n");
        } catch (final Throwable ignore) {
        }
        sDump.append("#### =================================================\n");

        return sDump.toString();
    }
}
