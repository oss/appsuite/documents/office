/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.cluster;

public interface ClusterLifecycleEvent {

    /**
     * Lifecycle states
     */
    public enum LifecycleState {
        /**
         * Fired when the member is starting.
         */
        STARTING,

        /**
         * Fired when the member start is completed.
         */
        STARTED,

        /**
         * Fired when the member is shutting down.
         */
        SHUTTING_DOWN,

        /**
         * Fired when the member shut down is completed.
         */
        SHUTDOWN,

        /**
         * Fired on each cluster member just before the start of a merge
         * process into another cluster. This is typically used when a
         * split-brain situation is healed.
         */
        MERGING,

        /**
         * Fired when the merge process was successful and all data has been
         * merged.
         */
        MERGED,

        /**
         * Fired when the merge process failed for some reason.
         */
        MERGE_FAILED,

        /**
         * Fired when a client is connected to the member.
         */
        CLIENT_CONNECTED,

        /**
         * Fired when a client is disconnected from the member.
         */
        CLIENT_DISCONNECTED,

        /**
         * Fired when a client is connected to a new cluster.
         */
        CLIENT_CHANGED_CLUSTER
    }
    
    LifecycleState getState();
}
