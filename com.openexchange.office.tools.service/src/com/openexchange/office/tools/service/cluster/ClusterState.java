/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.cluster;

public enum ClusterState {

    /**
     * In {@code ACTIVE} state, cluster will continue to operate without any restriction.
     * All operations are allowed. This is the default state of a cluster.
     */
    ACTIVE(true, true, true),
    
    /**
     * In {@code NO_MIGRATION} state of the cluster, migrations (partition rebalancing) and backup replications
     * are not allowed.
     * <ul>
     * <li>
     * When a new member joins, it will not be assigned any partitions until cluster state changes to {@link ClusterState#ACTIVE}.
     * </li>
     * <li>
     * When a member leaves, backups of primary replicas owned by that member will be promoted to primary.
     * But missing backup replicas will not be created/replicated until cluster state changes to {@link ClusterState#ACTIVE}.
     * </li>
     * </ul>
     *
     * Cluster will continue to operate without any restriction. All operations are allowed.
     */
    NO_MIGRATION(true, false, true),

	/**
     * In {@code FROZEN} state of the cluster:
     * <ul>
     * <li>
     * New members are not allowed to join, except the members left during {@code FROZEN} or {@link ClusterState#PASSIVE} state.
     * For example, cluster has 3 nodes; A, B and C in {@code FROZEN} state. If member B leaves
     * the cluster (either proper shutdown or crash), it will be allowed to re-join to the cluster.
     * But another member D, won't be able to join.
     * </li>
     * <li>
     * Partition table/assignments will be frozen. When a member leaves the cluster, its partition
     * assignments (as primary and backup) will remain the same, until either that member re-joins
     * to the cluster or {@code ClusterState} changes back to {@code ACTIVE}.
     * If that member re-joins while still in {@code FROZEN}, it will own all previously assigned partitions.
     * If {@code ClusterState} changes to {@code ACTIVE} then partition re-balancing process will
     * kick in and all unassigned partitions will be assigned to active members.
     * It's not allowed to change {@code ClusterState} to {@code FROZEN}
     * when there are pending migration/replication tasks in the system.
     * </li>
     * <li>
     * Nodes continue to stay in {@link NodeState#ACTIVE} state when cluster goes into the {@code FROZEN} state.
     * </li>
     * <li>
     * All other operations except migrations are allowed and will operate without any restriction.
     * </li>
     * </ul>
     */
    FROZEN(false, false, false),

    /**
     * In {@code PASSIVE} state of the cluster:
     * <ul>
     * <li>
     * New members are not allowed to join, except the members left during {@link ClusterState#FROZEN} or {@code PASSIVE} state.
     * </li>
     * <li>
     * Partition table/assignments will be frozen. It's not allowed to change {@code ClusterState}
     * to {@code PASSIVE} when there are pending migration/replication tasks in the system. If some
     * nodes leave the cluster while cluster is in {@code PASSIVE} state, they will be removed from the
     * partition table when cluster state moves back to {@link #ACTIVE}.
     * </li>
     * <li>
     * When cluster state is moved to {@code PASSIVE}, nodes are moved to {@link NodeState#PASSIVE} too.
     * Similarly when cluster state moves to another state from {@code PASSIVE}, nodes become
     * {@link NodeState#ACTIVE}.
     * </li>
     * <li>
     * All operations, except the ones marked with {@link AllowedDuringPassiveState},
     * will be rejected immediately.
     * </li>
     * </ul>
     */
    PASSIVE(false, false, false),

    /**
     * Shows that ClusterState is in transition. When a state change transaction is started,
     * ClusterState will be shown as {@code IN_TRANSITION} while the transaction is in progress.
     * After the transaction completes, cluster will be either in the new state or in the previous state,
     * depending on transaction result.
     * <p/>
     * This is a temporary & intermediate state, not allowed to be set explicitly.
     * <p/>
     * <ul>
     * <li>
     * Similarly to the {@code FROZEN} state, new members are not allowed
     * and migration/replication process will be paused.
     * </li>
     * <li>
     * If membership change occurs in the cluster, cluster state transition will fail
     * and will be reverted back to the previous state.
     * </li>
     * </ul>
     */
    IN_TRANSITION(false, false, false);

    private final boolean joinAllowed;
    private final boolean migrationAllowed;
    private final boolean partitionPromotionAllowed;

    ClusterState(boolean joinAllowed, boolean migrationAllowed, boolean partitionPromotionAllowed) {
        this.joinAllowed = joinAllowed;
        this.migrationAllowed = migrationAllowed;
        this.partitionPromotionAllowed = partitionPromotionAllowed;
    }

    /**
     * Returns {@code true}, if joining of a new member is allowed in this state.
     */
    public boolean isJoinAllowed() {
        return joinAllowed;
    }

    /**
     * Returns {@code true}, if migrations and replications are allowed in this state.
     */
    public boolean isMigrationAllowed() {
        return migrationAllowed;
    }

    /**
     * Returns {@code true}, if partition promotions are allowed in this state.
     */
    public boolean isPartitionPromotionAllowed() {
        return partitionPromotionAllowed;
    }	
}
