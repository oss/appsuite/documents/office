/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.cluster;

import java.util.Set;
import java.util.UUID;

public interface ClusterService {

	// For backward compatibility the rt2-prefix 
	public static final String CLUSTER_FULL_MEMBER_COUNT = "rt2.active-full-member-count";
	
	ClusterState getClusterState();
	
	String getLocalMemberUuid();
	
	Set<String> getAllMemberUuidsOfCluster();
	
	UUID getLocalMemberUuidAsUUID();

	void addMembershipListener(ClusterMembershipListener membershipListener);
	
	void addLifecycleListener(ClusterLifecycleListener lifecycleListener);
	
	int getCountMembers();
	
	boolean isLocalMemberLiteMember();
	
	boolean isRunning();
	
    int determineFullClusterMemberCount();
    
    String getFullClusterMemberListAsStr();
    
    void decreaseClusterFullMemberCount();
    
    void increaseClusterFullMemberCount();
    
    long getClusterFullMemberCount();
    
    boolean isActiveHzMember(String memberId);
}
