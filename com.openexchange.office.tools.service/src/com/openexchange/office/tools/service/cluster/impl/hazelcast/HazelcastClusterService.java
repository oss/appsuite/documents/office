/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.cluster.impl.hazelcast;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.cluster.Member;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.cp.IAtomicLong;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.cluster.ClusterLifecycleListener;
import com.openexchange.office.tools.service.cluster.ClusterMembershipListener;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.cluster.ClusterState;

@Service
@RegisteredService(registeredClass=ClusterService.class)
public class HazelcastClusterService implements ClusterService {

	@Autowired
	private HazelcastInstance hzInstance;
	
	@Override
	public String getLocalMemberUuid() {
		return hzInstance.getCluster().getLocalMember().getUuid().toString(); 
	}	

	@Override
	public UUID getLocalMemberUuidAsUUID() {
		return UUID.fromString(getLocalMemberUuid());
	}
	
	@Override
	public void addMembershipListener(ClusterMembershipListener membershipListener) {
		hzInstance.getCluster().addMembershipListener(new HazelcastClusterMembershipListenerProxy(membershipListener));
	}

	@Override
	public void addLifecycleListener(ClusterLifecycleListener lifecycleListener) {
		hzInstance.getLifecycleService().addLifecycleListener(new HazelcastClusterLifecycleListenerProxy(lifecycleListener));
	}	
	
	@Override
	public int getCountMembers() {
		return hzInstance.getCluster().getMembers().size();
	}
	
	@Override
	public boolean isLocalMemberLiteMember() {
		return hzInstance.getCluster().getLocalMember().isLiteMember();
	}

	@Override
	public boolean isRunning() {
		return hzInstance.getLifecycleService().isRunning();
	}

	@Override
	public ClusterState getClusterState() {
		return ClusterState.valueOf(hzInstance.getCluster().getClusterState().name());
	}

	@Override
	public int determineFullClusterMemberCount() {
		final Set<Member> members = hzInstance.getCluster().getMembers();
        int numberOfFullNodes = 0;
	    for (final Member member : members) {
	    	if (!member.isLiteMember()) {
	    		++numberOfFullNodes;
	    	}
	    }
        return numberOfFullNodes;
	}

	@Override
	public String getFullClusterMemberListAsStr() {
		final Set<Member> members = hzInstance.getCluster().getMembers();
		StringBuilder strBuilder = new StringBuilder();
        for (final Member member : members) {
        	strBuilder.append(member.getUuid());
        	strBuilder.append(", ");
        	strBuilder.append(member.getAddress().getHost() + ":" + member.getAddress().getPort());
        	strBuilder.append("\n");
        }
		return strBuilder.toString();
	}

	@Override
	public void decreaseClusterFullMemberCount() {
        IAtomicLong clusterMemberCount = hzInstance.getCPSubsystem().getAtomicLong(CLUSTER_FULL_MEMBER_COUNT);
        clusterMemberCount.decrementAndGet();
	}

	@Override
	public void increaseClusterFullMemberCount() {
        IAtomicLong clusterMemberCount = hzInstance.getCPSubsystem().getAtomicLong(CLUSTER_FULL_MEMBER_COUNT);
        clusterMemberCount.incrementAndGet();
	}

	@Override
	public long getClusterFullMemberCount() {
		IAtomicLong clusterMemberCount = hzInstance.getCPSubsystem().getAtomicLong(CLUSTER_FULL_MEMBER_COUNT);
		return clusterMemberCount.get();
	}

	@Override
	public boolean isActiveHzMember(String memberId) {
        if (StringUtils.isEmpty(memberId))
            return false;

        boolean isActive = false;
        final Set<Member> members = hzInstance.getCluster().getMembers();
        if (members != null) {
            isActive = members.stream().anyMatch(m -> memberId.equals(m.getUuid().toString()));
        }
        return isActive;
	}

	@Override
	public Set<String> getAllMemberUuidsOfCluster() {
        final Set<Member> currentMembers = hzInstance.getCluster().getMembers();
        final Set<String> currentMemberUUIDs = currentMembers.stream().map(m -> m.getUuid().toString()).collect(Collectors.toSet());
        return currentMemberUUIDs;
	}
}
