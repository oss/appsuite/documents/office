/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.encryption;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;

@Service
@RegisteredService
public class EncryptionInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(EncryptionInfoService.class);

	@Autowired
	private SessionService sessionService;
	
	@Autowired(required=false)
	private CryptographicServiceAuthenticationFactory cryptographicServiceAuthenticationFactory;
	
    public EncryptionInfo createEncryptionInfo(final AJAXRequestData request, String authCode) {

        String encryptionInfo = null;
        final HttpServletRequest servletRequest = request.optHttpServletRequest();

        if (null != servletRequest) {
            try {
                final Session session = sessionService.getSession4Id(servletRequest.getSession().getId());
                if (null != cryptographicServiceAuthenticationFactory) {
                    encryptionInfo = cryptographicServiceAuthenticationFactory.createAuthenticationFrom(session, authCode);
                }
            } catch (OXException e) {
                log.error("RTConnection: Exception caught trying to create encryptionInfo for reequest and authCode", e);
            }
        }

        return new EncryptionInfo(encryptionInfo);
    }

    public EncryptionInfo createEncryptionInfo(final Session session, String authCode) {

    	String encryptionInfo = null;

        if (null != session) {
            try {
                if (null != cryptographicServiceAuthenticationFactory) {
                    encryptionInfo = cryptographicServiceAuthenticationFactory.createAuthenticationFrom(session, authCode);
                }
            } catch (OXException e) {
                log.error("RTConnection: Exception caught trying to create encryptionInfo for reequest and authCode", e);
            }
        }
        return new EncryptionInfo(encryptionInfo);
    }
}
