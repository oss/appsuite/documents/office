/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config.impl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.OXException;
import com.openexchange.jslob.DefaultJSlob;
import com.openexchange.jslob.JSlobService;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.session.Session;

/**
 * A helper class to read/write user configuration entries in the
 * context of a specific user.
 *
 * @author Carsten Driesner
 */
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserConfigurationHelperImpl implements UserConfigurationHelper {

    private static final Logger LOG = LoggerFactory.getLogger(UserConfigurationHelperImpl.class);

    private JSlobService m_jslobService; 
    
    private Session m_session = null;
    private String m_id = null;
    private boolean m_bInit = false;    
    private JSONObject m_cache = null;
    private Mode m_mode = Mode.WRITE_THROUGH;    
    
    /**
     * Initializes a new {@link UserConfigurationHelperImpl}
     *
     * @param services
     *  The service lookup instance to be used by the new
     *  UserConfigurationHelper instance.
     *
     * @param session
     *  The session of the client in that context the user configuration data
     *  should be retrieved/set.
     *
     * @param id
     *  The root path for the configuration bundle, e.g. (io.ox/office). Must
     *  not be null.
     *
     * @param mode
     *  The work mode of the configuration helper instance. WRITE_THROUGH
     *  always reads/writes from the jslob configuration service, where
     *  WRITE_BACK caches data until flushCache() is called.
     */
	public UserConfigurationHelperImpl(JSlobService jslobService, Session session, String id, Mode mode) {
		this.m_jslobService = jslobService;
        m_session = session;
        m_id = id;
        m_mode = mode;
    }

	/**
	 * Retrieves a JSONObject from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  JSONObject.
	 *
	 * @return
	 *  The value as a JSONObject or null if the entry does not exists, or
	 *  the type is not a JSONObject.
	 */
	@Override
	public JSONObject getJSONObject(String path) {
        Object value = getGenericValue(path);
        return (value instanceof JSONObject) ? (JSONObject)value : null;
	}

	/**
	 * Retrieves a JSONArray from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  JSONArray.
	 *
	 * @return
	 *  The value as a JSONArray or null if the entry does not exists, or
	 *  the type is not a JSONArray.
	 */
	@Override
	public JSONArray getJSONArray(String path) {
		Object value = getGenericValue(path);
		return (value instanceof JSONArray) ? (JSONArray)value : null;
	}

	/**
	 * Retrieves a String from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @return
	 *  The value as a String or null if the entry does not exists, or
	 *  the type is not a String.
	 */
	@Override
	public String getString(String path) {
		Object value = getGenericValue(path);
		return (value instanceof String) ? (String)value : null;
	}

	/**
	 * Retrieves a String from the user configuration referenced by the
	 * provided relative path supporting a default value for missing
	 * entries.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @param defaultValue
	 *  The default value to be used if the entry is missing.
	 *
	 * @return
	 *  The value as a String or defaultValue if the entry does not
	 *  exists, or the type is not a String.
	 */
	@Override
	public String getString(String path, String defaultValue) {
		String value = getString(path);
		return (null == value) ? defaultValue : value;
	}
	
	/**
	 * Retrieves a Double from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  Double.
	 *
	 * @return
	 *  The value as a Double or null if the entry does not exists, or
	 *  the type is not a Double.
	 */
	@Override
	public Double getDouble(String path) {
		Object value = getGenericValue(path);
		return (value instanceof Double) ? (Double)value : null;
	}

	/**
	 * Retrieves a Double from the user configuration referenced by the
	 * provided relative path supporting a default value for missing
	 * entries.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @param defaultValue
	 *  The default value to be used if the entry is missing.
	 *
	 * @return
	 *  The value as a Double or null if the entry does not exists, or
	 *  the type is not a Double.
	 */
	@Override
    public Double getDouble(String path, Double defaultValue) {
    	Double value = getDouble(path);
		return (null == value) ? defaultValue : value;
    }

	/**
	 * Retrieves a Long from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  Long.
	 *
	 * @return
	 *  The value as a Long or null if the entry does not exists, or
	 *  the type is not a Long.
	 */
	@Override
	public Long getLong(String path) {
		Object value = getGenericValue(path);
		return ((value instanceof Long) || (value instanceof Integer)) ? (Long)value : null;
	}

	/**
	 * Retrieves a Long from the user configuration referenced by the
	 * provided relative path supporting a default value for missing
	 * entries.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @param defaultValue
	 *  The default value to be used if the entry is missing.
	 *
	 * @return
	 *  The value as a Long or null if the entry does not exists, or
	 *  the type is not a Long.
	 */
	@Override
    public Long getLong(String path, Long defaultValue) {
    	Long value = getLong(path);
		return (null == value) ? defaultValue : value;
    }

	/**
	 * Retrieves a Integer from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  Integer.
	 *
	 * @return
	 *  The value as a Integer or null if the entry does not exists, or
	 *  the type is not a Integer.
	 */
	@Override
	public Integer getInteger(String path) {
		Object value = getGenericValue(path);
		return (value instanceof Integer) ? (Integer)value : null;
	}

	/**
	 * Retrieves a Integer from the user configuration referenced by the
	 * provided relative path supporting a default value for missing
	 * entries.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @param defaultValue
	 *  The default value to be used if the entry is missing.
	 *
	 * @return
	 *  The value as a Integer or null if the entry does not exists, or
	 *  the type is not a Integer.
	 */
	@Override
    public Integer getInteger(String path, Integer defaultValue) {
    	Integer value = getInteger(path);
		return (null == value) ? defaultValue : value;
    }

	/**
	 * Retrieves a Boolean from the user configuration referenced by the
	 * provided relative path.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  Boolean.
	 *
	 * @return
	 *  The value as a Boolean or null if the entry does not exists, or
	 *  the type is not a Boolean.
	 */
	@Override
	public Boolean getBoolean(String path) {
		Object value = getGenericValue(path);
		return (value instanceof Boolean) ? (Boolean)value : null;
	}


	/**
	 * Retrieves a Boolean from the user configuration referenced by the
	 * provided relative path supporting a default value for missing
	 * entries.
	 *
	 * @param path
	 *  The relative path to he user configuration entry represented as a
	 *  String.
	 *
	 * @param defaultValue
	 *  The default value to be used if the entry is missing.
	 *
	 * @return
	 *  The value as a Boolean or null if the entry does not exists, or
	 *  the type is not a Boolean.
	 */
	@Override
    public Boolean getBoolean(String path, Boolean defaultValue) {
    	Boolean value = getBoolean(path);
		return (null == value) ? defaultValue : value;
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided boolean value.
	 *
     * @param value
     *  The new boolean value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
	public boolean setValue(String path, Boolean value) {
        return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided string value.
	 *
     * @param value
     *  The new string value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, String value) {
        return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided integer value.
	 *
     * @param value
     *  The new integer value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, Integer value) {
    	return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided long value.
	 *
     * @param value
     *  The new long value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, Long value) {
    	return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided double value.
	 *
     * @param value
     *  The new double value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, Double value) {
    	return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided JSONObject value.
	 *
     * @param value
     *  The new JSONObject value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, JSONObject value) {
    	return setGenericValue(path, value);
    }

    /**
     * Sets the value of the user configuration entry.
     *
     * @param path
	 *  The relative path to the user configuration entry which should get
	 *  the new provided JSONArray value.
	 *
     * @param value
     *  The new JSONArray value.
     *
     * @return
     *  TRUE if the value was set successfully, otherwise FALSE.
     */
	@Override
    public boolean setValue(String path, JSONArray value) {
    	return setGenericValue(path, value);
    }

    /**
     * Refreshes the cache content reading the user configuration again.
     * Be careful as the code doesn't check if the cache is dirty and
     * changes will be lost. Therefore call flushCache() before using
     * refreshCache().
     *
     * @return
     *  TRUE if the cache has been refreshed (also true if no cache is
     *  used -> mode = WRITE_THROUGH), otherwise FALSE.
     */
	@Override
    public boolean refreshCache() {
    	boolean result = false;

        if ((null != m_cache) && (null != m_jslobService)) {
        	try {
        	    m_cache = m_jslobService.get(m_id, m_session).getJsonObject();
        	    result = true;
        	} catch (OXException e) {
                LOG.error("RT connection: JSONException caught while reading data from JSlobService", e);
       	    }
        } else if (null == m_cache) {
        	result = true;
        }

        return result;
    }
    
    /**
     * Flushes the cache to the jslob configuration service.
     *
     * @return
     *  TRUE if writing back data was successful, otherwise FALSE.
     */
	@Override
    public boolean flushCache() {
    	boolean result = false;

    	if ((null != m_cache) && (null != m_jslobService)) {
    		try {
    		    m_jslobService.set(m_id, new DefaultJSlob(m_cache), m_session);
    		    m_cache = m_jslobService.get(m_id, m_session).getJsonObject();
    		    result = true;
    		} catch (OXException e) {
                LOG.error("RT connection: JSONException caught while writing data to JSlobService", e);
    		}
    	}

    	return result;
    }

    /**
     * Retrieves a value from the user configuration.
     *
     * @param path
     *  The relative path to the user configuration entry, e.g.
     *  "portal/recents" or "spreadsheet/maxCells".
     *
     * @return
     *  The value or null if the entry doesn't exists. The type of the value
     *  could be a simple type (Boolean, String, Double, Long, Integer), or a
     *  JSON type if the entry has a complex structure (JSONObject, JSONArray).
     */
	private Object getGenericValue(String path) {
        Object value = null;

        initOnDemand();
        if (null != path) {
            JSONObject configTree = readJSlobConfigTree();
            JSONObject jsonObject = getParentObject(path, configTree);
            String attrName = getAttributeNameFromPath(path);

            if ((null != attrName) && (null != jsonObject)) {
            	// retrieve value from the parent object
        	    value = jsonObject.opt(attrName);
            }
        }

        return value;
    }

	/**
	 * Sets a value to the configuration referenced by the provided path.
	 *
	 * @param path
	 *  The relative path to the configuration entry to be written into the
	 *  user configuration.
	 *
	 * @param value
	 *  The value that should be written to the configuration entry. It must
	 *  be a Boolean/Integer/String for simple types or a JSONObject/JSONArray
	 *  for complex types.
	 *
	 * @return
	 */
    private boolean setGenericValue(String path, Object value) {
        initOnDemand();

    	try {
            JSONObject configTree = readJSlobConfigTree();
            JSONObject parent = getParentObject(path, configTree);
            String attrName = getAttributeNameFromPath(path);

            if ((null != attrName) && (null != parent)) {
			    parent.put(attrName, value);
			    writeJSlobConfigTree(configTree);
			    return true;
            }
        } catch (JSONException e) {
            LOG.error("RT connection: JSONException caught while accessing JSlobService", e);
    	}
        return false;
    }

	/**
	 * Retrieves the parent object containing the referenced entry defined
	 * by the provided path.
	 *
	 * @param path
	 *  A relative path to the user configuration entry.
	 *
	 * @return
	 *  The JSONObject which contains the configuration entry or null if the
	 *  parent object does not exists.
	 */
	private JSONObject getParentObject(String path, JSONObject configTree) {
		JSONObject value = null;

		if (null != configTree) {

	        // cascade through the hierarchical structure to find the correct setting
            int i = 0;
            String[] entries = path.split("/");
            Object parent = configTree;

            while (i < (entries.length - 1)) {
                String entry = entries[i++];
                 // ignore empty strings
                if (entry.length() > 0) {
                    Object obj = configTree.opt(entry);
                    if (obj instanceof JSONObject) {
                        // step down on JSONObject
                    	configTree = (JSONObject)obj;
                        parent = configTree;
                    } else {
                    	parent = obj;
                    }
                }
            }

            if ((null != parent) && (parent instanceof JSONObject)) {
            	value = (JSONObject)parent;
            }
		}

		return value;
	}

	/**
	 *  Provides the attribute name of a relative configuration entry path.
	 *
	 * @param path
	 *  A relative configuration entry path.
	 *
	 * @return
	 *  The name of the attribute referenced by the provided path or null
	 *  if the attribute name cannot be determined.
	 */
	private String getAttributeNameFromPath(String path) {
		String attributeName = null;

		if ((null != path) && (path.length() > 0)) {
			int lastIndex = path.lastIndexOf('/');
			if ((lastIndex >= 0) && ((lastIndex + 1) < path.length())) {
				attributeName = path.substring(lastIndex+1);
			} else {
				attributeName = path;
			}
		}

		return attributeName;
	}

    /**
     * Initializes the instance on demand. Reading the configuration values
     * from the user configuration service.
     *
     * @return
     *  TRUE if the initialization was successful otherwise FALSE.
     */
    private boolean initOnDemand() {
        boolean result = false;

        if (!m_bInit && (null != m_session) && (null != m_id)) {
        	if (m_jslobService != null) {
        		try {            	
		        	// just to be sure that id exists and system works correctly
		            JSONObject cache = m_jslobService.get(m_id, m_session).getJsonObject();
		            if (m_mode == Mode.WRITE_BACK){
		            	m_cache = cache;
		            }
		            result = true;
            	}                
        		catch (OXException e) {
        			LOG.error("RT connection: Exception caught while accesing JSlobService", e);
        		}
        		m_bInit = true;
        	}
        }
        return result;
    }
    
    /**
     * Reads the configuration tree dependent on the mode. In mode WRITE_TROUGH
     * the data is automatically read from the jslob service, otherwise the
     * current cache data is returned.
     *
     * @return
     *  The JSONObject containing all configuration tree entries.
     */
    private JSONObject readJSlobConfigTree() {
    	try {
	    	if (null != m_cache) {
	    		return m_cache;
	    	} else {
	    		if (m_jslobService != null) {
	    			return m_jslobService.get(m_id, m_session).getJsonObject();
	    		}
	    	}
    	} catch (OXException e) {
    		LOG.error("RT connection: Exception caught while reading JSON config tree from JSlobService", e);
    	}
    	
    	return null;
    }
    
    /**
     * Writes the configuration tree dependent on the mode. In mode
     * WRITE_TROUGH the data is automatically stored via the jslob service,
     * otherwise the data is cached in the internal cache.
     * In case of mode WRITE_BACK, the data must be written back via the
     * jslob service using the flushCache() method.
     *
     * @param configTree
     *  The JSONObject containing all configuration tree entries.
     */
    private void writeJSlobConfigTree(JSONObject configTree) {
    	try {
    		if (null != m_cache) {
    			m_cache = configTree;
    		} else {
    			if (m_jslobService != null)
    				m_jslobService.set(m_id, new DefaultJSlob(configTree), m_session);    		
    		}
    	} catch (OXException e) {
    		LOG.error("RT connection: Exception caught while write JSON config tree to JSlobService", e);
    	}
    }
    
}
