/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.log.LoggingHelper;
import com.openexchange.office.tools.service.config.ConfigurationChecker;

/**
 *
 * Service to check certain configuration settings for inconsistency or
 * known to trigger issues regarding OX Documents usage.  
 *
 * @author carsten.driesner
 * @since 8.0
 *
 */
@Service
@RegisteredService(registeredClass=ConfigurationChecker.class)
public class ConfigurationCheckerImpl implements InitializingBean,  ConfigurationChecker {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationCheckerImpl.class);
    private static final String LOG_MSG_INTRO = "Warning: ConfigurationChecker detected a problematic configuration setting(s): ";

    public static final String MODE_DENY_ALL = "deny_all";
    public static final String MODE_STATIC   = "static"; // default
    public static final String MODE_INHERIT  = "inherit";

    private static final String OX_DOCS_CAPABILITIES[] = { "text","spreadsheet", "presentation" };
    private static final String CONFIG_CHECKER_STATE = "com.openexchange.office.checkConfiguration";

    @Autowired
    private ConfigurationService configService;

    AtomicBoolean initialzed = new AtomicBoolean(false);

    @Override
    public void afterPropertiesSet() throws Exception {
        if (initialzed.compareAndExchange(false, true)) {
            LOG.info("ConfigurationChecker initialized");
        }
    }

    /**
     * Checks the configuration settings for known problematic settings and
     * logs info or error messages in case anything has been found.
     * 
     * @return <code>true</code> in case no issues has been found in the configuration, 
     *  otherwise <code>false</code>.
     */
    @Override
    public boolean checkConfiguration() {
        if (!initialzed.get())
            throw new IllegalStateException("ConfigurationChecker not initalized!");

        // Check checkConfiguration configuration setting
        boolean noConfigIssuesFound = true;
        boolean runChecker = configService.getBoolProperty(CONFIG_CHECKER_STATE, true);
        if (runChecker) {
            noConfigIssuesFound &= checkForCorrectTransientSessionsAndGuestCapabilities();
        } else {
            LOG.info("ConfigurationChecker has been disabled.");
        }

        return noConfigIssuesFound;
    }

    /**
     * Checks for a consistent configuration regarding transient session handling and
     * sharing. Keeps track of guest capabilties and guest capability mode.
     *
     * @return <code>true</code> in case the configuration is known to work for
     *  sharing with guest users together with OX Documents, otherwise <code>false</code>.
     */
    private boolean checkForCorrectTransientSessionsAndGuestCapabilities() {
        boolean result = true;
        String logMessage = null;
        Level logLevel = Level.DEBUG;

        boolean transientSessions = configService.getBoolProperty("com.openexchange.share.transientSessions", true);
        if (transientSessions) {
            // in case we have transient session, we have to check further guest capabiltities
            String guestCapabilityMode = configService.getProperty("com.openexchange.share.guestCapabilityMode", MODE_STATIC);

            switch (guestCapabilityMode) {
                case MODE_STATIC: {
                    String staticGuestCapabilities = configService.getProperty("com.openexchange.share.staticGuestCapabilities", null);
                    if (!StringUtils.isEmpty(staticGuestCapabilities)) {
                        boolean guestHasOXDocsCapabilities = StringUtils.containsAny(staticGuestCapabilities, OX_DOCS_CAPABILITIES);
                        if (guestHasOXDocsCapabilities) {
                            logMessage = LOG_MSG_INTRO 
                                       + "com.openexchange.share.transientSessions=true AND the setting com.openexchange.share.staticGuestCapabilities "
                                       + "contains OX Documents values. This configuration combination can result in session errors for guest users in case "
                                       + "more than one Appsuite node is used. It's recommended in this case to use com.openexchange.share.transientSessions=false. "
                                       + "Please read the chapter \"Guest Login and Session Handling\" and section \"Session Lifecycle\" for more information.";
                            logLevel = Level.WARN;
                            result = false;
                        }
                    }
                    break;
                }
                case MODE_INHERIT: {
                    String permissions = configService.getProperty("permissions", null);
                    if (StringUtils.isNotEmpty(permissions)) {
                        boolean permissionsContainOXDocs = StringUtils.containsAny(permissions, OX_DOCS_CAPABILITIES);
                        if (permissionsContainOXDocs) {
                            logMessage = LOG_MSG_INTRO 
                                       + "com.openexchange.share.transientSessions=true AND com.openexchange.share.guestCapabilityMode=INHERIT_ALL AND "
                                       + "the permissions setting contains OX Documents permission. This configuration combination can result in session errors for guest "
                                       + "users in case more than one Appsuite node is used. It's recommended in this case to use com.openexchange.share.transientSessions=false. "
                                       + "Please read the chapter \"Guest Login and Session Handling\" and section \"Session Lifecycle\" for more information.";
                             logLevel = Level.WARN;
                             result = false;
                        }
                    }

                    if (logMessage == null) {
                        logMessage = LOG_MSG_INTRO
                                   + "com.openexchange.share.transientSessions=true AND com.openexchange.share.guestCapabilityMode=INHERIT_ALL is used AND "
                                   + "OX Documents bundles are active. You should keep in mind that it's recommended to set com.openexchange.share.transientSessions=false in "
                                   + "case sharing is used with guest users. If you encounter session issues together with guest sharing, please change this setting."
                                   + "Please read the chapter \"Guest Login and Session Handling\" and section \"Session Lifecycle\" for more information.";
                        logLevel = Level.WARN;
                        result = false;
                    }
                    break;
                }
                case MODE_DENY_ALL: {
                    result = true;
                    break;
                }
                default: {
                    result = false;
                    LOG.info("Unknown mode \"{}\" for com.openexchange.share.guestCapabilityMode found. ConfigurationChecker cannot correctly check settings!", guestCapabilityMode);
                }
            }
        }

        if (StringUtils.isNotEmpty(logMessage)) {
            LoggingHelper.log(LOG, logLevel, logMessage);
        }

        return result;
    }

}
