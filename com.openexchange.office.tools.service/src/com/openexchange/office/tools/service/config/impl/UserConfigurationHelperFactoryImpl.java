/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.jslob.JSlobService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.session.Session;

@Service
@RegisteredService(registeredClass=UserConfigurationFactory.class)
public class UserConfigurationHelperFactoryImpl implements UserConfigurationFactory, OsgiBundleContextAware, InitializingBean {

	private OsgiBundleContextAndActivator bundleContext;
	
	@Autowired
	private JSlobServiceRegistry jslobServiceRegistry;

	private JSlobService jslobService;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jslobService = jslobServiceRegistry.getJSlobService(ConfigurationHelper.DEFAULT_JSLOB_SERVICE_ID);
	}

	@Override
	public UserConfigurationHelper create(Session session, String rootPath, Mode mode) {
		UserConfigurationHelperImpl res = new UserConfigurationHelperImpl(jslobService, session, rootPath, mode);
		bundleContext.injectDependencies(res);
		return res;
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleContext = bundleCtx;
	}
}
