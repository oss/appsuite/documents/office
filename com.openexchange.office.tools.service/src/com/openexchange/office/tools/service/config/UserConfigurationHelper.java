/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config;

import org.json.JSONArray;
import org.json.JSONObject;

public interface UserConfigurationHelper {

	public enum Mode {
		WRITE_THROUGH,
		WRITE_BACK
	}

	public JSONObject getJSONObject(String path);
	public JSONArray getJSONArray(String path);
	public String getString(String path);
	public String getString(String path, String defaultValue);
	public Double getDouble(String path);
    public Double getDouble(String path, Double defaultValue);
	public Long getLong(String path);
    public Long getLong(String path, Long defaultValue);
	public Integer getInteger(String path);
    public Integer getInteger(String path, Integer defaultValue);
	public Boolean getBoolean(String path);
    public Boolean getBoolean(String path, Boolean defaultValue);
	public boolean setValue(String path, Boolean value);
    public boolean setValue(String path, String value);
    public boolean setValue(String path, Integer value);
    public boolean setValue(String path, Long value);
    public boolean setValue(String path, Double value);
    public boolean setValue(String path, JSONObject value);
    public boolean setValue(String path, JSONArray value);
    public boolean refreshCache();
    public boolean flushCache();

}
