/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.jslob.JSlob;
import com.openexchange.jslob.JSlobService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.session.Session;

@Service
@RegisteredService
public class ConfigurationHelper implements InitializingBean {

	public static final String DEFAULT_JSLOB_SERVICE_ID = "com.openexchange.jslob.config";

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationHelper.class);

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private JSlobServiceRegistry jSlobServiceRegistry;

    private JSlobService jslobService;

    @Override
	public void afterPropertiesSet() throws Exception {
    	this.jslobService = jSlobServiceRegistry.getJSlobService(DEFAULT_JSLOB_SERVICE_ID);
	}

	/**
     * Retrieves a office.properties boolean value using the cascade
     * configuration that supports to overwrite settings for a user.
     * A default value can be provided which is returned if the value
     * cannot be retrieved.
     *
     * @param serviceLookup
     *  The service lookup instance to be used for the retrieval.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval.
     *
     * @param settingPath
     *  The path including the setting name to be retrieved from the
     *  configuration, e.g //module
     *
     * @param defaultValue
     *  The default value to be used if the value cannot be retrieved.
     *
     * @return
     *  The value of the setting or defaultValue if the real value cannot
     *  be retrieved from the configuration.
     */
    public Boolean getBooleanOfficeConfigurationValue(Session session, String settingPath, Boolean defaultValue) {
        String strValue = null;

        strValue = getOfficeConfigurationValue(session, settingPath, null);
        if (null == strValue) {
            return defaultValue;
        } else {
            return Boolean.parseBoolean(strValue);
        }
    }

    /**
     * Retrieves a office.properties integer value using the cascade
     * configuration that supports to overwrite settings for a user.
     * A default value can be provided which is returned if the value
     * cannot be retrieved.
     *
     * @param serviceLookup
     *  The service lookup instance to be used for the retrieval.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval.
     *
     * @param settingPath
     *  The path including the setting name to be retrieved from the
     *  configuration, e.g //module
     *
     * @param defaultValue
     *  The default value to be used if the value cannot be retrieved.
     *
     * @return
     *  The value of the setting or defaultValue if the real value cannot
     *  be retrieved from the configuration.
     */
    public Integer getIntegerOfficeConfigurationValue(Session session, String settingPath, Integer defaultValue) {
        String strValue = null;

        strValue = getOfficeConfigurationValue(session, settingPath, null);
        if (null == strValue) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(strValue);
    	}
    	catch(NumberFormatException e) {
    		return defaultValue;
        }
    }

    /**
     * Retrieves a office.properties string value using the cascade
     * configuration that supports to overwrite settings for a user.
     * A default value can be provided which is returned if the value
     * cannot be retrieved.
     *
     * @param serviceLookup
     *  The service lookup instance to be used for the retrieval.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval.
     *
     * @param settingPath
     *  The path including the setting name to be retrieved from the
     *  configuration, e.g //module
     *
     * @param defaultValue
     *  The default value to be used if the value cannot be retrieved.
     *
     * @return
     *  The string value of the setting or defaultValue if the real value
     *  cannot be retrieved from the configuration.
     */
    public String getStringOfficeConfigurationValue(Session session, String settingPath, String defaultValue) {
        return getOfficeConfigurationValue(session, settingPath, defaultValue);
    }

    /**
     * Retrieves a office.properties value using the cascade configuration
     * that supports to overwrite settings for a user. A default value can
     * be provided which is returned if the value cannot be retrieved.
     *
     * @param serviceLookup
     *  The service lookup instance to be used for the retrieval.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval.
     *
     * @param settingPath
     *  The path including the setting name to be retrieved from the
     *  configuration, e.g //module/<setting-name>
     *
     * @param defaultValue
     *  The default value to be used if the value cannot be retrieved.
     *
     * @return
     *  The value of the setting or defaultValue if the real value cannot
     *  be retrieved from the configuration.
     */
    public String getOfficeConfigurationValue(Session session, String settingPath, String defaultValue) {
        String result = defaultValue;

        result = getCascadeConfigurationValue(session, "io.ox/office", settingPath);
        if (null == result) {
            result = defaultValue;
        }

        return result;
    }

    /**
     * Retrieves a properties value using the cascade configuration
     * that supports to overwrite settings for a user.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval. If the session is null so the user
     *  is unknown, the method uses the configuration service as fallback.
     *
     * @param configuration
     *  The name of the configuration file to used for the retrieval,
     *  e.g. io.ox/office
     *
     * @param path
     *  The path within the configuration file to retrieve the setting
     *  value, e.g. //module/maxTableCells
     *
     * @return
     *  The value of the setting or null if the setting is not available.
     */
    public String getCascadeConfigurationValue(Session session, String configuration, String path) {
        String result = null;

        if (null != configuration && null != path) {
            if ((null != session) && (null != jslobService)) {
                result = getCascadeConfigurationValue(jslobService, session, configuration, path);
            } else {
                String configurationFullPath = configuration + path;
                result = getConfigurationService().getProperty(configurationFullPath, (String)null);
            }
        }
        return result;
    }

    private ConfigurationService getConfigurationService() {
    	return configurationService;
    }
    
    /**
     * Retrieves a properties value using the cascade configuration
     * that supports to overwrite settings for a user.
     *
     * @param jslobService
     *  The jslob service lookup instance to be used for the retrieval.
     *
     * @param session
     *  The session of the user that defines the context for the cascade
     *  configuration value retrieval.
     *
     * @param configuration
     *  The name of the configuration file to used for the retrieval,
     *  e.g. io.ox/office
     *
     * @param path
     *  The path within the configuration file to retrieve the setting
     *  value, e.g. //module/maxTableCells
     *
     * @return
     *  The value of the setting or null if the setting is not available.
     */
    private String getCascadeConfigurationValue(JSlobService jslobService, Session session, String configuration, String path) {
        String result = null;

        if ((null != session) && (null != jslobService)) {
            try {
                final JSlob officeJSlob = jslobService.get(configuration, session);
                JSONObject json = null;

                if (null != officeJSlob) {
                    json = officeJSlob.getJsonObject();
                }

                // cascade through the hierarchical structure to find the correct setting
                if (null != json) {
                    int i = 0;
                    String[] entries = path.split("/");
                    while (i < entries.length) {
                        String entry = entries[i++];
                        // ignore empty strings
                        if (entry.length() > 0) {
                            Object obj = json.opt(entry);
                            if (null == obj) {
                                return null;
                            } else if (obj instanceof JSONObject) {
                                // step down on JSONObject
                                json = (JSONObject)obj;
                            } else {
                                // use other object types as return value
                                result = String.valueOf(obj);
                                break;
                            }
                        }
                    }
                }
            } catch (OXException e) {
                LOG.error("Exception caught while retrieving cascade configuration value", e);
            }
        }

        return result;
    }
}
