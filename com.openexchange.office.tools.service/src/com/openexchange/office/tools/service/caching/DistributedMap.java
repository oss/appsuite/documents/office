/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.caching;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.hazelcast.query.Predicate;

public interface DistributedMap<K, V> extends DistributedObject, ConcurrentMap<K, V>{
	
	void lock(K lockId);
	
	void unlock(K lockId);
	
	boolean isLocked(K lockId);

	boolean tryLock(K lockId, long waitTime, TimeUnit timeUnit) throws InterruptedException;

	boolean tryLock(K lockId, long waitTime, TimeUnit waitTimeUnit, long leaseTime, TimeUnit leaseTimeUnit) throws InterruptedException;

	void forceUnlock(K lockId);
	
	public Map<K, V> getAll(Set<K> keys);
	
	/**
	 * Hazelcast specific method
	 * TODO: Port to generic one
	 * @param predicate
	 * @return
	 */
	public Set<Map.Entry<K, V>> entrySet(Predicate<K, V> predicate);
}
