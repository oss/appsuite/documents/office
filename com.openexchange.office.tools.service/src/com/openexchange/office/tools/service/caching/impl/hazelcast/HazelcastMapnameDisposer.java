/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.caching.impl.hazelcast;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.config.MapConfig;
import com.hazelcast.core.HazelcastInstance;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.caching.DistributedMapnameDisposer;

@Service
@RegisteredService(registeredClass=DistributedMapnameDisposer.class)
public class HazelcastMapnameDisposer implements DistributedMapnameDisposer {

	@Autowired
	protected HazelcastInstance hzInstance;
	
	private Map<String, String> mapPrefixToMapName = new HashMap<>();
	
    //-------------------------------------------------------------------------
	/**
	 * Discovers map names in the supplied hazelcast configuration based on the map prefix.
	 *
	 * @param config The config object
	 * @return The prefix of the map name
	 */
	public String discoverMapName(String mapPrefix){
		if (mapPrefixToMapName.containsKey(mapPrefix)) {
			return mapPrefixToMapName.get(mapPrefix);
		}
	    Map<String, MapConfig> mapConfigs = hzInstance.getConfig().getMapConfigs();
	    if (null != mapConfigs && 0 < mapConfigs.size()) {
	        for (String mapName : mapConfigs.keySet()) {
	            if (mapName.startsWith(mapPrefix)) {
	            	mapPrefixToMapName.put(mapPrefix, mapName);
	                return mapName;
	            }
	        }
	    }
	    return null;
	}	

}
