/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.caching.impl.hazelcast;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.DistributedObjectUtil;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.cp.IAtomicLong;
import com.hazelcast.cp.lock.FencedLock;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;
import com.openexchange.office.tools.service.caching.DistributedLock;
import com.openexchange.office.tools.service.caching.DistributedMap;

@Service
@RegisteredService(registeredClass=CachingFacade.class)
public class HazelcastCachingFacade implements CachingFacade {
	
	@Autowired
	private HazelcastInstance hzInstance;

	@Override
	public <K, V> DistributedMap<K, V> getDistributedMap(String name, Class<K> keyClass, Class<V> valueClass) {
		return new HazelcastDistributedMap<>(hzInstance.getMap(name));
	}

	@Override
	public DistributedAtomicLong getDistributedAtomicLong(String name) {
		IAtomicLong atomicLong = hzInstance.getCPSubsystem().getAtomicLong(name);
		return new HazelcastDistributedAtomicLong(atomicLong);
	}
	
	@Override
	public Map<String, DistributedAtomicLong> getDistributedAtomicLongsWithPrefix(String prefix) {
		final Collection<DistributedObject> distributedObjects = hzInstance.getDistributedObjects();
        final Set<DistributedObject> atomicLongs = distributedObjects.stream()
                .filter(HazelcastCachingFacade::isAtomicLong)
                .filter(d -> DistributedObjectUtil.getName(d).startsWith(prefix))
                .collect(Collectors.toSet());
        Map<String, DistributedAtomicLong> res = new HashMap<>();
        atomicLongs.forEach(distObj -> res.put(distObj.getName(), new HazelcastDistributedAtomicLong((IAtomicLong) distObj)));
        return res;		
	}
	
	@Override
	public Set<DistributedLock> getDistributedLockWithPrefix(String prefix) {
		final Collection<DistributedObject> distributedObjects = hzInstance.getDistributedObjects();
        final Set<DistributedObject> locks = distributedObjects.stream()
                .filter(HazelcastCachingFacade::isLock)
                .filter(d -> DistributedObjectUtil.getName(d).startsWith(prefix))
                .collect(Collectors.toSet());
        return locks.stream().map(l -> new HazelcastDistributedLock((FencedLock) l)).collect(Collectors.toSet());
	}	

    public static boolean isAtomicLong(final DistributedObject distributedObject) {
        return (distributedObject instanceof IAtomicLong);
    }
    
    public static boolean isLock(final DistributedObject distributedObject) {
        return (distributedObject instanceof FencedLock);
    }
}
