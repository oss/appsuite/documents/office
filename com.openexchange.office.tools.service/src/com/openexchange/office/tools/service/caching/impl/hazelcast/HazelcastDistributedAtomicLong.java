/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.caching.impl.hazelcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.cp.IAtomicLong;
import com.hazelcast.spi.exception.DistributedObjectDestroyedException;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;

public class HazelcastDistributedAtomicLong implements DistributedAtomicLong {

	private static final Logger log = LoggerFactory.getLogger(HazelcastDistributedAtomicLong.class);
	
	private final IAtomicLong atomicLong;

	@Override
	public String getName() {
		return atomicLong.getName();
	}	
	
	public HazelcastDistributedAtomicLong(IAtomicLong atomicLong) {
		this.atomicLong = atomicLong;
	}
	
	public long incrementAndGet() {
		return atomicLong.incrementAndGet();
	}
	
	public long decrementAndGet() {
		return atomicLong.decrementAndGet();
	}
	
	public long get() {
		return atomicLong.get();
	}
	
	public void set(long newValue) {
		atomicLong.set(newValue);
	}
	
	public void destroy() {
		System.err.println("Destroying atomic long with name " + atomicLong.getName());
		log.error("Destroying atomic long with name " + atomicLong.getName(), new Exception("Destroying atomic long"));
		try {
			atomicLong.destroy();
		} catch (DistributedObjectDestroyedException ex) {
			log.error("Destroying AtomicLong with name " + atomicLong.getName() + ", exception: " + ex.getMessage(), ex);
		}
	}
}
