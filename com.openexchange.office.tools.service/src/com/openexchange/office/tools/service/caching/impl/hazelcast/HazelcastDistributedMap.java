/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.caching.impl.hazelcast;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.hazelcast.map.IMap;
import com.hazelcast.query.Predicate;
import com.openexchange.office.tools.service.caching.DistributedMap;

public class HazelcastDistributedMap<K, V> implements DistributedMap<K, V>{

	private final IMap<K, V> hzMap;

	public HazelcastDistributedMap(IMap<K, V> hzMap) {
		this.hzMap = hzMap;
	}	
	
	@Override
	public String getName() {
		return hzMap.getName();
	}
	
	@Override
	public Set<Entry<K, V>> entrySet(Predicate<K, V> predicate) {
		Set<Entry<K, V>> res = hzMap.entrySet(predicate);
		return res;
	}	
	
	@Override
	public Map<K, V> getAll(Set<K> keys) {
		return hzMap.getAll(keys);
	}
	
	@Override
	public void lock(K lockId) {
		hzMap.lock(lockId);		
	}

	@Override
	public void unlock(K lockId) {
		hzMap.unlock(lockId);
	}
	
	@Override
	public boolean isLocked(K lockId) {
		return hzMap.isLocked(lockId);
	}	
	
	@Override
	public boolean tryLock(K lockId, long waitTime, TimeUnit timeUnit) throws InterruptedException {
		return hzMap.tryLock(lockId, waitTime, timeUnit);
	}

	public boolean tryLock(K lockId, long waitTime, TimeUnit waitTimeUnit, long leaseTime, TimeUnit leaseTimeUnit) throws InterruptedException {
		return hzMap.tryLock(lockId, waitTime, waitTimeUnit, leaseTime, leaseTimeUnit);
	}

	@Override
	public void forceUnlock(K lockId) {
		hzMap.forceUnlock(lockId);
	}
		
	@Override
	public V putIfAbsent(K key, V value) {
		return hzMap.putIfAbsent(key, value);
	}

	@Override
	public boolean remove(Object key, Object value) {
		boolean res = hzMap.remove(key, value); 
		return res;
	}

	@Override
	public boolean replace(K key, V oldValue, V newValue) {
		boolean res = hzMap.replace(key, oldValue, newValue); 
		return res;
	}

	@Override
	public V replace(K key, V value) {
		return hzMap.replace(key, value);
	}

	@Override
	public int size() {
		return hzMap.size();
	}

	@Override
	public boolean isEmpty() {
		return hzMap.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return hzMap.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return hzMap.containsValue(value);
	}

	@Override
	public V get(Object key) {
		return hzMap.get(key);
	}

	@Override
	public V put(K key, V value) {
		return hzMap.put(key, value);
	}

	@Override
	public V remove(Object key) {
		return hzMap.remove(key);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		hzMap.putAll(m);
	}

	@Override
	public void clear() {
		hzMap.clear();
	}

	@Override
	public Set<K> keySet() {
		return hzMap.keySet();
	}

	@Override
	public Collection<V> values() {
		return hzMap.values();
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return hzMap.entrySet();
	}
}
