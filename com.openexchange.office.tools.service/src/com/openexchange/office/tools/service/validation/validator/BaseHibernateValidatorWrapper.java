/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.validator;

import java.lang.annotation.Annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;
import com.openexchange.office.tools.service.validation.validator.tools.AnnotationCopier;

public abstract class BaseHibernateValidatorWrapper<T, V extends ConstraintValidator<JavaXAnno, T>, OxAnno extends Annotation, JavaXAnno extends Annotation> implements ConstraintValidator<OxAnno, T> {
	
	private static final Logger log = LoggerFactory.getLogger(BaseHibernateValidatorWrapper.class);
	
	protected V hibernateValidator;
	
	@SuppressWarnings("serial")
	private TypeToken<V> hibernateValidatorTT = new TypeToken<V>(getClass()) {};
	
	@SuppressWarnings("serial")
	private TypeToken<OxAnno> oxAnnoTT = new TypeToken<OxAnno>(getClass()) {};
	
	@SuppressWarnings("serial")
	private TypeToken<JavaXAnno> javaXAnnoTT = new TypeToken<JavaXAnno>(getClass()) {};
	
	@Override
	public void initialize(OxAnno anno) {
		getHibernateValidator().initialize(AnnotationCopier.copyAnnotation(anno, getOxAnnotationClass(), getJavaxAnnotationClass()));
	}

	@Override
	public boolean isValid(T value, ConstraintValidatorContext ctx) {
		return getHibernateValidator().isValid(value, ctx);
	}
	
	@SuppressWarnings("unchecked")
	protected Class<V> getHibernateValidatorClass() {
		return (Class<V>) hibernateValidatorTT.getRawType();
	}
	
	@SuppressWarnings("unchecked")
	protected Class<OxAnno> getOxAnnotationClass() {
		return (Class<OxAnno>) oxAnnoTT.getRawType();
	}
	
	@SuppressWarnings("unchecked")
	protected Class<JavaXAnno> getJavaxAnnotationClass() {
		return (Class<JavaXAnno>) javaXAnnoTT.getRawType();
	}

	protected V getHibernateValidator() {
		if (hibernateValidator == null) {
			try {
				hibernateValidator = getHibernateValidatorClass().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
		return hibernateValidator;
	}
}
