/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.validator.tools;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.leangen.geantyref.AnnotationFormatException;
import io.leangen.geantyref.TypeFactory;

public class AnnotationCopier {
	
	private static final Logger log = LoggerFactory.getLogger(AnnotationCopier.class);
	
	private static Set<String> basicMethods = new HashSet<>();
	
	static {
		for (Method m : Annotation.class.getDeclaredMethods()) {
			basicMethods.add(m.getName());
		}
		for (Method m : Object.class.getDeclaredMethods()) {
			basicMethods.add(m.getName());
		}		
	}
	
	public static <S extends Annotation, T extends Annotation> T copyAnnotation(S sourceAnno, Class<S> sourceClass, Class<T> targetClass) {
		Method [] methods = sourceAnno.getClass().getMethods();
		Map<String, Object> params = new HashMap<>();
		for (Method method : methods) {
			if (method.isSynthetic()) {
				continue;
			}
			if (method.getParameterCount() > 0) {
				continue;
			}
			if (basicMethods.contains(method.getName())) {
				continue;
			}
			try {
				params.put(method.getName(), method.invoke(sourceAnno));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}			
		}		
		try {
			T res = TypeFactory.annotation(targetClass, params);
			return res;
		} catch (AnnotationFormatException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);			
		}				
	}
	
	private AnnotationCopier() {}
}
