/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.NegativeOrZero.List;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeOrZeroValidatorForShort;

/**
 * The annotated element must be a negative number or 0.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, {@code float},
 *     {@code double} and their respective wrappers</li>
 * </ul>
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomNegativeOrZeroValidatorForBigDecimal.class, CustomNegativeOrZeroValidatorForBigInteger.class, CustomNegativeOrZeroValidatorForByte.class,
				   CustomNegativeOrZeroValidatorForDouble.class, CustomNegativeOrZeroValidatorForFloat.class, CustomNegativeOrZeroValidatorForInteger.class,
				   CustomNegativeOrZeroValidatorForLong.class, CustomNegativeOrZeroValidatorForNumber.class, CustomNegativeOrZeroValidatorForShort.class})
public @interface NegativeOrZero {

	String message() default "{javax.validation.constraints.NegativeOrZero.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * Defines several {@link NegativeOrZero} constraints on the same element.
	 *
	 * @see NegativeOrZero
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		NegativeOrZero[] value();
	}
}
