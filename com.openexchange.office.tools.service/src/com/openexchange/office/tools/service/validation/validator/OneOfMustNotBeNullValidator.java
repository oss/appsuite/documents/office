/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.validator;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.tools.service.validation.annotation.OneOfMustNotBeNull;

public class OneOfMustNotBeNullValidator implements ConstraintValidator<OneOfMustNotBeNull, Object> {

	private static final Logger log = LoggerFactory.getLogger(OneOfMustNotBeNullValidator.class);
	
	private Set<String> attributes = new HashSet<>();
	
	@Override
	public void initialize(OneOfMustNotBeNull constraintAnnotation) {
		for (String str : constraintAnnotation.attributesToValidate()) {
			attributes.add(str);
		}
	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext ctx) {
		boolean foundNotNullValue = false;
		for (String attributeName : attributes) {
			try {
				Field field = obj.getClass().getDeclaredField(attributeName);
				field.setAccessible(true);
				foundNotNullValue = field.get(obj) != null;
				if (foundNotNullValue) {
					return true;
				}
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
		return false;
	}	
}
