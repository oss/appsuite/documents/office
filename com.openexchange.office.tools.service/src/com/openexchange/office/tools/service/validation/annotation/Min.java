/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.Min.List;
import com.openexchange.office.tools.service.validation.validator.CustomMinValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMinValidatorForShort;

/**
 * The annotated element must be a number whose value must be higher or
 * equal to the specified minimum.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, and their respective
 *     wrappers</li>
 * </ul>
 * Note that {@code double} and {@code float} are not supported due to rounding errors
 * (some providers might provide some approximative support).
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomMinValidatorForCharSequence.class, CustomMinValidatorForBigDecimal.class, CustomMinValidatorForBigInteger.class, CustomMinValidatorForByte.class,
				   CustomMinValidatorForDouble.class, CustomMinValidatorForFloat.class, CustomMinValidatorForInteger.class, CustomMinValidatorForLong.class,
				   CustomMinValidatorForNumber.class, CustomMinValidatorForShort.class})
public @interface Min {

	String message() default "{javax.validation.constraints.Min.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * @return value the element must be higher or equal to
	 */
	long value();

	/**
	 * Defines several {@link Min} annotations on the same element.
	 *
	 * @see Min
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		Min[] value();
	}
}
