/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.Max.List;
import com.openexchange.office.tools.service.validation.validator.CustomMaxValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.number.bound.CustomMaxValidatorForShort;

/**
 * The annotated element must be a number whose value must be lower or
 * equal to the specified maximum.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, and their respective
 *     wrappers</li>
 * </ul>
 * Note that {@code double} and {@code float} are not supported due to rounding errors
 * (some providers might provide some approximative support).
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomMaxValidatorForCharSequence.class, CustomMaxValidatorForBigDecimal.class, CustomMaxValidatorForBigInteger.class, 
				   CustomMaxValidatorForByte.class, CustomMaxValidatorForDouble.class, CustomMaxValidatorForFloat.class, CustomMaxValidatorForInteger.class,
				   CustomMaxValidatorForLong.class, CustomMaxValidatorForNumber.class, CustomMaxValidatorForShort.class})
public @interface Max {

	String message() default "{javax.validation.constraints.Max.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * @return value the element must be lower or equal to
	 */
	long value();

	/**
	 * Defines several {@link Max} annotations on the same element.
	 *
	 * @see Max
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		Max[] value();
	}
}
