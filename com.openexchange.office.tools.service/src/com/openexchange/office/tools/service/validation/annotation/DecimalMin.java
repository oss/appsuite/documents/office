/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.DecimalMin.List;
import com.openexchange.office.tools.service.validation.validator.CustomDecimalMinValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMinValidatorForShort;

/**
 * The annotated element must be a number whose value must be higher or
 * equal to the specified minimum.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code CharSequence}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, and their respective
 *     wrappers</li>
 * </ul>
 * Note that {@code double} and {@code float} are not supported due to rounding errors
 * (some providers might provide some approximative support).
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomDecimalMinValidatorForCharSequence.class, CustomDecimalMinValidatorForBigDecimal.class, CustomDecimalMaxValidatorForBigInteger.class,
				   CustomDecimalMinValidatorForByte.class, CustomDecimalMinValidatorForDouble.class, CustomDecimalMinValidatorForFloat.class,
				   CustomDecimalMinValidatorForInteger.class, CustomDecimalMinValidatorForLong.class, CustomDecimalMinValidatorForNumber.class,
				   CustomDecimalMinValidatorForShort.class})
public @interface DecimalMin {

	String message() default "{javax.validation.constraints.DecimalMin.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * The {@code String} representation of the min value according to the
	 * {@code BigDecimal} string representation.
	 *
	 * @return value the element must be higher or equal to
	 */
	String value();

	/**
	 * Specifies whether the specified minimum is inclusive or exclusive.
	 * By default, it is inclusive.
	 *
	 * @return {@code true} if the value must be higher or equal to the specified minimum,
	 *         {@code false} if the value must be higher
	 *
	 * @since 1.1
	 */
	boolean inclusive() default true;

	/**
	 * Defines several {@link DecimalMin} annotations on the same element.
	 *
	 * @see DecimalMin
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		DecimalMin[] value();
	}
}
