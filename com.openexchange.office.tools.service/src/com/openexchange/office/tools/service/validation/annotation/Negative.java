/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.Negative.List;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomNegativeValidatorForShort;

/**
 * The annotated element must be a strictly negative number (i.e. 0 is considered as an
 * invalid value).
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, {@code float},
 *     {@code double} and their respective wrappers</li>
 * </ul>
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
		validatedBy = {CustomNegativeValidatorForBigDecimal.class, CustomNegativeValidatorForBigInteger.class, CustomNegativeValidatorForByte.class,
					   CustomNegativeValidatorForDouble.class, CustomNegativeValidatorForFloat.class, CustomNegativeValidatorForInteger.class,
					   CustomNegativeValidatorForLong.class, CustomNegativeValidatorForNumber.class, CustomNegativeValidatorForShort.class})
public @interface Negative {

	String message() default "{javax.validation.constraints.Negative.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * Defines several {@link Negative} constraints on the same element.
	 *
	 * @see Negative
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		Negative[] value();
	}
}
