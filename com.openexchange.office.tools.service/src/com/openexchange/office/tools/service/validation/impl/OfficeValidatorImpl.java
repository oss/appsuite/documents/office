/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.impl;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.validation.OfficeValidator;
import com.openexchange.office.tools.service.validation.annotation.OneOfMustNotBeNull;
import com.openexchange.office.tools.service.validation.annotation.Valid;

@Service
@RegisteredService(registeredClass=OfficeValidator.class)
public class OfficeValidatorImpl implements OfficeValidator, InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(OfficeValidatorImpl.class);
	
	private Validator validator;
	
	@Override
	public void afterPropertiesSet() {
		try {
			ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
			validator = validatorFactory.getValidator();
		} catch (Throwable t) {
			log.error(t.getMessage(), t);
		}
	}

	@Override
	public <T> String validate(T obj) {				
		Set<ConstraintViolation<T>> mainValidationRes = validator.validate(obj); 
		if (mainValidationRes.isEmpty()) {
			Field [] fields = obj.getClass().getDeclaredFields();
			for (Field field : fields) {
				if (field.getAnnotation(Valid.class) != null) {
					if (Collection.class.isAssignableFrom(field.getType())) {
						field.setAccessible(true);						
						try {
							@SuppressWarnings({"rawtypes" })
							Collection col = (Collection) field.get(obj);
							for (Object colObj : col) {
								String tmp = validate(colObj);
								if (tmp != null) {
									return tmp;
								}
							};
						} catch (IllegalArgumentException | IllegalAccessException e) {
							throw new RuntimeException(e);
						}
						
					} else {
						if (Map.class.isAssignableFrom(field.getType())) {
							field.setAccessible(true);
							try {
								@SuppressWarnings("rawtypes")
								Map map = (Map) field.get(obj);
								for (Object key : map.keySet()) {
									String tmp = validate(key);
									if (tmp != null) {
										return tmp;
									}
									tmp = validate(map.get(key));
									if (tmp != null) {
										return tmp;
									}									
								}
							} catch (IllegalArgumentException | IllegalAccessException e) {
								throw new RuntimeException(e);
							}
						} else {
							throw new RuntimeException("Annotation com.openexchange.office.tools.service.validation.annotation.Valid is only allowed on fields of type collection or map.");
						}
					}
				}
			}
			return null;
		}
		StringBuilder strBuilder = new StringBuilder();
		boolean first = true;
		for (ConstraintViolation<T> constraintViolation : mainValidationRes) {
			if (first) {
				first = false;
			} else {
				strBuilder.append(", ");
			}
			if (constraintViolation.getMessageTemplate().equals("{com.openexchange.validation.constraints.OneOfMustNotBeNull.message}")) {
				OneOfMustNotBeNull oneOfMustNotBeNull = (OneOfMustNotBeNull) constraintViolation.getConstraintDescriptor().getAnnotation();
				strBuilder.append("One of these properties must not be null: ");
				strBuilder.append(Arrays.toString(oneOfMustNotBeNull.attributesToValidate()));
			} else {
				strBuilder.append(constraintViolation.getPropertyPath() + " " + constraintViolation.getMessage());
			}
		}
		return strBuilder.toString();
	}
}
