/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.Size.List;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArray;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfBoolean;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfByte;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfChar;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfDouble;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfFloat;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfInteger;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfLong;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForArraysOfShort;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForCollection;
import com.openexchange.office.tools.service.validation.validator.size.CustomSizeValidatorForMap;

/**
 * The annotated element size must be between the specified boundaries (included).
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code CharSequence} (length of character sequence is evaluated)</li>
 *     <li>{@code Collection} (collection size is evaluated)</li>
 *     <li>{@code Map} (map size is evaluated)</li>
 *     <li>Array (array length is evaluated)</li>
 * </ul>
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomSizeValidatorForArray.class, CustomSizeValidatorForArraysOfBoolean.class, CustomSizeValidatorForArraysOfByte.class, 
				   CustomSizeValidatorForArraysOfChar.class, CustomSizeValidatorForArraysOfDouble.class, CustomSizeValidatorForArraysOfFloat.class,
				   CustomSizeValidatorForArraysOfInteger.class, CustomSizeValidatorForArraysOfLong.class, CustomSizeValidatorForArraysOfShort.class,
				   CustomSizeValidatorForCharSequence.class, CustomSizeValidatorForCollection.class, CustomSizeValidatorForMap.class})
public @interface Size {

	String message() default "{javax.validation.constraints.Size.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * @return size the element must be higher or equal to
	 */
	int min() default 0;

	/**
	 * @return size the element must be lower or equal to
	 */
	int max() default Integer.MAX_VALUE;

	/**
	 * Defines several {@link Size} annotations on the same element.
	 *
	 * @see Size
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		Size[] value();
	}
}
