/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.Year;

import javax.validation.ClockProvider;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.openexchange.office.tools.service.validation.annotation.FutureOrPresent.List;
import com.openexchange.office.tools.service.validation.validator.time.futureorpresent.CustomFutureOrPresentValidatorForCalendar;
import com.openexchange.office.tools.service.validation.validator.time.futureorpresent.CustomFutureOrPresentValidatorForDate;
import com.openexchange.office.tools.service.validation.validator.time.futureorpresent.CustomFutureOrPresentValidatorForLocalDate;
import com.openexchange.office.tools.service.validation.validator.time.futureorpresent.CustomFutureOrPresentValidatorForLocalDateTime;
import com.openexchange.office.tools.service.validation.validator.time.futureorpresent.CustomFutureOrPresentValidatorForLocalTime;

/**
 * The annotated element must be an instant, date or time in the present or in the future.
 * <p>
 * <i>Now</i> is defined by the {@link ClockProvider} attached to the {@link Validator} or
 * {@link ValidatorFactory}. The default {@code clockProvider} defines the current time
 * according to the virtual machine, applying the current default time zone if needed.
 * <p>
 * The notion of present here is defined relatively to the type on which the constraint is
 * used. For instance, if the constraint is on a {@link Year}, present would mean the whole
 * current year.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code java.util.Date}</li>
 *     <li>{@code java.util.Calendar}</li>
 *     <li>{@code java.time.LocalDate}</li>
 *     <li>{@code java.time.LocalDateTime}</li>
 *     <li>{@code java.time.LocalTime}</li>
 * </ul>
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
		validatedBy = {CustomFutureOrPresentValidatorForCalendar.class, CustomFutureOrPresentValidatorForDate.class, CustomFutureOrPresentValidatorForLocalDate.class,
					   CustomFutureOrPresentValidatorForLocalDateTime.class, CustomFutureOrPresentValidatorForLocalTime.class})
public @interface FutureOrPresent {

	String message() default "{javax.validation.constraints.FutureOrPresent.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * Defines several {@link FutureOrPresent} annotations on the same element.
	 *
	 * @see FutureOrPresent
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		FutureOrPresent[] value();
	}
}
