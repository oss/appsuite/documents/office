/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.NotEmpty.List;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArray;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfBoolean;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfByte;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfChar;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfDouble;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfFloat;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfInt;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfLong;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForArraysOfShort;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForCollection;
import com.openexchange.office.tools.service.validation.validator.notempty.CustomNotEmptyValidatorForMap;

/**
 * The annotated element must not be {@code null} nor empty.
 * <p>
 * Supported types are:
 * <ul>
 * <li>{@code CharSequence} (length of character sequence is evaluated)</li>
 * <li>{@code Collection} (collection size is evaluated)</li>
 * <li>{@code Map} (map size is evaluated)</li>
 * <li>Array (array length is evaluated)</li>
 * </ul>
 */
@Documented
@Constraint(
	validatedBy = {CustomNotEmptyValidatorForArray.class, CustomNotEmptyValidatorForArraysOfBoolean.class, CustomNotEmptyValidatorForArraysOfByte.class, 
				   CustomNotEmptyValidatorForArraysOfChar.class, CustomNotEmptyValidatorForArraysOfDouble.class, CustomNotEmptyValidatorForArraysOfFloat.class,
				   CustomNotEmptyValidatorForArraysOfInt.class, CustomNotEmptyValidatorForArraysOfLong.class, CustomNotEmptyValidatorForArraysOfShort.class,
				   CustomNotEmptyValidatorForCharSequence.class, CustomNotEmptyValidatorForCollection.class, CustomNotEmptyValidatorForMap.class})
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface NotEmpty {

	String message() default "{javax.validation.constraints.NotEmpty.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * Defines several {@code @NotEmpty} constraints on the same element.
	 *
	 * @see NotEmpty
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	public @interface List {
		NotEmpty[] value();
	}
}
