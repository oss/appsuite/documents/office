/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.DecimalMax.List;
import com.openexchange.office.tools.service.validation.validator.CustomDecimalMaxValidatorForCharSequence;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.decimal.bound.CustomDecimalMaxValidatorForShort;

/**
 * The annotated element must be a number whose value must be lower or
 * equal to the specified maximum.
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code CharSequence}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, and their respective
 *     wrappers</li>
 * </ul>
 * Note that {@code double} and {@code float} are not supported due to rounding errors
 * (some providers might provide some approximative support).
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
	validatedBy = {CustomDecimalMaxValidatorForCharSequence.class, CustomDecimalMaxValidatorForBigDecimal.class, CustomDecimalMaxValidatorForBigInteger.class,
				   CustomDecimalMaxValidatorForByte.class, CustomDecimalMaxValidatorForDouble.class, CustomDecimalMaxValidatorForFloat.class,
				   CustomDecimalMaxValidatorForInteger.class, CustomDecimalMaxValidatorForLong.class, CustomDecimalMaxValidatorForNumber.class,
				   CustomDecimalMaxValidatorForShort.class})
public @interface DecimalMax {

	String message() default "{javax.validation.constraints.DecimalMax.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * The {@code String} representation of the max value according to the
	 * {@code BigDecimal} string representation.
	 *
	 * @return value the element must be lower or equal to
	 */
	String value();

	/**
	 * Specifies whether the specified maximum is inclusive or exclusive.
	 * By default, it is inclusive.
	 *
	 * @return {@code true} if the value must be lower or equal to the specified maximum,
	 *         {@code false} if the value must be lower
	 *
	 * @since 1.1
	 */
	boolean inclusive() default true;

	/**
	 * Defines several {@link DecimalMax} annotations on the same element.
	 *
	 * @see DecimalMax
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		DecimalMax[] value();
	}
}
