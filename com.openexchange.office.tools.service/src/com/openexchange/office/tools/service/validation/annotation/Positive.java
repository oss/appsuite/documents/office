/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.validation.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.openexchange.office.tools.service.validation.annotation.Positive.List;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForBigDecimal;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForBigInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForByte;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForDouble;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForFloat;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForInteger;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForLong;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForNumber;
import com.openexchange.office.tools.service.validation.validator.number.sign.CustomPositiveValidatorForShort;

/**
 * The annotated element must be a strictly positive number (i.e. 0 is considered as an
 * invalid value).
 * <p>
 * Supported types are:
 * <ul>
 *     <li>{@code BigDecimal}</li>
 *     <li>{@code BigInteger}</li>
 *     <li>{@code byte}, {@code short}, {@code int}, {@code long}, {@code float},
 *     {@code double} and their respective wrappers</li>
 * </ul>
 * <p>
 * {@code null} elements are considered valid.
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(
		validatedBy = {CustomPositiveValidatorForBigDecimal.class, CustomPositiveValidatorForBigInteger.class, CustomPositiveValidatorForByte.class,
					   CustomPositiveValidatorForDouble.class, CustomPositiveValidatorForFloat.class, CustomPositiveValidatorForInteger.class,
					   CustomPositiveValidatorForLong.class, CustomPositiveValidatorForNumber.class, CustomPositiveValidatorForShort.class})
public @interface Positive {

	String message() default "{javax.validation.constraints.Positive.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

	/**
	 * Defines several {@link Positive} constraints on the same element.
	 *
	 * @see Positive
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		Positive[] value();
	}
}
