/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.osgi;

import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.common.log.SpecialLoggerInfo;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.service.logging.CachedAppender;
import com.openexchange.office.tools.service.logging.LogErrorPublisher;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class ToolsServicesOsgiContextAndActivator extends OsgiBundleContextAndActivator {

	public ToolsServicesOsgiContextAndActivator() {
		super(new ToolsServicesOsgiBundleContextIdentificator());
	}

	@Override
	protected Set<Class<?>> getAdditionalNeededServices() {
		Set<Class<?>> res = new HashSet<>();
		res.add(HazelcastInstance.class);
		res.add(JmsTemplateWithoutTtl.class);
		return res;
	}

	@Override
	protected void onPreCreateInstances() throws OXException {
		CachedAppender cachedAppender = addCachedLogbackAppender();
		SpecialLogService specialLogService = new SpecialLogService(cachedAppender);
		registerService(specialLogService, true);
		registerServiceToOsgi(specialLogService, SpecialLogService.class);
		addLogErrorPublischer(specialLogService);
	}

	private CachedAppender addCachedLogbackAppender() {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		CachedAppender cachedAppender = new CachedAppender();
		cachedAppender.setContext(lc);
		cachedAppender.start();

		ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(SpecialLoggerInfo.SPECIAL_LOGGER_NAME);
		logbackLogger.setLevel(Level.ALL);
		logbackLogger.setAdditive(false);
		logbackLogger.addAppender(cachedAppender);
		return cachedAppender;
	}

	private LogErrorPublisher addLogErrorPublischer(SpecialLogService specialLogService) {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		LogErrorPublisher logErrorPublisher = new LogErrorPublisher(specialLogService);
		logErrorPublisher.setContext(lc);
		logErrorPublisher.start();

		ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		logbackLogger.setLevel(Level.INFO);
		logbackLogger.setAdditive(false);
		logbackLogger.addAppender(logErrorPublisher);
		return logErrorPublisher;
	}
}
