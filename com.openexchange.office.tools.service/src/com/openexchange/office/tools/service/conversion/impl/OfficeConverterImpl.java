/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.conversion.impl;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.conversion.ConversionException;
import com.openexchange.office.tools.service.conversion.ObjectMapperService;
import com.openexchange.office.tools.service.conversion.OfficeConverter;
import com.openexchange.office.tools.service.conversion.RequestDataIsNullException;
import com.openexchange.office.tools.service.http.ParamaterBasedRequestData;

@Service
@RegisteredService(registeredClass=OfficeConverter.class)
public class OfficeConverterImpl implements OfficeConverter {

	private static final Logger log = LoggerFactory.getLogger(OfficeConverterImpl.class);
	
	public static final String KEY_REQUESTDATA = "requestdata";
	
	@Autowired
	private ObjectMapperService objectMapperService;
	
	@Override
	public <T> T convert(ParamaterBasedRequestData requestData, Class<T> targetClass) throws ConversionException, RequestDataIsNullException {
		String requestBodyData = requestData.getParameter(KEY_REQUESTDATA);
		if (StringUtils.isBlank(requestBodyData)) {
			log.info("requestdata is blank or null");
			throw new RequestDataIsNullException();
		}
		T res; 
		try {
			res = objectMapperService.getObjectMapper().readValue(requestBodyData, targetClass);
			return res;
		} catch (JsonParseException | JsonMappingException e) {
			log.info(e.getMessage());
			throw new ConversionException(String.class, targetClass, e);
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
}
