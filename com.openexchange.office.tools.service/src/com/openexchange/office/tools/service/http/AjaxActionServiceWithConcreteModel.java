/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.http;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.reflect.TypeToken;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionConstants;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.http.HttpModelConverterRegistry;
import com.openexchange.office.tools.service.conversion.ConversionException;
import com.openexchange.office.tools.service.conversion.OfficeConverter;
import com.openexchange.office.tools.service.conversion.RequestDataIsNullException;
import com.openexchange.office.tools.service.validation.OfficeValidator;
import com.openexchange.tools.session.ServerSession;

public abstract class AjaxActionServiceWithConcreteModel<TO, DO> implements AJAXActionService {

	@Autowired
	private OfficeConverter converter;
	
	@Autowired
	private OfficeValidator validator;

	@Autowired
	private HttpModelConverterRegistry modelConverter;	
	
	@SuppressWarnings("serial")
	private TypeToken<TO> ajaxRequestModelTT = new TypeToken<TO>(getClass()) {};
	
	@SuppressWarnings("serial")
	private TypeToken<DO> dataModelTT = new TypeToken<DO>(getClass()) {};	
	
	@Override
	public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        if ((null == session) || (session.isAnonymous())) {
    		AJAXRequestResult res = new AJAXRequestResult();
    		res.setHttpStatusCode(HttpStatusCode.UNAUTHORIZED.getStatusCode());
    		return res;
        }	
		try {
			ParamaterBasedRequestData paramaterBasedRequestData = extractConreteRequestedata(requestData);
			@SuppressWarnings("unchecked")
			TO transferModel = (TO) ajaxRequestModelTT.getRawType().cast(converter.convert(paramaterBasedRequestData, ajaxRequestModelTT.getRawType()));
			String validationRes = validator.validate(transferModel);
			if (validationRes != null) {
				JSONObject jsonResult = new JSONObject();
				ErrorCode errorCode = ExceptionToErrorCode.map(new OXException(OXExceptionConstants.CODE_DEFAULT, validationRes), ErrorCode.GENERAL_ARGUMENTS_ERROR, false);
				ErrorCode errorCode2 = new ErrorCode(errorCode.getCode(), errorCode.getCodeAsStringConstant(), validationRes, errorCode.getErrorClass());
				try {jsonResult.put("error", errorCode2.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
				return new AJAXRequestResult(jsonResult);
			}
			@SuppressWarnings("unchecked")
			DO dataModel = (DO) modelConverter.convert(session, dataModelTT.getRawType(), transferModel);
			
			return perform(dataModel, requestData, session);
		} catch (RequestDataIsNullException ex) {
			JSONObject jsonResult = new JSONObject();
            ErrorCode errorCode = ExceptionToErrorCode.map(new OXException(OXExceptionConstants.CODE_DEFAULT, ex.getMessage()), ErrorCode.GENERAL_ARGUMENTS_ERROR, false);
            try {jsonResult.put("error", errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);			 
		} catch (ConversionException ex) {
			JSONObject jsonResult = new JSONObject();
            ErrorCode errorCode = ExceptionToErrorCode.map(new OXException(OXExceptionConstants.CODE_DEFAULT, ex.getMessage()), ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {jsonResult.put("error", errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);			
		}		
	}
	
	protected ParamaterBasedRequestData extractConreteRequestedata(AJAXRequestData requestData) throws ConversionException {
		return new SimpleAjaxRequestDataWrapper(requestData);
	}
	
	protected AJAXRequestResult perform(DO requestModel, AJAXRequestData requestData, ServerSession session) throws OXException {
		AJAXRequestResult res = new AJAXRequestResult();
		res.setHttpStatusCode(HttpStatusCode.OK.getStatusCode());
		return res;
	}
}
