/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.http;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.http.HttpModelConverter;
import com.openexchange.office.tools.common.http.HttpModelConverterRegistry;
import com.openexchange.session.Session;

@Service
@RegisteredService(registeredClass=HttpModelConverterRegistry.class)
public class ConcreteHttpModelConverterRegistry implements HttpModelConverterRegistry {

	private static final Logger log = LoggerFactory.getLogger(ConcreteHttpModelConverterRegistry.class);
	
	private Map<RegistryKey, HttpModelConverter<?, ?>> registry = new HashMap<>();
	
	@Override
	public <DO, TO> void registerConverter(HttpModelConverter<DO, TO> converter) {
		RegistryKey registryKey = new RegistryKey(converter.getDOClass(), converter.getTOClass());
		registry.put(registryKey, converter);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <DO, TO> HttpModelConverter<DO, TO> getConverter(Class<DO> to, Class<TO> from) {
		RegistryKey registryKey = new RegistryKey(to, from);
		return (HttpModelConverter<DO, TO>) registry.get(registryKey);
	}

	@Override
	public <DO, TO> DO convert(Session session, Class<DO> toClass, TO from) throws OXException {
		@SuppressWarnings("unchecked")
		HttpModelConverter<DO, TO> converter = (HttpModelConverter<DO, TO>) getConverter(toClass, from.getClass());		
		DO res = converter.convert(session, from);
		log.info("Converted transfer object {} to data object {}", from, res);
		return res;
	}	
	
	private class RegistryKey {
		private final Class<?> to;
		private final Class<?> from;
		
		private RegistryKey(Class<?> to, Class<?> from) {
			this.to = to;
			this.from = from;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((from == null) ? 0 : from.hashCode());
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RegistryKey other = (RegistryKey) obj;
			if (from == null) {
				if (other.from != null)
					return false;
			} else if (!from.getName().equals(other.from.getName()))
				return false;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.getName().equals(other.to.getName()))
				return false;
			return true;
		}
	}
}
