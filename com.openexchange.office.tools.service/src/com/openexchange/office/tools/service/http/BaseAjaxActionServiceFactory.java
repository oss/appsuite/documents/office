/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.http;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.reflect.TypeToken;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXActionServiceFactory;
import com.openexchange.ajax.requesthandler.DispatcherNotes;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;

public abstract class BaseAjaxActionServiceFactory<T extends AJAXActionService> implements AJAXActionServiceFactory, OsgiBundleContextAware {

	protected final Map<String, AJAXActionService> actions = new HashMap<>();
	
	protected abstract String getAjaxModuleId();

	private AtomicBoolean contextInitialized = new AtomicBoolean(false);

	@SuppressWarnings("serial")
	private TypeToken<T> baseClassOfActionsTT = new TypeToken<T>(getClass()) {};

	@Override
	public AJAXActionService createActionService(final String action) {
	    return (actions.get(action));
	}

	public Collection<? extends AJAXActionService> getSupportedServices() {
	    return Collections.unmodifiableCollection(actions.values());
	}
	
	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		if (contextInitialized.compareAndSet(false, true)) {
			@SuppressWarnings("unchecked")
			Collection<T> documentRESTActions = (Collection<T>) bundleCtx.getServicesOfType(baseClassOfActionsTT.getRawType());
			documentRESTActions.forEach(act -> {
				if (act.getClass().getAnnotation(DispatcherNotes.class) != null) {
					actions.put(act.getClass().getSimpleName().replace("Action", "").toLowerCase(), new AJAXActionServiceMDCWrapperWithDispatcherNotes(act));
				} else {
					actions.put(act.getClass().getSimpleName().replace("Action", "").toLowerCase(), new AJAXActionServiceMDCWrapper(act));
				}
			});
			bundleCtx.registerAjaxActionServiceFactoryToOSGI(this, getAjaxModuleId(),  true);
		}
	}	
}
