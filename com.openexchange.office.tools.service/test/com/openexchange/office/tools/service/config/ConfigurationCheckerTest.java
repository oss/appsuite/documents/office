/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.service.config;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;
import com.openexchange.office.tools.service.config.impl.ConfigurationCheckerImpl;
import com.openexchange.office.tools.service.util.ToolsServiceTestOsgiBundleContextAndUnitTestActivator;

public class ConfigurationCheckerTest {
    private static final String CONFIG_CHHECK_CONFIGURARTION = "com.openexchange.office.checkConfiguration";
    private static final String CONFIG_SHARE_TRANSIENT_SESSIONS = "com.openexchange.share.transientSessions";
    private static final String CONFIG_SHARE_GUEST_CAP_MODE = "com.openexchange.share.guestCapabilityMode";
    private static final String CONFIG_SHARE_STATIC_GUEST_CAP = "com.openexchange.share.staticGuestCapabilities";
    private static final String CONFIG_PERMISSIONS = "permissions";

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    @BeforeEach
    public void init() throws OXException {
        UnittestInformation unitTestInformation = new ConfigUnittestInformation();
        unitTestBundle = new ToolsServiceTestOsgiBundleContextAndUnitTestActivator(unitTestInformation);
        unitTestBundle.start();
    }

    @Test
    public void testGoodConfiguration_CheckWithCheckDisabled() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, false, true, ConfigurationCheckerImpl.MODE_STATIC, "text, presenation, spreadsheet");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertTrue(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testGoodConfiguration_NoTransientSessions() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, false, ConfigurationCheckerImpl.MODE_DENY_ALL, "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertTrue(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testGoodConfiguration_TransientSessionsAndNoGuestSharing() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_DENY_ALL, "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertTrue(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testGoodConfiguration_TransientSessionsAndGuestSharingWithoutDocuments_EmptyString() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_STATIC, "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertTrue(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testGoodConfiguration_TransientSessionsAndGuestSharingWithoutDocuments_FilledString() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_STATIC, "document_preview");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertTrue(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndGuestSharingWithDocuments() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_STATIC, "document_preview, text, presentation, spreadsheet");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndGuestSharingWithInherit_WithNonSetPermissions() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_INHERIT, "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndGuestSharingWithInherit_WithEmptyPermissions() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_INHERIT, "", "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndGuestSharingWithInherit_WithoutDocPermissions() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_INHERIT, "", "document_preview");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndGuestSharingWithInherit_WithDocPermissions() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, ConfigurationCheckerImpl.MODE_INHERIT, "", "document_preview, text, presentation, spreadsheet");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @Test
    public void testProblemConfiguration_TransientSessionsAndUnknownGuestSharingMode() {
        ConfigurationService configService = unitTestBundle.getService(ConfigurationService.class);
        setupConfigServiceMocks(configService, true, true, "unknown", "");

        try {
            ConfigurationChecker configChecker = preparedConfigurationChecker();
            boolean result = configChecker.checkConfiguration();
            assertFalse(result);
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    private void setupConfigServiceMocks(ConfigurationService configService, boolean enableCheckConfig, boolean transientSessions, String guestCapMode, String staticGuestCap) {
        Mockito.when(configService.getBoolProperty(Mockito.eq(CONFIG_CHHECK_CONFIGURARTION), Mockito.anyBoolean())).thenReturn(Boolean.valueOf(enableCheckConfig));
        Mockito.when(configService.getBoolProperty(Mockito.eq(CONFIG_SHARE_TRANSIENT_SESSIONS), Mockito.anyBoolean())).thenReturn(Boolean.valueOf(transientSessions));
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE))).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE), Mockito.anyString())).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE), Mockito.isNull())).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP))).thenReturn(staticGuestCap);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP), Mockito.anyString())).thenReturn(staticGuestCap);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP), Mockito.isNull())).thenReturn(staticGuestCap);
    }

    private void setupConfigServiceMocks(ConfigurationService configService, boolean enableCheckConfig, boolean transientSessions, String guestCapMode, String staticGuestCap, String permissions) {
        Mockito.when(configService.getBoolProperty(Mockito.eq(CONFIG_CHHECK_CONFIGURARTION), Mockito.anyBoolean())).thenReturn(Boolean.valueOf(enableCheckConfig));
        Mockito.when(configService.getBoolProperty(Mockito.eq(CONFIG_SHARE_TRANSIENT_SESSIONS), Mockito.anyBoolean())).thenReturn(Boolean.valueOf(transientSessions));
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE))).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE), Mockito.anyString())).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_GUEST_CAP_MODE), Mockito.isNull())).thenReturn(guestCapMode);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP))).thenReturn(staticGuestCap);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP), Mockito.anyString())).thenReturn(staticGuestCap);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_SHARE_STATIC_GUEST_CAP), Mockito.isNull())).thenReturn(staticGuestCap);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_PERMISSIONS))).thenReturn(permissions);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_PERMISSIONS), Mockito.anyString())).thenReturn(permissions);
        Mockito.when(configService.getProperty(Mockito.eq(CONFIG_PERMISSIONS), Mockito.isNull())).thenReturn(permissions);
    }

    private ConfigurationChecker preparedConfigurationChecker() throws Exception {
        ConfigurationCheckerImpl checkConfig = new ConfigurationCheckerImpl();
        unitTestBundle.injectDependencies(checkConfig);
        checkConfig.afterPropertiesSet();
        return checkConfig;
    }
}
