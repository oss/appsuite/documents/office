package com.openexchange.office.tools.service.util;

import org.mockito.Mockito;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;
import com.openexchange.office.tools.service.osgi.ToolsServicesOsgiBundleContextIdentificator;


public class ToolsServiceTestOsgiBundleContextAndUnitTestActivator extends TestOsgiBundleContextAndUnitTestActivator {

    public ToolsServiceTestOsgiBundleContextAndUnitTestActivator(UnittestInformation unittestInformation) {
        super(new ToolsServicesOsgiBundleContextIdentificator(), unittestInformation);
    }
    
    @Override
    protected Object createMock(Class<?> clazz) {
        return Mockito.mock(clazz);
    }

}
