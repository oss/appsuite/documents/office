/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.message.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.message.OperationHelper;

public class OperationHelperTest {

	@Test
	public void testGetIndexOfOperationWithOSNLargerThan() throws Exception {
		// create simple operations array
		final JSONArray opsArray1 = createJSONArrayWithOperations(1, 10);

		final int index1 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray1, 5);
		assertTrue(index1 == 5);

		// create operations array with ops with duplicate osn
		final JSONArray opsArray2 = new JSONArray();
		for (int i = 1; i <= 10; i++) {
			for (int j = 0; j < 5; j++) {
				opsArray2.put(createOperationsObject(i));
			}
		}
		final int index2 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray2, 5);
		assertTrue(index2 == 25);

		// filter out all operations
		final int index3 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray2, 10);
		assertTrue(index3 == -1);

		// filter out no operation
		final int index4 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray2, 0);
		assertTrue(index4 == 0);

		// filter out operation
		final int index5 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray2, 9);
		assertTrue(index5 == 45);

		// empty operations array
		final JSONArray emptyOpsArray = new JSONArray();
		final int index6 = OperationHelper.getIndexOfOperationWithOSNLargerThan(emptyOpsArray, 1);
		assertTrue(index6 == -1);

		// create a ops array with gaps - should be no problem at all
		final JSONArray opsArray3 = new JSONArray();
		opsArray3.put(createOperationsObject(1));
		opsArray3.put(createOperationsObject(3));
		opsArray3.put(createOperationsObject(5));
		opsArray3.put(createOperationsObject(7));
		opsArray3.put(createOperationsObject(9));
		final int index7 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray3, 5);
		assertTrue(index7 == 3);

		// create a ops array which is NOT sorted
		final JSONArray opsArray4 = new JSONArray();
		opsArray4.put(createOperationsObject(9));
		opsArray4.put(createOperationsObject(7));
		opsArray4.put(createOperationsObject(3));
		opsArray4.put(createOperationsObject(8));
		opsArray4.put(createOperationsObject(5));
		final int index8 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray4, 5);
		assertTrue(index8 == 0);

		// create a ops array with "bad" json objects
		final JSONArray opsArray5 = new JSONArray();
		final JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("test", 1);
		opsArray5.put(jsonObj1);
		final int index9 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray5, 5);
		assertTrue(index9 == -1);

		// create a ops array with "bad" json objects
		final JSONArray opsArray6 = new JSONArray();
		final JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put(OperationHelper.KEY_OSN, "invalid number!");
		opsArray6.put(jsonObj2);
		final int index10 = OperationHelper.getIndexOfOperationWithOSNLargerThan(opsArray6, 5);
		assertTrue(index10 == -1);
	}

	@Test
	public void testFilterOperationsWithOSNSmallerEqualThan() throws Exception {
		// create simple operations array
		final JSONArray opsArray1 = createJSONArrayWithOperations(1, 10);

		final JSONArray filteredOpsArray1 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray1, 5);
		assertTrue(filteredOpsArray1.length() == 5);
		assertTrue(getOSN(filteredOpsArray1.getJSONObject(0)) == 6);

		// create operations array with ops with duplicate osn
		final JSONArray opsArray2 = new JSONArray();
		for (int i = 1; i <= 10; i++) {
			for (int j = 0; j < 5; j++) {
				opsArray2.put(createOperationsObject(i));
			}
		}
		final JSONArray filteredOpsArray2 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray2, 5);
		assertTrue(filteredOpsArray2.length() == 25);
		assertTrue(getOSN(filteredOpsArray1.getJSONObject(0)) == 6);

		// filter out all operations
		final JSONArray filteredOpsArray3 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray2, 10);
		assertTrue(filteredOpsArray3.length() == 0);

		// filter out no operation
		final JSONArray filteredOpsArray4 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray2, 0);
		assertTrue(filteredOpsArray4.length() == 50);
		assertTrue(getOSN(filteredOpsArray4.getJSONObject(0)) == 1);

		// filter out operation
		final JSONArray filteredOpsArray5 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray2, 9);
		assertTrue(filteredOpsArray5.length() == 5);
		assertTrue(getOSN(filteredOpsArray5.getJSONObject(0)) == 10);

		// empty operations array
		final JSONArray emptyOpsArray = new JSONArray();
		final JSONArray filteredOpsArray6 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(emptyOpsArray, 1);
		assertTrue(filteredOpsArray6.length() == 0);

		// create a ops array with gaps - should be no problem at all
		final JSONArray opsArray3 = new JSONArray();
		opsArray3.put(createOperationsObject(1));
		opsArray3.put(createOperationsObject(3));
		opsArray3.put(createOperationsObject(5));
		opsArray3.put(createOperationsObject(7));
		opsArray3.put(createOperationsObject(9));
		final JSONArray filteredOpsArray7 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray3, 5);
		assertTrue(filteredOpsArray7.length() == 2);
		assertTrue(getOSN(filteredOpsArray7.getJSONObject(0)) == 7);

		// create a ops array which is NOT sorted
		final JSONArray opsArray4 = new JSONArray();
		opsArray4.put(createOperationsObject(9));
		opsArray4.put(createOperationsObject(7));
		opsArray4.put(createOperationsObject(3));
		opsArray4.put(createOperationsObject(8));
		opsArray4.put(createOperationsObject(5));
		final JSONArray filteredOpsArray8 = OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray4, 5);
		assertTrue(filteredOpsArray8.length() == 3);
		assertTrue(getOSN(filteredOpsArray8.getJSONObject(0)) == 9);

		// check invalid cases
		final JSONObject jsonObj1 = new JSONObject();
		final JSONObject jsonObj2 = new JSONObject();

		jsonObj1.put("test", 1);
		jsonObj2.put(OperationHelper.KEY_OSN, "invalid number!");

		try {
			final JSONArray opsArray5 = new JSONArray();
			// create a ops array with "bad" json objects
			opsArray5.put(jsonObj1);
			OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray5, 5);

			fail("Bad operations object in operations array must lead to JSONException!");
		} catch (JSONException e) {
			// this is the assumed case
		}

		try {
			final JSONArray opsArray5 = new JSONArray();
			// create a ops array with "bad" json objects
			opsArray5.put(jsonObj2);
			OperationHelper.filterOperationsWithOSNSmallerEqualThan(opsArray5, 5);

			fail("Bad operations object in operations array must lead to JSONException!");
		} catch (JSONException e) {
			// this is the assumed case
		}
	}

	@Test
	public void testHasOperationWithOSNLargerThan() throws Exception {
		// create simple operations array
		final JSONArray opsArray1 = createJSONArrayWithOperations(1, 10);

		final boolean result1 = OperationHelper.hasOperationWithOSNLargerThan(opsArray1, 5);
		assertTrue(result1);

		// create operations array with ops with duplicate osn
		final JSONArray opsArray2 = new JSONArray();
		for (int i = 1; i <= 10; i++) {
			for (int j = 0; j < 5; j++) {
				opsArray2.put(createOperationsObject(i));
			}
		}
		final boolean result2 = OperationHelper.hasOperationWithOSNLargerThan(opsArray2, 5);
		assertTrue(result2);

		// filter out all operations
		final boolean result3 = OperationHelper.hasOperationWithOSNLargerThan(opsArray2, 10);
		assertFalse(result3);

		// filter out no operation
		final boolean result4 = OperationHelper.hasOperationWithOSNLargerThan(opsArray2, 0);
		assertTrue(result4);

		// filter out operation
		final boolean result5 = OperationHelper.hasOperationWithOSNLargerThan(opsArray2, 9);
		assertTrue(result5);

		// empty operations array
		final JSONArray emptyOpsArray = new JSONArray();
		final boolean result6 = OperationHelper.hasOperationWithOSNLargerThan(emptyOpsArray, 1);
		assertFalse(result6);

		// create a ops array with gaps - should be no problem at all
		final JSONArray opsArray3 = new JSONArray();
		opsArray3.put(createOperationsObject(1));
		opsArray3.put(createOperationsObject(3));
		opsArray3.put(createOperationsObject(5));
		opsArray3.put(createOperationsObject(7));
		opsArray3.put(createOperationsObject(9));
		final boolean result7 = OperationHelper.hasOperationWithOSNLargerThan(opsArray3, 5);
		assertTrue(result7);

		// create a ops array which is NOT sorted - thisdoesn't work for hasOperationWithOSNLargerThan
		// which is optimized for sorted ops arrays only!
		final JSONArray opsArray4 = new JSONArray();
		opsArray4.put(createOperationsObject(9));
		opsArray4.put(createOperationsObject(7));
		opsArray4.put(createOperationsObject(3));
		opsArray4.put(createOperationsObject(8));
		opsArray4.put(createOperationsObject(5));
		final boolean result8 = OperationHelper.hasOperationWithOSNLargerThan(opsArray4, 5);
		assertFalse(result8);

		// create a ops array with "bad" json objects
		final JSONArray opsArray5 = new JSONArray();
		final JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("test", 1);
		opsArray5.put(jsonObj1);
		final boolean result9 = OperationHelper.hasOperationWithOSNLargerThan(opsArray5, 5);
		assertFalse(result9);

		// create a ops array with "bad" json objects
		final JSONArray opsArray6 = new JSONArray();
		final JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put(OperationHelper.KEY_OSN, "invalid number!");
		opsArray6.put(jsonObj2);
		final boolean result10 = OperationHelper.hasOperationWithOSNLargerThan(opsArray6, 5);
		assertFalse(result10);
	}

	@Test
	public void testCountOperationsWithOSNLargerThan() throws Exception {
		// create simple operations array
		final JSONArray opsArray1 = createJSONArrayWithOperations(1, 10);

		final int count1 = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 5, true);
		assertTrue(count1 == 5);

		// create operations array with ops with duplicate osn
		final JSONArray opsArray2 = new JSONArray();
		for (int i = 1; i <= 10; i++) {
			for (int j = 0; j < 5; j++) {
				opsArray2.put(createOperationsObject(i));
			}
		}
		final int count2 = OperationHelper.countOperationsWithOSNLargerThan(opsArray2, 5, true);
		assertTrue(count2 == 25);

		// filter out all operations
		final int count3 = OperationHelper.countOperationsWithOSNLargerThan(opsArray2, 10, true);
		assertTrue(count3 == 0);

		// filter out no operation
		final int count4 = OperationHelper.countOperationsWithOSNLargerThan(opsArray2, 0, true);
		assertTrue(count4 == 50);

		// filter out operation
		final int count5 = OperationHelper.countOperationsWithOSNLargerThan(opsArray2, 9, true);
		assertTrue(count5 == 5);

		// empty operations array
		final JSONArray emptyOpsArray = new JSONArray();
		final int count6 = OperationHelper.countOperationsWithOSNLargerThan(emptyOpsArray, 1, true);
		assertTrue(count6 == 0);

		// create a ops array with gaps - should be no problem at all
		final JSONArray opsArray3 = new JSONArray();
		opsArray3.put(createOperationsObject(1));
		opsArray3.put(createOperationsObject(3));
		opsArray3.put(createOperationsObject(5));
		opsArray3.put(createOperationsObject(7));
		opsArray3.put(createOperationsObject(9));
		final int count7 = OperationHelper.countOperationsWithOSNLargerThan(opsArray3, 5, true);
		assertTrue(count7 == 2);

		// create a ops array which is NOT sorted - this doesn't work for countOperationsWithOSNLargerThan
		// which is optimized for sorted ops arrays only!
		final JSONArray opsArray4 = new JSONArray();
		opsArray4.put(createOperationsObject(9));
		opsArray4.put(createOperationsObject(7));
		opsArray4.put(createOperationsObject(3));
		opsArray4.put(createOperationsObject(8));
		opsArray4.put(createOperationsObject(5));
		final int count8 = OperationHelper.countOperationsWithOSNLargerThan(opsArray4, 5, true);
		// countOperations starts from the end and counts until a smaller number is found
		// therefore 0 is correct here
		assertTrue(count8 == 0);

		// create a ops array with "bad" json objects
		final JSONArray opsArray5 = new JSONArray();
		final JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("test", 1);
		opsArray5.put(jsonObj1);
		final int count9 = OperationHelper.countOperationsWithOSNLargerThan(opsArray5, 5, true);
		assertTrue(count9 == 0);

		// create a ops array with "bad" json objects
		final JSONArray opsArray6 = new JSONArray();
		final JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put(OperationHelper.KEY_OSN, "invalid number!");
		opsArray6.put(jsonObj2);
		final int count10 = OperationHelper.countOperationsWithOSNLargerThan(opsArray6, 5, true);
		assertTrue(count10 == 0);
	}

    @Test
    public void testCountOperationsWithOSNLargerThanAndNonModifyingOps() throws Exception {
        // create simple operations array
        final JSONArray opsArray1 = createJSONArrayWithOperations(1, 10);

        final JSONObject obj9 = opsArray1.getJSONObject(9);
        obj9.put(OperationHelper.KEY_MODIFYING, false);
        int countT = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 5, true);
        int countF = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 5, false);
        assertTrue(countT == 5);
        assertTrue(countF == 4); // one non-modify op
        int countOpsT = OperationHelper.countOperations(opsArray1, true);
        int countOpsF = OperationHelper.countOperations(opsArray1, false);
        assertTrue(countOpsT == 10);
        assertTrue(countOpsF == 9); // one non-modify op

        final JSONObject obj0 = opsArray1.getJSONObject(0);
        obj0.put(OperationHelper.KEY_MODIFYING, false);
        countT = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 5, true);
        countF = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 5, false);
        assertTrue(countT == 5);
        assertTrue(countF == 4); // one non-modify op
        countOpsT = OperationHelper.countOperations(opsArray1, true);
        countOpsF = OperationHelper.countOperations(opsArray1, false);
        assertTrue(countOpsT == 10);
        assertTrue(countOpsF == 8); // two non-modify op
        countT = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 0, true);
        countF = OperationHelper.countOperationsWithOSNLargerThan(opsArray1, 0, false);
        assertTrue(countT == 10);
        assertTrue(countF == 8); // two non-modify op

        // edge cases
        countOpsT = OperationHelper.countOperations(null, true);
        countOpsF = OperationHelper.countOperations(null, false);
        countT = OperationHelper.countOperationsWithOSNLargerThan(null, 0, true);
        countF = OperationHelper.countOperationsWithOSNLargerThan(null, 0, false);
        assertTrue(countOpsT == 0);
        assertTrue(countOpsF == 0);
        assertTrue(countT == 0);
        assertTrue(countF == 0);

        countOpsT = OperationHelper.countOperations(new JSONArray(), true);
        countOpsF = OperationHelper.countOperations(new JSONArray(), false);
        countT = OperationHelper.countOperationsWithOSNLargerThan(new JSONArray(), 0, true);
        countF = OperationHelper.countOperationsWithOSNLargerThan(new JSONArray(), 0, false);
        assertTrue(countOpsT == 0);
        assertTrue(countOpsF == 0);
        assertTrue(countT == 0);
        assertTrue(countF == 0);
    }

	private JSONArray createJSONArrayWithOperations(int startOSN, int endOSN) throws JSONException, IllegalArgumentException {
		if ((startOSN < 0 ) || (endOSN < 0) || (startOSN > endOSN)) {
            throw new IllegalArgumentException("startOSN must be greater/equal to endOSN and both must be positive");
        }

		final JSONArray opsArray = new JSONArray();
		for (int i = startOSN; i <= endOSN; i++) {
			opsArray.put(createOperationsObject(i));
		}

		return opsArray;
	}

	private JSONObject createOperationsObject(int osn) throws JSONException {
		final JSONObject json = new JSONObject();
		json.put(OperationHelper.KEY_OSN, osn);
		json.put(OperationHelper.KEY_OPL, 1);
		return json;
	}

	private int getOSN(JSONObject opsObject) throws JSONException {
		return opsObject.getInt(OperationHelper.KEY_OSN);
	}

}
