/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.message;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.tools.common.TimeZoneHelper;
import com.openexchange.office.tools.common.json.JSONDebugWriter;
import com.openexchange.office.tools.common.json.JSONHelper;

public class OperationHelper {

	static protected final Logger log = LoggerFactory.getLogger(OperationHelper.class);
	
    // - Constants -------------------------------------------------------------

    public final static String OP_NAME_INVALID    = "_invalid_operation_";

    public final static String KEY_OSN            = "osn";
    public final static String KEY_OPL            = "opl";
    public static final String KEY_CLIENT_ID      = "clientId";
    public static final String KEY_UNIQUE_ID      = "uniqueId";
    public static final String KEY_FILE_NAME      = "fileName";
    public static final String KEY_FILE_VERSION   = "fileVersion";
    public static final String KEY_TARGET_FOLDER  = "targetFolder";
    public static final String KEY_FILE_ID        = "fileId";
    public static final String KEY_FOLDER_ID      = "folderId";
    public static final String KEY_AS_TEMPLATE    = "asTemplate";
    public static final String KEY_HTMLDOCUMENT   = "htmlDoc";
    public static final String KEY_ERROR_DATA     = "error";
    public static final String KEY_ACTIONS        = "actions";
    public static final String KEY_ACTION         = "action";
    public static final String KEY_HAS_ERRORS     = "hasErrors";
    public static final String KEY_MIMETYPE       = "file_mimetype";
    public static final String KEY_OPERATIONS     = "operations";
    public static final String KEY_FILEDESCRIPTOR = "file";
    public static final String KEY_SENDER         = "sender";
    public static final String KEY_PREVIEW        = "preview";
    public static final String KEY_ACTIVESHEET    = "activeSheet";
    public static final String KEY_NAME           = "name";
    public static final String KEY_MODIFYING      = "modifying";

    /**
     * Comparing, if two positions in the form of JSONArrays have the same
     * parents, grand parents, ... parentLevel = 1 means, both positions
     * have the same parent. parentLevel = 2 means, the grand parents are
     * identical and so on.
     *
     * @param position1
     *  The first position to be compared
     *
     * @param position2
     *  The second position to be compared
     *
     * @param parentLevel
     *  The level, until which the ancestors will be compared. parentLevel = 1
     *  means, that the direct parents must be identical.
     *
     * @return
     *  If the two positions have the same ancestor until the specified
     *  level, true is returned. Otherwise false
     */
    static private boolean hasSameParentComponent(JSONArray position1, JSONArray position2, int parentLevel) {

        boolean sameParent = true;

        if ((position1 == null) || (position2 == null)) {
            return false;
        }

        int length = position1.length();

        if ((length < parentLevel) || (length != position2.length())) {
            return false;
        }

        for (int index = length - parentLevel - 1; index >= 0; index -= 1) {

            try {
                if (position1.getInt(index) != position2.getInt(index)) {
                    return false;
                }
            } catch (JSONException e) {
                return false;
            }
        }

        return sameParent;
    }

    /**
     * Trying to merge two directly following insertText operations. If the
     * merge is possible the operation 'next' that follows the operation
     * 'last' must not be added to the operation array. Therefore the return
     * value 'insertNextOp' will be set to false in the case of successful
     * merging. In this case the content of the operation 'last' will be
     * modified.
     *
     * @param last
     *  The preceding insertText operation. This was already added to the
     *  operations array. Therefore it will be modified within this operation.
     *  And therefore it is necessary, that the insertText operations are
     *  copied before they are added to the operations array, so that the
     *  original operations are not modified.
     *
     * @param position2
     *  The following insertText operation. If merging this two operations
     *  is possible, then this operation will not be added to the operations
     *  array. Therefore the return value of this function is set to
     *  'insertNextOp' false.
     *
     * @return
     *  If the two insertText operations can be merged, it is not necessary
     *  to add the second operation to the operations array. Therefore the
     *  return value will be false. Otherwise it is true.
     *
     * @throws JSONException
     */
    static private boolean mergeInsertOperations(JSONObject last, JSONObject next) throws JSONException {

        boolean insertNextOp = true;

        int parentLevel = 1;
        String nextText = next.getString(OCKey.TEXT.value());
        JSONArray nextStart = next.getJSONArray(OCKey.START.value());
        JSONArray lastStart = last.getJSONArray(OCKey.START.value());
        // additional requirement is to check if operations have target, and if they are equal
        String lastTarget = last.optString(OCKey.TARGET.value(), "");
        String nextTarget = next.optString(OCKey.TARGET.value(), "");

        if ((lastStart != null) && (nextStart != null) && (hasSameParentComponent(lastStart, nextStart, parentLevel) && lastTarget.equals(nextTarget))) {

            int lastTextPosStart = lastStart.getInt(lastStart.length() - 1);
            int nextTextPosStart = nextStart.getInt(nextStart.length() - 1);

            JSONObject nextAttrs = null;
            JSONObject lastAttrs = null;

            try {
                nextAttrs = next.getJSONObject(OCKey.ATTRS.value());
            } catch (Exception e) {
                nextAttrs = null;
            }

            try {
                lastAttrs = last.getJSONObject(OCKey.ATTRS.value());
            } catch (Exception e) {
                lastAttrs = null;
            }

            if ((nextAttrs == null) && (!((lastAttrs != null) && (lastAttrs.has(OCKey.CHANGES.value()))))) {

            	// TODO: Furter optimization: Both have 'changes' attribute and next has no other attributes

                String lastText = last.getString(OCKey.TEXT.value());

                if ((lastTextPosStart + lastText.length() == nextTextPosStart)) {
                    // the new text is inserted directly behind an already existing text
                    last.put(OCKey.TEXT.value(), lastText + nextText);
                    insertNextOp = false;
                } else if ((lastTextPosStart <= nextTextPosStart) && (nextTextPosStart <= lastTextPosStart + lastText.length())) {
                    // the new text is inserted into an already existing text
                    int relativeStart = nextTextPosStart - lastTextPosStart;
                    last.put(OCKey.TEXT.value(), lastText.substring(0, relativeStart) + nextText + lastText.substring(relativeStart));
                    insertNextOp = false;
                }
            }
        }

        return insertNextOp;
    }

    /**
     * Trying to merge an insertText operation with a directly following delete
     * operation. If the merge is possible the 'delete' operation 'next' that
     * follows the insertText operation 'last' must not be added to the operation
     * array. Therefore the return value 'insertNextOp' will be set to false in
     * the case of successful merging. In this case the content of the operation
     * 'last' will be modified.
     *
     * @param last
     *  The preceding insertText operation. This was already added to the operations
     *  array. Therefore it will be modified within this operation. And therefore it
     *  is necessary, that the insertText operations are copied before they are added
     *  to the operations array, so that the original operations are not modified.
     *
     * @param next
     *  The following delete operation. If merging this two operations is possible,
     *  then this operation will not be added to the operations array. Therefore
     *  the return value of this function is set to 'insertNextOp' false.
     *
     * @return
     *  If the 'insertText' and the 'delete' operations can be merged, it
     *  is not necessary to add the 'delete' operation to the operations array.
     *  Therefore the return value will be false. Otherwise it is true.
     *
     * @throws JSONException
     */
    static private boolean mergeInsertAndDeleteOperations(JSONObject last, JSONObject next) throws JSONException {

        boolean insertNextOp = true;
        int parentLevel = 1;
        JSONArray nextStart = next.getJSONArray(OCKey.START.value());
        JSONArray lastStart = last.getJSONArray(OCKey.START.value());
        // delete has an optional end parameter
        JSONArray nextEnd = null;
        // additional requirement is to check if operations have target, and if they are equal
        String lastTarget = last.optString(OCKey.TARGET.value(), "");
        String nextTarget = next.optString(OCKey.TARGET.value(), "");

        try {
            nextEnd = next.getJSONArray(OCKey.END.value());
        } catch (Exception e) {
            nextEnd = null;
        }

        if (nextEnd == null) {
            nextEnd = nextStart;
        }

        if ((lastStart != null) && (nextStart != null) && (hasSameParentComponent(lastStart, nextStart, parentLevel)) && (hasSameParentComponent(
            nextStart,
            nextEnd,
            parentLevel)) && (lastTarget.equals(nextTarget))) {

            int lastTextPosStart = lastStart.getInt(lastStart.length() - 1);
            int nextTextPosStart = nextStart.getInt(nextStart.length() - 1);
            int nextTextPosEnd = nextEnd.getInt(nextEnd.length() - 1);
            String lastText = last.getString(OCKey.TEXT.value());

            if ((lastTextPosStart <= nextTextPosStart) && (nextTextPosEnd < lastTextPosStart + lastText.length())) {
                // using 'nextTextPosEnd < lastTextPosStart + lastText.length()', not '<=' !
                // insertText at [0,3], Text 'abc' -> length is 3.
                // deleteText 'abc' is delete from [0,3] to [0,5], not till [0,6]!

                try {
                    // the deleted text is part of an already existing text
                    int relativeStart = nextTextPosStart - lastTextPosStart;
                    int relativeEnd = nextTextPosEnd - lastTextPosStart + 1;
                    String newText = "";
                    if ((relativeStart > 0) && (relativeStart <= lastText.length())) {
                        newText = lastText.substring(0, relativeStart);
                    }
                    if ((relativeEnd >= 0) && (relativeEnd < lastText.length())) {
                        newText += lastText.substring(relativeEnd);
                    }

                    last.put(OCKey.TEXT.value(), newText);
                    insertNextOp = false;

                } catch (IndexOutOfBoundsException e) {
                    log.error("RT2: Wrong index used within mergeInsertAndDeleteOperations", e);
                }
            }
        }

        return insertNextOp;
    }

    /**
     * Optimizing the operations by trying to merge an insertText operation
     * with a directly following insertText or delete operation.
     *
     * @param jsonResult
     *  The JSONObject that contains the operations below the key "operations"
     *  as a JSONArray. The content of this JSONArray is modified. No return
     *  value is required.
     */
    static private void optimizeOperationsArray(JSONObject jsonResult) {

        if ((null != jsonResult) && (jsonResult.has(KEY_OPERATIONS))) {
            try {
                JSONArray operations = jsonResult.getJSONArray(KEY_OPERATIONS);
                JSONArray operationArr = new JSONArray();

                if (!operations.isEmpty()) {

                    int nOps = operations.length();

                    for (int i = 0; i < nOps; i++) {

                        JSONObject nextOp = operations.getJSONObject(i);
                        JSONObject next = null;

                        boolean insertNextOp = true;

                        // Optimizing the content of the JSONArray 'operationArr'.
                        // For example several insertText operations can be merged.
                        // INFO: It might be necessary to make this optimization optional,
                        // so that operations from several clients will not be merged.

                        try {

                            String nextName = nextOp.getString(OCKey.NAME.value());

                            if (nextName.equals(OCValue.INSERT_TEXT.value())) {
                                // Always make a copy of insertText operations, because they might be modified in
                                // optimization process. Taking care, that the original operations are not modified.
                                next = new JSONObject(nextOp, nextOp.keySet().toArray(new String[0]));
                                // next = new JSONObject(nextOrig.shortName()); -> alternative way
                            } else {
                                next = nextOp; // no need to copy operation, because it will not be modified
                            }

                            final JSONObject last = (operationArr.length() > 0) ? operationArr.getJSONObject(operationArr.length() - 1) : null;

                            if (last != null) {

                                String lastName = last.getString(OCKey.NAME.value());

                                if ((lastName.equals(OCValue.INSERT_TEXT.value())) && (nextName.equals(OCValue.INSERT_TEXT.value()))) {
                                    // First optimization: Merging two following insertText operations
                                    insertNextOp = mergeInsertOperations(last, next);
                                } else if ((lastName.equals(OCValue.INSERT_TEXT.value())) && (nextName.equals(OCValue.DELETE.value()))) {
                                    // Second optimization: Merging delete operation following insertText operation
                                    insertNextOp = mergeInsertAndDeleteOperations(last, next);

                                    if ((!insertNextOp) && (last.getString(OCKey.TEXT.value()).length() == 0)) {
                                        // taking care of empty new texts -> removing the last operation completely
                                        operationArr = JSONHelper.removeItemFromJSONArray(operationArr, operationArr.length() - 1);
                                    }
                                }

                                if (! insertNextOp) {
                                    if (last.has("opl") && (next.has("opl"))) {
                                        last.put("opl", last.getInt("opl") + next.getInt("opl"));
                                    }
                                }

                            }
                        } catch (JSONException e) {
                            log.error("RT2: Exception caught while setting up JSON result within optimizeOperationsArray", e);
                        }

                        if (insertNextOp) {
                            operationArr.put(next);
                        }
                    }

                    jsonResult.put(KEY_OPERATIONS, operationArr);
                }
            } catch (JSONException e) {
                log.error("RT2: Exception caught while setting up JSON result within optimizeOperationsArray", e);
            }
        }
    }

    /**
     * Concatenating two JSON objects containing actions
     *
     * @param jsonResult
     *
     * @param jsonAppender
     *
     * @return
     */
    public static JSONObject appendJSON(JSONObject jsonResult, JSONObject jsonAppender) {
        return appendJSON(jsonResult, jsonAppender, false);
    }

    /**
     * Concatenating two JSON objects containing actions
     *
     * @param jsonResult
     *
     * @param jsonAppender
     *
     * @param keepActions
     *
     * @return
     */
    public static JSONObject appendJSON(JSONObject jsonResult, JSONObject jsonAppender, boolean keepActions) {
        if ((null != jsonResult) && (null != jsonAppender)) {
            try {
                final Iterator<String> keys = jsonAppender.keys();

                while (keys.hasNext()) {
                    final String curKey = keys.next();
                    final Object curObject = jsonAppender.get(curKey);

                    if (curKey.equals(KEY_ACTIONS)) {
                        if (!keepActions) {
                            if (!jsonResult.has(KEY_OPERATIONS)) {
                                jsonResult.put(KEY_OPERATIONS, new JSONArray());
                            }

                            if (curObject instanceof JSONArray) {
                                final JSONArray operationsGroups = (JSONArray) curObject;

                                for (int i = 0, size = operationsGroups.length(); i < size; ++i) {
                                    final JSONObject operationsObject = operationsGroups.getJSONObject(i);
                                    if (operationsObject.has(KEY_OPERATIONS)) {
                                        final JSONArray newOperations = operationsObject.getJSONArray(KEY_OPERATIONS);

                                        for (int j = 0; j < newOperations.length(); j++) {
                                            jsonResult.append(KEY_OPERATIONS, newOperations.get(j));
                                        }
                                    } else {
                                        jsonResult.append(KEY_OPERATIONS, operationsGroups.get(i));
                                    }
                                }
                            } else {
                                jsonResult.append(KEY_OPERATIONS, curObject);
                            }
                        } else {
                            appendJSONElement(jsonResult, curKey, curObject);
                        }
                    } else {
                        appendJSONElement(jsonResult, curKey, curObject);
                    }
                }
            } catch (JSONException e) {
                log.error("RT2: Exception caught while setting up JSON result within appendJSON", e);
            }
        }

        return jsonResult;
    }

    /**
     * @param jsonResult
     * @param key
     * @param element
     */
    static private void appendJSONElement(JSONObject jsonResult, String key, Object element) {
        try {
            if (key.equals(KEY_OPERATIONS)) {
                if (!jsonResult.has(KEY_OPERATIONS)) {
                    jsonResult.put(KEY_OPERATIONS, new JSONArray());
                }

                if (element instanceof JSONArray) {
                    final JSONArray jsonArray = (JSONArray) element;

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        jsonResult.append(KEY_OPERATIONS, jsonArray.get(i));
                    }
                } else {
                    jsonResult.append(KEY_OPERATIONS, element);
                }
            } else {
                jsonResult.put(key, element);
            }
        } catch (JSONException e) {
            log.error("RT2: Exception caught while setting up JSON result within appendJSONElement", e);
        }
    }

    /**
     * Transforms an operation JSONObject to a string using a special writer to ensure
     * that the output won't be bigger that a maximum number of characters.
     *
     * @param operation The json object containing the data of an operation.
     *
     * @return A string representing the operation.
     */
    public static String operationToString(JSONObject operation) {
        String result = "";

        if (null != operation) {
            JSONDebugWriter myWriter = null;

            try {
                myWriter = new JSONDebugWriter();
                operation.write(myWriter);
                result = myWriter.getData();
            } catch (final Exception e) {
                log.warn("RT2: operationshortName catches exception writing operation to string. ", e);
            } finally {
                IOUtils.closeQuietly(myWriter);
            }
        }

        return result;
    }

    /**
     * Creates a synchronization no-operation to be added to a list of
     * operations.
     *
     * @param osn
     *  The OSN to be used by the no-op.
     *
     * @return
     *  A JSONObject filled with the necessary properties.
     */
    public static JSONObject createSyncNoOperation(int osn) throws JSONException {
        final JSONObject noOpSync = new JSONObject();
        noOpSync.put(OCKey.NAME.value(), OCValue.NO_OP.value());
        noOpSync.put("osn", (osn - 1));
        noOpSync.put("opl", 1);
        return noOpSync;
    }

    private static String SEP = System.getProperty("line.separator");

    private static void appendDataToLog(BufferedWriter writer, String uid, String key, Object data) throws IOException {
        writer.append(uid + ":" + key + ":" + data + SEP);
    }

    /**
     * Small helper class used to generate sorted performance measurement logs.
     */
    private static final class PerformanceStep implements Comparable<PerformanceStep> {

        public final String key;
        public final int time;
        public final int step;

        public PerformanceStep(final String key, final int time, final int step) {
            this.key = key;
            this.time = time;
            this.step = step;
        }

        @Override
        public int compareTo(PerformanceStep other) {
            return this.time - other.time;
        }
    }

    /**
     * Saving performance data into log file on server.
     *  with a directly following insertText or delete operation.
     *
     * @param perfData
     *  The JSONObject containing all performance information sent
     *  from the client.
     *
     * @param writer
     *  The buffered writer for content input.
     */
    public static void logEditorPerformanceData(JSONObject perfData, BufferedWriter writer) {

    	String uid = UUID.randomUUID().toString().substring(0, 8);  // only using 8 digits

    	// Saving performance data in log file
    	try {
        	appendDataToLog(writer, uid, "UTC", getISODateFormat(true));

            // extract all time measurement data items into an array (for sorting)
            ArrayList<PerformanceStep> perfSteps = new ArrayList<PerformanceStep>();
            for (Map.Entry<String, Object> entry: perfData.asMap().entrySet()) {
                String key = entry.getKey();
                if (key.endsWith("Step")) {
                    String baseKey = key.substring(0, key.length() - 4);
                    int time = perfData.optInt(baseKey + "Absolute", -1);
                    int step = perfData.optInt(key, -1);
                    if (time >= 0 && step >= 0) {
                        perfSteps.add(new PerformanceStep(baseKey, time, step));
                    }
                }
            }

            // sort all steps by absolute timestamp
            Collections.sort(perfSteps);

            // append the sorted step durations to the log writer
            for (PerformanceStep perfStep: perfSteps) {
                appendDataToLog(writer, uid, "STEP_" + perfStep.key.toUpperCase(), perfStep.step);
            }

            final int launchCount = perfData.optInt("launchCount", 0);
        	appendDataToLog(writer, uid, "LAUNCHEND:" + launchCount, perfData.optInt("launchEndDuration", 0));

            appendDataToLog(writer, uid, "LOCALSTORAGE", perfData.optBoolean("LocalStorage", false));
            appendDataToLog(writer, uid, "FASTEMPTYLOAD", perfData.optBoolean("FastEmptyLoad", false));

            appendDataToLog(writer, uid, "USERAGENT", perfData.optString("user-agent", ""));
            appendDataToLog(writer, uid, "PLATFORM", perfData.optString("platform", ""));
            appendDataToLog(writer, uid, "USER", perfData.optString("user", ""));
            appendDataToLog(writer, uid, "OXVERSION", perfData.optString("version", ""));
            appendDataToLog(writer, uid, "SERVERHOSTNAME", perfData.optString("server", ""));
            appendDataToLog(writer, uid, "APPLICATION", perfData.optString("application", ""));
            appendDataToLog(writer, uid, "FILENAME", perfData.optString("filename", ""));
            appendDataToLog(writer, uid, "TITLE", perfData.optString("title", ""));

        	writer.append(SEP).close();

		} catch (Exception e) {
            log.debug("LogEditorPerformanceData writing to log file failed due to exception", e);
			// Do nothing, simply not logging performance data
			log.debug("RT2: IOException caught trying to write performance data to log file!", e);
	    }

    }

    /**
     * Saving performance data into log file on server.
     *  with a directly following insertText or delete operation.
     *
     * @param performanceData
     *  The JSONObject containing all performance information sent
     *  from the client.
     *
     * @param writer
     *  The buffered writer for content input.
     */
    public static void logViewerPerformanceData(JSONObject performanceData, BufferedWriter writer) {

        String uid = UUID.randomUUID().toString().substring(0, 8);  // only using 8 digits
        int counter = 1;

        String beforePageZoom = "DocumentView:refresh_before_set_pagezoom_";
        String beforeGetPdfJsPage = "pdfView:renderPDFPage_before_getPDFJSPage_";
        String thenOfGetPdfJsPage = "pdfView:renderPDFPage_getPDFJSPage_then_handler_";
        String beforeRenderPdfJsPage = "pdfView:renderPDFPage_before_pdfjsPage_render_";
        String thenOfRenderPdfJsPage = "pdfView:renderPDFPage_pdfjsPage_render_then_handler_";
        String thenOfPageZoom = "DocumentView:refresh_then_from_set_pagezoom_";

        String beforePageZoomValue = beforePageZoom + counter;
        String beforeGetPdfJsPageValue = beforeGetPdfJsPage + counter;
        String thenOfGetPdfJsPageValue = thenOfGetPdfJsPage + counter;
        String beforeRenderPdfJsPageValue = beforeRenderPdfJsPage + counter;
        String thenOfRenderPdfJsPageValue = thenOfRenderPdfJsPage + counter;
        String thenOfPageZoomValue = thenOfPageZoom + counter;

        // Saving performance data in log file
        try {
            appendDataToLog(writer, uid, "UTC", getISODateFormat(true));
            appendDataToLog(writer, uid, "LAUNCHCONTSTART", performanceData.optInt("launchContStart", 0));
            appendDataToLog(writer, uid, "MAINVIEW:INITIALIZE", performanceData.optInt("MainView:initialize", 0));
            appendDataToLog(writer, uid, "DOCUMENTVIEW:INITIALIZE", performanceData.optInt("DocumentView:initialize", 0));
            appendDataToLog(writer, uid, "DOCUMENTVIEW:SHOW", performanceData.optInt("DocumentView:show", 0));
            appendDataToLog(writer, uid, "PDFDOCUMENT:PDFJS_GETDOCUMENT_THEN_HANDLER", performanceData.optInt("PDFDocument:PDFJs_getDocument_then_handler", 0));
            appendDataToLog(writer, uid, "DOCUMENTVIEW:SHOW_GETLOADPROMISE_DONE_HANDLER", performanceData.optInt("DocumentView:show_getLoadPromise_done_handler", 0));
            appendDataToLog(writer, uid, "DOCUMENTVIEW:LOADVISIBLEPAGES", performanceData.optInt("DocumentView:loadVisiblePages", 0));

            while (performanceData.optInt(beforePageZoomValue, 0) != 0) {
                appendDataToLog(writer, uid, beforePageZoom.toUpperCase() + counter, performanceData.optInt(beforePageZoomValue, 0));
                beforePageZoomValue = beforePageZoom + ++counter;
            }

            counter = 1;

            while (performanceData.optInt(beforeGetPdfJsPageValue, 0) != 0) {
                appendDataToLog(writer, uid, beforeGetPdfJsPage.toUpperCase() + counter, performanceData.optInt(beforeGetPdfJsPageValue, 0));
                beforeGetPdfJsPageValue = beforeGetPdfJsPage + ++counter;
            }

            counter = 1;

            while (performanceData.optInt(thenOfGetPdfJsPageValue, 0) != 0) {
                appendDataToLog(writer, uid, thenOfGetPdfJsPage.toUpperCase() + counter, performanceData.optInt(thenOfGetPdfJsPageValue, 0));
                thenOfGetPdfJsPageValue = thenOfGetPdfJsPage + ++counter;
            }

            counter = 1;

            while (performanceData.optInt(beforeRenderPdfJsPageValue, 0) != 0) {
                appendDataToLog(writer, uid, beforeRenderPdfJsPage.toUpperCase() + counter, performanceData.optInt(beforeRenderPdfJsPageValue, 0));
                beforeRenderPdfJsPageValue = beforeRenderPdfJsPage + ++counter;
            }

            counter = 1;

            while (performanceData.optInt(thenOfRenderPdfJsPageValue, 0) != 0) {
                appendDataToLog(writer, uid, thenOfRenderPdfJsPage.toUpperCase() + counter, performanceData.optInt(thenOfRenderPdfJsPageValue, 0));
                thenOfRenderPdfJsPageValue = thenOfRenderPdfJsPage + ++counter;
            }

            counter = 1;

            while (performanceData.optInt(thenOfPageZoomValue, 0) != 0) {
                appendDataToLog(writer, uid, thenOfPageZoom.toUpperCase() + counter, performanceData.optInt(thenOfPageZoomValue, 0));
                thenOfPageZoomValue = thenOfPageZoom + ++counter;
            }

            appendDataToLog(writer, uid, "USERAGENT", performanceData.optString("user-agent", ""));
            appendDataToLog(writer, uid, "PLATFORM", performanceData.optString("platform", ""));
            appendDataToLog(writer, uid, "USER", performanceData.optString("user", ""));
            appendDataToLog(writer, uid, "OXVERSION", performanceData.optString("version", ""));
            appendDataToLog(writer, uid, "SERVERHOSTNAME", performanceData.optString("server", ""));
            appendDataToLog(writer, uid, "FILENAME", performanceData.optString("filename", ""));
            appendDataToLog(writer, uid, "APPLICATION", performanceData.optString("application", ""));

            writer.append(SEP).close();

        } catch (Exception e) {
            log.debug("LogViewerPerformanceData writing to log file failed due to exception", e);
            // Do nothing, simply not logging performance data
        }

    }

    /**
     * The current date in ISO 8601 format.
     *
     * @param useSeconds
     *  Wheter seconds shall be included into the returnes string or not.
     *
     * @return
     *  The date as string in ISO 8601 format.
     */
    public static String getISODateFormat(Boolean useSeconds) {

    	TimeZone tz = TimeZoneHelper.getTimeZone("UTC");
    	DateFormat df = null;

    	if (useSeconds) {
    	    df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    	} else {
    	    df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm':00Z'");
    	}
    	df.setTimeZone(tz);
    	return df.format(new Date());
    }

    public static int getBaseOSN(final JSONObject operationsObject) {
        int result = -1;

        try {
            int operationCount = operationsObject.getJSONArray(KEY_OPERATIONS).length();
            if (operationCount > 0) {
                result = operationsObject.getJSONArray(KEY_OPERATIONS).getJSONObject(0).getInt(KEY_OSN);
            }
        } catch (final JSONException e) {
            // nothing to do
        } catch (final Exception e) {
            // nothing to do
        }

        return result;
    }

    public static int getFollowUpOSN(final JSONObject operationsObject) {
        int result = -1;

        try {
             result = getFollowUpOSN(operationsObject.getJSONArray(KEY_OPERATIONS));
        } catch (JSONException e) {
             // nothng to do
        }

        return result;
    }

    public static int getFollowUpOSN(final JSONArray operationsArray) {
        int result = -1;

        try {
            int operationCount = operationsArray.length();
            if (operationCount > 0) {
                int osn = operationsArray.getJSONObject(operationCount-1).getInt(KEY_OSN);
                int opl = operationsArray.getJSONObject(operationCount-1).getInt(KEY_OPL);
                result = osn + opl;
            }
        } catch (final JSONException e) {
            // nothing to do
        } catch (final Exception e) {
            // nothing to do
        }

        return result;
    }

    /**
     * Retrieves the last operation from a JSONObject containing
     * a set of operations.
     *
     * @param operationsObject a JSONObject containing operations provided by
     *        a client.
     * @return the last operation JSONObject or null, if not found
     */
    public static JSONObject getLastOperation(final JSONObject aOperationsObject) {
        JSONObject result = null;

        try
        {
            final JSONArray aOperations = aOperationsObject.getJSONArray(KEY_OPERATIONS);
            result = getLastOperation(aOperations);
        } catch (final JSONException e) {
            // nothing to do
        } catch (final Exception e) {
            // nothing to do
        }

        return result;
    }

    /**
     * Retrieves the last operation from a JSONArray containing
     * a set of operations.
     *
     * @param operationsObject a JSONObject containing operations provided by
     *        a client.
     * @return the last operation JSONObject or null, if not found
     */
    public static JSONObject getLastOperation(final JSONArray aOperations) {
        JSONObject result = null;

        try {
            int operationCount = aOperations.length();
            if (operationCount > 0) {
                result = aOperations.getJSONObject(operationCount-1);
            }
        } catch (final JSONException e) {
            // nothing to do
        } catch (final Exception e) {
            // nothing to do
        }

        return result;
    }

    public static JSONArray listOperationsInOperationsObject(final JSONObject operationsObject) {
        if (operationsObject != null) {
	        try {
	            final JSONArray operationsArray = operationsObject.getJSONArray(KEY_OPERATIONS);
	            return operationsArray;
	        } catch (final JSONException e) {
	            // nothing to do
	        } catch (final Exception e) {
	            // nothing to do
	        }
        }

        return new JSONArray();
    }

    /**
     * Extracts the operations from a actions request which can contain
     * more than one operations array entry in the operations queue.
     *
     * @param actionsRequest
     * @return
     * @throws Exception
     */
    public static JSONArray getOperationsArray(final JSONObject actionsRequest) throws Exception {
        JSONArray result = new JSONArray();

        final JSONArray actionsArray = actionsRequest.optJSONArray(KEY_ACTION);
        if (null != actionsArray && actionsArray.length() > 0) {
            int actions = actionsArray.length();

            for (int i=0; i < actions; i++) {
                final JSONObject actionsObject = actionsArray.getJSONObject(i);
                final JSONArray operationsArray = actionsObject.optJSONArray(KEY_OPERATIONS);

                // read the operationsArray and copy the JSONObject reference
                if ((null != operationsArray) && (operationsArray.length() > 0)) {
                    JSONHelper.appendArray(result, operationsArray);
                }
            }
        }

        return result;
    }

    /**
     * Find the index where the first operations is located which has a higher osn than
     * the provided one. If there is no operation at all -1 will be returned.
     *
     * @param operations a JSONArray with operations
     * @param largerThanOSN a osn which
     * @return the index of the first operations that has a higher osn than provided or -1
     *   if there is no operation with a higher osn or an empty JSONArray
     */
    public static int getIndexOfOperationWithOSNLargerThan(final JSONArray operations, int largerThanOSN) {
    	int result = -1;

        if (JSONHelper.isNotNullAndEmpty(operations)) {
            int opsCount = operations.length();

            try {
                for (int i=0; i < opsCount; i++) {
                    final JSONObject opsObject = operations.getJSONObject(i);
                    if (opsObject != null) {
                        int osn = opsObject.getInt(KEY_OSN);
                        if (osn > largerThanOSN) {
                        	result = i;
                        	break;
                        }
                    }
                }
            } catch (JSONException e) {
            	log.info("Exception caught trying to find index for operations with larger osn than " + largerThanOSN, e);
            }
        }

        return result;
    }

    /**
     * Filter out all operations which have a smaller or equal OSN than the provided filterOSN.
     *
     * @param opsArray
     * @param filterSmallThan
     * @return
     */
    public static JSONArray filterOperationsWithOSNSmallerEqualThan(final JSONArray opsArray, int filterSmallThan) throws JSONException {
    	final JSONArray filteredClone = new JSONArray();

		for (int i = 0; i < opsArray.length(); i++) {
			final JSONObject op = opsArray.getJSONObject(i);
			final int nOpOSN = op.getInt(OperationHelper.KEY_OSN);
			if (nOpOSN > filterSmallThan) {
			    // This must be clones. The original operation in the document must not be modified!
			    filteredClone.put(JSONHelper.cloneJSONObject(op));
			}
		}

    	return filteredClone;
    }

    /**
     * Determine if we have at least one operation which has a higher osn than provided.
     *
     * @param operations
     * @param largerThanOSN
     * @return TRUE if at least one operation has a higher osn than provided, otherwise FALSE
     */
    public static boolean hasOperationWithOSNLargerThan(final JSONArray operations, int largerThanOSN) {
    	boolean result = false;

        if (JSONHelper.isNotNullAndEmpty(operations)) {
            int opsCount = operations.length();

            // optimized look-up operations array is sorted there we just need to check
            // the last operation with its stored osn
            try {
                final JSONObject opsObject = operations.getJSONObject(opsCount - 1);
                if (opsObject != null) {
                    int osn = opsObject.getInt(KEY_OSN);
                    if (osn > largerThanOSN) {
                    	result = true;
                    }
                }
            } catch (JSONException e) {
            	log.info("Exception caught trying to find an operation with a larger osn than " + largerThanOSN, e);
            }
        }

        return result;
    }

    /**
     * Determine the number of operations which have a higher osn than provided.
     *
     * @param operations the operations array
     * @param largerThanOSN the osn to be used as a filter (ops with osn > largerThanOSN)
     * @param countNonModifiyingOps specifies if non-modifying ops are counted or not
     * @return the number of operations with a osn larger than largerThanOSN
     */
    public static int countOperationsWithOSNLargerThan(final JSONArray operations, int largerThanOSN, boolean countNonModifiyingOps) {
        int count = 0;

        if (JSONHelper.isNotNullAndEmpty(operations)) {
            int i = operations.length() - 1;
        	boolean isLarger = true;

            try {
            	while (isLarger && (i >= 0)) {
                    final JSONObject opsObject = operations.getJSONObject(i--);
                    if (opsObject != null) {
                        int osn = opsObject.getInt(KEY_OSN);
                        isLarger = (osn > largerThanOSN);
                        if (isLarger) {
                            count += (countNonModifiyingOps) ? 1 : (isModifyingOp(opsObject) ? 1 : 0);
                        }
                    }
            	}
            } catch (JSONException e) {
                log.info("Exception caught trying to count operations with a larger osn than " + largerThanOSN, e);
            }
        }

        return count;
    }

    /**
     * Determine the number of operations according to the provided
     * parameters.
     *
     * @param operations the operations array
     * @param countNonModifiyingOps specifies if non-modifying ops are counted or not
     * @return the number of operations with or without modifying operations according to the
     *  parameter countNonModifiyingOps
     */
    public static int countOperations(JSONArray operations, boolean countNonModifyingOps) {
        int count = 0;

        if (JSONHelper.isNotNullAndEmpty(operations)) {
            try {
                for (int i=0; i < operations.length(); i++) {
                    final JSONObject opsObject = operations.getJSONObject(i);
                    count += (countNonModifyingOps) ? 1 : (isModifyingOp(opsObject) ? 1 : 0);
                }
            } catch (JSONException e) {
                log.info("Exception caught trying to count operations", e);
            }
        }

        return count;
    }

    /**
     * Determine if this is a modifying operation or not.
     * ATTENTION:
     * A operation is treated as modifying if the property modifying is missing. Only
     * if the property is set to false it's detected as NOT modifying. The modifying
     * property is optional as almost all operations are modifying.
     *
     * @param op the operation to be checked
     * @return true if the op is modifying and false if not.
     */
    private static boolean isModifyingOp(JSONObject op) {
        boolean result = false;
        if (op != null) {
            result = true;
            Object obj = op.opt(KEY_MODIFYING);
            if (obj instanceof Boolean)
                result = (Boolean)obj;
        }
        return result;
    }

}
