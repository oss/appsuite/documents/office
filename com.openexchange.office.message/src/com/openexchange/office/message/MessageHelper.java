/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.message;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.tools.common.error.ErrorCode;


public class MessageHelper {

    public final static String KEY_SESSION = "session";
    public final static String KEY_RESOURCE = "resource";


    // - Members ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(MessageHelper.class);

    /**
     * Creates the final JSON result object to be sent to the client-side.
     * This function is for special purpose where a method already created
     * a JSON result object with error data. In that case finalize shall not
     * overwrite the first error data.
     *
     * @param jsonRequest
     *  The request to be processed.
     *
     * @param jsonExtra
     *  Extra data to be added to the JSON result object.
     *
     * @return
     *  The final JSON result object to be sent to the client-side.
     */
    static public JSONObject finalizeJSONResult(JSONObject tmpResult, JSONObject jsonRequest, JSONObject jsonExtra) {
        if (null != tmpResult) {
            try {
                // add unique message id, if given in request
                if ((null != jsonRequest) && jsonRequest.has(MessagePropertyKey.KEY_UNIQUE_ID)) {
                    tmpResult.put(MessagePropertyKey.KEY_UNIQUE_ID, jsonRequest.get(MessagePropertyKey.KEY_UNIQUE_ID));
                }
            } catch (JSONException e) {
                log.error("RT2: getJSfinalizeJSONResult catches exception trying to to put unique id to the JSON object. ", e);
            }

            // add extra data, if set
            if (null != jsonExtra) {
                OperationHelper.appendJSON(tmpResult, jsonExtra);
            }
        }

        return tmpResult;
    }

    /**
     * Creates the final JSON result object to be sent to the client-side.
     *
     * @param errorCode The result of the request as error code.
     * @param jsonRequest The request to be processed.
     * @param jsonExtra Extra data to be added to the JSON result object.
     *
     * @return The final JSON result object to be sent to the client-side.
     */
    static public JSONObject finalizeJSONResult(ErrorCode errorCode, JSONObject jsonRequest, JSONObject jsonExtra) {
        JSONObject jsonResult = new JSONObject();
        return finalizeJSONResult(jsonResult, errorCode, jsonRequest, jsonExtra);
    }

    /**
     * Creates the final JSON result object to be sent to the client-side.
     *
     * @param errorCode The result of the request as error code.
     * @return The final JSON result object to be sent to the client-side.
     */
    static public JSONObject finalizeJSONResult(final ErrorCode errorCode) {
        JSONObject jsonResult = new JSONObject();
        return finalizeJSONResult(jsonResult, errorCode, null, null);
    }

    /**
     * Creates the final JSON result object to be sent to the client-side.
     *
     * @param tmpResult
     *  A json object which is used to merge the following data into it.
     *
     * @param errorCode
     *  The result of the request as error code.
     *
     * @param jsonRequest
     *  The request to be processed.
     *
     * @param jsonExtra
     *  Extra data to be added to the JSON result object.
     *
     * @return
     *  The final JSON result object to be sent to the client-side. The object
     *  is the same as tmpResult extended with the properties provided as
     *  arguments.
     */
    static public JSONObject finalizeJSONResult(JSONObject tmpResult, ErrorCode errorCode, JSONObject jsonRequest, JSONObject jsonExtra) {
        if (null != tmpResult) {
            try {
                // add unique message id, if given in request
                if ((null != jsonRequest) && jsonRequest.has(MessagePropertyKey.KEY_UNIQUE_ID)) {
                    tmpResult.put(MessagePropertyKey.KEY_UNIQUE_ID, jsonRequest.get(MessagePropertyKey.KEY_UNIQUE_ID));
                }
                tmpResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());
            } catch (JSONException e) {
                log.error("RT2: getJSfinalizeJSONResult catches exception trying to to put unique id to the JSON object. ", e);
            }

            // add extra data, if set
            if (null != jsonExtra) {
                OperationHelper.appendJSON(tmpResult, jsonExtra);
            }
        }

        return tmpResult;
    }

    /**
     * Checks the json result object is empty and sets the "hasErrors"
     * property to indicate an errornous state.
     *
     * @param jsonResult
     *  A json object containing the result of a request which should be
     *  checked.
     *
     * @return
     *  The jsonResult which contains a "hasError" property if it doesn't
     *  contain any properties.
     */
    static public JSONObject checkJSONResult(JSONObject jsonResult) {
        if ((null != jsonResult) && jsonResult.isEmpty()) {
            try {
                jsonResult.put(MessagePropertyKey.KEY_HAS_ERRORS, true);
            } catch (JSONException e) {
                log.error("RT2: checkJSONResult catches exception trying to to put hasErrors to the JSON object. ", e);
            }
        }

        return jsonResult;
    }

}
