/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.message;

public interface MessagePropertyKey {
    // - Constants -------------------------------------------------------------

    String KEY_CLIENT_ID      = "clientId";
    String KEY_UNIQUE_ID      = "uniqueId";
    String KEY_FILE_NAME      = "fileName";
    String KEY_FILE_VERSION   = "fileVersion";
    String KEY_TARGET_FOLDER  = "targetFolder";
    String KEY_FILE_ID        = "fileId";
    String KEY_FOLDER_ID      = "folderId";
    String KEY_AS_TEMPLATE    = "asTemplate";
    String KEY_HTMLDOCUMENT   = "htmlDoc";
    String KEY_ERROR_DATA     = "error";
    String KEY_ACTIONS        = "actions";
    String KEY_HAS_ERRORS     = "hasErrors";
    String KEY_MIMETYPE       = "file_mimetype";
    String KEY_OPERATIONS     = "operations";
    String KEY_FILEDESCRIPTOR = "file";
    String KEY_SENDER         = "sender";
    String KEY_PREVIEW        = "preview";
    String KEY_ACTIVESHEET    = "activeSheet";
    String KEY_FASTLOAD       = "fastLoad";
    String KEY_SANITIZEDFILENAME = "com.openexchange.file.sanitizedFilename";
}
