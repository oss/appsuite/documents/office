/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.Loggable;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CleanupTask extends Task implements Loggable {
	private static final Logger log = LoggerFactory.getLogger(CleanupTask.class);
	
    //-------------------------------------------------------------------------
    private final String  m_sMasterUUID; 
    
	private final String  m_sNodeUUIDToCleanup;

    private AtomicBoolean m_bSuccessful = new AtomicBoolean(false);	
	
	//-------------------------------------------------------------------------
	@Autowired
	private RT2DocProxyRegistry rt2DocProxyRegistry;
	
	@Autowired
	private RT2DocOnNodeMap docOnNodeMap;

    //-------------------------------------------------------------------------
    public CleanupTask(String sTaskID, String sMasterUUID, String sNodeUUIDToCleanup) {
        super(sTaskID);
        m_sMasterUUID        = sMasterUUID;
        m_sNodeUUIDToCleanup = sNodeUUIDToCleanup;
    }

    //-------------------------------------------------------------------------
    public String getMasterUUID() {
        return m_sMasterUUID;
    }

    //-------------------------------------------------------------------------
    public String getMemberUUIDToCleanup() {
        return m_sNodeUUIDToCleanup;
    }

    //-------------------------------------------------------------------------
    public boolean successful() {
        return m_bSuccessful.get();
    }

    //-------------------------------------------------------------------------
    public boolean failed() {
        return !successful();
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean process() throws Exception {
    	LogMethodCallHelper.logMethodCall(this, "process");
        try {
            final Set<String> aDocUIDs = docOnNodeMap.getDocsOfMember(m_sNodeUUIDToCleanup);

            if (!aDocUIDs.isEmpty()) {
                int nCrashedDocsToNotify = aDocUIDs.size();
                log.info("RT2: CleanupTask found count documents {} controlled by crashed node with node-uid {}", nCrashedDocsToNotify, m_sNodeUUIDToCleanup);
                for (final String sDocUID : aDocUIDs)
                    notifyAndCleanupDocProxiesOfDocument(sDocUID);
            } else {
                log.info("RT2: CleanupTask found no document controlled by crashed node with node-uid {}", m_sNodeUUIDToCleanup);
            }
            m_bSuccessful.set(true);
        } finally {
            setCompleted(true);
        }
        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public String toString() {
        final StringBuilder aTmp = new StringBuilder(super.toString());
        aTmp.append(",member com.openexchange.rt2.backend.uid=");
        aTmp.append(m_sMasterUUID);
        aTmp.append(",master com.openexchange.rt2.backend.uid=");
        aTmp.append(this.getMasterUUID());
        aTmp.append(",cleanup com.openexchange.rt2.backend.uid=");
        aTmp.append(this.getMemberUUIDToCleanup());
        return aTmp.toString();
    }

    //-------------------------------------------------------------------------
    @Override
    public Logger getLogger() {
        return log;
    }

    //-------------------------------------------------------------------------
    @Override
    public Map<String, Object> getAdditionalLogInfo() {
        Map<String, Object> res = new HashMap<>();
        res.put("NodeUUIDToCleanup", m_sNodeUUIDToCleanup);
        return res;
    }

    //-------------------------------------------------------------------------
    private void notifyAndCleanupDocProxiesOfDocument(final String sDocUID) throws Exception {
        final List<RT2DocProxy> aProxies     = rt2DocProxyRegistry.getDocProxies4DocUID(new RT2DocUidType(sDocUID), null);

        if (!aProxies.isEmpty()) {
            try {
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, sDocUID);

                for (final RT2DocProxy aDocProxy : aProxies) {
                    try {
                        log.debug("RT2: Try to send 'crashed' broadcast and cleanup for com.openexchange.rt2.document.uid {}", aDocProxy.getDocUID());
                        aDocProxy.sendCrashedResponseToClient();
                        cleanupDocProxyResources(aDocProxy);
                    } catch (Throwable t) {
                        ExceptionUtils.handleThrowable(t);
                        log.error("RT2: CleanupTask caught exception on notifying/cleanup document proxy with com.openexchange.rt2.document.uid " + aDocProxy.getDocUID(), t);
                    }
                }
            } finally {
                MDC.remove(MDCEntries.DOC_UID);
            }
        }
    }

    //-------------------------------------------------------------------------
    private void cleanupDocProxyResources(final RT2DocProxy aDocProxy) throws Exception {
    	rt2DocProxyRegistry.deregisterDocProxy(aDocProxy, true);
        aDocProxy.markProxyAsDisposed();
    }

}
