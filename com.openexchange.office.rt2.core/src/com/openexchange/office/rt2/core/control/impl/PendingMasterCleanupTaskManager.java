/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.exception.ExceptionUtils;

public class PendingMasterCleanupTaskManager
{
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(PendingMasterCleanupTaskManager.class);

    //-------------------------------------------------------------------------
    private static final long                                            TIMER_CHECK_PERIOD  = 30000;

    //-------------------------------------------------------------------------
    private static final long                                            TASK_TIMEOUT_PERIOD = 300000;

    //-------------------------------------------------------------------------
    private final AtomicBoolean                                          m_stopped = new AtomicBoolean(false);

    //-------------------------------------------------------------------------
    private final AtomicBoolean                                          m_started = new AtomicBoolean(false);

    //-------------------------------------------------------------------------
    private final ConcurrentHashMap<String, MasterCleanupTask>           m_aPendingTasksMap;

    //-------------------------------------------------------------------------
    private Timer                                                        m_aTimer;

    //-------------------------------------------------------------------------
    private TimerTask                                                    m_aCheckForAgedTasks;

    //-------------------------------------------------------------------------
    private final WeakReference<ITaskTimeoutListener<MasterCleanupTask>> m_aTimeoutListener;

    //-------------------------------------------------------------------------
    public PendingMasterCleanupTaskManager(ITaskTimeoutListener<MasterCleanupTask> aTimeoutListener)
    {
        m_aPendingTasksMap = new ConcurrentHashMap<>();
        m_aTimeoutListener = new WeakReference<>(aTimeoutListener);
    }

    //-------------------------------------------------------------------------
    public void start()
    {
        if (m_started.compareAndSet(false, true))
        {
            m_aCheckForAgedTasks = createTimeoutTask();
            m_aTimer = new Timer("com.openexchange.office.rt2.TimeoutCleanupTask");
            m_aTimer.schedule(m_aCheckForAgedTasks, TIMER_CHECK_PERIOD, TIMER_CHECK_PERIOD);
            m_stopped.set(false);
        }
    }

    //-------------------------------------------------------------------------
    public void stop() throws Exception
    {
        if (m_started.get() && m_stopped.compareAndSet(false, true))
        {
            final TimerTask aTimerTask = m_aCheckForAgedTasks;
            if (null != aTimerTask)
            {
                aTimerTask.cancel();
                m_aCheckForAgedTasks = null;
            }

            final Timer aTimer = m_aTimer;
            if (null != aTimer)
            {
                aTimer.cancel();
                aTimer.purge();
                m_aTimer = null;
            }

            m_aPendingTasksMap.clear();
            m_started.set(false);
        }
    }

    //-------------------------------------------------------------------------
    public boolean isStarted()
    {
        return m_started.get();
    }

    //-------------------------------------------------------------------------
    public boolean hasPendingTasks()
    {
        final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
        return (null != aMap) ? !aMap.isEmpty() : false;
    }

    //-------------------------------------------------------------------------
    public boolean containsTask(final String sTaskID)
    {
        final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
        if (null != aMap)
            return aMap.containsKey(sTaskID);
        return false;
    }

    //-------------------------------------------------------------------------
    public MasterCleanupTask getTask(final String sTaskID)
    {
        final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
        return aMap.get(sTaskID);
    }

    //-------------------------------------------------------------------------
    public boolean setMemberToCompleted(final String sTaskID, final String sMemberUUID) throws NoSuchElementException
    {
        boolean bCompleted = false;
        final MasterCleanupTask aMasterCleanupTask = m_aPendingTasksMap.get(sTaskID);
        if (null != aMasterCleanupTask)
        {
            bCompleted = aMasterCleanupTask.setMemberToCompleted(sMemberUUID);
            if (bCompleted)
                m_aPendingTasksMap.remove(sTaskID);
            return bCompleted;
        }

        throw new NoSuchElementException("MasterCleanupTask with id " + sTaskID + " not found ");
    }

    //-------------------------------------------------------------------------
    public void storeTask(final MasterCleanupTask aCleanupTask) throws Exception
    {
        final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
        if (null != aMap)
        {
            log.debug("RT2: PendingMasterCleanupTaskManager stores pending master cleanup task with taskId {}", aCleanupTask.getTaskID());
            aMap.put(aCleanupTask.getTaskID(), aCleanupTask);
        }
    }

    //-------------------------------------------------------------------------
    private void notifyListener(final Set<MasterCleanupTask> aTasksToNotify) throws Exception
    {
        final ITaskTimeoutListener<MasterCleanupTask> aListener = m_aTimeoutListener.get();
        if (!CollectionUtils.isEmpty(aTasksToNotify) && (null != aListener))
        {
            for (final MasterCleanupTask aTimeoutTask : aTasksToNotify)
            {
                try
                {
                    aListener.timeoutReachedForTask(aTimeoutTask);
                }
                catch (Throwable t)
                {
                    ExceptionUtils.handleThrowable(t);
                    log.error("RT2: exception caught while trying notifying listener", t);
                }
                finally
                {
                    final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
                    if ((null != aMap) && !aMap.isEmpty())
                        aMap.remove(aTimeoutTask.getTaskID());
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    private TimerTask createTimeoutTask()
    {
        return new TimerTask()
        {

            @Override
            public void run()
            {

                try
                {
                    final ConcurrentHashMap<String, MasterCleanupTask> aMap = m_aPendingTasksMap;
                    if ((null != aMap) && !aMap.isEmpty())
                    {
                        final long             nCurrentTime  = System.currentTimeMillis();
                        Set<MasterCleanupTask> aTimeoutTasks = null;

                        for (final MasterCleanupTask t : aMap.values())
                        {
                            final long nCreationTime = t.getCreationTime();
                            final long nCurrPeriod   = Math.abs(nCurrentTime - nCreationTime);
                            if (nCurrPeriod > TASK_TIMEOUT_PERIOD)
                            {
                               if (null == aTimeoutTasks)
                                    aTimeoutTasks = new HashSet<>();
                                aTimeoutTasks.add(t);
                            }
                        }

                        notifyListener(aTimeoutTasks);
                    }
                }
                catch (Exception e)
                {
                    log.error("RT2: exception caught while trying to find timeout tasks", e);
                }
            }
        };
    }
}
