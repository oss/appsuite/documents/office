/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control;

import java.lang.ref.WeakReference;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.exception.ExceptionUtils;

public abstract class TaskProcessor<T extends Task> implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(TaskProcessor.class);

	//-------------------------------------------------------------------------
	private final static long DEFAULT_WAIT_TIME                           = 250L;

	//-------------------------------------------------------------------------
	private final AtomicBoolean                   m_aStarted              = new AtomicBoolean(false);
	private final AtomicBoolean                   m_aShutdown             = new AtomicBoolean(false);
	private BlockingQueue<T>                      m_aTaskQueue            = null;
	private final WeakReference<ITaskListener<T>> m_aListener;
	private Thread                                m_aQueueProcessorThread;
	
	//-------------------------------------------------------------------------
	public TaskProcessor(final ITaskListener<T> aListener)
	{
		m_aListener  = new WeakReference<>(aListener);
		m_aTaskQueue = createTaskQueue();
	}

	//-------------------------------------------------------------------------
	protected BlockingQueue<T> createTaskQueue()
	{
		return new LinkedBlockingDeque<>();
	}

	//-------------------------------------------------------------------------
	public boolean isStarted()
	{
		return m_aStarted.get();
	}

	//-------------------------------------------------------------------------
	public int getPendingTasks()
	{
		synchronized (m_aTaskQueue)
		{
			return m_aTaskQueue.size();
		}
	}

	//-------------------------------------------------------------------------
	public void start()
	{
		if (m_aStarted.compareAndSet(false, true))
		{
			synchronized (m_aTaskQueue)
			{
				m_aQueueProcessorThread = new Thread(this, getThreadName());
				m_aQueueProcessorThread.start();
			}

			log.trace("RT2: TaskProcessor started.");
		}
	}

	//-------------------------------------------------------------------------
	public void shutdown()
		throws Exception
	{
		if (isStarted() && m_aShutdown.compareAndSet(false, true))
		{
			Thread aQueueProcessorThread = null;
			synchronized (m_aTaskQueue)
			{
				m_aTaskQueue.clear();
				m_aTaskQueue.notifyAll();

				aQueueProcessorThread = m_aQueueProcessorThread;
				m_aQueueProcessorThread = null;
			}

			m_aStarted.set(false);

			try {
				if (null != aQueueProcessorThread)
					aQueueProcessorThread.join(2000);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}

			log.trace("RT2: TaskProcessor shutdown.");
		}
	}

	//-------------------------------------------------------------------------
	public void addTask(final T aTask)
		throws Exception
	{
		Validate.notNull(aTask);

		boolean bShutdown = false;

		synchronized (m_aTaskQueue)
		{
			bShutdown = m_aShutdown.get();
			if (!bShutdown)
			{
				log.debug("RT2: TaskProcessor adds task [{}]", aTask.getTaskID());

				m_aTaskQueue.add(aTask);
				m_aTaskQueue.notifyAll();
			}
		}

		if (bShutdown)
		{
			log.warn("RT2: TaskProcessor addTask called with shutdown true!");
		}
	}

	//-------------------------------------------------------------------------
	public void removeAll()
		throws Exception
	{
		boolean bShutdown = false;
		synchronized (m_aTaskQueue)
		{
			bShutdown = m_aShutdown.get();
			if (!bShutdown)
				m_aTaskQueue.clear();
		}

		if (bShutdown)
			log.warn("RT2: TaskProcessor removeAll called with shutdown true!");
	}

	//-------------------------------------------------------------------------
	@Override
	public void run()
	{
        while (!m_aShutdown.get())
        {
            T aCurrentTask = null;
			try
			{
				aCurrentTask = m_aTaskQueue.poll(getWaitingTime(), TimeUnit.MILLISECONDS);
				if (aCurrentTask == null) {
					continue;				
				}
				try
				{
					aCurrentTask.process();

					final ITaskListener<T> aListener = m_aListener.get();
					if (null != aListener)
						aListener.taskCompleted(aCurrentTask);
				}
				catch (Throwable t)
				{
					ExceptionUtils.handleThrowable(t);

					final String sTaskDump = aCurrentTask.toString();
					log.error("RT2: Throwable received in TaskProcessor for task [" + sTaskDump + "] run - state unknown!", t);
				}
			}
			catch (InterruptedException e)
			{
				Thread.currentThread().interrupt();
			}
			catch (Throwable t)
			{
				ExceptionUtils.handleThrowable(t);
				String sTaskDump = (aCurrentTask == null) ? "unknown" : aCurrentTask.toString();
				log.error("RT2: Throwable received in TaskProcessor for task [" + sTaskDump + "] run - state unknown!", t);
			}
        }
    }

	//-------------------------------------------------------------------------
	protected long getWaitingTime()
	{
		return DEFAULT_WAIT_TIME;
	}

	//-------------------------------------------------------------------------
	protected boolean canProcessTask(final T aTask)
	{
		return (null != aTask);
	}

	//-------------------------------------------------------------------------
	protected abstract String getThreadName();

}
