/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.jms.RT2AdminJmsConsumer;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;

public class MasterCleanupTask extends Task
{
	private static final Logger log = LoggerFactory.getLogger(MasterCleanupTask.class);
	
    //-------------------------------------------------------------------------
    private final long          m_nCreationTime;

    private final String        m_sNodeUUID;

	private final String        m_sNodeUUIDToCleanup;

    final Map<String, Boolean>  m_aHealthyMembers;

    //-------------------------------------------------------------------------
    
    @Autowired
    private RT2AdminJmsConsumer adminJmsConsumer;
    
    //-------------------------------------------------------------------------
    public MasterCleanupTask(final String sTaskID, final String sNodeUUID, final String sNodeUUIDToCleanup, final Set<String> aHealthyMembers)
    {
        super(sTaskID);

        m_sNodeUUID          = sNodeUUID;
        m_sNodeUUIDToCleanup = sNodeUUIDToCleanup;
        m_nCreationTime      = System.currentTimeMillis();
        m_aHealthyMembers    = new HashMap<>();

        for (String s : aHealthyMembers)
            m_aHealthyMembers.put(s, new Boolean(false));
    }

    //-------------------------------------------------------------------------
    public String getMemberUUID()
    {
        return m_sNodeUUID;
    }

    //-------------------------------------------------------------------------
    public String getMemberUUIDToCleanup()
    {
        return m_sNodeUUIDToCleanup;
    }

    //-------------------------------------------------------------------------
    public long getCreationTime()
    {
        return m_nCreationTime;
    }

    //-------------------------------------------------------------------------
    public synchronized Set<String> getHealthMembersUUID()
    {
        final Set<String> aSet = new HashSet<String>();
        final Set<String> aKeySet = m_aHealthyMembers.keySet();
        for (String s : aKeySet)
            aSet.add(s);

        return aSet;
    }

    //-------------------------------------------------------------------------
    public boolean setMemberToCompleted(String sUUID)
    {
        boolean bCompleted = false;

        synchronized (this)
        {
            if (m_aHealthyMembers.containsKey(sUUID))
            {
                m_aHealthyMembers.put(sUUID, true);

                bCompleted = true;
                final Set<String> aKeySet = m_aHealthyMembers.keySet();
                for (String id : aKeySet)
                    bCompleted &= m_aHealthyMembers.get(id);
            }
        }

        if (bCompleted)
            setCompleted(true);

        return bCompleted;
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean process() throws Exception
    {
        final RT2Message aCleanupForNodeTask = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_TASK_CLEANUP_FOR_CRASHED_NODE);

        // ATTENTION: The message ID MUST BE the taken from the clean-up task, otherwise
        // the master node cannot detect which sub-cleanup task belongs to which master task.
        // There can be more than one clean-up at a time!
        aCleanupForNodeTask.setMessageID(new RT2MessageIdType(getTaskID()));
        RT2MessageGetSet.setAdminHZMemberUUID(aCleanupForNodeTask, getMemberUUIDToCleanup());
        RT2MessageGetSet.setAdminHZMasterUUID(aCleanupForNodeTask, getMemberUUID());

        log.debug("RT2 MasterCleanupTask broadcast admin cleanup task to all nodes for crashed node with uid {}", getMemberUUIDToCleanup());

        adminJmsConsumer.send(aCleanupForNodeTask);

        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public String toString()
    {
        final StringBuffer aTmp = new StringBuffer(super.toString());
        aTmp.append(",member uuid=");
        aTmp.append(this.getMemberUUID());
        aTmp.append(",master uuid=");
        aTmp.append(this.getMemberUUIDToCleanup());
        return aTmp.toString();
    }
}
