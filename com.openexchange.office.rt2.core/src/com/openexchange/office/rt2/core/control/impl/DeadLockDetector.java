/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.openexchange.exception.ExceptionUtils;

public class DeadLockDetector
{
    //-------------------------------------------------------------------------
    private DeadLockDetector ()
    {}
    
    //-------------------------------------------------------------------------
    public static synchronized DeadLockDetector get ()
        throws Exception
    {
        if (m_gSingleton == null)
            m_gSingleton = new DeadLockDetector ();
        return m_gSingleton;
    }
    
    //-------------------------------------------------------------------------
    public synchronized void start ()
        throws Exception
    {
        if (m_aScheduler != null)
            return;
        
        m_aScheduler = Executors.newSingleThreadScheduledExecutor();
        m_aScheduler.scheduleAtFixedRate(new Runnable ()
        {
            @Override
            public void run()
            {
                try
                {
                    impl_check ();
                }
                catch (Throwable ex)
                {
                    ExceptionUtils.handleThrowable(ex);
                    System.err.println (ex.getMessage ());
                    ex.printStackTrace (System.err      );
                }
            }
        }, 10, 10, TimeUnit.SECONDS);
        System.err.println ("DeadLockDetector started ...");
    }
    
    //-------------------------------------------------------------------------
    private synchronized void impl_check ()
        throws Exception
    {
        final ThreadMXBean aThreadMXBean = ManagementFactory.getThreadMXBean      ();
              long[]       lThreadIds    = aThreadMXBean    .findDeadlockedThreads(); // Returns null if no threads are deadlocked.
        final StringBuffer sLog          = new StringBuffer (256);

        if (lThreadIds == null)
            lThreadIds = new long[] {};

        sLog.append ("-------------------------------------------------------------------\n");
        sLog.append ("DEAD LOCK DETECTOR : found " + lThreadIds.length + " threads"    +"\n");
        sLog.append (""                                                                +"\n");
        
        final ThreadInfo[] lThreadInfos = aThreadMXBean.getThreadInfo(lThreadIds);
        for (final ThreadInfo aThreadInfo : lThreadInfos)
        {
            sLog.append ("Thread ["+aThreadInfo.getThreadId()+"] : "+aThreadInfo.getThreadName   ()+"\n");
            sLog.append ("Lock   ["+aThreadInfo.getLockName()+"] : "+aThreadInfo.getLockOwnerName()+"\n");

            final StackTraceElement[] aStack = aThreadInfo.getStackTrace();
            for (final StackTraceElement aStackLine : aStack)
                sLog.append (aStackLine.toString()+"\n");
        }
        
        System.err.println (sLog.toString ());
    }
    
    //-------------------------------------------------------------------------
    private static DeadLockDetector m_gSingleton = null;
    
    //-------------------------------------------------------------------------
    private ScheduledExecutorService m_aScheduler = null;
}
