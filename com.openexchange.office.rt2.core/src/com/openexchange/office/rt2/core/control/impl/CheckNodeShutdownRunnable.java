/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.lang.ref.WeakReference;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.core.control.IRT2NodeHealthManager;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;

public class CheckNodeShutdownRunnable implements Runnable {

	//-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(CheckNodeShutdownRunnable.class);

	//-------------------------------------------------------------------------
	private final WeakReference<IRT2NodeHealthManager> listener;

	//-------------------------------------------------------------------------
	private final String uuid;

	//-------------------------------------------------------------------------
	public CheckNodeShutdownRunnable(IRT2NodeHealthManager listener, String uuid) {
		this.listener = new WeakReference<>(listener);
		this.uuid = uuid;
	}

	//-------------------------------------------------------------------------
	@Override
	public void run() {
		try {
			final IRT2NodeHealthManager rt2NodeHealthManager = WeakRefUtils.getHardRef(listener);
			if ((null != rt2NodeHealthManager) && (StringUtils.isNoneEmpty(uuid))) {
				log.debug("RT2: check node shutdown for node {} done correctly", uuid);

				rt2NodeHealthManager.checkCorrectNodeShutdown(uuid);
			}
		} catch (Exception e) {
			log.error("RT2: Exception caught while trying to check state of shutdown node " + uuid + " after a delay - cannot check state for correct clean-up", e);
		}
	}

}
