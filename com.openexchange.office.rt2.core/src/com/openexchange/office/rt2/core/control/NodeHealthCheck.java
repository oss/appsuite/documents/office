package com.openexchange.office.rt2.core.control;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponse.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.office.tools.annotation.RegisteredService;

@Service
@RegisteredService(registeredClass=HealthCheck.class)
public class NodeHealthCheck implements HealthCheck {

    private static final Logger log = LoggerFactory.getLogger(NodeHealthCheck.class);
    private final String NODE_HEALTH_CHECK = "RT2NodeHealthCheck";
    private final String SERVICE_AVAILABE = "serviceAvailable";

    @Autowired
    RT2NodeHealthMonitor nodeHealthMonitor;

    public NodeHealthCheck() {
        log.info("NodeHealthCheck initialized");
    }

    @Override
    public HealthCheckResponse call() {
        final Map<String, Object> healthData = new HashMap<>();
        final boolean serviceAvailable = (nodeHealthMonitor != null) && !nodeHealthMonitor.isDisposed();

        if (serviceAvailable) {
            final Optional<Map<String, Object>> originalHealthDataOpt = nodeHealthMonitor.checkHealth().getData();

            if (originalHealthDataOpt.isPresent()) {
                healthData.putAll(originalHealthDataOpt.get());
            }
        }

        healthData.put(SERVICE_AVAILABE, Boolean.valueOf(serviceAvailable));

        return new HealthCheckResponse(NODE_HEALTH_CHECK, Status.UP, Optional.of(healthData));
    }
}
