/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control.impl;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.doc.BackgroundSaveReason;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessorClientInfo;
import com.openexchange.office.rt2.core.doc.IBackgroundSavable;
import com.openexchange.office.rt2.core.doc.IDocProcessorContainer;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.Loggable;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;

public class GCLostClientsTask extends Task implements Loggable
{
	private static final Logger log = LoggerFactory.getLogger(GCLostClientsTask.class);

    //-------------------------------------------------------------------------
    private final String  m_sNodeUUID;

    private final String  m_sNodeUUIDToCleanup;

    private AtomicBoolean m_bSuccessful = new AtomicBoolean(false);

    //-------------------------------------------------------------------------

    @Autowired
    private IDocProcessorContainer docProcContainer;
    
    @Autowired
    private ClientEventService clientEventService;
    
    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;
    
    @Autowired
    private ClusterLockService clusterLockService;
    
    @Autowired
    private SpecialLogService specialLogService;
    
    //-------------------------------------------------------------------------
	public GCLostClientsTask(String sTaskID, String sNodeUUID, String sNodeUUIDToCleanup) {
		super(sTaskID);
		m_sNodeUUID          = sNodeUUID;
		m_sNodeUUIDToCleanup = sNodeUUIDToCleanup;
	}

    //-------------------------------------------------------------------------
    public String getMemberUUID() {
        return m_sNodeUUID;
    }

    //-------------------------------------------------------------------------
    public String getMemberUUIDToCleanup() {
        return m_sNodeUUIDToCleanup;
    }

    //-------------------------------------------------------------------------
    public boolean successful() {
        return m_bSuccessful.get();
    }

    //-------------------------------------------------------------------------
    private void setSuccess() {
        m_bSuccessful.set(true);
    }

    //-------------------------------------------------------------------------
	@Override
    public boolean process() throws Exception {
        LogMethodCallHelper.logMethodCall(this, "process");

        try {
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, m_sNodeUUID);
            updateStateOfLocalDocuments(m_sNodeUUIDToCleanup);
            setSuccess();
        } finally {
            setCompleted(true);
            MDC.remove(MDCEntries.BACKEND_UID);
        }
        return true;
    }

    //-------------------------------------------------------------------------
	@Override
	public String toString() {
		final StringBuilder aTmp = new StringBuilder(super.toString());
		aTmp.append(", com.openexchange.rt2.backend.uid: ");
		aTmp.append(this.m_sNodeUUID);
		aTmp.append(", clean-up com.openexchange.rt2.backend.uid: ");
		aTmp.append(this.m_sNodeUUIDToCleanup);
		return aTmp.toString();
	}

    //-------------------------------------------------------------------------
    private void updateStateOfLocalDocuments(final String sNodeToCleanup) throws Exception {
        Set<WeakReference<RT2DocProcessor>> aLocalDocProcessors = docProcContainer.getWeakReferenceToDocProcessors();
        LogMethodCallHelper.logMethodCall(this, "updateStateOfLocalDocuments", sNodeToCleanup, aLocalDocProcessors.size());

        for (final WeakReference<RT2DocProcessor> aWeakRefDocProc : aLocalDocProcessors) {
            final RT2DocProcessor aDocProcessor = aWeakRefDocProc.get();
            if (null != aDocProcessor) {
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, aDocProcessor.getDocUID().getValue());
                try {
                    updateDocumentState(aDocProcessor, sNodeToCleanup);
                } catch (final Exception e) {
                    log.error("GCLostClientsTask: Exception caught while trying to save modified document with com.openexchange.rt2.document.uid " +  aDocProcessor.getDocUID() + " held alive by remote clients on crashed node");
                } finally {
                    MDC.remove(MDCEntries.DOC_UID);
                }
            }
        }
        LogMethodCallHelper.logMethodCallRes(this, "updateStateOfLocalDocuments", Void.class, sNodeToCleanup, docProcContainer.getWeakReferenceToDocProcessors().size());
    }

    //-------------------------------------------------------------------------
    private void updateDocumentState(final RT2DocProcessor aDocProcessor, final String sNodeToCleanup) throws Exception {
        final RT2DocUidType docUID = aDocProcessor.getDocUID();
        final List<DocProcessorClientInfo> clientInfos = aDocProcessor.getClientsInfo();
        final Set<RT2CliendUidType> clientsOnCleanupNode = determineClientsOfDocOnNode(clientInfos, sNodeToCleanup);

        ClusterLock clusterLock = clusterLockService.getLock(docUID);
        try {
            // ATTENTION: We MUST use a document lock and atomic long for the ref-count!
            // In case the ref-count is zero, it's possible that a client wants to dispose
            // document resources while another client wants to create document resources.
            // To have a cluster-wide synchronization we need this lock!
        	clusterLock.lock();
            Set<RT2CliendUidType> remainingClients = distributedDocInfoMap.removeClients(docUID, clientsOnCleanupNode);
            if (remainingClients.isEmpty()) {
            	disposeDocumentResources(aDocProcessor, clientsOnCleanupNode);        	
            } else {
            	deregisterClientsFromDocument(aDocProcessor, clientsOnCleanupNode);
            }
        } finally {
			clusterLock.unlock();
		}        
    }

    //-------------------------------------------------------------------------
    private void deregisterClientsFromDocument(final RT2DocProcessor aDocProcessor, final Set<RT2CliendUidType> aClientsToDeregister) throws Exception {
        if ((null == aDocProcessor) || (null == aClientsToDeregister))
           return;

        for (final RT2CliendUidType sClientUID : aClientsToDeregister) {
        	clientEventService.notifyClientRemoved(new ClientEventData(aDocProcessor.getDocUID(), sClientUID));
        }
    }

    //-------------------------------------------------------------------------
    private void disposeDocumentResources(final RT2DocProcessor aDocProcessor, final Set<RT2CliendUidType> aClientsOfDoc) throws Exception {
        if (null == aDocProcessor)
            return;

        tryToDoEmergencySaveForDocument(aDocProcessor);
        deregisterClientsFromDocument(aDocProcessor, aClientsOfDoc);
        aDocProcessor.dispose();
    }

    //-------------------------------------------------------------------------
    private Set<RT2CliendUidType> determineClientsOfDocOnNode(final List<DocProcessorClientInfo> aClientInfos, final String sNodeToCleanup) throws Exception {
        final Set<RT2CliendUidType> aClientSet = new HashSet<>();

        for (final DocProcessorClientInfo aClientInfo : aClientInfos) {
            if (sNodeToCleanup.equals(aClientInfo.getNodeUUID()))
                aClientSet.add(aClientInfo.getClientUID());
        }

        return aClientSet;
    }

    //-------------------------------------------------------------------------
    private void tryToDoEmergencySaveForDocument(final RT2DocProcessor aDocProcessor) throws Exception {
        final IBackgroundSavable aBackgroundSavable = (IBackgroundSavable)aDocProcessor;
        if ((null != aBackgroundSavable) && (aBackgroundSavable.isModified())) {
            try {
            	specialLogService.log(Level.DEBUG, log, aDocProcessor.getDocUID().getValue(), new Throwable(), "Executing background save for com.openexchange.rt2.document.uid {}", aDocProcessor.getDocUID());
                aBackgroundSavable.save(BackgroundSaveReason.SHUTDOWN, true);
            } catch (Exception e) {
                log.error("CleanupTask: Exception caught while trying to save modified document with com.openexchange.rt2.document.uid " + aDocProcessor.getDocUID() + " held alive by remote clients on crashed node", e);
            }
        }
    }

	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	public Map<String, Object> getAdditionalLogInfo() {
		Map<String, Object> res = new HashMap<>();
		res.put("nodeUuidToCleanup", m_sNodeUUIDToCleanup);
		
		return res;
	}
}
