/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Task
{
    //-------------------------------------------------------------------------
    private AtomicBoolean bCompleted = new AtomicBoolean(false);

    //-------------------------------------------------------------------------
    private final String  sTaskID;

    //-------------------------------------------------------------------------
    public Task(final String sTaskID)
    {
        this.sTaskID = sTaskID;
    }

    //-------------------------------------------------------------------------
    public String getTaskID()
    {
        return sTaskID;
    }

    //-------------------------------------------------------------------------
    public boolean isCompleted()
    { 
    	return bCompleted.get();
    }

    //-------------------------------------------------------------------------
    public abstract boolean process() throws Exception;

	//-------------------------------------------------------------------------
    @Override
    public String toString()
    {
        final String       sClassName = getClass().getCanonicalName();
        final StringBuilder aTmp = new StringBuilder(64);
        aTmp.append((null == sClassName) ? "Task" : sClassName);
        aTmp.append(", task id=");
        aTmp.append(this.getTaskID());
        aTmp.append(", completed = ");
        aTmp.append(bCompleted);
        return aTmp.toString();
    }

	//-------------------------------------------------------------------------
    protected void setCompleted(boolean bCompleted)
    {
    	this.bCompleted.set(bCompleted);
    }
}
