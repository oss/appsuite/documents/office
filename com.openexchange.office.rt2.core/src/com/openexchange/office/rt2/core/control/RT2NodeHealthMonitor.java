/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.control;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponse.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.RT2Constants;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.control.impl.CheckNodeShutdownRunnable;
import com.openexchange.office.rt2.core.control.impl.CleanupTask;
import com.openexchange.office.rt2.core.control.impl.CleanupTaskProcessor;
import com.openexchange.office.rt2.core.control.impl.GCLostClientsTask;
import com.openexchange.office.rt2.core.control.impl.GCLostClientsTaskProcessor;
import com.openexchange.office.rt2.core.control.impl.ITaskTimeoutListener;
import com.openexchange.office.rt2.core.control.impl.ListenerWrapper;
import com.openexchange.office.rt2.core.control.impl.MasterCleanupTask;
import com.openexchange.office.rt2.core.control.impl.PendingMasterCleanupTaskManager;
import com.openexchange.office.rt2.core.jms.RT2AdminJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.osgi.BundleHelper;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap.DistributedDocInfoStatus;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealth;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthMap;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthState;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeHealthState;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.cluster.ClusterLifecycleEvent;
import com.openexchange.office.tools.service.cluster.ClusterLifecycleEvent.LifecycleState;
import com.openexchange.office.tools.service.cluster.ClusterLifecycleListener;
import com.openexchange.office.tools.service.cluster.ClusterMembershipEvent;
import com.openexchange.office.tools.service.cluster.ClusterMembershipListener;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.cluster.ClusterState;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

/**
 * Node health monitor and life-cycle listener to enable the
 * system to take over cleanup tasks from nodes which crashed.
 * Uses internally Hazelcast to get notifications about removed
 * or crashed nodes and which documents are controlled by a
 * backend-node.
 *
 * @author Carsten Driesner
 * @since 7.10.0
 *
 */
@Service
public class RT2NodeHealthMonitor implements IRT2NodeHealthManager, ClusterMembershipListener, ClusterLifecycleListener, ITaskListener<Task>, ITaskTimeoutListener<MasterCleanupTask>, InitializingBean, DisposableBean, OsgiBundleContextAware
{
	private static final Logger log = LoggerFactory.getLogger(RT2NodeHealthMonitor.class);
	private static final int                TIMESPAN_CHECK_SHUTDOWN_NODE      = 600000; // 10 minutes
	private static final long               FREQ_CHECK_OBSOLETE_NODE          = 120000; // 2 minutes
	private static final long               TIMESPAN_REMOVE_OBSOLETE_NODE     = 900000; // 15 minutes
	private static final String []          RT2_SYMBOLIC_BUNDLENAMES_TO_STOP  = { "com.openexchange.office.rt2.osgi", "com.openexchange.office.rt2.core" };
	private static final String             IS_ACTIVE_HZ_MEMBER               = "nodeIsActiveHZMember";
	private static final String             NODE_SHUTDOWN_STATE               = "nodeShutdownState";
	private static final String             DOCS_SERVICES_SHUTDOWN            = "docsServicesShutdown";

    //-----------------------------Services------------------------------------

	@Autowired
	private RT2NodeHealthMap nodeHealthMap;
	
	@Autowired
	private TimerService timerService;
	
	@Autowired
	private RT2AdminJmsConsumer adminJmsConsumer;

	@Autowired
	private RT2DocOnNodeMap docOnNodeMap;
	
	@Autowired
	private DistributedDocInfoMap distributedDocInfoMap;
	
	@Autowired
	private ClusterService clusterService;
	
	@Autowired
	private CachingFacade cachingFacade;

	@Autowired
	private RT2ConfigService rt2ConfigService;

    //-------------------------------------------------------------------------
	private OsgiBundleContextAndActivator bundleCtx;
	
	private CleanupTaskProcessor            m_aCleanupTaskProcessor;

	private PendingMasterCleanupTaskManager m_aMasterCleanupManager;

	private GCLostClientsTaskProcessor      m_aGCLostClientsProcessor;

	private AtomicBoolean                   m_isStarted = new AtomicBoolean(false);

	private AtomicBoolean                   m_isPartOfSafeCluster = new AtomicBoolean(false);

	private AtomicBoolean                   m_shutdownDocumentsService = new AtomicBoolean(false);

	private AtomicReference<LifecycleState> m_aLifeCycleState = new AtomicReference<>(LifecycleState.CLIENT_DISCONNECTED); // impossible case

	private AtomicReference<String>         m_nodeUUID = new AtomicReference<>("");

	private Map<String, Long>               m_nodesToCheckForRemoval = new ConcurrentHashMap<>();

	private ScheduledTimerTask              m_checkObsoleteNodesTimerTask = null;

	private AtomicBoolean                   m_disposed = new AtomicBoolean(false);

    //-------------------------------------------------------------------------    
    @Override
    public void afterPropertiesSet() {
        if (m_isStarted.compareAndSet(false, true)) {
            clusterService.addMembershipListener(this);
            clusterService.addLifecycleListener(this);

            m_aCleanupTaskProcessor = new CleanupTaskProcessor(this);
            m_aMasterCleanupManager = new PendingMasterCleanupTaskManager(this);
            m_aMasterCleanupManager.start();

            final ITaskListener<GCLostClientsTask> aGCListener = new ListenerWrapper<>(this);
            m_aGCLostClientsProcessor = new GCLostClientsTaskProcessor(aGCListener);

            // Hazelcast fails to call the life-cycle listener upon adding ourself as listener
            // to set an initial state. Therefore we have no valid state. We circumvent this
            // strange behavior setting the state by ourself.
            LifecycleState aInitialState = clusterService.isRunning() ? LifecycleState.STARTED : LifecycleState.SHUTDOWN;

            m_aLifeCycleState.compareAndSet(LifecycleState.CLIENT_DISCONNECTED, aInitialState);
            m_isPartOfSafeCluster.set(m_aLifeCycleState.get() == LifecycleState.STARTED);

            // remember current/old node uuid
            m_nodeUUID.set(clusterService.getLocalMemberUuid());

            m_checkObsoleteNodesTimerTask = timerService.scheduleAtFixedRate(new CheckObsoleteNodes(), FREQ_CHECK_OBSOLETE_NODE, FREQ_CHECK_OBSOLETE_NODE);
        }
    }

	//-------------------------------------------------------------------------    
    @Override
	public void destroy() throws Exception {
        if (m_isStarted.compareAndSet(true, false))
        {
            m_disposed.set(true);

            final CleanupTaskProcessor aCleanupTaskProcessor = m_aCleanupTaskProcessor;
            if (null != aCleanupTaskProcessor)
            {
                aCleanupTaskProcessor.shutdown();
                m_aCleanupTaskProcessor = null;
            }

            final PendingMasterCleanupTaskManager aMasterCleanupManager = m_aMasterCleanupManager;
            if (null != aMasterCleanupManager)
            {
                aMasterCleanupManager.stop();
                m_aMasterCleanupManager = null;
            }

            final GCLostClientsTaskProcessor aGCLostClientsProcessor = m_aGCLostClientsProcessor;
            if (null != aGCLostClientsProcessor)
            {
                aGCLostClientsProcessor.shutdown();
                m_aGCLostClientsProcessor = null;
            }

            final ScheduledTimerTask timerTask = m_checkObsoleteNodesTimerTask;
            if (null != timerTask) {
                timerTask.cancel();
                m_checkObsoleteNodesTimerTask = null;
            }

            m_nodesToCheckForRemoval.clear();
        }
	}

    //-------------------------------------------------------------------------
    public boolean isDisposed() {
        return m_disposed.get();
    }

    //-------------------------------------------------------------------------
    @Override
    public HealthCheckResponse checkHealth() {
        Map<String, Object> data = new HashMap<>();
        Status status = Status.DOWN;
        if (!m_disposed.get()) {
            final String clusterNodeUuid = clusterService.getLocalMemberUuid();
            boolean isActiveHzMember = clusterService.isActiveHzMember(clusterNodeUuid);
            boolean nodeShutdown = isNodeShuttingDown();
            boolean docsServicesShutdown = m_shutdownDocumentsService.get();
            if (!nodeShutdown && !docsServicesShutdown && isActiveHzMember) {
                status = Status.UP;
            } else {
                status = Status.DOWN;
            }
            data.put(IS_ACTIVE_HZ_MEMBER, isActiveHzMember);
            data.put(NODE_SHUTDOWN_STATE, (nodeShutdown ? "shutting down" : "up"));
            data.put(DOCS_SERVICES_SHUTDOWN, docsServicesShutdown);
        }
        return new HealthCheckResponse(this.getClass().getCanonicalName(), status, Optional.of(data));
    }

    //-------------------------------------------------------------------------
    @Override
    public void crashedNodeDocCleanupCompleted(RT2Message aCleanupOrderCompletedMsg)
    {
    	LogMethodCallHelper.logMethodCall(log, this, "crashedNodeDocCleanupCompleted", aCleanupOrderCompletedMsg);
    	String sTaskID = aCleanupOrderCompletedMsg.getMessageID().getValue();
        try
        {
        	log.info("RT2NodeHealthMonitor.crashedNodeDocCleanupCompleted started, task id {}", sTaskID);
            final String                          sMemberUUIDCompleted = RT2MessageGetSet.getAdminHZMemberUUID(aCleanupOrderCompletedMsg);
            final PendingMasterCleanupTaskManager aPendingMasterTasks  = m_aMasterCleanupManager;

            // Ensure that this instance is responsible for a master cleanup task - keep in mind
            // that every healthy OX Documents backend node will receive the completed admin task
            // notification.
            MasterCleanupTask aMasterCleanupTask = (null != aPendingMasterTasks) ? aPendingMasterTasks.getTask(sTaskID) : null;
            if (aMasterCleanupTask != null)
            {
                boolean bMasterCompleted     = false;
                String  sMemberUUIDToCleanup = aMasterCleanupTask.getMemberUUIDToCleanup();
                try
                {
                    bMasterCompleted = aPendingMasterTasks.setMemberToCompleted(sTaskID, sMemberUUIDCompleted);
                }
                catch (final NoSuchElementException e)
                {
                	log.warn("RT2NodeHealthMonitor completed cleanup notification from cluster member received, but pending task with id {} not found - should not happen!", sTaskID);
                }

                if (bMasterCompleted)
                    finalizeNodeCleanup(sMemberUUIDToCleanup);
            	log.info("RT2NodeHealthMonitor.crashedNodeDocCleanupCompleted completed");
            }
        }
        catch (Throwable t)
        {
            ExceptionUtils.handleThrowable(t);
            log.error("RT2NodeHealthMonitor task " + sTaskID + " to cleanup local resources failed with exception - resources won't be cleanup!");
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void startLocalNodeDocCleanup(RT2Message aCleanupOrderMsg)
    {
    	LogMethodCallHelper.logMethodCall(log, this, "startLocalNodeDocCleanup", aCleanupOrderMsg);
        String sTaskID = aCleanupOrderMsg.getMessageID().getValue();

        try
        {
        	log.info("RT2NodeHealthMonitor.startLocalNodeDocCleanup started");

            final String      sMemberUUIDToCleanup = RT2MessageGetSet.getAdminHZMemberUUID(aCleanupOrderMsg);
            String nodeUuid = clusterService.getLocalMemberUuid();

            final CleanupTask aCleanupTask = new CleanupTask(sTaskID, nodeUuid, sMemberUUIDToCleanup);
            bundleCtx.injectDependencies(aCleanupTask);
            addCleanupTask(aCleanupTask);

            GCLostClientsTask aGCLostClientsTask = new GCLostClientsTask(sTaskID, nodeUuid, sMemberUUIDToCleanup);
            bundleCtx.injectDependencies(aGCLostClientsTask);
            addGCTask(aGCLostClientsTask);
        }
        catch (Throwable t)
        {
           ExceptionUtils.handleThrowable(t);
           log.error("RT2NodeHealthMonitor task " + sTaskID + " to cleanup local resources failed with exception - resources won't be cleanup!", t);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void timeoutReachedForTask(final MasterCleanupTask aTask) throws Exception
    {
    	log.info("RT2NodeHealthMonitor.timeoutReachedForTask {}", aTask.getTaskID());

        finalizeNodeCleanup(aTask.getMemberUUIDToCleanup());
    }

    //-------------------------------------------------------------------------
    @Override
    public void stateChanged(ClusterLifecycleEvent event) {
        m_aLifeCycleState.set(event.getState());
        if (event.getState() == LifecycleState.SHUTDOWN)
            return;

        try {
            String nodeUuid = clusterService.getLocalMemberUuid();
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, nodeUuid);
            log.info("RT2NodeHealthMonitor.stateChanged {} for com.openexchange.rt2.backend.uid {}", event.getState(), nodeUuid);

            if (event.getState() == ClusterLifecycleEvent.LifecycleState.MERGED) {
                // DOCS-2853
                // This Hazelcast node has merged with the cluster again. Keep in
                // mind that in this situation Hazelcast generated a new node uuid for
                // the cluster node. We need to reset some instances, especially the
                // JMS consumer for the DocProcessors.
                handleStateChangedToMerged();
            }
        } finally {
            MDC.remove(MDCEntries.BACKEND_UID);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void memberAdded(ClusterMembershipEvent membershipEvent) {
        try {
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, membershipEvent.getMemberUid());
            log.info("RT2NodeHealthMonitor.memberAdded {}", membershipEvent.getMemberUid());
        } finally {
            MDC.remove(MDCEntries.BACKEND_UID);
        }
    }


    //-------------------------------------------------------------------------
    public boolean isNodeShuttingDown()
    {
        final LifecycleState aLifeCycleState = m_aLifeCycleState.get();
        return ((aLifeCycleState == LifecycleState.SHUTTING_DOWN) ||
                (aLifeCycleState == LifecycleState.SHUTDOWN));
    }

    //-------------------------------------------------------------------------
    @Override
    public void memberRemoved(ClusterMembershipEvent membershipEvent)
    {
        final String nodeRemovedUUID = membershipEvent.getMemberUid();

    	log.info("RT2NodeHealthMonitor.memberRemoved com.openexchange.rt2.backend.uid {}", membershipEvent.getMemberUid());

        // do nothing if this instance is going down!
        if (clusterService.getLocalMemberUuid().equals(nodeRemovedUUID) || isNodeShuttingDown())
            return;

        LogMethodCallHelper.logMethodCall(log, this, "memberRemoved", nodeRemovedUUID);

        try
        {
            boolean bClusterIsSafe = isClusterInSafeState();
            if (!bClusterIsSafe)
            {
                log.error("Cluster is not in safe state - to prevent alteration of document data this node will shutdown office service!");
                // this node is part of a non-safe cluster therefore  it MUST NEVER do a clean-up
                m_shutdownDocumentsService.set(true);
                asyncShutdownRT2();
                return;
            }

            final RT2NodeHealthState aNodeHealthState = nodeHealthMap.get(nodeRemovedUUID);

            if ((null != aNodeHealthState))
            {
                final String  sCleanupUUID        = aNodeHealthState.getCleanupUUID();
                final String  sNodeHealth         = aNodeHealthState.getState();
                final boolean bNotHandledCrash    = RT2NodeHealth.isNotShutdown(sNodeHealth) && StringUtils.isEmpty(sCleanupUUID);
                final boolean bNotHandledShutdown = RT2NodeHealth.isShutdown(sNodeHealth) && StringUtils.isEmpty(sCleanupUUID);

                if (bNotHandledCrash || bNotHandledShutdown)
                {
                    if (bNotHandledCrash)
                        log.info("RT2NodeHealthMonitor cluster member-removed notification received for member in unsafe state com.openexchange.rt2.backend.uid {} cleanup necessary!", nodeRemovedUUID);
                    else
                        log.info("RT2NodeHealthMonitor cluster member-removed notification received for member with shutdown com.openexchange.rt2.backend.uid {} may be cleanup for dependent nodes necessary!", nodeRemovedUUID);

                    boolean bSuccess = tryToTakeoverOwnership(nodeRemovedUUID, nodeHealthMap, bNotHandledCrash);
                    if (bSuccess)
                    {
                        // ATTENTION: a node which is shutdown decreases the member count on its own -
                        // don't do this twice here!
                        if (!membershipEvent.isLiteMember() && bNotHandledCrash)
                            clusterService.decreaseClusterFullMemberCount();

                        String nodeUuid = clusterService.getLocalMemberUuid();
                        // this node is now responsible for the cleanup
                        if (bNotHandledCrash)
                        {
                            log.info("RT2NodeHealthMonitor this cluster member {} takes over responsiblity to do necessary cleanups of removed member!", nodeUuid);

                            final Set<String> aDocUIDsToCleanup = getDocUIDsForNode(nodeRemovedUUID);
                            log.info("RT2: RT2NodeHealthMonitor.lockDocumentsOnCrashedNode");
                            setRefCountForDocs(aDocUIDsToCleanup, DistributedDocInfoMap.DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE);
                            final Set<String>  myHealthMembers = getHealthyOXDocumentsClusterNodes();
                            addMasterCleanupTask(nodeRemovedUUID, myHealthMembers);
                        }
                        else if (bNotHandledShutdown)
                        {
                            log.info("RT2NodeHealthMonitor sets up check task for shutdown member com.openexchange.rt2.backend.uid {} using a delay of {} ms.", nodeUuid, TIMESPAN_CHECK_SHUTDOWN_NODE);
                            timerService.schedule(new CheckNodeShutdownRunnable(this, sCleanupUUID), TIMESPAN_CHECK_SHUTDOWN_NODE);
                        }

                        takeOverOwnershipForDependentNodes(nodeHealthMap, nodeRemovedUUID);
                    }
                }
            }
            else
                log.info("RT2NodeHealthMonitor cannot find health state for cluster member removed - node already shutdown successfully");
        }
        catch (final Exception e)
        {
            log.error("RT2NodeHealthMonitor exception caught while trying to handle cluster member-removed notification - state of cluster is unknown if no other node can handle the notification", e);
        }
    }

    //-------------------------------------------------------------------------
    private boolean isClusterInSafeState() throws Exception
    {
        boolean bIsInSafeState = false;

        final long         nLastKnownFullMemberCount = cachingFacade.getDistributedAtomicLong(RT2Constants.RT2_CLUSTER_FULL_MEMBER_COUNT).get();
        final ClusterState aClusterState             = clusterService.getClusterState();
        boolean            bClusterStateOk           = aClusterState == ClusterState.ACTIVE;
        final long         nCurrFullNodesCount       = clusterService.determineFullClusterMemberCount();

        if (nCurrFullNodesCount >= ((nLastKnownFullMemberCount / 2) + 1))
        {
            bIsInSafeState = bClusterStateOk;
        }
        else if (nCurrFullNodesCount == (nLastKnownFullMemberCount / 2))
        {
            // IMPORTANT: We lost more full data nodes than we can compensate and both partitions
            // have the same size. Therefore we are lost and cannot determine what part should
            // be shutdown. In this special case we dump an error log message and continue.
        	String memberInfo = clusterService.getFullClusterMemberListAsStr();
            log.error("Detected a loss of full data hazelcast members that cannot be compensated - Cannot switch any part of the cluster into a safe-state. Cluster: " + memberInfo);
            bIsInSafeState = true;
        }

        return bIsInSafeState;
    }

    //-------------------------------------------------------------------------
    private void asyncShutdownRT2()
    {
    	try
    	{
    	    BundleHelper.stopBundles(Arrays.asList(RT2_SYMBOLIC_BUNDLENAMES_TO_STOP));
    	}
    	catch (Exception e)
    	{
    		log.error("RT2NodeHealthMonitor shutdown of RT2 sub-system caught exception!", e);
    	}
    }

    //-------------------------------------------------------------------------
    @Override
    public void taskCompleted(final Task aCompletedTask)
    {
        if (aCompletedTask instanceof CleanupTask)
            handleCompletedCleanupTask((CleanupTask)aCompletedTask);
    }

    //-------------------------------------------------------------------------
    @Override
    public void checkCorrectNodeShutdown(String nodeUUID) {
        try {
            final RT2NodeHealthState aNodeHealthState = nodeHealthMap.get(nodeUUID);

            if (aNodeHealthState != null) {
                final String sNodeHealth = aNodeHealthState.getState();
                if (RT2NodeHealth.RT2_NODE_HEALTH_SHUTTING_DOWN.equals(sNodeHealth)) {
                    finalizeNodeCleanup(nodeUUID);
                } else if (RT2NodeHealth.RT2_NODE_HEALTH_SHUTDOWN.equals(sNodeHealth)) {
                    removeCleanedupMemberFromHealthMap(nodeUUID);
                } else {
                    log.warn("Unexpected state {} detected for node com.openexchange.rt2.backend.uid {} that should be in or was in shutdown mode", aNodeHealthState, nodeUUID);
                }
            }
        } catch (final Exception e) {
            log.error("RT2NodeHealthMonitor caught exception to check correct state of shutdown node com.openexchange.rt2.backend.uid " + nodeUUID, e);
        }
    }

    //-------------------------------------------------------------------------
    private void handleCompletedCleanupTask(final CleanupTask aCompletedCleanupTask)
    {
    	String nodeUuid = clusterService.getLocalMemberUuid();
        if (aCompletedCleanupTask.successful())
        {
            try
            {
                final RT2Message   aCompletedTaskMsg = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_TASK_COMPLETED_CLEANUP_FOR_CRASHED_NODE);

                // ATTENTION:
                // use the same message id to enable the responsible admin channel
                // to identify which close doc task was completed
                aCompletedTaskMsg.setMessageID(new RT2MessageIdType(aCompletedCleanupTask.getTaskID().toString()));
                RT2MessageGetSet.setAdminHZMemberUUID(aCompletedTaskMsg, nodeUuid);
                RT2MessageGetSet.setAdminHZMasterUUID(aCompletedTaskMsg, aCompletedCleanupTask.getMasterUUID().toString());

                adminJmsConsumer.send(aCompletedTaskMsg);
            }
            catch (final Exception e)
            {
            	log.warn("RT2NodeHealthMonitor clean up of health map for member com.openexchange.rt2.backend.uid " + nodeUuid + " failed.", e);
            }
        }
        else
        {
        	log.error("RT2NodeHealthMonitor clean up for member com.openexchange.rt2.backend.uid " + nodeUuid + " failed. It's possible that certain documents cannot be opened anymore.");
        }
    }

    //-------------------------------------------------------------------------
    private void handleStateChangedToMerged() {
        try {
            boolean bHandled = false;
            String oldHZNodeUUID = m_nodeUUID.get();
            String newHZNodeUUID = clusterService.getLocalMemberUuid();
            if (!oldHZNodeUUID.equals(newHZNodeUUID)) {
                log.info("RT2NodeHealthMonitor detected change of com.openexchange.rt2.backend.uid due to merge of lost cluster node. New com.openexchange.rt2.backend.uid {} and old com.openexchange.rt2.backend.uid {}", newHZNodeUUID, oldHZNodeUUID);
                m_nodeUUID.set(newHZNodeUUID);

                final String sHzMapName = nodeHealthMap.getUniqueMapName();
                final DistributedMap<String, PortableNodeHealthState> aHzMap = cachingFacade.getDistributedMap(sHzMapName, String.class, PortableNodeHealthState.class);
                boolean bLocked = false;
                try {
                    int nRetryCount = 2;
                    while ((nRetryCount > 0) && !bLocked) {
                        bLocked = aHzMap.tryLock(newHZNodeUUID, 1000, TimeUnit.MILLISECONDS);
                        if (bLocked) {
                            RT2NodeHealthState newNodeHealthState = nodeHealthMap.get(newHZNodeUUID);
                            if (newNodeHealthState == null) {
                                newNodeHealthState = createNewNodeHealthState();
                                nodeHealthMap.set(newHZNodeUUID, newNodeHealthState);
                            }

                            // handle old node entry - set to not member anymore
                            final RT2NodeHealthState oldNodeHealthState = nodeHealthMap.get(oldHZNodeUUID);
                            if (oldNodeHealthState != null) {
                                oldNodeHealthState.setState(RT2NodeHealth.RT2_NODE_HEALTH_NOT_MEMBER_ANYMORE);
                                nodeHealthMap.set(oldHZNodeUUID, oldNodeHealthState);
                                m_nodesToCheckForRemoval.put(oldHZNodeUUID, System.currentTimeMillis());
                            }

                            log.info("RT2NodeHealthMonitor deregister obsolete RT2DocProcessorJmsConsumer for com.openexchange.rt2.backend.uid {}", oldHZNodeUUID);
                            deregisterAndDestroyDocProcessorJmsConsumer();

                            log.info("RT2NodeHealthMonitor register new RT2DocProcessorJmsConsumer for new com.openexchange.rt2.backend.uid {}", newHZNodeUUID);
                            registerNewDocProcessorJmsConsumer();
                            bHandled = true;
                        }

                        nRetryCount--;
                    }
                } catch (final InterruptedException e) {
                    Thread.currentThread().interrupt();

                    if (!bHandled) {
                        log.warn("RT2NodeHealthMonitor interrupted exception caught while trying to handle stateChanged to MERGED of member " + newHZNodeUUID, e);
                    }
                } finally {
                    if (bLocked)
                        aHzMap.unlock(newHZNodeUUID);
                    if (!bHandled) {
                        log.error("RT2NodeHealthMonitor could not handle merge of cluster member {} correctly. If further problems are detected, please try to restart this member", newHZNodeUUID);
                    }
                }
            }
        } catch (final Exception e) {
            log.error("RT2NodeHealthMonitor exception caught while trying to handle stateChanged to MERGED of member " + clusterService.getLocalMemberUuid() + ". Please try to restart this member to have a full working node again.", e);
        }
    }

    //-------------------------------------------------------------------------
    private RT2NodeHealthState createNewNodeHealthState() {
        return new RT2NodeHealthState(
            clusterService.getLocalMemberUuid(),
            rt2ConfigService.getOXNodeID(),
            RT2NodeHealth.RT2_NODE_HEALTH_UP,
            RT2NodeHealth.getNodeTypeString(!clusterService.isLocalMemberLiteMember()),
            RT2NodeHealth.RT2_CLEANUP_UUID_EMPTY);
    }

    //-------------------------------------------------------------------------
    private void deregisterAndDestroyDocProcessorJmsConsumer() throws Exception {
        RT2DocProcessorJmsConsumer oldDocProcessorJmsConsumer = bundleCtx.getService(RT2DocProcessorJmsConsumer.class);
        if (oldDocProcessorJmsConsumer != null) {
            oldDocProcessorJmsConsumer.destroy();
        }
        bundleCtx.unregisterService(oldDocProcessorJmsConsumer);
    }

    //-------------------------------------------------------------------------
    private void registerNewDocProcessorJmsConsumer() throws Exception {
        RT2DocProcessorJmsConsumer newDocProcessorJmsConsumer = new RT2DocProcessorJmsConsumer();
        bundleCtx.registerService(newDocProcessorJmsConsumer, true);
        newDocProcessorJmsConsumer.startReceiveMessages();
    }

    //-------------------------------------------------------------------------
    private boolean takeOverOwnershipForDependentNodes(final RT2NodeHealthMap aNodeHealthMap, String sNodeRemovedUUID)
    {
        boolean bTakeOverCompleted = false;

        try
        {
        	log.info("RT2NodeHealthMonitor.takeOverOwnershipForDependentNodes for com.openexchange.rt2.backend.uid {}", sNodeRemovedUUID);

        	// check for nodes where the removed node is responsible for clean-up
            final Set<RT2NodeHealthState> aDepCleanupNodes = aNodeHealthMap.getCleanupNodesOfMember(sNodeRemovedUUID);
            if ((null != aDepCleanupNodes) && (!aDepCleanupNodes.isEmpty()))
            {
                final Set<String> aHealthyNodes = this.getHealthyOXDocumentsClusterNodes();
                for (final RT2NodeHealthState aState : aDepCleanupNodes)
                {
                    final String sNodeUUIDToCleanup = aState.getNodeUUID();

                    // take over responsibility and update hz health map
                    aState.setCleanupUUID(clusterService.getLocalMemberUuid());
                    aNodeHealthMap.set(sNodeUUIDToCleanup, aState);

                    // add new master cleanup task for dependent node
                    addMasterCleanupTask(sNodeUUIDToCleanup, aHealthyNodes);
                }
            }

            bTakeOverCompleted = true;
        }
        catch (final Exception e)
        {
        	log.error("RT2NodeHealthMonitor clean up for dependent members failed. It's possible that certain documents cannot be opened anymore.", e);
        }

        return bTakeOverCompleted;
	}

    //-------------------------------------------------------------------------
    private boolean tryToTakeoverOwnership(String sNodeRemovedUUID, final RT2NodeHealthMap aNodeHealthMap, boolean bCrashedNode)
        throws Exception
    {
        final String                                sHzMapName = aNodeHealthMap.getUniqueMapName();
        final DistributedMap<String, PortableNodeHealthState> aHzMap = cachingFacade.getDistributedMap(sHzMapName, String.class, PortableNodeHealthState.class);

        boolean bLocked  = false;
        boolean bHandled = false;

    	log.info("RT2NodeHealthMonitor.takeOverOwnershipForDependentNodes com.openexchange.rt2.backend.uid {}, crashed node {}", sNodeRemovedUUID, bCrashedNode);

        try
        {
            int nRetryCount = 2;

            while ((nRetryCount > 0) && !bLocked)
            {
            	bLocked = aHzMap.tryLock(sNodeRemovedUUID.toString(), 1000, TimeUnit.MILLISECONDS);
                if (bLocked)
                {
                    // read state again to determine that no other node acquired the cleanup baton
                    final RT2NodeHealthState aCurrNodeHealthState = aNodeHealthMap.get(sNodeRemovedUUID);
                    final String             sCleanupUUID = (null != aCurrNodeHealthState) ? aCurrNodeHealthState.getCleanupUUID(): null;

                    nRetryCount = 0;
                    if ((aCurrNodeHealthState != null) && (StringUtils.isEmpty(sCleanupUUID)))
                    {
                        // update health state (only for a crashed node) and set clean-up node uuid
                        if (bCrashedNode)
                            aCurrNodeHealthState.setState(RT2NodeHealth.RT2_NODE_HEALTH_NOT_MEMBER_ANYMORE);
                        aCurrNodeHealthState.setCleanupUUID(clusterService.getLocalMemberUuid());
                        aNodeHealthMap.set(sNodeRemovedUUID, aCurrNodeHealthState);

                    	log.info("RT2NodeHealthMonitor.takeOverOwnershipForDependentNodes - successful com.openexchange.rt2.backend.uid {}, crashed node {}", sNodeRemovedUUID, bCrashedNode);

                        bHandled = true;
                    }
                }

                nRetryCount--;
            }
        }
        catch (final InterruptedException e)
        {
            Thread.currentThread().interrupt();

            if (!bHandled)
            {
            	log.warn("RT2NodeHealthMonitor interrupted exception caught while trying to handle member-remove notification - state of cluster is unknown if no other member can handle the notification!", e);
            }
        }
        finally
        {
            if (bLocked)
                aHzMap.unlock(sNodeRemovedUUID.toString());
        }

        return bHandled;
	}

    //-------------------------------------------------------------------------
    private void addMasterCleanupTask(String sCrashedNodeUUID, final Set<String> aHealthyMemberUUIDs) throws Exception
    {
        final String            sTaskID = UUID.randomUUID().toString();
        final MasterCleanupTask aTask   = new MasterCleanupTask(sTaskID, clusterService.getLocalMemberUuid(), sCrashedNodeUUID, aHealthyMemberUUIDs);
        bundleCtx.injectDependencies(aTask);

    	log.info("RT2NodeHealthMonitor.addMasterCleanupTask {}, crashed node {}", sTaskID, sCrashedNodeUUID);

        // store cleanup task in our pending map to wait for cluster member
        // notifications doing the local cleanup
        final PendingMasterCleanupTaskManager aMasterCleanupManager = m_aMasterCleanupManager;
        if (null != aMasterCleanupManager)
        {
            aMasterCleanupManager.storeTask(aTask);
            addCleanupTask(aTask);
        }
        else
        {
        	log.error("RT2NodeHealthMonitor tries to start clean-up crashed cluster, but there is no valid PendingMasterCleanupTaskManager instance. Clean-up impossible!");
        }
    }

    //-------------------------------------------------------------------------
    private void addCleanupTask(final Task aTask) throws Exception
    {
    	log.info("RT2NodeHealthMonitor.addCleanupTask {}", aTask.getTaskID());

        final TaskProcessor<Task> aTaskProcessor = m_aCleanupTaskProcessor;
        if (null != aTaskProcessor)
        {
            aTaskProcessor.start();
            aTaskProcessor.addTask(aTask);
        }
        else
        {
        	log.error("RT2NodeHealthMonitor no cleanup task processor, therefore task cannot be processed!");
        }
    }

    //-------------------------------------------------------------------------
    private void addGCTask(final GCLostClientsTask aGCTask) throws Exception
    {
    	log.info("RT2NodeHealthMonitor.addGCTask {}", aGCTask.getTaskID());

        final TaskProcessor<GCLostClientsTask> aTaskProcessor = m_aGCLostClientsProcessor;
        if (null != aTaskProcessor)
        {
            aTaskProcessor.start();
            aTaskProcessor.addTask(aGCTask);
        }
        else
        {
        	log.error("RT2NodeHealthMonitor gc lost clients task processor, therefore task cannot be processed!");
        }
    }

    //-------------------------------------------------------------------------
    private Set<String> getHealthyOXDocumentsClusterNodes() throws OXException {
        final Set<String>                           aResult         = new HashSet<>();

        final Set<RT2NodeHealthState>               aHealthyMembers = nodeHealthMap.getMembersOfState(RT2NodeHealth.RT2_NODE_HEALTH_UP);
        for (final RT2NodeHealthState aMemberState : aHealthyMembers)
            aResult.add(aMemberState.getNodeUUID());

        return aResult;
    }

    //-------------------------------------------------------------------------
    private void finalizeNodeCleanup(final String sNodeUUIDToCleanup) throws Exception {
    	log.info("RT2NodeHealthMonitor.finalizeNodeCleanup for com.openexchange.rt2.backend.uid {}", sNodeUUIDToCleanup);

    	LogMethodCallHelper.logMethodCall(log, "finalizeNodeCleanup", sNodeUUIDToCleanup.toString());
        Validate.notNull(sNodeUUIDToCleanup);

        final Set<String> aDocUIDsToCleanup = getDocUIDsForNode(sNodeUUIDToCleanup);
        if (!aDocUIDsToCleanup.isEmpty()) {
            cleanupDocRoutesAndMappings(aDocUIDsToCleanup);
        	log.info("RT2NodeHealthMonitor.unlockDocumentsOnCrashedNode");
        	aDocUIDsToCleanup.forEach(d -> distributedDocInfoMap.freeDocInfos(new RT2DocUidType(d)));
        }

        removeCleanedupMemberFromHealthMap(sNodeUUIDToCleanup);

        LogMethodCallHelper.logMethodCallRes(log, this.getClass(), "finalizeNodeCleanup", Void.class, sNodeUUIDToCleanup);
    }

    //-------------------------------------------------------------------------
    private void cleanupDocRoutesAndMappings(final Set<String> aDocUIDsToCleanup) {
    	log.info("RT2NodeHealthMonitor.cleanupDocRoutesAndMappings");
        if (!aDocUIDsToCleanup.isEmpty()) {
        	docOnNodeMap.remove(aDocUIDsToCleanup);
        }
    }

    //-------------------------------------------------------------------------
    private void setRefCountForDocs(final Set<String> aDocUIDsToCleanup, DistributedDocInfoStatus status) {
        if (!aDocUIDsToCleanup.isEmpty()) {
            for (final String sDocUID : aDocUIDsToCleanup) {
                distributedDocInfoMap.setStatus(new RT2DocUidType(sDocUID), status);                
            }
        }
    }

    //-------------------------------------------------------------------------
    private void removeCleanedupMemberFromHealthMap(final String sNodeUUIDToCleanup) throws OXException {
    	log.info("RT2NodeHealthMonitor.removeCleanedupMemberFromHealthMap com.openexchange.rt2.backend.uid {}", sNodeUUIDToCleanup);
    	nodeHealthMap.remove(sNodeUUIDToCleanup);
    }

    //-------------------------------------------------------------------------
    private Set<String> getDocUIDsForNode(final String sNodeUUID) throws OXException {
        return docOnNodeMap.getDocsOfMember(sNodeUUID);
    }

    //-------------------------------------------------------------------------
	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}

    //-------------------------------------------------------------------------
    class CheckObsoleteNodes implements Runnable {

        //-------------------------------------------------------------------------
        @Override
        public void run() {
            try {
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "CheckObsoleteNodes");
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
                checkAndRemoveObsoleteNodeEntries();
            } finally {
                MDC.remove(MDCEntries.BACKEND_PART);
                MDC.remove(MDCEntries.BACKEND_UID);
            }
        }

        //-------------------------------------------------------------------------
        private void checkAndRemoveObsoleteNodeEntries() {
            try {
                // find all entries for immediate removal
                final Set<String> entriesToRemove = new HashSet<>();
                final long now = System.currentTimeMillis();
                m_nodesToCheckForRemoval.keySet().stream().forEach(n -> {
                    final Long timeStamp = m_nodesToCheckForRemoval.get(n);
                    if (timeStamp != null) {
                        if ((timeStamp + TIMESPAN_REMOVE_OBSOLETE_NODE) < now) {
                            entriesToRemove.add(n);
                        }
                    }
                });

                // remove all entries for immediate removal
                entriesToRemove.stream().forEach(n -> {
                    try {
                        removeCleanedupMemberFromHealthMap(n);
                        m_nodesToCheckForRemoval.remove(n);
                    } catch (OXException e) {
                        log.warn("CheckObsoleteNodes caught exception trying to remove node health entry " + n, e);
                    }
                });

                // find possible obsolete entries in HZ nodeHealthMap
                final Set<RT2NodeHealthState> allMembers = nodeHealthMap.getAllMembers();
                allMembers.stream().forEach(s -> {
                    String nodeUUID = s.getNodeUUID();
                    if (!clusterService.isActiveHzMember(nodeUUID)) {
                        m_nodesToCheckForRemoval.putIfAbsent(nodeUUID, now);
                    } else {
                        m_nodesToCheckForRemoval.remove(nodeUUID);
                    }
                });
            } catch (Throwable t) {
                ExceptionUtils.handleThrowable(t);
                log.error("CheckObsoleteNodes caught exception try to find/clean-up obsolete node health entries", t);
            }
        }
    }

}
