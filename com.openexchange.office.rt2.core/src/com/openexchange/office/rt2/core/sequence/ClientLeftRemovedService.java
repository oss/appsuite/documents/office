/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.exception.RT2InvalidClientUIDException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@Async(initialDelay=ClientLeftRemovedService.FREQ_CLEANUP_CLIENTS_LEFT, period=ClientLeftRemovedService.FREQ_CLEANUP_CLIENTS_LEFT, timeUnit=TimeUnit.MILLISECONDS)
public class ClientLeftRemovedService implements Runnable, IDocProcessorEventListener, IClientEventListener {

	private static final Logger log = LoggerFactory.getLogger(ClientLeftRemovedService.class);

	//-------------------------------------------------------------------------	
	public static final long TIME_CLEANUP_CLIENTS_LEFT = 300000;

	public static final long FREQ_CLEANUP_CLIENTS_LEFT = 60000;

	//--------------------------Services--------------------------------------	
	@Autowired
	private ClientSequenceQueueDisposerService clientSequenceQueueDisposerService;

	@Autowired
	private RT2MessageSender messageSender;

	@Autowired
	private DocProcessorMessageLoggerService messageLoggerService;

	@Autowired
	private ClusterService clusterService;

	//-------------------------------------------------------------------------
	private final ConcurrentHashMap<RT2DocUidType, Map<RT2CliendUidType, Long>> clientsLeft = new ConcurrentHashMap<>();

	//-------------------------------------------------------------------------	
	public void addClientLeft(RT2DocUidType docUid, RT2CliendUidType clientUid) {
		getClientMapOfDoc(docUid).put(clientUid, System.currentTimeMillis());
	}

	//-------------------------------------------------------------------------
	public long getClientLeft(RT2DocUidType docUid, RT2CliendUidType clientUid) {
		Map<RT2CliendUidType, Long> clientMap = getClientMapOfDoc(docUid);
		if (clientMap.containsKey(clientUid)) {
				return clientMap.get(clientUid);
		}
		return -1l;
	}

	//-------------------------------------------------------------------------	
    public boolean checkStateIfClientUnknown(final RT2DocUidType docUid, final RT2CliendUidType sClientUID, final RT2Message msgFromUnknownClient) throws RT2InvalidClientUIDException {
    	final RT2MessageType msgType = msgFromUnknownClient.getType();

		switch (msgType) {
			case ACK_SIMPLE:
			case NACK_SIMPLE: {
				// We ignore low-level msgs if the client uid is not known - this
				// could be a race of msgs where a client is about to leave and we
				// already received the last msg, but the leave-process is still in
				// progress - therefore the RT2DocProxy can send msgs to us!
				return true;
			}
			case REQUEST_CLOSE_DOC:
			case REQUEST_LEAVE: {
				long timeLeft = getClientLeft(docUid, sClientUID);
				if (timeLeft >= 0) {
					// We handle messages from clients that try to close/leave although they
					// already left the document instance more gracefully. This can happen if
					// we close/leave a client due to previous errors which the client hasn't
					// received yet.
					final Date date = new Date(timeLeft);
					log.debug("RT2: received message {} with com.openexchange.rt2.client.uid {} that left already at {} - just ignore message", msgFromUnknownClient, sClientUID, SimpleDateFormat.getDateInstance().format(date));
					return true;
				}
				break;
			}
			default: { /* nothing to do - throws exception */ } break;
			}

		log.warn("RT2: received message {} with unknown com.openexchange.rt2.client.uid {}", msgFromUnknownClient, sClientUID);
		throw new RT2InvalidClientUIDException(messageLoggerService.getMsgsAsList(docUid));
    }

	//-------------------------------------------------------------------------	
	@Override
	public void run() {
		checkIfClientHasToBeRemoved();
		checkForGapsInMessages();
	}

	private void checkForGapsInMessages() {
		clientsLeft.forEach((docUid, m) -> {
			for (RT2CliendUidType clientUid : clientSequenceQueueDisposerService.determineReceivers(docUid, null)) {
				try {					
					ClientSequenceQueue clientSequenceQueue = clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid, clientUid, false);
					if (clientSequenceQueue != null) {
						if (clientSequenceQueue.hasQueuedInMessages()) {
							RT2Message nackMsg = clientSequenceQueueDisposerService.checkForAndRequestMissingMessagesViaNack(docUid, clientUid);
							if (nackMsg != null) {
								messageSender.sendMessageTo(clientUid, nackMsg);
							}
						}
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				}
			}
		});
	}

	private void checkIfClientHasToBeRemoved() {
		try {
			MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "ClientLeftRemoveThread");
			MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());

			final long now = System.currentTimeMillis();

			clientsLeft.values().forEach(c -> {
				Set<RT2CliendUidType> clientsToCleanup = c.keySet()
						.stream()
						.filter(k -> { return needToCleanupClientUID(c.get(k), now); })
						.collect(Collectors.toSet());
				clientsToCleanup.forEach(k -> { c.remove(k); });
				if ((clientsToCleanup != null) && (!clientsToCleanup.isEmpty())) {
					log.debug("RT2: removing {} left com.openexchange.rt2.client.uids after timeout", clientsToCleanup.size());
				}
			});
		} catch (Exception e) {
            log.warn("RT2: Exception caught while trying to lazy remove stored left com.openexchange.rt2.client.uids", e);
		} finally {
			MDC.remove(MDCEntries.BACKEND_PART);
			MDC.remove(MDCEntries.BACKEND_UID);
		}
	}

	//-------------------------------------------------------------------------
	private static boolean needToCleanupClientUID(final Long time, long now) {
		return (time != null) ? ((now - time) >= TIME_CLEANUP_CLIENTS_LEFT) : false;
	}

	@Override
	public void docCreated(RT2DocUidType docUid) {
		if (clientsLeft.put(docUid, Collections.synchronizedMap(new HashMap<>())) != null) {
			log.warn("New doc instance added, but old doc instance for com.openexchange.rt2.document.uid {} found! Overwrite it!", docUid);
		}
	}

	@Override
	public void docDisposed(RT2DocUidType docUid) {
		clientsLeft.remove(docUid);
	}

	@Override
	public void clientAdded(ClientEventData clientEventData) {
	}

	@Override
	public void clientRemoved(ClientEventData clientEventData) {
		if (clientsLeft.contains(clientEventData.getDocUid())) {
			clientsLeft.get(clientEventData.getDocUid()).remove(clientEventData.getClientUid());
		}
	}

	private Map<RT2CliendUidType, Long> getClientMapOfDoc(RT2DocUidType docUid) {
		Map<RT2CliendUidType, Long> newMap = Collections.synchronizedMap(new HashMap<>());
		Map<RT2CliendUidType, Long> oldMap = clientsLeft.putIfAbsent(docUid, newMap);
		if (oldMap != null) {
			newMap = oldMap;
		}
		return newMap;
	}
}
