/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.exception.LoadDocumentAbortedByClientException;
import com.openexchange.office.rt2.core.exception.RT2InvalidClientUIDException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class ClientSequenceQueueDisposerService implements IClientEventListener, IDocProcessorEventListener {

	private static final Logger log = LoggerFactory.getLogger(ClientSequenceQueueDisposerService.class);

	private final ConcurrentHashMap<RT2DocUidType, Map<RT2CliendUidType, ClientSequenceQueue>> clientsSeqQueuesOfDocs = new ConcurrentHashMap<>(); //Collections.synchronizedMap(new HashMap<>());

	@Autowired
	private ClientLeftRemovedService clientLeftRemovedService;

	@Autowired
	private DocProcessorMessageLoggerService messageLoggerService;

	@Autowired
	private RT2ConfigService rt2ConfigService;

	//-------------------------------------------------------------------------
    @Override
	public void clientAdded(ClientEventData clientEventData) {
    	final RT2CliendUidType clientUID = clientEventData.getClientUid();
    	final RT2DocUidType docUid = clientEventData.getDocUid();
		final Object old = clientsSeqQueuesOfDocs.get(docUid).put(clientUID, new ClientSequenceQueue(clientUID, rt2ConfigService.getNackFrequenceOfServer()));
		MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
		MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUID.getValue());
		try {
	        if (old != null) {
	            log.warn("RT2: Client sequence queue overwritten for com.openexchange.rt2.client.uid {} in com.openexchange.rt2.document.uid {}!", clientUID, docUid);
	        }
	        log.debug("ClientSequenceQueueDisposerService added client sequence queue of com.openexchange.rt2.client.uid {} for com.openexchange.rt2.document.uid {}", clientUID, docUid);
		} finally {
		    MDC.remove(MDCEntries.DOC_UID);
		    MDC.remove(MDCEntries.CLIENT_UID);
		}
	}

	//-------------------------------------------------------------------------
	@Override
	public void clientRemoved(ClientEventData clientEventData) {
		final RT2CliendUidType clientUid = clientEventData.getClientUid();
    	final RT2DocUidType docUid = clientEventData.getDocUid();
		if (clientsSeqQueuesOfDocs.containsKey(clientEventData.getDocUid())) {
			ClientSequenceQueue aClientQueue = clientsSeqQueuesOfDocs.get(docUid).remove(clientUid);
	        if (aClientQueue != null) {
	            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
	            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUid.getValue());
	            try {
	                clientLeftRemovedService.addClientLeft(docUid, clientUid);
	                log.debug("ClientSequenceQueueDisposerService removed client sequence queue of com.openexchange.rt2.client.uid {} for com.openexchange.rt2.document.uid {}", clientUid, docUid);
	            } finally {
	                MDC.remove(MDCEntries.DOC_UID);
	                MDC.remove(MDCEntries.CLIENT_UID);
	            }
	        }
		}
	}

	//-------------------------------------------------------------------------
    @Override
	public void docCreated(RT2DocUidType docUid) {
        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
        try {
            log.debug("ClientSequenceQueueDisposerService notification 'docCreated' received for com.openexchange.rt2.document.uid {}", docUid);
            final Map<RT2CliendUidType, ClientSequenceQueue> newMap = Collections.synchronizedMap(new HashMap<>());
            clientsSeqQueuesOfDocs.put(docUid, newMap);
            log.debug("ClientSequenceQueueDisposerService empty client sequence queue set for com.openexchange.rt2.document.uid {}", docUid);
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
        }
	}

	//-------------------------------------------------------------------------
	@Override
	public void docDisposed(RT2DocUidType docUid) {
        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
        try {
            log.debug("ClientSequenceQueueDisposerService notification 'docDisposed' received for com.openexchange.rt2.document.uid {}", docUid);
            clientsSeqQueuesOfDocs.remove(docUid);
            log.debug("ClientSequenceQueueDisposerService client sequence queues removed for com.openexchange.rt2.document.uid {}", docUid);
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
        }
	}

    //-------------------------------------------------------------------------
	public ClientSequenceQueue getClientSequenceQueueForClient(final RT2DocUidType docUID, final RT2CliendUidType clientUID, boolean throwEx) throws RT2TypedException {
		Map<RT2CliendUidType, ClientSequenceQueue> clientsSeqQueuesOfDoc = getClientsSeqQueuesOfDoc(docUID, throwEx);
		if (clientsSeqQueuesOfDoc == null) {
			return null;
		}
		final ClientSequenceQueue aClientSeqQueue = clientsSeqQueuesOfDoc.get(clientUID);
        if (throwEx && (aClientSeqQueue == null)) {
            throw new RT2TypedException(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR, messageLoggerService.getMsgsAsList(docUID));
        }
		return aClientSeqQueue;
	}

	//-------------------------------------------------------------------------
	public Map<RT2CliendUidType, ClientSequenceQueue> getClientsSeqQueuesOfDoc(final RT2DocUidType docUID, boolean throwEx) throws RT2TypedException {
		Map<RT2CliendUidType, ClientSequenceQueue> clientsSeqQueuesOfDoc = clientsSeqQueuesOfDocs.get(docUID);
		if (throwEx && (clientsSeqQueuesOfDoc == null)) {
			throw new RT2TypedException(ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR, messageLoggerService.getMsgsAsList(docUID));
		}
		return clientsSeqQueuesOfDoc;
	}

	//-------------------------------------------------------------------------
	public void hasAbortOpenBeenReceived(final RT2DocUidType docUID, final RT2CliendUidType clientUID) throws RT2TypedException {
		if (getClientSequenceQueueForClient(docUID, clientUID, true).isAborted()) {
			throw new LoadDocumentAbortedByClientException(messageLoggerService.getMsgsAsList(docUID));
		}
	}

	//-------------------------------------------------------------------------
	public Collection<RT2CliendUidType> determineReceivers(final RT2DocUidType docUID, final RT2CliendUidType exceptClientID)	{
		Map<RT2CliendUidType, ClientSequenceQueue> clientsSeqQueuesOfDoc = clientsSeqQueuesOfDocs.get(docUID);
		if (clientsSeqQueuesOfDoc != null) {
			synchronized (clientsSeqQueuesOfDoc) {
				final Set<RT2CliendUidType> aClientSet = clientsSeqQueuesOfDoc.keySet();
				return aClientSet.stream()
                                 .filter(c -> !c.equals(exceptClientID))
                                 .collect(Collectors.toSet());
			}
		}
		return Collections.emptySet();
	}

	//-------------------------------------------------------------------------
	public boolean setSeqNrOnMsg(final RT2CliendUidType to, final RT2Message msg) {
		try {
			ClientSequenceQueue aClientSeqQueue = getClientSequenceQueueForClient(msg.getDocUID(), to, true);
	        int nSeq = aClientSeqQueue.incAndGetOutSeqNumber();
	        RT2MessageGetSet.setSeqNumber(msg, nSeq);
	        log.trace("RT2: set SEQ-NR {} for message {}", nSeq, msg.getHeader());
		} catch (ValidationException | RT2TypedException ex) {
			return false;
		}
        return true;
	}

	//-------------------------------------------------------------------------
    public RT2Message checkForAndRequestMissingMessagesViaNack(RT2DocUidType docUid, final RT2CliendUidType clientUID)	throws Exception {
        ClientSequenceQueue clientSeqQueue = getClientSequenceQueueForClient(docUid, clientUID, true);
        Set<Integer> nacks = clientSeqQueue.checkForAndGenerateNacks();
		if (!nacks.isEmpty()) {
			final RT2Message aNackRequest = RT2MessageFactory.newSimpleNackRequest(clientUID, docUid, nacks);
			log.debug("RT2: Sending NACK for com.openexchange.rt2.client.uid {} with SEQ-NRS: {}", clientUID, nacks.toString());
			return aNackRequest;
		}
		return null;
    }

    //-------------------------------------------------------------------------
    public boolean checkStateIfClientUnknown(final RT2DocUidType docUid, final RT2CliendUidType sClientUID, final RT2Message msgFromUnknownClient) throws RT2InvalidClientUIDException {
    	return clientLeftRemovedService.checkStateIfClientUnknown(docUid, sClientUID, msgFromUnknownClient);
    }
}
