/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

//-------------------------------------------------------------------------
public class ClientSequenceQueue
{
	private static final Logger log = LoggerFactory.getLogger(ClientSequenceQueue.class);
	
	private final RT2CliendUidType clientUid;
	
	private int waitForSeqNr = 1;

	private int currentOutSeqNr = 0;
	
	private int msgNackSinceInSeconds = 60;

	// This messages cannot be processed because a sequence number is missing. 
	private Queue<RT2Message> queuedMsgs  = new PriorityQueue<>();

	private SortedMap<Integer, LocalDateTime> nackedMsgs = new TreeMap<>();   
	
	private boolean aborted = false;

	//-------------------------------------------------------------------------	
	public ClientSequenceQueue(RT2CliendUidType clientUid) {
		this.clientUid = clientUid;
	}	
	
	//-------------------------------------------------------------------------	
	public ClientSequenceQueue(RT2CliendUidType clientUid, int msgNackSinceInSeconds) {
		this.clientUid = clientUid;
		this.msgNackSinceInSeconds = msgNackSinceInSeconds;
	}
	
	//-------------------------------------------------------------------------	
	public int getInWaitSeqNumber(){
		return waitForSeqNr;
	}

	//-------------------------------------------------------------------------
	public boolean wasMsgAlreadySeen(final RT2Message msg) throws Exception
	{
		final int msgSeqNr = RT2MessageGetSet.getSeqNumber(msg);

		// make most likely check first
		if (msgSeqNr == waitForSeqNr) {
			return false;
		}
		if (msgSeqNr < waitForSeqNr) {
			return true;
		}		
		return !queuedMsgs.stream().filter(m -> m.getSeqNumber().getValue().equals(msgSeqNr)).collect(Collectors.toSet()).isEmpty();
	}

	//-------------------------------------------------------------------------	
    public List<RT2Message> determinePendingMessagesWhichCanBeProcessed() {
    	List<RT2Message> res = new ArrayList<>();
		final Iterator<RT2Message> aQueueIter = queuedMsgs.iterator();
		while (aQueueIter.hasNext()) {
			final RT2Message aQueueMsg   = aQueueIter.next();
			final Integer    seqNrOfQueueMsg = aQueueMsg.getSeqNumber().getValue();
			if (seqNrOfQueueMsg.equals(waitForSeqNr)) {
				res.add(aQueueMsg);
				waitForSeqNr++;
				aQueueIter.remove();
			}
		}		 
		nackedMsgs.keySet().removeAll(res.stream().map(m -> m.getSeqNumber().getValue()).collect(Collectors.toSet()));
		log.trace("RT2: Set waiting for SEQ-NR to {} for client: {}", waitForSeqNr, clientUid);
		return res;
    }
		
    //-------------------------------------------------------------------------
    public Set<Integer> checkForAndGenerateNacks()	throws Exception {
    	
    	if (hasQueuedInMessages()) {
			int nSeqStart  = getInWaitSeqNumber();
			int nQueuedSeq = getMinQueuedSeqNr();
			int nNum       = nQueuedSeq - nSeqStart;
			if (nNum > 0) {
				Collection<Integer> msgsAckedInLastMinute = getMsgsNackedDuringLastMinute();
				// create nack request and send it to the client
				Set<Integer> nacks = new HashSet<>();
				
				for (int i = 0; i < nNum; i++) {
					int seqNr = nSeqStart + i;
					if (!msgsAckedInLastMinute.contains(seqNr)) {
						nacks.add(seqNr);
					}
				}				
				if (!nacks.isEmpty()) {
					LocalDateTime now = LocalDateTime.now();
					nacks.forEach(i -> nackedMsgs.put(i, now));
				}
				return nacks;
			}
    	}
		return new HashSet<>();
    }
    
    
	//-------------------------------------------------------------------------
	public void enqueueInMessage(final RT2Message aMsg) throws Exception
	{
		final Integer aSeq   = RT2MessageGetSet.getSeqNumber(aMsg);

		if ((aSeq == null) || (aSeq.intValue() == 0))
			log.error("RT2: Impossible case detected - message "+aMsg.getMessageID()+" with invalid SEQ-NR " + aSeq + "should be enqueued!");

		queuedMsgs.add(aMsg);
		log.trace("RT2: Message with SEQ-NR: {} for client {} queued.", aSeq, clientUid);
	}

	//-------------------------------------------------------------------------
	public boolean hasQueuedInMessages() {
		return !queuedMsgs.isEmpty();
	}

	//-------------------------------------------------------------------------	
	private int getMinQueuedSeqNr() {
		return queuedMsgs.peek().getSeqNumber().getValue();
	}
	
	//-------------------------------------------------------------------------
	private Collection<Integer> getMsgsNackedDuringLastMinute() {
		LocalDateTime nowMinusOneMinute = LocalDateTime.now().minusSeconds(msgNackSinceInSeconds);
		return getMsgsNackedSince(nowMinusOneMinute);
	}

	//-------------------------------------------------------------------------
	public Collection<Integer> getMsgsNackedSince(LocalDateTime compareTime) {
		return nackedMsgs.entrySet().stream()
				         .filter(e -> e.getValue().isAfter(compareTime))
				         .map(e -> e.getKey())
				         .collect(Collectors.toSet());
	}
	
	//-------------------------------------------------------------------------
	public void removedNacksOfReceivedMessages(List<RT2Message> canBeProcessed) {
		canBeProcessed.forEach(m -> nackedMsgs.remove(m.getSeqNumber().getValue()));
	}
	
	//-------------------------------------------------------------------------
	public int incAndGetOutSeqNumber() {
		return ++currentOutSeqNr;
	}

	//-------------------------------------------------------------------------
	public boolean isAborted() {
		return aborted;
	}

	//-------------------------------------------------------------------------
	public void setAborted(boolean aborted) {
		this.aborted = aborted;
	}
}
