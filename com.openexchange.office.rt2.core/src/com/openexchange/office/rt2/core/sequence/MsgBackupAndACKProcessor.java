/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.exception.RT2ProtocolException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;

public class MsgBackupAndACKProcessor implements IClientEventListener {

	//-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(MsgBackupAndACKProcessor.class);
	private static final String STR_ACK_RANGE_SEPARATOR = "-";
	private static final long MAX_TIME_IN_REMOVED_STATE_IN_MS = 4 * 60 * 1000;

	//-------------------------------------------------------------------------
	private Map<RT2CliendUidType, ClientState> clientStates = Collections.synchronizedMap(new HashMap<>());
	private Map<RT2CliendUidType, Long> removedClients = Collections.synchronizedMap(new HashMap<>());
	private AtomicBoolean shutdown = new AtomicBoolean(false);
	private final RT2DocUidType docUid;

	//-------------------------------------------------------------------------
	public MsgBackupAndACKProcessor(RT2DocUidType docUid) {
		this.docUid = docUid;
	}

	//-------------------------------------------------------------------------
	public RT2DocUidType getDocUID() {
		return docUid;
	}

	//-------------------------------------------------------------------------
	public void switchState(RT2CliendUidType clientUID, boolean online) {
		final ClientState aClientState = clientStates.get(clientUID);
		if (aClientState != null) {
			aClientState.setOnlineState(online);
		} else if (removedClients.containsKey(clientUID)) {
			log.debug("MsgBackupAndACKProcessor: Online state {} for com.openexchange.rt2.client.uid {} will be ignored - client was removed", online, clientUID);
		} else {
			log.error("MsgBackupAndACKProcessor: Online state {} cannot be set for unknown com.openexchange.rt2.client.uid {}", online, clientUID);
		}
	}

	//-------------------------------------------------------------------------
	public boolean isClientOnline(RT2CliendUidType sClientUID) {
		boolean result = false;
		final ClientState aClientState = clientStates.get(sClientUID);
		if (aClientState != null)
			result = aClientState.isOnline();
		return result;
	}

	//-------------------------------------------------------------------------	
	@Override
	public void clientAdded(ClientEventData clientEventData) {
		final RT2CliendUidType clientUid = clientEventData.getClientUid();
		clientStates.put(clientUid, new ClientState(clientUid, true));
		removedClients.remove(clientUid);
	}

	//-------------------------------------------------------------------------	
	@Override
	public void clientRemoved(ClientEventData clientEventData) {
		final RT2CliendUidType clientUid = clientEventData.getClientUid();
		clientStates.remove(clientUid);
		removedClients.put(clientUid, System.currentTimeMillis());
	}

	//-------------------------------------------------------------------------
	public void cleanupRemovedClients() {
		final long now = System.currentTimeMillis();
		removedClients.entrySet().removeIf(e -> {
			final boolean remove = (now > (e.getValue() + MAX_TIME_IN_REMOVED_STATE_IN_MS));
			if (remove) {
				log.info("MsgBackupAndACKProcessor removed marked com.openexchange.rt2.client.uid {} from removed clients map", e.getKey());
			}
			return remove;
		});
	}

	//-------------------------------------------------------------------------
	public void updateClientState(RT2Message msg) {
		ClientState clientState = clientStates.get(msg.getClientUID());
		if (clientState != null) {
			switch (msg.getType()) {
				case ACK_SIMPLE: clientState.updateLastAckMsgReceived();
				case ACK_ERROR:
				case NACK_SIMPLE:
				case REQUEST_ABORT_OPEN:
				case REQUEST_APP_ACTION:
				case REQUEST_APPLY_OPS:
				case REQUEST_CLOSE_DOC:
				case REQUEST_EDITRIGHTS:
				case REQUEST_EMERGENCY_LEAVE:
				case REQUEST_JOIN:
				case REQUEST_LEAVE:
				case REQUEST_OPEN_DOC:
				case REQUEST_RESET:
				case REQUEST_SAVE_DOC:
				case REQUEST_SYNC:
				case REQUEST_SYNC_STABLE:
				case REQUEST_UPDATE_SLIDE: clientState.updateLastMsgReceived();
										   break;
				default: log.debug("Message of type {} received which is an internal message", msg.getType());
			}
		}
	}
	
	//-------------------------------------------------------------------------
	public void backupMessage(final RT2Message aOutMsg) {

		// don't backup messages without seq number
		if (!RT2MessageGetSet.hasSeqNumber(aOutMsg)) {
			return;
		}

		final Integer nOutSeq = RT2MessageGetSet.getSeqNumber(aOutMsg);
		log.trace("RT2: Queued message for com.openexchange.rt2.client.uid {} waiting for ACK: {}", aOutMsg.getClientUID(), nOutSeq);

		final ClientState aClientState = clientStates.get(aOutMsg.getClientUID());
		if (aClientState != null)
			aClientState.backupMessage(aOutMsg);
	}

    //-------------------------------------------------------------------------
    public void removeBackupMessagesForACKs(final RT2Message aAck) throws Exception {
        final ClientState aClientState = clientStates.get(aAck.getClientUID());

        if (aClientState != null) {
        	try {
                final Object ackBody = aAck.getBody().get(RT2Protocol.BODYPART_ACKS);
                // We now support two different syntaxes for ACKs
                if (ackBody instanceof JSONArray) {
                    // 1.) a json arrray with seq-nrs to be ACK: [1, 2, 3, 4, 5, ...]
                    final JSONArray jAckList = (JSONArray)ackBody;
                    log.trace("RT2: Received ACKs : {}" + jAckList.toString() + " for com.openexchange.rt2.client.uid " + aAck.getClientUID());
                    aClientState.removeStoredMessages(jAckList.asList());
                } else if (ackBody instanceof String) {
                    // 2.) a simple string with a range to be ACK: "1-1000", where both
                    //     values (start & end) are inclusive, means message with this seq-nr
                    //     must be removed. If end < start the range will be ignored, start == end
                    //     means that just one seq-nr is removed
                    try {
                        final String[] parts = ((String)ackBody).split(STR_ACK_RANGE_SEPARATOR);
                        if (parts.length == 2) {
                            int start = Integer.parseInt(parts[0]);
                            int end = Integer.parseInt(parts[1]);
                            aClientState.removeStoredMessages(start, end);
                        } else {
                            throw new RT2ProtocolException("Invalid syntax for ACK message detected: " + ackBody);
                        }
                    } catch (NumberFormatException e) {
                        throw new RT2ProtocolException("Invalid syntax for ACK message detected: " + ackBody, e);
                    }
                } else {
                    throw new RT2ProtocolException("Invalid body type for ACK message detected: " + aAck.getBodyString());
                }
            } catch (JSONException e) {
                throw new RT2ProtocolException("Invalid body format for ACK message detected: " + aAck.getBodyString(), e);
            }
        } else {
            log.debug("RT2: Unknown com.openexchange.rt2.client.uid "+aAck.getClientUID()+" used by ACK message: " + aAck.toString());
        }
    }

	//-------------------------------------------------------------------------
	public List<RT2Message> getMessagesForResent(final RT2CliendUidType sClientUID, final List<Object> aNackList) {
		List<RT2Message> aResentList;

		log.trace("RT2: Looking for messages request by com.openexchange.rt2.client.uid {} with NACK with seq-nrs: {}", sClientUID, aNackList.toString());

		final ClientState aClientState = clientStates.get(sClientUID);
		if (aClientState != null) {
			aResentList = aClientState.getRequestedMessages(aNackList);
		} else {
			log.debug("RT2: Unknown com.openexchange.rt2.client.uid "+sClientUID+" used by NACK message.");
			aResentList = new ArrayList<RT2Message>();
		}
		return aResentList;
	}

 	//-------------------------------------------------------------------------
	public void addReceivedSeqNr(final RT2CliendUidType clientUID, int start, int end) {
		log.trace("RT2: Add received seq-nrs {} - {} for com.openexchange.rt2.client.uid: {}", start, end, clientUID);

		final ClientState aClientState = clientStates.get(clientUID);
		if (aClientState != null) {
			aClientState.addReceivedSeqNrCol(IntStream.range(start, end+1).boxed().collect(Collectors.toSet()));
		}
		else
			log.debug("RT2: Unknown com.openexchange.rt2.client.uid "+clientUID+" used addReceivedSeqNr().");
	}

	//-------------------------------------------------------------------------
	public Map<RT2CliendUidType, Integer> getClientStatesMsgBackupSize() {
		final Map<RT2CliendUidType, Integer> res = new HashMap<>();
		synchronized (clientStates) {
			clientStates.forEach((k,v) -> res.put(k, v.getMsgBackupSize()));
		}
		return res;
	}

	//-------------------------------------------------------------------------
	public int getMsgBackupSize(RT2CliendUidType clientUid) {
		Optional<Integer> optSize = null;
		synchronized (clientStates) {
			final ClientState clientState = clientStates.get(clientUid);
			if (clientState != null) {
				optSize = Optional.of(clientState.getMsgBackupSize());
			}
		}

		return Optional.ofNullable(optSize).isPresent() ? optSize.get() : 0;
	}

	//-------------------------------------------------------------------------
	public void collectionClientAckListInfo(Map<RT2CliendUidType, Set<Integer>> clientsAckList, Map<RT2CliendUidType, ClientAckState> clientsAckStateMap) {
		if (!this.shutdown.get()) {
			synchronized (clientStates) {
				for (final RT2CliendUidType clientUID : clientStates.keySet()) {
					final ClientState clientState = clientStates.get(clientUID);
					final Set<Integer> ackSet = clientState.getAckSet();

					if (!ackSet.isEmpty()) {
						clientsAckList.put(clientUID, ackSet);
					}

					final boolean isOnline = clientState.isOnline();
					final long timeStampOnOffline = (isOnline) ? clientState.onlineSince(): clientState.offlineSince();
					final ClientAckState clientAckState = 
							new ClientAckState(clientUID, isOnline, timeStampOnOffline, clientState.getOldestBackupMessageTimeStamp(), clientState.getBackupMsgs());
					clientsAckStateMap.put(clientUID, clientAckState);
				}
			}
		}
	}

	//-------------------------------------------------------------------------
	public Map<RT2CliendUidType, ClientAckState> getClientsAckInfo() {
		final Map<RT2CliendUidType, ClientAckState> result = new HashMap<>();
		if (!this.shutdown.get()) {
			synchronized (clientStates) {
				for (final RT2CliendUidType clientUID : clientStates.keySet()) {
					final ClientState  clientState = clientStates.get(clientUID);
					final boolean isOnline = clientState.isOnline();
					final long timeStampOnOffline = (isOnline) ? clientState.onlineSince(): clientState.offlineSince();
					final ClientAckState clientAckState = 
							new ClientAckState(clientUID, isOnline, timeStampOnOffline, clientState.getOldestBackupMessageTimeStamp(), clientState.getBackupMsgs());
					result.put(clientUID, clientAckState);
				}
			}
		}

		return result;
	}
}
