/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.exception.RT2InvalidDocumentIdentifierException;
import com.openexchange.office.rt2.core.exception.RT2ProtocolException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageFormatter;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.common.TimeStampUtils;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@Async(initialDelay=MsgBackupAndAckProcessorService.TIME_FLUSH_ACK, configNameInitialDelay=MsgBackupAndAckProcessorService.KEY_ACK_FLUSH,
       period=MsgBackupAndAckProcessorService.TIME_FLUSH_ACK, configNameInitialPeriod=MsgBackupAndAckProcessorService.KEY_ACK_FLUSH, 
       timeUnit=TimeUnit.MILLISECONDS)
public class MsgBackupAndAckProcessorService implements IClientEventListener, IDocProcessorEventListener, Runnable, InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(MsgBackupAndAckProcessorService.class);

	public static final String KEY_TIMEOUT_FOR_BAD_CLIENT = "rt2.client.timeout.bad.client";
	public static final String KEY_MAX_MESSAGES_BEFORE_HANGUP = "rt2.client.max.messages.before.hangup";
	public static final String KEY_ACK_FLUSH = "rt2.ack.flush";

	// ATTENTION: this value must be synchronized with the client implementation
	public static final int CLIENT_TIME_FLUSH_ACK = 15000;

	public static final int TIME_FLUSH_ACK = 30000;
	public static final int TIMEOUT_FOR_RECONNECTED_CLIENT = 60000 * 2;
	public static final int TIMEOUT_FOR_BAD_CLIENT = 8 * CLIENT_TIME_FLUSH_ACK;
	public static final int THRESHOLD_MAX_MSGS_WO_ACK =  3 * (TIME_FLUSH_ACK / 1000);
	private static final int OT_MSG_SCALING_FACTOR = 8;
	private static final int MAX_MSGS_FOR_DUMP_ON_BAD_ACK = 20;
	private static final int NON_SYNCABLE_MSG_FACTOR = 5;
	private static final long TIMEOUT_FOR_HANGUP_CLIENT = 5 * 60000;
	private static final int MAX_HANGUPS_FOR_BAD_CLIENT_FOR_FORCEFULL_REMOVAL = 2;

	private static class HangupClientEntry {
		public RT2CliendUidType clientUid;
		public ErrorCode errorCode;
		public int hangups = 0;
		public long timeStamp;

		public HangupClientEntry(RT2CliendUidType clientUid, ErrorCode errorCode, int hangups, long timeStamp) {
			this.clientUid = clientUid;
			this.errorCode = errorCode;
			this.hangups = hangups;
			this.timeStamp = timeStamp;
		}

		public boolean isObsolete(long now) {
			return (TimeStampUtils.timeDiff(timeStamp, now) > TIMEOUT_FOR_HANGUP_CLIENT);
		}

		public String toString() {
			long now = System.currentTimeMillis();
			return "clientUid: " + clientUid + ", error: " + errorCode.getCodeAsStringConstant() + ", hangups: " + hangups + ", time span: " + DurationFormatUtils.formatDuration(TimeStampUtils.timeDiff(timeStamp, now), "HH:mm:ss") + "}";
		}
	};

	private final ConcurrentHashMap<RT2DocUidType, MsgBackupAndACKProcessor> msgBackAndAckProcs = new ConcurrentHashMap<>();
	private final Map<RT2CliendUidType, HangupClientEntry> clientsWithHangups = new ConcurrentHashMap<>();

	@Value("${" + KEY_TIMEOUT_FOR_BAD_CLIENT + ":" + TIMEOUT_FOR_BAD_CLIENT + "}")
	private int timeoutValueBadClient;

	@Value("${" + KEY_MAX_MESSAGES_BEFORE_HANGUP + ":" + THRESHOLD_MAX_MSGS_WO_ACK + "}")
	private int thresholdValueMaxMsgs;

	@Autowired
	private RT2MessageSender messageSender;

	@Autowired
	private ClientEventService clientEventService;

	@Autowired
	private ClusterService clusterService;

	@Autowired
	private RT2DocProcessorManager rtDocProcessorManagerService;

	private int thresholdMaxMsgsForNonSync;

	//-------------------------------------------------------------------------
	public MsgBackupAndAckProcessorService() {}

	//-------------------------------------------------------------------------
	public MsgBackupAndAckProcessorService(int timeoutValueBadClient, int thresholdValueMaxMsgs) {
		this.timeoutValueBadClient = timeoutValueBadClient;
		this.thresholdValueMaxMsgs = thresholdValueMaxMsgs;
		this.thresholdMaxMsgsForNonSync = thresholdValueMaxMsgs * NON_SYNCABLE_MSG_FACTOR;
	}

	//-------------------------------------------------------------------------
	@Override
	public void afterPropertiesSet() throws Exception {
		this.thresholdMaxMsgsForNonSync = thresholdValueMaxMsgs * NON_SYNCABLE_MSG_FACTOR;
	}

	//-------------------------------------------------------------------------
    @Override
	public void docCreated(RT2DocUidType docUid) {
    	msgBackAndAckProcs.put(docUid, new MsgBackupAndACKProcessor(docUid));
	}

	//-------------------------------------------------------------------------    
	@Override
	public void docDisposed(RT2DocUidType docUid) {
		msgBackAndAckProcs.remove(docUid);
	}

	//-------------------------------------------------------------------------	
	@Override
	public void clientAdded(ClientEventData clientEventData) {
		final MsgBackupAndACKProcessor msgBackAndAckProc = msgBackAndAckProcs.get(clientEventData.getDocUid());
		if (msgBackAndAckProc != null) {
			msgBackAndAckProc.clientAdded(clientEventData);
		} else {
			log.warn("MsgBackupAndAckProcessorService: clientAdded com.openexchange.rt2.client.uid {} called with unknown com.openexchange.rt2.document.uid {}",
				clientEventData.getClientUid(), clientEventData.getDocUid());
		}
	}

	//-------------------------------------------------------------------------
	@Override
	public void clientRemoved(ClientEventData clientEventData) {
		final MsgBackupAndACKProcessor msgBackAndAckProc = msgBackAndAckProcs.get(clientEventData.getDocUid());
		if (msgBackAndAckProc != null) {
			msgBackAndAckProc.clientRemoved(clientEventData);
		}
	}

    //-------------------------------------------------------------------------	
	public Map<String, String> getStatusMsgsWaitingForAckOfDoc() {
		final Map<String, String> res = new HashMap<>();
		final long now = System.currentTimeMillis();

		msgBackAndAckProcs.forEach((docUid, msgBackupAndACKProc) -> {
			msgBackupAndACKProc.getClientsAckInfo().forEach((k,v) -> {
				final StringBuilder tmp = new StringBuilder(200);
				final long timeSpanOldestAckMsg = timeSpanForOldestNonAckMsg(now, v.getTimeStampOldestMsgForAck());
				final long timeSpanOnOffline = now - v.getTimeStampStartedOnOffline();
				tmp.append("count: ").append(v.getMsgsStoredForResend().size());
				tmp.append(", oldest msgs: ").append(timeSpanOldestAckMsg);
				tmp.append(", online: ").append(msgBackupAndACKProc.isClientOnline(k));
				tmp.append(", on-/offline duration: ").append(DurationFormatUtils.formatDuration(timeSpanOnOffline, "ddD HH:mm:ss"));
				res.put(docUid.getValue() + "_" + k.toString(), tmp.toString());
			});
		});

		return res;
	}

	//-------------------------------------------------------------------------
	public Set<String> getHangupClients() {
		Set<String> res = new HashSet<>();
		clientsWithHangups.forEach((k, v) -> {
			if (v != null) {
				res.add(v.toString());
			}
		});
		return res;
	}

	//-------------------------------------------------------------------------	
	public Optional<MsgBackupAndACKProcessor> getMsgBackupAndACKProcessor(final RT2DocUidType docUid, boolean throwIfNotFound) throws RT2InvalidDocumentIdentifierException {
		MsgBackupAndACKProcessor res = msgBackAndAckProcs.get(docUid);
		if (throwIfNotFound && (res == null)) {
			throw new RT2InvalidDocumentIdentifierException(docUid, new ArrayList<>());
		}
		return Optional.ofNullable(res);
	}

    //-------------------------------------------------------------------------
    public void updateClientState(RT2Message msg) throws RT2InvalidDocumentIdentifierException {
    	getMsgBackupAndACKProcessor(msg.getDocUID(), true).get().updateClientState(msg);
    }

    //-------------------------------------------------------------------------
    public void addReceivedSeqNr(final RT2DocUidType docUid, final RT2CliendUidType clientUid, int start, int end) throws RT2InvalidDocumentIdentifierException {
    	getMsgBackupAndACKProcessor(docUid, true).get().addReceivedSeqNr(clientUid, start, end);
    }

    //-------------------------------------------------------------------------
    public void removeBackupMessagesForACKs(RT2Message msg) throws Exception {
    	Optional<MsgBackupAndACKProcessor> msgBackupAndACKProc = getMsgBackupAndACKProcessor(msg.getDocUID(), false); 
    	if (msgBackupAndACKProc.isPresent()) {
    		msgBackupAndACKProc.get().removeBackupMessagesForACKs(msg);
    	}
    }

    //-------------------------------------------------------------------------
    public void resendMessages(RT2Message msg) throws RT2InvalidDocumentIdentifierException, RT2ProtocolException {     
    	Optional<MsgBackupAndACKProcessor> msgBackupAndACKProc = getMsgBackupAndACKProcessor(msg.getDocUID(), false);
    	if (msgBackupAndACKProc.isPresent()) {
    		JSONArray aNackJSONArray = null;    		
    		try {
    			aNackJSONArray = msg.getBody().getJSONArray(RT2Protocol.BODYPART_NACKS);
			} catch (JSONException e) {
				throw new RT2ProtocolException(e.getMessage(), e);
			}
    		List<RT2Message> resendMessages = msgBackupAndACKProc.get().getMessagesForResent(msg.getClientUID(), aNackJSONArray.asList());
    		for (final RT2Message aResendMsg : resendMessages) {
    			messageSender.sendMessageWOSeqNrTo(aResendMsg.getClientUID(), aResendMsg);
    		}
    	}    	
    }

	//-------------------------------------------------------------------------
	public void switchState(RT2Message msg) throws RT2InvalidDocumentIdentifierException {
		getMsgBackupAndACKProcessor(msg.getDocUID(), true).get().switchState(msg.getClientUID(), msg.getUnvailableTime().getValue() == 0l);
	}

	//-------------------------------------------------------------------------
	public void backupMessage(RT2Message msg) throws RT2InvalidDocumentIdentifierException {
		getMsgBackupAndACKProcessor(msg.getDocUID(), true).get().backupMessage(msg);
	}

	//-------------------------------------------------------------------------
	@Override
	public void run() {
		try {
	        MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "MsgBackupAndACKProcessor");
	        MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
	        msgBackAndAckProcs.forEach((k, v) -> {
	        	MDCHelper.safeMDCPut(MDCEntries.DOC_UID, k.getValue());
				final Map<RT2CliendUidType, Set<Integer>> clientsAckList = new HashMap<>();
				final Map<RT2CliendUidType, ClientAckState> clientsAckStateMap = new HashMap<>();
				v.collectionClientAckListInfo(clientsAckList, clientsAckStateMap);
				v.cleanupRemovedClients();
				sendAcksToClients(clientsAckList, k);
				checkClientBehaviourForHangup(clientsAckStateMap, k, v);
				cleanupObsoleteClientHangupEntries();
	        });
		} catch (Throwable t) {
			ExceptionUtils.handleThrowable(t);
			log.error("MsgBackupAndAckProcessorService: Throwable received while processing pending ACKs!", t);
		} finally {
			MDC.clear();
		}
	}

	//-------------------------------------------------------------------------	
	private void checkClientBehaviourForHangup(final Map<RT2CliendUidType, ClientAckState> clientsAckStateMap, RT2DocUidType docUid, MsgBackupAndACKProcessor msgBackupAndACKProcessor) {
		// Determine max count msgs waiting for an ACK dependent on the edit mode (real-time editing or baton for edit rights)
		final int maxMsgsForNonSync = thresholdMaxMsgsForNonSync * (isOTEnabled(docUid) ? OT_MSG_SCALING_FACTOR : 1);

		for (final RT2CliendUidType clientUID : clientsAckStateMap.keySet()) {
			final ClientAckState clientAckState = clientsAckStateMap.get(clientUID);
			final int numOfPendingAckMsgs = clientAckState.getMsgsStoredForResend().size();

			if (isBadClientACKBehaviour(clientAckState, msgBackupAndACKProcessor)) {
				// protection: check for clients which don't send acks although they send msgs -
				// these client must be hang-up to protect us from oom situations (we have to
				// store msgs for resend until we receive an ack) 
				log.warn("MsgBackupAndAckProcessorService: Detected a com.openexchange.rt2.client.uid {} that sends messages without correct ack handling - hang-up this client", clientUID);
				hangUpClient(docUid, clientUID, ErrorCode.GENERAL_MALICIOUS_ACK_HANDLING_ERROR);
			} else if (!clientAckState.isOnline() && (numOfPendingAckMsgs >= maxMsgsForNonSync)) {
				// Protection: check for offline clients that have a huge amount of pending
				// messages. These client won't ever be able to synchronize all messages in the
				// available sync time. Reloading the document is much faster and less error
				// prone. Therefore we hang-up these clients, too.
				log.info("MsgBackupAndAckProcessorService: Detected a com.openexchange.rt2.client.uid {} that is offline and has collected an unsyncable amount of pending messages, count={}", clientUID, numOfPendingAckMsgs);
				hangUpClient(docUid, clientUID, ErrorCode.GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE);
			}
		}
	}

    //-------------------------------------------------------------------------
	private void hangUpClient(RT2DocUidType docUid, RT2CliendUidType clientUid, ErrorCode errorCode) {
		final RT2Message hangupMsg = RT2MessageFactory.newBroadcastMessage(clientUid, docUid, RT2MessageType.BROADCAST_HANGUP);
		hangupMsg.setError(new RT2ErrorCodeType(errorCode));
		messageSender.sendMessageWOSeqNrTo(clientUid, hangupMsg);

		if (clientsWithHangups.putIfAbsent(clientUid, new HangupClientEntry(clientUid, errorCode, 1, System.currentTimeMillis())) != null) {
			clientsWithHangups.computeIfPresent(clientUid, (k, v) -> { return new HangupClientEntry(clientUid, errorCode, v.hangups + 1, v.timeStamp); });
		}
		HangupClientEntry hangupDataOfClient = clientsWithHangups.get(clientUid);

		// We force the removal of the client if it doesn't do it by itself - latest
		// with the 2nd encounter of a hangup. This first one should not force the
		// removal as we want to give the client a chance to cleanly close the
		// connection.
		if (hangupDataOfClient.hangups >= MAX_HANGUPS_FOR_BAD_CLIENT_FOR_FORCEFULL_REMOVAL) {
			clientEventService.notifyClientRemoved(new ClientEventData(docUid, clientUid));
			log.info("MsgBackupAndAckProcessorService: force com.openexchange.rt2.client.uid {} to be removed from com.openexchange.rt2.document.uid {} due to hangup", clientUid, docUid);
		}
	}

	//-------------------------------------------------------------------------
	private void cleanupObsoleteClientHangupEntries() {
		final long now = System.currentTimeMillis();
		final List<RT2CliendUidType> obsoleteEntries =
				clientsWithHangups.keySet().stream()
				                           .filter(k -> {
				                        	   HangupClientEntry e = clientsWithHangups.get(k);
				                        	   return ((e != null) && (e.isObsolete(now))); })
				                           .collect(Collectors.toList());
        if (obsoleteEntries.size() > 0) {
        	obsoleteEntries.stream().forEach(k -> clientsWithHangups.remove(k));
        }
	}

	//-------------------------------------------------------------------------	
	private void sendAcksToClients(Map<RT2CliendUidType, Set<Integer>> clientsAckList, RT2DocUidType docUid) {
		// check if we need to send the next lazy ACKs for a client
		// send the lazy ack lists to the clients
		for (final RT2CliendUidType sClientUID : clientsAckList.keySet()) {
			final Set<Integer> aClientAckList = clientsAckList.get(sClientUID);
			try {
				final RT2Message aAckResponse = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, sClientUID, docUid);
				aAckResponse.getBody().put(RT2Protocol.BODYPART_ACKS, new JSONArray(aClientAckList));
				messageSender.sendMessageWOSeqNrTo(sClientUID, aAckResponse);
			} catch (JSONException t) {				
				ExceptionUtils.handleThrowable(t);
				log.error("MsgBackupAndAckProcessorService: JSONException received while sending ACKs - client state unknown!", t);
				messageSender.sendErrorResponseToClient(sClientUID, null, null, t);
			}
		}
	}

	//-------------------------------------------------------------------------
	private boolean isBadClientACKBehaviour(ClientAckState clientAckState, MsgBackupAndACKProcessor msgBackupAndACKProcessor) {
		// bad client behavior must only be checked for online clients - offline clients and their 
		// referenced resources will be removed by the rt2 garbage collector
		if (msgBackupAndACKProcessor.isClientOnline(clientAckState.getClientUid())) {
			final long now = System.currentTimeMillis();
			final long onlineSince = clientAckState.isOnline() ? clientAckState.getTimeStampStartedOnOffline() : TimeStampUtils.NO_TIMESTAMP;
			final long timeStampOldestMsgForAck = clientAckState.getTimeStampOldestMsgForAck();
			final int numOfMsgsForResend = clientAckState.getMsgsStoredForResend().size();

			int currentThresholdMaxMsgs = thresholdValueMaxMsgs;
			if (isOTEnabled(msgBackupAndACKProcessor.getDocUID())) {
				// in case of OT we need to use a much higher number of messages
				// as the client does not collect message before sending
				// them, but send them as soon as possible
				currentThresholdMaxMsgs *= OT_MSG_SCALING_FACTOR;
			}

			final long onlineTime = TimeStampUtils.timeDiff(onlineSince, now);
			final long waitForAck = TimeStampUtils.timeDiff(timeStampOldestMsgForAck, now);

			final boolean timeoutBadClient = ((onlineTime > timeoutValueBadClient) && (waitForAck > timeoutValueBadClient));
			final boolean thresholdMaxMsgs = ((onlineTime > timeoutValueBadClient) && (numOfMsgsForResend > currentThresholdMaxMsgs));
			final boolean badClientResult = (timeoutBadClient) || (thresholdMaxMsgs);

			log.debug("State for com.openexchange.rt2.client.uid {}: timeoutBadClient: {}, thresholdMaxMsgs: {}, time online: {}, oldest msg waiting for ack: {}, timeoutValueBadClient: {}",
					clientAckState.getClientUid().getValue(), timeoutBadClient, thresholdMaxMsgs, onlineTime, waitForAck, timeoutValueBadClient);

			if (badClientResult) {
				
				dumpBadClientAckInformation(clientAckState, msgBackupAndACKProcessor);
			}

			return badClientResult;
		}

		return false;
	}

	//-------------------------------------------------------------------------
	private void dumpBadClientAckInformation(ClientAckState clientAckState, MsgBackupAndACKProcessor msgBackupAndACKProcessor) {
		final RT2CliendUidType clientUid = clientAckState.getClientUid();
		final long now = System.currentTimeMillis();
		final long onOfflineSince = clientAckState.getTimeStampStartedOnOffline();
		final long timeStampOldestMsgForAck = clientAckState.getTimeStampOldestMsgForAck();
		final int numOfMsgsForResend = clientAckState.getMsgsStoredForResend().size();
		final RT2DocUidType docUid = msgBackupAndACKProcessor.getDocUID();
		final RT2DocProcessor docProc = rtDocProcessorManagerService.getDocProcessor(docUid);

		final long onOfflineTimeSpan = TimeStampUtils.timeDiff(onOfflineSince, now);
		final long waitForAck = TimeStampUtils.timeDiff(timeStampOldestMsgForAck, now);

		MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
		MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUid.getValue());
		try {
	        log.warn("Bad ack behaviour for com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {} found! Online: {}, On-/Offline time: {}, time span of oldest msg waiting for ack {}, number of messages to resend: {}",
                clientUid, msgBackupAndACKProcessor.getDocUID(), clientAckState.isOnline(), onOfflineTimeSpan, waitForAck, numOfMsgsForResend);
            log.warn("Messages without ACK sent by client {}", RT2MessageFormatter.formatForOutput(clientAckState.getMsgsStoredForResend(), MAX_MSGS_FOR_DUMP_ON_BAD_ACK,
                    RT2Protocol.HEADER_MSG_ID,RT2MessageFormatter.MSG_TYPE,RT2Protocol.HEADER_SEQ_NR));
            log.warn("Latest messages sent by bad client: {}", (null != docProc) ? docProc.formatMsgsLogInfoForClient(clientUid) : "unknown - no doc processor reference found!");
		} finally {
	        MDC.remove(MDCEntries.DOC_UID);
	        MDC.remove(MDCEntries.CLIENT_UID);
		}
	}

	//-------------------------------------------------------------------------
	private boolean isOTEnabled(RT2DocUidType docUid) {
		final RT2DocProcessor docProc = rtDocProcessorManagerService.getDocProcessor(docUid);
		return ((docProc != null) && (docProc.isOTEnabled()));
	}

	//-------------------------------------------------------------------------
	private static long timeSpanForOldestNonAckMsg(long now, long oldestNonAckMsgTimeStamp) {
		return (oldestNonAckMsgTimeStamp == TimeStampUtils.NO_TIMESTAMP) ? 0 : (now - oldestNonAckMsgTimeStamp);
	}

}
