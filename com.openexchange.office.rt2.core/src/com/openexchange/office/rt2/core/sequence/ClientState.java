/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.tools.common.TimeStampUtils;

public class ClientState {
	private static final Logger log = LoggerFactory.getLogger(ClientState.class);

	private static class BackupMessageData {
		private RT2Message backupMessage;
		private long timeStamp;

		BackupMessageData(RT2Message msg) {
			backupMessage = msg;
			timeStamp = System.currentTimeMillis();
		}

		private static int oldest(BackupMessageData b1, BackupMessageData b2) {
			return (b1.timeStamp < b2.timeStamp) ?  -1 : ((b1.timeStamp == b2.timeStamp) ? 0 : 1);
		}
	}

	private RT2CliendUidType clientUID;
	private AtomicBoolean online = new AtomicBoolean(true);
	private Map<Integer, BackupMessageData> msgBackup = Collections.synchronizedMap(new HashMap<>());
	private Set<Integer> ackSet = Collections.synchronizedSet(new HashSet<>());
	private AtomicLong lastMsgReceived = new AtomicLong(0);
	private AtomicLong lastAckMsgReceived = new AtomicLong(0);
	private AtomicLong onOfflineTimeStamp = new AtomicLong(0);
	
	//-------------------------------------------------------------------------
	public ClientState(final RT2CliendUidType clientUID, boolean online) {
		this.clientUID = clientUID;
		this.online.set(online);
		this.lastMsgReceived.set(TimeStampUtils.NO_TIMESTAMP);
		this.onOfflineTimeStamp.set(System.currentTimeMillis());
	}

	//-------------------------------------------------------------------------
	public boolean isOnline() {
		return online.get();
	}

	//-------------------------------------------------------------------------
	public void setOnlineState(boolean bNewState) {
		if (online.compareAndSet(!bNewState, bNewState)) {
			onOfflineTimeStamp.set(System.currentTimeMillis());
		}
	}

	//-------------------------------------------------------------------------
	public void addReceivedSeqNrCol(Collection<Integer> seqNrCol) {
		ackSet.addAll(seqNrCol);
	}

	//-------------------------------------------------------------------------	
	public Set<RT2Message> getBackupMsgs() {
		synchronized(msgBackup) {
			return msgBackup.values().stream()
	                 .map(b -> b.backupMessage)
	                 .collect(Collectors.toSet());
		}
	}

	//-------------------------------------------------------------------------
	public int getMsgBackupSize() {
		return msgBackup.size();
	}
	
	//-------------------------------------------------------------------------
	public Set<Integer> getAckSet() {
		synchronized (ackSet) {
			final Set<Integer> aNextAckSet = ackSet;
			ackSet = Collections.synchronizedSet(new HashSet<>());
			return aNextAckSet;
		}
	}

	//-------------------------------------------------------------------------
	public long onlineSince() {
		return online.get() ? onOfflineTimeStamp.get() : TimeStampUtils.NO_TIMESTAMP;
	}

	//-------------------------------------------------------------------------
	public long offlineSince() {
		return (!online.get()) ? onOfflineTimeStamp.get() : TimeStampUtils.NO_TIMESTAMP;
	}

	//-------------------------------------------------------------------------
	public long getLastMsgReceived() {
		return lastMsgReceived.get();
	}
	
	//-------------------------------------------------------------------------
	public void updateLastMsgReceived() {
		this.lastMsgReceived.set(System.currentTimeMillis());
	}

	//-------------------------------------------------------------------------
	public long getLastAckMsgReceived() {
		return lastAckMsgReceived.get();
	}

	//-------------------------------------------------------------------------
	public void updateLastAckMsgReceived() {
		lastAckMsgReceived.set(System.currentTimeMillis());
	}

	//-------------------------------------------------------------------------
	public long getOldestBackupMessageTimeStamp() {
		synchronized (msgBackup) {
			final Optional<BackupMessageData> oldestBackupMsg = 
					msgBackup.values().stream().min(BackupMessageData::oldest);
			return oldestBackupMsg.isPresent() ? oldestBackupMsg.get().timeStamp : TimeStampUtils.NO_TIMESTAMP;
		}
	}

	//-------------------------------------------------------------------------
	public void backupMessage(final RT2Message rMsg) {
	    final Integer nSeqNr = RT2MessageGetSet.getSeqNumber(rMsg);
	    
		final BackupMessageData rOld = msgBackup.put(nSeqNr, new BackupMessageData(rMsg));

		if (rOld != null)
			log.error("RT2: Message with seq-nr " + nSeqNr + " already stored!");
	}

	//-------------------------------------------------------------------------
	public void removeStoredMessages(List<Object> ackList) {
		synchronized (msgBackup) {
			msgBackup.keySet().removeAll(ackList);
		}
	}

	//-------------------------------------------------------------------------
	public void removeStoredMessages(int start, int end) {
		synchronized (msgBackup) {
			msgBackup.keySet().removeIf(k -> (k >= start && k <= end));
		}
	}

	//-------------------------------------------------------------------------
	public List<RT2Message> getRequestedMessages(List<Object> aNackList) {
		List<RT2Message> lResult = new ArrayList<>();
		for (Object i : aNackList) {
			final BackupMessageData msgData = msgBackup.get(i);
			if (msgData != null)
				lResult.add(msgData.backupMessage);
			else {
				lResult.clear();
				log.error("RT2: Couldn't find requested message with seq-nr {} for client UID {}. Existing entries: {}", i, clientUID, msgBackup.keySet());
				break;
			}
		}

		return lResult;
	}
}
