/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.RT2MessageTypeDispatcher;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class QueueProcessorDisposer implements DisposableBean, InitializingBean{

	private static final Logger log = LoggerFactory.getLogger(QueueProcessorDisposer.class);

	//-------------------------------Services------------------------------------------
	@Autowired
	private RT2MessageTypeDispatcher messageTypeDispatcher;

	@Autowired
	private RT2MessageSender msgSender;

	@Autowired
	private ClusterService clusterService;

	//---------------------------------------------------------------------------------
	private final ConcurrentLinkedDeque<Pair<List<RT2Message>, SequenceDocProcessor>> messageQueue = new ConcurrentLinkedDeque<>();

	private final ConcurrentSkipListSet<SequenceDocProcessor> currentUsedDocProcessors = new ConcurrentSkipListSet<>((l, r) -> l.getDocUID().getValue().compareTo(r.getDocUID().getValue()));

	private Thread messageQueuesDispatcher;

	private ExecutorService executorService;

	private Set<MessageQueuesDispatcherInfo> messageQueuesDispatcherInfos = new HashSet<>();

	private AtomicReference<RT2Message> lastProcessedMessage = new AtomicReference<>();

	private AtomicBoolean stopped = new AtomicBoolean(false);

	public QueueProcessorDisposer() {
		this.executorService = Executors.newFixedThreadPool(10, new ThreadFactoryBuilder("MessageProcessor-%d").build());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		messageQueuesDispatcher = new Thread(new MessageQueuesDispatcher(), "MessageQueuesDispatcher");
		messageQueuesDispatcher.setDaemon(true);
		messageQueuesDispatcher.start();
	}

	@Override
	public void destroy() throws Exception {
		stopped.set(true);
		executorService.shutdown();
	}

	public void addMessages(SequenceDocProcessor docProc, List<RT2Message> msgs) {
		messageQueue.offer(Pair.of(msgs, docProc));
	}

	public int getMessageQueueSize() {
		return messageQueue.size();
	}

	public int getCurrentUsedDocProcessors() {
		return currentUsedDocProcessors.size();
	}

	public Set<MessageQueuesDispatcherInfo> getMessageQueuesDispatcherInfos() {
		return new HashSet<>(messageQueuesDispatcherInfos);
	}

	public RT2Message getLastProcessedMessage() {
		return lastProcessedMessage.get();
	}

	private class MessageQueuesDispatcher implements Runnable {

		@Override
		public void run() {
			while (!stopped.get()) {
				try {
					MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "MessageQueuesDispatcher");
		            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
					Deque<Pair<List<RT2Message>, SequenceDocProcessor>> unableToProcess = new LinkedBlockingDeque<>();
					Pair<List<RT2Message>, SequenceDocProcessor> next = determineNextMessagesToProcess(unableToProcess);
					if (!unableToProcess.isEmpty()) {
						unableToProcess.forEach(e -> messageQueue.addFirst(e));
					}
					if (next == null) {
						Thread.sleep(100);
						continue;
					}
					currentUsedDocProcessors.add(next.getRight());
					MessageProcessor msgProcessor = new MessageProcessor(next.getRight(), next.getLeft());
					executorService.execute(msgProcessor);
				} catch (Throwable t) {
					ExceptionUtils.handleThrowable(t);
					// Nothing todo, wait for next run
				} finally {
					MDC.clear();
				}
			}
		}

		private Pair<List<RT2Message>, SequenceDocProcessor> determineNextMessagesToProcess(Deque<Pair<List<RT2Message>, SequenceDocProcessor>> unableToProcess) {
			Pair<List<RT2Message>, SequenceDocProcessor> head = messageQueue.poll();
			if (head == null) {
				return null;
			}
			if (currentUsedDocProcessors.contains(head.getRight())) {
				unableToProcess.addFirst(head);
				return determineNextMessagesToProcess(unableToProcess);
			}
			return head;
		}
	}

	private class MessageProcessor implements Runnable {

		private final SequenceDocProcessor docProc;
		private final List<RT2Message> messagesToProcess;
		private MessageQueuesDispatcherInfo messageQueuesDispatcherInfo;

		public MessageProcessor(SequenceDocProcessor docProc, List<RT2Message> messagesToProcess) {
			this.docProc = docProc;
			this.messagesToProcess = messagesToProcess;

		}

		@Override
		public void run() {
			try {
				this.messageQueuesDispatcherInfo = new MessageQueuesDispatcherInfo(Thread.currentThread(), System.currentTimeMillis(), docProc.getDocUID());
				messageQueuesDispatcherInfos.add(messageQueuesDispatcherInfo);
				messagesToProcess.forEach(aNextMessage -> {
		        	try {
		        		if (aNextMessage.getDocUID() != null) {
		        			MDCHelper.safeMDCPut(MDCEntries.DOC_UID, aNextMessage.getDocUID().getValue());
	                    }
		                if (aNextMessage.getClientUID() != null) {
		                	MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, aNextMessage.getClientUID().getValue());
	                    }
		                MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "processor");
		                MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
		                MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, aNextMessage.getType().getValue());

		                log.trace("RT2: call handleRequest() for message {}", aNextMessage);

		                try {
		                	messageTypeDispatcher.handleRequest(aNextMessage, docProc);
		                }
			            catch (final RT2TypedException rt2ex) {
			            	final ErrorCode aErrorCode = rt2ex.getError();
			                msgSender.sendErrorResponseToClient(null, aNextMessage, aErrorCode, rt2ex);
                            MDCHelper.safeMDCPut(MDCEntries.ERROR, aErrorCode.getCodeAsStringConstant());
			                log.warn("RT2: RT2TypedException received while processing message " + aNextMessage + "!", rt2ex);
                            MDC.remove(MDCEntries.ERROR);
			            }
				        catch (final InterruptedException e) {
				        	Thread.currentThread().interrupt();
				        	MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_SYSTEM_BUSY_ERROR.getCodeAsStringConstant());
				        	log.warn("MessageProcessor for document with id {} terminated!", aNextMessage.getDocUID());
				        	MDC.remove(MDCEntries.ERROR);
				        	msgSender.sendErrorResponseToClient(null, aNextMessage, ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, e);
				        }
			            catch (final Throwable exOnHandleRequest) {
			                ExceptionUtils.handleThrowable(exOnHandleRequest);

			                msgSender.sendErrorResponseToClient(null, aNextMessage, ErrorCode.GENERAL_UNKNOWN_ERROR, exOnHandleRequest);
			                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
			                log.error("RT2: Throwable received while processing message " + aNextMessage + " - state unknown!", exOnHandleRequest);
			                MDC.remove(MDCEntries.ERROR);
			            }
		        	}
			        catch (final Throwable t) {
			            ExceptionUtils.handleThrowable(t);
			            log.error("RT2: Throwable received in queue processor run - state unknown!", t);
			        } finally {
			        	MDC.clear();
					}
		        	lastProcessedMessage.set(aNextMessage);
	        	});
				currentUsedDocProcessors.remove(docProc);
			} finally {
				messageQueuesDispatcherInfos.remove(messageQueuesDispatcherInfo);
			}
		}
	}

	public class MessageQueuesDispatcherInfo {
		private final Thread thread;
		private LocalDateTime startTime;
		private final RT2DocUidType docUid;

		public MessageQueuesDispatcherInfo(Thread thread, long startTime, RT2DocUidType docUid) {
			this.thread = thread;
			this.startTime = LocalDateTime.now();
			this.docUid = docUid;
		}

		public StackTraceElement [] getStacktraceOfThread() {
			return thread.getStackTrace();
		}

		public void interrupt() {
			thread.interrupt();
			messageQueuesDispatcherInfos.remove(this);
		}

		public Duration getRunningtime() {
			return Duration.between(startTime, LocalDateTime.now());
		}

		public RT2DocUidType getDocUid() {
			return docUid;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((docUid == null) ? 0 : docUid.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MessageQueuesDispatcherInfo other = (MessageQueuesDispatcherInfo) obj;
			if (docUid == null) {
				if (other.docUid != null)
					return false;
			} else if (!docUid.equals(other.docUid))
				return false;
			return true;
		}
	}
}
