/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.sequence;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo.Direction;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.log.Loggable;

public abstract class SequenceDocProcessor extends RT2DocProcessor implements Loggable {

	private static final Logger log = LoggerFactory.getLogger(SequenceDocProcessor.class);

	//---------------------------Services--------------------------------------
	@Autowired
	protected ClientEventService clientEventService;

	@Autowired
	protected MsgBackupAndAckProcessorService msgBackupAndAckProcessorService;

	@Autowired

	protected ClientSequenceQueueDisposerService clientSequenceQueueDisposerService;
	
	@Autowired
	protected QueueProcessorDisposer queueProcessorDisposer;
	
	@Autowired
	protected RT2MessageSender messageSender;

	//-------------------------------------------------------------------------
	@Override
	public Logger getLogger() {
		return log;
	}

	//-------------------------------------------------------------------------
	@Override
	public Map<String, Object> getAdditionalLogInfo() {
		Map<String, Object> res = new HashMap<>();
		res.put("com.openexchange.rt2.document.uid", getDocUID());
		return res;
	}

	//-------------------------------------------------------------------------
    // Central method to handle every message sent to the higher-level DOC
    // processors.
	//-------------------------------------------------------------------------

    @Override
	public boolean enqueueMessage(final RT2Message msg) throws Exception {
    	log.debug("enqueueMessage {} for com.openexchange.rt2.document.uid {} from com.openexchange.rt2.client.uid {}", msg.getType(), getDocUID(), msg.getClientUID());

    	rt2MessageLoggerService.addMessageForQueueToLog(Direction.FROM_JMS, msg);
    	if (RT2MessageType.REQUEST_JOIN.equals(msg.getType())) {
    		clientEventService.notifyClientAdded(msg);
    	}

        final RT2CliendUidType clientUID = msg.getClientUID();

		// There are special situations where messages with seq-nrs must be processed although missing them.
		// E.g. CLOSE_DOC/LEAVE which are called by the GC which has no knowledge about seq-nrs
        Integer msgSeqNr  = null;
		if (msg.isSequenceMessage() && !RT2MessageGetSet.getInternalHeader(msg, RT2Protocol.HEADER_INTERNAL_FORCE, false))
			msgSeqNr = RT2MessageGetSet.getSeqNumber(msg);

		msgBackupAndAckProcessorService.updateClientState(msg);
			
		Map<RT2CliendUidType, ClientSequenceQueue> clientsSeqQueues = 
				clientSequenceQueueDisposerService.getClientsSeqQueuesOfDoc(getDocUID(), true);
		synchronized (clientsSeqQueues) {
			final ClientSequenceQueue clientQueue = clientsSeqQueues.get(clientUID);
			if (clientQueue == null) {
				clientSequenceQueueDisposerService.checkStateIfClientUnknown(getDocUID(), clientUID, msg);
				return true;
			}
			boolean checkForNeededNACK = false;			
			if (msgSeqNr == null) {
				handleNoneSequenceMessage(msg);
			} else {
				if (clientQueue != null) {
					final Integer waitForSeqNr = clientQueue.getInWaitSeqNumber();
					if (msgSeqNr.equals(waitForSeqNr)) {	
						checkForNeededNACK = handleExpectedSequenceMessage(msg, clientQueue, waitForSeqNr);
					} else {
						checkForNeededNACK = handleUnexpectedSequenceMessage(msg, clientQueue, waitForSeqNr);
					}
				}
			}			
			// request missing messages via NACK
			if (checkForNeededNACK)
				checkForAndRequestMissingMessagesViaNack(clientUID);			
		}			
		return true;
	}

	//-------------------------------------------------------------------------    
	boolean handleUnexpectedSequenceMessage(final RT2Message aRT2Message, final ClientSequenceQueue clientQueue, final Integer waitForSeqNr) throws Exception {
		final RT2CliendUidType clientUID = aRT2Message.getClientUID();
		Integer msgSeqNr = aRT2Message.getSeqNumber().getValue();
		boolean seqNrAlreadySeen = clientQueue.wasMsgAlreadySeen(aRT2Message);
		if (!seqNrAlreadySeen) {
			clientQueue.enqueueInMessage(aRT2Message);
			msgBackupAndAckProcessorService.addReceivedSeqNr(aRT2Message.getDocUID(), clientUID, msgSeqNr, msgSeqNr);

			log.trace("RT2: Message queued as SEQ-NR: {} not in order (wait for {})" + ", for com.openexchange.rt2.client.uid {}", msgSeqNr, waitForSeqNr, clientUID);
			return true;
		}
		log.trace("RT2: Message already seen with SEQ-NR: {}, for com.openexchange.rt2.client.uid {}", msgSeqNr, clientUID);
		return false;
	}

	//-------------------------------------------------------------------------	
	boolean handleExpectedSequenceMessage(final RT2Message aRT2Message, final ClientSequenceQueue clientQueue, final Integer waitForSeqNr) throws Exception {
		final RT2CliendUidType clientUID = aRT2Message.getClientUID();
		Integer msgSeqNr = aRT2Message.getSeqNumber().getValue();		
		boolean checkForNeededNACK;
		clientQueue.enqueueInMessage(aRT2Message);				

		List<RT2Message> canBeProcessed = clientQueue.determinePendingMessagesWhichCanBeProcessed();
		int nextWaitForSeqNr = waitForSeqNr + canBeProcessed.size();

		// store received seq number range for an async ACK
		msgBackupAndAckProcessorService.addReceivedSeqNr(aRT2Message.getDocUID(), clientUID, msgSeqNr, Math.max(msgSeqNr, nextWaitForSeqNr - 1));				
		
		// add messages to queue for further processing
		queueProcessorDisposer.addMessages(this, canBeProcessed);
		clientQueue.removedNacksOfReceivedMessages(canBeProcessed);
		
		checkForNeededNACK = clientQueue.hasQueuedInMessages();
		return checkForNeededNACK;
	}

	//-------------------------------------------------------------------------	
	void handleNoneSequenceMessage(final RT2Message aRT2Message) throws Exception, JSONException {
		final RT2CliendUidType clientUID = aRT2Message.getClientUID();
		final ClientSequenceQueue clientQueue = 
				clientSequenceQueueDisposerService.getClientSequenceQueueForClient(getDocUID(), clientUID, true);
		boolean bQueueMessage = true;
		switch (aRT2Message.getType()) {
		    case ACK_SIMPLE: {
		        // remove messages from backup map
		    	msgBackupAndAckProcessorService.removeBackupMessagesForACKs(aRT2Message);
		        bQueueMessage = false;
		    } break;
		    case NACK_SIMPLE: {
		        // a simple NACK must be directly processed
		    	msgBackupAndAckProcessorService.resendMessages(aRT2Message);
		        bQueueMessage = false;
		    } break;
		    case REQUEST_ABORT_OPEN: {
		    	log.debug("RT2: REQUEST_ABORT_OPEN received for com.openexchange.rt2.document.uid {} and com.openexchange.rt2.client.uid {}", getDocUID(), clientUID);
		    	clientQueue.setAborted(true);
		        bQueueMessage = false;

		        final RT2Message aAbortOpenResponse = RT2MessageFactory.createResponseFromMessage(aRT2Message, RT2MessageType.RESPONSE_ABORT_OPEN);
		        messageSender.sendResponseToClient(clientUID, aAbortOpenResponse);
		    } break;
		    case REQUEST_UNAVAILABILITY: {  // Really queue this message???
				log.debug("RT2: REQUEST_UNAVAILABILITY received for com.openexchange.rt2.document.uid {} and com.openexchange.rt2.client.uid {}", getDocUID(), clientUID);
				msgBackupAndAckProcessorService.switchState(aRT2Message);
		    } break;
		    default: break;
		}
		if (bQueueMessage) {
			queueProcessorDisposer.addMessages(this, Arrays.asList(aRT2Message));
		}
	}
	
    //-------------------------------------------------------------------------
    public void checkForAndRequestMissingMessagesViaNack(final RT2CliendUidType clientUID)	throws Exception {
        ClientSequenceQueue clientSeqQueue = 
        		clientSequenceQueueDisposerService.getClientSequenceQueueForClient(getDocUID(), clientUID, true);
        Set<Integer> nacks = clientSeqQueue.checkForAndGenerateNacks();
		if (!nacks.isEmpty()) {			
			final RT2Message aNackRequest = RT2MessageFactory.newSimpleNackRequest(clientUID, getDocUID(), nacks);
			
			log.debug("RT2: Sending NACK for com.openexchange.rt2.client.uid {} with SEQ-NRS: {}", clientUID, nacks.toString());
			messageSender.sendMessageWOSeqNrTo(clientUID, aNackRequest);
		}
    }
}
