/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.domain;

import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.rt2.core.doc.EditableDocumentStatus;
import com.openexchange.office.tools.common.json.JSONHelper;

public class PendingOperations implements Cloneable {

	private static final Logger log = LoggerFactory.getLogger(PendingOperations.class);

	private final JSONArray pendingOperations = new JSONArray();

	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	public PendingOperations() {}

	private PendingOperations(JSONArray pendingOperations) throws JSONException {
		JSONHelper.appendArray(this.pendingOperations, pendingOperations);
	}

	@Override
	public PendingOperations clone() {
		try {
			return new PendingOperations(getPendingOperationsCloned());
		} catch (JSONException ex) {
			throw new RuntimeException(ex);
		}
	}

	public int getPendingOperationsCount(boolean otEnabled, EditableDocumentStatus docStatus) {
		ReadLock readLock = lock.readLock();
		try {
			readLock.lock();
			if (otEnabled) {
				try {
					final EditableDocumentStatus aDocStatus = docStatus.clone();
					final int documentOSN = aDocStatus.getDocumentOSN();
					// DOCS-2144 ensure that non-modifying ops are not counted
					return OperationHelper.countOperationsWithOSNLargerThan(pendingOperations, documentOSN, false);
				} catch (CloneNotSupportedException e) {
					log.error("Exception caught trying to determine number of pending operations of the current document", e);
					return pendingOperations.length();
				}
			} else {
				return OperationHelper.countOperations(pendingOperations, false);
			}
		} finally {
			readLock.unlock();
		}
	}

	// -------------------------------------------------------------------------
	public boolean hasPendingOperations(boolean otEnabled, EditableDocumentStatus docStatus) {
		return getPendingOperationsCount(otEnabled, docStatus) > 0;
	}

	// -------------------------------------------------------------------------
	public JSONArray getPendingOperationsCloned() throws JSONException {
		final JSONArray clone = new JSONArray();
		ReadLock readLock = lock.readLock();		
		try {
			readLock.lock();
			JSONHelper.appendArray(clone, pendingOperations);
		} finally {
			readLock.unlock();
		}
		return clone;
	}

	// -------------------------------------------------------------------------
	public JSONArray getPendingOperationsFilteredCloned(int clientOSN) throws JSONException {
		ReadLock readLock = lock.readLock();
		try {
			readLock.lock();
			return OperationHelper.filterOperationsWithOSNSmallerEqualThan(pendingOperations, clientOSN);
		} finally {
			readLock.unlock();
		}
	}

    // -------------------------------------------------------------------------
    public int addOperations(final JSONArray aNewOps) throws JSONException {
        int nNewOSN = -1;
        final JSONObject aLastOp = OperationHelper.getLastOperation(aNewOps);
        if (aLastOp != null) {
        	nNewOSN = aLastOp.getInt(OperationHelper.KEY_OSN);
            nNewOSN += aLastOp.getInt(OperationHelper.KEY_OPL);        
        }
        addOperationsArray(aNewOps);
        return nNewOSN;
    }

    // -------------------------------------------------------------------------
	public void addOperationsOT(final JSONArray aNewOps) throws JSONException {
		addOperationsArray(aNewOps);
	}

	// -------------------------------------------------------------------------
	public void addEmergencyOperations(final JSONArray aEmergencyOps) throws JSONException {
		addOperationsArray(aEmergencyOps);
	}

	// -------------------------------------------------------------------------
	public void clearPendingOperations() {
		WriteLock writeLock = lock.writeLock();
		try {
			writeLock.lock();
			pendingOperations.reset();
		} finally {
			writeLock.unlock();
		}
	}

	// -------------------------------------------------------------------------
	public void cutOperations(int nSize) {
		WriteLock writeLock = lock.writeLock();
		try {
			writeLock.lock();
			if (pendingOperations.length() > nSize) {
				try {
					JSONHelper.remove(pendingOperations, 0, nSize);
				} catch (JSONException e) {
					log.error("Exception caught trying to remove obsolete operations from pending operations array", e);
				}
			} else {
				pendingOperations.reset();
			}
		} finally {
			writeLock.unlock();
		}
	}
	
    // -------------------------------------------------------------------------
	private void addOperationsArray(final JSONArray aNewOps) throws JSONException {
		WriteLock writeLock = lock.writeLock();
		try {
			writeLock.lock();
			JSONHelper.appendArray(pendingOperations, aNewOps);
		} finally {
			writeLock.unlock();
		}
	}
}
