/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.metric;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.metrics.micrometer.Micrometer;
import com.openexchange.office.rt2.core.management.RT2BackendManagement;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

@Service
public class RT2BackendMetricService implements InitializingBean, DisposableBean {

    private static final String NO_UNIT = null;
    private static final String GROUP_DOCPROCESSOR = "appsuite.documents.docprocessor";
    private static final String GROUP_DOCPROXY = "appsuite.documents.docproxy";

    @Autowired
    private RT2BackendManagement metricDocsService;

    @Override
    public void destroy() throws Exception {
        // nothing to do
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_DOCPROCESSOR + ".count", Tags.empty(), "The current number of open documents.", NO_UNIT, this, (m) -> getCountRT2DocProcessors());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_DOCPROXY + ".count", Tags.empty(), "The current number of clients bound to open documents.", NO_UNIT, this, (m) -> getCountRT2DocProxies());
    }

    public int getCountRT2DocProcessors() {
        return metricDocsService.getCountRT2DocProcessors();
    }

    public int getCountRT2DocProxies() {
        return metricDocsService.getCountRT2DocProxies();
    }

}
