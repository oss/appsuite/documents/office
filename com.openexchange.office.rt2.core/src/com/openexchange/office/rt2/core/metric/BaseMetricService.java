/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.metric;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import io.micrometer.core.instrument.Metrics;

public abstract class BaseMetricService implements InitializingBean, DisposableBean {
	private static final long TIME_CLEANUP_FREQUENCY_IN_MS   = 1000 * 60 * 10;
	private static final long TIMEOUT_FOR_CLEANUP_IN_MINUTES = 3;
	private static final Logger log = LoggerFactory.getLogger(BaseMetricService.class);
	private static final String PREFIX = "appsuite.documents.";

	protected ConcurrentHashMap<RT2MessageIdType, Pair<Timer.Context, LocalDateTime>> metricTimerContextMap;
	private final Map<LocalDateTime, Set<RT2MessageIdType>> timeMap = new ConcurrentHashMap<>();
	protected Timer metricTimer;
	protected io.micrometer.core.instrument.Timer micrometerTimer;
	private Thread cleanupThread;
	private String metricName;

	private AtomicBoolean stopped = new AtomicBoolean(false);
	
	@Autowired
	private MetricRegistry metricRegistry;
	
	protected BaseMetricService(String metricName) {
		this.metricName = metricName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.metricTimerContextMap = new ConcurrentHashMap<>();
		this.metricTimer = metricRegistry.timer(metricName);
		this.micrometerTimer = io.micrometer.core.instrument.Timer.builder(PREFIX + StringUtils.lowerCase(metricName))
            .description("Timing of rt2 message round trip")
            .tags()
            .register(Metrics.globalRegistry);

		this.cleanupThread = new Thread(new CleanupTask());
		cleanupThread.setName(metricName + ".CleanupThread");
		cleanupThread.setDaemon(true);
		cleanupThread.start();
	}

	@Override
	public void destroy() throws Exception {
		stopped.set(true);
	}

	public void startTimer(RT2MessageIdType msgId) {
		LocalDateTime now = LocalDateTime.now();
		Set<RT2MessageIdType> msgIds = timeMap.get(now);
		if (msgIds == null) {
			msgIds = new HashSet<>();
			timeMap.put(now, msgIds);
		} 
		msgIds.add(msgId);
		metricTimerContextMap.put(msgId, Pair.of(metricTimer.time(), now));
	}

	public void stopTimer(RT2MessageIdType msgId) {
		Pair<Timer.Context, LocalDateTime> p = metricTimerContextMap.get(msgId);
		if (p != null) {
			long nanos = p.getKey().stop();
			micrometerTimer.record(Duration.ofNanos(nanos));
			metricTimerContextMap.remove(msgId);
			Set<RT2MessageIdType> msgIds = timeMap.get(p.getValue());
			if ((msgIds != null) && (msgIds.size() > 1)) {
				msgIds.remove(msgId);
			} else {
				if ((msgIds != null) &&  msgIds.contains(msgId)) {
					timeMap.remove(p.getValue());
				}
			}
		}
	}

	private class CleanupTask implements Runnable {

		@Override
		public void run() {
			while (!stopped.get()) {
				try {
					Thread.sleep(TIME_CLEANUP_FREQUENCY_IN_MS);
					final LocalDateTime cmpDt = LocalDateTime.now().minusMinutes(TIMEOUT_FOR_CLEANUP_IN_MINUTES);
					final AtomicLong removed = new AtomicLong(0);

					synchronized (timeMap) {
						List<LocalDateTime> timeList = new ArrayList<LocalDateTime>(timeMap.keySet()); 
						Collections.sort(timeList);
						for (LocalDateTime dt : timeList) {
							if (dt.isBefore(cmpDt)) {
								Set<RT2MessageIdType> msgIdSet = timeMap.remove(dt);
								msgIdSet.forEach(msgId -> {
									if (null != metricTimerContextMap.remove(msgId))
										removed.incrementAndGet();
								});
							}
						}
					}

					synchronized (metricTimerContextMap) {
						// remove pending timer after  too
						metricTimerContextMap.keySet().forEach(k -> {
							final Pair<Timer.Context, LocalDateTime> entry = metricTimerContextMap.get(k);
							if ((entry != null) && (entry.getRight().isBefore(cmpDt))) {
								// stop pending timer after timeout
								long nanos = entry.getLeft().stop();
								micrometerTimer.record(Duration.ofNanos(nanos));
								metricTimerContextMap.remove(k);
							}
						});
					}

					log.debug("{} removed {} entries from metrics map - time map has still {} entries & metricTimerContextMap has {} entries" , Thread.currentThread().getName(), removed.get(), timeMap.size(), metricTimerContextMap.size());
				} catch (Throwable ex) {
					ExceptionUtils.handleThrowable(ex);
					log.warn("CleanupTask: Exception caught while trying to remove old metric data", ex);
				}
			}
		}

	}
}
