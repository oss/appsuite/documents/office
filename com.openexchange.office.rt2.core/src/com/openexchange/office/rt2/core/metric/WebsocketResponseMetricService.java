/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.metric;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.metrics.micrometer.Micrometer;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

@Service
public class WebsocketResponseMetricService extends BaseMetricService implements InitializingBean, DisposableBean{
	
	private static final Logger log = LoggerFactory.getLogger(WebsocketResponseMetricService.class);

    private static final String NO_UNIT = null;
	private static final long MSG_TIME_CLEANUP_FREQUENCY_IN_MS = 1000 * 60 * 10;
	private static final long MSG_TMEOUT_FOR_CLEANUP_IN_MINS   = 3;

	private static class MessageTimeEntry implements Comparable<MessageTimeEntry> {
		public final RT2MessageIdType id;
		public final LocalDateTime dateTime;

		public MessageTimeEntry(RT2MessageIdType id) {
			this.id = id;
			this.dateTime = LocalDateTime.now();
		}

		@Override
		public int compareTo(MessageTimeEntry o) {
			return this.dateTime.compareTo(o.dateTime);
		}
	}

	@Autowired
	private MetricRegistry metricRegistry;
	
	private final Map<RT2MessageIdType, MessageTimeEntry> receivedMessageIds = Collections.synchronizedMap(new HashMap<>());
	private Thread receivedMsgCleanupThread;

	private AtomicLong messagesReceived = new AtomicLong(0);
	private AtomicLong succMessages = new AtomicLong(0);
	private AtomicLong failedMessages = new AtomicLong(0);

	private AtomicBoolean stopped = new AtomicBoolean(false);
	
	public WebsocketResponseMetricService() {
        super(MetricRegistry.name("WSChannel", "runtime"));
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();

		// dropwizard metrics
		metricRegistry.register(MetricRegistry.name("messages", "countAll"), 
				new Gauge<Long>() {
					@Override
					public Long getValue() {
						return messagesReceived.get();
					}
				});
		metricRegistry.register(MetricRegistry.name("messages", "countSucc"), 
				new Gauge<Long>() {
					@Override
					public Long getValue() {
						return succMessages.get();
					}
				});
		metricRegistry.register(MetricRegistry.name("messages", "countFailed"), 
				new Gauge<Long>() {
					@Override
					public Long getValue() {
						return failedMessages.get();
					}
				});
		metricRegistry.register(MetricRegistry.name("messages", "noResponse"),
		        new Gauge<Long>() {
                    @Override
                    public Long getValue() {
                        return (messagesReceived.get() - failedMessages.get() - succMessages.get());
                    }
		        });

		// io.micrometer metrics
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry,
            "appsuite.documents.rt2.messages.count.total",
            Tags.of("state", "all"),
            "The total number of rt2 messages received.",
            NO_UNIT,
            this, (m) -> messagesReceived.get());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry,
            "appsuite.documents.rt2.messages.count",
            Tags.of("state", "succeeded"),
            "The number of rt2 messages received and successfully processed.",
            NO_UNIT,
            this, (m) -> succMessages.get());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry,
            "appsuite.documents.rt2.messages.count",
            Tags.of("state", "failed"),
            "The number of rt2 messages received, but failed processing.",
            NO_UNIT,
            this, (m) -> failedMessages.get());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry,
            "appsuite.documents.rt2.messages.count",
            Tags.of("state", "noResponse"),
            "The number of rt2 messages received which resulted in no response.",
            NO_UNIT,
            this, m -> (messagesReceived.get() - failedMessages.get() - succMessages.get()));

		this.receivedMsgCleanupThread = new Thread(new ReceivedMsgIdsCleanupTask());
		receivedMsgCleanupThread.setName("ReceivedMsgIds.CleanupThread");
		receivedMsgCleanupThread.setDaemon(true);
		receivedMsgCleanupThread.start();
	}

	@Override
	public void destroy() throws Exception {
		stopped.set(true);
	}

	public void incMessagesReceived(RT2MessageIdType msgId) {
		receivedMessageIds.put(msgId, new MessageTimeEntry(msgId));
		messagesReceived.incrementAndGet();
	}

	public void incMessageSucc(RT2MessageIdType msgId) {
		if (receivedMessageIds.containsKey(msgId)) {
			succMessages.incrementAndGet();
			receivedMessageIds.remove(msgId);
		}
	}

	public void incMessageFailed(RT2MessageIdType msgId) {
		if (receivedMessageIds.containsKey(msgId)) {
			failedMessages.incrementAndGet();
			receivedMessageIds.remove(msgId);
		}
	}

	private class ReceivedMsgIdsCleanupTask implements Runnable {

		@Override
		public void run() {
			while (!stopped.get()) {
				try {
					Thread.sleep(MSG_TIME_CLEANUP_FREQUENCY_IN_MS);
					final LocalDateTime cmpDt = LocalDateTime.now().minusMinutes(MSG_TMEOUT_FOR_CLEANUP_IN_MINS);
					final AtomicLong removed = new AtomicLong(0);

					synchronized (receivedMessageIds) {
						List<MessageTimeEntry> timeList = new ArrayList<MessageTimeEntry>(receivedMessageIds.values()); 
						Collections.sort(timeList);
						for (MessageTimeEntry dt : timeList) {
							if (dt.dateTime.isBefore(cmpDt)) {
								receivedMessageIds.remove(dt.id);
								removed.incrementAndGet();
							}
						}
					}

					log.debug("{} removed {} entries from receivedMessageIds map - map has still {} entries", Thread.currentThread().getName(), removed.get(), receivedMessageIds.size());
				} catch (Throwable ex) {
					ExceptionUtils.handleThrowable(ex);
					log.warn("CleanupTask: Exception caught while trying to remove old metric data", ex);
				}
			}
		}

	}

}
