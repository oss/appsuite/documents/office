/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.lang.ref.WeakReference;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.Sets;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.java.ConcurrentHashSet;
import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.proxy.EDocProxyState;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealth;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthMap;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthState;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeHealthState;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;

@Service
@ShutdownOrder(value=-9)
@RegisteredService
public class RT2GarbageCollector implements Runnable, DisposableBean, InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(RT2GarbageCollector.class);
	public static final String REFCOUNT_CLIENT_PREFIX  = "rt2cache.atomic.";
	public static final String REFCOUNT_CLIENT_POSTFIX = ".refcount.clients.a";

    //-------------------------------------------------------------------------
	public static final String KEY_GC_FREQUENCY = "rt2.gc.frequency";

    //---------------------------Services--------------------------------------
	@Autowired
	private RT2DocProcessorManager docProcMngr;

	@Autowired
	private ClusterLockService clusterLockService;

	@Autowired
	private ClusterService clusterService;

	@Autowired
	private DistributedDocInfoMap  distributedDocInfoMap;

	@Autowired
	private RT2DocProxyRegistry docProxyRegistry;

	@Autowired
	private RT2DocOnNodeMap docOnNodeMap;;

	@Autowired
	private RT2ConfigService configService;

	@Autowired
	private MetricRegistry metricRegistry;

    @Autowired
    private RT2NodeInfoService rt2NodeInfo;

    @Autowired
    private SpecialLogService specialLogService;

    @Autowired
    private RT2NodeHealthMap nodeHealthMap;

    @Autowired
    private CachingFacade cachingFacade;

    //-------------------------------------------------------------------------

	private final ScheduledExecutorService finalRt2DocProcessorRemoveScheduler = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2GarbageCollectorFinalProcRemove-%d").build());

	private ScheduledExecutorService m_aScheduler = null;

	private Counter docProxyOfflineCounter;
	private Counter docProxyRemovedCounter;
	private Counter docProcRemovedCounter;
	private Counter refCountClientsRemovedCounter;
	private Counter docOnNodeMapEntryRemovedCounter;

	private Map<RT2DocUidType, LocalDateTime> atomicLongsToGc = new HashMap<>();
	private Set<RT2DocUidType> atomicLongsToBeVerified = new ConcurrentHashSet<>();
	private Set<RT2DocUidType> docUidsToClear = new ConcurrentHashSet<>();
	private Map<RT2NodeUuidType, LocalDateTime> docOnNodeEntriesToGC = new ConcurrentHashMap<>();
	private long minWaitTimeBeforeGcAtomicLong = 5 * 60; // 5 minutes in seconds

	public RT2GarbageCollector() {
	}

	public RT2GarbageCollector(long minWaitTimeBeforeGcAtomicLong) {
		this.minWaitTimeBeforeGcAtomicLong = minWaitTimeBeforeGcAtomicLong;
	}

	//-------------------------------------------------------------------------
    public Map<RT2DocUidType, LocalDateTime> getAtomicLongsToGc() {
		return new HashMap<>(atomicLongsToGc);
	}

	//-------------------------------------------------------------------------
	public Set<RT2DocUidType> getAtomicLongsToBeVerified() {
		return new HashSet<>(atomicLongsToBeVerified);
	}

    @Override
	public void afterPropertiesSet() throws Exception {

	    if (m_aScheduler != null)
	        return;

		this.docProxyOfflineCounter = metricRegistry.counter(MetricRegistry.name("GarbageCollector", "docProxy", "offline"));
		this.docProxyRemovedCounter = metricRegistry.counter(MetricRegistry.name("GarbageCollector", "docProxy", "removed"));
		this.docProcRemovedCounter = metricRegistry.counter(MetricRegistry.name("GarbageCollector", "docProcessor", "removed"));
		this.refCountClientsRemovedCounter = metricRegistry.counter(MetricRegistry.name("GarbageCollector", "refCountClients", "removed"));
		this.docOnNodeMapEntryRemovedCounter = metricRegistry.counter(MetricRegistry.name("GarbageCollector", "docOnNodeMap", "removed"));

	    LogMethodCallHelper.logMethodCall(log, this, "start");

        final Runnable aRunner = () -> {impl_doGC ();};

        final long                     nFrequencyInMS = configService.getRT2GCFrequencyInMS();
        if (nFrequencyInMS > 0) {
	        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("RT2GarbageCollector-%d").build();
	        final ScheduledExecutorService aScheduler     = Executors.newScheduledThreadPool(1, namedThreadFactory);
	        aScheduler.scheduleAtFixedRate(aRunner, nFrequencyInMS, nFrequencyInMS, TimeUnit.MILLISECONDS);
	        m_aScheduler = aScheduler;
        }
        LogMethodCallHelper.logMethodCallRes(log, this.getClass(), "start", Void.class);
	}

	@Override
	public void destroy() throws Exception {
        if (m_aScheduler == null)
            return;

        final ScheduledExecutorService aScheduler = m_aScheduler;
        m_aScheduler = null;
        LogMethodCallHelper.logMethodCall(log, this, "stop");

        aScheduler.shutdownNow();
        finalRt2DocProcessorRemoveScheduler.shutdown();

        try {
	        final boolean bOK = aScheduler.awaitTermination(30000, TimeUnit.MILLISECONDS);
	        if ( ! bOK) {
	            log.warn("... RT2-GC not stopped in time !");
	            // no exception please - GC is stopped in shutdown of process ...
	            // so thread will be stopped anyway ...
	        } else {
	            LogMethodCallHelper.logMethodCallRes(log, this.getClass(), "stop", Void.class);
	        }
        } catch (InterruptedException ex) {
        	Thread.currentThread().interrupt();
        }
    }

	//-------------------------------------------------------------------------
	@Override
    public void run () {
        impl_doGC ();
        LogMethodCallHelper.logMethodCallRes(log, this.getClass(), "start", Void.class);
	}

    //-------------------------------------------------------------------------

	public boolean doGcForDocUid(RT2DocUidType docUid, boolean rememberWhenException) {
        final ClusterLock clusterLock = clusterLockService.getLock(docUid);
        boolean locked = false;
        boolean lockExceptionOccured = false;
        try {
            log.debug("Lock called by garbage collector for com.openexchange.rt2.document.uid [{}]", docUid);
            locked = clusterLock.lock();
            rt2NodeInfo.deregisterDocOnNode(docUid);
            distributedDocInfoMap.freeDocInfos(docUid);
            return true;
        } catch (ClusterLockException e) {
        	lockExceptionOccured = true;
		} finally {
        	if (locked) {
        		clusterLock.unlock();
        	}
		}
        if (rememberWhenException && (!locked || lockExceptionOccured)) {
        	docUidsToClear.add(docUid);
        }
        return false;
	}

    //-------------------------------------------------------------------------

    public void doGC() {
    	impl_doGC();
    }

    //-------------------------------------------------------------------------
    public void registerDocRefCountForRemovalVerification(RT2DocUidType refCountOfdocUIDToCheck) {
        atomicLongsToBeVerified.add(refCountOfdocUIDToCheck);
    }

    //-------------------------------------------------------------------------
    private void impl_doGC () {
        try {
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2GarbageCollector");
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
            checkAndGCOfflineOrStaleDocProxies();
            checkAndGCStaleDocResources();
            checkAndGCAtomicLongAsClientRef();
            checkAndEnsureConsitencyOfDocOnNodeMapForThisNode();
            checkDetectedDocUids();
            checkAndEnsureConsistencyOfDocOnNodeMapGlobally();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            MDC.clear();
        }
    }

    //-------------------------------------------------------------------------
    private void checkDetectedDocUids() {
		final Set<RT2DocUidType> toRemove = new HashSet<>();
		docUidsToClear.forEach(docUid -> {
			if (doGcForDocUid(docUid, false)) {
				toRemove.add(docUid);
			}
		});
		docUidsToClear.removeAll(toRemove);
	}

    //-------------------------------------------------------------------------
    private void checkAndGCOfflineOrStaleDocProxies () {
        LogMethodCallHelper.logMethodCall(log, this, "checkAndGCOfflineOrStaleDocProxies");

        final List< RT2DocProxy > docProxies = docProxyRegistry.listAllDocProxies();
        // Default: 30 Minutes
        final long offlineTimeoutThreshold = configService.getRT2GCOfflineTresholdInMS();
        final long now = System.currentTimeMillis();

        for (final RT2DocProxy docProxy : docProxies) {
            final long offlineTimeoutProxy  = docProxy.getOfflineTimeInMS();
            log.debug("offlineTimeoutOfProxyToLong, offlineTimeoutProxy: {}, offlineTimeoutThreshold: {}", offlineTimeoutProxy, offlineTimeoutThreshold);
            final boolean offlineTimeoutOfProxyToLong = !docProxy.isOnline() && (offlineTimeoutProxy > offlineTimeoutThreshold);
            final boolean docProxyHangingInInitPhase  = isDocProxyHangingInInitPhase(docProxy, now, offlineTimeoutThreshold);

            if (offlineTimeoutOfProxyToLong || docProxyHangingInInitPhase) {
                if (offlineTimeoutOfProxyToLong) {
                	docProxyOfflineCounter.inc();
                    final String sOfflineSinceHumanReadable = DurationFormatUtils.formatDuration(offlineTimeoutProxy, "HH:mm:ss", /* pad with zeros */ true);
                    specialLogService.log(Level.DEBUG, log, docProxy.getDocUID().getValue(), new Throwable(), "Doc proxy {} is offline for {}", docProxy, sOfflineSinceHumanReadable);
                } else {
               		final long nStaleDuration = Math.abs(now - docProxy.getCreationTimeInMS());
               		final String sStaleSinceHumanReadable = DurationFormatUtils.formatDuration(nStaleDuration, "HH:mm:ss", /* pad with zeros */ true);
               		specialLogService.log(Level.DEBUG, log, docProxy.getDocUID().getValue(), new Throwable(), "Doc proxy {} is hanging in init phase for {}", docProxy, sStaleSinceHumanReadable);
                }

	            if (!docProxyHangingInInitPhase) {
	            	try {
	            		docProxy.closeHard(Cause.GARBAGE_COLLECTING);
	            	} catch (Exception ex) {
	            		// An exception here is not a problem because there is an additional task(checkAndGCStaleDocResources()) which removes the corresponding RT2DocProcessor
	            		specialLogService.log(Level.DEBUG, log, docProxy.getDocUID().getValue(), new Throwable(), "GC failed for docProxy {}", docProxy, ex);
	            	}
	            }
	            docProxyRemovedCounter.inc();
                docProxyRegistry.deregisterDocProxy(docProxy, true);
            }
        }
    }

    //-------------------------------------------------------------------------
    private boolean isDocProxyHangingInInitPhase(final RT2DocProxy aDocProxy, long nNow, long nOfflineTresholdInMS) {

    	final long proxyCreationTime = aDocProxy.getCreationTimeInMS();
        final EDocProxyState docProxyState = aDocProxy.getDocState();

        return docProxyState.equals(EDocProxyState.E_INIT) && (Math.abs(nNow - proxyCreationTime) > nOfflineTresholdInMS);
    }

    //-------------------------------------------------------------------------
    private void checkAndGCStaleDocResources ()
    {
        LogMethodCallHelper.logMethodCall(log, this, "checkAndGCStaleDocResources");
        final Set<WeakReference<RT2DocProcessor>> localDocProcs = docProcMngr.getWeakReferenceToDocProcessors();

        if (!localDocProcs.isEmpty())
        {
            final Set<RT2DocProcessor> docProcsWOClients = retrieveDocProcessorsWithoutClients(localDocProcs);
            Set<RT2DocUidType> possibleDocProcsToRemove = new HashSet<>();
            for (final RT2DocProcessor docProc : docProcsWOClients)
            {
                final RT2DocUidType docUID = docProc.getDocUID();
               	destroyRefCount4Clients(docUID);
               	specialLogService.log(Level.DEBUG, log, docProc.getDocUID().getValue(), new Throwable(), "Added com.openexchange.rt2.document.uid {} to possible removed DocProcessors", docProc.getDocUID().getValue());
                possibleDocProcsToRemove.add(docUID);
            }
            if (!possibleDocProcsToRemove.isEmpty()) {
            	finalRt2DocProcessorRemoveScheduler.schedule(new FinalRt2DocProcessorRemoveThread(possibleDocProcsToRemove), 5, TimeUnit.MINUTES);
            }
        }
    }

    //-------------------------------------------------------------------------
	private void destroyRefCount4Clients(final RT2DocUidType docUID) {
		final ClusterLock clusterLock = clusterLockService.getLock(docUID);
		boolean locked = false;
		try {
			locked = clusterLock.lock();
			if (locked) {
				distributedDocInfoMap.freeDocInfos(docUID);
			}
		} catch (ClusterLockException e) {
			// Ignore, try again next time
		} finally {
			if (locked) {
		        clusterLock.unlock();
			}
		}
	}

    //-------------------------------------------------------------------------
    void checkAndGCAtomicLongAsClientRef ()
    {
    	{
    		distributedDocInfoMap.getAllRegisteredDocIds().forEach(docUid -> {
	            try {
	                checkAndDestroyAtomicLongSafely(docUid);
	            }
	            catch (Throwable t) {
	                ExceptionUtils.handleThrowable(t);
	                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
	                log.error("RT2: GC failed for client ref-count instance com.openexchange.rt2.document.uid " + docUid , t);
	                MDC.remove(MDCEntries.DOC_UID);
	            }
    		});
    	}

        // insert the registered atomic long names in the gc map
        for (final RT2DocUidType docUIDOfRefCount : atomicLongsToBeVerified) {
            try {
                checkAndDestroyAtomicLongSafely(docUIDOfRefCount);
            } catch (Throwable t) {
                ExceptionUtils.handleThrowable(t);
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUIDOfRefCount.getValue());
                log.error("RT2: GC failed for client ref-count instance com.openexchange.rt2.document.uid " + docUIDOfRefCount  + " found with count = 0", t);
                MDC.remove(MDCEntries.DOC_UID);
            }
        }
    }

    //-------------------------------------------------------------------------
    private void checkAndDestroyAtomicLongSafely(final RT2DocUidType docUID) {
        if (shouldBeVerifiedOrGarbageCollected(docUID)) {
        	ClusterLock clusterLock = clusterLockService.getLock(docUID);
            try {
            	clusterLock.lock();
                boolean canBeGC = distributedDocInfoMap.getRefCount4Clients(docUID) == 0;
                if (!canBeGC && (DistributedDocInfoMap.DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE.equals(distributedDocInfoMap.getStatus(docUID)) || distributedDocInfoMap.getRefCount4Clients(docUID) == 0)) {
                	if (atomicLongsToGc.containsKey(docUID)) {
                		LocalDateTime insertTime = atomicLongsToGc.get(docUID);
                		if (insertTime.plusSeconds(minWaitTimeBeforeGcAtomicLong).isBefore(LocalDateTime.now())) {
                			canBeGC = true;
                			atomicLongsToGc.remove(docUID);
                            atomicLongsToBeVerified.remove(docUID);
                		}
                	} else {
                		atomicLongsToGc.put(docUID, LocalDateTime.now());
                	}
                } else {
                	atomicLongsToGc.remove(docUID);
                    atomicLongsToBeVerified.remove(docUID);
                }
                if (canBeGC) {
					MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUID.getValue());
					log.info("Removing AtomicLong with name com.openexchange.rt2.document.uid {}", docUID);
                	refCountClientsRemovedCounter.inc();
                	distributedDocInfoMap.freeDocInfos(docUID);
                }
        	} catch (ClusterLockException e) {
        		// Ignore, try again next time
        	} finally {
            	clusterLock.unlock();
            	MDC.remove(MDCEntries.DOC_UID);
            }
        } else {
        	// verification completed, remove entry as we see a normal value for the ref-count
        	if (docUID != null) {
	            atomicLongsToBeVerified.remove(docUID);
	            atomicLongsToGc.remove(docUID);
        	}
        }
    }

    //-------------------------------------------------------------------------
    private boolean shouldBeVerifiedOrGarbageCollected(final RT2DocUidType docUID) {
    	return DistributedDocInfoMap.DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE.equals(distributedDocInfoMap.getStatus(docUID)) || (distributedDocInfoMap.getRefCount4Clients(docUID) == 0);
    }

    //-------------------------------------------------------------------------
    private void checkAndEnsureConsitencyOfDocOnNodeMapForThisNode() {
        LogMethodCallHelper.logMethodCall(log, this, "checkAndEnsureConsitencyOfDocOnNodeMapForThisNode");

        RT2DocOnNodeMap aDocOnNodeMap = docOnNodeMap;
        Set<String> docIds = aDocOnNodeMap.getDocsOfMember();
        for (String str : docIds) {
            RT2DocUidType docUid = new RT2DocUidType(str);
            if (!docProcMngr.contains(docUid)) {
                removeDocOnNodeEntryAndAtomicLongForDocUid(docUid, false);
            }
        }
    }

    //-------------------------------------------------------------------------
    private void checkAndEnsureConsistencyOfDocOnNodeMapGlobally() {
        LogMethodCallHelper.logMethodCall(log, this, "checkAndEnsureConsistencyOfDocOnNodeMapGlobally");

        RT2DocOnNodeMap aDocOnNodeMap = docOnNodeMap;
        final Set<String> currentMemberUUIDs = clusterService.getAllMemberUuidsOfCluster();
        final Set<String> memberUUIDsWithDocs = aDocOnNodeMap.getMember();
        if (memberUUIDsWithDocs != null) {
            final Set<String> unknownMembersWithDocs = Sets.difference(memberUUIDsWithDocs, currentMemberUUIDs);
            unknownMembersWithDocs.stream().forEach(m -> {
                try {
                    final UUID cleanupNode = UUID.fromString(m);
                    final RT2NodeUuidType cleanupNodeUUID = new RT2NodeUuidType(cleanupNode);

                    if (tryLockForMember(cleanupNodeUUID)) {
                        log.info("Removing docOnNodeMap entries for unknown member com.openexchange.rt2.backend.uid {}", m);
                        final Set<String> docsOnUnknownNode = aDocOnNodeMap.getDocsOfMember(m);
                        docsOnUnknownNode.stream().forEach(d -> {
                            RT2DocUidType docUid = new RT2DocUidType(d);
                            removeDocOnNodeEntryAndAtomicLongForDocUid(docUid, true);
                        });
                        removeCleanedUpMemberFromHealthMap(cleanupNodeUUID);
                    }
                } catch (IllegalArgumentException e) {
                    log.error("Node uuid detected in DocOnNodeMap with illegal uuid " + m, e);
                } catch (Exception e) {
                    log.error("Exception caught trying to clean-up documents for unknown member com.openexchange.rt2.backend.uid " + m, e);
                }
            });
            // in case there are no unknown members we clear our map to remove obsolete entries
            if (unknownMembersWithDocs.isEmpty()) {
                docOnNodeEntriesToGC.clear();
            }
        }
    }

    //-------------------------------------------------------------------------
    private boolean tryLockForMember(RT2NodeUuidType nodeUuid) {
        boolean result = false;

        final String strNodeUUID = nodeUuid.getValue().toString();

        try {
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, strNodeUUID);

            boolean locked = false;
            if (shouldThisNodeTryToTakeOverOwnershipForNotMemberNode(nodeUuid)) {
                RT2NodeHealthState nodeHealthState = nodeHealthMap.get(strNodeUUID);
                final String nodeHealthMapName = nodeHealthMap.getUniqueMapName();

                final DistributedMap<String, PortableNodeHealthState> hzNodeHealthMap = cachingFacade.getDistributedMap(nodeHealthMapName, String.class, PortableNodeHealthState.class);

                try {
                    int nRetryCount = 2;
                    while ((nRetryCount > 0) && !locked) {
                        locked = hzNodeHealthMap.tryLock(nodeUuid.getValue().toString(), 1000, TimeUnit.MILLISECONDS);

                        if (locked) {
                            final String thisMemberUuidString = clusterService.getLocalMemberUuid();
                            final String cleanupUUID = (null != nodeHealthState) ? nodeHealthState.getCleanupUUID(): null;
                            if (nodeHealthState == null) {
                                nodeHealthState = new RT2NodeHealthState(strNodeUUID, "Unknown", RT2NodeHealth.RT2_NODE_HEALTH_NOT_MEMBER_ANYMORE, RT2NodeHealth.RT2_NODE_TYPE_LITE_MEMBER, thisMemberUuidString);
                                nodeHealthMap.set(strNodeUUID, nodeHealthState);
                                result = true;
                            } else if (StringUtils.isEmpty(cleanupUUID) || !clusterService.isActiveHzMember(cleanupUUID)) {
                                nodeHealthState.setCleanupUUID(thisMemberUuidString);
                                result = true;
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.warn("RT2GarbageCollector caught InterruptedException while trying to handle document entries in DocOnNodeMap with unknown " + MDCEntries.BACKEND_UID + " " + strNodeUUID, e);
                } catch (Exception e) {
                    log.warn("RT2GarbageCollector caught exception  while trying to handle document entries in DocOnNodeMap with unknown " + MDCEntries.BACKEND_UID + " " + strNodeUUID, e);
                } finally {
                    if (locked)
                        hzNodeHealthMap.unlock(strNodeUUID);
                }
            }
        } catch (Exception e) {
            log.warn("Trying analyze health map entry or try to lock for node " + MDCEntries.BACKEND_UID + " " + strNodeUUID + " failed with exception", e);
        } finally {
            MDC.remove(MDCEntries.BACKEND_UID);
        }

        return result;
    }

    //-------------------------------------------------------------------------
    private boolean shouldThisNodeTryToTakeOverOwnershipForNotMemberNode(RT2NodeUuidType notMemberUuid) {
        boolean tryToTakeOver = false;

        try {
            final LocalDateTime now = LocalDateTime.now();
            final LocalDateTime lastTime = docOnNodeEntriesToGC.get(notMemberUuid);

            if (lastTime != null) {
                if ((lastTime.plusSeconds(minWaitTimeBeforeGcAtomicLong).isBefore(now))) {
                    docOnNodeEntriesToGC.remove(notMemberUuid);
                    tryToTakeOver = true;
                }
            } else {
                final RT2NodeHealthState nodeHealthState = nodeHealthMap.get(notMemberUuid.toString());
                if (nodeHealthState != null) {
                    final String nodeState = nodeHealthState.getState();
                    final String cleanupUUID = nodeHealthState.getCleanupUUID();
                    if ((lastTime == null) && ((RT2NodeHealth.RT2_NODE_HEALTH_SHUTTING_DOWN.equals(nodeState)) ||
                        StringUtils.isEmpty(cleanupUUID) || !clusterService.isActiveHzMember(cleanupUUID))) {
                        docOnNodeEntriesToGC.put(notMemberUuid, now);
                        log.debug("RT2GarbageCollector marked node com.openexchange.rt2.backend.uid {} for gc as node is not member and health state does not indicate that a clean-up process is running", notMemberUuid);
                    }
                } else {
                    docOnNodeEntriesToGC.put(notMemberUuid, now);
                    log.debug("RT2GarbageCollector marked node com.openexchange.rt2.backend.uid {} for gc as node is not member and there is no health state available", notMemberUuid);
                }
            }
        } catch (Exception e) {
            log.warn("Trying analyze health map entry for clean-up or try to lock for node com.openexchange.rt2.backend.uid " + notMemberUuid.toString() + " failed with exception", e);
        }

        return tryToTakeOver;
    }

    //-------------------------------------------------------------------------
    private void removeDocOnNodeEntryAndAtomicLongForDocUid(RT2DocUidType docUid, boolean destroyRefCountGlobally) {
        try {
            RT2DocOnNodeMap aDocOnNodeMap = docOnNodeMap;
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
            log.info("Removing docOnNodeMap entry with key com.openexchange.rt2.document.uid {}", docUid.getValue());
            ClusterLock clusterLock = clusterLockService.getLock(docUid);
            try {
                clusterLock.lock();
                if (aDocOnNodeMap.remove(docUid.getValue()) == null) {
                    log.error("Cannot remove entry with com.openexchange.rt2.document.uid {} from DocOnNodeMap", docUid.getValue());
                }
                docOnNodeMapEntryRemovedCounter.inc();
                if (!distributedDocInfoMap.freeDocInfos(docUid)) {
                    log.warn("No RefCount4Clients for document with com.openexchange.rt2.document.uid {} found!", docUid.getValue());
                }
            } catch (ClusterLockException e) {
                // Ignore, try again next time
            } finally {
                clusterLock.unlock();
            }
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
        }
    }

    //-------------------------------------------------------------------------
    private void removeCleanedUpMemberFromHealthMap(final RT2NodeUuidType nodeUuToCleanup) throws OXException {
        log.info("Removing unknown groupware member com.openexchange.rt2.backend.uid {} from NodeHealthMap", nodeUuToCleanup.toString());
        nodeHealthMap.remove(nodeUuToCleanup.toString());
    }

    //-------------------------------------------------------------------------
    private Set<RT2DocProcessor> retrieveDocProcessorsWithoutClients (final Set<WeakReference<RT2DocProcessor>> aLocalDocProcessors)
    {
        return aLocalDocProcessors.stream()
                                  .map(WeakRefUtils::getHardRef)
                                  .filter(Objects::nonNull)
                                  .filter(docProc -> CollectionUtils.isEmpty(docProc.getClientsInfo()))
                                  .collect(Collectors.toSet());
    }

    private class FinalRt2DocProcessorRemoveThread implements Runnable {

    	private final Set<RT2DocUidType> possibleDocProcessorToRemove;

    	public FinalRt2DocProcessorRemoveThread(Set<RT2DocUidType> possibleDocProcessorToRemove) {
    		this.possibleDocProcessorToRemove = possibleDocProcessorToRemove;
    	}

		@Override
		public void run() {
			final Set<WeakReference<RT2DocProcessor>> aLocalDocProcessors = docProcMngr.getWeakReferenceToDocProcessors();
			final Set<RT2DocProcessor> aDocProcessorsWOClients = retrieveDocProcessorsWithoutClients(aLocalDocProcessors);
			for (RT2DocProcessor docProc : aDocProcessorsWOClients) {
				if (possibleDocProcessorToRemove.contains(docProc.getDocUID())) {
	                final RT2DocUidType docUID = docProc.getDocUID();
	                distributedDocInfoMap.freeDocInfos(docUID);
					docProcMngr.docProcessorDisposed(docProc);
					MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUID.getValue());
					log.info("Removed doc processor with com.openexchange.rt2.document.uid {} because there is no client for the last 5 minutes.", docUID);
					MDC.remove(MDCEntries.DOC_UID);
					docProcRemovedCounter.inc();
				}
			}
		}
    }
}
