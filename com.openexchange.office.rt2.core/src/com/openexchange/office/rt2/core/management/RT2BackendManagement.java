/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.management;

import java.lang.ref.WeakReference;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.jms.JMSException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;
import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.RT2DocProcessorExistsTester;
import com.openexchange.office.rt2.core.RT2GarbageCollector;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessor;
import com.openexchange.office.rt2.core.doc.DocProcessorClientInfo;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.drive.DriveEventsListenerService;
import com.openexchange.office.rt2.core.drive.MovedDeletedFilesAdminTaskProcessor;
import com.openexchange.office.rt2.core.exception.RT2InvalidDocumentIdentifierException;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndACKProcessor;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.core.sequence.QueueProcessorDisposer;
import com.openexchange.office.rt2.core.sequence.QueueProcessorDisposer.MessageQueuesDispatcherInfo;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;
import com.openexchange.office.tools.jms.EnhActiveMQSSLConnectionFactory;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedLock;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.sessiond.SessiondService;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;
import com.udojava.jmx.wrapper.JMXBeanOperation;
import com.udojava.jmx.wrapper.JMXBeanParameter;
import ch.qos.logback.classic.spi.ILoggingEvent;

@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office.rt2:name=BackendMonitoring")
public class RT2BackendManagement {

    private static final Logger log = LoggerFactory.getLogger(RT2BackendManagement.class);

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    //------------------------Services-----------------------------------------
    @Autowired
    private RT2DocProcessorManager rt2DocProcessorManager;

    @Autowired
    private RT2DocProxyRegistry rt2DocRegistry;

    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;

    @Autowired
    private RT2GarbageCollector garbageCollector;

    @Autowired
    private QueueProcessorDisposer queueProcessorDisposer;

    @Autowired
    private RT2DocProcessorExistsTester docProcExistsTester;

    @Autowired
    private ClusterLockService clusterLockService;

    @Autowired
    private DocProcessorMessageLoggerService msgLoggerService;

	@Autowired
	private MsgBackupAndAckProcessorService msgBackupAndAckProcessorService;

	@Autowired
	private RT2ConfigService rt2ConfigService;

    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;

    @Autowired
    private ClientEventService clientEventService;

    @Autowired
    private SpecialLogService specialLogService;

    @Autowired
    private RT2NodeInfoService nodeInfoService;

    @Autowired
    private SessiondService sessiondService;

    @Autowired
    private CachingFacade cachingFacade;

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private MovedDeletedFilesAdminTaskProcessor movedDeletedFilesService;

    @Autowired
    private DriveEventsListenerService driveEventsListenerService;

    @Autowired
    private DocProxyMessageLoggerService docProxyMessageLoggerService;

    @JMXBeanAttribute(name="CountSessionInvalidNotificationsReceived")
    public long getNumberOfSessionInvalidNotificationsReceived() {
    	return sessionService.getCountOfSessionInvalidNotifications();
    }

    @JMXBeanAttribute(name="LastSessionInvalidNotificationsReceived")
    public List<String> getLastSessionInvalidNotificationsReceived() {
    	return sessionService.getLatestStoredSessionInvalidated();
    }

    @JMXBeanOperation
    public void removeClientUidFromDocProc(@JMXBeanParameter(name="clientUid") String clientUid, @JMXBeanParameter(name="docUid") String docUid) {
    	DocProcessor docProcessor = (DocProcessor) rt2DocProcessorManager.getDocProcessor(new RT2DocUidType(docUid));
    	if (docProcessor != null) {
    		docProcessor.removeUser(new RT2CliendUidType(clientUid));
    		clientEventService.notifyClientRemoved(new ClientEventData(new RT2DocUidType(docUid), new RT2CliendUidType(clientUid)));
    	}
    }

    @JMXBeanAttribute(name="HazelcastUid")
	public String getHazelcastUid() {
    	return clusterService.getLocalMemberUuid();
	}

    @JMXBeanOperation
    public Set<String> getClientsOfDocUid(@JMXBeanParameter(name="docUid") String docUid) {
    	return distributedDocInfoMap.getClientsOfDocUid(new RT2DocUidType(docUid));
    }

    @JMXBeanOperation
    public String getStatusOfDocUid(@JMXBeanParameter(name="docUid") String docUid) {
    	return distributedDocInfoMap.getStatus(new RT2DocUidType(docUid)).name();
    }

	@JMXBeanAttribute(name="RT2DocProxies")
	public Map<String, String> getRT2DocProxies() {
        Map<String, String> res = new HashMap<>();
        for (RT2DocProxy docProxy : rt2DocRegistry.listAllDocProxies()) {
            StringBuilder strBuilderValue = new StringBuilder();
            strBuilderValue.append("docUid: ");
            strBuilderValue.append(docProxy.getDocUID());
            strBuilderValue.append(", clientUid: ");
            strBuilderValue.append(docProxy.getClientUID());

            StringBuilder strBuilderKey = new StringBuilder();
            strBuilderKey.append("proxyId: ");
            strBuilderKey.append(docProxy.getProxyID());

            res.put(strBuilderKey.toString(), strBuilderValue.toString());
        }
        return res;
    }

	@JMXBeanAttribute(name="RT2DocProxyIds")
    public List<String> getRT2DocProxyIds() {
        List<String> res = new ArrayList<>();
        for (RT2DocProxy docProxy : rt2DocRegistry.listAllDocProxies()) {
            res.add(docProxy.getProxyID());
        }
        return res;
    }

	@JMXBeanAttribute(name="RT2DocProxyLogInfoStates")
	public List<String> getRT2DocProxyLogInfoStates() {
	    return docProxyMessageLoggerService.getLogInfoMsgsStates();
	}

	@JMXBeanAttribute(name="CountRT2DocProxies")
    public int getCountRT2DocProxies() {
        return getRT2DocProxies().size();
    }

	@JMXBeanOperation(description="Sets the offline time of a doc proxy - use only for testing purpose")
	public void setOfflineTimeOfRT2DocProxy(@JMXBeanParameter(name="clientUid") String clientUid, @JMXBeanParameter(name="docUid") String docUid, @JMXBeanParameter(name="offlineTime") long offlineTime) {
		String id = RT2DocProxy.calcID(new RT2CliendUidType(clientUid.trim()), new RT2DocUidType(docUid.trim()));
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(id);
		if (docProxy != null) {
			docProxy.setOfflineTimeInMS(offlineTime);
		}
	}

	@JMXBeanOperation(description="Determines if a doc processor instance is running on this node")
	public boolean isActiveDocProcessorOnThisNode(@JMXBeanParameter(name="docUid") String docUid) {
        return rt2DocProcessorManager.getWeakReferenceToDocProcessors()
            .stream()
            .map(w ->  WeakRefUtils.getHardRef(w))
            .filter(Objects::nonNull)
            .filter(d -> d.getDocUID().getValue().equals(docUid))
            .count() > 0;
	}

	@JMXBeanAttribute(name="DocProcessors")
    public Map<String, String> getDocProcessors() {
        Map<String, String> res = new HashMap<>();
        for (WeakReference<RT2DocProcessor> ref : rt2DocProcessorManager.getWeakReferenceToDocProcessors()) {
            RT2DocProcessor docProc = ref.get();
            if (docProc != null) {
                StringBuilder strBuilderKey = new StringBuilder();
                strBuilderKey.append("com.openexchange.rt2.document.uid ");
                strBuilderKey.append(docProc.getDocUID());

                StringBuilder strBuilderValue = new StringBuilder();
                for (DocProcessorClientInfo clientInfo : docProc.getClientsInfo()) {
                    strBuilderValue.append(", ");
                    strBuilderValue.append(clientInfo.toString());
                }
                res.put(strBuilderKey.toString(), strBuilderValue.toString());
            }
        }
        return res;
    }

	@JMXBeanAttribute(name="DocProcessorIds")
    public List<String> getDocProcessorIds() {
        List<String> res = new ArrayList<>();
        for (WeakReference<RT2DocProcessor> ref : rt2DocProcessorManager.getWeakReferenceToDocProcessors()) {
            RT2DocProcessor docProc = ref.get();
            if (docProc != null) {
                res.add(docProc.getDocUID().getValue());
            }
        }
        return res;
    }

	@JMXBeanAttribute(name="CountRT2DocProcessors")
    public int getCountRT2DocProcessors() {
        return getDocProcessors().size();
    }

	@JMXBeanOperation(description="Clears the docRegistry, docInfoRegistry and docProcessorManagerRegistry")
    public void clearAll() {
        rt2DocRegistry.clear();
        distributedDocInfoMap.clear();
        rt2DocProcessorManager.getWeakReferenceToDocProcessors().forEach(r -> rt2DocProcessorManager.docProcessorDisposed(r.get()));
    }

	@JMXBeanAttribute(name="GCTimeout")
    public long getGCTimeout() {
        return rt2ConfigService.getRT2GCOfflineTresholdInMS();
    }

	@JMXBeanAttribute(name="GCFrequency")
    public long getGCFrequency() {
        return rt2ConfigService.getRT2GCFrequencyInMS();
    }

	@JMXBeanOperation
    public void setGCTiming(@JMXBeanParameter(name="timeout") Long timeout, @JMXBeanParameter(name="frequency") Long frequency) {
    	rt2ConfigService.setRT2GCOfflineTresholdInMS(timeout);
    	rt2ConfigService.setRT2GCFrequencyInMS(frequency);

        try {
        	garbageCollector.destroy();
        	garbageCollector.afterPropertiesSet();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            log.error("Exception caught trying to change garbage collector timing", e);
        }
    }

	@SuppressWarnings("unused")
    @JMXBeanAttribute(name="DcsConnectionUrl")
	public String getDcsConnectionUrl() throws JMSException {
		return ((ActiveMQConnectionFactory) pooledConnectionFactoryProxy.getPooledConnectionFactory().getConnectionFactory()).getBrokerURL();
	}

	@JMXBeanAttribute(name="StatusMsgsWaitingForAckOfDoc")
	public Map<String, String> getStatusMsgsWaitingForAckOfDoc() {
		return msgBackupAndAckProcessorService.getStatusMsgsWaitingForAckOfDoc();
	}

	@JMXBeanAttribute(name="DetectedClientsForHangup")
	public Set<String> getDetectedClientsForHangup() {
		return msgBackupAndAckProcessorService.getHangupClients();
	}

	@JMXBeanAttribute(name="CountQueuedMessagesOfProcessor")
	public int getCountQueuedMessagesOfProcessor() {
		return queueProcessorDisposer.getMessageQueueSize();
	}

	@JMXBeanAttribute(name="CountCurrentUsedProcessors")
	public int getCountCurrentUsedProcessors() {
		return queueProcessorDisposer.getCurrentUsedDocProcessors();
	}

	@JMXBeanOperation
	public void triggerRT2GC() {
		garbageCollector.doGC();
	}

	@JMXBeanAttribute(name="DocProxyMarkedForRemove")
	public Set<String> getDocProxyMarkedForRemove() {
		return docProcExistsTester.getMarkedForRemove().stream().map(p -> p.getValue()).collect(Collectors.toSet());
	}

	@JMXBeanOperation
	public boolean removeDocProxy(@JMXBeanParameter(name="clientUid") String clientUid, @JMXBeanParameter(name="docUid") String docUid) {
		String id = RT2DocProxy.calcID(new RT2CliendUidType(clientUid.trim()), new RT2DocUidType(docUid.trim()));
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(id);
		if (docProxy != null) {
			rt2DocRegistry.deregisterDocProxy(docProxy, true);
			return true;
		}
		return false;
	}

	@JMXBeanOperation
	public void setThrowExceptionOnLeaveAtDocProxy(@JMXBeanParameter(name="docProxyId") String docProxyId, @JMXBeanParameter(name="value") boolean value) {
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(docProxyId.trim());
		if (docProxy != null) {
			docProxy.setThrowExceptionOnLeave(value);
		}
	}

	@JMXBeanOperation
	public List<String> getMsgsOfDocUid(@JMXBeanParameter(name="docUid") String docUid) {
		List<ILoggingEvent> loggingEvents = new ArrayList<>(specialLogService.getLoggingEventsForDocUid(docUid));
		Collections.sort(loggingEvents, (ILoggingEvent o1, ILoggingEvent o2) -> Long.compare(o1.getTimeStamp(), o2.getTimeStamp()));
		return loggingEvents.stream().map(e -> e.getFormattedMessage()).collect(Collectors.toList());
	}

	@JMXBeanOperation
	public boolean removeDocProxy(@JMXBeanParameter(name="docProxyId") String docProxyId) {
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(docProxyId.trim());
		if (docProxy != null) {
			rt2DocRegistry.deregisterDocProxy(docProxy, true);
			return true;
		}
		return false;
	}

	@JMXBeanOperation
	public boolean removeDocProcessor(@JMXBeanParameter(name="docUid") String docUid) {
		RT2DocProcessor docProc = rt2DocProcessorManager.getDocProcessor(new RT2DocUidType(docUid.trim()));
		if (docProc != null) {
			rt2DocProcessorManager.docProcessorDisposed(docProc);
			return true;
		}
		return false;
	}

	@JMXBeanOperation
	public boolean removeDocOnNodeMapEntry(@JMXBeanParameter(name = "docUid") String docUid) {
	    if (StringUtils.isNotEmpty(docUid)) {
	        final RT2DocUidType docUidToRemove = new RT2DocUidType(docUid);
	        ClusterLock clusterLock = clusterLockService.getLock(docUidToRemove);
	        try {
	            clusterLock.lock();
	            return (nodeInfoService.deregisterDocOnNode(docUidToRemove) != null);
            } catch (ClusterLockException e) {
	            // Ignore exception the result will be false
	        } finally {
	            clusterLock.unlock();
	        }
	    }
        return false;
	}

    @JMXBeanOperation
    public boolean setDocOnNodeMapEntry(@JMXBeanParameter(name = "docUid") String docUid, @JMXBeanParameter(name = "nodeUuid") String nodeUUID) {
        if (StringUtils.isNotEmpty(docUid) && StringUtils.isNotEmpty(nodeUUID)) {
            UUID checkedNodeUUID = null;
            try {
                checkedNodeUUID = UUID.fromString(nodeUUID);
            } catch (IllegalArgumentException e) {
                return false;
            }

            final RT2DocUidType docUidToRemove = new RT2DocUidType(docUid);
            ClusterLock clusterLock = clusterLockService.getLock(docUidToRemove);
            try {
                clusterLock.lock();
                nodeInfoService.setDocOnNode(new RT2DocUidType(docUid), new RT2NodeUuidType(checkedNodeUUID));
                return true;
            } catch (ClusterLockException e) {
                // Ignore exception the result will be false
            } finally {
                clusterLock.unlock();
            }
        }

        return false;
    }

	@JMXBeanOperation
	public List<String> getMsgsOfDocProxy(@JMXBeanParameter(name="clientUid") String clientUid, @JMXBeanParameter(name="docUid") String docUid) {
		String id = RT2DocProxy.calcID(new RT2CliendUidType(clientUid.trim()), new RT2DocUidType(docUid.trim()));
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(id);
		if (docProxy != null) {
			return docProxy.formatMsgsLogInfo();
		}
		return Arrays.asList("Not found!");
	}

	@JMXBeanOperation
	public List<String> getMsgsOfDocProxy(@JMXBeanParameter(name="proxyId") String proxyId) {
		RT2DocProxy docProxy = rt2DocRegistry.getDocProxy(proxyId.trim());
		if (docProxy != null) {
			return docProxy.formatMsgsLogInfo();
		}
		return Arrays.asList("Not found!");
	}

	@JMXBeanOperation
	public List<String> getMsgsOfDocProcessor(@JMXBeanParameter(name="docUid") String docUid) {
		return msgLoggerService.formatMsgsLogInfo(new RT2DocUidType(docUid));
	}

	@JMXBeanOperation
	public List<String> getMsgsOfDocProcessor(@JMXBeanParameter(name="clientUid") String clientUid, @JMXBeanParameter(name="docUid") String docUid) {
		return msgLoggerService.formatMsgsLogInfoForClient(new RT2DocUidType(docUid), new RT2CliendUidType(clientUid));
	}

	@JMXBeanOperation
	public void setQueueSizeForMsgsOfDocProcessor(@JMXBeanParameter(name="docUid") String docUid, @JMXBeanParameter(name="count") int count) {
		msgLoggerService.resetQueueSizeForLogInfo(new RT2DocUidType(docUid), count);
	}

	@JMXBeanAttribute(name="CurrentJmsConnections")
	public Set<String> getCurrentJmsConnections() throws Exception {
		Set<String> res = new HashSet<>();
		EnhActiveMQSSLConnectionFactory amqConnFact = (EnhActiveMQSSLConnectionFactory) pooledConnectionFactoryProxy.getPooledConnectionFactory().getConnectionFactory();
		amqConnFact.getConnections().forEach(c -> {
			try {
				res.add("ClientId: " + c.getClientID().toString() + ", BrokerId: " + c.getBrokerInfo().getBrokerId() + ", BrokerUrl: " + c.getBrokerInfo().getBrokerURL() + ", BrokerName: " + c.getBrokerInfo().getBrokerName());
			} catch (JMSException e) {
				log.info(e.getMessage());
			}
		});
		return res;
	}

	@JMXBeanAttribute(name="MessageProcessorRunningThreadTime")
	public Map<String, String> getMessageProcessorRunningThreadTime() {
		Map<String, String> res = new HashMap<>();
		queueProcessorDisposer.getMessageQueuesDispatcherInfos().stream().forEach(m -> res.put(m.getDocUid().toString(), m.getRunningtime().toString()
	            .substring(2)
	            .replaceAll("(\\d[HMS])(?!$)", "$1 ")
	            .toLowerCase()));
		return res;
	}

	@JMXBeanOperation
	public boolean terminateMessageProcessorThread(@JMXBeanParameter(name="docUid") String docUid) {
		Set<MessageQueuesDispatcherInfo> messageQueuesDispatcherInfos = queueProcessorDisposer.getMessageQueuesDispatcherInfos();
		for (MessageQueuesDispatcherInfo info : messageQueuesDispatcherInfos) {
			if (info.getDocUid().getValue().equals(docUid)) {
				info.interrupt();
				return true;
			}
		}
		return false;
	}

	@JMXBeanOperation
	public List<String> getStackTraceOfMessageProcessorThread(@JMXBeanParameter(name="docUid") String docUid) {
		List<String> res = new ArrayList<>();
		Set<MessageQueuesDispatcherInfo> messageQueuesDispatcherInfos = queueProcessorDisposer.getMessageQueuesDispatcherInfos();
		for (MessageQueuesDispatcherInfo info : messageQueuesDispatcherInfos) {
			if (info.getDocUid().getValue().equals(docUid)) {
				StackTraceElement [] stackTraceArray = info.getStacktraceOfThread();
				for (StackTraceElement ele : stackTraceArray) {
					res.add(ele.toString());
				}
			}
		}
		return res;
	}

	@JMXBeanAttribute(name="AllHazelcastRT2LockObjects")
    public List<String> getAllHazelcastRT2LockObjects() {
		Collection<DistributedLock> distributedLocks = cachingFacade.getDistributedLockWithPrefix(ClusterLockService.LOCK_PREFIX);
		List<String> res = distributedLocks.stream().map(l -> l.getName() + ":" + l.getLockCount()).collect(Collectors.toList());
		return res;
	}

	@JMXBeanAttribute(name="RegisteredRT2LockObjects")
    public List<String> getRegisteredRT2LockObjects() {
    	return new ArrayList<>(clusterLockService.getLocks());
    }

	@JMXBeanOperation
	public void unlockRT2LockObject(@JMXBeanParameter(name="docUid") String docUid) {
		DistributedMap<String, String> hzLockMap = cachingFacade.getDistributedMap(ClusterLockService.LOCK_MAP_NAME, String.class, String.class);
		if (hzLockMap.isLocked(ClusterLockService.LOCK_PREFIX + docUid)) {
			hzLockMap.unlock(ClusterLockService.LOCK_PREFIX + docUid);
		}
	}

    @JMXBeanOperation
    public void forceUnlockRT2LockObject(@JMXBeanParameter(name="docUid") String docUid) {
        DistributedMap<String, String> hzLockMap = cachingFacade.getDistributedMap(ClusterLockService.LOCK_MAP_NAME, String.class, String.class);
        if (hzLockMap.isLocked(ClusterLockService.LOCK_PREFIX + docUid)) {
            hzLockMap.forceUnlock(ClusterLockService.LOCK_PREFIX + docUid);
        }
    }

	@JMXBeanOperation
	public void destroyRT2LockObject(@JMXBeanParameter(name="docUid") String docUid) {
		unlockRT2LockObject(docUid);
		DistributedMap<String, String> hzLockMap = cachingFacade.getDistributedMap(ClusterLockService.LOCK_MAP_NAME, String.class, String.class);
		hzLockMap.remove(ClusterLockService.LOCK_PREFIX + docUid);
	}

	@JMXBeanOperation
	public void runClusterLockUnlockThread() {
		// Obsolete as we now use locks with lease time which are
	    // handled by the current lock implementation. Method is
	    // still available to be compatible with older versions.
	}

	@JMXBeanOperation
	public void runDocLockRemoveThread() {
		clusterLockService.runDocLockRemoveThread();
	}

	@JMXBeanOperation
	public Map<String, Integer> getNotAckedMessageCount(@JMXBeanParameter(name="docUid") String docUid) {
		Map<String, Integer> res = new HashMap<>();
		Optional<MsgBackupAndACKProcessor> msgBackupAndAckProc;
		try {
				msgBackupAndAckProc = msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(new RT2DocUidType(docUid), false);
			if (msgBackupAndAckProc.isPresent()) {
				msgBackupAndAckProc.get().getClientStatesMsgBackupSize().entrySet().stream().forEach(p -> res.put(p.getKey().getValue(), p.getValue()));
			}
		} catch (RT2InvalidDocumentIdentifierException e) {
			// Could not be thrown when second parameter of getMsgBackupAndACKProcessor is false
		}
		return res;
	}

	@JMXBeanAttribute(name="NackFrequenceOfServer")
	public Integer getNackFrequenceOfServer() {
		return rt2ConfigService.getNackFrequenceOfServer();
	}

	@JMXBeanOperation
	public void updateNackFrequenceOfServer(@JMXBeanParameter(name="newNackFrequency") Integer value) {
		rt2ConfigService.updateNackFrequenceOfServer(value);
	}

    @JMXBeanOperation
    public boolean removeClientSession(@JMXBeanParameter(name = "sessionId") String sessionId) {
        return sessiondService.removeSession(sessionId);
    }

	@JMXBeanAttribute(name="AtomicLongsToGc")
	public List<String> getAtomicLongsToGc() {
		Map<RT2DocUidType, LocalDateTime> atomicLongsToGc = garbageCollector.getAtomicLongsToGc();
		final List<String> res = new ArrayList<>();
		atomicLongsToGc.forEach((k, v) -> {
			res.add("com.openexchange.rt2.document.uid " + k.getValue() + ", date: " + formatter.format(v));
		});
		return res;
	}

	@JMXBeanAttribute(name="AtomicLongsToBeVerified")
	public List<String> getAtomicLongsToBeVerified() {
		Set<RT2DocUidType> atomicLongsToBeVerified = garbageCollector.getAtomicLongsToBeVerified();
		final List<String> res = new ArrayList<>();
		atomicLongsToBeVerified.forEach(t -> {
			res.add(t.getValue());
		});
		return res;
	}

	@JMXBeanAttribute(name="DriveEventsForDocsProcessed")
	public List<String> getDriveEventsForDocsProcessed() {
	    return movedDeletedFilesService.getProcessedMovedDeletedTasks();
	}

    @JMXBeanAttribute(name="DriveEventsReceived")
    public long getDriveEventsReceived() {
        return driveEventsListenerService.getDriveEventsReceived();
    }

}
