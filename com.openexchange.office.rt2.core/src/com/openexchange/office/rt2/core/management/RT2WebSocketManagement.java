/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2SessionCountValidator;
import com.openexchange.office.rt2.core.ws.RT2WSApp;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;
import com.udojava.jmx.wrapper.JMXBeanOperation;
import com.udojava.jmx.wrapper.JMXBeanParameter;

@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office.rt2:name=WebsocketMonitoring")
public class RT2WebSocketManagement { 

	@Autowired
	private RT2WSChannelDisposer channelDisposer;
	
	@Autowired
	private RT2WSApp wsApp;
	
	@Autowired
	private RT2SessionCountValidator sessionCountValidator;
	
	@JMXBeanAttribute(name="AllActiveWSChannels")
	public List<String> getAllActiveWSChannels() {
		Set<RT2ChannelId> channelIds = wsApp.getCurrentActiveWebsocketIds();
		List<String> res = new ArrayList<>();		
		channelIds.forEach(id -> res.add(id.getId()));
		return res;
	}

	@JMXBeanOperation
	public List<String> getClientUidsOfChannel(@JMXBeanParameter(name="channelId") String channelId) {
		Set<RT2CliendUidType> clientUids = channelDisposer.getClientsAssociatedToChannel(new RT2ChannelId(channelId));
		List<String> res = new ArrayList<>();
		clientUids.forEach(c -> res.add(c.getValue()));
		return res;		
	}

	@JMXBeanAttribute(name="ReponsesQueueSize")
	public Integer getReponsesQueueSize() {
		return channelDisposer.getResponseQueueSize();
	}

	@JMXBeanAttribute(name="DetailInfoAboutAllActiveWSChannels")
	public List<String> getDetailInfoAboutAllActiveWSChannels() {
		List<String> res = new ArrayList<>();
		Map<RT2CliendUidType, Pair<RT2DocUidType, RT2ChannelId>> channels = channelDisposer.getChannels();
		channels.forEach((k, v) -> {
			res.add(v.getRight().getId() + "_" + v.getLeft().getValue() + "_" + k.getValue());
		});
		return res;
	}

	@JMXBeanOperation
	public void terminateAllWsConnections() {
		wsApp.terminateAllWsConnections();
	}
	
	@JMXBeanAttribute(name="DetailInfoAboutAllActiveWSChannelSize")
	public Integer getDetailInfoAboutAllActiveWSChannelSize() {
		return channelDisposer.getChannels().size();
	}
	
	@JMXBeanAttribute(name="AllOfflineWSChannels")
	public List<String> getAllOfflineWSChannels() {
		final List<String> res = new ArrayList<>();
		final Map<RT2ChannelId, Long> channelsOffline = channelDisposer.getChannelsOffline();
		final long now = System.currentTimeMillis();
		channelsOffline.forEach((k, v) -> {
			final long diff = (v == null) ? 0 :  ((now - v) / 1000);
			res.add(k + "=" + diff + "s");
		});
		return res;
	}
	
	@JMXBeanAttribute(name="CountConnectionsOfUsers")
	public Map<Integer, Integer> getCountConnectionsOfUsers() {
		return sessionCountValidator.getCountSessionsOfUsers();
	}
}
