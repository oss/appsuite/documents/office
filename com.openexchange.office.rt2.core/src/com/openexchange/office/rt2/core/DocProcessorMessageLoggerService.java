/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.google.common.collect.EvictingQueue;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo.Direction;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;

@Service
public class DocProcessorMessageLoggerService implements IDocProcessorEventListener {
	
	private static final int DEFAULT_COUNT_MESSAGES_STORED = 20;
	
	private ConcurrentHashMap<RT2DocUidType, Collection<RT2DocProcessorLogInfo>> msgs = new ConcurrentHashMap<>(); //Collections.synchronizedCollection(EvictingQueue.create(DEFAULT_COUNT_MESSAGES_STORED));
	
	//-------------------------------------------------------------------------
    public void addMessageForQueueToLog(Direction direction, final RT2Message msg) {
    	if (msg != null) {
    		getMsgsOfDocProc(msg.getDocUID()).add(new RT2DocProcessorLogInfo(direction, msg));
    	}
    }

    //-------------------------------------------------------------------------	
	public void resetQueueSizeForLogInfo(RT2DocUidType docUid, int count) {
		if (msgs.computeIfAbsent(docUid, k -> {return Collections.synchronizedCollection(EvictingQueue.create(count));}) != null) {
			msgs.computeIfPresent(docUid, (k, v) -> {
				final Collection<RT2DocProcessorLogInfo> newQueue = Collections.synchronizedCollection(EvictingQueue.create(count));
				newQueue.addAll(v);
				return newQueue;
			});
		}
	}
	
    //-------------------------------------------------------------------------
    public List<String> formatMsgsLogInfo(RT2DocUidType docUid) {
    	List<RT2DocProcessorLogInfo> res = getMsgsOfDocProcAsList(docUid);
    	return res.stream().map(m -> m.toString()).collect(Collectors.toList());
    }

    //-------------------------------------------------------------------------
    public List<String> formatMsgsLogInfoForClient(RT2DocUidType docUid, RT2CliendUidType clientUid) {
    	List<RT2DocProcessorLogInfo> res = getMsgsOfDocProcAsList(docUid);
    	return res.stream().filter(m -> (m.getClientUid() != null) && m.getClientUid().equals(clientUid)) 
    			            .map(m -> m.toString())
    			            .collect(Collectors.toList());
    }
    
    //-------------------------------------------------------------------------
    public List<RT2LogInfo> getMsgsAsList(RT2DocUidType docUid) {
    	List<RT2LogInfo> res = new ArrayList<>();
    	Collection<RT2DocProcessorLogInfo> logList = getMsgsOfDocProc(docUid);
    	if (logList != null) {
    		res.addAll(logList);
    	}
    	return res;
    }

    //-------------------------------------------------------------------------
    public void add(RT2DocUidType docUid, Direction direction, String msgType, RT2CliendUidType clientUid) {
    	RT2DocProcessorLogInfo docProcessorLogInfo = new RT2DocProcessorLogInfo(direction, msgType, clientUid, docUid);
    	getMsgsOfDocProc(docUid).add(docProcessorLogInfo);
    }
        
	//-------------------------------------------------------------------------    
    @Override
	public void docCreated(RT2DocUidType docUid) {
    	getMsgsOfDocProc(docUid);		
	}

	//-------------------------------------------------------------------------    
	@Override
	public void docDisposed(RT2DocUidType docUid) {
		msgs.remove(docUid);
	}

    //-------------------------------------------------------------------------
	private List<RT2DocProcessorLogInfo> getMsgsOfDocProcAsList(RT2DocUidType docUid) {
		List<RT2DocProcessorLogInfo> res = new ArrayList<>();
    	Collection<RT2DocProcessorLogInfo> logList = getMsgsOfDocProc(docUid);
    	if (logList != null) {
    		res.addAll(logList);
    	}
    	Collections.sort(res);
		return res;
	}    

	//-------------------------------------------------------------------------
	private Collection<RT2DocProcessorLogInfo> getMsgsOfDocProc(RT2DocUidType docUid) {
		Collection<RT2DocProcessorLogInfo> currCol = msgs.get(docUid);
		if (currCol == null) {
			Collection<RT2DocProcessorLogInfo> newCol = Collections.synchronizedCollection(EvictingQueue.create(DEFAULT_COUNT_MESSAGES_STORED));
			Collection<RT2DocProcessorLogInfo> oldCol = msgs.putIfAbsent(docUid, newCol);
			if (oldCol == null) {
				return newCol;
			} // else
			return oldCol; 
		}
		return currCol;
	}
}
