/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

public class RT2Constants 
{
    private RT2Constants() {}

    //-------------------------------------------------------------------------
    public static final long   REF_COUNT_LOCKED           = 999999999;

    //-------------------------------------------------------------------------
    public static final long   REF_COUNT_TRESHHOLD        = 900000000;
    
    //-------------------------------------------------------------------------
    public static final long   IN_REMOVE_REF_COUNT_TRESHHOLD = -900000000;

    //-------------------------------------------------------------------------
    public static final long   CLUSTERLOCK_WAIT_TIME      = 5000; // 5 sec

    //-------------------------------------------------------------------------
    public static final long   CLUSTERLOCK_LEASE_TIME     = 5; // 5 min

    //-------------------------------------------------------------------------
    public static final long   DELAY_GC_LOST_CLIENTS      = 300000; // 5 min

    //-------------------------------------------------------------------------
    public static final long   FREQ_GC_LOST_CLIENTS       = 60000; // 1 min

    //-------------------------------------------------------------------------
    public static final String RT2_CLUSTER_FULL_MEMBER_COUNT = "rt2.active-full-member-count";
        
    //-------------------------------------------------------------------------
    public static final String RT2_CLIENT_COUNTER_MAP = "rt2.client-counter-map";
}
