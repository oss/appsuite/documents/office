/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.cache;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;

@Service
public class ClusterLockService implements InitializingBean, DisposableBean {

    public static final String LOCK_PREFIX = "rt2_";

    public static final String LOCK_MAP_NAME = "rt2DocLockMap";

    private static final Logger log = LoggerFactory.getLogger(ClusterLockService.class);

    @Autowired
    private CachingFacade cachingFacade;

    private DistributedMap<String, String> hzLockMap;

    private final long maxLockTimeInSeconds;

    private final long maxLockExistTimeInSeconds;

    private final ConcurrentHashMap<String, ClusterLock> lockMap = new ConcurrentHashMap<>();

    private final ScheduledExecutorService docLockRemoveScheduledExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder("DocLockRemoveThread-%d").build());

    public ClusterLockService() {
        this.maxLockTimeInSeconds = 60;
        this.maxLockExistTimeInSeconds = 1800;
    }

    public ClusterLockService(CachingFacade cachingFacade, long maxLockTimeInSeconds, long maxLockExistTimeInSeconds) {
        this.cachingFacade = cachingFacade;
        this.maxLockTimeInSeconds = maxLockTimeInSeconds;
        this.maxLockExistTimeInSeconds = maxLockExistTimeInSeconds;
    }

    @Override
    public void afterPropertiesSet() {
        this.hzLockMap = cachingFacade.getDistributedMap(LOCK_MAP_NAME, String.class, String.class);
        docLockRemoveScheduledExecutorService.scheduleAtFixedRate(new DocLockRemoveThread(), 1, 1, TimeUnit.MINUTES);
    }

    @Override
    public void destroy() {
        docLockRemoveScheduledExecutorService.shutdown();
    }

    public long getMaxLockTimeInSec() {
        return this.maxLockTimeInSeconds;
    }

    public long getMaxLockExistsTimeInSec() {
        return this.maxLockExistTimeInSeconds;
    }

    public ClusterLock getLock(RT2DocUidType docUid) {
        String lockName = LOCK_PREFIX + docUid.getValue();
        ClusterLock newLock = new ClusterLock(lockName);
        ClusterLock prevLock = lockMap.putIfAbsent(lockName, newLock);
        return prevLock == null ? newLock : prevLock;
    }

    public Set<String> getLocks() {
        return lockMap.values().stream().map(lock -> lock.toString()).collect(Collectors.toSet());
    }

    private class DocLockRemoveThread implements Runnable {

        @Override
        public void run() {
            try {
                final LocalTime now = LocalTime.now();
                Set<String> toDel = new HashSet<>();
                lockMap.values().forEach(c -> {
                    if (now.minusSeconds(maxLockExistTimeInSeconds).isAfter(c.getLastAccessTime())) {
                        toDel.add(c.getName());
                    }
                });
                lockMap.keySet().removeIf(e -> toDel.contains(e));
            } catch (Exception e) {
                log.error("ClusterLockUnlockThread caught exception", e);
            }
        }
    }

    public class ClusterLock {

        private final String       name;
        private final LocalTime    creationTime;
        private volatile LocalTime lastAccessTime;
        private volatile LocalTime lastLockTime;
        private volatile LocalTime lastUnlockTime;
        private volatile LocalTime lastForceUnlockTime;
        private AtomicLong         lockCount = new AtomicLong(0);

        public ClusterLock(String name) {
            this.name = name;
            this.creationTime = LocalTime.now();
            this.lastAccessTime = LocalTime.now();
        }

        public boolean lock() throws ClusterLockException {
            return lock(10000);
        }

        public boolean lock(long waitTime) throws ClusterLockException {
            return lock(waitTime, 1);
        }

        private boolean lock(long waitTime, int tryIdx) throws ClusterLockException {
            log.debug("Lock with id {}, instance {}", name, this);
            boolean res = false;
            this.lastAccessTime = LocalTime.now();
            try {
                res = (hzLockMap.tryLock(name, waitTime, TimeUnit.MILLISECONDS, maxLockTimeInSeconds, TimeUnit.SECONDS));
                if (res) {
                    lockCount.incrementAndGet();
                    lastLockTime = LocalTime.now();
                }
            } catch (InterruptedException ex) {
                res = false;
            }
            if (!res) {
                if (tryIdx < 3) {
                    log.warn("Wait time {} for lock {} exceeded, tries {}, trying again", waitTime, name, tryIdx);
                    res = lock(waitTime, ++tryIdx);
                } else {
                    log.warn("Wait time {} for lock {} exceeded, tries {}, force unlock", waitTime, name, tryIdx);
                    forceUnlock();
                    res = lock(waitTime, 1);
                }
            }
            return res;
        }

        public void unlock() {
            log.debug("Unlock called with id {}, instance {}", name, this);
            lastAccessTime = lastUnlockTime = LocalTime.now();
            hzLockMap.unlock(name);
            lockCount.decrementAndGet();
        }

        private void forceUnlock() {
            hzLockMap.forceUnlock(name);
            lastAccessTime = lastForceUnlockTime = LocalTime.now();
            lockCount.set(0);
        }

        public boolean isLocked() {
            return hzLockMap.isLocked(name);
        }

        public String getName() {
            return name;
        }

        public long getAssumedLockCount() {
            return lockCount.get();
        }

        public LocalTime getLastAccessTime() {
            return lastAccessTime;
        }

        public LocalTime getLastLockTime() {
            return lastLockTime;
        }

        public LocalTime getLastUnlockTime() {
            return lastUnlockTime;
        }

        public LocalTime getCreationTime() {
            return creationTime;
        }

        public LocalTime getLasttForceUnlockTime() {
            return lastForceUnlockTime;
        }

        @Override
        public String toString() {
            LocalTime localLastLockTime = getLastLockTime();
            LocalTime localLastUnlockTime = getLastLockTime();
            LocalTime localLastAccessTime = getLastUnlockTime();
            LocalTime localForceUnlockTime = getLasttForceUnlockTime();
            long localLockCount = getAssumedLockCount();
            boolean locked = hzLockMap.isLocked(name);
            return "ClusterLock [name=" + name
                 + ", creationTime=" + creationTime
                 + ", nodeLockCount=" + localLockCount
                 + ", lastAccessTime=" + (localLastAccessTime == null ? "<unset>" : localLastAccessTime)
                 + ", lastLockTime=" + (localLastLockTime == null ? "<unset>" : localLastLockTime)
                 + ", lastUnlockTime=" + (localLastLockTime == null ? "<unset>" : localLastUnlockTime)
                 + ", lastForceUnlockTime=" + (localForceUnlockTime == null ? "<unset>" : localForceUnlockTime)
                 + ", isLocked=" + locked + "]";
        }
    }

    public void runDocLockRemoveThread() {
        new DocLockRemoveThread().run();
    }

}
