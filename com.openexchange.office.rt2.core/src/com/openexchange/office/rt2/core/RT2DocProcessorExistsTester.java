/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageTimeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ServerIdType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.service.cluster.ClusterService;

@Service
@Async(initialDelay=5, period=5, timeUnit=TimeUnit.MINUTES)
public class RT2DocProcessorExistsTester implements Runnable {
	
	//---------------------------------Services----------------------------------------------------
	@Autowired
	private RT2JmsMessageSender jmsMessageSender;
	
	@Autowired
	private RT2DocProxyRegistry docProxyRegistry;
	
	@Autowired
	private RT2DocProcessorManager docProcMngr;

	@Autowired
	private ClusterService clusterService;
	
	//---------------------------------------------------------------------------------------------
	private final AtomicLong currentlyRunning = new AtomicLong(0);	
	
	private Map<UUID, Collection<DocUidType>> collectedDocProcs = new HashMap<>();
	
	private Set<RT2DocUidType> markedForRemove = new HashSet<>();
	
	@Override
	public void run() {
		try {
			if (currentlyRunning.compareAndSet(0l, System.currentTimeMillis())) {
				String nodeUuid = clusterService.getLocalMemberUuid();
				collectedDocProcs.clear();
				if (clusterService.getCountMembers() > 1) {
					AdminMessage adminRequest = AdminMessage.newBuilder()
															.setMsgType(AdminMessageType.UPDATE_CURR_DOC_PROCESSORS_REQUEST)
															.setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
															.setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
															.setOriginator(ServerIdType.newBuilder().setValue(nodeUuid))
															.build();
					jmsMessageSender.sendAdminMessage(adminRequest);
				} else {
					processUpdateCurrProcResponse(UUID.fromString(nodeUuid), System.currentTimeMillis(), new HashSet<>());
				}
			} else {
				LocalDateTime started =
					    LocalDateTime.ofInstant(Instant.ofEpochMilli(currentlyRunning.get()), ZoneId.systemDefault());
				LocalDateTime now = LocalDateTime.now();
				Duration duration = Duration.between(now, started);
				if (duration.toMinutes() > 10) {				
					currentlyRunning.set(0l);
					run();
				}
			}
		} finally {
			MDC.clear();
		}
	}
	
	
	public void processUpdateCurrProcResponse(UUID nodeUuid, long msgDate, Collection<DocUidType> docUids) {
		if (msgDate < currentlyRunning.get()) {
			return;
		}		
		if (currentlyRunning.get() > 0l) {
			if (collectedDocProcs.containsKey(nodeUuid)) {
				currentlyRunning.set(0l);
				return;
			}
			if (!docUids.isEmpty()) {
				collectedDocProcs.put(nodeUuid, docUids);
			}
			if (collectedDocProcs.size() == clusterService.getCountMembers() - 1) {
				collectedDocProcs.put(clusterService.getLocalMemberUuidAsUUID(), docProcMngr.getDocProcessors().stream().map(p -> DocUidType.newBuilder().setValue(p.getDocUID().getValue()).build()).collect(Collectors.toSet()));
				Set<RT2DocUidType> currentDocProcessors = new HashSet<>();
				collectedDocProcs.values().forEach(col -> {
					col.forEach(c -> currentDocProcessors.add(new RT2DocUidType(c.getValue())));
				});
				Set<RT2DocUidType> currDocproxyDocUids = docProxyRegistry.listAllDocProxies().stream().map(p -> p.getDocUID()).collect(Collectors.toSet());
				
				currDocproxyDocUids.removeAll(currentDocProcessors);				
								
				markedForRemove.retainAll(currDocproxyDocUids);
				if (!markedForRemove.isEmpty()) {
					List<RT2DocProxy> currentDocProxys = docProxyRegistry.listAllDocProxies();								
					currentDocProxys.forEach(currentDocProxy -> {
						if (markedForRemove.contains(currentDocProxy.getDocUID())) {
							docProxyRegistry.deregisterDocProxy(currentDocProxy, true);
						}
					});
				}
				markedForRemove = currDocproxyDocUids;
			}
			currentlyRunning.set(0l);
		}
	}

	public Set<RT2DocUidType> getMarkedForRemove() {
		return new HashSet<>(markedForRemove);
	}		
	
	public long getCurrentlyRunning() {
		return currentlyRunning.get();
	}
}
