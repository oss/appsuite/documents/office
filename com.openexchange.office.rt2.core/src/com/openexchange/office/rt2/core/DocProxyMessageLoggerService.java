/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.EvictingQueue;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import com.openexchange.java.util.ImmutablePair;

@Service
public class DocProxyMessageLoggerService implements InitializingBean, DisposableBean {
    public static final long CLEANUP_ORPHANED_LOG_INFO = 600000;

    private static final long CLEANUP_TIMER_PERIOD = 60000;
    private static final long TIME_FOR_NEXT_STATUS_INFO_LOG = 300000;
    private static final int DEFAULT_COUNT_MESSAGES_STORED = 20;

    private static final Logger LOG = LoggerFactory.getLogger(DocProxyMessageLoggerService.class);
	
    @Autowired
    private TimerService timerService;

    @Autowired
    private RT2DocProxyRegistry docProxyRegistry;

    @Autowired
    private LocalDateTimeService localDateTimeService;

    private ScheduledTimerTask cleanupTimerTask = null;

	private final ConcurrentHashMap<RT2CliendUidType, Collection<RT2DocProxyLogInfo>> msgs = new ConcurrentHashMap<>();

	private final ConcurrentHashMap<RT2CliendUidType, LocalDateTime> logInfosCleanUpMap = new ConcurrentHashMap<>();

	private static class CleanUpOrphanedLogInfoTask implements Runnable {
	    private Map<RT2CliendUidType, Collection<RT2DocProxyLogInfo>> msgsToCheck;
	    private Map<RT2CliendUidType, LocalDateTime> logInfosCleanUpMap;
	    private RT2DocProxyRegistry docProxyRegistry;
	    private LocalDateTimeService localDateTimeService;
	    private final AtomicReference<LocalDateTime> lastStatusLogMessage = new AtomicReference<>(null);
	    private final AtomicLong lastPeriodCleanedUp = new AtomicLong(0L);

	    //-------------------------------------------------------------------------
	    public CleanUpOrphanedLogInfoTask(Map<RT2CliendUidType, Collection<RT2DocProxyLogInfo>> msgsToCheck,
	                                      Map<RT2CliendUidType, LocalDateTime> logInfosCleanUpMap,
	                                      RT2DocProxyRegistry docProxyRegistry,
	                                      LocalDateTimeService localDateTimeService) {
	        this.msgsToCheck = msgsToCheck;
	        this.logInfosCleanUpMap = logInfosCleanUpMap;
	        this.docProxyRegistry = docProxyRegistry;
	        this.localDateTimeService = localDateTimeService;
	        lastStatusLogMessage.set(localDateTimeService.getNowAsUTC());
	    }

	    //-------------------------------------------------------------------------
        @Override
        public void run() {
            try {
                var entriesToMarkForDelayedGC = msgsToCheck.keySet().stream()
                                                                   .map(rt2ClientUid -> getProxyUidOrNull(rt2ClientUid))
                                                                   .filter(Objects::nonNull)
                                                                   .filter(docProxyRef -> docProxyNotInRegistry(docProxyRef))
                                                                   .toList();

                var now = localDateTimeService.getNowAsUTC();

                entriesToMarkForDelayedGC.stream().forEach(docProxyPair -> {
                    var rt2ClientUid = docProxyPair.getFirst();
                    logInfosCleanUpMap.putIfAbsent(rt2ClientUid, now);
                });

                var cleanUpNow = logInfosCleanUpMap.entrySet()
                                    .stream()
                                    .filter(entry -> mustBeRemovedNow(now, entry))
                                    .map(entry -> entry.getKey())
                                    .toList();
                lastPeriodCleanedUp.addAndGet(cleanUpNow.size());

                cleanUpNow.stream().forEach(rt2ClientUid -> {
                    LOG.debug("DocProxyMessageLoggerService remove log entries for {}", rt2ClientUid);
                    msgsToCheck.remove(rt2ClientUid);
                    logInfosCleanUpMap.remove(rt2ClientUid);
                });

                var durationBetweenLastStatusLogMessageAndNow = Duration.between(lastStatusLogMessage.get(), now);
                if ((durationBetweenLastStatusLogMessageAndNow.getSeconds() * 1000) >= TIME_FOR_NEXT_STATUS_INFO_LOG) {
                    LOG.info("DocProxyMessageLoggerService overview during last observation period of {}s: status=doc_proxy_log_count: {}, status=doc_proxy_log_mark_for_gc_count: {}, status=doc_proxy_log_removed_count: {} ",
                        (TIME_FOR_NEXT_STATUS_INFO_LOG / 1000), msgsToCheck.size(), logInfosCleanUpMap.size(), lastPeriodCleanedUp.get());

                    lastPeriodCleanedUp.set(0);
                    lastStatusLogMessage.set(now);
                }
            } catch (Exception ex) {
                LOG.warn("DocProxyMessageLoggerService caught exception while trying to clean-up logging entries", ex);
            }
        }

        //-------------------------------------------------------------------------
        private boolean mustBeRemovedNow(LocalDateTime now, Entry<RT2CliendUidType, LocalDateTime> entry) {
            var durationNowAndNotUsedDetected = Duration.between(entry.getValue(), now);
            return ((durationNowAndNotUsedDetected.getSeconds() * 1000) >= CLEANUP_ORPHANED_LOG_INFO);
        }

        //-------------------------------------------------------------------------
        private ImmutablePair<RT2CliendUidType, RT2DocUidType> getProxyUidOrNull(RT2CliendUidType clientUid) {
            ImmutablePair<RT2CliendUidType, RT2DocUidType> result = null;
            var logInfos = msgsToCheck.get(clientUid);

            if (logInfos != null) {
                RT2DocUidType docUid = null;
                for (var logInfo : logInfos) {
                    if (logInfo.getDocUid() != null) {
                        docUid = logInfo.getDocUid();
                        break;
                    }
                }
                result = (docUid != null) ? ImmutablePair.newInstance(clientUid, docUid) : null;
            }

            return result;
        }

        //-------------------------------------------------------------------------
        private boolean docProxyNotInRegistry(ImmutablePair<RT2CliendUidType, RT2DocUidType> docProxyRef) {
            return docProxyRegistry.getDocProxy(docProxyRef.getFirst(), docProxyRef.getSecond()) == null;
        }
	}
	
    //-------------------------------------------------------------------------
    @Override
    public void afterPropertiesSet() throws Exception {
        cleanupTimerTask = timerService.scheduleAtFixedRate(
            new CleanUpOrphanedLogInfoTask(msgs, logInfosCleanUpMap, docProxyRegistry, localDateTimeService),
            CLEANUP_TIMER_PERIOD, CLEANUP_TIMER_PERIOD);
    }

    //-------------------------------------------------------------------------
    @Override
    public void destroy() throws Exception {
        msgs.clear();
        logInfosCleanUpMap.clear();
        if (cleanupTimerTask != null) {
            cleanupTimerTask.cancel();
        }
    }

    //-------------------------------------------------------------------------
    public List<String> getLogInfoMsgsStates() {
        var result = new ArrayList<String>();
        msgs.keySet().stream().forEach(rt2ClientUid -> {
            var strBuf = new StringBuilder(rt2ClientUid.getValue());
            strBuf.append("=");
            var coll = msgs.get(rt2ClientUid);
            strBuf.append("msgs: ").append((coll != null) ? coll.size() : 0);
            var cleanupMapEntry = logInfosCleanUpMap.get(rt2ClientUid);
            strBuf.append(", state: ").append((cleanupMapEntry != null) ? "non-active " + cleanupMapEntry.toString() : "active");
            result.add(strBuf.toString());
        });
        return result;
    }

	//-------------------------------------------------------------------------
    public void addMessageForQueueToLog(Direction direction, final RT2Message msg) {
    	if (msg != null) {
    		getMsgsOfDocProxy(msg.getClientUID()).add(new RT2DocProxyLogInfo(direction, msg));
    	}
    }

    //-------------------------------------------------------------------------	
	public void resetQueueSizeForLogInfo(RT2CliendUidType clientUid, int count) {
		if (msgs.computeIfAbsent(clientUid, k -> {return Collections.synchronizedCollection(EvictingQueue.create(count));}) != null) {
			msgs.computeIfPresent(clientUid, (k, v) -> {
				final Collection<RT2DocProxyLogInfo> newQueue = Collections.synchronizedCollection(EvictingQueue.create(count));
				newQueue.addAll(v);
				return newQueue;
			});
		}
	}
	
    //-------------------------------------------------------------------------
    public List<String> formatMsgsLogInfo(RT2CliendUidType clientUid) {
    	var res = getMsgsOfDocProxyAsList(clientUid);
    	return res.stream().map(m -> m.toString()).collect(Collectors.toList());
    }

    //-------------------------------------------------------------------------
    public List<RT2LogInfo> getMsgsAsList(RT2CliendUidType clientUid) {
    	var res = new ArrayList<RT2LogInfo>();
    	var logList = getMsgsOfDocProxy(clientUid);
    	if (logList != null) {
    		res.addAll(logList);
    	}
    	return res;
    }

    //-------------------------------------------------------------------------
    public void add(RT2DocUidType docUid, Direction direction, String msgType, RT2CliendUidType clientUid) {
    	var docProxyLogInfo = new RT2DocProxyLogInfo(direction, msgType, clientUid, docUid);
    	getMsgsOfDocProxy(clientUid).add(docProxyLogInfo);
    }
        
    //-------------------------------------------------------------------------    
    public void remove(RT2CliendUidType clientUid) {
    	msgs.remove(clientUid);
    }
    
    //-------------------------------------------------------------------------
	private List<RT2DocProxyLogInfo> getMsgsOfDocProxyAsList(RT2CliendUidType clientUid) {
		var res = new ArrayList<RT2DocProxyLogInfo>();
    	var logList = getMsgsOfDocProxy(clientUid);
    	if (logList != null) {
    		res.addAll(logList);
    	}
    	Collections.sort(res);
		return res;
	}    

	//-------------------------------------------------------------------------
	private Collection<RT2DocProxyLogInfo> getMsgsOfDocProxy(RT2CliendUidType clientUid) {
		var currCol = msgs.get(clientUid);
		if (currCol == null) {
			Collection<RT2DocProxyLogInfo> newCol = Collections.synchronizedCollection(EvictingQueue.create(DEFAULT_COUNT_MESSAGES_STORED));
			var oldCol = msgs.putIfAbsent(clientUid, newCol);
			if (oldCol == null) {
				return newCol;
			}
		}
		return currCol;
	}

}
