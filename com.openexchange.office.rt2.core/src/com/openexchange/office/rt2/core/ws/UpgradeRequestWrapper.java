/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpgradeRequestWrapper extends HttpServletRequestWrapper {
	private static final String USER_AGENT_MS_EDGE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 14) Edge/18";

	private final boolean fakeMSEdge;
	private final String knownUserAgent;

	public UpgradeRequestWrapper(HttpServletRequest request) {
		super(request);
		this.fakeMSEdge = false;
		this.knownUserAgent = null;
	}

	public UpgradeRequestWrapper(HttpServletRequest request, boolean fakeMSEdge) {
		super(request);
		this.fakeMSEdge = fakeMSEdge;
		this.knownUserAgent = null;
	}

	public UpgradeRequestWrapper(HttpServletRequest request, String knownUserAgent, boolean fakeMSEdge) {
		super(request);
		this.fakeMSEdge = fakeMSEdge;
		this.knownUserAgent = knownUserAgent;
	}

	@Override
    public String getHeader(String name) {
    	if ("user-agent".equalsIgnoreCase(name)) {
    		String result;

    		String userAgent = super.getHeader(name);
    		userAgent = (StringUtils.isEmpty(userAgent)) ? knownUserAgent : userAgent;
    		if (StringUtils.isEmpty(userAgent) && fakeMSEdge) {
    			// ATTENTION: This is a workaround and can fail if the backend
    			// code changes the browser detection.
    			// We know that MS Edge has an issue that it doesn't send any
    			// user-agent info via a websocket connection. In this special
    			// case we fake the user-agent to ensure that a valid is set.
    			result = USER_AGENT_MS_EDGE;
    		} else {
        		result = userAgent;
    		}
    		return result;
    	} else {
    		return super.getHeader(name);
    	}
    }

}
