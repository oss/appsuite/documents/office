/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.validation.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.glassfish.grizzly.websockets.DataFrame;
import org.glassfish.grizzly.websockets.WebSocket;
import org.glassfish.grizzly.websockets.WebSocketListener;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.metric.DocProxyRequestMetricService;
import com.openexchange.office.rt2.core.metric.DocProxyResponseMetricService;
import com.openexchange.office.rt2.core.metric.WebsocketResponseMetricService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.string.StringHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-3)
@RegisteredService
public class RT2WebSocketListener implements WebSocketListener, DisposableBean {

	private static final Logger log = LoggerFactory.getLogger(RT2WebSocketListener.class);
	private static final byte[] data = new byte[] { 0 };

	private final ExecutorService onWebSocketMsgThreadPool = Executors.newFixedThreadPool(10, new ThreadFactoryBuilder("RT2WebSocketListenerOnMsg-%d").build());

	//--------------------------Services--------------------------------------
	@Autowired
	private RT2SessionValidator sessionValidator;

	@Autowired
	private RT2SessionCountValidator sessionCountValidator;

	@Autowired
	private RT2WSChannelDisposer channelDisposer;

	@Autowired
	private RT2DocProxyRegistry docProxyRegistry;

	@Autowired
    private WebsocketResponseMetricService websocketResponseMetriceService;

	@Autowired
    private DocProxyRequestMetricService docProxyRequestMetricService;

	@Autowired
    private DocProxyResponseMetricService docProxyResponseMetricService;

	@Autowired
	private RT2WSApp rt2WsApp;

	@Autowired
	private ClusterService clusterService;

	//------------------------------------------------------------------------

	@Override
	public void destroy() throws Exception {
		onWebSocketMsgThreadPool.shutdown();
	}

	@Override
	public void onClose(WebSocket socket, DataFrame frame) {
		final RT2EnhDefaultWebSocket rt2WebSocket = (RT2EnhDefaultWebSocket) socket;
		// ATTENTION: We should only trigger an offline if we don't have a websocket
		// instance in our websocket container. The Grizzly uses for a certain time
		// span more than one  websocket for the same channel. Therefore we need to
		// use the following check.
		if (!rt2WsApp.contains(rt2WebSocket.getId())) {
			final Set<Pair<RT2CliendUidType, RT2DocUidType>> assocClients = channelDisposer.getClientDocPairsAssociatedToChannel(rt2WebSocket.getId());
			assocClients.forEach(p -> {
				log.debug("RT2: ... com.openexchange.rt2.client.uid '{}' switch into offline mode for com.openexchange.rt2.document.uid '{}'", p.getKey(), p.getValue());
				RT2DocProxy docProxy = null;
				try {
					docProxy = docProxyRegistry.getDocProxy(p.getKey(), p.getValue());
					if ((docProxy != null) && (docProxy.isOnline())) {
						docProxy.goOffline();
					}
				} catch (Throwable ex) {
	                com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
	                try {
	                    MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, p.getKey().getValue());
	                    MDCHelper.safeMDCPut(MDCEntries.DOC_UID, p.getValue().getValue());
	                    log.error("RT2: ... com.openexchange.rt2.client.uid '"+p.getKey()+"' failed on switching into offline mode for com.openexchange.rt2.document.uid '"+p.getValue() + ", msgs: " + (docProxy == null ? "" : docProxy.formatMsgsLogInfo()), ex);
	                } finally {
	                    MDC.remove(MDCEntries.CLIENT_UID);
	                    MDC.remove(MDCEntries.DOC_UID);
	                }
	            }
			});
		}
		this.channelDisposer.removeChannel(rt2WebSocket);
		sessionCountValidator.removeSession(rt2WebSocket.getId());
	}

	@Override
	public void onConnect(WebSocket socket) {
		// Nothing todo
	}

	@Override
	public void onMessage(WebSocket socket, String msg) {
		onWebSocketMsgThreadPool.execute(() -> {
			onMessageSync(socket, msg);
		});
	}

	public void onMessageSync(WebSocket socket, String msg) {
		RT2EnhDefaultWebSocket rt2WebSocket = (RT2EnhDefaultWebSocket) socket;
		rt2WebSocket.setCurrentlyProcessingRequest(true);
		rt2WebSocket.updateLastRecMsgTime();
		RT2Message request = null;
		try {
			request = RT2MessageFactory.fromJSONString(msg);
		} catch (JSONException | ValidationException ex) {
			log.warn(ex.getMessage(), ex);
			return;
		}
		try {
			if (request.getType() == RT2MessageType.PONG) {
				log.debug("RT2: pong msg {} received from ws {}", msg, socket);
				onPong(socket, data);
				Set<Pair<RT2CliendUidType, RT2DocUidType>> clientDocPairsAssociatedToChannel = channelDisposer.getClientDocPairsAssociatedToChannel(rt2WebSocket.getId());
				clientDocPairsAssociatedToChannel.stream().forEach(p -> {
					RT2DocProxy docProxy =  docProxyRegistry.getDocProxy(p.getKey(), p.getValue());
					if ((docProxy != null) && (!docProxy.isOnline())) {
						docProxy.goOnline();
					}
				});
			} else {
				impl_handleRequest(socket, request);
			}

			if (!rt2WebSocket.isAvaible()) {
				rt2WebSocket.setAvaible(true);
				channelDisposer.notifyUnavailability(Direction.FROM_WS, rt2WebSocket, 0);
			}
        } catch (final Throwable e) {
            ExceptionUtils.handleThrowable(e);
            log.error(e.getMessage(), e);
            // no further handling !
            // All responses was send within impl_handleRequest() already !
        } finally {
        	MDC.clear();
            rt2WebSocket.setCurrentlyProcessingRequest(false);
        }
	}

	@Override
	public void onMessage(WebSocket socket, byte[] bytes) {
		onMessage(socket, new String(bytes));
	}

	@Override
	public void onPong(WebSocket socket, byte[] bytes) {
		RT2EnhDefaultWebSocket rt2WebSocket = (RT2EnhDefaultWebSocket) socket;
		rt2WebSocket.updateLastRecMsgTime();
		log.debug("RT2: websocket channel got pong response [{}]", StringHelper.toHexString(bytes, true));
		if (!rt2WebSocket.isAvaible()) {
			rt2WebSocket.setAvaible(true);
			log.info("Notifiy unavilability for websocket with id {} because of onPong", rt2WebSocket.getId());
			channelDisposer.notifyUnavailability(Direction.INTERNAL, rt2WebSocket, 0);
		}
	}

	public void emergencyLeave(WebSocket socket, String docUID, String clientUID, String sessionId, JSONObject leaveData) throws Exception {
		  // ATTENTION: This is a special method called out-side the web
		  // socket communication env to enable clients to send a last-message
		  // that notifies the RT2WSChannel that a client has to leave in an
		  // emergency case and won't be able to send/receive any more messages.
		  //
		  // The implementation needs to create a priority RT2Message and
		  // send it to the DocProcessor ignoring the current state machine
		  // state. It's also responsible to clean-up the resources that the
		  // client references when the response of the DocProcessor has been
		  // received.
		  final RT2Message aEmergencyLeaveRequest = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_EMERGENCY_LEAVE, new RT2CliendUidType(clientUID), new RT2DocUidType(docUID));
		  RT2MessageGetSet.setSessionID(aEmergencyLeaveRequest, sessionId);
		  aEmergencyLeaveRequest.setBody(leaveData);
		  try {
			  impl_handleRequest(socket, aEmergencyLeaveRequest);
		  } finally {
			MDC.clear();
		}
	}


    //-------------------------------------------------------------------------
    /** handle incoming client requests.
     *
     *  @param	sRequest [IN]
     *      the request from client side formated as JSON.
     */

	private void impl_handleRequest(WebSocket socket, final RT2Message request) {
		boolean failed = false;
		RT2DocProxy docProxy = null;
		RT2EnhDefaultWebSocket enhSocket = (RT2EnhDefaultWebSocket) socket;
        var msgType = request.getType();

        try {
			setMDCVariables(request);
			startMetricsMeasurement(request);

        	channelDisposer.addDocUidToChannel(enhSocket, request.getClientUID(), request.getDocUID());
        	sessionValidator.validateSession(request, enhSocket);
        	sessionCountValidator.addSession(request.getSessionID(), enhSocket.getId());

        	if (RT2MessageType.REQUEST_JOIN.equals(request.getType())) {
        		channelDisposer.addDocUidToChannel(enhSocket, request.getClientUID(), request.getDocUID());
        		docProxy = docProxyRegistry.getOrCreateDocProxy(request, true, enhSocket.getId(), request.getSessionID());
        	} else {
        		docProxy = docProxyRegistry.getDocProxy(RT2DocProxy.calcID(request.getClientUID(), request.getDocUID()));
        	}

        	if (docProxy == null) {
        		if (!canIgnoreMessageTypeInCaseNoDocProxy(msgType)) {
        			if (!RT2MessageType.REQUEST_SYNC_STABLE.equals(msgType)) {
        				throw new RT2TypedException(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR, new ArrayList<>());
        			}

      				log.debug("com.openexchange.rt2.client.uid {} not found when SYNC_STABLE-Request was received.", request.getClientUID());
//    				enhSocket.send(rt2Message);
       				return;
        		}
        		return;
        	}

        	docProxy.addMsgFromWebSocket(request);
            switch (msgType) {
                case REQUEST_JOIN:
                	if (enhSocket.getAdvisoryLockMode() == AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN)
                		impl_extendRequestWithHostName(enhSocket, request);
                	docProxy.join(request);
                	break;
                case REQUEST_OPEN_DOC:
                	if (enhSocket.getAdvisoryLockMode() == AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN)
                		impl_extendRequestWithHostName(enhSocket, request);
                	docProxy.open(request);
                    break;
                case REQUEST_CLOSE_DOC:
                	docProxy.close(request);
                    break;
                case REQUEST_LEAVE:
                case REQUEST_EMERGENCY_LEAVE:
                	docProxy.leave(request);
                    break;
                case REQUEST_ABORT_OPEN: docProxy.abortOpen(request);
                    break;
                default:                 docProxy.sendRequest(request);
                    break;
            }
            websocketResponseMetriceService.incMessageSucc(request.getMessageID());
        } catch (RT2SessionInvalidException ex) {
        	failed = true;
        	if (ex.isLog()) {
        		log.info("Session invalid exception " + ex.getMessage() + " occured during processing request " + request.toString());
        	}
        	impl_handleRequestErrors (request, ex, enhSocket);
        } catch (final Throwable e) {
            ExceptionUtils.handleThrowable(e);

        	failed = true;
            // some rt2 typed exception are just informational - check ex type
            if (RT2TypedException.class.isAssignableFrom(e.getClass ())) {
                final RT2TypedException aRT2Ex = (RT2TypedException) e;
                final ErrorCode aError = aRT2Ex.getError();

                if (aError.isWarning()) {
                    log.debug(e.getMessage() + "; request: " + request.getHeader() + ", msgs: " + (docProxy == null ? "" : docProxy.formatMsgsLogInfo()));
                } else if (shouldLogErrorOnInfoLevel(aError) ) {
                    MDCHelper.safeMDCPut(MDCEntries.ERROR, aError.getCodeAsStringConstant());
                    log.info("Exception " + e.getMessage() + " occured during processing request " + request.getHeader() + ", msgs: " + (docProxy == null ? "" : docProxy.formatMsgsLogInfo()), e);
                    MDC.remove(MDCEntries.ERROR);
                } else {
                    MDCHelper.safeMDCPut(MDCEntries.ERROR, aError.getCodeAsStringConstant());
                    log.error("Exception " + e.getMessage() + " occured during processing request " + request.getHeader() + ", msgs: " + (docProxy == null ? "" : docProxy.formatMsgsLogInfo()), e);
                    MDC.remove(MDCEntries.ERROR);
                }
            } else {
                log.error("Exception " + e.getMessage() + " occured during processing request " + request.getHeader() + ", msgs: " + (docProxy == null ? "" : docProxy.formatMsgsLogInfo()), e);
            }
            impl_handleRequestErrors (request, e, enhSocket);
        } finally {
			if (failed) {
				websocketResponseMetriceService.incMessageFailed(request.getMessageID());
			}
			websocketResponseMetriceService.stopTimer(request.getMessageID());
			resetMDCVariables();
		}
    }

    //-------------------------------------------------------------------------
	private boolean shouldLogErrorOnInfoLevel(ErrorCode errorCode) {
	    var code = errorCode.getCode();
	    switch (code) {
	        case ErrorCode.CODE_GENERAL_CLIENT_UID_UNKNOWN_ERROR -> { return true; }
	        default -> { return false; }
	    }
	}

    //-------------------------------------------------------------------------
	private boolean canIgnoreMessageTypeInCaseNoDocProxy(RT2MessageType msgType) {
	    switch (msgType) {
	        case ACK_SIMPLE, NACK_SIMPLE -> { return true; }
	        default -> { return false; }
	    }
	}

	private void impl_extendRequestWithHostName(RT2EnhDefaultWebSocket enhSocket, RT2Message request) {
    	final String origin = enhSocket.getUpgradeRequest().getHeader("origin");
    	final String hostNameFromOrigin = extractHostFromOrigin(origin);
    	RT2MessageGetSet.setOriginAsHostName(request, hostNameFromOrigin);
    	log.debug("Enhanced request from com.openexchange.rt2.client.uid {} with origin as hostname {}", request.getClientUID(), hostNameFromOrigin);
	}

    //-------------------------------------------------------------------------
    private void impl_handleRequestErrors (final RT2Message aRequest, final Throwable  ex, RT2EnhDefaultWebSocket webSocket) {
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_GENERIC_ERROR);
        ErrorCode aError = ErrorCode.GENERAL_UNKNOWN_ERROR;

        if (RT2TypedException.class.isAssignableFrom(ex.getClass ())) {
            final RT2TypedException aRT2Ex = (RT2TypedException) ex;
            aError = aRT2Ex.getError();

            RT2MessageGetSet.setError(aResponse, aError);

            try {
                setMDCVariables(aRequest);
                MDCHelper.safeMDCPut(MDCEntries.ERROR, aError.getCodeAsStringConstant());
                log.debug("Send error response {} to client", aResponse);
            } finally {
                resetMDCVariables();
                MDC.remove(MDCEntries.ERROR);
            }

            if (aError.equals(ErrorCode.GENERAL_SESSION_INVALID_ERROR)) {
            	impl_sendSyncResponse(aResponse, webSocket);
                impl_autoCloseOneClient (aRequest.getClientUID(), aRequest.getDocUID(), Cause.SESSION_INVALID);
            } else {
            	impl_sendResponse (aResponse);
            }
        } else {
            try {
                setMDCVariables(aRequest);
                MDCHelper.safeMDCPut(MDCEntries.ERROR, aError.getCodeAsStringConstant());
                log.debug("Send error response {} to client", aResponse);
            } finally {
                resetMDCVariables();
                MDC.remove(MDCEntries.ERROR);
            }
            RT2MessageGetSet.setError(aResponse, aError);
            impl_sendResponse (aResponse);
        }
    }

    //-------------------------------------------------------------------------
    private  void impl_sendResponse(final RT2Message aResponse)
    {
        // Warum? Klingt auch so, dass alle HeaderInfo geloescht werden, was nicht der Fall ist
        RT2Message.cleanHeaderBeforeSendToClient(aResponse);

        docProxyResponseMetricService.stopTimer(aResponse.getMessageID());
        log.debug("RT2WSChannel: Sending message to ws-client: {}", aResponse.getHeader());
        channelDisposer.addMessageToResponseQueue(aResponse);
    }

    //-------------------------------------------------------------------------
    private void impl_sendSyncResponse(final RT2Message response, RT2EnhDefaultWebSocket webSocket)
    {
        // Warum? Klingt auch so, dass alle HeaderInfo geloescht werden, was nicht der Fall ist
        RT2Message.cleanHeaderBeforeSendToClient(response);

        docProxyResponseMetricService.stopTimer(response.getMessageID());
        try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		    // ignore interrupt
		}
        log.debug("RT2WSChannel: Sending message sync to ws-client: {}", response.getHeader());
   		webSocket.send(response);
    }

    //-------------------------------------------------------------------------
    private  void impl_autoCloseOneClient (final RT2CliendUidType sClientUID, final RT2DocUidType docUid, Cause cause) {
        LogMethodCallHelper.logMethodCall(log, this, "impl_autoCloseOneClient", sClientUID, docUid);

        try {
            if (docUid != null) {
                log.debug("RT2: impl_autoCloseOneClient with com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", sClientUID, docUid);

                final RT2DocProxy aDocProxy = docProxyRegistry.getDocProxy(sClientUID, docUid);
                if (aDocProxy == null)
                    return;

                aDocProxy.closeHard(cause);
                // TODO: do we really need to call this? should be done by the async file leave by the proxy itself?
                docProxyRegistry.deregisterDocProxy(aDocProxy, false);
            } else {
                log.debug("RT2: impl_autoCloseOneClient com.openexchange.rt2.client.uid {} has no bound doc - nothing to do", sClientUID);
            }
        }
        catch (IllegalArgumentException ex) {
            log.debug("RT2: impl_autoCloseOneClient - no client info found auto close already completed");
        }
        catch (Throwable ex) {
            com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
            try {
                MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, sClientUID.getValue());
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, ((docUid != null) ? docUid.getValue() : "unknown"));
                log.error("RT2: impl_autoCloseOneClient - releasing connection between com.openexchange.rt2.client.uid "+sClientUID+" and com.openexchange.rt2.document.uid "+docUid+" was not successful due to exception", ex);
            } finally {
                MDC.remove(MDCEntries.CLIENT_UID);
                MDC.remove(MDCEntries.DOC_UID);
            }
        }
    }

    //-------------------------------------------------------------------------
	private void setMDCVariables(final RT2Message aRequest) {
        if (aRequest.getDocUID() != null) {
        	MDCHelper.safeMDCPut(MDCEntries.DOC_UID, aRequest.getDocUID().getValue());
        }
        if (aRequest.getClientUID() != null) {
        	MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, aRequest.getClientUID().getValue());
        }
        MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "proxy_forward");
        MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
        MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, aRequest.getType().getValue());
    }

    //-------------------------------------------------------------------------
	private void resetMDCVariables() {
        MDC.remove(MDCEntries.DOC_UID);
        MDC.remove(MDCEntries.CLIENT_UID);
        MDC.remove(MDCEntries.BACKEND_PART);
        MDC.remove(MDCEntries.BACKEND_UID);
        MDC.remove(MDCEntries.REQUEST_TYPE);
	}

    //-------------------------------------------------------------------------
    private void startMetricsMeasurement(final RT2Message aRequest) {
    	if (websocketResponseMetriceService != null) {
    		websocketResponseMetriceService.startTimer(aRequest.getMessageID());
    	}
    	if (websocketResponseMetriceService != null) {
    		websocketResponseMetriceService.incMessagesReceived(aRequest.getMessageID());
    	}
    	if (docProxyRequestMetricService != null) {
    		docProxyRequestMetricService.startTimer(aRequest.getMessageID());
    	}
    }

	@Override
	public void onFragment(WebSocket socket, String fragment, boolean last) {
		// Nothing to do, implemented in RT2EnhDefaultWebSocket class
	}

	@Override
	public void onFragment(WebSocket socket, byte[] fragment, boolean last) {
		// Nothing to do, implemented in RT2EnhDefaultWebSocket class
	}

	@Override
	public void onPing(WebSocket socket, byte[] bytes) {
		// Nothing to do
	}

    //-------------------------------------------------------------------------
	private String extractHostFromOrigin(String origin) {
    	String hostName = null;
    	if (StringUtils.isNotEmpty(origin)) {
    		try {
    			hostName = new URL(origin).getHost();
    		} catch (MalformedURLException e) {
    			log.info("Exception caught trying to determine hostname from origin of upgrade-request", e);
    		}
    	} else {
    	    // fallback for not set origin to empty string
    	    hostName = "";
    	}

    	return hostName;
	}
}
