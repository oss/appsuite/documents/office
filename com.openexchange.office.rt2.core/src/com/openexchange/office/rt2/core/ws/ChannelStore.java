/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//=============================================================================
/**
 * A thread-safe associative storage for RT2EnhDefaultWebSocket instances. It provides
 * methods to search, put or remove instances and functions to retrieve collections
 * of web sockets filtered by certain arguments.
 * 
 * @author Carsten Driesner 
 *
 */
public class ChannelStore {
    private static final Logger log = LoggerFactory.getLogger(ChannelStore.class);    

    private static class ChannelData {
    	public WeakReference<RT2EnhDefaultWebSocket> ws;
    	public long creationTime;

    	public ChannelData(RT2EnhDefaultWebSocket ws) {
    		this.ws = new WeakReference<>(ws);
    		this.creationTime = ws.getCreationTime();
    	}
    };

    private final Map<RT2ChannelId, ChannelData> channels = new HashMap<>();

    public ChannelStore() {
    	// nothing to do
    }

    public int size() {
    	synchronized(channels) {
        	return channels.size();
    	}
    }

    public boolean containsChannel(RT2ChannelId channelId) {
    	synchronized(channels) {
    		return channels.containsKey(channelId);
    	}
    }

    public WeakReference<RT2EnhDefaultWebSocket> get(RT2ChannelId channelId) {
    	synchronized(channels) {
        	final ChannelData channelData = channels.get(channelId);
        	if (channelData != null)
        		return channelData.ws;
    	}

    	return null;
    }

    public WeakReference<RT2EnhDefaultWebSocket> put(RT2ChannelId channelId, RT2EnhDefaultWebSocket ws) {
    	synchronized(channels) {
    		final ChannelData oldData = channels.put(channelId, new ChannelData(ws));
    		if (oldData != null) {
    			return oldData.ws;
    		}
    	}

		return null;
    }

    public boolean remove(RT2EnhDefaultWebSocket ws) {
    	if (ws != null) {
    		synchronized(channels) {
    			final RT2ChannelId id = ws.getId();
        		final ChannelData channelData = channels.get(id);
        		if ((channelData != null) && (channelData.creationTime == ws.getCreationTime())) {
        			channels.remove(id);
        			log.debug("websocket {} with id {} removed from ChannelStore", ws, id);
        			return true;
        		}
    		}
    	}

    	return false;
    }

    public Collection<WeakReference<RT2EnhDefaultWebSocket>> values() {
    	synchronized(channels) {
    		Collection<ChannelData> coll = channels.values();
    		return coll.stream()
    				   .map(d -> d.ws)
    				   .collect(Collectors.toList());
    	}
    }

	public List<RT2ChannelId> getAllWSChannelIds() {
		synchronized(channels) {
			return channels.entrySet().stream().filter(e -> {
				return e.getValue().ws.get() != null;
			}).map(e -> e.getKey()).collect(Collectors.toList());
		}
    }

    public Set<RT2ChannelId> getActiveWebsocketIds() {
    	Set<RT2ChannelId> res;

    	synchronized(channels) {
            res = channels.entrySet().stream().filter(e -> {
    		    RT2EnhDefaultWebSocket ws = e.getValue().ws.get();
        		if (ws != null) {
        			final boolean available = ws.isAvaible();
        			log.trace("websocket {} with id {} found in ChannelStore, available state {}", ws, e.getKey().toString(), available);
        			return available;
        		} else {
        			log.debug("websocket {} with id {} found in ChannelStore, but instance null", ws, e.getKey());
        		}
        		return false;
        	}).map(e -> e.getKey())
        	  .collect(Collectors.toSet());
    	}

    	return res;
    }

}
