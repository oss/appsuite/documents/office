/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.ajax.SessionUtility;
import com.openexchange.config.ConfigurationService;
import com.openexchange.configuration.CookieHashSource;
import com.openexchange.configuration.ServerConfig.Property;
import com.openexchange.exception.OXException;
import com.openexchange.log.LogProperties;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;

@Service
public class RT2SessionValidator {

	private static final Logger log = LoggerFactory.getLogger(RT2SessionValidator.class);

	@Autowired
	private ConfigurationService configService;

	@Autowired
	private SessionService sessionService;

	/**
	 * Will refresh current session of webSocket, but will fail if session is already expired or invalid.
	 * @param rt2Msg Current request
	 * @param webSocket The connection to check the session of
	 */
	public void validateSession(RT2Message rt2Msg, RT2EnhDefaultWebSocket webSocket) throws Exception {
	    try {
	        impl_refreshOrFailOnInvalidSession (rt2Msg, webSocket);
	    } catch (RT2SessionInvalidException ex) {
	        // Handle invalid exception and check out if the request can
	        // be ignored or we have to process the exception. In that
	        // case the handler has to re-throw the original exception.
	        impl_handleInvalidSession(rt2Msg, webSocket, ex);
	    }
	}

    private void impl_refreshOrFailOnInvalidSession (final RT2Message aRequest, final RT2EnhDefaultWebSocket webSocket)
            throws Exception
        {
            log.debug("impl_refreshOrFailOnInvalidSession");

            if (!aRequest.isSessionMessage())
                return;

            if (aRequest.getSessionID() == null) {
                final String logMessage = "no session defined for request [" + aRequest + "]";
                final RT2TypedException ex = new RT2SessionInvalidException(new ArrayList<>(), "details", logMessage);
                throw ex;
            }

            final Session aSession = sessionService.getSession4Id(aRequest.getSessionID().getValue());
            if (aSession == null) {
                final String logMessage = "RT2: Session '" + aRequest.getSessionID() + "' do not exists - or is expired. Request: [" + aRequest + "]";
                final RT2TypedException ex = new RT2SessionInvalidException(new ArrayList<>(), "details", logMessage);
                throw ex;
            }
            if (log.isTraceEnabled()) {
                try {
                    MDCHelper.safeMDCPut(LogProperties.Name.SESSION_SESSION_ID.getName(), aRequest.getSessionID().getValue());
                    log.trace("Session found for request with session id " + aRequest.getSessionID());
                } finally {
                    MDC.remove(LogProperties.Name.SESSION_SESSION_ID.getName());
                }
            }

            final boolean bIsValid = sessionService.isSessionValid(aSession);
            if ( ! bIsValid) {
                final String logMessage = "RT2: Session '"+aSession.getSessionID()+"' is not valid. Request: [" + aRequest + "]";
                final RT2TypedException ex = new RT2SessionInvalidException(new ArrayList<>(), "details", logMessage);
                throw ex;
            }

            impl_validateSessionSecrets(aRequest, aSession, webSocket);
        }

        //-------------------------------------------------------------------------
        private void impl_validateSessionSecrets (final RT2Message aRequest, final Session aSession, final RT2EnhDefaultWebSocket webSocket) throws RT2TypedException {

        	Object obj = webSocket.getUpgradeRequestSessionValid();
        	if (obj == null) {
        		obj = Boolean.TRUE;
        	}
        	Boolean sessionValid = (Boolean) obj;
            if (!sessionValid.booleanValue()) {
                final String logMessage = "RT2: Upgrade request has no valid session '" + aSession.getSessionID() + "'";
                final RT2TypedException ex = new RT2SessionInvalidException(new ArrayList<>(), "details", logMessage);
                throw ex;
            }

            // workaround for MS Edge browsers which don't send a valid user agent string via web-socket messages
            if ((aRequest.getType() == RT2MessageType.REQUEST_JOIN) && StringUtils.isNotEmpty(RT2MessageGetSet.getUserAgent(aRequest))) {
            	webSocket.setUserAgentIfNotSet(RT2MessageGetSet.getUserAgent(aRequest));
            }

            final String               sCookieHash     = configService.getProperty(Property.COOKIE_HASH.getPropertyName());
            final CookieHashSource     aHashSource     = CookieHashSource.parse(sCookieHash);
            final HttpServletRequest   aServletRequest = webSocket.getUpgradeRequest();
            final String               knownUserAgent  = webSocket.getUserAgent();

            try {
            	final UpgradeRequestWrapper requestWrapped = new UpgradeRequestWrapper(aServletRequest, knownUserAgent, true);
                SessionUtility.checkSecret(aHashSource, requestWrapped, aSession);
                webSocket.setUpgradeRequestSessionValid(true);
            } catch (OXException ex) {
            	webSocket.setUpgradeRequestSessionValid(false);
                final String logMessage = "RT2: Session secret validation failed for session '" + aSession + "'";
                final RT2TypedException rt2Ex = new RT2SessionInvalidException(new ArrayList<>(), "details", logMessage);
                throw rt2Ex;
            } catch (Throwable ex) {
            	webSocket.setUpgradeRequestSessionValid(false);
                com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
                log.error(ex.getMessage(), ex);
				throw ex;
        	}
        }

        //-------------------------------------------------------------------------
        private void impl_handleInvalidSession(RT2Message aRequest, final RT2EnhDefaultWebSocket webSocket, final RT2SessionInvalidException origEx) throws RT2SessionInvalidException {
            final RT2MessageType type = aRequest.getType();

            // In case we receive a ack or nack and the session is already
            // not valid anymore - we assume that this could be a time-out
            // handling of the client, where is closes the application and
            // logs, but the ACK/NACK msgs are still on there way.
            // So we have a special handling for these two messages, which
            // will be ignored.
            // ATTENTION: Never let messages pass if we know that the session
            // is not valid.
            if ((type == RT2MessageType.ACK_SIMPLE) ||
                (type == RT2MessageType.NACK_SIMPLE)) {
            	origEx.disableLog();
                throw origEx;
            }
            if (!webSocket.isUnavailabeLogged()) {
            	log.info("RT2: websocket channel {} received more requests from client {} while session is invalid - drop request", webSocket.getId(), aRequest.getClientUID());
            	webSocket.setUnavailabeLogged(true);
            }
            throw origEx;
        }
}
