/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.config.RT2Const;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;

@Service
public class RT2SessionCountValidator {

	public static final String KEY_MAX_SESSIONS_PER_NODE = "com.openexchange.office.maxOpenDocumentsPerUser";
	
	@Autowired
	private SessionService sessionService;
	
	@Value("${" + KEY_MAX_SESSIONS_PER_NODE + ":" + RT2Const.DEFAULT_MAX_SESSIONS_PER_NODE + "}")
	private Integer maxSessionsPerUser;
	
	private ConcurrentHashMap<Integer, Set<RT2ChannelId>> countSessionsOfUser = new ConcurrentHashMap<>();
	
	public RT2SessionCountValidator() {}
	
	public RT2SessionCountValidator(int maxSessionsPerUser) {
		this.maxSessionsPerUser = maxSessionsPerUser;		
	}
	
	public void addSession(RT2SessionIdType sessionId, RT2ChannelId channelId) throws RT2TypedException, OXException {
		Session session = sessionService.getSession4Id(sessionId.getValue());
		if (session == null) {
			throw new RT2SessionInvalidException(new ArrayList<>(), RT2SessionInvalidException.SESSION_ID_ADD_INFO, sessionId.getValue());
		}
		Set<RT2ChannelId> channelIds = Collections.synchronizedSet(new HashSet<>());
		channelIds = countSessionsOfUser.putIfAbsent(session.getUserId(), channelIds);
		if (channelIds == null) {
			channelIds = countSessionsOfUser.get(session.getUserId());
		}
		synchronized (channelIds) {
			channelIds.add(channelId);
			if (channelIds.size() > maxSessionsPerUser) {
				channelIds.remove(channelId);
				throw new RT2TypedException(ErrorCode.TOO_MANY_CONNECTIONS_ERROR, new ArrayList<>());
			}
		}
	}
	
	public void removeSession(RT2ChannelId channelId) {
		synchronized (countSessionsOfUser) {
			final Set<Integer> emptyEntries = new HashSet<>();
			countSessionsOfUser.entrySet().stream().forEach(p -> {				
				p.getValue().remove(channelId);
				if (p.getValue().isEmpty()) {
					emptyEntries.add(p.getKey());
				}
			});
			countSessionsOfUser.keySet().removeAll(emptyEntries);
		}		
	}
	
	public Map<Integer, Integer> getCountSessionsOfUsers() {
		Map<Integer, Integer> res = new HashMap<>();
		for (Map.Entry<Integer, Set<RT2ChannelId>> entry : countSessionsOfUser.entrySet()) {
			res.put(entry.getKey(), entry.getValue().size());
		}
		return res;
	}
}
