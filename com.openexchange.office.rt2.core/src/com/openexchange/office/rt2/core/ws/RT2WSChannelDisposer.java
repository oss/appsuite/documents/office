/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.tuple.Pair;
import org.glassfish.grizzly.EmptyCompletionHandler;
import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.websockets.DataFrame;
import org.glassfish.grizzly.websockets.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.metric.DocProxyResponseMetricService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-3)
public class RT2WSChannelDisposer implements DisposableBean, FatalErrorListener {
    private static final int MAX_RESPONSE_THREADS = 8;

	private static final Logger log = LoggerFactory.getLogger(RT2WSChannelDisposer.class);

	//--------------------------Services--------------------------------------
	@Autowired
	private RT2WSApp webSocketApplication;

	@Autowired
	private RT2DocProxyRegistry docProxyRegistry;

	@Autowired
	private DocProxyResponseMetricService docProxyResponseMetricService;
	
	@Autowired
	private RT2ConfigService rt2ConfigService;

	@Autowired
	private DocProxyMessageLoggerService docProxyMessageLoggerService;

	@Autowired
	private ClusterService clusterService;
	
	//------------------------------------------------------------------------
	private final Map<RT2CliendUidType, Pair<RT2DocUidType, RT2ChannelId>> channels = new ConcurrentHashMap<>();

	private final Map<RT2ChannelId, Long> channelsOffline = new ConcurrentHashMap<>();

	private final BlockingQueue<RT2Message> responseQueue = new LinkedBlockingQueue<>();

	private final ScheduledExecutorService channelExistsExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2ChannelExists-%d").build());

	private final ScheduledExecutorService keepAliveExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2ChannelKeepAlive-%d").build());

	private final ScheduledExecutorService detectNonActiveClientsExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2ChannelDetectNonActiveClients-%d").build());

	private long gcTimeout;
	
	private AtomicBoolean stopped = new AtomicBoolean(false);

	private final FatalErrorListener fatalErrorListener; 

	public RT2WSChannelDisposer() {
	    fatalErrorListener = (FatalErrorListener)this;
	}

	public void startExecuterServices() {
		gcTimeout = rt2ConfigService.getRT2GCOfflineTresholdInMS();
		channelExistsExecutorService.scheduleAtFixedRate(new RT2WSChannelExistsCheckThread(), (gcTimeout / 10), (gcTimeout / 10), TimeUnit.MILLISECONDS);

        final long nPingFrequencyInMS = rt2ConfigService.getRT2KeepAliveFrequencyInMS();
        if (nPingFrequencyInMS != 0) {
            keepAliveExecutorService.scheduleAtFixedRate(new RT2WSChannelKeepAliveThread(), nPingFrequencyInMS, nPingFrequencyInMS, TimeUnit.MILLISECONDS);
            detectNonActiveClientsExecutorService.scheduleAtFixedRate(new RT2WSChannelDetectNonActiveClientsThread(), (nPingFrequencyInMS / 2), (nPingFrequencyInMS / 2), TimeUnit.MILLISECONDS);
        }

		for (int i = 0; i < MAX_RESPONSE_THREADS; ++i) {
			final Thread thread = new Thread(new ResponseThread(), "ResponseThread-" + i);
			thread.setDaemon(true);
			thread.start();
		}
	}

	@Override
	public void destroy() {
		stopped.set(true);
		channelExistsExecutorService.shutdown();
		keepAliveExecutorService.shutdown();
		detectNonActiveClientsExecutorService.shutdown();
	}

	@NotNull
	public Set<Pair<RT2CliendUidType, RT2DocUidType>> getClientDocPairsAssociatedToChannel(RT2ChannelId channelId) {
		final Set<Pair<RT2CliendUidType, RT2DocUidType>> res = channels.entrySet().stream().
				filter(p -> (p.getValue().getValue() != null) && p.getValue().getValue().equals(channelId)).
				map(p -> Pair.of(p.getKey(), p.getValue().getKey())).
				collect(Collectors.toSet());
		return res;
	}

	@NotNull
	public Set<RT2CliendUidType> getClientsAssociatedToChannel(RT2ChannelId channelId) {
		final Set<RT2CliendUidType> res = new HashSet<>();
		channels.forEach((clientUid, p) -> {
			if (p.getValue().equals(channelId)) {
				res.add(clientUid);
			}
		});
		return res;
	}

	@NotNull
	public Set<RT2DocUidType> getDocsAssociatedToChannel(RT2ChannelId channelId) {
		final Set<RT2DocUidType> res = new HashSet<>();
		channels.forEach((clientUid, p) -> {
			if (p.getValue().equals(channelId)) {
				res.add(p.getKey());
			}
		});
		return res;
	}

	@NotNull
	public Map<RT2ChannelId, Long> getChannelsOffline() {
		final Map<RT2ChannelId, Long> offline = new HashMap<>();
		channelsOffline.forEach((channelId, time) -> {
			offline.put(channelId, time);
		});
		return offline;
	}

	public void addDocUidToChannel(@NotNull RT2EnhDefaultWebSocket channel, @NotNull RT2CliendUidType clientUid, @NotNull RT2DocUidType docUid) {
		log.debug("addDocUidToChannel with channel-uid {} and com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", channel.getId(), clientUid, docUid);
		final RT2ChannelId channelId = channel.getId();
		final Pair<RT2DocUidType, RT2ChannelId> docUidToChannelId = Pair.of(docUid, channelId);
		channels.put(clientUid, docUidToChannelId);
	}

	public void addClientToChannel(@NotNull RT2EnhDefaultWebSocket channel, @NotNull RT2CliendUidType clientUid) {
		if (!channels.containsKey(clientUid)) {
			log.debug("addClientToChannel with channel-uid {} and com.openexchange.rt2.client.uid {}", channel.getId(), clientUid);
			final RT2ChannelId channelId = channel.getId();
			final Pair<RT2DocUidType, RT2ChannelId> docUidToChannelId = Pair.of(null, channelId);
			channels.put(clientUid, docUidToChannelId);
		}
	}

	public void removeChannel(@NotNull RT2EnhDefaultWebSocket channel) {
		final RT2ChannelId channelId = channel.getId();

		log.debug("removeChannel with channel-uid {} from channels with ws {} - let the gc remove the channel mapping later", channel.getId(), channel);

		// ATTENTION: Don't remove the channel here as we have two different behavior scenarios from the
		// Grizzly. First it adds a new web socket and removes the previous one sometimes later (looks like gc). This
		// happens if the client stays offline a little bit longer (about 5 minutes).
		// The second scenario can happen if the client stays offline for about 2 minutes. Grizzly now removes the
		// original web socket and creates a new one for the channel. This would lead to a missing mapping
		// if we would remove it in this method. Therefore we let the gc do the job and remove the channel mapping
		// asynchronously to survive both scenarios.

		if (getClientsAssociatedToChannel(channelId).size() > 0) {
			channelsOffline.putIfAbsent(channelId, System.currentTimeMillis());
		}
	}

	public void removeClientUid(@NotNull RT2CliendUidType clientUid) {
		log.debug("removeClient com.openexchange.rt2.client.uid {} from channels", clientUid);
		RT2ChannelId channelId = channels.get(clientUid).getValue();		
		channels.remove(clientUid);
		channelsOffline.remove(channelId);
	}

	static int sendBack = 0;
	
 	public void addMessageToResponseQueue(@NotNull RT2Message msg) {
 		log.debug("sendBack: {}", msg.getHeader());
 		if (RT2MessageType.RESPONSE_JOIN.equals(msg.getType())) {
 		    log.info("add join_response to response queue for client-uid {}", msg.getClientUID().getValue());
 		}
 		this.responseQueue.add(msg);
	}

    public void notifyUnavailability (Direction direction, @NotNull RT2EnhDefaultWebSocket webSocket, long nTime) {
    	final Set<Pair<RT2CliendUidType, RT2DocUidType>> clientsAssocToSocket = getClientDocPairsAssociatedToChannel(webSocket.getId());
    	clientsAssocToSocket.forEach(p -> {
    		try {
    			log.debug("RT2: ... com.openexchange.rt2.client.uid '{}' notify unavailability for com.openexchange.rt2.doc.uid '{}'", p.getKey(), p.getValue());
                final RT2DocProxy aDocProxy = docProxyRegistry.getDocProxy(p.getKey(), p.getValue());
                if (aDocProxy != null) {
                    aDocProxy.notifyUnavailibility(direction, nTime);
                }
    		}
            catch (Throwable ex) {
            	com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
                log.error("... com.openexchange.rt2.client.uid '"+p.getKey()+"' failed to notify unavailability for doc '"+p.getValue(), ex);
            }
        });
    }

    @Override
    public void fatalSendErrorOccurred(@NotNull RT2EnhDefaultWebSocket webSocket, @NotNull Throwable t) {
        if (!stopped.get()) {
            final Set<Pair<RT2CliendUidType, RT2DocUidType>> clientsAssocToSocket = getClientDocPairsAssociatedToChannel(webSocket.getId());
            clientsAssocToSocket.forEach(p -> {
                try {
                    log.debug("RT2: com.openexchange.rt2.client.uid '{}' notify fatal web socket error for com.openexchange.rt2.doc.uid '{}'", p.getKey(), p.getValue());
                    final RT2DocProxy aDocProxy = docProxyRegistry.getDocProxy(p.getKey(), p.getValue());
                    if (aDocProxy != null) {
                        aDocProxy.closeHard(Cause.FATAL_WEBSOCKET_ERROR);
                    }
                }
                catch (Throwable ex) {
                    com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
                    log.error("... com.openexchange.rt2.client.uid '"+p.getKey()+"' failed to react on fatal web socket error for com.openexchange.rt2.doc.uid '"+p.getValue() + "'", ex);
                }
            });
        }
    }

	@NotNull
    public Map<RT2CliendUidType, Pair<RT2DocUidType, RT2ChannelId>> getChannels() {
		return new HashMap<>(channels);
	}

	public int getResponseQueueSize() {
		return responseQueue.size();
	}

	class WSSendCompletionHandler extends EmptyCompletionHandler<DataFrame> {
	    private RT2EnhDefaultWebSocket websocket;
	    private FatalErrorListener regFatalErrorListener;

	    public WSSendCompletionHandler(@NotNull RT2EnhDefaultWebSocket websocket, @NotNull FatalErrorListener fatalErrorListener) {
	        this.websocket = websocket;
	        this.regFatalErrorListener = fatalErrorListener;
	    }

        @Override
        public void failed(Throwable throwable) {
            log.warn("Websocket send() encountered low-level error on ws {}", websocket.getId());

            if (throwable instanceof Error) {
                if (websocket.isConnected()) {
                    // we encountered a low-level error
                    websocket.close(WebSocket.ABNORMAL_CLOSE);
                }

                websocket.setFatalErrorOnSend(true);

                try {
                    regFatalErrorListener.fatalSendErrorOccurred(websocket, throwable);
                } catch (Throwable t) {
                    // nothing to log - handler threw exception and should have logged cause
                    com.openexchange.exception.ExceptionUtils.handleThrowable(t);
                }
            }
        }
    }

	class ResponseThread implements Runnable {

		@Override
		public void run() {
			MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "ResponseThread");
			MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
			while (!stopped.get()) {
				RT2Message responseMsg = null;
				try {
					responseMsg = responseQueue.poll(1, TimeUnit.SECONDS);
					if (responseMsg != null) {
                        if (responseMsg.getDocUID() != null) {
                        	MDCHelper.safeMDCPut(MDCEntries.DOC_UID, responseMsg.getDocUID().getValue());
                        	docProxyMessageLoggerService.add(responseMsg.getDocUID(), Direction.TO_WS, responseMsg.getType().getValue(), responseMsg.getClientUID());
                        }
                        RT2MessageType msgType = responseMsg.getType();
                        MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, responseMsg.getClientUID().getValue());
                        MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, msgType.getValue());
                        if (RT2MessageType.RESPONSE_JOIN.equals(msgType)) {
                            log.info("Sending join_response to ws client");
                        }
                        log.debug("Sending response [{}] to ws client", responseMsg.getHeader());

                        final RT2CliendUidType clientUid = responseMsg.getClientUID();
                        final Pair<RT2DocUidType, RT2ChannelId> docUidToChannelId = channels.get(clientUid);
                        if (docUidToChannelId != null) {
                       		final String sResponse = RT2MessageFactory.toJSONString(responseMsg);
                       		log.debug("impl_sendResponse on channel-uid {}", docUidToChannelId.getValue());
                       		final RT2EnhDefaultWebSocket webSocket = webSocketApplication.getWebSocketWithId(docUidToChannelId.getValue());
                       		if (webSocket != null) {
                       			log.debug("websocket instance used {} for sending msg", webSocket);
                       			if (ErrorCode.TOO_MANY_CONNECTIONS_ERROR.equals(responseMsg.getError().getValue())) {
                       				GrizzlyFuture<DataFrame> fut = webSocket.send(sResponse);
                       				fut.get(3, TimeUnit.SECONDS);
                       				webSocket.close(WebSocket.ABNORMAL_CLOSE);
                       			} else {                       				
                       				var future = webSocket.send(sResponse);
                       				future.addCompletionHandler(new WSSendCompletionHandler(webSocket, fatalErrorListener));
                       			}
                       		} else {
                   				log.debug("There is no response channel for com.openexchange.rt2.client.uid {}.", clientUid);
                       		}
                       		// remove client mapping immediately if client decided to leave
                       		if ((responseMsg.getType() == RT2MessageType.RESPONSE_LEAVE) ||
                       			(responseMsg.getType() == RT2MessageType.RESPONSE_EMERGENCY_LEAVE)) {
                       			removeClientUid(clientUid);
                       		}
                        } else {
                        	log.info("There is no response channel registered for com.openexchange.rt2.client.uid {}.", clientUid);
                        }
                        docProxyResponseMetricService.stopTimer(responseMsg.getMessageID());
 					}
				} catch (InterruptedException ex) {
                    // ignored ... as it's expected ... somehow ;-)
                    Thread.currentThread().interrupt();
                } catch (Throwable ex) {
                    com.openexchange.exception.ExceptionUtils.handleThrowable(ex);
                    log.error(ex.getMessage(), ex);
                } finally {
                    MDC.remove(MDCEntries.DOC_UID);
                    MDC.remove(MDCEntries.CLIENT_UID);
                    MDC.remove(MDCEntries.REQUEST_TYPE);
				}
			}
			MDC.clear();
		}
	}

	class RT2WSChannelExistsCheckThread implements Runnable {

		@Override
		public void run() {
			try {
				MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2WSChannelExistsCheckThread");
				MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
				checkIfWebsocketOfDocProxyExists();
			} finally {
				MDC.clear();
			}
		}

		private void checkIfWebsocketOfDocProxyExists() {
			final Set<RT2ChannelId> currentActiveWsIds = webSocketApplication.getCurrentActiveWebsocketIds();
			final List<RT2DocProxy> rt2DocProxies = docProxyRegistry.listAllDocProxies();
			rt2DocProxies.forEach(p -> {
				if (p.isOnline()) {
					final Pair<RT2DocUidType, RT2ChannelId> channelInfo = channels.get(p.getClientUID());
					final RT2ChannelId channelId = channelInfo.getValue();
					final RT2DocUidType docUid = channelInfo.getLeft();
					if (!currentActiveWsIds.contains(channelId)) {
						log.info("No channel with channel-uid {} found! Doc Proxy for com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {} switches to offline.", channelId, p.getClientUID(), docUid);
						p.goOffline();
						channelsOffline.putIfAbsent(channelId, System.currentTimeMillis());
					}
				}
			});
		}
	}

    class RT2WSChannelKeepAliveThread implements Runnable {

        @Override
        public void run() {
            // DOCS-1396: make sure that an exception doesn't lead to a cancellation
            // of this periodically called implementation (vital keep alive code which
            // triggers ping-messages).
            log.trace("Running keepAliveThread...");
            webSocketApplication.getCurrentActiveWebsocketIds().parallelStream().forEach(id -> {
                try {
                    MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2WSChannelKeepAliveThread");
                    MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
                    final RT2EnhDefaultWebSocket webSocket = webSocketApplication.getWebSocketWithId(id);
                    if (webSocket != null) {
                        webSocket.sendPing();
                    }
                } catch (Throwable t) {
                    com.openexchange.exception.ExceptionUtils.handleThrowable(t);
                    log.warn(t.getMessage(), t);
                } finally {
                    MDC.clear();
                }
            });
        }
    }

	class RT2WSChannelDetectNonActiveClientsThread implements Runnable {

        @Override
        public void run() {
            log.trace("Checking for non-active clients...");
            webSocketApplication.getCurrentActiveWebsocketIds().parallelStream().forEach(id -> {
                try {
                    MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2WSChannelKeepAliveThread");
                    MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());
					final RT2EnhDefaultWebSocket webSocket = webSocketApplication.getWebSocketWithId(id);
					if (webSocket != null) {
						if (webSocket.isAvaible()) {
							long now = System.currentTimeMillis();
							long unavail = webSocket.testIsAlive(now);
							if (unavail > 0l) {
								log.info("Notifiy unavilability for websocket with id {}", webSocket.getId());
								notifyUnavailability(Direction.INTERNAL, webSocket, unavail);
							}
						} else {
							log.debug("Websocket with id {} is unavailable", webSocket.getId());
						}
					}
            	} catch (Throwable t) {
            		com.openexchange.exception.ExceptionUtils.handleThrowable(t);
            		log.warn(t.getMessage(), t);
            	} finally {
            		MDC.clear();
				}
            });
		}
	}

}
