/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.glassfish.grizzly.http.HttpRequestPacket;
import org.glassfish.grizzly.websockets.ProtocolHandler;
import org.glassfish.grizzly.websockets.WebSocket;
import org.glassfish.grizzly.websockets.WebSocketApplication;
import org.glassfish.grizzly.websockets.WebSocketListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.openexchange.http.grizzly.service.websocket.WebApplicationService;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.config.RT2Const;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;

@Service
@ShutdownOrder(value=-3)
@RegisteredService
public class RT2WSApp extends WebSocketApplication implements RT2WebSocketContainer, InitializingBean, DisposableBean
{
	public static final String REQUEST_URL_PREFIX = "/rt2/v1/default/";

    private static final Logger log = LoggerFactory.getLogger(RT2WSApp.class);    
    
    @Autowired
    private RT2DocProxyRegistry docProxyRegistry;

    @Autowired
    private RT2WebSocketListener webSocketListener;
    
    @Autowired
    private RT2ConfigService rt2ConfigService;
    
    @Autowired
    private MetricRegistry metricRegistry;
    
    @Autowired
    private WebApplicationService webApplicationService;

    @Autowired
    private AdvisoryLockInfoService advisoryLockInfoService;
    
    private final ScheduledExecutorService scheduledExecService = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder().setNameFormat("RT2WSChannelHasCorrespondingProxyCheckThread-%d").build());
        
    private Set<RT2ChannelId> markedForRemove = new HashSet<>();

    /**
     * The implementation in the Grizzly has a bug. With getWebsockets() you do not always receive the whole active web-socket list.
     * Because of this situation we have to keep our own list.
     */
    private ChannelStore channels = new ChannelStore();
    
    //-------------------------------------------------------------------------    
	@Override
	public void afterPropertiesSet() throws Exception {
    	metricRegistry.register(MetricRegistry.name("ChannelRegistry", "size"), 
        		new Gauge<Integer>() {

    				@Override
    				public Integer getValue() {
    					return channels.size();
    				}
        		});	
    	scheduledExecService.scheduleAtFixedRate(new RT2WSChannelHasCorrespondingProxyCheckThread(), 5, 5, TimeUnit.MINUTES);
    	webApplicationService.registerWebSocketApplication("", RT2Const.RT2_URL_PATTERN, this, null);
    }    
    
    //-------------------------------------------------------------------------	
    @Override
	public void destroy() throws Exception {
    	scheduledExecService.shutdown();	
    	webApplicationService.unregisterWebSocketApplication(this);
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized WebSocket createSocket(final ProtocolHandler      protocolHandler,
    							               final HttpRequestPacket    requestPacket  ,
    							               final WebSocketListener... listener       ) {
    	final RT2EnhDefaultWebSocket webSocket = new RT2EnhDefaultWebSocket(protocolHandler, requestPacket, listener,
    												rt2ConfigService.getRT2KeepAliveFrequencyInMS(),
    												advisoryLockInfoService.getAdvisoryLockMode());
        log.info("create websocket channel with id {} and ws {} and creation time {}", webSocket.getId(), webSocket, webSocket.getCreationTime());
    	webSocket.add(webSocketListener);
    	
    	channels.put(webSocket.getId(), webSocket);
    	return webSocket;
    }

    /**
     * Associates the specified {@link WebSocket} with this application.
     *
     * @param socket the {@link WebSocket} to associate with this application.
     *
     * @return <code>true</code> if the socket was successfully associated,
     *  otherwise returns <code>false</code>.
     */
    @Override
    protected boolean add(WebSocket socket) {
        final RT2EnhDefaultWebSocket channel = (RT2EnhDefaultWebSocket)socket;
        log.debug("add websocket channel with id {} and ws {}", channel.getId(), socket);
        return super.add(socket);
    }

    //-------------------------------------------------------------------------
    /**
     * Remove association of specified {@link WebSocket} with this application.
     *
     * @param socket the {@link WebSocket} to remove with this application.
     *
     * @return <code>true</code> if the socket association was successfully removed,
     *  otherwise returns <code>false</code>.
     */
    @Override
    public boolean remove(WebSocket socket) {
        final RT2EnhDefaultWebSocket channel = (RT2EnhDefaultWebSocket)socket;
        if (channels.remove(channel)) {
            log.debug("remove websocket channel with id {} and ws {}", channel.getId(), socket);
        } else {
            log.debug("remove websocket channel ignored - id {} and ws {}", channel.getId(), socket);
        }

        return super.remove(socket);
    }

	public List<RT2ChannelId> getAllWSChannelIds() {
		return channels.getAllWSChannelIds();
    }

	@Override
	public boolean contains(RT2ChannelId id) {
		return channels.containsChannel(id);
	}    

    public Integer getCountWsChannels() {
        return getAllWSChannelIds().size();
    }

    @Override
    public Set<RT2ChannelId> getCurrentActiveWebsocketIds() {
    	return channels.getActiveWebsocketIds();
    }

    @Override
    public RT2EnhDefaultWebSocket getWebSocketWithId(RT2ChannelId channelId) {
    	// ATTENTION: Don't optimize this code if you don't know what you do!
    	// Grizzly can and will (for a certain time span) store more than one
    	// websocket for a unique channel. Therefore we have to find the most
    	// current websocket and provide it. --> Now we use our own list 
    	// and in our own list is only the newest websocket stored.
    	WeakReference<RT2EnhDefaultWebSocket> wrSocket = channels.get(channelId);
    	if (wrSocket != null) {
    		log.debug("getWebSocketWithId {}", channelId);
    		return wrSocket.get();
    	}
    	log.debug("getWebSocketWithId {} not found!", channelId);
    	return null;
    }

    public void terminateAllWsConnections() {
		for (WebSocket ws : getWebSockets()) {
			ws.close(WebSocket.ABNORMAL_CLOSE, "Unused Websocket connection");
		}
    }

	private class RT2WSChannelHasCorrespondingProxyCheckThread implements Runnable {
			
		@Override
		public void run() {
			Set<RT2ChannelId> newMarkedForRemove = new HashSet<>();
			Set<RT2ChannelId> docProxiesChannelId = docProxyRegistry.listAllDocProxies().stream().map(p -> p.getChannelId()).collect(Collectors.toSet());			
			for (WeakReference<RT2EnhDefaultWebSocket> wr : channels.values()) {
				RT2EnhDefaultWebSocket enhWs = wr.get();
				if (enhWs != null) {
					if (!docProxiesChannelId.contains(enhWs.getId())) {
						if (markedForRemove.contains(enhWs.getId())) {
							enhWs.close(WebSocket.ABNORMAL_CLOSE, "Unused Websocket connection");
						} else {
							newMarkedForRemove.add(enhWs.getId());
						}						
					}
				}
			}
			markedForRemove = newMarkedForRemove;
		}		
	}
}
