/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.ws;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.validation.constraints.NotNull;
import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.http.HttpRequestPacket;
import org.glassfish.grizzly.websockets.DataFrame;
import org.glassfish.grizzly.websockets.DefaultWebSocket;
import org.glassfish.grizzly.websockets.ProtocolHandler;
import org.glassfish.grizzly.websockets.WebSocket;
import org.glassfish.grizzly.websockets.WebSocketListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.tools.common.log.Loggable;

public class RT2EnhDefaultWebSocket extends DefaultWebSocket implements Loggable {

	private static final String HEADER_USER_AGENT = "user-agent";
	public static final String REQUEST_URL_PREFIX = "/rt2/v1/default/";

	private static final Logger log = LoggerFactory.getLogger(RT2EnhDefaultWebSocket.class);

	private final long timeSpanForNotify;

	private AtomicBoolean fatalErrorOnSend = new AtomicBoolean(false);
	private boolean available = true;
	private boolean unavailabeLogged = false;
	private final long creationTime = System.currentTimeMillis();
	private AdvisoryLockMode adivsoryLockMode = AdvisoryLockMode.ADVISORY_LOCK_MODE_NOT_ENABLED;

	private long lastRecvMsgTime = System.currentTimeMillis();

	private ByteArrayOutputStream fragementBuffer = null;

	private Boolean upgradeRequestSessionValid;

	private AtomicBoolean currentlyProcessingRequest = new AtomicBoolean(false);

	private RT2ChannelId channelId = null;

	private AtomicReference<String> userAgentDetected = new AtomicReference<>(null);

	public RT2EnhDefaultWebSocket(final ProtocolHandler protocolHandler, final HttpRequestPacket requestPacket,
			final WebSocketListener[] listeners, long pingFrequencyInMS, AdvisoryLockMode adivsoryLockMode) {
		super(protocolHandler, requestPacket, listeners);
		this.timeSpanForNotify = pingFrequencyInMS * 2;
		this.adivsoryLockMode = adivsoryLockMode;
		userAgentDetected.set(requestPacket.getHeader(HEADER_USER_AGENT));
	}

	public RT2EnhDefaultWebSocket(final ProtocolHandler protocolHandler, final HttpRequestPacket requestPacket,
			final WebSocketListener[] listeners, long pingFrequencyInMS, RT2ChannelId channelId, AdvisoryLockMode adivsoryLockMode) {
		super(protocolHandler, requestPacket, listeners);
		this.timeSpanForNotify = pingFrequencyInMS * 2;
		this.channelId = channelId;
		this.adivsoryLockMode = adivsoryLockMode;
		userAgentDetected.set(requestPacket.getHeader(HEADER_USER_AGENT));
	}

	public static RT2ChannelId getIdOfSocket(WebSocket socket) {
		DefaultWebSocket defWebSocket = (DefaultWebSocket) socket;
		if (defWebSocket instanceof RT2EnhDefaultWebSocket) {
			return ((RT2EnhDefaultWebSocket) defWebSocket).getId();
		}
		String requestURI = defWebSocket.getUpgradeRequest().getRequestURI();
		return new RT2ChannelId(requestURI.substring(requestURI.indexOf(REQUEST_URL_PREFIX)));
	}

	public RT2ChannelId getId() {
		if (channelId == null) {
			String requestURI = getUpgradeRequest().getRequestURI();
			this.channelId = new RT2ChannelId(requestURI.substring(requestURI.indexOf(REQUEST_URL_PREFIX)));
		}
		return channelId;
	}

	public AdvisoryLockMode getAdvisoryLockMode() {
		return adivsoryLockMode;
	}

	public String getUserAgent() {
		return userAgentDetected.get();
	}

	public void setUserAgentIfNotSet(@NotNull String userAgent) {
		userAgentDetected.compareAndSet(null, userAgent);
	}

	public boolean isAvaible() {
		return available;
	}

	public void setAvaible(boolean avaible) {
		if (this.available && !avaible) {
			this.unavailabeLogged = false;
		}
		this.available = avaible;
	}

	public long getCreationTime() {
		return this.creationTime;
	}

	public boolean isUnavailabeLogged() {
		return unavailabeLogged;
	}

	public void setUnavailabeLogged(boolean unavailabeLogged) {
		this.unavailabeLogged = unavailabeLogged;
	}

	public void setFatalErrorOnSend(boolean fatalErrorHappened) {
	    this.fatalErrorOnSend.set(fatalErrorHappened);
	}

	public boolean hasFatalErrorOnSendHappened() {
	    return this.fatalErrorOnSend.get();
	}

	public Boolean getUpgradeRequestSessionValid() {
		return upgradeRequestSessionValid;
	}

	public void setUpgradeRequestSessionValid(@NotNull Boolean upgradeRequestSessionValid) {
		this.upgradeRequestSessionValid = upgradeRequestSessionValid;
	}

	@Override
	public void onClose(DataFrame frame) {
		this.available = false;
		super.onClose(frame);
	}

	@Override
	public void onFragment(boolean last,  @NotNull byte[] fragment) {
		updateLastRecMsgTime();
		synchronized (this) {
			if (fragementBuffer == null) {
				fragementBuffer = new ByteArrayOutputStream();
			}
			try {
				fragementBuffer.write(fragment);
			} catch (IOException ex) {
				log.error(ex.getMessage(), ex);
				return;
			}
			if (last) {
				onMessage(new String(fragementBuffer.toByteArray()));
				fragementBuffer = null;
			}
		}
	}

	@Override
	public void onFragment(boolean last, String fragment) {
		onFragment(last, fragment.getBytes());
	}

	public void updateLastRecMsgTime() {
		this.lastRecvMsgTime = System.currentTimeMillis();
	}

	public long testIsAlive(long now) {
		long lastRecvDiff = now - lastRecvMsgTime;
		if (lastRecvDiff < timeSpanForNotify) {
			return 0l;
		}
		setAvaible(false);
		return lastRecvDiff;
	}

	/**
	 * Sends a ping to the other participant of the connection
	 * 
	 * @return A future if it was time to send the ping or null if no ping was send.
	 */
	@NotNull
	public GrizzlyFuture<DataFrame> sendPing() {

		final String sPing = RT2MessageFactory.toJSONString(RT2MessageFactory.newPing());
		log.trace("send pingl [{}]: {}", getId(), sPing);
		send(sPing);
		return null;
	}

    @NotNull
	public GrizzlyFuture<DataFrame> send(@NotNull RT2Message rt2Message) {
		final String sResponse = RT2MessageFactory.toJSONString(rt2Message);
		return send(sResponse);
	}

	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	@NotNull
	public Map<String, Object> getAdditionalLogInfo() {
		final Map<String, Object> res = new HashMap<>();
		res.put("channelId", getId());
		return res;
	}

	public void setCurrentlyProcessingRequest(boolean value) {
		currentlyProcessingRequest.set(value);
	}

	public boolean isCurrentlyProcessingRequest() {
		return currentlyProcessingRequest.get();
	}
}
