/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.TextStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.log.LogProperties;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.logging.IMessagesLoggable;
import com.openexchange.office.rt2.core.logging.IMessagesObjectManager;
import com.openexchange.office.rt2.core.logging.MessagesLogger;
import com.openexchange.office.rt2.core.osgi.RT2CoreContextAndActivator;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2AdminIdType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;

//=============================================================================
/** knows all open documents within the RT2 cluster ...
 *  It works internal with a cluster wide cache to hold it's state.
 */
@Service
@ShutdownOrder(value=-8)
@RegisteredService
@Async(initialDelay=RT2DocProxyRegistry.CHECK_UNAVAILABILITY_FREQUENCY, period=RT2DocProxyRegistry.CHECK_UNAVAILABILITY_FREQUENCY, timeUnit=TimeUnit.MILLISECONDS)
public class RT2DocProxyRegistry implements IMessagesObjectManager, InitializingBean, DisposableBean, OsgiBundleContextAware, Runnable {
    //-------------------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(RT2DocProxyRegistry.class);

    //-------------------------------------------------------------------------
	public static final String PERSISTENCE_SUBSET_DOC_STATES = "doc.states";
	public static final long CHECK_UNAVAILABILITY_FREQUENCY = 30000;
	private static final Long[] NOTIFY_TIMEFRAMES = new Long[] {
        CHECK_UNAVAILABILITY_FREQUENCY,
	    CHECK_UNAVAILABILITY_FREQUENCY * 2,
	    CHECK_UNAVAILABILITY_FREQUENCY * 4,
	    CHECK_UNAVAILABILITY_FREQUENCY * 8,
	    CHECK_UNAVAILABILITY_FREQUENCY * 16,
	    CHECK_UNAVAILABILITY_FREQUENCY * 32
    };
	private static final int LASTINDEX = NOTIFY_TIMEFRAMES.length - 1;

    //-------------------------------------------------------------------------
    private final Map< String, RT2DocProxy > docProxyRegistry = new ConcurrentHashMap<>();
    private final Map<String, WeakReference< RT2DocProxy >> unavailableDocProxies = new ConcurrentHashMap<>();
    private final ScheduledExecutorService logExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2DocProxyRegistryLogger-%d").build());
    private OsgiBundleContextAndActivator bundleCtx;
    private AtomicBoolean shutdown = new AtomicBoolean(false);

    //-------------------------------------------------------------------------

    @Autowired
    private MetricRegistry metricRegistry;

    @Autowired
    private RT2ConfigService rt2ConfigService;

    @Autowired
    private RT2DocProxyStateHolderFactory docProxyStateHolderFactory;

    @Autowired
    private SpecialLogService specialLogService;

    @Autowired
    private SessionService sessionService;

	@Override
	public void afterPropertiesSet() throws Exception {
    	if (metricRegistry != null) {
	    	metricRegistry.register(MetricRegistry.name("DocProxyRegistry", "size"),
	    		new Gauge<Integer>() {

					@Override
					public Integer getValue() {
						return docProxyRegistry.size();
					}
	    		});
    	}
    	logExecutor.scheduleAtFixedRate(new MessagesLogger(this, log), 3, 3, TimeUnit.MINUTES);
	}

	//-------------------------------------------------------------------------
    @Override
	public void destroy() throws Exception {
        log.debug("RT2DocProxyRegistry shutdown - will close all DocProxy instances registered on this node.");
        shutdown.set(true);

        shutdownDocProxies();

        clear();

        logExecutor.shutdown();
	}

    //-------------------------------------------------------------------------
    public void clear() {
        docProxyRegistry.clear ();
    }

    //-------------------------------------------------------------------------
    /** look for an existing doc proxy instance or create new one otherwise.
     *
     *  @param  clientUID [IN]
     *          the unique ID of the client.
     *
     *  @param  docUID [IN]
     *          the unique ID of the document.
     *
     *  @return a doc proxy instance always.
     * @throws RT2Exception
     * @throws RT2TypedException
     */
    public RT2DocProxy getOrCreateDocProxy(final RT2Message rt2Msg, boolean goOnlineIfNew, RT2ChannelId channelId, RT2SessionIdType sessionId) throws RT2TypedException, RT2Exception {
        synchronized (docProxyRegistry) {
            final String proxyID = RT2DocProxy.calcID(rt2Msg.getClientUID(), rt2Msg.getDocUID());
            RT2DocProxy docProxy = docProxyRegistry.get(proxyID);

            if (docProxy == null) {
                RT2DocProxyStateHolder docProxyStateHolder = docProxyStateHolderFactory.createDocProxyStateHolder(rt2Msg);
                docProxy = new RT2DocProxy(rt2Msg.getClientUID(), rt2Msg.getDocUID(), channelId, docProxyStateHolder, rt2Msg.getSessionID());
                logDocProxyCreationData(docProxy.getID(), rt2Msg, channelId, sessionId);
                bundleCtx.injectDependencies(docProxy);
                docProxy.setContextAndActivator((RT2CoreContextAndActivator) bundleCtx);
                docProxyRegistry.put(proxyID, docProxy);
                if (goOnlineIfNew) {
                    docProxy.goOnline();
                }
                return docProxy;
            } else {
                docProxy.setSessionId(sessionId);
            }

            log.debug("use existing doc proxy {}", docProxy);
            return docProxy;
        }
    }

    //-------------------------------------------------------------------------
    public void registerDocProxy(final RT2DocProxy docProxy) {
    	docProxyRegistry.put(docProxy.getProxyID(), docProxy);
    }

    //-------------------------------------------------------------------------
    /** deregister the given doc proxy from this registry ...
     *
     *  @param aDocProxy [IN]
     *         the proxy to be deregistered here.
     */
    public void deregisterDocProxy (final RT2DocProxy aDocProxy, boolean force) {
    	List<String> msgsLogs = aDocProxy.formatMsgsLogInfo();
    	if (!msgsLogs.isEmpty()) {
	    	TextStringBuilder builder = new TextStringBuilder(msgsLogs.get(0));
	    	msgsLogs.remove(0);
	    	msgsLogs.forEach(s -> builder.appendln("    " + s));
	    	log.info("deregister doc proxy {}, {}...", aDocProxy.getProxyID(), builder);
    	} else {
    		log.info("deregister doc proxy {}...", aDocProxy.getProxyID());
    	}
        // remove proxy from registry only
        // do not call any methods on proxy : no clear, no dispose, ... !!!
        docProxyRegistry.remove(aDocProxy.getProxyID());

        // unmark it to remove it from the unavailable list
        unmarkDocProxyAsUnavailable(aDocProxy);
    }

    //-------------------------------------------------------------------------
    /** @return the doc proxy instance bound to the given ID (if it exists - null otherwise)
     *
     *  @param sProxyID []
     *          the ID of the requested doc proxy object.
     */
    public RT2DocProxy getDocProxy (final RT2CliendUidType sClientUID, final RT2DocUidType sDocUID) {
        final String sProxyID  = RT2DocProxy.calcID(sClientUID, sDocUID);
        final RT2DocProxy aDocProxy = docProxyRegistry.get(sProxyID);
        return aDocProxy;
    }

    //-------------------------------------------------------------------------
    public RT2DocProxy getDocProxy (String proxyId) {
        final RT2DocProxy aDocProxy = docProxyRegistry.get(proxyId);
        return aDocProxy;
    }

    //-------------------------------------------------------------------------
    public void markDocProxyAsUnavailable(RT2DocProxy docProxy) {
    	unavailableDocProxies.put(docProxy.getID(), new WeakReference<RT2DocProxy>(docProxy));
    }

    //-------------------------------------------------------------------------
    public void unmarkDocProxyAsUnavailable(RT2DocProxy docProxy) {
    	unavailableDocProxies.remove(docProxy.getID());
    }

    //-------------------------------------------------------------------------
    /** @return the a list of doc proxy instances bound to the given document
     *          Can be an empty list - but wont be null.
     *
     *  @param  sDocUID [IN]
     *          the UID of the document where all proxy instances are searched for.
     */
    public List< RT2DocProxy > getDocProxies4DocUID (final RT2DocUidType sDocUID, final RT2SessionIdType sessionId) {
        final String sIDMatch  = RT2DocProxy.calcIDMatch4DocUID(sDocUID.getValue());
        synchronized (docProxyRegistry) {
	        Set<RT2DocProxy> allDocProxies = new HashSet<>(docProxyRegistry.values());
	        List< RT2DocProxy > rt2DocProxies = allDocProxies.stream().filter(p -> p.getProxyID().matches(sIDMatch)).collect(Collectors.toList());
	        if (sessionId != null) {
	        	rt2DocProxies = rt2DocProxies.stream().filter(p -> p.getSessionId().equals(sessionId)).collect(Collectors.toList());
	        }
	        return rt2DocProxies;
        }
    }

    //-------------------------------------------------------------------------
    public List< RT2DocProxy > listAllDocProxies () {
        final List< RT2DocProxy > res  = new ArrayList<> ();
        res.addAll(docProxyRegistry.values());
        return res;
    }

    //-------------------------------------------------------------------------
	@Override
	public Collection<IMessagesLoggable> getManagedCollection() {
		return new HashSet<>(docProxyRegistry.values());
	}

    //-------------------------------------------------------------------------
    public RT2AdminIdType generateAdminID () {
        final String        sNodeID     = rt2ConfigService.getOXNodeID();
        String        sNormalized = sNodeID;

        sNormalized = StringUtils.replace(sNormalized, ":" , "-");
        sNormalized = StringUtils.replace(sNormalized, ";" , "-");
        sNormalized = StringUtils.replace(sNormalized, "/" , "-");
        sNormalized = StringUtils.replace(sNormalized, "\\", "-");
        sNormalized = StringUtils.replace(sNormalized, "\"", "-");
        sNormalized = StringUtils.replace(sNormalized, "'" , "-");
        sNormalized = StringUtils.replace(sNormalized, " " , "-");
        return new RT2AdminIdType(sNormalized);
    }

    //-------------------------------------------------------------------------
	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}

	//-------------------------------------------------------------------------
	@Override
	public void run() {
		if (!shutdown.get()) {
			try {
				final Iterator<Entry< String, WeakReference< RT2DocProxy >>> iter = unavailableDocProxies.entrySet().iterator();
				while (iter.hasNext()) {
					final RT2DocProxy docProxy = iter.next().getValue().get();
					if (docProxy != null) {
						final long unavailTime = docProxy.getUnavailabilityTimeInMS();
						final long offlineTime = docProxy.getOfflineTimeInMS();
						final long lastNotifiedUnavailTime = docProxy.getLastNotifiedUnavailabilityTimeInMS();
						final long notifyUnavailTime = determineNotifyUnavailabilityTime(docProxy, lastNotifiedUnavailTime, unavailTime, offlineTime);
						if (notifyUnavailTime > 0) {
							log.debug("RT2DocProxyRegistry: Notify unavailability time {} - unavail {}, offline {}, lastNotified {}", notifyUnavailTime, unavailTime, offlineTime, lastNotifiedUnavailTime);
							docProxy.notifyUnavailibility(Direction.INTERNAL, notifyUnavailTime);
						}
					} else {
						iter.remove();
					}
				}
			} catch (Throwable t) {
	            com.openexchange.exception.ExceptionUtils.handleThrowable(t);
	            log.error("RT2: Exception caught trying to check offline doc proxies for missing unavailable notifications", t);
			}
		}
	}

	//-------------------------------------------------------------------------
    private void shutdownDocProxies () {
    	synchronized (docProxyRegistry) {
	        for (RT2DocProxy aDocProxy : docProxyRegistry.values()) {
	            try {
	                aDocProxy.closeHard(Cause.GARBAGE_COLLECTING);
	            } catch (final Throwable exIgnore) {
	                ExceptionUtils.handleThrowable(exIgnore);
	                log.error("... closeHard for doc proxy [{}] failed", aDocProxy);
	            }
	        }
    	}
    }

	//-------------------------------------------------------------------------
    private long determineNotifyUnavailabilityTime(RT2DocProxy docProxy, long lastNotifiedTime, long unavailTime, long offlineTime) throws Exception {
    	final long maxUnavailableTime = Math.max(unavailTime, offlineTime);

    	if ((maxUnavailableTime > lastNotifiedTime) && (lastNotifiedTime < NOTIFY_TIMEFRAMES[LASTINDEX])) {
			for (int i=0; i < NOTIFY_TIMEFRAMES.length; i++) {
				final long checkTimeFrame = NOTIFY_TIMEFRAMES[i];
				if ((checkTimeFrame > lastNotifiedTime)  &&
					(((maxUnavailableTime >= checkTimeFrame) && (i < LASTINDEX) && (NOTIFY_TIMEFRAMES[i+1] > maxUnavailableTime)) ||
					 ((i == LASTINDEX) && (maxUnavailableTime >= checkTimeFrame)))) {
					return checkTimeFrame;
				} else if (maxUnavailableTime < NOTIFY_TIMEFRAMES[i]) {
					return -1;
				}
			}
    	}
    	return -1;
    }

    //-------------------------------------------------------------------------
    private void logDocProxyCreationData(String rt2DocProxyId, RT2Message rt2JoinMsg, RT2ChannelId channelId, RT2SessionIdType sessionId) {
        String fileId = RT2MessageGetSet.getOptionalHeader(rt2JoinMsg, RT2Protocol.HEADER_FILE_ID, "unkown"); // ensure that no exception is thrown if file-id is not available
        Integer contextId = 0;
        Integer userId = 0;
        try {
            Session session = sessionService.getSession4Id(sessionId.getValue());
            if (session != null) {
                contextId = session.getContextId();
                userId = session.getUserId();
            }
        } catch (OXException e) {
            log.info("Exception caught trying to determine context and user-id for session", e);
        }

        try {
            MDCHelper.safeMDCPut(LogProperties.Name.SESSION_CONTEXT_ID.getName(), Integer.toString(contextId));
            MDCHelper.safeMDCPut(LogProperties.Name.SESSION_USER_ID.getName(), Integer.toString(userId));
            MDCHelper.safeMDCPut(LogProperties.Name.SESSION_SESSION_ID.getName(), sessionId.getValue());
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, rt2JoinMsg.getDocUID().getValue());
            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, rt2JoinMsg.getClientUID().getValue());
            log.info("Created Doc Proxy instance {} associated with channel-uid {}, com.openexchange.rt2.client.uid {} for com.openexchange.session.contextId {}, com.openexchange.session.userId {} with com.openexchange.session.sessionId {} and com.openexchange.rt2.document.uid {} for file-id {}", rt2DocProxyId, channelId, rt2JoinMsg.getClientUID(), contextId, userId, sessionId, rt2JoinMsg.getDocUID(), fileId);
        } finally {
            MDC.remove(LogProperties.Name.SESSION_CONTEXT_ID.getName());
            MDC.remove(LogProperties.Name.SESSION_USER_ID.getName());
            MDC.remove(LogProperties.Name.SESSION_SESSION_ID.getName());
            MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
        }
    }

}
