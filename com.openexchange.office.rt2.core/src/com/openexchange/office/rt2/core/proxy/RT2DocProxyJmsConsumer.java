/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.openexchange.office.rt2.core.jms.RT2JmsDestination;
import com.openexchange.office.rt2.core.metric.DocProxyResponseMetricService;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageType;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessagePostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-5)
public class RT2DocProxyJmsConsumer implements MessageListener, JmsMessageListener, InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(RT2DocProxyJmsConsumer.class);

    private String nodeUUID;

	//--------------------------Services--------------------------------------
    @Autowired
    private RT2DocProxyRegistry rt2DocRegistry;

    @Autowired
    private DocProxyResponseMetricService docProxyResponseMetricService;

    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;

    @Autowired
    private ClusterService clusterService;

	//------------------------------------------------------------------------

    private Map<String, Long> receivedMessages = new HashMap<>();

    private Thread cleanupThread;

    private DefaultMessageListenerContainer msgListenerCont;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.nodeUUID = clusterService.getLocalMemberUuid();
	}

	//-------------------------------------------------------------------------
	@Override
	public void startReceiveMessages() {
        if (msgListenerCont == null) {
            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);
            msgListenerCont.setConnectionFactory(pooledConnectionFactoryProxy.getPooledConnectionFactory());
            msgListenerCont.setConcurrentConsumers(3);
            msgListenerCont.setDestination(RT2JmsDestination.clientResponseTopic);
            msgListenerCont.setMaxConcurrentConsumers(3);
            msgListenerCont.setPubSubDomain(true);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("RT2DocProcessorJmsConsumer-%d").build()));
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.start();

            cleanupThread = new Thread(new CleanupThread());
            cleanupThread.setName(cleanupThread.getClass().getName());
            cleanupThread.setDaemon(true);
            cleanupThread.start();
        }
	}

	//-------------------------------------------------------------------------    
    @Override
	public void destroy() throws Exception {
        if (msgListenerCont != null) {
            msgListenerCont.destroy();
            msgListenerCont = null;
        }
        if (cleanupThread != null) {
            cleanupThread.interrupt();
            try {
                cleanupThread.join(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            cleanupThread = null;
        }
	}

	//-------------------------------------------------------------------------
    @Override
    public void onMessage(Message jmsMsg) {
    	RT2Message rt2Msg = null;
        try {
            synchronized (receivedMessages) {
                if (receivedMessages.containsKey(jmsMsg.getJMSMessageID())) {
                    return;
                }
                receivedMessages.put(jmsMsg.getJMSMessageID(), jmsMsg.getJMSTimestamp());
            }
        	if (jmsMsg.getBooleanProperty(RT2MessagePostProcessor.HEADER_GPB_MSG)) {
        		BytesMessage byteMsg = (BytesMessage) jmsMsg;
        		byte [] data = new byte[(int)byteMsg.getBodyLength()];
        		byteMsg.readBytes(data);        		
        		BroadcastMessage broadcastMessage = BroadcastMessage.parseFrom(data);
        		for (BroadcastMessageReceiver broadcastMessageReceiver : broadcastMessage.getReceiversList()) {
        			rt2Msg = RT2MessageFactory.newBroadcastMessage(getRT2MessageTypeOfGpbMessage(broadcastMessage.getMsgType()), new RT2DocUidType(broadcastMessage.getDocUid().getValue()));
        			rt2Msg.setMessageID(new RT2MessageIdType(broadcastMessage.getMessageId().getValue()));
        			rt2Msg.setBodyString(broadcastMessage.getBody().getValue());
        			rt2Msg.setClientUID(new RT2CliendUidType(broadcastMessageReceiver.getReceiver().getValue()));
        			if (broadcastMessageReceiver.getSeqNr().getValue() >= 0) {
        				rt2Msg.setSeqNumber(new RT2SeqNumberType(broadcastMessageReceiver.getSeqNr().getValue()));
        			}
        			if (StringUtils.isNotEmpty(broadcastMessage.getErrorCode().getValue())) {
        				rt2Msg.setError(new RT2ErrorCodeType(getErrorCode(broadcastMessage.getErrorCode())));
        			}
        			onRT2Message(jmsMsg, rt2Msg);
        		}
        	} else {
        		rt2Msg = RT2MessageFactory.fromJmsMessage(jmsMsg);
        		onRT2Message(jmsMsg, rt2Msg);
        	}
        }
       catch (Exception ex) {
    	   if (rt2Msg != null) {
    		   RT2DocProxy rt2DocProxy = rt2DocRegistry.getDocProxy(rt2Msg.getClientUID(), rt2Msg.getDocUID());
    		   if (rt2DocProxy != null) {
    			   log.error(ex.getMessage() + ", msgs: " + rt2DocProxy.formatMsgsLogInfo(), ex);
    		   } else {
    			   log.error(ex.getMessage(), ex);
    		   }
    	   } else {
    		   log.error(ex.getMessage(), ex);
    	   }
       } finally {
    	   MDC.clear();
       }
    }

	private RT2MessageType getRT2MessageTypeOfGpbMessage(MessageType msgType) {
		switch (msgType) {
			case BROADCAST_CRASHED: return RT2MessageType.BROADCAST_CRASHED;
			case BROADCAST_EDITREQUEST_STATE: return RT2MessageType.BROADCAST_EDITREQUEST_STATE;
			case BROADCAST_HANGUP: return RT2MessageType.BROADCAST_HANGUP;
			case BROADCAST_RENAMED_RELOAD: return RT2MessageType.BROADCAST_RENAMED_RELOAD;
			case BROADCAST_SHUTDOWN: return RT2MessageType.BROADCAST_SHUTDOWN;
			case BROADCAST_UPDATE: return RT2MessageType.BROADCAST_UPDATE;
			case BROADCAST_UPDATE_CLIENTS: return RT2MessageType.BROADCAST_UPDATE_CLIENTS;
			case BROADCAST_OT_RELOAD: return RT2MessageType.BROADCAST_OT_RELOAD;
			default: throw new RuntimeException("Not a broadcast message type: " + msgType);
		}
	}

	private ErrorCode getErrorCode(com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ErrorCodeType errorCodeType) {
		ErrorCode errorCode = ErrorCode.NO_ERROR;

		final String strErrorCode = errorCodeType.getValue();
		try {
			errorCode = ErrorCode.createFromJSONObject(new JSONObject(strErrorCode), ErrorCode.NO_ERROR);
		} catch (JSONException e) {
			log.error("JSONException caught trying to extract erorr code from broadcast", e);
		}
		return errorCode;
	}

    private void onRT2Message(Message jmsMsg, RT2Message rt2Msg) throws JMSException {
    	
        docProxyResponseMetricService.startTimer(rt2Msg.getMessageID());
        try {
	        if (rt2Msg.getDocUID() != null) {
	        	MDCHelper.safeMDCPut(MDCEntries.DOC_UID, rt2Msg.getDocUID().getValue());
	        }
	        if (rt2Msg.getClientUID() != null) {
	        	MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, rt2Msg.getClientUID().getValue());
	        }
	        MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "processor");
	        MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, nodeUUID);
	        MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, rt2Msg.getType().getValue());
	        
	        RT2DocProxy rt2DocProxy = rt2DocRegistry.getDocProxy(rt2Msg.getClientUID(), rt2Msg.getDocUID());
	        log.debug("Received ClientDoc-Msg: {}", rt2Msg);
	
	        if (rt2DocProxy == null) {
	            log.debug("No doc proxy found for com.openexchange.rt2.client.uid: {} and com.openexchange.rt2.document.uid: {}", rt2Msg.getClientUID(), rt2Msg.getDocUID());
	            for (RT2DocProxy rt2DocProxyTmp : rt2DocRegistry.listAllDocProxies()) {
	                log.debug("com.openexchange.rt2.client.uid: {}, com.openexchange.rt2.document.uid: {}", rt2DocProxyTmp.getClientUID(), rt2DocProxyTmp.getDocUID());
	            }
	        } else {
	            log.debug("Found RT2DocProxy: {}", rt2DocProxy);
	            rt2DocProxy.handleResponse(rt2Msg);
	        }
        } finally {
        	MDC.clear();
		}
    }

    //-------------------------------------------------------------------------
    private class CleanupThread implements Runnable {

        @Override
        public void run() {
            boolean stop = false;
            while (!stop) {
                try {
                   MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "CleanupThread");
                   MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, nodeUUID);
                    Thread.sleep(60000);
                    final Set<String> toRemove = new HashSet<>();
                    final long compareTime = System.currentTimeMillis() - 600000;

                    synchronized (receivedMessages) {
                        receivedMessages.forEach((k, v) -> { if (v < compareTime) toRemove.add(k);});
                        toRemove.forEach(s -> receivedMessages.remove(s));
                    }
                } catch (InterruptedException ex) {
                    stop = true;
                    Thread.currentThread().interrupt();
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                } finally {
                	MDC.clear();
				}
            }
        }
    }
}
