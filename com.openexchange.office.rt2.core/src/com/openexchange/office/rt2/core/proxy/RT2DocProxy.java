/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.RT2Constants;
import com.openexchange.office.rt2.core.RT2GarbageCollector;
import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.logging.IMessagesLoggable;
import com.openexchange.office.rt2.core.osgi.RT2CoreContextAndActivator;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap.DistributedDocInfoStatus;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.Loggable;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

//=============================================================================
public class RT2DocProxy implements Loggable, IMessagesLoggable {

    public static final long TIMEOUT_HARD_CLOSE = 30000;

    //-------------------------------------------------------------------------

    private static final Logger log = LoggerFactory.getLogger(RT2DocProxy.class);

    private final RT2CliendUidType clientUID;
    private final RT2DocUidType docUID;
    private final RT2ChannelId channelId;
    private final String proxyID;
    private final long creationTime;

    //---------------------------------Services--------------------------------

    @Autowired
    private RT2DocProxyRegistry docProxyRegistry;

    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;

    @Autowired
    private RT2NodeInfoService rt2NodeInfo;

    @Autowired
    private RT2JmsMessageSender jmsMessageSender;

    @Autowired
    private RT2WSChannelDisposer channelDisposer;

    @Autowired
    private MessageCounters messageCounters;

    @Autowired
    private RT2DocProcessorManager docProcessorManager;

    @Autowired
    private RT2GarbageCollector garbageCollector;

    @Autowired
    private RT2DocOnNodeMap rt2DocOnNodeMap;

    @Autowired
    private DocProxyMessageLoggerService docProxyMessageLoggerService;

    @Autowired
    private ClusterLockService clusterLockService;

	@Autowired
	private ClusterService clusterService;

    private RT2CoreContextAndActivator contextAndActivator;

    //-------------------------------------------------------------------------
    protected RT2DocProxyStateHolder docProxyStateHolder;

    protected RT2DocProxyConnectionStateHolder docProxyConnectionStateHolder;

    //-------------------------------------------------------------------------
    private CountDownLatch m_bJoinLeaveSyncPoint = new CountDownLatch(1);

    //-------------------------------------------------------------------------
    private CountDownLatch m_LeaveSyncPoint = new CountDownLatch(1);

    private final AtomicReference<RT2SessionIdType> sessionId;

    private Map<RT2MessageIdType, CompletableFuture<RT2Message>> waitingForResponseDuringCloseHard = Collections.synchronizedMap(new HashMap<>());

    private boolean throwExceptionOnLeave = false;

    //-------------------------------------------------------------------------
    public RT2DocProxy (RT2CliendUidType clientUID, RT2DocUidType docUID, RT2ChannelId channelId,
    					RT2DocProxyStateHolder docProxyStateHolder, RT2SessionIdType sessionId) {
        this.clientUID = clientUID;
        this.docUID = docUID;
        this.proxyID = calcID(clientUID, docUID);
        this.channelId = channelId;
        this.creationTime = System.currentTimeMillis();
        this.docProxyStateHolder = docProxyStateHolder;
        this.sessionId = new AtomicReference<RT2SessionIdType>(sessionId);
        this.docProxyConnectionStateHolder = new RT2DocProxyConnectionStateHolder(clientUID, docUID);
    }

    //-------------------------------------------------------------------------
    public static String calcID(RT2CliendUidType sClientUID, RT2DocUidType sDocUID) {
    	return sClientUID.getValue() + "_" + sDocUID.getValue();
    }

    //-------------------------------------------------------------------------
    public static String calcIDMatch4DocUID (final String sDocUID) {
        return "(.*)("+sDocUID+")($)";
    }

    //-------------------------------------------------------------------------
    public String getID() {
    	return proxyID;
    }

	//-------------------------------------------------------------------------
    public void addMsgFromWebSocket(RT2Message msg) {
    	docProxyMessageLoggerService.addMessageForQueueToLog(Direction.FROM_JMS, msg);
   		docProxyConnectionStateHolder.goOnlineIfOffline();
    }

    //-------------------------------------------------------------------------
    public void closeHard (Cause closeHardCause) throws RT2Exception
    {
    	docProxyMessageLoggerService.add(getDocUID(), Direction.CLOSE_HARD, "closeHard", getClientUID());
        final RT2Message closeLeaveMsg = RT2MessageFactory.newLeaveAndCloseRequest(clientUID, docUID, closeHardCause);
        log.debug("RT2: hard-close : wait for the results for proxy with id ''{}''...", getProxyID(), new Exception("DocProxy::closeHard"));
        CompletableFuture<RT2Message> resultFtrOfCloseLeaveMsg = new CompletableFuture<RT2Message>();
        resultFtrOfCloseLeaveMsg.thenAccept(msg -> {
        	this.messageCounters.incCloseResponseCounter();
        	handleFinalLeave(msg);
        	this.messageCounters.incLeaveResponseCounter();
        	log.debug("RT2: hard-close : got response for proxy with id ''{}''...", getProxyID());
        });
        waitingForResponseDuringCloseHard.put(closeLeaveMsg.getMessageID(), resultFtrOfCloseLeaveMsg);
        sendRequest(closeLeaveMsg);

        long waitTime = Long.parseLong(System.getProperty("TIMEOUT_HARD_CLOSE", Long.toString(TIMEOUT_HARD_CLOSE)));
        RT2Message responseMsg = null;
        try {
			responseMsg = resultFtrOfCloseLeaveMsg.get(waitTime, TimeUnit.MILLISECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			log.warn("RT2: hard-close : InterruptedException : result of closeAndLeave-operation ''{}'' for docProxy with id ''{}'' is undefined !", closeLeaveMsg, getProxyID());
		}
        if (responseMsg == null) {
        	log.warn("RT2: hard-close : timeout : result of operation ''{}'' for docProxy with id ''{}'' is undefined !", closeLeaveMsg, getProxyID());
        }

        log.debug("RT2: hard-close for docProxy with id ''{}'': DONE", getProxyID());
    }

     //-------------------------------------------------------------------------
    public void join (final RT2Message aJoinRequest) throws Exception {
        LogMethodCallHelper.logMethodCall(this, "join", aJoinRequest.getHeader());
        this.messageCounters.incJoinRequestCounter();

        RT2MessageGetSet.setNodeUUID(aJoinRequest, clusterService.getLocalMemberUuid());

        // Tricky part :
        // If this instance is already in "shutdown mode" ...
        // we need to sync with that operation.
        // Otherwise some resources (as e.g. camel routes) will be closed ...
        // during our try to join/open a same document within this instance.

        // IMPORTANT to call this BEFORE switch into new proxy state !
        syncJoinWithLeaveIfNeeded ();

        boolean triggerGc = false;

        final ClusterLock clusterLock = clusterLockService.getLock(docUID);
        boolean locked = false;

        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUID.getValue());
        MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUID.getValue());

        try {
	        log.debug("Lock call by com.openexchange.rt2.client.uid [{}] for com.openexchange.rt2.document.uid [{}]", clientUID, docUID);
        	locked = clusterLock.lock();
        	if (locked) {
        	    log.debug("Lock acquired");
		        docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_IN_JOIN);

		        Set<RT2CliendUidType> remainingClients = distributedDocInfoMap.addClient(docUID, clientUID);
		        int clientRefCount = remainingClients.size();
		        final boolean bIsFirstJoin = (clientRefCount == 1L) && remainingClients.contains(clientUID);

		        if (bIsFirstJoin) {
		        	log.info("RT2: join context - accepted : com.openexchange.rt2.client.uid {} is first", clientUID.getValue());
		        	rt2NodeInfo.registerDocOnNode(docUID);
		        }
		        else {
		        	if (DistributedDocInfoStatus.RUNNING.equals(distributedDocInfoMap.getStatus(docUID))) {
		        		String nodeId = rt2DocOnNodeMap.get(docUID.getValue());
		        		if (nodeId == null) {
		        			log.warn("RT2: join context - clientRefCount is greater 0 but rt2DocOnNodeMap has no entry for com.openexchange.rt2.document.uid {}", docUID);
		        			triggerGc = true;
		        		} else {
		        			log.info("RT2: join context - accepted : ref-count {} and doc-processor located at node-uuid {}", clientRefCount, nodeId);
		        		}
		        	}
		        	else {
			            // The ref-count value indicates that the document is part of a clean-up
			            // process, therefore we need to bail-out now with an error. The ref-count
			            // will be reset by the clean-up process, therefore we don't need to decrement
			            // it here (see RT2NodeHealthMonitor/MasterCleanupTask/CleanupTask).
			            //
			            // We have to make sure that the ref-count will be removed in a defined time-frame even
			            // if the clean-up process was not completed. Therefore the ref-count will be also
			            // registered at the garbage collector service for verification and alternative
			            // clean-up.
			        	garbageCollector.registerDocRefCountForRemovalVerification(docUID);

			            log.info("RT2: join context - detected ref-count {} set by gc process - join must be aborted", clientRefCount);
			            if (clientRefCount > RT2Constants.REF_COUNT_TRESHHOLD) {
			            	throw new RT2TypedException(ErrorCode.GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR, getMsgsAsList());
			            } else {
			           		throw new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, getMsgsAsList());
			            }
		        	}
		        }
        	} else {
            	// we didn't get the lock -> bail out with an error
           		throw new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, getMsgsAsList());
        	}
        } finally {
        	if (locked) {
                log.debug("Unlock call by com.openexchange.rt2.client.uid [{}] for com.openexchange.rt2.document.uid [{}]", clientUID, docUID);
                clusterLock.unlock();
                log.debug("Lock released");
        	}
        	MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
        }

        if (triggerGc) {
            log.info("RT2: join - trigger gc for doc-uid {}", docUID);
        	if (!garbageCollector.doGcForDocUid(docUID, true)) {
        		throw new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, getMsgsAsList());
        	}
        	join(aJoinRequest);
        	return;
        }
        // forward join request to doc processor (which might exist on a different node).
        log.info("RT2: send join_request via jms message sender");
        sendRequest (aJoinRequest);
    }

    //-------------------------------------------------------------------------
    public void leave (final RT2Message aLeaveRequest) throws RT2Exception {
        LogMethodCallHelper.logMethodCall(this, "leave", aLeaveRequest.getHeader());
        this.messageCounters.incLeaveRequestCounter();
       	docProxyStateHolder.checkDocumentAlreadyDisposed(getMsgsAsList());

        Boolean bForce = RT2MessageGetSet.getInternalHeader(aLeaveRequest, RT2Protocol.HEADER_AUTO_CLOSE, false) ||
                aLeaveRequest.isType(RT2MessageType.REQUEST_EMERGENCY_LEAVE);
        docProxyStateHolder.setForceQuit(bForce);
        docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_IN_LEAVE);
        sendRequest(aLeaveRequest);
        if (throwExceptionOnLeave) {
        	log.error("Test exception");
        }
    }

    //-------------------------------------------------------------------------
    public void close (final RT2Message aCloseRequest) throws RT2Exception
    {
        LogMethodCallHelper.logMethodCall(this, "close", aCloseRequest.getHeader());
        this.messageCounters.incCloseRequestCounter();
       	docProxyStateHolder.checkDocumentAlreadyDisposed(getMsgsAsList());
        docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_IN_CLOSE);
        sendRequest(aCloseRequest);
    }

    //-------------------------------------------------------------------------
    public void open (final RT2Message aOpenRequest)
        throws Exception
    {
        LogMethodCallHelper.logMethodCall(this, "open", aOpenRequest.getHeader());
        this.messageCounters.incOpenRequestCounter();
        docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_IN_OPEN);
        sendRequest (aOpenRequest);
    }

    //-------------------------------------------------------------------------
    public void abortOpen (final RT2Message aAbortOpenRequest) throws RT2TypedException
    {
    	LogMethodCallHelper.logMethodCall(this, "abortOpen", aAbortOpenRequest.getHeader());
    	this.messageCounters.incAbortCounter();
   		docProxyStateHolder.checkDocumentAlreadyDisposed(getMsgsAsList());
       	docProxyStateHolder.setAbortOpen(true);
        sendRequest (aAbortOpenRequest);
    }

	//-------------------------------------------------------------------------
	public RT2SessionIdType getSessionId() {
		return sessionId.get();
	}

	//-------------------------------------------------------------------------
	public void setSessionId(RT2SessionIdType sessionId) {
		this.sessionId.set(sessionId);
	}

	//-------------------------------------------------------------------------
    public long getOfflineTimeInMS () {
    	return docProxyConnectionStateHolder.getOfflineTimeInMS();
    }

	//-------------------------------------------------------------------------
    public void setOfflineTimeInMS(long offlineTime) {
    	docProxyConnectionStateHolder.setOfflineTimeInMS(offlineTime);
    }

    //-------------------------------------------------------------------------
    public EDocProxyState getDocState() {
        return docProxyStateHolder.getProxyState();
    }

    //-------------------------------------------------------------------------
    public long getCreationTimeInMS() {
        return creationTime;
    }

    //-------------------------------------------------------------------------
    public RT2ChannelId getChannelId() {
		return channelId;
	}

	//-------------------------------------------------------------------------
    public boolean isOnline() {
    	return docProxyConnectionStateHolder.isOnline();
    }

    //-------------------------------------------------------------------------
    public long getUnavailabilityTimeInMS() {
    	return docProxyConnectionStateHolder.getUnavailableTimeInMS();
    }

    //-------------------------------------------------------------------------
    public long getLastNotifiedUnavailabilityTimeInMS() {
    	return docProxyConnectionStateHolder.getLastNotifiedUnavailableTime();
    }

    //-------------------------------------------------------------------------
    public void goOffline () {
    	if (docProxyConnectionStateHolder.goOfflineIfOnline()) {
    		docProxyMessageLoggerService.add(docUID, Direction.INTERNAL, "goOffline", clientUID);
	        docProxyRegistry.markDocProxyAsUnavailable(this);
    	}
    }

    //-------------------------------------------------------------------------
    public void goOnline () {
    	if (docProxyConnectionStateHolder.goOnlineIfOffline()) {
    		docProxyMessageLoggerService.add(docUID, Direction.INTERNAL, "goOnline", clientUID);
	    	log.debug("RT2: doc proxy [{}"+getProxyID()+"] is offline : enable online mode. com.openexchange.rt2.client.uid [{}], com.openexchange.rt2.document.uid [{}]", getProxyID(), getClientUID(), getDocUID());
	        docProxyRegistry.unmarkDocProxyAsUnavailable(this);

	        final EDocProxyState proxyState = docProxyStateHolder.getProxyState();
	        if (proxyState != EDocProxyState.E_INIT) {
		        // send unavailability with unavail = 0 to enable possible deactivated client
		        try {
			        notifyUnavailibility(Direction.INTERNAL, 0);
		        } catch (Exception e) {
		        	log.error("Exception caught, trying to send unavailable notification to com.openexchange.rt2.client.uid [{}]", getClientUID(), e);
		        }
	        }
    	}
    }

    //-------------------------------------------------------------------------
    public void notifyUnavailibility(Direction direction, long nUnavailTime) throws Exception {
		docProxyMessageLoggerService.add(docUID, Direction.INTERNAL, "notifyUnavailibility", clientUID);
        LogMethodCallHelper.logMethodCall(this, "notifyUnavailibility", nUnavailTime);
        final RT2Message aUnavailRequest = RT2MessageFactory.newUnavailabilityRequest(getClientUID(), getDocUID(), new RT2UnavailableTimeType(nUnavailTime));
        docProxyConnectionStateHolder.setNotifiedUnavailbleTime(nUnavailTime);
        if (nUnavailTime == 0)
        	docProxyRegistry.unmarkDocProxyAsUnavailable(this);
        else
        	docProxyRegistry.markDocProxyAsUnavailable(this);
        sendRequest(aUnavailRequest);
    }

    //-------------------------------------------------------------------------
    public void sendCrashedResponseToClient () throws RT2Exception, RT2TypedException {
        LogMethodCallHelper.logMethodCall(this, "sendCrashedResponseToClient", docProxyStateHolder.isInShutdown());
        if (!contextAndActivator.isStopped() && !docProxyStateHolder.isInShutdown()) {
            final RT2Message aCrashedBroadcast = RT2MessageFactory.newBroadcastMessage(getClientUID(), getDocUID(), RT2MessageType.BROADCAST_CRASHED);
            forwardResponseToHandler(aCrashedBroadcast);
        }
    }

    //-------------------------------------------------------------------------
    public void handleResponse(RT2Message aResponse) {
    	docProxyMessageLoggerService.addMessageForQueueToLog(Direction.INTERNAL, aResponse);
        try
        {
            // count request/response pairs (if possible : e.g. broadcasts cant and has not to be counted)
            boolean bForwardResponseToHandler = true;
            switch (aResponse.getType()) {
                case RESPONSE_UNAVAILABILITY: bForwardResponseToHandler = false;
                    break;
                case RESPONSE_JOIN:
                	docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_JOINED);
                    log.info("RT2: DocProxy join_response for client-uid {} received", aResponse.getClientUID());
                	this.messageCounters.incJoinResponseCounter();
                    break;
                case RESPONSE_OPEN_DOC:
                	docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_OPEN);
                	this.messageCounters.incOpenResponseCounter();
                    break;
                case RESPONSE_CLOSE_DOC:
                	docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_CLOSED);
                	this.messageCounters.incCloseResponseCounter();
                    break;
                case RESPONSE_LEAVE:
                case RESPONSE_EMERGENCY_LEAVE:
                	handleFinalLeave(aResponse);
                	this.messageCounters.incLeaveResponseCounter();
                    break;
                case BROADCAST_SHUTDOWN: handleRemoteNodeShutdown (aResponse);
                    break;
                case RESPONSE_CLOSE_AND_LEAVE: bForwardResponseToHandler = false;
                	handleCloseAndLeaveMsg(aResponse);
                	break;
                default: /* nothing to do */;
            }

            if (bForwardResponseToHandler)
                forwardResponseToHandler   (aResponse);
        }
        catch (Throwable ex)
        {
            log.error("RT2: Exception caught in handleResponse. msgs: {}", this.formatMsgsLogInfo(), ex);
            ExceptionUtils.handleThrowable(ex);
            handleResponseErrors (aResponse, ex);
        }
    }

    //-------------------------------------------------------------------------
    public void sendRequest (final RT2Message aRequest) throws RT2TypedException
    {
        // For sync request check if proxy state is sufficient to handle it
        if (RT2MessageType.REQUEST_SYNC.equals(aRequest.getType()) && !docProxyStateHolder.isProxyInOpenState()) {
        	throw new RT2TypedException(ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR, getMsgsAsList());
        }
        jmsMessageSender.sendToDocumentQueue(aRequest, getMsgsAsList());
    }

    //-------------------------------------------------------------------------
    private void handleResponseErrors (final RT2Message aMessage, final Throwable  aEx) {
    	LogMethodCallHelper.logMethodCall(this, "handleResponseErrors", aMessage.getHeader(), aEx);
        try {
            final RT2Message aErrorResponse = RT2MessageFactory.cloneMessage(aMessage, RT2MessageType.RESPONSE_GENERIC_ERROR);
                  ErrorCode  aError         = ErrorCode.GENERAL_UNKNOWN_ERROR;

            if (RT2TypedException.class.isAssignableFrom(aEx.getClass ())) {
                final RT2TypedException aRT2Ex = (RT2TypedException) aEx;
                aError = aRT2Ex.getError();
            }
            RT2MessageGetSet.setError(aErrorResponse, aError);
            forwardResponseToHandler   (aErrorResponse);
        }
        catch (Throwable exIgnore) {
            ExceptionUtils.handleThrowable(exIgnore);
            log.error(exIgnore.getMessage() + ", proxyId: " + getProxyID(), exIgnore);
            // error in error (handling) ?
            // Do not do anything then log those error for debug purposes.
        }
    }

    //-------------------------------------------------------------------------
    public void handleFinalLeave (final RT2Message leaveResponse) {
      	docProxyRegistry.deregisterDocProxy(this, false);
        m_LeaveSyncPoint.countDown();
        log.debug("... unblock join/leave sync point");
        m_bJoinLeaveSyncPoint.countDown();
    }

    //-------------------------------------------------------------------------
    private void handleCloseAndLeaveMsg(final RT2Message closeAndLeaveMsg) {
    	CompletableFuture<RT2Message> future = waitingForResponseDuringCloseHard.get(closeAndLeaveMsg.getMessageID());
    	if (future == null) {
    		log.warn("No completeable future found for msg with id {}", closeAndLeaveMsg.getMessageID());
    	} else {
    		future.complete(closeAndLeaveMsg);
    	}
    }

    //-------------------------------------------------------------------------
    private void handleRemoteNodeShutdown (final RT2Message aShutdownMsg) throws Exception {
        LogMethodCallHelper.logMethodCall(this, "handleRemoteNodeShutdown", aShutdownMsg.getHeader());

        final RT2DocUidType docUID = aShutdownMsg.getDocUID();
        final boolean isRemoteDoc = !docProcessorManager.contains(docUID);

        // ATTENTION:
        // We only handle shutdown notifications of remote nodes - if our
        // own node is going to be shutdown, then doc proxies will be closed
        // by RT2Activator.deactivate()!
        if (isRemoteDoc) {
        	rt2NodeInfo.setNodeShutdown(true);

        	boolean locked = false;
        	final ClusterLock clusterLock = clusterLockService.getLock(docUID);
        	try {
	        	locked = clusterLock.lock();
	        	if (locked) {
		            distributedDocInfoMap.removeClient(docUID, clientUID);

		            // now it's time to deregister THIS proxy from global registry
		            docProxyRegistry.deregisterDocProxy(this, true);
		            log.debug("RT2DocProxy [{}] was shutdown...", this);
		            docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_SHUTDOWN);

		            rt2NodeInfo.setNodeShutdown(false);
		            m_bJoinLeaveSyncPoint.countDown();
	        	}
        	} finally {
				if (locked) {
					clusterLock.unlock();
				}
			}
        }
    }

    //-------------------------------------------------------------------------
    private void syncJoinWithLeaveIfNeeded () throws RT2TypedException, InterruptedException {

        if ( !docProxyStateHolder.isInShutdown () && !rt2NodeInfo.isNodeShutdown()) {
            return;
        }

        log.info("RT2Proxy [{}] is in shutdown - wait for finish", this);

        // wait for final leave signal 5s max
        // This feature is not thought to handle long running save operations within close/leave !
        // It's thought to handle "fast reload clashes" in some rare situations !

        final boolean bOK = m_bJoinLeaveSyncPoint.await(5000, TimeUnit.MILLISECONDS);
        if ( ! bOK) {
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_SYSTEM_BUSY_ERROR.getCodeAsStringConstant());
                log.error("System busy error!", new RT2TypedException (ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, getMsgsAsList()));
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
        	throw new RT2TypedException (ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, getMsgsAsList());
        }
        m_bJoinLeaveSyncPoint = new CountDownLatch((int) m_bJoinLeaveSyncPoint.getCount());
    }

    //-------------------------------------------------------------------------
    /** response handler is a 'dynamic resource'.
     *  It will be (de)registered from RT2WSChannel at runtime on cached RT2DocProxy
     *  instances. So it can happen there is no response handler known for some milliseconds.
     *
     *  On the other side we are on a stack where camel read a message from JMS and pass it to this method.
     *  If we fail ... this message is lost.
     *
     *  So what can we do ?
     *
     *  We can retry to find a suitable (might be new registered) response handler.
     *  We do it synchronous to be on the JMS-Camel-Process-Chain.
     *  But we give up after some time.
     *
     *  @param  aResponse [IN]
     *          the response to be forwarded to our response handler.
     */
    private void forwardResponseToHandler (final RT2Message aResponse) throws RT2Exception, RT2TypedException {
    	log.debug("forwardResponseToHandler: {}", aResponse.getHeader());
    	channelDisposer.addMessageToResponseQueue(aResponse);
    }

    //-------------------------------------------------------------------------
    @Override
    public String toString() {
        String res = "RT2DocProxy [com.openexchange.rt2.client.uid=" + clientUID + ", com.openexchange.rt2.document.uid=" + docUID + docProxyStateHolder + docProxyConnectionStateHolder;
        List<String> msgsLogInfos = formatMsgsLogInfo();
        if (!msgsLogInfos.isEmpty()) {
            res += msgsLogInfos.toString();
        }
        return res;
    }

    //-------------------------------------------------------------------------
    @Override
    public Map<String, Object> getAdditionalLogInfo() {
        Map<String, Object> res = new HashMap<>();
        res.put("com.openexchange.rt2.client.uid", clientUID);
        res.put("com.openexchange.rt2.document.uid", docUID);
        return res;
    }

  //-------------------------------------------------------------------------
    public List<String> formatMsgsLogInfo() {
    	return docProxyMessageLoggerService.formatMsgsLogInfo(getClientUID());
    }

    private List<RT2LogInfo> getMsgsAsList() {
    	return docProxyMessageLoggerService.getMsgsAsList(getClientUID());
    }

    //-------------------------------------------------------------------------
    @Override
    public Logger getLogger() {
        return log;
    }

    //-------------------------------------------------------------------------
    public String getProxyID () {
        return proxyID;
    }

    //-------------------------------------------------------------------------
    public RT2CliendUidType getClientUID () {
        return clientUID;
    }

    //-------------------------------------------------------------------------
    public RT2DocUidType getDocUID () {
        return docUID;
    }

    //-------------------------------------------------------------------------
    public void markProxyAsDisposed() throws RT2Exception {
    	docProxyMessageLoggerService.add(docUID, Direction.INTERNAL, "forceCleanup", clientUID);
        docProxyStateHolder.switchProxyStateOrFail(EDocProxyState.E_SHUTDOWN);
    }

    //-------------------------------------------------------------------------
	public void setContextAndActivator(RT2CoreContextAndActivator contextAndActivator) {
		this.contextAndActivator = contextAndActivator;
	}

    //-------------------------------------------------------------------------
	public void setThrowExceptionOnLeave(boolean throwExceptionOnLeave) {
		this.throwExceptionOnLeave = throwExceptionOnLeave;
	}
}
