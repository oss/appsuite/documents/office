/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.Loggable;

public class RT2DocProxyStateHolder implements Loggable {
	
	private static final Logger log = LoggerFactory.getLogger(RT2DocProxyStateHolder.class);
	
	private final RT2CliendUidType clientUid;
	
	private final RT2DocUidType docUid;
	
	private AtomicReference<EDocProxyState> proxyState = new AtomicReference<EDocProxyState>(EDocProxyState.E_INIT);

	private AtomicBoolean forceQuit = new AtomicBoolean(false);
	
    private AtomicBoolean abortOpen = new AtomicBoolean(false);    
    
	public RT2DocProxyStateHolder(RT2CliendUidType clientUid, RT2DocUidType docUid) {
		this.clientUid = clientUid;
		this.docUid = docUid;
	}

	public void checkDocumentAlreadyDisposed(List<RT2LogInfo> msgs) throws RT2TypedException {
		EDocProxyState proxyStateTmp = proxyState.get();
		if (proxyStateTmp.equals(EDocProxyState.E_SHUTDOWN) || proxyStateTmp.equals(EDocProxyState.E_INIT)) {
			throw new RT2TypedException(ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR, msgs);
		}
	}

    public boolean isInShutdown () {
        final EDocProxyState eState  = proxyState.get();
        // dont check for E_LEAVE ... as it's not IN shutdown ... it's the result of a shutdown !
        return eState.equals(EDocProxyState.E_IN_CLOSE) || eState.equals(EDocProxyState.E_CLOSED) || eState.equals(EDocProxyState.E_IN_LEAVE);
    }	
	
    public boolean isProxyInOpenState() {
        final EDocProxyState eState  = proxyState.get();
        return !eState.equals(EDocProxyState.E_IN_LEAVE) && !eState.equals(EDocProxyState.E_LEFT) &&
        	   !eState.equals(EDocProxyState.E_SHUTDOWN) && !eState.equals(EDocProxyState.E_INIT);
    }    
    
    //-------------------------------------------------------------------------
    public void switchProxyStateOrFail (final EDocProxyState eTo) throws RT2Exception {
        final EDocProxyState eResult = switchProxyState(eTo);

        // Make sure proxy switched into correct state - in case of a special mode we must
        // handle this more relaxed to ensure that shutdown will be completed.
        if (!eResult.equals(eTo) && !forceQuit.get() && !abortOpen.get()) {
        	RT2Exception ex = new RT2Exception ("Doc Proxy did not switch into right state. expected-state=["+eTo+"], current-state=["+eResult+"], com.openexchange.rt2.client.uid=["+clientUid+"], com.openexchange.rt2.document.uid=["+docUid+"]");
        	log.error("Doc Proxy did not switch into right state. expected-state=["+eTo+"], current-state=["+eResult+"], "
        			+ "com.openexchange.rt2.client.uid=["+clientUid+"], com.openexchange.rt2.document.uid=["+docUid+"] forceQuit=" + forceQuit.get() + ", abortOpen=" + abortOpen.get(), ex);
            throw ex; 
        }
    }
    
    
    //-------------------------------------------------------------------------
    /** switch the internal doc state to the new request one (if possible)
     *
     *  @param  eTo [IN]
     *          the new doc state
     *
     *  @return the new doc state if switch was possible;
     *          the old doc state if not possible (!!!)
     */
    public EDocProxyState switchProxyState (final EDocProxyState eTo) {
        final EDocProxyState eFrom = proxyState.get();
        LogMethodCallHelper.logMethodCall(this, "switchProxyState", eTo, forceQuit);
        
        boolean bOK =  (((eFrom == EDocProxyState.E_LEFT   ) || (eFrom == EDocProxyState.E_INIT)) && (eTo   == EDocProxyState.E_IN_JOIN))  
        			|| ((eFrom == EDocProxyState.E_IN_JOIN) && (eTo == EDocProxyState.E_JOINED ))
        			|| ((eFrom == EDocProxyState.E_JOINED ) && (eTo == EDocProxyState.E_IN_OPEN))
        			|| ((eFrom == EDocProxyState.E_JOINED ) && (eTo == EDocProxyState.E_IN_LEAVE))
        			|| ((eFrom == EDocProxyState.E_IN_OPEN) && (eTo == EDocProxyState.E_OPEN))
        			|| ((eFrom == EDocProxyState.E_OPEN) && (eTo == EDocProxyState.E_IN_CLOSE))
        			|| ((eFrom == EDocProxyState.E_IN_CLOSE) && (eTo == EDocProxyState.E_CLOSED))
        			|| ((eFrom == EDocProxyState.E_CLOSED) && (eTo == EDocProxyState.E_IN_LEAVE))
        			|| ((eFrom == EDocProxyState.E_IN_LEAVE) && (eTo == EDocProxyState.E_LEFT))
        			// Special mode : if user press cancel on client side where we stand in an open request ...
        			// we will get a leave without close before.
        			|| ((eFrom == EDocProxyState.E_IN_OPEN ) && (eTo   == EDocProxyState.E_IN_LEAVE))
        			// Special mode : a shutdown of a backend-node will dispose local doc processors and any
        			// remote client is automatically in E_SHUTDOWN state - this must be possible
        			|| (eTo == EDocProxyState.E_SHUTDOWN);

        // Special mode: A client needs to force a close/leave without to comply with our state
        // machine - in case of client errors, to enforce a clean state and in an emergency (tab-
        // browsing, see DOCS-1063
        //
        // We also need to ignore in this case state changes which try to go to a previous
        // state, e.g. the client sends a close/leave in a short time where we later receive the
        // close response from the doc processor (eFrom == E_IN_LEAVE, eTo == E_CLOSED). We have
        // to stay in E_IN_LEAVE.
        if (!bOK) {
	        if (forceQuit.get())
	        {
	            if (eTo == EDocProxyState.E_IN_LEAVE)
	                bOK = true;
	            else
	            if ((eFrom == EDocProxyState.E_IN_LEAVE) &&
	                (eTo   == EDocProxyState.E_CLOSED )) {
	            	LogMethodCallHelper.logMethodCallRes(this, "switchProxyState", eFrom, eTo);
	                return eFrom;
	            }
	        }
        }
        
        if (bOK) {
        	proxyState.set(eTo);
        	LogMethodCallHelper.logMethodCallRes(this, "switchProxyState", eTo, eTo);
            return eTo;
        }
        LogMethodCallHelper.logMethodCallRes(this, "switchProxyState", eFrom, eTo);
        return eFrom;
    }
    
    public void setForceQuit(boolean value) {
    	forceQuit.set(value);
    }
    
    public void setAbortOpen(boolean value) {
    	if (proxyState.get().equals(EDocProxyState.E_IN_OPEN)) {
    		abortOpen.set(value);
    	}
    }
    
    public boolean isAbortOpen() {
    	return abortOpen.get();
    }
    
	public EDocProxyState getProxyState() {
		return proxyState.get();
	}

	@Override
	public String toString() {
		return "[proxyState=" + proxyState + "]";
	}

	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	public Map<String, Object> getAdditionalLogInfo() {
		Map<String, Object> res = new HashMap<>();
		res.put("proxyState", proxyState);
		res.put("forceQuit", forceQuit);
		return res;
	}
}

