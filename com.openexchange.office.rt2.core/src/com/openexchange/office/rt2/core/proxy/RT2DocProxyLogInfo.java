/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import java.time.LocalDateTime;

import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.tools.common.error.ErrorCode;

public class RT2DocProxyLogInfo implements Comparable<RT2DocProxyLogInfo>, RT2LogInfo {
	
	public enum Direction {CREATE, CLOSE_HARD, FROM_WS, FROM_JMS, INTERNAL, TO_WS, TO_JMS};
	
	private final Direction direction;
	private final String msgType;
	private final RT2SeqNumberType seqNr;
	private final RT2CliendUidType clientUid;
	private final RT2DocUidType docUid;
	private final RT2ErrorCodeType errorCode;
	private final LocalDateTime occured;
	
	public RT2DocProxyLogInfo(Direction direction, String msgType, RT2CliendUidType clientUid, RT2DocUidType docUid) {
		this.direction = direction;
		this.msgType = msgType;
		seqNr = new RT2SeqNumberType(-1);
		this.clientUid = clientUid;
		this.docUid = docUid;
		this.errorCode = new RT2ErrorCodeType(ErrorCode.NO_ERROR);
		this.occured = LocalDateTime.now();
	}
	
	public RT2DocProxyLogInfo(Direction direction, RT2Message msg) {
		this.direction = direction;
		msgType = msg.getType().getValue();
		if (msg.isSequenceMessage()) {
			seqNr = msg.getSeqNumber();
		} else {
			seqNr = new RT2SeqNumberType(-1);
		}
		clientUid = msg.getClientUID();
		docUid = msg.getDocUID();
		if (msg.isError()) {
			errorCode = msg.getError();
		} else {
			errorCode = new RT2ErrorCodeType(ErrorCode.NO_ERROR);
		}
		this.occured = LocalDateTime.now();
	}

	@Override	
	public RT2CliendUidType getClientUid() {
		return clientUid;
	}

	@Override
	public RT2DocUidType getDocUid() {
		return docUid;
	}
	
	@Override
	public int compareTo(RT2DocProxyLogInfo o) {
		return this.occured.compareTo(o.occured);
	}

	@Override
	public String toString() {
		return "RT2DocProxyLogInfo [direction=" + direction + ", msgType=" + msgType + ", seqNr=" + seqNr
				+ ", com.openexchange.rt2.client.uid=" + clientUid + ", com.openexchange.rt2.document.uid=" + docUid + ", errorCode=" + errorCode + ", occured="
				+ occured + "]";
	}

	@Override
	public boolean isDocProcessorLogInfo() {
		return false;
	}
}
