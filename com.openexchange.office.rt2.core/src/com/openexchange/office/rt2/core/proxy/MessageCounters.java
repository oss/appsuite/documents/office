/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;

@Service
public class MessageCounters implements InitializingBean {

    private Counter joinRequestCounter;
    private Counter joinResponseCounter;
    private Counter openRequestCounter;
    private Counter openResponseCounter;
    private Counter closeRequestCounter;
    private Counter closeResponseCounter;
    private Counter leaveRequestCounter;
    private Counter leaveResponseCounter;
    private Counter abortCounter;
    
    @Autowired
    private MetricRegistry metricRegistry;
    
    @Override
	public void afterPropertiesSet() throws Exception {
    	this.joinRequestCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "request", "join"));
    	this.joinResponseCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "response", "join"));
    	this.openRequestCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "request", "open"));
    	this.openResponseCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "response", "open"));
    	this.closeRequestCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "request", "close"));
    	this.closeResponseCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "response", "close"));
    	this.leaveRequestCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "request", "leave"));
    	this.leaveResponseCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "response", "leave"));    	
    	this.abortCounter = metricRegistry.counter(MetricRegistry.name("MessageCounter", "abort"));
    }

	public void incJoinRequestCounter() {
    	this.joinRequestCounter.inc();
    }
    
    public void incJoinResponseCounter() {
    	this.joinResponseCounter.inc();
    }
    
    public void incOpenRequestCounter() {
    	this.openRequestCounter.inc();
    }
    
    public void incOpenResponseCounter() {
    	this.openResponseCounter.inc();
    }
    
    public void incCloseRequestCounter() {
    	this.closeRequestCounter.inc();
    }
    
    public void incCloseResponseCounter() {
    	this.closeResponseCounter.inc();
    }
    
    public void incLeaveRequestCounter() {
    	this.leaveRequestCounter.inc();
    }
    
    public void incLeaveResponseCounter() {
    	this.leaveResponseCounter.inc();
    }
    
    public void incAbortCounter() {
    	this.abortCounter.inc();
    }
}
