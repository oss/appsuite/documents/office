/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;

public class RT2DocProxyConnectionStateHolder {

	public static final boolean HACK_AUTO_ONLINE_MODE = true;

	private static final Logger log = LoggerFactory.getLogger(RT2DocProxyConnectionStateHolder.class);

	private EDocProxyConnectionState proxyConnState = EDocProxyConnectionState.E_OFFLINE;

	private long offlineTimestamp = 0;

	private long unavailableTimeStamp = 0;

	private long lastNotifiedUnavailableTime = 0;

	private final RT2CliendUidType clientUid;

	private final RT2DocUidType docUid;

	private final Object sync = new Object();

	public RT2DocProxyConnectionStateHolder(RT2CliendUidType clientUid, RT2DocUidType docUid) {
		this.clientUid = clientUid;
		this.docUid = docUid;
	}

	public boolean isOnline () {
		synchronized (sync) {
			return isOnlineState();
		}
    }

	public boolean goOfflineIfOnline() {
    	LogMethodCallHelper.logMethodCall(log, this, "goOffline");

    	boolean result = false;
    	synchronized (sync) {
    		if (isOnlineState()) {
    	        proxyConnState = EDocProxyConnectionState.E_OFFLINE;
    	        // store time when we switched to offline mode
    	        long offlineTimeStamp = System.currentTimeMillis();
    	        setOfflineTimeInMS(offlineTimeStamp);
    	        log.debug("Set com.openexchange.rt2.client.uid {} with com.openexchange.rt2.document.uid {} to offline timestamp {}", clientUid, docUid, offlineTimeStamp);
    	        result = true;
    		}
    	}
    	return result;
	}
	
	public boolean goOnlineIfOffline() {
    	LogMethodCallHelper.logMethodCall(log, this, "goOnline");

    	boolean result = false;
    	synchronized (sync) {
    		if (isOfflineState()) {
    			resetOfflineTime();
    	        proxyConnState = EDocProxyConnectionState.E_ONLINE;
    	        log.debug("Switched com.openexchange.rt2.client.uid {} with com.openexchange.rt2.document.uid {} to online", clientUid, docUid);
    	        result = true;
        	} else {
        		log.debug("Tried to switch com.openexchange.rt2.client.uid {} with com.openexchange.rt2.document.uid {} to online, but is not offline!", clientUid, docUid);
        	}
    	}
    	return result;
	}

	/**
	 * Determines if this the state of this instance is
	 * E_OFFLINE. This method is NOT thread-safe, please
	 * only call it with sync monitor acquired.
	 *
	 * @return TRUE if state is E_OFFLINE, otherwise FALSE
	 */
	private boolean isOfflineState () {
		return proxyConnState.equals(EDocProxyConnectionState.E_OFFLINE);
    }

	private boolean isOnlineState() {
		return !isOfflineState();
	}

	public long getOfflineTimeInMS () {
		synchronized (sync) {
			if (isOnlineState()) {
				return 0;
			}
	        final long nNow = System.currentTimeMillis();
	        return nNow - offlineTimestamp;
		}
    }

    public void setOfflineTimeInMS(long offlineTime) {
        synchronized (sync) {
            if (log.isDebugEnabled()) {
                if ((offlineTime != 0) && (offlineTimestamp != 0)) {
                    log.debug("Switching offline com.openexchange.rt2.client.uid {} with com.openexchange.rt2.document.uid {} with offline timestamp {} to offline using offlineTime {}",
                              clientUid, docUid, offlineTimestamp, offlineTime, new Exception());
                }
            }
            offlineTimestamp = offlineTime;
        }
    }

    private void resetOfflineTime() {
        if (log.isDebugEnabled()) {
            if (offlineTimestamp == 0) {
                log.debug("Reset offline time for com.openexchange.rt2.client.uid {} with com.openexchange.rt2.document.uid {} although offline timestamp {} is already zeor!", clientUid, docUid, offlineTimestamp);
            }
        }
        offlineTimestamp = 0;
    }

    public void setNotifiedUnavailbleTime(long unavailTime) {
        synchronized (sync) {
            final long timeStamp = (unavailTime == 0) ? 0 : System.currentTimeMillis() - unavailTime;
            lastNotifiedUnavailableTime = unavailTime;
            unavailableTimeStamp = timeStamp;
        }
    }

    public long getUnavailableTimeInMS() {
        synchronized (sync) {
            final long timeStamp = unavailableTimeStamp;
            return (timeStamp == 0) ? 0 : System.currentTimeMillis() - timeStamp;
        }
    }

    public long getLastNotifiedUnavailableTime() {
        synchronized (sync) {
            return lastNotifiedUnavailableTime;
        }
    }

	@Override
	public String toString() {
		synchronized (sync) {
			return "[proxyConnectionState=" + proxyConnState + "]";
		}
	}
}
