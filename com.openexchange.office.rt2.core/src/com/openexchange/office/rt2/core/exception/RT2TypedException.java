/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.exception;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.TextStringBuilder;

import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.tools.common.error.ErrorCode;

//=============================================================================
public class RT2TypedException extends RT2Exception
{
	//-------------------------------------------------------------------------
	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------------
	private final ErrorCode m_aError;

	//-------------------------------------------------------------------------
	public RT2TypedException (final ErrorCode aError, List<RT2LogInfo> msgs)
	{
	    super (getTextFromErrorCode(aError) + " " + format(msgs));
	    m_aError = (null == aError) ? ErrorCode.GENERAL_UNKNOWN_ERROR : aError;
	}

    //-------------------------------------------------------------------------
    public RT2TypedException (final ErrorCode aError, List<RT2LogInfo> msgs, String ... additionalInfo)
    {
        super (getTextFromErrorCode(aError) + format(additionalInfo)  + " " + format(msgs));
        m_aError = (null == aError) ? ErrorCode.GENERAL_UNKNOWN_ERROR : aError;
    }

	//-------------------------------------------------------------------------
	public RT2TypedException (final ErrorCode aError, List<RT2LogInfo> msgs, final Throwable aCause)
	{
		super (getTextFromErrorCode(aError) + " " + format(msgs), aCause);
		m_aError = (null == aError) ? ErrorCode.GENERAL_UNKNOWN_ERROR : aError;
	}

	//-------------------------------------------------------------------------
	public ErrorCode getError ()
	{
		return m_aError;
	}

	//-------------------------------------------------------------------------
	private static String getTextFromErrorCode(final ErrorCode aErrorCode)
	{
		if (null == aErrorCode) {
			return ErrorCode.GENERAL_UNKNOWN_ERROR.getDescription();
		}
		return (StringUtils.isEmpty(aErrorCode.getDescription())) ? aErrorCode.getCodeAsStringConstant() : aErrorCode.getDescription();
	}

	private static String format(List<RT2LogInfo> msgs) {
		if ((msgs != null) && !msgs.isEmpty()) {
			TextStringBuilder strBuilder = new TextStringBuilder();
			RT2LogInfo logInfo = msgs.get(0);
			if (logInfo.isDocProcessorLogInfo()) {
				strBuilder.append("Current messages of DocProcessor with com.openexchange.rt2.document.uid '");
				strBuilder.append(logInfo.getDocUid());					 
			} else {
				 strBuilder.append("Current messages of DocProxy with com.openexchange.rt2.document.uid '");
				 strBuilder.append(logInfo.getDocUid());
				 strBuilder.append("' and com.openexchange.rt2.client.uid '");
				 strBuilder.append(logInfo.getClientUid());
			}
			strBuilder.append("': ");
			strBuilder.appendNewLine();
			msgs.forEach(s -> strBuilder.appendln("    " + s));		    				 
			return strBuilder.toString();
		}
		return "";
	}
	

	
	//-------------------------------------------------------------------------
	private static String format(String [] strArray) {
	    StringBuilder strBuilder = new StringBuilder();
	    for (int i=0;i+1<strArray.length;i+=2) {
	        strBuilder.append(strArray[i]);
	        strBuilder.append(":");
	        strBuilder.append(strArray[i+1]);
	        strBuilder.append(";");
	    }
	    return strBuilder.toString();
	}
}
