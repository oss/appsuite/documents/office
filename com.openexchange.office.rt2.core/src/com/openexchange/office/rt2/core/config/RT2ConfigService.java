/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.config.ConfigurationService;

//=============================================================================
public class RT2ConfigService {

    //-------------------------------------------------------------------------
    public static final String KEY_MONITOR_SERVER_HOST = "rt2.monitoring.server.host";
    public static final String KEY_MONITOR_SERVER_PORT = "rt2.monitoring.server.port";

    public static final String KEY_OX_NODE_ID = "com.openexchange.server.backendRoute";

    public static final String KEY_GC_FREQUENCY = "rt2.gc.frequency";
    public static final String KEY_GC_OFFLINE_TRESHOLD = "rt2.gc.offline.treshold";

    public static final String KEY_TIMEOUT_4_SEND = "rt2.timeout.send";

    public static final String KEY_KEEPALIVE_FREQUENCY = "rt2.keepalive.frequency";

    public static final String KEY_MAX_SESSIONS_PER_NODE = "com.openexchange.office.maxOpenDocumentsPerUser";

    public static final String KEY_ACK_FLUSH = "rt2.ack.flush";

    public static final String KEY_TIMEOUT_FOR_RECONNECTED_CLIENT = "rt2.client.timeout.reconnect.before.hangup";
    public static final String KEY_TIMEOUT_FOR_BAD_CLIENT = "rt2.client.timeout.bad.client";
    public static final String KEY_MAX_MESSAGES_BEFORE_HANGUP = "rt2.client.max.messages.before.hangup";
    public static final String KEY_NACK_FREQUENCE_OF_SERVER = "internalNackFrequenceOfServer";

    public static final String KEY_BACKGROUND_SAVE_DOC = "io.ox/office//module/backgroundSaveDoc";

	private static final String PREFIX_CONCURRENT_EDITING = "io.ox/office//";
    private static final String POSTFIX_CONCURRENT_EDITING = "/concurrentEditing";

    //-------------------------------------------------------------------------
    private Map<String, Object> m_overrides = new ConcurrentHashMap<>();
    
    @Autowired
    private ConfigurationService configurationService;

    //-------------------------------------------------------------------------
    public boolean isRT2MonitoringOn() {
        final boolean bIsOn = impl_existsKey(KEY_MONITOR_SERVER_HOST);        
        return bIsOn;
    }

    //-------------------------------------------------------------------------
    public String getRT2MonitoringServerHost() {
        final String sHost = impl_readString(KEY_MONITOR_SERVER_HOST, RT2Const.DEFAULT_MONITOR_HOST);
        return sHost;
    }

    //-------------------------------------------------------------------------
    public int getRT2MonitoringServerPort() {
        final int nPort = impl_readInt(KEY_MONITOR_SERVER_PORT, RT2Const.DEFAULT_MONITOR_PORT);
        return nPort;
    }

    //-------------------------------------------------------------------------
    public String getOXNodeID() {
        final String sID = "OX0";
        return sID;
    }

    //-------------------------------------------------------------------------
    public long getRT2GCFrequencyInMS() {
        long nFrequencyInMS = RT2Const.DEFAULT_GC_FREQUENCY_IN_MS;

        final Long nValue = getValueFromOverride(KEY_GC_FREQUENCY, Long.class);
        if (null == nValue)
            nFrequencyInMS = impl_readLong(KEY_GC_FREQUENCY, RT2Const.DEFAULT_GC_FREQUENCY_IN_MS);
        else
            nFrequencyInMS = nValue;
        return nFrequencyInMS;
    }

    //-------------------------------------------------------------------------
    public long getRT2GCOfflineTresholdInMS() {
        long nTresholdInMS = RT2Const.DEFAULT_GC_OFFLINE_TRESHOLD_IN_MS;

        final Long nValue = getValueFromOverride(KEY_GC_OFFLINE_TRESHOLD, Long.class);
        if (null == nValue)
            nTresholdInMS = impl_readLong(KEY_GC_OFFLINE_TRESHOLD, RT2Const.DEFAULT_GC_OFFLINE_TRESHOLD_IN_MS);
        else
            nTresholdInMS = nValue;
        return nTresholdInMS;
    }

    //-------------------------------------------------------------------------
    public long getRT2KeepAliveFrequencyInMS() {
        final long nFrequencyInMS = impl_readLong(KEY_KEEPALIVE_FREQUENCY, RT2Const.DEFAULT_KEEPALIVE_FREQUENCY_IN_MS);
        return nFrequencyInMS;
    }

    //-------------------------------------------------------------------------
    public boolean isBackgroundSaveOn() {
        final boolean bIsOn = impl_readBoolean(KEY_BACKGROUND_SAVE_DOC, RT2Const.DEFAULT_BACKGROUND_SAVE_DOC);
        return bIsOn;
    }

    //-------------------------------------------------------------------------
    public long getRT2TimeoutSendInMS() {
        final long nTimeoutInMS = impl_readLong(KEY_TIMEOUT_4_SEND, RT2Const.DEFAULT_TIMEOUT_4_SEND_IN_MS);
        return nTimeoutInMS;
    }

    //-------------------------------------------------------------------------
    public void setRT2GCFrequencyInMS(long frequency) {
        m_overrides.put(KEY_GC_FREQUENCY, frequency);
    }

    //-------------------------------------------------------------------------
    public void setRT2GCOfflineTresholdInMS(long timeout) {
        m_overrides.put(KEY_GC_OFFLINE_TRESHOLD, timeout);
    }

    //-------------------------------------------------------------------------
    public int getRT2MaxSessionsPerNode() {
        int maxSessions = impl_readInt(KEY_MAX_SESSIONS_PER_NODE, RT2Const.DEFAULT_MAX_SESSIONS_PER_NODE);
        return maxSessions;
    }

    //-------------------------------------------------------------------------
    public int getAckResponsePeriod(int defValue) {
        int res = impl_readInt(KEY_ACK_FLUSH, defValue);
        return res;
    }

    //-------------------------------------------------------------------------
    public int getTimeoutBeforeHangupReconnectedClient(int defValue) {
        int res = impl_readInt(KEY_TIMEOUT_FOR_RECONNECTED_CLIENT, defValue);
        return res;
    }

    //-------------------------------------------------------------------------
    public int getTimeoutForBadClient(int defValue) {
        int res = impl_readInt(KEY_TIMEOUT_FOR_BAD_CLIENT, defValue);
        return res;
    }

    //-------------------------------------------------------------------------
    public int getMaxMessagesCountWithoutAck(int defValue) {
        int res = impl_readInt(KEY_MAX_MESSAGES_BEFORE_HANGUP, defValue);
        return res;
    }

    //-------------------------------------------------------------------------
    public Integer getNackFrequenceOfServer() {
        if (m_overrides.containsKey(KEY_NACK_FREQUENCE_OF_SERVER)) {
            return (Integer) m_overrides.get(KEY_NACK_FREQUENCE_OF_SERVER);
        }
        return 60;
    }

    //-------------------------------------------------------------------------
    public void updateNackFrequenceOfServer(Integer value) {
        m_overrides.put(KEY_NACK_FREQUENCE_OF_SERVER, value);
    }

    //-------------------------------------------------------------------------
	public boolean isOTEnabled(String appType) {
   		final String configEntry = PREFIX_CONCURRENT_EDITING + appType + POSTFIX_CONCURRENT_EDITING;
   		return configurationService.getBoolProperty(configEntry, true);
    }

    //-------------------------------------------------------------------------
    private boolean impl_existsKey(final String sKey) {
        final String sValue = configurationService.getProperty(sKey);
        final boolean bIs = !StringUtils.isEmpty(sValue);
        return bIs;
    }

    //-------------------------------------------------------------------------
    private String impl_readString(final String sKey, final String sDefault) {
        final String sValue = configurationService.getProperty(sKey, sDefault);
        if (sValue == null) {
            return sDefault;
        }
        return sValue;
    }

    //-------------------------------------------------------------------------
    private int impl_readInt(final String sKey, final int nDefault) {
        final String sValue = configurationService.getProperty(sKey, Integer.toString(nDefault));
        if (sValue == null) {
            return nDefault;
        }
        final int nValue = Integer.parseInt(sValue);
        return nValue;
    }

    //-------------------------------------------------------------------------
    private long impl_readLong(final String sKey, final long nDefault) {
        final String sValue = configurationService.getProperty(sKey, Long.toString(nDefault));
        if (sValue == null) {
            return nDefault;
        }
        final long nValue = Long.parseLong(sValue);
        return nValue;
    }

    //-------------------------------------------------------------------------
    private boolean impl_readBoolean(final String sKey, final boolean bDefault) {
        final String sValue = configurationService.getProperty(sKey, Boolean.toString(bDefault));
        if (sValue == null) {
            return bDefault;
        }
        final boolean bValue = Boolean.parseBoolean(sValue);
        return bValue;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    private <T> T getValueFromOverride(String key, Class<T> clazz) {
        final Object objValue = m_overrides.get(key);
        if ((objValue != null) && (clazz.isAssignableFrom(objValue.getClass()))) {
            return (T) objValue;
        }

        return null;
    }

}
