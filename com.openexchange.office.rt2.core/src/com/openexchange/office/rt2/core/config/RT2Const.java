/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.config;

//=============================================================================
public class RT2Const
{
    //-------------------------------------------------------------------------
    // max number of users that collaborate on one document
    public static final int    RT2_MAX_COUNT_COLLABORATION_USERS = 200;

    //-------------------------------------------------------------------------
    // version number of the RT2 REST API (will be part of the URL schema ...)
    public static final int    RT2_VERSION = 1;

    //-------------------------------------------------------------------------
    // the URL root for our REST API
    public static final String RT2_URL_BASE = "/rt2";

    //-------------------------------------------------------------------------
    // the URL pattern where our REST API will be registered for
    public static final String RT2_URL_PATTERN = RT2Const.RT2_URL_BASE+"/v"+RT2_VERSION+"/*";

    //-------------------------------------------------------------------------
    // the ID of the JMS Connection bean within our Camel Routing Context
    public static final String BEANID_JMS_CONNECTION = "jms";

    //-------------------------------------------------------------------------
    public static final String DOCTYPE_TEXT         = "text"        ;
    public static final String DOCTYPE_SPREADSHEET  = "spreadsheet" ;
    public static final String DOCTYPE_PRESENTATION = "presentation";
    public static final String DOCTYPE_PRESENTER    = "presenter"   ;

    //-------------------------------------------------------------------------
    public static final int     DEFAULT_PORT_JMS                  = 61616;

    public static final String  DEFAULT_MONITOR_HOST              = "localhost";
    public static final int     DEFAULT_MONITOR_PORT              = 9876;

    public static final long    DEFAULT_GC_FREQUENCY_IN_MS        = 300000 ; // 5 min
    public static final long    DEFAULT_GC_OFFLINE_TRESHOLD_IN_MS = 1800000; // 30 min
    public static final long    DEFAULT_KEEPALIVE_FREQUENCY_IN_MS = 10000  ; // 10 sec
    public static final boolean DEFAULT_BACKGROUND_SAVE_DOC       = true;
    public static final long    DEFAULT_TIMEOUT_4_SEND_IN_MS      = 60000  ; // 1 min
    public static final int 	DEFAULT_MAX_SESSIONS_PER_NODE     = 50;
}
