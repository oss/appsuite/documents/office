/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.logging;

import java.util.Collection;
import java.util.List;

import org.apache.commons.text.TextStringBuilder;
import org.slf4j.Logger;
import org.slf4j.MDC;

import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;

public class MessagesLogger implements Runnable {

	private final IMessagesObjectManager msgsObjectMgr;
	
	private final Logger logger;
	
	public MessagesLogger(IMessagesObjectManager msgsObjectMgr, Logger logger) {
		this.msgsObjectMgr = msgsObjectMgr;
		this.logger = logger;
	}

	@Override
	public void run() {
		Collection<IMessagesLoggable> loggableCol = msgsObjectMgr.getManagedCollection();
		try {
			for (IMessagesLoggable loggableObj : loggableCol) {
				 List<String> loggList = loggableObj.formatMsgsLogInfo();
				 RT2CliendUidType clientUid = loggableObj.getClientUID();
				 RT2DocUidType docUid = loggableObj.getDocUID();
				 StringBuilder strBuilder = new StringBuilder();
				 MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
				 MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "MessagesLogger");
				 if (clientUid == null) {				 
					 strBuilder.append("Current messages of DocProcessor with com.openexchange.rt2.document.uid '");
					 strBuilder.append(docUid);
					 
				 } else {
					 MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUid.getValue());
					 strBuilder.append("Current messages of DocProxy with com.openexchange.rt2.document.uid '");
					 strBuilder.append(docUid);
					 strBuilder.append("' and clientUid '");
					 strBuilder.append(clientUid);
				 }
				 strBuilder.append("': ");
			     TextStringBuilder builder = new TextStringBuilder(loggList.get(0));
			     loggList.remove(0);
			     loggList.forEach(s -> builder.appendln("    " + s));		    	
				 strBuilder.append(loggList);			 
				 logger.debug(strBuilder.toString());
				 MDC.clear();
				 try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
		} finally {
			MDC.clear();
		}
	}
}
