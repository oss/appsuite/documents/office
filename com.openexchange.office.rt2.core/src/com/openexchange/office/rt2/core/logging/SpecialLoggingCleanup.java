/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.logging;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.office.rt2.core.doc.IDocNotificationHandler;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

@Service
public class SpecialLoggingCleanup implements InitializingBean, DisposableBean, IDocNotificationHandler, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(SpecialLoggingCleanup.class); 

    private static final long MAX_TO_LIVE_AFTER_DISPOSED = 2 * 24 * 60 * 60 * 1000; // 2 days
    private static final long GC_CHECK_TIME_PERIOD = 5 * 60 * 1000; // 5 minutes

    @Autowired
    private TimerService timerService;

    @Autowired
    private SpecialLogService specialLogService;

    @Autowired
    private RT2DocStateNotifier rt2DocStateNotifier;

    @Autowired
    private LocalDateTimeService localDateTimeService;

    @Autowired
    private RT2DocOnNodeMap docOnNodeMap;;

    private final Map<String, LocalDateTime> mustBeGCAfterTimeout = new ConcurrentHashMap<String, LocalDateTime>();

    private ScheduledTimerTask cleanupScheduledTimerTask;

    @Override
    public void afterPropertiesSet() throws Exception {
        LOG.info("SpecialLoggingCleanup initialized wih {} time-to-live (hh:mm:ss.ms) and {}s check period.", DurationFormatUtils.formatDurationHMS(MAX_TO_LIVE_AFTER_DISPOSED), (GC_CHECK_TIME_PERIOD / 1000));

        rt2DocStateNotifier.addDocNotificationHandler(this);
        cleanupScheduledTimerTask = timerService.scheduleAtFixedRate(this, GC_CHECK_TIME_PERIOD, GC_CHECK_TIME_PERIOD);
    }

    @Override
    public void destroy() throws Exception {
        LOG.info("SpecialLoggingCleanup destroy called.");

        if (cleanupScheduledTimerTask != null) {
            cleanupScheduledTimerTask.cancel(true);
        }

        mustBeGCAfterTimeout.clear();
    }

    public int getCountOfSpecialLoggingEntriesToGC() {
        return mustBeGCAfterTimeout.size();
    }

    public boolean areSpecialLoggingMessagesForDocUidMarkedForGC(@NotNull RT2DocUidType docUid) {
        return mustBeGCAfterTimeout.containsKey(docUid.toString());
    }

    @Override
    public void docProcessorCreated(RT2DocProcessor aCreatedInstance) {
        var docUid = aCreatedInstance.getDocUID();

        if (StringUtils.isNotEmpty(docUid.getValue())) {
            mustBeGCAfterTimeout.remove(docUid.getValue());
        }
    }

    @Override
    public void docProcessorDisposed(RT2DocProcessor aDisposedInstance) {
        var docUid = aDisposedInstance.getDocUID();

        if (StringUtils.isNotEmpty(docUid.getValue())) {
            mustBeGCAfterTimeout.putIfAbsent(docUid.getValue(), localDateTimeService.getNowAsUTC());
        }
    }

    @Override
    public void run() {
        try {
            var now = localDateTimeService.getNowAsUTC();

            checkDocOnNodeMapToAddOrRemoveDocUidsToGCMap(now);

            var entriesToBeRemoved = determineEntriesToBeRemovedNow(now);
            entriesToBeRemoved.forEach(docUid -> removeEntryFromMaps(docUid));

            LOG.info("SpecialLoggingCleanup overview during last observation period of {}s: status=speciallog_doc_count: {}, status=speciallog_doc_marked_for_gc_count: {}, status=speciallog_doc_removed_count: {}", (GC_CHECK_TIME_PERIOD / 1000), specialLogService.getEntriesCount(), mustBeGCAfterTimeout.size(), entriesToBeRemoved.size());
        } catch (Exception ex) {
            LOG.warn("Exception caught trying to check for special log messages to be clean-up", ex);
        }
    }

    private void checkDocOnNodeMapToAddOrRemoveDocUidsToGCMap(LocalDateTime now) {
        addDocUidsToGCMapInCaseNotInDocOnNodeMap(now);
        removeEntriesFromGCMapExistingInDocOnNodeMap();
    }

    private void addDocUidsToGCMapInCaseNotInDocOnNodeMap(LocalDateTime now) {
        specialLogService.getDocUidsWithEntries()
                         .stream()
                         .filter(docUid -> !doesGCMapContainsDocUid(docUid))
                         .filter(docUid -> !doesDocOnNodeMapContainsDocUid(docUid))
                         .forEach(docUid -> mustBeGCAfterTimeout.putIfAbsent(docUid, now));
    }

    private void removeEntriesFromGCMapExistingInDocOnNodeMap() {
        specialLogService.getDocUidsWithEntries()
                         .stream()
                         .filter(docUid -> doesDocOnNodeMapContainsDocUid(docUid))
                         .forEach(docUid -> mustBeGCAfterTimeout.remove(docUid));
    }

    private List<String> determineEntriesToBeRemovedNow(LocalDateTime now) {
        return mustBeGCAfterTimeout.entrySet()
                                   .stream()
                                   .filter(entry -> mustBeRemoved(entry.getValue(), now))
                                   .map(entry -> entry.getKey())
                                   .toList();
    }

    private boolean mustBeRemoved(LocalDateTime timeDocDisposed, LocalDateTime now) {
        return (Math.abs(Duration.between(timeDocDisposed, now).getSeconds() * 1000)) >= MAX_TO_LIVE_AFTER_DISPOSED; 
    }

    private boolean doesGCMapContainsDocUid(String docUid) {
        return mustBeGCAfterTimeout.containsKey(docUid);
    }

    private boolean doesDocOnNodeMapContainsDocUid(String docUid) {
        return docOnNodeMap.get(docUid) != null;
    }

    private void removeEntryFromMaps(String docUid) {
        specialLogService.removeEntriesForKey(docUid);
        mustBeGCAfterTimeout.remove(docUid);
    }
}
