/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo.Direction;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.OXDocumentException;

//=============================================================================
@Service
public class RT2MessageTypeDispatcher implements InitializingBean {
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(RT2MessageTypeDispatcher.class);

    //-------------------------------------------------------------------------
	private static final Map<String, String> PREDEFINED_DEFAULT_METHODS_MAP;
	static
	{
		// predefined message types to be mapped to defined methods
		final HashMap<String, String> tmp = new HashMap<>();
        tmp.put(RT2Protocol.REQUEST_JOIN,               "onJoin"                 );
        tmp.put(RT2Protocol.REQUEST_LEAVE,              "onLeave"                );
		tmp.put(RT2Protocol.REQUEST_OPEN_DOC,           "onOpenDoc"              );
		tmp.put(RT2Protocol.REQUEST_CLOSE_DOC,          "onCloseDoc"             );
        tmp.put(RT2Protocol.REQUEST_SWITCH_DOCSTATE,    "onSwitchDocState"       );
		tmp.put(RT2Protocol.REQUEST_APPLY_OPS,          "onApplyOperations"      );
		tmp.put(RT2Protocol.REQUEST_SAVE_DOC,           "onSaveDoc"              );
		tmp.put(RT2Protocol.REQUEST_SYNC,               "onSync"                 );
		tmp.put(RT2Protocol.REQUEST_SYNC_STABLE,        "onSyncStable"           );
		tmp.put(RT2Protocol.REQUEST_EDITRIGHTS,         "onEditRights"           );
		tmp.put(RT2Protocol.REQUEST_UNAVAILABILITY,     "onUnavailable"          );
		tmp.put(RT2Protocol.REQUEST_EMERGENCY_LEAVE,    "onEmergencyLeave"       );
		tmp.put(RT2Protocol.REQUEST_CLOSE_AND_LEAVE, 	"onCloseAndLeave"		 );

		PREDEFINED_DEFAULT_METHODS_MAP = Collections.unmodifiableMap(tmp);
	}

	private Map<Class<?>, MessageDispatcher> customMsgDispatchers = new HashMap<>();
	
	private Map<Class<?>, MessageDispatcher> defaultMsgDispatchers = new HashMap<>();

	private ReadWriteLock defaultRwLock = new ReentrantReadWriteLock();
	
	private Lock defaultReadLock;
	
	private Lock defaultWriteLock;
	
	private ReadWriteLock customRwLock = new ReentrantReadWriteLock();
	
	private Lock customReadLock;
	
	private Lock customWriteLock;

	@Autowired
    private DocProcessorMessageLoggerService messageLoggerService;

	//-------------------------------------------------------------------------
    @Override
	public void afterPropertiesSet() {
    	defaultReadLock = defaultRwLock.readLock();
    	defaultWriteLock = defaultRwLock.writeLock();
    	customReadLock = customRwLock.readLock();
    	customWriteLock = customRwLock.writeLock();
	}
		
    //-------------------------------------------------------------------------
	public void handleRequest(final RT2Message aRawRequest, RT2DocProcessor docProcessor) throws Exception
	{
		messageLoggerService.addMessageForQueueToLog(Direction.QUEUE, aRawRequest);
		
        boolean bProcessed = processMessage(aRawRequest, docProcessor);
        if (!bProcessed) {
          	log.warn("Message of type {} not successfully processed.", aRawRequest.getType());
        }
	}
	
    //-------------------------------------------------------------------------
	protected boolean processMessage(final RT2Message aMsg, RT2DocProcessor docProcessor) throws Exception
	{
		final String sMsgType = aMsg.getType().getValue();

		// don't dispatch messages to a disposed doc processor
		if (docProcessor.isDisposed())
		    throw new OXDocumentException("Document com.openexchange.rt2.document.uid " + docProcessor.getDocUID() + " is already disposed!", ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR);

		// Dispatch message to the implementation method
		// using the default msg dispatcher or a custom msg
		// dispatcher.
		boolean bProcessed = false;
		if (PREDEFINED_DEFAULT_METHODS_MAP.containsKey(sMsgType)) {
			bProcessed = getDefaultMsgDispatcher(docProcessor).callMethod(docProcessor, PREDEFINED_DEFAULT_METHODS_MAP.get(sMsgType), aMsg);
		} else {
			bProcessed = getCustomMsgDispatcher(docProcessor).callMethod(docProcessor, aMsg);
		}
		return bProcessed;
	}
	
	//-------------------------------------------------------------------------	
	private MessageDispatcher getCustomMsgDispatcher(RT2DocProcessor docProcessor) throws Exception {
		customReadLock.lock();
		try {
			MessageDispatcher res = customMsgDispatchers.get(docProcessor.getClass());
			if (res != null) {
				return res;
			}
		} finally {
			customReadLock.unlock();
		}
		customWriteLock.lock();
		try {
			MessageDispatcher res = customMsgDispatchers.get(docProcessor.getClass());
			if (res != null) {
				return res;
			}
			res = new MessageDispatcher(docProcessor.getClass(), "");
			customMsgDispatchers.put(docProcessor.getClass(), res);
			return res;
		} finally {
			customWriteLock.unlock();
		}		
	}
	
	//-------------------------------------------------------------------------	
	private MessageDispatcher getDefaultMsgDispatcher(RT2DocProcessor docProcessor) throws Exception {
		defaultReadLock.lock();
		try {
			MessageDispatcher res = defaultMsgDispatchers.get(docProcessor.getClass());
			if (res != null) {
				return res;
			}
		} finally {
			defaultReadLock.unlock();
		}
		defaultWriteLock.lock();
		try {
			MessageDispatcher res = defaultMsgDispatchers.get(docProcessor.getClass());
			if (res != null) {
				return res;
			}
			res = new MessageDispatcher(docProcessor.getClass(), PREDEFINED_DEFAULT_METHODS_MAP.values());
			defaultMsgDispatchers.put(docProcessor.getClass(), res); 
			return res;
		} finally {
			defaultWriteLock.unlock();
		}				
	}	
}
