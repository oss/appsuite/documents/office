/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.tools.doc.SyncInfo;

public class LoadableDocumentStatus extends DocumentStatus
{
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(LoadableDocumentStatus.class);

    //-------------------------------------------------------------------------
    private boolean   m_hasErrors             = false;
    private boolean   m_locked                = false;
    private boolean   m_finalLoad             = false;
    private boolean   m_syncLoad              = false;
    private int       m_operationStateNumber  = SyncInfo.SYNC_UNKNOWN_DOCOSN;
    private int       m_documentOSN           = SyncInfo.SYNC_UNKNOWN_DOCOSN;
    private String    m_lockedByUser          = EMPTY_USERNAME;
    private int       m_lockedById            = NO_USERID;
    private Boolean   m_optWriteProtected     = null;
    private String    m_version               = null;
    private Boolean   m_forcedReadOnly        = false;

    //-------------------------------------------------------------------------
    public LoadableDocumentStatus()
    {
        super();
    }

    //-------------------------------------------------------------------------
    public LoadableDocumentStatus(final LoadableDocumentStatus aStatus) throws Exception
    {
    	super(aStatus);

        m_hasErrors            = aStatus.m_hasErrors;
        m_locked               = aStatus.m_locked;
        m_finalLoad            = aStatus.m_finalLoad;
        m_syncLoad             = aStatus.m_syncLoad;
        m_operationStateNumber = aStatus.m_operationStateNumber;
        m_documentOSN          = aStatus.m_documentOSN;
        m_lockedByUser         = aStatus.m_lockedByUser;
        m_lockedById           = aStatus.m_lockedById;
        m_optWriteProtected    = aStatus.m_optWriteProtected;
        m_version              = aStatus.m_version;
        m_forcedReadOnly       = aStatus.m_forcedReadOnly;
    }

    //-------------------------------------------------------------------------
    public static boolean isValidOSN(int nOSN)
    {
        return ((nOSN != SyncInfo.SYNC_UNKNOWN_DOCOSN) && (nOSN > 0));
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the error state.
     *
     * @return The m_hasErrors
     */
    public boolean hasErrors()
    {
        return m_hasErrors;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the error state.
     *
     * @param hasErrors The m_hasErrors to set
     */
    public void setHasErrors(boolean hasErrors)
    {
        this.m_hasErrors = hasErrors;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Is this connection bound to a forced read-only
     * document (e.g. rescue document)
     *
     * @return TRUE if this connection references a rescue doc, otherwise
     *         FALSE
     */
    public boolean isForcedReadOnly()
    {
        return m_forcedReadOnly;
    }

    //-------------------------------------------------------------------------
    /**
     * Set the force read-onyl state. Must only be called once and by
     * the first joining user.
     *
     * @param bForceReadOnly the initial forced read-only state
     */
    public void setForceReadOnly(boolean bForceReadOnly)
    {
        m_forcedReadOnly = bForceReadOnly;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Sets a temporary write protected state.
     * ATTENTION: This must be a temporary state as the read/write
     * state depends on the client accessing the file. Don't use it
     * to modify the global connection status!!
     *
     * @param isWriteProtected
     */
    public void setWriteProtected(boolean isWriteProtected)
    {
        this.m_optWriteProtected = new Boolean(isWriteProtected);
    }

    //-------------------------------------------------------------------------
    /**
     * Get the lock state
     *
     * @return The current lock state
     */
    public boolean isLocked()
    {
        return m_locked;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the lock state
     *
     * @param locked The new lock state
     */
    public void setLockState(boolean locked)
    {
        this.m_locked = locked;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Get user name who locked the file
     *
     * @return The name of the user who locked the file
     * or empty string if user is unknown or nobody set
     * a lock.
     */
    public String getLockedByUser()
    {
        return m_lockedByUser;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the user who locked the file
     *
     * @param lockedByUser The name of the user
     * or empty string if unknown or nobody set
     * a lock.
     */
    public void setLockedByUser(String lockedByUser)
    {
        this.m_lockedByUser = lockedByUser;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Get the user id of the user who locked the file.
     *
     * @return The id of the user who locked the file. NO_USERID, if no
     * user locked the file.
     */
    public int getLockedByUserId()
    {
        return this.m_lockedById;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the user id of the user who locked the file.
     *
     * @param userId The id of the user who locked the file. NO_USERID, if no
     * user locked the file.
     */
    public void setLockedByUserId(int userId)
    {
        this.m_lockedById = userId;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    public final String getVersion()
    {
        return this.m_version;
    }

    //-------------------------------------------------------------------------
    public void setVersion(final String version)
    {
        this.m_version = version;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the m_operationStateNumber
     *
     * @return The m_operationStateNumber
     */
    public int getOperationStateNumber()
    {
        return this.m_operationStateNumber;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the m_operationStateNumber
     *
     * @param operationStateNumber The operation state number to set
     */
    public void setOperationStateNumber(int operationStateNumber)
    {
        this.m_operationStateNumber = operationStateNumber;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the operation state number of the last saved
     * document (or the first loaded document).
     *
     * @return The m_operationStateNumber
     */
    public int getDocumentOSN()
    {
        return this.m_documentOSN;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the operation state number of the last saved
     * document (or the first loaded document).
     *
     * @param docOSN
     */
    public void setDocumentOSN(int docOSN)
    {
        this.m_documentOSN = docOSN;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the 'finalLoad' attribute, which notifies the client that the
     * asynchronous loading process is finished and the 'update' with
     * this attribute contains the document actions.
     *
     * @param finalLoad
     *  Sets the finalLoad attribute.
     */
    public void setFinalLoad(boolean set)
    {
    	m_finalLoad = set;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the finalLoad attribute of this connection status
     * object.
     *
     * @return
     *  TRUE if the finalLoad attribute is set otherwise FALSE.
     */
    public boolean getFinalLoad()
    {
    	return m_finalLoad;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the 'syncLoad' attribute which notifies the client that the
     * welcome message contains the full set of document actions. If
     * 'syncLoad' is not set the client must wait for the next 'update'
     * notification which has the 'finalLoad' attribute set.
     */
    public void setSyncLoad(boolean set)
    {
    	m_syncLoad = set;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the 'syncLoad' attribute of this connection status
     * object.
     *
     * @return
     *  TRUE if the syncLoad attribute is set otherwise FALSE.
     */
    public boolean getSyncLoad()
    {
    	return m_syncLoad;
    }

    //-------------------------------------------------------------------------
    @Override
	public LoadableDocumentStatus clone() throws CloneNotSupportedException
    {
    	try
    	{
    		return new LoadableDocumentStatus(this);
    	}
    	catch (Throwable e)
    	{
            ExceptionUtils.handleThrowable(e);
            log.error("Cannot create clone due to exception", e);
    		throw new CloneNotSupportedException("Cannot create clone");
    	}
	}

    //-------------------------------------------------------------------------
	@Override
	public JSONObject toJSON()
	{
        final JSONObject ret = super.toJSON();

        try {
            ret.put(DocStatusProperties.KEY_HAS_ERRORS,     m_hasErrors);
            ret.put(DocStatusProperties.KEY_LOCKED,         m_locked);
            ret.put(DocStatusProperties.KEY_LOCKED_BY_USER, m_lockedByUser);
            ret.put(DocStatusProperties.KEY_SERVER_OSN,     m_operationStateNumber);
            ret.put(DocStatusProperties.KEY_LOCKED_BY_ID,   m_lockedById);
            ret.put(DocStatusProperties.KEY_RESCUEDOC,      m_forcedReadOnly);
        } catch (JSONException e) {
            log.error("Exception while creating JSON object of the current LoadableDocumentStatus", e);
        }
        return ret;
	}

}
