/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.tools.common.string.StringHelper;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;

@Service
public class AdvisoryLockHelperService {

    private static final Logger LOG = LoggerFactory.getLogger(AdvisoryLockHelperService.class);
    private static final String ADVISORY_LOCK_RESET_ACTION = "reset";

    @Autowired
    private SessionService sessionService;

    @Autowired
    private AdvisoryLockInfoService advisoryLockInfoService;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    // -------------------------------------------------------------------------
    public void clearAdvisoryLock(UserData userData) throws Exception {
        final String folderId = userData.getFolderId();
        final String fileId = userData.getFileId();

        final Session session = sessionService.getSession4Id(userData.getSessionId());
        advisoryLockInfoService.clearAdvisoryLock(session, folderId, fileId);
    }

    // -------------------------------------------------------------------------
    public boolean checkAndApplyAdvisoryLockAction(RT2Message openDocMsg, String nameOfSystem) throws Exception {
        final String sessionId = RT2MessageGetSet.getSessionID(openDocMsg);
        final Session session = sessionService.getSession4Id(sessionId);
        final String advisoryLockAction = RT2MessageGetSet.getAdvisoryLockAction(openDocMsg);

        if ((session != null) && advisoryLockAction.equalsIgnoreCase(ADVISORY_LOCK_RESET_ACTION)) {
            final String folderId = RT2MessageGetSet.getFolderID(openDocMsg);
            final String fileId = RT2MessageGetSet.getFileID(openDocMsg);
            boolean result = advisoryLockInfoService.resetAdvisoryLock(session, nameOfSystem, folderId, fileId);
            LOG.info("Advisory lock info reset for com.openexchange.rt2.document.uid {} referencing file-id {} by com.openexchange.rt2.client.uid {}", openDocMsg.getDocUID(), fileId, openDocMsg.getClientUID());
            return result;
        } else if (session == null) {
            LOG.info("Advisory lock info cannot process advisory lock action due to unknown session for com.openexchange.rt2.client.uid {}", openDocMsg.getClientUID());
        }
        return false;
    }

    // -------------------------------------------------------------------------
    public boolean isLastUserWithWriteAccess(List<UserData> completeUserDataList, final RT2CliendUidType clientUidToGo) {
        if ((clientUidToGo == null) || (StringUtils.isEmpty(clientUidToGo.getValue())))
            return false;

        final String clientUidToCheck = clientUidToGo.getValue();
        final Optional<UserData> userDataToQuit = completeUserDataList.stream().filter(u -> isSameClientUid(u, clientUidToCheck)).findFirst();
        if (userDataToQuit.isPresent()) {
            return (canUserWriteToFile(userDataToQuit.get()) && 
                    completeUserDataList.stream().filter(u -> !isSameClientUid(u, clientUidToCheck)).noneMatch(u -> canUserWriteToFile(u)));
        }
        return false;
    }

    // -------------------------------------------------------------------------
    private boolean canUserWriteToFile(final UserData userData) {
        if ((userData == null) || StringUtils.isEmpty(userData.getSessionId()) || StringUtils.isEmpty(userData.getFileId()))
            return false;

        try {
            final Session session = sessionService.getSession4Id(userData.getSessionId());
            if (session != null) {
                FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
                return fileHelperService.canWriteToFile(userData.getFileId());
            }

            return false;
        } catch (Exception e) {
            return false;
        }
    }

    // -------------------------------------------------------------------------
    private static boolean isSameClientUid(UserData userData, String clientUid) {
        return ((userData != null) && (StringHelper.isNotEmptyAndEqual(userData.getClientUid(), clientUid)));
    }

}
