/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class DocProcessorClientInfo
{
	//-------------------------------------------------------------------------
	private final String sNodeUUID;

	//-------------------------------------------------------------------------
	private final RT2CliendUidType sClientUID;

	//-------------------------------------------------------------------------
	public DocProcessorClientInfo(RT2CliendUidType sClientUID, String sNodeUUID) {
		this.sClientUID = sClientUID;
		this.sNodeUUID  = sNodeUUID;
	}

	//-------------------------------------------------------------------------
	public RT2CliendUidType getClientUID() { return sClientUID; }

	//-------------------------------------------------------------------------
	public String getNodeUUID() { return sNodeUUID; }

    @Override
    public String toString() {
        return "ClientInfo [sNodeUUID=" + sNodeUUID + ", sClientUID=" + sClientUID + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sClientUID == null) ? 0 : sClientUID.hashCode());
		result = prime * result + ((sNodeUUID == null) ? 0 : sNodeUUID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocProcessorClientInfo other = (DocProcessorClientInfo) obj;
		if (sClientUID == null) {
			if (other.sClientUID != null)
				return false;
		} else if (!sClientUID.equals(other.sClientUID))
			return false;
		if (sNodeUUID == null) {
			if (other.sNodeUUID != null)
				return false;
		} else if (!sNodeUUID.equals(other.sNodeUUID))
			return false;
		return true;
	}
}
