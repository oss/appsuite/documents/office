/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class PresenterDocumentStatus extends DocumentStatus
{
	private static final Logger log = LoggerFactory.getLogger(PresenterDocumentStatus.class);
	
    //-------------------------------------------------------------------------
    public static final String  PROP_PRESENTER_ID   = "presenterId";
    public static final String  PROP_PRESENTER_NAME = "presenterName";
    public static final String  PROP_ACTIVE_SLIDE   = "activeSlide";
    public static final String  PROP_PAUSED         = "paused";

    // - Members ---------------------------------------------------------------

    private String  m_currentPresenterName          = EMPTY_USERNAME;
    private RT2CliendUidType  m_currentPresenterId  = new RT2CliendUidType();
    private int     m_activeSlide                   = 0;
    private boolean m_paused                        = false;

    // - Implementation --------------------------------------------------------

    /**
     * Initializes a new {@link ConnectionStatus}.
     */
    public PresenterDocumentStatus()
    {
        super();
    }

    //-------------------------------------------------------------------------
    public PresenterDocumentStatus(final PresenterDocumentStatus status) throws Exception
    {
        setCurrentPresenter(status.m_currentPresenterId, status.m_currentPresenterName);
        setActiveSlide(status.m_activeSlide);
        setPausedState(status.m_paused);
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the current presenter name
     *
     * @return
     *  The m_currentPresenterName
     */
    public void setCurrentPresenter(RT2CliendUidType currentPresenterId, String currentPresenterName)
    {
        m_currentPresenterId   = currentPresenterId;
        m_currentPresenterName = currentPresenterName;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the m_currentPresenterName
     *
     * @return
     *  The m_currentPresenterName
     */
    public RT2CliendUidType getCurrentPresenterId()
    {
        return m_currentPresenterId;
    }

    //-------------------------------------------------------------------------
    /**
     * Determines if the provided user is the presenter or not.
     *
     * @param userId
     *  The ID of the user to find out the enabled state.
     *
     * @return
     *  Whether the provided user is the presenter.
     */
    public boolean isPresenter(RT2CliendUidType clientId)
    {
        return (ClientsStatus.isNotEmptyUser(clientId.getValue()) && m_currentPresenterId.equals(clientId));
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the m_currentPresenterName
     *
     * @param currentPresenterName
     * 	The currentPresenterName to set
     */
    public String getCurrentPresenterName()
    {
        return m_currentPresenterName;
    }

    //-------------------------------------------------------------------------
    /**
     * Determine if the presentation is paused.
     *
     * @return
     *  Whether the presentation is paused.
     */
    public boolean isPaused()
    {
        return this.m_paused;
    }

    //-------------------------------------------------------------------------
    /**
     * Set the paused state of the presentation.
     *
     * @param state
     *  The paused state.
     */
    public void setPausedState(boolean state)
    {
        if (m_paused != state)
        {
            this.m_paused = state;
            setModified(true);
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the id of the active slide.
     *
     * @return
     *  The zero based slide id.
     */
    public int getActiveSlide()
    {
    	return this.m_activeSlide;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the active slide.
     *
     * @param slide
     *  The slide id.
     *
     * @return
     *  Returns true if the previous slide id differs from the new one.
     */
    public boolean setActiveSlide(int slide) {
        if (this.m_activeSlide != slide)
        {
            this.m_activeSlide = slide;
            setModified(true);
            return true;
    	}
    	return false;
    }

    //-------------------------------------------------------------------------
    /**
     * Creates a shallow clone of this connection status
     * object.
     *
     * @return
     *  A shallow clone.
     */
    @Override
    public PresenterDocumentStatus clone() throws CloneNotSupportedException
    {
        PresenterDocumentStatus clone = null;
        try
        {
            clone = new PresenterDocumentStatus(this);
        }
        catch (Exception e)
        {
            throw new CloneNotSupportedException("Clone not possible!");
        }
        return clone;
    }

    //-------------------------------------------------------------------------
    /**
     * Creates the JSON representation of the current connection status
     * object. The JSON object should only provide properties that are
     * part of the client/server communication. Internal properties should
     * be omitted.
     *
     * @return
     *  A JSON object that contains the properties of the current connection
     *  status. The JSON object can be empty in case of an error.
     */
    @Override
    public JSONObject toJSON()
    {
        final JSONObject ret = new JSONObject();

        try
        {
            ret.put("presenterId"  , m_currentPresenterId);
            ret.put("presenterName", m_currentPresenterName);
            ret.put("activeSlide"  , m_activeSlide);
            ret.put("paused"       , m_paused);
        }
        catch (JSONException e)
        {
        	log.error("Exception while creating JSON object of the current connection status", e);
        }

        return ret;
    }

}
