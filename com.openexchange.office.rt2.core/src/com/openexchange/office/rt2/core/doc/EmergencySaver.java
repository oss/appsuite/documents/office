/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class EmergencySaver {

    private static final Logger log = LoggerFactory.getLogger(EmergencySaver.class);

    @Autowired
    private RT2DocProcessorManager docProcessorManager;

    //-------------------------------------------------------------------------
    public void emergencySave() {
        final Set<WeakReference<RT2DocProcessor>> aOpenDocs = docProcessorManager.getWeakReferenceToDocProcessors();
        List<WeakReference<IBackgroundSavable>> savableDocs =
            aOpenDocs.stream()
                     .map(w -> WeakRefUtils.getHardRef(w))
                     .filter(Objects::nonNull)
                     .filter(r -> (r instanceof IBackgroundSavable))
                     .map(r -> new WeakReference<IBackgroundSavable>((IBackgroundSavable)r))
                     .collect(Collectors.toList());
        emergencySave(savableDocs);
    }

    //-------------------------------------------------------------------------
    private void emergencySave(final List<WeakReference<IBackgroundSavable>> docsForEmergencySave) {
        for (WeakReference<IBackgroundSavable> ref : docsForEmergencySave) {
            final IBackgroundSavable savableDoc = ref.get();
            if (null != savableDoc) {
                try {
                    MDCHelper.safeMDCPut(MDCEntries.DOC_UID, savableDoc.getUniqueID().getValue());
                    // filter document list to find only documents that must be saved or
                    // reset their advisory lock info
                    if (savableDoc.isModified() || savableDoc.hasSetAdvisoryLock()) {
                        saveDocument(savableDoc);
                    }
                } finally {
                    MDC.remove(MDCEntries.DOC_UID);
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    private boolean saveDocument(final IBackgroundSavable aSavableDoc) {
        try {
            aSavableDoc.save(BackgroundSaveReason.SHUTDOWN, false);
            return true;
        } catch (final Throwable t) {
            ExceptionUtils.handleThrowable(t);
            log.error("RT2: Exception caught while trying to save document with com.openexchange.rt2.document.uid " + aSavableDoc.getUniqueID(), t);
            return false;
        }
    }

}
