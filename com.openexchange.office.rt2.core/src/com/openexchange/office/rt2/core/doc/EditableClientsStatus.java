/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class EditableClientsStatus extends ClientsStatus {

    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(EditableClientsStatus.class);
	
    //-------------------------------------------------------------------------
    public static final String PROP_REQUESTS_EDITRIGHTS = "requestsEditRights";
    public static final String PROP_CLIENT_OSN = "clientOSN";
    public static final String PROP_HANGUP_STATE = "hangupState";

    public static final int    MIN_CLIENT_OSN = -1;

	//-------------------------------------------------------------------------
	public EditableClientsStatus() {
		super();
	}

	//-------------------------------------------------------------------------
	public EditableClientsStatus(final EditableClientsStatus aEditableClientsStatus) throws Exception {
		super(aEditableClientsStatus);
	}

    //-------------------------------------------------------------------------
    public void setRequestsEditRights(RT2CliendUidType userId, boolean state) throws Exception {
    	final JSONObject aUserData = getUserData(userId);

    	if (null != aUserData) {
    		aUserData.put(PROP_REQUESTS_EDITRIGHTS, state);
    		setModified(userId);
    	}
    }

    //-------------------------------------------------------------------------
    public boolean hasEditRigths(RT2CliendUidType userId) throws Exception {
    	final JSONObject aUserData = getUserData(userId);
    	if (null != aUserData) {
    		return aUserData.getBoolean(PROP_REQUESTS_EDITRIGHTS);
    	}
    	return false;
    }
    
    //-------------------------------------------------------------------------
    public boolean requestsEditRights(RT2CliendUidType userId) throws Exception {
    	final JSONObject aUserData = getUserData(userId);

    	if (null != aUserData) {
    		return aUserData.optBoolean(PROP_REQUESTS_EDITRIGHTS, false);
    	}

    	return false;
    }

    //-------------------------------------------------------------------------
    public void clearRequestsEditRights() throws Exception {
        final Map<RT2CliendUidType, JSONObject> aClientsMap = getClientsMap();

        for (final JSONObject o : aClientsMap.values()) {
            final String sClientUID           = o.getString(PROP_CLIENTUID);
            final boolean bRequestsEditRights = o.optBoolean(PROP_REQUESTS_EDITRIGHTS, false);

            if (bRequestsEditRights) {
                setModified(new RT2CliendUidType(sClientUID));
                o.put(PROP_REQUESTS_EDITRIGHTS, false);
            }
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Get the client osn associated with the provided com.openexchange.rt2.client.uid.
     * REMARK: This value is only used in the concurrent editing context.
     *
     * @param clientUID
     *  The client uid
     * @return
     *  The client osn of the associated com.openexchange.rt2.client.uid currently known
     *  by the backend or -1 if the com.openexchange.rt2.client.uid is unknown.
     * @since 7.10.3 
     */
    public int getClientOSN(RT2CliendUidType clientUID) {
        final JSONObject userData = getUserData(clientUID);
        return (null != userData) ? userData.optInt(PROP_CLIENT_OSN, -1) : -1;
    }

    //-------------------------------------------------------------------------
    /**
     * Get the client osn associated with the provided com.openexchange.rt2.client.uid.
     * REMARK: This value is only used in the concurrent editing context.
     *
     * @param clientUID
     *  The client uid
     * @return
     *  The client osn of the associated com.openexchange.rt2.client.uid currently known
     *  by the backend.
     * @since 7.10.3 
     */
    public void setClientOSN(RT2CliendUidType clientUID, int newClientOSN) {
        final JSONObject userData = getUserData(clientUID);
        if (null != userData) {
            try {
	        	userData.put(PROP_CLIENT_OSN, newClientOSN);
            } catch (JSONException e) {
                log.error("Exception caught changing client data", e);
            }
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieves the hang-up state of a client.
     *
     * @param clientUID the client uid of the client for which the hang-up state
     * should be retrieved
     * @return the hang-up state - will be FALSE if the client is unknown
     */
    public boolean isHangupClient(RT2CliendUidType clientUID) {
    	return getHangupState(clientUID);
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieves the hang-up state of a client.
     *
     * @param clientUID the client uid of the client for which the hang-up state
     * should be retrieved
     * @return the hang-up state - will be FALSE if the state was not set or the
     * client is unknown
     */
    public boolean getHangupState(RT2CliendUidType clientUID) {
        final JSONObject userData = getUserData(clientUID);
        return (null != userData) ? userData.optBoolean(PROP_HANGUP_STATE, false) : false;
    }

    //-------------------------------------------------------------------------
    /**
     * Changes the hang-up state of a client.
     *
     * @param clientUID the client uid that should change the hang-up state
     * @param newHangupState the new hang-up state
     */
    public void setHangupState(RT2CliendUidType clientUID, boolean newHangupState) {
        final JSONObject userData = getUserData(clientUID);
        if (null != userData) {
            try {
	        	userData.put(PROP_HANGUP_STATE, newHangupState);
            } catch (JSONException e) {
                log.error("Exception caught changing client data", e);
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
	public EditableClientsStatus clone() throws CloneNotSupportedException {
        try {
            return new EditableClientsStatus(this);
        } catch (Exception e) {
            log.error("Cannot create clone due to exception", e);
            throw new CloneNotSupportedException("Cannot create clone");
        }
	}

    //-------------------------------------------------------------------------
    @Override
    protected void mergeInitalClientStatus(final JSONObject aClientData, String userDisplayName, int id, boolean guest, final JSONObject userData) throws Exception {
        aClientData.put(PROP_REQUESTS_EDITRIGHTS, false);
    }
}
