/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.monitoring.CloseEvent;
import com.openexchange.office.tools.monitoring.CloseType;
import com.openexchange.office.tools.monitoring.DocumentEvent;
import com.openexchange.office.tools.monitoring.ErrorType;
import com.openexchange.office.tools.monitoring.OTEvent;
import com.openexchange.office.tools.monitoring.OpenEvent;
import com.openexchange.office.tools.monitoring.OpenType;
import com.openexchange.office.tools.monitoring.OperationsEvent;
import com.openexchange.office.tools.monitoring.OperationsType;
import com.openexchange.office.tools.monitoring.SaveEvent;
import com.openexchange.office.tools.monitoring.SaveType;
import com.openexchange.office.tools.monitoring.Statistics;

@Service
public class DocumentEventHelper
{
	@Autowired
	private Statistics statistics;

    //-------------------------------------------------------------------------
    static private final long MAX_TIME_FOR_BINARY_CONVERSION_CONSIDERATION = 30000; // time in milliseconds

    //-------------------------------------------------------------------------
    /**
     * Adds a open error event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addOpenErrorEvent(final DocumentType aDocType)
    {
        statistics.handleDocumentEvent(new DocumentEvent(aDocType, ErrorType.LOAD));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a open event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addOpenEvent(final DocumentFormat aDocFormat, final DocumentMetaData docMetaData)
    {
        statistics.handleDocumentEvent(getOpenEvent(aDocFormat, docMetaData));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a save event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addSaveEvent(final DocumentType aDocType, SaveType type)
    {
        statistics.handleDocumentEvent(new SaveEvent(aDocType, type));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a close event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addCloseEvent(final DocumentType aDocType)
    {
        statistics.handleDocumentEvent(new CloseEvent(aDocType, CloseType.CLOSE));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a timeout on close event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addTimeoutEvent(final DocumentType aDocType)
    {
        statistics.handleDocumentEvent(new CloseEvent(aDocType, CloseType.TIMEOUT));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a distributed document event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     * @param otherClientCount The number of clients connected with this document.
     */
    public void addDistributedEvent(final DocumentType aDocType, int otherClientCount)
    {
        if (otherClientCount > 0) {
            statistics.handleDocumentEvent(new OperationsEvent(aDocType, OperationsType.DISTRIBUTED, otherClientCount));
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Adds an incoming event to the monitoring data.
     *
     * @param fileHelper The current file helper instance.
     */
    public void addIncomingEvent(final DocumentType aDocType)
    {
        statistics.handleDocumentEvent(new OperationsEvent(aDocType, OperationsType.INCOMING));
    }

    //-------------------------------------------------------------------------
    /**
     * Adds a transformation event to the monitoring data.
     *
     * @param otEvent The transformation event that occuered
     */
    public void addTransformationEvent(final OTEvent otEvent)
    {
        statistics.handleTransformation(otEvent);
    }

    //-------------------------------------------------------------------------
    /**
     * Provides an OpenEvent instance to be used for administrative purposes like VM surveillance.
     *
     * @return An OpenEvent instance describing the current document.
     */
    private static final OpenEvent getOpenEvent(DocumentFormat aDocFormat, final DocumentMetaData docMetaData)
    {
        OpenEvent openEvent = null;

        OpenType documentOpenType = OpenType.NONE;
        final Date docCreated = (docMetaData == null) ? new Date() : docMetaData.getCreated();
        final long docExistsForMilliseconds = (null != docCreated) ? (System.currentTimeMillis() - docCreated.getTime()) : Long.MAX_VALUE;

        // mark document type as binary only if it has just been converted
        if ((docMetaData != null) && docMetaData.isBinaryConverted() && (docExistsForMilliseconds <= MAX_TIME_FOR_BINARY_CONVERSION_CONSIDERATION))
        {
            documentOpenType = OpenType.BINARY;
        }
        else
        {
            switch (aDocFormat)
            {
            case DOCX:
            case XLSX:
            case PPTX:
            {
                documentOpenType = OpenType.MSX;
                break;
            }

            case ODT:
            case ODS:
            case ODP:
            {
                documentOpenType = OpenType.ODF;
                break;
            }

            case NONE:
            case ODG:
            default:
            {
                documentOpenType = OpenType.NONE;
                break;
            }
            }
        }

        if (OpenType.NONE != documentOpenType) {
            openEvent = new OpenEvent(DocumentFormat.toDocumentType(aDocFormat), documentOpenType, docMetaData.getFileSize());
        }

        return openEvent;
    }
}
