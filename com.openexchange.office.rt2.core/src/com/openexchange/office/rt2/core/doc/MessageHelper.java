/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.SyncInfo;

public class MessageHelper {

	// -------------------------------------------------------------------------
	private MessageHelper() {}

	// -------------------------------------------------------------------------
	public static void setDocumentStatus(final RT2Message aMsg, final DocumentStatus aDocStatus) throws JSONException {
		final JSONObject aBody = aMsg.getBody();
		aBody.put(MessageProperties.PROP_DOCSTATUS, aDocStatus.toJSON());
		aMsg.setBody(aBody);
	}

	// -------------------------------------------------------------------------
	public static void setDocumentStatus(final RT2Message aMsg, final JSONObject aDocStatus) throws Exception {
		final JSONObject aBody = aMsg.getBody();
		aBody.put(MessageProperties.PROP_DOCSTATUS, aDocStatus);
		aMsg.setBody(aBody);
	}

	// -------------------------------------------------------------------------
	public static void setClientsStatus(final RT2Message aMsg, final ClientsStatus aClientsStatus, boolean bComplete)
			throws Exception {
		final JSONObject aBody = aMsg.getBody();
		if (bComplete)
			aBody.put(MessageProperties.PROP_CLIENTS, aClientsStatus.toFullJSON());
		else
			aBody.put(MessageProperties.PROP_CLIENTS, aClientsStatus.toJSON());
		aMsg.setBody(aBody);
	}

	// -------------------------------------------------------------------------
	public static void setClientsStatus(final RT2Message aMsg, final JSONObject aClientsStatus) throws Exception {
		final JSONObject aBody = aMsg.getBody();
		aBody.put(MessageProperties.PROP_CLIENTS, aClientsStatus);
		aMsg.setBody(aBody);
	}

	// -------------------------------------------------------------------------
	public static JSONObject getError(final RT2Message aMsg) throws Exception {
		return aMsg.getBody().getJSONObject(MessageProperties.PROP_ERROR);
	}

	// -------------------------------------------------------------------------
	public static void setError(final RT2Message aMsg, final ErrorCode aErrorCode) throws Exception {
		aMsg.getBody().put(MessageProperties.PROP_ERROR, aErrorCode.getAsJSON());
	}

	// -------------------------------------------------------------------------
	public static JSONArray getOperations(final RT2Message aMsg) throws Exception {
		return aMsg.getBody().getJSONArray(MessageProperties.PROP_OPERATIONS);
	}

	// -------------------------------------------------------------------------
	public static void setOperations(final RT2Message aMsg, final JSONArray lOps) throws Exception {
		aMsg.getBody().put(MessageProperties.PROP_OPERATIONS, lOps);
	}

	// -------------------------------------------------------------------------
	public static JSONArray getRescueOperations(RT2Message msg) throws Exception {
		JSONArray result = msg.getBody().optJSONArray(MessageProperties.PROP_RESCUE_OPERATIONS);
		return (result == null) ? new JSONArray() : result;
	}

	// -------------------------------------------------------------------------
	public static void setRescueOperations(RT2Message msg, JSONArray ops) throws Exception {
		msg.getBody().put(MessageProperties.PROP_RESCUE_OPERATIONS, ops);
	}

	// -------------------------------------------------------------------------
	public static void setHtmlDoc(final RT2Message aMsg, final String htmlDoc) throws Exception {
		aMsg.getBody().put(MessageProperties.PROP_HTMLDOC, htmlDoc);
	}

	// -------------------------------------------------------------------------
	public static String getHtmlDoc(final RT2Message aMsg) throws Exception {
		return aMsg.getBody().getString(MessageProperties.PROP_HTMLDOC);
	}

	// -------------------------------------------------------------------------
	public static void setSyncInfo(final RT2Message aMsg, final SyncInfo syncInfo) throws Exception {
		aMsg.getBody().put(MessageProperties.PROP_SYNCINFO, syncInfo.toJSON());
	}

	// -------------------------------------------------------------------------
	public static SyncInfo getSyncInfo(final RT2Message aMsg) throws Exception {
		final JSONObject aJSONSyncInfo = aMsg.getBody().getJSONObject(MessageProperties.PROP_SYNCINFO);
		return new SyncInfo(aJSONSyncInfo);
	}

	// -------------------------------------------------------------------------
	public static int getCurrentSlide(final RT2Message aMsg) throws Exception {
		final JSONObject aSlideInfo = aMsg.getBody().getJSONObject(MessageProperties.PROP_SLIDEINFO);
		return aSlideInfo.getInt(MessageProperties.PROP_ACTIVESLIDE);
	}

	// -------------------------------------------------------------------------
	public static void setCurrentSlide(final RT2Message aMsg, int nActiveSlide) throws Exception {
		final JSONObject aSlideInfo = aMsg.getBody().getJSONObject(MessageProperties.PROP_SLIDEINFO);
		aSlideInfo.put(MessageProperties.PROP_ACTIVESLIDE, nActiveSlide);
	}

	// -------------------------------------------------------------------------
	public static void setActiveSheet(final RT2Message aMsg, int nActiveSheet) throws Exception {
		aMsg.getBody().put(MessageProperties.PROP_ACTIVESHEET, nActiveSheet);
	}

	// -------------------------------------------------------------------------
	public static JSONObject getResult(final RT2Message aMsg) throws Exception {
		return aMsg.getBody().optJSONObject(MessageProperties.PROP_RESULT);
	}

	// -------------------------------------------------------------------------
	public static void setResult(final RT2Message aMsg, final JSONObject aResult) throws Exception {
		JSONObject aBody = aMsg.getBody();
		if (null == aBody)
			aBody = new JSONObject();

		aBody.put(MessageProperties.PROP_RESULT, aResult);
	}

	// -------------------------------------------------------------------------
	public static void setForceEditResult(final RT2Message aMsg, final boolean bForceEditApproved) throws Exception {
		JSONObject aBody = aMsg.getBody();
		if (null == aBody)
			aBody = new JSONObject();

		aBody.put(MessageProperties.PROP_FORCEEDITRESULT, bForceEditApproved);
	}

    // -------------------------------------------------------------------------
	public static void setFolderAndFileId(final RT2Message aMsg, RT2FolderIdType folderId, RT2FileIdType fileId) throws Exception {
        JSONObject aBody = aMsg.getBody();
        if (null == aBody)
            aBody = new JSONObject();

        aBody.put(MessageProperties.PROP_FOLDERID, folderId.getValue());
        aBody.put(MessageProperties.PROP_FILEID, fileId.getValue());
	}

}
