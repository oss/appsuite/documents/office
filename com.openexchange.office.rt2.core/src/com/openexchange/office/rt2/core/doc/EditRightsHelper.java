/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONObject;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;

/**
 * Helper class to retrieve action and value from a "editrights"
 * request.
 *
 * @author Carsten Driesner
 *
 */
public class EditRightsHelper
{
	//-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_REQUESTS = "request";

	//-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_APPROVED = "approved";

	//-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_DECLINED = "declined";

	//-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_FORCE    = "force";

	//-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_ALL      = "@ALL";

	//-------------------------------------------------------------------------
	public static String getAction(final RT2Message aMsg) throws Exception
	{
		if (RT2MessageType.REQUEST_EDITRIGHTS.equals(aMsg.getType())) {
			JSONObject body = aMsg.getBody();
			return body.getString(MessageProperties.PROP_ACTION);
		}
		throw new IllegalArgumentException("RT2Message is not a REQUEST_EDITRIGHTS");
	}

	//-------------------------------------------------------------------------
	public static String getActionValue(final RT2Message aMsg) throws Exception
	{
		if (RT2MessageType.REQUEST_EDITRIGHTS.equals(aMsg.getType())) {
			JSONObject body = aMsg.getBody();
			return body.optString(MessageProperties.PROP_VALUE, null);
		}
		throw new IllegalArgumentException("RT2Message is not a REQUEST_EDITRIGHTS");
	}

	//-------------------------------------------------------------------------
	public static String getAction(final JSONObject aMsgBody) throws Exception
	{
		return aMsgBody.getString(MessageProperties.PROP_ACTION);
	}

	//-------------------------------------------------------------------------
	public static String getActionValue(final JSONObject aMsgBody) throws Exception
	{
		return aMsgBody.optString(MessageProperties.PROP_VALUE, null);
	}

	//-------------------------------------------------------------------------
	private EditRightsHelper() {}
}
