/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class DocumentStatus implements Cloneable
{
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(DocumentStatus.class);

    //-------------------------------------------------------------------------
    public static final RT2CliendUidType EMPTY_CLIENTID = new RT2CliendUidType("");
    public static final String EMPTY_USERNAME = "";
    public static final int    NO_USERID      = -1;

    //-------------------------------------------------------------------------
    private boolean m_bModified = false;

    //-------------------------------------------------------------------------
    public DocumentStatus()
    {
    }

    //-------------------------------------------------------------------------
    public DocumentStatus(final DocumentStatus aDocStatus)
    {
    	m_bModified = aDocStatus.isModified();
    }

    //-------------------------------------------------------------------------
    public boolean isModified()
    {
        return m_bModified;
    }

    //-------------------------------------------------------------------------
    public void setModified(boolean bModificationState)
    {
        m_bModified = bModificationState;
    }

    //-------------------------------------------------------------------------
    @Override
    public DocumentStatus clone() throws CloneNotSupportedException
    {
    	try
    	{
            return new DocumentStatus(this);
    	}
    	catch (Throwable e)
    	{
            ExceptionUtils.handleThrowable(e);
    		log.error("Cannot create clone due to exception", e);
    		throw new CloneNotSupportedException("Cannot create clone");
    	}
    }

    //-------------------------------------------------------------------------
    public JSONObject toJSON()
    {
    	return new JSONObject();
    }
}
