/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.service.logging.SpecialLogService;

/**
 * Implementation class of the global RT2 service to register/unregister for
 * document events like created/disposed.
 * Don't create instances of this class by yourself. The functions are provided
 * via services and must be retrieved from your service manager.
 *
 * @author Carsten Driesner
 * @since 7.10.0
 *
 */
@RegisteredService
@Service
public class RT2DocStateNotifier 
{
    //-------------------------------------------------------------------------
    public static final String DOCSTATE_EVENT_CREATED  = "created";
    public static final String DOCSTATE_EVENT_DISPOSED = "disposed";

    private static final Logger log = LoggerFactory.getLogger(RT2DocStateNotifier.class);

    //-------------------------------------------------------------------------
    private static class Event
    {
        public final String m_sEventName;
        public final RT2DocProcessor m_aDocProcInstance;

        public Event(String sEventName, final RT2DocProcessor aDocProc)
        {
            m_sEventName       = sEventName;
            m_aDocProcInstance = aDocProc;
        }
    }

    @Autowired
    private SpecialLogService specialLogService;
    
    //-------------------------------------------------------------------------
    private final CopyOnWriteArraySet<IDocNotificationHandler> handlers = new CopyOnWriteArraySet<>();

    //-------------------------------------------------------------------------
    public void addDocNotificationHandler(IDocNotificationHandler handler) {
    	handlers.add(handler);
    }
    
    //-------------------------------------------------------------------------
    public void notifyEvent(final String sEventName, final RT2DocProcessor aDocProc)
    {    	
    	specialLogService.log(Level.DEBUG, log, aDocProc.getDocUID().getValue(), new Throwable(), "Received event {} for com.openexchange.rt2.document.uid {}", sEventName, aDocProc.getDocUID());
        final Event aEvent = new Event(sEventName, aDocProc);
        notifyRegisteredHandlers(aEvent);
    }

    //-------------------------------------------------------------------------
    private void notifyRegisteredHandlers(final Event aEvent)
    {
        if (aEvent != null)
        {
        	if (handlers.isEmpty()) {
        		return;
        	}
            boolean bCancelNotification = false;
            final Iterator<IDocNotificationHandler> aIter = handlers.iterator();
            while (!bCancelNotification && aIter.hasNext())
            {
                try
                {
                    final IDocNotificationHandler aHandler = aIter.next();
                    switch (aEvent.m_sEventName)
                    {
                        case DOCSTATE_EVENT_CREATED: aHandler.docProcessorCreated(aEvent.m_aDocProcInstance); break;
                        case DOCSTATE_EVENT_DISPOSED: aHandler.docProcessorDisposed(aEvent.m_aDocProcInstance); break;
                        default:
                            log.warn("RT2DocNotifier detected a unknown notification: " + aEvent.m_sEventName + ". Event will be discarded!");
                            bCancelNotification = true;
                            break;
                    }
                }
                catch (final Throwable t)
                {
                    ExceptionUtils.handleThrowable(t);
                    log.error("RT2DocStateNotifier: received exception while notification handler has been called!", t);
                }
            }
        }
    }
}
