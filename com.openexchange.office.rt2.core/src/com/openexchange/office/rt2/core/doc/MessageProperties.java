/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

public class MessageProperties {
	private MessageProperties() {}

	public static final String PROP_WRITE_PROTECTED = "writeProtected";
	public static final String PROP_CLIENT_IDENTIFIER = "clientId";
	public static final String PROP_RENAME_NEEDS_RELOAD = "renameNeedsReload";
	public static final String PROP_CLIENTID = PROP_CLIENT_IDENTIFIER;
	public static final String PROP_EDITUSERID = "editUserId";
	public static final String PROP_HASERRORS = "hasErrors";
	public static final String PROP_ERROR = "error";
	public static final String PROP_OPERATIONS = "operations";
	public static final String PROP_RESCUE_OPERATIONS = "rescueOperations";
	public static final String PROP_HTMLDOC = "htmlDoc";
	public static final String PROP_SYNCINFO = "syncInfo";
	public static final String PROP_SYNCLOAD = "syncLoad";
	public static final String PROP_PREVIEW = "preview";
	public static final String PROP_SERVEROSN = "serverOSN";
	public static final String PROP_DOCSTATUS = "docStatus";
	public static final String PROP_SLIDEINFO = "slideInfo";
	public static final String PROP_ACTIVESLIDE = "activeSlide";
	public static final String PROP_ACTIVESHEET = "activeSheet";
	public static final String PROP_FILENAME = "fileName";
	public static final String PROP_FILE = "file";
	public static final String PROP_FILEID = "fileId";
	public static final String PROP_FOLDERID = "folderId";
	public static final String PROP_SYNC = "sync";
	public static final String PROP_ACTION = "action";
	public static final String PROP_VALUE = "value";
	public static final String PROP_CLIENTS = "clients";
	public static final String PROP_SANITIZEDFILENAME = "com.openexchange.file.sanitizedFilename";
	public static final String PROP_FORCEEDITRESULT = "forceEditResult";
	public static final String PROP_RESULT = "result";
	public static final String PROP_PERFORMANCE = "performanceData";
}
