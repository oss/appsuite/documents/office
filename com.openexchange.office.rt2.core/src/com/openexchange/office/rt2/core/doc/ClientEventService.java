/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.concurrent.CopyOnWriteArraySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.service.logging.SpecialLogService;

/**
 * Service to manage listeners and notifications for client events.
 * Client events includes the adding and removal of clients to
 * DocProcessor instances.
 *
 * @author Carsten Driesner
 * @since 7.10.3
 */
@Service
public class ClientEventService {
	
	private static final Logger log = LoggerFactory.getLogger(ClientEventService.class);
	
	@Autowired
	private SpecialLogService specialLogService;
	
	private CopyOnWriteArraySet<IClientEventListener> listeners = new CopyOnWriteArraySet<>();
	
	public void addListener(IClientEventListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(IClientEventListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notify listeners that a client has been added to a specific
	 * DocProcessor instance.
	 *
	 * @implNote The notifications must be sent synchronously as the
	 * implementation relies on this fact.
	 *
	 * @param msg the REQUEST_JOIN message of the client that has been added to the doc processor
	 */
	public void notifyClientAdded(RT2Message msg) {
		specialLogService.log(Level.DEBUG, log, msg.getDocUID().getValue(), new Throwable(), "notifiy client with com.openexchange.rt2.client.uid {} added to document with com.openexchange.rt2.document.uid {}", msg.getClientUID(), msg.getDocUID());
		if (RT2MessageType.REQUEST_JOIN.equals(msg.getType())) {
			listeners.forEach(l -> {
			    try {
	                l.clientAdded(new ClientEventData(msg.getDocUID(),  msg.getClientUID()));
			    } catch (Exception e) {
                    log.error("ClientEventService caught exception while notifying listener about clientAdded", e);
			    }
			});
		}
	}

	/**
	 * Notify listeners that a client has been removed from a specific
	 * DocProcessor instance.
	 *
     * @implNote The notifications must be sent synchronously as the
     * implementation relies on this fact.
	 *
	 * @param clientEventData the client event data which provides the context
	 * information of the client and doc processor instance.
	 */
	public void notifyClientRemoved(ClientEventData clientEventData) {
		specialLogService.log(Level.DEBUG, log, clientEventData.getDocUid().getValue(), new Throwable(), "notifiy client with com.openexchange.rt2.client.uid {} removed from document with com.openexchange.rt2.document.uid {}", clientEventData.getClientUid(), clientEventData.getDocUid());
		listeners.forEach(l -> {
		    try {
	            l.clientRemoved(clientEventData);
		    } catch (Exception e) {
                log.error("ClientEventService caught exception while notifying listener about clientRemoved", e);
		    }
		});
	}
}
