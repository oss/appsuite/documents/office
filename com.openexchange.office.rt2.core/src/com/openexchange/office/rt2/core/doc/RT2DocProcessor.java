/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.document.api.Document;
import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo.Direction;
import com.openexchange.office.rt2.core.logging.IMessagesLoggable;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;

//=============================================================================
/** implements the back bone of a RT2 document ...
 *  This instance exists one times for every document within the cluster.
 *  It knows all states about those document and control it's life cycle.
 */
public abstract class RT2DocProcessor implements Document, IMessagesLoggable {

	public enum ProcessingState {
		CREATED,
		INITIALIZING,
		OPEN,
		DISPOSED
	}	
	
    private RT2DocUidType m_sDocUID = null;
    
    protected AtomicReference<ProcessingState> m_aProcState = new AtomicReference<>(ProcessingState.CREATED);
   
    //----------------------------Services-------------------------------------
    @Autowired
    protected DocProcessorMessageLoggerService rt2MessageLoggerService;
    
    @Autowired
    protected RT2DocStateNotifier rt2DocStateNotifier;
    
    @Autowired
    protected SessionService sessionService;
    
    @Autowired
    protected ConfigurationService configurationService;
    
    @Autowired
    protected Statistics statistics;
    
    @Autowired
    protected DocumentEventHelper documentEventHelper;
    
    @Autowired
    protected SpecialLogService specialLogService;
    
    //-------------------------------------------------------------------------
	public abstract String getDocTypeIdentifier();

    //-------------------------------------------------------------------------
	public abstract List<DocProcessorClientInfo> getClientsInfo();
	
	//-------------------------------------------------------------------------
	public abstract void updateClientsInfo(Collection<RT2CliendUidType> existingClients);

	//-------------------------------------------------------------------------	
	public abstract boolean enqueueMessage(final RT2Message msg) throws Exception;
	
	//=============================================================================
	// Default methods for predefined messages
	//=============================================================================

    //-------------------------------------------------------------------------
    public abstract boolean onJoin(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
    public abstract boolean onCloseAndLeave(final RT2Message aMsg) throws Exception;
    
    //-------------------------------------------------------------------------
    public abstract boolean onLeave(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onAdditionalOpenDoc(final RT2Message aMsg) throws Exception;    

    //-------------------------------------------------------------------------
	public abstract boolean onFirstOpenDoc(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onOpenDoc(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onCloseDoc(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onApplyOperations(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onSaveDoc(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onSync(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onSyncStable(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onEditRights(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onUnavailable(final RT2Message aMsg) throws Exception;

    //-------------------------------------------------------------------------
	public abstract boolean onEmergencyLeave(final RT2Message aMsg) throws Exception;

	public abstract List<UserData> getUserDatasCloned();

	public abstract int getPendingOperationsCount();
	
	public void init() {
	    // nothing to do in base class - must be implemented by sub-class
	};

	//-------------------------------------------------------------------------
	public synchronized void setDocUID (final RT2DocUidType docUID) {
		m_sDocUID = docUID;
		rt2MessageLoggerService.add(docUID, Direction.NEW_INSTANCE, "RT2DocProcessor::constructor", null);
		rt2DocStateNotifier.notifyEvent(RT2DocStateNotifier.DOCSTATE_EVENT_CREATED, this);
	}
	
	//-------------------------------------------------------------------------	
	public Map<RT2SessionIdType, RT2CliendUidType> getSessionIdToClientUids() {
		Map<RT2SessionIdType, RT2CliendUidType> res = new HashMap<>();
		for (UserData userData : getUserDatasCloned()) {
			res.put(new RT2SessionIdType(userData.getSessionId()), new RT2CliendUidType(userData.getClientUid()));
		}
		return res;
	}

	//-------------------------------------------------------------------------
	public boolean isDisposed() {
        return (m_aProcState.get() == ProcessingState.DISPOSED);
    }

    //-------------------------------------------------------------------------
	protected void setDisposed() {
        m_aProcState.set(ProcessingState.DISPOSED);
	}
		
    //-------------------------------------------------------------------------
	public void dispose() {
		rt2DocStateNotifier.notifyEvent(RT2DocStateNotifier.DOCSTATE_EVENT_DISPOSED, this);
    }

    //-------------------------------------------------------------------------
	public List<String> formatMsgsLogInfoForClient(RT2CliendUidType clientUid) {
    	return rt2MessageLoggerService.formatMsgsLogInfoForClient(getDocUID(), clientUid);
	}

    //-------------------------------------------------------------------------
    @Override
	public RT2DocUidType getDocUID () {
		return m_sDocUID;
	}

    //-------------------------------------------------------------------------
    @Override
	public List<String> formatMsgsLogInfo() {
    	return rt2MessageLoggerService.formatMsgsLogInfo(getDocUID());
	}

    //-------------------------------------------------------------------------
	@Override
	public RT2CliendUidType getClientUID() {
		return null;
	}

	//-------------------------------------------------------------------------	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_sDocUID == null) ? 0 : m_sDocUID.hashCode());
		return result;
	}

    //-------------------------------------------------------------------------	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RT2DocProcessor other = (RT2DocProcessor) obj;
		if (m_sDocUID == null) {
			if (other.m_sDocUID != null)
				return false;
		} else if (!m_sDocUID.equals(other.m_sDocUID))
			return false;
		return true;
	}
}
