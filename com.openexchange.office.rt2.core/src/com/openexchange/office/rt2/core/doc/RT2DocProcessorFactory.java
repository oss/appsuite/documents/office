/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.config.RT2Const;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;

@Service
public class RT2DocProcessorFactory implements OsgiBundleContextAware {
	private static final Logger log = LoggerFactory.getLogger(RT2DocProcessorFactory.class);

	private OsgiBundleContextAndActivator bundleCtx;

    //-------------------------------------------------------------------------
    public RT2DocProcessor newInstance(String sDocType) throws Exception {
        Validate.notEmpty(sDocType);

        RT2DocProcessor instance = null;

        switch (sDocType) {
	        case RT2Const.DOCTYPE_TEXT: instance = new TextDocProcessor(); break;
	        case RT2Const.DOCTYPE_SPREADSHEET: instance = new SpreadsheetDocProcessor(); break;
	        case RT2Const.DOCTYPE_PRESENTATION: instance = new PresentationDocProcessor(); break;
	        case RT2Const.DOCTYPE_PRESENTER: instance = new PresenterDocProcessor(); break;
	        default:
	            throw new UnsupportedOperationException ("No support for '"+sDocType+"' implemented yet.");
	        }
        return instance;
    }

    //-------------------------------------------------------------------------
    public RT2DocProcessor createInstance(String sDocType, RT2DocUidType rt2DocUid, RT2FileIdType rt2FileId) {
        try {
            RT2DocProcessor docProc = newInstance(sDocType);
            bundleCtx.injectDependencies(docProc);
            docProc.init();
            docProc.setDocUID(rt2DocUid);
            log.info("Created Doc Processor instance for doc-type {} and com.openexchange.rt2.document.uid {} using file-id {}", sDocType, rt2DocUid.getValue(), rt2FileId.getValue());
            return docProc;
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
