/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.log.LogProperties;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.backup.restore.RestoreData;
import com.openexchange.office.document.api.AdvisoryLockInfo;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.document.api.BackupFileService;
import com.openexchange.office.document.api.FlushableDocument;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentInfo;
import com.openexchange.office.document.api.ResolvedStreamInfo;
import com.openexchange.office.document.api.SaveResult;
import com.openexchange.office.document.tools.EmptyDocData;
import com.openexchange.office.document.tools.EmptyDocumentCache;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.ot.OTException;
import com.openexchange.office.ot.TransformOptions;
import com.openexchange.office.rt2.core.domain.PendingOperations;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2OldClientUuidType;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.TimeStampUtils;
import com.openexchange.office.tools.common.debug.SaveDebugProperties;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.common.log.LogMethodCallHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.user.LoadState;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.OXDocumentException;
import com.openexchange.office.tools.doc.SyncInfo;
import com.openexchange.office.tools.monitoring.OTEvent;
import com.openexchange.office.tools.monitoring.OTEventType;
import com.openexchange.office.tools.monitoring.SaveType;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tx.TransactionAwares;
import com.openexchange.user.User;

//=============================================================================
public abstract class EditableDocProcessor extends LoadableDocProcessor
		implements IBackgroundSavable, FlushableDocument {

	private enum BroadcastHangup {
		BROADCAST_HANGUP_ALL,
		BROADCAST_HANGUP_EXCEPT_CLOSING_CLIENT
	}

	// -------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(EditableDocProcessor.class);

	// -------------------------------------------------------------------------
	private static final int WAIT_FOR_SAVE_LOCK_FOR_FLUSH = 20000;

    // -------------------------------------------------------------------------
	private final PendingOperations m_aPendingOperations = new PendingOperations();

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aSaveInProgress = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aRevisionless = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aProcessedState = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aReadOnlyState = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	private AtomicLong m_aLastSaveTimeStamp = new AtomicLong(new Date().getTime());

    // -------------------------------------------------------------------------
	private ErrorCode m_aLastSaveErrorCode = ErrorCode.NO_ERROR;

	// -------------------------------------------------------------------------
	private AtomicReference<DocRestoreID> m_aDocRestoreID = new AtomicReference<>(DocRestoreID.getEmptyDocRestoryID());

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aRestoreDocEnabled = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	@Autowired
	protected DocumentBackupController m_aBackupController;

	@Autowired
	protected EmptyDocumentCache emptyDocumentCache;

	@Autowired
	protected ManagedFileManagement managedFileManagement;

	@Autowired
	protected FileHelperServiceFactory fileHelperServiceFactory;

	@Autowired
	protected FolderHelperServiceFactory folderHelperServiceFactory;

	@Autowired
	protected BackupFileService backupFileService;

	@Autowired
	protected IDBasedFileAccessFactory idBasedFileAccessFactory;

	@Autowired
	protected UserConfigurationFactory userConfigurationFactory;

	@Autowired
	protected IRestoreDocument restoreDocument;

	@Autowired
	protected AdvisoryLockInfoService advisoryLockInfoService;

	@Autowired
	protected AdvisoryLockHelperService advisoryLockHelperService;

	// -------------------------------------------------------------------------
	private final Object m_aLock = new Object();

	// -------------------------------------------------------------------------
	private boolean m_bSlowSave = false; // for debugging purpose only (enable/disable) slow save for users

	// -------------------------------------------------------------------------
	private int m_nSlowSaveTime = 0; // default time for slow save delay, must be defined in office.properties

	// -------------------------------------------------------------------------
	private boolean m_bDebugOperations = false;

	// -------------------------------------------------------------------------
	private boolean m_bDebugSaveDocHistory = false;

	// -------------------------------------------------------------------------
	private AtomicReference<RestoreData> m_aRestoreData = new AtomicReference<>(null);

	// -------------------------------------------------------------------------
	private AtomicBoolean m_aNodeShutdown = new AtomicBoolean(false);

    // -------------------------------------------------------------------------
    private AtomicBoolean m_advisoryLockSet = new AtomicBoolean(false);

	// -------------------------------------------------------------------------
	public EditableDocProcessor(ApplicationType appType) throws Exception {
		super(appType);
	}

	@Override
	public void init() {
		super.init();
		// initialize configuration settings
		m_aRestoreDocEnabled.set(configurationService.getBoolProperty("io.ox/office//module/documentRestoreEnabled", true));
		m_bDebugOperations = configurationService.getBoolProperty("io.ox/office//module/debugoperations", false);
		m_bDebugSaveDocHistory = configurationService.getBoolProperty("com.openexchange.office.debugSaveDocHistory", false) && m_bDebugOperations;
		m_bDebugOperations |= configurationService.getBoolProperty("io.ox/office//module/debugavailable", false);
		m_bSlowSave = configurationService.getBoolProperty("io.ox/office//module/debugslowsave", false) && m_bDebugOperations;
		m_nSlowSaveTime = configurationService.getIntProperty("io.ox/office//module/debugslowsavetime", 0) * 1000; // time in
	}

	// -------------------------------------------------------------------------
	protected DocRestoreID createDocRestoreID(final ServerSession session, final File aFileMetaData) {
		final int nContext = session.getContextId();
		final long nLastModifiedTime = aFileMetaData.getLastModified().getTime();
		m_aDocRestoreID.compareAndSet(DocRestoreID.getEmptyDocRestoryID(), DocRestoreID.create(nContext, getDocUID().getValue(), nLastModifiedTime));
		return getDocRestoreID();
	}

	// -------------------------------------------------------------------------
	protected DocRestoreID getDocRestoreID() {
		return m_aDocRestoreID.get();
	}

	// -------------------------------------------------------------------------
	protected final RestoreData getRestoreData() {
		return m_aRestoreData.get();
	}

	// -------------------------------------------------------------------------
	public boolean isSaveRevisionless() {
		return m_aRevisionless.get();
	}

	// -------------------------------------------------------------------------
	public boolean isRestoreEnabled() {
		return m_aRestoreDocEnabled.get() && !isOTEnabled();
	}

	// -------------------------------------------------------------------------
	public boolean isReadOnlyState() {
		return m_aReadOnlyState.get();
	}

	// -------------------------------------------------------------------------
	public void setReadOnlyState() {
		m_aReadOnlyState.compareAndSet(false, true);
	}

    // -------------------------------------------------------------------------
	@Override
    public boolean hasSetAdvisoryLock() {
	    return m_advisoryLockSet.get();
	}

	// -------------------------------------------------------------------------
	private boolean isNodeShutdown() {
		return m_aNodeShutdown.get();
	}

    // -------------------------------------------------------------------------
    private boolean isAdvisoryLockEnabled() {
        return advisoryLockInfoService.isAdvisoryLockEnabled();
    }

    // -------------------------------------------------------------------------
    private boolean isEditor(RT2CliendUidType clientUID) {
        synchronized (getStatusSyncObject()) {
            return RT2CliendUidType.isNotEmptyAndEqual(clientUID, ((EditableDocumentStatus) getDocumentStatus()).getCurrentEditingUserId());
        }
    }

    // -------------------------------------------------------------------------
    private RT2CliendUidType getCurrentEditingClientUID() {
        synchronized (getStatusSyncObject()) {
            return ((EditableDocumentStatus) getDocumentStatus()).getCurrentEditingUserId();
        }
    }

    // -------------------------------------------------------------------------
    private RT2CliendUidType checkToResetEditor(RT2CliendUidType leavingClientUID) {
        RT2CliendUidType result = new RT2CliendUidType();
        synchronized (getStatusSyncObject()) {
            final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatus();

            if (leavingClientUID.equals(aDocStatus.getCurrentEditingUserId())) {
                // reset editor if he/she closes the document to enable other users to receive edit rights
                aDocStatus.setCurrentEditingUser(new RT2CliendUidType(), DocumentStatus.EMPTY_USERNAME);

                // OT: in case of concurrent editing we have to find a possible new savior
                if (isOTEnabled()) {
                    result = findNewSavior();
                    setNewSavior(result);
                }
            }
        }
        return result;
    }

	// -------------------------------------------------------------------------
	@Override
	public void dispose() {
		setDisposed();

		final RT2DocUidType docUID= this.getDocUID();
		final String sDocUID = docUID.getValue();

		MDCHelper.safeMDCPut(MDCEntries.DOC_UID, sDocUID);
		try {
    		log.debug("RT2: dispose called on EditableDocProcessor with com.openexchange.rt2.document.uid {}", docUID);
    
    		// de-register document from the backup manager to enable the
    		// restore gc process to remove the document data after the
    		// configured time-out
    		removeDocumentState();
    
    		if (m_aPendingOperations.hasPendingOperations(isOTEnabled(),(EditableDocumentStatus) getDocumentStatus())) {
    			// There are certain situations where we cannot prevent that a doc processor
    			// with pending operations gets disposed. E.g. closing a document without any
    			// valid session, error on save etc. This log entry on warn would be an
    			// additional log without any benefit. These error cases have been noted/logged
    			// long before. Therefore this log uses debug. If we encounter other scenarios
    			// where logging this on a higher level can give us additional info, please change
    			// the log-level.
    			log.debug("RT2: dispose called with pending operations within com.openexchange.rt2.document.uid {}!", docUID);
    		}
    
    		m_aPendingOperations.clearPendingOperations();
    		super.dispose();
		} finally {
		    MDC.remove(MDCEntries.DOC_UID);
		}
	}

	// -------------------------------------------------------------------------
	protected ErrorCode saveDocument(RT2Message aMsg, boolean bClose, boolean bDirty) throws Exception {
	    MDCHelper.safeMDCPut(MDCEntries.DOC_UID, getDocUID().getValue());

	    try {
	        if (!(aMsg.getType().equals(RT2MessageType.REQUEST_CLOSE_AND_LEAVE) &&
	            Cause.SESSION_INVALID.equals(Cause.fromString(aMsg.getHeader(RT2Protocol.HEADER_CAUSE))))) {
	            final RT2CliendUidType sClientUID = aMsg.getClientUID();

	            RT2CliendUidType sEditorUID = getCurrentEditingClientUID();
	            final boolean bEditorLeaves = (sClientUID.equals(sEditorUID) && bClose);

	            final boolean bNoRestore = RT2MessageGetSet.getNoRestore(aMsg);

	            // Saving the document should always be done using the editor's context.
	            // As fallback we use the client that triggered saveDocument - although
	            // this can fail as well
	            String sSessionID = null;
	            if (aMsg.isSessionMessage()) {
	                sSessionID = RT2MessageGetSet.getSessionID(aMsg);
	            }
	            if (StringUtils.isNotEmpty(sEditorUID.getValue())) {
	                final UserData editorUserData = getUserData(sEditorUID);
	                if (editorUserData != null) {
	                    final Session aSession = sessionService.getSession4Id(editorUserData.getSessionId());
	                    try {
	                        if (aSession != null) {
	                            sSessionID = editorUserData.getSessionId();
	                            MDCHelper.safeMDCPut(LogProperties.Name.SESSION_SESSION_ID.getName(), sSessionID);
	                            log.debug("Switched session in saveDocument from com.openexchange.session.sessionId {} to com.openexchange.session.sessionId {}", sSessionID, editorUserData.getSessionId());
	                        } else {
	                            log.info("com.openexchange.session.sessionId {} of current editing user is invalid! Using current client user com.openexchange.session.sessionId {}", editorUserData.getSessionId(), sSessionID);
	                        }
	                    } finally {
	                        MDC.remove(LogProperties.Name.SESSION_SESSION_ID.getName());
	                    }
	                } else {
	                    sEditorUID = sClientUID;
	                }
	            } else {
	                // fallback - use current user for saving
	                sEditorUID = sClientUID;
	            }

	            ErrorCode errorCode = ErrorCode.NO_ERROR;
	            if (m_aPendingOperations.hasPendingOperations(isOTEnabled(), (EditableDocumentStatus) getDocumentStatusClone())) {
	                final Session aSession = sessionService.getSession4Id(sSessionID);
	                if (aSession == null) {
	                    throw new RT2SessionInvalidException(rt2MessageLoggerService.getMsgsAsList(getDocUID()), RT2SessionInvalidException.SESSION_ID_ADD_INFO, sSessionID);
	                }
	                errorCode = impl_saveDocument(sEditorUID, aSession, true, bClose, bEditorLeaves, false, bNoRestore, bDirty, BroadcastHangup.BROADCAST_HANGUP_EXCEPT_CLOSING_CLIENT);
	                if (errorCode.isNoError()) {
	                    documentEventHelper.addSaveEvent(getDocumentType(), bClose ? SaveType.CLOSE : SaveType.FLUSH);
	                }
	            }
	            return errorCode;
	        }
	    } finally {
	        MDC.remove(MDCEntries.DOC_UID);
	    }
		return ErrorCode.NO_ERROR;
	}

	// -------------------------------------------------------------------------
	@Override
	public int getPendingOperationsCount() {
		return m_aPendingOperations.getPendingOperationsCount(isOTEnabled(), (EditableDocumentStatus) getDocumentStatus());
	}

	// -------------------------------------------------------------------------
	public PendingOperations getPendingOperationsCloned() {
		return m_aPendingOperations.clone();
	}

	// -------------------------------------------------------------------------
    private static void renumberOperations(final JSONArray operations, int nNewServerOSN) {
        try {
            for (int i = 0; i < operations.length(); i++) {
                final JSONObject op = operations.getJSONObject(i);
                if (null != op) {
                    op.put(OperationHelper.KEY_OSN, nNewServerOSN);
                    op.remove(OperationHelper.KEY_OPL);
                }
            }
        } catch (final JSONException e) {
            log.error("JSONException caught trying to renumber operations", e);
        }
    }

	// -------------------------------------------------------------------------
	/**
	 * Checks and if not available sets the persistent document state to the global
	 * DocumentDataManager (which manages the Hazelcast data).
	 *
	 * <br/>
	 * Throws exception if the consistency between the connection document state and
	 * the document loaded is broken. This can happen if a client loaded the
	 * document after the current version was changed due to uploading or set
	 * current version in the OX Drive UI.
	 *
	 * @param oxDoc
	 *            the OXDocument instance of a loaded document
	 */
	@Override
	protected ErrorCode checkAndUpdateDocumentState(final ServerSession aSession, final OXDocumentInfo oxDoc,
			boolean bFirstUser, boolean bFastEmpty) throws Exception {
		final ErrorCode errorCode = super.checkAndUpdateDocumentState(aSession, oxDoc, bFirstUser, bFastEmpty);

		if (bFirstUser) {
		    final File docMetaData = oxDoc.getMetaData();
			if (isRestoreEnabled() && (docMetaData != null)) {
	            final DocRestoreID aDocResId = createDocRestoreID(aSession, docMetaData);
				final String sDocFileName = oxDoc.getDocumentMetaData().getFileName();
				final String sDocMimeType = oxDoc.getDocumentMetaData().getMimeType();

				try {
					final int nUniqueDocId = oxDoc.getUniqueDocumentId();
					byte[] aDocBuffer = oxDoc.getDocumentBuffer();

					if (bFastEmpty) {
						// Retrieve document buffer from the empty cache service which must have
						// been filled before by using the "createdefaultdocument" rest API
						final String sLangCode = UserHelper.getLangCode(aSession.getUser());
						final EmptyDocData aModuleDocData = emptyDocumentCache.getDocData(ApplicationType.enumToString(getApplicationType()), sLangCode);

						if (null != aModuleDocData)
							aDocBuffer = aModuleDocData.getDocumentData();
					}

					m_aBackupController.registerDocument(aDocResId, sDocFileName, sDocMimeType, nUniqueDocId, oxDoc.getVersion(), aDocBuffer);
				} catch (Exception e) {
					log.warn("RT2: Exception caught while trying to register a document to the backup manager", e);
				}
			}
		} else if (errorCode.isError() && errorCode.getCode() == ErrorCode.LOADDOCUMENT_FILE_HAS_CHANGED_ERROR.getCode()) {
			final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatus();
			// set the optional value to the current editor
			if (isOTEnabled()) {
	            errorCode.setValue("");
			} else {
	            errorCode.setValue(aDocStatus.getCurrentEditingUserName());
			}
		}

		return errorCode;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean isSaveInProgress() {
		return m_aSaveInProgress.get();
	}

	// -------------------------------------------------------------------------
	@Override
	public ErrorCode getLastSaveErrorCode() {
		synchronized (m_aLock) {
			return m_aLastSaveErrorCode.clone();
		}
	}

    // -------------------------------------------------------------------------
	@Override
    public void setLastSaveErrorCode(ErrorCode lastSaveErrorCode) {
        synchronized (m_aLock) {
            m_aLastSaveErrorCode = lastSaveErrorCode;
        }
	}

	// -------------------------------------------------------------------------
	@Override
	public RT2DocUidType getUniqueID() {
		return getDocUID();
	}

    // -------------------------------------------------------------------------
    @Override
    public boolean isModified() {
        return m_aPendingOperations.hasPendingOperations(isOTEnabled(), (EditableDocumentStatus) getDocumentStatus());
    }

	// -------------------------------------------------------------------------
	@Override
	public long getLastSaveTimeStamp() {
		return m_aLastSaveTimeStamp.get();
	}

    // -------------------------------------------------------------------------
    @Override
    public void save(BackgroundSaveReason eReason, boolean force) throws Exception {
        final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatusClone();
        final RT2CliendUidType sEditorID = aDocState.getCurrentEditingUserId();

        final UserData aUserData = getUserData(sEditorID);

        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, getDocUID().getValue());
        try {
            if (aUserData == null) {
                log.debug("RT2: EditableDocProcessor.save no user data for editor found!");
                // in case we don't find any user data reset editor to enable
                // saving again
                RT2CliendUidType newEditor = checkToResetEditor(sEditorID);
                if (RT2CliendUidType.isEmpty(newEditor)) {
                    // set last save error to prevent background save to try saving
                    // endlessly. See DOCS-4113.
                    setLastSaveErrorCode(ErrorCode.SAVEDOCUMENT_NO_CLIENT_IS_CAPABLE_ERROR);
                }
                return;
            }

            final String sSessionID = aUserData.getSessionId();
            if (StringUtils.isEmpty(sSessionID)) {
                // set last save error to prevent background save to try saving
                // endlessly. See DOCS-4113.
                log.debug("RT2: EditableDocProcessor.save for com.openexchange.rt2.document.uid {} not possible, com.openexchange.session.sessionId for editor not set!", getDocUID());
                setLastSaveErrorCode(ErrorCode.SAVEDOCUMENT_NO_CLIENT_HAS_VALID_SESSION_ERROR);
                return;
            }

            Session aEditorSession = null;
            try {
                aEditorSession = sessionService.getSession4Id(sSessionID);
                if (aEditorSession == null) {
                    // set last save error to prevent background save to try saving
                    // endlessly. See DOCS-4113.
                    log.debug("RT2: EditableDocProcessor.save for com.openexchange.rt2.document.uid {} session cannot be retrieved from com.openexchange.session.sessionId {}!", getDocUID(), sSessionID);
                    setLastSaveErrorCode(ErrorCode.SAVEDOCUMENT_NO_CLIENT_HAS_VALID_SESSION_ERROR);
                    return;
                }
            } catch (OXException e) {
                // DOCS-4276 treat exceptions while tying to retrieve session for sessionId as final - to prevent
                // long running background save retries which will fail with a high probability.
                final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_SESSION_INVALID_ERROR, false);
                log.debug("RT2: EditableDocProcessor.save for doc uid {} - exception caught trying to retrieve session object from com.openexchange.session.sessionId {}!", getDocUID(), sSessionID);
                setLastSaveErrorCode(errorCode);
                return;
            }

            if (eReason != BackgroundSaveReason.DELETE_PENDING_OPERATIONS) {
                // store that this document will be saved due to a shutdown
                if (BackgroundSaveReason.SHUTDOWN == eReason)
                    m_aNodeShutdown.set(true);

                // send no hang-up broadcasts in case we do an emergency save on shutdown
                final boolean bNoHangup = (BackgroundSaveReason.SHUTDOWN == eReason);

                final long lastSaveTimeStamp = m_aLastSaveTimeStamp.get();
                final long nStartTime = System.currentTimeMillis();
                final ErrorCode aErrorCode = impl_saveDocument(sEditorID, aEditorSession, false, false, false, bNoHangup, false, false, BroadcastHangup.BROADCAST_HANGUP_ALL);
                final long nEndTime = System.currentTimeMillis();

                final long now = new Date().getTime();
                final long diffInMS = TimeStampUtils.timeDiff(nStartTime, nEndTime);
                final long lastSaveAgoInSecs = TimeStampUtils.timeDiff(lastSaveTimeStamp, now) / 1000;

                specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "Document com.openexchange.rt2.document.uid {} was auto-saved in {} ms due to {}, last save was {} secs ago", getDocUID(), diffInMS, eReason.name(), lastSaveAgoInSecs);

                if (aErrorCode.isNoError()) {
                    documentEventHelper.addSaveEvent(getDocumentType(), (eReason == BackgroundSaveReason.MODIFICATION_TIMEOUT) ? SaveType.OPS_15MINS : SaveType.OPS_100);
                }
            } else {
                // delete pending operations - just for sizing tests
                m_aPendingOperations.clearPendingOperations();
                m_aLastSaveTimeStamp.set(new Date().getTime());
            }
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
        }
    }

	// -------------------------------------------------------------------------
	@Override
	public ManagedFile flushToManagedFile(RT2CliendUidType sClientUID) throws Exception {
		LogMethodCallHelper.logMethodCall(log, this, "flushToManageFile", sClientUID);
        ManagedFile aResultFile = null;

        if (isDisposed())
            throw new OXDocumentException("Document is already disposed!", ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR);

        if (StringUtils.isEmpty(sClientUID.getValue()))
            throw new OXDocumentException("Unknown com.openexchange.rt2.client.uid provided!", ErrorCode.GENERAL_ARGUMENTS_ERROR);

        final UserData aUserData = getUserData(sClientUID);
        if (null == aUserData)
            throw new OXDocumentException("Unknown com.openexchange.rt2.client.uid provided!", ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR);

        final Session aSession = sessionService.getSession4Id(aUserData.getSessionId());
        if (null == aSession)
            throw new OXDocumentException("No session for client accessible!", ErrorCode.GENERAL_SESSION_INVALID_ERROR);

        final IResourceManager aResMgr = getResourceManager();

        OXDocument aOxDocument = null;
        JSONArray aPendingOpsArray = null;

        // ATTENTION:
        // This method can be called from another thread. We need to synchronize with
        // a possible parallel save - otherwise we could use the file content
        // which is no longer in sync with the pending operations!
        if (lockForLoadOrSaveDocumentWithTimeout(WAIT_FOR_SAVE_LOCK_FOR_FLUSH)) {
            try {
                aOxDocument = oxDocumentFactory.createDocumentAccess(aSession, aUserData, false, aResMgr, false, null);

                ErrorCode errCode = aOxDocument.getLastError();
                if (!errCode.isNoError())
                    throw new OXDocumentException(errCode);

                aPendingOpsArray = getAdditionalOperations();

                final EditableDocumentStatus aDocState = (EditableDocumentStatus)getDocumentStatusClone();
                if (!checkConsistencyOfLoadedDocument(aDocState.getVersion(), aDocState.getDocumentOSN(), aDocState.getOperationStateNumber(), aOxDocument))
                    throw new OXDocumentException("Base document file has changed!", ErrorCode.SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR);
            }
            finally {
            	unlockForLoadOrSaveDocument();
            }
        } else {
            throw new OXDocumentException("Couldn't get save lock in time!", ErrorCode.GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR);
        }

        try {
            final IExporter exporter = imExporterAccessFactory.getExporterAccess(aOxDocument.getDocumentFormat());
            final ResolvedStreamInfo aStreamInfo = aOxDocument.getResolvedDocumentStream(exporter, aResMgr, aPendingOpsArray, FileStorageFileAccess.CURRENT_VERSION, true, false, false);

            if (aStreamInfo.errorCode.isNoError() && (null != aStreamInfo.resolvedStream)) {
                final String sFileUUID = UUID.randomUUID().toString();
                aResultFile = managedFileManagement.createManagedFile(sFileUUID, aStreamInfo.resolvedStream);

                if (null == aResultFile)
                    throw new OXDocumentException("Managed file could not be created!", ErrorCode.GENERAL_UNKNOWN_ERROR);
            }
        }
        catch (OXDocumentException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
            log.error(e.getMessage(), e);
            MDC.remove(MDCEntries.ERROR);
            throw new OXDocumentException(ErrorCode.GENERAL_UNKNOWN_ERROR);
        }

        return aResultFile;
    }

	// -------------------------------------------------------------------------
	@Override
	public void setProcessedState(boolean bProcessState) {
		m_aProcessedState.set(bProcessState);
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean isProcessed() {
		return m_aProcessedState.get();
	}

    //-------------------------------------------------------------------------
	// message processing
    //-------------------------------------------------------------------------

	private String getNameOfSystemForAdvisoryLock(RT2Message request) {
	    String nameOfSystem;
	    if (advisoryLockInfoService.getAdvisoryLockMode() == AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN) {
	        nameOfSystem = RT2MessageGetSet.getOriginAsHostName(request);
	    } else {
	        nameOfSystem = advisoryLockInfoService.getAdvisoryLockSystemName();
	    }
	    return nameOfSystem;
	}

    //-------------------------------------------------------------------------
    @Override
    protected RT2Message createJoinResponse(RT2Message joinRequest) throws Exception {
        final RT2Message joinResponse = RT2MessageFactory.createResponseFromMessage(joinRequest, RT2MessageType.RESPONSE_JOIN);

        if (isAdvisoryLockEnabled()) {
            final String nameOfSystem = getNameOfSystemForAdvisoryLock(joinRequest);
            final RT2FolderIdType folderId = joinRequest.getFolderID();
            final RT2FileIdType fileId = joinRequest.getFileID();
            final String sessionId = RT2MessageGetSet.getSessionID(joinRequest);
            final Session session = sessionService.getSession4Id(sessionId);
            final AdvisoryLockInfo advisoryLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, nameOfSystem, folderId.getValue(), fileId.getValue());
            if (advisoryLockInfo.getAdvisoryLockWritten()) {
                m_advisoryLockSet.set(true);
            }

            MessageHelper.setError(joinResponse, advisoryLockInfo.getErrorCode());
        }

        return joinResponse;
    }

	// -------------------------------------------------------------------------
	@Override
	public boolean onFirstOpenDoc(final RT2Message aMsg) throws Exception {
		final Boolean bFastEmpty = RT2MessageGetSet.getFastEmpty(aMsg);
		final Boolean bNewDoc = RT2MessageGetSet.getNewDocState(aMsg);
		final String sAuthCode = RT2MessageGetSet.getAuthCode(aMsg);
		final String advisoryLockAction = RT2MessageGetSet.getAdvisoryLockAction(aMsg);

		// determine restore function dependent on encryption
		// restore is automatically disabled if document is encrypted
		final boolean bRestoreDocEnabled = m_aRestoreDocEnabled.get();
		m_aRestoreDocEnabled.set(bRestoreDocEnabled && StringUtils.isEmpty(sAuthCode));

		if (isAdvisoryLockEnabled() && StringUtils.isNotEmpty(advisoryLockAction)) {
            final String nameOfSystem = getNameOfSystemForAdvisoryLock(aMsg);
            if (advisoryLockHelperService.checkAndApplyAdvisoryLockAction(aMsg, nameOfSystem)) {
                m_advisoryLockSet.set(true);
            }
		}
		checkAndUpdateUserEncryptionInfo(aMsg, true);
		return processOpenDocRequest(aMsg, bFastEmpty, bNewDoc, true);
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onAdditionalOpenDoc(RT2Message aMsg) throws Exception {
		// onOpenDoc is never called for the first client which created
		// this instance.
		// Therefore it's safe to provide FALSE for fastEmpty and
		// for new document (it's safe to assume that the document is
		// in collaboration mode).

		final String advisoryLockAction = RT2MessageGetSet.getAdvisoryLockAction(aMsg);
		if (isAdvisoryLockEnabled() && StringUtils.isNotEmpty(advisoryLockAction)) {
		    final String nameOfSystem = getNameOfSystemForAdvisoryLock(aMsg);
            if (advisoryLockHelperService.checkAndApplyAdvisoryLockAction(aMsg, nameOfSystem)) {
                m_advisoryLockSet.set(true);
            }
		}
		checkAndUpdateUserEncryptionInfo(aMsg, false);
		return processOpenDocRequest(aMsg, false, false, false);
	}

	// -------------------------------------------------------------------------
    @Override
    public boolean onApplyOperations(final RT2Message aMsg) throws Exception {
        boolean result;
        if (isOTEnabled())
            result = applyOperationsOT(aMsg);
        else
            result = applyOperationsOld(aMsg);
        return result;
    }

	// -------------------------------------------------------------------------
	private boolean applyOperationsOT(final RT2Message aMsg) throws Exception {
		final RT2CliendUidType sClientUID = aMsg.getClientUID();
		final JSONObject aBody = aMsg.getBody();
		final Boolean bSync = aBody.optBoolean(MessageProperties.PROP_SYNC);
		final Integer clientOSN = aBody.optInt(MessageProperties.PROP_SERVEROSN);
		final EditableDocumentStatus aDocStatus = (EditableDocumentStatus)getDocumentStatus();
		final EditableClientsStatus clientsStatus = (EditableClientsStatus)getClientsStatusClone();
		final boolean hangupClient = clientsStatus.isHangupClient(sClientUID);
		JSONArray lOperations = aBody.optJSONArray(MessageProperties.PROP_OPERATIONS);

		if (!hangupClient && !isReadOnlyState() && (lOperations != null) && (lOperations.length() > 0)) {
		    final DocumentType documentType = getDocumentType();

			if (BooleanUtils.isTrue(bSync))
				lOperations = impl_checkSyncOperations(lOperations);

			// in case of debug operations we check every operation for some
			// debugging stuff to test client/server code
			if (m_bDebugOperations && !impl_checkValidOTOperations(lOperations)) {
			    documentEventHelper.addTransformationEvent(new OTEvent(OTEventType.UNRESOLVABLE_TRANSFORMATION_RELOAD, documentType));
				hangupClientDueToNonResolvableOTConflicts(sClientUID, ErrorCode.HANGUP_OT_UNRESOLVABLE_OP_CONFLICT_ERROR);
				return false;
			}

			if ((lOperations != null) && (lOperations.length() > 0)) {
				// update statistics with incoming operations requests
				documentEventHelper.addIncomingEvent(documentType);

				int serverOSN = 0;
				synchronized (getStatusSyncObject()) {
					serverOSN = aDocStatus.getOperationStateNumber();
				}

                log.trace("OT DEBUGGING: **********************************************************");
				log.debug("OT DEBUGGING: Apply ops received from com.openexchange.rt2.client.uid " + sClientUID.getValue() + " with osn: " + clientOSN);
				log.trace("OT DEBUGGING: ops: " + lOperations.toString());

				JSONArray transformedOps = null;
				boolean clientUpToDate = (clientOSN == serverOSN);

				try {
					if (m_aPendingOperations.hasPendingOperations(isOTEnabled(), aDocStatus) && !clientUpToDate) {
						final JSONArray pendingOpsClone = m_aPendingOperations.getPendingOperationsFilteredCloned(clientOSN);
						log.trace("OT DEBUGGING: Backend must transform: Pending ops filtered, before : " + pendingOpsClone.toString());
                        final TransformOptions transformOptions = new TransformOptions(documentType, getDocumentFormat());
						if (getApplicationType() == ApplicationType.APP_TEXT) {
	                        transformedOps = com.openexchange.office.ot.text.Transformer.transformOperations(transformOptions, pendingOpsClone, lOperations);
						} else if (getApplicationType() == ApplicationType.APP_PRESENTATION) {
	                        transformedOps = com.openexchange.office.ot.presentation.Transformer.transformOperations(transformOptions, pendingOpsClone, lOperations);
						} else if (getApplicationType() == ApplicationType.APP_SPREADSHEET) {
						    transformedOps = com.openexchange.office.ot.spreadsheet.Transformer.transformOperations(transformOptions, pendingOpsClone, lOperations);
						}
						log.trace("OT DEBUGGING: Backend must transform: Pending ops filtered, after  : " + pendingOpsClone.toString());
					} else {
						transformedOps = lOperations;
					}

					log.trace("OT DEBUGGING: Transformed ops: " + transformedOps.toString());
				} catch (OTException e) {
					final String msg = e.getMessage()!=null ? e.getMessage() : "";
					log.warn("OTException caught - client cannot continue collaboration - must reload document: " + e.getType().toString() + ", " + msg, e);

					documentEventHelper.addTransformationEvent(new OTEvent(OTEventType.UNRESOLVABLE_TRANSFORMATION_RELOAD, documentType));

					final ErrorCode errorCode = mapOTExceptionToErrorCode(e);
					hangupClientDueToNonResolvableOTConflicts(sClientUID, errorCode);
					return false;
				} catch (Exception e) {
				    MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.HANGUP_INVALID_OPERATIONS_SENT_ERROR.getCodeAsStringConstant());
					log.error("Exception caught while trying to transform client operations. com.openexchange.rt2.client.uid " + sClientUID.getValue() + ", client-osn: " + clientOSN, e);
					MDC.remove(MDCEntries.ERROR);
					throw new RT2TypedException(ErrorCode.HANGUP_INVALID_OPERATIONS_SENT_ERROR, null);
				}

				synchronized (getStatusSyncObject()) {
					++serverOSN;
					aDocStatus.setOperationStateNumber(serverOSN);
				}

				// renumber transmitted operations
				renumberOperations(transformedOps, serverOSN);

				m_aPendingOperations.addOperationsOT(transformedOps);

				// add the operations to the DocumentDataManager to have a copy
				// of the operations for a possible later save-as
				log.debug("OT DEBUGGING: Operations received and processed from com.openexchange.rt2.client.uid " + sClientUID + " - new server osn = " + serverOSN);

				final RT2Message aApplyOpsResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APPLY_OPS);
				aApplyOpsResponse.getBody().put(MessageProperties.PROP_SERVEROSN, serverOSN);
				messageSender.sendMessageTo(sClientUID, aApplyOpsResponse);

				// update statistics with distributed operations count
				documentEventHelper.addDistributedEvent(documentType, getUserCount() - 1);

				// Broadcast update message to all other clients
				// Except the sender & clients which are in the load phase
				// as they receive these updates in the last chunk load
				// message!!
				// ATTENTION: ALWAYS use broadcastUpdateMessage to send
				// operations to other clients.
				broadcastUpdateMessage(sClientUID, transformedOps, aMsg);
			}
		} else {
			if ((lOperations == null) || (lOperations.length() == 0)) {
				// should not happen
				log.debug("RT2: No operations could be found within message");
			} else {
				// hang-up client is not allowed to apply further operations
				log.debug("RT2: com.openexchange.rt2.client.uid {} state hang-up = {} prevents client to apply operations", sClientUID, hangupClient);
			}
		}

		return true;
	}

    // -------------------------------------------------------------------------
	private ErrorCode mapOTExceptionToErrorCode(OTException e) {
		final ErrorCode errorCode = ErrorCode.HANGUP_OT_UNRESOLVABLE_OP_CONFLICT_ERROR;
		errorCode.setValue(e.getType().toString());
		errorCode.setDescription(e.getMessage());
		return errorCode;
	}

    // -------------------------------------------------------------------------
    private boolean applyOperationsOld(final RT2Message aMsg) throws Exception {
        final RT2CliendUidType sClientUID = aMsg.getClientUID();
        final JSONObject aBody = aMsg.getBody();
        final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatus();
        JSONArray lOperations = aBody.optJSONArray(MessageProperties.PROP_OPERATIONS);

        if (!isReadOnlyState() && (lOperations != null) && (lOperations.length() > 0)) {

            if (!isEditor(sClientUID))
                throw new RT2TypedException(ErrorCode.HANGUP_NO_EDIT_RIGHTS_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));

            if ((lOperations != null) && (lOperations.length() > 0)) {
                // update statistics with incoming operations requests
                documentEventHelper.addIncomingEvent(getDocumentType());

                final int nNewOSN = m_aPendingOperations.addOperations(lOperations);

                // add the operations to the DocumentDataManager to have a copy
                // of the operations for a possible later save-as
                pushOperationsForRestore(lOperations, nNewOSN);

                log.trace("RT2: Operations received from editor - current server osn = {}", nNewOSN);

                synchronized (getStatusSyncObject()) {
                    aDocStatus.setOperationStateNumber(nNewOSN);
                }

                final RT2Message aApplyOpsResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APPLY_OPS);
                aApplyOpsResponse.getBody().put(MessageProperties.PROP_SERVEROSN, nNewOSN);
                messageSender.sendMessageTo(sClientUID, aApplyOpsResponse);

                // update statistics with distributed operations count
                documentEventHelper.addDistributedEvent(getDocumentType(), getUserCount() - 1);

                // Broadcast update message to all other clients
                // Except the sender & clients which are in the load phase
                // as they receive these updates in the last chunk load
                // message!!
                // ATTENTION: ALWAYS use broadcastUpdateMessage to send
                // operations to other clients.
                broadcastUpdateMessage(sClientUID, lOperations, aMsg);
            }
        } else if (isReadOnlyState()) {
            // Make sure that client doesn't remove its operations - we are not able
            // to store them as the instance doesn't represent the document file anymore.
            // Caused by a storage which only supports non-transparent rename operations.
            log.warn("RT2: Cannot apply operations as document was forced to read-only mode - due to non-transparent rename operation");
            throw new RT2TypedException(ErrorCode.HANGUP_NO_EDIT_RIGHTS_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
        } else {
            // should not happen
            log.debug("RT2: No operations could be found within message");
        }

        return true;
    }

	// -------------------------------------------------------------------------
	@Override
	public boolean onSaveDoc(final RT2Message aMsg) throws Exception {
		final ErrorCode aErrorCode = saveDocument(aMsg, false, false);
		final RT2Message aSaveDocResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_SAVE_DOC);

		RT2MessageGetSet.setError(aSaveDocResponse, aErrorCode);
		messageSender.sendMessageTo(aMsg.getClientUID(), aSaveDocResponse);

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onCloseDoc(RT2Message aMsg) throws Exception {
		final RT2CliendUidType sClientUID = aMsg.getClientUID();
		final boolean bForce = RT2MessageGetSet.getInternalHeader(aMsg, RT2Protocol.HEADER_INTERNAL_FORCE, false);
		final UserData aUserData = getUserData(sClientUID);

		// in case of a force close-request we have to provide the session id
		final String sessionId = aUserData.getSessionId();
		if (bForce)
			RT2MessageGetSet.setSessionID(aMsg, sessionId);

		ErrorCode aErrorCode = ErrorCode.NO_ERROR;
		if (!isNodeShutdown()) {
			// Don't save document on closeDoc in case we were notified
			// that a node shutdown is in progress. Emergency save was
			// already done.
			aErrorCode = saveDocument(aMsg, true, false);
		}

		// Only reset an advisory lock if the feature is enabled and
		// this DocProcessor instance wrote the advisory lock to the
		// document file.
		if (isAdvisoryLockEnabled() && m_advisoryLockSet.get()) {
		    final List<UserData> userDataList = getUserDatasCloned();
		    if (advisoryLockHelperService.isLastUserWithWriteAccess(userDataList, sClientUID))
		        advisoryLockHelperService.clearAdvisoryLock(aUserData);
		}

		// notify document event to update statistics
		final ClientsStatus aClientsStatus = getClientsStatusClone();
		if (aClientsStatus.getDurationOfInactivity(sClientUID) > 0)
			documentEventHelper.addTimeoutEvent(getDocumentType());
		else
			documentEventHelper.addCloseEvent(getDocumentType());

		removeUser(sClientUID);

		final SyncInfo aSyncInfo = new SyncInfo(aUserData.getFolderId(), aUserData.getFileId());
		synchronized (getStatusSyncObject()) {
			final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatus();
			aSyncInfo.updateSyncInfo(aDocStatus.getVersion(), aDocStatus.getDocumentOSN());
		}

	    checkToResetEditor(sClientUID);

		final RT2Message aCloseDocResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_CLOSE_DOC);
		MessageHelper.setSyncInfo(aCloseDocResponse, aSyncInfo);
		RT2MessageGetSet.setError(aCloseDocResponse, aErrorCode);

		// in case we triggered a restore document action while flushing the document
		// we received the new document folder/file data and provide it to the client
		final JSONObject aBodyJSON = aCloseDocResponse.getBody();
		final RestoreData aRestoreData = getRestoreData();
		if (null != aRestoreData) {
			aBodyJSON.put(RestoreData.KEY_RESTORE_FILENAME, aRestoreData.getRestoredFileName());
			aBodyJSON.put(RestoreData.KEY_RESTORE_FILEID, aRestoreData.getRestoredFileId());
			aBodyJSON.put(RestoreData.KEY_RESTORE_FOLDERID, aRestoreData.getRestoredFolderId());
			aBodyJSON.put(RestoreData.KEY_RESTORE_ERRORCODE, aRestoreData.getErrorCode().getCodeAsStringConstant());
		}

		if (!RT2MessageType.REQUEST_CLOSE_AND_LEAVE.equals(aMsg.getType())) {
			messageSender.sendMessageTo(sClientUID, aCloseDocResponse);
		}

		// ATTENTION:
		// Don't call the onCloseDoc at our super class which wants
		// to do the same thing (send close doc response, but without
		// save result!
		if (getUserCount() > 0) {
			// send document/clients status to all other clients
			impl_broadcastDocumentStatusIfModified(sClientUID);
			impl_broadcastClientsStatusIfModified(sClientUID);
		}

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onLeave(final RT2Message aMsg) throws Exception {
		final RT2CliendUidType sClientUID = aMsg.getClientUID();
		final boolean bAutoClose = RT2MessageGetSet.getAutoClose(aMsg);
		final UserData aUserData = getMemberUserData(sClientUID.getValue());

		final boolean bResult = super.onLeave(aMsg);

		if (bAutoClose && (null != aUserData) && (getUserCount() > 0)) {
			// send document/clients status to all other clients
			impl_broadcastDocumentStatusIfModified(sClientUID);
			impl_broadcastClientsStatusIfModified(sClientUID);
		}

		return bResult;
	}

	//-------------------------------------------------------------------------
	@Override
	public boolean onEmergencyLeave(final RT2Message aMsg) throws Exception {
        final RT2CliendUidType sClientUID = aMsg.getClientUID();

        if (!isNodeShutdown()) {
            boolean recalcFlag = false;

            // Security - check edit rights before accepting emergency ops
            // In case of OT we allow every client to send document-content
            // independent operations.
            if (isEditor(sClientUID) || isOTEnabled()) {
                final JSONObject leaveData = aMsg.getBody();
                if (!JSONHelper.isNullOrEmpty(leaveData)) {
                    // ATTENTION:
                    // Emergency operations MUST always be document-content
                    // independent and therefore don't need OSN/OPL. We have
                    // to ensure that we NEVER increase OSN/OPL while applying
                    // emergency operations. They can be sent via HTTP-REST and
                    // this is a race between websocket/http communication
                    // channels.
                    final JSONArray lOperations = leaveData.optJSONArray(LeaveDataProperties.PROP_OPERATIONS);
                    recalcFlag = leaveData.optBoolean(LeaveDataProperties.PROP_RECALCFLAG, false);

                    if (JSONHelper.isNotNullAndEmpty(lOperations)) {
                    	m_aPendingOperations.addEmergencyOperations(lOperations);
                    }
                }
            }

            saveDocument(aMsg, true, recalcFlag);
        }

        if (isAdvisoryLockEnabled() && m_advisoryLockSet.get()) {
            final List<UserData> userDataList = getUserDatasCloned();
            if (advisoryLockHelperService.isLastUserWithWriteAccess(userDataList, sClientUID))
                advisoryLockHelperService.clearAdvisoryLock(getUserData(sClientUID));
        }

        checkToResetEditor(sClientUID);

        // We need to call super-class before we send out broadcasts
        // as the base classes will remove the user from internal lists.
        final boolean result = super.onEmergencyLeave(aMsg);

        if (getUserCount() > 0) {
            impl_broadcastDocumentStatusIfModified(sClientUID);
            impl_broadcastClientsStatusIfModified(sClientUID);
        }

        return result;
    }

	// -------------------------------------------------------------------------
	@Override
	public boolean onSync(final RT2Message aMsg) throws Exception {
		final RT2CliendUidType sClientUID = aMsg.getClientUID();
		final DocumentStatus aDocStatus = getDocumentStatusClone();
		final String sFileID = getUserData(sClientUID).getFileId();
		final String sSessionID = getUserData(sClientUID).getSessionId();
		final String sVersion = null; // latest version

		log.debug("onSync called for com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", sClientUID, getDocUID().toString());

		ErrorCode aErrorCode = ErrorCode.NO_ERROR;

		try {
			// retrieve meta data to check existence
			final ServerSession aSession = sessionService.getSession4Id(sSessionID);
			if (aSession == null)
				throw new RT2SessionInvalidException(rt2MessageLoggerService.getMsgsAsList(getDocUID()), RT2SessionInvalidException.SESSION_ID_ADD_INFO, sSessionID);

			// retrieve meta data to check existence
        	final FileHelperService fileHelperService = fileHelperServiceFactory.create(aSession);

        	fileHelperService.getFileMetaData(sFileID, sVersion);
		} catch (OXException e) {
			aErrorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR, false);
		}

		// send the latest connection status and possible error code
		final RT2Message aSyncResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_SYNC);
		MessageHelper.setDocumentStatus(aSyncResponse, aDocStatus);
		RT2MessageGetSet.setError(aSyncResponse, aErrorCode);
		messageSender.sendMessageTo(sClientUID, aSyncResponse);

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onSyncStable(final RT2Message aMsg) throws Exception {
		final RT2CliendUidType sClientUID = aMsg.getClientUID();
		final DocumentStatus aDocStatus = getDocumentStatusClone();

		log.debug("onSyncStable called for com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", sClientUID, getDocUID().toString());

		// This part is currently only used for ot which receives the
		// current client-osn for operations clean-up
		final JSONObject syncStable = aMsg.getBody();
		if (JSONHelper.isNotNullAndEmpty(syncStable)) {
			final Integer clientOSN = syncStable.optInt("osn");
			if (clientOSN != null) {
				synchronized (getStatusSyncObject()) {
					final EditableClientsStatus clientsStatus = (EditableClientsStatus)getClientsStatus();
					clientsStatus.setClientOSN(sClientUID, clientOSN);
				}
			}
		}

		// send the latest connection status and possible error code
		final RT2Message aSyncResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_SYNC_STABLE);
		MessageHelper.setDocumentStatus(aSyncResponse, aDocStatus);
		RT2MessageGetSet.setError(aSyncResponse, ErrorCode.NO_ERROR);
		messageSender.sendMessageTo(sClientUID, aSyncResponse);

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onEditRights(final RT2Message aMsg) throws Exception {
        final RT2CliendUidType sClientUID = aMsg.getClientUID();

	    if (isOTEnabled()) {
	        final RT2Message aReqEditRightsResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_EDITRIGHTS);
	        RT2MessageGetSet.setError(aReqEditRightsResponse, ErrorCode.GENERAL_SERVICE_DISABLED_ERROR);
	        messageSender.sendMessageTo(sClientUID, aReqEditRightsResponse);
	        return true;
	    }

        final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatus();
        final EditableClientsStatus aClientsStatus = (EditableClientsStatus) getClientsStatus();
        final String sAction = EditRightsHelper.getAction(aMsg);
        final UserData aUserData = this.getUserData(sClientUID);
        RT2CliendUidType sActionClientUID = null;
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        boolean bForceEdit = false;
        boolean bForceEditResult = false;

        if (this.isReadOnlyState()) {
            synchronized (getStatusSyncObject()) {
                aClientsStatus.clearRequestsEditRights();
            }
        } else {
            if (EditRightsHelper.ACTION_EDITRIGHTS_REQUESTS.equals(sAction)) {
                // handle a edit-right request of a client => update user state
                synchronized (getStatusSyncObject()) {
                    // in case nobody has the edit rights - give them to the first requesting
                    if (DocumentStatus.EMPTY_CLIENTID.equals(aDocStatus.getCurrentEditingUserId())) {
                        final String sDisplayName = impl_getUserDisplayName(aUserData.getSessionId());
                        aDocStatus.setCurrentEditingUser(sClientUID, sDisplayName);
                    } else {
                        aClientsStatus.setRequestsEditRights(sClientUID, true);
                    }
                }
            } else if (EditRightsHelper.ACTION_EDITRIGHTS_FORCE.equals(sAction)) {
                bForceEdit = true;
                synchronized (getStatusSyncObject()) {
                    if (aClientsStatus.requestsEditRights(sClientUID)) {
                        final String sDisplayName = impl_getUserDisplayName(aUserData.getSessionId());
                        aDocStatus.setCurrentEditingUser(sClientUID, sDisplayName);
                        aClientsStatus.clearRequestsEditRights();
                        bForceEditResult = true;
                    }
                }
            } else {
                boolean bEditorGracefullyChanged = false;
                String sEditorSessionId = null;

                sActionClientUID = new RT2CliendUidType(EditRightsHelper.getActionValue(aMsg));
                if (StringUtils.isNoneEmpty(sActionClientUID.getValue())) {
                    synchronized (getStatusSyncObject()) {
                        // only the editor is allowed to approve/decline edit rights requests
                        if (sClientUID.equals(aDocStatus.getCurrentEditingUserId())) {
                            // handle a edit-right request that the editor approves to lose edit rights
                            if (EditRightsHelper.ACTION_EDITRIGHTS_APPROVED.equals(sAction)) {
                                final UserData aActionUserData = this.getUserData(sActionClientUID);
                                if (null != aActionUserData) {
                                    final String sDisplayName = impl_getUserDisplayName(aActionUserData.getSessionId());
                                    aDocStatus.setCurrentEditingUser(sActionClientUID, sDisplayName);
                                    aClientsStatus.clearRequestsEditRights();
                                    bEditorGracefullyChanged = true;
                                    sEditorSessionId = aUserData.getSessionId();
                                }
                            } else if (EditRightsHelper.ACTION_EDITRIGHTS_DECLINED.equals(sAction)) {
                                // handle a edit-right request that the editor declines
                                if (sActionClientUID.getValue().equals(EditRightsHelper.ACTION_EDITRIGHTS_ALL))
                                    aClientsStatus.clearRequestsEditRights();
                                else
                                    aClientsStatus.setRequestsEditRights(sActionClientUID, false);
                            }
                        } else {
                            errorCode = ErrorCode.SWITCHEDITRIGHTS_NOT_POSSIBLE_NOT_EDITOR;
                        }
                    }

                    if (bEditorGracefullyChanged && StringUtils.isNotEmpty(sEditorSessionId)) {
                		if (!(aMsg.getType().equals(RT2MessageType.REQUEST_CLOSE_AND_LEAVE) &&
                				Cause.SESSION_INVALID.equals(Cause.fromString(aMsg.getHeader(RT2Protocol.HEADER_CAUSE))))) {
	                        // The editor just gave-up his edit rights - make sure we store his/her latest
	                        // changes before we broadcast the edit rights transfer.
	                        final ServerSession aSession = sessionService.getSession4Id(sEditorSessionId);
	                        errorCode = impl_saveDocument(sClientUID, aSession, true, false, true, false, true, false, BroadcastHangup.BROADCAST_HANGUP_EXCEPT_CLOSING_CLIENT);
	                        if (errorCode.isNoError()) {
	                            documentEventHelper.addSaveEvent(getDocumentType(), SaveType.FLUSH);
	                        }
                		}
                    }
                } else {
                    // unknown action
                    errorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
                }
            }
        }

        // send response to request
        final RT2Message aReqEditRightsResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_EDITRIGHTS);
        RT2MessageGetSet.setError(aReqEditRightsResponse, errorCode);
        if (bForceEdit)
            MessageHelper.setForceEditResult(aReqEditRightsResponse, bForceEditResult);
        messageSender.sendMessageTo(sClientUID, aReqEditRightsResponse);

        // broadcast document and client status if modified
        impl_broadcastDocumentStatusIfModified(null);
        impl_broadcastClientsStatusIfModified(null);

        return true;
	}

	// -------------------------------------------------------------------------
	@Override
	public boolean onUnavailable(final RT2Message aMsg) throws Exception {
		final EditableClientsStatus aClientsStatus = (EditableClientsStatus) getClientsStatus();

		// determine duration of inactivity in seconds due to restricted precision of
		// number in js
		int nSecondsOfInactivity = Math.toIntExact(Math.floorDiv(aMsg.getUnvailableTime().getValue(), 1000));

		synchronized (getStatusSyncObject()) {
			aClientsStatus.changeStateOfActiveClient(aMsg.getClientUID(), nSecondsOfInactivity);
		}

		impl_broadcastClientsStatusIfModified(aMsg.getClientUID());

		return true;
	}

	// -------------------------------------------------------------------------
	// custom messages
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	public boolean handleAdd_Image_ID_Request(final RT2Message aMsg) throws Exception {
		final JSONObject aBodyJSON = aMsg.getBody();
		final JSONObject aImageInfo = aBodyJSON.getJSONObject(ActionsConst.ACTION_ADD_IMAGE_ID_IMAGEINFO);
		final String sManagedResourceID = aImageInfo.getString(ActionsConst.ACTION_ADD_IMAGE_ID_IMAGEINFO_ADDED_FILEID);

		// create a managed resource to add it to our document based resource manager
		final Resource aResource = getResourceManager().createManagedResource(sManagedResourceID);
		if (aResource != null) {
			// Add resource to the document specific resource manager to make
			// sure that it's known, can be used by the filter and is kept alive
			getResourceManager().addResource(aResource);
			pushAddResourceForRestore(aResource);
		} else
			log.warn("RT2: Image via DistributedManagedFileManagement not found! Managed resource ID: {}", sManagedResourceID);

		final RT2Message aAddImageIdResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
		messageSender.sendMessageTo(aMsg.getClientUID(), aAddImageIdResponse);

		return true;
	}

	// -------------------------------------------------------------------------
	public boolean handleUpdateUserData(final RT2Message aMsg) throws Exception {
		final JSONObject aBody = aMsg.getBody();
		final EditableClientsStatus aClientsStatus = (EditableClientsStatus) getClientsStatus();

		synchronized (getStatusSyncObject()) {
			aClientsStatus.updateClientData(aMsg.getClientUID(), aBody);
		}

		final RT2Message aUpdateUserDataResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
		messageSender.sendMessageTo(aMsg.getClientUID(), aUpdateUserDataResponse);

		// broadcast client update message to all other clients
		impl_broadcastClientsStatusIfModified(aMsg.getClientUID());

		return true;
	}

	// -------------------------------------------------------------------------
	public boolean handleCopyDocument(final RT2Message aMsg) throws Exception {
		final JSONObject aBody = aMsg.getBody();
		final String sTargetFileName = aBody.optString("target_filename");
		final String sTargetFolderID = aBody.optString("target_folder_id");
		final Boolean bAsTemplate = aBody.optBoolean("asTemplate");
		final RT2Message aCopyResultResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);

		ErrorCode aErrorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;

		// check mandatory arguments
		if (StringUtils.isNoneEmpty(sTargetFileName)) {
			final CopyDocumentInfo aCopyDocInfo = impl_copyDocument(aMsg.getClientUID(), sTargetFolderID, sTargetFileName,
					bAsTemplate);
			aErrorCode = aCopyDocInfo.getErrorCode();
			if (aErrorCode.isNoError()) {
				// in case of no error we can provide the copied file data to the caller
				aCopyResultResponse.setBody(aCopyDocInfo.toJSON());
			}
		}

		// send response
		RT2MessageGetSet.setError(aCopyResultResponse, aErrorCode);
		messageSender.sendMessageTo(aMsg.getClientUID(), aCopyResultResponse);

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	protected boolean processOpenDocRequest(final RT2Message rReq, final Boolean bFastEmpty, final Boolean bNewDoc, final Boolean bFirstUser) throws Exception {
		final RT2CliendUidType sClientUID = rReq.getClientUID();
		final String sSessionID = RT2MessageGetSet.getSessionID(rReq);

		ErrorCode errorCode = loadDocument(sSessionID, rReq, sClientUID, bFastEmpty, bNewDoc, bFirstUser);

		log.trace("... final message : START");

		final RT2Message aFinalResponse = RT2MessageFactory.createResponseFromMessage(rReq, RT2MessageType.RESPONSE_OPEN_DOC);
		RT2MessageGetSet.setError(aFinalResponse, errorCode);

		final ServerSession aSession = sessionService.getSession4Id(sSessionID);
		final UserData aUserData = getUserData(sClientUID);
		final boolean bCanWriteFile = impl_canUserAccessFile(aSession, aUserData.getFileId());

		// OT: different handling of an editor/savior
        final RT2OldClientUuidType oldClientUID = rReq.getOldClientUID();
        if (errorCode.isNoError()) {
            if (isOTEnabled()) {
            	impl_checkClientToSetAsSavior(sSessionID, sClientUID, bCanWriteFile);
            } else {
                impl_handleEditRights(sSessionID, sClientUID, oldClientUID, bCanWriteFile);
            }
        }

        // ensure that active state of an old client is set to disabled
        if (StringUtils.isNotEmpty(oldClientUID.getValue())) {
            synchronized (getStatusSyncObject()) {
                this.getClientsStatus().disableClient(new RT2CliendUidType(oldClientUID.getValue()));
            }
        }

		// set the initial osn for the restore document function, too
		final EditableDocumentStatus aDocStatus = (EditableDocumentStatus) getDocumentStatusClone();
		int nInitialOSN = aDocStatus.getDocumentOSN();
		nInitialOSN = (nInitialOSN != -1) ? nInitialOSN : aDocStatus.getOperationStateNumber();

		synchronized (getStatusSyncObject()) {
			final EditableClientsStatus clientsStatus = (EditableClientsStatus)getClientsStatus();
			clientsStatus.setClientOSN(sClientUID, nInitialOSN);
		}

		updateDocumentInitState(nInitialOSN);

		// send complete document/clients status with last open message
		final JSONObject aJSONDocStatus = aDocStatus.toJSON();
		final EditableClientsStatus aClientsStatus = (EditableClientsStatus) getClientsStatusClone();

		impl_setFinalDocLoadState(aJSONDocStatus, aSession, sClientUID.getValue(), aUserData.getFolderId(), bCanWriteFile);
		MessageHelper.setDocumentStatus(aFinalResponse, aJSONDocStatus);
		MessageHelper.setClientsStatus(aFinalResponse, aClientsStatus, true);
		messageSender.sendMessageTo(sClientUID, aFinalResponse);

		// set load state for client/user to enable it for operation updates
		updateUserData(sClientUID, LoadState.LOADSTATE_COMPLETED);

		// send documents/clients status update for all other clients
		// New client opens the document and the edit rights could have changed!
		impl_broadcastDocumentStatusIfModified(sClientUID);
		impl_broadcastClientsStatusIfModified(sClientUID);

		log.trace("... final message : END");

		return true;
	}

	// -------------------------------------------------------------------------
	@Override
	protected ErrorCode makeAdditionalFileAccessChecks(final ServerSession aSession, final String sFolderID,
			final DocumentMetaData aDocumentMetaData, final ErrorCode aCurrentErrorCode) {
		// We have to check if we can possibly overwrite a present BAK file- depends on
		// the capabilities of the storage, user access permission, etc.
		if (impl_canCreateOrWriteBackupFile(aSession, aDocumentMetaData, sFolderID, isEncrypted()))
			return aCurrentErrorCode;
		return ErrorCode.BACKUPFILE_WONT_WRITE_WARNING;
	}

	// -------------------------------------------------------------------------
	@Override
	protected void updateSyncInfo(ServerSession session, String folderID, SyncInfo syncInfo) {
		super.updateSyncInfo(session, folderID, syncInfo);
		final DocRestoreID aDocRestoreID = getDocRestoreID();
		if (null != aDocRestoreID)
			syncInfo.setRestoreID(aDocRestoreID.toString());
	}

	// -------------------------------------------------------------------------
	@Override
	protected DocumentStatus createDocumentStatus() {
		return new EditableDocumentStatus();
	}

	// -------------------------------------------------------------------------
	@Override
	protected ClientsStatus createClientsStatus() {
		return new EditableClientsStatus();
	}

	// -------------------------------------------------------------------------
	/**
	 * Provides the operations which have been applied to the document
	 * since the last save operation.
	 *
	 * @return JSONArray a JSONArray which contains all operations that have
	 *  been applied to the document since the last save operation. The JSONArray
	 *  is a deep clone and can therefore safely be used.
	 */
	@Override
	protected JSONArray getAdditionalOperations() throws Exception {
		if (isOTEnabled()) {
			synchronized (getStatusSyncObject()) {
				// We need to ensure that we only provide operations added to the
				// pending operations list that are newer than the last saved
				// document state.
				final EditableDocumentStatus docStatus = (EditableDocumentStatus)getDocumentStatus();
				final int docOSN = docStatus.getDocumentOSN();

				int filterOSN = 0;
				if (docOSN > OXDocument.DEFAULT_DOCUMENT_ID) {
					filterOSN = docOSN;
				}
				log.debug("OT DEBUGGING: " + filterOSN);
				return m_aPendingOperations.getPendingOperationsFilteredCloned(filterOSN);
			}
		} else {
			// We have to return our pending operations to provide
			// a consistent state to the joining client.
			return m_aPendingOperations.getPendingOperationsCloned();
		}
	}

	// -------------------------------------------------------------------------
	protected String impl_getUserDisplayName(final String sSessionId) throws Exception {
		final ServerSession aSession = sessionService.getSession4Id(sSessionId);
		if (aSession == null) {
			throw new RT2TypedException(ErrorCode.GENERAL_SESSION_INVALID_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
		}
		final User aUserInfo = aSession.getUser();
		return UserHelper.getFullName(aUserInfo);
	}

	// -------------------------------------------------------------------------
	protected void impl_setFinalDocLoadState(final JSONObject aDocState, final ServerSession aSession,
			final String sClientID, final String sFolderID, boolean bCanWriteFile) throws Exception {

		boolean bRenameNeedsReload = false;
		final StorageHelperService storageHelper = storageHelperServiceFactory.create(aSession, sFolderID);

		if (!storageHelper.supportsPersistentIDs())
			bRenameNeedsReload = true;

		aDocState.put(MessageProperties.PROP_WRITE_PROTECTED, !bCanWriteFile);
		aDocState.put(MessageProperties.PROP_CLIENT_IDENTIFIER, sClientID);
		aDocState.put(MessageProperties.PROP_RENAME_NEEDS_RELOAD, bRenameNeedsReload);
	}

	// -------------------------------------------------------------------------
    protected void impl_handleEditRights(final String sSessionID, final RT2CliendUidType sNewClientUID, final RT2OldClientUuidType sOldClientUID, boolean bCanWriteFile) throws Exception {
        final ServerSession aSession = sessionService.getSession4Id(sSessionID);

		if (bCanWriteFile && !isReadOnlyState()) {
			if (aSession == null) {
				throw new RT2TypedException(ErrorCode.GENERAL_SESSION_INVALID_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
			}
			final User aUserInfo = aSession.getUser();
			final String sDisplayName = UserHelper.getFullName(aUserInfo);

            synchronized (getStatusSyncObject()) {
                final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatus();
                final RT2CliendUidType sEditingUserUID = aDocState.getCurrentEditingUserId();

                // Edit rights can bet set if
                // 1. There is currently no editor at all
                // 2. The new editor is the same as the old editor (in case of a browser reload)
                if (StringUtils.isEmpty(sEditingUserUID.getValue()) ||
                    (StringUtils.isNotEmpty(sOldClientUID.getValue()) && (sOldClientUID.getValue().equals(sEditingUserUID.getValue())))) {
                    aDocState.setCurrentEditingUser(sNewClientUID, sDisplayName);
                }
            }
        }
    }

    // -------------------------------------------------------------------------
	@Override
	protected ErrorCode prepareRenameDocumentWithReload(RT2CliendUidType sClientUID, final ServerSession aSession)
			throws Exception {
		final ErrorCode aErrorCode = impl_saveDocument(sClientUID, aSession, true, true, false, false, false, false, BroadcastHangup.BROADCAST_HANGUP_EXCEPT_CLOSING_CLIENT);

		if (aErrorCode.isNoError())
			setReadOnlyState();

		documentEventHelper.addSaveEvent(getDocumentType(), SaveType.CLOSE);
		return aErrorCode;
	}

    // -------------------------------------------------------------------------
	public ErrorCode safteySaveDocumentDueToMove(Session session, RT2FolderIdType toFolderId, RT2FileIdType toFileId, boolean bWaitForLock) throws Exception {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        if (session == null)
            return ErrorCode.GENERAL_SESSION_INVALID_ERROR;

        if (!m_aPendingOperations.hasPendingOperations(isOTEnabled(), (EditableDocumentStatus) getDocumentStatusClone())) {
            // No need to save the document if we have no pending operations
            return ErrorCode.NO_ERROR;
        }

        // Make sure only one thread can call doSaveDocument at a time. Other
        // threads will wait for a certain time for the lock or (in case of a
        // background save) get a SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR. It's
        // up to the calling function to decide if this is a "real" error or
        // can be accepted (true in case of background save).
        long nWaitingTime = (bWaitForLock) ? WAIT_FOR_LOADSAVE_LOCK : 0;
        if (lockForLoadOrSaveDocumentWithTimeout(nWaitingTime)) {
            try {
                errorCode = impl_doSafteySaveDocumentDueToMove(session, toFolderId, toFileId);
            } finally {
                // release lock for next save
                unlockForLoadOrSaveDocument();
            }
        } else {
            log.info("RT2: Waiting for save lock timed out - previous save is still in progress - giving up, save not possible!");
            errorCode = ErrorCode.SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR;
        }

        return errorCode;
	}

    // -------------------------------------------------------------------------
	public void handleMovedFile(RT2FolderIdType toFolderId, RT2FileIdType toFileId, ErrorCode saveErrorCode) {
	    if (saveErrorCode.isNoError()) {
	        // Check every client how the movement of the document file must be
	        // treated (reload or access lost).
	        List<UserData> clients = getUserDatasCloned();
	        if ((clients != null) && (clients.size() > 0)) {
	            clients.stream().forEach(c -> {
	                RT2CliendUidType clientUid = new RT2CliendUidType(c.getClientUid());
	                String clientSessionId = c.getSessionId();
	                String oldFolderId = c.getFolderId();
	                String oldFileId = c.getFileId();

	                try {
	                    Session clientSession = sessionService.getSession4Id(clientSessionId);
	                    if (clientSession != null) {
                            String folderId = toFolderId.getValue();
                            String fileId = toFileId.getValue();

                            final FolderHelperService folderHelperService = folderHelperServiceFactory.create(clientSession);
                            // first try - check folder sharing
                            boolean canAccess = impl_canUserAccessFile(clientSession, fileId);
                            if (!canAccess && folderHelperService.isSystemUserInfostoreFolder(oldFolderId)) {
                                // second try - check using virtual sharing folder 10
                                canAccess = impl_canUserAccessFile(clientSession, oldFileId);
                                if (canAccess) {
                                    folderId = oldFolderId;
                                    fileId = oldFileId;
                                }
                            }

	                        if (canAccess) {
	                            // client has still access -> must get moved reload broadcast message
	                            RT2Message broadcastFileMovedReload = RT2MessageFactory.newBroadcastMessage(clientUid, getDocUID(), RT2MessageType.BROADCAST_FILE_MOVED_RELOAD);
	                            MessageHelper.setFolderAndFileId(broadcastFileMovedReload, new RT2FolderIdType(folderId), new RT2FileIdType(fileId));
	                            messageSender.sendMessageTo(clientUid, broadcastFileMovedReload);
	                        } else {
	                            // client has no access anymore -> must get hang-up broadcast message
	                            RT2Message broadcastHangup = RT2MessageFactory.newBroadcastMessage(clientUid, getDocUID(), RT2MessageType.BROADCAST_HANGUP);
	                            RT2MessageGetSet.setError(broadcastHangup, ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR);
	                            messageSender.sendMessageWOSeqNrTo(clientUid, broadcastHangup);
	                        }
	                    }
	                } catch (Exception e) {
	                    log.info("Exception caught trying to handle a detected file move for com.openexchange.rt2.document.uid " + getDocUID() + " and com.openexchange.rt2.client.uid " +  clientUid, e);
	                }
	            });
	        }
	    } else {
	        // in case of an error during save -> send hang-up broadcast to all clients
	        try {
	            final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatusClone();
                broadcastHangupMessageToAll(getCurrentEditingClientUID(), saveErrorCode, aDocState);
            } catch (Exception e) {
                log.error("Exception caught trying to broadcast hang-up to all clients for com.openexchange.rt2.document.uid " + getDocUID(), e);
            }
	    }
	}

    // -------------------------------------------------------------------------
    public void handleDeletedDocFile() throws Exception {
        // in case of an error send a hang-up to editor and all viewer clients
        EditableDocumentStatus aDocState = null;
        RT2CliendUidType sEditingUserUID = null;
        synchronized (getStatusSyncObject()) {
            aDocState = (EditableDocumentStatus) getDocumentStatus();
            sEditingUserUID = aDocState.getCurrentEditingUserId();
        }
        
        impl_broadcastHangupToEditorAndViewersIfNeeded(sEditingUserUID, null, aDocState, ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR, false,
            true, false, BroadcastHangup.BROADCAST_HANGUP_ALL);
    }

    // -------------------------------------------------------------------------
	private ErrorCode impl_doSafteySaveDocumentDueToMove(Session session, RT2FolderIdType toFolderId, RT2FileIdType toFileId) throws Exception {
        final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatus();
        final boolean bRevisionless = m_aRevisionless.get();
        final IResourceManager aResMgr = getResourceManager();

        JSONArray aPendingOperations = null;
        int nSize = 0;
        int nCurrentOSN = -1;
        int nDocumentOSN = -1;

        synchronized (getStatusSyncObject()) {
            aPendingOperations = m_aPendingOperations.getPendingOperationsCloned();
            nSize = aPendingOperations.length();
            nCurrentOSN = aDocState.getOperationStateNumber();
            nDocumentOSN = aDocState.getDocumentOSN();
        }

        if (isOTEnabled()) {
            final int nMinClientOSN = impl_determineMinClientOSN();
            final int nCutOSN = Math.min(nDocumentOSN, nMinClientOSN);
            nSize = (nCutOSN <= 0) ? 0 : OperationHelper.getIndexOfOperationWithOSNLargerThan(aPendingOperations, nCutOSN);

            log.debug("OT DEBUGGING: saveDocumentDueToMoveAndTriggerReload clean-up of pending operations list with {} entries", aPendingOperations.length());
            log.trace("OT DEBUGGING: documentOSN {}, currentOSN {}, minClientOSN {}, cutOSN {} -> remove first {} operations", nDocumentOSN, nCurrentOSN, nMinClientOSN, nCutOSN, nSize);
        }

        final SaveDebugProperties dbgProps = impl_setupSaveDebugProperties(session);

        // Create the new document stream containing the pending operations
        final OXDocument oxDocument = oxDocumentFactory.createDocumentAccess(session, toFolderId.getValue(), toFileId.getValue(), false, aResMgr, false, dbgProps);

        ErrorCode errCode = oxDocument.getLastError();
        if (errCode.isNoError()) {
            // We have to make a consistency check before we start to write
            // the document. It could be possible, that a user has overwritten
            // his/her document via synchronization, OX Drive upload or other
            // tools. Normally this would be detected by the filter which would
            // not able to apply the pending operations, but can sometimes work for
            // Spreadsheet documents!
            final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus) getDocumentStatus();
            if (!checkConsistencyOfLoadedDocument(aDocStatus.getVersion(), aDocStatus.getDocumentOSN(),
                    aDocStatus.getOperationStateNumber(), oxDocument)) {
                errCode = ErrorCode.SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR;
            } else {
                if (isOTEnabled()) {
                    // we need to cut the pending operations array to use only operations that
                    // were applied after this document content has been saved
                    final int docOSN = oxDocument.getUniqueDocumentId();
                    final int filterOSN = (docOSN <= 0) ? 0 : docOSN;
                    aPendingOperations = OperationHelper.filterOperationsWithOSNSmallerEqualThan(aPendingOperations, filterOSN);
                }

                // ATTENTION: always update the current document osn AFTER we filtered the pending operations
                oxDocument.setUniqueDocumentId(nCurrentOSN);

                final IExporter exporter = imExporterAccessFactory.getExporterAccess(oxDocument.getDocumentFormat());
                final boolean bFinalSave = true;
                final SaveResult result = oxDocument.save(exporter, aResMgr, aPendingOperations, bRevisionless,
                        bFinalSave, isEncrypted(), false);

                m_aRevisionless.set(true);

                synchronized (m_aLock) {
                    m_aLastSaveErrorCode = result.errorCode;
                }

                errCode = result.errorCode;
                if (errCode.isNoError()) {
                    synchronized (getStatusSyncObject()) {
                        aDocState.setDocumentOSN(nCurrentOSN);
                        aDocState.setVersion(result.version);
                    }

                    m_aLastSaveTimeStamp.set(new Date().getTime());
                    m_aPendingOperations.cutOperations(nSize);
                }
            }
        } else {
            synchronized (m_aLock) {
                m_aLastSaveErrorCode = errCode;
            }
        }

        documentEventHelper.addSaveEvent(getDocumentType(), SaveType.FLUSH);

        return errCode;
	}

	// -------------------------------------------------------------------------
	/**
	 * Updates the initial document state which includes the initial operation state
	 * number.
	 *
	 * @param initialOSN
	 *            the initial operation state number of this document
	 */
	protected void updateDocumentInitState(int initialOSN) {
		if (isRestoreEnabled()) {
			try {
				// set the initial osn for the backup controller, too
				m_aBackupController.updateDocumentInitState(getDocRestoreID(), initialOSN);
			} catch (Exception e) {
				log.warn("RT2: Exception caught while trying to update the document init state for the backup manager", e);
			}
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Remove the document state from the backup manager.
	 */
	protected void removeDocumentState() {
		if (isRestoreEnabled()) {
			try {
				m_aBackupController.deregisterDocument(getDocRestoreID());
			} catch (Exception e) {
				log.warn("RT2: Exception caught while trying to update the document init state for the backup manager", e);
			}
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Provides the current action chunk to the backup controller for the restore
	 * document function.
	 *
	 * @param operationsArray
	 *            the current operations sent by the editor client
	 * @param osn
	 *            the current osn (after the actions have been applied)
	 */
	protected void pushOperationsForRestore(final JSONArray operationsArray, int osn) {
		if (isRestoreEnabled()) {
			try {
				// add the operations to the DocumentDataManager to have a copy
				// of the operations for a possible later save-as
				m_aBackupController.addOperations(getDocRestoreID(), operationsArray, osn);
			} catch (Exception e) {
				log.warn("RT2: Exception caught while pushing operations to the document backup manager", e);
			}
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Provides the a new resource to the backup controller for the restore document
	 * function.
	 *
	 * @param managedResource
	 *            a managed resource instance referencing a used resource
	 */
	protected void pushAddResourceForRestore(final Resource managedResource) {
		if (isRestoreEnabled()) {
			try {
				m_aBackupController.addResource(getDocRestoreID(), managedResource);
			} catch (Exception e) {
				log.warn("RT2: Exception thrown while pushing a new resource to the document backup manager", e);
			}
		}
	}

    // -------------------------------------------------------------------------
	/**
	 * Saves the current document including the latest changes in the message chunk
	 * list to a specified folder. The current state of the connection including the
	 * message chunk list, editor etc. are not changed.
	 *
	 * @param session
	 *            The session of the user that requested to copy the current
	 *            document to a specified folder.
	 * @param id
	 *            The id of the client requested to copy the current document to a
	 *            specific folder.
	 * @param jsonRequest
	 *            The request containing the folder/file id, the target folder id,
	 *            the new file name. The method uses the folder of the document if
	 *            the target folder id is missing, if this is not possible, the user
	 *            folder is used. If the new file name is missing the original file
	 *            name is used.
	 * @return A CopyDocumentInfo object containing information about the
	 *         success/failure of the copy document processing.
	 */
	private CopyDocumentInfo impl_copyDocument(RT2CliendUidType sClientUID, String sWriteFolderID, String sFileName,
			boolean bAsTemplate) throws Exception {
		final CopyDocumentInfo aCopyInfo = new CopyDocumentInfo(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR);
		final UserData aUserData = this.getUserData(sClientUID);

		if (null != aUserData) {
			try {
				final ServerSession aSession = sessionService.getSession4Id(aUserData.getSessionId());
				String sNewFileName = sFileName;
				String sTargetFolderID = sWriteFolderID;

				if (null != aSession) {
					boolean bCreateDocumentCopy = true;

					if (StringUtils.isEmpty(sTargetFolderID)) {
						// There is no target folder
						boolean bCanUseFolder = false;
						if (!bAsTemplate) {
							// First we try to use the source folder and check the
							// permissions. If we cannot write, we take the users
							// default folder as fallback.
							sTargetFolderID = aUserData.getFolderId();

	                    	final FileHelperService fileHelperService = fileHelperServiceFactory.create(aSession);
							if (fileHelperService.canCreateFileAndWrite(sTargetFolderID))
								bCanUseFolder = true;
						}

						// For template this is the default case!
						if (!bCanUseFolder) {
							// Fall-back use the users documents folder to create the document
							final FolderHelperService folderHelperService = folderHelperServiceFactory.create(aSession);
							sTargetFolderID = folderHelperService.getUserDocumentsFolderId(aSession);
							if (null == sTargetFolderID) {
								aCopyInfo.setErrorCode(ErrorCode.COPYDOCUMENT_USERFOLDER_UNKOWN_ERROR);
								bCreateDocumentCopy = false;
							}
						}
					}

					if (StringUtils.isEmpty(sNewFileName)) {
						// We don't have a new file name for the copy of the
						// document. Therefore we retrieve the original name.
						try {
							sNewFileName = docFileServiceFactory.createInstance().getFilenameFromFile(aSession, aUserData.getFileId());
						} catch (OXException e) {
						    MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.COPYDOCUMENT_FILENAME_UNKNOWN_ERROR.getCodeAsStringConstant());
							log.error("impl_copyDocument failed - cannot determine file name of the document!", e);
							MDC.remove(MDCEntries.ERROR);
							aCopyInfo.setErrorCode(ErrorCode.COPYDOCUMENT_FILENAME_UNKNOWN_ERROR);
							bCreateDocumentCopy = false;
						}
					}

					if (bCreateDocumentCopy)
						return impl_copyDocumentContent(aSession, aUserData, sTargetFolderID, sNewFileName, bAsTemplate);
				} else {
					aCopyInfo.setErrorCode(ErrorCode.COPYDOCUMENT_FAILED_ERROR);
				}
			} catch (final Exception e) {
			    MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.COPYDOCUMENT_FAILED_ERROR.getCodeAsStringConstant());
				log.error("impl_copyDocument failed - Exception caught!", e);
				MDC.remove(MDCEntries.ERROR);
				aCopyInfo.setErrorCode(ErrorCode.COPYDOCUMENT_FAILED_ERROR);
			}
		}

		return aCopyInfo;
	}

	// -------------------------------------------------------------------------
	/**
	 * Determines, if the current client is able to create/write the backup file.
	 *
	 * @param session
	 *            The session of the current client
	 * @param folderId
	 *            The ID of the folder containing the associated document.
	 * @return TRUE, if the backup file can be written/created by the current
	 *         client, otherwise FALSE.
	 */
	private boolean impl_canCreateOrWriteBackupFile(final ServerSession aSession, final DocumentMetaData aDocMetaData,
			final String sFolderID, boolean encrypted) {

		return backupFileService.canCreateOrWriteBackupFile(aSession, sFolderID, aDocMetaData, encrypted);
	}

	// -------------------------------------------------------------------------
	private boolean impl_checkValidOTOperations(final JSONArray lCheckOperations) {
		boolean result = true;

		try {
			for (int i = 0; i < lCheckOperations.length(); i++) {
				final JSONObject aOperation = lCheckOperations.getJSONObject(i);
				final String name = aOperation.getString(OCKey.NAME.value());
				if (OperationHelper.OP_NAME_INVALID.equalsIgnoreCase(name)) {
					throw new RT2TypedException(ErrorCode.HANGUP_INVALID_OPERATIONS_SENT_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
				}
			}
		} catch (final JSONException e) {
			log.warn("RT2: JSONException caught - bad operation format detected", e);
			result = false;
		} catch (final Exception e) {
			log.warn("RT2: Bad operation for OT detected or runtime error!", e);
			result = false;
		}

		return result;
	}

	// -------------------------------------------------------------------------
	private JSONArray impl_checkSyncOperations(final JSONArray lCheckOperations) {
		JSONArray lOpsToBeApplied = null;

		// Check operations in case of sync, it could be possible that we have
		// already seen the operations.
		try {
			boolean bStartFound = false;
			final JSONArray lSafeOperations = new JSONArray();
			int nServerOSN = ((EditableDocumentStatus) getDocumentStatusClone()).getOperationStateNumber();
			for (int i = 0; i < lCheckOperations.length(); i++) {
				final JSONObject aOperation = lCheckOperations.getJSONObject(i);
				int nOSN = aOperation.getInt(OperationHelper.KEY_OSN);
				int nOPL = aOperation.getInt(OperationHelper.KEY_OPL);

				if (!bStartFound) {
					if (nOSN < nServerOSN) // operation already seen
						continue;
					else if (nOSN == nServerOSN)
						bStartFound = true;
					else
						throw new RT2TypedException(ErrorCode.HANGUP_INVALID_OPERATIONS_SENT_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
				}

				if (nOSN != nServerOSN)
					throw new RT2TypedException(ErrorCode.HANGUP_INVALID_OSN_DETECTED_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
				nServerOSN += nOPL;
				lSafeOperations.put(aOperation);
			}

			// operations sent with sync were checked and filtered - can be applied now
			lOpsToBeApplied = lSafeOperations;
		} catch (final JSONException e) {
			log.debug("RT2: invalid/bad operation detected - sync with operations aborted!", e);
		} catch (final Exception e) {
			log.debug("RT2: Operation with bad osn/opl detected - sync with operations aborted!", e);
		}

		return lOpsToBeApplied;
	}

	// -------------------------------------------------------------------------
	/**
	 * Determines if a user has write access to a file checking the locking state
	 * and the folder & file permissions.
	 *
	 * @param session
	 *            the session of the client which should be checked for access
	 *            rights
	 * @param folderId
	 *            the folder id which should be checked for access rights
	 * @param fileId
	 *            the file id which should be checked for access rights
	 * @param userId
	 *            the user id of the user that should determine his/her access
	 *            rights
	 * @return TRUE, if the user has access, otherwise FALSE.
	 */
	private boolean impl_canUserAccessFile(final Session session, final String fileId) {
	    if (session != null) {
	        final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
	        return fileHelperService.canWriteToFile(fileId);
	    }
	    return false;
	}

	// -------------------------------------------------------------------------
	/**
	 * Blocks until the timeout has been reached or the save in progress lock is
	 * available.
	 *
	 * @param timeout
	 *            time out value in milliseconds, zero means no timeout.
	 * @return TRUE if the save in progress lock has been successfully locked. FALSE
	 *         if the timeout has been reached.
	 */
	@Override
    protected boolean lockForLoadOrSaveDocumentWithTimeout(long timeout) {
		final long nSleep = Math.max(1, (timeout / 10));
		final StopWatch aStopWatch = new StopWatch();
		boolean bResult = false;
		boolean bResultSet = false;

		aStopWatch.start();
		while (!bResultSet) {
			boolean bSuccess = m_aSaveInProgress.compareAndSet(false, true);

			if (bSuccess) {
				bResult = true;
				bResultSet = true;
				continue;
			}

			if (aStopWatch.getTime() < timeout) {
				try {
					Thread.sleep(nSleep);
				} catch (InterruptedException e) {
					bResultSet = true;
					Thread.currentThread().interrupt();
				}
			} else {
				// time is up - giving up
				bResultSet = true;
			}
		}

		return bResult;
	}

	// -------------------------------------------------------------------------
	@Override
    protected void unlockForLoadOrSaveDocument() {
	   	m_aSaveInProgress.set(false);
	}

    // -------------------------------------------------------------------------
    private ErrorCode impl_saveDocument(final RT2CliendUidType sClientUID, final Session aSession, boolean bWaitForLock, boolean bCloseDoc, boolean bEditorChanges, boolean bNoHangup, boolean bNoRestore, boolean bDirty, BroadcastHangup howToBroadcastHangup) throws Exception {
        ErrorCode aErrorCode = ErrorCode.NO_ERROR;

        if (aSession == null)
            return ErrorCode.GENERAL_SESSION_INVALID_ERROR;

        if (!m_aPendingOperations.hasPendingOperations(isOTEnabled(), (EditableDocumentStatus) getDocumentStatusClone())) {
            // No need to save the document if we have no pending operations, but we have to
            // check if we need to reset the advisory lock in case this is an emergency save
            // due to a shutdown, where at least the lock must be cleared (whether the doc has been
            // modified or not).
            if (isAdvisoryLockEnabled() && m_advisoryLockSet.get() && isNodeShutdown()) {
                final UserData aUserData = getUserData(sClientUID);
                if (aUserData != null) {
                    try {
                        advisoryLockInfoService.clearAdvisoryLock(aSession, aUserData.getFolderId(), aUserData.getFileId());
                    } catch (Exception e) {
                        log.warn("Clearing advisory lock failed for com.openexchange.rt2.document.uid " + getDocUID() + " during shutdown due to an exception", e);
                    }
                }
            }
            return aErrorCode;
        }

        // Make sure only one thread can call doSaveDocument at a time. Other
        // threads will wait for a certain time for the lock or (in case of a
        // background save) get a SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR. It's
        // up to the calling function to decide if this is a "real" error or
        // can be accepted (true in case of background save).
        long nWaitingTime = (bWaitForLock) ? WAIT_FOR_LOADSAVE_LOCK : 0;
        if (lockForLoadOrSaveDocumentWithTimeout(nWaitingTime)) {
            try {
                aErrorCode = impl_doSaveDocument(sClientUID, aSession, bCloseDoc, bEditorChanges, bNoHangup, bNoRestore, false, howToBroadcastHangup);
            } finally {
                // release lock for next save
                unlockForLoadOrSaveDocument();
            }
        } else {
            log.info("RT2: Waiting for save lock timed out - previous save is still in progress - giving up, save not possible!");
            aErrorCode = ErrorCode.SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR;
        }

        return aErrorCode;
    }

	// -------------------------------------------------------------------------
	/**
	 * Saves the document content using the provided user data dependent on the
	 * close and editor state. <b>ATTENTION:</b> This function doesn't check the
	 * session and if the document has been modified or not. It also doesn't use the
	 * save lock to ensure that only one thread can save the document. This must be
	 * done by the caller.
	 *
	 * @param sClientUID
	 *            the client uid to be used to store the document changes
	 * @param aSession
	 *            the session of the user that want to store the document changes
	 * @param bCloseDoc
	 *            must be true if the document is saved due to a close document
	 *            request
	 * @param bEditorChanges
	 *            must be true if the editor will change
	 * @return the resulting error code of saving the document
	 * @throws Exception
	 */
	private ErrorCode impl_doSaveDocument(final RT2CliendUidType sClientUID, final Session aSession, boolean bCloseDoc,
			boolean bEditorChanges, boolean bNoHangup, boolean bNoRestore, boolean bDirty, BroadcastHangup howToBroadcastHangup) throws Exception {

		final UserData aUserData = getUserData(sClientUID);
		final IResourceManager aResMgr = getResourceManager();
		final boolean bRevisionless = m_aRevisionless.get();
		final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatus();
		final int nClientCount = getUserCount();

		JSONArray aPendingOperations = null;
		int nSize = 0;
		int nCurrentOSN = -1;
		int nDocumentOSN = -1;

		synchronized (getStatusSyncObject()) {
			aPendingOperations = m_aPendingOperations.getPendingOperationsCloned();
			nSize = aPendingOperations.length();
			nCurrentOSN = aDocState.getOperationStateNumber();
			nDocumentOSN = aDocState.getDocumentOSN();
		}

		if (isOTEnabled()) {
			final int nMinClientOSN = impl_determineMinClientOSN();
			final int nCutOSN = Math.min(nDocumentOSN, nMinClientOSN);
			nSize = (nCutOSN <= 0) ? 0 : OperationHelper.getIndexOfOperationWithOSNLargerThan(aPendingOperations, nCutOSN);

			log.debug("OT DEBUGGING: impl_doSaveDocument clean-up of pending operations list with {} entries", aPendingOperations.length());
			log.trace("OT DEBUGGING: documentOSN {}, currentOSN {}, minClientOSN {}, cutOSN {} -> remove first {} operations", nDocumentOSN, nCurrentOSN, nMinClientOSN, nCutOSN, nSize);
		}

		final SaveDebugProperties dbgProps = impl_setupSaveDebugProperties(aSession);

		// Create the new document stream containing the pending operations
        final boolean bIsEditor = isOTEnabled() ? false : aDocState.isEditor(sClientUID.getValue());
        final boolean bEditorLeaves = isOTEnabled() ? false : (bIsEditor && bEditorChanges && bCloseDoc);
		final OXDocument oxDocument = oxDocumentFactory.createDocumentAccess(aSession, aUserData, false, aResMgr, false, dbgProps);

		ErrorCode errCode = oxDocument.getLastError();
		if (errCode.isNoError()) {
			// We have to make a consistency check before we start to write
			// the document. It could be possible, that a user has overwritten
			// his/her document via synchronization, OX Drive upload or other
			// tools. Normally this would be detected by the filter which would
			// not able to apply the pending operations, but can sometimes work for
			// Spreadsheet documents!
			final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus) getDocumentStatus();
			if (!checkConsistencyOfLoadedDocument(aDocStatus.getVersion(), aDocStatus.getDocumentOSN(),
					aDocStatus.getOperationStateNumber(), oxDocument)) {
				errCode = ErrorCode.SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR;

				// We try to use restore the document in case the editor leaves instead of
				// disturbing the user - may be we are lucky and can restore it.
				if (bEditorLeaves && isRestoreEnabled() && !bNoRestore)
					return impl_tryToRestoreDocumentForEditorAndBroadcastHangupToViewers(sClientUID, aSession,
							aUserData, oxDocument, aPendingOperations, aDocState, errCode);
			} else {
				if (isOTEnabled()) {
					// we need to cut the pending operations array to use only operations that
					// were applied after this document content has been saved
					final int docOSN = oxDocument.getUniqueDocumentId();
					final int filterOSN = (docOSN <= 0) ? 0 : docOSN;
					aPendingOperations = OperationHelper.filterOperationsWithOSNSmallerEqualThan(aPendingOperations, filterOSN);
				}

				// ATTENTION: always update the current document osn AFTER we filtered the pending operations
				oxDocument.setUniqueDocumentId(nCurrentOSN);

				final IExporter exporter = imExporterAccessFactory.getExporterAccess(oxDocument.getDocumentFormat());
				final boolean bFinalSave = bCloseDoc && (nClientCount == 1);
				final SaveResult result = oxDocument.save(exporter, aResMgr, aPendingOperations, bRevisionless,
						bFinalSave, isEncrypted(), bDirty);

				// Ensure that only a DocProcessor that set/reset the advisory lock clears it again.
				if (isAdvisoryLockEnabled() && m_advisoryLockSet.get() && isNodeShutdown()) {
					try {
					    advisoryLockInfoService.clearAdvisoryLock(aSession, oxDocument.getFolderId(), oxDocument.getFileId());
					} catch (Exception e) {
						log.warn("Clearing advisory lock failed for com.openexchange.rt2.document.uid " + getDocUID() + " during shutdown due to an exception", e);
					}
				}

				// Make sure to set the revisionless state correctly - if the editor changes
				// revisionless should be set to false. This state will be provided to the
				// OXDocument
				// save method which uses it to create/or not create a new version (dependent on
				// this state and other storage characteristics
				m_aRevisionless.set(!bEditorChanges);

				synchronized (m_aLock) {
					m_aLastSaveErrorCode = result.errorCode;
				}

				errCode = result.errorCode;
				if (errCode.isNoError()) {
					synchronized (getStatusSyncObject()) {
						aDocState.setDocumentOSN(nCurrentOSN);
						aDocState.setVersion(result.version);
					}

					m_aLastSaveTimeStamp.set(new Date().getTime());
					m_aPendingOperations.cutOperations(nSize);
				}
			}
        } else {
            synchronized (m_aLock) {
                m_aLastSaveErrorCode = errCode;
            }

            if (bEditorLeaves && isRestoreEnabled() && !bNoRestore) {
                return impl_tryToRestoreDocumentForEditorAndBroadcastHangupToViewers(sClientUID, aSession, aUserData,
                        oxDocument, aPendingOperations, aDocState, errCode);
            }
        }

		// in case of an error send a hang-up to editor and all viewer clients
		impl_broadcastHangupToEditorAndViewersIfNeeded(sClientUID, aPendingOperations, aDocState, errCode, bIsEditor,
		    bEditorLeaves, bNoHangup, howToBroadcastHangup);

		return errCode;
	}

	// -------------------------------------------------------------------------
	private int impl_determineMinClientOSN() {
		final EditableClientsStatus clientsStatus = (EditableClientsStatus)getClientsStatusClone();

		OptionalInt result = clientsStatus.getClientsMap()
					              .keySet()
					              .stream()
							      .mapToInt(k -> { return clientsStatus.getClientOSN(k); })
				                  .min();
		return result.isPresent() ? result.getAsInt() : EditableClientsStatus.MIN_CLIENT_OSN;
	}

	// -------------------------------------------------------------------------
	private SaveDebugProperties impl_setupSaveDebugProperties(final Session aSession) throws Exception {
		final SaveDebugProperties dbgProps = new SaveDebugProperties();
		setupSaveDebugProperties(dbgProps, aSession);
		return dbgProps;
	}

	// -------------------------------------------------------------------------
	private ErrorCode impl_tryToRestoreDocumentForEditorAndBroadcastHangupToViewers(final RT2CliendUidType sClientUID,
			final Session aSession, final UserData aUserData, final OXDocument oxDocument,
			final JSONArray aPendingOperations, final EditableDocumentStatus aDocState, ErrorCode errCode)
			throws Exception {
		final File aMetaData = oxDocument.getMetaData();
		final String sInitialFileName = (null == aMetaData) ? null : aMetaData.getFileName();

		final ServerSession serverSession = (aSession instanceof ServerSession) ? (ServerSession)aSession : null;
		if (serverSession != null) {
			tryToRestoreDocument(serverSession, aUserData, sInitialFileName, aPendingOperations);
		}
		broadcastHangupMessageExceptClientUID(sClientUID, errCode, aDocState);
		return errCode;
	}

	// -------------------------------------------------------------------------
	private void impl_broadcastHangupToEditorAndViewersIfNeeded(final RT2CliendUidType sClientUID,
			final JSONArray aPendingOperations, final EditableDocumentStatus aDocState, ErrorCode errCode,
			boolean bIsEditor, boolean bEditorLeaves, boolean bNoHangup, BroadcastHangup howToBroadcastHangup) throws Exception {

		if (errCode.isError() && !bNoHangup) {
			if (bIsEditor && !bEditorLeaves) {
				sendHangupMessage(sClientUID, errCode, aPendingOperations, aDocState);
			} else if (howToBroadcastHangup == BroadcastHangup.BROADCAST_HANGUP_ALL) {
				broadcastHangupMessageToAll(sClientUID, errCode, aDocState);
			} else {
				broadcastHangupMessageExceptClientUID(sClientUID, errCode, aDocState);
			}
		}
	}

	// -------------------------------------------------------------------------
	private void hangupClientDueToNonResolvableOTConflicts(RT2CliendUidType clientUID, ErrorCode errorCode) throws Exception {
		log.warn("RT2: com.openexchange.rt2.client.uid {} must reload document due to a operation transformation that cannot be resolved.", clientUID.getValue());

		synchronized (getStatusSyncObject()) {
			final EditableClientsStatus clientsStatus = (EditableClientsStatus) this.getClientsStatus();
			clientsStatus.setHangupState(clientUID, true);
		}

		broadcastOTReloadToClient(clientUID, errorCode);

		throw new RT2TypedException(errorCode, null);
	}

	// -------------------------------------------------------------------------
	private CopyDocumentInfo impl_copyDocumentContent(final Session aSession, final UserData aUserData,
			String sTargetFolderID, String sFileName, boolean bAsTemplate) {
		CopyDocumentInfo aCopyDocInfo = new CopyDocumentInfo(ErrorCode.COPYDOCUMENT_FAILED_ERROR);

		if ((null != aSession) && (null != sTargetFolderID) && (null != sFileName)) {
			boolean locked = false;
			boolean rollback = false;
			boolean giveUp = false;
			InputStream documentStm = null;
			IDBasedFileAccess aFileAccess = null;

			try {
				int nCurrentOSN = -1;
				JSONArray aPendingOperations = null;

				locked = lockForLoadOrSaveDocumentWithTimeout(WAIT_FOR_LOADSAVE_LOCK);
				if (!locked) {
					// We are not able to continue although waiting for WAIT_FOR_SAVEINPROGRESS_LOCK
					// milliseconds
					// bail out and provide a error to the client.
					aCopyDocInfo.setErrorCode(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR);
					return aCopyDocInfo;
				}

				// from here we can be sure that no one can interrupt us storing this document
				synchronized (getStatusSyncObject()) {
					EditableDocumentStatus aDocState  = (EditableDocumentStatus) getDocumentStatus();
					aPendingOperations = getAdditionalOperations();
					nCurrentOSN = aDocState.getOperationStateNumber();
				}

				// set-up file access to read/write copy of file
				aFileAccess = idBasedFileAccessFactory.createAccess(aSession);

				// set up the new file object used to create a new infostore item
				final String sFileId = aUserData.getFileId();
				final File aTargetFile = new DefaultFile();
				final File aSourceFile = aFileAccess.getFileMetadata(sFileId, FileStorageFileAccess.CURRENT_VERSION);

				String sExtension = FileHelper.getExtension(aSourceFile.getFileName());
				String sMimeType = aSourceFile.getFileMIMEType();

				// Handle rescue formats which we don't save as _ox but with their
				// original extension.
				if (FileHelper.isRescueExtension(sExtension)) {
					sExtension = FileHelper.removeRescueExtensionPart(sExtension);
				}

				// We want to copy the document and store it as a template
				// document. Make sure that the document is not already a
				// document template.
				if (bAsTemplate && !DocumentFormatHelper.isSupportedTemplateFormat(sMimeType, sExtension)) {
					// Document should be copied and "transformed" into a document template.
					final Map<String, String> templateFormatInfo = DocumentFormatHelper
							.getTemplateFormatInfoForDocument(sMimeType, sExtension);
					if (null != templateFormatInfo) {
						sMimeType = templateFormatInfo.get(DocumentFormatHelper.PROP_MIME_TYPE);
						sExtension = templateFormatInfo.get(DocumentFormatHelper.PROP_INPUT_TYPE);
					} else {
						// We don't know target template format. Give up and provide error code.
						aCopyDocInfo.setErrorCode(ErrorCode.COPYDOCUMENT_TEMPLATE_FORMAT_UNKOWN_ERROR);
						giveUp = true;
					}
				}

				if (!giveUp) {
					// use provided values to initialize file object
					aTargetFile.setId(FileStorageFileAccess.NEW);
					aTargetFile.setFileName(sFileName + "." + sExtension);
					aTargetFile.setFolderId(sTargetFolderID);
					aTargetFile.setFileMIMEType(sMimeType);
					aTargetFile.setDescription(aSourceFile.getDescription());

					// Create the new document stream containing the latest changes
					// from the message chunk list.
					final SaveDebugProperties dbgProps = impl_setupSaveDebugProperties(aSession);
					final IResourceManager aResMgr = getResourceManager();
					final OXDocument oxDocument = oxDocumentFactory.createDocumentAccess(aSession, aUserData, false, aResMgr, false, dbgProps);

					// we can safely unlock here as we need a consistent doc content
					// and the additional operations
					locked = false;
				    unlockForLoadOrSaveDocument();

					oxDocument.setUniqueDocumentId(nCurrentOSN);

					final IExporter exporter = imExporterAccessFactory.getExporterAccess(oxDocument.getDocumentFormat());
					final ResolvedStreamInfo docStreamInfo = oxDocument.getResolvedDocumentStream(
							exporter, aResMgr, aPendingOperations, null, false,
							bAsTemplate, false);

					if ((null != docStreamInfo) && (null != docStreamInfo.resolvedStream)
							&& (docStreamInfo.debugStream == null)) {
						documentStm = docStreamInfo.resolvedStream;
						aFileAccess.startTransaction();
						rollback = true;
						log.trace("RT connection: [copyDocument] saveDocument using meta data: {}", aTargetFile.toString());
						aFileAccess.saveDocument(aTargetFile, documentStm, FileStorageFileAccess.DISTANT_FUTURE,
								new ArrayList<Field>());
						aFileAccess.commit();
						rollback = false;

						// return actual parameters of new file
						aCopyDocInfo.setFileID(aTargetFile.getId());
						aCopyDocInfo.setFolderID(aTargetFile.getFolderId());
						aCopyDocInfo.setFileName(aTargetFile.getFileName());
						aCopyDocInfo.setErrorCode(ErrorCode.NO_ERROR);
					} else {
						// extract error code from the result object
						aCopyDocInfo.setErrorCode((null != docStreamInfo) ? docStreamInfo.errorCode
								: ErrorCode.COPYDOCUMENT_FAILED_ERROR);
					}
				}
			} catch (final OXException e) {
			    MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR.getCodeAsStringConstant());
				log.error("RT2: impl_copyDocumentContent - failed to copy document.", e);
                MDC.remove(MDCEntries.ERROR);
				aCopyDocInfo.setErrorCode(
						ExceptionToErrorCode.map(e, ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR, true));
			} catch (final Exception e) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
				log.error("RT2: impl_copyDocumentContent - failed to copy document due to exception.", e);
                MDC.remove(MDCEntries.ERROR);
				aCopyDocInfo.setErrorCode(ErrorCode.GENERAL_UNKNOWN_ERROR);
			} finally {
				// Roll-back (if needed) and finish
				if (rollback) {
					TransactionAwares.rollbackSafe(aFileAccess);
				}

				TransactionAwares.finishSafe(aFileAccess);
				IOHelper.closeQuietly(documentStm);

				if (locked)
				    unlockForLoadOrSaveDocument(); // unlock load/save lock
			}
		}

		return aCopyDocInfo;
	}

	// -------------------------------------------------------------------------
	private void impl_broadcastDocumentStatusIfModified(final RT2CliendUidType sExceptClientUID) throws Exception {
		final EditableDocumentStatus aUpdateDocStatus = (EditableDocumentStatus) getDocumentStatusCloneIfModified(true);
		if (null != aUpdateDocStatus) {
			// broadcast document update message to all other clients in case status has
			// changed
			final RT2Message aUpdateDocStateBroadcast = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_UPDATE, getDocUID());
			MessageHelper.setDocumentStatus(aUpdateDocStateBroadcast, aUpdateDocStatus);
			messageSender.broadcastMessageExceptClient(sExceptClientUID, aUpdateDocStateBroadcast, RT2MessageType.BROADCAST_UPDATE);
		}
	}

	// -------------------------------------------------------------------------
	private void impl_broadcastClientsStatusIfModified(final RT2CliendUidType sExceptClientUID) throws Exception {
		// send clients update message to all other clients
		final EditableClientsStatus aUpdateClientsStatus = (EditableClientsStatus) this.getClientsStatusCloneIfModified(true);
		if (null != aUpdateClientsStatus) {
			// broadcast clients update message to all other clients in case status has
			// changed
			final RT2Message aUpdateDocStateBroadcast = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_UPDATE_CLIENTS, getDocUID());
			MessageHelper.setClientsStatus(aUpdateDocStateBroadcast, aUpdateClientsStatus, false);
			messageSender.broadcastMessageExceptClient(sExceptClientUID, aUpdateDocStateBroadcast, RT2MessageType.BROADCAST_UPDATE_CLIENTS);
		}
	}

	// -------------------------------------------------------------------------
	protected void setupSaveDebugProperties(final SaveDebugProperties props, final Session session) throws Exception {
		try {
			final UserConfigurationHelper userConfHelper = userConfigurationFactory.create(session, "io.ox/office", Mode.WRITE_BACK);

			final boolean bSlowSaveUser = userConfHelper.getBoolean("module/debugslowsaveuser", false);

			props.setProperty(SaveDebugProperties.DEBUG_PROP_SLOWSAVE, m_bSlowSave);
			props.setProperty(SaveDebugProperties.DEBUG_PROP_SLOWSAVETIME, m_nSlowSaveTime);
			props.setProperty(SaveDebugProperties.DEBUG_PROP_SLOWSAVEUSER, bSlowSaveUser);
			props.setProperty(SaveDebugProperties.DEBUG_PROP_SAVEDOCHISTORY, m_bDebugSaveDocHistory);
		} catch (Exception e) {
			log.info("Exception caught while trying to setup debug slow save!", e);
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Tries to restore a document using the provided data. If successful, this will
	 * create a new document file with the latest operations applied.
	 *
	 * @param session
	 *            the session of the client requesting to restore the document
	 * @param userData
	 *            the UserData instance of the client
	 * @param initialFilename
	 *            the initial file name to be used for the restored document
	 * @param aOpsArray
	 *            the current pending operations
	 */
	private void tryToRestoreDocument(final ServerSession session, final UserData userData, final String initialFilename,
			final JSONArray aOpsArray) throws Exception {
		// call restore document to save the document with the back data and our
		// pending operations.
		final RestoreData result = restoreDocument.saveAsWithBackupDocumentAndOperations(session, userData.getFileId(),
				getDocRestoreID().toString(), userData.getFolderId(), initialFilename, aOpsArray);

		if ((null != result) && (result.getErrorCode().isNoError() || result.getErrorCode()
				.getCode() == ErrorCode.BACKUPDOCUMENT_RESTORE_DOCUMENT_PARTIAL_WRITTEN.getCode()))
			m_aRestoreData.set(result);
	}

	// -------------------------------------------------------------------------
	/**
	 * Broadcast document update message for all clients that need to be notified.
	 *
	 * @param sExceptClientID
	 *            a client which triggered the broadcast (must not be notified).
	 * @param lOperations
	 *            an array of operations which should be broadcasted
	 * @param aOpsMessage
	 *            the original message which triggered this broadcast
	 * @throws Exception
	 */
	private void broadcastUpdateMessage(final RT2CliendUidType sExceptClientID, final JSONArray lOperations, final RT2Message aOpsMessage) throws Exception {
		final JSONObject aDocState = getDocumentStatusClone().toJSON();
		final RT2Message aUpdateBroadcast = RT2MessageFactory.createResponseFromMessage(aOpsMessage, RT2MessageType.BROADCAST_UPDATE);
		MessageHelper.setOperations(aUpdateBroadcast, lOperations);
		MessageHelper.setDocumentStatus(aUpdateBroadcast, aDocState);

		final Collection<RT2CliendUidType> sReceivers = getReceiversForUpdate(sExceptClientID);
		messageSender.broadcastMessageTo(sReceivers, aUpdateBroadcast, RT2MessageType.BROADCAST_UPDATE);
	}

	// -------------------------------------------------------------------------
	/**
	 * Provides an array of client UIDs which must be notified on document updates.
	 *
	 * @param sExceptClientID
	 *            the client which triggered the updates (must not be notified)
	 * @return an array of client UIDs which must be notified (can be empty)
	 * @throws Exception
	 */
	private Collection<RT2CliendUidType> getReceiversForUpdate(final RT2CliendUidType sExceptClientID) throws Exception {
		final List<RT2CliendUidType> aReceiversList = getUserIDsFiltered(UserData::filterOutLoadStateInProgress).stream()
				.filter(s -> !s.equals(sExceptClientID.getValue())).map(c -> new RT2CliendUidType(c)) .collect(Collectors.toList());
		return aReceiversList;
	}

	// -------------------------------------------------------------------------
	private void broadcastHangupMessageToAll(final RT2CliendUidType sExceptClientID, ErrorCode aErrorCode,
			final EditableDocumentStatus aDocState) throws Exception {
		final RT2Message aBackgroundSaveFailedMsg = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_HANGUP, sExceptClientID, getDocUID());

		// A hang-up message is sent to all clients (except sExceptClientID)
		// to notify them that the document cannot be used for collaboration
		// anymore.
		RT2MessageGetSet.setError(aBackgroundSaveFailedMsg, aErrorCode);
		MessageHelper.setDocumentStatus(aBackgroundSaveFailedMsg, aDocState);

		messageSender.broadcastMessageExceptClient(null, aBackgroundSaveFailedMsg, RT2MessageType.BROADCAST_HANGUP);
	}

	// -------------------------------------------------------------------------
	private void broadcastOTReloadToClient(RT2CliendUidType clientUID, ErrorCode errorCode) throws Exception {
		final RT2Message otReloadMsg = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_OT_RELOAD, clientUID, getDocUID());
		MessageHelper.setError(otReloadMsg, errorCode);
		MessageHelper.setDocumentStatus(otReloadMsg, this.getDocumentStatusClone());
		final Collection<RT2CliendUidType> clients = Arrays.asList(clientUID);
		messageSender.broadcastMessageTo(clients, otReloadMsg, RT2MessageType.BROADCAST_OT_RELOAD);
	}

	// -------------------------------------------------------------------------
	private void broadcastHangupMessageExceptClientUID(final RT2CliendUidType sExceptClientID, ErrorCode aErrorCode,
			final EditableDocumentStatus aDocState) throws Exception {
		final RT2Message aBackgroundSaveFailedMsg = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_HANGUP, sExceptClientID, getDocUID());

		// A hang-up message is sent to all clients (except sExceptClientID)
		// to notify them that the document cannot be used for collaboration
		// anymore.
		RT2MessageGetSet.setError(aBackgroundSaveFailedMsg, aErrorCode);
		MessageHelper.setDocumentStatus(aBackgroundSaveFailedMsg, aDocState);

		messageSender.broadcastMessageExceptClient(sExceptClientID, aBackgroundSaveFailedMsg, RT2MessageType.BROADCAST_HANGUP);
	}

	// -------------------------------------------------------------------------
	private void sendHangupMessage(final RT2CliendUidType sClientID, ErrorCode aErrorCode, final JSONArray aPendingOpsArray,
			final EditableDocumentStatus aDocState) throws Exception {

	    final RT2Message aHangUpMessage = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_HANGUP, sClientID, getDocUID());

		// A single hang-up message is sent to a specific client to notify it that
		// the document content has changed and that this client cannot be used
		// for collaboration anymore. Normally happens when a client sends operations
		// although it has no edit rights.
		RT2MessageGetSet.setError(aHangUpMessage, aErrorCode);
		MessageHelper.setDocumentStatus(aHangUpMessage, aDocState);

		if (null != aPendingOpsArray)
			MessageHelper.setOperations(aHangUpMessage, aPendingOpsArray);

		messageSender.sendMessageWOSeqNrTo(sClientID, aHangUpMessage);
	}

    // -------------------------------------------------------------------------
    private RT2CliendUidType findNewSavior() {
        RT2CliendUidType newSaviorUid = null;

        final List<UserData> users = getUserDatasCloned();
        if ((users != null) && (users.size() > 0)) {
            for (final UserData userData : users) {
                try {
                    final ServerSession aSession = sessionService.getSession4Id(userData.getSessionId());
                    if ((aSession != null) && impl_canUserAccessFile(aSession, userData.getFileId())) {
                        newSaviorUid = new RT2CliendUidType(userData.getClientUid());
                    }
                } catch (Exception e) {
                    log.debug("OT: exception caught trying to determine new savior for concurrent editing", e);
                }
            }
        }

        return (newSaviorUid == null) ? DocumentStatus.EMPTY_CLIENTID : newSaviorUid;
    }

	// -------------------------------------------------------------------------
	private boolean setNewSavior(RT2CliendUidType newSaviorUid) {
		if (RT2CliendUidType.isNotEmpty(newSaviorUid)) {
			final UserData userData = getUserData(newSaviorUid);
			if (userData != null) {
				try {
					return impl_checkClientToSetAsSavior(userData.getSessionId(), newSaviorUid, true);
				} catch (OXException e) {
					log.info("OT: exception caught trying to determine and set new possible savior!", e);
				}
			}
		}

		return false;
	}

	// -------------------------------------------------------------------------
    protected boolean impl_checkClientToSetAsSavior(final String sSessionID, final RT2CliendUidType sNewClientUID, boolean bCanWriteFile) throws OXException {
        if (bCanWriteFile && !isReadOnlyState()) {
            final ServerSession aSession = sessionService.getSession4Id(sSessionID);
            final User aUserInfo = aSession.getUser();
            final String sDisplayName = UserHelper.getFullName(aUserInfo);

            // we set a user-id as new "savior" in case we don't have a current one
            synchronized (getStatusSyncObject()) {
                final EditableDocumentStatus aDocState = (EditableDocumentStatus) getDocumentStatus();
                final RT2CliendUidType sEditingUserUID = aDocState.getCurrentEditingUserId();

                if (RT2CliendUidType.isEmpty(sEditingUserUID)) {
                	specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "Set current editing user with com.openexchange.rt2.client.uid {} for com.openexchange.rt2.document.uid {}", sNewClientUID, getDocUID());
                	aDocState.setCurrentEditingUser(sNewClientUID, sDisplayName);
                	return true;
                }
            }
        }
        return false;
    }
}
