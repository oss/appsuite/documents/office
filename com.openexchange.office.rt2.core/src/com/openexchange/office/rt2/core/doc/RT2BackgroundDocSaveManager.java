/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.EasyStopWatch;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.monitoring.BackgroundSaveEvent;
import com.openexchange.office.tools.monitoring.BackgroundSaveEventType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.logging.SpecialLogService;

/**
 * A class which controls the background save of modified documents according to
 * the pre-defined rule set. The background processing is done using the OX
 * TimerService and tries to have a minimal impact of overall system performance.
 * It checks the runtime of save processing and delays it if necessary.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.10.0
 *
 */
@Service
@ShutdownOrder(value = -10)
@ConditionalOnProperty(name = { RT2ConfigService.KEY_BACKGROUND_SAVE_DOC }, matchIfMissing = true)
public class RT2BackgroundDocSaveManager implements IDocNotificationHandler, IProcessDocCollection, InitializingBean, DisposableBean {

    @Autowired
    private Statistics statistics;

    @Autowired
    private EmergencySaver emergencySaver;

    @Autowired
    private SpecialLogService specialLogService;

    /**
     * Internal class to encapsulate the timer based background processing of the
     * document processors.
     *
     * {@link ProcessingRunnable}
     *
     * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
     * @since v7.8.4
     */
    private static class ProcessingRunnable implements Runnable {

        private final WeakReference<IProcessDocCollection> m_handler;

        public ProcessingRunnable(final IProcessDocCollection handler) {
            super();

            m_handler = new WeakReference<>(handler);
        }

        @Override
        public void run() {
            IProcessDocCollection handler = m_handler.get();
            if (null != handler) {
                handler.processDocCollection();
            }
        }
    }

    /**
     * Stores data of a document instance where a not-handled exception
     * was caught during the save process. This data determines how the
     * RT2BackgroundDocSaveManager will process further background save
     * tries.
     * {@link RT2BackgroundSaveData}
     *
     * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
     * @since v8.0.0
     */
    private static class RT2BackgroundSaveData {
        private int numOfRetries = 0;

        public RT2BackgroundSaveData() {
            super();
        }

        public int numOfRetries() {
            return this.numOfRetries;
        }

        public int incNumOfRetries() {
            return ++this.numOfRetries;
        }
    }

    //-------------------------------------------------------------------------
    private static final Logger log                         = LoggerFactory.getLogger(RT2BackgroundDocSaveManager.class);
    private static final String BACKGROUND_SAVE_THREAD_NAME = "com.openexchange.office.rt2.core.doc.RT2BackgroundDocSaveManager";

    private static final int MIN_TIME_FOR_METRIC_RESET               = 60 * 1000;      // time in milliseconds
    private static final int MIN_OPERATIONS_FOR_FAILSAFESAVE         = 50;
    private static final int MIN_OPERATIONS_FOR_FAILSAFESAVE_FOR_OT  = 200;
    private static final long MIN_TIME_FOR_MANY_OPS_FAILSAFESAVE     = 60 * 1000;      // time in milliseconds
    private static final long MAX_TIME_FOR_MODIFIED_FAILSAFESAVE     = 15 * 60 * 1000; // time in milliseconds

    public static final int MAX_NUMBER_OF_RETRIES = 10;

    //-------------------------------------------------------------------------
    private Thread                                         m_backgroundThread     = null;
    private Map<String, WeakReference<IBackgroundSavable>> m_savableDocCollection = new HashMap<>();
    private Map<String, RT2BackgroundSaveData>             m_saveDocWithProblems  = new ConcurrentHashMap<>();
    private boolean                                        m_clearInsteadOfSave   = false;

    @Autowired
    private ConfigurationService cfgService;

    @Autowired
    private RT2ConfigService rt2ConfigService;

    @Override
    public void afterPropertiesSet() throws Exception {
        int minOperationsForFailSafeSaveText = MIN_OPERATIONS_FOR_FAILSAFESAVE;
        int minOperationsForFailSafeSaveSpreadsheet = MIN_OPERATIONS_FOR_FAILSAFESAVE;
        int minOperationsForFailSafeSavePresentation = MIN_OPERATIONS_FOR_FAILSAFESAVE;

        if (null != cfgService) {
            // Retrieve configuration to support reset operation queue instead of saving the document
            // ATTENTION: This is ONLY for sizing tests, otherwise data can/will be lost
            m_clearInsteadOfSave = cfgService.getBoolProperty("io.ox/office//module/fssResetOperationQueue", false);
            if (m_clearInsteadOfSave) {
                log.warn("RT2: Warning - fail safe save is configured to clear the operation queue of the documents - this is for sizing tests only!");
            }
            minOperationsForFailSafeSaveText = rt2ConfigService.isOTEnabled(ApplicationType.APP_TEXT_STRING) ? MIN_OPERATIONS_FOR_FAILSAFESAVE_FOR_OT : MIN_OPERATIONS_FOR_FAILSAFESAVE;
            minOperationsForFailSafeSaveSpreadsheet = rt2ConfigService.isOTEnabled(ApplicationType.APP_SPREADSHEET_STRING) ? MIN_OPERATIONS_FOR_FAILSAFESAVE_FOR_OT : MIN_OPERATIONS_FOR_FAILSAFESAVE;
            minOperationsForFailSafeSavePresentation = rt2ConfigService.isOTEnabled(ApplicationType.APP_PRESENTATION_STRING) ? MIN_OPERATIONS_FOR_FAILSAFESAVE_FOR_OT : MIN_OPERATIONS_FOR_FAILSAFESAVE;
        }

        if (null != statistics) {
            statistics.setBackgroundSaveMinNumOfMsgForFasterSaveText(minOperationsForFailSafeSaveText);
            statistics.setBackgroundSaveMinNumOfMsgForFasterSaveSpreadsheet(minOperationsForFailSafeSaveSpreadsheet);
            statistics.setBackgroundSaveMinNumOfMsgForFasterSavePresentation(minOperationsForFailSafeSavePresentation);
            statistics.setBackgroundSaveMinTimeFasterSave(MIN_TIME_FOR_MANY_OPS_FAILSAFESAVE);
            statistics.setBackgroundSaveMaxTimeForSave(MAX_TIME_FOR_MODIFIED_FAILSAFESAVE);
        }

        if (m_backgroundThread == null) {
            m_backgroundThread = new Thread(new ProcessingRunnable(this));
            m_backgroundThread.setPriority(Thread.NORM_PRIORITY - 1);
            m_backgroundThread.setName(BACKGROUND_SAVE_THREAD_NAME);
            m_backgroundThread.start();
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Stops a running document processing timer.
     */
    public synchronized boolean stop() {
        if (m_backgroundThread != null) {
            try {
                m_backgroundThread.interrupt();
                m_backgroundThread.join();
                m_backgroundThread = null;
                return true;
            } catch (final InterruptedException e) {
                log.debug("RT2: RT2BackgroundDocSaveManager InterruptedException caught preparing shutdown of the background saving thread", e);
                Thread.currentThread().interrupt();
            }
        }

        return false;
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorCreated(final RT2DocProcessor aCreatedInstance) {
        if (aCreatedInstance instanceof IBackgroundSavable) {
            synchronized (m_savableDocCollection) {
                final IBackgroundSavable iSavableDoc = (IBackgroundSavable) aCreatedInstance;
                final WeakReference<IBackgroundSavable> aWeakRef = new WeakReference<>(iSavableDoc);

                final WeakReference<IBackgroundSavable> aOldWeakRef = m_savableDocCollection.put(aCreatedInstance.getDocUID().getValue(), aWeakRef);
                if (aOldWeakRef != null) {
                    specialLogService.log(Level.DEBUG, log, aCreatedInstance.getDocUID().getValue(), new Throwable(), "RT2BackgroundDocSaveManager.docProcessorCreated: There is a previous reference to the com.openexchange.rt2.document.uid {} stored", aCreatedInstance.getDocUID());
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorDisposed(final RT2DocProcessor aDisposedInstance) {
        if (aDisposedInstance instanceof IBackgroundSavable) {
            String docUid = aDisposedInstance.getDocUID().getValue();
            m_saveDocWithProblems.remove(docUid);

            synchronized (m_savableDocCollection) {
                final WeakReference<IBackgroundSavable> aOldWeakRef = m_savableDocCollection.remove(docUid);
                if (aOldWeakRef == null) {
                    specialLogService.log(Level.DEBUG, log, docUid, new Throwable(), "RT2BackgroundDocSaveManager.docProcessorDisposed: Unknown com.openexchange.rt2.document.uid {}!", docUid);
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void processDocCollection() {
        boolean interrupted = false;
        Date lastValuesSet = new Date();

        while (!interrupted) {
            try {
                final EasyStopWatch stopWatch = new EasyStopWatch();
                final List<WeakReference<IBackgroundSavable>> aAllSavableDocList = new ArrayList<>();

                synchronized (m_savableDocCollection) {
                    if (!m_savableDocCollection.isEmpty())
                        aAllSavableDocList.addAll(m_savableDocCollection.values());
                }

                // filter document list to find only documents that must be saved now
                final List<WeakReference<IBackgroundSavable>> aSavableDocList = getDocumentsToBeSaved(aAllSavableDocList);
                final Iterator<WeakReference<IBackgroundSavable>> iter = aSavableDocList.iterator();
                final Date aNow = new Date();

                if (iter.hasNext()) {
                    try {
                        int numOfDocumentsSaved = 0;

                        // process documents
                        stopWatch.start();
                        while (iter.hasNext()) {
                            final IBackgroundSavable aSavableDoc = iter.next().get();

                            BackgroundSaveReason eReason = BackgroundSaveReason.NO_REASON;
                            if ((null != aSavableDoc) && ((eReason = mustBeProcessed(aSavableDoc, aNow)) != BackgroundSaveReason.NO_REASON)) {
                                if (m_clearInsteadOfSave)
                                    eReason = BackgroundSaveReason.DELETE_PENDING_OPERATIONS;

                                // save document and suppress any exception
                                String uid = aSavableDoc.getUniqueID().getValue();
                                boolean bSuccess = backgroundSaveDocument(aSavableDoc, eReason);
                                if (bSuccess) {
                                    // document is processed, if contEnumeration is true
                                    m_saveDocWithProblems.remove(uid);
                                    aSavableDoc.setProcessedState(true);
                                    numOfDocumentsSaved++;
                                }
                            }

                            // detect interrupted state to bail out early
                            if (Thread.interrupted())
                                throw new InterruptedException();
                        }

                        final long elapsedTime = stopWatch.stop();

                        if (numOfDocumentsSaved > 0) {
                            final long timePerDoc = (elapsedTime / Math.max(numOfDocumentsSaved, 1));

                            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_DOC_COUNT, numOfDocumentsSaved));
                            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME, elapsedTime));
                            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME_PER_DOC, timePerDoc));

                            log.debug("RT2: FSS Pool contains {} document(s) to be saved - {} document(s) were saved in {} ms, time per document = {}", aSavableDocList.size(), numOfDocumentsSaved, elapsedTime, timePerDoc);
                            lastValuesSet = new Date();
                        } else {
                            resetBackgroundSaveMetrics(lastValuesSet);
                        }

                        if (!iter.hasNext()) {
                            // reset processed state to enable processing again
                            final Iterator<WeakReference<IBackgroundSavable>> resetIter = aAllSavableDocList.iterator();
                            while (resetIter.hasNext()) {
                                final IBackgroundSavable aSavableDoc = resetIter.next().get();
                                if (null != aSavableDoc) {
                                    aSavableDoc.setProcessedState(false);
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        interrupted = true;
                        throw new InterruptedException();
                    } catch (final Exception e) {
                        log.error("RT2: Exception caught while FSS processes connections", e);
                    }
                } else {
                    resetBackgroundSaveMetrics(lastValuesSet);
                }

                cleanupSaveDocWithProblems();

                Thread.sleep(1000);
            } catch (final InterruptedException e) {
                Thread.currentThread().interrupt();
                interrupted = true;
            }
        }
    }

    //-------------------------------------------------------------------------
    private void resetBackgroundSaveMetrics(@NotNull Date lastValuesSet) {
        final long lastValuesSetTimestamp = lastValuesSet.getTime();
        final long timeDiff = getTimeDifference(lastValuesSetTimestamp, new Date().getTime());
        if (timeDiff >= MIN_TIME_FOR_METRIC_RESET) {
            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_DOC_COUNT, 0));
            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME, 0));
            statistics.handleBackgroundSaveDocEvent(new BackgroundSaveEvent(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME_PER_DOC_RESET));
        }
    }

    //-------------------------------------------------------------------------
    private boolean backgroundSaveDocument(final IBackgroundSavable aSavableDoc, BackgroundSaveReason eReason) {
        try {
            aSavableDoc.save(eReason, false);
            return true;
        } catch (final Throwable t) {
            ExceptionUtils.handleThrowable(t);
            log.warn("RT2: Exception caught while trying to background save document with com.openexchange.rt2.document.uid " + aSavableDoc.getUniqueID(), t);
            handleExceptionDuringSave(aSavableDoc, t, eReason);
            return false;
        }
    }

    //-------------------------------------------------------------------------
    private List<WeakReference<IBackgroundSavable>> getDocumentsToBeSaved(final List<WeakReference<IBackgroundSavable>> aSavableDocList) {
        List<WeakReference<IBackgroundSavable>> aFilteredList = aSavableDocList;

        if ((null != aSavableDocList) && (!aSavableDocList.isEmpty())) {
            final ArrayList<WeakReference<IBackgroundSavable>> aFilteredCollection = new ArrayList<>();
            final Iterator<WeakReference<IBackgroundSavable>> iter = aSavableDocList.iterator();

            while (iter.hasNext()) {
                final WeakReference<IBackgroundSavable> aSavableDocRef = iter.next();
                final IBackgroundSavable aSavableDoc = aSavableDocRef.get();

                if ((aSavableDoc != null) && (aSavableDoc.isModified()))
                    aFilteredCollection.add(aSavableDocRef);
            }

            aFilteredList = aFilteredCollection;
        }

        return aFilteredList;
    }

    //-------------------------------------------------------------------------
    private void handleExceptionDuringSave(IBackgroundSavable badSaveDoc, Throwable t, BackgroundSaveReason eReason) {
        String uid = badSaveDoc.getUniqueID().getValue();
        RT2BackgroundSaveData data = m_saveDocWithProblems.putIfAbsent(uid, new RT2BackgroundSaveData());
        log.debug("handleExceptionDuringSave data {}, reason {}", data, eReason);
        if ((data != null) && (eReason != BackgroundSaveReason.SHUTDOWN)) {
            // DOCS-4276: Always handle an caught exception to prevent that we try to
            // background save a document many times. We now limit the retries to MAX_NUMBER_OF_RETRIES
            // and give up afterward by setting an error code at the document instance.
            final AtomicBoolean removeData = new AtomicBoolean(false);
            m_saveDocWithProblems.computeIfPresent(uid, (key, value) -> {
                value.incNumOfRetries();
                log.debug("handleExceptionDuringSave for docUid {} - retry {}", uid, value.numOfRetries());
                if (value.numOfRetries() >= MAX_NUMBER_OF_RETRIES) {
                    ErrorCode errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                    if (t instanceof OXException) {
                        errorCode = ExceptionToErrorCode.map((OXException)t, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
                    }
                    log.warn("Giving up to store document instance with com.openexchange.rt2.document.uid {} - number of retries {} reached max {}.", uid, value.numOfRetries(), MAX_NUMBER_OF_RETRIES);
                    badSaveDoc.setLastSaveErrorCode(errorCode);
                    removeData.compareAndSet(false, true);
                }

                return value;
            });

            // Remove temporary background save state as we set a final
            // error code to the document instance.
            if (removeData.get()) {
                m_saveDocWithProblems.remove(uid);
            }
        }
    }

    //-------------------------------------------------------------------------
    private void cleanupSaveDocWithProblems() {
        Map<String, WeakReference<IBackgroundSavable>> knownSavableDocs = new HashMap<>();

        synchronized (m_savableDocCollection) {
            knownSavableDocs.putAll(m_savableDocCollection);
        }

        final Set<String> removeKeys = new HashSet<>();
        m_saveDocWithProblems.forEach((k, v) -> {
            WeakReference<IBackgroundSavable> r = knownSavableDocs.get(k);
            if (WeakRefUtils.getHardRef(r) == null)
                removeKeys.add(k);
        });

        removeKeys.stream().forEach(k -> m_saveDocWithProblems.remove(k));
    }

    //-------------------------------------------------------------------------
    /**
     * Determines, if a document instance must be processed or not by fail safe save.
     *
     * @param aSavableDoc A document to be checked for background save processing.
     * @param now The current time
     * @return the reason to save the document or NO_REASON if the instance
     *  does not meet the criteria to be saved now.
     */
    private BackgroundSaveReason mustBeProcessed(final IBackgroundSavable aSavableDoc, final Date now) {
        BackgroundSaveReason eReason = BackgroundSaveReason.NO_REASON;

        if (shouldBeSaved(aSavableDoc)) {
            long offset = 0;
            final RT2DocUidType docUid = aSavableDoc.getUniqueID();
            final RT2BackgroundSaveData furtherData = this.m_saveDocWithProblems.get(docUid.getValue());
            // delay further retries to increase probability that
            // next background save could work
            if (furtherData != null) {
                offset += furtherData.numOfRetries() * MIN_TIME_FOR_MANY_OPS_FAILSAFESAVE;
            }
            final long diff = getTimeDifference(aSavableDoc.getLastSaveTimeStamp() + offset, now.getTime());
            final long nOps = aSavableDoc.getPendingOperationsCount();

            BackgroundSaveReason saveReason = BackgroundSaveReason.NO_REASON;
            final int minOpsForFasterFailSafeSave = aSavableDoc.isOTEnabled() ? MIN_OPERATIONS_FOR_FAILSAFESAVE_FOR_OT : MIN_OPERATIONS_FOR_FAILSAFESAVE;

            if ((diff >= MIN_TIME_FOR_MANY_OPS_FAILSAFESAVE) && (nOps >= minOpsForFasterFailSafeSave))
                saveReason = BackgroundSaveReason.TOO_MANY_OPS;
            else if (((diff >= MAX_TIME_FOR_MODIFIED_FAILSAFESAVE) && (nOps > 0)))
                saveReason = BackgroundSaveReason.MODIFICATION_TIMEOUT;

            log.trace("BackgroundSave determined save reason {} for document {} with time diff {} and ops {}", saveReason, aSavableDoc.getUniqueID(), diff, nOps);
            eReason = saveReason;
        }

        return eReason;
    }

    //-------------------------------------------------------------------------
    /**
     * Determines if a document should be saved or not.
     *
     * @param aSavableDoc a document which can be saved in the background
     * @return TRUE if the document should be saved, otherwise FALSE
     * @throws Exception
     */
    private boolean shouldBeSaved(final IBackgroundSavable aSavableDoc) {
        return (isInValidState(aSavableDoc) && aSavableDoc.isModified() && !aSavableDoc.isSaveInProgress() && (aSavableDoc.getLastSaveErrorCode().getCode() == ErrorCode.NO_ERROR.getCode()));
    }

    //-------------------------------------------------------------------------
    /**
     * Determines if a document instance is valid to be processed.
     *
     * @param aSavableDoc a document which should be checked
     * @return TRUE if the document is in a valid state otherwise FALSE
     * @throws Exception
     */
    private boolean isInValidState(final IBackgroundSavable aSavableDoc) {
        return ((aSavableDoc != null) && !aSavableDoc.isDisposed());
    }

    //-------------------------------------------------------------------------
    /**
     * Calculates the absolute difference between two Dates
     *
     * @param t1 time stamp start in milliseconds
     * @param t2 time stamp end in milliseconds
     * @return The difference in milliseconds.
     */
    private long getTimeDifference(final long t1, final long t2) {
        return Math.abs(t2 - t1);
    }

    //-------------------------------------------------------------------------
    @Override
    public void destroy() throws Exception {
        if (stop()) {
            // Let the emergency saver decide which document must be
            // handle via emergencySave. Due to the advisory lock feature
            // this depends on several more conditions.
            emergencySaver.emergencySave();
        }
    }

}
