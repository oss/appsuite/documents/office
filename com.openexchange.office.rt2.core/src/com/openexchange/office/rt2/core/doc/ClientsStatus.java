/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

//-------------------------------------------------------------------------

public class ClientsStatus implements Cloneable {
	// -------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(ClientsStatus.class);

	// -------------------------------------------------------------------------
	public static final String PROP_UPDATECLIENTS_LEFT = "left";
	public static final String PROP_UPDATECLIENTS_JOINED = "joined";
	public static final String PROP_UPDATECLIENTS_CHANGED = "changed";
	public static final String PROP_UPDATECLIENTS_LIST = "list";

	// -------------------------------------------------------------------------
	public static final String PROP_CLIENTUID = "clientUID";
	public static final String PROP_ACTIVE = "active";
	public static final String PROP_DURATION_OF_INACTIVITY = "durationOfInactivity";

	// userId
	public static final String PROP_ID = "id";

	// Blob, wird einfach nur an andere Clients weitergeschickt(Wird nicht interpretiert)
	public static final String PROP_USERDATA = "userData";

	public static final String PROP_USER_DISPLAY_NAME = "userDisplayName";
	public static final String PROP_GUEST = "guest";

	// -------------------------------------------------------------------------
	private Map<RT2CliendUidType, JSONObject> m_aClientsMap = new HashMap<>();
	private Set<RT2CliendUidType> m_aModified = new HashSet<>();
	private Set<RT2CliendUidType> m_aAdded = new HashSet<>();
	private Set<RT2CliendUidType> m_aRemoved = new HashSet<>();

	// -------------------------------------------------------------------------
	public static class GenericClientStatusModifier<T> implements ClientsStatusModifier {
		// -------------------------------------------------------------------------
		private T m_aValue;
		private Class<T> m_aType;
		private String m_sProperty;

		// -------------------------------------------------------------------------
		public GenericClientStatusModifier(final String sProperty, final Class<T> aType, T aValue) {
			m_sProperty = sProperty;
			m_aType = aType;
			m_aValue = aValue;
		}

		// -------------------------------------------------------------------------
		@Override
		public boolean modify(JSONObject aClientStatus) throws Exception {
			aClientStatus.put(m_sProperty, m_aType.cast(m_aValue));
			return true;
		}
	}

	// -------------------------------------------------------------------------
	public static class GenericClientStatusCounter implements ClientsStatusObserver {
		// -------------------------------------------------------------------------
		private final Object m_aValue;
		private final String m_sProperty;
		private int m_nCounter = 0;

		// -------------------------------------------------------------------------
		public GenericClientStatusCounter(final String sProperty, Object aValue) {
			m_sProperty = sProperty;
			m_aValue = aValue;
		}

		// -------------------------------------------------------------------------
		int getCount() {
			return m_nCounter;
		}

		// -------------------------------------------------------------------------
		@Override
		public void observe(final JSONObject aClientStatus) throws Exception {
			m_nCounter = (m_aValue.equals(aClientStatus.opt(m_sProperty))) ? m_nCounter + 1 : m_nCounter;
		}
	}

	// -------------------------------------------------------------------------
	public static class GenericClientsStatusFilter implements ClientsStatusObserver {
		// -------------------------------------------------------------------------
		private final Object m_aValue;
		private final String m_sProperty;
		private final Set<JSONObject> m_aMatchedEntries = new HashSet<>();

		public GenericClientsStatusFilter(final String sProperty, Object aValue) {
			m_sProperty = sProperty;
			m_aValue = aValue;
		}

		// -------------------------------------------------------------------------
		public Set<JSONObject> getMatchedEntries() {
			return m_aMatchedEntries;
		}

		// -------------------------------------------------------------------------
		@Override
		public void observe(JSONObject aClientStatus) throws Exception {
			if (m_aValue.equals(aClientStatus.opt(m_sProperty)))
				m_aMatchedEntries.add(aClientStatus);
		}
	}

	// -------------------------------------------------------------------------
	public ClientsStatus() {
	}

	// -------------------------------------------------------------------------
	public ClientsStatus(final ClientsStatus aStatus) throws Exception {
		m_aClientsMap = createDeepCloneOfClientsMap(aStatus.m_aClientsMap);
		m_aModified = new HashSet<>(aStatus.m_aModified);
		m_aAdded = new HashSet<>(aStatus.m_aAdded);
		m_aRemoved = new HashSet<>(aStatus.m_aRemoved);
	}

	// -------------------------------------------------------------------------
	public static boolean isEmptyUser(final String user) {
		return StringUtils.isEmpty(user);
	}

	// -------------------------------------------------------------------------
	public static boolean isEmptyUserName(final String userName) {
		return StringUtils.isEmpty(userName);
	}

	// -------------------------------------------------------------------------
	public static boolean isNotEmptyUser(final String user) {
		return !StringUtils.isEmpty(user);
	}

	// -------------------------------------------------------------------------
	public static boolean isNotEmptyUserName(final String userName) {
		return !StringUtils.isEmpty(userName);
	}

	// -------------------------------------------------------------------------
	/**
	 * Determines if this instance has been modified since creation or the last
	 * clearModifications() call. See
	 * {@link com.openexchange.office.rt2.core.doc.ClientsStatus#clearModifications()}
	 * to reset this modification indicator.
	 *
	 * @return TRUE if modified otherwise FALSE
	 */
	public boolean isModified() {
		return (!this.m_aAdded.isEmpty() || !this.m_aModified.isEmpty() || !this.m_aRemoved.isEmpty());
	}

	// -------------------------------------------------------------------------
	/**
	 * Provides the number of active clients.
	 *
	 * @return The number of active clients.
	 */
	public int numberOfClients() {
		return (null != m_aClientsMap) ? m_aClientsMap.size() : 0;
	}

	// -------------------------------------------------------------------------
	/**
	 * Disables an active user so her data is NOT sent to other clients.
	 *
	 * @param userId The ID of the user to be disabled.
	 *
	 * @return TRUE if the user is disabled, FALSE if the user couldn't be found in
	 *         the list of active users.
	 */
	public boolean disableClient(RT2CliendUidType sClientUID) {
		final JSONObject user = getUserData(sClientUID);
		if (null != user) {
			try {
				user.put(PROP_ACTIVE, false);
				setModified(sClientUID);
			} catch (JSONException e) {
				log.error("Exception caught chaning client data", e);
			}
			return true;
		}

		return false;
	}

	// -------------------------------------------------------------------------
	public boolean changeStateOfActiveClient(RT2CliendUidType sClientUID, long durationOfInactivity) {
		log.debug("RT connection: Change user state for clientUID {}. EnableActiveUser: {}", sClientUID,
				durationOfInactivity == 0);
		if (durationOfInactivity == 0) {
			return enableActiveClient(sClientUID);
		} else {
			return disableActiveClient(sClientUID, durationOfInactivity);
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Enables an active user again so her data is sent to other clients.
	 *
	 * @param userId The ID of the user to be enabled again.
	 * @return TRUE if the user is active again, FALSE if the user couldn't be found
	 *         in the list of active users.
	 *
	 * @throws JSONException
	 */
	private boolean enableActiveClient(RT2CliendUidType sClientUID) {
		if (!isClientEnabled(sClientUID)) {
			final JSONObject user = m_aClientsMap.get(sClientUID);
			if (null != user) {
				try {
					user.put(PROP_ACTIVE, true);
					user.put(PROP_DURATION_OF_INACTIVITY, 0L);
					setModified(sClientUID);
					return true;
				} catch (JSONException e) {
					log.error("Exception caught changing client data", e);
				}
			}
		}
		return false;
	}

	// -------------------------------------------------------------------------
	/**
	 * Disables an active user or set a new inactivity duration for a disabled
	 * client.
	 *
	 * @param userId The ID of the user to be disabled.
	 *
	 * @param durationOfInactivity The duration of inactivity in seconds.
	 *
	 * @return TRUE if the user is disabled, FALSE if the user couldn't be found in
	 *         the list of active users.
	 */
	private boolean disableActiveClient(RT2CliendUidType sClientUID, long durationOfInactivity) {
		// DOCS-1741: Never use isClientEnabled() to optimize this code as the client
		// relies on the fact that it get several updates with increasing unavailability
		// time.
		final JSONObject user = m_aClientsMap.get(sClientUID);
		if (null != user) {
			try {
				int lastDurationOfInactivity = user.optInt(PROP_DURATION_OF_INACTIVITY, 0);
				user.put(PROP_ACTIVE, false);
				if (lastDurationOfInactivity < durationOfInactivity) {
					// Don't set a lower inactivity time in case of
					// disable active user. He/she must be activated
					// before.
					user.put(PROP_DURATION_OF_INACTIVITY, durationOfInactivity);
				}
				setModified(sClientUID);
				return true;
			} catch (JSONException e) {
				log.error("Exception caught chaning client data", e);
			}
		}

		return false;
	}

	// -------------------------------------------------------------------------
	/**
	 * Determine if the provided user is enabled or not.
	 *
	 * @param userId The ID of the user to find out the enabled state.
	 *
	 * @return TRUE if the user is enabled, FALSE if the user couldn't be found in
	 *         the list or is not enabled.
	 */
	private boolean isClientEnabled(RT2CliendUidType sClientUID) {
		final JSONObject user = m_aClientsMap.get(sClientUID);
		return ((null != user) && (user.optBoolean(PROP_ACTIVE, false)));
	}

	// -------------------------------------------------------------------------
	/**
	 * Determine if the provided user is enabled or not.
	 *
	 * @param userId The ID of the user to find out the enabled state.
	 *
	 * @return The duration of inactivity, where 0 means no inactivity and -1 that
	 *         the inactivity could not be determined (normally the user is
	 *         unknown).
	 */
	public long getDurationOfInactivity(RT2CliendUidType sClientUID) {
		final JSONObject user = m_aClientsMap.get(sClientUID);
		return (null != user) ? user.optLong(PROP_DURATION_OF_INACTIVITY, 0) : -1;
	}

	// -------------------------------------------------------------------------
	/**
	 * Determine if the provided user is enabled or not.
	 *
	 * @param userId    The ID of the user to find out the enabled state.
	 *
	 * @param nDuration The duration of inactivity in seconds.
	 *
	 * @return The duration of inactivity, where 0 means no inactivity and -1 that
	 *         the inactivity could not be determined (normally the user is
	 *         unknown).
	 */
	public void setDurationOfInactivity(RT2CliendUidType sClientUID, int nDuration) {
		final JSONObject user = m_aClientsMap.get(sClientUID);
		if (null != user) {
			try {
				user.put(PROP_DURATION_OF_INACTIVITY, nDuration);
				setModified(sClientUID);
			} catch (JSONException e) {
				log.error("Exception caught changing client data", e);
			}
		}
	}

	// -------------------------------------------------------------------------
	/**
	 * Get user id of the user associated with the com.openexchange.rt2.client.uid.
	 *
	 * @param clientUID The client uid
	 *
	 * @return The session user id of the user associated width the RT-id or -1 if
	 *         the RT-id is unknown.
	 */
	public int getClientId(RT2CliendUidType sClientUID) {
		final JSONObject user = m_aClientsMap.get(sClientUID);
		return (null != user) ? user.optInt(PROP_ID, -1) : -1;
	}

	// -------------------------------------------------------------------------
	/**
	 * Determines if a user id is part of the active users.
	 *
	 * @param userId The ID of the user to determine that is a member of this
	 *               connection.
	 *
	 * @return TRUE if the user is member or FALSE if not.
	 */
	public boolean hasClient(RT2CliendUidType sClientUID) {
		return (null != m_aClientsMap.get(sClientUID));
	}

	// -------------------------------------------------------------------------
	/**
	 * Update the userdata of a specific user
	 *
	 * @param userId   ID of the user
	 * @param userData JSON Object containing user data, e.g. {userName:'Rocky
	 *                 Balboa', selection:{...}, ...}
	 * @return Returns true if an update is done.
	 */
	public boolean updateClientData(RT2CliendUidType sClientUID, JSONObject userData) {
		final JSONObject user = m_aClientsMap.get(sClientUID);
		if (null != user) {
			try {
				user.put(PROP_USERDATA, userData);
				user.put(PROP_ACTIVE, true);
				setModified(sClientUID);
				return true;
			} catch (JSONException e) {
				log.error("Exception while setting updated user data", e);
			}
		}

		return false;
	}

	// -------------------------------------------------------------------------
	/**
	 * Add a user to the activeUsers map
	 *
	 * @param userId          ID of the user
	 * @param userDisplayName The UI name of the new user
	 * @param guest           Specifies, if the user is a guest or not
	 * @param userData        JSON Object containing user data, e.g.
	 *                        {userName:'Rocky Balboa', selection:{...}, ...}
	 * @throws JSONException
	 */
	public void addClient(RT2CliendUidType sClientUID, String userDisplayName, int id, boolean guest,
			final JSONObject userData) throws Exception {
		final JSONObject aClientState = createInitialClientStatus(sClientUID);

		try {
			aClientState.put(PROP_CLIENTUID, sClientUID);
			aClientState.put(PROP_USER_DISPLAY_NAME, userDisplayName);
			aClientState.put(PROP_GUEST, guest);
			aClientState.put(PROP_USERDATA, userData);
			aClientState.put(PROP_ID, id);
		} catch (JSONException e) {
			log.error("Exception caught while adding new active user", e);
		}

		// enable derived classes to add their specific properties
		mergeInitalClientStatus(aClientState, userDisplayName, id, guest, userData);

		m_aClientsMap.put(sClientUID, aClientState);
		m_aAdded.add(sClientUID);
	}

	// -------------------------------------------------------------------------
	/**
	 * Remove a specified user from the activeUsers map
	 *
	 * @param userId ID from the user to be removed
	 * @throws JSONException
	 */
	public boolean removeClient(RT2CliendUidType sClientUID) {
		final JSONObject aClientState = m_aClientsMap.remove(sClientUID);
		if (null != aClientState) {
			m_aRemoved.add(sClientUID);
			return true;
		}
		return false;
	}

	// -------------------------------------------------------------------------
	@Override
	public ClientsStatus clone() throws CloneNotSupportedException {
		try {
			return new ClientsStatus(this);
		} catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
			log.error("Cannot create clone due to exception", e);
			throw new CloneNotSupportedException("Cannot create clone");
		}
	}

	// -------------------------------------------------------------------------
	public JSONObject toJSON() {
		final JSONObject ret = new JSONObject();
		final JSONArray aModifiedList = new JSONArray();
		final JSONArray aAddedList = new JSONArray();
		final JSONArray aRemovedList = new JSONArray();

		try {
			// prevent to add client data more than once in different
			// notification buckets
			final Set<RT2CliendUidType> aProcessedIDs = new HashSet<>();

			for (final RT2CliendUidType id : m_aRemoved) {
				aRemovedList.put(id);
				aProcessedIDs.add(id);
			}

			for (final RT2CliendUidType id : m_aAdded) {
				final JSONObject aObj = m_aClientsMap.get(id);
				if ((null != aObj) && !aProcessedIDs.contains(id)) {
					aAddedList.put(new JSONObject(aObj.toString()));
					aProcessedIDs.add(id);
				}
			}

			for (final RT2CliendUidType id : m_aModified) {
				final JSONObject aObj = m_aClientsMap.get(id);
				if ((null != aObj) && !aProcessedIDs.contains(id))
					aModifiedList.put(new JSONObject(aObj.toString()));
			}

			if (!aModifiedList.isEmpty())
				ret.put(PROP_UPDATECLIENTS_CHANGED, aModifiedList);
			if (!aAddedList.isEmpty())
				ret.put(PROP_UPDATECLIENTS_JOINED, aAddedList);
			if (!aRemovedList.isEmpty())
				ret.put(PROP_UPDATECLIENTS_LEFT, aRemovedList);
		} catch (JSONException e) {
			log.error("Exception while creating JSON object of the current ClientStatus", e);
		}

		return ret;
	}

	// -------------------------------------------------------------------------
	public JSONObject toFullJSON() {
		final JSONObject ret = new JSONObject();
		final JSONArray aClientsArray = new JSONArray();

		try {
			for (final JSONObject o : m_aClientsMap.values())
				aClientsArray.put(o);

			ret.put(PROP_UPDATECLIENTS_LIST, aClientsArray);
		} catch (JSONException e) {
			log.error("Exception while creating JSON object of the current LoadableDocumentStatus", e);
		}

		return ret;
	}

	// -------------------------------------------------------------------------
	public void clearModifications() {
		m_aModified.clear();
		m_aAdded.clear();
		m_aRemoved.clear();
	}

	// -------------------------------------------------------------------------
	protected JSONObject getUserData(RT2CliendUidType clientId) {
		return m_aClientsMap.get(clientId);
	}

	// -------------------------------------------------------------------------
	protected void setModified(final RT2CliendUidType sClientUID) {
		m_aModified.add(sClientUID);
	}

	// -------------------------------------------------------------------------
	protected Map<RT2CliendUidType, JSONObject> getClientsMap() {
		return m_aClientsMap;
	}

	// -------------------------------------------------------------------------
	protected JSONObject createInitialClientStatus(RT2CliendUidType sClientUID) throws Exception {
		final JSONObject aInitClientState = new JSONObject();
		try {
			aInitClientState.put(PROP_CLIENTUID, sClientUID.getValue());
			aInitClientState.put(PROP_ACTIVE, true);
			aInitClientState.put(PROP_DURATION_OF_INACTIVITY, 0L);
		} catch (JSONException e) {
			log.error("Exception caught initializing new client instance", e);
		}

		return aInitClientState;
	}

	// -------------------------------------------------------------------------
	protected void changeAll(ClientsStatusModifier aModifier) throws Exception {
		for (final JSONObject o : m_aClientsMap.values()) {
			if (aModifier.modify(o))
				setModified(new RT2CliendUidType(o.getString(PROP_CLIENTUID)));
		}
	}

	// -------------------------------------------------------------------------
	protected void each(ClientsStatusObserver aObserver) throws Exception {
		for (final JSONObject o : m_aClientsMap.values())
			aObserver.observe(o);
	}

	// -------------------------------------------------------------------------
	protected void mergeInitalClientStatus(final JSONObject aClientData, String userDisplayName, int id, boolean guest,
			final JSONObject userData) throws Exception {
		// nothing to do - can be overwritten by derived classes to add specific
		// properties
	}

	// -------------------------------------------------------------------------
	private Map<RT2CliendUidType, JSONObject> createDeepCloneOfClientsMap(
			final Map<RT2CliendUidType, JSONObject> aClientsMap) throws JSONException {
		Validate.notNull(aClientsMap, "Clients map must NOT be null!");

		final Map<RT2CliendUidType, JSONObject> aMap = new HashMap<>();
		for (final Map.Entry<RT2CliendUidType, JSONObject> o : aClientsMap.entrySet())
			aMap.put(o.getKey(), new JSONObject(o.getValue().toString()));

		return aMap;
	}
}
