/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.text.TextStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.logging.IMessagesLoggable;
import com.openexchange.office.rt2.core.logging.IMessagesObjectManager;
import com.openexchange.office.rt2.core.logging.MessagesLogger;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.common.weakref.WeakRefUtils;
import com.openexchange.office.tools.service.logging.MDCEntries;

/**
 * Singleton to store all references to known RT2DocProcessor instances. The
 * references are held weak. This class should not be created, but retrieved
 * via services registry.
 *
 * @author Carsten Driesner
 * @since 7.10.0
 *
 */
@Service
@ShutdownOrder(value = -7)
@RegisteredService
public class RT2DocProcessorManager implements IDocNotificationHandler, IDocProcessorContainer, IMessagesObjectManager, IClientEventListener, DisposableBean, InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(RT2DocProcessorManager.class);

    //-------------------------------------------------------------------------
    private Map<RT2DocUidType, RT2DocProcessor> m_aKnownDocProcessorInstances = new HashMap<>();

    @Autowired
    private RT2MessageSender messageSender;

    @Autowired
    private DocProcessorEventService docProcessorEventService;

    @Autowired
    private DocumentDisposer documentsDisposer;

    @Autowired
    private DocProcessorMessageLoggerService rt2MessageLoggerService;

    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;

    @Autowired
    private RT2NodeInfoService nodeInfo;

    @Autowired
    private ClusterLockService clusterLockService;

    private final ScheduledExecutorService logExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder("RT2DocProcessorManagerRegistryLogger-%d").build());

    private final AtomicLong numOfClashIncidents = new AtomicLong(0);

    //-------------------------------------------------------------------------
    @Override
    public void afterPropertiesSet() throws Exception {
        logExecutor.scheduleAtFixedRate(new MessagesLogger(this, log), 3, 3, TimeUnit.MINUTES);
    }

    //-------------------------------------------------------------------------
    @Override
    public void destroy() throws Exception {
        logExecutor.shutdown();
        log.debug("RT2DocProcessorManager.destroy");

        Set<RT2DocUidType> keys = null;
        synchronized (m_aKnownDocProcessorInstances) {
            keys = new HashSet<>(m_aKnownDocProcessorInstances.keySet());
            m_aKnownDocProcessorInstances.clear();
        }

        keys.stream().forEach(d -> {
            disposeDocProcessor(d);
        });
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean isEmpty() {
        synchronized (m_aKnownDocProcessorInstances) {
            return m_aKnownDocProcessorInstances.isEmpty();
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public int size() {
        synchronized (m_aKnownDocProcessorInstances) {
            return m_aKnownDocProcessorInstances.size();
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean contains(RT2DocUidType rt2DocUid) {
        synchronized (m_aKnownDocProcessorInstances) {
            return m_aKnownDocProcessorInstances.containsKey(rt2DocUid);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean contains(final RT2DocProcessor aDocProcessor) {
        synchronized (m_aKnownDocProcessorInstances) {
            return m_aKnownDocProcessorInstances.containsKey(aDocProcessor.getDocUID());
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public RT2DocProcessor getDocProcessor(RT2DocUidType rt2DocUid) {
        synchronized (m_aKnownDocProcessorInstances) {
            return m_aKnownDocProcessorInstances.get(rt2DocUid);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<WeakReference<RT2DocProcessor>> getWeakReferenceToDocProcessors() {
        final Set<WeakReference<RT2DocProcessor>> aResult = new HashSet<>();
        synchronized (m_aKnownDocProcessorInstances) {
            m_aKnownDocProcessorInstances.values().forEach(proc -> aResult.add(new WeakReference<RT2DocProcessor>(proc)));
        }
        return aResult;
    }

    //-------------------------------------------------------------------------
    public Set<RT2DocProcessor> getDocProcessors() {
        final Set<RT2DocProcessor> aResult = new HashSet<>();
        synchronized (m_aKnownDocProcessorInstances) {
            m_aKnownDocProcessorInstances.values().forEach(proc -> aResult.add(proc));
        }
        return aResult;
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorCreated(final RT2DocProcessor aCreatedInstance) {
        RT2DocProcessor existingInstance = null;

        // DOCS-4622 ATTENTION: We HAVE to make sure that we notify DocCreated BEFORE
        // we add the created doc processor to the doc processor list. Otherwise this
        // will trigger DOCS-4622 again. The order of the following statements must
        // be ensured to prevent timing issues.
        documentsDisposer.addDocumentRef(aCreatedInstance);
        docProcessorEventService.notifyDocCreated(aCreatedInstance.getDocUID());

        synchronized (m_aKnownDocProcessorInstances) {
            existingInstance = m_aKnownDocProcessorInstances.put(aCreatedInstance.getDocUID(), aCreatedInstance);
        }

        if (existingInstance != null) {
            log.warn("Added newly created doc processor instance for com.openexchange.rt2.document.uid {} although there already exists one!", aCreatedInstance.getDocUID());
            numOfClashIncidents.incrementAndGet();
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorDisposed(final RT2DocProcessor aDisposedInstance) {
        if (aDisposedInstance != null) {
            final RT2DocUidType sDocUID = aDisposedInstance.getDocUID();

            synchronized (m_aKnownDocProcessorInstances) {
                if (m_aKnownDocProcessorInstances.containsKey(sDocUID)) {
                    log.debug("Removing docProcessor with com.openexchange.rt2.document.uid ''{}''", sDocUID);
                    m_aKnownDocProcessorInstances.remove(sDocUID);
                    documentsDisposer.removeDocumentRef(sDocUID.getValue());
                    MDCHelper.safeMDCPut(MDCEntries.DOC_UID, aDisposedInstance.getDocUID().getValue());
                    try {
                        List<String> logMsgs = rt2MessageLoggerService.formatMsgsLogInfo(aDisposedInstance.getDocUID());
                        if (!logMsgs.isEmpty()) {
                            TextStringBuilder builder = new TextStringBuilder(logMsgs.get(0));
                            logMsgs.remove(0);
                            logMsgs.forEach(s -> builder.appendln("    " + s));
                            log.info("Messages of disposed docprocessor with com.openexchange.rt2.document.uid {}: {}", aDisposedInstance.getDocUID(), builder);
                        } else {
                            log.info("Messages of disposed docprocessor with com.openexchange.rt2.document.uid {}", aDisposedInstance.getDocUID());
                        }
                    } finally {
                        MDC.remove(MDCEntries.DOC_UID);
                    }
                    docProcessorEventService.notifyDocDisposed(aDisposedInstance.getDocUID());
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    public void preShutdown() {
        final Set<WeakReference<RT2DocProcessor>> knownDocProcessors = getWeakReferenceToDocProcessors();
        final Iterator<WeakReference<RT2DocProcessor>> iter = knownDocProcessors.iterator();

        while (iter.hasNext()) {
            RT2DocUidType docUID = null;

            try {
                final RT2DocProcessor docProcessor = WeakRefUtils.getHardRef(iter.next());
                if (docProcessor != null) {
                    docUID = docProcessor.getDocUID();
                    final RT2Message shutdownBroadcastMsg = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_SHUTDOWN, docUID);
                    shutdownBroadcastMsg.setDocUID(docUID);
                    messageSender.broadcastMessageExceptClient(null, shutdownBroadcastMsg, RT2MessageType.BROADCAST_SHUTDOWN);
                }
            } catch (Exception e) {
                String sDocUid = (docUID == null) ? "null" : docUID.getValue();
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, sDocUid);
                log.error("Exception caught trying to send clients of com.openexchange.rt2.document.uid {} a BROADCAST_SHUTDOWN message", sDocUid, e);
                MDC.remove(MDCEntries.DOC_UID);
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public Collection<IMessagesLoggable> getManagedCollection() {
        return new HashSet<>(m_aKnownDocProcessorInstances.values());
    }

    //-------------------------------------------------------------------------
    public long getNumOfConflictingCreations() {
        return this.numOfClashIncidents.get();
    }

    //-------------------------------------------------------------------------
    public Set<RT2DocProcessor> getDocProcessorsAssociatedToSessionId(RT2SessionIdType sessionId) {
        Set<RT2DocProcessor> res = new HashSet<>();
        for (RT2DocProcessor docProcessor : m_aKnownDocProcessorInstances.values()) {
            if (docProcessor.getSessionIdToClientUids().containsKey(sessionId)) {
                res.add(docProcessor);
            }
        }
        return res;
    }


    //-------------------------------------------------------------------------
    private void disposeDocProcessor(RT2DocUidType docUid) {
        final ClusterLock clusterLock = clusterLockService.getLock(docUid);
        boolean locked = false;
        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
        try {
            locked = clusterLock.lock();
            if (locked) {
                log.debug("tryLock for com.openexchange.rt2.document.uid {}", docUid);
                disposeAndFreeDocProcessorResources(docUid);
            } else {
                log.warn("Lock couldn't acquired in for com.openexchange.rt2.document.uid {}", docUid);
            }
        } catch (Throwable ex) {
            ExceptionUtils.handleThrowable(ex);
            log.error(ex.getMessage(), ex);
        } finally {
            if (locked) {
                clusterLock.unlock();
            }
            MDC.remove(MDCEntries.DOC_UID);
        }
    }

	@Override
	public void clientAdded(ClientEventData clientEventData) {
		// Nothing to do
	}

	@Override
	public void clientRemoved(ClientEventData clientEventData) {
        final ClusterLock clusterLock = clusterLockService.getLock(clientEventData.getDocUid());
        boolean locked = false;
        final RT2DocUidType docUid = clientEventData.getDocUid();
        final RT2CliendUidType clientUid = clientEventData.getClientUid();
        MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
        MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUid.getValue());
        try {
            log.debug("clientRemoved acquiring clusterLock");
        	locked = clusterLock.lock();
        	log.debug("clientRemoved acquired clusterLock");
        	if (locked) {
		        Set<RT2CliendUidType> remainingClients = distributedDocInfoMap.removeClient(docUid, clientUid);
		        if ((remainingClients == null) || remainingClients.isEmpty()) {
		        	log.debug("Last leave for com.openexchange.rt2.document.uid {}", clientEventData.getDocUid());
		        	disposeAndFreeDocProcessorResources(clientEventData.getDocUid());
		        }
        	} else {
                log.warn("Lock couldn't acquired for com.openexchange.rt2.client.uid [{}] and com.openexchange.rt2.document.uid [{}]", clientUid, docUid);
        	}
        } catch (Throwable ex) {
            ExceptionUtils.handleThrowable(ex);
            log.error(ex.getMessage(), ex);
        }
        finally {
            if (locked) {
                log.debug("clientRemoved releasing clusterLock");
                clusterLock.unlock();
                log.debug("clientRemoved released clusterLock");
            }
            MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
        }
	}

	//-------------------------------------------------------------------------
    private void disposeAndFreeDocProcessorResources(RT2DocUidType docUid) {
        final RT2DocProcessor docProcessor = getDocProcessor(docUid);
        if (docProcessor != null) {
            docProcessor.dispose();
        }
        nodeInfo.deregisterDocOnNode(docUid);
        distributedDocInfoMap.freeDocInfos(docUid);
    }
}
