/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.json.JSONObject;

import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.tools.doc.ApplicationType;


public class RecentFileListHelper
{

   /**
    * Provides the recent file list for the provided application type and
    * checks/does a is migration if needed.
    *
    * @param recentFileListManager
    *  The recent file list manager instance to be used to retrieve the
    *  recent file list.
    *
    * @param app
    *  Selects for what application the recent file list should be retrieved.
    *
    * @return
    */
   public static List<JSONObject> getRecentFileList(RecentFileListManager recentFileListManager, ApplicationType app) {
       List<JSONObject> recentFileList = recentFileListManager.readRecentFileList(app);
       if (null == recentFileList) {
           // migrate old list for app on demand
           recentFileListManager.migrateOldRecentFileList(app);
           recentFileList = recentFileListManager.readRecentFileList(app);
       }
       return recentFileList;
   }

   /**
    * Adds a file to the recent file - the order will be changed, if the file
    * is already part of the recent file list.
    *
    * @param recentFileListManager
    *  A recent file list manager instance.
    *
    * @param app
    *  The application type that specifies which recent file list should
    *  be updated.
    *
    * @return
    *  TRUE if the file has been added/set to the top, otherwise FALSE.
    */
   public static boolean addToRecentFileList(final RecentFileListManager recentFileListManager, ApplicationType app, final JSONObject addToFile, final Date lastSaveTime) {
       if (null != recentFileListManager) {
           List<JSONObject> recentFileList = RecentFileListHelper.getRecentFileList(recentFileListManager, app);
           if (null != recentFileList) {
               recentFileListManager.addCurrentFile(recentFileList, addToFile, lastSaveTime);
               recentFileListManager.writeRecentFileList(app, recentFileList);
               recentFileListManager.flush();
               return true;
           }
       }
       return false;
   }

   /**
    * Updates the file of the recent file list. If the file is not part of the
    * recent file list, nothing will happen.
    *
    * @param recentFileListManager
    *  A recent file list manager instance.
    *
    * @param app
    *  The application type that specifies which recent file list should
    *  be updated.
    *
    * @param oldFile
    * @param newFile
    * @param lastSaveTime
    *
    * @return
    *  TRUE if the file has been updated, otherwise FALSE.
    */
   public static boolean updateFileInRecentFileList(final RecentFileListManager recentFileListManager, ApplicationType app, final String oldFileId, final JSONObject newFile, final Date lastSaveTime) {
       Validate.notNull(newFile, "The new file descriptor must always be provided and cannot be null!");

       if (null != recentFileListManager) {
           List<JSONObject> recentFileList = RecentFileListHelper.getRecentFileList(recentFileListManager, app);
           if (null != recentFileList) {
               recentFileListManager.updateFile(recentFileList, oldFileId, newFile, lastSaveTime);
               recentFileListManager.writeRecentFileList(app, recentFileList);
               recentFileListManager.flush();
               return true;
           }
       }
       return false;
   }

   /**
    * Removes a file from the recent file list and store it.
    *
    * @param recentFileListManager
    *  A recent file list manager instance.
    *
    * @param app
    *  The application type that specifies which recent file list should
    *  be updated.
    *
    * @param folderId
    *  The folder id of the file to be removed.
    *
    * @param fileId
    *  The file id of the file to be removed.
    *
    * @return
    *  TRUE if the file has been removed, otherwise FALSE.
    */
   public static boolean removeFromRecentFileList(RecentFileListManager recentFileListManager, ApplicationType app, String folderId, String fileId) {
       boolean result = false;

       List<JSONObject> recentFileList = getRecentFileList(recentFileListManager, app);
       if (null != recentFileList) {
           JSONObject removeFileDescriptor = recentFileListManager.createFileDescriptor(folderId, fileId);
           if (null != removeFileDescriptor) {
               boolean changed = recentFileListManager.removeFileFromList(recentFileList, removeFileDescriptor);
               if (changed) {
                   recentFileListManager.writeRecentFileList(app, recentFileList);
                   recentFileListManager.flush();
                   result = true;
               }
           }
       }

       return result;
   }
}
