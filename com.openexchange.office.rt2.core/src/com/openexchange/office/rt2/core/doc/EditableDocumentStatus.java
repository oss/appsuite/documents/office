/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;


/**
 * {@link EditableDocumentStatus}
 *
 * The DocumentStatus class stores important state information about a
 * document processor state. The content is used to be sent to connected clients
 * to update their internal state. The state includes the editor name and
 * id, the current server osn, the number of clients, the current selection
 * of every client etc.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class EditableDocumentStatus extends LoadableDocumentStatus {
    private static final Logger log = LoggerFactory.getLogger(EditableDocumentStatus.class);

    // - Members ---------------------------------------------------------------
    private String m_currentEditingUserName = EMPTY_USERNAME;
    private RT2CliendUidType m_currentEditingClientId = EMPTY_CLIENTID;
    private boolean m_failSafeSaveDone = false;

    // - Implementation --------------------------------------------------------

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link ConnectionStatus}.
     */
    public EditableDocumentStatus() {
        super();
    }

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link ConnectionStatus}.
     */
    public EditableDocumentStatus(EditableDocumentStatus aEditableDocStatus)
        throws Exception {
        super(aEditableDocStatus);

        m_currentEditingUserName = aEditableDocStatus.m_currentEditingUserName;
        m_currentEditingClientId = aEditableDocStatus.m_currentEditingClientId;
        m_failSafeSaveDone = aEditableDocStatus.m_failSafeSaveDone;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides information that a fail safe
     * save operation was done successfully.
     *
     * @return
     *  TRUE if there recently was a successful
     *  fail safe save operation, otherwise FALSE.
     */
    public boolean isFailSafeSaveDone() {
        return m_failSafeSaveDone;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the state of a fail safe save
     * operation.
     *
     * @param saveDone The state of the fail safe save
     */
    void setFailSafeSaveDone(boolean saveDone) {
        this.m_failSafeSaveDone = saveDone;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    public void setCurrentEditingUser(RT2CliendUidType currentEditingUserId, String currentEditingUserName) {
        m_currentEditingClientId = currentEditingUserId;
        m_currentEditingUserName = currentEditingUserName;
        setModified(true);
    }

    //-------------------------------------------------------------------------
    /**
     * Gets the m_currentEditingUserName
     *
     * @return The m_currentEditingUserName
     */
    public RT2CliendUidType getCurrentEditingUserId() {
        return m_currentEditingClientId;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets the m_currentEditingUserName
     *
     * @param currentEditingUserName The currentEditingUserName to set
     */
    public String getCurrentEditingUserName() {
        return m_currentEditingUserName;
    }

    //-------------------------------------------------------------------------
    public boolean isEditor(String sUserId) {
        return (StringUtils.isNotEmpty(sUserId))
        ? (sUserId.equals(m_currentEditingClientId.getValue())) : false;
    }

    //-------------------------------------------------------------------------
    public boolean isEditor(RT2CliendUidType userId) {
        return (userId != null)
        ? (userId.getValue().equals(m_currentEditingClientId.getValue())) : false;
    }

    //-------------------------------------------------------------------------
    /**
     * Creates a shallow clone of this connection status
     * object.
     *
     * @return
     *  A shallow clone.
     */
    @Override
    public EditableDocumentStatus clone() throws CloneNotSupportedException {
        try {
            final EditableDocumentStatus clone = new EditableDocumentStatus(this);

            return clone;
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            log.error("Exception while cloning EditableDocumentStatus", e);
            throw new CloneNotSupportedException(
                "Exception while cloning EditableDocumentStatus");
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Creates the JSON representation of the current connection status
     * object. The JSON object should only provide properties that are
     * part of the client/server communication. Internal properties should
     * be omitted.
     *
     * @return
     *  A JSON object that contains the properties of the current connection
     *  status. The JSON object can be empty in case of an error.
     */
    @Override
    public JSONObject toJSON() {
        final JSONObject ret = super.toJSON();

        try {
            ret.put(DocStatusProperties.KEY_EDIT_USERID,
                m_currentEditingClientId);
            ret.put(DocStatusProperties.KEY_EDIT_USER, m_currentEditingUserName);
            ret.put(DocStatusProperties.KEY_FAILSAFESAVEDONE, m_failSafeSaveDone);
        } catch (JSONException e) {
            log.error("Exception while creating JSON object of the current connection status",
                e);
        }

        return ret;
    }
}
