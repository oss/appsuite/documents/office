/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;

/**
 * Interface for the background save process to store modified document instances
 * in a common way.
 *
 * {@link IBackgroundSavable}
 *
 * @author <a href="mailto:carsteb.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.8.0
 */
public interface IBackgroundSavable extends IDocState
{
    /**
     * Retrieve unique document id.
     */
    RT2DocUidType getUniqueID();

    /**
     * Provide the modification state of the document instance.
     * @return TRUE if modified else FALSE
     */
    boolean isModified();

    /**
     * Retrieve the number of operations queued that need to be
     * persistently stored.
     *
     * @return number of pending operations.
     */
    int getPendingOperationsCount();

    /**
     * Determines if a saving process is in progress.
     * @return TRUE if save is in progress, otherwise FALSE
     */
    boolean isSaveInProgress();

    /**
     * Provides the number of milliseconds since January 1, 1970,
     * 00:00:00 GMT when the last save process has been completed
     * successfully. A newly created document instance sets this
     * to the creation time.
     */
    long getLastSaveTimeStamp();

    /**
     * Retrieves the last save error code.
     * @return the error code of the last save. Can be NO_ERROR
     * in case no error happened.
     */
    ErrorCode getLastSaveErrorCode();

    /**
     * Sets the last save error code in case the external background
     * save processor detected an error.
     *
     * @param lastSaveErrorCode the error code to be set due to an
     * detected error while trying to process the last save operation.
     */
    void setLastSaveErrorCode(ErrorCode lastSaveErrorCode);

    /**
     * Save the document instance persistently.
     *
     * @param reason the reason why the document should be saved.
     * @param force specifies if the documents needs to be saved although
     *  the background save algorithm would not.
     * @throws Exception
     */
    void save(BackgroundSaveReason reason, boolean force) throws Exception;

    /**
     * Sets the save process state.
     * @param bProcessState TRUE if the saving process was successful or FALSE
     *  if not. The background saving will try to store the document as soon as
     *  possible if bProcessState is set to FALSE.
     */
    void setProcessedState(boolean bProcessState);

    /**
     * Determines if the document instance has been process by the latest
     * background save processing.
     * @return TRUE if the document has been processed or FALSE if not.
     */
    boolean isProcessed();

    /**
     * Determines if "Operation Transformation" (OT) is enabled for the
     * document instance or not.
     * @return TRUE if OT is abled or FALSE if not.
     */
    boolean isOTEnabled();

    /**
     * Determines if the document has an advisory lock set or not.
     * @return TRUE if the advisory lock has been set for the instance
     * and FALSE if not.
     */
    boolean hasSetAdvisoryLock();
}
