/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class PresenterClientsStatus extends ClientsStatus
{
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
	public PresenterClientsStatus()
	{
	}

	//-------------------------------------------------------------------------
	public PresenterClientsStatus(final PresenterClientsStatus aPresenterClientsStatus) throws Exception
	{
		super(aPresenterClientsStatus);
	}

	//-------------------------------------------------------------------------
    /**
     * Set the participant state of the user.
     *
     * @param userId
     *  ID from the user to set the state for
     * @param state
     *  true if the user joined, false if the user left the presentation
     * @return
     *  true if the user was previously present in the list, otherwise false.
     * @throws JSONException
     */
    public boolean setParticipantState(RT2CliendUidType sClientUID, boolean state) throws Exception
    {
        final JSONObject aUserData = getUserData(sClientUID);

        if (null != aUserData)
        {
            aUserData.put("joined", state);
            setModified(sClientUID);
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------
    /**
     * Disjoin all users.
     *
     * @throws Exception
     */
    public void disjoinAll() throws Exception
    {
        changeAll(new GenericClientStatusModifier<>("joined", Boolean.class, Boolean.FALSE));
    }

    //-------------------------------------------------------------------------
    public Set<RT2CliendUidType> getParticipants() throws Exception
    {
    	final Set<RT2CliendUidType> aIds = new HashSet<>();
    	final GenericClientsStatusFilter aParticipantsFilter = new GenericClientsStatusFilter("joined", Boolean.TRUE);
    	each(aParticipantsFilter);
    	for (final JSONObject o : aParticipantsFilter.getMatchedEntries())
    		aIds.add(new RT2CliendUidType(o.getString(ClientsStatus.PROP_CLIENTUID)));
    	return aIds;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the number of participants.
     *
     * @return
     *  The number of participants.
     */
    public int getParticipantsCount() throws Exception
    {
        final GenericClientStatusCounter aCounter = new GenericClientStatusCounter("joined", Boolean.TRUE);
        each(aCounter);
        return aCounter.getCount();
    }

    //-------------------------------------------------------------------------
    @Override
	public PresenterClientsStatus clone() throws CloneNotSupportedException
	{
        try
        {
            return new PresenterClientsStatus(this);
        }
        catch (Throwable e)
        {
            ExceptionUtils.handleThrowable(e);
            throw new CloneNotSupportedException("Cannot create clone");
        }
	}

    //-------------------------------------------------------------------------
    @Override
    protected void mergeInitalClientStatus(final JSONObject aClientData, String userDisplayName, int id, boolean guest, final JSONObject userData) throws Exception
    {
        aClientData.put("joined", false);
    }

}
