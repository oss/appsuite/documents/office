/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.api.OXDocumentInfo;
import com.openexchange.office.document.api.RenameResult;
import com.openexchange.office.document.tools.ChunkableDocLoader;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.core.FilterExceptionToErrorCode;
import com.openexchange.office.htmldoc.DocMetaData;
import com.openexchange.office.htmldoc.GenericHtmlDocumentBuilder;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.exception.LoadDocumentAbortedByClientException;
import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.system.SystemInfoHelper;
import com.openexchange.office.tools.common.user.AuthorizationCache;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.FileDescriptor;
import com.openexchange.office.tools.doc.OXDocumentException;
import com.openexchange.office.tools.doc.SyncInfo;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.filename.FileNameTools;
import com.openexchange.tools.session.ServerSession;


public abstract class LoadableDocProcessor extends DocProcessor {
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(LoadableDocProcessor.class);

	// -------------------------------------------------------------------------
	protected static final int WAIT_FOR_LOADSAVE_LOCK = 5000;

	private static final int                CHUNK_OPS_COUNT   = 50;

    private ApplicationType                 m_aAppType        = ApplicationType.APP_NONE;

    private AtomicReference<DocumentFormat> m_aDocFormat      = new AtomicReference<>(DocumentFormat.NONE);

    private boolean                         m_bEncrypted      = false;

	private IResourceManager                m_aResManager     = null;

    private boolean                         m_debugOperations = false;

    private boolean                         m_isOTEnabled     = false;
    //-----------------------------Services------------------------------------

    @Autowired
    protected IResourceManagerFactory resourceManagerFactory;

    @Autowired
    protected RT2ConfigService rt2ConfigService;

    @Autowired
    protected StorageHelperServiceFactory storageHelperServiceFactory;

    @Autowired
    protected DocFileServiceFactory docFileServiceFactory;

    @Autowired
    protected RecentFileListManagerFactory recentFileListManagerFactory;

    @Autowired
    protected OXDocumentFactory oxDocumentFactory;

    @Autowired
    protected ImExporterAccessFactory imExporterAccessFactory;

    @Autowired
    protected GenericHtmlDocumentBuilder genericHtmlDocumentBuilder;

    @Autowired(required = false)
    protected CryptographicServiceAuthenticationFactory cryptographicServiceAuthenticationFactory;

    @Autowired(required = false)
    protected IDocumentConverter documentConverter;

    //-------------------------------------------------------------------------
	public LoadableDocProcessor(ApplicationType appType) throws Exception {
		super();

		setApplicationType(appType);
	}

    //-------------------------------------------------------------------------
	@Override
	public void init() {
		super.init();
		final String stringAppType = getApplicationType().toString();

		m_debugOperations = configurationService.getBoolProperty("io.ox/office//module/debugoperations", false);
		m_isOTEnabled = rt2ConfigService.isOTEnabled(stringAppType);
	}

    //-------------------------------------------------------------------------
	@Override
	public synchronized void setDocUID (final RT2DocUidType docUID) {
		super.setDocUID(docUID);
		m_aResManager = resourceManagerFactory.createInstance(this.getDocUID().getValue());
	}

    //-------------------------------------------------------------------------
	protected ApplicationType getApplicationType() {
		return m_aAppType;
	}

    //-------------------------------------------------------------------------
	protected void setApplicationType(ApplicationType aAppType) {
		m_aAppType = aAppType;
	}

    //-------------------------------------------------------------------------
	protected DocumentType getDocumentType() {
		return DocumentType.fromApplicationType(m_aAppType);
	}

    //-------------------------------------------------------------------------
	protected DocumentFormat getDocumentFormat() {
		return m_aDocFormat.get();
	}

    //-------------------------------------------------------------------------
	protected void setDocumentFormat(DocumentFormat aDocFormat) {
		m_aDocFormat.set(aDocFormat);
	}

    //-------------------------------------------------------------------------
	protected IResourceManager getResourceManager() {
		return m_aResManager;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isOTEnabled() {
		return m_isOTEnabled;
	}

    //-------------------------------------------------------------------------
	public boolean isEncrypted() {
		return m_bEncrypted;
	}

    //-------------------------------------------------------------------------
	protected boolean isDebugOperations() {
		return m_debugOperations;
	}

    //-------------------------------------------------------------------------
	// message processing
    //-------------------------------------------------------------------------

	protected RT2Message createJoinResponse(RT2Message joinRequest) throws Exception {
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(joinRequest, RT2MessageType.RESPONSE_JOIN);
        return aResponse;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean onJoin(RT2Message request) throws Exception {
	    log.info("Loadable DocProcessor.onJoin for doc-uid {}", this.getDocUID().getValue());
		super.onJoin(request);

		log.info("Loadable DocProcessor send join_response via RT2MessageSender");
		final RT2Message aResponse = createJoinResponse(request);
        messageSender.sendMessageWOSeqNrTo (request.getClientUID(), aResponse);
        return true;
	}

    // -------------------------------------------------------------------------
    @Override
    public boolean onOpenDoc(final RT2Message aMsg) throws Exception {
        if (m_aProcState.compareAndSet(ProcessingState.CREATED, ProcessingState.INITIALIZING)) {
            boolean res = onFirstOpenDoc(aMsg);
            //  make sure to set processing state in all cases
            m_aProcState.set(ProcessingState.OPEN);
            return res;
        } else if (m_aProcState.get() == ProcessingState.OPEN) {
            boolean res = onAdditionalOpenDoc(aMsg);
            return res;
        } else if (m_aProcState.get() == ProcessingState.DISPOSED) {
            throw new OXDocumentException("Document com.openexchange.rt2.document.uid " + this.getDocUID() + " is already disposed!", ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR);
        } else {
            log.info("Race detected on open-doc request for com.openexchange.rt2.document.uid {} and com.openexchange.rt2.client.uid {}", getDocUID(), aMsg.getClientUID());
            throw new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean onCloseDoc(final RT2Message aRequest) throws Exception {
    	notifyDocumentEventForStatistic(aRequest);

    	final RT2CliendUidType sClientUID = aRequest.getClientUID();
        final UserData      aUserData      = getUserData(sClientUID);

        final SyncInfo               aSyncInfo  = new SyncInfo(aUserData.getFolderId(), aUserData.getFileId());
        final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
        synchronized (this.getStatusSyncObject()) {
            aSyncInfo.updateSyncInfo(aDocStatus.getVersion(), aDocStatus.getDocumentOSN());
        }

        if (!RT2MessageType.REQUEST_CLOSE_AND_LEAVE.equals(aRequest.getType())) {
        	final RT2Message aCloseDocResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_CLOSE_DOC);
        	MessageHelper.setSyncInfo(aCloseDocResponse, aSyncInfo);
        	messageSender.sendMessageTo (sClientUID, aCloseDocResponse);
        }

        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean onEmergencyLeave(final RT2Message aRequest) throws Exception {
    	notifyDocumentEventForStatistic(aRequest);
        return super.onEmergencyLeave(aRequest);
    }

    //-------------------------------------------------------------------------
    private void notifyDocumentEventForStatistic(final RT2Message aRequest) {
        final RT2CliendUidType sClientUID     = aRequest.getClientUID();
        final ClientsStatus    aClientsStatus = getClientsStatusClone();

        // notify document event to update statistics
        if (aClientsStatus.getDurationOfInactivity(sClientUID) > 0)
            documentEventHelper.addTimeoutEvent(getDocumentType());
        else
            documentEventHelper.addCloseEvent(getDocumentType());
    }

    //-------------------------------------------------------------------------
    public boolean handleRename_Document(final RT2Message aRequest) throws Exception {
        final RT2CliendUidType sClientUID = aRequest.getClientUID();
        final UserData aUserData = getUserData(sClientUID);
        final ServerSession aSession = sessionService.getSession4Id(aUserData.getSessionId());
        final String sFolderID = aUserData.getFolderId();
        final String sFileID = aUserData.getFileId();
        final StorageHelperService aStoreHelper = storageHelperServiceFactory.create(aSession, sFolderID);
        final RT2Message aRenameResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_APP_ACTION);

        String sNewFileName = null;
        String sSanitizedFileName = null;
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        final boolean bMustReload = !aStoreHelper.supportsPersistentIDs();

        if (bMustReload)
        	errorCode = prepareRenameDocumentWithReload(sClientUID, aSession);

        if (errorCode.isNoError()) {
	        // Storage where the file resides supports persistent IDs. Therefore a rename is a safe operation.
	        final JSONObject aBody = aRequest.getBody();

	        sNewFileName = aBody.optString(ActionsConst.ACTION_RENAME_DOCUMENT_NEW_FILENAME);
	        final RenameResult aResult = docFileServiceFactory.createInstance().renameDocument(aSession, sNewFileName, sFolderID, sFileID);

	        errorCode = aResult.errorCode();

	        final DocumentStatus aDocState = getDocumentStatusClone();
	        RT2MessageGetSet.setError(aRenameResponse, errorCode);
	        MessageHelper.setDocumentStatus(aRenameResponse, aDocState);

	        if (errorCode.isNoError()) {
	            // add new file name to the response message if rename was successful
	            sNewFileName       = aResult.metaData().getFileName();
	            sSanitizedFileName = FileNameTools.sanitizeFilename(sNewFileName);

	            final JSONObject aRenameResultBody = aRenameResponse.getBody();
	            aRenameResultBody.put(MessageProperties.PROP_FILENAME, sNewFileName);
	            aRenameResultBody.put(MessageProperties.PROP_SANITIZEDFILENAME, sSanitizedFileName);

	            if (bMustReload) {
	            	errorCode =  ErrorCode.RENAMEDOCUMENT_SAVE_IN_PROGRESS_WARNING;
	            	RT2MessageGetSet.setError(aRenameResponse, errorCode);
	            }

	            // update the recent list, too
	            final RecentFileListManager recentFileListManager = recentFileListManagerFactory.create(aSession);
	            final JSONObject newFileDescriptor = FileDescriptor.createJSONObject(aResult.metaData(), null);
	            RecentFileListHelper.updateFileInRecentFileList(recentFileListManager, getApplicationType(), aUserData.getFileId(), newFileDescriptor, new Date());
	        }

			// always send result to the requesting client
	        messageSender.sendMessageTo (sClientUID, aRenameResponse);

			if (errorCode.isNoError()) {
				boolean    bBroadcastAll = false;
	            RT2Message aBroadcast    = null;

				if (bMustReload) {
					bBroadcastAll = true;
					aBroadcast    = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_RENAMED_RELOAD, sClientUID, getDocUID());

		            final JSONObject aBroadcastBody = aBroadcast.getBody();
		            aBroadcastBody.put(MessageProperties.PROP_FILENAME, sNewFileName);
		            aBroadcastBody.put(MessageProperties.PROP_SANITIZEDFILENAME, sSanitizedFileName);
		            aBroadcastBody.put(MessageProperties.PROP_FILEID, aResult.newFileId());
				} else {
					aBroadcast = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_UPDATE, sClientUID, getDocUID());
		            aBroadcast.setBody(aRenameResponse.getBody());
				}

	            MessageHelper.setDocumentStatus(aBroadcast, aDocState);

	            if (bBroadcastAll) {
	            	final Collection<RT2CliendUidType> aReceivers = Arrays.asList(sClientUID);
	            	messageSender.broadcastMessageTo(aReceivers, aBroadcast, aBroadcast.getType());

	                // this is a dirty workaround to prioritize the broadcast to the original
	            	// client and wait a little until all other clients are notified
	            	if (getUserCount() > 1)
	            	    Thread.sleep(300);
	            }
	            messageSender.broadcastMessageExceptClient (sClientUID, aBroadcast, aBroadcast.getType());
			}
        } else {
	        final DocumentStatus aDocState = getDocumentStatusClone();
	        RT2MessageGetSet.setError(aRenameResponse, errorCode);
	        MessageHelper.setDocumentStatus(aRenameResponse, aDocState);

			// always send result to the requesting client
	        messageSender.sendMessageTo (sClientUID, aRenameResponse);
        }

	    return true;
    }

    //-------------------------------------------------------------------------
    protected abstract ErrorCode prepareRenameDocumentWithReload(RT2CliendUidType sClientUID, final ServerSession aServerSession) throws Exception;

    //-------------------------------------------------------------------------
    @Override
    public void dispose() {
        setDisposed();

        final IResourceManager aResManager = m_aResManager;
        if (aResManager != null) {
            aResManager.lockResources(false);
            aResManager.dispose();
            m_aResManager = null;
        }

        super.dispose();
    }

    //-------------------------------------------------------------------------
    protected abstract boolean processOpenDocRequest(final RT2Message rReq, final Boolean bFastEmpty, final Boolean bNewDoc, final Boolean bFirstUser) throws Exception;

    //-------------------------------------------------------------------------
    /**
     * Checks and if not available sets the persistent document state to the
     * global DocumentDataManager (which manages the Hazelcast data).
     *
     * <br/>Throws exception if the consistency between the connection document state
     * and the document loaded is broken. This can happen if a client loaded the
     * document after the current version was changed due to uploading or set
     * current version in the OX Drive UI.
     *
     * @param oxDoc the OXDocument instance of a loaded document
     */
    protected ErrorCode checkAndUpdateDocumentState(final ServerSession aSession, final OXDocumentInfo oxDocInfo, boolean bFirstUser, boolean bFastEmpty) throws Exception {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        if (bFirstUser) {
            final boolean bForceROMode = oxDocInfo.isRescueDocument();

            // set the base version of the currently stored document
            final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
            synchronized (aDocStatus) {
                int nUniqueID = oxDocInfo.getUniqueDocumentId();
                aDocStatus.setForceReadOnly(bForceROMode);
                aDocStatus.setDocumentOSN(nUniqueID);
                aDocStatus.setVersion(oxDocInfo.getVersion());
                log.debug("Document osn set by first client: {}", nUniqueID);
            }
        } else {
            // Check consistency between the connection document state and the newly
            // loaded document
            final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
            if (!checkConsistencyOfLoadedDocument(aDocStatus.getVersion(), aDocStatus.getDocumentOSN(), aDocStatus.getOperationStateNumber(), oxDocInfo)) {
                errorCode = ErrorCode.LOADDOCUMENT_FILE_HAS_CHANGED_ERROR;
            }
        }

        return errorCode;
    }

    //-------------------------------------------------------------------------
	public ErrorCode loadDocument(final String sSessionId, final RT2Message aRequest, RT2CliendUidType clientUid, final Boolean bFastEmpty, final Boolean bNewDoc, final Boolean bFirstUser) throws Exception {
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        ErrorCode openErrorCode = ErrorCode.NO_ERROR;

        final ServerSession aSession = sessionService.getSession4Id(sSessionId);

        // Check, if we have a valid session. Include using the session id
        // which cannot be processed correctly in some cases, if the session storage hazelcast bundle
        // is not started!!
        if (null == aSession) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR.getCodeAsStringConstant());
            log.error("RT2: Session information not accessible - check cluster setup");
            MDC.remove(MDCEntries.ERROR);
            return ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR;
        }

        // Check if we can access the user data of the provided client uid
        final UserData aUserData = getUserData(clientUid);
        if (aUserData == null) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR.getCodeAsStringConstant());
            log.error("RT2: com.openexchange.rt2.client.uid {} is unknown - cannot load document", clientUid);
            MDC.remove(MDCEntries.ERROR);
            return ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR;
        }

        boolean locked = lockForLoadOrSaveDocumentWithTimeout(WAIT_FOR_LOADSAVE_LOCK);
        if (locked) {
            try {
                final IResourceManager aResManager = m_aResManager;
                final OXDocument aOXDocument = (bFastEmpty) ? null : oxDocumentFactory.createDocumentAccess(aSession, aUserData, bNewDoc, aResManager, false, null);
                final OXDocumentInfo aOXDocInfo = (aOXDocument != null) ? aOXDocument : oxDocumentFactory.createDocumentInfoAccess(aSession, aUserData.getFileId(), false);

                final JSONArray aAdditionalOperations = getAdditionalOperations();
                final DocumentFormat docFormat = aOXDocInfo.getDocumentFormat();

                locked = false;
                unlockForLoadOrSaveDocument();

                // initialize document format
                if (bFirstUser) {
                    setDocumentFormat(docFormat);
                }

                clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);

                openErrorCode = aOXDocInfo.getLastError();
                errorCode = openErrorCode;
                if (!errorCode.isError()) {
                    // check consistency and update/set document state for restore document
                    errorCode = checkAndUpdateDocumentState(aSession, aOXDocInfo, bFirstUser, bFastEmpty);
                    if (errorCode.isError()) {
                        // create document events to update statistics
                        documentEventHelper.addOpenErrorEvent(getDocumentType());

                        // in case of an inconsistency bail out with set error code
                        specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "Inconsistency detected between connection document state and document loaded!");
                        return errorCode;
                    }

                    if (canHandleLocalStorageLoading(aOXDocInfo, RT2MessageGetSet.getStorageVersion(aRequest), RT2MessageGetSet.getStorageOSN(aRequest), bFirstUser)) {
                        impl_addToRecentFileList(aOXDocInfo.getDocumentMetaData(), aSession);
                        impl_generateFastLocalStorageResponse(aSession, aOXDocInfo, aRequest, aAdditionalOperations);
                        m_aResManager.lockResources(true);
                        return errorCode;
                    }

                    // Initialize result object with operations from persistent document first.
                    // Check for FilterExceptions that can occur due to problems with the file.
                    try {
                        String sFastLoadHtmlDoc = null;

                        // "Fast empty" can be processed much faster - check if this load request
                        // has been requested by the first client - only then we can use the fast
                        // empty processing. Treating bFastEmpty == null as FALSE.
                        if (BooleanUtils.toBoolean(bFastEmpty)) {
                            // create document events to update statistics
                            documentEventHelper.addOpenEvent(getDocumentFormat(), aOXDocInfo.getDocumentMetaData());

                            // update recent file list on open file now
                            impl_addToRecentFileList(aOXDocInfo.getDocumentMetaData(), aSession);
                            impl_generateFastEmptyResponses(aSession, aOXDocInfo, aRequest, aAdditionalOperations);
                            m_aResManager.lockResources(true);
                            return errorCode;
                        }

                        // check that we don't reach this section without a valid OXDocument instance!
                        if (aOXDocument == null) {
                            // create document events to update statistics
                            documentEventHelper.addOpenErrorEvent(getDocumentType());

                            // impossible case - return general error
                            log.debug("OXDocument instance is null - this must never happen!");
                            return ErrorCode.GENERAL_UNKNOWN_ERROR;
                        }

                        final String sModule = ApplicationType.enumToString(m_aAppType);
                        final boolean useFastLoad = GenericHtmlDocumentBuilder.isFastLoadSupported(sModule) && genericHtmlDocumentBuilder.isFastLoadActive(sModule, aSession);

                        final IImporter importer = imExporterAccessFactory.getImporterAccess(docFormat);
                        final ChunkableDocLoader chunkDocLoader = new ChunkableDocLoader(aOXDocument, importer, useFastLoad);
                        chunkDocLoader.prepareLoad();

                        // Load the document in two parts
                        // We should change this in the future providing many chunks
                        // DOCS-3742: Ensure that getGlobalOperations & getActiveOperations are called
                        // before getPartsCount()/getActivePartIndex get called. The calls fill up the
                        // internal map of the ChunkDocLoader. Otherwise defaults are provided.
                        final JSONObject globalOpsObject  = chunkDocLoader.getGlobalOperations();
                        final JSONObject activeObsObject  = chunkDocLoader.getActiveOperations();
                        final int        nPartsCount      = chunkDocLoader.getPartsCount();
                        final int        nActivePartIndex = chunkDocLoader.getActivePartIndex();

                        clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);

                        JSONObject previewOpsObject   = null;
                        JSONObject remainingOpsObject = null;

                        // check for optional fast load

                        if (useFastLoad) {

                            final JSONObject documentOpsObject  = new JSONObject();
                            OperationHelper.appendJSON(documentOpsObject, globalOpsObject);
                            OperationHelper.appendJSON(documentOpsObject, activeObsObject);

                            // fast load needs all operations to create the DOM string
                            OperationHelper.appendJSON(documentOpsObject, chunkDocLoader.getRemainingOperations());
                            sFastLoadHtmlDoc = genericHtmlDocumentBuilder.buildHtmlDocument(sModule, documentOpsObject, new DocMetaData(aOXDocument.getMetaData()), aSession);

                            if(sFastLoadHtmlDoc==null) { // an error happened, try to load the document again without fastload
                                chunkDocLoader.setCreateFastLoadOperations(false);
                            }
                            else {
                                remainingOpsObject = documentOpsObject;
                            }
                        }

                        if(sFastLoadHtmlDoc==null) {

                            final JSONObject documentOpsObject  = new JSONObject();
                            OperationHelper.appendJSON(documentOpsObject, globalOpsObject);
                            OperationHelper.appendJSON(documentOpsObject, activeObsObject);

                            if (nPartsCount > 1) {
                                // multi-part - preview and remaining operations object
                                previewOpsObject   = documentOpsObject;
                                remainingOpsObject = new JSONObject();
                                OperationHelper.appendJSON(remainingOpsObject, chunkDocLoader.getRemainingOperations());
                            }
                            else {
                                // single-part - initialize only remaining operations object
                                remainingOpsObject = documentOpsObject;
                                OperationHelper.appendJSON(remainingOpsObject, chunkDocLoader.getRemainingOperations());
                            }
                        }
                        chunkDocLoader.prepareReplacementGraphics(getApplicationType(), documentConverter, aSession);

                        final JSONArray          lPreviewOps      = OperationHelper.listOperationsInOperationsObject (previewOpsObject  );
                        final int                nPreviewOps      = lPreviewOps  .length  ();
                        final Iterator< Object > rPreviewOps      = lPreviewOps  .iterator();
                        final JSONArray          lRemainingOps    = OperationHelper.listOperationsInOperationsObject (remainingOpsObject  );
                        final Iterator< Object > rRemainingOps    = lRemainingOps.iterator();
                        int                      nRemainingOps    = lRemainingOps.length  ();

                              boolean            bIsFirstChunk    = true;
                              JSONObject         aLastOp          = null;
                              int                nPreviewChunks   = impl_getChunkCount(nPreviewOps);
                              int                nRemainingChunks = impl_getChunkCount(nRemainingOps);
                              int                nChunks          = nPreviewChunks + nRemainingChunks; // Nx preview ops + Nx remaining ops

                        // --------------------------------------------------------------
                        {
                            final long nPreviewSize   = (previewOpsObject == null) ? 0 : StringUtils.length(previewOpsObject.toString());
                            final long nFileSize      = aOXDocument.getDocumentMetaData().getFileSize();

                            log.debug("load doc chunked ... file-size: {}, chunks: {}, count-preview-ops {}, size-preview-ops: {}", nFileSize, nChunks, nPreviewOps, nPreviewSize);
                        }

                        // --------------------------------------------------------------
                        // a) chunk previews
                        log.trace("... preview : START");

                        int nCurrentOpsCount = nPreviewOps;
                        while (nPreviewChunks > 0) {
                        	clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);
                            int              nOpsCount          = CHUNK_OPS_COUNT;
                            final JSONArray  aChunkOps          = new JSONArray();
                            final RT2Message aChunkDataResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_OPEN_DOC_CHUNK);

                            while (rPreviewOps.hasNext() && (nOpsCount > 0)) {
                            	clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);
                                final JSONObject aPreviewOp = (JSONObject) rPreviewOps.next ();

                                nOpsCount--;
                                nCurrentOpsCount--;
                                if ((nCurrentOpsCount == 0) && (nRemainingOps == 0)) {
                                    aLastOp = aPreviewOp;
                                    break;
                                }

                                if (bIsFirstChunk) {
                                    bIsFirstChunk = false;

                                    if ((getApplicationType() == ApplicationType.APP_SPREADSHEET) && (nActivePartIndex >= 0))
                                        MessageHelper.setActiveSheet(aChunkDataResponse, nActivePartIndex);
                                }
                                log.trace("... data. chunk: {}", nChunks);

                                aChunkOps.put(aPreviewOp);
                            }

                            MessageHelper.setOperations(aChunkDataResponse, aChunkOps);

                            RT2MessageGetSet.setError          (aChunkDataResponse, ErrorCode.NO_ERROR             );
                            RT2MessageGetSet.markAsPreview     (aChunkDataResponse                                 );

                            messageSender.sendMessageTo (clientUid, aChunkDataResponse);
                            nPreviewChunks--;
                        }

                        log.trace("... preview : END");

                        if (previewOpsObject != null) {
                            // remove active operations to release memory as soon as possible
                            previewOpsObject.reset();
                            lPreviewOps.reset();
                        }

                        // --------------------------------------------------------------
                        // b) chunk remaining ops

                        log.trace("... remaining : START");

                        nCurrentOpsCount = nRemainingOps;
                        while (nRemainingChunks > 0) {
                        	clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);
                            int              nOpsCount          = CHUNK_OPS_COUNT;
                            final RT2Message aChunkDataResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_OPEN_DOC_CHUNK);
                            final JSONArray  aChunkOps          = new JSONArray();

                            while (rRemainingOps.hasNext() && (nOpsCount > 0)) {
                                final JSONObject aRemainingOp = (JSONObject) rRemainingOps.next ();

                                nOpsCount--;
                                nCurrentOpsCount--;
                                if (nCurrentOpsCount == 0) {
                                    aLastOp = aRemainingOp;
                                    break;
                                }

                                if (bIsFirstChunk) {
                                    bIsFirstChunk = false;

                                    if ( ! StringUtils.isEmpty(sFastLoadHtmlDoc))
                                        MessageHelper.setHtmlDoc(aChunkDataResponse, sFastLoadHtmlDoc);
                                }
                                log.trace("... data. Chunks: {}", nChunks );

                                aChunkOps.put(aRemainingOp);
                            }

                            MessageHelper.setOperations(aChunkDataResponse, aChunkOps);
                            RT2MessageGetSet.setError          (aChunkDataResponse, ErrorCode.NO_ERROR              );

                            messageSender.sendMessageTo (clientUid, aChunkDataResponse);
                            nRemainingChunks--;
                        }

                        log.trace("... remaining : END");

                        // --------------------------------------------------------------
                        // c) send last chunk explicit ! (doesn't matter if it comes from preview or remaining set)
                        {
                        	clientSequenceQueueDisposerService.hasAbortOpenBeenReceived(getDocUID(), clientUid);

                            log.trace("... last-op : START");

                            final int nDocOSN = impl_getDocOSN(aOXDocument);

                            final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
                            synchronized (aDocStatus) {
                                int nCurrentOSN = aDocStatus.getOperationStateNumber();

                                if (aAdditionalOperations.isEmpty()) {
                                    // We have to calculate the current osn/opl for the last
                                    // operation, if there are no additional operations & the
                                    // operation state number has not be initialized yet.
                                    if (!LoadableDocumentStatus.isValidOSN(nCurrentOSN)) {
                                        nCurrentOSN = impl_determineServerOSN (nDocOSN, nPreviewOps + nRemainingOps);
                                        aDocStatus.setOperationStateNumber(nCurrentOSN);
                                    }

                                    final int nLastOpOSN = impl_calcLastOpOSN(nCurrentOSN);
                                    log.debug("... data. Chunks: {}, doc-osn: {}, server-osn: {}, last-op-osn: {}", nChunks, nDocOSN, nCurrentOSN, nLastOpOSN);
                                    impl_setLastOpOSN(aLastOp, nLastOpOSN);
                                    aDocStatus.setVersion(aOXDocument.getVersion());
                                }

                                // the last operation from the document must always be added
                                aAdditionalOperations.add(0, aLastOp);

                                aDocStatus.setDocumentOSN(nDocOSN);
                            }

                            final SyncInfo aSyncInfo = new SyncInfo(aUserData.getFolderId(), aUserData.getFileId());
                            final String sDocVersion = aOXDocInfo.getVersion();
                            aSyncInfo.updateSyncInfo(sDocVersion, nDocOSN);
                            // enable derived classes to update/add synchronization info
                            updateSyncInfo(aSession, aUserData.getFolderId(), aSyncInfo);

                            // client will not detect the end of loading the document if last chunk is marked as preview (even if it's a preview chunk)
                            final RT2Message aLastChunkDataResponse = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_OPEN_DOC_CHUNK);
                            MessageHelper.setSyncInfo(aLastChunkDataResponse, aSyncInfo);
                            MessageHelper.setOperations(aLastChunkDataResponse, aAdditionalOperations);
                            if (aOXDocument.isRescueDocument()) {
                                MessageHelper.setRescueOperations(aLastChunkDataResponse, aOXDocument.getRescueOperations());
                            }
                            RT2MessageGetSet.setError(aLastChunkDataResponse, ErrorCode.NO_ERROR);

                            messageSender.sendMessageTo(clientUid, aLastChunkDataResponse);

                            // create document events to update statistics
                            documentEventHelper.addOpenEvent(getDocumentFormat(), aOXDocInfo.getDocumentMetaData());

                            // update recent file list on open file now
                            impl_addToRecentFileList(aOXDocument.getDocumentMetaData(), aSession);
                            m_aResManager.lockResources(true);

                            // In some cases we need to make further access checks using the folder id
                            // and document meta data.
                            errorCode = makeAdditionalFileAccessChecks(aSession, aUserData.getFolderId(), aOXDocument.getDocumentMetaData(), errorCode);

                            log.trace("... last-op : END");
                        }
                    } catch (LoadDocumentAbortedByClientException ex) {
                    	return ex.getError();
                    } catch (final FilterException e) {
                        errorCode = getErrorCodeFromFilterExceptionAtImport(e, clientUid, aUserData, aOXDocument);
                    } catch (final Throwable e) {
                        ExceptionUtils.handleThrowable(e);

                        errorCode = ErrorCode.LOADDOCUMENT_FAILED_ERROR;
                        log.warn("RT2LoadableDocProcessor: loadDocument, Exception caught", e);

                        // create document events to update statistics
                        documentEventHelper.addOpenErrorEvent(getDocumentType());
                    }
                } else {
                    impl_handleError4Open(aSession, errorCode, aUserData.getFolderId(), aUserData.getFileId());
                }
            } catch (LoadDocumentAbortedByClientException ex) {
            	return ex.getError();
            } finally {
                if (locked)
                    unlockForLoadOrSaveDocument();
            }
        }

        // make sure that warnings during opening a document are not lost
        // in case further document actions didn't reveal any problems
        if (errorCode.isNoError())
        	errorCode = openErrorCode;

        return errorCode;
    }

    //-------------------------------------------------------------------------
    protected ErrorCode getErrorCodeFromFilterExceptionAtImport(FilterException e, RT2CliendUidType sClientUID, UserData userData, OXDocument oxDocument) {
        ErrorCode errorCode = FilterExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR, true);

        // DOCS-2794 ensure that not every filter exception gets logged
        if (!FilterExceptionToErrorCode.shouldBeLogged(errorCode))
            return errorCode;

        // make sure that filter exception is logged on log-level warn
        if(e.getErrorcode() == FilterException.ErrorCode.COMPLEXITY_TOO_HIGH) {
            specialLogService.log(Level.INFO, log, getDocUID().getValue(), new Throwable(), "LoadableDocProcessor: Document could not be loaded because it exceeds the maxWordCount / maxCells limit.");
        }
        else {
            specialLogService.log(Level.WARN, log, getDocUID().getValue(), new Throwable(), "LoadableDocProcessor: loading document stream resulted in a filter exception.", e);
        }

        if ((e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_TOO_HIGH) || (e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED)) {
            // try to figure out what happened with the memory usage of the filter as
            // we want to provide two different error codes.
            errorCode = ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR;
            if (e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED) {
                final String freeMemString =  ((Long) (SystemInfoHelper.freeMemory() / SystemInfoHelper.MBYTES)).toString();
                specialLogService.log(Level.INFO, log, getDocUID().getValue(), new Throwable(), "Document could not be loaded, because current available heap space is not sufficient. Current free heap space: {} MB", freeMemString);
            }
        }

        return errorCode;
    }

    //-------------------------------------------------------------------------
	protected void updateSyncInfo(final ServerSession session, final String folderID, final SyncInfo syncInfo) {
        final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderID);

		syncInfo.setRenameNeedsReload(!storageHelper.supportsPersistentIDs());
	}

    //-------------------------------------------------------------------------
    @SuppressWarnings("unused")
    protected ErrorCode makeAdditionalFileAccessChecks(final ServerSession aSession, String sFolderID, final DocumentMetaData aDocumentMetaData, final ErrorCode aCurrentErrorCode)
    {
        // no further checks for loadable document necessary - must be overwritten by sub-class
        // in case it must do additional file checks, e.g. backup file access rights!
        return aCurrentErrorCode;
    }

    //-------------------------------------------------------------------------
   protected boolean canHandleLocalStorageLoading(final OXDocumentInfo oxDocInfo, String sStorageVersion, int nStorageOSN, boolean bFirstUser) {
       boolean canLoadViaLocalStorage = isLocalStorageLoadPossible(oxDocInfo, sStorageVersion, nStorageOSN);

       if (canLoadViaLocalStorage && bFirstUser)
       {
           final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
           synchronized (aDocStatus)
           {
        	   aDocStatus.setOperationStateNumber(nStorageOSN);
           }
       }

       return canLoadViaLocalStorage;
   }

   //-------------------------------------------------------------------------
   /**
    * Determines if we can omit several load steps in case of a local storage
    * load by the client.
    *
    * @param oxDocInfo the OXDocumentInfo instance of the document to be loaded
    * @param sStorageVersion the document version the client has in its local storage
    * @param nStorageOSN the document osn the client has in its local storage
    * @return true if the client can load via local storage and false if not
    */
   private boolean isLocalStorageLoadPossible(final OXDocumentInfo oxDocInfo, String sStorageVersion, int nStorageOSN) {
       final String docVersion = oxDocInfo.getVersion();
       final long   docOSN     = oxDocInfo.getUniqueDocumentId();

       return !oxDocInfo.isRescueDocument() && (docOSN == nStorageOSN) && (!StringUtils.isEmpty(sStorageVersion) && docVersion.equals(sStorageVersion));
   }

	//-------------------------------------------------------------------------
	protected void checkAndUpdateUserEncryptionInfo(final RT2Message aOpenDocReq, boolean bFirstUser)
	    throws Exception
	{
        // encryption information must only be sent via an open doc request
        if (RT2MessageType.REQUEST_OPEN_DOC.equals(aOpenDocReq.getType()))
        {
            final String sAuthCode       = RT2MessageGetSet.getAuthCode(aOpenDocReq);
        	if (StringUtils.isNotEmpty(sAuthCode)) {
                final String sSessionID      = RT2MessageGetSet.getSessionID(aOpenDocReq);
                final ServerSession aSession = sessionService.getSession4Id(sSessionID);
        		if (null != aSession)
	            {
	                if (bFirstUser)
	                    m_bEncrypted = true;

	                // create the encryption info string using guard specific service
	                if (StringUtils.isNotEmpty(sAuthCode))
	                {
	                    final String sClientUID      = RT2MessageGetSet.getClientUID(aOpenDocReq);
	                    final String sDocUID         = RT2MessageGetSet.getDocUID(aOpenDocReq);
	                    final String sFileID         = RT2MessageGetSet.getFileID(aOpenDocReq);

	                    if (null != cryptographicServiceAuthenticationFactory)
	                    {
	                        try
	                        {
	                            // needed in get_file action - make sure that connection/session can only decrypt with the authCode
	                            final String sContextID = Integer.toString(aSession.getContextId());
	                            final String sUserID    = Integer.toString(aSession.getUserId());

	                            AuthorizationCache.addPersistentKey(sContextID, sUserID, sFileID, sAuthCode);
	                            updateUserData(sClientUID, cryptographicServiceAuthenticationFactory.createAuthenticationFrom(aSession, sAuthCode));
	                        }
	                        catch (OXException e)
	                        {
	                            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.LOADDOCUMENT_ENCRYPTIONINFO_NOT_AVAILABLE_ERROR.getCodeAsStringConstant());
	                            log.error("RT2: Not able to retrieve encryption info from CryptographicServiceAuthenticationFactory for com.openexchange.rt2.document.uid " + sDocUID, e);
	                            MDC.remove(MDCEntries.ERROR);
	                            throw new RT2TypedException(ErrorCode.LOADDOCUMENT_ENCRYPTIONINFO_NOT_AVAILABLE_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
	                        }
	                    }
	                    else
	                    {
                            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.LOADDOCUMENT_GUARD_NOT_AVAILABLE_ERROR.getCodeAsStringConstant());
	                        log.error("RT2: Cannot get mandatory service CryptographicServiceAuthenticationFactory for com.openexchange.rt2.document.uid {}", sDocUID);
                            MDC.remove(MDCEntries.ERROR);
	                        throw new RT2TypedException(ErrorCode.LOADDOCUMENT_GUARD_NOT_AVAILABLE_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
	                    }
	                }
	            }
            }
        }
        else
        {
            log.error("RT2LoadableDocProcessor: expected " + RT2Protocol.REQUEST_OPEN_DOC + " with encryption info - protocol error!");
            throw new RT2Exception("RT2 connection: checkAndUpdateUserEncryptionInfo called with wrong RT2Message - must be REQUEST_OPEN_DOC");
        }
	}

	//-------------------------------------------------------------------------
    protected JSONArray getAdditionalOperations()
        throws Exception
    {
        return new JSONArray();
    }

    //-------------------------------------------------------------------------
    /**
     * Add the current file to the recent file list.
     *
     * @param oxDoc
     * @param session
     */
    protected void impl_addToRecentFileList(final DocumentMetaData docMetaData, final ServerSession session) {
        // update recent file list on open file now
        final RecentFileListManager recentFileListManager = recentFileListManagerFactory.create(session);
        final JSONObject addToFile = DocumentMetaData.createJSONObject(docMetaData, new Date());
        RecentFileListHelper.addToRecentFileList(recentFileListManager, getApplicationType(), addToFile, docMetaData.getLastModified());
    }

    //-------------------------------------------------------------------------
    /**
     * Handles an error code and updates the recent file according to the provided error code.
     *
     * @param onBehalfOf The ID of the client requesting the action.
     * @param serverSession The session of the client requesting the action.
     * @param errorCode The error code describing the problem of a recent action.
     * @param folderId The folder id of the document file.
     * @param fileId The file id of the document file.
     */
    protected void impl_handleError4Open(final Session serverSession, ErrorCode errorCode, String folderId, String fileId) {
        // create document events to update statistics
        documentEventHelper.addOpenErrorEvent(getDocumentType());

        // It's clear that we encountered on an error. Check for some
        // specific errors which need special handling.
        if (errorCode.getCode() == ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode()) {
            // file was not found => the recent file list must be updated
            final RecentFileListManager recentFileListManager = recentFileListManagerFactory.create(serverSession);
            RecentFileListHelper.removeFromRecentFileList(recentFileListManager, m_aAppType, folderId, fileId);
        }
    }

    //-------------------------------------------------------------------------
    private void impl_generateFastEmptyResponses(ServerSession session, OXDocumentInfo aOXDocInfo, RT2Message aRequest, JSONArray additionalOps) throws Exception {
        final String sFolderId = aOXDocInfo.getFolderId();
        final String sFileId = aOXDocInfo.getFileId  ();
        final String sDocVersion = aOXDocInfo.getVersion ();
        final DocumentType eDocType    = aOXDocInfo.getDocumentType();

        log.trace("... generate fast empty response START");

        final SyncInfo aSyncInfo = new SyncInfo(sFolderId, sFileId);
        aSyncInfo.updateSyncInfo(sDocVersion, OXDocument.DEFAULT_DOCUMENT_ID);
        // enable derived classes to update/add synchronization info
        updateSyncInfo(session, sFolderId, aSyncInfo);

        final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
        synchronized (aDocStatus) {
            aDocStatus.setDocumentOSN(OXDocument.DEFAULT_DOCUMENT_ID);
            aDocStatus.setOperationStateNumber(RT2MessageGetSet.getFastEmptyOSN(aRequest));
            aDocStatus.setVersion(sDocVersion);
        }

        sendSingleChunkResponse(aRequest, additionalOps, aSyncInfo, eDocType);

        log.trace("... generate fast empty response : END");
    }

    //-------------------------------------------------------------------------
    private void impl_generateFastLocalStorageResponse(ServerSession session, OXDocumentInfo aOXDocInfo, RT2Message aRequest, JSONArray additionalOps) throws Exception {
        final String sFolderId = aOXDocInfo.getFolderId();
        final String sFileId = aOXDocInfo.getFileId  ();
        final String sDocVersion = aOXDocInfo.getVersion ();
        final DocumentType eDocType = aOXDocInfo.getDocumentType();

        log.trace("... generate fast local storage load response START");

        final SyncInfo aSyncInfo = new SyncInfo(sFolderId, sFileId);
        aSyncInfo.updateSyncInfo(sDocVersion, aOXDocInfo.getUniqueDocumentId());
        updateSyncInfo(session, sFolderId, aSyncInfo);

        final LoadableDocumentStatus aDocStatus = (LoadableDocumentStatus)getDocumentStatus();
        synchronized (aDocStatus) {
            aDocStatus.setDocumentOSN(aOXDocInfo.getUniqueDocumentId());
            aDocStatus.setVersion(sDocVersion);
        }

        sendSingleChunkResponse(aRequest, additionalOps, aSyncInfo, eDocType);

        log.trace("... generate fast local storage load response END");
    }

    //-------------------------------------------------------------------------
    private void sendSingleChunkResponse(RT2Message aOpenRequest, JSONArray aAdditionalOps, SyncInfo aSyncInfo, DocumentType eDocType) throws Exception {
        final RT2Message aSingleChunkDataResponse = RT2MessageFactory.createResponseFromMessage(aOpenRequest, RT2MessageType.RESPONSE_OPEN_DOC_CHUNK);
        MessageHelper.setSyncInfo(aSingleChunkDataResponse, aSyncInfo);
        RT2MessageGetSet.setError(aSingleChunkDataResponse, ErrorCode.NO_ERROR);

        // additional operations must be sent to the client
        if (!aAdditionalOps.isEmpty())
            MessageHelper.setOperations(aSingleChunkDataResponse, aAdditionalOps);

        messageSender.sendMessageTo (aOpenRequest.getClientUID(), aSingleChunkDataResponse);
    }

    //-------------------------------------------------------------------------
    private int impl_getChunkCount(int nOps) {
        return Math.max(0, (int)Math.ceil(((double)nOps / CHUNK_OPS_COUNT)));
    }

    //-------------------------------------------------------------------------
    private int impl_getDocOSN (final OXDocument aOXDocument) throws Exception {
    	return aOXDocument.getUniqueDocumentId();
    }

    //-------------------------------------------------------------------------
    private int impl_determineServerOSN(int nDocOSN, int nOpsCount) throws Exception {
        // a) DocOSN is valid -> we have to use it always
        //    it's the OSN behind the last operation within the current document
        int nServerOSN = nDocOSN;

        // b) no (valid) DocOSN -> count operations and define ServerOSN new
        //    ServerOSN = CurrServerOSN + nOpsCount + 1
        if (!LoadableDocumentStatus.isValidOSN(nServerOSN))
            nServerOSN = nOpsCount + 1;

        return nServerOSN;
    }

    //-------------------------------------------------------------------------
    private int impl_calcLastOpOSN (final int nCurrServerOSN) throws Exception {
    	return nCurrServerOSN - 1;
    }

    //-------------------------------------------------------------------------
    private void impl_setLastOpOSN(JSONObject aOpJson, int nLastOpsOSN) throws Exception {
        JSONObject aLastOp = aOpJson;

        if (aLastOp.isArray())
        	aLastOp = OperationHelper.getLastOperation (aOpJson);

        if (aLastOp == null)
        	return;

        if (isOTEnabled()) {
            // ot uses a opn to uniquely define an operation message
            aLastOp.put(OperationHelper.KEY_OSN, nLastOpsOSN + 1);
        } else {
            // "old" applications use osn/opl for every single op
            aLastOp.put(OperationHelper.KEY_OSN, nLastOpsOSN);
            aLastOp.put(OperationHelper.KEY_OPL, 1);
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Checks the consistency of a loaded document with the lastest document state
     * data stored in this connection instance.
     *
     * @param currDocVersion the current known version of the document
     * @param currDocOSN the current document state number stored by this connection
     * @param currOSN the current operation state number stored by this connection
     * @param aDocument a document instance completely initialized
     * @return TRUE if the document loaded is consistent to the stored connection instance
     *         data and FALSE if the document is not consistent. If FALSE the connection
     *         instance must provide this information to the client
     */
    protected boolean checkConsistencyOfLoadedDocument(final String currDocVersion, int currDocOSN, int currOSN, final OXDocumentInfo aDocument)
    {
        final String    docVersion = aDocument.getVersion();
        final int       docOSN     = aDocument.getUniqueDocumentId();
        final ErrorCode docError   = aDocument.getLastError();

        return checkConsistencyOfLoadedDocument(currDocVersion, currDocOSN, currOSN, docVersion, docOSN, docError);
    }

    //-------------------------------------------------------------------------
    /**
     * Checks the consistency of a loaded document with the lastest document state
     * data stored in this connection instance.
     *
     * @param currDocVersion
     * @param currDocOSN
     * @param currOSN
     * @param docVersion
     * @param docOSN
     * @param docErrorCode
     * @return
     */
    protected boolean checkConsistencyOfLoadedDocument(final String currDocVersion, int currDocOSN, int currOSN, final String docVersion, int docOSN, final ErrorCode docErrorCode)
    {
        boolean consistent = false;

        if (docErrorCode.isNoError()) {
            // a consistent document must have the same version and the current osn is
            // always higher or equal than the document osn
            boolean versionOk = StringUtils.equals(currDocVersion,  docVersion);
            // check both document osn, too - the current doc osn must higher or equal to the loaded document osn
            consistent = (versionOk && (currDocOSN >= docOSN));

            if (!consistent) {
                log.debug("RT connection: Client loaded document. Consistency check result: " + consistent + " with last known version: " + currDocVersion + ", osn: " + currOSN + ", doc osn: " + currDocOSN + " and loaded document version: " + docVersion + ", osn: " + docOSN);
            }
        }

        return consistent;
    }

    //-------------------------------------------------------------------------
    protected boolean lockForLoadOrSaveDocumentWithTimeout(@SuppressWarnings("unused") long timeout) {
    	// Must be implemented by a derived class that supports
    	// save's which must be synchronized with a load

    	// This class supports loading only, therefore there is no
    	// need for a synchronization
    	return true;
    }

    //-------------------------------------------------------------------------
    protected void unlockForLoadOrSaveDocument() {
    	// This class supports loading only, therefore there is no
       	// need for a synchronization (unlock the lock)
    }

}
