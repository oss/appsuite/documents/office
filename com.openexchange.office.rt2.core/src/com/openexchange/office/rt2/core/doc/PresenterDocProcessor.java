/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.capabilities.CapabilityService;
import com.openexchange.capabilities.CapabilitySet;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2OldClientUuidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.Loggable;
import com.openexchange.office.tools.monitoring.PresenterEvent;
import com.openexchange.office.tools.monitoring.PresenterEventType;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

public class PresenterDocProcessor extends DocProcessor implements Loggable
{
    public static final String DOCTYPE_IDENTIFIER                        = "presenter";

    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(PresenterDocProcessor.class);
    private static final int    MAX_DEFAULT_PARTICIPANTS_FOR_PRESENTATION = 100;
    private static int          MAX_NUMBER_OF_PARTICIPANTS                = MAX_DEFAULT_PARTICIPANTS_FOR_PRESENTATION;

    @Autowired
    private CapabilityService capabilityService;

    @Autowired
    private UserService userService;

    //-------------------------------------------------------------------------
	public PresenterDocProcessor() throws Exception {
		super();
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isOTEnabled() {
		return false;
	}

    //-------------------------------------------------------------------------
	@Override
    protected DocumentStatus createDocumentStatus()
	{
		return new PresenterDocumentStatus();
	}

    //-------------------------------------------------------------------------
	@Override
	protected ClientsStatus createClientsStatus()
	{
		return new PresenterClientsStatus();
	}

    //-------------------------------------------------------------------------
    @Override
    public String getDocTypeIdentifier()
    {
        return DOCTYPE_IDENTIFIER;
    }

    //-------------------------------------------------------------------------
	// message processing
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    @Override
    public boolean onJoin(RT2Message request) throws Exception {
        super.onJoin(request);

        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(request, RT2MessageType.RESPONSE_JOIN);
        messageSender.sendMessageWOSeqNrTo (request.getClientUID(), aResponse);
        return true;
    }

    //-------------------------------------------------------------------------
	@Override
	public boolean onOpenDoc(final RT2Message aMsg) throws Exception
	{
        final RT2CliendUidType sClientUID    = aMsg.getClientUID();
        final String sSessionID    = RT2MessageGetSet.getSessionID(aMsg);
        final RT2OldClientUuidType sOldClientUID = aMsg.getOldClientUID();
        final String sDocType      = RT2MessageGetSet.getDocType(aMsg);

        // make sure that a non-presenter client (e.g. presentation) won't receives
        // an error trying to open the document being presented
        if (!getDocTypeIdentifier().equals(sDocType))
            throw new RT2TypedException(ErrorCode.LOADDOCUMENT_PRESENTATION_IS_BEING_PRESENTED_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));

        log.debug("PresenterConnection [onOpenDoc] for com.openexchange.rt2.client.uid {}", sClientUID);

        final ServerSession aSession = sessionService.getSession4Id(sSessionID);
        impl_handlePresenter(aSession, sClientUID, sOldClientUID);

        // send open response with current document/clients status
        final RT2Message              aOpenResponse  = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_OPEN_DOC);
        final PresenterDocumentStatus aDocStatus     = (PresenterDocumentStatus)this.getDocumentStatusClone();
        final PresenterClientsStatus  aClientsStatus = (PresenterClientsStatus)this.getClientsStatusClone();
        MessageHelper.setDocumentStatus(aOpenResponse, aDocStatus);
        MessageHelper.setClientsStatus(aOpenResponse, aClientsStatus, true);
        messageSender.sendMessageTo(sClientUID, aOpenResponse);

        // send updates to all if document and/or clients status has changed
        impl_broadcastDocumentStatusIfModified(sClientUID);
        impl_broadcastClientsStatusIfModified(sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPEN));

        return true;
	}

    //-------------------------------------------------------------------------
    @Override
	public boolean onAdditionalOpenDoc(RT2Message aMsg) throws Exception {
    	return onOpenDoc(aMsg);
	}

	//-------------------------------------------------------------------------
	@Override
	public boolean onCloseDoc(final RT2Message aMsg) throws Exception
	{
        final RT2CliendUidType        sClientUID    = aMsg.getClientUID();
        final PresenterDocumentStatus aDocStatus    = (PresenterDocumentStatus)this.getDocumentStatus();
        final PresenterClientsStatus  aClientstatus = (PresenterClientsStatus)this.getClientsStatus();

        synchronized (getStatusSyncObject())
        {
            if (aDocStatus.isPresenter(sClientUID)) {
                aDocStatus.setCurrentPresenter(new RT2CliendUidType(), PresenterDocumentStatus.EMPTY_USERNAME);
            }
            aClientstatus.removeClient(sClientUID);
        }

        // send close doc response
        if (!RT2MessageType.REQUEST_CLOSE_AND_LEAVE.equals(aMsg.getType())) {
        	final RT2Message aCloseResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_CLOSE_DOC);
        	messageSender.sendMessageTo(sClientUID, aCloseResponse);
        }

        // send updates to all if document and/or clients status has changed
        impl_broadcastDocumentStatusIfModified(sClientUID);
        impl_broadcastClientsStatusIfModified(sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.CLOSE));

		return true;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean onEmergencyLeave(final RT2Message aMsg) throws Exception
	{
        final RT2CliendUidType        sClientUID    = aMsg.getClientUID();
        final PresenterDocumentStatus aDocStatus    = (PresenterDocumentStatus)this.getDocumentStatus();
        final PresenterClientsStatus  aClientstatus = (PresenterClientsStatus)this.getClientsStatus();

        // handle emergency leave as a combination of close & leave
        synchronized (getStatusSyncObject())
        {
            if (aDocStatus.isPresenter(sClientUID)) {
                aDocStatus.setCurrentPresenter(PresenterDocumentStatus.EMPTY_CLIENTID, PresenterDocumentStatus.EMPTY_USERNAME);
            }
            aClientstatus.removeClient(sClientUID);
        }

        // send updates to all if document and/or clients status has changed
        impl_broadcastDocumentStatusIfModified(sClientUID);
        impl_broadcastClientsStatusIfModified(sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.CLOSE));

        // base class sends response to emergency leave
		return super.onEmergencyLeave(aMsg);
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean onSync(final RT2Message aMsg) throws Exception
	{
        final PresenterDocumentStatus aDocStatus = (PresenterDocumentStatus)this.getDocumentStatusClone();
        final RT2Message aSyncResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_SYNC);

        aSyncResponse.setBody(aDocStatus.toJSON());
        messageSender.sendMessageTo(aMsg.getClientUID(), aSyncResponse);

		return true;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean onSyncStable(final RT2Message aMsg) throws Exception
	{
        final PresenterDocumentStatus aDocStatus = (PresenterDocumentStatus)this.getDocumentStatusClone();
        final RT2Message aSyncResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_SYNC_STABLE);

        aSyncResponse.setBody(aDocStatus.toJSON());
        messageSender.sendMessageTo(aMsg.getClientUID(), aSyncResponse);

		return true;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean onUnavailable(final RT2Message aMsg) throws Exception
	{
		// TODO: Check if we should handle this notification
		return true;
	}

    //-------------------------------------------------------------------------
	// custom messages
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    /**
     * Change presentation slide.
     *
     * @param aMsg
     *  The message containing the slide information.
     * @throws Oxception
     */
    public boolean handleUpdate_Slide_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType        sClientUID = aMsg.getClientUID();
        final PresenterDocumentStatus aDocStatus = (PresenterDocumentStatus)this.getDocumentStatus();

        log.debug("PresenterConnection [handleUpdateSlide] for com.openexchange.rt2.client.uid {}", sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_INCOMING));

        int countParticipants = 0;
        Set<RT2CliendUidType>   aParticipants    = null;
        PresenterDocumentStatus aUpdateDocStatus = null;
        synchronized (getStatusSyncObject())
        {
            if ( ! aDocStatus.isPresenter(sClientUID))
                throw new RT2TypedException (ErrorCode.HANGUP_NO_EDIT_RIGHTS_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));

            final int nActiveSlide = MessageHelper.getCurrentSlide(aMsg);
            boolean   bChanged     = aDocStatus.setActiveSlide(nActiveSlide);

            // send response to presenter
            final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
            messageSender.sendMessageTo(sClientUID, aResponse);

            if (bChanged)
            {
                aUpdateDocStatus = (PresenterDocumentStatus)getDocumentStatusClone();
                aDocStatus.setModified(false);

                final PresenterClientsStatus aClientsStatus = (PresenterClientsStatus)getClientsStatus();
                aParticipants = aClientsStatus.getParticipants();
                countParticipants = aParticipants.size() - 1;
            }
        }

        if (null != aUpdateDocStatus) {
            impl_broadcastDocumentStatusToParticipants(aParticipants, aUpdateDocStatus);
            statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_DISTRIBUTED, countParticipants));
        }

        return true;
    }

    //-------------------------------------------------------------------------
    /**
     * Start the presentation as presenter
     *
     * @param stanza
     *  The Stanza containing the presenter and slide information.
     * @throws Exception
     */
    public void handleStart_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType         sClientUID     = aMsg.getClientUID();
        final String                   sSessionID     = RT2MessageGetSet.getSessionID(aMsg);
        final PresenterDocumentStatus  aDocStatus     = (PresenterDocumentStatus)this.getDocumentStatus();
        final PresenterClientsStatus   aClientsStatus = (PresenterClientsStatus)this.getClientsStatus();

        log.debug("PresenterConnection [handleStartPresentation] for com.openexchange.rt2.client.uid {}", sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_INCOMING));

        final ServerSession           aSession           = sessionService.getSession4Id(sSessionID);
        final CapabilitySet           aCapabilitySet     = (null != capabilityService) ? capabilityService.getCapabilities(aSession) : null;

        if ((null == aCapabilitySet) || (!aCapabilitySet.contains("remote_presenter")))
        {
            log.info("PresenterConnection [handleStartPresentation], remote-presentation disabled by capabilities");
            throw new RT2TypedException (ErrorCode.GENERAL_SERVICE_DISABLED_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
        }

        final int nActiveSlide     = MessageHelper.getCurrentSlide(aMsg);
        String    sUserDisplayName = DocumentStatus.EMPTY_USERNAME;

        try
        {
            sUserDisplayName = sessionService.getUserDisplayName(aSession);
        }
        catch (Exception e)
        {
            log.error("PresenterConnection [handleStartPresentation], exception caught", e);
        }

        int countParticipants = 0;
        synchronized (getStatusSyncObject())
        {
            // another presenter active ?
            if ( ! ClientsStatus.isEmptyUser(aDocStatus.getCurrentPresenterId().getValue()))
            {
                log.debug("PresenterConnection [handleStartPresentation], remote-presentation already started");
                throw new RT2TypedException (ErrorCode.PRESENTER_REMOTE_PRESENTATION_ALREADY_RUNNING_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
            }

            try
            {
                aDocStatus.setCurrentPresenter(sClientUID, sUserDisplayName);
                aDocStatus.setActiveSlide(nActiveSlide);
                aClientsStatus.setParticipantState(sClientUID, true);
                countParticipants = aClientsStatus.getParticipantsCount() - 1;
            }
            catch (JSONException e)
            {
                log.error("PresenterConnection [handleStartPresentation], exception caught", e);
            }
        }

        // send response to presenter
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
        messageSender.sendMessageTo(sClientUID, aResponse);

        // send updates to all if document and/or clients status has changed
        impl_broadcastDocumentStatusIfModified(null);
        impl_broadcastClientsStatusIfModified(null);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.REMOTE_PRESENTER_START));
        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_DISTRIBUTED, countParticipants));
    }

    //-------------------------------------------------------------------------
    /**
     * End the presentation as presenter
     *
     * @param stanza
     *  The Stanza containing the presenter information.
     */
    public void handleEnd_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType        sClientUID     = aMsg.getClientUID();
        final PresenterDocumentStatus aDocStatus     = (PresenterDocumentStatus)this.getDocumentStatus();
        final PresenterClientsStatus  aClientsStatus = (PresenterClientsStatus)this.getClientsStatus();

        log.debug("PresenterConnection [handleEndPresentation] for com.openexchange.rt2.client.uid {}", sClientUID);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_INCOMING));

        int countParticipants = 0;
        synchronized (getStatusSyncObject())
        {
            if (aDocStatus.isPresenter(sClientUID))
            {
                try
                {
                    aDocStatus.setCurrentPresenter(new RT2CliendUidType(), DocumentStatus.EMPTY_USERNAME);
                    aDocStatus.setPausedState(false);
                    countParticipants = aClientsStatus.getParticipantsCount() - 1;
                    aClientsStatus.disjoinAll();
                }
                catch (Exception e)
                {
                    log.error("PresenterConnection [handleEndPresentation], exception caught", e);
                }
            }
        }

        // send response to presenter
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
        messageSender.sendMessageTo(sClientUID, aResponse);

        // send updates to all if document and/or clients status has changed
        impl_broadcastDocumentStatusIfModified(null);
        impl_broadcastClientsStatusIfModified(null);

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_DISTRIBUTED, countParticipants));
        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.REMOTE_PRESENTER_END));
    }

    //-------------------------------------------------------------------------
    /**
     * Pause the presentation as presenter
     *
     * @param stanza
     * @throws Exception
     */
    public void handlePause_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType         sClientUID = aMsg.getClientUID();
        final PresenterDocumentStatus  aDocStatus = (PresenterDocumentStatus)this.getDocumentStatus();

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_INCOMING));

        log.debug("PresenterConnection [handlePausePresentation] for com.openexchange.rt2.client.uid {}", sClientUID);

        Set<RT2CliendUidType>   aParticipants    = null;
        PresenterDocumentStatus aUpdateDocStatus = null;

        int countParticipants = 0;
        synchronized (getStatusSyncObject())
        {
            if (aDocStatus.isPresenter(sClientUID) && !aDocStatus.isPaused())
            {
                aDocStatus.setPausedState(true);
                aUpdateDocStatus = aDocStatus.clone();

	            final PresenterClientsStatus aClientsStatus = (PresenterClientsStatus)getClientsStatus();
	            aParticipants = aClientsStatus.getParticipants();
	            countParticipants = aClientsStatus.getParticipantsCount() - 1;
            }
        }

        // send response to presenter
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
        messageSender.sendMessageTo(sClientUID, aResponse);

        // send update to all participants
        if (null != aUpdateDocStatus) {
            impl_broadcastDocumentStatusToParticipants(aParticipants, aUpdateDocStatus);
            statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_DISTRIBUTED, countParticipants));
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Continue the previously paused presentation as presenter.
     *
     * @param stanza
     * @throws Exception
     */
    public void handleContinue_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType        sClientUID = aMsg.getClientUID();
        final PresenterDocumentStatus aDocStatus = (PresenterDocumentStatus)this.getDocumentStatus();

        statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_INCOMING));

        log.debug("PresenterConnection [handleContinuePresentation] for com.openexchange.rt2.client.uid {}", sClientUID);

        Set<RT2CliendUidType>   aParticipants    = null;
        PresenterDocumentStatus aUpdateDocStatus = null;

        int countParticipants = 0;
        synchronized (getStatusSyncObject())
        {
            if (aDocStatus.isPresenter(sClientUID) && aDocStatus.isPaused())
            {
                aDocStatus.setPausedState(false);
                aUpdateDocStatus = aDocStatus.clone();

                final PresenterClientsStatus aClientsStatus = (PresenterClientsStatus)getClientsStatus();
                aParticipants = aClientsStatus.getParticipants();
	            countParticipants = aClientsStatus.getParticipantsCount() - 1;
            }
        }

        // send response to presenter
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
        messageSender.sendMessageTo(sClientUID, aResponse);

        // send update to all participants
        if (null != aUpdateDocStatus) {
            impl_broadcastDocumentStatusToParticipants(aParticipants, aUpdateDocStatus);
            statistics.handlePresenterEvent(new PresenterEvent(PresenterEventType.OPERATIONS_DISTRIBUTED, countParticipants));
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Join the presentation as participant / listener.
     *
     * @param stanza
     * @throws OXException
     */
    public void handleJoin_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final PresenterClientsStatus  aClientsStatus = (PresenterClientsStatus)this.getClientsStatus();
        final PresenterDocumentStatus aDocStatus     = (PresenterDocumentStatus)this.getDocumentStatusClone();

        log.debug("PresenterConnection [handleJoinPresentation] for com.openexchange.rt2.client.uid {}", aMsg.getClientUID());

        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);

        synchronized (getStatusSyncObject())
        {
        	initMaxParticipants();
            if (aClientsStatus.getParticipantsCount() < MAX_NUMBER_OF_PARTICIPANTS)
                aClientsStatus.setParticipantState(aMsg.getClientUID(), true);
            else
                RT2MessageGetSet.setError(aResponse, ErrorCode.PRESENTER_MAX_PARTICIPANTS_FOR_PRESENTATION_REACHED_ERROR);
        }

        // send response to joining client
        messageSender.sendMessageTo(aMsg.getClientUID(), aResponse);

        // Send joining client a document update to ensure that it receives the latest
        // active slide. Non-participants won't receive updates, therefore the active slide
        // is unknown to them.
        impl_sendDocumentStatusTo(aMsg.getClientUID(), aDocStatus);

        // broadcast updated clients status to all participants
        impl_broadcastClientsStatusIfModified(null);
    }

    //-------------------------------------------------------------------------
    /**
     * Leave the presentation as participant / listener.
     *
     * @param stanza
     * @throws OXException
     */
    public void handleLeave_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        final RT2CliendUidType       sClientUID     = aMsg.getClientUID();
        final PresenterClientsStatus aClientsStatus = (PresenterClientsStatus)this.getClientsStatus();

        log.debug("PresenterConnection [handleLeavePresentation] for com.openexchange.rt2.client.uid {}", sClientUID);

        synchronized (getStatusSyncObject())
        {
            aClientsStatus.setParticipantState(sClientUID, false);
        }

        // send response to presenter
        final RT2Message aResponse = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_APP_ACTION);
        messageSender.sendMessageTo(sClientUID, aResponse);

        impl_broadcastClientsStatusIfModified(null);
    }

    //-------------------------------------------------------------------------
    /**
     * Determines who is/will be the presenter, when a new client joins.
     *
     * @param newId The real-time ID of the joining client.
     */
    private void impl_handlePresenter(final Session session, final RT2CliendUidType sNewClientId, final RT2OldClientUuidType sOldClientUID) throws Exception
    {
        User userInfo = null;
        try
        {
            userInfo = userService.getUser(session.getUserId(),  session.getContextId());
        }
        catch (Exception e)
        {
            log.debug("PresenterConnection [impl_handlePresenter] for com.openexchange.rt2.client.uid {} exception caught while trying to get user info. Exception: {}", sNewClientId, e);
        }

        final PresenterDocumentStatus aDocStatus     = (PresenterDocumentStatus)this.getDocumentStatus();
        final PresenterClientsStatus  aClientsStatus = (PresenterClientsStatus)this.getClientsStatus();

        synchronized (getStatusSyncObject())
        {
            // set new presenter if client provides its old client uid and it's the same as
            // the current presenter id
            final RT2CliendUidType sCurrentPresenterId = aDocStatus.getCurrentPresenterId();
            if ((StringUtils.isNotEmpty(sCurrentPresenterId.getValue())) &&
                sCurrentPresenterId.getValue().equals(sOldClientUID.getValue()) &&
                ((null != userInfo) && (!userInfo.isGuest())))
            {
                aDocStatus.setCurrentPresenter(sNewClientId, sessionService.getUserDisplayName(session));
                aClientsStatus.setParticipantState(sNewClientId, true);
            }
        }
    }

    //-------------------------------------------------------------------------
    private void impl_sendDocumentStatusTo(final RT2CliendUidType clientUID, final DocumentStatus aDocStatus) throws Exception
    {
        final RT2Message aUpdateDocStatetMsg = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_UPDATE, clientUID, getDocUID());
        MessageHelper.setDocumentStatus(aUpdateDocStatetMsg, aDocStatus);
        messageSender.sendMessageTo(clientUID, aUpdateDocStatetMsg);
    }

    //-------------------------------------------------------------------------
    private void impl_broadcastDocumentStatusIfModified(final RT2CliendUidType sExceptClientUID) throws Exception
    {
        final DocumentStatus aUpdateDocStatus = getDocumentStatusCloneIfModified(true);
        if (null != aUpdateDocStatus)
        {
            // broadcast document update message to all other clients in case status has changed
            final RT2Message aUpdateDocStateBroadcast = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_UPDATE, getDocUID());
            MessageHelper.setDocumentStatus(aUpdateDocStateBroadcast, aUpdateDocStatus);
            messageSender.broadcastMessageExceptClient (sExceptClientUID, aUpdateDocStateBroadcast, RT2MessageType.BROADCAST_UPDATE);
        }
    }

    //-------------------------------------------------------------------------
    private void impl_broadcastClientsStatusIfModified(final RT2CliendUidType sExceptClientUID) throws Exception
    {
        // send clients update message to all other clients
        final ClientsStatus aUpdateClientsStatus = getClientsStatusCloneIfModified(true);
        if (null != aUpdateClientsStatus)
        {
            // broadcast clients update message to all other clients in case status has changed
            final RT2Message aUpdateDocStateBroadcast = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_UPDATE_CLIENTS, getDocUID());
            MessageHelper.setClientsStatus(aUpdateDocStateBroadcast, aUpdateClientsStatus, false);
            messageSender.broadcastMessageExceptClient (sExceptClientUID, aUpdateDocStateBroadcast, RT2MessageType.BROADCAST_UPDATE_CLIENTS);
        }
    }

    //-------------------------------------------------------------------------
    private void impl_broadcastDocumentStatusToParticipants(final Set<RT2CliendUidType> aParticipants, final DocumentStatus aUpdatedDocStatus) throws Exception
    {
        Validate.notNull(aUpdatedDocStatus);

        if ((null != aParticipants) && !aParticipants.isEmpty())
        {
	        // broadcast update message to all other clients
	        final RT2Message aUpdateDocStateBroadcast = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_UPDATE, getDocUID());
	        MessageHelper.setDocumentStatus(aUpdateDocStateBroadcast, aUpdatedDocStatus);
	        messageSender.broadcastMessageTo(aParticipants, aUpdateDocStateBroadcast, RT2MessageType.BROADCAST_UPDATE);
        }
    }

    //-------------------------------------------------------------------------
    private synchronized void initMaxParticipants()
    {
       	MAX_NUMBER_OF_PARTICIPANTS = configurationService.getIntProperty("com.openexchange.presenter.maxParticipants", MAX_DEFAULT_PARTICIPANTS_FOR_PRESENTATION);
    }

    //-------------------------------------------------------------------------
	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	public boolean onFirstOpenDoc(RT2Message aMsg) throws Exception {
		return onOpenDoc(aMsg);
	}

	@Override
	public boolean onApplyOperations(RT2Message aMsg) throws Exception {
		return false;
	}

	@Override
	public boolean onSaveDoc(RT2Message aMsg) throws Exception {
		return false;
	}

	@Override
	public boolean onEditRights(RT2Message aMsg) throws Exception {
		return false;
	}

	@Override
	public int getPendingOperationsCount() {
		return 0;
	}

}
