/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;

public class MessageDispatcher
{
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(MessageDispatcher.class);
	
    //-------------------------------------------------------------------------
    public static final String        PREFIX_CUSTOM_METHOD = "handle";

    //-------------------------------------------------------------------------
    private final Map<String, Method> aMsgHandleMethods    = new HashMap<>();

    //-------------------------------------------------------------------------
    public MessageDispatcher(Class<?> clazz, final String sMethodPrefix)
    {
        final String sPrefix = StringUtils.isEmpty(sMethodPrefix) ? PREFIX_CUSTOM_METHOD : sMethodPrefix;
        for (Method method: clazz.getMethods())
        {
            if (method.getName().startsWith(sPrefix))
            {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if ((parameterTypes.length == 1) && (parameterTypes[0].isAssignableFrom(RT2Message.class)))
                {
                    String name = method.getName().substring(sPrefix.length()).toLowerCase();
                    aMsgHandleMethods.put(name, method);
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    public MessageDispatcher(Class<?> clazz, final Collection<String> aMethodNames)
    {
        Validate.notNull(clazz);
        Validate.notNull(aMethodNames);

        for (Method method: clazz.getMethods())
        {
            final String sMethodName = method.getName();
            if (aMethodNames.contains(sMethodName))
            {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1 && (parameterTypes[0].isAssignableFrom(RT2Message.class)))
                    aMsgHandleMethods.put(sMethodName, method);
            }
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Have the CustomMessageDispatcher choose an appropriate method for 
     * the given RT2Message and calls it on the given handler.
     *
     * @return true when a method is found and it in turn didn't return
     *         true, false if either no matching method was found or the
     *         method returned false
     * @throws Exception
     */
    public boolean callMethod(Object handler, final String sMethod, RT2Message aMsg) throws Exception
    {
        Method method = aMsgHandleMethods.get(sMethod);
        if (method != null)
           return invoke(method, handler, aMsg);

        throw new UnsupportedOperationException("No handler found for msg method: " + sMethod + ", existing: " + aMsgHandleMethods.keySet());
    }

    //-------------------------------------------------------------------------
    /**
     * Have the CustomMessageDispatcher choose an appropriate method for 
     * the given RT2Message and calls it on the given handler.
     *
     * @return true when a method is found and it in turn didn't return
     *         true, false if either no matching method was found or the
     *         method returned false
     * @throws Exception
     */
    public boolean callMethod(Object handler, RT2Message aMsg) throws Exception
    {
        final String sMsgAction = RT2MessageGetSet.getAppAction(aMsg);

        Method method = aMsgHandleMethods.get(sMsgAction.toLowerCase());
        if (method != null)
           return invoke(method, handler, aMsg);

        throw new UnsupportedOperationException("No handler found for msg action: " + sMsgAction + ", existing: " + aMsgHandleMethods.keySet());
    }

    //-------------------------------------------------------------------------
    private boolean invoke(final Method method, final Object handler, final RT2Message aMsg) throws Exception
    {
        try
        {
            Object result = method.invoke(handler, aMsg);
            if (result != null && Boolean.class.isInstance(result))
                return (Boolean)result;
        }
        catch (final InvocationTargetException e)
        {
            // make sure that we throw the original exception that must be
            // extracted from the InvocationTargetException, otherwise
            // our RT2TypedException gets lost.
            log.debug("Called method threw exception", e);

            final Throwable t = e.getCause();
            if (t instanceof Exception)
                throw (Exception)t;
            else
                ExceptionUtils.handleThrowable(t);

            throw e;
        }
        catch (final Exception e)
        {
            log.error("Exception caught while trying to invoke method for RT2Message!", e);
            throw e;
        }

        return true;
    }
}
