/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.office.tools.common.error.ErrorCode;

//-------------------------------------------------------------------------
public class CopyDocumentInfo
{
    private ErrorCode aErrorCode;
    private String    sCopyFileID;
    private String    sCopyFolderID;
    private String    sCopyFileName;

    //-------------------------------------------------------------------------
    public CopyDocumentInfo(final ErrorCode errorCode)
    {
    	aErrorCode = errorCode;
    }

    //-------------------------------------------------------------------------
    public ErrorCode getErrorCode() { return aErrorCode; }

    //-------------------------------------------------------------------------
    public String    getFileID()    { return sCopyFileID; }

    //-------------------------------------------------------------------------
    public String    getFolderID()  { return sCopyFolderID; }

    //-------------------------------------------------------------------------
    public String    getFileName()  { return sCopyFileName; }

    //-------------------------------------------------------------------------
    public void setErrorCode(ErrorCode aErrorCode) { this.aErrorCode = aErrorCode; }

    //-------------------------------------------------------------------------
    public void setFileID(String sFileID)          { sCopyFileID = sFileID; }

    //-------------------------------------------------------------------------
    public void setFolderID(String sFolderID)      { sCopyFolderID = sFolderID; }

    //-------------------------------------------------------------------------
    public void setFileName(String sFileName)      { sCopyFileName = sFileName; }

    //-------------------------------------------------------------------------
    public JSONObject toJSON() throws JSONException
    {
        final JSONObject aJSON = new JSONObject();
        aJSON.put("filename" , getFileName());
        aJSON.put("folder_id", getFolderID());
        aJSON.put("id"       , getFileID());
        return aJSON;
    }
}
