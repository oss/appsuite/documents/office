/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.ApplicationType;

//=============================================================================
public class PresentationDocProcessor extends EditableDocProcessor
{
	//-------------------------------------------------------------------------
	public static final String DOCTYPE_IDENTIFIER = "presentation";

	//-------------------------------------------------------------------------
	public PresentationDocProcessor()
		throws Exception
	{
		super(ApplicationType.APP_PRESENTATION);
	}

    //-------------------------------------------------------------------------
    @Override
    public String getDocTypeIdentifier()
    {
        return DOCTYPE_IDENTIFIER;
    }

    //-------------------------------------------------------------------------
    /**
     * Start the presentation as presenter.
     *
     * Information:
     * This is just a small handler to notify a presenter client that presenting
     * is not possible as this document is currently handled by a Presentation
     * doc processor.
     */
    public boolean handleStart_Presentation_Request(final RT2Message aMsg) throws Exception
    {
        throw new RT2TypedException (ErrorCode.PRESENTER_PRESENTATION_IS_BEING_EDITED_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
    }
}
