/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.mutable.MutableInt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DocStatistics implements IDocNotificationHandler, IStatisticData {

	private static final Logger log = LoggerFactory.getLogger(DocStatistics.class);
    private static class Holder {
        static final DocStatistics INSTANCE = new DocStatistics();
    }

    //-------------------------------------------------------------------------
    private AtomicInteger           m_aNum              = new AtomicInteger(0);
    private Map<String, MutableInt> m_aNumForDocTypeMap = new HashMap<>();

    //-------------------------------------------------------------------------
    private DocStatistics() {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorCreated(final RT2DocProcessor aCreatedInstance) {
        m_aNum.incrementAndGet();

        final String sDocType = aCreatedInstance.getDocTypeIdentifier();
        synchronized(m_aNumForDocTypeMap) {
            MutableInt aInteger = m_aNumForDocTypeMap.get(sDocType);
            if (aInteger == null) {
                aInteger = new MutableInt(1);
                m_aNumForDocTypeMap.put(sDocType, aInteger);
            } else {
                aInteger.increment();
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void docProcessorDisposed(final RT2DocProcessor aDisposedInstance) {
        m_aNum.decrementAndGet();

        final String sDocType = aDisposedInstance.getDocTypeIdentifier();
        synchronized(m_aNumForDocTypeMap) {
            MutableInt aInteger = m_aNumForDocTypeMap.get(sDocType);
            if (aInteger == null) {
                log.warn("RT2StatisticDocManager: docProcessorDisposed received but no number instance available");
            } else {
                aInteger.decrement();
            }
        }
    }

	//-------------------------------------------------------------------------
    public int getNumberOfDocs() {
        return m_aNum.get();
    }

    //-------------------------------------------------------------------------
    public DocStatistics getInstance() {
        return Holder.INSTANCE;
    }

    //-------------------------------------------------------------------------
    @Override
    public int getNumberOfOpenDocuments() {
        return m_aNum.get();
    }

    //-------------------------------------------------------------------------
    @Override
    public int getNumberOfOpenDocuments(String sDocType) {
        synchronized (m_aNumForDocTypeMap) {
            final MutableInt aInteger = m_aNumForDocTypeMap.get(sDocType);
            return (aInteger != null) ? aInteger.getValue() : 0;
        }
    }

}
