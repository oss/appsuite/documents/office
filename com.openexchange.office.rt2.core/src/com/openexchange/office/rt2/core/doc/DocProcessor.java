/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.doc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.exception.RT2InvalidClientUIDException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.sequence.SequenceDocProcessor;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.user.LoadState;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.common.user.UserDataManager;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

//=============================================================================
public abstract class DocProcessor extends SequenceDocProcessor
{
    //-------------------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(DocProcessor.class);

    //-------------------------------------------------------------------------
	private final UserDataManager m_aUserData       = new UserDataManager();

    //-------------------------------------------------------------------------
    private final ClientsStatus   m_aClientsStatus;

    //-------------------------------------------------------------------------
    private final DocumentStatus  m_aDocumentStatus;

    //-------------------------------------------------------------------------
    private final Object          m_aStatusSync     = new Object();

    //-------------------------------------------------------------------------
	public DocProcessor() throws Exception
	{
		super();

		m_aDocumentStatus = createDocumentStatus();
		m_aClientsStatus  = createClientsStatus();
	}

    //-------------------------------------------------------------------------
	protected Object getStatusSyncObject()
	{
		return m_aStatusSync;
	}

    //-------------------------------------------------------------------------
	public UserData getUserData(final RT2CliendUidType sClientUID)
	{
		return m_aUserData.getUserData(sClientUID.getValue());
	}

    //-------------------------------------------------------------------------
	@Override
	public List<UserData> getUserDatasCloned()
	{
		return m_aUserData.getUserDataCloned();
	}

    //-------------------------------------------------------------------------
	public int numOfUsers() {
		return this.m_aUserData.getCount();
	}

    //-------------------------------------------------------------------------
	public UserData getUserDataThrowIfUnknown(final RT2CliendUidType sClientUID) throws RT2Exception {
		final UserData userData = getUserData(sClientUID);
		if (userData == null) {
            final String logMessage = "no user data for com.openexchange.rt2.client.uid [" + sClientUID + "]";
            final RT2TypedException ex = new RT2InvalidClientUIDException(new ArrayList<>(), "details", logMessage);
            throw ex;
		}
		return userData;
	}

    //-------------------------------------------------------------------------
	public UserData getMemberUserData(final String sClientUID)
	{
		return m_aUserData.getMemberUserData(sClientUID);
	}

    //-------------------------------------------------------------------------
	public List<String> getUserIDsFiltered(final Predicate<UserData> aFilter)
	{
		List<String> aFilteredList = m_aUserData.getFilteredUserData(aFilter);
		return aFilteredList;
	}

    //-------------------------------------------------------------------------
	public ClientsStatus getClientsStatus()
	{
		return m_aClientsStatus;
	}

    //-------------------------------------------------------------------------
	public ClientsStatus getClientsStatusClone() 
	{
		synchronized (m_aStatusSync)
		{
			try {
				return m_aClientsStatus.clone();
			} catch (CloneNotSupportedException e) {
				// Should never be happened
				throw new RuntimeException(e);
			}
		}
	}

    //-------------------------------------------------------------------------
	public ClientsStatus getClientsStatusCloneIfModified(boolean bResetModifiedState) throws CloneNotSupportedException
	{
		ClientsStatus result = null;

		synchronized (m_aStatusSync)
		{
			if (m_aClientsStatus.isModified())
			{
				result = m_aClientsStatus.clone();
				if (bResetModifiedState)
					m_aClientsStatus.clearModifications();
			}
		}

		return result;
	}

    //-------------------------------------------------------------------------
	public DocumentStatus getDocumentStatus()
	{
		return m_aDocumentStatus;
	}

    //-------------------------------------------------------------------------
	public DocumentStatus getDocumentStatusClone() throws CloneNotSupportedException
	{
		synchronized (m_aStatusSync)
		{
			return m_aDocumentStatus.clone();
		}
	}

    //-------------------------------------------------------------------------
	public DocumentStatus getDocumentStatusCloneIfModified(boolean bResetModifiedState) throws CloneNotSupportedException
	{
		DocumentStatus result = null;

		synchronized (m_aStatusSync)
		{
			if (m_aDocumentStatus.isModified())
			{
				result = m_aDocumentStatus.clone();
				if (bResetModifiedState)
					m_aDocumentStatus.setModified(false);
			}
		}

		return result;
	}

    //-------------------------------------------------------------------------
	@Override
	public List<DocProcessorClientInfo> getClientsInfo()
	{
		List<DocProcessorClientInfo> aClientsInfo = new ArrayList<>();

		final List<UserData> aUserDataList = m_aUserData.getUserData();
		for (final UserData aUserData : aUserDataList) {
			aClientsInfo.add(new DocProcessorClientInfo(new RT2CliendUidType(aUserData.getUserId()), aUserData.getNodeUUID()));
		}
		return aClientsInfo;
	}

	//-------------------------------------------------------------------------
    @Override
	public void updateClientsInfo(Collection<RT2CliendUidType> existingClients) {
    	if (!m_aUserData.updateUserData(existingClients.stream().map(c -> c.getValue()).collect(Collectors.toSet()))) {
    		specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "updateClientsInfo: " + m_aUserData.getUserDataCloned());
    	}
	}

	//-------------------------------------------------------------------------    
	@Override
	public boolean onCloseAndLeave(RT2Message aMsg) throws Exception {
		onCloseDoc(aMsg);
		onLeave(aMsg);
		final RT2Message aResponse  = RT2MessageFactory.createResponseFromMessage(aMsg, RT2MessageType.RESPONSE_CLOSE_AND_LEAVE);
		messageSender.sendMessageWOSeqNrTo (aMsg.getClientUID(), aResponse);
		return true;
	}
    
    
	//-------------------------------------------------------------------------
    @Override
    public boolean onJoin(final RT2Message aRequest) throws Exception
    {
        final String sFolderID    = RT2MessageGetSet.getFolderID (aRequest);
        final String sFileID      = RT2MessageGetSet.getFileID   (aRequest);
        final String sSessionID   = RT2MessageGetSet.getSessionID(aRequest);
        final String sNodeUID     = RT2MessageGetSet.getNodeUUID (aRequest);

        addUser(aRequest.getClientUID(), sFolderID, sFileID, sSessionID, sNodeUID);

        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean onLeave(final RT2Message aRequest) throws Exception
    {
    	if (!RT2MessageType.REQUEST_CLOSE_AND_LEAVE.equals(aRequest.getType())) {
    		final RT2Message aResponse  = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_LEAVE);
    		messageSender.sendMessageWOSeqNrTo (aRequest.getClientUID(), aResponse);
    	}

        removeUser(aRequest.getClientUID());

        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean onEmergencyLeave(final RT2Message aRequest) throws Exception
    {
        final RT2Message aResponse  = RT2MessageFactory.createResponseFromMessage(aRequest, RT2MessageType.RESPONSE_EMERGENCY_LEAVE);

        messageSender.sendMessageWOSeqNrTo (aRequest.getClientUID(), aResponse);

        removeUser(aRequest.getClientUID());

        return true;
    }


    //-------------------------------------------------------------------------
	protected int getUserCount()
	{
		return m_aUserData.getCount();
	}

    //-------------------------------------------------------------------------
	public void addUser(RT2CliendUidType sClientUID, String sFolderID, String sFileID, String sSessionID, String sNodeUUID)
	    throws Exception
	{
        synchronized(m_aUserData)
        {
            UserData aUserData  = m_aUserData.getUserData(sClientUID.getValue());
            if (aUserData == null)
            {
                aUserData  = new UserData(sClientUID.getValue(), sFolderID, sFileID, sSessionID, false, null, sNodeUUID);
                if (m_aUserData.addUser(sClientUID.getValue(), aUserData)) {
                	specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "Add user with com.openexchange.rt2.client.uid {}", sClientUID);
                }
            }
        }

        final ServerSession aSession = sessionService.getSession4Id(sSessionID);
        if (aSession == null) {
        	throw new RT2TypedException(ErrorCode.GENERAL_SESSION_INVALID_ERROR, rt2MessageLoggerService.getMsgsAsList(getDocUID()));
        }
        final User   aUserInfo    = aSession.getUser();
        final String sDisplayName = UserHelper.getFullName(aUserInfo);

        synchronized (m_aStatusSync)
        {
            m_aClientsStatus.addClient(sClientUID, sDisplayName, aUserInfo.getId(), aUserInfo.isGuest(), null);
        }
	}

    //-------------------------------------------------------------------------
	protected void updateUserData(String sClientUID, String sEncryptionInfo)
        throws Exception
	{
        boolean bUserFound = false;

        synchronized(m_aUserData)
        {
            final UserData aUserData  = m_aUserData.getUserData(sClientUID);
            if (aUserData != null)
            {
                bUserFound = true;
                aUserData.setEncryptionInfo(sEncryptionInfo);
            }
        }

        if (!bUserFound)
            log.warn("Unknown com.openexchange.rt2.client.uid {}", sClientUID);
    }

    //-------------------------------------------------------------------------
	protected void updateUserData(RT2CliendUidType sClientUID, LoadState eLoadState)
        throws Exception
	{
        boolean bUserFound = false;

        final UserData aUserData  = m_aUserData.getUserData(sClientUID.getValue());
        if (aUserData != null) {
            bUserFound = true;
            aUserData.setLoadState(eLoadState);
        }

        if (!bUserFound)
            log.warn("Unknown com.openexchange.rt2.client.uid {}", sClientUID);
	}

    //-------------------------------------------------------------------------
    public void removeUser(RT2CliendUidType sClientUID)
    {
        if (m_aUserData.removeUser(sClientUID.getValue())) {
        	specialLogService.log(Level.DEBUG, log, getDocUID().getValue(), new Throwable(), "Removed user with com.openexchange.rt2.client.uid {}", sClientUID);
        }

        synchronized (m_aStatusSync)
        {
        	m_aClientsStatus.removeClient(sClientUID);
        }
    }

    //-------------------------------------------------------------------------
    protected abstract DocumentStatus createDocumentStatus() throws Exception;

    //-------------------------------------------------------------------------
    protected abstract ClientsStatus createClientsStatus() throws Exception;
}
