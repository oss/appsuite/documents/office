/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorLogInfo.Direction;
import com.openexchange.office.rt2.core.exception.RT2InvalidDocumentIdentifierException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueue;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BodyType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ErrorCodeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.SequenceNrType;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageFlags;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class RT2MessageSender {

	private static final Logger log = LoggerFactory.getLogger(RT2MessageSender.class);

    //---------------------------Services--------------------------------------
	@Autowired
	private DocProcessorMessageLoggerService messageLoggerService;

	@Autowired
	private RT2JmsMessageSender jmsMessageSender;

	@Autowired
	private ClientEventService clientEventService;

	@Autowired
	private ClientSequenceQueueDisposerService clientSequenceQueueDisposerService;

	@Autowired
	private MsgBackupAndAckProcessorService msgBackupAndAckProcessorService;

	//-------------------------------------------------------------------------
    /** send out a normal response ...
     *
     *  @param  response [IN]
     *          the response to be routed back to client.
     */
    public void sendResponseToClient (final RT2CliendUidType clientUID, final RT2Message response)
    {
    	log.debug("sendResponseToClient with com.openexchange.rt2.client.uid {} and response {}", clientUID, response.getType());
    	messageLoggerService.addMessageForQueueToLog(Direction.TO_JMS, response);

    	// There should be no issue that notifyClientRemoved() is triggered before
    	// the special responses (a client is leaving a DocProcessor instance)
    	// are sent via JMS to the specific MW node that contains the RT2DocProxy instance.
    	// The life-time of the RT2DocProxy is connected to the WebSocket channel
    	// or it receives the final response (RESPONSE_CLOSE_AND_LEAVE, RESPONSE_EMERGENCY_LEAVE
    	// or RESPONSE_LEAVE). The life time of the JMS topic is bound to the RT2 OSGi services
    	// and processOutMessage just puts the response into the client response topic (without
    	// interacting with other client or doc processor specific resources).
    	// Therefore its MNANDATORY that procesOutMessage() sticks to this constraint to keep
    	// this requirement fulfilled.
    	//
    	// OLD comment:
    	//    as leave response e.g. deregister our recipient channel ...
    	//    we have to call this methods after processOutMessage !
        switch (response.getType()) {
        	case RESPONSE_CLOSE_AND_LEAVE:
        	case RESPONSE_EMERGENCY_LEAVE:
            case RESPONSE_LEAVE: {
            	log.debug("sendResponseToClient notifyClientRemoved for com.openexchange.rt2.document.uid {} and with com.openexchange.rt2.client.uid {} due to response {}", response.getDocUID(), clientUID, response.getType());
            	clientEventService.notifyClientRemoved(new ClientEventData(response.getDocUID(), clientUID));
                break;
            }
            default:
        }

        // DOCS-3009 ensure that we process response messages after a possible
        // RESPONSE_CLOSE_AND_LEAVE, RESPONSE_EMERGENCY_LEAVE or RESPONSE_LEAVE
        // notification to prevent timing issues where the same client tries to
        // reload the same document again.
        processOutMessage (response);
    }

    //-------------------------------------------------------------------------
    /** send out an error response ...
     *
     *  If no error code nor an exception is given an UNKNOEN_ERROR is send.
     *
     *  @param  sClientUID [IN, OPTIONAL]
     *          the client UID where this message has to be send to
     *          If it's not given - it's retrieved from the original request.
     *          But at least any client UID must be given - otherwise error message
     *          can't be send.
     *
     *  @param  aOrgRequest [IN, OPTIONAL]
     *          the original request where this error occurred and
     *          where the error response has to be send for
     *          If no such request is given a new error response
     *          is generated from scratch.
     *
     *  @param  aError [IN, OPTIONAL]
     *          the error code to be send
     *
     *  @param  aException [IN, OPTIONAL]
     *          the exception to be analyzed
     */
    public void sendErrorResponseToClient (final RT2CliendUidType     sClientUID ,
                                                                 final RT2Message aOrgRequest,
                                                                 final ErrorCode  aError     ,
                                                                 final Throwable  aException )
    {
        final RT2Message aErrorResponse = RT2MessageFactory.createResponseFromMessage(aOrgRequest, RT2MessageType.RESPONSE_GENERIC_ERROR);
              RT2CliendUidType sSendTo = sClientUID;
              ErrorCode aSendError = null;

        if (aOrgRequest != null) {
            sSendTo = aOrgRequest.getClientUID();
        }

        if (aError != null) {
            aSendError = aError;
        } else {
            if (aException != null) {
                if (RT2TypedException.class.isAssignableFrom(aException.getClass ())) {
                    final RT2TypedException aRTEx = (RT2TypedException) aException;
                    aSendError = aRTEx.getError();
                } else {
                    aSendError = ErrorCode.GENERAL_UNKNOWN_ERROR;
                }
            } else {
                aSendError = ErrorCode.GENERAL_UNKNOWN_ERROR;
            }
        }

        // set error and clear seq-nr - an error response cannot use the cloned one
        RT2MessageGetSet.setError(aErrorResponse, aSendError);
        RT2MessageGetSet.clearSeqNumber(aErrorResponse);

        sendMessageWOSeqNrTo(sSendTo, aErrorResponse);
    }


	//-------------------------------------------------------------------------
	public void sendMessageTo(final RT2CliendUidType sTo, final RT2Message rMsg) {
	    if (clientSequenceQueueDisposerService.setSeqNrOnMsg(sTo, rMsg)) {
	    	try {
				msgBackupAndAckProcessorService.backupMessage(rMsg);
			} catch (RT2InvalidDocumentIdentifierException e) {
				// Should never occur
				throw new RuntimeException(e);
			}
	    	sendResponseToClient    (sTo, rMsg);
	    }
	}

    //-------------------------------------------------------------------------
    public void sendMessageWOSeqNrTo(final RT2CliendUidType sTo, final RT2Message rMsg)  {
        sendResponseToClient(sTo, rMsg);
    }

	//-------------------------------------------------------------------------
	public void broadcastMessageExceptClient(final RT2CliendUidType sExceptClientID, final RT2Message rMsg, final RT2MessageType sMsgType) throws RT2TypedException {
	    final Collection<RT2CliendUidType> aReceiverUIDs = clientSequenceQueueDisposerService.determineReceivers(rMsg.getDocUID(), sExceptClientID);
	    if (!aReceiverUIDs.isEmpty()) {
    		implBroadcastMessage(aReceiverUIDs, rMsg, sMsgType);
	    }
	}

	//-------------------------------------------------------------------------
	public void broadcastMessageTo(final Collection<RT2CliendUidType> aReceiverUIDs, final RT2Message rMsg, final RT2MessageType sMsgType) throws RT2TypedException {
		implBroadcastMessage(aReceiverUIDs, rMsg, sMsgType);
	}

	//-------------------------------------------------------------------------
	private void implBroadcastMessage(final Collection<RT2CliendUidType> aReceiverUIDs, final RT2Message rMsg, final RT2MessageType sMsgType) throws RT2TypedException {
		if (aReceiverUIDs != null) {
    		final List<RT2Message> aBroadcastList = new LinkedList<>();
    		Map<RT2CliendUidType, ClientSequenceQueue> clientsSeqQueues = clientSequenceQueueDisposerService.getClientsSeqQueuesOfDoc(rMsg.getDocUID(), true);
    		synchronized(clientsSeqQueues) {
    			for (final RT2CliendUidType sClientUID : aReceiverUIDs) {
    				final ClientSequenceQueue aClientSeqQueue = clientsSeqQueues.get(sClientUID);
    				if (null != aClientSeqQueue) {

    					final RT2Message msg = RT2MessageFactory.cloneMessage(rMsg, sMsgType);
    					msg.setClientUID(sClientUID);
    					msg.setDocUID(rMsg.getDocUID());

    					if (msg.hasFlags(RT2MessageFlags.FLAG_SEQUENCE_NR_BASED)) {
    						final int nOutSeq = aClientSeqQueue.incAndGetOutSeqNumber();
    						RT2MessageGetSet.setSeqNumber(msg, nOutSeq);
    					}
    					aBroadcastList.add(msg);
    				}
    			}
    		}
    		BroadcastMessage.Builder broadcastMsgBuilder = BroadcastMessage.newBuilder().setBody(BodyType.newBuilder().setValue(rMsg.getBodyString()).build())
    									 .setDocUid(DocUidType.newBuilder().setValue(rMsg.getDocUID().getValue()))
    									 .setMessageId(MessageIdType.newBuilder().setValue(rMsg.getMessageID().getValue()))
    									 .setMsgType(getMessageTypeOfRT2Message(sMsgType));
    		if (RT2MessageGetSet.hasError(rMsg)) {
    			final String errorCodeAsString = rMsg.getError().getValue().getAsJSON().toString();
    			broadcastMsgBuilder.setErrorCode(ErrorCodeType.newBuilder().setValue(errorCodeAsString));
    		}

    		for (final RT2Message aMsg : aBroadcastList) {
    			broadcastMsgBuilder.addReceivers(
    					BroadcastMessageReceiver.newBuilder()
    						.setSeqNr(SequenceNrType.newBuilder().setValue(aMsg.hasSeqNumber() ? aMsg.getSeqNumber().getValue() : -1))
    						.setReceiver(ClientUidType.newBuilder().setValue(aMsg.getClientUID().getValue())));
    			try {
    				if (aMsg.hasFlags(RT2MessageFlags.FLAG_SEQUENCE_NR_BASED))
    				    msgBackupAndAckProcessorService.backupMessage(aMsg);
    				if (aBroadcastList.size() == 1) {
    					sendResponseToClient(aMsg.getClientUID(), aMsg);
    				}
    			} catch (Throwable t) {
    				ExceptionUtils.handleThrowable(t);
    				MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, aMsg.getClientUID().getValue());
    				log.error("RT2: Throwable caught while sending broadcast to client " + aMsg.getClientUID() + ", state unknown!");
                    MDC.remove(MDCEntries.CLIENT_UID);
    			}
    		}
    		if (aBroadcastList.size() > 1) {
	    		BroadcastMessage broadcastMessage = broadcastMsgBuilder.build();
	    		sendBroadcastMessageToClients(broadcastMessage);
    		}
		}
	}

	private MessageType getMessageTypeOfRT2Message(RT2MessageType msgType) {
		switch (msgType) {
			case BROADCAST_CRASHED: return MessageType.BROADCAST_CRASHED;
			case BROADCAST_EDITREQUEST_STATE: return MessageType.BROADCAST_EDITREQUEST_STATE;
			case BROADCAST_HANGUP: return MessageType.BROADCAST_HANGUP;
			case BROADCAST_RENAMED_RELOAD: return MessageType.BROADCAST_RENAMED_RELOAD;
			case BROADCAST_SHUTDOWN: return MessageType.BROADCAST_SHUTDOWN;
			case BROADCAST_UPDATE: return MessageType.BROADCAST_UPDATE;
			case BROADCAST_UPDATE_CLIENTS: return MessageType.BROADCAST_UPDATE_CLIENTS;
			case BROADCAST_OT_RELOAD: return MessageType.BROADCAST_OT_RELOAD;
			default: throw new RuntimeException("Not a broadcast message type: " + msgType);
		}
	}

    private void processOutMessage (final RT2Message msg) {
        jmsMessageSender.sendToClientResponseTopic(msg);
    }

    private void sendBroadcastMessageToClients(BroadcastMessage msg) {
    	jmsMessageSender.sendBroadcastMessage(msg);
    }
}
