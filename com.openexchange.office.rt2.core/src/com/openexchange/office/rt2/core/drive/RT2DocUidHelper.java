/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.MD5;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.codec.digest.DigestUtils;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;

public class RT2DocUidHelper {
    private static final AtomicBoolean init = new AtomicBoolean(false);
    private static final AtomicReference<RT2DocUidHelper> singleton = new AtomicReference<>();

    private DigestUtils md5 = null;

    private RT2DocUidHelper(DigestUtils md5) {
        this.md5 = md5;
    }

    public String calcDocUid(int contextId, RT2FileIdType fileId) {
        String docUidBase = String.valueOf(contextId) + "@" + fileId.getValue();
        String hdigest = md5.digestAsHex(docUidBase.getBytes());
        return hdigest;
    }

    public static RT2DocUidHelper getInstance() {
        if (init.compareAndSet(false, true)) {
            DigestUtils digest = new DigestUtils(MD5);
            singleton.set(new RT2DocUidHelper(digest));
        }
        return singleton.get();
    }

}
