/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;

public class InsertFileEventData extends FileEventData {

    public InsertFileEventData(RT2SessionIdType sessionId, RT2DocUidType docUid, RT2FileIdType fileId, RT2FolderIdType folderId) {
        super(FileEventType.FILE_EVENT_INSERT, sessionId, docUid, fileId, folderId);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put(FileEventDataProperties.PROP_SESSION_ID, getSessionId());
        result.put(FileEventDataProperties.PROP_DOCUID, getDocUid());
        result.put(FileEventDataProperties.PROP_FILE_ID, getFileId());
        return result;
    }

}
