/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;

public abstract class FileEventData {
    private FileEventType type;
    private RT2DocUidType docUid;
    private RT2FileIdType fileId;
    private RT2FolderIdType folderId;
    private RT2SessionIdType sessionId;

    public FileEventData(FileEventType type, RT2SessionIdType sessionId, RT2DocUidType docUid, RT2FileIdType fileId, RT2FolderIdType folderId) {
        this.type = type;
        this.sessionId = sessionId;
        this.docUid = docUid;
        this.fileId = fileId;
        this.folderId = folderId;
    }

    public RT2FileIdType getFileId() {
        return fileId;
    }

    public RT2FolderIdType getFolderId() {
        return folderId;
    }

    public RT2DocUidType getDocUid() {
        return docUid;
    }
    
    public FileEventType getType() {
        return type;
    }

    public RT2SessionIdType getSessionId() {
        return sessionId;
    }

    public abstract JSONObject toJSON() throws JSONException;

}
