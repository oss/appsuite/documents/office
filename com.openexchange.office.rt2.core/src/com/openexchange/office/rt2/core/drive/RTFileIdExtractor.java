package com.openexchange.office.rt2.core.drive;

import org.apache.commons.lang3.StringUtils;

public class RTFileIdExtractor {

    public static final String FILE_FOLDER_ID_SEP = "/";

    /**
     * Extracts the realtime file-id part from a infostore file-id
     * which normally consists of <folder-id>'/'<file-id>. This
     * part is unique for the complete filestore.
     *
     * @implNote
     * This implementation uses deep-knowledge about the structure
     * of the infostore file-id. This can breaks as soon as this
     * structure changes.
     *
     * @param fileId a infostore file-id
     * @return the realtime file-id extracted or null if not possible
     */
    public static String extractRTFileIdFromOrigFileId(String fileId) {
        if (StringUtils.isNotEmpty(fileId)) {
            String[] parts = fileId.split(FILE_FOLDER_ID_SEP);
            if (parts.length == 2) {
                return parts[1];
            }
        }
        return null;
    }

}
