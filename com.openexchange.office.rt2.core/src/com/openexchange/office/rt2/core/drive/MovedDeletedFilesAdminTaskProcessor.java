/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

@Component
public class MovedDeletedFilesAdminTaskProcessor implements InitializingBean, DisposableBean, Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(MovedDeletedFilesAdminTaskProcessor.class);
    private static final String MOVED_DELETED_FILES_TASK_PROCESSOR = "RT2DriveEventsTaskProcessor";
    private static final long FREQ_UPDATE_MOVED_DELETE_LIST = 60000;

    @Autowired
    private RT2DocProcessorManager docProcMngr;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private TimerService timerService;

    private BlockingQueue<FilesTask> movedDeletedFilesTasks = new LinkedBlockingQueue<>();
    private AtomicBoolean destroyed = new AtomicBoolean(false);
    private Thread queueProcessorThread = null;
    private ScheduledTimerTask updateCurrentMovedDeletedListTimerTask = null;
    private Queue<String> movedDeletedTaskData = new ConcurrentLinkedQueue<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        updateCurrentMovedDeletedListTimerTask = timerService.scheduleAtFixedRate(new FilesMovedDeletedMetricCollector(movedDeletedTaskData), 
            FREQ_UPDATE_MOVED_DELETE_LIST, FREQ_UPDATE_MOVED_DELETE_LIST);

        queueProcessorThread = new Thread(this);
        queueProcessorThread.setName(MOVED_DELETED_FILES_TASK_PROCESSOR);
        queueProcessorThread.start();
    }

    public List<String> getProcessedMovedDeletedTasks() {
        List<String> result = new ArrayList<>();
        result.addAll(movedDeletedTaskData);
        return result;
    }

    @Override
    public void destroy() throws Exception {
        if (destroyed.compareAndSet(false, true)) {
            ScheduledTimerTask timerTask = updateCurrentMovedDeletedListTimerTask;
            if (timerTask != null) {
                timerTask.cancel();
            }
            Thread thread = queueProcessorThread;
            if (thread != null) {
                try {
                    thread.interrupt();
                    thread.join();
                    queueProcessorThread = null;
                } catch (final InterruptedException e) {
                    LOG.debug("InterruptedException caught preparing shutdown of the MovedDeletedFilesAdminTaskProcessor queue thread", e);
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void addMoveDeleteFilesTask(FilesTask task) {
        LOG.debug("task added to moved/deleted files task queue");
        movedDeletedFilesTasks.add(task);
    }

    @Override
    public void run() {
        try {
            while (!destroyed.get()) {
                FilesTask head = movedDeletedFilesTasks.take();
                if (head.getFilesData().length() > 0) {
                    FilesTaskType type = head.getType();
                    switch (type) {
                        case FILES_MOVED_TASK:
                            MovedFilesTask movedFilesTask = (MovedFilesTask)head;
                            processMovedFilesTask(movedFilesTask);
                            break;
                        case FILES_DELETED_TASK:
                            DeletedFilesTask deletedFilesTask = (DeletedFilesTask)head;
                            processDeletedFilesTask(deletedFilesTask);
                            break;
                        default:
                            LOG.warn("Unknown task type {} - check implementation!", type);
                            break;
                    }
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void processMovedFilesTask(MovedFilesTask movedFilesTask) {
        Map<String, MoveFileEventData> rtIdToMovedFileEventData = new HashMap<>();
        JSONArray movedFilesData = movedFilesTask.getFilesData();
        movedFilesData.stream().forEach(o -> {
            JSONObject json = (JSONObject)o;
            try {
                MoveFileEventData movedFileData = MoveFileEventData.fromJSON(json);
                rtIdToMovedFileEventData.put(movedFileData.getDocUid().getValue(), movedFileData);
            } catch (JSONException e) {
                LOG.info("JSONException caught trying to extract json from admin task!", e);
            }
        });

        Set<WeakReference<RT2DocProcessor>> docProcs = docProcMngr.getWeakReferenceToDocProcessors();
        boolean waitForSaveLock = true;
        docProcs.stream().forEach( wr -> {
            RT2DocProcessor rtDoc = wr.get();
            if ((rtDoc != null) && (rtDoc instanceof EditableDocProcessor) && rtIdToMovedFileEventData.containsKey(rtDoc.getDocUID().getValue())) {

                // document processor references moved file
                RT2DocUidType docUid = rtDoc.getDocUID();
                MoveFileEventData moveFileEventData = rtIdToMovedFileEventData.get(rtDoc.getDocUID().getValue());
                String sessionId = moveFileEventData.getSessionId().getValue();
                Session session = null;

                try {
                    session = sessionService.getSession4Id(sessionId);
                } catch (OXException e) {
                    LOG.info("Exception caught trying to get Session object for session id {}", sessionId);
                }

                EditableDocProcessor doc = (EditableDocProcessor)rtDoc;
                ErrorCode errorCode = (session != null) ? ErrorCode.NO_ERROR : ErrorCode.GENERAL_SESSION_INVALID_ERROR;
                if (session != null) {
                    try {
                        errorCode = doc.safteySaveDocumentDueToMove(session, moveFileEventData.getToFolderId(), moveFileEventData.getToFileId(), waitForSaveLock);
                    } catch (Exception e) {
                        errorCode = ErrorCode.SAVEDOCUMENT_FAILED_ERROR;
                        if (e instanceof OXException) {
                            errorCode = ExceptionToErrorCode.map((OXException)e, errorCode, false);
                        }
                        LOG.info("Exception caught trying to save docment {} due to moved file!", docUid.getValue());
                    }

                    movedDeletedTaskData.add(getMovedFileMetricDataString(moveFileEventData, errorCode.getCode()));
                }

                doc.handleMovedFile(moveFileEventData.getToFolderId(), moveFileEventData.getToFileId(), errorCode);
            }
        }); 
    }

    private void processDeletedFilesTask(DeletedFilesTask deletedFilesTask) {
        Map<String, DeleteFileEventData> rtIdToDeletedFileEventData = new HashMap<>();
        JSONArray deletedFilesData = deletedFilesTask.getFilesData();
        deletedFilesData.stream().forEach(o -> {
            JSONObject json = (JSONObject)o;
            try {
                DeleteFileEventData deletedFileData = DeleteFileEventData.fromJSON(json);
                rtIdToDeletedFileEventData.put(deletedFileData.getDocUid().getValue(), deletedFileData);
            } catch (JSONException e) {
                LOG.info("JSONException caught trying to extract json from admin task!", e);
            }
        });

        Set<WeakReference<RT2DocProcessor>> docProcs = docProcMngr.getWeakReferenceToDocProcessors();
        docProcs.stream().forEach( wr -> {
            RT2DocProcessor rtDoc = wr.get();
            if ((rtDoc != null) && (rtDoc instanceof EditableDocProcessor) && rtIdToDeletedFileEventData.containsKey(rtDoc.getDocUID().getValue())) {
                
                // document processor references deleted file -> all clients must be
                // notified via hang-up
                DeleteFileEventData delEvData = rtIdToDeletedFileEventData.get(rtDoc.getDocUID().getValue());
                EditableDocProcessor doc = (EditableDocProcessor)rtDoc;
                try {
                    doc.handleDeletedDocFile();
                } catch (Exception e) {
                    LOG.info("Exception caught trying to handle detected deleted document file for com.openexchange.rt2.document.uid" + doc.getDocUID(), e);
                }
                movedDeletedTaskData.add(getDeletedFileMetricDataString(delEvData));
            }
        });
    }

    private static String getMovedFileMetricDataString(MoveFileEventData moveFileEventData, int errorCode) {
        StringBuilder tmp = new StringBuilder("FileMoved doc-uid=");
        tmp.append(moveFileEventData.getDocUid());
        tmp.append(", moved to file-id=");
        tmp.append(moveFileEventData.getToFileId());
        tmp.append(", errCode=");
        tmp.append(errorCode);
        return tmp.toString();
    }

    private static String getDeletedFileMetricDataString(DeleteFileEventData delFileEventData) {
        StringBuilder tmp = new StringBuilder("FileDeleted doc-uid=");
        tmp.append(delFileEventData.getDocUid());
        tmp.append(", deleted file-id=");
        tmp.append(delFileEventData.getFileId());
        tmp.append(", errCode=");
        tmp.append(ErrorCode.CODE_GENERAL_FILE_NOT_FOUND_ERROR);
        return tmp.toString();
    }

    private static class FilesMovedDeletedMetricCollector implements Runnable {
        private Queue<String> metricData;

        public FilesMovedDeletedMetricCollector(Queue<String> metricData) {
            this.metricData = metricData;
        }

        @Override
        public void run() {
            metricData.clear();
        }
        
    }
}
