/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;


public class FileEventDataProperties {
    private FileEventDataProperties() {
        // nothing to do
    }

    public static final String PROP_FILE_ID = "id";
    public static final String PROP_FOLDER_ID = "folderId";
    public static final String PROP_SESSION_ID = "sessionId";
    public static final String PROP_TO_FILE_ID = "toId";
    public static final String PROP_TO_FOLDER_ID = "toFolderId";
    public static final String PROP_DOCUID = "docUid";
}
