/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.drive;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Component;
import com.openexchange.file.storage.FileStorageEventConstants;
import com.openexchange.office.rt2.core.jms.RT2AdminJmsConsumer;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.session.Session;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

@Component
public class DriveEventsListenerService implements EventHandler, InitializingBean, DisposableBean {
    private static final Logger LOG = LoggerFactory.getLogger(DriveEventsListenerService.class);
    private static final long FREQ_CHECK_FOR_MOVED_DELETED_FILES = 5000;
    private static final long FREQ_RESET_DRIVE_EVENTS_COUNT = 60000;
    
    @Autowired
    private TimerService timerService;

    @Autowired
    private RT2AdminJmsConsumer adminJmsConsumerService;

    private final Map<String, FileEventData> deleteFileEvents = new ConcurrentHashMap<>();
    private final Map<String, FileEventData> insertFileEvents = new ConcurrentHashMap<>();
    private final AtomicBoolean shutdown = new AtomicBoolean(false);
    private final AtomicLong driveEventsReceived = new AtomicLong(0);
    private ScheduledTimerTask rt2DriveEventsDispatcherTimerTask = null;
    private ScheduledTimerTask driveEventsCountResetTimerTask = null;
    private RT2DocUidHelper rt2DocUidHelper = null;

    @Override
    public void afterPropertiesSet() throws Exception {
        rt2DriveEventsDispatcherTimerTask = timerService.scheduleAtFixedRate(new DriveEventsDispatcher(LOG, adminJmsConsumerService, deleteFileEvents, insertFileEvents), 
            FREQ_CHECK_FOR_MOVED_DELETED_FILES, FREQ_CHECK_FOR_MOVED_DELETED_FILES);
        driveEventsCountResetTimerTask = timerService.scheduleAtFixedRate(new DriveEventsCountResetTask(driveEventsReceived),
            FREQ_RESET_DRIVE_EVENTS_COUNT, FREQ_RESET_DRIVE_EVENTS_COUNT);

        rt2DocUidHelper = RT2DocUidHelper.getInstance();
    }

    @Override
    public void destroy() throws Exception {
        if (shutdown.compareAndSet(false, true)) {
            deleteFileEvents.clear();
            insertFileEvents.clear();
            ScheduledTimerTask timerTask = rt2DriveEventsDispatcherTimerTask;
            if (null != timerTask) {
                timerTask.cancel();
                rt2DriveEventsDispatcherTimerTask = null;
            }
            timerTask = driveEventsCountResetTimerTask;
            if (null != timerTask) {
                timerTask.cancel();
                driveEventsCountResetTimerTask = null;
            }
        }
    }

    public long getDriveEventsReceived() {
        return driveEventsReceived.get();
    }

    @Override
    public void handleEvent(Event event) {
        if (shutdown.get())
            return;

        String eventTopic = event.getTopic();
        if (FileStorageEventConstants.DELETE_TOPIC.equals(eventTopic) ||
            FileStorageEventConstants.CREATE_TOPIC.equals(eventTopic)) {
            driveEventsReceived.incrementAndGet();
            FileEventData fileEventData = createFileEventDataFromEvent(event);

            if (fileEventData != null) {
                String fileId = fileEventData.getFileId().getValue();
                String rtFileId = RTFileIdExtractor.extractRTFileIdFromOrigFileId(fileId);
                switch (event.getTopic()) {
                    case FileStorageEventConstants.DELETE_TOPIC:
                        LOG.debug("FileStorageEvent delete topic file-id {}", fileId);
                        deleteFileEvents.put(rtFileId, fileEventData); break;
                    case FileStorageEventConstants.CREATE_TOPIC:
                        LOG.debug("FileStorageEvent insert topic file-id {}", fileId);
                        insertFileEvents.put(rtFileId, fileEventData); break;
                    default: break;
                }
            }
        }
    }

    private FileEventData createFileEventDataFromEvent(Event event) {
        FileEventData result = null;

        String fileId = (String)event.getProperty(FileStorageEventConstants.OBJECT_ID);
        String folderId = (String)event.getProperty(FileStorageEventConstants.FOLDER_ID);
        Session session = (Session)event.getProperty(FileStorageEventConstants.SESSION);
        String sessionId = session.getSessionID();
        String rtFileId = RTFileIdExtractor.extractRTFileIdFromOrigFileId(fileId);
        String docUid = rt2DocUidHelper.calcDocUid(session.getContextId(), new RT2FileIdType(rtFileId));

        if (StringUtils.isNotEmpty(fileId)) {
            switch (event.getTopic()) {
                case FileStorageEventConstants.DELETE_TOPIC:
                    result = new DeleteFileEventData(new RT2SessionIdType(sessionId), new RT2DocUidType(docUid), new RT2FileIdType(fileId)); break;
                case FileStorageEventConstants.CREATE_TOPIC:
                    result = new InsertFileEventData(new RT2SessionIdType(sessionId), new RT2DocUidType(docUid), new RT2FileIdType(fileId), new RT2FolderIdType(folderId)); break;
                default: break;
            }
        }

        return result;
    }

    private class DriveEventsCountResetTask implements Runnable {
        private final AtomicLong driveEventsCounter;

        public DriveEventsCountResetTask(AtomicLong driveEventsCounter) {
            this.driveEventsCounter = driveEventsCounter;
        }

        @Override
        public void run() {
            driveEventsCounter.set(0);
        }
    }

    private class DriveEventsDispatcher implements Runnable {

        private final Map<String, FileEventData> deleteFilesMap;
        private final Map<String, FileEventData> insertFilesMap;
        private final Logger logger;
        private final RT2AdminJmsConsumer adminJmsConsumer;

        public DriveEventsDispatcher(Logger logger, RT2AdminJmsConsumer adminJmsConsumer, Map<String, FileEventData> deleted, Map<String, FileEventData> inserted) {
            this.logger = logger;
            this.adminJmsConsumer = adminJmsConsumer;
            this.deleteFilesMap = deleted;
            this.insertFilesMap = inserted;
        }

        @Override
        public void run() {
            Map<String, FileEventData> movedMap = new HashMap<>();
            Map<String, FileEventData> deletedMap = new HashMap<>();
            Set<String> deletedKeys = new HashSet<>();
            Set<String> insertedKeys = new HashSet<>();

            try {
                synchronized (insertFilesMap) {
                    insertFilesMap.keySet().stream().forEach( k -> insertedKeys.add(k));
                }
                synchronized (deleteFilesMap) {
                    deleteFilesMap.keySet().stream().forEach(k -> deletedKeys.add(k));
                }
                // detect and store moved files
                insertedKeys.stream().forEach(k -> {
                    FileEventData evDel = deleteFilesMap.get(k);
                    if (evDel != null) {
                        FileEventData evIns = insertFilesMap.get(k);
                        
                        MoveFileEventData moveEv = new MoveFileEventData(evDel.getSessionId(), evDel.getDocUid(), evDel.getFileId(), evDel.getFolderId(), evIns.getFileId(), evIns.getFolderId());
                        movedMap.put(k, moveEv);
                        deletedKeys.remove(k);
                    }
                });
                // store deleted files
                deletedKeys.stream().forEach(k -> {
                    deletedMap.put(k, deleteFilesMap.get(k));
                });
            } finally {
                deleteFilesMap.clear();
                insertFilesMap.clear();
            }

            if ((insertedKeys.size() > 0) || (deletedKeys.size() > 0)) {
                final RT2Message filesMovedDeleted = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_TASK_FILES_MOVED_DELETED);

                final JSONObject body = new JSONObject();
                final JSONArray moved = new JSONArray();
                final JSONArray deleted = new JSONArray();

                movedMap.keySet().stream().forEach(k -> {
                    try {
                        moved.put(movedMap.get(k).toJSON());
                    } catch (JSONException e) {
                        // ignore it
                    }
                });

                deletedMap.keySet().stream().forEach(k -> {
                    try {
                        deleted.put(deletedMap.get(k).toJSON());
                    } catch (JSONException e) {
                        // ignore it
                    }
                });

                try {
                    body.put(RT2Protocol.BODYPARTS_MOVED, moved);
                    body.put(RT2Protocol.BODYPARTS_DELETED, deleted);
                    filesMovedDeleted.setBody(body);
                    adminJmsConsumer.send(filesMovedDeleted);
                } catch (JSONException e) {
                    logger.info("JSONException caught trying to create ADMIN_TASK_FILES_MOVED_DELETED message for mw nodes", e);
                } catch (JmsException e) {
                    logger.info("JmsException caught trying to send ADMIN_TASK_FILES_MOVED_DELETED message to mw nodes", e);
                } catch (Exception e) {
                    logger.warn("Exception caught trying to send/create ADMIN_TASK_FILES_MOVED_DELETED", e);
                }
            }
        }
    }

}
