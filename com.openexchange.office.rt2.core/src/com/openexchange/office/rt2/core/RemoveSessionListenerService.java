/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.openexchange.log.LogProperties;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageTimeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ServerIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.SessionIdType;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondEventConstants;

@Component
public class RemoveSessionListenerService implements EventHandler {

	private static final Logger log = LoggerFactory.getLogger(RemoveSessionListenerService.class);

	@Autowired
	private RT2JmsMessageSender jmsMessageSender;

	@Autowired
	private ClusterService clusterService;

	@Autowired
	private SessionService sessionService;

	@Override
	public void handleEvent(Event event) {
		Session session = (Session) event.getProperty(SessiondEventConstants.PROP_SESSION);
		final String sessionId = session.getSessionID();
		try {
		    MDCHelper.safeMDCPut(LogProperties.Name.SESSION_SESSION_ID.getName(), sessionId);
	        log.info("Received session removed event for session id {}", sessionId);
		} finally {
		    MDC.remove(LogProperties.Name.SESSION_SESSION_ID.getName());
		}

		sessionService.incSessionInvalidNotification(sessionId);
		AdminMessage adminRequest = AdminMessage.newBuilder()
				.setMsgType(AdminMessageType.ADMIN_TASK_SESSION_REMOVED)
				.setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
				.setServerId(ServerIdType.newBuilder().setValue(clusterService.getLocalMemberUuid()))
				.setOriginator(ServerIdType.newBuilder().setValue(clusterService.getLocalMemberUuid()))
				.setSessionId(SessionIdType.newBuilder().setValue(session.getSessionID()).build())
				.build();
		jmsMessageSender.sendAdminMessage(adminRequest);
	}
}
