/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.osgi;

import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jmx.JmxReporter;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceNotActiveException;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageEventConstants;
import com.openexchange.health.MWHealthCheckService;
import com.openexchange.office.rt2.core.RT2Constants;
import com.openexchange.office.rt2.core.RemoveSessionListenerService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessorEventService;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.EmergencySaver;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.doc.IDocNotificationHandler;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.doc.PresenterDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.core.drive.DriveEventsListenerService;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealth;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthMap;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthState;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.config.ConfigurationChecker;
import com.openexchange.sessiond.SessiondEventConstants;
import com.openexchange.sessiond.SessiondService;

public class RT2CoreContextAndActivator extends OsgiBundleContextAndActivator {

	private static final Logger log = LoggerFactory.getLogger(RT2CoreContextAndActivator.class);
	
	private JmxReporter jmxReporter;
	
	private RT2ConfigService rt2ConfigService;
	
	private ClientEventService clientEventService = null;
	private DocProcessorEventService docProcessorEventService = null;
	private RT2DocStateNotifier docStateNotifier = null;
	
	private CachingFacade cachingFacade;
	private ClusterService clusterService;
	
	public RT2CoreContextAndActivator() {
		super(new RT2CoreBundleContextIdentificator());
	}

	@Override
    public void registerAllListenersToEventServices() {
		if (clientEventService == null) {
			log.error("ClientEventService is null!!!");
			throw new RuntimeException("ClientEventService is null!!!");
		}
		if (docProcessorEventService == null) {
			log.error("DocProcessorEventService is null!!!");
			throw new RuntimeException("DocProcessorEventService is null!!!");
		}
		if (docStateNotifier == null) {
			log.error("RT2DocStateNotifier is null!!!");
			throw new RuntimeException("RT2DocStateNotifier is null!!!");			
		}
		services.forEach(s -> registerServiceToEventServices(s));
	}
	
	@Override
    public void registerServiceToEventServices(Object service) {
		registerListenerToClientEventService(service);
		registerListenerToDocProcessorEventService(service);
		registerListenerToDocStateNotifierService(service);
	}
	
	private void registerListenerToClientEventService(Object service) {
		if (service instanceof IClientEventListener) {
			clientEventService.addListener((IClientEventListener) service);
		}
	}
	
	private void registerListenerToDocProcessorEventService(Object service) {
		if (service instanceof IDocProcessorEventListener) {
			docProcessorEventService.addListener((IDocProcessorEventListener) service);
		}
	}
	
	private void registerListenerToDocStateNotifierService(Object service) {
		if (service instanceof IDocNotificationHandler) {
			docStateNotifier.addDocNotificationHandler((IDocNotificationHandler) service);
		}
	}
	
	@Override
	public boolean registerService(Object service, boolean doInject) {
		boolean res = super.registerService(service, doInject);
		if (res) {
			if (service instanceof ClientEventService) {
				clientEventService = (ClientEventService) service;
			} else {
				if (service instanceof DocProcessorEventService) {
					docProcessorEventService = (DocProcessorEventService) service;
				} else {
					if (service instanceof RT2DocStateNotifier) {
						docStateNotifier = (RT2DocStateNotifier) service;
					}
				}
			}
		}
		return res;
	}
	
	@Override
	protected Set<Class<?>> getAdditionalNeededServices() {
		Set<Class<?>> res = new HashSet<>();
		res.add(HazelcastInstance.class);
		res.add(RT2NodeHealthMap.class);
		res.add(SessiondService.class);
		res.add(MWHealthCheckService.class);
		res.add(ConfigurationChecker.class);
		return res;
	}
	
	@Override
	protected Set<Class<?>> getAdditionalDependenciesOfClasses() {
		Set<Class<?>> res = new HashSet<>();
		res.add(EditableDocProcessor.class);
		res.add(PresenterDocProcessor.class);
		return res;
	}

	@Override
	protected Set<Class<?>> getServicesOfModuleWithForeignPackage() {
		Set<Class<?>> res = new HashSet<>();
		res.add(MetricRegistry.class);
		return res;
	}

	@Override
	protected void onPreCreateInstances() throws OXException {
		cachingFacade = getExternalDependency(CachingFacade.class, this.getClass(), false);
		cachingFacade.getDistributedMap(RT2Constants.RT2_CLIENT_COUNTER_MAP, RT2DocUidType.class, Set.class);
		
		clusterService = getExternalDependency(ClusterService.class, this.getClass(), false);

		log.info("... register to cluster full member count");
        boolean bFullMember = !clusterService.isLocalMemberLiteMember();
        if (bFullMember)
        {
            final DistributedAtomicLong aClusterMemberCount = cachingFacade.getDistributedAtomicLong(RT2Constants.RT2_CLUSTER_FULL_MEMBER_COUNT);
            aClusterMemberCount.incrementAndGet();
        }

        rt2ConfigService = new RT2ConfigService();
        registerService(rt2ConfigService, true);
        
        log.debug("... register rt2 node to node health map");
        final RT2NodeHealthMap   aRT2NodeHealthMap = this.getExternalDependency(RT2NodeHealthMap.class, this.getClass(), false);        
        final RT2NodeHealthState aNodeHealthState  = new RT2NodeHealthState(clusterService.getLocalMemberUuid(),
        																	rt2ConfigService.getOXNodeID(),
                                                                            RT2NodeHealth.RT2_NODE_HEALTH_UP,
                                                                            RT2NodeHealth.getNodeTypeString(bFullMember),
                                                                            RT2NodeHealth.RT2_CLEANUP_UUID_EMPTY);
        aRT2NodeHealthMap.set(clusterService.getLocalMemberUuid(), aNodeHealthState);

        log.info("... activate metrics");
        MetricRegistry metricRegistry = new MetricRegistry();
        jmxReporter = JmxReporter.forRegistry(metricRegistry).inDomain("com.openexchange.office.rt2.metrics").build();
        jmxReporter.start();
        registerModuleForeignService(metricRegistry);       
  	}

	@SuppressWarnings("unused")
    @Override
	protected void onAfterCreateInstances() throws OXException {
        log.info("Registering session removed listener...");
        RemoveSessionListenerService sessionGoneListener = getService(RemoveSessionListenerService.class);
        final Dictionary<String, Object> sessionEventsProperties = new Hashtable<String, Object>(1);
        sessionEventsProperties.put(EventConstants.EVENT_TOPIC, new String[] { SessiondEventConstants.TOPIC_REMOVE_SESSION });
        context.registerService(new String[] { RemoveSessionListenerService.class.getName() , EventHandler.class.getName() }, sessionGoneListener, sessionEventsProperties);

        log.info("Registering drive events listener ...");
        DriveEventsListenerService driveEventsListener = getService(DriveEventsListenerService.class);
        final Dictionary<String, Object> driveEventsProperties = new Hashtable<String, Object>(1);
        driveEventsProperties.put(EventConstants.EVENT_TOPIC, new String[] { FileStorageEventConstants.DELETE_TOPIC, FileStorageEventConstants.CREATE_TOPIC });
        context.registerService(new String[] { DriveEventsListenerService.class.getName() , EventHandler.class.getName() }, driveEventsListener, driveEventsProperties);

        // use check configuration service to check current configuration settings - issues
        // will be logged by the service itself
        ConfigurationChecker confChecker = getService(ConfigurationChecker.class);
        confChecker.checkConfiguration();
	}

	@Override
	public void shutdown() throws OXException {
		log.info("... decrease rt2 atomic full-member count");
		boolean bFullMember = false;
		boolean hazelcastInstanceActive = true;
    	try {
    		bFullMember = !clusterService.isLocalMemberLiteMember();
    		if (bFullMember) {
	            final DistributedAtomicLong aClusterMemberCount = cachingFacade.getDistributedAtomicLong(RT2Constants.RT2_CLUSTER_FULL_MEMBER_COUNT);
	            aClusterMemberCount.decrementAndGet();
        	}
    	} catch (HazelcastInstanceNotActiveException e) {
    		log.warn("HazelcastInstanceNotActiveException caught trying to decrement cluster memebr count via Hazelcast", e);
    		hazelcastInstanceActive = false;
        } catch (Throwable t) {
        	ExceptionUtils.handleThrowable(t);
        	log.warn("Exception caught trying to decrement cluster memebr count via Hazelcast", t);
        }

        log.info("... update rt2 node health map with new shutdown state");

        String sNodeUUID = "";
        RT2NodeHealthMap aRT2NodeHealthMap = this.getService(RT2NodeHealthMap.class);
        if (hazelcastInstanceActive) {
	        try {
	            sNodeUUID = clusterService.getLocalMemberUuid(); 
	            RT2NodeHealthState aNodeHealthState  = new RT2NodeHealthState(sNodeUUID,
	               	  														  rt2ConfigService.getOXNodeID(),
	                                                                          RT2NodeHealth.RT2_NODE_HEALTH_SHUTTING_DOWN,
	                                                                          RT2NodeHealth.getNodeTypeString(bFullMember),
	                                                                          RT2NodeHealth.RT2_CLEANUP_UUID_EMPTY);
	            aRT2NodeHealthMap.set(sNodeUUID, aNodeHealthState);
	    	} catch (HazelcastInstanceNotActiveException e) {
	    		log.warn("HazelcastInstanceNotActiveException caught trying to set shutting down state in Hazelcast node health map", e);
	    		hazelcastInstanceActive = false;
	        } catch (Throwable t) {
	        	ExceptionUtils.handleThrowable(t);
	    		log.warn("Exception caught trying to set shutting down state in Hazelcast node health map", t);
	        }
        }

        log.info("... broadcast shutdown to all dependent document clients");
        RT2DocProcessorManager docProcessorManager = getService(RT2DocProcessorManager.class);
        docProcessorManager.preShutdown();
        
        if (!rt2ConfigService.isBackgroundSaveOn()) {
        	 getService(EmergencySaver.class).emergencySave();
        }

        // ATTENTION: don't use any services from this point
		super.shutdown();

        //---------------------------------------
        log.info("... update rt2 node health map with shutdown completed state");

        // update the HZ node health map and store the final "shutdown" state for
        // this node
        if (hazelcastInstanceActive) {
	        try {
	            RT2NodeHealthState aNodeHealthState = new RT2NodeHealthState(sNodeUUID, rt2ConfigService.getOXNodeID(), RT2NodeHealth.RT2_NODE_HEALTH_SHUTDOWN, RT2NodeHealth.getNodeTypeString(bFullMember), RT2NodeHealth.RT2_CLEANUP_UUID_EMPTY);
	            aRT2NodeHealthMap.set(sNodeUUID, aNodeHealthState);
	    	} catch (HazelcastInstanceNotActiveException e) {
	    		log.warn("HazelcastInstanceNotActiveException caught trying to set shutdown state in Hazelcast node health map", e);
	    		hazelcastInstanceActive = false;	            
	        } catch (Throwable t) {
	        	ExceptionUtils.handleThrowable(t);
	    		log.warn("Exception caught trying to set shutdown state in Hazelcast node health map", t);
	        }
        }

        //-------------------------------------------------------------------------
        log.info("... deactivate JMX reporter");
        if (jmxReporter != null) {
        	jmxReporter.stop();
        }

        //---------------------------------------
        log.info("... remove node from rt2 node health map");
        if (hazelcastInstanceActive) {
	        try {
	            aRT2NodeHealthMap.remove(sNodeUUID);
	    	} catch (HazelcastInstanceNotActiveException e) {
	    		log.warn("HazelcastInstanceNotActiveException caught trying to remove node from node health map", e);
	    		hazelcastInstanceActive = false;
	        } catch (Exception e) {
	    		log.warn("Exception caught trying to remove node from node health map", e);
	        }
        }

		jmxReporter.close();
	}

	@Override
    public void startScheduledExecutorServices() {
		super.startScheduledExecutorServices();
		getService(RT2WSChannelDisposer.class).startExecuterServices();
	}

}
