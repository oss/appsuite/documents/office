/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.osgi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.exception.ExceptionUtils;

public class BundleHelper
{
	private static final Logger log = LoggerFactory.getLogger(BundleHelper.class);
	
	private BundleHelper() {}

    //-------------------------------------------------------------------------
	public static boolean stopBundle(String sBundleSymbolicName) throws Exception
	{
		Validate.notNull(sBundleSymbolicName);

		final Bundle aBundle = findBundle(sBundleSymbolicName);
		boolean bCanShutdown = (aBundle != null);

		if (bCanShutdown)
			stopBundleAsync(aBundle);

        return bCanShutdown;
	}

    //-------------------------------------------------------------------------
	public static void stopBundles(final List<String> aBundleNames) throws Exception
	{
		Validate.notNull(aBundleNames);

		final List<Bundle> aBundleList = new ArrayList<>();
		for (final String sBundleSymbolicName : aBundleNames)
		{
			final Bundle aBundle = findBundle(sBundleSymbolicName);
			if (null != aBundle)
				aBundleList.add(aBundle);
		}

		aBundleList.forEach(bundle -> {
			try
			{
				stopBundleAsync(bundle);
			}
			catch (Throwable t)
			{
                ExceptionUtils.handleThrowable(t);
                log.error("RT2: Exception caught trying to stop bundle " + bundle.getSymbolicName(), t);
			}
		});

	}

    //-------------------------------------------------------------------------
	public static void stopBundleAsync(final Bundle aBundle) throws Exception
	{
		Validate.notNull(aBundle);

        final Runnable aAsyncShutdown = new Thread() {
            @Override
            public void run ()
            {
                try
                {
                    aBundle.stop();
                }
                catch (Throwable ex)
                {
                    ExceptionUtils.handleThrowable(ex);
                    log.info("RT2: Exception caught trying to stop bundle " + aBundle.getSymbolicName(), ex);
                }
            }
        };
        aAsyncShutdown.run();
	}

    //-------------------------------------------------------------------------
	public static Bundle findBundle(String sBundleSymbolicName) throws Exception
	{
		Bundle aBundleToFind = null;

		final BundleContext aBundleContext = FrameworkUtil.getBundle(BundleHelper.class).getBundleContext();
		final Bundle[]      aBundles       = aBundleContext.getBundles();

		if (null != aBundles)
		{
			for (final Bundle aBundle : aBundles)
			{
				if (aBundle.getSymbolicName().equals(sBundleSymbolicName))
				{
					aBundleToFind = aBundle;
					break;
				}
			}
		}

		return aBundleToFind;
	}
}
