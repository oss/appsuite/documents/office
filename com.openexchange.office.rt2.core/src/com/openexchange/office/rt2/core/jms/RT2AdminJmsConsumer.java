/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.jms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.JmsException;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.google.protobuf.InvalidProtocolBufferException;
import com.openexchange.office.rt2.core.RT2DocProcessorClientExistsTester;
import com.openexchange.office.rt2.core.RT2DocProcessorExistsTester;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.control.IRT2NodeHealthManager;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.drive.DeletedFilesTask;
import com.openexchange.office.rt2.core.drive.MovedDeletedFilesAdminTaskProcessor;
import com.openexchange.office.rt2.core.drive.MovedFilesTask;
import com.openexchange.office.rt2.core.exception.RT2Exception;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthMap;
import com.openexchange.office.rt2.protocol.GpbMessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientInfo;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageTimeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ServerIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.SessionIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.VersionMessageRequest;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.VersionMessageResponse;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.RT2MessagePostProcessor;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2AdminIdType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-5)
@RegisteredService
public class RT2AdminJmsConsumer implements MessageListener, JmsMessageListener, InitializingBean, DisposableBean {
	
    private static final Logger log = LoggerFactory.getLogger(RT2AdminJmsConsumer.class);

    //-------------------------------------------------------------------------

    private RT2AdminIdType adminId;

	//--------------------------Services--------------------------------------
    @Autowired
    private JmsTemplateWithoutTtl jmsTemplate;

    @Autowired
    private RT2DocProcessorManager docProcMngr;    

    @Autowired
    private RT2DocProxyRegistry docProxyRegistry;

    @Autowired
    private IRT2NodeHealthManager nodeHealthManager;
    
    @Autowired
    private RT2NodeHealthMap nodeHealthMap;
    
    @Autowired
    private RT2DocProcessorClientExistsTester docProcClientExistsTester;
    
    @Autowired
    private RT2DocProcessorExistsTester docProcessorExistsTester;
    
    @Autowired
    private RT2ConfigService rt2ConfigService;

    @Autowired
    private ClusterService clusterService;
    
	@Autowired
	private RT2JmsMessageSender jmsMessageSender;

	@Autowired
	private MovedDeletedFilesAdminTaskProcessor movedDeletedFileTaskProcessor;

	//------------------------------------------------------------------------    
    private DefaultMessageListenerContainer msgListenerCont;

    //-------------------------------------------------------------------------
    public boolean isRecovering() {
        return msgListenerCont.isRecovering();
    }

    //-------------------------------------------------------------------------    
	@Override
	public void afterPropertiesSet() throws Exception {
		   this.adminId = generateAdminID();
	}

	//-------------------------------------------------------------------------    
	@Override
	public void startReceiveMessages() {
        if (msgListenerCont == null) {
            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);            
            msgListenerCont.setConnectionFactory(jmsTemplate.getConnectionFactory());
            msgListenerCont.setConcurrentConsumers(1);
            msgListenerCont.setDestination(RT2JmsDestination.adminTopic);
            msgListenerCont.setMaxConcurrentConsumers(1);
            msgListenerCont.setPubSubDomain(true);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("RT2AdminJmsConsumer-%d").build()));
            msgListenerCont.start();
        }
	}

	//-------------------------------------------------------------------------	
    @Override
	public void destroy() throws Exception {
        if (msgListenerCont != null) {
            msgListenerCont.destroy();
        }
	}

	//-------------------------------------------------------------------------
    public void send (final RT2Message aTask) {
        aTask.setAdminID(adminId);
        log.debug("admin send request ''{}''...", aTask);
        String rt2MsgAsStr = aTask.getBodyString();
        // JmsTemplate is thread safe
        jmsTemplate.convertAndSend(RT2JmsDestination.adminTopic, rt2MsgAsStr, new RT2MessageJmsPostProcessor(aTask));
    }

    //-------------------------------------------------------------------------
    @Override
    public void onMessage(Message jmsMsg) {
        log.debug("Received admin message...");
        boolean removeAdminMsgType = false;
        try {
        	MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2AdminJmsConsumer");
        	MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());      	            	
        	if (jmsMsg.getBooleanProperty(RT2MessagePostProcessor.HEADER_VERSION_MSG_RCV)) {
        		return;
        	}
    		if (jmsMsg.getBooleanProperty(RT2MessagePostProcessor.HEADER_VERSION_MSG_SND)) {
        		BytesMessage byteMsg = (BytesMessage) jmsMsg;
        		byte [] data = new byte[(int)byteMsg.getBodyLength()];
        		byteMsg.readBytes(data);
        		String originator = VersionMessageRequest.parseFrom(data).getOriginator();						
        		String version = "unknown";
    			VersionMessageResponse versMsg = VersionMessageResponse.newBuilder()
						.setHost(InetAddress.getLocalHost().getHostName())
						.setVersion(version)
						.setHostId(clusterService.getLocalMemberUuid())
						.setOriginator(originator)
						.setCountBackends(nodeHealthMap.getMembersOfState("up").size())
						.build();
    	    	ByteArrayOutputStream baOut = new ByteArrayOutputStream();
    	        try {
    	        	versMsg.writeTo(baOut);
    	        	jmsTemplate.convertAndSend(RT2JmsDestination.adminTopic, baOut.toByteArray(), (newMsg) -> {
    	        		newMsg.setBooleanProperty(RT2MessagePostProcessor.HEADER_VERSION_MSG_SND, false);
    	        		newMsg.setBooleanProperty(RT2MessagePostProcessor.HEADER_VERSION_MSG_RCV, true);
    	        		return newMsg;
    	        	});
    	        } catch (JmsException | IOException ex) {    
    	        	log.error("sending VersionMessageResponse", ex);
    	        }
    	        return;
    		}
        	
        	
	        if (jmsMsg.getBooleanProperty(RT2MessagePostProcessor.HEADER_GPB_MSG)) {
        		BytesMessage byteMsg = (BytesMessage) jmsMsg;
        		byte [] data = new byte[(int)byteMsg.getBodyLength()];
        		byteMsg.readBytes(data);        		
        		AdminMessage adminRequest = AdminMessage.parseFrom(data);
	        	handleResponse(adminRequest);
	        } else {
	        	try {
	        		RT2Message rt2Msg = RT2MessageFactory.fromJmsMessage(jmsMsg);
	        		MDCHelper.safeMDCPut(MDCEntries.ADMIN_MSG_TYPE, rt2Msg.getType().getValue());
	        		removeAdminMsgType = true;
	        		handleResponse(rt2Msg);
	        	} catch (Exception ex) {
	        		log.error(ex.getMessage(), ex);
	        	}
	        }
        } catch (JMSException | UnknownHostException | InvalidProtocolBufferException ex) {
        	log.error(ex.getMessage(), ex);
        } finally {
			MDC.remove(MDCEntries.BACKEND_PART);
			MDC.remove(MDCEntries.BACKEND_UID);
			if (removeAdminMsgType) {
				MDC.remove(MDCEntries.ADMIN_MSG_TYPE);
			}
		}
    }

    //-------------------------------------------------------------------------
    protected void handleResponse(AdminMessage adminRequest) {

    	String nodeUuid = clusterService.getLocalMemberUuid();
        log.debug("Received admin message ''{}''...", adminRequest);
        switch (adminRequest.getMsgType()) {
        	case UPDATE_CURR_CLIENT_LIST_OF_DOC_PROCESSOR_REQUEST:        		
        		if (!adminRequest.getOriginator().getValue().equals(nodeUuid)) {
        			AdminMessage adminResponse = AdminMessage.newBuilder()
        													 .setMsgType(AdminMessageType.UPDATE_CURR_CLIENT_LIST_OF_DOC_PROCESSOR_RESPONSE)
        													 .setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
        													 .setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
        													 .setOriginator(adminRequest.getOriginator())
        													 .addAllActiveClients(docProxyRegistry.listAllDocProxies().stream().map(p ->
        													 	ClientInfo.newBuilder()
        													 		.setClientUid(ClientUidType.newBuilder().setValue(p.getClientUID().getValue()))
        													 		.setDocUid(DocUidType.newBuilder().setValue(p.getDocUID().getValue()))
        													 	.build()).collect(Collectors.toSet()))
        													 .build();
        	    	ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        	        try {
        	        	adminResponse.writeTo(baOut);
        	        	jmsTemplate.convertAndSend(RT2JmsDestination.adminTopic, baOut.toByteArray(), new GpbMessageJmsPostProcessor());
        	        } catch (JmsException | IOException jmsEx) {    
        	        	log.error("sendAdminMessage-Exception", jmsEx);
        	        }    	    	        			
        		}
        		break;
        	case UPDATE_CURR_CLIENT_LIST_OF_DOC_PROCESSOR_RESPONSE:
        		if (adminRequest.getOriginator().getValue().equals(nodeUuid)) {
        			docProcClientExistsTester.processUpdateCurrClientListOfDocProcResponse(UUID.fromString(adminRequest.getServerId().getValue()), adminRequest.getMessageTime().getValue(), adminRequest.getActiveClientsList());
        		}
        		break;
        	case UPDATE_CURR_DOC_PROCESSORS_REQUEST:
        		if (!adminRequest.getOriginator().getValue().equals(nodeUuid)) {
        			AdminMessage adminResponse = AdminMessage.newBuilder()
							 .setMsgType(AdminMessageType.UPDATE_CURR_DOC_PROCESSORS_RESPONSE)
							 .setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
							 .setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
							 .setOriginator(adminRequest.getOriginator())
							 .addAllActiveDocProcessors(docProcMngr.getDocProcessors().stream().map(p -> DocUidType.newBuilder().setValue(p.getDocUID().getValue()).build()).collect(Collectors.toSet()))
							 .build();
        			ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        			try {
        				adminResponse.writeTo(baOut);
        				jmsTemplate.convertAndSend(RT2JmsDestination.adminTopic, baOut.toByteArray(), new GpbMessageJmsPostProcessor());
        			} catch (JmsException | IOException jmsEx) {    
        				log.error("sendAdminMessage-Exception", jmsEx);
        			}    	    	        			        			
        		}
        		break;
        	case UPDATE_CURR_DOC_PROCESSORS_RESPONSE:
        		if (adminRequest.getOriginator().getValue().equals(nodeUuid)) {
        			docProcessorExistsTester.processUpdateCurrProcResponse(UUID.fromString(adminRequest.getServerId().getValue()), adminRequest.getMessageTime().getValue(), adminRequest.getActiveDocProcessorsList());
        		}
        		break;
        	case ADMIN_TASK_SESSION_REMOVED:
        		if ((adminRequest.getSessionId() != null) && (adminRequest.getSessionId().getValue() != null)) {
        			Set<RT2DocUidType> docUids = new HashSet<>();
        			RT2SessionIdType sessionId = new RT2SessionIdType(adminRequest.getSessionId().getValue());
        			
        			Set<RT2DocProcessor> docProcessors = docProcMngr.getDocProcessorsAssociatedToSessionId(sessionId);        			
        			for (RT2DocProcessor docProcessor : docProcessors) {        				
        				Map<RT2SessionIdType, RT2CliendUidType> sessionToClientUids = docProcessor.getSessionIdToClientUids();        				
        				sessionToClientUids.remove(sessionId);
        				if ((sessionToClientUids.size() > 0) || (docProcessor.getPendingOperationsCount() == 0)) {
       						docUids.add(docProcessor.getDocUID());
        				}
        			}
        			Set<DocUidType> docUidsGpb = new HashSet<>();
        			docUids.forEach(d -> docUidsGpb.add(DocUidType.newBuilder().setValue(d.getValue()).build()));
        			if (!docUids.isEmpty()) {
        				AdminMessage.Builder adminRequestDocProxyRemoveBuilder = AdminMessage.newBuilder()
        						.setMsgType(AdminMessageType.ADMIN_TASK_REMOVE_DOC_PROXIES_WITH_DOC_UUID)
        						.setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
        						.setServerId(ServerIdType.newBuilder().setValue(clusterService.getLocalMemberUuid()))
        						.setSessionId(SessionIdType.newBuilder().setValue(sessionId.getValue()).build())
        						.setOriginator(ServerIdType.newBuilder().setValue(clusterService.getLocalMemberUuid()));        				
        				docUidsGpb.forEach(d -> adminRequestDocProxyRemoveBuilder.addDocProcessorsRemoved(d));
        				AdminMessage adminRequestDocProxyRemove = adminRequestDocProxyRemoveBuilder.build();
        				jmsMessageSender.sendAdminMessage(adminRequestDocProxyRemove);		        				
        			}
        		}        		
        		break;
        	case ADMIN_TASK_REMOVE_DOC_PROXIES_WITH_DOC_UUID:
        		RT2SessionIdType sessionId = new RT2SessionIdType(adminRequest.getSessionId().getValue());
        		adminRequest.getDocProcessorsRemovedList().forEach(d -> {
        			RT2DocUidType docUid = new RT2DocUidType(d.getValue());        			
        			for (RT2DocProxy docProxy : docProxyRegistry.getDocProxies4DocUID(docUid, sessionId)) {        				
        				try {        					
							docProxy.closeHard(Cause.SESSION_INVALID);
						} catch (RT2Exception e) {
							log.error("Cannot close Doc Proxy with com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", docProxy.getClientUID(), docUid, e);
						}
        			}
        		});
        		break;
        }
    }
    
    //-------------------------------------------------------------------------
    protected void handleResponse(final RT2Message aTask) {

        log.debug("Received admin message ''{}''...", aTask);

        switch (aTask.getType()) {
            case ADMIN_TASK_CLEANUP_FOR_CRASHED_NODE:
                nodeHealthManager.startLocalNodeDocCleanup(aTask);
                break;
            case ADMIN_TASK_COMPLETED_CLEANUP_FOR_CRASHED_NODE:
                nodeHealthManager.crashedNodeDocCleanupCompleted(aTask);
                break;
            case ADMIN_HEALTH_CHECK_REQUEST: {
                String nodeUuid = clusterService.getLocalMemberUuid();
                if (StringUtils.equals(nodeUuid, RT2MessageGetSet.getNodeUUID(aTask))) {
                    RT2Message adminHealthCheckResponse = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_HEALTH_CHECK_RESPONSE);
                    RT2MessageGetSet.setNodeUUID(adminHealthCheckResponse, nodeUuid);
                    send(adminHealthCheckResponse);
                }
                break;
            }
            case ADMIN_HEALTH_CHECK_RESPONSE: {
                // do nothing to be compatible with old health check
                break;
            }
            case ADMIN_TASK_FILES_MOVED_DELETED: {
                String nodeUuid = clusterService.getLocalMemberUuid();
                log.debug("ADMIN_TASK_FILES_MOVED_DELETED task recived on {}", nodeUuid);
                JSONObject taskData = aTask.getBody();
                JSONArray moved = taskData.optJSONArray(RT2Protocol.BODYPARTS_MOVED);
                JSONArray deleted = taskData.optJSONArray(RT2Protocol.BODYPARTS_DELETED);
                if ((moved != null) && (moved.length() > 0)) {
                    movedDeletedFileTaskProcessor.addMoveDeleteFilesTask(new MovedFilesTask(moved));
                }
                if ((deleted != null) && (deleted.length() > 0)) {
                    movedDeletedFileTaskProcessor.addMoveDeleteFilesTask(new DeletedFilesTask(deleted));
                }
                break;
            }
            default:
                log.error("No support for task {} implemented yet.", aTask.getType());
                throw new UnsupportedOperationException ("No support for task '"+aTask.getType()+"' implemented yet.");
        }
    }

    //-------------------------------------------------------------------------
    public void sendAdminHealthCheckRequest() {
        String nodeUuid = clusterService.getLocalMemberUuid();
        RT2Message adminHealthCheckRequest = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_HEALTH_CHECK_REQUEST);
        RT2MessageGetSet.setNodeUUID(adminHealthCheckRequest, nodeUuid);
        send(adminHealthCheckRequest);
    }

    //-------------------------------------------------------------------------
    public RT2AdminIdType generateAdminID () {
        final String        sNodeID     = rt2ConfigService.getOXNodeID();
        String        sNormalized = sNodeID;

        sNormalized = StringUtils.replace(sNormalized, ":" , "-");
        sNormalized = StringUtils.replace(sNormalized, ";" , "-");
        sNormalized = StringUtils.replace(sNormalized, "/" , "-");
        sNormalized = StringUtils.replace(sNormalized, "\\", "-");
        sNormalized = StringUtils.replace(sNormalized, "\"", "-");
        sNormalized = StringUtils.replace(sNormalized, "'" , "-");
        sNormalized = StringUtils.replace(sNormalized, " " , "-");
        return new RT2AdminIdType(sNormalized);
    }

}
