package com.openexchange.office.rt2.core.jms;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponse.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.jms.EnhActiveMQSSLConnectionFactory;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;

@Service
@RegisteredService(registeredClass=HealthCheck.class)
public class RT2JmsClientHealthCheck implements HealthCheck, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(RT2JmsClientHealthCheck.class);
    private final String JMS_CLIENT_HEALTH_CHECK = "RT2JmsClientHealthCheck";
    private final String JMS_CONNECTION_RECOVERING = "jmsConnectionRecovering";
    private final String JMS_BROKER_URL = "jmsBrokerUrl";
    private final String JMS_BROKER_URL_NOT_AVAILABLE = "n/a";
    private final String SERVICE_AVAILABLE = "serviceAvailable";

    @Autowired
    RT2AdminJmsConsumer jmsAdminConsumer;

    @Autowired
    PooledConnectionFactoryProxy pooledConnectionFactoryProxy;

    AtomicBoolean destroyed = new AtomicBoolean(false);

    @Override
    public HealthCheckResponse call() {
        final Map<String, Object> healthData = new HashMap<>();

        try {
            boolean serviceAvailable = false;
            String jmsBrokerUrl = null;

            if (!destroyed.get()) {
                boolean isConsumerRecovering = (jmsAdminConsumer != null) ? jmsAdminConsumer.isRecovering() : true;
                healthData.put(JMS_CONNECTION_RECOVERING, isConsumerRecovering);
                serviceAvailable = !isConsumerRecovering;

                // set currently used JMS broker Url
                final PooledConnectionFactory pooledConnFact = (null != pooledConnectionFactoryProxy) ?
                    pooledConnectionFactoryProxy.getPooledConnectionFactory() :
                        null;
                final EnhActiveMQSSLConnectionFactory amqConnFact = (EnhActiveMQSSLConnectionFactory) ((null != pooledConnFact) ?
                    pooledConnFact.getConnectionFactory() :
                        null);
                jmsBrokerUrl = (null != amqConnFact) ? amqConnFact.getBrokerURL() : null;
            }

            healthData.put(JMS_BROKER_URL, (null != jmsBrokerUrl) ? jmsBrokerUrl : JMS_BROKER_URL_NOT_AVAILABLE);
            healthData.put(SERVICE_AVAILABLE, Boolean.valueOf(serviceAvailable));
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            log.error("RT2JmsClientHealthCheck caught unexpected exception in health check", e);
        }

        return new HealthCheckResponse(JMS_CLIENT_HEALTH_CHECK, Status.UP, Optional.of(healthData));
    }

    @Override
    public void destroy() throws Exception {
        if (destroyed.compareAndSet(false, true)) {
            log.info("RT2JmsClientHealthCheck destroyed.");
        }
    }
}
