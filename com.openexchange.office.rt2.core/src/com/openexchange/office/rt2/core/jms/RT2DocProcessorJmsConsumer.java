/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.jms;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.validation.ValidationException;

import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorFactory;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.exception.RT2InvalidClientUIDException;
import com.openexchange.office.rt2.core.metric.DocProcessorMetricService;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-5)
public class RT2DocProcessorJmsConsumer implements MessageListener, JmsMessageListener, InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(RT2DocProcessorJmsConsumer.class);

    private String nodeUUID;

    private ActiveMQQueue queue;

    //--------------------------Services--------------------------------------
    @Autowired
    private RT2DocProcessorManager docProcessorManager;

    @Autowired
    private DocProcessorMetricService docProcessorMetricService;
    
    @Autowired
    private RT2DocProcessorFactory docProcessorFactory;
    
    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;
    
    @Autowired
    private ClusterService clusterService;
    
    @Autowired
    private RT2MessageSender messageSender;

    //------------------------------------------------------------------------
    private DefaultMessageListenerContainer msgListenerCont;
    
    @Override
    public void afterPropertiesSet() throws Exception {
        String queueName = RT2JmsProperties.DOCUMENT_QUEUE_NAME + clusterService.getLocalMemberUuid();
        this.queue = new ActiveMQQueue(queueName);
        log.debug("Consuming from queue with name {}", queueName);
        this.nodeUUID = clusterService.getLocalMemberUuid();
    }

    //------------------------------------------------------------------------
    @Override
    public void startReceiveMessages() {
        if (msgListenerCont == null) {
            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);            
            msgListenerCont.setConnectionFactory(pooledConnectionFactoryProxy.getPooledConnectionFactory());
            msgListenerCont.setConcurrentConsumers(3);
            msgListenerCont.setDestination(this.queue);
            msgListenerCont.setMaxConcurrentConsumers(3);
            msgListenerCont.setPubSubDomain(false);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("RT2DocProcessorJmsConsumer-%d").build()));
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.start();
        }
    }

    //------------------------------------------------------------------------
    @Override
    public void destroy() throws Exception {
        if (msgListenerCont != null) {
            msgListenerCont.destroy();
        }
    }

    //------------------------------------------------------------------------
    @Override
    public void onMessage(Message jmsMsg) {
        try {
            RT2Message rt2Msg = null;
            try {
                rt2Msg = RT2MessageFactory.fromJmsMessage(jmsMsg);
                docProcessorMetricService.startTimer(rt2Msg.getMessageID());
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, rt2Msg.getDocUID().getValue());
                MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, rt2Msg.getClientUID().getValue());
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "processor");
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, nodeUUID);
                MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, rt2Msg.getType().getValue());
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                return;
            }

            boolean joinRequest = RT2MessageType.REQUEST_JOIN.equals(rt2Msg.getType());
            log.debug("Received msg {}", rt2Msg.getHeader());
            if (joinRequest) {
                // ensure that join round-trip is logged on info
                log.info("Received join_request");
            }

            RT2DocUidType docUid = rt2Msg.getDocUID();
            RT2DocProcessor rt2DocProc = docProcessorManager.getDocProcessor(docUid);
            if ((rt2DocProc == null) && (RT2MessageType.REQUEST_JOIN.equals(rt2Msg.getType()))) {
                try {
                    String docType = RT2MessageGetSet.getDocType(rt2Msg);
                    RT2FileIdType fileId = rt2Msg.getFileID();
                    rt2DocProc = getOrCreateDocProcessor(docType, docUid, fileId);
                } catch (ValidationException ex) {
                    log.debug("No doc processor found for request message {}", rt2Msg);
                }
            }

            try {
                if (rt2DocProc != null) {
                    if (joinRequest) {
                        log.info("Enqueueing join_request - com.openexchange.rt2.document.uid {}, com.openexchange.rt2.client.uid {}", docUid, rt2Msg.getClientUID().getValue());
                    }
                    rt2DocProc.enqueueMessage(rt2Msg);
                }
            } catch (RT2InvalidClientUIDException ex) {
                messageSender.sendErrorResponseToClient(rt2Msg.getClientUID(), rt2Msg, null, ex);
                // Nothing todo, it is possible under some circumstances
            } catch (Exception ex) {
                log.error("Exception during processing RT2Message " + rt2Msg + ": " + ex.getMessage(), ex);
                messageSender.sendErrorResponseToClient(rt2Msg.getClientUID(), rt2Msg, null, ex);
            }
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
            MDC.remove(MDCEntries.BACKEND_PART);
            MDC.remove(MDCEntries.BACKEND_UID);
            MDC.remove(MDCEntries.REQUEST_TYPE);
        }
    }

    //------------------------------------------------------------------------
    private synchronized RT2DocProcessor getOrCreateDocProcessor(String docType, RT2DocUidType docUid, RT2FileIdType fileId) {
        RT2DocProcessor result = docProcessorManager.getDocProcessor(docUid);

        if (result == null) {
            log.debug("Creating new docProcessor for com.openexchange.rt2.document.uid {}", docUid);
            result = docProcessorFactory.createInstance(docType, docUid, fileId);
        }
        return result;
    }

}
