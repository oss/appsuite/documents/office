/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.JmsConfigService;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@ShutdownOrder(value=-5)
public class RT2DcsUpdateConsumer implements MessageListener, JmsMessageListener, DisposableBean {

	private static final Logger log = LoggerFactory.getLogger(RT2DcsUpdateConsumer.class);
	private static final String ACTIVEMQ_QUERYPARAM_UPDATEURISSUPPORTED = "updateURIsSupported";
	private static final String ACTIVEMQ_QUERYPARAM_FOR_CONN_VERIFYHOSTNAME = "verifyHostName";

	//--------------------------Services--------------------------------------
    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;

    @Autowired
    private MetricRegistry metricRegistry;

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private JmsConfigService jmsConfigService;

	//------------------------------------------------------------------------
    private DefaultMessageListenerContainer msgListenerCont;

    private Counter counter;

    //-------------------------------------------------------------------------
    public boolean isRecovering() {
        return msgListenerCont.isRecovering();
    }

    //-------------------------------------------------------------------------
	@Override
	public void onMessage(Message msg) {

        counter.inc();
        ActiveMQTextMessage txtMsg = (ActiveMQTextMessage) msg;
        try {
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "RT2DcsUpdateConsumer");
            MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, clusterService.getLocalMemberUuid());

            String newBrokerUrl = txtMsg.getText();
            // DOCS-3619: ensure that we extend the newBrokerUrl in case
            // we have a SSL activated and a disabled verifyHostName property.
            // The broker which sent us this dcs update is not able to detect if
            // we want to verify or not the hostname on a ssl connection.
            // We have to ensure that the failover option updateURIsSupported is
            // set to false as otherwise the verifyHostName option can get lost
            // in case a ConnectionControl message is received a broker.
            // See also comment in PooledConnectionFactoryCreator.java
            newBrokerUrl = checkAndExtendBrokerUrlIfNecessary(newBrokerUrl);

            ActiveMQConnectionFactory connFactory = (ActiveMQConnectionFactory) pooledConnectionFactoryProxy.getPooledConnectionFactory().getConnectionFactory();
            if (!newBrokerUrl.equals(connFactory.getBrokerURL())) {
                log.info("Received new and updated brokerURL: {}", newBrokerUrl);
                connFactory.setBrokerURL(newBrokerUrl);
            }
        } catch (JMSException ex) {
            log.error(ex.getErrorCode(), ex);
        } finally {
            MDC.remove(MDCEntries.BACKEND_PART);
            MDC.remove(MDCEntries.BACKEND_UID);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void startReceiveMessages() {
        if (msgListenerCont == null) {
            this.counter = metricRegistry.counter(MetricRegistry.name("RT2DcsUpdateConsumer", "count"));

            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);
            msgListenerCont.setConnectionFactory(pooledConnectionFactoryProxy.getPooledConnectionFactory());
            msgListenerCont.setConcurrentConsumers(3);
            msgListenerCont.setDestination(RT2JmsDestination.brokerChangedTopic);
            msgListenerCont.setMaxConcurrentConsumers(1);
            msgListenerCont.setPubSubDomain(true);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("RT2DcsUpdateConsumer-%d").build()));
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.start();
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void destroy() throws Exception {
        if (msgListenerCont != null) {
            msgListenerCont.destroy();
            msgListenerCont = null;
        }
    }

    //-------------------------------------------------------------------------
    // DOCS-3619:
    // Ensure that updateURIsSupported option is set to false in combination when
    // verifyHostName was configured to false on failover url with ssl connections
    // as this workaround a strange issue that a DCS cluster sends
    // ConnectionControl messages which can result in a client connection
    // rebalancing. Unfortunately the FailoverTransport class uses the connected
    // broker information from the ConnectionControl message where important query
    // parameters are missing (e.g. verifyHostName). This can lead to the fact that
    // client won't be able to connect to a DCS broker with a valid certificate which
    // doesn't fit the ip address or domain/hostname. The updateURIsSupported option
    // set to false prevents the client to evaluate ConntectionControl messages.
    // By default this option is set to true.
    // We als
    private String checkAndExtendBrokerUrlIfNecessary(String newBrokerUrl) {
        String result = newBrokerUrl;

        if (jmsConfigService.isRT2SSLEnabled() && !jmsConfigService.isRT2SSLVerifyHostname()) {
            try {
                result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamWithValue(newBrokerUrl, ACTIVEMQ_QUERYPARAM_UPDATEURISSUPPORTED, "false");
                result = FailoverBrokerUrlHelper.ensureFailoverBrokerUrlContainsQueryParamForEveryConnection(result, ACTIVEMQ_QUERYPARAM_FOR_CONN_VERIFYHOSTNAME, "false");
            } catch (IllegalArgumentException e) {
                log.warn("IllegalArgumentException caught trying to adopt failover brokerURL to the current configuration", e);
            }
        }

        return result;
    }
}
