/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.jms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Service;

import com.google.protobuf.GeneratedMessageV3;
import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.metric.DocProcessorMetricService;
import com.openexchange.office.rt2.core.metric.DocProxyRequestMetricService;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.GpbMessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2AdminIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.jms.JmsTemplateWithTtl;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;

@Service
public class RT2JmsMessageSender {

    private static final Logger log = LoggerFactory.getLogger(RT2JmsMessageSender.class);

    //-------------------------------Services------------------------------------------
    @Autowired
    private RT2DocOnNodeMap rt2DocOnNodeMap;

    @Autowired
    private DocProxyRequestMetricService docProxyRequestMetricService;

    @Autowired
    private DocProcessorMetricService docProcessorMetricService;

    @Autowired
    private JmsTemplateWithoutTtl jmsTemplateWithoutTtl;

    @Autowired
    private JmsTemplateWithTtl jmsTemplateWithTtl;

    @Autowired
    private SpecialLogService specialLogService;

    @Autowired
    private ClusterService clusterService;

    public void sendBroadcastMessage(BroadcastMessage msg) {
        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        try {
            if (msg.getDocUid() != null) {
                specialLogService.log(Level.DEBUG, log, msg.getDocUid().getValue(), new Throwable(), "Sending broadcastMsg of type {} for com.openexchange.rt2.document.uid {}", msg.getMsgType(), msg.getDocUid().getValue());
            }
            msg.writeTo(baOut);
            jmsTemplateWithoutTtl.convertAndSend(RT2JmsDestination.clientResponseTopic, baOut.toByteArray(), new GpbMessageJmsPostProcessor(msg));
        } catch (JmsException | IOException jmsEx) {
            log.error("sendToClientResponseTopic-Exception", jmsEx);
        }
    }

    public void sendToClientResponseTopic(RT2Message msg) {

        try {
            String rt2MsgAsStr = msg.getBodyString();
            docProcessorMetricService.stopTimer(msg.getMessageID());
            try {
                jmsTemplateWithoutTtl.convertAndSend(RT2JmsDestination.clientResponseTopic, rt2MsgAsStr, new RT2MessageJmsPostProcessor(msg));
            } catch (JmsException jmsEx) {
                log.error("sendToClientResponseTopic-Exception", jmsEx);
            }
        } finally {
            MDC.clear();
        }
    }

    public void sendAdminMessage(GeneratedMessageV3 msg) {
        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        try {
            msg.writeTo(baOut);
            jmsTemplateWithoutTtl.convertAndSend(RT2JmsDestination.adminTopic, baOut.toByteArray(), new GpbMessageJmsPostProcessor());
        } catch (JmsException | IOException jmsEx) {
            log.error("sendAdminMessage-Exception", jmsEx);
        }
    }

    public void sendToAdminTopic(RT2Message msg, RT2AdminIdType adminId) {
        msg.setAdminID(adminId);
        try {
            log.debug("admin send request ''{}''...", msg);

            MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, msg.getType().getValue());
            String rt2MsgAsStr = msg.getBodyString();
            try {
                jmsTemplateWithoutTtl.convertAndSend(RT2JmsDestination.adminTopic, rt2MsgAsStr, new RT2MessageJmsPostProcessor(msg));
            } catch (JmsException jmsEx) {
                log.error("sendToAdminTopic-Exception", jmsEx);
            }
        } finally {
            MDC.remove(MDCEntries.REQUEST_TYPE);
        }
    }

    public void sendToDocumentQueue(RT2Message msg, List<RT2LogInfo> msgs) throws RT2TypedException {
        // Cannot be null, because it will be added before
        String docUidAsStr = msg.getDocUID().getValue();
        String nodeId = rt2DocOnNodeMap.get(docUidAsStr);

        if (nodeId == null) {
            // Special case: It's possible that we receive an ack although we
            // already cleaned up client data and sent the leave-response. The
            // client can send it before it receives the leave-response and
            // closes the connection and ends the ACK process. Therefore we just
            // have to ignore ACK_SIMPLE messages in case we cannot find the nodeId.
            if ((msg.getType() == RT2MessageType.ACK_SIMPLE) || msg.getType().equals(RT2MessageType.REQUEST_UNAVAILABILITY)) {
                return;
            }
            log.warn("There has to be an entry in the docOnNodeMap for com.openexchange.rt2.document.uid {}, we assume that it was already disposed!", msg.getDocUID().getValue());
            throw new RT2TypedException(ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR, new ArrayList<>(msgs));
        }

        if ((msg.getType() == RT2MessageType.REQUEST_JOIN) && !clusterService.isActiveHzMember(nodeId)) {
            // Special case: It's possible that the DocOnNode map contains invalid node
            // information. It makes no sense to send this join message to a queue which
            // has no consumer. Therefore we bail out here with an exception to provide
            // the client with better information. Hopefully the RT2 GC or the node crashed
            // code will clean-up the DocOnNode map.
            log.warn("Detected that docOnNodeMap entry uuid {} for com.openexchange.rt2.document.uid {} is not member of the hz cluster. Clean-up process must reset entry.", nodeId, msg.getDocUID().getValue());
            throw new RT2TypedException(ErrorCode.GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR, new ArrayList<>(msgs));
        }

        msg.setInternalBackendProcessingNode(nodeId);
        String queueName = RT2JmsProperties.DOCUMENT_QUEUE_NAME + nodeId;
        try {
            MDCHelper.safeMDCPut(MDCEntries.REQUEST_TYPE, msg.getType().getValue());
            log.debug("Sending msg [{}] to documentQueue: [{}]", msg, queueName);
            ActiveMQQueue activeMQQueue = new ActiveMQQueue(queueName);
            String rt2MsgAsStr = msg.getBodyString();
            docProxyRequestMetricService.stopTimer(msg.getMessageID());
            try {
                jmsTemplateWithTtl.convertAndSend(activeMQQueue, rt2MsgAsStr, new RT2MessageJmsPostProcessor(msg));
            } catch (JmsException jmsEx) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR.getCodeAsStringConstant());
                log.error("sendToDocumentQueue-Exception", jmsEx);
                throw new RT2TypedException(ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR, new ArrayList<>(msgs));
            }
        } finally {
            MDC.remove(MDCEntries.ERROR);
            MDC.remove(MDCEntries.REQUEST_TYPE);
        }
    }
}
