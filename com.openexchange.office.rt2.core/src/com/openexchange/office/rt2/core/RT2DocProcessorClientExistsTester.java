/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientInfo;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageTimeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ServerIdType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@Async(initialDelay=5, period=5, timeUnit=TimeUnit.MINUTES)
public class RT2DocProcessorClientExistsTester implements Runnable {
	
	private static final Logger log = LoggerFactory.getLogger(RT2DocProcessorClientExistsTester.class);
	
	//---------------------------------Services----------------------------------------------------
	@Autowired
	private RT2JmsMessageSender jmsMessageSender;
	
	@Autowired
	private RT2DocProxyRegistry docProxyRegistry;
	
	@Autowired
	private RT2DocProcessorManager docProcMngr;
	
	@Autowired
	private DistributedDocInfoMap distributedDocInfoMap;
	
	@Autowired
	private ClusterService clusterService;
	
	//---------------------------------------------------------------------------------------------
	private final AtomicLong currentlyRunning = new AtomicLong(0);	
	
	private Map<UUID, Collection<ClientInfo>> collectedClients = new HashMap<>();
	
	private Collection<RT2DocUidType> markedAtomicsForDelete = new HashSet<>();
	
	@Override
	public void run() {
		try {
			if (currentlyRunning.compareAndSet(0l, System.currentTimeMillis())) {
				String nodeUuid = clusterService.getLocalMemberUuid();
				collectedClients.clear();
				if (clusterService.getCountMembers() == 1) {
					processUpdateCurrClientListOfDocProcResponse(UUID.fromString(nodeUuid), System.currentTimeMillis(), new HashSet<>());
				} else {
					AdminMessage adminRequest = AdminMessage.newBuilder()
															.setMsgType(AdminMessageType.UPDATE_CURR_CLIENT_LIST_OF_DOC_PROCESSOR_REQUEST)
															.setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
															.setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
															.setOriginator(ServerIdType.newBuilder().setValue(nodeUuid))
															.build();
					jmsMessageSender.sendAdminMessage(adminRequest);
				}
			} else {
				LocalDateTime started =
					    LocalDateTime.ofInstant(Instant.ofEpochMilli(currentlyRunning.get()), ZoneId.systemDefault());
				LocalDateTime now = LocalDateTime.now();
				Duration duration = Duration.between(now, started);
				if (duration.toMinutes() > 10) {				
					currentlyRunning.set(0l);
					run();
				}
			}
		} finally {
			MDC.clear();
		}
	}
	
	public void processUpdateCurrClientListOfDocProcResponse(UUID nodeUuid, long msgDate, Collection<ClientInfo> clientUids) {
		if (msgDate < currentlyRunning.get()) {
			return;
		}
		if (currentlyRunning.get() > 0l) {
			if (collectedClients.containsKey(nodeUuid)) {
				currentlyRunning.set(0l);
				return;
			}
			if (!clientUids.isEmpty()) {
				collectedClients.put(nodeUuid, clientUids);
			}
			if (collectedClients.size() == clusterService.getCountMembers() - 1) {
				collectedClients.put(UUID.fromString(clusterService.getLocalMemberUuid()), 
						docProxyRegistry.listAllDocProxies().stream().map(p -> 
						 	ClientInfo.newBuilder()
						 		.setClientUid(ClientUidType.newBuilder().setValue(p.getClientUID().getValue()))
						 		.setDocUid(DocUidType.newBuilder().setValue(p.getDocUID().getValue()))
						 	.build()).collect(Collectors.toSet()));						
				Set<RT2CliendUidType> res = new HashSet<>();
				collectedClients.values().forEach(col -> {
					col.forEach(c -> res.add(new RT2CliendUidType(c.getClientUid().getValue())));
				});
				docProcMngr.getDocProcessors().forEach(r -> r.updateClientsInfo(res));
				checkAtomicLongs();
			}
			currentlyRunning.set(0l);			
		}
	}	
	
	private void checkAtomicLongs() {
		Set<RT2DocUidType> allRegisteredDocIds = distributedDocInfoMap.getAllRegisteredDocIds();
		Set<RT2DocUidType> isDocProxies = new HashSet<>();		
		collectedClients.values().forEach(col -> col.forEach(c -> isDocProxies.add(new RT2DocUidType(c.getDocUid().getValue()))));
		Collection<RT2DocUidType> newMarkedAtomicForDelete = new HashSet<>();
		for (RT2DocUidType docUid : allRegisteredDocIds) {
			try {
				MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
				if (!isDocProxies.contains(docUid)) {
					if (markedAtomicsForDelete.contains(docUid)) {
						log.info("Removing docInfo entry for com.openexchange.rt2.document.uid {}", docUid);
						distributedDocInfoMap.freeDocInfos(docUid);
					} else {
						newMarkedAtomicForDelete.add(docUid);
					}
				}
			} finally {
				MDC.remove(MDCEntries.DOC_UID);
			}
		}
		markedAtomicsForDelete = newMarkedAtomicForDelete;
		if (!markedAtomicsForDelete.isEmpty()) {
			for (RT2DocUidType docUid : markedAtomicsForDelete) {
				try {
					MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
					log.info("Marked docInfo for com.openexchange.rt2.document.uid {}, current clients: {}, currentDocProxys: {}", docUid.getValue(), distributedDocInfoMap.getClientsOfDocUid(docUid), collectedClients.values());
				} finally {
					MDC.remove(MDCEntries.DOC_UID);
				}
			}
		}
	}

	public Collection<RT2DocUidType> getMarkedAtomicsForDelete() {
		return new HashSet<>(markedAtomicsForDelete);
	}
	
	public long getCurrentlyRunning() {
		return currentlyRunning.get();
	}
}
