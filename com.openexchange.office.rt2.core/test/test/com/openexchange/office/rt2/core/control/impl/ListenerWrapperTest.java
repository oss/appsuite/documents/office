/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.ITaskListener;
import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.control.impl.CleanupTask;
import com.openexchange.office.rt2.core.control.impl.ListenerWrapper;

public class ListenerWrapperTest {

	//-------------------------------------------------------------------------
	private static final int NUMBER_OF_TASKS = 5;
	private final List<String> aTaskIDs = new ArrayList<>();
	private final List<String> aMasterUUIDs = new ArrayList<>();
	private final List<String> aCleanupUUIDs = new ArrayList<>();
	private final List<CleanupTask> aCleanupTasks = new ArrayList<>();

	//-------------------------------------------------------------------------
	public static class TestTaskListener implements ITaskListener<Task>
	{
		private List<Task> aCompletedTaskList = new LinkedList<>();

		public TestTaskListener() {}

		public List<Task> getCompletedTasks()
		{
			return aCompletedTaskList;
		}

		@Override
		public void taskCompleted(Task aCompletedTask) {
			aCompletedTaskList.add(aCompletedTask);
		}

	}

	//-------------------------------------------------------------------------
	private void addCleanupTaskData(final CleanupTask aCleanupTask)
	{
		aCleanupTasks.add(aCleanupTask);
		aTaskIDs.add(aCleanupTask.getTaskID());
		aMasterUUIDs.add(aCleanupTask.getMasterUUID());
		aCleanupUUIDs.add(aCleanupTask.getMemberUUIDToCleanup());
	}

	//-------------------------------------------------------------------------
	@Test
	public void test() {

		final TestTaskListener aListener = new TestTaskListener();
		final ListenerWrapper<CleanupTask> aListenerWrapper = new ListenerWrapper<CleanupTask>(aListener);

		assertNotNull(aListener);
		assertNotNull(aListenerWrapper);

		for (int i = 0; i < NUMBER_OF_TASKS; i++)
		{
			final String sTaskID = UUID.randomUUID().toString();
			final String sMasterUUID = UUID.randomUUID().toString();
			final String scleanupUUID = UUID.randomUUID().toString();
			final CleanupTask aCleanupTask = new CleanupTask(sTaskID, sMasterUUID, scleanupUUID);

			addCleanupTaskData(aCleanupTask);
		}

		for (final CleanupTask aTask : aCleanupTasks) {
            aListenerWrapper.taskCompleted(aTask);
        }

		int i = 0;
		final List<Task> aCompletedTasks = aListener.getCompletedTasks();
		for (final Task aTask : aCompletedTasks)
		{
			assertTrue(aTask instanceof CleanupTask);
			assertTrue(aTask == aCleanupTasks.get(i++));
		}
	}

}
