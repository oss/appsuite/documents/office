/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.ws;

import org.apache.commons.lang3.tuple.Pair;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;


public class PairMock extends Pair<RT2CliendUidType, RT2DocUidType> {

    private RT2CliendUidType rt2CliendUid;
    private RT2DocUidType rt2DocUid;

    public PairMock(RT2CliendUidType rt2CliendUid, RT2DocUidType rt2DocUid) {
        this.rt2CliendUid = rt2CliendUid;
        this.rt2DocUid = rt2DocUid;
    }

    @Override
    public RT2DocUidType setValue(RT2DocUidType value) {
        var old = rt2DocUid;
        rt2DocUid = value;
        return old;
    }

    @Override
    public RT2CliendUidType getLeft() {
        return this.rt2CliendUid;
    }

    @Override
    public RT2DocUidType getRight() {
        return this.rt2DocUid;
    }

}
