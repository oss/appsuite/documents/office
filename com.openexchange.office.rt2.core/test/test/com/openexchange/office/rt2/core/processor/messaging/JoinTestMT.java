package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.config.RT2Const;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2DocTypeType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestConstants;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class JoinTestMT extends BaseTest {

    @Override
    protected void initServices() {
        this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
        this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
        this.msgBackupAndAckProcessorService = unitTestBundle.getService(MsgBackupAndAckProcessorService.class);
        this.clientSequenceQueueDisposerService = unitTestBundle.getService(ClientSequenceQueueDisposerService.class);

        this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
    }

    @Override
    protected RT2Message createTestMessage() {
        RT2Message joinRequest = TestRT2MessageCreator.createJoinMsg(clientId1, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()));
        joinRequest.setDocType(new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
        return joinRequest;
    }

    protected RT2Message createJoinMessageClient2() {
        RT2Message joinRequest = TestRT2MessageCreator.createJoinMsg(clientId2, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()));
        joinRequest.setDocType(new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
        return joinRequest;
    }

    protected RT2Message createJoinMessageClient3() {
        RT2Message joinRequest = TestRT2MessageCreator.createJoinMsg(clientId3, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()));
        joinRequest.setDocType(new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
        return joinRequest;
    }

    //@Test - threading issue must be fixed, see DOCS-4622 (RT2 Race possible between creation of the DocProcessor instance and getting it in RT2DocProcessorJmsConsumer.onMessage())
    @UnittestMetadata(countClients=3, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
    public void testSucc() throws Exception {

        for (int i=1; i <= 5; i++) {
            createAndDistributeJoinMessages();

            Thread.sleep(2000);

            assertEquals(1, docProcMngr.size());
            assertEquals(0, docProcMngr.getNumOfConflictingCreations());
            TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
            this.msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1);
            this.msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId2);
            this.msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId3);
            assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
            assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId2, false));
            assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId3, false));
            assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));
            assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId2));
            assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId3));

            ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
            Mockito.verify(jmsMessageSender, times(3 * i)).sendToClientResponseTopic(rt2MsgCaptor.capture());
            List<RT2Message> messages = rt2MsgCaptor.getAllValues();
            assertEquals(3 * i, messages.size());
            RT2Message joinResponse1 =
                    TestRT2MessageCreator.createJoinResponseMsg(clientId1, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), new RT2FolderIdType(TestConstants.FOLDER_ID),
                                                                new RT2FileIdType(TestConstants.FILE_ID), new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
            RT2Message joinResponse2 =
                TestRT2MessageCreator.createJoinResponseMsg(clientId2, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), new RT2FolderIdType(TestConstants.FOLDER_ID),
                                                            new RT2FileIdType(TestConstants.FILE_ID), new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
            RT2Message joinResponse3 =
                TestRT2MessageCreator.createJoinResponseMsg(clientId3, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), new RT2FolderIdType(TestConstants.FOLDER_ID),
                                                            new RT2FileIdType(TestConstants.FILE_ID), new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
            TestRT2MessageCreator.setMessagePostProcDefaultHeader(joinResponse1, RT2MessageType.RESPONSE_JOIN);
            TestRT2MessageCreator.setMessagePostProcDefaultHeader(joinResponse2, RT2MessageType.RESPONSE_JOIN);
            TestRT2MessageCreator.setMessagePostProcDefaultHeader(joinResponse3, RT2MessageType.RESPONSE_JOIN);
            assertEquals(i, numOfValidJoinResponses(messages, joinResponse1));
            assertEquals(i, numOfValidJoinResponses(messages, joinResponse2));
            assertEquals(i, numOfValidJoinResponses(messages, joinResponse3));

            docProcMngr.docProcessorDisposed(textDocProcessor);

            Thread.sleep(100);
        }
    }

    private long numOfValidJoinResponses(List<RT2Message> messages, RT2Message joinResponse) {
        return messages.stream().filter((m -> RT2MessageValidator.isValid(m, joinResponse))).count();
    }

    private void createAndDistributeJoinMessages() throws Exception {
        RT2Message joinMsg = createTestMessage();
        RT2Message join2Msg = createJoinMessageClient2();
        RT2Message join3Msg = createJoinMessageClient3();
        CountDownLatch latch = new CountDownLatch(3);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(() -> {
            TextMessage txtMsg;
            try {
                Thread.sleep(100);
                txtMsg = createAmqMessage(joinMsg);
                latch.countDown();
                latch.await();
                docProcJmsCons.onMessage(txtMsg);
            } catch (JMSException e) {
                fail();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
        executorService.submit(() -> {
            TextMessage txtMsg;
            try {
                Thread.sleep(100);
                latch.countDown();
                latch.await();
                txtMsg = createAmqMessage(join2Msg);
                docProcJmsCons.onMessage(txtMsg);
            } catch (JMSException e) {
                fail();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
        executorService.submit(() -> {
            TextMessage txtMsg;
            try {
                Thread.sleep(100);
                latch.countDown();
                latch.await();
                txtMsg = createAmqMessage(join3Msg);
                docProcJmsCons.onMessage(txtMsg);
            } catch (JMSException e) {
                fail();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }
}
