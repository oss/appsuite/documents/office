/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.ws;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.tuple.Pair;
import org.glassfish.grizzly.websockets.DataFrame;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.rt2.core.RT2LogInfo;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.metric.WebsocketResponseMetricService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2EnhDefaultWebSocket;
import com.openexchange.office.rt2.core.ws.RT2SessionCountValidator;
import com.openexchange.office.rt2.core.ws.RT2SessionValidator;
import com.openexchange.office.rt2.core.ws.RT2WSApp;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class RT2WebSocketListenerTest {

    @RegisterExtension
    public final LoggerRule loggerRule = new LoggerRule();

    @Captor
    private ArgumentCaptor<RT2Message> rt2MessageCaptor;

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private RT2WSApp rt2WSApp;

    private RT2WSChannelDisposer rt2WSChannelDisposer;

    private RT2SessionCountValidator sessionCountValidator;

    private RT2SessionValidator sessionValidator;

    private RT2DocProxyRegistry rt2DocProxyRegistry;

    private WebsocketResponseMetricService webSocketResponseMetricService;

    private RT2WebSocketListener sut = new RT2WebSocketListener();

    private RT2CliendUidType rt2ClientUid;
    private RT2DocUidType rt2DocUid;
    private RT2ChannelId rt2ChannelId;
    private String sessionIdStr;
    private RT2SessionIdType sessionId;
    private final AtomicBoolean socketAvailable = new AtomicBoolean(false);
    private final AtomicBoolean socketCurrentProcessingRequest = new AtomicBoolean(false);

    @BeforeEach
    public void init() throws OXException {
        MockitoAnnotations.openMocks(this);
        var unittestInformation = new RT2WebSocketListenerTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();

        rt2WSApp = unitTestBundle.getMock(RT2WSApp.class);
        rt2WSChannelDisposer = unitTestBundle.getMock(RT2WSChannelDisposer.class);
        sessionCountValidator = unitTestBundle.getMock(RT2SessionCountValidator.class);
        rt2DocProxyRegistry = unitTestBundle.getMock(RT2DocProxyRegistry.class);
        sessionValidator = unitTestBundle.getMock(RT2SessionValidator.class);
        webSocketResponseMetricService = unitTestBundle.getMock(WebsocketResponseMetricService.class);

        unitTestBundle.injectDependencies(sut);
        unitTestBundle.injectValues(sut);

        rt2ClientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        rt2DocUid = new RT2DocUidType(UUID.randomUUID().toString());
        rt2ChannelId = new RT2ChannelId(UUID.randomUUID().toString());
        sessionIdStr = UUID.randomUUID().toString();
        sessionId = new RT2SessionIdType(sessionIdStr);
    }

    @Test
    public void rt2WebSocketListenerDestroy() throws Exception {
        assertDoesNotThrow(() -> sut.destroy());
    }

    @Test
    public void rt2WebSocketListenerOnCloseSocketPartOfRT2WSApp() throws Exception {
        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var dataFrame = mock(DataFrame.class);

        when(webSocket.getId()).thenReturn(rt2ChannelId);
        when(rt2WSApp.contains(rt2ChannelId)).thenReturn(true);

        sut.onClose(webSocket, dataFrame);

        verify(rt2WSChannelDisposer, times(0)).getClientDocPairsAssociatedToChannel(rt2ChannelId);
        verify(rt2WSChannelDisposer, times(1)).removeChannel(webSocket);
        verify(sessionCountValidator, times(1)).removeSession(rt2ChannelId);
    }

    @Test
    public void rt2WebSocketListenerOnCloseSocketNOTPartOfRT2WSApp() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);
        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var dataFrame = mock(DataFrame.class);
        var docProxy = mock(RT2DocProxy.class);

        when(docProxy.isOnline()).thenReturn(true);
        when(webSocket.getId()).thenReturn(rt2ChannelId);
        when(rt2WSApp.contains(rt2ChannelId)).thenReturn(false);
        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(rt2ClientUid, rt2DocUid)).thenReturn(docProxy);

        sut.onClose(webSocket, dataFrame);

        verify(docProxy, times(1)).goOffline();
        verify(rt2WSChannelDisposer, times(1)).removeChannel(webSocket);
        verify(sessionCountValidator, times(1)).removeSession(rt2ChannelId);
    }

    @Test
    public void rt2WebSocketListenerOnCloseSocketNOTPartOfRT2WSAppAndException() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);
        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var dataFrame = mock(DataFrame.class);
        var docProxy = mock(RT2DocProxy.class);

        when(docProxy.isOnline()).thenThrow(new RuntimeException());
        when(webSocket.getId()).thenReturn(rt2ChannelId);
        when(rt2WSApp.contains(rt2ChannelId)).thenReturn(false);
        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(rt2ClientUid, rt2DocUid)).thenReturn(docProxy);

        sut.onClose(webSocket, dataFrame);

        verify(docProxy, times(0)).goOffline();
        verify(rt2WSChannelDisposer, times(1)).removeChannel(webSocket);
        verify(sessionCountValidator, times(1)).removeSession(rt2ChannelId);
    }

    @Test
    public void rt2WebSocketListenerOnConnect() {
        var webSocket = mock(RT2EnhDefaultWebSocket.class);

        assertDoesNotThrow(() -> sut.onConnect(webSocket));

        verifyNoInteractions(webSocket);
    }

    @Test
    public void rt2WebSocketListenerOnMessageWithPong() throws Exception {
        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var pingMsgStr = jsonFromMessage(RT2MessageFactory.newPong());

        when(webSocket.getId()).thenReturn(rt2ChannelId);

        // TODO: onMessage uses asynchronous processing with an ExecutorService and currently we cannot mock this
        // This must be refactored to be testable!
        sut.onMessage(webSocket, pingMsgStr.getBytes(StandardCharsets.UTF_8));

        // TODO: don't check anything with the mock as we don't know when
        // the executor service processes the message
    }

    @Test
    public void rt2WebSocketListenerOnMessageInvalidJSON() throws Exception {
        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var badJsonMsg = "bad json";

        when(webSocket.getId()).thenReturn(rt2ChannelId);

        sut.onMessageSync(webSocket, badJsonMsg);

        verifyNoInteractions(rt2WSChannelDisposer);
        verifyNoInteractions(rt2DocProxyRegistry);
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithPong() throws Exception {
        var pingMsgStr = jsonFromMessage(RT2MessageFactory.newPong());
        var set = prepareSet(rt2ClientUid, rt2DocUid);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);

        when(docProxy.isOnline()).thenReturn(false);
        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(rt2ClientUid, rt2DocUid)).thenReturn(docProxy);
        when(webSocket.getId()).thenReturn(rt2ChannelId);
        doAnswer(invocation -> {
            var args = invocation.getArguments();
            socketAvailable.set((Boolean)args[0]);
            return null;
        }).when(webSocket).setAvaible(anyBoolean());
        when(webSocket.isAvaible()).thenAnswer(invocation -> {
            return socketAvailable.get();
        });

        sut.onMessageSync(webSocket, pingMsgStr);

        verify(webSocket, times(2)).updateLastRecMsgTime();
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
    }

    @Test
    public void rt2WebSocketListenerOnMessageWithPongAndNoDocProxy() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);
        var pingMsgStr = jsonFromMessage(RT2MessageFactory.newPong());

        var webSocket = mock(RT2EnhDefaultWebSocket.class);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(eq(rt2ClientUid), eq(rt2DocUid))).thenReturn(null);
        prepareSocket(webSocket, rt2ChannelId);

        sut.onMessageSync(webSocket, pingMsgStr);

        verify(webSocket, times(2)).updateLastRecMsgTime();
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
    }

    @Test
    public void rt2WebSocketListenerOnMessageWithPongAndNoDocProxyAndThrowException() throws Exception {
        var runtimeException = new RuntimeException("Unknown error!");
        var pingMsgStr = jsonFromMessage(RT2MessageFactory.newPong());

        var webSocket = mock(RT2EnhDefaultWebSocket.class);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenThrow(runtimeException);
        when(webSocket.getId()).thenReturn(rt2ChannelId);
        prepareSocket(webSocket, rt2ChannelId);

        sut.onMessageSync(webSocket, pingMsgStr);

        verify(webSocket, times(2)).updateLastRecMsgTime();
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequest() throws Exception {
        var set = new HashSet<Pair<RT2CliendUidType, RT2DocUidType>>();

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getOrCreateDocProxy(any(RT2Message.class), eq(true), eq(rt2ChannelId), eq(sessionId))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(docProxy, times(1)).join(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
        verify(webSocketResponseMetricService, times(1)).incMessageSucc(any(RT2MessageIdType.class));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithOpenDocRequest() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);

        var openDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_OPEN_DOC, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(openDocMsg, sessionIdStr);
        var openDocMsgStr = jsonFromMessage(openDocMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, openDocMsgStr);

        verify(docProxy, times(0)).join(any(RT2Message.class));
        verify(docProxy, times(1)).open(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
        verify(webSocketResponseMetricService, times(1)).incMessageSucc(any(RT2MessageIdType.class));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithOpenDocRequestWithoutDocProxy() throws Exception {
        var emptySet = new HashSet<Pair<RT2CliendUidType, RT2DocUidType>>();

        var openDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_OPEN_DOC, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(openDocMsg, sessionIdStr);
        var openDocMsgStr = jsonFromMessage(openDocMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        prepareSocket(webSocket, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(emptySet);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(null);

        sut.onMessageSync(webSocket, openDocMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(1)).addMessageToResponseQueue(any(RT2Message.class));
        verify(webSocketResponseMetricService, times(0)).incMessageSucc(any(RT2MessageIdType.class));

        verify(rt2WSChannelDisposer).addMessageToResponseQueue(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());

        // verify that we log GENERAL_CLIENT_UID_UNKNOWN_ERROR with log-level INFO
        var msgs = loggerRule.getLoggingEvents();
        verifyContainsLoggingEvent(msgs, ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR.getDescription(), Level.INFO);
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithNackSimpleWithoutDocProxy() throws Exception{
        var emptySet = new HashSet<Pair<RT2CliendUidType, RT2DocUidType>>();

        var ackSimpleMsg = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(ackSimpleMsg, sessionIdStr);
        var ackSimpleMsgStr = jsonFromMessage(ackSimpleMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        prepareSocket(webSocket, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(emptySet);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(null);

        sut.onMessageSync(webSocket, ackSimpleMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(webSocketResponseMetricService, times(0)).incMessageSucc(any(RT2MessageIdType.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithCloseDocRequest() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);

        var closeDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_DOC, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(closeDocMsg, sessionIdStr);
        var closeDocMsgStr = jsonFromMessage(closeDocMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, closeDocMsgStr);

        verify(docProxy, times(0)).join(any(RT2Message.class));
        verify(docProxy, times(0)).open(any(RT2Message.class));
        verify(docProxy, times(1)).close(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
        verify(webSocketResponseMetricService, times(1)).incMessageSucc(any(RT2MessageIdType.class));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithAbortOpenRequest() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);

        var abortOpenMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_ABORT_OPEN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(abortOpenMsg, sessionIdStr);
        var abortOpenMsgStr = jsonFromMessage(abortOpenMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, abortOpenMsgStr);

        verify(docProxy, times(0)).join(any(RT2Message.class));
        verify(docProxy, times(0)).open(any(RT2Message.class));
        verify(docProxy, times(0)).close(any(RT2Message.class));
        verify(docProxy, times(1)).abortOpen(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithApplyOpsRequest() throws Exception {
        var set = prepareSet(rt2ClientUid, rt2DocUid);

        var applyOpsMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(applyOpsMsg, sessionIdStr);
        var applyOpsMsgStr = jsonFromMessage(applyOpsMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(set);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, applyOpsMsgStr);

        verify(docProxy, times(0)).join(any(RT2Message.class));
        verify(docProxy, times(0)).open(any(RT2Message.class));
        verify(docProxy, times(0)).close(any(RT2Message.class));
        verify(docProxy, times(0)).abortOpen(any(RT2Message.class));
        verify(docProxy, times(1)).sendRequest(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));

        verify(docProxy).sendRequest(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.REQUEST_APPLY_OPS, rt2Message.getType());
    }

    @Test
    public void rt2WebSocketListenerEmergencyLeave() throws Exception {
        var leaveData = new JSONObject();

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.emergencyLeave(webSocket, rt2DocUid.getValue(), rt2ClientUid.getValue(), sessionId.getValue(), leaveData);

        verify(rt2WSChannelDisposer, times(1)).addDocUidToChannel(eq(webSocket), eq(rt2ClientUid), eq(rt2DocUid));
        verify(docProxy, times(1)).addMsgFromWebSocket(any(RT2Message.class));
        verify(webSocket, times(0)).send(any(RT2Message.class));

        verify(docProxy).addMsgFromWebSocket(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.REQUEST_EMERGENCY_LEAVE, rt2Message.getType());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndAdvisoryLockInfo() throws Exception {
        var upgradeRequest = mock(HttpServletRequest.class);

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(upgradeRequest.getHeader(eq("origin"))).thenReturn("https://appsuite.example.org");
        when(webSocket.getAdvisoryLockMode()).thenReturn(AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN);
        when(webSocket.getUpgradeRequest()).thenReturn(upgradeRequest);
        when(rt2DocProxyRegistry.getOrCreateDocProxy(any(RT2Message.class), eq(true), eq(rt2ChannelId), eq(sessionId))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(docProxy, times(1)).join(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithOpenDocRequestAndAdvisoryLockInfo() throws Exception {
        var upgradeRequest = mock(HttpServletRequest.class);

        var openDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_OPEN_DOC, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(openDocMsg, sessionIdStr);
        var openDocMsgStr = jsonFromMessage(openDocMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(upgradeRequest.getHeader(eq("origin"))).thenReturn("https://appsuite.example.org");
        when(webSocket.getAdvisoryLockMode()).thenReturn(AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN);
        when(webSocket.getUpgradeRequest()).thenReturn(upgradeRequest);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(docProxy);

        sut.onMessageSync(webSocket, openDocMsgStr);

        verify(docProxy, times(0)).join(any(RT2Message.class));
        verify(docProxy, times(1)).open(any(RT2Message.class));
        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndInvalidSession() throws Exception {
        var emptyList = new ArrayList<RT2LogInfo>();
        var invalidSessionException = new RT2SessionInvalidException(emptyList);

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2DocProxyRegistry.getDocProxy(eq(rt2ClientUid), eq(rt2DocUid))).thenReturn(docProxy);
        doThrow(invalidSessionException).when(sessionValidator).validateSession(any(RT2Message.class), eq(webSocket));

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(1)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verifyNoInteractions(sessionCountValidator);
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(1)).deregisterDocProxy(eq(docProxy), eq(false));
        verify(docProxy, times(1)).closeHard(Cause.SESSION_INVALID);

        verify(webSocket).send(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());
        assertEquals(ErrorCode.GENERAL_SESSION_INVALID_ERROR.getCode(), rt2Message.getError().getValue().getCode());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndInvalidSessionAndfExceptionOnCloseHard() throws Exception {
        var emptyList = new ArrayList<RT2LogInfo>();
        var invalidSessionException = new RT2SessionInvalidException(emptyList);
        var rt2Exception = new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, emptyList);

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        doThrow(invalidSessionException).when(sessionValidator).validateSession(any(RT2Message.class), eq(webSocket));
        when(rt2DocProxyRegistry.getDocProxy(eq(rt2ClientUid), eq(rt2DocUid))).thenReturn(docProxy);
        doThrow(rt2Exception).when(docProxy).closeHard(Cause.SESSION_INVALID);

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(1)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verifyNoInteractions(sessionCountValidator);
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));
        verify(docProxy, times(1)).closeHard(Cause.SESSION_INVALID);

        verify(webSocket).send(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());
        assertEquals(ErrorCode.GENERAL_SESSION_INVALID_ERROR.getCode(), rt2Message.getError().getValue().getCode());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndRT2TypedExceptionWithError() throws Exception {
        var emptyList = new ArrayList<RT2LogInfo>();
        var rt2Exception = new RT2TypedException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR, emptyList);

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2DocProxyRegistry.getOrCreateDocProxy(any(RT2Message.class), eq(true), eq(rt2ChannelId), eq(sessionId))).thenReturn(docProxy);
        doThrow(rt2Exception).when(docProxy).join(any(RT2Message.class));

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));

        verify(rt2WSChannelDisposer).addMessageToResponseQueue(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());
        assertEquals(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR.getCode(), rt2Message.getError().getValue().getCode());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndRT2TypedExceptionWithWarning() throws Exception {
        var emptyList = new ArrayList<RT2LogInfo>();
        var rt2Exception = new RT2TypedException(ErrorCode.ADVISORY_LOCK_SET_WARNING, emptyList);

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2DocProxyRegistry.getOrCreateDocProxy(any(RT2Message.class), eq(true), eq(rt2ChannelId), eq(sessionId))).thenReturn(docProxy);
        doThrow(rt2Exception).when(docProxy).join(any(RT2Message.class));

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));

        verify(rt2WSChannelDisposer).addMessageToResponseQueue(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());
        assertEquals(ErrorCode.ADVISORY_LOCK_SET_WARNING.getCode(), rt2Message.getError().getValue().getCode());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithJoinRequestAndRuntimeException() throws Exception {
        var runtimeException = new RuntimeException();

        var joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(joinMsg, sessionIdStr);
        var joinMsgStr = jsonFromMessage(joinMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        var docProxy = mock(RT2DocProxy.class);
        prepareSocketAndDocProxyMocks(webSocket, docProxy, rt2ChannelId);

        when(rt2DocProxyRegistry.getOrCreateDocProxy(any(RT2Message.class), eq(true), eq(rt2ChannelId), eq(sessionId))).thenReturn(docProxy);
        doThrow(runtimeException).when(docProxy).join(any(RT2Message.class));

        sut.onMessageSync(webSocket, joinMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).addMessageToResponseQueue(any(RT2Message.class));
        verify(rt2DocProxyRegistry, times(0)).deregisterDocProxy(eq(docProxy), eq(false));

        verify(rt2WSChannelDisposer).addMessageToResponseQueue(rt2MessageCaptor.capture());
        var rt2Message = rt2MessageCaptor.getValue();
        assertEquals(RT2MessageType.RESPONSE_GENERIC_ERROR, rt2Message.getType());
        assertEquals(ErrorCode.GENERAL_UNKNOWN_ERROR.getCode(), rt2Message.getError().getValue().getCode());
    }

    @Test
    public void rt2WebSocketListenerOnMessageSyncWithSyncStableRequestAndWithoutDocProxy() throws Exception {
        var emptySet = new HashSet<Pair<RT2CliendUidType, RT2DocUidType>>();

        var openDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_SYNC_STABLE, rt2ClientUid, rt2DocUid);
        RT2MessageGetSet.setSessionID(openDocMsg, sessionIdStr);
        var openDocMsgStr = jsonFromMessage(openDocMsg);

        var webSocket = mock(RT2EnhDefaultWebSocket.class);
        prepareSocket(webSocket, rt2ChannelId);

        when(rt2WSChannelDisposer.getClientDocPairsAssociatedToChannel(rt2ChannelId)).thenReturn(emptySet);
        when(rt2DocProxyRegistry.getDocProxy(RT2DocProxy.calcID(rt2ClientUid, rt2DocUid))).thenReturn(null);

        sut.onMessageSync(webSocket, openDocMsgStr);

        verify(webSocket, times(1)).updateLastRecMsgTime();
        verify(webSocket, times(0)).send(any(RT2Message.class));
        verify(sessionValidator, times(1)).validateSession(any(RT2Message.class), eq(webSocket));
        verify(sessionCountValidator, times(1)).addSession(eq(sessionId), eq(rt2ChannelId));
        verify(rt2WSChannelDisposer, times(1)).notifyUnavailability(any(Direction.class), eq(webSocket), eq(0L));
        verify(rt2WSChannelDisposer, times(0)).addMessageToResponseQueue(any(RT2Message.class));
    }

    private String jsonFromMessage(RT2Message msg) throws Exception {
        var tmpJsonObj = RT2MessageFactory.toJSON(msg);
        return tmpJsonObj.toString();
    }

    private Set<Pair<RT2CliendUidType, RT2DocUidType>> prepareSet(RT2CliendUidType clientUid, RT2DocUidType docUid) {
        var set = new HashSet<Pair<RT2CliendUidType, RT2DocUidType>>();
        var pair = new PairMock(clientUid, docUid);
        set.add(pair);
        return set;
    }

    private void prepareSocketAndDocProxyMocks(@NotNull RT2EnhDefaultWebSocket webSocketMock, @NotNull RT2DocProxy docProxyMock, RT2ChannelId channelId) {
        assert(Mockito.mockingDetails(webSocketMock).isMock());
        assert(Mockito.mockingDetails(docProxyMock).isMock());

        when(docProxyMock.isOnline()).thenReturn(true);
        prepareSocket(webSocketMock, channelId);
    }

    private void prepareSocket(@NotNull RT2EnhDefaultWebSocket webSocketMock, RT2ChannelId channelId) {
        when(webSocketMock.getId()).thenReturn(channelId);

        doAnswer(invocation -> {
            var args = invocation.getArguments();
            socketAvailable.set((Boolean)args[0]);
            return null;
        }).when(webSocketMock).setAvaible(anyBoolean());
        when(webSocketMock.isAvaible()).thenAnswer(invocation -> {
            return socketAvailable.get();
        });

        doAnswer(invocation -> {
            var args = invocation.getArguments();
            socketCurrentProcessingRequest.set((Boolean)args[0]);
            return null;
        }).when(webSocketMock).setCurrentlyProcessingRequest(anyBoolean());
        when(webSocketMock.isCurrentlyProcessingRequest()).thenAnswer(invocation -> {
            return socketCurrentProcessingRequest.get();
        });
    }

    private void verifyContainsLoggingEvent(@NotNull List<ILoggingEvent> loggingEvents, @NotNull String message, Level logLevel) {
        assertTrue(loggingEvents.stream()
                            .filter(event -> event.getLevel().equals(logLevel) && event.getFormattedMessage().contains(message))
                            .count() > 0);
    }

}
