/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.anyBoolean;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;



public class MovedDeletedFilesAdminTaskProcessorTestInformation extends RT2UnittestInformation {
    TimerServiceMock timerServiceMock;

    Map<String, Session> sessionMap = new HashMap<>();

    RT2DocUidType knowsDocUid = null;

    EditableDocProcessor docProcessor;

    public void prepareSession(Session session) {
        sessionMap.put(session.getSessionID(), session);
    }

    public EditableDocProcessor getDocProcessor() {
        return docProcessor;
    }

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res =  new HashSet<>();
        res.addAll(getMandantoryDependenciesOfDocProcessor());
        res.add(TimerService.class);
        res.add(CachingFacade.class);
        res.add(ClusterService.class);
        res.add(RT2DocProcessorManager.class);
        res.add(RT2DocProxyRegistry.class);
        res.add(RT2JmsMessageSender.class);
        res.add(DistributedDocInfoMap.class);
        return res;
    }

    @Override
    protected void initMocks() {
        docProcessor = Mockito.mock(EditableDocProcessor.class);
        try {
            when(docProcessor.safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean())).thenAnswer(new Answer<ErrorCode>() {

                @Override
                public ErrorCode answer(InvocationOnMock invocation) throws Throwable {
                    return ErrorCode.NO_ERROR;
                }
                
            });
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }

        timerServiceMock = new TimerServiceMock();
        TimerService timerService = testOsgiBundleContextAndUnitTestActivator.getMock(TimerService.class);
        when(timerService.scheduleAtFixedRate(Mockito.isA(Runnable.class), Mockito.isA(Long.class), Mockito.isA(Long.class))).thenAnswer(new Answer<ScheduledTimerTask>() {
            @Override
            public ScheduledTimerTask answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return timerServiceMock.scheduleAtFixedRate((Runnable)args[0], 0, 0);
            }
        });

        SessionService sessionService = testOsgiBundleContextAndUnitTestActivator.getMock(SessionService.class);
        try {
            when(sessionService.getSession4Id(Mockito.anyString())).thenAnswer(new Answer<Session>() {
                @Override
                public Session answer(InvocationOnMock invocation) throws Throwable {
                  Object[] args = invocation.getArguments();
                  String sessionId = (String)args[0];
                  Session session = sessionMap.get(sessionId);
                  if (session == null) {
                      session = new SessionMock(sessionId);
                      sessionMap.put(sessionId, session);
                  }
                  return session;
                }
            });
        } catch (OXException e) {
            throw new RuntimeException(e);
        }

        ConfigurationService cfgService = Mockito.mock(ConfigurationService.class);
        Mockito.when(cfgService.getBoolProperty(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(false);

        RT2DocProcessorManager rt2DocProcMgr = testOsgiBundleContextAndUnitTestActivator.getMock(RT2DocProcessorManager.class);
        when(rt2DocProcMgr.getWeakReferenceToDocProcessors()).thenAnswer(new Answer<Set<WeakReference<RT2DocProcessor>>>() {

            @Override
            public Set<WeakReference<RT2DocProcessor>> answer(InvocationOnMock invocation) throws Throwable {
                Set<WeakReference<RT2DocProcessor>> result = new HashSet<>();
                RT2DocUidType knownDocUid = getDocUid();
                if (knownDocUid != null) {
                    WeakReference<RT2DocProcessor> weakRefToDocProc = new WeakReference<>(docProcessor);
                    result.add(weakRefToDocProc);
                }
                return result;
            }
        });
    }
}
