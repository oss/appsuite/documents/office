/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.glassfish.grizzly.http.HttpRequestPacket;
import org.glassfish.grizzly.websockets.ProtocolHandler;
import org.glassfish.grizzly.websockets.WebSocketListener;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.InitializingBean;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.EditableDocumentStatus;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.osgi.RT2CoreBundleContextIdentificator;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.core.sequence.QueueProcessorDisposer;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2EnhDefaultWebSocket;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.osgi.context.IOsgiBundleContextIdentificator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.common.user.LoadState;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;
import test.com.openexchange.office.rt2.core.util.TestConstants;
import test.com.openexchange.office.rt2.core.util.UnittestMetadataRule;

@ExtendWith({ InUnitTestRule.class, UnittestMetadataRule.class })
public abstract class BaseTest {

    @RegisterExtension
    public InUnitTestRule inUnitTestRule = new InUnitTestRule();

    @RegisterExtension
    public UnittestMetadataRule unittestMetadata = new UnittestMetadataRule();

    public static int DEFAULT_PING_FREQ = 600000;

    protected TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    protected RT2UnittestInformation unittestInformation;

    protected RT2EnhDefaultWebSocket rt2EnhDefaultWebSocket;

    protected RT2DocProxyStateHolderFactory docProxyStateHolderFactory = new RT2DocProxyStateHolderFactory();

    protected RT2DocProcessorJmsConsumer docProcJmsCons;

    // Services
    protected RT2DocProcessorManager docProcMngr;
    protected RT2JmsMessageSender jmsMessageSender;

    protected ClientSequenceQueueDisposerService clientSequenceQueueDisposerService;

    protected MsgBackupAndAckProcessorService msgBackupAndAckProcessorService;
    protected RT2DocProxyRegistry docProxyRegistry;
    protected RT2WSChannelDisposer channelDisposer;
    protected RT2WebSocketListener webSocketListener;

    protected ClientEventService clientEventService;

    protected DistributedDocInfoMap distributedDocInfoMap;

    // Additional Attributes
    protected String nodeUUID = UUID.randomUUID().toString();

    protected RT2CliendUidType clientId1;
    protected RT2CliendUidType clientId2;
    protected RT2CliendUidType clientId3;
    protected List<RT2CliendUidType> clientIds = new ArrayList<>();

    protected RT2DocUidType docUid1;

    protected abstract void initServices();

    protected abstract RT2Message createTestMessage();

    @BeforeEach
    public void init() throws Exception {
        assertTrue(unittestMetadata.getCountClients() > 0, "Annotation CountClients was not set! CountClients: " + unittestMetadata.getCountClients());
        initClientAndDocIds(unittestMetadata);
        RT2UnittestInformation unittestInformation = createUnittestInformation(unittestMetadata, unittestMetadata.getCountClients());
        initServices();
        mockSession();
        unittestInformation.initClusterLock();
    }

    @AfterEach
    public void afterTest() {
        this.clientId1 = null;
        this.clientId2 = null;
        this.clientId3 = null;
        this.clientIds.clear();
        this.docUid1 = null;
    }

    protected void setAdditionalFieldsInUnittestInformation() {}

    protected RT2UnittestInformation createUnittestInformation(UnittestMetadataRule unittestMetadata, int count) throws OXException {
        this.unittestInformation = (RT2UnittestInformation) unittestMetadata.getUnittestInformation();
        this.unittestInformation.setDocUid(docUid1);
        setAdditionalFieldsInUnittestInformation();
        createUnitTestBundle(unittestInformation);
        return unittestInformation;
    }

    protected void initClientAndDocIds(UnittestMetadataRule unittestMetadata) {
        switch (unittestMetadata.getCountClients()) {
            case 3: clientId3 = new RT2CliendUidType(UUID.randomUUID().toString());
                    clientIds.add(clientId3);
            case 2: clientId2 = new RT2CliendUidType(UUID.randomUUID().toString());
                    clientIds.add(clientId2);
            case 1: clientId1 = new RT2CliendUidType(UUID.randomUUID().toString());
                    clientIds.add(clientId1);
                    break;
            default: fail();
        }
        Collections.reverse(clientIds);
        docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
    }

    protected IOsgiBundleContextIdentificator getOsgiBundleContextIdentificator() {
        return new RT2CoreBundleContextIdentificator();
    }

    protected void createUnitTestBundle(RT2UnittestInformation unittestInformation) throws OXException {
        this.unittestInformation = unittestInformation;
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();
    }

    protected void initRT2EnhDefWebSocket() {
        ProtocolHandler protocolHandler = Mockito.mock(ProtocolHandler.class);
        HttpRequestPacket requestPacket = Mockito.mock(HttpRequestPacket.class);
        RT2ChannelId channelId = new RT2ChannelId("channelId");
        WebSocketListener [] listeners = {unitTestBundle.getService(RT2WebSocketListener.class)};
        rt2EnhDefaultWebSocket = new RT2EnhDefaultWebSocket(protocolHandler, requestPacket, listeners, DEFAULT_PING_FREQ, channelId, AdvisoryLockMode.ADVISORY_LOCK_MODE_NOT_ENABLED);
    }

    protected void mockSession() {
        SessionService sessionService = unitTestBundle.getMock(SessionService.class);
        ServerSession serverSession = Mockito.mock(ServerSession.class);
        try {
            Mockito.when(sessionService.getSession4Id("testSessionId")).thenReturn(serverSession);
        } catch (OXException e) {
            fail(e.getMessage());
        }
        User mockUser = Mockito.mock(User.class);
        Mockito.when(mockUser.getGivenName()).thenReturn("testGivename");
        Mockito.when(mockUser.getSurname()).thenReturn("testSurname");
        Mockito.when(serverSession.getUser()).thenReturn(mockUser);
    }

    protected EditableDocProcessor initDocProcessor(LoadState loadState) throws Exception {
        TextDocProcessor textDocProcessor = new TextDocProcessor();
        unitTestBundle.injectDependencies(textDocProcessor);
        textDocProcessor.setDocUID(docUid1);
        for (RT2CliendUidType client : clientIds) {
            textDocProcessor.addUser(client, TestConstants.FOLDER_ID, TestConstants.FILE_ID, TestConstants.SESSION_ID, nodeUUID);
            if (loadState != null) {
                textDocProcessor.getUserData(client).setLoadState(loadState);
            }
        }
        EditableDocumentStatus editableDocumentStatus = (EditableDocumentStatus) textDocProcessor.getDocumentStatus();
        editableDocumentStatus.setCurrentEditingUser(clientIds.get(0), "CurrentEditingUser");
        unitTestBundle.getService(RT2DocProcessorManager.class).docProcessorCreated(textDocProcessor);
        addClients();
        return textDocProcessor;
    }

    protected void initDocProcessor(RT2DocUidType docUid1, String nodeUUID, LoadState loadState, RT2CliendUidType...clients) throws Exception {
        TextDocProcessor textDocProcessor = new TextDocProcessor();
        unitTestBundle.injectDependencies(textDocProcessor);
        textDocProcessor.setDocUID(docUid1);
        for (RT2CliendUidType client : clients) {
            textDocProcessor.addUser(client, TestConstants.FOLDER_ID, TestConstants.FILE_ID, TestConstants.SESSION_ID, nodeUUID);
            textDocProcessor.getUserData(client).setLoadState(loadState);
        }
        EditableDocumentStatus editableDocumentStatus = (EditableDocumentStatus) textDocProcessor.getDocumentStatus();
        editableDocumentStatus.setCurrentEditingUser(clients[0], "CurrentEditingUser");
        unitTestBundle.getService(RT2DocProcessorManager.class).docProcessorCreated(textDocProcessor);
        addClients();
    }

    protected void prepareObject(Object obj) {
        unitTestBundle.injectValues(obj);
        unitTestBundle.injectDependencies(obj);
        if (obj instanceof OsgiBundleContextAware) {
            ((OsgiBundleContextAware) obj).setApplicationContext(unitTestBundle);
        }
        if (obj instanceof InitializingBean) {
            try {
                ((InitializingBean) obj).afterPropertiesSet();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        unitTestBundle.registerServiceToEventServices(obj);
    }

    protected void addClients() {
        for (RT2CliendUidType clientUid : clientIds) {
            RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientUid, docUid1);
            unitTestBundle.getService(ClientEventService.class).notifyClientAdded(msg);
        }
    }

    protected TextMessage createAmqMessage(RT2Message rt2Msg) throws JMSException {
        TextMessage txtMsg = new ActiveMQTextMessage();
        txtMsg.setText(RT2MessageFactory.toJSONString(rt2Msg));
        RT2MessageJmsPostProcessor rt2MessageJmsPostProcessor = new RT2MessageJmsPostProcessor(rt2Msg);;
        txtMsg = (TextMessage) rt2MessageJmsPostProcessor.postProcessMessage(txtMsg);
        return txtMsg;
    }

    protected void waitForLastProcessedMessage() throws InterruptedException {
        QueueProcessorDisposer queueProcessorDisposer = unitTestBundle.getService(QueueProcessorDisposer.class);
        while (queueProcessorDisposer.getLastProcessedMessage() == null) {
            Thread.sleep(500);
        }
    }

    protected List<RT2Message> validateJmsMessageSenderForSingleMessage() {
        ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
        Mockito.verify(jmsMessageSender, times(1)).sendToClientResponseTopic(rt2MsgCaptor.capture());
        Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
        List<RT2Message> messages = rt2MsgCaptor.getAllValues();
        assertEquals(1, messages.size());
        return messages;
    }

    protected List<RT2Message> validateJmsMessageSenderForTwoMessages() {
        ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
        Mockito.verify(jmsMessageSender, times(2)).sendToClientResponseTopic(rt2MsgCaptor.capture());
        Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
        List<RT2Message> messages = rt2MsgCaptor.getAllValues();
        assertEquals(2, messages.size());
        return messages;
    }

    protected MessagesContainer validateJmsMessageSenderForThreeMessages() {
        ArgumentCaptor<BroadcastMessage> broadcastMsgCaptor = ArgumentCaptor.forClass(BroadcastMessage.class);
        ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
        Mockito.verify(jmsMessageSender, times(1)).sendToClientResponseTopic(rt2MsgCaptor.capture());
        Mockito.verify(jmsMessageSender, times(1)).sendBroadcastMessage(broadcastMsgCaptor.capture());
        List<RT2Message> messages = rt2MsgCaptor.getAllValues();
        assertEquals(1, messages.size());
        List<BroadcastMessage> broadcastMsgs = broadcastMsgCaptor.getAllValues();
        assertEquals(1, broadcastMsgs.size());
        return new MessagesContainer(messages, broadcastMsgs);
    }

    protected void defaultTestBeforeProcessorMessaging() throws Exception, JMSException {
        RT2Message testRequest = createTestMessage();

        initDocProcessor(null);

        TextMessage txtMsg = createAmqMessage(testRequest);
        docProcJmsCons.onMessage(txtMsg);
    }
}
