/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.timer.ScheduledTimerTask;

public class ScheduledTimerTaskMock implements ScheduledTimerTask {
    TimerServiceMock timerServiceMock;
    Runnable task = null;
    AtomicLong triggered = new AtomicLong(0);

    public ScheduledTimerTaskMock(TimerServiceMock timerServiceMock, Runnable task) {
        this.timerServiceMock = timerServiceMock;
        this.task = task;
    }

    public long triggerCount() {
        return triggered.get();
    }

    public void triggerTask() {
        if (task != null) {
            triggered.incrementAndGet();
            task.run();
        }
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        timerServiceMock.removeScheduledTimerTask(this);
        return true;
    }

    @Override
    public boolean cancel() {
        timerServiceMock.removeScheduledTimerTask(this);
        return true;
    }

}
