/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class EmergencyLeaveTest extends BaseTest {

    @Override
    protected void initServices() {
        this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
        this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
        this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
        this.msgBackupAndAckProcessorService = unitTestBundle.getService(MsgBackupAndAckProcessorService.class);
        this.clientSequenceQueueDisposerService = unitTestBundle.getService(ClientSequenceQueueDisposerService.class);
        this.clientEventService = unitTestBundle.getService(ClientEventService.class);
        this.distributedDocInfoMap = unitTestBundle.getMock(DistributedDocInfoMap.class);
    }

    @Override
    protected RT2Message createTestMessage() {
        RT2Message leaveRequest = TestRT2MessageCreator.createEmergencyLeaveRequestMsg(clientId1, docUid1, new RT2SessionIdType("testSessionId"));
        return leaveRequest;
    }

    @Test
    @UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
    public void testSingleClient() throws Exception {
        clientSequenceQueueDisposerService.docCreated(docUid1);
        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId1, docUid1));

        defaultTestBeforeProcessorMessaging();

        waitForLastProcessedMessage();

        validateDocProcessor(clientId1, 0);

        List<RT2Message> messages = validateJmsMessageSenderForSingleMessage();
        RT2Message emergencyLeaveResponse = TestRT2MessageCreator.createEmergencyLeaveResponseMsg(clientId1, docUid1);
        RT2MessageValidator.validate(emergencyLeaveResponse, messages.get(0), "msg_id_header");
    }

    @Test
    @UnittestMetadata(countClients=2, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
    public void testTwoClients() throws Exception {
        Mockito.when(distributedDocInfoMap.removeClient(docUid1, clientId1)).thenReturn(new HashSet<>(Arrays.asList(clientId2)));

        clientSequenceQueueDisposerService.docCreated(docUid1);

        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId1, docUid1));
        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId2, docUid1));

        defaultTestBeforeProcessorMessaging();

        waitForLastProcessedMessage();

        validateDocProcessor(clientId1, 1);

        ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
        Mockito.verify(jmsMessageSender, times(3)).sendToClientResponseTopic(rt2MsgCaptor.capture());
        Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());

        final Map<RT2MessageType, RT2Message> messages = new HashMap<>();
        rt2MsgCaptor.getAllValues().forEach(m -> messages.put(m.getType(), m));

        RT2Message emergencyLeaveResponse = TestRT2MessageCreator.createEmergencyLeaveResponseMsg(clientId1, docUid1);
        RT2MessageValidator.validate(emergencyLeaveResponse, messages.get(RT2MessageType.RESPONSE_EMERGENCY_LEAVE), "msg_id_header");

        RT2Message updateBroadcastMsg = TestRT2MessageCreator.createUpdateBroadcast(false, RT2MessageType.RESPONSE_EMERGENCY_LEAVE, clientId2, docUid1, 1, null);
        RT2MessageValidator.validate(updateBroadcastMsg, messages.get(RT2MessageType.BROADCAST_UPDATE), "msg_id_header");

        RT2Message updateClientsBroadcastMsg = TestRT2MessageCreator.createUpdateClientsBroadcast(false, RT2MessageType.RESPONSE_EMERGENCY_LEAVE, clientId2, docUid1, 2, null);
        RT2MessageValidator.validate(updateClientsBroadcastMsg, messages.get(RT2MessageType.BROADCAST_UPDATE_CLIENTS), "msg_id_header");
    }

    @Test
    @UnittestMetadata(countClients=3, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
    public void testThreeClients() throws Exception {
        Mockito.when(distributedDocInfoMap.removeClient(docUid1, clientId1)).thenReturn(new HashSet<>(Arrays.asList(clientId2, clientId3)));

        clientSequenceQueueDisposerService.docCreated(docUid1);

        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId1, docUid1));
        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId2, docUid1));
        clientEventService.notifyClientAdded(RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientId3, docUid1));

        defaultTestBeforeProcessorMessaging();

        waitForLastProcessedMessage();

        validateDocProcessor(clientId1, 1);

        ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
        Mockito.verify(jmsMessageSender, times(1)).sendToClientResponseTopic(rt2MsgCaptor.capture());
        ArgumentCaptor<BroadcastMessage> broadcastMsgCaptor = ArgumentCaptor.forClass(BroadcastMessage.class);
        Mockito.verify(jmsMessageSender, times(2)).sendBroadcastMessage(broadcastMsgCaptor.capture());

        RT2Message emergencyLeaveResponse = TestRT2MessageCreator.createEmergencyLeaveResponseMsg(clientId1, docUid1);
        RT2MessageValidator.validate(emergencyLeaveResponse, rt2MsgCaptor.getAllValues().get(0), "msg_id_header");

        List<BroadcastMessage> broadcastMessages = broadcastMsgCaptor.getAllValues();
        for (BroadcastMessage broadcastMessage : broadcastMessages) {
            switch (broadcastMessage.getMsgType()) {
                case BROADCAST_UPDATE:
                case BROADCAST_UPDATE_CLIENTS:
                    assertEquals(docUid1.getValue(), broadcastMessage.getDocUid().getValue());
                    assertEquals(2, broadcastMessage.getReceiversCount());
                    for (BroadcastMessageReceiver r : broadcastMessage.getReceiversList()) {
                        assertTrue((r.getSeqNr().getValue() == 1) || (r.getSeqNr().getValue() == 2));
                        assertTrue(r.getReceiver().getValue().equals(clientId2.getValue()) ||
                                          r.getReceiver().getValue().equals(clientId3.getValue()));
                    }
                    break;
                default:
                    fail("Illegal message type: " + broadcastMessage.getMsgType());
            }
        }
    }

    protected void validateDocProcessor(RT2CliendUidType client1, int docProcMngrSize) throws RT2TypedException {
        assertEquals(docProcMngrSize, docProcMngr.size());
        if (docProcMngrSize > 0) {
            TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
            assertFalse(msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(client1));
            assertNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(textDocProcessor.getDocUID(), client1, false));
            assertFalse(textDocProcessor.getClientsStatusClone().hasClient(client1));
        }
    }
}
