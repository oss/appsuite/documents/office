/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.logging;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.logging.SpecialLoggingCleanup;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class SpecialLoggingCleanupTest {

    @RegisterExtension
    public InUnitTestRule inUnitTestRule = new InUnitTestRule();

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private LocalDateTimeService localDateTimeService;

    private TimerService timerService;

    private RT2DocOnNodeMap rt2DocOnNodeMap;

    private SpecialLogService specialLogService;

    private ScheduledTimerTask scheduledTimerTask;

    @BeforeEach
    public void init() throws OXException {
        var unittestInformation = new SpecialLoggingCleanupUnittestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();

        localDateTimeService = unitTestBundle.getMock(LocalDateTimeService.class);
        timerService = unitTestBundle.getMock(TimerService.class);
        rt2DocOnNodeMap = unitTestBundle.getMock(RT2DocOnNodeMap.class);
        specialLogService = unitTestBundle.getMock(SpecialLogService.class);
    }

    @Test
    public void testSpecialLoggingCleanupSimple() throws Exception {
        var md5 = MessageDigest.getInstance("MD5");
        var docUid = new RT2DocUidType(md5.digest("1234567890/1234567890".getBytes()).toString());

        var sut = createAndPrepareSpecialLoggingCleanup();

        assertNotNull(sut);
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));

        var docProcessor = mock(RT2DocProcessor.class);
        when(docProcessor.getDocUID()).thenReturn(docUid);
        when(localDateTimeService.getNowAsUTC()).thenReturn(LocalDateTime.now(ZoneOffset.UTC));

        // docProcessorCreated should not change anything
        sut.docProcessorCreated(docProcessor);

        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));

        // docProcessorDisposed should add the docUid of the docProcessor to the GC map
        sut.docProcessorDisposed(docProcessor);
        assertEquals(1, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));

        sut.destroy();
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));
    }

    @Test
    public void testSpecialLoggingCleanupDestroyWithoutAfterPropertiesSet() throws Exception {
        var sut = new SpecialLoggingCleanup();
        unitTestBundle.injectDependencies(sut);

        assertDoesNotThrow(() -> sut.destroy());
    }

    @Test
    public void testSpecialLoggingCleanupAsyncCleanup() throws Exception {
        var md5 = MessageDigest.getInstance("MD5");
        var docUid1 = new RT2DocUidType(md5.digest("1234567890/1234567890".getBytes()).toString());
        var docUid2 = new RT2DocUidType(md5.digest("12345678/12345678".getBytes()).toString());
        var docUid3 = new RT2DocUidType(md5.digest("123456/123456".getBytes()).toString());
        when(rt2DocOnNodeMap.get(anyString())).thenReturn("exists");

        var sut = createAndPrepareSpecialLoggingCleanup();
        verify(timerService, times(1)).scheduleAtFixedRate(isA(Runnable.class), anyLong(), anyLong());

        assertNotNull(sut);
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());

        var now = LocalDateTime.now(ZoneOffset.UTC);
        var docProcessor1 = mock(RT2DocProcessor.class);
        var docProcessor2 = mock(RT2DocProcessor.class);
        var docProcessor3 = mock(RT2DocProcessor.class);
        when(docProcessor1.getDocUID()).thenReturn(docUid1);
        when(docProcessor2.getDocUID()).thenReturn(docUid2);
        when(docProcessor3.getDocUID()).thenReturn(docUid3);
        when(localDateTimeService.getNowAsUTC()).thenReturn(now, now);

        sut.docProcessorCreated(docProcessor1);
        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid1));

        sut.docProcessorDisposed(docProcessor1);
        assertEquals(1, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid1));

        var future24Hours = now.plusHours(24);
        when(localDateTimeService.getNowAsUTC()).thenReturn(future24Hours);

        sut.docProcessorCreated(docProcessor2);
        assertEquals(1, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid1));
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid2));

        sut.docProcessorDisposed(docProcessor2);
        assertEquals(2, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid2));

        var future36Hours = now.plusHours(36);
        when(localDateTimeService.getNowAsUTC()).thenReturn(future36Hours);

        sut.docProcessorCreated(docProcessor3);
        assertEquals(2, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid2));
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid3));

        sut.docProcessorDisposed(docProcessor3);
        assertEquals(3, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid1));
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid2));
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid3));

        var future48Hours = now.plusHours(48);
        when(localDateTimeService.getNowAsUTC()).thenReturn(future48Hours);

        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(2, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid1));

        sut.destroy();
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid2));
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid3));
    }

    @Test
    public void testSpecialLoggingCleanupAsyncCleanupWithDocOnNodeMap() throws Exception {
        var md5 = MessageDigest.getInstance("MD5");
        var docUid = new RT2DocUidType(md5.digest("1234567890/1234567890".getBytes()).toString());
        when(rt2DocOnNodeMap.get(anyString())).thenReturn(null);

        var sut = createAndPrepareSpecialLoggingCleanup();

        verify(timerService, times(1)).scheduleAtFixedRate(isA(Runnable.class), anyLong(), anyLong());

        var now = LocalDateTime.now(ZoneOffset.UTC);
        var docProcessor = mock(RT2DocProcessor.class);
        var docUidsInSpecialLogService = new HashSet<String>();
        docUidsInSpecialLogService.add(docUid.getValue());

        when(docProcessor.getDocUID()).thenReturn(docUid);
        when(localDateTimeService.getNowAsUTC()).thenReturn(now);
        when(specialLogService.getDocUidsWithEntries()).thenReturn(docUidsInSpecialLogService);

        // We don't call sut.docProcessroDisposed(docProcessor) to verify that
        // the clean-up code detects that the doc processor is not within the DocOnMap
        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(1, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));

        var future48Hours = now.plusHours(48);
        when(localDateTimeService.getNowAsUTC()).thenReturn(future48Hours);

        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));
        verify(specialLogService).removeEntriesForKey(eq(docUid.getValue()));
    }

    @Test
    public void testSpecialLoggingCleanupWithDocOnNodeMapRemoveExistingDocUidFromGCMap() throws Exception {
        var md5 = MessageDigest.getInstance("MD5");
        var docUid = new RT2DocUidType(md5.digest("1234567890/1234567890".getBytes()).toString());

        var sut = createAndPrepareSpecialLoggingCleanup();

        verify(timerService, times(1)).scheduleAtFixedRate(isA(Runnable.class), anyLong(), anyLong());

        var now = LocalDateTime.now(ZoneOffset.UTC);
        var docProcessor = mock(RT2DocProcessor.class);
        var docUidsInSpecialLogService = new HashSet<String>();
        docUidsInSpecialLogService.add(docUid.getValue());

        when(rt2DocOnNodeMap.get(anyString())).thenReturn(null);
        when(docProcessor.getDocUID()).thenReturn(docUid);
        when(localDateTimeService.getNowAsUTC()).thenReturn(now);
        when(specialLogService.getDocUidsWithEntries()).thenReturn(docUidsInSpecialLogService);

        // We don't call sut.docProcessroDisposed(docProcessor) to verify that
        // the clean-up code detects that the doc processor is not within the DocOnMap
        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(1, sut.getCountOfSpecialLoggingEntriesToGC());
        assertTrue(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));

        var future1Hour = now.plusHours(1);
        when(localDateTimeService.getNowAsUTC()).thenReturn(future1Hour);
        when(rt2DocOnNodeMap.get(anyString())).thenReturn("exists now");

        assertDoesNotThrow(() -> sut.run(), "Unexpected exception");
        assertEquals(0, sut.getCountOfSpecialLoggingEntriesToGC());
        assertFalse(sut.areSpecialLoggingMessagesForDocUidMarkedForGC(docUid));
        verify(specialLogService, times(0)).removeEntriesForKey(eq(docUid.getValue()));
    }

    @Test
    public void testSpecialLoggingCleanupSwallowsNormalExceptionsInRunnable() throws Exception {
        var sut = createAndPrepareSpecialLoggingCleanup();

    }

    private SpecialLoggingCleanup createAndPrepareSpecialLoggingCleanup() throws Exception {
        var specialLoggingCleanup = new SpecialLoggingCleanup();
        unitTestBundle.injectDependencies(specialLoggingCleanup);

        scheduledTimerTask = mock(ScheduledTimerTask.class);
        when(timerService.scheduleAtFixedRate(isA(Runnable.class), anyLong(), anyLong())).thenReturn(scheduledTimerTask);

        specialLoggingCleanup.afterPropertiesSet();
        return specialLoggingCleanup;
    }

}
