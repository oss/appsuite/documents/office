/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.jmsToWebsocket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.never;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyJmsConsumer;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolder;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.RT2MessagePostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;
import test.com.openexchange.office.rt2.proxy.TestDocProxy;

public class JoinTest extends BaseTest {

	private RT2DocProxyJmsConsumer docProxyJmsConsumer;

	@Override
	protected void initServices() {
		this.docProxyJmsConsumer = this.unitTestBundle.getService(RT2DocProxyJmsConsumer.class);
		this.docProxyRegistry = this.unitTestBundle.getService(RT2DocProxyRegistry.class);
		this.channelDisposer = this.unitTestBundle.getMock(RT2WSChannelDisposer.class);

		initRT2EnhDefWebSocket();
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message joinResponse = TestRT2MessageCreator.createJoinResponseMsg(clientId1, docUid1);
		joinResponse.setMessageID(new RT2MessageIdType());
		return joinResponse;
	}


	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JmsToWebsocketUnittestInformation.class)
	public void testSucc() throws JMSException {
		RT2DocProxyStateHolder docProxyStateHolder = Mockito.mock(RT2DocProxyStateHolder.class);
		RT2DocProxy docProxy = new TestDocProxy(clientId1, docUid1, rt2EnhDefaultWebSocket.getId(), docProxyStateHolder);
		this.unitTestBundle.prepareObject(docProxy);
		this.docProxyRegistry.registerDocProxy(docProxy);

		RT2Message joinResponse = createTestMessage();

		TextMessage txtMsg = createAmqMessage(joinResponse);

		docProxyJmsConsumer.onMessage(txtMsg);

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(channelDisposer).addMessageToResponseQueue(rt2MsgCaptor.capture());
		List<RT2Message> messages = rt2MsgCaptor.getAllValues();
		assertEquals(1, messages.size());
		joinResponse.setHeader(RT2MessagePostProcessor.HEADER_VERSION_MSG_RCV, false);
		joinResponse.setHeader(RT2MessagePostProcessor.HEADER_VERSION_MSG_SND, false);
		joinResponse.setHeader(RT2MessagePostProcessor.HEADER_GPB_MSG, false);
		joinResponse.setHeader(RT2MessageJmsPostProcessor.HEADER_MSG_TYPE, joinResponse.getType().getValue());
		RT2MessageValidator.validate(joinResponse, messages.get(0), "msg_id_header");
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JmsToWebsocketUnittestInformation.class)
	public void testFailedNoDocProxy() throws JMSException {

		RT2Message joinResponse = createTestMessage();

		TextMessage txtMsg = createAmqMessage(joinResponse);

		docProxyJmsConsumer.onMessage(txtMsg);

		Mockito.verify(channelDisposer, never()).addMessageToResponseQueue(Mockito.any());
	}
}
