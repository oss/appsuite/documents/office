/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.ITaskListener;
import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.control.TaskProcessor;

public class TaskProcessorTest
{
	//-------------------------------------------------------------------------
	private final static String THREAD_NAME = "TestTaskProcessor";

	//-------------------------------------------------------------------------
	public static class TestTask extends Task
	{
		public TestTask(String sTaskID)	{
			super(sTaskID);
		}

		@Override
		public boolean process() throws Exception {
			this.setCompleted(true);
			return false;
		}

	};

	//-------------------------------------------------------------------------
	public static class Listener implements ITaskListener<TestTask>
	{
		AtomicInteger m_aCalled = new AtomicInteger(0);

		public boolean wasCalled() { return (m_aCalled.get() > 0); }

		public int calledCount() { return m_aCalled.get(); }

		@Override
		public void taskCompleted(TestTask aCompletedTask)
		{
			m_aCalled.incrementAndGet();
		}

	}

	//-------------------------------------------------------------------------
	public static class TestTaskProcessor<T extends Task> extends TaskProcessor<T>
	{
		public TestTaskProcessor(ITaskListener<T> aListener) {
			super(aListener);
		}

		@Override
		protected String getThreadName() {
			return THREAD_NAME;
		}

		protected long getTimeout()
		{
			return 10;
		}

	}

	//-------------------------------------------------------------------------
	@Test
	public void test()
		throws Exception
	{
		final Listener                    aListener  = new Listener();
		final TestTaskProcessor<TestTask> aProcessor = new TestTaskProcessor<>(aListener);

		assertFalse(aProcessor.isStarted());
		assertTrue(aProcessor.getPendingTasks() == 0, "Newly created TaskProcessor must not have any tasks");

		final String                      sTaskID1   = UUID.randomUUID().toString();
		final String                      sTaskID2   = UUID.randomUUID().toString();
		final String                      sTaskID3   = UUID.randomUUID().toString();
		final TestTask                    aTask1     = new TestTask(sTaskID1);
		final TestTask                    aTask2     = new TestTask(sTaskID2);
		final TestTask                    aTask3     = new TestTask(sTaskID3);

		assertEquals(sTaskID1, aTask1.getTaskID());
		assertFalse(aTask1.isCompleted());

		final String aToString = aTask1.toString();
		assertTrue(aToString.indexOf(sTaskID1) >= 0);

		try
		{
			aProcessor.addTask(null);
			fail("No exception - which is not expected!");
		}
		catch (NullPointerException e)
		{
			// this is what we expect
		}
		catch (Exception e)
		{
			fail("Non expected exception!");
		}

		// does nothing
		aProcessor.shutdown();
		assertFalse(aProcessor.isStarted(), "State must be not started");

		aProcessor.start();
		assertTrue(aProcessor.isStarted(), "State must be started");

		aProcessor.start();
		assertTrue(aProcessor.isStarted(), "State must be still started");

		aProcessor.addTask(aTask1);

		Thread.sleep(1000);

		assertTrue(aProcessor.isStarted());
		assertTrue(aProcessor.getPendingTasks() == 0);
		assertTrue(aTask1.isCompleted());
		assertTrue(aListener.wasCalled());
		assertTrue(aListener.calledCount() == 1);

		aProcessor.addTask(aTask2);
		aProcessor.addTask(aTask3);

		aProcessor.shutdown();
		assertFalse(aProcessor.isStarted(), "State must be not started");
		assertTrue(aProcessor.getPendingTasks() == 0);

		// does nothing
		aProcessor.shutdown();
		assertFalse(aProcessor.isStarted(), "State must be still not started");
		assertTrue(aProcessor.getPendingTasks() == 0);

		aProcessor.removeAll();
		assertTrue(aProcessor.getPendingTasks() == 0);
	}
}
