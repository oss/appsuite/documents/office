/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.user.LoadState;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.proxy.messaging.MessagesContainer;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestConstants;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class ApplyOpsTest extends BaseTest {

	@Override
	protected void initServices() {
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
	}

	@Override
	protected TextMessage createAmqMessage(RT2Message rt2Msg) throws JMSException {
		TextMessage txtMsg = super.createAmqMessage(rt2Msg);
		txtMsg.setText("{\"operations\":[{\"osn\":23,\"opl\":1,\"n\":\"ip\",\"o\":[0],\"a\":{}},{\"osn\":24,\"opl\":1,\"n\":\"it\",\"w\":\"sdf\",\"o\":[0,0]},{\"osn\":25,\"opl\":2,\"n\":\"it\",\"w\":\"sdefg\",\"o\":[0,3]}]}");
		return txtMsg;
	}


	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testWithOneClient() throws Exception {

		RT2Message applyOpsMsg = createTestMessage();

		EditableDocProcessor txtDocProc = initDocProcessor(LoadState.LOADSTATE_COMPLETED);

		TextMessage txtMsg = createAmqMessage(applyOpsMsg);
		txtMsg.setText("{\"operations\":[{\"osn\":23,\"opl\":1,\"n\":\"ip\",\"o\":[0],\"a\":{}},{\"osn\":24,\"opl\":1,\"n\":\"it\",\"w\":\"sdf\",\"o\":[0,0]},{\"osn\":25,\"opl\":2,\"n\":\"it\",\"w\":\"sdefg\",\"o\":[0,3]}]}");
		docProcJmsCons.onMessage(txtMsg);

		waitForLastProcessedMessage();

		List<RT2Message> messages = validateJmsMessageSenderForSingleMessage();

		JSONArray jsonArray = txtDocProc.getPendingOperationsCloned().getPendingOperationsCloned();
		assertEquals(3, jsonArray.asList().size());
		RT2Message sollRespApplyOps =
				TestRT2MessageCreator.createResponseApplyOpsMsg(clientId1, docUid1, 1, "{\"serverOSN\":27}");
		RT2MessageValidator.validate(sollRespApplyOps, messages.get(0));
	}

	@Test
	@UnittestMetadata(countClients=2, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testWithTwoClients() throws Exception {
		RT2Message applyOpsMsg = createTestMessage();

		EditableDocProcessor txtDocProc = initDocProcessor(LoadState.LOADSTATE_COMPLETED);

		TextMessage txtMsg = createAmqMessage(applyOpsMsg);
		txtMsg.setText("{\"operations\":[{\"osn\":23,\"opl\":1,\"n\":\"ip\",\"o\":[0],\"a\":{}},{\"osn\":24,\"opl\":1,\"n\":\"it\",\"w\":\"sdf\",\"o\":[0,0]},{\"osn\":25,\"opl\":2,\"n\":\"it\",\"w\":\"sdefg\",\"o\":[0,3]}]}");

		docProcJmsCons.onMessage(txtMsg);

		waitForLastProcessedMessage();

		List<RT2Message> messages = validateJmsMessageSenderForTwoMessages();

		JSONArray jsonArray = txtDocProc.getPendingOperationsCloned().getPendingOperationsCloned();
		assertEquals(3, jsonArray.asList().size());
		RT2Message sollRespApplyOps =
				TestRT2MessageCreator.createResponseApplyOpsMsg(clientId1, docUid1, 1, "{\"serverOSN\":27}");
		RT2MessageValidator.validate(sollRespApplyOps, messages.get(0));
		RT2Message sollUpdateBroadcast =
				//TODO: checkBody
				TestRT2MessageCreator.createUpdateBroadcast(true, RT2MessageType.RESPONSE_APPLY_OPS, clientId2, docUid1, 1, null);
		RT2MessageValidator.validate(sollUpdateBroadcast, messages.get(1));
	}

	@Test
	@UnittestMetadata(countClients=3, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testWithThreeClients() throws Exception {
		RT2Message applyOpsMsg = createTestMessage();

		EditableDocProcessor txtDocProc = initDocProcessor(LoadState.LOADSTATE_COMPLETED);

		TextMessage txtMsg = createAmqMessage(applyOpsMsg);
		txtMsg.setText("{\"operations\":[{\"osn\":23,\"opl\":1,\"n\":\"ip\",\"o\":[0],\"a\":{}},{\"osn\":24,\"opl\":1,\"n\":\"it\",\"w\":\"sdf\",\"o\":[0,0]},{\"osn\":25,\"opl\":2,\"n\":\"it\",\"w\":\"sdefg\",\"o\":[0,3]}]}");

		docProcJmsCons.onMessage(txtMsg);

		waitForLastProcessedMessage();

		MessagesContainer messagesContainer = validateJmsMessageSenderForThreeMessages();
		List<RT2Message> messages = messagesContainer.getMessages();
		List<BroadcastMessage> broadcastMsgs = messagesContainer.getBroadcastMsgs();

		JSONArray jsonArray = txtDocProc.getPendingOperationsCloned().getPendingOperationsCloned();
		assertEquals(3, jsonArray.asList().size());
		RT2Message sollRespApplyOps =
				TestRT2MessageCreator.createResponseApplyOpsMsg(clientId1, docUid1, 1, "{\"serverOSN\":27}");
		RT2MessageValidator.validate(sollRespApplyOps, messages.get(0));
		assertEquals(docUid1.getValue(), broadcastMsgs.get(0).getDocUid().getValue());
		broadcastMsgs.get(0).getReceiversList().forEach(r -> {
			assertEquals(1, r.getSeqNr().getValue());
			assertTrue(
					r.getReceiver().getValue().equals(clientId2.getValue()) ||
					r.getReceiver().getValue().equals(clientId3.getValue()));
		});
	}

	protected TextDocProcessor createTestDocProcessor() {
		TextDocProcessor txtDocProc = (TextDocProcessor) docProcMngr.getDocProcessor(docUid1);
		txtDocProc.getUserData(clientId1).setLoadState(LoadState.LOADSTATE_COMPLETED);
		txtDocProc.getUserData(clientId2).setLoadState(LoadState.LOADSTATE_COMPLETED);
		return txtDocProc;
	}

	@Override
    protected RT2Message createTestMessage() {
		RT2Message applyOpsMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientId1, docUid1);
		applyOpsMsg.setSeqNumber(new RT2SeqNumberType(1));
		applyOpsMsg.setSessionID(new RT2SessionIdType(TestConstants.SESSION_ID));
		return applyOpsMsg;
	}
}
