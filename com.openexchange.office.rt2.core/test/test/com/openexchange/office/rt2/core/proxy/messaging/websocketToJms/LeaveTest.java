/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.websocketToJms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolder;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;
import test.com.openexchange.office.rt2.proxy.TestDocProxy;

public class LeaveTest extends BaseTest {

	private RT2DocProxyRegistry docProxyRegistry;
	private RT2WebSocketListener webSocketListener;
	private RT2JmsMessageSender jmsMessageSender;

	private RT2MessageType requestMsgType;

	@Override
	protected void initServices() {
		this.docProxyRegistry = unitTestBundle.getService(RT2DocProxyRegistry.class);
		this.webSocketListener = unitTestBundle.getService(RT2WebSocketListener.class);
		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message msg = RT2MessageFactory.newMessage(requestMsgType, clientId1, docUid1);
		msg.setSessionID(new RT2SessionIdType("testSession"));
		return msg;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=LeaveUnittestInformation.class)
	public void testLeave() throws Exception {
		this.requestMsgType = RT2MessageType.REQUEST_LEAVE;

		RT2DocProxyStateHolder docProxyStateHolder = Mockito.mock(RT2DocProxyStateHolder.class);
		initRT2EnhDefWebSocket();

		RT2DocProxy docProxy = new TestDocProxy(clientId1, docUid1, rt2EnhDefaultWebSocket.getId(), docProxyStateHolder);
		unitTestBundle.injectDependencies(docProxy);
		this.docProxyRegistry.registerDocProxy(docProxy);

		RT2Message msg = createTestMessage();
		webSocketListener.onMessageSync(rt2EnhDefaultWebSocket, RT2MessageFactory.toJSONString(msg));

		Mockito.verify(docProxyStateHolder).setForceQuit(false);

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender).sendToDocumentQueue(rt2MsgCaptor.capture(), Mockito.anyList());
		List<RT2Message> jmsMessageSenderIsMsgs = rt2MsgCaptor.getAllValues();
		assertEquals(1, jmsMessageSenderIsMsgs.size());
		RT2Message sollLeaveRequest = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_LEAVE, clientId1, docUid1);
		sollLeaveRequest.setSessionID(new RT2SessionIdType("testSession"));
		RT2MessageValidator.validate(sollLeaveRequest, jmsMessageSenderIsMsgs.get(0), "msg_id_header");
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=LeaveUnittestInformation.class)
	public void testEmergencyLeave()  throws RT2TypedException {
		this.requestMsgType = RT2MessageType.REQUEST_EMERGENCY_LEAVE;

		RT2DocProxyStateHolder docProxyStateHolder = Mockito.mock(RT2DocProxyStateHolder.class);

		initRT2EnhDefWebSocket();

		RT2DocProxy docProxy = new TestDocProxy(clientId1, docUid1, rt2EnhDefaultWebSocket.getId(), docProxyStateHolder);
		unitTestBundle.injectDependencies(docProxy);
		this.docProxyRegistry.registerDocProxy(docProxy);

		RT2Message msg = createTestMessage();
		webSocketListener.onMessageSync(rt2EnhDefaultWebSocket, RT2MessageFactory.toJSONString(msg));

		Mockito.verify(docProxyStateHolder).setForceQuit(true);

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender).sendToDocumentQueue(rt2MsgCaptor.capture(), Mockito.anyList());
		List<RT2Message> jmsMessageSenderIsMsgs = rt2MsgCaptor.getAllValues();
		assertEquals(1, jmsMessageSenderIsMsgs.size());
		RT2Message sollLeaveRequest = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_EMERGENCY_LEAVE, clientId1, docUid1);
		sollLeaveRequest.setSessionID(new RT2SessionIdType("testSession"));
		RT2MessageValidator.validate(sollLeaveRequest, jmsMessageSenderIsMsgs.get(0), "msg_id_header");
	}
}
