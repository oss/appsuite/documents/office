/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.doc.EditRightsHelper;
import com.openexchange.office.rt2.core.doc.EditableClientsStatus;
import com.openexchange.office.rt2.core.doc.MessageProperties;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

@ExtendWith(EditRightsMetadataRule.class)
public class EditRightsTest extends BaseTest {

    @RegisterExtension
    public EditRightsMetadataRule editRightsMetadataRule = new EditRightsMetadataRule();

	@Override
	protected void initServices() {
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		try {
			RT2Message request = TestRT2MessageCreator.createEditRightsRequestMsg(clientId1, docUid1, 1);
			request.getBody().put(MessageProperties.PROP_ACTION, editRightsMetadataRule.getRight());
			TextMessage txtMsg = new ActiveMQTextMessage();
			txtMsg.setText(RT2MessageFactory.toJSONString(request));
			RT2MessageJmsPostProcessor rt2MessageJmsPostProcessor = new RT2MessageJmsPostProcessor(request);;
			txtMsg = (TextMessage) rt2MessageJmsPostProcessor.postProcessMessage(txtMsg);
			txtMsg.setText("{\"action\": \"" + editRightsMetadataRule.getRight() + "\"}");
			return request;
		} catch (JSONException | JMSException ex) {
			fail(ex.getMessage());
			throw new RuntimeException(ex);
		}
	}

	@Override
	protected TextMessage createAmqMessage(RT2Message rt2Msg) throws JMSException {
		TextMessage txtMsg = super.createAmqMessage(rt2Msg);
		txtMsg.setText("{\"action\": \"" + editRightsMetadataRule.getRight() + "\", \"value\": \"" + clientId1.getValue() + "\"}");
		return txtMsg;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	@EditRightsMetadata(right=EditRightsHelper.ACTION_EDITRIGHTS_REQUESTS)
	public void testActionEditRightsRequest() throws Exception {
		testEditRights(true);
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	@EditRightsMetadata(right=EditRightsHelper.ACTION_EDITRIGHTS_DECLINED)
	public void testActionEditRightsDeclined() throws Exception {
		testEditRights(false);
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	@EditRightsMetadata(right=EditRightsHelper.ACTION_EDITRIGHTS_FORCE)
	public void testActionEditRightsForce() throws Exception {
		testEditRights(false);
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	@EditRightsMetadata(right=EditRightsHelper.ACTION_EDITRIGHTS_ALL)
	public void testActionEditRightAll() throws Exception {
		testEditRights(false);
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	@EditRightsMetadata(right=EditRightsHelper.ACTION_EDITRIGHTS_APPROVED)
	public void testActionEditRightApproved() throws Exception {
		testEditRights(false);
	}

	private void testEditRights(boolean hasEditRights) throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		final EditableClientsStatus clientsStatus = (EditableClientsStatus) textDocProcessor.getClientsStatus();
		assertArrayEquals(Arrays.asList(hasEditRights).toArray(), Arrays.asList(clientsStatus.hasEditRigths(clientId1)).toArray());

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
		Mockito.verify(jmsMessageSender, times(3)).sendToClientResponseTopic(rt2MsgCaptor.capture());
		Map<RT2MessageType, RT2Message> msgMap = new HashMap<>();
		for (RT2Message msg : rt2MsgCaptor.getAllValues()) {
			msgMap.put(msg.getType(), msg);
		}
		RT2Message editRightResponse = TestRT2MessageCreator.createEditRightsResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(editRightResponse, msgMap.get(RT2MessageType.RESPONSE_EDITRIGHTS));
		RT2Message updateBroadcast = TestRT2MessageCreator.createUpdateBroadcast(false, RT2MessageType.RESPONSE_EDITRIGHTS, clientId1, docUid1, 2, null);
		RT2MessageValidator.validate(updateBroadcast, msgMap.get(RT2MessageType.BROADCAST_UPDATE));
		RT2Message updateClientsBroadcast = TestRT2MessageCreator.createUpdateClientsBroadcast(false, RT2MessageType.RESPONSE_EDITRIGHTS, clientId1, docUid1, 3, null);
		RT2MessageValidator.validate(updateClientsBroadcast, msgMap.get(RT2MessageType.BROADCAST_UPDATE_CLIENTS));
	}
}
