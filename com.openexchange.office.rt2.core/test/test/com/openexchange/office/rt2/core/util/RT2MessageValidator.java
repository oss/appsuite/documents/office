/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.protocol.RT2Message;

public class RT2MessageValidator {

    public static void validate(RT2Message soll, RT2Message is, String...validateNotNull) {
        assertEquals(soll.getType(), is.getType());
        Set<String> validateNotNullSet = new HashSet<>();
        validateNotNullSet.add("msg_id_header");
        for (String str : validateNotNull) {
            validateNotNullSet.add(str);
        }
        Set<String> headerSoll = soll.listHeader();
        Set<String> headerIs = is.listHeader();
        assertEquals(headerSoll.size(), headerIs.size(), "Size of header of RT2Messages is not equal! Soll: " + headerSoll + ", Is: " + headerIs);
        List<String> headerListSoll = new ArrayList<>(headerSoll);
        Collections.sort(headerListSoll);
        List<String> headerListIs = new ArrayList<>(headerIs);
        Collections.sort(headerListIs);
        assertEquals(headerSoll.size(), headerIs.size(), "Header of RT2Messages is not equal! Soll: " + headerSoll + ", Is: " + headerIs);
        for (String header : headerListSoll) {
            if (validateNotNullSet.contains(header)) {
                assertNotNull(is.getHeader(header));
            } else {
                Object valueSoll = soll.getHeader(header);
                Object valueIs = is.getHeader(header);
                assertEquals(valueSoll, valueIs, "Value of header " + header + " is not equal! Soll: " + valueSoll + ", Is: " + valueIs);
            }
        }
        String sollBodyStr = soll.getBodyString();
        if (StringUtils.isNotBlank(sollBodyStr) && (!sollBodyStr.equals("{}"))) {
            String isBodyStr = is.getBodyString();
            assertEquals(sollBodyStr, isBodyStr, "Soll: " + sollBodyStr + ", is:" + isBodyStr);
        }
    }

    public static boolean isValid(RT2Message soll, RT2Message is, String...validateNotNull) {
        if (!(soll.getType().equals(is.getType()))) {
            return false;
        }

        Set<String> validateNotNullSet = new HashSet<>();
        validateNotNullSet.add("msg_id_header");
        for (String str : validateNotNull) {
            validateNotNullSet.add(str);
        }

        Set<String> headerSoll = soll.listHeader();
        Set<String> headerIs = is.listHeader();
        if ((headerSoll.size() != headerIs.size())) {
            return false;
        }

        List<String> headerListSoll = new ArrayList<>(headerSoll);
        Collections.sort(headerListSoll);
        List<String> headerListIs = new ArrayList<>(headerIs);
        Collections.sort(headerListIs);
        if (headerSoll.size() != headerIs.size()) {
            return false;
        }

        for (String header : headerListSoll) {
            if (validateNotNullSet.contains(header)) {
                if ((is.getHeader(header) == null)) {
                    return false;
                }
            } else {
                Object valueSoll = soll.getHeader(header);
                Object valueIs = is.getHeader(header);
                if (!valueSoll.equals(valueIs)) {
                    return false;
                }
            }
        }

        String sollBodyStr = soll.getBodyString();
        if (StringUtils.isNotBlank(sollBodyStr) && (!sollBodyStr.equals("{}"))) {
            String isBodyStr = is.getBodyString();
            assertEquals("Soll: " + sollBodyStr + ", is:" + isBodyStr, sollBodyStr, isBodyStr);
        }

        return true;
    }
}
