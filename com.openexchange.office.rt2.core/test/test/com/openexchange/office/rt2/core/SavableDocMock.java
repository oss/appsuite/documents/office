/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.BackgroundSaveReason;
import com.openexchange.office.rt2.core.doc.DocProcessorClientInfo;
import com.openexchange.office.rt2.core.doc.IBackgroundSavable;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.user.UserData;

public class SavableDocMock extends RT2DocProcessor implements IBackgroundSavable {
    private static final Logger log = LoggerFactory.getLogger(SavableDocMock.class);

    public enum SaveBehavior {
        SAVE_THROWS_OXEXCEPTION,
        SAVE_THROWS_EXCEPTION,
        SAVE_PROVIDES_ERROR,
        SAVE_NO_ERROR
    };

    private final String docTypeIdentifier;
    private boolean modified = false;
    private int pendingOpsCount = 0;
    private boolean saveInProgress = false;
    private long lastSaveTimeStamp = new Date().getTime();
    private ErrorCode lastSaveErrorCode = ErrorCode.NO_ERROR;
    private SaveBehavior saveBehavior = SaveBehavior.SAVE_NO_ERROR;
    private boolean processed = false;
    private int numOfSavesTries = 0;
    private int numOfSavesSuccess = 0;
    private CountDownLatch countDownLatch = null;

    public SavableDocMock(String docTypeIdentifier) {
        this.docTypeIdentifier = docTypeIdentifier;
    }

    public SavableDocMock(String docTypeIdentifier, int saveLatchValue) {
        this(docTypeIdentifier);
        countDownLatch = new CountDownLatch(saveLatchValue);
    }

    private void decCountDownLatch() {
        if (countDownLatch != null) {
            countDownLatch.countDown();
            log.debug("decCountDownLatch, latch value: {}", countDownLatch.getCount());
        }
    }

    public synchronized CountDownLatch getSaveCountDownLatch() {
        return countDownLatch;
    }

    public synchronized void setLastSaveTimeStamp(long newTimeStamp) {
        lastSaveTimeStamp = newTimeStamp;
    }

    public synchronized long numOfSaveTries() {
        return numOfSavesTries;
    }

    public synchronized long numOfSaveSuccess() {
        return numOfSavesSuccess;
    }

    public synchronized void setSaveBehavior(SaveBehavior saveBehavior) {
        log.debug("SaveBehavior changed to {}", saveBehavior);
        this.saveBehavior = saveBehavior;
    }

    public synchronized void setPendingOperationsCount(int count) {
        if (count < 0) throw new IllegalArgumentException("pending ops count cannot be lower than zero!");

        pendingOpsCount = count;
        if (count > 0) {
            modified = true;
        } else if (count == 0) {
            modified = false;
        }
    }

    @Override
    public void dispose() {
        setDisposed();
        super.dispose();
    }

    @Override
    public RT2DocUidType getUniqueID() {
        return this.getDocUID();
    }

    @Override
    public synchronized boolean isModified() {
        return modified;
    }

    @Override
    public synchronized int getPendingOperationsCount() {
        return pendingOpsCount;
    }

    @Override
    public boolean isSaveInProgress() {
        return saveInProgress;
    }

    @Override
    public synchronized long getLastSaveTimeStamp() {
        return lastSaveTimeStamp;
    }

    @Override
    public synchronized ErrorCode getLastSaveErrorCode() {
        return lastSaveErrorCode;
    }

    @Override
    public synchronized void setLastSaveErrorCode(ErrorCode lastSaveErrorCode) {
        this.lastSaveErrorCode = lastSaveErrorCode;
    }

    @Override
    public synchronized void save(BackgroundSaveReason reason, boolean force) throws Exception {
        ++numOfSavesTries;

        log.debug("SavableDocMock.save - docUid: {}, reason: {}, force: {}, saveBehavior: {}", getDocUID(), reason, force, saveBehavior);
        switch (saveBehavior) {
            case SAVE_THROWS_OXEXCEPTION: {
                decCountDownLatch();
                throw new OXException();
            }
            case SAVE_THROWS_EXCEPTION: {
                decCountDownLatch();
                throw new RuntimeException();
            }
            case SAVE_PROVIDES_ERROR: {
                setLastSaveErrorCode(ErrorCode.GENERAL_UNKNOWN_ERROR);
                decCountDownLatch();
                break;
            }
            case SAVE_NO_ERROR: {
                lastSaveTimeStamp = new Date().getTime();
                setLastSaveErrorCode(ErrorCode.NO_ERROR);
                ++numOfSavesSuccess;
                pendingOpsCount = 0;
                decCountDownLatch();
                break;
            }
        }

    }

    @Override
    public synchronized void setProcessedState(boolean bProcessState) {
        processed = bProcessState;
    }

    @Override
    public synchronized boolean isProcessed() {
        return processed;
    }

    @Override
    public boolean isOTEnabled() {
        return false;
    }

    @Override
    public boolean hasSetAdvisoryLock() {
        return false;
    }

    @Override
    public String getDocTypeIdentifier() {
        return docTypeIdentifier;
    }

    @Override
    public List<DocProcessorClientInfo> getClientsInfo() {
        return new ArrayList<DocProcessorClientInfo>();
    }

    @Override
    public void updateClientsInfo(Collection<RT2CliendUidType> existingClients) {
        // nothing to do
    }

    @Override
    public boolean enqueueMessage(RT2Message msg) throws Exception {
        return true;
    }

    @Override
    public boolean onJoin(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onCloseAndLeave(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onLeave(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onAdditionalOpenDoc(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onFirstOpenDoc(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onOpenDoc(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onCloseDoc(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public synchronized boolean onApplyOperations(RT2Message aMsg) throws Exception {
        modified = true;
        return true;
    }

    @Override
    public synchronized boolean onSaveDoc(RT2Message aMsg) throws Exception {
        modified = false;
        return true;
    }

    @Override
    public boolean onSync(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onSyncStable(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onEditRights(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onUnavailable(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public boolean onEmergencyLeave(RT2Message aMsg) throws Exception {
        return true;
    }

    @Override
    public List<UserData> getUserDatasCloned() {
        return new ArrayList<UserData>();
    }

}
