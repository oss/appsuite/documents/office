/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.util;

import static org.mockito.Mockito.lenient;
import java.util.Collection;
import java.util.UUID;

import org.mockito.Mockito;

import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;
import com.openexchange.office.tools.service.cluster.ClusterService;

public abstract class RT2UnittestInformation extends UnittestInformation {

	protected ClusterLock clusterLock;

	protected RT2ConfigService rt2ConfigService;

	protected RT2DocUidType docUid;

	protected RT2ConfigService initRT2ConfigService() {
		if (rt2ConfigService == null) {
			rt2ConfigService = testOsgiBundleContextAndUnitTestActivator.getMock(RT2ConfigService.class);
			lenient().when(rt2ConfigService.getOXNodeID()).thenReturn(UUID.randomUUID().toString());
			lenient().when(rt2ConfigService.isOTEnabled(Mockito.anyString())).thenReturn(false);
		}
		return rt2ConfigService;
	}

	protected String initHazelcastNodeId() {
		String nodeId = testOsgiBundleContextAndUnitTestActivator.getHazelcastNodeId().toString();
		ClusterService clusterService = testOsgiBundleContextAndUnitTestActivator.getMock(ClusterService.class);
		Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(nodeId);
		RT2DocOnNodeMap docOnNodeMap = this.testOsgiBundleContextAndUnitTestActivator.getMock(RT2DocOnNodeMap.class);
		Mockito.when(docOnNodeMap.get(Mockito.anyString())).thenReturn(nodeId);
		return nodeId;
	}

	public ClusterLock initClusterLock() throws ClusterLockException {
		ClusterLockService clusterLockService = this.testOsgiBundleContextAndUnitTestActivator.getMock(ClusterLockService.class);
		clusterLock = Mockito.mock(ClusterLock.class);
		Mockito.when(clusterLock.lock()).thenReturn(true);
		Mockito.when(clusterLockService.getLock(docUid)).thenReturn(clusterLock);
		return clusterLock;
	}

	public RT2DocUidType getDocUid() {
		return docUid;
	}

	public void setDocUid(RT2DocUidType docUid) {
		this.docUid = docUid;
	}

	public ClusterLock getClusterLock() {
		return clusterLock;
	}

	public void setClusterLock(ClusterLock clusterLock) {
		this.clusterLock = clusterLock;
	}

	protected Collection<Class<?>> getMandantoryDependenciesOfDocProcessor() {
		return testOsgiBundleContextAndUnitTestActivator.getMandantoryDependenciesOfClass(TextDocProcessor.class);
	}
}
