/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.sequence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.exception.RT2InvalidDocumentIdentifierException;
import com.openexchange.office.rt2.core.exception.RT2ProtocolException;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndACKProcessor;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.core.sequence.SequenceDocProcessor;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class MsgBackupAndACKProcessorTest {

	private RT2CliendUidType testClientUid1 = new RT2CliendUidType("testClientUid1");
	private RT2DocUidType testDocUid1 = new RT2DocUidType("testDocUid1");

	private RT2MessageSender msgSenderMock;
	private ClientEventService clientEventServiceMock;

	private MsgBackupAndACKProcessor msgBackupAndACKProcessor;

	private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

	@BeforeEach
	public void init() throws OXException {
		MsgBackupAndACKProcessorTestInformation unittestInformation = new MsgBackupAndACKProcessorTestInformation();
		unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
		unitTestBundle.start();

		this.msgSenderMock = unitTestBundle.getMock(RT2MessageSender.class);
		this.clientEventServiceMock = unitTestBundle.getMock(ClientEventService.class);
	}

	@Test
	public void testWithOnlineClientWithoutBadBehaviour() throws Exception {

		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(
				MsgBackupAndAckProcessorService.TIMEOUT_FOR_BAD_CLIENT,
				MsgBackupAndAckProcessorService.THRESHOLD_MAX_MSGS_WO_ACK);
		standardInit(objectToTest, false);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);

		Thread.sleep(100);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 0);
	}

	@Test
	public void testWithOnlineClientWithTimeoutButZeroMessages() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(1,
				MsgBackupAndAckProcessorService.THRESHOLD_MAX_MSGS_WO_ACK);

		standardInit(objectToTest, false);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);

		Thread.sleep(100);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 0);
	}

	@Test
	public void testWithOnlineClientWithoutTimeoutAndNotZeroMessages() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(
				MsgBackupAndAckProcessorService.TIMEOUT_FOR_BAD_CLIENT,
				MsgBackupAndAckProcessorService.THRESHOLD_MAX_MSGS_WO_ACK);
		standardInit(objectToTest, false);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);
		objectToTest.backupMessage(msg);

		Thread.sleep(100);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 0);
	}

	@Test
	public void testWithOnlineClientWithAckTimeoutAndOneMessageLeft() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(100,
				MsgBackupAndAckProcessorService.THRESHOLD_MAX_MSGS_WO_ACK);
		standardInit(objectToTest, false);
		RT2Message rt2Message = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertFalse(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 0);

		Thread.sleep(120);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, times(1)).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, times(2)).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 1);
	}

	@Test
	public void testWithOfflineClient() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(
				1, MsgBackupAndAckProcessorService.THRESHOLD_MAX_MSGS_WO_ACK);
		standardInit(objectToTest, false);
		RT2Message rt2Message = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message);
		msgBackupAndACKProcessor.switchState(testClientUid1, false);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 1);

		Thread.sleep(100);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 1);
	}

	@Test
	public void testWithOfflineClientAndMaxMsgsForSync() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(1, 1);
		standardInit(objectToTest, false);
		RT2Message rt2Message = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message);
		msgBackupAndACKProcessor.switchState(testClientUid1, false);

		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());

		Thread.sleep(100);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());

		RT2Message msg2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		msg2.setSeqNumber(new RT2SeqNumberType(2));
		objectToTest.backupMessage(msg2);
		RT2Message msg3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		msg3.setSeqNumber(new RT2SeqNumberType(3));
		objectToTest.backupMessage(msg3);
		RT2Message msg4 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		msg4.setSeqNumber(new RT2SeqNumberType(4));
		objectToTest.backupMessage(msg4);
		RT2Message msg5 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		msg5.setSeqNumber(new RT2SeqNumberType(5));
		objectToTest.backupMessage(msg5);

		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, times(1)).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, times(2)).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
	}

	@Test
	public void testWithOnlineClientAndReachedCountOfMessagesAndAckTimeout() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(200, 2);
		standardInit(objectToTest, false);
		RT2Message rt2Message1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message1.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message1);
		RT2Message rt2Message2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message2.setSeqNumber(new RT2SeqNumberType(2));
		objectToTest.backupMessage(rt2Message2);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertEquals(2, msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1));

		RT2Message rt2Message3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message3.setSeqNumber(new RT2SeqNumberType(3));
		objectToTest.backupMessage(rt2Message3);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());

		Thread.sleep(220);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, times(1)).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, times(2)).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
	}

	@Test
	public void testWithOnlineClientButReachedCountOfMessages() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(300, 2);
		standardInit(objectToTest, false);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);

		Thread.sleep(250);
		RT2Message rt2Message1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message1.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message1);
		RT2Message rt2Message2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message2.setSeqNumber(new RT2SeqNumberType(2));
		objectToTest.backupMessage(rt2Message2);

		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());

		RT2Message rt2Message3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message3.setSeqNumber(new RT2SeqNumberType(3));
		objectToTest.backupMessage(rt2Message3);

		Thread.sleep(100);
		objectToTest.run();
		objectToTest.run();

		Mockito.verify(clientEventServiceMock, times(1)).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, times(2)).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
	}

	@Test
	public void testWithOffOnlineClientAndReachedCountOfMessages() throws Exception {
		MsgBackupAndAckProcessorService objectToTest = new MsgBackupAndAckProcessorService(300, 1);
		standardInit(objectToTest, false);
		RT2Message rt2Message1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message1.setSeqNumber(new RT2SeqNumberType(1));
		objectToTest.backupMessage(rt2Message1);
		msgBackupAndACKProcessor.switchState(testClientUid1, false);

		RT2Message rt2Message2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message2.setSeqNumber(new RT2SeqNumberType(2));
		objectToTest.backupMessage(rt2Message2);
		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, testClientUid1, testDocUid1);
		objectToTest.updateClientState(msg);

		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertEquals(2, msgBackupAndACKProcessor.getMsgBackupSize((testClientUid1)));

		msgBackupAndACKProcessor.switchState(testClientUid1, true);
		Thread.sleep(100);
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, never()).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, never()).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertEquals(2, msgBackupAndACKProcessor.getMsgBackupSize((testClientUid1)));

		Thread.sleep(300);
		objectToTest.run();
		objectToTest.run();
		Mockito.verify(clientEventServiceMock, times(1)).notifyClientRemoved(Mockito.any());
		Mockito.verify(msgSenderMock, times(2)).sendMessageWOSeqNrTo(Mockito.any(), Mockito.any());
		assertEquals(2, msgBackupAndACKProcessor.getMsgBackupSize((testClientUid1)));
	}

	@Test
	public void testCorrectAckHandlingOfMsgBackupAndACKProcessor() throws Exception {
		SequenceDocProcessor docProc = Mockito.mock(SequenceDocProcessor.class);
		Mockito.when(docProc.getDocUID()).thenReturn(testDocUid1);
		MsgBackupAndACKProcessor objectToTest = new MsgBackupAndACKProcessor(testDocUid1);
		objectToTest.clientAdded(new ClientEventData(testDocUid1, testClientUid1));
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		Map<RT2CliendUidType, Integer> backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 5);

		// Syntax 1
		RT2Message ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		JSONObject ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, new JSONArray("[1,2,3,4,6]"));
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 0);

		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, new JSONArray("[1,2,3,4,6]"));
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 0);

		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, new JSONArray("[4,6]"));
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 3);

		// Syntax 2
		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "1-6");
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 0);

		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "1-3");
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 2);

		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "1-3");
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 2);

		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "10-1000");
		ackMessage.setBody(ackBody);
		objectToTest.removeBackupMessagesForACKs(ackMessage);
		backUpSizes = objectToTest.getClientStatesMsgBackupSize();
		assertTrue(backUpSizes.size() == 1);
		assertTrue(backUpSizes.containsKey(testClientUid1));
		assertTrue(backUpSizes.get(testClientUid1) == 5);

		// Bad protocol
		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "1-3,4-6");
		ackMessage.setBody(ackBody);
		try {
			objectToTest.removeBackupMessagesForACKs(ackMessage);
			fail("Ack message must trigger a RT2ProtocolException!");
		} catch (RT2ProtocolException e) {
			// expected
		}

		resetMessagesForClient(objectToTest, testClientUid1);
		addMessagesToMsgBackAndACKProcessor(objectToTest);
		ackMessage = RT2MessageFactory.newMessage(RT2MessageType.ACK_SIMPLE, testClientUid1, testDocUid1);
		ackBody = new JSONObject();
		ackBody.put(RT2Protocol.BODYPART_ACKS, "[1,2,3],4-6");
		ackMessage.setBody(ackBody);
		try {
			objectToTest.removeBackupMessagesForACKs(ackMessage);
			fail("Ack message must trigger a RT2ProtocolException!");
		} catch (RT2ProtocolException e) {
			// expected
		}
	}

	private void resetMessagesForClient(MsgBackupAndACKProcessor msgBackupAckProc, RT2CliendUidType clientUid) {
		ClientEventData clientEventData = new ClientEventData(testDocUid1, clientUid);
		msgBackupAckProc.clientRemoved(clientEventData);
		msgBackupAckProc.clientAdded(clientEventData);
	}

	private void addMessagesToMsgBackAndACKProcessor(MsgBackupAndACKProcessor msgBackupAckProc) {
		RT2Message rt2Message1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message1.setSeqNumber(new RT2SeqNumberType(1));
		msgBackupAckProc.backupMessage(rt2Message1);
		RT2Message rt2Message2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message2.setSeqNumber(new RT2SeqNumberType(2));
		msgBackupAckProc.backupMessage(rt2Message2);
		RT2Message rt2Message3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message3.setSeqNumber(new RT2SeqNumberType(3));
		msgBackupAckProc.backupMessage(rt2Message3);
		RT2Message rt2Message4 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message4.setSeqNumber(new RT2SeqNumberType(4));
		msgBackupAckProc.backupMessage(rt2Message4);
		RT2Message rt2Message6 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, testClientUid1,
				testDocUid1);
		rt2Message6.setSeqNumber(new RT2SeqNumberType(6));
		msgBackupAckProc.backupMessage(rt2Message6);
	}

	private void standardInit(MsgBackupAndAckProcessorService objectToTest, boolean addLastStoredMsgsForResend)
			throws RT2InvalidDocumentIdentifierException {
		unitTestBundle.injectDependencies(objectToTest);
		objectToTest.docCreated(testDocUid1);
		msgBackupAndACKProcessor = objectToTest.getMsgBackupAndACKProcessor(testDocUid1, true).get();
		objectToTest.clientAdded(new ClientEventData(testDocUid1, testClientUid1));
		if (addLastStoredMsgsForResend) {
			RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_OPEN_DOC, testClientUid1, testDocUid1);
			msg.setSeqNumber(new RT2SeqNumberType(1));
			msgBackupAndACKProcessor.backupMessage(msg);
			assertTrue(msgBackupAndACKProcessor.getMsgBackupSize(testClientUid1) == 1);
		}
	}

}
