/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageType;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class UnavailableTest extends BaseTest {

    @RegisterExtension
	public UnavailableMetadataRule unavailableMetadataRule = new UnavailableMetadataRule();

	@Override
	protected void initServices() {
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
		this.msgBackupAndAckProcessorService = unitTestBundle.getService(MsgBackupAndAckProcessorService.class);
		this.clientSequenceQueueDisposerService = unitTestBundle.getService(ClientSequenceQueueDisposerService.class);

		this.jmsMessageSender = unitTestBundle.getService(RT2JmsMessageSender.class);
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message request = TestRT2MessageCreator.createUnavailRequestMsg(clientId1, docUid1, unavailableMetadataRule.getUnavailTime());
		return request;
	}

	@Test
	@UnavailableTestMetadata(unavailTime=42l)
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSwitchUnavaibleWithOneClient() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		assertFalse(this.msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
		Mockito.verify(jmsMessageSender, never()).sendToClientResponseTopic(Mockito.any());
	}

	@Test
	@UnavailableTestMetadata(unavailTime=42l)
	@UnittestMetadata(countClients=2, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSwitchUnavaibleWithTwoClients() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		assertFalse(msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
		Mockito.verify(jmsMessageSender).sendToClientResponseTopic(rt2MsgCaptor.capture());
		RT2Message updateClientBroadcast = TestRT2MessageCreator.createUpdateClientsBroadcast(false, RT2MessageType.RESPONSE_UNAVAILABILITY, clientId2, docUid1, 1, null);
		RT2MessageValidator.validate(updateClientBroadcast, rt2MsgCaptor.getAllValues().get(0), "msg_id_header");
	}

	@Test
	@UnavailableTestMetadata(unavailTime=42l)
	@UnittestMetadata(countClients=3, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSwitchUnavaibleWithThreeClients() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		assertFalse(msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		ArgumentCaptor<BroadcastMessage> broadcastMsgCaptor = ArgumentCaptor.forClass(BroadcastMessage.class);
		Mockito.verify(jmsMessageSender).sendBroadcastMessage(broadcastMsgCaptor.capture());
		Mockito.verify(jmsMessageSender, never()).sendToClientResponseTopic(Mockito.any());
		BroadcastMessage broadcastMessage = broadcastMsgCaptor.getAllValues().get(0);
		assertEquals(MessageType.BROADCAST_UPDATE_CLIENTS, broadcastMessage.getMsgType());
		assertEquals(docUid1.getValue(), broadcastMessage.getDocUid().getValue());
		assertEquals(2, broadcastMessage.getReceiversCount());
		for (BroadcastMessageReceiver r : broadcastMessage.getReceiversList()) {
			assertTrue((r.getSeqNr().getValue() == 1) || (r.getSeqNr().getValue() == 2));
			assertTrue(r.getReceiver().getValue().equals(clientId2.getValue()) ||
					          r.getReceiver().getValue().equals(clientId3.getValue()));
		}
	}

	@Test
	@UnavailableTestMetadata(unavailTime=0l)
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSwitchAvaibleWithOneClient() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		assertTrue(msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
		Mockito.verify(jmsMessageSender, never()).sendToClientResponseTopic(Mockito.any());
	}
}
