/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.ws;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2SessionCountValidator;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.common.session.FakeSession;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.session.Session;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class SessionCountValidatorTest {

	private Integer userId = 42;
	private String sessionId1 = "testSessionId1";
	private RT2SessionIdType rt2SessionId1 = new RT2SessionIdType(sessionId1);
	private RT2ChannelId channelId1 = new RT2ChannelId("channelId1");
	private RT2ChannelId channelId2 = new RT2ChannelId("channelId2");
	private RT2ChannelId channelId3 = new RT2ChannelId("channelId3");
	private RT2ChannelId channelId4 = new RT2ChannelId("channelId4");

	private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

	private SessionService sessionSrvMock;

	private RT2SessionCountValidator objectToTest = new RT2SessionCountValidator();

	@BeforeEach
	public void init() throws OXException {
		SessionCountValidatorTestInformation unittestInformation = new SessionCountValidatorTestInformation();
		unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
		unitTestBundle.start();

		this.sessionSrvMock = unitTestBundle.getMock(SessionService.class);

		unitTestBundle.injectDependencies(objectToTest);
		unitTestBundle.injectValues(objectToTest);
	}

	@Test
	public void testAddSessionSucc() throws OXException, RT2TypedException {
		Mockito.when(sessionSrvMock.getSession4Id(sessionId1)).thenReturn(new FakeSession(userId, Mockito.mock(Session.class)));
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
	}

	@Test
	public void testAddSessionTooManyConnections() throws OXException, RT2TypedException {
		Mockito.when(sessionSrvMock.getSession4Id(sessionId1)).thenReturn(new FakeSession(userId, Mockito.mock(Session.class)));
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId2);
		assertEquals(2, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId3);
		assertEquals(3, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		try {
			objectToTest.addSession(rt2SessionId1, channelId4);
			fail();
		} catch (RT2TypedException ex) {
			assertEquals(ErrorCode.TOO_MANY_CONNECTIONS_ERROR, ex.getError());
		}
		assertEquals(3, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
	}

	@Test
	public void testWithRemove() throws OXException, RT2TypedException {
		Mockito.when(sessionSrvMock.getSession4Id(sessionId1)).thenReturn(new FakeSession(userId, Mockito.mock(Session.class)));
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId2);
		assertEquals(2, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId3);
		assertEquals(3, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		try {
			objectToTest.addSession(rt2SessionId1, channelId4);
			fail();
		} catch (RT2TypedException ex) {
			assertEquals(ErrorCode.TOO_MANY_CONNECTIONS_ERROR, ex.getError());
		}
		assertEquals(3, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.removeSession(channelId1);
		assertEquals(2, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.removeSession(channelId4);
		assertEquals(2, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId4);
		assertEquals(3, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
	}

	@Test
	public void testWithInvalidSession() throws OXException, RT2TypedException {
		try {
			objectToTest.addSession(rt2SessionId1, channelId1);
		} catch (RT2TypedException ex) {
			assertEquals(ErrorCode.GENERAL_SESSION_INVALID_ERROR, ex.getError());
		}
		assertFalse(objectToTest.getCountSessionsOfUsers().containsKey(userId));
	}

	@Test
	public void testRemoveCompleteAndAdd() throws OXException, RT2TypedException {
		Mockito.when(sessionSrvMock.getSession4Id(sessionId1)).thenReturn(new FakeSession(userId, Mockito.mock(Session.class)));
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.addSession(rt2SessionId1, channelId2);
		assertEquals(2, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.removeSession(channelId2);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
		objectToTest.removeSession(channelId1);
		assertFalse(objectToTest.getCountSessionsOfUsers().containsKey(userId));
		objectToTest.addSession(rt2SessionId1, channelId1);
		assertEquals(1, objectToTest.getCountSessionsOfUsers().get(userId).intValue());
	}
}
