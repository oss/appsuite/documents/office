/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.impl.GCLostClientsTask;

public class GCLostClientsTaskTest {

	//-------------------------------------------------------------------------
	final static long ACCEPTED_TIME_DIFF = 1000;

	//-------------------------------------------------------------------------
	@Test
	public void test() {
		final String sTaskID = UUID.randomUUID().toString();
		final String sNodeUUID = UUID.randomUUID().toString();
		final String sCleanupUUID = UUID.randomUUID().toString();

		final GCLostClientsTask aLostClientTask = new GCLostClientsTask(sTaskID, sNodeUUID, sCleanupUUID);
		assertNotNull(aLostClientTask);

		assertFalse(aLostClientTask.successful());
		assertEquals(sTaskID, aLostClientTask.getTaskID());
		assertEquals(sNodeUUID, aLostClientTask.getMemberUUID());
		assertEquals(sCleanupUUID, aLostClientTask.getMemberUUIDToCleanup());

		final String sToString = aLostClientTask.toString();
		assertNotNull(sToString);
		assertTrue(sToString.indexOf(sTaskID) >= 0);
		assertTrue(sToString.indexOf(sNodeUUID) >= 0);
		assertTrue(sToString.indexOf(sCleanupUUID) >= 0);
	}

}
