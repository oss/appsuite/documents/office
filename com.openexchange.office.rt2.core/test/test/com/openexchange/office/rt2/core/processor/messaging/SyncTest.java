/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.mockito.Mockito.never;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2Message;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class SyncTest extends BaseTest {

	@Override
	protected void initServices() {
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);

		this.jmsMessageSender = unitTestBundle.getService(RT2JmsMessageSender.class);
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message request = TestRT2MessageCreator.createSyncRequestMsg(clientId1, docUid1);
		return request;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void test() throws Exception {

		IDBasedFileAccessFactory idBasedFileAccessFactory = unitTestBundle.getMock(IDBasedFileAccessFactory.class);
		IDBasedFileAccess idBasedFileAccess = Mockito.mock(IDBasedFileAccess.class);
		Mockito.when(idBasedFileAccessFactory.createAccess(Mockito.any())).thenReturn(idBasedFileAccess);

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender, never()).sendBroadcastMessage(Mockito.any());
		Mockito.verify(jmsMessageSender).sendToClientResponseTopic(rt2MsgCaptor.capture());
		RT2Message syncResponse = TestRT2MessageCreator.createSyncResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(syncResponse, rt2MsgCaptor.getAllValues().get(0), "msg_id_header");
	}
}
