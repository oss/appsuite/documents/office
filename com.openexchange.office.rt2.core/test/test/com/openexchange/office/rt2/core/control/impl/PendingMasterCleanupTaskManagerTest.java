/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.Validate;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.impl.ITaskTimeoutListener;
import com.openexchange.office.rt2.core.control.impl.MasterCleanupTask;
import com.openexchange.office.rt2.core.control.impl.PendingMasterCleanupTaskManager;

public class PendingMasterCleanupTaskManagerTest {

	//-------------------------------------------------------------------------
	public static class TestTaskTimeoutListener implements ITaskTimeoutListener<MasterCleanupTask>
	{
		@Override
		public void timeoutReachedForTask(MasterCleanupTask aTask) throws Exception {
			fail("Impossible case - timeout listener should never be called!");
		}
	}

	//-------------------------------------------------------------------------
	private <T> Set<T> asSet(final List<T> aList)
	{
		Validate.notNull(aList);

		Set<T> aSet = new HashSet<>();
		for (final T t : aList) {
            aSet.add(t);
        }

		return aSet;
	}

	//-------------------------------------------------------------------------
	@Test
	public void test() throws Exception {
		final ITaskTimeoutListener<MasterCleanupTask> aTaskListener = new TestTaskTimeoutListener();
		final PendingMasterCleanupTaskManager aPendingMasterCleanupTaskManager = new PendingMasterCleanupTaskManager(aTaskListener);

		assertNotNull(aTaskListener);
		assertNotNull(aPendingMasterCleanupTaskManager);

		final String sTaskID = UUID.randomUUID().toString();
		assertFalse(aPendingMasterCleanupTaskManager.containsTask(sTaskID));
		assertNull(aPendingMasterCleanupTaskManager.getTask(sTaskID));
		assertFalse(aPendingMasterCleanupTaskManager.hasPendingTasks());
		assertFalse(aPendingMasterCleanupTaskManager.isStarted());

		aPendingMasterCleanupTaskManager.stop();
		assertFalse(aPendingMasterCleanupTaskManager.isStarted());
		assertFalse(aPendingMasterCleanupTaskManager.hasPendingTasks());

		aPendingMasterCleanupTaskManager.start();
		assertTrue(aPendingMasterCleanupTaskManager.isStarted());

		final String sNodeUUID = UUID.randomUUID().toString();
		final String scleanupUUID = UUID.randomUUID().toString();
		final List<String> aUUIDList = Arrays.asList( UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
		final Set<String> aHealthyMemberSet = asSet(aUUIDList);
		final MasterCleanupTask aMasterCleanupTask = new MasterCleanupTask(sTaskID, sNodeUUID, scleanupUUID, aHealthyMemberSet);

		aPendingMasterCleanupTaskManager.storeTask(aMasterCleanupTask);
		assertTrue(aPendingMasterCleanupTaskManager.containsTask(sTaskID));
		assertNotNull(aPendingMasterCleanupTaskManager.getTask(sTaskID));
		assertEquals(aMasterCleanupTask, aPendingMasterCleanupTaskManager.getTask(sTaskID));
		assertTrue(aPendingMasterCleanupTaskManager.hasPendingTasks());

		final String sTaskIDNotExisting = UUID.randomUUID().toString();
		try
		{
			aPendingMasterCleanupTaskManager.setMemberToCompleted(sTaskIDNotExisting, aUUIDList.get(0));
			fail("impossible case - calling setMemberToCompleted() with non-existing task id must throw exception");
		}
		catch (NoSuchElementException e)
		{
			// we expect this - nothing to do
		}
		catch (Exception e)
		{
			fail("impossible case - calling setMemberToCompleted() with non-existing task id must throw only a NoSuchElementException");
		}

		// make sure that the timer task code is processed at least once
		Thread.sleep(40000);

		boolean bCompleted = false;
		for (final String s : aUUIDList) {
            bCompleted = aPendingMasterCleanupTaskManager.setMemberToCompleted(sTaskID, s);
        }

		assertTrue(bCompleted);
		assertFalse(aPendingMasterCleanupTaskManager.hasPendingTasks());

		aPendingMasterCleanupTaskManager.stop();
    }

}
