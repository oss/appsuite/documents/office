/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndACKProcessor;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.user.LoadState;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class CloseTest extends BaseTest {

	private TextDocProcessor textDocProcessor;

	@Override
	protected void initServices() {
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
		this.clientSequenceQueueDisposerService = unitTestBundle.getService(ClientSequenceQueueDisposerService.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message closeRequest = TestRT2MessageCreator.createCloseRequestMsg(clientId1, docUid1, 1, new RT2SessionIdType("testSessionId"));
		return closeRequest;
	}

	@Override
	protected EditableDocProcessor initDocProcessor(LoadState loadState) throws Exception {
		EditableDocProcessor res = super.initDocProcessor(loadState);

		textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		textDocProcessor.getDocumentStatus().setModified(true);

		return res;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSingleClient() throws Exception {
		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		assertEquals(1, docProcMngr.size());
		textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		MsgBackupAndACKProcessor msgBackupAndACKProcessor = unitTestBundle.getService(MsgBackupAndAckProcessorService.class).getMsgBackupAndACKProcessor(docUid1, true).get();
		assertTrue(msgBackupAndACKProcessor.isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertFalse(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		RT2Message closeDocResponseIs = validateJmsMessageSenderForSingleMessage().get(0);

		RT2Message closeDocResponse = TestRT2MessageCreator.createCloseResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(closeDocResponse, closeDocResponseIs, "msg_id_header");
	}

	@Test
	@UnittestMetadata(countClients=2, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testTwoClients() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		assertEquals(1, docProcMngr.size());

		MsgBackupAndACKProcessor msgBackupAndACKProcessor = unitTestBundle.getService(MsgBackupAndAckProcessorService.class).getMsgBackupAndACKProcessor(docUid1, true).get();
		assertTrue(msgBackupAndACKProcessor.isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertFalse(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender, times(3)).sendToClientResponseTopic(rt2MsgCaptor.capture());
		List<RT2Message> messages = rt2MsgCaptor.getAllValues();
		assertEquals(3, messages.size());
		Map<RT2MessageType, RT2Message> resMsgs = new HashMap<>();
		for (RT2Message msg : messages) {
			resMsgs.put(msg.getType(), msg);
		}
		RT2Message closeDocResponse = TestRT2MessageCreator.createCloseResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(closeDocResponse, resMsgs.get(RT2MessageType.RESPONSE_CLOSE_DOC), "msg_id_header");
		RT2Message updateBroadcast = TestRT2MessageCreator.createUpdateBroadcast(false, RT2MessageType.RESPONSE_CLOSE_DOC, clientId2, docUid1, 1, null);
		RT2MessageValidator.validate(updateBroadcast, resMsgs.get(RT2MessageType.BROADCAST_UPDATE), "msg_id_header");
		RT2Message updateClientsBroadcast = TestRT2MessageCreator.createUpdateClientsBroadcast(false, RT2MessageType.RESPONSE_CLOSE_DOC, clientId2, docUid1, 2, null);
		RT2MessageValidator.validate(updateClientsBroadcast, resMsgs.get(RT2MessageType.BROADCAST_UPDATE_CLIENTS), "msg_id_header");
	}

	@Test
	@UnittestMetadata(countClients=3, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testThreeClients() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		assertEquals(1, docProcMngr.size());

		MsgBackupAndACKProcessor msgBackupAndACKProcessor = unitTestBundle.getService(MsgBackupAndAckProcessorService.class).getMsgBackupAndACKProcessor(docUid1, true).get();
		assertTrue(msgBackupAndACKProcessor.isClientOnline(clientId1));
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertFalse(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender, times(1)).sendToClientResponseTopic(rt2MsgCaptor.capture());
		List<RT2Message> messages = rt2MsgCaptor.getAllValues();
		assertEquals(1, messages.size());
		RT2Message closeDocResponse = TestRT2MessageCreator.createCloseResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(closeDocResponse, messages.get(0), "msg_id_header");

		ArgumentCaptor<BroadcastMessage> broadcastMsgCaptor = ArgumentCaptor.forClass(BroadcastMessage.class);
		Mockito.verify(jmsMessageSender, times(2)).sendBroadcastMessage(broadcastMsgCaptor.capture());
		List<BroadcastMessage> broadcastMessages = broadcastMsgCaptor.getAllValues();
		for (BroadcastMessage broadcastMessage : broadcastMessages) {
			switch (broadcastMessage.getMsgType()) {
				case BROADCAST_UPDATE:
				case BROADCAST_UPDATE_CLIENTS:
					assertEquals(docUid1.getValue(), broadcastMessage.getDocUid().getValue());
					assertEquals(2, broadcastMessage.getReceiversCount());
					for (BroadcastMessageReceiver r : broadcastMessage.getReceiversList()) {
						assertTrue((r.getSeqNr().getValue() == 1) || (r.getSeqNr().getValue() == 2));
						assertTrue(r.getReceiver().getValue().equals(clientId2.getValue()) ||
								          r.getReceiver().getValue().equals(clientId3.getValue()));
					}
					break;
				default:
					fail("Illegal message type: " + broadcastMessage.getMsgType());
			}
		}
	}
}
