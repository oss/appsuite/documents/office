/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.websocketToJms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import test.com.openexchange.office.rt2.core.util.MessagesValidator;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;

public class JoinTestMessageValidator extends MessagesValidator {

	public JoinTestMessageValidator(RT2JmsMessageSender jmsMessageSender, RT2NodeUuidType nodeUid, RT2Message sendMsg) {
		super(jmsMessageSender, nodeUid, sendMsg);
	}

	@Override
	public void validateMessages() {
		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		try {
			Mockito.verify(jmsMessageSender, Mockito.times(1)).sendToDocumentQueue(rt2MsgCaptor.capture(), Mockito.any());
		} catch (RT2TypedException e) {
			fail(e.getMessage());
		}
		List<RT2Message> rt2Msgs = rt2MsgCaptor.getAllValues();
		assertEquals(1, rt2Msgs.size());
		RT2Message joinMsgToJms = null;
		for (RT2Message msg : rt2Msgs) {
			switch (msg.getType()) {
				case REQUEST_JOIN:
					joinMsgToJms = msg;
					break;
				default: fail();
			}
		}
		RT2Message sollJoinRequest =
				TestRT2MessageCreator.createJoinMsg(sendMsg.getClientUID(), sendMsg.getDocUID(), nodeUid, sendMsg.getFolderID(), sendMsg.getFileID(), sendMsg.getSessionID());
		RT2MessageValidator.validate(sollJoinRequest, joinMsgToJms, "msg_id_header");
	}
}
