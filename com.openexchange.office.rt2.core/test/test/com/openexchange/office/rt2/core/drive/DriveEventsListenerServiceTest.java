/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.invocation.Invocation;
import org.osgi.service.event.Event;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageEventConstants;
import com.openexchange.office.rt2.core.drive.DriveEventsListenerService;
import com.openexchange.office.rt2.core.jms.RT2AdminJmsConsumer;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.timer.ScheduledTimerTask;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

@ExtendWith(InUnitTestRule.class)
public class DriveEventsListenerServiceTest {

    private DriveEventsListenerServiceTestInformation unittestInformation;

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    @BeforeEach
    public void init() throws OXException {
        unittestInformation = new DriveEventsListenerServiceTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();
    }

    @Test
    public void testDriveEventsListenerService() throws Exception {
        DriveEventsListenerService driveEventsListenerService = prepareDriveEventsListenerService();

        long num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);

        String topic = FileStorageEventConstants.DELETE_TOPIC;
        Map<String, Object> props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(1, num);

        topic = FileStorageEventConstants.CREATE_TOPIC;
        props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(2, num);

        topic = FileStorageEventConstants.CREATE_FOLDER_TOPIC;
        props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(2, num);

        TimerServiceMock timerServiceMock = unittestInformation.getTimerServiceMock();
        assertNotNull(timerServiceMock);
        List<ScheduledTimerTask> scheduledTimerTasks = timerServiceMock.getTimerTasks();
        assertEquals(2, scheduledTimerTasks.size());

        driveEventsListenerService.destroy();

        assertEquals(0, scheduledTimerTasks.size());

        topic = FileStorageEventConstants.DELETE_TOPIC;
        props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(2, num);
    }

    @Test
    public void testDriveEventsListenerServiceEventCount() throws Exception {
        DriveEventsListenerService driveEventsListenerService = prepareDriveEventsListenerService();

        long num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);

        String topic = FileStorageEventConstants.DELETE_TOPIC;
        Map<String, Object> props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(1, num);

        topic = FileStorageEventConstants.CREATE_TOPIC;
        props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(2, num);

        TimerServiceMock timerServiceMock = unittestInformation.getTimerServiceMock();
        assertNotNull(timerServiceMock);
        List<ScheduledTimerTask> scheduledTimerTasks = timerServiceMock.getTimerTasks();
        assertEquals(2, scheduledTimerTasks.size());

        ScheduledTimerTask resetTask = scheduledTimerTasks.get(1);
        assertNotNull(resetTask);
        assertTrue(resetTask instanceof ScheduledTimerTaskMock);

        ScheduledTimerTaskMock taskMock = (ScheduledTimerTaskMock)resetTask;
        taskMock.triggerTask();

        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);
    }

    @Test
    public void testDriveEventsListenerServiceProcessDeleteEvent() throws Exception {
        DriveEventsListenerService driveEventsListenerService = prepareDriveEventsListenerService();

        long num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);

        String topic = FileStorageEventConstants.DELETE_TOPIC;
        Map<String, Object> props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(1, num);

        TimerServiceMock timerServiceMock = unittestInformation.getTimerServiceMock();
        assertNotNull(timerServiceMock);

        List<ScheduledTimerTask> scheduledTimerTasks = timerServiceMock.getTimerTasks();
        assertEquals(2, scheduledTimerTasks.size());
        ScheduledTimerTask processingTimerTask = scheduledTimerTasks.get(0);
        assertNotNull(processingTimerTask);
        assertTrue(processingTimerTask instanceof ScheduledTimerTaskMock);

        ScheduledTimerTaskMock taskMock = (ScheduledTimerTaskMock)processingTimerTask;
        taskMock.triggerTask();

        RT2AdminJmsConsumer jmsConsumerMock = unittestInformation.getRT2AdminJmsConsumerMock();
        verify(jmsConsumerMock, times(1)).send(Mockito.isA(RT2Message.class));
        Collection<Invocation> invocations = Mockito.mockingDetails(jmsConsumerMock).getInvocations();
        assertEquals(1, invocations.size());
        Invocation invocation = invocations.iterator().next();
        Object arg0 = invocation.getArgument(0);
        assertTrue(arg0 instanceof RT2Message);
        verifyEventIsPartOfRT2Message((RT2Message)arg0, event, false);
    }

    @Test
    public void testDriveEventsListenerServiceProcessCreateEvent() throws Exception {
        DriveEventsListenerService driveEventsListenerService = prepareDriveEventsListenerService();

        long num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);

        String topic = FileStorageEventConstants.CREATE_TOPIC;
        Map<String, Object> props = new HashMap<String, Object>();
        props.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event = new Event(topic, props);
        driveEventsListenerService.handleEvent(event);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(1, num);

        TimerServiceMock timerServiceMock = unittestInformation.getTimerServiceMock();
        assertNotNull(timerServiceMock);

        List<ScheduledTimerTask> scheduledTimerTasks = timerServiceMock.getTimerTasks();
        assertEquals(2, scheduledTimerTasks.size());
        ScheduledTimerTask processingTimerTask = scheduledTimerTasks.get(0);
        assertNotNull(processingTimerTask);
        assertTrue(processingTimerTask instanceof ScheduledTimerTaskMock);

        ScheduledTimerTaskMock taskMock = (ScheduledTimerTaskMock)processingTimerTask;
        taskMock.triggerTask();

        // create event is ignored if it's not part of a move (delete + create)
        RT2AdminJmsConsumer jmsConsumerMock = unittestInformation.getRT2AdminJmsConsumerMock();
        verify(jmsConsumerMock, times(1)).send(Mockito.isA(RT2Message.class));
        Collection<Invocation> invocations = Mockito.mockingDetails(jmsConsumerMock).getInvocations();
        assertEquals(1, invocations.size());
        Invocation invocation = invocations.iterator().next();
        Object arg0 = invocation.getArgument(0);
        assertTrue(arg0 instanceof RT2Message);
        verifyEventIsPartOfRT2Message((RT2Message)arg0, event, false);
    }

    @Test
    public void testDriveEventsListenerServiceProcessDeleteAndCreateEvent() throws Exception {
        DriveEventsListenerService driveEventsListenerService = prepareDriveEventsListenerService();

        long num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(0, num);

        String topic = FileStorageEventConstants.DELETE_TOPIC;
        Map<String, Object> props1 = new HashMap<String, Object>();
        props1.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props1.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props1.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event1 = new Event(topic, props1);
        driveEventsListenerService.handleEvent(event1);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(1, num);

        topic = FileStorageEventConstants.CREATE_TOPIC;
        Map<String, Object> props2 = new HashMap<String, Object>();
        props2.put(FileStorageEventConstants.OBJECT_ID, "1000/1002");
        props2.put(FileStorageEventConstants.FOLDER_ID, "1000");
        props2.put(FileStorageEventConstants.SESSION, new SessionMock("1234567890"));

        Event event2 = new Event(topic, props2);
        driveEventsListenerService.handleEvent(event2);
        num = driveEventsListenerService.getDriveEventsReceived();
        assertEquals(2, num);

        TimerServiceMock timerServiceMock = unittestInformation.getTimerServiceMock();
        assertNotNull(timerServiceMock);

        List<ScheduledTimerTask> scheduledTimerTasks = timerServiceMock.getTimerTasks();
        assertEquals(2, scheduledTimerTasks.size());
        ScheduledTimerTask processingTimerTask = scheduledTimerTasks.get(0);
        assertNotNull(processingTimerTask);
        assertTrue(processingTimerTask instanceof ScheduledTimerTaskMock);

        ScheduledTimerTaskMock taskMock = (ScheduledTimerTaskMock)processingTimerTask;
        taskMock.triggerTask();

        RT2AdminJmsConsumer jmsConsumerMock = unittestInformation.getRT2AdminJmsConsumerMock();
        verify(jmsConsumerMock, times(1)).send(Mockito.isA(RT2Message.class));
        Collection<Invocation> invocations = Mockito.mockingDetails(jmsConsumerMock).getInvocations();
        assertEquals(1, invocations.size());
        Invocation invocation = invocations.iterator().next();
        Object arg0 = invocation.getArgument(0);
        assertTrue(arg0 instanceof RT2Message);
        verifyEventIsPartOfRT2Message((RT2Message)arg0, event2, true);
    }

    private DriveEventsListenerService prepareDriveEventsListenerService() throws Exception {
        DriveEventsListenerService driveEventsListenerService = new DriveEventsListenerService();
        unitTestBundle.injectDependencies(driveEventsListenerService);
        driveEventsListenerService.afterPropertiesSet();
        return driveEventsListenerService;
    }

    private void verifyEventIsPartOfRT2Message(RT2Message msg, Event baseEvent, boolean isMove) {
        assertNotNull(msg);
        assertEquals(RT2MessageType.ADMIN_TASK_FILES_MOVED_DELETED, msg.getType());

        String topic = baseEvent.getTopic();
        if ((FileStorageEventConstants.DELETE_TOPIC.equals(topic)) ||
            (FileStorageEventConstants.CREATE_TOPIC.equals(topic))) {
            JSONObject json = msg.getBody();
            assertNotNull(json);
            JSONArray moved = json.optJSONArray(RT2Protocol.BODYPARTS_MOVED);
            JSONArray deleted = json.optJSONArray(RT2Protocol.BODYPARTS_DELETED);
            assertNotNull(moved);
            assertNotNull(deleted);
            if (topic.equals(FileStorageEventConstants.DELETE_TOPIC)) {
                assertTrue(deleted.length() == 1);
            } else if (topic.equals(FileStorageEventConstants.CREATE_TOPIC) && isMove) {
                assertTrue(moved.length() == 1);
            }
        } else {
            fail("Unexpected event!");
        }
    }

}
