/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.doc.EditableClientsStatus;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.protocol.RT2Message;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class SyncStableTest extends BaseTest {

	@Override
	protected void initServices() {
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);

		this.jmsMessageSender = unitTestBundle.getService(RT2JmsMessageSender.class);
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message request;
		try {
			request = TestRT2MessageCreator.createSyncStableRequestMsg(clientId1, docUid1, 1, 42);
			return request;
		} catch (JSONException e) {
			fail(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	protected TextMessage createAmqMessage(RT2Message rt2Msg) throws JMSException {
		TextMessage txtMsg = super.createAmqMessage(rt2Msg);
		txtMsg.setText("{\"osn\": 42}");
		return txtMsg;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void test() throws Exception {

		defaultTestBeforeProcessorMessaging();

		waitForLastProcessedMessage();

		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		final EditableClientsStatus clientsStatus = (EditableClientsStatus) textDocProcessor.getClientsStatus();
		assertEquals(42, clientsStatus.getClientOSN(clientId1));

		List<RT2Message> msgs = validateJmsMessageSenderForSingleMessage();
		RT2Message syncStableResponse = TestRT2MessageCreator.createSyncStableResponseMsg(clientId1, docUid1, 1);
		RT2MessageValidator.validate(syncStableResponse, msgs.get(0), "msg_id_header");
	}
}
