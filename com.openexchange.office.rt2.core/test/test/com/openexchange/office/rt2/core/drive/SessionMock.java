/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.session.Origin;
import com.openexchange.session.Session;

public class SessionMock implements Session {
    String sessionId;
    int contextId = 1;
    int userId = 1;

    public SessionMock(String sessionId) {
        if (StringUtils.isEmpty(sessionId)) {
            this.sessionId = UUID.randomUUID().toString();
        } else {
            this.sessionId = sessionId;
        }
    }

    public SessionMock(String sessionId, int contextId, int userId) {
        if (StringUtils.isEmpty(sessionId)) {
            this.sessionId = UUID.randomUUID().toString();
        } else {
            this.sessionId = sessionId;
        }
        this.contextId = contextId;
        this.userId = userId;
    }

    @Override
    public int getContextId() {
        return contextId;
    }

    @Override
    public String getLocalIp() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setLocalIp(String ip) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getLoginName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean containsParameter(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Object getParameter(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPassword() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getRandomToken() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSecret() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSessionID() {
        return sessionId;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public String getUserlogin() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getLogin() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setParameter(String name, Object value) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getAuthId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getHash() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setHash(String hash) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getClient() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setClient(String client) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isTransient() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Set<String> getParameterNames() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Origin getOrigin() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isStaySignedIn() {
        // TODO Auto-generated method stub
        return false;
    }

}
