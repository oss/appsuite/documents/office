/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.doc;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.ClientEventData;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class ClientEventServiceTest {

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    public static class ClientEventListener implements IClientEventListener {
        boolean throwExceptionOnClientAdded = false;
        boolean throwExceptionOnClientRemoved = false;
        boolean clientAddedCalled = false;
        boolean clientRemovedCalled = false;

        public ClientEventListener(boolean throwOnClientAdded, boolean throwOnClientRemoved) {
            throwExceptionOnClientAdded = throwOnClientAdded;
            throwExceptionOnClientRemoved = throwOnClientRemoved;
        }

        public void resetCalledFlags() {
            clientAddedCalled = false;
            clientRemovedCalled = false;
        }

        @Override
        public void clientAdded(ClientEventData clientEventData) {
            clientAddedCalled = true;
            if (throwExceptionOnClientAdded) {
                throw new RuntimeException();
            }
        }

        @Override
        public void clientRemoved(ClientEventData clientEventData) {
            clientRemovedCalled = true;
            if (throwExceptionOnClientRemoved) {
                throw new RuntimeException();
            }
        }
    }

    @BeforeEach
    public void init() throws OXException {
        ClientEventServiceTestInformation unittestInformation = new ClientEventServiceTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();
    }

    @Test
    public void testClientEventServiceAddRemoveListener() throws Exception {
        ClientEventService clientEventService = new ClientEventService();
        unitTestBundle.injectDependencies(clientEventService);

        ClientEventListener l1 = new ClientEventListener(false, false);
        ClientEventListener l2 = new ClientEventListener(true, true);

        clientEventService.addListener(l1);
        clientEventService.addListener(l2);

        RT2CliendUidType clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        RT2DocUidType docUid = new RT2DocUidType("d131dd02c5e6eec4");
        RT2Message rt2Msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientUid, docUid);

        try {
            clientEventService.notifyClientAdded(rt2Msg);
        } catch (Exception e) {
            fail("No exception should be thrown by notifyClientAdded");
        }

        assertTrue(l1.clientAddedCalled);
        assertTrue(l2.clientAddedCalled);

        clientEventService.removeListener(l2);
        l1.resetCalledFlags();
        l2.resetCalledFlags();

        try {
            clientEventService.notifyClientAdded(rt2Msg);
        } catch (Exception e) {
            fail("No exception should be thrown by notifyClientAdded");
        }

        assertTrue(l1.clientAddedCalled);
        assertFalse(l2.clientAddedCalled);

        clientEventService.removeListener(l1);
        l1.resetCalledFlags();
        l2.resetCalledFlags();

        try {
            clientEventService.notifyClientAdded(rt2Msg);
        } catch (Exception e) {
            fail("No exception should be thrown by notifyClientAdded");
        }

        assertFalse(l1.clientAddedCalled);
        assertFalse(l2.clientAddedCalled);
    }

    @Test
    public void testClientEventServiceHandlesExceptionOnClientAddedNotification() throws Exception {
        ClientEventService clientEventService = new ClientEventService();
        unitTestBundle.injectDependencies(clientEventService);

        ClientEventListener l1 = new ClientEventListener(false, false);
        ClientEventListener l2 = new ClientEventListener(true, false);
        ClientEventListener l3 = new ClientEventListener(false, true);
        ClientEventListener l4 = new ClientEventListener(true, true);

        clientEventService.addListener(l1);
        clientEventService.addListener(l2);
        clientEventService.addListener(l3);
        clientEventService.addListener(l4);

        RT2CliendUidType clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        RT2DocUidType docUid = new RT2DocUidType("d131dd02c5e6eec4");
        RT2Message rt2Msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientUid, docUid);

        try {
            clientEventService.notifyClientAdded(rt2Msg);
        } catch (Exception e) {
            fail("No exception should be thrown by notifyClientAdded");
        }

        assertTrue(l1.clientAddedCalled);
        assertTrue(l2.clientAddedCalled);
        assertTrue(l3.clientAddedCalled);
        assertTrue(l4.clientAddedCalled);
    }

    @Test
    public void testClientEventServiceHandlesExceptionOnClientRemovedNotification() throws Exception {
        ClientEventService clientEventService = new ClientEventService();
        unitTestBundle.injectDependencies(clientEventService);

        ClientEventListener l1 = new ClientEventListener(false, false);
        ClientEventListener l2 = new ClientEventListener(true, false);
        ClientEventListener l3 = new ClientEventListener(false, true);
        ClientEventListener l4 = new ClientEventListener(true, true);

        clientEventService.addListener(l1);
        clientEventService.addListener(l2);
        clientEventService.addListener(l3);
        clientEventService.addListener(l4);

        RT2CliendUidType clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        RT2DocUidType docUid = new RT2DocUidType("d131dd02c5e6eec4");
        ClientEventData clientEventData = new ClientEventData(docUid, clientUid);

        try {
            clientEventService.notifyClientRemoved(clientEventData);
        } catch (Exception e) {
            fail("No exception should be thrown by notifyClientAdded");
        }

        assertTrue(l1.clientRemovedCalled);
        assertTrue(l2.clientRemovedCalled);
        assertTrue(l3.clientRemovedCalled);
        assertTrue(l4.clientRemovedCalled);
    }
}
