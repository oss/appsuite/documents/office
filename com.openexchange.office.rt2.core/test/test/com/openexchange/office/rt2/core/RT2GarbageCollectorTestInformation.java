/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import java.util.Collection;
import java.util.HashSet;

import org.mockito.Mockito;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.openexchange.office.rt2.core.RT2GarbageCollector;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.cluster.ClusterService;

import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class RT2GarbageCollectorTestInformation extends RT2UnittestInformation {
	
	@Override
	protected Collection<Class<?>> getClassesToMock() {		
		Collection<Class<?>> res = new HashSet<>();
		res.addAll(testOsgiBundleContextAndUnitTestActivator.getMandantoryDependenciesOfClass(RT2GarbageCollector.class));
		res.add(CachingFacade.class);
		res.add(ClusterService.class);
		return res;
	}

	@Override
	protected void initMocks() {
		MetricRegistry metricRegistryMock = testOsgiBundleContextAndUnitTestActivator.getMock(MetricRegistry.class);
		Mockito.when(metricRegistryMock.counter(Mockito.anyString())).thenReturn(Mockito.mock(Counter.class));
	}
}
