/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.RT2GarbageCollector;
import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap.DistributedDocInfoStatus;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;
import test.com.openexchange.office.rt2.core.util.MethodWithParameterNotFoundException;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.rt2.core.util.TestMethodExecutor;

public class RT2GarbageCollectorTest {

	private ClusterLockService clusterLockServiceMock;

	private CachingFacade cachingFacade;

	private ClusterLock clusterLockMock;

	private DistributedDocInfoMap distributedDocInfoMap;

	private RT2GarbageCollector objectUnderTest;

	private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

	@BeforeEach
	public void init() throws Exception {
		RT2GarbageCollectorTestInformation unittestInformation = new RT2GarbageCollectorTestInformation();
		unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
		unitTestBundle.start();

		objectUnderTest = new RT2GarbageCollector(5);
		unitTestBundle.injectDependencies(objectUnderTest);
		objectUnderTest.afterPropertiesSet();

		this.clusterLockMock = Mockito.mock(ClusterLock.class);
		this.clusterLockServiceMock = unitTestBundle.getMock(ClusterLockService.class);
		this.cachingFacade = unitTestBundle.getMock(CachingFacade.class);
		this.distributedDocInfoMap = unitTestBundle.getMock(DistributedDocInfoMap.class);
	}

	@Test
	public void testWithGetDistributedObjectsNoAtomicLongToGcInvalidAtomicLongName() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);

		Map<String, DistributedAtomicLong> distributedObjects = new HashMap<>();
		DistributedAtomicLong atomicLong = Mockito.mock(DistributedAtomicLong.class);
		Mockito.when(atomicLong.getName()).thenReturn(RT2GarbageCollector.REFCOUNT_CLIENT_PREFIX + "1234567");
		distributedObjects.put(atomicLong.getName(), atomicLong);
		Mockito.when(cachingFacade.getDistributedAtomicLongsWithPrefix(RT2GarbageCollector.REFCOUNT_CLIENT_PREFIX)).thenReturn(distributedObjects);
		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(atomicLong, never()).destroy();
		Mockito.verify(clusterLockMock, never()).unlock();
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
	}


	@Test
	public void testWithGetDistributedObjectsNoAtomicLongToGc() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);
		RT2DocUidType docUid = new RT2DocUidType("1234567");

		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.RUNNING);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(new HashSet<>(Arrays.asList("abc")));
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(1);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock, never()).unlock();
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithGetDistributedObjectsAtomicLongToGcBecauseOfZeroValueOfAtomicLong() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);

		RT2DocUidType docUid = new RT2DocUidType("1234567");

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(new HashSet<>(Arrays.asList(docUid)));
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.RUNNING);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(0);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock).unlock();
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithGetDistributedObjectsAtomicLongToGcBecauseOfRefCountThreshholdValueOfAtomicLong() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);

		RT2DocUidType docUid = new RT2DocUidType("1234567");

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(new HashSet<>(Arrays.asList(docUid)));
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(1000000);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock).unlock();
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithGetDistributedObjectsAtomicLongToGcBecauseOfRefCountThreshholdValueOfAtomicLongWithSecondCall() throws InterruptedException, ClusterLockException, MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);

		RT2DocUidType docUid = new RT2DocUidType("1234567");

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(new HashSet<>(Arrays.asList(docUid)));
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(1000000);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
		Thread.sleep(6000);
		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
		Mockito.verify(clusterLockMock, times(3)).lock();
		Mockito.verify(clusterLockMock, times(3)).unlock();
	}

	//-----------------------------------------------------------------------------------------------------------------

	@Test
	public void testWithAtomicLongsToBeVerifiedAtomicHasValue0() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);

		RT2DocUidType docUid = new RT2DocUidType("12345678");

		objectUnderTest.registerDocRefCountForRemovalVerification(docUid);

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(new HashSet<>(Arrays.asList(docUid)));
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.RUNNING);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(0);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock).unlock();
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithAtomicLongsToBeVerifiedAtomicHasValue5() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);
		RT2DocUidType docUid = new RT2DocUidType("12345678");
		objectUnderTest.registerDocRefCountForRemovalVerification(docUid);

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(new HashSet<>(Arrays.asList(docUid)));
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.RUNNING);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(5);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock, never()).unlock();
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithAtomicLongsToBeVerifiedAtomicHasValueVeryHigh() throws MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);
		RT2DocUidType docUid = new RT2DocUidType("12345678");
		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(new HashSet<>(Arrays.asList("abc")));
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(1000000);
		objectUnderTest.registerDocRefCountForRemovalVerification(docUid);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock).unlock();
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
	}

	@Test
	public void testWithAtomicLongsToBeVerifiedAtomicHasValueVeryHighCalledTwice() throws InterruptedException, ClusterLockException, MethodWithParameterNotFoundException {
		Mockito.when(clusterLockServiceMock.getLock(Mockito.any())).thenReturn(clusterLockMock);
		RT2DocUidType docUid = new RT2DocUidType("12345678");
		objectUnderTest.registerDocRefCountForRemovalVerification(docUid);

		Mockito.when(distributedDocInfoMap.getStatus(docUid)).thenReturn(DistributedDocInfoStatus.DOCUMENT_ON_CRASHED_NODE);
		Mockito.when(distributedDocInfoMap.getClientsOfDocUid(docUid)).thenReturn(Collections.emptySet());
		Mockito.when(distributedDocInfoMap.getRefCount4Clients(docUid)).thenReturn(1000000);

		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		Mockito.verify(clusterLockMock).unlock();
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		assertEquals(1, objectUnderTest.getAtomicLongsToGc().size());
		Thread.sleep(6000);
		new TestMethodExecutor().execute(objectUnderTest, "checkAndGCAtomicLongAsClientRef");
		assertEquals(0, objectUnderTest.getAtomicLongsToGc().size());
		Mockito.verify(clusterLockMock, times(3)).lock();
		Mockito.verify(clusterLockMock, times(3)).unlock();
	}
}
