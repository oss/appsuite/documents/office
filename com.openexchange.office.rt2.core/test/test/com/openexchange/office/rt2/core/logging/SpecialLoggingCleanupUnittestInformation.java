/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.logging;

import java.util.Collection;
import java.util.HashSet;
import org.mockito.Mockito;
import com.openexchange.config.ConfigurationService;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.TimerService;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class SpecialLoggingCleanupUnittestInformation extends RT2UnittestInformation {

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res =  new HashSet<>();
        res.addAll(getMandantoryDependenciesOfDocProcessor());
        res.add(SpecialLogService.class);
        res.add(RT2DocStateNotifier.class);
        res.add(LocalDateTimeService.class);
        res.add(TimerService.class);
        res.add(RT2DocOnNodeMap.class);
        return res;
    }

    @Override
    protected void initMocks() {
        var cfgService = Mockito.mock(ConfigurationService.class);
        Mockito.when(cfgService.getBoolProperty(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(false);
    }

    @Override
    protected Collection<Object> getAdditionalServices() {
        Collection<Object> res = new HashSet<>();
        res.add(new RT2DocProxyStateHolderFactory());
        return res;
    }

}
