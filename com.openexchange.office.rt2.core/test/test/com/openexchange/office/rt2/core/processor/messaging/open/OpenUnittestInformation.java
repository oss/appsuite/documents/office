/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging.open;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.mockito.Mockito;

import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFileStoragePermission;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.group.GroupService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.document.DocFileServiceFactoryImpl;
import com.openexchange.office.document.OXDocumentFactoryImpl;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.document.api.BackupFileService;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.tools.EmptyDocumentCache;
import com.openexchange.office.htmldoc.GenericHtmlDocumentBuilder;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.imagemgr.impl.ResourceLocker;
import com.openexchange.office.imagemgr.impl.ResourceManagerFactory;
import com.openexchange.office.imagemgr.impl.ResourceManagers;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.AdvisoryLockHelperService;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessorEventService;
import com.openexchange.office.rt2.core.doc.DocumentEventHelper;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorFactory;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.core.doc.RT2MessageTypeDispatcher;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.metric.DocProcessorMetricService;
import com.openexchange.office.rt2.core.sequence.ClientLeftRemovedService;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.core.sequence.QueueProcessorDisposer;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.user.UserService;

import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;
import test.com.openexchange.office.rt2.core.util.TestConstants;

public abstract class OpenUnittestInformation extends RT2UnittestInformation {

	protected abstract String getFileName();

	protected abstract String getFilePath();

	@Override
	public ConfigurationService initConfigService() {
		ConfigurationService configurationService = super.initConfigService();
		Mockito.when(configurationService.getIntProperty(Mockito.eq("com.openexchange.office.maxDocumentFileSize"), Mockito.anyInt())).thenReturn(100);
		return configurationService;
	}

	@Override
	protected Collection<Class<?>> getClassesToMock() {
		Collection<Class<?>> res = new HashSet<>();

		res.add(AdvisoryLockHelperService.class);
		res.add(AdvisoryLockInfoService.class);

		res.add(BackupFileService.class);

		res.add(ClientLeftRemovedService.class);
		res.add(ClusterService.class);
		res.add(ClusterLockService.class);
		res.add(ConfigurationService.class);

		res.add(DistributedDocInfoMap.class);
		res.add(DocumentBackupController.class);
		res.add(DocProcessorMetricService.class);

		res.add(EmptyDocumentCache.class);

		res.add(FolderService.class);

		res.add(GenericHtmlDocumentBuilder.class);
		res.add(GroupService.class);

		res.add(IDBasedFileAccessFactory.class);
		res.add(IDBasedFolderAccessFactory.class);
		res.add(IRestoreDocument.class);
		res.add(IResourceManagerFactory.class);

		res.add(JmsTemplateWithoutTtl.class);
		res.add(JSlobServiceRegistry.class);

		res.add(RT2JmsMessageSender.class);

		res.add(ManagedFileManagement.class);

		res.add(PooledConnectionFactoryProxy.class);

		res.add(RecentFileListManagerFactory.class);
		res.add(DocProcessorMessageLoggerService.class);
		res.add(RT2ConfigService.class);
		res.add(RT2NodeInfoService.class);

		res.add(SessionService.class);
		res.add(SpecialLogService.class);

		res.add(UserService.class);

		res.add(Statistics.class);
		return res;
	}

	@Override
	protected Collection<Class<?>> getClassesToInstantiateForUnitttest() {
		Collection<Class<?>> res = new HashSet<>();

		res.add(ClientEventService.class);
		res.add(ClientSequenceQueueDisposerService.class);
		res.add(ConfigurationHelper.class);

		res.add(DocFileServiceFactoryImpl.class);
		res.add(DocProcessorEventService.class);
		res.add(DocumentDisposer.class);
		res.add(DocumentEventHelper.class);

		res.add(FileHelperServiceFactory.class);
		res.add(FolderHelperServiceFactory.class);

		res.add(com.openexchange.office.filter.ooxml.docx.Importer.class);
		res.add(com.openexchange.office.filter.ooxml.pptx.Importer.class);
		res.add(com.openexchange.office.filter.ooxml.xlsx.Importer.class);
		res.add(com.openexchange.office.filter.odf.odp.dom.Importer.class);
		res.add(com.openexchange.office.filter.odf.ods.dom.Importer.class);
		res.add(com.openexchange.office.filter.odf.odt.dom.Importer.class);

		res.add(ImExporterAccessFactory.class);

		res.add(OXDocumentFactoryImpl.class);

		res.add(ResourceManagers.class);
		res.add(ResourceManagerFactory.class);
		res.add(ResourceLocker.class);
		res.add(RT2MessageSender.class);
		res.add(RT2DocProcessorJmsConsumer.class);
		res.add(RT2DocProcessorManager.class);
		res.add(RT2DocProcessorFactory.class);
		res.add(RT2MessageTypeDispatcher.class);
		res.add(RT2DocStateNotifier.class);

		res.add(MsgBackupAndAckProcessorService.class);

		res.add(QueueProcessorDisposer.class);

		res.add(StorageHelperServiceFactory.class);

		res.add(UserConfigurationFactory.class);

		return res;
	}

    @Override
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> mocks) {
        ClusterService clusterService = (ClusterService)mocks.get(ClusterService.class);
        if (clusterService != null) {
            Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(this.testOsgiBundleContextAndUnitTestActivator.getHazelcastNodeId().toString());
        }
    }

	@Override
	protected void initMocks() {
		try {
			initIDBasedFolderAccessMock();
			initIDBasedFileAccessMock();
			initResourceManagerFactory();
		} catch (OXException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	protected void initResourceManagerFactory() {
		IResourceManagerFactory resourceManagerFactory = testOsgiBundleContextAndUnitTestActivator.getMock(IResourceManagerFactory.class);
		Mockito.when(resourceManagerFactory.createInstance(Mockito.anyString())).thenReturn(Mockito.mock(IResourceManager.class));
	}

	protected void initIDBasedFolderAccessMock() throws OXException {
		IDBasedFolderAccessFactory idBasedFolderAccessFactory = this.testOsgiBundleContextAndUnitTestActivator.getMock(IDBasedFolderAccessFactory.class);
		IDBasedFolderAccess idBasedFolderAccess = Mockito.mock(IDBasedFolderAccess.class);
		Mockito.when(idBasedFolderAccessFactory.createAccess(Mockito.any())).thenReturn(idBasedFolderAccess);
		FileStorageFolder fileStorageFolder = Mockito.mock(FileStorageFolder.class);
		Mockito.when(idBasedFolderAccess.getFolder(Mockito.anyString())).thenReturn(fileStorageFolder);
		Mockito.when(fileStorageFolder.getOwnPermission()).thenReturn(DefaultFileStoragePermission.newInstance());
	}

	protected void initIDBasedFileAccessMock() throws OXException, IOException {
		IDBasedFileAccessFactory idBasedFileAccessFactory = this.testOsgiBundleContextAndUnitTestActivator.getMock(IDBasedFileAccessFactory.class);
		IDBasedFileAccess idBasedFileAccess = Mockito.mock(IDBasedFileAccess.class);
		Mockito.when(idBasedFileAccessFactory.createAccess(Mockito.any())).thenReturn(idBasedFileAccess);
		InputStream isStream = ClassLoader.getSystemClassLoader().getResourceAsStream(getFilePath());
		System.err.println("Test document name = " + getFileName());
		if (isStream == null)
		    isStream = ClassLoader.getSystemClassLoader().getResourceAsStream(getFileName());
		byte[] streamData = IOUtils.toByteArray(isStream);
		ByteArrayInputStream memStream = new ByteArrayInputStream(streamData);
		Mockito.when(idBasedFileAccess.getDocument(Mockito.anyString(), Mockito.isNull())).thenReturn(memStream);
		File oxFile = Mockito.mock(File.class);
		Mockito.when(idBasedFileAccess.getFileMetadata(Mockito.anyString(), Mockito.isNull())).thenReturn(oxFile);
		Mockito.when(oxFile.getFileSize()).thenReturn((long)streamData.length);
		Mockito.when(oxFile.getLastModified()).thenReturn(new Date());
		Mockito.when(oxFile.getFileName()).thenReturn(getFileName());
		Mockito.when(oxFile.getFolderId()).thenReturn(TestConstants.FOLDER_ID);
	}
}
