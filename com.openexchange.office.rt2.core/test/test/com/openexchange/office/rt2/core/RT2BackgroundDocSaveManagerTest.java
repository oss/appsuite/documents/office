/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.EmergencySaver;
import com.openexchange.office.rt2.core.doc.PresenterDocProcessor;
import com.openexchange.office.rt2.core.doc.RT2BackgroundDocSaveManager;
import com.openexchange.office.rt2.core.doc.RT2DocProcessor;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.cluster.ClusterService;
import test.com.openexchange.office.rt2.core.SavableDocMock.SaveBehavior;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class RT2BackgroundDocSaveManagerTest {
    private static final long MIN15_IN_MSEC = 15 * 60 * 1000;
    private static final long WAIT_FOR_BKGND_SAVE = 1100;

    @RegisterExtension
    public InUnitTestRule inUnitTestRule = new InUnitTestRule();

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private ClusterService clusterService;

    @BeforeEach
    public void init() throws OXException {
        RT2BackgroundDocSaveManagerTestInformation unittestInformation = new RT2BackgroundDocSaveManagerTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();

        this.clusterService = unitTestBundle.getMock(ClusterService.class);
        Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(UUID.randomUUID().toString());
    }

    @Test
    public void testNotificationsCreatedAndDisposed() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        RT2DocProcessor docNotSavable = new PresenterDocProcessor();
        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        // won't be background saved (does not implement IBackgroundSavable)
        rt2BkgndDocSaveMgr.docProcessorCreated(docNotSavable);

        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(200);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        long saved = docMock.getLastSaveTimeStamp();
        assertTrue(saved != lastSaveTimeStamp);
        assertEquals(1, docMock.numOfSaveTries());
        assertEquals(1, docMock.numOfSaveSuccess());

        rt2BkgndDocSaveMgr.docProcessorDisposed(docMock);

        // won't be background saved (does not implement IBackgroundSavable)
        rt2BkgndDocSaveMgr.docProcessorDisposed(docNotSavable);

        now = new Date().getTime();
        lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(200);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // no save due to the docProcessorDisposed notification
        saved = docMock.getLastSaveTimeStamp();
        assertTrue(saved == lastSaveTimeStamp);
        assertEquals(200, docMock.getPendingOperationsCount());
        assertEquals(1, docMock.numOfSaveTries());
        assertEquals(1, docMock.numOfSaveSuccess());

        // nothing happens if we try to notify the same doc again
        rt2BkgndDocSaveMgr.docProcessorDisposed(docMock);

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testStopAndDestroy() throws Exception {
        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        boolean stopped = rt2BkgndDocSaveMgr.stop();
        assertTrue(stopped);

        try {
            rt2BkgndDocSaveMgr.destroy();
        } catch (Exception e) {
            fail("Exception while destroy() RT2BackgroundDocSaveManager instance");
        }

        // stop() has been called before destroy(), therefore no emergency save
        EmergencySaver emergencySave = unitTestBundle.getMock(EmergencySaver.class);
        Mockito.verify(emergencySave, Mockito.times(0)).emergencySave();

        // renew rt2 background save manager
        rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        try {
            rt2BkgndDocSaveMgr.destroy();
        } catch (Exception e) {
            fail("Exception while destroy() RT2BackgroundDocSaveManager instance");
        }

        // destroy() must call emergency save in case rt2 background doc save mgr has not been stopped
        emergencySave = unitTestBundle.getMock(EmergencySaver.class);
        Mockito.verify(emergencySave, Mockito.times(1)).emergencySave();
    }

    @Test
    public void testSimpleBackgroundSave() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());

        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        assertFalse(docMock.isModified());
        assertFalse(docMock.isDisposed());

        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // no save
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(0, docMock.getPendingOperationsCount());

        // set modified state and last save time to force background save
        docMock.setPendingOperationsCount(50);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // background save has been performed by RT2BackgroundDocSaveManager
        assertNotEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(0, docMock.getPendingOperationsCount());

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testBackgroundSaveWithErrorAndResetWithSuccess() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());

        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        docMock.setSaveBehavior(SaveBehavior.SAVE_PROVIDES_ERROR);

        // set modified state and last save time to force background save
        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(50);

        Thread.sleep(WAIT_FOR_BKGND_SAVE);
        // background save has been performed unsuccessfully by RT2BackgroundDocSaveManager due to error
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(50, docMock.getPendingOperationsCount());
        assertEquals(1, docMock.numOfSaveTries());
        assertEquals(0, docMock.numOfSaveSuccess());
        assertNotEquals(ErrorCode.NO_ERROR, docMock.getLastSaveErrorCode());

        Thread.sleep(WAIT_FOR_BKGND_SAVE);
        // a set error code on the document instance won't perform any new background save
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(50, docMock.getPendingOperationsCount());
        assertEquals(1, docMock.numOfSaveTries());
        assertEquals(0, docMock.numOfSaveSuccess());

        // reset error code to let background save to do it again
        docMock.setSaveBehavior(SaveBehavior.SAVE_NO_ERROR);
        docMock.setLastSaveErrorCode(ErrorCode.NO_ERROR);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        assertNotEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(0, docMock.getPendingOperationsCount());
        assertEquals(2, docMock.numOfSaveTries());
        assertEquals(1, docMock.numOfSaveSuccess());

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testBackgroundSaveWithException() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());

        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER, RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES + 1);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        docMock.setSaveBehavior(SaveBehavior.SAVE_THROWS_EXCEPTION);

        // set modified state and last save time to force background save
        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(1);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // manipulate last save time stamp to speed up retries of background thread
        lastSaveTimeStamp = lastSaveTimeStamp - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);

        CountDownLatch latch = docMock.getSaveCountDownLatch();
        assertNotNull(latch);
        latch.await();

        // we need to wait to enable the background save thread to handle the
        // exception and set a stable document state
        Thread.sleep(100);

        // no more tries than (1 normal save and MAX_NUMBER_OF_RETRIES retries)
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(1, docMock.getPendingOperationsCount());
        assertEquals(RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES + 1, docMock.numOfSaveTries());
        assertEquals(0, docMock.numOfSaveSuccess());
        // error code must be set at document instance
        assertFalse(ErrorCode.NO_ERROR.equals(docMock.getLastSaveErrorCode()));

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testBackgroundSaveWithOXException() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER, RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES + 1);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        docMock.setSaveBehavior(SaveBehavior.SAVE_THROWS_OXEXCEPTION);

        // set modified state and last save time to force background save
        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(1);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // manipulate last save time stamp to speed up retries of background thread
        lastSaveTimeStamp = lastSaveTimeStamp - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);

        CountDownLatch latch = docMock.getSaveCountDownLatch();
        assertNotNull(latch);
        latch.await();

        // we need to wait to enable the background save thread to handle the
        // exception and set a stable document state
        Thread.sleep(100);

        // no more tries than (1 normal save and MAX_NUMBER_OF_RETRIES retries)
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(1, docMock.getPendingOperationsCount());
        assertEquals(RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES + 1, docMock.numOfSaveTries());
        assertEquals(0, docMock.numOfSaveSuccess());
        // error code must be set at document instance
        assertFalse(ErrorCode.NO_ERROR.equals(docMock.getLastSaveErrorCode()));

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testBackgroundSaveWithDisposedDocInstance() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        // set dispose state of document instance
        docMock.dispose();

        // set modified state and last save time to force background save
        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(1);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // nothing happens for disposed document instances
        assertEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(1, docMock.getPendingOperationsCount());
        assertEquals(0, docMock.numOfSaveTries());
        assertEquals(0, docMock.numOfSaveSuccess());

        rt2BkgndDocSaveMgr.destroy();
    }

    @Test
    public void testBackgroundSaveWithExceptionAndSuccessfullRetryOnMAX_NUMBER_OF_RETRIES() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());

        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = prepareBackgroundSaveManager();

        // prepare synchronize with 2nd retry to reset save behavior
        // (1. normal save and 2 retries => 3 save calls)
        SavableDocMock docMock = new SavableDocMock(TextDocProcessor.DOCTYPE_IDENTIFIER, RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES);
        unitTestBundle.injectDependencies(docMock);
        docMock.setDocUID(docUid);

        rt2BkgndDocSaveMgr.docProcessorCreated(docMock);

        docMock.setSaveBehavior(SaveBehavior.SAVE_THROWS_EXCEPTION);

        // set modified state and last save time to force background save
        long now = new Date().getTime();
        long lastSaveTimeStamp = now - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);
        docMock.setPendingOperationsCount(1);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // manipulate last save time stamp to speed up retries of background thread
        lastSaveTimeStamp = lastSaveTimeStamp - MIN15_IN_MSEC;
        docMock.setLastSaveTimeStamp(lastSaveTimeStamp);

        CountDownLatch latch = docMock.getSaveCountDownLatch();
        assertNotNull(latch);
        latch.await();

        docMock.setSaveBehavior(SaveBehavior.SAVE_NO_ERROR);
        Thread.sleep(WAIT_FOR_BKGND_SAVE);

        // check that last retry was successful
        assertNotEquals(docMock.getLastSaveTimeStamp(), lastSaveTimeStamp);
        assertEquals(0, docMock.getPendingOperationsCount());
        assertEquals(RT2BackgroundDocSaveManager.MAX_NUMBER_OF_RETRIES + 1, docMock.numOfSaveTries());
        assertEquals(1, docMock.numOfSaveSuccess());
        assertTrue(ErrorCode.NO_ERROR.equals(docMock.getLastSaveErrorCode()));

        rt2BkgndDocSaveMgr.destroy();
    }

    private RT2BackgroundDocSaveManager prepareBackgroundSaveManager() throws Exception {
        RT2BackgroundDocSaveManager rt2BkgndDocSaveMgr = new RT2BackgroundDocSaveManager();
        unitTestBundle.injectDependencies(rt2BkgndDocSaveMgr);
        rt2BkgndDocSaveMgr.afterPropertiesSet();
        return rt2BkgndDocSaveMgr;
    }

}
