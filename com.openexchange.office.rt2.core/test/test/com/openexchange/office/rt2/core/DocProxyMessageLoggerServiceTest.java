/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyLogInfo.Direction;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

@ExtendWith(InUnitTestRule.class)
public class DocProxyMessageLoggerServiceTest {

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private ScheduledTimerTaskMock scheduledTimerTaskMock = null;

    @BeforeEach
    public void init() throws OXException {
        var unittestInformation = new DocProxyMessageLoggerServiceTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();

        var timerService = unitTestBundle.getMock(TimerService.class);
        prepareTimerServiceMock(timerService);
    }

    @Test
    public void testDocProxyMessageLoggerService() throws Exception {
        var md5MessageDigest = MessageDigest.getInstance("MD5");
        var docFileId = md5MessageDigest.digest("123456789/9876543212".getBytes(Charset.forName("UTF-8"))).toString();
        var docUid = new RT2DocUidType(docFileId);
        var clientUid = new RT2CliendUidType(UUID.randomUUID().toString());

        var docProxyMessageLoggerService = prepareDocProxyMessageLoggerService();

        assertNotNull(docProxyMessageLoggerService);
        assertEquals(0, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        var rt2MsgCreate = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientUid, docUid);
        docProxyMessageLoggerService.addMessageForQueueToLog(Direction.TO_JMS, rt2MsgCreate);
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
        assertEquals(1, docProxyMessageLoggerService.formatMsgsLogInfo(clientUid).size());
        assertEquals(1, docProxyMessageLoggerService.getLogInfoMsgsStates().size());

        docProxyMessageLoggerService.remove(clientUid);
        assertEquals(0, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        docProxyMessageLoggerService.add(docUid, Direction.TO_JMS, RT2MessageType.REQUEST_JOIN.toString(), clientUid);
        docProxyMessageLoggerService.add(docUid, Direction.TO_WS, RT2MessageType.REQUEST_JOIN.toString(), clientUid);
        docProxyMessageLoggerService.add(docUid, Direction.FROM_WS, RT2MessageType.REQUEST_OPEN_DOC.toString(), clientUid);
        assertEquals(3, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
        assertEquals(3, docProxyMessageLoggerService.formatMsgsLogInfo(clientUid).size());
        assertEquals(1, docProxyMessageLoggerService.getLogInfoMsgsStates().size());

        docProxyMessageLoggerService.resetQueueSizeForLogInfo(clientUid, 2);
        assertEquals(2, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
        assertEquals(2, docProxyMessageLoggerService.formatMsgsLogInfo(clientUid).size());
        assertEquals(1, docProxyMessageLoggerService.getLogInfoMsgsStates().size());

        docProxyMessageLoggerService.destroy();
        assertEquals(0, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
    }

    @Test
    public void testDocProxyMessageLoggerServiceAsyncCleanup() throws Exception {
        var md5MessageDigest = MessageDigest.getInstance("MD5");
        var docFileId = md5MessageDigest.digest("123456789/9876543212".getBytes(Charset.forName("UTF-8"))).toString();
        var docUid = new RT2DocUidType(docFileId);
        var clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        var now = LocalDateTime.now(ZoneOffset.UTC);
        var nowPlusCleanupTimeout = now.plus(DocProxyMessageLoggerService.CLEANUP_ORPHANED_LOG_INFO, ChronoUnit.MILLIS);

        var localDateTimeServiceMock = unitTestBundle.getMock(LocalDateTimeService.class);
        when(localDateTimeServiceMock.getNowAsUTC()).thenReturn(now, now, nowPlusCleanupTimeout);

        var docProxyMessageLoggerService = prepareDocProxyMessageLoggerService();
        docProxyMessageLoggerService.add(docUid, Direction.TO_JMS, RT2MessageType.REQUEST_JOIN.toString(), clientUid);
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        var cleanupRunnable = scheduledTimerTaskMock.getRunnable();

        // 1. run puts entry into gc map as DocProxyRegistry mock provides unknown for docProxyUid
        cleanupRunnable.run();
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        // 2. run removes entry as we provide a local date time which is equal to the clean-up time
        cleanupRunnable.run();
        assertEquals(0, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        var docProxy = createDocProxy(clientUid, docUid);
        var docProxyRegistry = unitTestBundle.getMock(RT2DocProxyRegistry.class);
        when(docProxyRegistry.getDocProxy(clientUid, docUid)).thenReturn(docProxy);
        when(localDateTimeServiceMock.getNowAsUTC()).thenReturn(now, nowPlusCleanupTimeout);

        docProxyMessageLoggerService.add(docUid, Direction.TO_JMS, RT2MessageType.REQUEST_JOIN.toString(), clientUid);
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        // 1. run does nothing as the DocProxy instance is alive
        cleanupRunnable.run();
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        // 2. run also does nothing as the DocProxy instance is still alive
        cleanupRunnable.run();
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        // "remove" doc proxy from registry
        when(docProxyRegistry.getDocProxy(clientUid, docUid)).thenReturn(null);
        when(localDateTimeServiceMock.getNowAsUTC()).thenReturn(now, nowPlusCleanupTimeout);

        // 1. run puts entry into gc map as DocProxyRegistry mock provides unknown for docProxyUid
        cleanupRunnable.run();
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        // 2. run removes entry as we provide a local date time which is equal to the clean-up time
        cleanupRunnable.run();
        assertEquals(0, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
    }

    @Test
    public void testDocProxyMessageLoggerServiceCleanupWithException() throws Exception {
        var md5MessageDigest = MessageDigest.getInstance("MD5");
        var docFileId = md5MessageDigest.digest("123456789/9876543212".getBytes(Charset.forName("UTF-8"))).toString();
        var docUid = new RT2DocUidType(docFileId);
        var clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
        var now = LocalDateTime.now(ZoneOffset.UTC);

        var localDateTimeServiceMock = unitTestBundle.getMock(LocalDateTimeService.class);
        when(localDateTimeServiceMock.getNowAsUTC()).thenReturn(now).thenThrow(new IllegalArgumentException());

        var docProxyMessageLoggerService = prepareDocProxyMessageLoggerService();
        docProxyMessageLoggerService.add(docUid, Direction.TO_JMS, RT2MessageType.REQUEST_JOIN.toString(), clientUid);
        assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());

        var cleanupRunnable = scheduledTimerTaskMock.getRunnable();

        try {
            cleanupRunnable.run();
            assertEquals(1, docProxyMessageLoggerService.getMsgsAsList(clientUid).size());
        } catch (Exception ex) {
            fail("Exception should be caught by runnable code to prevent clean-up task to be stopped by TimerService");
        }
    }

    private DocProxyMessageLoggerService prepareDocProxyMessageLoggerService() throws Exception {
        var docProxyMessageLoggerService = new DocProxyMessageLoggerService();
        unitTestBundle.injectDependencies(docProxyMessageLoggerService);
        docProxyMessageLoggerService.afterPropertiesSet();
        return docProxyMessageLoggerService;
    }

    private void prepareTimerServiceMock(TimerService timerServiceMock) {
        doAnswer(new Answer<ScheduledTimerTask>() {
            @Override
            public ScheduledTimerTask answer(InvocationOnMock invocation) {
               var task = (Runnable) invocation.getArguments()[0];
               if (scheduledTimerTaskMock == null) {
                   scheduledTimerTaskMock = new ScheduledTimerTaskMock(task);
               } else {
                   fail("Test expects just one call for TimerService.scheduleAtFixedRate()!");
               }

               return scheduledTimerTaskMock;
            }
        }).when(timerServiceMock).scheduleAtFixedRate(isA(Runnable.class), anyLong(), anyLong());
    }

    private RT2DocProxy createDocProxy(RT2CliendUidType clientUid, RT2DocUidType docUid) {
        var rt2JoinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, clientUid, docUid);
        var docProxyStateHolderFactory = unitTestBundle.getBean(RT2DocProxyStateHolderFactory.class);
        var docProxyStateHolder = docProxyStateHolderFactory.createDocProxyStateHolder(rt2JoinMsg);
        var sessionId = new RT2SessionIdType(UUID.randomUUID().toString());
        return new RT2DocProxy(clientUid, docUid, new RT2ChannelId(UUID.randomUUID().toString()), docProxyStateHolder, sessionId);
    }
}
