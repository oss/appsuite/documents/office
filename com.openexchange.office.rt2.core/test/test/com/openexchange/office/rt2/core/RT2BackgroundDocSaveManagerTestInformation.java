/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import java.util.Collection;
import java.util.HashSet;
import org.mockito.Mockito;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.doc.EmergencySaver;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class RT2BackgroundDocSaveManagerTestInformation extends RT2UnittestInformation {

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res =  new HashSet<>();
        res.addAll(getMandantoryDependenciesOfDocProcessor());
        res.add(CachingFacade.class);
        res.add(ClusterService.class);
        res.add(RT2DocProcessorManager.class);
        res.add(RT2DocProxyRegistry.class);
        res.add(RT2JmsMessageSender.class);
        res.add(DistributedDocInfoMap.class);
        res.add(EmergencySaver.class);
        return res;
    }

    @Override
    protected void initMocks() {
        ServerSession session = Mockito.mock(ServerSession.class);
        User mockUser = Mockito.mock(User.class);
        Mockito.when(session.getUser()).thenReturn(mockUser);
        SessionService sessionService = this.testOsgiBundleContextAndUnitTestActivator.getMock(SessionService.class);
        try {
            Mockito.when(sessionService.getSession4Id(Mockito.anyString())).thenReturn(session);
        } catch (OXException e) {
            // Cannot occure, pure mocking code
        }
        ConfigurationService cfgService = Mockito.mock(ConfigurationService.class);
        Mockito.when(cfgService.getBoolProperty(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(false);
    }

    @Override
    protected Collection<Object> getAdditionalServices() {
        Collection<Object> res = new HashSet<>();
        res.add(new RT2DocProxyStateHolderFactory());
        return res;
    }
}
