/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.sequence;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.tools.service.cluster.ClusterService;

import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class MsgBackupAndACKProcessorTestInformation extends RT2UnittestInformation {

	@Override
	protected Collection<Class<?>> getClassesToMock() {
		Collection<Class<?>> res =  new HashSet<>();
		res.add(ClusterService.class);
		res.add(RT2ConfigService.class);
		res.add(RT2MessageSender.class);
		res.add(RT2NodeInfoService.class);
		res.add(ClientEventService.class);
		res.add(RT2DocProcessorManager.class);
		return res;
	}

    @Override
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> mocks) {
        ClusterService clusterService = (ClusterService)mocks.get(ClusterService.class);
        if (clusterService != null) {
            Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(this.testOsgiBundleContextAndUnitTestActivator.getHazelcastNodeId().toString());
        }
    }
}
