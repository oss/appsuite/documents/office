/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.websocketToJms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.exception.RT2SessionInvalidException;
import com.openexchange.office.rt2.core.exception.RT2TypedException;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.EDocProxyState;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolder;
import com.openexchange.office.rt2.core.ws.RT2EnhDefaultWebSocket;
import com.openexchange.office.rt2.core.ws.RT2SessionValidator;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;
import test.com.openexchange.office.rt2.proxy.TestDocProxy;

public class InvalidSessionTest extends BaseTest {

	public static final String OX_NODE_ID = "oxNodeId";

	@Override
	protected void initServices() {
		this.docProxyRegistry = unitTestBundle.getService(RT2DocProxyRegistry.class);
		this.webSocketListener = unitTestBundle.getService(RT2WebSocketListener.class);
		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		return null;
	}

	@Override
    @BeforeEach
	public void init() throws Exception {
		super.init();

		RT2SessionValidator sessionValidator = unitTestBundle.getMock(RT2SessionValidator.class);

		System.setProperty("TIMEOUT_HARD_CLOSE", "500");
		try {
			Mockito.doThrow(new RT2SessionInvalidException(new ArrayList<>())).when(sessionValidator).validateSession(Mockito.any(), Mockito.any());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@AfterEach
	public void destroy() {
		System.setProperty("TIMEOUT_HARD_CLOSE", Long.toString(RT2DocProxy.TIMEOUT_HARD_CLOSE));
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=CloseDocUnittestInformation.class)
	public void testProxyStateJoined() throws Exception {

		rt2EnhDefaultWebSocket = Mockito.mock(RT2EnhDefaultWebSocket.class);

		RT2DocProxyStateHolder docProxyStateHolder = Mockito.mock(RT2DocProxyStateHolder.class);
		RT2DocProxy docProxy = new TestDocProxy(clientId1, docUid1, rt2EnhDefaultWebSocket.getId(), docProxyStateHolder);
		unitTestBundle.injectDependencies(docProxy);
		this.docProxyRegistry.registerDocProxy(docProxy);

		Mockito.when(docProxyStateHolder.getProxyState()).thenReturn(EDocProxyState.E_JOINED);

		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_DOC, clientId1, docUid1);

		webSocketListener.onMessageSync(rt2EnhDefaultWebSocket, RT2MessageFactory.toJSONString(msg));

		Thread.sleep(1000);

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(rt2EnhDefaultWebSocket).send(rt2MsgCaptor.capture());
		List<RT2Message> socketSendMsgs = rt2MsgCaptor.getAllValues();
		assertEquals(1, socketSendMsgs.size());
		RT2Message sollGenericErrorResp = TestRT2MessageCreator.createGenericErrorResp(clientId1, docUid1, "GENERAL_SESSION_INVALID_ERROR", 2, 25);
		sollGenericErrorResp.setHeader("internal.type", "true");
		RT2MessageValidator.validate(sollGenericErrorResp, socketSendMsgs.get(0), "msg_id_header", "internal.type");

		ArgumentCaptor<RT2Message> rt2MsgCaptorJmsMsgSender = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender).sendToDocumentQueue(rt2MsgCaptorJmsMsgSender.capture(), Mockito.anyList());
		List<RT2Message> jmsMessageSenderMsgs = rt2MsgCaptorJmsMsgSender.getAllValues();
		assertEquals(1, jmsMessageSenderMsgs.size());
		RT2Message sollLeaveRequest = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_AND_LEAVE, clientId1, docUid1);
		sollLeaveRequest.setHeader("cause_header", "SESSION_INVALID");
		RT2MessageValidator.validate(sollLeaveRequest, jmsMessageSenderMsgs.get(0), "msg_id_header");
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=CloseDocUnittestInformation.class)
	public void testProxyStateOpen() throws InterruptedException, RT2TypedException {

		rt2EnhDefaultWebSocket = Mockito.mock(RT2EnhDefaultWebSocket.class);

		RT2DocProxyStateHolder docProxyStateHolder = Mockito.mock(RT2DocProxyStateHolder.class);
		RT2DocProxy docProxy = new TestDocProxy(clientId1, docUid1, rt2EnhDefaultWebSocket.getId(), docProxyStateHolder);
		unitTestBundle.injectDependencies(docProxy);
		this.docProxyRegistry.registerDocProxy(docProxy);

		Mockito.when(docProxyStateHolder.getProxyState()).thenReturn(EDocProxyState.E_OPEN);

		RT2Message msg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_DOC, clientId1, docUid1);

		webSocketListener.onMessageSync(rt2EnhDefaultWebSocket, RT2MessageFactory.toJSONString(msg));

		Thread.sleep(1000);

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(rt2EnhDefaultWebSocket).send(rt2MsgCaptor.capture());
		List<RT2Message> socketSendMsgs = rt2MsgCaptor.getAllValues();
		assertEquals(1, socketSendMsgs.size());
		RT2Message sollGenericErrorResp = TestRT2MessageCreator.createGenericErrorResp(clientId1, docUid1, "GENERAL_SESSION_INVALID_ERROR", 2, 25);
		sollGenericErrorResp.setHeader("internal.type", "true");
		RT2MessageValidator.validate(sollGenericErrorResp, socketSendMsgs.get(0), "msg_id_header", "internal.type");

		ArgumentCaptor<RT2Message> rt2MsgCaptorJmsMsgSender = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender).sendToDocumentQueue(rt2MsgCaptorJmsMsgSender.capture(), Mockito.anyList());
		List<RT2Message> jmsMessageSenderMsgs = rt2MsgCaptorJmsMsgSender.getAllValues();
		assertEquals(1, jmsMessageSenderMsgs.size());
		RT2Message sollCloseDocReq = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_AND_LEAVE, clientId1, docUid1);
		sollCloseDocReq.setHeader("cause_header", "SESSION_INVALID");
		RT2MessageValidator.validate(sollCloseDocReq, jmsMessageSenderMsgs.get(0), "msg_id_header");
	}
}
