/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.sequence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueue;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;

public class ClientSequenceQueueTest {

	private RT2CliendUidType clientUid = new RT2CliendUidType("testClientUid");
	private RT2DocUidType docUid = new RT2DocUidType("testDocUid");

	@Test
	public void testDeterminePendingMessagesWhichCanBeProcessed() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid);
		Set<Integer> soll = new HashSet<>();
		for (int i=1;i<10;++i) {
			objectToTest.enqueueInMessage(createRT2Message(i));
			soll.add(i);
		}
		List<RT2Message> pendingMessages = objectToTest.determinePendingMessagesWhichCanBeProcessed();
		for (RT2Message msg : pendingMessages) {
			assertTrue(soll.remove(msg.getSeqNumber().getValue()));
		}
		assertTrue(soll.isEmpty(), soll.toString());
	}

	@Test
	public void testDeterminePendingMessagesWhichCanBeProcessedWithGaps() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid);
		Set<Integer> soll = new HashSet<>();
		for (int i=1;i<4;++i) {
			objectToTest.enqueueInMessage(createRT2Message(i));
			soll.add(i);
		}
		for (int i=7;i<10;++i) {
			objectToTest.enqueueInMessage(createRT2Message(i));
		}
		List<RT2Message> pendingMessages = objectToTest.determinePendingMessagesWhichCanBeProcessed();
		for (RT2Message msg : pendingMessages) {
			assertTrue(soll.remove(msg.getSeqNumber().getValue()));
		}
		assertTrue(soll.isEmpty(), soll.toString());
	}

	@Test
	public void testDeterminePendingMessagesWhichCanBeProcessedWithNackedMessages() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid, 1);
		objectToTest.enqueueInMessage(createRT2Message(10));
		Set<Integer> isNacks = objectToTest.checkForAndGenerateNacks();
		Set<Integer> sollNacks = new HashSet<>();
		for (int i=1;i<10;++i) {
			sollNacks.add(i);
		}
		assertEquals(sollNacks, isNacks);
		//-------------------------------------------------------------------------
		for (int i=1;i<6;++i) {
			objectToTest.enqueueInMessage(createRT2Message(i));
		}
		objectToTest.determinePendingMessagesWhichCanBeProcessed();
		//-------------------------------------------------------------------------
		Thread.sleep(1000);
		isNacks = objectToTest.checkForAndGenerateNacks();
		sollNacks = new HashSet<>();
		for (int i=6;i<10;++i) {
			sollNacks.add(i);
		}
		assertEquals(sollNacks, isNacks);
	}

	@Test
	public void testCheckForAndGenerateNacks() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid);
		objectToTest.enqueueInMessage(createRT2Message(10));
		Set<Integer> isNacks = objectToTest.checkForAndGenerateNacks();
		Set<Integer> sollNacks = new HashSet<>();
		for (int i=1;i<10;++i) {
			sollNacks.add(i);
		}
		assertEquals(sollNacks, isNacks);
	}

	@Test
	public void testCheckForAndGenerateNacksWithNacksNotToSendAgain() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid);
		objectToTest.enqueueInMessage(createRT2Message(10));
		Set<Integer> isNacks = objectToTest.checkForAndGenerateNacks();
		Set<Integer> sollNacks = new HashSet<>();
		for (int i=1;i<10;++i) {
			sollNacks.add(i);
		}
		assertEquals(sollNacks, isNacks);
		isNacks = objectToTest.checkForAndGenerateNacks();
		assertTrue(isNacks.isEmpty(), isNacks.toString());
	}

	@Test
	public void testCheckForAndGenerateNacksWithNacksToSendAgain() throws Exception {
		ClientSequenceQueue objectToTest = new ClientSequenceQueue(clientUid, 3);
		objectToTest.enqueueInMessage(createRT2Message(10));
		Set<Integer> isNacks = objectToTest.checkForAndGenerateNacks();
		Set<Integer> sollNacks = new HashSet<>();
		for (int i=1;i<10;++i) {
			sollNacks.add(i);
		}
		assertEquals(sollNacks, isNacks);
		Thread.sleep(3000);
		isNacks = objectToTest.checkForAndGenerateNacks();
		assertEquals(sollNacks, isNacks);
	}

	private RT2Message createRT2Message(int seqNr) {
		RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APP_ACTION, clientUid, docUid);
		res.setSeqNumber(new RT2SeqNumberType(seqNr));
		return res;
	}
}
