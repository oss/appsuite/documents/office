/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import com.openexchange.timer.ScheduledTimerTask;
import com.openexchange.timer.TimerService;

public class TimerServiceMock implements TimerService {
    private List<ScheduledTimerTask> scheduledTimerTasks = new ArrayList<>();

    public List<ScheduledTimerTask> getTimerTasks() {
        return scheduledTimerTasks;
    }

    public void removeScheduledTimerTask(ScheduledTimerTask task) {
        scheduledTimerTasks.remove(task);
    }

    @Override
    public ScheduledTimerTask schedule(Runnable task, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledTimerTask schedule(Runnable task, long delay) {
        return null;
    }

    @Override
    public ScheduledTimerTask scheduleAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledTimerTask scheduleAtFixedRate(Runnable task, long initialDelay, long period) {
        ScheduledTimerTask timerTask = new ScheduledTimerTaskMock(this, task);
        scheduledTimerTasks.add(timerTask);
        return timerTask;
    }

    @Override
    public ScheduledTimerTask scheduleWithFixedDelay(Runnable task, long initialDelay, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledTimerTask scheduleWithFixedDelay(Runnable task, long initialDelay, long delay) {
        return null;
    }

    @Override
    public void purge() {
        // nothing to do
    }

    @Override
    public Executor getExecutor() {
        return null;
    }

}
