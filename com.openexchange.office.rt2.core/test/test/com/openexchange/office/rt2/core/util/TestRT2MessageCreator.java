/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.util;

import static org.junit.jupiter.api.Assertions.fail;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageJmsPostProcessor;
import com.openexchange.office.rt2.protocol.RT2MessagePostProcessor;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocTypeType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;

public class TestRT2MessageCreator {

    public static RT2Message createJoinMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2NodeUuidType nodeId) {
        return createJoinMsg(client, docUid, nodeId, null, null, null);
    }

    public static RT2Message createJoinResponseMsg(RT2CliendUidType client, RT2DocUidType docUid) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_JOIN, client, docUid);
        return res;
    }

    public static RT2Message createJoinResponseMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2NodeUuidType nodeId, RT2FolderIdType folderId, RT2FileIdType fileId, RT2DocTypeType docTypeType) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_JOIN, client, docUid);
        res.setFolderID(folderId);
        res.setFileID(fileId);
        res.setDocType(docTypeType);
        return res;
    }

    public static RT2Message createJoinMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2NodeUuidType nodeId, RT2FolderIdType folderId, RT2FileIdType fileId, RT2SessionIdType sessionId) {
        if (folderId == null) {
            folderId = new RT2FolderIdType(TestConstants.FOLDER_ID);
        }
        if (fileId == null) {
            fileId = new RT2FileIdType(TestConstants.FILE_ID);
        }
        if (sessionId == null) {
            sessionId = new RT2SessionIdType(TestConstants.SESSION_ID);
        }
        RT2Message joinMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_JOIN, client, docUid);
        joinMsg.setFolderID(folderId);
        joinMsg.setFileID(fileId);
        joinMsg.setSessionID(sessionId);
        return joinMsg;
    }

    public static RT2Message createOpenDocMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2NodeUuidType nodeId, RT2FolderIdType folderId, RT2FileIdType fileId, RT2SessionIdType sessionId) {
        if (folderId == null) {
            folderId = new RT2FolderIdType(TestConstants.FOLDER_ID);
        }
        if (fileId == null) {
            fileId = new RT2FileIdType(TestConstants.FILE_ID);
        }
        if (sessionId == null) {
            sessionId = new RT2SessionIdType(TestConstants.SESSION_ID);
        }
        RT2Message openDocMsg = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_OPEN_DOC, client, docUid);
        openDocMsg.setFolderID(folderId);
        openDocMsg.setFileID(fileId);
        openDocMsg.setSessionID(sessionId);
        return openDocMsg;
    }

    public static RT2Message createGenericErrorResp(RT2CliendUidType client, RT2DocUidType docUid, String errNameHeader, int errClassHeader, int errCodeHeader) {
        RT2Message errMsg = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_GENERIC_ERROR, client, docUid);
        errMsg.setHeader("err_name_header", errNameHeader);
        errMsg.setHeader("err_class_header", errClassHeader);
        errMsg.setHeader("err_code_header", errCodeHeader);
        return errMsg;
    }

    public static RT2Message createResponseOpenDocChunkMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String folderId, String fileId) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_OPEN_DOC_CHUNK, client, docUid);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        res.setFolderID(new RT2FolderIdType(folderId));
        res.setFileID(new RT2FileIdType(fileId));
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_OPEN_DOC);
        return res;
    }

    public static RT2Message createResponseOpenDocMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String folderId, String fileId) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_OPEN_DOC, client, docUid);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        res.setFolderID(new RT2FolderIdType(folderId));
        res.setFileID(new RT2FileIdType(fileId));
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_OPEN_DOC);
        return res;
    }

    public static void setMessagePostProcDefaultHeader(RT2Message msg, RT2MessageType rt2MessageType) {
        msg.setHeader(RT2MessagePostProcessor.HEADER_VERSION_MSG_RCV, false);
        msg.setHeader(RT2MessagePostProcessor.HEADER_VERSION_MSG_SND, false);
        msg.setHeader(RT2MessagePostProcessor.HEADER_GPB_MSG, false);
        msg.setHeader(RT2MessageJmsPostProcessor.HEADER_MSG_TYPE, rt2MessageType);
    }

    public static RT2Message createApplyOpsMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String body) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_APPLY_OPS);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        try {
            res.setBodyString(body);
        } catch (JSONException e) {
            fail(e.getMessage());
        }
        return res;
    }

    public static RT2Message createResponseApplyOpsMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String body) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_APPLY_OPS, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_APPLY_OPS);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        try {
            res.setBodyString(body);
        } catch (JSONException e) {
            fail(e.getMessage());
        }
        return res;
    }

    public static RT2Message createUpdateBroadcast(boolean withDefaultHeader, RT2MessageType baseMsgType, RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String body) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_UPDATE, client, docUid);
        if (withDefaultHeader) {
            setMessagePostProcDefaultHeader(res, baseMsgType);
        }
        if (seqNr != -1) {
            res.setSeqNumber(new RT2SeqNumberType(seqNr));
        }
        if (body != null) {
            try {
                res.setBodyString(body);
            } catch (JSONException e) {
                fail(e.getMessage());
            }
        }
        return res;
    }

    public static RT2Message createUpdateClientsBroadcast(boolean withDefaultHeader, RT2MessageType baseMsgType, RT2CliendUidType client, RT2DocUidType docUid, int seqNr, String body) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.BROADCAST_UPDATE_CLIENTS, client, docUid);
        if (withDefaultHeader) {
            setMessagePostProcDefaultHeader(res, baseMsgType);
        }
        if (seqNr != -1) {
            res.setSeqNumber(new RT2SeqNumberType(seqNr));
        }
        if (body != null) {
            try {
                res.setBodyString(body);
            } catch (JSONException e) {
                fail(e.getMessage());
            }
        }
        return res;
    }

    public static RT2Message createCloseRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, RT2SessionIdType sessionId) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_CLOSE_DOC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_CLOSE_DOC);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        res.setSessionID(sessionId);
        return res;
    }

    public static RT2Message createLeaveRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2SessionIdType sessionId) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_LEAVE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_LEAVE);
        res.setSessionID(sessionId);
        return res;
    }

    public static RT2Message createEmergencyLeaveRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2SessionIdType sessionId) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_EMERGENCY_LEAVE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_EMERGENCY_LEAVE);
        res.setSessionID(sessionId);
        return res;
    }

    public static RT2Message createSaveRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, RT2SessionIdType sessionId, int seqNr) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_SAVE_DOC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_SAVE_DOC);
        res.setSessionID(sessionId);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createUnavailRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, long unavailTime) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_UNAVAILABILITY, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_SAVE_DOC);
        res.setUnvailableTime(new RT2UnavailableTimeType(unavailTime));
        return res;
    }

    public static RT2Message createSyncRequestMsg(RT2CliendUidType client, RT2DocUidType docUid) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_SYNC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_SYNC);
        return res;
    }

    public static RT2Message createSyncStableRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr, int osn) throws JSONException {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_SYNC_STABLE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_SYNC_STABLE);
        res.setBody(new JSONObject().put("osn", osn));
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createEditRightsRequestMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) throws JSONException {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_EDITRIGHTS, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.REQUEST_EDITRIGHTS);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createCloseResponseMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_CLOSE_DOC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_CLOSE_DOC);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createLeaveResponseMsg(RT2CliendUidType client, RT2DocUidType docUid) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_LEAVE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_LEAVE);
        return res;
    }

    public static RT2Message createAbortOpenResponseMsg(RT2CliendUidType client, RT2DocUidType docUid) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_ABORT_OPEN, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_ABORT_OPEN);
        return res;
    }

    public static RT2Message createEmergencyLeaveResponseMsg(RT2CliendUidType client, RT2DocUidType docUid) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_EMERGENCY_LEAVE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_EMERGENCY_LEAVE);
        return res;
    }

    public static RT2Message createSaveDocResponse(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_SAVE_DOC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_SAVE_DOC);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createSyncResponseMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_SYNC, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_SYNC);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createSyncStableResponseMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_SYNC_STABLE, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_SYNC_STABLE);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

    public static RT2Message createEditRightsResponseMsg(RT2CliendUidType client, RT2DocUidType docUid, int seqNr) throws JSONException {
        RT2Message res = RT2MessageFactory.newMessage(RT2MessageType.RESPONSE_EDITRIGHTS, client, docUid);
        setMessagePostProcDefaultHeader(res, RT2MessageType.RESPONSE_EDITRIGHTS);
        res.setSeqNumber(new RT2SeqNumberType(seqNr));
        return res;
    }

}
