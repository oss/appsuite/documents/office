/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.util;

import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessorEventService;
import com.openexchange.office.rt2.core.doc.IClientEventListener;
import com.openexchange.office.rt2.core.doc.IDocNotificationHandler;
import com.openexchange.office.rt2.core.doc.IDocProcessorEventListener;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.core.osgi.RT2CoreBundleContextIdentificator;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;

public class RT2TestOsgiBundleContextAndUnitTestActivator extends TestOsgiBundleContextAndUnitTestActivator {

	private static final Logger log = LoggerFactory.getLogger(RT2TestOsgiBundleContextAndUnitTestActivator.class);
	
	protected ClientEventService clientEventService;
	protected DocProcessorEventService docProcessorEventService;
	protected RT2DocStateNotifier docStateNotifier;
	
	public RT2TestOsgiBundleContextAndUnitTestActivator(UnittestInformation unittestInformation) {
		super(new RT2CoreBundleContextIdentificator(), unittestInformation);
	}
	
	@Override
	protected Object createMock(Class<?> clazz) {
		return Mockito.mock(clazz);
	}

	protected void initConfigService() {
		unittestInformation.initConfigService();
		((RT2UnittestInformation)unittestInformation).initRT2ConfigService();
	}
	
	public void registerAllListenersToEventServices() {
		services.forEach(s -> registerServiceToEventServices(s));
	}
	
	public void registerServiceToEventServices(Object service) {
		registerListenerToClientEventService(service);
		registerListenerToDocProcessorEventService(service);
		registerListenerToDocStateNotifierService(service);
	}

	private void registerListenerToClientEventService(Object service) {
		if (clientEventService != null) {
			if (service instanceof IClientEventListener) {
				clientEventService.addListener((IClientEventListener) service);
			}
		}
	}
	
	private void registerListenerToDocProcessorEventService(Object service) {
		if (docProcessorEventService != null) {
			if (service instanceof IDocProcessorEventListener) {
				docProcessorEventService.addListener((IDocProcessorEventListener) service);
			}
		}
	}
	
	private void registerListenerToDocStateNotifierService(Object service) {
		if (docStateNotifier != null) {
			if (service instanceof IDocNotificationHandler) {
				docStateNotifier.addDocNotificationHandler((IDocNotificationHandler) service);
			}
		}
	}
		
	@Override
	public void afterCreatedServices() {		
		if (hasService(ClientEventService.class)) {
			this.clientEventService = findService(ClientEventService.class, false);
		}
		if (hasService(DocProcessorEventService.class)) {
			this.docProcessorEventService = findService(DocProcessorEventService.class, false);
		}
		if (hasService(RT2DocStateNotifier.class)) {
			this.docStateNotifier = findService(RT2DocStateNotifier.class, false);
		}
	}
}
