/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.ITaskListener;
import com.openexchange.office.rt2.core.control.Task;
import com.openexchange.office.rt2.core.control.impl.CleanupTaskProcessor;

public class CleanupTaskProcessorTest {

	//-------------------------------------------------------------------------
	public static class TestTaskListener implements ITaskListener<Task>
	{

		@Override
		public void taskCompleted(Task aCompletedTask) {
			// won't be called
		}

	}

	//-------------------------------------------------------------------------
	@Test
	public void test() throws Exception {
		final ITaskListener<Task> aTaskListener = new TestTaskListener();
		final CleanupTaskProcessor aCleanupTaskProcessor = new CleanupTaskProcessor(aTaskListener);

		assertNotNull(aTaskListener);
		assertNotNull(aCleanupTaskProcessor);

		assertFalse(aCleanupTaskProcessor.isStarted());
		assertTrue(aCleanupTaskProcessor.getPendingTasks() == 0);

		aCleanupTaskProcessor.removeAll();
		assertTrue(aCleanupTaskProcessor.getPendingTasks() == 0);
		assertFalse(aCleanupTaskProcessor.isStarted());

		aCleanupTaskProcessor.shutdown();
		assertFalse(aCleanupTaskProcessor.isStarted());

		aCleanupTaskProcessor.start();
		assertTrue(aCleanupTaskProcessor.isStarted());

		aCleanupTaskProcessor.shutdown();
		assertFalse(aCleanupTaskProcessor.isStarted());
	}

}
