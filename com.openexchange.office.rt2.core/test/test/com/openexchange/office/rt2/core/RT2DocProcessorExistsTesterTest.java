/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.RT2DocProcessorExistsTester;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.cluster.ClusterService;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class RT2DocProcessorExistsTesterTest {

    @RegisterExtension
	public InUnitTestRule inUnitTestRule = new InUnitTestRule();

	private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

	private RT2DocProxyRegistry docProxyRegistry;
	private RT2DocProcessorManager docProcMngr;

	private RT2DocProcessorExistsTester objectUnderTest;

	private RT2DocProxyStateHolderFactory docProxyStateHolderFactory;

	@BeforeEach
	public void init() throws Exception {

		RT2DocProcessorExistsTesterTestInformation unittestInformation = new RT2DocProcessorExistsTesterTestInformation();
		unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
		unitTestBundle.start();

		ClusterService clusterService = this.unitTestBundle.getMock(ClusterService.class);
		Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(UUID.randomUUID().toString());

		docProxyRegistry = unitTestBundle.getMock(RT2DocProxyRegistry.class);
		docProcMngr = unitTestBundle.getMock(RT2DocProcessorManager.class);

		objectUnderTest = new RT2DocProcessorExistsTester();
		unitTestBundle.injectDependencies(objectUnderTest);

		docProxyStateHolderFactory = unitTestBundle.getService(RT2DocProxyStateHolderFactory.class);
	}

	@Test
	public void testWithOnlyOneMember() throws OXException {

		ClusterService clusterService = this.unitTestBundle.getMock(ClusterService.class);
		Mockito.when(clusterService.getCountMembers()).thenReturn(1);

		RT2DocUidType docUid1 = new RT2DocUidType("docUid1");
		RT2DocUidType docUid2 = new RT2DocUidType("docUid2");
		RT2DocUidType docUid3 = new RT2DocUidType("docUid3");
		RT2DocUidType docUid4 = new RT2DocUidType("docUid4");
		RT2DocUidType docUid5 = new RT2DocUidType("docUid5");
		RT2DocUidType docUid6 = new RT2DocUidType("docUid6");
		RT2DocUidType docUid7 = new RT2DocUidType("docUid7");
		RT2DocUidType docUid8 = new RT2DocUidType("docUid8");

		try {
			TextDocProcessor textDocProcessorDocUuid1 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid1);
			textDocProcessorDocUuid1.setDocUID(docUid1);

			TextDocProcessor textDocProcessorDocUuid3 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid3);
			textDocProcessorDocUuid3.setDocUID(new RT2DocUidType(docUid3.toString()));

			TextDocProcessor textDocProcessorDocUuid5 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid5);
			textDocProcessorDocUuid5.setDocUID(new RT2DocUidType(docUid5.toString()));

			Mockito.when(docProcMngr.getDocProcessors()).thenReturn(new HashSet<>(Arrays.asList(textDocProcessorDocUuid1, textDocProcessorDocUuid3, textDocProcessorDocUuid5)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
		RT2SessionIdType sessionId = new RT2SessionIdType("testSessionId");
		Mockito.when(docProxyRegistry.listAllDocProxies()).thenReturn(
				Arrays.asList(
						new RT2DocProxy(new RT2CliendUidType("proxy1"), docUid1, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy1"), docUid1), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy2"), docUid2, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy2"), docUid2), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy3"), docUid3, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy3"), docUid3), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy4"), docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy4"), docUid4), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy5"), docUid5, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy5"), docUid5), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy6"), docUid6, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy6"), docUid6), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy7"), docUid7, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy7"), docUid7), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy8"), docUid8, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy8"), docUid8), sessionId)
				)
		);

		objectUnderTest.run();

		List<String> markedForRemove = objectUnderTest.getMarkedForRemove().stream().map(d -> d.getValue()).collect(Collectors.toList());
		Collections.sort(markedForRemove);
		List<String> sollMarkedForRemove = Arrays.asList(docUid2.getValue(), docUid4.getValue(), docUid6.getValue(), docUid7.getValue(), docUid8.getValue());
		Collections.sort(sollMarkedForRemove);
		assertArrayEquals(sollMarkedForRemove.toArray(), markedForRemove.toArray());
	}

	@Test
	public void testWithMultipleMembers() throws OXException {

		ClusterService clusterService = this.unitTestBundle.getMock(ClusterService.class);
		Mockito.when(clusterService.getCountMembers()).thenReturn(2);

		RT2DocUidType docUid1 = new RT2DocUidType("docUid1");
		RT2DocUidType docUid2 = new RT2DocUidType("docUid2");
		RT2DocUidType docUid3 = new RT2DocUidType("docUid3");
		RT2DocUidType docUid4 = new RT2DocUidType("docUid4");
		RT2DocUidType docUid5 = new RT2DocUidType("docUid5");
		RT2DocUidType docUid6 = new RT2DocUidType("docUid6");
		RT2DocUidType docUid7 = new RT2DocUidType("docUid7");
		RT2DocUidType docUid8 = new RT2DocUidType("docUid8");

		try {
			TextDocProcessor textDocProcessorDocUuid1 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid1);
			textDocProcessorDocUuid1.setDocUID(docUid1);

			TextDocProcessor textDocProcessorDocUuid3 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid3);
			textDocProcessorDocUuid3.setDocUID(docUid3);

			Mockito.when(docProcMngr.getDocProcessors()).thenReturn(new HashSet<>(Arrays.asList(textDocProcessorDocUuid1, textDocProcessorDocUuid3)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
		RT2SessionIdType sessionId = new RT2SessionIdType("testSessionId");
		Mockito.when(docProxyRegistry.listAllDocProxies()).thenReturn(
				Arrays.asList(
						new RT2DocProxy(new RT2CliendUidType("proxy1"), docUid1, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy1"), docUid1), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy2"), docUid2, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy2"), docUid2), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy3"), docUid3, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy3"), docUid3), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy4"), docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy4"), docUid4), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy5"), docUid5, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy5"), docUid5), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy6"), docUid6, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy6"), docUid6), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy7"), docUid7, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy7"), docUid7), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy8"), docUid8, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy8"), docUid8), sessionId)
				)
		);


		objectUnderTest.run();

		objectUnderTest.processUpdateCurrProcResponse(UUID.randomUUID(), System.currentTimeMillis(),
				Arrays.asList(DocUidType.newBuilder().setValue(docUid5.getValue()).build(), DocUidType.newBuilder().setValue(docUid7.getValue()).build()));

		List<String> markedForRemove = objectUnderTest.getMarkedForRemove().stream().map(d -> d.getValue()).collect(Collectors.toList());
		Collections.sort(markedForRemove);
		List<String> sollMarkedForRemove = Arrays.asList(docUid2.getValue(), docUid4.getValue(), docUid6.getValue(), docUid8.getValue());
		Collections.sort(sollMarkedForRemove);
		assertArrayEquals(sollMarkedForRemove.toArray(), markedForRemove.toArray());
	}

	@Test
	public void testTwiceTimes() {
		ClusterService clusterService = this.unitTestBundle.getMock(ClusterService.class);
		Mockito.when(clusterService.getCountMembers()).thenReturn(2);

		objectUnderTest.run();
		long currentlyRunning = objectUnderTest.getCurrentlyRunning();
		objectUnderTest.run();
		assertEquals(currentlyRunning, objectUnderTest.getCurrentlyRunning());
	}

	@Test
	public void testMarkedForRemove() {
		ClusterService clusterService = this.unitTestBundle.getMock(ClusterService.class);
		Mockito.when(clusterService.getCountMembers()).thenReturn(1);

		RT2DocUidType docUid1 = new RT2DocUidType("docUid1");
		RT2DocUidType docUid2 = new RT2DocUidType("docUid2");
		RT2DocUidType docUid3 = new RT2DocUidType("docUid3");
		RT2DocUidType docUid4 = new RT2DocUidType("docUid4");
		RT2DocUidType docUid5 = new RT2DocUidType("docUid5");
		RT2DocUidType docUid6 = new RT2DocUidType("docUid6");
		RT2DocUidType docUid7 = new RT2DocUidType("docUid7");
		RT2DocUidType docUid8 = new RT2DocUidType("docUid8");

		try {
			TextDocProcessor textDocProcessorDocUuid1 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid1);
			textDocProcessorDocUuid1.setDocUID(docUid1);

			TextDocProcessor textDocProcessorDocUuid3 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid3);
			textDocProcessorDocUuid3.setDocUID(new RT2DocUidType(docUid3.toString()));

			TextDocProcessor textDocProcessorDocUuid5 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid5);
			textDocProcessorDocUuid5.setDocUID(new RT2DocUidType(docUid5.toString()));

			Mockito.when(docProcMngr.getDocProcessors()).thenReturn(new HashSet<>(Arrays.asList(textDocProcessorDocUuid1, textDocProcessorDocUuid3, textDocProcessorDocUuid5)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
		RT2SessionIdType sessionId = new RT2SessionIdType("testSessionId");
		Mockito.when(docProxyRegistry.listAllDocProxies()).thenReturn(
				Arrays.asList(
						new RT2DocProxy(new RT2CliendUidType("proxy1"), docUid1, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy1"), docUid1), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy2"), docUid2, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy2"), docUid2), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy3"), docUid3, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy3"), docUid3), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy4"), docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy4"), docUid4), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy5"), docUid5, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy5"), docUid5), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy6"), docUid6, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy6"), docUid6), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy7"), docUid7, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy7"), docUid7), sessionId),
						new RT2DocProxy(new RT2CliendUidType("proxy8"), docUid8, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(new RT2CliendUidType("proxy8"), docUid8), sessionId)
				)
		);

		objectUnderTest.run();
		objectUnderTest.run();

		Mockito.verify(docProxyRegistry, Mockito.times(5)).deregisterDocProxy(Mockito.any(), Mockito.eq(true));
	}
}
