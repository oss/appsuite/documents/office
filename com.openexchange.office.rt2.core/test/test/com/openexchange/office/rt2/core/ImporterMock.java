/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.IPartImporter;
import com.openexchange.session.Session;


public class ImporterMock implements IImporter, IPartImporter {

    private String globalOperations;
    private String acitveOperations;
    private String nextOperations;
    private DocumentProperties docProperties;

    public ImporterMock(String globalOperations) {
        this.globalOperations = globalOperations;
    }

    @Override
    public void initPartitioning(Session session, InputStream inputDocument, DocumentProperties documentProperties, boolean createFastLoadOperations) throws FilterException {
        // nothing to do
    }

    @Override
    public Map<String, Object> getMetaData(DocumentProperties documentProperties) throws FilterException {
        var metaData = new HashMap<String, Object>();
        try {
            metaData.put("operations", new JSONArray(globalOperations));
        } catch (JSONException e) {
            throw new FilterException("Bad json string", ErrorCode.CRITICAL_ERROR);
        }
        documentProperties.put(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX, 1);
        return metaData;
    }

    @Override
    public Map<String, Object> getActivePart(DocumentProperties documentProperties) throws FilterException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> getNextPart(DocumentProperties documentProperties) throws FilterException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public JSONObject createOperations(Session session, InputStream inputDocument, DocumentProperties documentProperties, boolean createFastLoadOperations) throws FilterException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InputStream getDefaultDocument(InputStream templateDocument, DocumentProperties documentProperties) throws FilterException {
        // TODO Auto-generated method stub
        return null;
    }

}
