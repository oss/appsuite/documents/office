package test.com.openexchange.office.rt2.core;

import java.util.Collection;
import java.util.HashSet;
import org.mockito.Mockito;
import com.openexchange.config.ConfigurationService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.tools.service.time.LocalDateTimeService;
import com.openexchange.timer.TimerService;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class DocProxyMessageLoggerServiceTestInformation extends RT2UnittestInformation {

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        var res =  new HashSet<Class<?>>();
        res.addAll(getMandantoryDependenciesOfDocProcessor());
        res.add(RT2DocProxyRegistry.class);
        res.add(TimerService.class);
        res.add(LocalDateTimeService.class);
        return res;
    }

    @Override
    protected void initMocks() {
        var cfgService = Mockito.mock(ConfigurationService.class);
        Mockito.when(cfgService.getBoolProperty(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(false);
    }

    @Override
    protected Collection<Object> getAdditionalServices() {
        Collection<Object> res = new HashSet<>();
        res.add(new RT2DocProxyStateHolderFactory());
        return res;
    }
}
