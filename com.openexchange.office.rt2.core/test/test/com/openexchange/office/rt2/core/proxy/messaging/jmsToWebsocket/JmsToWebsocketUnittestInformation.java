/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.jmsToWebsocket;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import org.mockito.Mockito;
import com.codahale.metrics.MetricRegistry;
import com.hazelcast.core.HazelcastInstance;
import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.RT2GarbageCollector;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.jms.RT2AdminJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.metric.DocProxyRequestMetricService;
import com.openexchange.office.rt2.core.metric.DocProxyResponseMetricService;
import com.openexchange.office.rt2.core.metric.WebsocketResponseMetricService;
import com.openexchange.office.rt2.core.proxy.MessageCounters;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyJmsConsumer;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.core.ws.RT2SessionCountValidator;
import com.openexchange.office.rt2.core.ws.RT2SessionValidator;
import com.openexchange.office.rt2.core.ws.RT2WSApp;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;

import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class JmsToWebsocketUnittestInformation extends RT2UnittestInformation {

	@Override
	protected Collection<Class<?>> getClassesToMock() {
		Collection<Class<?>> res = new HashSet<>();				
		
		res.add(CachingFacade.class);
		res.add(ClusterService.class);
		res.add(ClusterLockService.class);
		
		res.add(DistributedDocInfoMap.class);
		
		res.add(RT2ConfigService.class);
		
		res.add(RT2JmsMessageSender.class);
		
		res.add(MetricRegistry.class);
		
		res.add(RT2NodeInfoService.class);
		
		res.add(PooledConnectionFactoryProxy.class);
		
		res.add(HazelcastInstance.class);
		
		res.add(SessionService.class);
		res.add(SpecialLogService.class);
		res.add(RT2SessionValidator.class);
		res.add(RT2SessionCountValidator.class);
		
		res.add(RT2WSApp.class);
		res.add(RT2WSChannelDisposer.class);
		
		
		res.add(RT2DocOnNodeMap.class);
		res.add(RT2DocProcessorManager.class);
		res.add(RT2GarbageCollector.class);
		
		res.add(RT2AdminJmsConsumer.class);		
		
		res.add(DocProxyResponseMetricService.class);
		res.add(DocProxyRequestMetricService.class);
		res.add(DocProxyMessageLoggerService.class);
		res.add(MessageCounters.class);
		res.add(WebsocketResponseMetricService.class);
		
		return res;
	}

	@Override
	protected Collection<Class<?>> getClassesToInstantiateForUnitttest() {
		Collection<Class<?>> res = new HashSet<>();
		res.add(RT2DocProxyJmsConsumer.class);
		res.add(RT2DocProxyRegistry.class);
		res.add(RT2DocProxyStateHolderFactory.class);
		res.add(RT2WebSocketListener.class);
		return res;
	}

    @Override
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> mocks) {
        ClusterService clusterService = (ClusterService)mocks.get(ClusterService.class);
        if (clusterService != null) {
            Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(this.testOsgiBundleContextAndUnitTestActivator.getHazelcastNodeId().toString());
        }
    }
}
