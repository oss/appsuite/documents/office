/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import javax.jms.TextMessage;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.openexchange.office.rt2.core.config.RT2Const;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.value.RT2DocTypeType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.RT2MessageValidator;
import test.com.openexchange.office.rt2.core.util.TestConstants;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

public class JoinTest extends BaseTest {

	@Override
	protected void initServices() {
		this.docProcJmsCons = unitTestBundle.getService(RT2DocProcessorJmsConsumer.class);
		this.docProcMngr = unitTestBundle.getService(RT2DocProcessorManager.class);
		this.msgBackupAndAckProcessorService = unitTestBundle.getService(MsgBackupAndAckProcessorService.class);
		this.clientSequenceQueueDisposerService = unitTestBundle.getService(ClientSequenceQueueDisposerService.class);

		this.jmsMessageSender = unitTestBundle.getMock(RT2JmsMessageSender.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message joinRequest = TestRT2MessageCreator.createJoinMsg(clientId1, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()));
		joinRequest.setDocType(new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
		return joinRequest;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=ProcessorMessagingUnittestInformation.class)
	public void testSucc() throws Exception {
		RT2Message joinMsg = createTestMessage();

		TextMessage txtMsg = createAmqMessage(joinMsg);

		docProcJmsCons.onMessage(txtMsg);

		Thread.sleep(2000);

		assertEquals(1, docProcMngr.size());
		TextDocProcessor textDocProcessor = (TextDocProcessor) docProcMngr.getDocProcessors().iterator().next();
		this.msgBackupAndAckProcessorService.getMsgBackupAndACKProcessor(docUid1, true).get().isClientOnline(clientId1);
		assertNotNull(clientSequenceQueueDisposerService.getClientSequenceQueueForClient(docUid1, clientId1, false));
		assertTrue(textDocProcessor.getClientsStatusClone().hasClient(clientId1));

		ArgumentCaptor<RT2Message> rt2MsgCaptor = ArgumentCaptor.forClass(RT2Message.class);
		Mockito.verify(jmsMessageSender).sendToClientResponseTopic(rt2MsgCaptor.capture());
		List<RT2Message> messages = rt2MsgCaptor.getAllValues();
		assertEquals(1, messages.size());
		RT2Message jointResponse =
				TestRT2MessageCreator.createJoinResponseMsg(clientId1, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), new RT2FolderIdType(TestConstants.FOLDER_ID),
															new RT2FileIdType(TestConstants.FILE_ID), new RT2DocTypeType(RT2Const.DOCTYPE_TEXT));
		TestRT2MessageCreator.setMessagePostProcDefaultHeader(jointResponse, RT2MessageType.RESPONSE_JOIN);
		RT2MessageValidator.validate(jointResponse, messages.get(0));
	}
}
