/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.drive;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.List;
import org.json.JSONArray;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.office.rt2.core.doc.EditableDocProcessor;
import com.openexchange.office.rt2.core.drive.DeleteFileEventData;
import com.openexchange.office.rt2.core.drive.DeletedFilesTask;
import com.openexchange.office.rt2.core.drive.MoveFileEventData;
import com.openexchange.office.rt2.core.drive.MovedDeletedFilesAdminTaskProcessor;
import com.openexchange.office.rt2.core.drive.MovedFilesTask;
import com.openexchange.office.rt2.core.drive.RT2DocUidHelper;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.session.Session;
import com.openexchange.timer.ScheduledTimerTask;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;

public class MovedDeletedFilesAdminTaskProcessorTest {

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private MovedDeletedFilesAdminTaskProcessorTestInformation unittestInformation;

    @BeforeEach
    public void init() throws OXException {
        unittestInformation = new MovedDeletedFilesAdminTaskProcessorTestInformation();
        unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();
    }

    @Test
    public void testMovedDeletedFilesAdminTaskProcessor() throws Exception {
        MovedDeletedFilesAdminTaskProcessor taskProcessor = new MovedDeletedFilesAdminTaskProcessor();
        unitTestBundle.injectDependencies(taskProcessor);
        taskProcessor.afterPropertiesSet();

        List<String> infoList = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(infoList);
        assertEquals(0, infoList.size());

        TimerServiceMock timerServiceMock = unittestInformation.timerServiceMock;
        List<ScheduledTimerTask> timerTasks = timerServiceMock.getTimerTasks();
        assertNotNull(timerTasks);
        assertEquals(1, timerTasks.size());

        taskProcessor.destroy();

        timerTasks = timerServiceMock.getTimerTasks();
        assertNotNull(timerTasks);
        assertEquals(0, timerTasks.size());
    }

    @Test
    public void testMovedDeletedFilesAdminTaskProcessorMovedTaskWihtDocProcessorInstance() throws Exception {
        MovedDeletedFilesAdminTaskProcessor taskProcessor = new MovedDeletedFilesAdminTaskProcessor();
        unitTestBundle.injectDependencies(taskProcessor);
        taskProcessor.afterPropertiesSet();

        // prepare session
        String sessionId = "1234567890";
        int contextId = 1;
        int userId = 19;
        Session session = new SessionMock(sessionId, contextId, userId);
        unittestInformation.prepareSession(session);

        // prepare JSONArray with moved files data
        RT2DocUidHelper rt2DocUidHelper = RT2DocUidHelper.getInstance();
        String rtResourceId = "2000";
        String folderId = "1000";
        String fileId = folderId + "/" + rtResourceId;
        String docUidStr = rt2DocUidHelper.calcDocUid(session.getContextId(), new RT2FileIdType(rtResourceId));
        String toFolderId = "3000";
        String toFileId = toFolderId + "/" + rtResourceId;

        RT2DocUidType docUid = new RT2DocUidType(docUidStr);
        unittestInformation.setDocUid(docUid);
        EditableDocProcessor doc = unittestInformation.getDocProcessor();

        when(doc.getDocUID()).thenReturn(docUid);

        MoveFileEventData movedFileEventData = new MoveFileEventData(new RT2SessionIdType(sessionId), docUid, new RT2FileIdType(fileId), new RT2FolderIdType(folderId), new RT2FileIdType(toFileId), new RT2FolderIdType(toFolderId));
        JSONArray movedFilesTaskData = new JSONArray();
        movedFilesTaskData.put(movedFileEventData.toJSON());

        MovedFilesTask movedFilesTask = new MovedFilesTask(movedFilesTaskData);
        taskProcessor.addMoveDeleteFilesTask(movedFilesTask);

        // must wait until background thread processed task
        Thread.sleep(1000);

        List<String> tasksProcessed = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(tasksProcessed);
        assertEquals(1, tasksProcessed.size());
        String infoString = tasksProcessed.get(0);
        assertTrue(infoString.contains(docUidStr));
        assertTrue(infoString.contains(toFileId));
        assertTrue(infoString.contains("moved"));
        assertTrue(infoString.contains("=" + ErrorCode.NO_ERROR.getCode()));

        TimerServiceMock timerServiceMock = unittestInformation.timerServiceMock;
        List<ScheduledTimerTask> timerTasks = timerServiceMock.getTimerTasks();
        assertNotNull(timerTasks);
        assertEquals(1, timerTasks.size());
        ScheduledTimerTask timerTask = timerTasks.get(0);
        assertTrue(timerTask instanceof ScheduledTimerTaskMock);

        ScheduledTimerTaskMock timerTaskMock = (ScheduledTimerTaskMock)timerTask;
        timerTaskMock.triggerTask();
        tasksProcessed = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(tasksProcessed);
        assertEquals(0, tasksProcessed.size());

        verify(doc, times(1)).handleMovedFile(isA(RT2FolderIdType.class), isA(RT2FileIdType.class), isA(ErrorCode.class));
        verify(doc, times(1)).safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean());
    }

    @Test
    public void testMovedDeletedFilesAdminTaskProcessorDeletedTaskWihtDocProcessorInstance() throws Exception {
        MovedDeletedFilesAdminTaskProcessor taskProcessor = new MovedDeletedFilesAdminTaskProcessor();
        unitTestBundle.injectDependencies(taskProcessor);
        taskProcessor.afterPropertiesSet();

        // prepare session
        String sessionId = "1234567890";
        int contextId = 1;
        int userId = 19;
        Session session = new SessionMock(sessionId, contextId, userId);
        unittestInformation.prepareSession(session);

        // prepare JSONArray with moved files data
        RT2DocUidHelper rt2DocUidHelper = RT2DocUidHelper.getInstance();
        String rtResourceId = "2000";
        String folderId = "1000";
        String fileId = folderId + "/" + rtResourceId;
        String docUidStr = rt2DocUidHelper.calcDocUid(session.getContextId(), new RT2FileIdType(rtResourceId));

        RT2DocUidType docUid = new RT2DocUidType(docUidStr);
        unittestInformation.setDocUid(docUid);
        EditableDocProcessor doc = unittestInformation.getDocProcessor();

        when(doc.getDocUID()).thenReturn(docUid);

        DeleteFileEventData delFileEventData = new DeleteFileEventData(new RT2SessionIdType(sessionId), docUid, new RT2FileIdType(fileId));
        JSONArray delFilesTaskData = new JSONArray();
        delFilesTaskData.put(delFileEventData.toJSON());

        DeletedFilesTask delFilesTask = new DeletedFilesTask(delFilesTaskData);
        taskProcessor.addMoveDeleteFilesTask(delFilesTask);

        // must wait until background thread processed task
        Thread.sleep(1000);

        List<String> tasksProcessed = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(tasksProcessed);
        assertEquals(1, tasksProcessed.size());
        String infoString = tasksProcessed.get(0);
        assertTrue(infoString.contains(docUidStr));
        assertTrue(infoString.contains("deleted"));
        assertTrue(infoString.contains("=" + ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode()));

        verify(doc, times(1)).handleDeletedDocFile();
    }

    @Test
    public void testMovedDeletedFilesAdminTaskProcessorMovedTaskWihtDocProcessorInstanceSaveFailedWithError() throws Exception {
        MovedDeletedFilesAdminTaskProcessor taskProcessor = new MovedDeletedFilesAdminTaskProcessor();
        unitTestBundle.injectDependencies(taskProcessor);
        taskProcessor.afterPropertiesSet();

        // prepare session
        String sessionId = "1234567890";
        int contextId = 1;
        int userId = 19;
        Session session = new SessionMock(sessionId, contextId, userId);
        unittestInformation.prepareSession(session);

        // prepare JSONArray with moved files data
        RT2DocUidHelper rt2DocUidHelper = RT2DocUidHelper.getInstance();
        String rtResourceId = "2000";
        String folderId = "1000";
        String fileId = folderId + "/" + rtResourceId;
        String docUidStr = rt2DocUidHelper.calcDocUid(session.getContextId(), new RT2FileIdType(rtResourceId));
        String toFolderId = "3000";
        String toFileId = toFolderId + "/" + rtResourceId;

        RT2DocUidType docUid = new RT2DocUidType(docUidStr);
        unittestInformation.setDocUid(docUid);
        EditableDocProcessor doc = unittestInformation.getDocProcessor();

        when(doc.getDocUID()).thenReturn(docUid);
        try {
            when(doc.safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean())).thenAnswer(new Answer<ErrorCode>() {

                @Override
                public ErrorCode answer(InvocationOnMock invocation) throws Throwable {
                    return ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR;
                }
            });
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }

        MoveFileEventData movedFileEventData = new MoveFileEventData(new RT2SessionIdType(sessionId), docUid, new RT2FileIdType(fileId), new RT2FolderIdType(folderId), new RT2FileIdType(toFileId), new RT2FolderIdType(toFolderId));
        JSONArray movedFilesTaskData = new JSONArray();
        movedFilesTaskData.put(movedFileEventData.toJSON());

        MovedFilesTask movedFilesTask = new MovedFilesTask(movedFilesTaskData);
        taskProcessor.addMoveDeleteFilesTask(movedFilesTask);

        // must wait until background thread processed task
        Thread.sleep(1000);

        List<String> tasksProcessed = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(tasksProcessed);
        assertEquals(1, tasksProcessed.size());
        String infoString = tasksProcessed.get(0);
        assertTrue(infoString.contains(docUidStr));
        assertTrue(infoString.contains(toFileId));
        assertTrue(infoString.contains("moved"));
        assertTrue(infoString.contains("=" + ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR.getCode()));

        verify(doc, times(1)).handleMovedFile(isA(RT2FolderIdType.class), isA(RT2FileIdType.class), isA(ErrorCode.class));
        verify(doc, times(1)).safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean());
    }

    @Test
    public void testMovedDeletedFilesAdminTaskProcessorMovedTaskWihtDocProcessorInstanceSaveFailedWithException() throws Exception {
        MovedDeletedFilesAdminTaskProcessor taskProcessor = new MovedDeletedFilesAdminTaskProcessor();
        unitTestBundle.injectDependencies(taskProcessor);
        taskProcessor.afterPropertiesSet();

        // prepare session
        String sessionId = "1234567890";
        int contextId = 1;
        int userId = 19;
        Session session = new SessionMock(sessionId, contextId, userId);
        unittestInformation.prepareSession(session);

        // prepare JSONArray with moved files data
        RT2DocUidHelper rt2DocUidHelper = RT2DocUidHelper.getInstance();
        String rtResourceId = "2000";
        String folderId = "1000";
        String fileId = folderId + "/" + rtResourceId;
        String docUidStr = rt2DocUidHelper.calcDocUid(session.getContextId(), new RT2FileIdType(rtResourceId));
        String toFolderId = "3000";
        String toFileId = toFolderId + "/" + rtResourceId;

        RT2DocUidType docUid = new RT2DocUidType(docUidStr);
        unittestInformation.setDocUid(docUid);
        EditableDocProcessor doc = unittestInformation.getDocProcessor();

        when(doc.getDocUID()).thenReturn(docUid);
        when(doc.safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean())).thenAnswer(new Answer<ErrorCode>() {

            @Override
            public ErrorCode answer(InvocationOnMock invocation) throws Throwable {
                throw FolderExceptionErrorMessage.FOLDER_NOT_VISIBLE.create();
            }
        });

        MoveFileEventData movedFileEventData = new MoveFileEventData(new RT2SessionIdType(sessionId), docUid, new RT2FileIdType(fileId), new RT2FolderIdType(folderId), new RT2FileIdType(toFileId), new RT2FolderIdType(toFolderId));
        JSONArray movedFilesTaskData = new JSONArray();
        movedFilesTaskData.put(movedFileEventData.toJSON());

        MovedFilesTask movedFilesTask = new MovedFilesTask(movedFilesTaskData);
        taskProcessor.addMoveDeleteFilesTask(movedFilesTask);

        // must wait until background thread processed task
        Thread.sleep(1000);

        List<String> tasksProcessed = taskProcessor.getProcessedMovedDeletedTasks();
        assertNotNull(tasksProcessed);
        assertEquals(1, tasksProcessed.size());
        String infoString = tasksProcessed.get(0);
        assertTrue(infoString.contains(docUidStr));
        assertTrue(infoString.contains(toFileId));
        assertTrue(infoString.contains("moved"));
        assertTrue(infoString.contains("=" + ErrorCode.GENERAL_FOLDER_NOT_VISIBLE_ERROR.getCode()));

        verify(doc, times(1)).handleMovedFile(isA(RT2FolderIdType.class), isA(RT2FileIdType.class), isA(ErrorCode.class));
        verify(doc, times(1)).safteySaveDocumentDueToMove(isA(Session.class), isA(RT2FolderIdType.class), isA(RT2FileIdType.class), anyBoolean());
    }

}
