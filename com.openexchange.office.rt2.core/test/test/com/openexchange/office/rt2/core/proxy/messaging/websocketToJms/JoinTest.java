/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.proxy.messaging.websocketToJms;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.ws.RT2SessionValidator;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;
import test.com.openexchange.office.rt2.core.proxy.messaging.BaseTest;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;
import test.com.openexchange.office.rt2.core.util.UnittestMetadata;

@Disabled
public class JoinTest extends BaseTest {

	private DistributedAtomicLong atomicLong = Mockito.mock(DistributedAtomicLong.class);

	private RT2SessionValidator sessionValidator;
	private RT2NodeInfoService rt2NodeInfoService;
	private RT2WebSocketListener webSocketListener;
	private RT2WSChannelDisposer channelDisposer;
	private RT2DocProxyRegistry docProxyRegistry;

	@Override
	protected void initServices() {
		initRT2EnhDefWebSocket();

		sessionValidator = this.unitTestBundle.getMock(RT2SessionValidator.class);
		rt2NodeInfoService = this.unitTestBundle.getMock(RT2NodeInfoService.class);
		webSocketListener = unitTestBundle.getService(RT2WebSocketListener.class);
		channelDisposer = unitTestBundle.getService(RT2WSChannelDisposer.class);
		docProxyRegistry = unitTestBundle.getService(RT2DocProxyRegistry.class);
	}

	@Override
	protected RT2Message createTestMessage() {
		RT2Message joinMsg = TestRT2MessageCreator.createJoinMsg(clientId1, docUid1, new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()));
		return joinMsg;
	}

	@Override
	protected void setAdditionalFieldsInUnittestInformation() {
		((JoinUnittestInformation) unittestInformation).setAtomicLong(atomicLong);
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JoinUnittestInformation.class)
	public void testJoinFirst() throws Exception {
		Mockito.when(atomicLong.incrementAndGet()).thenReturn(1l);
		RT2Message joinMsg = testJoinBasics();
		JoinTestMessageValidator msgValidator =
				new JoinTestMessageValidator(unitTestBundle.getMock(RT2JmsMessageSender.class),
											 new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), joinMsg);
		msgValidator.validateMessages();
		verifyMocks(Mockito.times(1), Mockito.times(1));
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JoinUnittestInformation.class)
	public void testJoinWithNegativeRefCount() throws Exception {
		Mockito.when(atomicLong.incrementAndGet()).thenReturn(-1l, 1l);
		RT2Message joinMsg = testJoinBasics();
		JoinTestMessageValidator msgValidator = new JoinTestMessageValidator(unitTestBundle.getMock(RT2JmsMessageSender.class), new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), joinMsg);
		msgValidator.validateMessages();
		verifyMocks(Mockito.times(1), Mockito.times(2));
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JoinUnittestInformation.class)
	public void testJoinNotFirstJoin() throws Exception {
		Mockito.when(atomicLong.incrementAndGet()).thenReturn(2l);
		RT2Message joinMsg = testJoinBasics();
		JoinTestMessageValidator msgValidator = new JoinTestMessageValidator(unitTestBundle.getMock(RT2JmsMessageSender.class), new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId()), joinMsg);
		msgValidator.validateMessages();
		verifyMocks(Mockito.never(), Mockito.times(1));
	}

	protected void verifyMocks(VerificationMode docOnNodeVerificationMode, VerificationMode clusterLockVerificationMode) throws Exception {
		Mockito.verify(sessionValidator).validateSession(Mockito.any(), Mockito.any());

		Mockito.verify(rt2NodeInfoService, docOnNodeVerificationMode).registerDocOnNode(unittestInformation.getDocUid());
		Mockito.verify(unittestInformation.getClusterLock(), clusterLockVerificationMode).lock();
	}

	private RT2Message testJoinBasics() throws Exception {
		RT2Message joinMsg = createTestMessage();
		webSocketListener.onMessageSync(rt2EnhDefaultWebSocket, RT2MessageFactory.toJSONString(joinMsg));

		assertTrue(channelDisposer.getChannels().containsKey(clientId1));
		assertTrue(rt2EnhDefaultWebSocket.isAvaible());

		String rt2DocProxyId = RT2DocProxy.calcID(clientId1, docUid1);
		RT2DocProxy docProxy = docProxyRegistry.getDocProxy(rt2DocProxyId);
		assertNotNull(docProxy);

		return joinMsg;
	}

	@Test
	@UnittestMetadata(countClients=1, unittestInformationClass=JoinGettingNoLockUnittestInformation.class)
	public void testJoinGettingNoLock() throws Exception {
		testJoinBasics();
		Mockito.verify(unitTestBundle.getMock(RT2JmsMessageSender.class), never()).sendToDocumentQueue(Mockito.any(), Mockito.any());
		Mockito.verify(atomicLong, Mockito.never()).incrementAndGet();
		verifyMocks(Mockito.never(), Mockito.times(1));
	}

	public static class JoinGettingNoLockUnittestInformation extends JoinUnittestInformation {

		@Override
		public ClusterLock initClusterLock() throws ClusterLockException {
			ClusterLockService clusterLockService = testOsgiBundleContextAndUnitTestActivator.getMock(ClusterLockService.class);
			ClusterLock clusterLock = Mockito.mock(ClusterLock.class);
			setClusterLock(clusterLock);
			Mockito.when(clusterLock.lock()).thenReturn(false);
			Mockito.when(clusterLockService.getLock(docUid)).thenReturn(clusterLock);
			return clusterLock;
		}
	}
}
