/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.RT2DocProcessorClientExistsTester;
import com.openexchange.office.rt2.core.doc.DocProcessorClientInfo;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.TextDocProcessor;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyStateHolderFactory;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientInfo;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2NodeUuidType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.service.cluster.ClusterService;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;
import test.com.openexchange.office.rt2.core.util.RT2TestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.rt2.core.util.TestRT2MessageCreator;

public class RT2DocProcessorClientExistsTesterTest {

    @RegisterExtension
	public InUnitTestRule inUnitTestRule = new InUnitTestRule();

	private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;
	private RT2NodeUuidType nodeUuid;

	private RT2DocProcessorManager docProcMngrMock;
	private ClusterService clusterService;

	private DistributedDocInfoMap distributedDocInfoMap;

	@BeforeEach
	public void init() throws OXException {
		RT2DocProcessorClientExistsTesterTestInformation unittestInformation = new RT2DocProcessorClientExistsTesterTestInformation();
		unitTestBundle = new RT2TestOsgiBundleContextAndUnitTestActivator(unittestInformation);
		unitTestBundle.start();

		this.docProcMngrMock = unitTestBundle.getMock(RT2DocProcessorManager.class);
		this.clusterService = unitTestBundle.getMock(ClusterService.class);
		this.distributedDocInfoMap = unitTestBundle.getMock(DistributedDocInfoMap.class);
		Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(UUID.randomUUID().toString());
		this.nodeUuid = new RT2NodeUuidType(unitTestBundle.getHazelcastNodeId());
	}

	@Test
	public void testWithOnlyOneMember() throws OXException {

		RT2DocUidType docUid1 = new RT2DocUidType("docUid1");
		RT2CliendUidType client1 = new RT2CliendUidType("client1");
		RT2CliendUidType client11 = new RT2CliendUidType("client11");
		RT2DocUidType docUid2 = new RT2DocUidType("docUid2");
		RT2CliendUidType client2 = new RT2CliendUidType("client2");
		RT2DocUidType docUid3 = new RT2DocUidType("docUid3");
		RT2CliendUidType client3 = new RT2CliendUidType("client3");
		RT2DocUidType docUid4 = new RT2DocUidType("docUid4");
		RT2CliendUidType client4 = new RT2CliendUidType("client4");
		RT2CliendUidType client41 = new RT2CliendUidType("client41");
		RT2CliendUidType client42 = new RT2CliendUidType("client42");
		RT2DocUidType docUid5 = new RT2DocUidType("docUid5");
		RT2CliendUidType client5 = new RT2CliendUidType("client5");
		RT2DocUidType docUid6 = new RT2DocUidType("docUid6");
		RT2DocUidType docUid7 = new RT2DocUidType("docUid7");
		RT2DocUidType docUid8 = new RT2DocUidType("docUid8");
		Set<RT2DocUidType> allRegisteredDocUids = new HashSet<>(Arrays.asList(docUid1, docUid2, docUid3, docUid4, docUid5, docUid6, docUid7, docUid8));
		RT2DocProxyRegistry docProxyRegistry = unitTestBundle.getMock(RT2DocProxyRegistry.class);
		RT2DocProxyStateHolderFactory docProxyStateHolderFactory = unitTestBundle.getService(RT2DocProxyStateHolderFactory.class);
		RT2SessionIdType sessionId = new RT2SessionIdType("testSession");
		Mockito.when(docProxyRegistry.listAllDocProxies()).thenReturn(
				Arrays.asList(
						new RT2DocProxy(client3, docUid3, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client3, docUid3), sessionId),
						new RT2DocProxy(client4, docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client4, docUid4), sessionId),
						new RT2DocProxy(client42, docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client42, docUid4), sessionId),
						new RT2DocProxy(client5, docUid5, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client5, docUid5), sessionId)
				)
		);

		TextDocProcessor textDocProcessorDocUuid1 = null;
		TextDocProcessor textDocProcessorDocUuid2 = null;
		TextDocProcessor textDocProcessorDocUuid3 = null;
		TextDocProcessor textDocProcessorDocUuid4 = null;
		TextDocProcessor textDocProcessorDocUuid5 = null;
		try {
			textDocProcessorDocUuid1 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid1);
			textDocProcessorDocUuid1.setDocUID(docUid1);
			textDocProcessorDocUuid1.onJoin(TestRT2MessageCreator.createJoinMsg(client1, docUid1, nodeUuid));
			textDocProcessorDocUuid1.onJoin(TestRT2MessageCreator.createJoinMsg(client11, docUid1, nodeUuid));

			textDocProcessorDocUuid2 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid2);
			textDocProcessorDocUuid2.setDocUID(new RT2DocUidType(docUid2.toString()));
			textDocProcessorDocUuid2.onJoin(TestRT2MessageCreator.createJoinMsg(client2, docUid2, nodeUuid));

			textDocProcessorDocUuid3 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid3);
			textDocProcessorDocUuid3.setDocUID(new RT2DocUidType(docUid3.toString()));
			textDocProcessorDocUuid3.onJoin(TestRT2MessageCreator.createJoinMsg(client3, docUid3, nodeUuid));

			textDocProcessorDocUuid4 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid4);
			textDocProcessorDocUuid4.setDocUID(new RT2DocUidType(docUid4.toString()));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client4, docUid4, nodeUuid));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client41, docUid4, nodeUuid));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client42, docUid4, nodeUuid));

			textDocProcessorDocUuid5 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid5);
			textDocProcessorDocUuid5.setDocUID(new RT2DocUidType(docUid5.toString()));
			textDocProcessorDocUuid5.onJoin(TestRT2MessageCreator.createJoinMsg(client5, docUid5, nodeUuid));
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
		Mockito.when(docProcMngrMock.getDocProcessors()).thenReturn(new HashSet<>(Arrays.asList(textDocProcessorDocUuid1, textDocProcessorDocUuid2, textDocProcessorDocUuid3, textDocProcessorDocUuid4, textDocProcessorDocUuid5)));
		Mockito.when(clusterService.getCountMembers()).thenReturn(1);

		Mockito.when(distributedDocInfoMap.getAllRegisteredDocIds()).thenReturn(allRegisteredDocUids);

		RT2DocProcessorClientExistsTester objectUnderTest = new RT2DocProcessorClientExistsTester();
		unitTestBundle.injectDependencies(objectUnderTest);
		objectUnderTest.run();

		assertTrue(textDocProcessorDocUuid1.getClientsInfo().isEmpty(), "Clients of textDocProcessorDocUuid1 have to be empty");
		assertTrue(textDocProcessorDocUuid2.getClientsInfo().isEmpty(), "Clients of textDocProcessorDocUuid2 have to be empty");
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client3, null)).toArray(), textDocProcessorDocUuid3.getClientsInfo().toArray());
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client4, null), new DocProcessorClientInfo(client42, null)).toArray(), textDocProcessorDocUuid4.getClientsInfo().toArray());
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client5, null)).toArray(), textDocProcessorDocUuid5.getClientsInfo().toArray());
		List<RT2DocUidType> sollMarkedAtomiceForDelete = Arrays.asList(docUid1, docUid2, docUid6, docUid7, docUid8);
		List<RT2DocUidType> markedAtomicForDelete = new ArrayList<>(objectUnderTest.getMarkedAtomicsForDelete());
		Collections.sort(sollMarkedAtomiceForDelete, (v1, v2) -> v1.getValue().compareTo(v2.getValue()));
		Collections.sort(markedAtomicForDelete, (v1, v2) -> v1.getValue().compareTo(v2.getValue()));
		assertArrayEquals(sollMarkedAtomiceForDelete.toArray(), markedAtomicForDelete.toArray());
	}

	@Test
	public void testWithMultipleMembers() throws OXException {

		RT2DocUidType docUid1 = new RT2DocUidType("docUid1");
		RT2CliendUidType client1 = new RT2CliendUidType("client1");
		RT2CliendUidType client11 = new RT2CliendUidType("client11");
		RT2DocUidType docUid2 = new RT2DocUidType("docUid2");
		RT2CliendUidType client2 = new RT2CliendUidType("client2");
		RT2DocUidType docUid3 = new RT2DocUidType("docUid3");
		RT2CliendUidType client3 = new RT2CliendUidType("client3");
		RT2DocUidType docUid4 = new RT2DocUidType("docUid4");
		RT2CliendUidType client4 = new RT2CliendUidType("client4");
		RT2CliendUidType client41 = new RT2CliendUidType("client41");
		RT2CliendUidType client42 = new RT2CliendUidType("client42");
		RT2DocUidType docUid5 = new RT2DocUidType("docUid5");
		RT2CliendUidType client5 = new RT2CliendUidType("client5");
		RT2DocProxyRegistry docProxyRegistry = unitTestBundle.getMock(RT2DocProxyRegistry.class);
		RT2DocProxyStateHolderFactory docProxyStateHolderFactory = unitTestBundle.getService(RT2DocProxyStateHolderFactory.class);
		RT2SessionIdType sessionId = new RT2SessionIdType("testSessionId");
		Mockito.when(docProxyRegistry.listAllDocProxies()).thenReturn(
				Arrays.asList(
						new RT2DocProxy(client42, docUid4, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client42, docUid4), sessionId),
						new RT2DocProxy(client5, docUid5, new RT2ChannelId(""), docProxyStateHolderFactory.createDocProxyStateHolder(client5, docUid5), sessionId)
				)
		);

		TextDocProcessor textDocProcessorDocUuid1 = null;
		TextDocProcessor textDocProcessorDocUuid2 = null;
		TextDocProcessor textDocProcessorDocUuid3 = null;
		TextDocProcessor textDocProcessorDocUuid4 = null;
		TextDocProcessor textDocProcessorDocUuid5 = null;
		try {
			textDocProcessorDocUuid1 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid1);
			textDocProcessorDocUuid1.setDocUID(docUid1);
			textDocProcessorDocUuid1.onJoin(TestRT2MessageCreator.createJoinMsg(client1, docUid1, nodeUuid));
			textDocProcessorDocUuid1.onJoin(TestRT2MessageCreator.createJoinMsg(client11, docUid1, nodeUuid));

			textDocProcessorDocUuid2 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid2);
			textDocProcessorDocUuid2.setDocUID(new RT2DocUidType(docUid2.toString()));
			textDocProcessorDocUuid2.onJoin(TestRT2MessageCreator.createJoinMsg(client2, docUid2, nodeUuid));

			textDocProcessorDocUuid3 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid3);
			textDocProcessorDocUuid3.setDocUID(new RT2DocUidType(docUid3.toString()));
			textDocProcessorDocUuid3.onJoin(TestRT2MessageCreator.createJoinMsg(client3, docUid3, nodeUuid));

			textDocProcessorDocUuid4 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid4);
			textDocProcessorDocUuid4.setDocUID(new RT2DocUidType(docUid4.toString()));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client4, docUid4, nodeUuid));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client41, docUid4, nodeUuid));
			textDocProcessorDocUuid4.onJoin(TestRT2MessageCreator.createJoinMsg(client42, docUid4, nodeUuid));

			textDocProcessorDocUuid5 = new TextDocProcessor();
			unitTestBundle.injectDependencies(textDocProcessorDocUuid5);
			textDocProcessorDocUuid5.setDocUID(new RT2DocUidType(docUid5.toString()));
			textDocProcessorDocUuid5.onJoin(TestRT2MessageCreator.createJoinMsg(client5, docUid5, nodeUuid));
		} catch (Exception e) {
			fail(e.getMessage());
		}
		Mockito.when(docProcMngrMock.getDocProcessors()).thenReturn(new HashSet<>(Arrays.asList(textDocProcessorDocUuid1, textDocProcessorDocUuid2, textDocProcessorDocUuid3, textDocProcessorDocUuid4, textDocProcessorDocUuid5)));
		Mockito.when(clusterService.getCountMembers()).thenReturn(2);

		RT2DocProcessorClientExistsTester objectUnderTest = new RT2DocProcessorClientExistsTester();
		unitTestBundle.injectDependencies(objectUnderTest);
		objectUnderTest.run();

		Collection<ClientInfo> clientInfos = Arrays.asList(ClientInfo.newBuilder().setClientUid(ClientUidType.newBuilder().setValue(client3.getValue())).setDocUid(DocUidType.newBuilder().setValue(docUid3.getValue())).build(),
														   ClientInfo.newBuilder().setClientUid(ClientUidType.newBuilder().setValue(client4.getValue())).setDocUid(DocUidType.newBuilder().setValue(docUid4.getValue())).build());
		objectUnderTest.processUpdateCurrClientListOfDocProcResponse(UUID.randomUUID(), System.currentTimeMillis(), clientInfos);

		assertTrue(textDocProcessorDocUuid1.getClientsInfo().isEmpty(), "Clients of textDocProcessorDocUuid1 have to be empty");
		assertTrue(textDocProcessorDocUuid2.getClientsInfo().isEmpty(), "Clients of textDocProcessorDocUuid2 have to be empty");
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client3, null)).toArray(), textDocProcessorDocUuid3.getClientsInfo().toArray());
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client4, null), new DocProcessorClientInfo(client42, null)).toArray(), textDocProcessorDocUuid4.getClientsInfo().toArray());
		assertArrayEquals(Arrays.asList(new DocProcessorClientInfo(client5, null)).toArray(), textDocProcessorDocUuid5.getClientsInfo().toArray());
	}

	@Test
	public void testTwiceTimes() {
		RT2DocProcessorClientExistsTester objectUnderTest = new RT2DocProcessorClientExistsTester();
		unitTestBundle.injectDependencies(objectUnderTest);
		objectUnderTest.run();
		long startTime = objectUnderTest.getCurrentlyRunning();
		objectUnderTest.run();
		assertEquals(startTime, objectUnderTest.getCurrentlyRunning());
	}
}
