/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.processor.messaging;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import org.mockito.Mockito;
import com.codahale.metrics.MetricRegistry;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.group.GroupService;
import com.openexchange.jslob.registry.JSlobServiceRegistry;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.document.api.BackupFileService;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.tools.EmptyDocumentCache;
import com.openexchange.office.htmldoc.GenericHtmlDocumentBuilder;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rt2.core.DocProcessorMessageLoggerService;
import com.openexchange.office.rt2.core.DocProxyMessageLoggerService;
import com.openexchange.office.rt2.core.RT2MessageSender;
import com.openexchange.office.rt2.core.RT2NodeInfoService;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.doc.AdvisoryLockHelperService;
import com.openexchange.office.rt2.core.doc.ClientEventService;
import com.openexchange.office.rt2.core.doc.DocProcessorEventService;
import com.openexchange.office.rt2.core.doc.DocumentEventHelper;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorFactory;
import com.openexchange.office.rt2.core.doc.RT2DocProcessorManager;
import com.openexchange.office.rt2.core.doc.RT2DocStateNotifier;
import com.openexchange.office.rt2.core.doc.RT2MessageTypeDispatcher;
import com.openexchange.office.rt2.core.jms.RT2DocProcessorJmsConsumer;
import com.openexchange.office.rt2.core.jms.RT2JmsMessageSender;
import com.openexchange.office.rt2.core.metric.DocProcessorMetricService;
import com.openexchange.office.rt2.core.sequence.ClientLeftRemovedService;
import com.openexchange.office.rt2.core.sequence.ClientSequenceQueueDisposerService;
import com.openexchange.office.rt2.core.sequence.MsgBackupAndAckProcessorService;
import com.openexchange.office.rt2.core.sequence.QueueProcessorDisposer;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.office.tools.service.session.SessionService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;

import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;

public class ProcessorMessagingUnittestInformation extends RT2UnittestInformation {
	
	@Override
	protected Collection<Class<?>> getClassesToMock() {
		Collection<Class<?>> res = new HashSet<>();
		
		res.add(AdvisoryLockInfoService.class);
		res.add(AdvisoryLockHelperService.class);
		
		res.add(AdvisoryLockHelperService.class);
		res.add(AdvisoryLockInfoService.class);		
		
		res.add(ClusterLockService.class);
		res.add(ClusterService.class);
		
		// Backup
		res.add(DocumentBackupController.class);
		res.add(BackupFileService.class);
		res.add(IRestoreDocument.class);
		res.add(JmsTemplateWithoutTtl.class);
		
		// RecentFileList
		res.add(RecentFileListManagerFactory.class);
		
		// Defaults of Office
		res.add(ClientLeftRemovedService.class);
		
		res.add(DistributedDocInfoMap.class);
		res.add(DocProcessorMetricService.class);		
		
		res.add(GenericHtmlDocumentBuilder.class);
		res.add(GroupService.class);
		
		res.add(DocProxyMessageLoggerService.class);
		res.add(DocProcessorMessageLoggerService.class);
		res.add(IResourceManagerFactory.class);
		
		res.add(MetricRegistry.class);
		
		res.add(RT2ConfigService.class);	
		res.add(RT2JmsMessageSender.class);
		res.add(RT2NodeInfoService.class);
		
		res.add(PooledConnectionFactoryProxy.class);
		
		res.add(SessionService.class);
		
		res.add(SpecialLogService.class);
		
		res.add(Statistics.class);
		
		// Defaults of Backend
		res.add(IDBasedFileAccessFactory.class);
		
		res.add(JSlobServiceRegistry.class);
		
		res.add(ManagedFileManagement.class);
		
		
		// Unittest specific
		
		return res;
	}
	
	@Override
	protected Collection<Class<?>> getClassesToInstantiateForUnitttest() {
		Collection<Class<?>> res = new HashSet<>();
				
		res.add(ClientEventService.class);
		res.add(ClientSequenceQueueDisposerService.class);
		
		res.add(DocFileServiceFactory.class);
		res.add(DocProcessorEventService.class);
		res.add(DocumentDisposer.class);
		res.add(DocumentEventHelper.class);
		
		res.add(EmptyDocumentCache.class);
		
		res.add(FileHelperServiceFactory.class);
		res.add(FolderHelperServiceFactory.class);
		
		res.add(ImExporterAccessFactory.class);
		
		res.add(MsgBackupAndAckProcessorService.class);
		
		res.add(OXDocumentFactory.class);
		
		res.add(QueueProcessorDisposer.class);
		
		res.add(RT2DocProcessorJmsConsumer.class);
		res.add(RT2DocProcessorFactory.class);
		res.add(RT2DocProcessorManager.class);
		res.add(RT2DocStateNotifier.class);
		res.add(RT2MessageSender.class);
		res.add(RT2MessageTypeDispatcher.class);
		
		res.add(StorageHelperServiceFactory.class);
		
		res.add(UserConfigurationFactory.class);
		
		return res;
	}

	@Override
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> mocks) {
        ClusterService clusterService = (ClusterService)mocks.get(ClusterService.class);
        if (clusterService != null) {
            Mockito.when(clusterService.getLocalMemberUuid()).thenReturn(this.testOsgiBundleContextAndUnitTestActivator.getHazelcastNodeId().toString());
        }
    }
}
