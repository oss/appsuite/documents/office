/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.control.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.Validate;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.control.impl.MasterCleanupTask;

public class MasterCleanupTaskTest {

	//-------------------------------------------------------------------------
	final static long ACCEPTED_TIME_DIFF = 1000;

	//-------------------------------------------------------------------------
	private <T> Set<T> asSet(final List<T> aList)
	{
		Validate.notNull(aList);

		Set<T> aSet = new HashSet<>();
		for (final T t : aList) {
            aSet.add(t);
        }

		return aSet;
	}

	//-------------------------------------------------------------------------
	private boolean isInTimeRange(long nTime1, long nTime2, long nAcceptedDiff)
	{
		final long nDiff = Math.abs(nTime1 - nTime2);
		return (nDiff <= nAcceptedDiff);
	}

	//-------------------------------------------------------------------------
	@Test
	public void test() {
		final String sTaskID = UUID.randomUUID().toString();
		final String sNodeUUID = UUID.randomUUID().toString();
		final String scleanupUUID = UUID.randomUUID().toString();
		final List<String> aUUIDList = Arrays.asList( UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
		final Set<String> aHealthyMemberSet = asSet(aUUIDList);

		final long nNow = System.currentTimeMillis();
		final MasterCleanupTask aMasterCleanupTask = new MasterCleanupTask(sTaskID, sNodeUUID, scleanupUUID, aHealthyMemberSet);

		assertNotNull(aMasterCleanupTask);
		assertEquals(sTaskID, aMasterCleanupTask.getTaskID());
		assertEquals(sNodeUUID, aMasterCleanupTask.getMemberUUID());
		assertEquals(scleanupUUID, aMasterCleanupTask.getMemberUUIDToCleanup());

		final long nCreationTime = aMasterCleanupTask.getCreationTime();
		assertTrue(isInTimeRange(nCreationTime, nNow, ACCEPTED_TIME_DIFF));

		assertFalse(aMasterCleanupTask.isCompleted());

		final Set<String> aResultSet = aMasterCleanupTask.getHealthMembersUUID();
		for (final String s : aUUIDList) {
            assertTrue(aResultSet.contains(s));
        }

		aMasterCleanupTask.setMemberToCompleted(UUID.randomUUID().toString());
		assertFalse(aMasterCleanupTask.isCompleted());

		for (final String s : aUUIDList) {
            aMasterCleanupTask.setMemberToCompleted(s);
        }
		assertTrue(aMasterCleanupTask.isCompleted());

		final String sToString = aMasterCleanupTask.toString();
		assertTrue(sToString.indexOf(sTaskID) >= 0);
		assertTrue(sToString.indexOf(sNodeUUID) >= 0);
		assertTrue(sToString.indexOf(scleanupUUID) >= 0);
	}

}
