/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.sequence;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.core.sequence.ClientState;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.tools.common.TimeStampUtils;

public class ClientStateTest {

	@Test
	public void testClientState() throws Exception {
		final RT2CliendUidType clientUid = new RT2CliendUidType(UUID.randomUUID().toString());
		final RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
		final ClientState clientState = new ClientState(clientUid, true);

		// check init state
        long lastAckMsgReceived = clientState.getLastAckMsgReceived();
        long onOfflineTimeStamp = clientState.onlineSince();
        long lastMsgReceived = clientState.getLastMsgReceived();
		long oldestMsgTimeStamp = clientState.getOldestBackupMessageTimeStamp();

        long now = System.currentTimeMillis();
		assertTrue(clientState.isOnline());
		assertTrue(lastAckMsgReceived == 0);
		assertTrue((onOfflineTimeStamp > 0) && (lastAckMsgReceived <= now));
		assertTrue(lastMsgReceived == TimeStampUtils.NO_TIMESTAMP);
		assertTrue(oldestMsgTimeStamp == TimeStampUtils.NO_TIMESTAMP);

		long offlineSince = clientState.offlineSince();
		long onlineSince = clientState.onlineSince();
		assertTrue(offlineSince == TimeStampUtils.NO_TIMESTAMP);
		assertTrue(onlineSince > 0);

		Set<Integer> ackSet = clientState.getAckSet();
		int count = clientState.getMsgBackupSize();
		assertTrue(ackSet.isEmpty());
		assertTrue(count == 0);
		Set<RT2Message> backupMsgs = clientState.getBackupMsgs();
		assertTrue(backupMsgs.isEmpty());

		// check on-/offline state
		clientState.setOnlineState(false);
		assertFalse(clientState.isOnline());
		try {Thread.sleep(100); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }

		offlineSince = clientState.offlineSince();
		onlineSince = clientState.onlineSince();
		assertTrue(offlineSince >= 100);
		assertTrue(onlineSince == TimeStampUtils.NO_TIMESTAMP);

		clientState.setOnlineState(true);
		offlineSince = clientState.offlineSince();
		onlineSince = clientState.onlineSince();
		assertTrue(clientState.isOnline());
		assertTrue(onlineSince > 0);
		assertTrue(offlineSince == TimeStampUtils.NO_TIMESTAMP);

		clientState.setOnlineState(true);
		assertTrue(clientState.isOnline());

		// check last msg received
        now = System.currentTimeMillis();
		clientState.updateLastMsgReceived();
        lastMsgReceived = clientState.getLastMsgReceived();
		assertTrue(lastMsgReceived >= now);

		// check last ack msg received
        now = System.currentTimeMillis();
		clientState.updateLastAckMsgReceived();
		lastAckMsgReceived = clientState.getLastAckMsgReceived();
		assertTrue(lastAckMsgReceived >= now);

		ackSet = clientState.getAckSet();
	    count = clientState.getMsgBackupSize();
		assertTrue(ackSet.isEmpty());
		assertTrue(count == 0);

		// check backup msgs
        now = System.currentTimeMillis();
		RT2Message msg1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg1.setSeqNumber(new RT2SeqNumberType(1));
		clientState.backupMessage(msg1);

		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertFalse(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);
		oldestMsgTimeStamp = clientState.getOldestBackupMessageTimeStamp();
		assertTrue(oldestMsgTimeStamp >= now);

		List<RT2Message> reqMsgs = clientState.getRequestedMessages(Arrays.asList(1));
		assertFalse(reqMsgs.isEmpty());
		assertTrue(reqMsgs.size() == 1);

		RT2Message msg2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg2.setSeqNumber(new RT2SeqNumberType(2));
		clientState.backupMessage(msg2);

		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertFalse(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);
		long currentOldestMsgTimeStamp = clientState.getOldestBackupMessageTimeStamp();
		assertTrue(currentOldestMsgTimeStamp == oldestMsgTimeStamp);

		// check ack state
		Collection<Integer> ackCollection = new ArrayList<>(Arrays.asList(1, 2));
		ackSet = clientState.getAckSet();
		assertTrue(ackSet.isEmpty());

		clientState.addReceivedSeqNrCol(ackCollection);
		ackSet = clientState.getAckSet();
		assertFalse(ackSet.isEmpty());
		assertTrue(ackSet.size() == 2);
		// get ack set removes the stored ack (seq-nrs)!
		ackSet = clientState.getAckSet();
		assertTrue(ackSet.isEmpty());

		// remove messages
		clientState.removeStoredMessages(Arrays.asList(1,2));
		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertTrue(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);

		// 2nd try with backup & get msgs
		msg1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg1.setSeqNumber(new RT2SeqNumberType(1));
		clientState.backupMessage(msg1);

		msg2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg2.setSeqNumber(new RT2SeqNumberType(2));
		clientState.backupMessage(msg2);

		RT2Message msg4 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg4.setSeqNumber(new RT2SeqNumberType(4));
		clientState.backupMessage(msg4);

		RT2Message msg3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg3.setSeqNumber(new RT2SeqNumberType(3));
		clientState.backupMessage(msg3);

        reqMsgs = clientState.getRequestedMessages(Arrays.asList(1,2));
		assertFalse(reqMsgs.isEmpty());
		assertTrue(reqMsgs.size() == 2);
		assertTrue(reqMsgs.get(0).getSeqNumber().getValue() == 1);
		assertTrue(reqMsgs.get(1).getSeqNumber().getValue() == 2);

        reqMsgs = clientState.getRequestedMessages(Arrays.asList(2,4));
		assertFalse(reqMsgs.isEmpty());
		assertTrue(reqMsgs.size() == 2);
		assertTrue(reqMsgs.get(0).getSeqNumber().getValue() == 2);
		assertTrue(reqMsgs.get(1).getSeqNumber().getValue() == 4);

		clientState.removeStoredMessages(1, 3);
		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertFalse(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);

        reqMsgs = clientState.getRequestedMessages(Arrays.asList(4, 8));
		assertTrue(reqMsgs.isEmpty());

		clientState.removeStoredMessages(4, 4);
        reqMsgs = clientState.getRequestedMessages(Arrays.asList(1,4));
		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertTrue(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);

		// 2nd try with backup & get msgs
		msg1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg1.setSeqNumber(new RT2SeqNumberType(1));
		clientState.backupMessage(msg1);

		msg1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg1.setSeqNumber(new RT2SeqNumberType(1));
		clientState.backupMessage(msg1);
		reqMsgs = clientState.getRequestedMessages(Arrays.asList(1));
		assertFalse(reqMsgs.isEmpty());
		assertTrue(reqMsgs.size() == 1);
		assertTrue(msg1 == reqMsgs.get(0));

		clientState.removeStoredMessages(1, 1);

		// check get oldest msg again
		long before = System.currentTimeMillis();
		msg3 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg3.setSeqNumber(new RT2SeqNumberType(3));
		clientState.backupMessage(msg3);
        now = System.currentTimeMillis();
		try {Thread.sleep(5); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }

		msg1 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg1.setSeqNumber(new RT2SeqNumberType(1));
		clientState.backupMessage(msg1);
		try {Thread.sleep(5); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }

		msg4 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg4.setSeqNumber(new RT2SeqNumberType(4));
		clientState.backupMessage(msg4);
		try {Thread.sleep(5); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }

		msg2 = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, clientUid, docUid);
		msg2.setSeqNumber(new RT2SeqNumberType(2));
		clientState.backupMessage(msg2);

		currentOldestMsgTimeStamp = clientState.getOldestBackupMessageTimeStamp();
		assertTrue((before <= currentOldestMsgTimeStamp) && (now >= currentOldestMsgTimeStamp));

		clientState.removeStoredMessages(1, 10);
		backupMsgs = clientState.getBackupMsgs();
		count = clientState.getMsgBackupSize();
		assertTrue(backupMsgs.isEmpty());
		assertTrue(backupMsgs.size() == count);
	}

}
