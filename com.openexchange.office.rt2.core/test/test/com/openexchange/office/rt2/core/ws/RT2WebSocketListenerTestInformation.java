/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.ws;

import java.util.Collection;
import java.util.HashSet;
import com.openexchange.office.rt2.core.config.RT2ConfigService;
import com.openexchange.office.rt2.core.metric.DocProxyRequestMetricService;
import com.openexchange.office.rt2.core.metric.DocProxyResponseMetricService;
import com.openexchange.office.rt2.core.metric.WebsocketResponseMetricService;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.ws.RT2SessionCountValidator;
import com.openexchange.office.rt2.core.ws.RT2SessionValidator;
import com.openexchange.office.rt2.core.ws.RT2WSApp;
import com.openexchange.office.rt2.core.ws.RT2WSChannelDisposer;
import com.openexchange.office.tools.service.cluster.ClusterService;
import test.com.openexchange.office.rt2.core.util.RT2UnittestInformation;


public class RT2WebSocketListenerTestInformation extends RT2UnittestInformation {

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res = new HashSet<>();
        res.add(RT2SessionValidator.class);
        res.add(RT2SessionCountValidator.class);
        res.add(RT2WSChannelDisposer.class);
        res.add(RT2DocProxyRegistry.class);
        res.add(RT2ConfigService.class);
        res.add(WebsocketResponseMetricService.class);
        res.add(DocProxyRequestMetricService.class);
        res.add(DocProxyResponseMetricService.class);
        res.add(RT2WSApp.class);
        res.add(ClusterService.class);
        return res;
    }

}
