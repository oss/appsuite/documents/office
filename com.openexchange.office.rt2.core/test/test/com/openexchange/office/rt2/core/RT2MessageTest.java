/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.UUID;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;

//=============================================================================
public class RT2MessageTest
{
    //-------------------------------------------------------------------------
    @Test
    public void testDecodeFromJSON()
        throws Exception
    {
        final RT2Protocol aProtocol = RT2Protocol.get ();
        final String      MSG_ID    = "msg-4711";
        final String      sJSON     =
            "{\n" +
            "   t   : '" + aProtocol.encode(RT2Protocol.REQUEST_JOIN) + "',\n" +
            "   h   :\n" +
            "          {\n" +
            "             " + aProtocol.encode(RT2Protocol.HEADER_MSG_ID) + ": '" + MSG_ID + "'\n" +
            "          }\n" +
            "}\n" ;

        final RT2Message aMsg = RT2MessageFactory.fromJSONString(sJSON);

        assertEquals(RT2MessageType.REQUEST_JOIN, aMsg.getType(), "testDecodeFromJSON [01] decode of type didnt work");
        assertEquals(MSG_ID, aMsg.getHeader(RT2Protocol.HEADER_MSG_ID), "testDecodeFromJSON [02] decode of header didnt work");
    }

    //-------------------------------------------------------------------------
    @Test
    public void testEncodeToJSON()
        throws Exception
    {
        final RT2Protocol aProtocol = RT2Protocol.get ();
        final String      MSG_ID    = "msg-5812";
        final RT2Message  aMsg      = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_LEAVE, new RT2CliendUidType(UUID.randomUUID().toString()), new RT2DocUidType(UUID.randomUUID().toString()));

        aMsg.setHeader (RT2Protocol.HEADER_MSG_ID, MSG_ID);

        final JSONObject aJSON = RT2MessageFactory.toJSON(aMsg);

        assertEquals(aProtocol.encode(RT2Protocol.REQUEST_LEAVE), aJSON.getString("t"), "testEncodeToJSON [01] encode of type didnt work");
        assertEquals(MSG_ID, aJSON.getJSONObject("h").get(aProtocol.encode(RT2Protocol.HEADER_MSG_ID)), "testEncodeToJSON [02] encode of header didnt work");
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGenericErrorMessage()
        throws Exception
    {
        final RT2Protocol aProtocol      = RT2Protocol.get ();
        final String      ClIENT_UID     = UUID.randomUUID().toString();
        final String      DOC_UID        = UUID.randomUUID().toString();
        final String      REQ_MSG_ID     = UUID.randomUUID().toString();
        final String      SESSION_ID     = UUID.randomUUID().toString();
        final Integer     SEQ_NR         = 10;
        final Integer     CLEARED_SEQ_NR = 0;
        final RT2Message  aReqMsg        = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_APPLY_OPS, new RT2CliendUidType(ClIENT_UID), new RT2DocUidType(DOC_UID));

        // initialize apply ops message
        aReqMsg.setHeader (RT2Protocol.HEADER_MSG_ID    , REQ_MSG_ID);
        aReqMsg.setHeader (RT2Protocol.HEADER_SEQ_NR    , SEQ_NR);
        aReqMsg.setHeader (RT2Protocol.HEADER_CLIENT_UID, ClIENT_UID);
        aReqMsg.setHeader (RT2Protocol.HEADER_DOC_UID   , DOC_UID);
        aReqMsg.setHeader (RT2Protocol.HEADER_SESSION_ID, SESSION_ID);

        String  sReqMsgID  = aReqMsg.getHeader(RT2Protocol.HEADER_MSG_ID);
        Integer aSeqNr     = aReqMsg.getHeader(RT2Protocol.HEADER_SEQ_NR);
        String  sClientUID = aReqMsg.getHeader(RT2Protocol.HEADER_CLIENT_UID);
        String  sDocUID    = aReqMsg.getHeader(RT2Protocol.HEADER_DOC_UID);
        String  sSessionID = aReqMsg.getHeader(RT2Protocol.HEADER_SESSION_ID);

        assertEquals(sReqMsgID , REQ_MSG_ID);
        assertEquals(aSeqNr    , SEQ_NR    );
        assertEquals(sClientUID, ClIENT_UID);
        assertEquals(sDocUID   , DOC_UID   );
        assertEquals(sSessionID, SESSION_ID);

        final RT2Message aErrorResponse = RT2MessageFactory.createResponseFromMessage(aReqMsg, RT2MessageType.RESPONSE_GENERIC_ERROR);

        sReqMsgID  = aErrorResponse.getHeader(RT2Protocol.HEADER_MSG_ID);
        aSeqNr     = aErrorResponse.getHeader(RT2Protocol.HEADER_SEQ_NR);
        sClientUID = aErrorResponse.getHeader(RT2Protocol.HEADER_CLIENT_UID);
        sDocUID    = aErrorResponse.getHeader(RT2Protocol.HEADER_DOC_UID);

        assertEquals(sReqMsgID , REQ_MSG_ID);
        assertEquals(aSeqNr    , SEQ_NR    );
        assertEquals(sClientUID, ClIENT_UID);
        assertEquals(sDocUID   , DOC_UID   );

        RT2MessageGetSet.clearSeqNumber(aErrorResponse);
        aSeqNr = aErrorResponse.getHeader(RT2Protocol.HEADER_SEQ_NR);
        assertEquals(CLEARED_SEQ_NR, aSeqNr);
    }

}
