/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.core.util;

public class TestConstants {
	
	public static final String PRAEFIX = "test";
	public static final String SESSION_ID = PRAEFIX + "SessionId";
	public static final String GIVEN_NAME = PRAEFIX + "Givenname";
	public static final String SUR_NAME = PRAEFIX + "Surname";
	public static final String FOLDER_ID = "413";
	public static final String FILE_ID = "413/26978";
	public static final String DOCX_TESTFILENAME = "simpleTestFile.docx";
	public static final String DOCX_TESTFILEPATH = "files/" + DOCX_TESTFILENAME;
	public static final String XLSX_TESTFILENAME = "simpleTestFile.xlsx";
	public static final String XLSX_TESTFILEPATH = "files/" + XLSX_TESTFILENAME;
	public static final String PPTX_TESTFILENAME = "simpleTestFile.pptx";
	public static final String PPTX_TESTFILEPATH = "files/" + PPTX_TESTFILENAME;
	public static final String ODT_TESTFILENAME = "simpleTestFile.odt";
	public static final String ODT_TESTFILEPATH = "files/" + ODT_TESTFILENAME;
	public static final String ODS_TESTFILENAME = "simpleTestFile.ods";
	public static final String ODS_TESTFILEPATH = "files/" + ODS_TESTFILENAME;
	public static final String ODP_TESTFILENAME = "simpleTestFile.odp";
	public static final String ODP_TESTFILEPATH = "files/" + ODP_TESTFILENAME;
	
	
	private TestConstants() {}
}
