/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.cache;

import java.time.LocalTime;
import static java.time.temporal.ChronoUnit.MILLIS;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;

public class LockResource {
    private static final Logger LOG = LoggerFactory.getLogger(ClusterLockService.class);

    private static final ScheduledExecutorService leaseTimeThreadScheduledExecService = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder("LeaseUnlockThread-%d").build());
    private static final AtomicBoolean leaseThreadRunning = new AtomicBoolean(false);
    private static final Map<String, LockResource> locks = new ConcurrentHashMap<>();

    final String name;
    AtomicLong counter = new AtomicLong(0);
    AtomicLong threadId = new AtomicLong(0);
    AtomicLong leaseTime = new AtomicLong(0); // lease time in milliseconds
    Object sync = new Object();
    volatile LocalTime lockTime;

    public static LockResource getOrCreateLock(String name) {
        LockResource lock = new LockResource(name);
        LockResource oldLock = locks.putIfAbsent(name, lock);
        if (oldLock != null)
            lock = oldLock;
        return lock;
    }

    private LockResource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getOwnerThreadId() {
        return threadId.get();
    }

    public long getLeaseTime() {
        return leaseTime.get();
    }

    public LocalTime getLockTime() {
        synchronized (sync) {
            return lockTime; 
        }
    }

    public boolean isLocked() {
        return counter.get() > 0;
    }

    public long getLockCount() {
        return counter.get();
    }

    public void lock() {
        doLock();
    }

    public boolean tryLock() {
        return doTryLock(0, TimeUnit.MILLISECONDS, 0, TimeUnit.MILLISECONDS);
    }

    public boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
        return doTryLock(timeout, unit, 0, TimeUnit.MILLISECONDS);
    }

    public boolean tryLock(long timeout, TimeUnit unit, long lease, TimeUnit leaseUnit) throws InterruptedException {
        return doTryLock(timeout, unit, lease, leaseUnit);
    }

    public void unlock() {
        if (threadId.get() != Thread.currentThread().getId()) {
            throw new IllegalMonitorStateException("Thread does not own lock!");
        }

        if (counter.get() == 0)
            throw new IllegalMonitorStateException("Lock is not in lock state!");

        if (counter.decrementAndGet() == 0) {
            resetLock();
        }
    }

    public void forceUnlock() {
        resetLock();
    }

    private boolean doLock() {
        boolean locked = false;
        while (!locked) {
            if (counter.incrementAndGet() > 1) {
                if (threadId.get() != Thread.currentThread().getId()) {
                    counter.decrementAndGet();
                    synchronized(sync) {
                        try {
                            sync.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                } else {
                    locked = true;
                }
            } else {
                synchronized(sync) {
                    leaseTime.set(0);
                    lockTime = LocalTime.now();
                    threadId.set(Thread.currentThread().getId());
                    locked = true;
                }
            }
        }
        return locked;
    }

    private boolean doTryLock(long timeout, TimeUnit unit, long leaseTimeout, TimeUnit leaseUnit) {
        if (unit.equals(TimeUnit.MICROSECONDS) || leaseUnit.equals(TimeUnit.MICROSECONDS)) {
            throw new NotImplementedException("tryLock with microseconds!");
        }

        if ((leaseTimeout > 0) && (leaseThreadRunning.compareAndSet(false, true))) {
            leaseTimeThreadScheduledExecService.scheduleAtFixedRate(new LeaseUnlockThread(), 1, 1, TimeUnit.MILLISECONDS);
        }

        boolean locked = false;
        boolean timeoutReached = false;
        long maxWaitTime = TimeUnit.MILLISECONDS.convert(timeout, unit);
        LocalTime time = LocalTime.now();

        while (!timeoutReached && !locked) {
            if (counter.incrementAndGet() > 1) {
                if (threadId.get() != Thread.currentThread().getId()) {
                    counter.decrementAndGet();
                    try {
                        synchronized(sync) {
                            sync.wait(maxWaitTime);
                        }
                        LocalTime now = LocalTime.now();
                        timeoutReached = MILLIS.between(time, now) >= maxWaitTime;
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                   }
                } else {
                    locked = true;
                }
            } else {
                locked = true;
                synchronized(sync) {
                    leaseTime.set(TimeUnit.MILLISECONDS.convert(leaseTimeout, leaseUnit));
                    lockTime = LocalTime.now();
                    threadId.set(Thread.currentThread().getId());
                }
            }
        }
        return locked;
    }

    private void resetLock() {
        synchronized(sync) {
            counter.set(0);
            leaseTime.set(0);
            lockTime = null;
            threadId.set(0);
            sync.notifyAll();
        }
    }

    private class LeaseUnlockThread implements Runnable {

        @Override
        public void run() {
            final LocalTime now = LocalTime.now();
            locks.values().forEach(l -> {
                try {
                    long leaseTime = l.getLeaseTime();
                    LocalTime lockTime = l.getLockTime();
                    if ((leaseTime > 0) && (lockTime != null) && ((MILLIS.between(lockTime, now) >= leaseTime))) {
                        l.forceUnlock();
                        LOG.info("Lock {} was reset by LeaseUnlockThread", l.getName());
                    }
                } catch (Exception e) {
                    LOG.error("Exception caught trying to release lock after lease time expired");
                }
            });
        }
    }

}
