/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.cache;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.time.LocalTime;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;

public class LockResourceTest {

    private static final long LOCK_TIME = 1000;
    private static final long LEASE_TIME = 2 * LOCK_TIME;

    @Test
    public void testSimpleChecksForLock() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        assertNotNull(lock);
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());
        assertEquals(docUid.getValue(), lock.getName());
        assertEquals(0, lock.getLeaseTime());
        assertEquals(null, lock.getLockTime());

        LockResource lock2 = LockResource.getOrCreateLock(docUid.getValue());
        assertTrue(lock == lock2);
    }

    @Test
    public void testRegularLocking() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        lock.lock();
        assertTrue(lock.isLocked());
        assertTrue(lock.getLockCount() == 1);
        assertNotNull(lock.getLockTime());
        assertEquals(Thread.currentThread().getId(), lock.getOwnerThreadId());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertTrue(lock.getLockCount() == 0);
        assertNull(lock.getLockTime());

        lock.lock();
        assertTrue(lock.isLocked());
        assertTrue(lock.getLockCount() == 1);

        lock.lock();
        assertTrue(lock.isLocked());
        assertTrue(lock.getLockCount() == 2);

        lock.unlock();
        assertTrue(lock.isLocked());
        assertTrue(lock.getLockCount() == 1);

        lock.unlock();
        assertFalse(lock.isLocked());
        assertTrue(lock.getLockCount() == 0);
        assertEquals(0, lock.getOwnerThreadId());
    }

    @Test
    public void testRegularLockingWithConcurrency() throws Exception {
        final RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        final LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        final AtomicLong otherThreadId = new AtomicLong(0);
        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicReference<LocalTime> startTime = new AtomicReference<>();

        Thread t = new Thread() {
            @Override
            public void run() {
                boolean locked = lock.tryLock();
                otherThreadId.set(Thread.currentThread().getId());
                latch.countDown();
                try {
                    startTime.set(LocalTime.now());
                    Thread.sleep(LOCK_TIME);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    if (locked) {
                        lock.unlock();
                    }
                }
            }
        };

        lock.lock();
        assertTrue(lock.isLocked());

        lock.unlock();
        assertFalse(lock.isLocked());

        t.start();
        latch.await();

        final long currThreadId = Thread.currentThread().getId();
        lock.lock();
        final LocalTime end = LocalTime.now();

        final LocalTime start = startTime.get();
        assertTrue(lock.isLocked());
        assertEquals(currThreadId, lock.getOwnerThreadId());
        assertTrue(MILLIS.between(start, end) >= LOCK_TIME);
    }

    @Test
    public void testLockingWithTryLock() throws Exception {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        final LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        final CountDownLatch latch = new CountDownLatch(1);

        final Thread t = new Thread() {
            @Override
            public void run() {
                boolean locked = lock.tryLock();
                latch.countDown();
                try {
                    Thread.sleep(LOCK_TIME);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    if (locked) {
                        lock.unlock();
                    }
                }
            }
        };
        t.start();

        latch.await();
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        boolean locked = lock.tryLock();
        assertFalse(locked);

        Thread.sleep(LOCK_TIME + 250);

        locked = lock.tryLock();
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());

        locked = lock.tryLock();
        assertTrue(locked);
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        locked = lock.tryLock();
        assertTrue(locked);
        assertTrue(lock.isLocked());
        assertEquals(2, lock.getLockCount());
        lock.unlock();
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());
    }

    @Test
    public void testLockingWithTryLockAndTimeout() throws Exception {
        final RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        final LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        final CountDownLatch latch = new CountDownLatch(1);

        final Thread t = new Thread() {
            @Override
            public void run() {
                boolean locked = lock.tryLock();
                latch.countDown();
                try {
                    Thread.sleep(LOCK_TIME);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    if (locked) {
                        lock.unlock();
                    }
                }
            }
        };
        t.start();

        latch.await();

        assertTrue(lock.isLocked());
        boolean locked = lock.tryLock(250, TimeUnit.MILLISECONDS);
        assertFalse(locked);
        assertEquals(1, lock.getLockCount());

        locked = lock.tryLock(LOCK_TIME, TimeUnit.MILLISECONDS);
        assertTrue(locked);
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());

        locked = lock.tryLock(LOCK_TIME,TimeUnit.MILLISECONDS);
        assertTrue(locked);
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        locked = lock.tryLock(LOCK_TIME, TimeUnit.MILLISECONDS);
        assertTrue(locked);
        assertTrue(lock.isLocked());
        assertEquals(2, lock.getLockCount());

        lock.unlock();
        assertTrue(lock.isLocked());
        assertEquals(1, lock.getLockCount());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());
    }

    @Test
    public void testLockingWithTryLockAndTimeoutAndLeaseTime() throws Exception {
        final RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        final LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        final CountDownLatch latch = new CountDownLatch(1);

        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    lock.tryLock(0, TimeUnit.MILLISECONDS, LEASE_TIME, TimeUnit.MILLISECONDS);
                    latch.countDown();
                    Thread.sleep(LOCK_TIME);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                // We don't unlock the lock here as we want to test the lease
                // functions!
            }
        };
        t.start();

        latch.await();

        assertTrue(lock.isLocked());
        assertEquals(LEASE_TIME, lock.getLeaseTime());

        boolean locked = lock.tryLock(250, TimeUnit.MILLISECONDS);
        assertFalse(locked);

        locked = lock.tryLock(LEASE_TIME, TimeUnit.MILLISECONDS);
        assertTrue(locked);
        assertTrue(lock.isLocked());

        lock.unlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());

        locked = lock.tryLock(0, TimeUnit.MILLISECONDS, LEASE_TIME, TimeUnit.MILLISECONDS);
        assertTrue(locked);
        assertTrue(lock.isLocked());

        Thread.sleep(LEASE_TIME + 250);
        assertFalse(lock.isLocked());
        assertEquals(0,  lock.getLockCount());
    }

    @Test
    public void testBadLockingUsage() throws Exception {
        final RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        final LockResource lock = LockResource.getOrCreateLock(docUid.getValue());
        final CountDownLatch latch = new CountDownLatch(1);

        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    lock.tryLock(0, TimeUnit.MILLISECONDS, LEASE_TIME, TimeUnit.MILLISECONDS);
                    latch.countDown();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                // We don't unlock the lock here as we want to test forceUnlock
            }
        };

        t.start();
        latch.await();

        String e1Text = "";
        String e2Text = "";

        try {
            lock.unlock();
            fail("Not expected behavior, thread does not own lock and is not allowed to call unlock()!");
        } catch (IllegalMonitorStateException e) {
            e1Text = e.getMessage();
            assertTrue(StringUtils.isNotEmpty(e1Text));
        }

        lock.forceUnlock();
        assertFalse(lock.isLocked());
        assertEquals(0, lock.getLockCount());

        try {
            lock.unlock();
            fail("Not expected behavior, thread does not own lock and is not allowed to call unlock()!");
        } catch (IllegalMonitorStateException e) {
            e2Text = e.getMessage();
            assertTrue(StringUtils.isNotEmpty(e2Text));
        }

        assertTrue(e1Text.equals(e2Text));

        try {
            lock.tryLock(10, TimeUnit.MICROSECONDS);
            fail("Support for microseconds has not been implemented!");
        } catch (NotImplementedException e) {
            assertTrue(e != null);
        }
    }
}
