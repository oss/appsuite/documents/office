/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.cache;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.rt2.core.cache.ClusterLockException;
import com.openexchange.office.rt2.core.cache.ClusterLockService;
import com.openexchange.office.rt2.core.cache.ClusterLockService.ClusterLock;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;
import test.com.openexchange.office.rt2.core.util.InUnitTestRule;

@ExtendWith(InUnitTestRule.class)
public class ClusterLockServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(ClusterLockServiceTest.class);

    private DistributedMap<String, String> hzLockMap;
    private CachingFacade cachingFacadeMock;

    @BeforeEach
    public void init() {
        hzLockMap = new TestHzLockMap();
        cachingFacadeMock = new CachingFacadeMock<String, String>(hzLockMap);
    }

    @Test
    public void testClusterLockServiceGetter() {
        ClusterLockService objectUnderTest = new ClusterLockService();

        assertEquals(60, objectUnderTest.getMaxLockTimeInSec());
        assertEquals(1800, objectUnderTest.getMaxLockExistsTimeInSec());
    }

    @Test
    public void testGetterOfLocks() throws ClusterLockException {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 30, 60);
        objectUnderTest.afterPropertiesSet();

        LocalTime before = LocalTime.now();
        ClusterLock clusterLock = objectUnderTest.getLock(docUid);
        LocalTime after = LocalTime.now();

        final LocalTime creationTime = clusterLock.getCreationTime();
        assertTrue(creationTime.compareTo(before) >= 0);
        assertTrue(creationTime.compareTo(after) <= 0);

        assertEquals(0, clusterLock.getAssumedLockCount());
    }

    @Test
    public void testLockIdentity() throws ClusterLockException {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 1, 2);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock lock1 = objectUnderTest.getLock(docUid);
        assertNotNull(lock1);
        assertFalse(lock1.isLocked());

        ClusterLock lock2 = objectUnderTest.getLock(docUid);
        assertNotNull(lock2);
        assertFalse(lock2.isLocked());

        assertTrue(lock1 == lock2);

        boolean interrupted = false;
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            interrupted = true;
            Thread.currentThread().interrupt();
        }

        // trigger manually to ensure expired locks are removed
        objectUnderTest.runDocLockRemoveThread();

        assertFalse(interrupted);
        ClusterLock lock3 = objectUnderTest.getLock(docUid);
        assertFalse(lock3 == lock1);
        assertFalse(lock3 == lock2);
    }

    @Test
    public void testRegularLocking() throws ClusterLockException {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 30, 60);
        objectUnderTest.afterPropertiesSet();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid);
        boolean locked = clusterLock.lock();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertFalse(clusterLock.isLocked());
    }

    @Test
    public void testIrregularLocking() throws InterruptedException, ClusterLockException {
        RT2DocUidType docUid1 = new RT2DocUidType("ToUnlock");
        RT2DocUidType docUid2 = new RT2DocUidType("StackLocked");
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 1, 5);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock1 = objectUnderTest.getLock(docUid1);
        ClusterLock clusterLock2 = objectUnderTest.getLock(docUid2);

        boolean locked1 = clusterLock1.lock();
        assertTrue(locked1);
        assertTrue(clusterLock1.isLocked());

        Thread.sleep(700);

        boolean locked2 = clusterLock2.lock();
        assertTrue(locked2);
        assertTrue(clusterLock2.isLocked());

        Thread.sleep(700);

        assertFalse(clusterLock1.isLocked());
        assertTrue(clusterLock2.isLocked());
    }

    @Test
    public void testDeleteLockingObject() throws InterruptedException, ClusterLockException {
        RT2DocUidType docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
        RT2DocUidType docUid2 = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 1, 2);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock1 = objectUnderTest.getLock(docUid1);

        boolean locked1 = clusterLock1.lock();
        assertTrue(locked1);
        assertTrue(clusterLock1.isLocked());

        Thread.sleep(1200);

        ClusterLock clusterLock2 = objectUnderTest.getLock(docUid2);
        boolean locked2 = clusterLock2.lock();
        assertTrue(locked2);
        assertTrue(clusterLock2.isLocked());

        Thread.sleep(900);

        // trigger lock remove thread manually
        objectUnderTest.runDocLockRemoveThread();

        final List<String> locks = new ArrayList<>(objectUnderTest.getLocks());
        assertFalse(locks.get(0).contains(clusterLock1.getName()), "Lock1: " + clusterLock1.getName() + ", existing: " + locks.toString());
        assertTrue(locks.get(0).contains(clusterLock2.getName()));
    }

    @Test
    public void testReentrantLocking() throws InterruptedException, ClusterLockException {
        RT2DocUidType docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 60, 300);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid1);

        boolean locked = clusterLock.lock();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        locked = clusterLock.lock();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        locked = clusterLock.lock();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertFalse(clusterLock.isLocked());
    }

    @Test
    public void testTryLock() throws ClusterLockException {
        RT2DocUidType docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 60, 300);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid1);

        LocalTime start = LocalTime.now();
        boolean locked = clusterLock.lock(2000);
        LocalTime end = LocalTime.now();
        // ensure that waiting time has no effect on the duration of the
        // lock call without concurrency - use longer values to ensure that
        // slower system are able to runs this test successfully
        long diff = MILLIS.between(start, end);
        assertTrue(diff < 2000);

        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        start = LocalTime.now();
        locked = clusterLock.lock(3000);
        end = LocalTime.now();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        diff = MILLIS.between(start, end);
        assertTrue(diff < 3000);

        start = LocalTime.now();
        locked = clusterLock.lock(4000);
        end = LocalTime.now();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        diff = MILLIS.between(start, end);
        assertTrue(diff < 4000);

        start = LocalTime.now();
        locked = clusterLock.lock(5000);
        end = LocalTime.now();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        diff = MILLIS.between(start, end);
        assertTrue(diff < 5000);

        clusterLock.unlock();
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertTrue(clusterLock.isLocked());

        clusterLock.unlock();
        assertFalse(clusterLock.isLocked());
    }

    @Test
    public void testLockWithConcurrency() throws InterruptedException, ClusterLockException {
        final long LOCK_TIME = 1000;

        RT2DocUidType docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 60, 300);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid1);

        final AtomicBoolean threadSuccess = new AtomicBoolean(false);
        final CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread() {

            @Override
            public void run() {
                boolean locked = false;
                try {
                    locked = clusterLock.lock();
                    latch.countDown();
                    Thread.sleep(LOCK_TIME);
                    threadSuccess.set(true);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (ClusterLockException e) {
                    LOG.error("ClusterLockException caught!", e);
                } finally {
                    if (locked) {
                        clusterLock.unlock();
                    }
                }
            }
        };

        t.start();

        LocalTime start = LocalTime.now();
        latch.await();

        boolean locked = clusterLock.lock();
        LocalTime end = LocalTime.now();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        long timeDiff = MILLIS.between(start, end);
        LOG.info(String.valueOf(timeDiff));
        assertTrue(timeDiff >= LOCK_TIME);

        start = LocalTime.now();
        locked = clusterLock.lock();
        end = LocalTime.now();
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        assertTrue(MILLIS.between(start, end) < LOCK_TIME);
    }

    @Test
    public void testLockingWithLease() throws InterruptedException, ClusterLockException {
        RT2DocUidType docUid = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 1, 10);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid);

        boolean locked = clusterLock.lock(1000);
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());

        Thread.sleep(1050);

        // lease time expired cluster is not locked anymore
        assertFalse(clusterLock.isLocked());
    }

    @Test
    public void testTryLockWithInternalRetryAndForceUnlock() throws InterruptedException, ClusterLockException {
        final LocalTime started = LocalTime.now();
        RT2DocUidType docUid1 = new RT2DocUidType(UUID.randomUUID().toString());
        ClusterLockService objectUnderTest = new ClusterLockService(cachingFacadeMock, 60, 180);
        objectUnderTest.afterPropertiesSet();

        // ensure that automatic remove thread does not run
        objectUnderTest.destroy();

        ClusterLock clusterLock = objectUnderTest.getLock(docUid1);

        final AtomicBoolean threadLockedClusterLockSuccessfully = new AtomicBoolean(false);
        final CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    clusterLock.lock();
                    threadLockedClusterLockSuccessfully.set(true);
                    latch.countDown();
                } catch (ClusterLockException e) {
                    LOG.error("ClusterLockException caught!", e);
                }
            }
        };

        t.start();

        latch.await();
        assertTrue(threadLockedClusterLockSuccessfully.get());

        boolean locked = clusterLock.lock(500);
        assertTrue(locked);
        assertTrue(clusterLock.isLocked());
        assertEquals(1, clusterLock.getAssumedLockCount());
        assertNotNull(clusterLock.getLasttForceUnlockTime());
        assertTrue(clusterLock.getLasttForceUnlockTime().isAfter(started));
    }
}
