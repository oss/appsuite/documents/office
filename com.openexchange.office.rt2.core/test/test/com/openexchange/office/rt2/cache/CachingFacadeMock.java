/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.cache;

import java.util.Collection;
import java.util.Map;

import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedAtomicLong;
import com.openexchange.office.tools.service.caching.DistributedLock;
import com.openexchange.office.tools.service.caching.DistributedMap;

import org.apache.commons.lang3.NotImplementedException;

public class CachingFacadeMock<K, V> implements CachingFacade {
	private DistributedMap<K, V> testHzMap;

	public CachingFacadeMock(DistributedMap<K, V> testHzMap) {
		this.testHzMap = testHzMap;
	}

    @SuppressWarnings("unchecked")
	@Override
    public <K, V> DistributedMap<K, V> getDistributedMap(String name, Class<K> keyClass, Class<V> valueClass) {
        return (DistributedMap<K, V>) testHzMap;
	}

	@Override
	public DistributedAtomicLong getDistributedAtomicLong(String name) {
		throw new NotImplementedException("getDistributedAtomicLong");
	}

	@Override
	public Map<String, DistributedAtomicLong> getDistributedAtomicLongsWithPrefix(String prefix) {
		throw new NotImplementedException("getDistributedAtomicLongsWithPrefix");
	}

	@Override
	public Collection<DistributedLock> getDistributedLockWithPrefix(String prefix) {
		throw new NotImplementedException("getDistributedLockWithPrefix");
	}

}
