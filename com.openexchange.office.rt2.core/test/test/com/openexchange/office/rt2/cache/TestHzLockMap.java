/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.rt2.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.NotImplementedException;

import com.hazelcast.query.Predicate;
import com.openexchange.office.tools.service.caching.DistributedMap;

public class TestHzLockMap implements DistributedMap<String, String> {

    private Map<String, String> valueMap = new ConcurrentHashMap<>();

    public TestHzLockMap() {
        // nothing to do
    }

    @Override
    public int size() {
        return valueMap.size();
    }

    @Override
    public boolean isEmpty() {
        return valueMap.isEmpty();
    }

    @Override
    public void putAll(Map<? extends String, ? extends String> m) {
        valueMap.putAll(m);
    }

    @Override
    public boolean containsKey(Object key) {
        return valueMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return valueMap.containsValue(value);
    }

    @Override
    public String get(Object key) {
        return valueMap.get(key);
    }

    @Override
    public String put(String key, String value) {
        return valueMap.put(key, value);
    }

    @Override
    public String remove(Object key) {
        return valueMap.remove(key);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return valueMap.remove(key, value);
    }

    public void delete(Object key) {
        valueMap.remove(key);
    }

    @Override
    public Map<String, String> getAll(Set<String> keys) {
        Map<String, String> res = new HashMap<>();
        keys.forEach(k -> {
            String v = valueMap.get(k);
            if (v != null) {
                res.put(k, v);
            }
        });
        return res;
    }

    @Override
    public void clear() {
        valueMap.clear();
    }

    @Override
    public String putIfAbsent(String key, String value) {
        return valueMap.putIfAbsent(key, value);
    }

    @Override
    public boolean replace(String key, String oldValue, String newValue) {
        return valueMap.replace(key, oldValue, newValue);
    }

    @Override
    public String replace(String key, String value) {
        return valueMap.replace(key, value);
    }

    public void set(String key, String value) {
        valueMap.put(key, value);
    }

    @Override
    public void lock(String key) {
        throw new NotImplementedException("lock");
    }

    @Override
    public boolean isLocked(String key) {
        LockResource lock = LockResource.getOrCreateLock(key);
        return lock.isLocked();
    }

    public boolean tryLock(String key) {
        throw new NotImplementedException("tryLock");
    }

    @Override
    public boolean tryLock(String key, long time, TimeUnit timeunit) throws InterruptedException {
        LockResource lock = LockResource.getOrCreateLock(key);
        boolean result = lock.tryLock(time, timeunit);
        return result;
    }

    public boolean tryLock(String key, long time, TimeUnit timeunit, long leaseTime, TimeUnit leaseTimeunit) throws InterruptedException {
        LockResource lock = LockResource.getOrCreateLock(key);
        boolean result = lock.tryLock(time, timeunit, leaseTime, leaseTimeunit);
        return result;
    }

    @Override
    public void unlock(String key) {
        LockResource lock = LockResource.getOrCreateLock(key);
        lock.unlock();
    }

    @Override
    public void forceUnlock(String key) {
        LockResource lock = LockResource.getOrCreateLock(key);
        lock.forceUnlock();
    }

    public boolean evict(String key) {
        throw new NotImplementedException("evict");
    }

    public void evictAll() {
        throw new NotImplementedException("evictAll");
    }

    @Override
    public Set<String> keySet() {
        return valueMap.keySet();
    }

    @Override
    public Collection<String> values() {
        return valueMap.values();
    }

    @Override
    public Set<Entry<String, String>> entrySet() {
        return valueMap.entrySet();
    }

    @Override
    public Set<Entry<String, String>> entrySet(Predicate<String, String> predicate) {
        throw new NotImplementedException("entrySetWithPredicated");
    }

    @Override
    public String getName() {
        return "testHzLockMap";
    }
}
