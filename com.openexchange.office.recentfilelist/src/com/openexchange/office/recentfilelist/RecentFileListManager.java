/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.recentfilelist;

import java.util.List;

import org.json.JSONObject;

import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.ApplicationType;

/**
 * RecentFileListManager provides methods to read/write the user recent file
 * list from the user configuration. Additional support for methods to
 * add/update the list to reflect the latest changes.
 *
 * @author Carsten Driesner
 * @since 7.6.0
 */
public interface RecentFileListManager {

	/**
	 * Reads the recent file list from the user that is referenced by the
	 * session provided to this instance.
	 *
	 * @return
	 *  A list of recent files as JSONObjects.
	 */
	public List<JSONObject> readRecentFileList(ApplicationType app);

    /**
	 * Adds a recentFile JSONObject to the recent file list. If the entry
	 * already exists in the list it will be removed and added at the front.
	 *
	 * @param recentFiles
	 *  A list of JSONObjects describing recent file list entries.
	 *
	 * @param recentFile
	 *  A JSONObject describing a recent file list entry.
	 *
	 * @param lastModified
	 *  The date of the last modification or null if there was no modification.
	 */
    public void addCurrentFile(List<JSONObject> recentFiles, JSONObject recentFile, java.util.Date lastModified);

    /**
     * Updates a recent file entry without changing the order of entries. If the
     * entry cannot be found it will be added at the front.
     *
     * @param recentFiles
     * @param fileId
     * @param updatedFileDescriptorProps
     * @param lastModified
     */
    public void updateFile(final List<JSONObject> recentFiles, final String fileId, final JSONObject updatedFileDescriptorProps, java.util.Date lastModified);

    /**
     * Removes a file from the recent file list if it part of the list.
     * It does nothing if the file is not part of the list.
     *
     * @param recentFiles
     *  The current recent file list.
     *
     * @param fileToBeRemoved
     *  A file descriptor which references the file to be removed. Must at
     *  least contain a valid folder and file id.
     *
     * @return
     *  TRUE if the file has been removed or FALSE if nothing has been
     *  changed.
     */
    public boolean removeFileFromList(final List<JSONObject> recentFiles, final JSONObject fileToBeRemoved);

    /**
     * Writes the recent file list from a user to the recent file list. The
     * old list entries will be overwritten.
     *
     * @param app
     *  Specifies for which application type the list should be written.
     *
     * @param recentFiles
     *  The JSONObject list of recent files.
     *
     * @return
     *  TRUE if writing the list was successful, otherwise FALSE.
     */
    public boolean writeRecentFileList(ApplicationType app, final List<JSONObject> recentFiles);

    /**
     * Flush writes the recent file list data persistently to the user
     * configuration.
     *
     * @return
     *  TRUE if flushing the data was successful, otherwise FALSE.
     */
    public boolean flush();

    /**
     * Migrate an old recent file user configuration structure to the new
     * data structure. The old recent file user configuration stays intact
     * to enable users to use an old version. Be careful calling this more
     * than once for any application will overwrite current user data.
     * The method doesn't check if there are existing recent files in the
     * new data structure.
     *
     * @param app
     *  The application type which should be migrated to the new data
     *  structure.
     *
     * @return
     *  TRUE if the migration was successful, otherwise FALSE.
     */
    public boolean migrateOldRecentFileList(ApplicationType app);

    /**
     * Creates a simple file descriptor to check if a file is part of a
     * recent file list. It's not compliant to be added to the recent file
     * list.
     *
     * @param folderId
     *  The folder id of the new file descriptor.
     *
     * @param fileId
     *  The file identifier of the new file descriptor.
     *
     * @return
     *  A file descriptor as a JSONObject or null if one of the arguments is
     *  not set.
     */
    public JSONObject createFileDescriptor(String folderId, String fileId);

    /**
     * Determines the importance of an exception dependent on the
     * mapped error code.
     *
     * @param errorCode
     * @return TRUE if the exception is important and should be logged on
     *         WARN/ERROR. FALSE if the exception must not be logged on
     *         ERROR/WARN or INFO.
     */
    public boolean isImportantException(final ErrorCode errorCode);
}
