/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.recentfilelist.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.exception.OXException;
import com.openexchange.folderstorage.ContentType;
import com.openexchange.folderstorage.FolderResponse;
import com.openexchange.folderstorage.FolderService;
import com.openexchange.folderstorage.FolderServiceDecorator;
import com.openexchange.folderstorage.UserizedFolder;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.tools.common.TimeZoneHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.ExtensionHelper;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

/**
 * RecentFileListManager provides methods to read/write the user recent file
 * list from the user configuration. Additional support for methods to
 * add/update the list to reflect the latest changes.
 *
 * @author Carsten Driesner
 * @since 7.6.0
 */
public class RecentFileListManagerImpl implements RecentFileListManager, InitializingBean {
    private static final Logger LOG = LoggerFactory.getLogger(RecentFileListManagerImpl.class);
    private static final String[] neededKeys = {"folder_id", "id", "last_opened", "last_modified", "creation_date", "title", "file_mimetype", "filename" };
    private static final HashSet<String> filterKeys = new HashSet<>(Arrays.asList(neededKeys));
    private static final int MAX_RECENT_ENTRIES = 30;
    private static final String TREEID_FOR_STORAGES = "1";

	private Session session = null;
	private UserConfigurationHelper userConfHelper;
	
	@Autowired
	private UserConfigurationFactory userConfigFactory;

	@Autowired
	private FolderService folderService;
	
	/**
	 * Stores user/client specific data to retrieve path information from the
	 * FolderService.
	 * {@link FolderContextData}
	 *
	 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
	 * @since v7.10.0
	 */
	private static class FolderContextData {
	    public static final Locale DEFAULT_LOCALE = Locale.US;

	    private Locale locale = DEFAULT_LOCALE;
	    private List<ContentType> allowedContentTypes = Collections.<ContentType> emptyList();
	    private FolderServiceDecorator folderServiceDecorator;

	    public FolderContextData(Locale locale, TimeZone timeZone) {
	        this.locale = locale;

           folderServiceDecorator = new FolderServiceDecorator();
           folderServiceDecorator.setLocale(locale);
           folderServiceDecorator.setTimeZone(timeZone);
           folderServiceDecorator.setAllowedContentTypes(allowedContentTypes);
           folderServiceDecorator.put("altNames", true);
	    }

	    public Locale getLocale() { return locale; }
	    public FolderServiceDecorator getDecorator() { return folderServiceDecorator; }
	}

    /**
     * Creates a new instance of a RecentFileListManager.
     *
     * @param services
     *  The service lookup instance to be used by the new instance.
     *
     * @param session
     *  The session of the user where the recent file list should be read or
     *  modified.
     */
    public RecentFileListManagerImpl(Session session) {
        this.session = session;
    }

	@Override
	public void afterPropertiesSet() {
		userConfHelper = userConfigFactory.create(session, "io.ox/office", Mode.WRITE_BACK);
	}

	/**
	 * Reads the recent file list from the user that is referenced by the
	 * session provided to this instance.
	 *
	 * @return
	 *  A list of recent files as JSONObjects.
	 */
	@Override
	public List<JSONObject> readRecentFileList(ApplicationType app) {
		ArrayList<JSONObject> result = null;

	    if (null != session) {
	        final String userConfEntryKey = "portal/" + ApplicationType.enumToString(app) + "/recents";
	       	final String jsonRecentFileListString = userConfHelper.getString(userConfEntryKey);

	       	if (null != jsonRecentFileListString) {
	            Locale locale = FolderContextData.DEFAULT_LOCALE;
	            String timeZoneId = "UTC";
	            if (session instanceof ServerSession) {
	                locale = ((ServerSession)session).getUser().getLocale();
	                timeZoneId = ((ServerSession)session).getUser().getTimeZone();
	            }
	
	            final TimeZone timeZone = TimeZoneHelper.getTimeZone(timeZoneId);
	            final FolderContextData folderContextData = new FolderContextData(locale, timeZone);
	
	            try {
	                final JSONArray recentFileArray = new JSONArray(jsonRecentFileListString);
	                final ArrayList<JSONObject> list = new ArrayList<>();
	
	                for (int i = 0; i < Math.min(recentFileArray.length(), MAX_RECENT_ENTRIES); i++) {
	                    try {
	                        final Object obj = recentFileArray.get(i);
	                        if (obj instanceof JSONObject) {
	                            final JSONObject fileDescriptor = (JSONObject) obj;
	                            // filter out non-important attributes
	                            filterFileDescriptorAttributes(fileDescriptor);
	                            fileDescriptor.put("path", getPath(fileDescriptor.getString("folder_id"), folderContextData));
	                            list.add(fileDescriptor);
	                        }
	                    } catch (final JSONException e) {
	                        LOG.error("RT connection: JSONException while trying to create recent file list entry ", e);
	                    } catch (final OXException e) {
	                        LOG.info("RT connection: OXException while trying to determine path of recent file list entry ", e);
	                    }
	                }
	                result = list;
	            } catch (final JSONException e) {
	                LOG.error("RT connection: JSONException while reading recent file list entries " + userConfEntryKey, e);
	            }
	        }
	    }

   	    return result;
    }

   /**
	 * Adds a recentFile JSONObject to the recent file list. If the entry
	 * already exists in the list it will be removed and added at the front.
	 *
	 * @param recentFiles
	 *  A list of JSONObjects describing recent file list entries.
	 *
	 * @param recentFile
	 *  A JSONObject describing a recent file list entry.
	 *
	 * @param lastModified
	 *  The date of the last modification or null if there was no modification.
	 */
	@Override
    public void addCurrentFile(List<JSONObject> recentFiles, JSONObject recentFile, java.util.Date lastModified) {
        if ((null != recentFiles) && (null != recentFile)) {
   		    String id1 = recentFile.optString("id", "");
   		    String folder1 = recentFile.optString("folder_id", "");

	   		if ((id1.length() > 0) && (folder1.length() > 0)) {
	   		    int foundIndex = -1;
	
				for (int i = 0; i < recentFiles.size(); i++) {
				    Object obj = recentFiles.get(i);
				   	if (obj instanceof JSONObject) {
				   	    final JSONObject json = (JSONObject)obj;
				   		if (id1.equals(json.optString("id", "")) && folder1.equals(json.optString("folder_id", ""))) {
				   			foundIndex = i;
				   			break;
				   		}
				   	}
				}
	
			    // an entry which already exists must be moved to the first pos
			    if (foundIndex >= 0) {
			        recentFiles.remove(foundIndex);
			    }

		    	// filter out non-important attributes
			    filterFileDescriptorAttributes(recentFile);
			    recentFiles.add(0, recentFile);

                // remove entries that exceeds the max size of the recent list
                if (recentFiles.size() > MAX_RECENT_ENTRIES) {
                    recentFiles.subList(MAX_RECENT_ENTRIES, recentFiles.size()).clear();
                }
	        }
        }
    }

   /**
    * Updates a recent file entry without changing the order of entries. If the
    * entry cannot be found it will be added at the front.
    *
    * @param recentFiles
    * @param fileId
    * @param updatedFileDescriptorProps
    * @param lastModified
    */
    @Override
    public void updateFile(final List<JSONObject> recentFiles, final String fileId, final JSONObject updatedFileDescriptorProps, java.util.Date lastModified) {
       if ((null != recentFiles) && (StringUtils.isNotEmpty(fileId))) {

           int foundIndex = -1;

           for (int i = 0; i < recentFiles.size(); i++) {
               Object obj = recentFiles.get(i);
               if (obj instanceof JSONObject) {
                   if (fileId.equals(((JSONObject) obj).optString("id", ""))) {
                       foundIndex = i;
                       break;
                   }
               }
           }

           if (foundIndex >= 0) {
               // update existing entry
               final JSONObject oldRecentFile = recentFiles.get(foundIndex);
               final JSONObject newRecentFile = RecentFileListManagerImpl.makeUpdatedCopy(oldRecentFile, updatedFileDescriptorProps);
               // filter out non-important attributes
               filterFileDescriptorAttributes(newRecentFile);
               recentFiles.set(foundIndex, newRecentFile);
           }
       }
   }

    /**
     * Removes a file from the recent file list if it part of the list.
     * It does nothing if the file is not part of the list.
     *
     * @param recentFiles
     *  The current recent file list.
     *
     * @param fileToBeRemoved
     *  A file descriptor which references the file to be removed. Must at
     *  least contain a valid folder and file id.
     *
     * @return
     *  TRUE if the file has been removed or FALSE if nothing has been
     *  changed.
     */
    @Override
    public boolean removeFileFromList(final List<JSONObject> recentFiles, final JSONObject fileToBeRemoved) {
        if ((null != recentFiles) && (null != fileToBeRemoved)) {
	   		String id1 = fileToBeRemoved.optString("id", "");
	   		String folder1 = fileToBeRemoved.optString("folder_id", "");
	
	   		if ((id1.length() > 0) && (folder1.length() > 0)) {
	   		    int foundIndex = -1;
	
			    for (int i = 0; i < recentFiles.size(); i++) {
			    	Object obj = recentFiles.get(i);
			    	if (obj instanceof JSONObject) {
			    		if (id1.equals(((JSONObject) obj).optString("id", "")) &&
		    			    folder1.equals(((JSONObject)obj).optString("folder_id", ""))) {
			    			foundIndex = i;
			    			break;
			    		}
			    	}
			    }
	
			    if (foundIndex >= 0) {
			    	recentFiles.remove(foundIndex);
			    	return true;
			    }
   		   }
   	    }

   	    return false;
    }

    /**
     * Writes the recent file list from a user to the recent file list. The
     * old list entries will be overwritten.
     *
     * @param app
     *  Specifies for which application type the list should be written.
     *
     * @param recentFiles
     *  The JSONObject list of recent files.
     *
     * @return
     *  TRUE if writing the list was successful, otherwise FALSE.
     */
    @Override
    public boolean writeRecentFileList(ApplicationType app, final List<JSONObject> recentFiles) {
		boolean result = false;

		if ((null != userConfHelper) && (app != ApplicationType.APP_NONE) && (null != recentFiles)) {
			String userConfEntryKey = "portal/" + ApplicationType.enumToString(app) +"/recents";

			JSONArray recentFileArray = new JSONArray(recentFiles);
			String jsonString = recentFileArray.toString();
			userConfHelper.setValue(userConfEntryKey, jsonString);
			result = true;
		}

		return result;
    }

    /**
     * Flush writes the recent file list data persistently to the user
     * configuration.
     *
     * @return
     *  TRUE if flushing the data was successful, otherwise FALSE.
     */
    @Override
    public boolean flush() {
    	boolean result = false;
    	final UserConfigurationHelper localUserConfHelper = userConfHelper;

    	if (null != localUserConfHelper) {
    		result = localUserConfHelper.flushCache();
    	}

    	return result;
    }

   /**
    * Migrate an old recent file user configuration structure to the new
    * data structure. The old recent file user configuration stays intact
    * to enable users to use an old version. Be careful calling this more
    * than once for any application will overwrite current user data.
    * The method doesn't check if there are existing recent files in the
    * new data structure.
    *
    * @param app
    *  The application type which should be migrated to the new data
    *  structure.
    *
    * @return
    *  TRUE if the migration was successful, otherwise FALSE.
    */
    @Override
    public boolean migrateOldRecentFileList(ApplicationType app) {
	   	boolean result = false;
	
	   	String userConfEntryKey = "portal/recents";
	   	String oldRecentFiles = userConfHelper.getString(userConfEntryKey);
	
	   	if ((null != oldRecentFiles) && (app != ApplicationType.APP_NONE)) {
	   		HashSet<String> appExtensionSet = null;
	
	   		if (app == ApplicationType.APP_TEXT)
	   			appExtensionSet = new HashSet<>(Arrays.asList(ExtensionHelper.TEXT_EXTENSIONS));
	   		else if (app == ApplicationType.APP_SPREADSHEET)
	   			appExtensionSet = new HashSet<>(Arrays.asList(ExtensionHelper.SPREADSHEET_EXTENSIONS));
	   		else if (app == ApplicationType.APP_PRESENTATION)
	   			appExtensionSet = new HashSet<>(Arrays.asList(ExtensionHelper.PRESENTATION_EXTENSIONS));
	
	   		final ArrayList<JSONObject> recentAppFiles = new ArrayList<>();
	   		try {
	   			final JSONArray oldRecentFilesArray = new JSONArray(oldRecentFiles);
	   			for (int i = 0; i < oldRecentFilesArray.length(); i++) {
	   				Object obj = oldRecentFilesArray.get(i);
	   				if (obj instanceof JSONObject) {
	   					JSONObject fileDescriptor = (JSONObject)obj;
	
	   					filterFileDescriptorAttributes(fileDescriptor);
	   					String fileName = fileDescriptor.optString("filename", null);
	   					String ext = FileHelper.getExtension(fileName);
	   					if (appExtensionSet.contains(ext)) {
	   						recentAppFiles.add(fileDescriptor);
	   					}
	   				}
	   			}
	
	   			// prepare the new data structure to write to the new recent
	   			// data structure
	   			prepareNewRecentDataStructure(app);
	   			if (!recentAppFiles.isEmpty()) {
	   				writeRecentFileList(app, recentAppFiles);
	   			}
	   		} catch (JSONException e) {
	   			LOG.error("Exception caught while migrating old user recent files to new data structure", e);
	   		}
	   	} else {
	   		// prepare the new data structure to enable writing to the new
	   		// recent data structure.
	   		prepareNewRecentDataStructure(app);
	   	}

	   	return result;
    }

   /**
    * Creates a simple file descriptor to check if a file is part of a
    * recent file list. It's not compliant to be added to the recent file
    * list.
    *
    * @param folderId
    *  The folder id of the new file descriptor.
    *
    * @param fileId
    *  The file identifier of the new file descriptor.
    *
    * @return
    *  A file descriptor as a JSONObject or null if one of the arguments is
    *  not set.
    */
    @Override
    public JSONObject createFileDescriptor(String folderId, String fileId) {
    	JSONObject result = null;

   		if ((null != folderId) && (null != fileId)) {
	    	try {
	    		JSONObject jsonObject = new JSONObject();
	    	    jsonObject.put("id", fileId);
	    	    jsonObject.put("folder_id", folderId);
	    	    result = jsonObject;
	    	} catch (JSONException e) {
	    		// no exception -> error case is mapped to result == null
	    	}
   		}
   		return result;
    }

    /**
     * Determines the importance of an exception dependent on the
     * mapped error code.
     *
     * @param errorCode
     * @return TRUE if the exception is important and should be logged on
     *         WARN/ERROR. FALSE if the exception must not be logged on
     *         ERROR/WARN or INFO.
     */
    @Override
    public boolean isImportantException(final ErrorCode errorCode) {
        boolean result = true;

        if (errorCode.isNoError())
            result = false;
        else {
            final int code = errorCode.getCode();
            switch (code) {
                // Currently we define folder exceptions where the folder is not found
                // or invisible (due to missing permissions) as "normal" and NOT important
                // The owner of a folder can set the permissions or delete it at runtime
                // therefore it's "normal" that we encounter these exceptions/error checking
                // the recent file list.
                case ErrorCode.CODE_GENERAL_FOLDER_NOT_FOUND_ERROR:
                case ErrorCode.CODE_GENERAL_FOLDER_NOT_VISIBLE_ERROR: result = false; break;
                default: result = true; break;
            }
        }

        return result;
    }

	/**
	 * Retrieves the path of a folder.
	 *
	 * @param folderId the folder id where the path should be retrieved
	 * @param folderContextData necessary folder context data to retrieve the path (time zone, locale, etc.)
	 * @return a JSONArray containing the folder-id & name of path levels, starting with the top-most folder
	 * @throws JSONException
	 */
    private JSONArray getPath(String folderId, final FolderContextData folderContextData) throws OXException, JSONException {
        final FolderResponse<UserizedFolder[]> subfoldersResponse = folderService.getPath(
            TREEID_FOR_STORAGES, folderId, session, folderContextData.getDecorator());

        final List<JSONObject> result = new ArrayList<>();
        final UserizedFolder[] subFolders = subfoldersResponse.getResponse();

        if (subFolders.length > 0) {
            for (final UserizedFolder folder : subFolders) {
                final JSONObject folderInfo = new JSONObject();

                folderInfo.put("id", folder.getID());
                folderInfo.put("filename", folder.getName(folderContextData.getLocale(), true));
                result.add(0, folderInfo);
            }
        }

        return new JSONArray(result);
    }

    /**
	 * Prepares the new recent file data structure. For a specific application
	 * type.
	 *
	 * @param app
	 *  The application type
	 * @return
	 *  TRUE if the new data structure has been written, otherwise FALSE.
	 */
    private boolean prepareNewRecentDataStructure(ApplicationType app) {
    	boolean result = false;

    	if (app != ApplicationType.APP_NONE) {
    		final String appString = ApplicationType.enumToString(app);
	    	final String userConfAppEntryKey = "portal/" + appString + "/recents";
			final String strEmptyJSONArray = (new JSONArray()).toString();

	    	if (null != userConfHelper) {
	    		try {
	    			// check "portal" config tree and create if missing
		    		String data = null;
		    		JSONObject portalConfTree = userConfHelper.getJSONObject("portal");
		    		if (null == portalConfTree) {
		    			// create portal config tree
		    			portalConfTree = new JSONObject();
		    			userConfHelper.setValue("portal", portalConfTree);
		    		}

		    		// check text application JSONObject - create if missing
		    		data = userConfHelper.getString(userConfAppEntryKey);
		    		if (null == data) {
		    			// create application JSONObject which will contain the
		    			// "recents" JSONString attribute
		    			JSONObject appContainer = new JSONObject();
		    			appContainer.put("recents", strEmptyJSONArray);
		    			// put the application JSONObject to the portal config
		    			// tree
		    			portalConfTree.put(appString, appContainer);
		    		}
		    		userConfHelper.flushCache();
		    		result = true;
	    		}
	            catch (JSONException e) {
	    		    LOG.error("Exception caught while creating new structure for user recent files", e);
	    		}
	    	}
    	}

    	return result;
    }

    /**
     * Filters non-important attributes from a JSONObject that contains
     * file descriptor attributes. This saves memory and time.
     *
     * @param fileDescriptor
     *  A JSONObject that contains the data of a file descriptor.
	 */
    private void filterFileDescriptorAttributes(JSONObject fileDescriptor) {
    	if (null != fileDescriptor) {
    		final Set<String> keys = fileDescriptor.asMap().keySet();
    		for (String key : keys) {
    			if (!filterKeys.contains(key)) {
    				fileDescriptor.remove(key);
    			}
    		}
    	}
    }

    /**
     * Creates an updated copy of the provided JSONObject.
     *
     * @param source the source JSONObject to be copied
     * @param updateFileProps the updated properties merged into the copy
     * @return the copied JSONObject containing the updated properties
     */
    private static final JSONObject makeUpdatedCopy(final JSONObject source, final JSONObject updateFileProps) {
    	JSONObject result = source;

    	try {
    		result = new JSONObject(source, source.keySet().toArray(new String[0]));

    		final Iterator<String> props = updateFileProps.keys();
    		while (props.hasNext()) {
    			final String key = props.next();
    			result.put(key, updateFileProps.get(key));
    		}
    	} catch (JSONException e) {
    		LOG.error("JSONException caught trying to copy JSONObject for recent file list", e);
    	}

    	return result;
    }

}
