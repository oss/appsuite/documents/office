import org.gradle.plugins.ide.eclipse.model.Classpath
import org.gradle.plugins.ide.eclipse.model.ClasspathEntry
import org.gradle.plugins.ide.eclipse.model.AbstractClasspathEntry

plugins {
    `java-library`
    `eclipse`
}

eclipse {
   classpath {
        containers("org.eclipse.buildship.core.gradleclasspathcontainer")
   }
}

eclipse.classpath.file {
    whenMerged(closureOf<Classpath>{
        val entries = getEntries()
        for (entry in entries) {
            if ((entry.getKind() == "con") && (entry is AbstractClasspathEntry) &&
                (entry.getPath() == "org.eclipse.buildship.core.gradleclasspathcontainer")) {
                entry.setExported(true)
            }
        }
        setEntries(entries)
    })
}

val jarContents: Configuration by configurations.creating {
    description = "Contents of this jars are provided by com.openexchange.office.dependencies bundle"
}

repositories {
    mavenCentral()
}

dependencies {
    jarContents("org.apache.commons:commons-text:1.12.0")
    jarContents("javax.activation:activation:1.1.1")
    jarContents("javax.validation:validation-api:2.0.1.Final")
    jarContents("org.apache.activemq:activemq-pool:5.18.6")
    jarContents("org.apache.activemq:activemq-client:5.18.6")
    jarContents("org.glassfish.jersey.containers:jersey-container-servlet-core:2.45")
    jarContents("org.springframework:spring-web:5.3.32")
    jarContents("org.springframework:spring-jms:5.3.32")
    jarContents("org.springframework:spring-context:5.3.32")
    jarContents("org.springframework.boot:spring-boot-autoconfigure:2.7.18")
    jarContents("com.google.protobuf:protobuf-java:3.6.1")
    jarContents("io.dropwizard.metrics:metrics-core:4.2.30")
    jarContents("io.dropwizard.metrics:metrics-jmx:4.2.30")
    jarContents("com.udojava:JMXWrapper:1.4")
    jarContents("io.leangen.geantyref:geantyref:1.3.16")
    api(files("lib/com.openexchange.office.dependencies.jar") {
        builtBy("dependenciesJar")
    })
}

val jarTask = tasks.register<Jar>("dependenciesJar") {
    destinationDirectory.set(layout.projectDirectory.dir("lib"))
    archiveClassifier.set("")
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
      from(file("META-INF/MANIFEST.MF"))
    }
    from({
        jarContents.map { zipTree(it) }
    })
}

defaultTasks("dependenciesJar")
