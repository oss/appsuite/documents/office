/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.cleanup;

import java.net.InetSocketAddress;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.core.doc.HazelcastDocumentDirectory;
import com.openexchange.office.hazelcast.core.doc.HazelcastDocumentResourcesDirectory;
import com.openexchange.office.hazelcast.core.utils.DocRestoreIDMap;
import com.openexchange.office.hazelcast.serialization.document.PortableCleanupStatus;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentState;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.caching.DistributedMapnameDisposer;
import com.openexchange.office.tools.service.cluster.ClusterMembershipEvent;
import com.openexchange.office.tools.service.cluster.ClusterMembershipListener;
import com.openexchange.office.tools.service.cluster.ClusterService;

/**
 * {@link CleanupMembershipListener} - Reacts to a member removal by cleaning up resources that
 * have been located on that member.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 *
 */
@Service
public class CleanupMembershipListener implements ClusterMembershipListener, InitializingBean {
	
	private static final Logger log = LoggerFactory.getLogger(CleanupMembershipListener.class); 

	@Autowired
	private DistributedMapnameDisposer distributedMapnameDisposer;

	@Autowired
    private HazelcastDocumentDirectory directory;

	@Autowired
    private HazelcastDocumentResourcesDirectory resourceDir;

	@Autowired
	private ClusterService clusterService;
	
	@Autowired
	private CachingFacade cachingFacade;
	
    private String cleanupLockMapName;

    @Override
	public void afterPropertiesSet() throws Exception {
    	this.cleanupLockMapName = distributedMapnameDisposer.discoverMapName("officeCleanupLock-");
    	clusterService.addMembershipListener(this);
	}

	@Override
    public void memberAdded(ClusterMembershipEvent membershipEvent) {
        // nothing to do
    }

    @Override
    public void memberRemoved(ClusterMembershipEvent membershipEvent) {
        final String memberUid = membershipEvent.getMemberUid();
        final InetSocketAddress socketAddr = membershipEvent.getMemberSocketAddress();

        try {
            DistributedMap<String, PortableCleanupStatus> cleanupMapping = getCleanupMapping();

            cleanupMapping.lock(memberUid);
            try {
                PortableCleanupStatus cleanupStatus = cleanupMapping.get(memberUid);
                // is somebody already cleaning up for him?
                if (!cleanupDone(cleanupStatus)) {
                    final long nStartTime = System.currentTimeMillis();
                    log.debug("RT connection: Starting cleanup for member " + memberUid + " with IP " + socketAddr);
                    cleanupStatus = new PortableCleanupStatus(clusterService.getLocalMemberUuid(), memberUid);

                    // do actual cleanup
                    DocRestoreIDMap<DocumentState> docsOfMember = directory.getDocumentsOfMember(memberUid);
                    log.debug("RT connection: Found the following resources to clean up: " + docsOfMember.toString());

                    // just remove entries from both maps (document and resource state)
                    Collection<DocRestoreID> ids = docsOfMember.keySet();
                    directory.remove(ids);
                    resourceDir.remove(ids);

                    // update status and put to map
                    final long nEndTime = System.currentTimeMillis();
                    cleanupStatus.setCleaningFinishTime(nEndTime);
                    cleanupMapping.put(memberUid, cleanupStatus);
                    log.debug("RT connection: CleanupMapping after cleanup: " + cleanupMapping.entrySet());
                    log.debug("RT connection: Cleanup needed " + (nEndTime - nStartTime) + "ms");
                } else {
                    log.debug("RT connection: Cleanup was already started: " + cleanupStatus);
                }
            } catch (Exception e) {
                log.error("RT connection: Failed to start cleanup after member " + memberUid + " with IP " + socketAddr + " left the cluster", e);
            } finally {
                cleanupMapping.unlock(memberUid);
            }
        } catch (OXException oxe) {
            log.error("RT connection: Failed to start cleanup after member " + memberUid + " with IP " + socketAddr + " left the cluster", oxe);
        }
    }

    private DistributedMap<String, PortableCleanupStatus> getCleanupMapping() throws OXException {
        return cachingFacade.getDistributedMap(cleanupLockMapName, String.class, PortableCleanupStatus.class);
    }

    /**
     * Check the distributed memberCleanup map for a status indicating that some other member has already finished cleaning up.
     *
     * @param member may be null
     * @return true if member is not null, the cleanup was finished during the last run and the last run was not more than 5 minutes ago
     */
    private boolean cleanupDone(PortableCleanupStatus cleanupStatus) {
        if(cleanupStatus!= null) {
            long cleaningFinishTime = cleanupStatus.getCleaningFinishTime();
            if(cleaningFinishTime == -1) {
                //the cleanup wasn't finished
                return false;
            }
            long diff = System.currentTimeMillis() - cleaningFinishTime;
            if(diff > 300000) {
                //the last cleanup was more than 5 minutes ago, clean again!
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

}
