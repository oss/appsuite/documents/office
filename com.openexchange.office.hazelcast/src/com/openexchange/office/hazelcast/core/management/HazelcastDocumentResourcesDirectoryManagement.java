/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.core.doc.HazelcastDocumentResourcesDirectory;
import com.openexchange.office.hazelcast.serialization.document.PortableDocumentResources;
import com.openexchange.office.hazelcast.serialization.document.PortableRestoreID;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;


/**
* {@link HazelcastDocumentDirectoryManagement}
*
* @author <a href="mailto:marc.arens@open-xchange.com">Carsten Driesner</a>
*/
@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=HazelcastDocumentResourcesDirectory")
public class HazelcastDocumentResourcesDirectoryManagement {

    @Autowired
    private HazelcastDocumentResourcesDirectory resourceDirectory;

   /**
    * Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premuim/random <-> ResourceMap.
    *
    * @return the map used for mapping full IDs to ResourceMaps.
    * @throws OXException if the map couldn't be fetched from hazelcast
    */
    @JMXBeanAttribute(name="DocResourcesMapping", description="Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premuim/random <-> ResourceMap.")    
    public Map<String, String> getDocResourcesMapping() throws OXException {
    	DistributedMap<PortableRestoreID, PortableDocumentResources> resourceMapping = resourceDirectory.getDocResourcesMapping();
    	Map<String, String> jmxMap = new HashMap<String, String>(resourceMapping.size());
    	for (Entry<PortableRestoreID, PortableDocumentResources> entry : resourceMapping.entrySet()) {
    		PortableRestoreID concreteID = entry.getKey();
    		PortableDocumentResources resource = entry.getValue();
    		jmxMap.put(concreteID.toString(), resource.toString());
    	}
    	return jmxMap;
    }
}
