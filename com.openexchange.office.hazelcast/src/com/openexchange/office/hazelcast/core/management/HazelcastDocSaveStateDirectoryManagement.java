/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.core.doc.HazelcastDocSaveStateDirectory;
import com.openexchange.office.hazelcast.serialization.document.PortableDocSaveState;
import com.openexchange.office.hazelcast.serialization.document.PortableID;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;


/**
* {@link HazelcastDocumentDirectoryManagement}
*
* @author <a href="mailto:marc.arens@open-xchange.com">Carsten Driesner</a>
*/
@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=HazelcastDocSaveStateDirectory")
public class HazelcastDocSaveStateDirectoryManagement {

    @Autowired
    private HazelcastDocSaveStateDirectory resourceDirectory;

   /**
    * Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premuim/random <-> ResourceMap.
    *
    * @return the map used for mapping full IDs to ResourceMaps.
    * @throws OXException if the map couldn't be fetched from hazelcast
    */
    @JMXBeanAttribute(name="DocSaveStateMapping", description="Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premium/random <-> Resource. The resource includes the " + 
			  "RoutingInfo needed to address clients identified by the ID")
    public Map<String, String> getDocSaveStateMapping() throws OXException {
       DistributedMap<PortableID, PortableDocSaveState> resourceMapping = resourceDirectory.getDocSaveStateMapping();
       Map<String, String> jmxMap = new HashMap<String, String>(resourceMapping.size());
       for (Entry<PortableID, PortableDocSaveState> entry : resourceMapping.entrySet()) {
           PortableID concreteID = entry.getKey();
           PortableDocSaveState resource = entry.getValue();
           jmxMap.put(concreteID.toString(), resource.toString());
       }
       return jmxMap;
    }
}
