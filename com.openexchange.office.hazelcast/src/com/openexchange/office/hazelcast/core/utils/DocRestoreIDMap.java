/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.openexchange.office.tools.directory.DocRestoreID;

public class DocRestoreIDMap<T> implements Map<DocRestoreID, T> {

    protected final Map<DocRestoreID, T> delegate;

    /**
     * Initializes a new {@link IDMap}.
     */
    public DocRestoreIDMap() {
        super();
        delegate = new HashMap<DocRestoreID, T>();
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    public boolean containsKey(Object id) {
        return delegate.containsKey(id);
    }

    @Override
    public boolean containsValue(Object value) {
        return delegate.containsValue(value);
    }

    @Override
    public T get(Object key) {
        return delegate.get(key);
    }

    @Override
    public T put(DocRestoreID key, T value) {
        return delegate.put(key, value);
    }

    @Override
    public T remove(Object key) {
        return delegate.remove(key);
    }

    @Override
    public void putAll(Map<? extends DocRestoreID, ? extends T> m) {
        delegate.putAll(m);
    }

    @Override
    public void clear() {
        delegate.clear();
    }

    @Override
    public Set<DocRestoreID> keySet() {
        return delegate.keySet();
    }

    @Override
    public Collection<T> values() {
        return delegate.values();
    }

    @Override
    public Set<java.util.Map.Entry<DocRestoreID, T>> entrySet() {
        return delegate.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        return delegate.equals(o);
    }

    @Override
    public int hashCode() {
        return delegate.hashCode();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

}
