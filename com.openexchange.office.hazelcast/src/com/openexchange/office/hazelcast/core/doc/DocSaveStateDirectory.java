/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.doc;

import java.util.Collection;
import java.util.Map;

import com.openexchange.exception.OXException;
import com.openexchange.office.tools.directory.DocResourceID;
import com.openexchange.office.tools.directory.DocSaveState;

/**
* {@link DocSaveStateDirectory}
*
* Directory for document resources.
*
* @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
*/
public interface DocSaveStateDirectory {

   /**
    * Gets all available resources for the supplied ID, or an empty map if no matching resources are known. If the ID is in general
    * form, multiple entries may be returned, each mapped to their concrete ID. If the ID denotes a concrete resource, i.e. it's
    * resource-part is set, only one entry is returned if found.
    *
    * @param id The ID to lookup the status for
    * @return The resolved resources matching the supplied ID
    * @throws OXException
    */
   DocSaveState get(DocResourceID id) throws OXException;

   /**
    * Gets all available resources for the supplied IDs, or an empty map if no matching resources are known. For each supplied ID, if
    * it is in general form, multiple entries may be returned, each mapped to their concrete ID. If a supplied ID denotes a concrete
    * resource, i.e. it's resource-part is set, only one entry is returned for that ID if found.
    *
    * @param ids The IDs to lookup the status for
    * @return The resolved resources matching the supplied ID
    * @throws OXException
    */
   Map<DocResourceID, DocSaveState> get(Collection<DocResourceID> ids) throws OXException;

   /**
    * Sets or updates the presence data of a resource identified by the supplied ID.
    *
    * @param id The (concrete) resource ID to set the status for
    * @param resource The resource data to set
    * @return The previously associated resource of the ID in case of an update, <code>null</code> if there was no value associated
    *         with the ID before
    * @throws OXException
    */
   DocSaveState set(DocResourceID id, DocSaveState resource) throws OXException;

   /**
    * Sets or updates the presence data of a resource identified by the supplied ID if and only if no previous resource was set for this id.
    *
    * @param id The (concrete) resource ID to set the status for
    * @param resource The resource data to set
    * @return The previously associated resource of the ID in case of an update, <code>null</code> if there was no value associated
    *         with the ID before. If a non-null value is returned here, the resource directory has not saved the supplied value.
    * @throws OXException
    */
   void setIfAbsent(DocResourceID id, DocSaveState resource) throws OXException;

   /**
    * Removes all available resources for the supplied ID. If the ID is in general form, all matching entries are removed from the
    * directory. If the ID denotes a concrete resource, i.e. it's resource-part is set, only one entry is removed if found.
    *
    * @param id The ID to remove from the directory
    * @return All previously associated resources mapped to the ID, each mapped to the it's concrete resource ID.
    * @throws OXException
    */
   DocSaveState remove(DocResourceID id) throws OXException;

   /**
    * Removes all available resources for the supplied IDs. For each ID, if it is in general form, all matching entries are removed
    * from the directory. If an ID denotes a concrete resource, i.e. it's resource-part is set, only one entry is removed if found.
    *
    * @param ids The IDs to remove from the directory
    * @return All previously associated resource mapped to the IDs, each mapped to the it's concrete resource ID.
    * @throws OXException
    */
   Map<DocResourceID, DocSaveState> remove(Collection<DocResourceID> ids) throws OXException;

}
