/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.core.utils.DocRestoreIDMap;
import com.openexchange.office.hazelcast.serialization.document.PortableDocumentState;
import com.openexchange.office.hazelcast.serialization.document.PortableMemberPredicate;
import com.openexchange.office.hazelcast.serialization.document.PortableRestoreID;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentState;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.osgi.ExceptionUtils;


@Service
@RegisteredService(registeredClass=DocumentDirectory.class)
public class HazelcastDocumentDirectory extends HazelcastDocService implements DocumentDirectory  {

	private static final Logger log = LoggerFactory.getLogger(HazelcastDocumentDirectory.class);
    
	@Override
	public void afterPropertiesSet() throws Exception {
		mapIdentifier = distributedMapnameDisposer.discoverMapName("officeDocumentDirectory-");
		if(StringUtils.isEmpty(mapIdentifier)) {
		    final String msg = "Distributed documents persistent state directory map couldn't be found in hazelcast configuration";
		    throw new IllegalStateException(msg, new BundleException(msg, BundleException.ACTIVATOR_ERROR));
		}
	}

    @Override
    public DocumentState get(DocRestoreID id) throws OXException {
        DocumentState foundDoc = null;
        PortableRestoreID currPortableRestoreId = new PortableRestoreID(id);

        DistributedMap<PortableRestoreID, PortableDocumentState> allDocStates = getDocStateMapping();
        PortableDocumentState portableDoc = allDocStates.get(currPortableRestoreId);
        if (portableDoc != null) {
            foundDoc = PortableDocumentState.createFrom(portableDoc);
        }

        return foundDoc;
    }

    @Override
    public Map<DocRestoreID, DocumentState> get(Collection<DocRestoreID> ids) throws OXException {
        final Map<DocRestoreID, DocumentState> foundDocs = new HashMap<DocRestoreID, DocumentState>();
        final Set<PortableRestoreID> docResIds = new HashSet<PortableRestoreID>();

        for (DocRestoreID id : ids) {
            docResIds.add(new PortableRestoreID(id));
        }

        if (!docResIds.isEmpty()) {
        	DistributedMap<PortableRestoreID, PortableDocumentState> allResources = getDocStateMapping();
            Map<PortableRestoreID, PortableDocumentState> matchingPortableDocs = allResources.getAll(docResIds);
            if (matchingPortableDocs != null) {
                for (Entry<PortableRestoreID, PortableDocumentState> entry : matchingPortableDocs.entrySet()) {
                    final PortableRestoreID foundID = entry.getKey();
                    final PortableDocumentState foundDoc = entry.getValue();
                    final DocRestoreID docResId = PortableRestoreID.createFrom(foundID);
                    foundDocs.put(docResId, PortableDocumentState.createFrom(foundDoc));
                }
            }
        }

        return foundDocs;
    }

    @Override
    public DocumentState set(final DocRestoreID id, final DocumentState docState) throws OXException {
        final PortableDocumentState currentPortableDoc = new PortableDocumentState(docState);
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentState previousPortableDocState = null;
        DocumentState prevDocState = null;
        try {
            final DistributedMap<PortableRestoreID, PortableDocumentState> allDocStates = getDocStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState != null) {
                // but the previous resource provides a presence
                allDocStates.put(currentPortableID, currentPortableDoc);
            } else {
                previousPortableDocState = allDocStates.putIfAbsent(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocState) {
            prevDocState = PortableDocumentState.createFrom(previousPortableDocState);
        }

        return prevDocState;
    }

    @Override
    public void setIfAbsent(DocRestoreID id, DocumentState docState) throws OXException {
        final PortableDocumentState currentPortableDoc = new PortableDocumentState(docState);
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentState previousPortableDocState = null;
        try {
            final DistributedMap<PortableRestoreID, PortableDocumentState> allDocStates = getDocStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState == null) {
                allDocStates.put(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }
    }

    @Override
    public DocumentState remove(DocRestoreID id) throws OXException {
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentState previousPortableDocState = null;
        DocumentState prevDocState = null;
        try {
            final DistributedMap<PortableRestoreID, PortableDocumentState> allDocStates = getDocStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState != null) {
                allDocStates.remove(currentPortableID);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocState) {
            prevDocState = PortableDocumentState.createFrom(previousPortableDocState);
        }

        return prevDocState;
    }

    @Override
    public Map<DocRestoreID, DocumentState> remove(Collection<DocRestoreID> ids) throws OXException {
        final Map<DocRestoreID, DocumentState> removed = new HashMap<DocRestoreID, DocumentState>();
        final Set<PortableRestoreID> docResIds = new HashSet<PortableRestoreID>();

        for (DocRestoreID id : ids) {
            docResIds.add(new PortableRestoreID(id));
        }

        if (!docResIds.isEmpty()) {
        	DistributedMap<PortableRestoreID, PortableDocumentState> allResources = getDocStateMapping();
            Map<PortableRestoreID, PortableDocumentState> matchingPortableDocs = allResources.getAll(docResIds);

            if (matchingPortableDocs != null) {
                for (Entry<PortableRestoreID, PortableDocumentState> entry : matchingPortableDocs.entrySet()) {
                    final PortableRestoreID foundID = entry.getKey();
                    final PortableDocumentState foundDoc = entry.getValue();
                    removed.put(PortableRestoreID.createFrom(foundID), PortableDocumentState.createFrom(foundDoc));

                    allResources.remove(foundID);
                }
            }
        }

        return removed;
    }

    /**
     * Find all Resources in this directory that are located on a given member node.
     *
     * @param member The cluster member
     * @return all Resources in this directory that are located on the given member node.
     * @throws OXException
     */
    public DocRestoreIDMap<DocumentState> getDocumentsOfMember(String memberUid)throws OXException {
    	DistributedMap<PortableRestoreID, PortableDocumentState> allResources = getDocStateMapping();
        PortableMemberPredicate memberPredicate = new PortableMemberPredicate(memberUid);
        Set<Entry<PortableRestoreID, PortableDocumentState>> matchingResources = allResources.entrySet(memberPredicate);

        DocRestoreIDMap<DocumentState> foundIds = new DocRestoreIDMap<DocumentState>();
        Iterator<Entry<PortableRestoreID, PortableDocumentState>> iterator = matchingResources.iterator();
        while(iterator.hasNext()) {
            try {
                Entry<PortableRestoreID, PortableDocumentState> next = iterator.next();

                foundIds.put(PortableRestoreID.createFrom(next.getKey()),
                             PortableDocumentState.createFrom(next.getValue()));
            } catch (Exception e) {
                log.error("Couldn't add resource that was found for member " + memberUid, e);
            }
        }
        return foundIds;
    }

    /**
     * Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premium/random <-> Resource. The resource includes the
     * {@link RoutingInfo} needed to address clients identified by the {@link ID}
     *
     * @return the map used for mapping full IDs to ResourceMaps.
     * @throws OXException if the map couldn't be fetched from hazelcast
     */
    public DistributedMap<PortableRestoreID, PortableDocumentState> getDocStateMapping() throws OXException {
        return cachingFacade.getDistributedMap(mapIdentifier, PortableRestoreID.class, PortableDocumentState.class);
    }
}
