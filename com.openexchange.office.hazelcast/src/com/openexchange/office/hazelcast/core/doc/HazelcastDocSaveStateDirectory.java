/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.serialization.document.PortableDocSaveState;
import com.openexchange.office.hazelcast.serialization.document.PortableID;
import com.openexchange.office.tools.directory.DocResourceID;
import com.openexchange.office.tools.directory.DocSaveState;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.osgi.ExceptionUtils;


@Service
public class HazelcastDocSaveStateDirectory extends HazelcastDocService implements DocSaveStateDirectory {

	private static final Logger log = LoggerFactory.getLogger(HazelcastDocSaveStateDirectory.class);
		
	@Override
	public void afterPropertiesSet() throws Exception {
		mapIdentifier = distributedMapnameDisposer.discoverMapName("officeDocSaveStateDirectory-");
		if(StringUtils.isEmpty(mapIdentifier)) {
		    final String msg = "Distributed document save state directory map couldn't be found in hazelcast configuration";
		    throw new IllegalStateException(msg, new BundleException(msg, BundleException.ACTIVATOR_ERROR));
		}
		log.debug("Registered officeDocSaveStateDirectory to Hazelcast");
	}
    
    @Override
    public DocSaveState get(DocResourceID id) throws OXException {
        DocSaveState foundDoc = null;
        PortableID currentPortableId = new PortableID(id);

        DistributedMap<PortableID, PortableDocSaveState> allDocStates = getDocSaveStateMapping();
        PortableDocSaveState portableDoc = allDocStates.get(currentPortableId);
        if (portableDoc != null) {
            foundDoc = PortableDocSaveState.createFrom(portableDoc);
        }

        return foundDoc;
    }

    @Override
    public Map<DocResourceID, DocSaveState> get(Collection<DocResourceID> ids) throws OXException {
        final Map<DocResourceID, DocSaveState> foundDocs = new HashMap<DocResourceID, DocSaveState>();
        final Set<PortableID> docResIds = new HashSet<PortableID>();

        for (DocResourceID id : ids) {
            docResIds.add(new PortableID(id));
        }

        if (!docResIds.isEmpty()) {
        	DistributedMap<PortableID, PortableDocSaveState> allResources = getDocSaveStateMapping();
            Map<PortableID, PortableDocSaveState> matchingPortableDocs = allResources.getAll(docResIds);
            if (matchingPortableDocs != null) {
                for (Entry<PortableID, PortableDocSaveState> entry : matchingPortableDocs.entrySet()) {
                    final PortableID foundID = entry.getKey();
                    final PortableDocSaveState foundDoc = entry.getValue();
                    final DocResourceID docResId = DocResourceID.createDocResourceID(foundID.toString());
                    foundDocs.put(docResId, PortableDocSaveState.createFrom(foundDoc));
                }
            }
        }

        return foundDocs;
    }

    @Override
    public DocSaveState set(final DocResourceID id, final DocSaveState docState) throws OXException {
        final PortableDocSaveState currentPortableDoc = new PortableDocSaveState(docState);
        final PortableID currentPortableID = new PortableID(id);

        PortableDocSaveState previousPortableDocState = null;
        DocSaveState prevDocState = null;
        try {
            final DistributedMap<PortableID, PortableDocSaveState> allDocStates = getDocSaveStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState != null) {
                // but the previous resource provides a presence
                allDocStates.put(currentPortableID, currentPortableDoc);
            } else {
                previousPortableDocState = allDocStates.putIfAbsent(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocState) {
            prevDocState = PortableDocSaveState.createFrom(previousPortableDocState);
        }

        return prevDocState;
    }

    @Override
    public void setIfAbsent(DocResourceID id, DocSaveState docState) throws OXException {
        final PortableDocSaveState currentPortableDoc = new PortableDocSaveState(docState);
        final PortableID currentPortableID = new PortableID(id);

        PortableDocSaveState previousPortableDocState = null;
        try {
            final DistributedMap<PortableID, PortableDocSaveState> allDocStates = getDocSaveStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState == null) {
                allDocStates.put(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }
    }

    @Override
    public DocSaveState remove(DocResourceID id) throws OXException {
        final PortableID currentPortableID = new PortableID(id);

        PortableDocSaveState previousPortableDocState = null;
        DocSaveState prevDocState = null;
        try {
            final DistributedMap<PortableID, PortableDocSaveState> allDocStates = getDocSaveStateMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState == null) {
                allDocStates.remove(currentPortableID);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocState) {
            prevDocState = PortableDocSaveState.createFrom(previousPortableDocState);
        }

        return prevDocState;
    }

    @Override
    public Map<DocResourceID, DocSaveState> remove(Collection<DocResourceID> ids) throws OXException {
        final Map<DocResourceID, DocSaveState> removed = new HashMap<DocResourceID, DocSaveState>();
        final Set<PortableID> docResIds = new HashSet<PortableID>();

        for (DocResourceID id : ids) {
            docResIds.add(new PortableID(id));
        }

        if (!docResIds.isEmpty()) {
            DistributedMap<PortableID, PortableDocSaveState> allResources = getDocSaveStateMapping();
            Map<PortableID, PortableDocSaveState> matchingPortableDocs = allResources.getAll(docResIds);

            if (matchingPortableDocs != null) {
                for (Entry<PortableID, PortableDocSaveState> entry : matchingPortableDocs.entrySet()) {
                    final PortableID foundID = entry.getKey();
                    final PortableDocSaveState foundDoc = entry.getValue();
                    final DocResourceID docResId = DocResourceID.createDocResourceID(foundID.toString());
                    removed.put(docResId, PortableDocSaveState.createFrom(foundDoc));

                    allResources.remove(foundID);
                }
            }
        }

        return removed;
    }

    /**
     * Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premium/random <-> Resource. The resource includes the
     * {@link RoutingInfo} needed to address clients identified by the {@link ID}
     *
     * @return the map used for mapping full IDs to ResourceMaps.
     * @throws OXException if the map couldn't be fetched from hazelcast
     */
    public DistributedMap<PortableID, PortableDocSaveState> getDocSaveStateMapping() throws OXException {
    	DistributedMap<PortableID, PortableDocSaveState> res = cachingFacade.getDistributedMap(mapIdentifier, PortableID.class, PortableDocSaveState.class);
    	return res;
    }
}
