/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.core.doc;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.hazelcast.serialization.document.PortableDocumentResources;
import com.openexchange.office.hazelcast.serialization.document.PortableRestoreID;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentResources;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.osgi.ExceptionUtils;


/**
 * Implementation of a Hazelcast map which stores resource
 * data associated with documents.
 *
 * @author Carsten Driesner
 * @since 7.8.1
 *
 */
@Service
@RegisteredService(registeredClass=DocumentResourcesDirectory.class)
public class HazelcastDocumentResourcesDirectory extends HazelcastDocService implements DocumentResourcesDirectory {

	private static final Logger log = LoggerFactory.getLogger(HazelcastDocumentResourcesDirectory.class);
	
    @Override
	public void afterPropertiesSet() throws Exception {
    	this.mapIdentifier = distributedMapnameDisposer.discoverMapName("officeDocumentResources-");
    	if(StringUtils.isEmpty(mapIdentifier)) {
    	    final String msg = "Distributed document resources directory map couldn't be found in hazelcast configuration";
    	    throw new IllegalStateException(msg, new BundleException(msg, BundleException.ACTIVATOR_ERROR));
    	}
    	log.debug("Registered officeDocumentResources to Hazelcast");
	}



	@Override
    public DocumentResources get(DocRestoreID id) throws OXException {
        DocumentResources foundDoc = null;
        PortableRestoreID currentPortableId = new PortableRestoreID(id);

        DistributedMap<PortableRestoreID, PortableDocumentResources> allDocResources = getDocResourcesMapping();
        PortableDocumentResources portableDoc = allDocResources.get(currentPortableId);
        if (portableDoc != null) {
            foundDoc = PortableDocumentResources.createFrom(portableDoc);
        }

        return foundDoc;
    }

    @Override
    public Map<DocRestoreID, DocumentResources> get(Collection<DocRestoreID> ids) throws OXException {
        final Map<DocRestoreID, DocumentResources> foundDocs = new HashMap<DocRestoreID, DocumentResources>();
        final Set<PortableRestoreID> docResIds = new HashSet<PortableRestoreID>();

        for (DocRestoreID id : ids) {
            docResIds.add(new PortableRestoreID(id));
        }

        if (!docResIds.isEmpty()) {
        	DistributedMap<PortableRestoreID, PortableDocumentResources> allResources = getDocResourcesMapping();
            Map<PortableRestoreID, PortableDocumentResources> matchingPortableDocs = allResources.getAll(docResIds);
            if (matchingPortableDocs != null) {
                for (Entry<PortableRestoreID, PortableDocumentResources> entry : matchingPortableDocs.entrySet()) {
                    final PortableRestoreID foundID = entry.getKey();
                    final PortableDocumentResources foundDoc = entry.getValue();
                    final DocRestoreID docResId = PortableRestoreID.createFrom(foundID);
                    foundDocs.put(docResId, PortableDocumentResources.createFrom(foundDoc));
                }
            }
        }

        return foundDocs;
    }

    @Override
    public DocumentResources set(final DocRestoreID id, final DocumentResources docResources) throws OXException {
        final PortableDocumentResources currentPortableDoc = new PortableDocumentResources(docResources);
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentResources previousPortableDocRes = null;
        DocumentResources prevDocRes = null;

        try {
            final DistributedMap<PortableRestoreID, PortableDocumentResources> allDocStates = getDocResourcesMapping();
            previousPortableDocRes = allDocStates.get(currentPortableID);

            if (previousPortableDocRes != null) {
                // but the previous resource provides a presence
                allDocStates.put(currentPortableID, currentPortableDoc);
            } else {
                previousPortableDocRes = allDocStates.putIfAbsent(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocRes) {
            prevDocRes = PortableDocumentResources.createFrom(previousPortableDocRes);
        }

        return prevDocRes;
    }

    @Override
    public void setIfAbsent(DocRestoreID id, DocumentResources docResources) throws OXException {
        final PortableDocumentResources currentPortableDoc = new PortableDocumentResources(docResources);
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentResources previousPortableDocState = null;
        try {
            final DistributedMap<PortableRestoreID, PortableDocumentResources> allDocStates = getDocResourcesMapping();
            previousPortableDocState = allDocStates.get(currentPortableID);

            if (previousPortableDocState == null) {
                allDocStates.put(currentPortableID, currentPortableDoc);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }
    }

    @Override
    public DocumentResources remove(DocRestoreID id) throws OXException {
        final PortableRestoreID currentPortableID = new PortableRestoreID(id);

        PortableDocumentResources previousPortableDocRes = null;
        DocumentResources prevDocRes = null;
        try {
            final DistributedMap<PortableRestoreID, PortableDocumentResources> allDocResources = getDocResourcesMapping();
            previousPortableDocRes = allDocResources.get(currentPortableID);

            if (previousPortableDocRes != null) {
                allDocResources.remove(currentPortableID);
            }
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != previousPortableDocRes) {
            prevDocRes = PortableDocumentResources.createFrom(previousPortableDocRes);
        }

        return prevDocRes;
    }

    @Override
    public Map<DocRestoreID, DocumentResources> remove(Collection<DocRestoreID> ids) throws OXException {
        final Map<DocRestoreID, DocumentResources> removed = new HashMap<DocRestoreID, DocumentResources>();
        final Set<PortableRestoreID> docResIds = new HashSet<PortableRestoreID>();

        for (DocRestoreID id : ids) {
            docResIds.add(new PortableRestoreID(id));
        }

        if (!docResIds.isEmpty()) {
        	DistributedMap<PortableRestoreID, PortableDocumentResources> allResources = getDocResourcesMapping();
            Map<PortableRestoreID, PortableDocumentResources> matchingPortableDocs = allResources.getAll(docResIds);

            if (matchingPortableDocs != null) {
                for (Entry<PortableRestoreID, PortableDocumentResources> entry : matchingPortableDocs.entrySet()) {
                    final PortableRestoreID foundID = entry.getKey();
                    final PortableDocumentResources foundDocRes = entry.getValue();
                    final DocRestoreID docResId = PortableRestoreID.createFrom(foundID);
                    removed.put(docResId, PortableDocumentResources.createFrom(foundDocRes));

                    allResources.remove(foundID);
                }
            }
        }

        return removed;
    }

    /**
     * Get the mapping of full IDs to the Resource e.g. ox://marc.arens@premium/random <-> Resource. The resource includes the
     * {@link RoutingInfo} needed to address clients identified by the {@link ID}
     *
     * @return the map used for mapping full IDs to ResourceMaps.
     * @throws OXException if the map couldn't be fetched from hazelcast
     */
    public DistributedMap<PortableRestoreID, PortableDocumentResources> getDocResourcesMapping() throws OXException {
        return cachingFacade.getDistributedMap(mapIdentifier, PortableRestoreID.class, PortableDocumentResources.class);
    }
}
