/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.wml.CTBookmark;
import org.docx4j.wml.CTMarkupRange;
import org.docx4j.wml.CommentRangeEnd;
import org.docx4j.wml.CommentRangeStart;
import org.docx4j.wml.RunDel;
import org.docx4j.wml.RunIns;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;

public class RunMoveToContext extends ComponentContext<OfficeOpenXMLOperationDocument> {

    public RunMoveToContext(OfficeOpenXMLOperationDocument operationDocument, DLNode<Object> n) {
        super(operationDocument, n);
    }

    public RunMoveToContext(ComponentContext<OfficeOpenXMLOperationDocument> p, DLNode<Object> n) {
        super(p, n);
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> runInsNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)runInsNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = OfficeOpenXMLComponent.getContentModel(childNode, runInsNode.getData());
            if(TextRunContext.isRepresentableTextRunContext(o)) {
                final TextRunContext textRunContext = new TextRunContext(this, childNode);
                nextComponent = textRunContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunIns) {
                final RunInsContext runInsContext = new RunInsContext(this, childNode);
                nextComponent = runInsContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunDel) {
                final RunDelContext runDelContext = new RunDelContext(this, childNode);
                nextComponent = runDelContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof CommentRangeStart) {
            	nextComponent = new CommentRangeStartComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CommentRangeEnd) {
            	nextComponent = new CommentRangeEndComponent(this, childNode, nextComponentNumber);
            }
            else if(o.getClass()==CTBookmark.class) {
                nextComponent = new BookmarkStartComponent(this, childNode, nextComponentNumber);
            }
            else if(o.getClass()==CTMarkupRange.class) {
                nextComponent = new BookmarkEndComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }
}
