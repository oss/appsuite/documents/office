/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.Iterator;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTLanguage;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTVerticalAlignRun;
import org.docx4j.wml.Color;
import org.docx4j.wml.Highlight;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.RPrBase;
import org.docx4j.wml.STVerticalAlignRun;
import org.docx4j.wml.U;
import org.docx4j.wml.UnderlineEnumeration;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class Character {

    public static void applyCharacterProperties(DocxOperationDocument operationDocument, JSONObject characterProperties, RPrBase rPr )
        throws FilterException, JSONException {

        if(characterProperties==null)
            return;

        Iterator<String> keys = characterProperties.keys();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = characterProperties.get(attr);
            if(attr.equals(OCKey.BOLD.value())) {
                if (value instanceof Boolean) {
                    final BooleanDefaultTrue booleanDefaultTrue = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                    booleanDefaultTrue.setVal(((Boolean) value).booleanValue());
                    rPr.setB(booleanDefaultTrue);
                }
                else
                    rPr.setB(null);
            }
            else if(attr.equals(OCKey.ITALIC.value())) {
                if (value instanceof Boolean) {
                    final BooleanDefaultTrue booleanDefaultTrue = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                    booleanDefaultTrue.setVal(((Boolean) value).booleanValue());
                    rPr.setI(booleanDefaultTrue);
                }
                else
                    rPr.setI(null);
            }
            else if(attr.equals(OCKey.UNDERLINE.value())){
                if (value instanceof Boolean) {
                    final U u = Context.getWmlObjectFactory().createU();
                    u.setVal(((Boolean) value).booleanValue() ? UnderlineEnumeration.SINGLE : UnderlineEnumeration.NONE);
                    rPr.setU(u);
                }
                else rPr.setU(null);
            }
            else if(attr.equals(OCKey.FONT_NAME.value())) {
                if (value instanceof String) {
                    rPr.setrFonts(operationDocument.getThemeFonts().setFont((String)value));
                }
                else {
                    rPr.setrFonts(null);
                }
            }
            else if(attr.equals(OCKey.FONT_SIZE.value())) {
                if (value instanceof Number) {
                    HpsMeasure size = rPr.getSz();
                    if (size==null){
                        size = Context.getWmlObjectFactory().createHpsMeasure();
                        rPr.setSz(size);
                    }
                    size.setVal(Double.valueOf((((Number)value).doubleValue()*2)+0.50).longValue());
                }
                else
                    rPr.setSz(null);
            }
            else if(attr.equals(OCKey.COLOR.value())) {
                if(value instanceof JSONObject) {
                    Utils.initColorFromJSONColor(operationDocument, (JSONObject)value, rPr.getColor(true));
                }
                else
                    rPr.setColor(null);
            }
            else if (attr.equals(OCKey.FILL_COLOR.value())) {
                if (value instanceof JSONObject) {
                    JSONObject shdObject = (JSONObject)value;
                    Utils.initShdFromJSONColor(operationDocument, shdObject, rPr.getShd(true));
                    Highlight highlight = rPr.getHighlight();
                    if (highlight!=null) {
                        // reset possible highlight
                        rPr.setHighlight(null);
                    }
                }
                else {
                    rPr.setShd(null);
                    rPr.setHighlight(null);
                }
            }
            else if (attr.equals(OCKey.NO_PROOF.value())) {
            	if(value instanceof Boolean) {
            		final BooleanDefaultTrue noProof = Context.getWmlObjectFactory().createBooleanDefaultTrue();
            		noProof.setVal(((Boolean)value).booleanValue());
            	}
            	else {
            		rPr.setNoProof(null);
            	}
            }
            else if(attr.equals(OCKey.LANGUAGE.value())){
                if(value instanceof String){
                	rPr.getLang(true).setVal((String)value);
                }
                else {
                	final CTLanguage lang = rPr.getLang(false);
                    if(lang!=null) {
                        lang.setVal(null);
                    }
                }
            }
            else if(attr.equals(OCKey.LANGUAGE_EA.value())){
                if( value instanceof String){
                	rPr.getLang(true).setEastAsia((String)value);
                }
                else {
                	final CTLanguage lang = rPr.getLang(false);
                    if(lang!=null) {
                        lang.setEastAsia(null);
                    }
                }
            }
            else if(attr.equals(OCKey.LANGUAGE_BIDI.value())){
                if(value instanceof String){
                	rPr.getLang(true).setBidi((String)value);
                }
                else {
                    final CTLanguage lang = rPr.getLang(false);
                    if(lang!=null) {
                        lang.setBidi(null);
                    }
                }
            }
            else if(attr.equals(OCKey.VERT_ALIGN.value())){
                if( value instanceof String){
                    CTVerticalAlignRun vertAlign = rPr.getVertAlign();
                    if(vertAlign == null){
                        vertAlign = Context.getWmlObjectFactory().createCTVerticalAlignRun();
                        rPr.setVertAlign(vertAlign);
                    }
                    String align = (String)value;
                    if( align.equals("sub"))
                        vertAlign.setVal(STVerticalAlignRun.SUBSCRIPT);
                    else if( align.equals("super"))
                        vertAlign.setVal(STVerticalAlignRun.SUPERSCRIPT);
                    else
                        vertAlign.setVal(STVerticalAlignRun.BASELINE);
                }
                else {
                    rPr.setVertAlign(null);
                }
            }
            else if(attr.equals(OCKey.STRIKE.value())) {
                if (value instanceof String) {
                    final BooleanDefaultTrue booleanDefaultTrue = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                    String sValue = (String)value;
                    boolean isNone = sValue.equals("none");
                    booleanDefaultTrue.setVal(!isNone);
                    if(sValue.equals("single")){
                        rPr.setStrike(booleanDefaultTrue);
                        rPr.setDstrike(null);
                    }
                    else if( sValue.equals("double") ) {
                        rPr.setDstrike(booleanDefaultTrue);
                        rPr.setStrike(null);
                    } else {
                        if( rPr.getDstrike() != null )
                            rPr.setDstrike(null);
                        rPr.setStrike(booleanDefaultTrue);
                    }
                }
                else {
                    rPr.setStrike(null);
                    rPr.setDstrike(null);
                }
            }
            else if(attr.equals(OCKey.CAPS.value())) {
            	if(value instanceof String) {
            		if(((String)value).equals("all")) {
            			rPr.setSmallCaps(null);
                        final BooleanDefaultTrue booleanDefaultTrue = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                        booleanDefaultTrue.setVal(true);
            			rPr.setCaps(booleanDefaultTrue);
            		}
            		else if (((String)value).equals("small")) {
            			rPr.setCaps(null);
                        final BooleanDefaultTrue booleanDefaultTrue = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                        booleanDefaultTrue.setVal(true);
            			rPr.setSmallCaps(booleanDefaultTrue);
            		}
            		else if (((String)value).equals("none")) {
            			rPr.setCaps(null);
            			rPr.setSmallCaps(null);
            		}
            	}
            	else {
            		rPr.setCaps(null);
            		rPr.setSmallCaps(null);
            	}
            }
        }
    }

    public static JSONObject createCharacterProperties(DocxOperationDocument operationDocument, RPrBase characterProperties)
        throws JSONException {

        JSONObject jsonCharacterProperties = null;
        if(characterProperties!=null) {
            jsonCharacterProperties = new JSONObject();
            BooleanDefaultTrue bold = characterProperties.getB();
            if(bold!=null)
                jsonCharacterProperties.put(OCKey.BOLD.value(), bold.isVal());
            BooleanDefaultTrue italic = characterProperties.getI();
            if(italic!=null)
                jsonCharacterProperties.put(OCKey.ITALIC.value(), italic.isVal());
            U u = characterProperties.getU();
            if(u!=null) {
                boolean bUnderline = false;
                // docs-3434: this fix should be sufficient (to fully support this feature access to parent paragraph level is required.
                // the specification: u.getVal(), If this attribute is omitted, its value is determined by the setting previously set at any level
                // of the style hierarchy. If this setting is never specified in the style hierarchy, then its value shall be "auto")
                if (u.getVal()!=null && u.getVal()!=UnderlineEnumeration.NONE) {
                    bUnderline = true;
                }
                jsonCharacterProperties.put(OCKey.UNDERLINE.value(), bUnderline);
            }
            Commons.jsonPut(jsonCharacterProperties, OCKey.FONT_NAME.value(), operationDocument.getThemeFonts().getFont(characterProperties.getrFonts(false)));
            HpsMeasure size = characterProperties.getSz();
            if(size!=null) {
                jsonCharacterProperties.put(OCKey.FONT_SIZE.value(), size.getVal()>>>1);
            }
            Color color = characterProperties.getColor(false);
            if(color!=null)
                Commons.jsonPut(jsonCharacterProperties, OCKey.COLOR.value(), Utils.createColor(color));
            Highlight highlight = characterProperties.getHighlight();
            if(highlight!=null){
                Commons.jsonPut(
                    jsonCharacterProperties,
                    OCKey.FILL_COLOR.value(),
                    Utils.createColor(null, Commons.mapHightlightColorToRgb(highlight.getVal())));
            }
            else {
                final CTShd shd = characterProperties.getShd(false);
                if(shd!=null) {
                    Commons.jsonPut(
                        jsonCharacterProperties,
                        OCKey.FILL_COLOR.value(),
                        Utils.createFillColor(shd));
                }
            }
            final BooleanDefaultTrue noProof = characterProperties.getNoProof();
            if(noProof != null) {
            	jsonCharacterProperties.put(OCKey.NO_PROOF.value(), noProof.isVal());
            }
            final CTLanguage lang = characterProperties.getLang(false);
            if(lang!= null){
                if(lang.getVal()!=null) {
                    jsonCharacterProperties.put(OCKey.LANGUAGE.value(), lang.getVal());
                    operationDocument.getUsedLanguages().add(lang.getVal());
                }
                if(lang.getEastAsia()!=null) {
                    jsonCharacterProperties.put(OCKey.LANGUAGE_EA.value(), lang.getEastAsia());
                    operationDocument.getUsedLanguagesEA().add(lang.getEastAsia());
                }
                if(lang.getBidi()!=null) {
                    jsonCharacterProperties.put(OCKey.LANGUAGE_BIDI.value(), lang.getBidi());
                    operationDocument.getUsedLanguagesBidi().add(lang.getBidi());
                }
            }
            final CTVerticalAlignRun vertAlign = characterProperties.getVertAlign();
            if(vertAlign != null){
                STVerticalAlignRun eAlign = vertAlign.getVal();
                jsonCharacterProperties.put(OCKey.VERT_ALIGN.value(), eAlign == STVerticalAlignRun.SUBSCRIPT ? "sub" :
                        eAlign == STVerticalAlignRun.SUPERSCRIPT ? "super" : "baseline");
            }
            final BooleanDefaultTrue strike = characterProperties.getStrike();
            if(strike != null) {
                jsonCharacterProperties.put(OCKey.STRIKE.value(), strike.isVal() ? "single" : "none");
            }
            final BooleanDefaultTrue dstrike = characterProperties.getDstrike();
            if(dstrike != null) {
                jsonCharacterProperties.put(OCKey.STRIKE.value(), dstrike.isVal() ? "double" : "none");
            }
            final BooleanDefaultTrue caps = characterProperties.getCaps();
            if(caps!=null&&caps.isVal()) {
            	jsonCharacterProperties.put(OCKey.CAPS.value(), "all");
            }
            else {
            	final BooleanDefaultTrue smallCaps = characterProperties.getSmallCaps();
            	if(smallCaps!=null&&smallCaps.isVal()) {
            		jsonCharacterProperties.put(OCKey.CAPS.value(), "small");
            	}
            }
        }
        return jsonCharacterProperties!=null&&jsonCharacterProperties.length()>0 ? jsonCharacterProperties : null;
    }
}
