/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.operations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import jakarta.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.TreeMap;
import org.apache.commons.lang3.tuple.Triple;
import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.FooterPart;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.docx4j.w15.CTPeople;
import org.docx4j.w15.CTPerson;
import org.docx4j.w15.CTPresenceInfo;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTBookmark;
import org.docx4j.wml.CTLanguage;
import org.docx4j.wml.CTMarkupRange;
import org.docx4j.wml.CTRel;
import org.docx4j.wml.CTSettings;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTTabStop;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.CTTblStylePr;
import org.docx4j.wml.CommentRangeEnd;
import org.docx4j.wml.CommentRangeStart;
import org.docx4j.wml.Comments;
import org.docx4j.wml.Comments.Comment;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.FooterReference;
import org.docx4j.wml.HdrFtrRef;
import org.docx4j.wml.HeaderReference;
import org.docx4j.wml.InstrText;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.Lvl;
import org.docx4j.wml.NumFmt;
import org.docx4j.wml.NumberFormat;
import org.docx4j.wml.Numbering;
import org.docx4j.wml.Numbering.AbstractNum;
import org.docx4j.wml.Numbering.Num;
import org.docx4j.wml.Numbering.Num.AbstractNumId;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase.Ind;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.RStyle;
import org.docx4j.wml.STFldCharType;
import org.docx4j.wml.STPageOrientation;
import org.docx4j.wml.STTblStyleOverrideType;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.SectPr.PgSz;
import org.docx4j.wml.Style;
import org.docx4j.wml.Styles;
import org.docx4j.wml.Tabs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IParagraph;
import com.openexchange.office.filter.ooxml.components.IRow;
import com.openexchange.office.filter.ooxml.components.ITable;
import com.openexchange.office.filter.ooxml.components.MultiComponent;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.FldCharBegin;
import com.openexchange.office.filter.ooxml.docx.components.FldCharSeparate;
import com.openexchange.office.filter.ooxml.docx.tools.Paragraph;
import com.openexchange.office.filter.ooxml.docx.tools.Table;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;
import com.openexchange.office.filter.ooxml.operations.ApplyOperationHelper;

public class DocxApplyOperationHelper extends ApplyOperationHelper {

    final private static Logger log = LoggerFactory.getLogger(DocxApplyOperationHelper.class);
    final private static ImmutableMap<String, String> conditionalTableAttrNames = ImmutableMap.<String, String> builder()
        .put(OCKey.WHOLE_TABLE.value(), "wholeTable")
        .put(OCKey.FIRST_ROW.value(), "firstRow")
        .put(OCKey.LAST_ROW.value(), "lastRow")
        .put(OCKey.FIRST_COL.value(), "firstCol")
        .put(OCKey.LAST_COL.value(), "lastCol")
        .put(OCKey.BAND1_VERT.value(), "band1Vert")
        .put(OCKey.BAND2_VERT.value(), "band2Vert")
        .put(OCKey.BAND1_HORZ.value(), "band1Horz")
        .put(OCKey.BAND2_HORZ.value(), "band2Horz")
        .put(OCKey.NORTH_EAST_CELL.value(), "neCell")
        .put(OCKey.NORTH_WEST_CELL.value(), "nwCell")
        .put(OCKey.SOUTH_EAST_CELL.value(), "seCell")
        .put(OCKey.SOUTH_WEST_CELL.value(), "swCell")
        .build();

    private final DocxOperationDocument operationDocument;
    private final WordprocessingMLPackage wordMLPackage;
    private final ObjectFactory objectFactory;

    public DocxApplyOperationHelper(DocxOperationDocument _operationDocument) {
        super();

        operationDocument = _operationDocument;
        wordMLPackage = _operationDocument.getPackage();
        objectFactory = Context.getWmlObjectFactory();
    }

    @Override
    public DocxOperationDocument getOperationDocument() {
        return operationDocument;
    }

    /**
     * moving components, only DrawingComponents are supported
     *
     * @param startPosition
     * @param endPosition
     *
     */

    public void move(JSONArray startPosition, JSONArray endPosition, JSONArray toPosition)
        throws JSONException {

        OfficeOpenXMLComponent.move(getOperationDocument(), startPosition, endPosition, toPosition);
    }

    public void insertParagraph(JSONArray start, JSONObject attrs) throws Exception {

        getOperationDocument().getRootComponent().getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
    }

    public void splitParagraph(JSONArray start)
        throws JSONException {

        ((IParagraph)getOperationDocument().getRootComponent().getComponent(start, start.length()-1)).splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start) {

        ((IParagraph)getOperationDocument().getRootComponent().getComponent(start, start.length())).mergeParagraph();
    }

    public void insertText(JSONArray start, String text, JSONObject attrs) throws Exception {

        if(text.length()>0) {
            final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length()-1);
            ((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
        }
    }

    public void insertTab(JSONArray start, JSONObject attrs) throws Exception {

        getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TAB);
    }

    public void insertHardBreak(JSONArray start, String type, JSONObject attrs) throws Exception {

        ComponentType textRunChild = ComponentType.HARDBREAK_DEFAULT;
        if(type.equals("page")) {
            textRunChild = ComponentType.HARDBREAK_PAGE;
        }
        else if(type.equals("column")) {
            textRunChild = ComponentType.HARDBREAK_COLUMN;
        }
        getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, textRunChild);
    }

    public void insertDrawing(JSONArray start, String type, JSONObject attrs) throws Exception {

        ComponentType childComponentType = ComponentType.AC_IMAGE;
        if(type.equals("shape")) {
            // we will create an AlternateContent for our shape (drawingML + VML)
            childComponentType = ComponentType.AC_SHAPE;
        }
        else if(type.equals("connector")) {
            childComponentType = ComponentType.AC_CONNECTOR;
        }
        else if(type.equals("group")) {
            // we will create an AlternateContent for our group (drawingML + VML)
            childComponentType = ComponentType.AC_GROUP;
        }
        else if(type.equals("image")) {
            // we will create an AlternateContent for our group (drawingML + VML)
            childComponentType = ComponentType.AC_IMAGE;
        }
        getOperationDocument().getRootComponent().getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, childComponentType);
    }

    public void insertTable(JSONArray start, JSONObject attrs) throws Exception {

        getOperationDocument().getRootComponent().getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TABLE);
    }

    public void splitTable(JSONArray start)
        throws JSONException {

        ((ITable)getOperationDocument().getRootComponent().getComponent(start, start.length()-1)).splitTable(start.getInt(start.length()-1));
    }

    public void mergeTable(JSONArray start) {

        ((ITable)getOperationDocument().getRootComponent().getComponent(start, start.length())).mergeTable();
    }

    public void insertRows(JSONArray start, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

        ((ITable)getOperationDocument().getRootComponent().getComponent(start, start.length()-1))
            .insertRows(start.getInt(start.length()-1), count, insertDefaultCells, referenceRow, attrs);
    }

    public void insertCells(JSONArray start, int count, JSONObject attrs) throws Exception {

        ((IRow)getOperationDocument().getRootComponent().getComponent(start, start.length()-1)).insertCells(start.getInt(start.length()-1), count, attrs);
    }

    public void insertColumn(JSONArray start, JSONArray tableGrid, int gridPosition, String insertMode)
        throws JSONException {

        ((ITable)getOperationDocument().getRootComponent().getComponent(start, start.length())).insertColumn(tableGrid, gridPosition, insertMode);
    }

    public void deleteColumns(JSONArray position, int gridStart, int gridEnd)
        throws JSONException {

        ((ITable)getOperationDocument().getRootComponent().getComponent(position, position.length())).deleteColumns(gridStart, gridEnd);
    }

    public void setAttributes(JSONObject attrs, JSONArray start, JSONArray end) throws Exception {

        if(attrs==null) {
            return;
        }
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length());
        component.splitStart(startIndex, SplitMode.ATTRIBUTE);
        int nextComponentNumber = 0;
        while(component!=null&&component.getComponentNumber()<=endIndex) {
            nextComponentNumber = component.getNextComponentNumber();
            if(nextComponentNumber>=endIndex+1) {
                component.splitEnd(endIndex, SplitMode.ATTRIBUTE);
            }
            component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
        if(endIndex>=nextComponentNumber) {
            throw new RuntimeException();
        }
    }

    public void changeStyleSheet(String type, String styleId, String styleName, JSONObject attrs, String parentId, Boolean hidden, Integer uipriority)
        throws Exception {

        final Styles styles = getOperationDocument().getStyles(true);
        final List<Style> stylesList = styles.getStyle();

        Style style = null;

        Iterator<Style> styleIter = stylesList.iterator();
        while(styleIter.hasNext()){
            Style aStyle = styleIter.next();
            if ((aStyle.getType().equals(type)) && (aStyle.getStyleId().equals(styleId))) {
                style = aStyle;
                break;
            }
        };

        if (null != style) {

            if (null != attrs) {
                //reset attributes
                style.setPPr(null);
                style.setRPr(null);
                style.setNext(null);
                style.setTblPr(null);
                style.setTrPr(null);
                style.setTcPr(null);
                style.setPPr(null);
                style.setRPr(null);

                //change attrs
                handleNewStyleSheet(style, type, styleName.isEmpty() ? style.getName().getVal() : styleName, attrs, parentId, hidden, uipriority, null, null);
            } else {
                //change name
                Style.Name name = objectFactory.createStyleName();
                name.setVal(styleName);
                style.setName(name);
            }

        }
    }

    public void insertStyleSheet(String type, String styleId, String styleName, JSONObject attrs, String parentId, Boolean hidden, Integer uipriority, Boolean isDefault, Boolean custom)
        throws Exception {

        final Styles styles = getOperationDocument().getStyles(true);
        final List<Style> stylesList = styles.getStyle();

        // removing style if it already exists
        Iterator<Style> styleIter = stylesList.iterator();
        while(styleIter.hasNext()){
            Style aStyle = styleIter.next();
            if ((aStyle.getType().equals(type)) && (aStyle.getStyleId().equals(styleId))) {
                styleIter.remove();
                break;
            }
        };

        Style style = objectFactory.createStyle();
        stylesList.add(style);

        // style properties
        style.setType(type);
        style.setStyleId(styleId);

        handleNewStyleSheet(style, type, styleName, attrs, parentId, hidden, uipriority, isDefault, custom);
    }

    private void handleNewStyleSheet(Style style, String type, String styleName, JSONObject attrs, String parentId, Boolean hidden, Integer uipriority, Boolean isDefault, Boolean custom)
        throws Exception {

        // style properties
        if (styleName != null) {
            Style.Name name = objectFactory.createStyleName();
            name.setVal(styleName);
            style.setName(name);
        }
        if (hidden!=null) {
            BooleanDefaultTrue hiddenValue = objectFactory.createBooleanDefaultTrue();
            hiddenValue.setVal(hidden.booleanValue());
            style.setHidden(hiddenValue);

            // fix for #49652, making visible styles also to a qFormat style
            if(hidden.booleanValue()) {
                style.setQFormat(null);
            }
            else {
                style.setQFormat(objectFactory.createBooleanDefaultTrue());
            }
        }
        if (isDefault != null) {
            style.setDefault(isDefault.booleanValue());
        }
        if (uipriority != null) {
            Style.UiPriority uiPrio = objectFactory.createStyleUiPriority();
            uiPrio.setVal(Long.valueOf(uipriority.longValue()));
            style.setUiPriority(uiPrio);
        }
        if (parentId != null) {
            Style.BasedOn basedOn = objectFactory.createStyleBasedOn();
            basedOn.setVal(parentId);
            style.setBasedOn(basedOn);
        }
        if (custom != null) {
            style.setCustomStyle(custom);
        }
        // paragraph properties
        JSONObject paragraphProps = attrs.optJSONObject(OCKey.PARAGRAPH.value());
        if (paragraphProps != null) {
            String nextStyle = paragraphProps.optString(OCKey.NEXT_STYLE_ID.value());
            if (nextStyle != null) {
               Style.Next next = objectFactory.createStyleNext();
               next.setVal(nextStyle);
               style.setNext(next);
            }
            Paragraph.applyParagraphProperties(operationDocument, paragraphProps, style.getPPr(true));
        }

        // character properties
        JSONObject characterProps = attrs.optJSONObject(OCKey.CHARACTER.value());
        if (characterProps != null) {
            com.openexchange.office.filter.ooxml.docx.tools.Character.applyCharacterProperties(operationDocument, characterProps, style.getRPr(true));
        }

        if(type.equals("table")) {
            Iterator<String> keys = attrs.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                if(conditionalTableAttrNames.containsKey(key)) {
                    JSONObject conditionalTableProps = attrs.optJSONObject(key);
                    if (conditionalTableProps != null) {
                        String conditionalTableName = conditionalTableAttrNames.get(key);
                        List<CTTblStylePr> conditionalStyles = style.getTblStylePr();
                        CTTblStylePr conditionalStyle = objectFactory.createCTTblStylePr();
                        conditionalStyle.setType(STTblStyleOverrideType.fromValue(conditionalTableName));
                        Iterator<String> tablePropKeys = conditionalTableProps.keys();
                        while(tablePropKeys.hasNext()) {
                            String tablePropKey = tablePropKeys.next();
                            JSONObject tablePropAttrs = conditionalTableProps.optJSONObject(tablePropKey);
                            if(tablePropAttrs!=null) {
                                if(tablePropKey.equals(OCKey.TABLE.value())) {
                                    Table.applyTableProperties(operationDocument, tablePropAttrs, null, null, conditionalStyle.getTblPr(true));
                                } else if(tablePropKey.equals(OCKey.ROW.value())) {
                                    Table.applyRowProperties(tablePropAttrs, conditionalStyle.getTrPr(true));
                                } else if(tablePropKey.equals(OCKey.CELL.value())) {
                                   Table.applyCellProperties(operationDocument, tablePropAttrs, conditionalStyle.getTcPr(true));
                                } else if(tablePropKey.equals(OCKey.PARAGRAPH.value())) {
                                    Paragraph.applyParagraphProperties(operationDocument, tablePropAttrs, conditionalStyle.getPPr(true));
                                } else if(tablePropKey.equals(OCKey.CHARACTER.value())) {
                                    com.openexchange.office.filter.ooxml.docx.tools.Character.applyCharacterProperties(operationDocument, tablePropAttrs, conditionalStyle.getRPr(true));
                                }
                            }
                        }

                        // we will not create a whole_table style, because it does not seem to work,
                        // (even the TblLook does not mention the conditional WholeTable style) instead
                        // we will set the properties direct as tblPr
                        if(conditionalStyle.getType()==STTblStyleOverrideType.WHOLE_TABLE) {
                            style.setTblPr(conditionalStyle.getTblPr(false));
                            style.setTrPr(conditionalStyle.getTrPr(false));
                            style.setTcPr(conditionalStyle.getTcPr(false));
                            style.setPPr(conditionalStyle.getPPr(false));
                            style.setRPr(conditionalStyle.getRPr(false));

                            // if no shading is used within the TblPr, then we have to copy the shading from
                            // the cell into TblPr (LO does not take care about cell shading, so we have to do this workaround)
                            CTTblPrBase tblPr = style.getTblPr(true);
                            if(tblPr==null||tblPr.getShd(false)==null&&style.getTcPr(false)!=null) {
                                final CTShd tableShade = style.getTcPr(false).getShd(false);
                                if(tableShade!=null) {
                                    if(tblPr==null) {
                                        tblPr = objectFactory.createCTTblPrBase();
                                        style.setTblPr(tblPr);
                                    }
                                    tblPr.setShd(XmlUtils.deepCopy(tableShade, getOperationDocument().getPackage()));
                                }
                            }
                        }
                        else {
                            conditionalStyles.add(conditionalStyle);
                        }
                    }
                }
            }
        }
    }

    public void deleteStyleSheet(String type, String styleId)
            throws Exception {

        final Styles styles = getOperationDocument().getStyles(true);
        final List<Style> stylesList = styles.getStyle();

        Iterator<Style> styleIter = stylesList.iterator();
        while(styleIter.hasNext()){
            Style aStyle = styleIter.next();
            if ((aStyle.getType().equals(type)) && (aStyle.getStyleId().equals(styleId))) {
                styleIter.remove();
                return;
            }
        };
    }
    public void insertListStyle(String listStyleId, JSONObject listDefinition) throws Exception {

        NumberingDefinitionsPart numDefPart = (NumberingDefinitionsPart)wordMLPackage.getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.NUMBERING);
        if( numDefPart == null ) {
            numDefPart = new NumberingDefinitionsPart();
            numDefPart.unmarshalDefaultNumbering();
            Numbering numbering = numDefPart.getJaxbElement();
            // remove defaults
            numbering.getAbstractNum().clear();
            numbering.getNum().clear();
            wordMLPackage.getMainDocumentPart().addTargetPart(numDefPart);
        }
        Numbering numbering = numDefPart.getJaxbElement();
        List<AbstractNum> abstractNumList = numbering.getAbstractNum();

        //find a 'free' abstractNumId
        Iterator<AbstractNum> abstractIter = abstractNumList.iterator();
        HashSet<Long> existingAbstractIds = new HashSet<Long>();
        while( abstractIter.hasNext() ){
            AbstractNum abstractNum = abstractIter.next();
            long abstractNumId = abstractNum.getAbstractNumId().longValue();
            existingAbstractIds.add(Long.valueOf(abstractNumId));
        }
        long newAbstractId = 0;
        while(existingAbstractIds.contains(Long.valueOf(newAbstractId))) {
            ++newAbstractId;
        }

        AbstractNum newAbstractNum = objectFactory.createNumberingAbstractNum();
        newAbstractNum.setParent(numDefPart);
        Long abstractId = Long.valueOf(newAbstractId);
        newAbstractNum.setAbstractNumId(abstractId);
        List<Lvl> lvlList = newAbstractNum.getLvl();
        for(long lvlIndex = 0; lvlIndex < 9; ++lvlIndex) {
            JSONObject levelObject = listDefinition.getJSONObject(OCKey.LIST_LEVEL.value() + lvlIndex);
            Lvl lvl = objectFactory.createLvl();
            lvl.setParent(newAbstractNum);
            lvl.setIlvl(Long.valueOf(lvlIndex));
            if( levelObject.has(OCKey.NUMBER_FORMAT.value())) {
                NumFmt numFmt = new NumFmt();
                numFmt.setVal( NumberFormat.fromValue( levelObject.getString(OCKey.NUMBER_FORMAT.value()) ));
                lvl.setNumFmt(numFmt);
            }
            if( levelObject.has(OCKey.LIST_START_VALUE.value())) {
                Lvl.Start lvlStart = objectFactory.createLvlStart();
                lvlStart.setVal(Long.valueOf(levelObject.getLong(OCKey.LIST_START_VALUE.value())));
                lvl.setStart(lvlStart);
            }

            if( levelObject.has(OCKey.LEVEL_TEXT.value())) {
                if( lvl.getLvlText() == null )
                    lvl.setLvlText(objectFactory.createLvlLvlText());
                lvl.getLvlText().setVal(levelObject.getString(OCKey.LEVEL_TEXT.value()));
            }
            if( levelObject.has(OCKey.TEXT_ALIGN.value()) ){
                lvl.setLvlJc( objectFactory.createJc() );
                lvl.getLvlJc().setVal(JcEnumeration.fromValue(levelObject.getString(OCKey.TEXT_ALIGN.value())));
            }
            RPr runProperties = objectFactory.createRPr();
            lvl.setRPr(runProperties);
            if(levelObject.has(OCKey.FONT_NAME.value())){
                String font = levelObject.getString(OCKey.FONT_NAME.value());
                runProperties.getrFonts(true).setAscii(font);
                runProperties.getrFonts(true).setHAnsi(font);
            }
            if( levelObject.has(OCKey.PARA_STYLE.value()) ){
                Lvl.PStyle pstyle = objectFactory.createLvlPStyle();
                pstyle.setVal(levelObject.getString(OCKey.PARA_STYLE.value()));
            }

            PPr paraProperties = objectFactory.createPPr();
            lvl.setPPr(paraProperties);
            Ind ind = objectFactory.createPPrBaseInd();
            if( levelObject.has(OCKey.INDENT_LEFT.value())) {
                ind.setLeft(Utils.map100THMMToTwip( levelObject.getInt(OCKey.INDENT_LEFT.value())) );
            }
            if( levelObject.has(OCKey.INDENT_FIRST_LINE.value())) {
                int firstline = levelObject.getInt(OCKey.INDENT_FIRST_LINE.value());
                if( firstline > 0){
                    ind.setFirstLine( Utils.map100THMMToTwip( firstline ));
                    ind.setHanging( null );
                } else if( firstline < 0) {
                    ind.setFirstLine( null );
                    ind.setHanging( Utils.map100THMMToTwip( -firstline));
                }
            }
            paraProperties.setInd( ind );
            if( levelObject.has(OCKey.TAB_STOP_POSITION.value())){
                Tabs tabs = objectFactory.createTabs();
                CTTabStop tabStop = objectFactory.createCTTabStop();
                tabStop.setPos( Utils.map100THMMToTwip( levelObject.getInt(OCKey.TAB_STOP_POSITION.value()) ) );
                tabs.getTab().add(tabStop);
                paraProperties.setTabs(tabs);
            }
            lvlList.add(lvl);
        }
        abstractNumList.add(newAbstractNum);
//        newAbstractNum.setName(value);
//        newAbstractNum.setMultiLevelType(value);

        List<Num> numList = numbering.getNum();
        Num num = objectFactory.createNumberingNum();

        num.setParent(numDefPart);
        AbstractNumId abstractNumId = new AbstractNumId();
        abstractNumId.setVal(abstractId);
        num.setAbstractNumId( abstractNumId );
        Long listId;
        try {
            if(!Character.isDigit(listStyleId.charAt(0))){
                listId = Long.parseLong(listStyleId.substring(1));
            } else {
                listId = Long.parseLong(listStyleId);
            }
            num.setNumId(listId);

            // removing list style if it already exists
            Iterator<Num> numIter = numList.iterator();
            while(numIter.hasNext()){
                Num aNum = numIter.next();
                if (aNum.getNumId().equals(listId)) {
                    numIter.remove();
                    break;
                }
            };
            numList.add(num);
        }
        catch (NumberFormatException e) {
            log.debug("docx export insertListStyle: invalid listStyleId found!");
        }
    }

    public void setDocumentLanguage(String langStr, boolean applyOnlyIfNotAlreadySet)
        throws Exception {

        final CTLanguage ctLanguage = getOperationDocument().getStyles(true).getDocDefaults(true).getRPrDefault(true).getRPr(true).getLang(true);
        String lang = ctLanguage.getVal();
        if (!applyOnlyIfNotAlreadySet||(lang == null || lang.length() == 0)) {
            if (langStr != null && langStr.length() > 0)
                ctLanguage.setVal(langStr);
            else
                ctLanguage.setVal("en-US"); // default according to MS
        }
        lang = ctLanguage.getEastAsia();
        if (!applyOnlyIfNotAlreadySet||(lang == null || lang.length() == 0)) {
            ctLanguage.setEastAsia("en-US"); // default according to MS
        }
        lang = ctLanguage.getBidi();
        if (!applyOnlyIfNotAlreadySet||(lang == null || lang.length() == 0)) {
            ctLanguage.setBidi("ar-SA"); // default according to MS
        }
    }

    private SectPr getDocumentProperties(SectPr sectPr) {
        if(sectPr!=null) {
            return sectPr;
        }
        return Utils.getDocumentProperties(getOperationDocument().getPackage().getMainDocumentPart(), true);
    }

    public void setDocumentAttributes(JSONObject attrs)
        throws Exception {

        SectPr sectPr = null;
        final JSONObject documentAttrs = attrs.optJSONObject(OCKey.DOCUMENT.value());
        if(documentAttrs!=null) {
            final Object changeTracking = documentAttrs.opt(OCKey.CHANGE_TRACKING.value());
            if(changeTracking instanceof Boolean) {
                final CTSettings settings = getOperationDocument().getSettings(true);
                if((Boolean)changeTracking) {
                    final BooleanDefaultTrue trackRevision = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                    trackRevision.setVal(true);
                    settings.setTrackRevisions(trackRevision);
                }
                else {
                    settings.setTrackRevisions(null);
                }
            }
        }
        final JSONObject pageAttrs = attrs.optJSONObject(OCKey.PAGE.value());
        if(pageAttrs!=null) {
            for(Entry<String, Object> pageAttr : pageAttrs.entrySet()) {
                final String attr = pageAttr.getKey();
                final Object value = pageAttr.getValue();
                if(attr.equals(OCKey.EVEN_ODD_PAGES.value())) {
                    final CTSettings settings = getOperationDocument().getSettings(true);
                    if(value instanceof Boolean) {
                        final BooleanDefaultTrue evenOddHeaders = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                        evenOddHeaders.setVal((Boolean)value);
                        settings.setEvenAndOddHeaders(evenOddHeaders);
                    }
                    else {
                        settings.setEvenAndOddHeaders(null);
                    }
                }
                else if(attr.equals(OCKey.FIRST_PAGE.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Boolean) {
                        final BooleanDefaultTrue titlePg = Context.getWmlObjectFactory().createBooleanDefaultTrue();
                        titlePg.setVal((Boolean)value);
                        sectPr.setTitlePg(titlePg);
                    }
                    else {
                        sectPr.setTitlePg(null);
                    }
                }
                else if(attr.equals(OCKey.WIDTH.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        final int w = ((Number)value).intValue();
                        if(w==21000) {
                            sectPr.getPgSz(true).setW(Long.valueOf(11907));
                        }
                        else {
                            sectPr.getPgSz(true).setW(Utils.map100THMMToTwip(((Number)value).longValue()));
                        }
                    }
                }
                else if(attr.equals(OCKey.HEIGHT.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        final int h = ((Number)value).intValue();
                        if(h==29700) {
                            sectPr.getPgSz(true).setH(Long.valueOf(16840));
                        }
                        else {
                            sectPr.getPgSz(true).setH(Utils.map100THMMToTwip(((Number)value).longValue()));
                        }
                    }
                }
                else if(attr.equals(OCKey.MARGIN_LEFT.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setLeft(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.MARGIN_TOP.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setTop(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.MARGIN_RIGHT.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setRight(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.MARGIN_BOTTOM.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setBottom(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.MARGIN_HEADER.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setHeader(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.MARGIN_FOOTER.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    if(value instanceof Number) {
                        sectPr.getPgMar(true).setFooter(Utils.map100THMMToTwip(((Number)value).longValue()));
                    }
                }
                else if(attr.equals(OCKey.ORIENTATION.value())) {
                    sectPr = getDocumentProperties(sectPr);
                    final PgSz pgSz = sectPr.getPgSz(true);
                    if(value instanceof String) {
                        pgSz.setOrient(((String)value).equals("landscape") ? STPageOrientation.LANDSCAPE : STPageOrientation.PORTRAIT);
                    }
                    else {
                        pgSz.setOrient(STPageOrientation.PORTRAIT);
                    }
                }
            }
        }
    }

    public void insertHeaderFooter(String id, String type)
        throws InvalidFormatException {

        final MainDocumentPart mainDocumentPart = wordMLPackage.getMainDocumentPart();
        final String[] typeArray = type.split("_");
        final HdrFtrRef hdrFtrRef = typeArray[1].equals("first")?HdrFtrRef.FIRST:typeArray[1].equals("even")?HdrFtrRef.EVEN:HdrFtrRef.DEFAULT;
        CTRel ctRel;
        Part part;
        if(typeArray[0].equals("header")) {
            final HeaderPart headerPart = new HeaderPart();
            headerPart.setJaxbElement(Context.getWmlObjectFactory().createHdr());
            final HeaderReference headerReference = Context.getWmlObjectFactory().createHeaderReference();
            headerReference.setType(hdrFtrRef);
            ctRel = headerReference;
            part = headerPart;
        }
        else {
            final FooterPart footerPart = new FooterPart();
            footerPart.setJaxbElement(Context.getWmlObjectFactory().createFtr());
            ctRel = Context.getWmlObjectFactory().createFooterReference();
            final FooterReference footerReference = Context.getWmlObjectFactory().createFooterReference();
            footerReference.setType(hdrFtrRef);
            ctRel = footerReference;
            part = footerPart;
        }
        final Relationship rel = mainDocumentPart.addTargetPart(part, AddPartBehaviour.RENAME_IF_NAME_EXISTS, id);
        if(!rel.getId().equals(id)) {
            log.debug("docx: insertHeaderFooter... Id:" + id + " is not unique!!!");
        }
        final SectPr sectPr = Utils.getDocumentProperties(mainDocumentPart, true);
        ctRel.setParent(sectPr);
        ctRel.setId(rel.getId());
        // adding updating targetToRelId map so that further operations can use its own id
        sectPr.getEGHdrFtrReferences().add(ctRel);
    }

    public void deleteHeaderFooter(String id) {
        final MainDocumentPart mainDocumentPart = wordMLPackage.getMainDocumentPart();
        mainDocumentPart.getRelationshipsPart(false).getPart(id).remove();
        final List<CTRel> hdrFtrReferenceList = Utils.getDocumentProperties(mainDocumentPart, false).getEGHdrFtrReferences();
        for(int i=0;i<hdrFtrReferenceList.size();i++) {
            final CTRel rel = hdrFtrReferenceList.get(i);
            if(rel.getId().equals(id)) {
                hdrFtrReferenceList.remove(i);
                break;
            }
        }
    }

    public void changeComment(JSONArray start, String text, JSONArray mentions, JSONObject attrs)
        throws Exception {

        final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length());
        final R.CommentReference commentReference = (R.CommentReference)component.getObject();
        final Comment comment = getOperationDocument().getComment(commentReference.getId(), false);
        if(text!=null) {
            comment.getContent().clear();
            applyCommentText(comment, text, mentions);
        }
        if(attrs!=null) {
            component.applyAttrsFromJSON(attrs);
        }
    }

    public void insertComment(JSONArray start, String text, JSONArray mentions, String operationId, String parentId, String uid, String author, String date, JSONArray children, JSONObject attrs)
        throws Exception {

        final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.COMMENT_REFERENCE);

        final int id = DocxOperationDocument.commentOperationIdToId(operationId);

        final R.CommentReference commentReference = (R.CommentReference)component.getObject();
        commentReference.setId(id);

        final Comment comment = getOperationDocument().getComment(id, true);
        comment.getContent().clear();
        if(parentId!=null) {
            comment.setParentId(DocxOperationDocument.commentOperationIdToId(parentId));
        }
        else {
            comment.setParent(null);
        }
        if(children!=null) {
            for(int i = 0; i < children.length(); i++) {
                final String childId = children.getString(i);
                final int childIndex = DocxOperationDocument.commentOperationIdToId(childId);
                final Comment child = getOperationDocument().getComment(childIndex, false);
                if(child!=null) {
                    child.setParentId(id);
                }
            }
        }
        Utils.applyTrackInfoFromJSON(getOperationDocument(), null, author, date, uid, comment);
        applyCommentText(comment, text, mentions);
        if(attrs!=null) {
            component.applyAttrsFromJSON(attrs);
        }
    }

    private void applyCommentText(Comment comment, String text, JSONArray mentions) throws Exception {
        final Part oldContextPart = getOperationDocument().getContextPart();
        setContext(comment);
        final String[] paragraphs = text.split("\\n", -1);
        for(int i=0; i < paragraphs.length; i++) {
            final JSONArray pStart = new JSONArray(1);
            pStart.put(i);
            final JSONObject attrs = new JSONObject(1);
            attrs.put(OCKey.STYLE_ID.value(), "CommentText");
            insertParagraph(pStart, attrs);
            final JSONArray tStart = new JSONArray(2);
            tStart.put(i);
            tStart.put(0);
            insertText(tStart, paragraphs[i], new JSONObject());
        }
        if(mentions!=null) {

            // mention position needs to be corrected to text with paragraphs instead of a simple text array
            int offset = 0;
            int paragraphCount = 0;

            TreeMap<Integer, Integer> newLines = new TreeMap<Integer, Integer>();
            newLines.put(-1, paragraphCount++);

            while(offset!=-1) {
                offset = text.indexOf('\n', offset);
                if(offset!=-1) {
                    newLines.put(offset++, paragraphCount++);
                }
            }

            if(!mentions.isEmpty()) {
                final List<Triple<DLNode<Object>, DLNode<Object>, String>> mTextRuns = new ArrayList<Triple<DLNode<Object>, DLNode<Object>, String>>(mentions.length());
                for(int i = 0; i < mentions.length(); i++) {
                    final JSONObject mention = mentions.getJSONObject(i);
                    final String email = mention.optString(OCKey.EMAIL.value(), null);
                    if(email!=null && !email.isEmpty()) { // email is necessary for a mention...

                        addMentionToAuthorsList(mention);

                        final int simpleTextPos = mention.getInt(OCKey.POS.value());
                        final Entry<Integer, Integer> pEntry = newLines.floorEntry(simpleTextPos);

                        final JSONArray start = new JSONArray(2);
                        start.put(pEntry.getValue());
                        final int startIndex = (simpleTextPos - pEntry.getKey()) - 1;
                        start.put(startIndex);
                        final int endIndex = (startIndex + mention.getInt(OCKey.LENGTH.value())) - 1;

                        final IComponent<OfficeOpenXMLOperationDocument> startComponent = getOperationDocument().getRootComponent().getComponent(start, start.length());
                        startComponent.splitStart(startIndex, SplitMode.ATTRIBUTE);
                        final IComponent<OfficeOpenXMLOperationDocument> endComponent = startComponent.getComponent(endIndex);
                        endComponent.splitEnd(endIndex, SplitMode.ATTRIBUTE);

                        mTextRuns.add(Triple.of(startComponent.getParentContext().getNode(), endComponent.getParentContext().getNode(), email));
                    }
                }
                _addHyperlinks(mTextRuns);
            }
        }
        getOperationDocument().setContextPart(oldContextPart);
    }

    private void _addHyperlinks(List<Triple<DLNode<Object>, DLNode<Object>, String>> mTextRuns) {
        for(int i=0; i < mTextRuns.size(); i++) {

            final Triple<DLNode<Object>, DLNode<Object>, String> t = mTextRuns.get(i);
            final DLNode<Object> startTextRunNode = t.getLeft();
            final DLNode<Object> endTextRunNode = t.getMiddle();
            final String email = t.getRight();

            final P startParagraph = (P)((Child)startTextRunNode.getData()).getParent();
            final P endParagraph = (P)((Child)endTextRunNode.getData()).getParent();

            ((R)startTextRunNode.getData()).getRPr(true).setrStyle(new RStyle("Mention"));

    /* adding following elements before the text referenced by mention.start and mention.length
            <w:r>
                <w:fldChar w:fldCharType="begin" />
            </w:r>
            <w:r>
                <w:instrText xml:space="preserve"> HYPERLINK "mailto:john.doe@example.com"</w:instrText>
            </w:r>
            <w:bookmarkStart w:name="_@_1234567890123456789012345678901234Z" w:id="2345627" />
            <w:r>
                <w:fldChar w:fldCharType="separate" />
            </w:r>
            <w:bookmarkEnd w:id="2345627" />
    */

            final FldChar fldCharBegin = new FldChar(STFldCharType.BEGIN);
            final FldChar fldCharSeparate = new FldChar(STFldCharType.SEPARATE);
            final FldChar fldCharEnd = new FldChar(STFldCharType.END);
            FldChar.initializeFld(fldCharBegin, fldCharSeparate, fldCharEnd);

            insertTextRunContent(startParagraph, startTextRunNode, fldCharBegin, true);
            final InstrText instrText = new InstrText();
            instrText.setValue(" HYPERLINK \"mailto:" + email + "\"");
            insertTextRunContent(startParagraph, startTextRunNode, instrText, true);
            final CTBookmark bookmarkStart = new CTBookmark();
            bookmarkStart.setParent(startParagraph);
            final Integer bookmarkId = wordMLPackage.getNextMarkupId();
            bookmarkStart.setId(bookmarkId);
            bookmarkStart.setName("_@_" + "123400000000000000000000" + Integer.toHexString(bookmarkId) + "Z");
            startParagraph.getContent().addNode(startTextRunNode, new DLNode<Object>(bookmarkStart), true);
            insertTextRunContent(startParagraph, startTextRunNode, fldCharSeparate, true);
            final CTMarkupRange bookmarkEndRange = new CTMarkupRange();
            bookmarkEndRange.setId(bookmarkId);
            bookmarkEndRange.setParent(startParagraph);
            final JAXBElement<CTMarkupRange> bookmarkEnd = new JAXBElement<CTMarkupRange>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookmarkEnd"), CTMarkupRange.class, P.class, bookmarkEndRange);
            startParagraph.getContent().addNode(startTextRunNode, new DLNode<Object>(bookmarkEnd), true);

    /* adding following elements behind the text referenced by< mention.start and mention.length
            <w:r>
                <w:fldChar w:fldCharType="end" />
            </w:r>
    */

            insertTextRunContent(endParagraph, endTextRunNode, fldCharEnd, false);
        }
    }

    private void addMentionToAuthorsList(JSONObject mention) {
        final String author = mention.optString(OCKey.DISPLAY_NAME.value(), null);
        final String userId = mention.optString(OCKey.USER_ID.value(), null);
        final String provider = mention.optString(OCKey.PROVIDER_ID.value(), null);
        if(author!=null && "ox".equals(provider) && userId != null) {
            final String id = Utils.getIdFromUserId(userId);
            final CTPeople ctPeople = operationDocument.getPeopleList(true);
            if(ctPeople!=null) {
                final List<CTPerson> peopleList = ctPeople.getPerson();
                if(peopleList!=null) {
                    int i=0;
                    for(;i<peopleList.size();i++) {
                        final CTPerson person = peopleList.get(i);
                        if(author.equals(person.getAuthor())) {
                            final CTPresenceInfo presenceInfo = person.getPresenceInfo(false);
                            if(presenceInfo!=null) {
                                final String p_Id = presenceInfo.getProviderId();
                                final String u_Id = presenceInfo.getUserId();
                                if(p_Id.equals("None")&&u_Id.startsWith("ox_")) {
                                    if(id!=null && id.equals(Utils.getIdFromUserId(u_Id.substring(3)))) {
                                        if((presenceInfo.getUserId().length() - 3) < userId.length()) {
                                            presenceInfo.setUserId("ox_" + userId);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if(i==peopleList.size()) {
                        final CTPerson newPerson = new CTPerson();
                        newPerson.setAuthor(author);
                        peopleList.add(newPerson);
                        final CTPresenceInfo presenceInfo = newPerson.getPresenceInfo(true);
                        presenceInfo.setProviderId("None");
                        presenceInfo.setUserId("ox_" + userId);
                        newPerson.setPresenceInfo(presenceInfo);
                    }
                }
            }
        }
    }

    private void insertTextRunContent(P paragraph, DLNode<Object> refNode, Object newContent, boolean before) {
        final R run = new R();
        if(newContent instanceof Child) {
            ((Child)newContent).setParent(run);
        }
        run.getContent().add(newContent);
        run.setParent(paragraph);
        paragraph.getContent().addNode(refNode, new DLNode<Object>(run), before);
    }

    private void setContext(Comment comment) {
        // not to be allowed to be zero... a comment has to be inserted
        // before it could be used, so this part must be there
        final CommentsPart commentsPart = getOperationDocument().getCommentsPart(false);
        final Comments comments = commentsPart.getJaxbElement();
        comments.setCommentId(comment.getId());
        getOperationDocument().setContextPart(commentsPart);
    }

    public void insertRange(JSONArray start, String id, String type, String position, JSONObject attrs) throws Exception {

        if(type.equals("comment")) {
            final int cId = DocxOperationDocument.commentOperationIdToId(id);
            final ComponentType rangeType = position.equals("start") ? ComponentType.COMMENT_RANGE_START : ComponentType.COMMENT_RANGE_END;
            final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
                .insertChildComponent(start.getInt(start.length()-1), attrs, rangeType);

            final Object commentRangeObject = component.getObject();
            if(commentRangeObject instanceof CommentRangeStart) {
                ((CommentRangeStart)commentRangeObject).setId(cId);
            }
            else {
                ((CommentRangeEnd)commentRangeObject).setId(cId);
            }
        }
        else if(type.equals("field")) {
            getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
                .insertChildComponent(start.getInt(start.length()-1), attrs, position.equals("start") ? ComponentType.COMPLEX_FIELD_START : ComponentType.COMPLEX_FIELD_END);
        }
    }

    public void insertBookmark(JSONArray start, String id, String position, String anchorName) throws Exception {

        final Integer cId = Integer.parseInt(id.substring(2));
        final ComponentType rangeType = position.equals("start") ? ComponentType.BOOKMARK_START : ComponentType.BOOKMARK_END;
        final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), null, rangeType);

        if(component instanceof MultiComponent) {
            for(OfficeOpenXMLComponent c:((MultiComponent)component).getComponentList()) {
                applyBookmarkId(c, cId, anchorName);
            }
        }
        else {
            applyBookmarkId(component, cId, anchorName);
        }
    }

    private static void applyBookmarkId(IComponent<OfficeOpenXMLOperationDocument> component, Integer cId, String anchorName) {
        final Object bookmarkObject = component.getObject();
        if(bookmarkObject.getClass()==CTBookmark.class) {
            ((CTBookmark)bookmarkObject).setId(cId);
            ((CTBookmark)bookmarkObject).setName(anchorName);
        }
        else {
            ((CTMarkupRange)bookmarkObject).setId(cId);
        }
    }

    public void insertComplexField(JSONArray start, String instruction, JSONObject attrs) throws Exception {

        final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.COMPLEX_FIELD_SEPARATE);

        if(component instanceof MultiComponent) {
            for(OfficeOpenXMLComponent fldCharComponent:((MultiComponent)component).getComponentList()) {
                setInstrText((FldCharSeparate)fldCharComponent, instruction);
            }
        }
        else {
            setInstrText((FldCharSeparate)component, instruction);
        }
    }

    private void setInstrText(FldCharSeparate component, String instruction) {
        final R textRun = component.getTextRun();
        final InstrText instrText = new InstrText();
        instrText.setParent(textRun);
        instrText.setValue(instruction);
        textRun.getContent().addNode(component.getNode(), new DLNode<Object>(instrText), true);
    }

    public void updateComplexField(JSONArray start, String instruction, JSONObject attrs) throws Exception {

        final IComponent<OfficeOpenXMLOperationDocument> component = getOperationDocument().getRootComponent().getComponent(start, start.length());
        if(component instanceof MultiComponent) {
            for(OfficeOpenXMLComponent fldCharComponent:((MultiComponent)component).getComponentList()) {
                if(instruction!=null) {
                    updateInstrText((FldCharSeparate)fldCharComponent, instruction);
                }
                if(attrs!=null) {
                    component.applyAttrsFromJSON(attrs);
                }
            }
        }
        else {
            if(instruction!=null) {
                updateInstrText((FldCharSeparate)component, instruction);
            }
            if(attrs!=null) {
                component.applyAttrsFromJSON(attrs);
            }
        }
    }

    private void updateInstrText(FldCharSeparate component, String instruction) {
        final FldCharBegin fldCharBegin = component.getFldCharBegin();
        if(fldCharBegin!=null) {
            final List<DLNode<Object>> instructionNodes = fldCharBegin.getInstructionNodes();
            if(!instructionNodes.isEmpty()) {
                ((InstrText)instructionNodes.get(instructionNodes.size()-1).getData()).setValue(instruction);
            }
        }
    }
}
