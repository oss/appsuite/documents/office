/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.awt.Rectangle;
import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import org.docx4j.dml.CTGroupTransform2D;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.GraphicData;
import org.docx4j.dml.picture.Pic;
import org.docx4j.dml.wordprocessingCanvas2010.CTWordprocessingCanvas;
import org.docx4j.dml.wordprocessingDrawing.Anchor;
import org.docx4j.dml.wordprocessingDrawing.CTEffectExtent;
import org.docx4j.dml.wordprocessingDrawing.CTPosH;
import org.docx4j.dml.wordprocessingDrawing.CTPosV;
import org.docx4j.dml.wordprocessingDrawing.CTWrapNone;
import org.docx4j.dml.wordprocessingDrawing.CTWrapPath;
import org.docx4j.dml.wordprocessingDrawing.CTWrapSquare;
import org.docx4j.dml.wordprocessingDrawing.CTWrapThrough;
import org.docx4j.dml.wordprocessingDrawing.CTWrapTight;
import org.docx4j.dml.wordprocessingDrawing.CTWrapTopBottom;
import org.docx4j.dml.wordprocessingDrawing.GraphicBase;
import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.dml.wordprocessingDrawing.ObjectFactory;
import org.docx4j.dml.wordprocessingDrawing.STAlignH;
import org.docx4j.dml.wordprocessingDrawing.STAlignV;
import org.docx4j.dml.wordprocessingDrawing.STRelFromH;
import org.docx4j.dml.wordprocessingDrawing.STRelFromV;
import org.docx4j.dml.wordprocessingDrawing.STWrapText;
import org.docx4j.dml.wordprocessingGroup2010.CTWordprocessingGroup;
import org.docx4j.dml.wordprocessingShape2010.CTTextboxInfo;
import org.docx4j.dml.wordprocessingShape2010.CTWordprocessingShape;
import org.docx4j.jaxb.Context;
import org.docx4j.mce.AlternateContent;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.wml.CTTxbxContent;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.SectPr;
import org.docx4j.wp14.CTSizeRelH;
import org.docx4j.wp14.CTSizeRelV;
import org.docx4j.wp14.STPercentage;
import org.docx4j.wp14.STPositivePercentage;
import org.docx4j.wp14.STSizeRelFromH;
import org.docx4j.wp14.STSizeRelFromV;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShape;
import com.openexchange.office.filter.ooxml.components.IShapeTxBxContentAccessor;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.RootComponent;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.drawingml.GroupShape;
import com.openexchange.office.filter.ooxml.drawingml.Picture;
import com.openexchange.office.filter.ooxml.drawingml.Shape;

public abstract class DrawingML {

    final static String alternateContentNS =
            "xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" " +
            "xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" " +
            "xmlns:o=\"urn:schemas-microsoft-com:office:office\" " +
            "xmlns:v=\"urn:schemas-microsoft-com:vml\" " +
            "xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
            "xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" ";

    final static String defaultSpPr =
            "<wps:spPr>" +
            "    <a:xfrm>" +
            "        <a:off x=\"0\" y=\"0\" />" +
            "        <a:ext cx=\"100000\" cy=\"100000\" />" +
            "    </a:xfrm>" +
            "    <a:prstGeom prst=\"rect\">" +
            "        <a:avLst />" +
            "    </a:prstGeom>" +
            "    <a:noFill />" +
            "    <a:ln>" +
            "        <a:noFill />" +
            "    </a:ln>" +
            "</wps:spPr>";

    final static String defaultSpConnectorPr =
        "<wps:spPr>" +
        "    <a:xfrm>" +
        "        <a:off x=\"0\" y=\"0\" />" +
        "        <a:ext cx=\"100000\" cy=\"100000\" />" +
        "    </a:xfrm>" +
        "    <a:prstGeom prst=\"straightConnector1\">" +
        "        <a:avLst />" +
        "    </a:prstGeom>" +
        "    <a:noFill />" +
        "    <a:ln>" +
        "        <a:noFill />" +
        "    </a:ln>" +
        "</wps:spPr>";

    final static String defaultSpStyle =
            "<wps:style>" +
            "    <a:lnRef idx=\"2\">" +
            "        <a:schemeClr val=\"dk1\" />" +
            "    </a:lnRef>" +
            "    <a:fillRef idx=\"1\">" +
            "        <a:schemeClr val=\"lt1\" />" +
            "    </a:fillRef>" +
            "    <a:effectRef idx=\"0\">" +
            "        <a:schemeClr val=\"accent1\" />" +
            "    </a:effectRef>" +
            "    <a:fontRef idx=\"minor\">" +
            "        <a:schemeClr val=\"dk1\" />" +
            "    </a:fontRef>" +
            "</wps:style>";

    // creates an AlternateContent textframe (drawingML + VML), this shape is empty and has no textframe
    public static AlternateContent createAlternateContentShape(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<mc:AlternateContent " +
            alternateContentNS +
            "xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\" " +
            ">" +
            "   <mc:Choice Requires=\"wps\">" +
            "       <w:drawing>" +
            "           <wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\">" +
            "               <wp:extent cx=\"100000\" cy=\"100000\" />" +
            "               <wp:effectExtent l=\"0\" t=\"0\" r=\"19050\" b=\"16510\" />" +
            "               <wp:docPr name=\"Textfeld 1\" />" +
            "               <wp:cNvGraphicFramePr>" +
            "               <a:graphicFrameLocks" +
            "                   xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" />" +
            "               </wp:cNvGraphicFramePr>" +
            "               <a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">" +
            "                   <a:graphicData uri=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\">" +
            "                       <wps:wsp>" +
            "                           <wps:cNvSpPr txBox=\"1\">" +
            "                               <a:spLocks noChangeArrowheads=\"1\"/>" +
            "                           </wps:cNvSpPr>" +
            defaultSpPr +
            defaultSpStyle +
            "                           <wps:bodyPr rot=\"0\" vert=\"horz\" wrap=\"square\" lIns=\"91440\" tIns=\"45720\" rIns=\"91440\" bIns=\"45720\" anchor=\"t\" anchorCtr=\"0\">" +
            "                               <a:noAutoFit/>" +
            "                           </wps:bodyPr>" +
            "                       </wps:wsp>" +
            "                   </a:graphicData>" +
            "               </a:graphic>" +
            "           </wp:inline>" +
            "       </w:drawing>" +
            "   </mc:Choice>" +
            "   <mc:Fallback>" +
            "       <w:pict>" +
            "           <v:shapetype id=\"${typeId}\" coordsize=\"21600,21600\" o:spt=\"202\" path=\"m,l,21600r21600,l21600,xe\">" +
            "               <v:stroke joinstyle=\"miter\"/>" +
            "               <v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>" +
            "           </v:shapetype>" +
            "           <v:shape id=\"${shapeId}\" o:spid=\"_x0000_s1026\" type=\"#${typeId}\" style=\"width:185.9pt;height:110.6pt;visibility:visible;mso-wrap-style:square;mso-left-percent:-10001;mso-top-percent:-10001;mso-position-horizontal:absolute;mso-position-horizontal-relative:char;mso-position-vertical:absolute;mso-position-vertical-relative:line;mso-left-percent:-10001;mso-top-percent:-10001;v-text-anchor:top\"/>" +
            "       </w:pict>" +
            "   </mc:Fallback>" +
            "</mc:AlternateContent>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        mappings.put("shapeId", opcPackage.getNextMarkupId().toString());
        mappings.put("typeId", opcPackage.getNextMarkupId().toString());
        return (AlternateContent)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings);
    }

    public static AlternateContent createAlternateContentConnector(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<mc:AlternateContent " +
            alternateContentNS +
            "xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\" " +
            ">" +
            "    <mc:Choice Requires=\"wps\">" +
            "        <w:drawing>" +
            "            <wp:anchor distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\" simplePos=\"0\" relativeHeight=\"251659264\" behindDoc=\"0\" locked=\"0\" layoutInCell=\"1\" allowOverlap=\"1\">" +
            "                <wp:simplePos x=\"0\" y=\"0\"/>" +
            "                <wp:positionH relativeFrom=\"column\">" +
            "                    <wp:posOffset>1000</wp:posOffset>" +
            "                </wp:positionH>" +
            "                <wp:positionV relativeFrom=\"paragraph\">" +
            "                    <wp:posOffset>1000</wp:posOffset>" +
            "                </wp:positionV>" +
            "                <wp:extent cx=\"100000\" cy=\"100000\"/>" +
            "                <wp:effectExtent l=\"0\" t=\"0\" r=\"76200\" b=\"85725\"/>" +
            "                <wp:wrapNone/>" +
            "                <wp:docPr name=\"Verbinder: gewinkelt 1\"/>" +
            "                <wp:cNvGraphicFramePr/>" +
            "                <a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">" +
            "                    <a:graphicData uri=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\">" +
            "                        <wps:wsp>" +
            "                            <wps:cNvCnPr/>" +
            "                            <wps:spPr>" +
            "                                <a:xfrm>" +
            "                                    <a:off x=\"0\" y=\"0\"/>" +
            "                                    <a:ext cx=\"3752850\" cy=\"2009775\"/>" +
            "                                </a:xfrm>" +
            "                                <a:prstGeom prst=\"bentConnector3\">" +
            "                                    <a:avLst/>" +
            "                                </a:prstGeom>" +
            "                            </wps:spPr>" +
            defaultSpStyle +
            "                            <wps:bodyPr/>" +
            "                        </wps:wsp>" +
            "                    </a:graphicData>" +
            "                </a:graphic>" +
            "            </wp:anchor>" +
            "        </w:drawing>" +
            "    </mc:Choice>" +
            "    <mc:Fallback>" +
            "        <w:pict>" +
            "            <v:shapetype id=\"${typeId}\" coordsize=\"21600,21600\" o:spt=\"34\" o:oned=\"t\" adj=\"10800\" path=\"m,l@0,0@0,21600,21600,21600e\" filled=\"f\">" +
            "                <v:stroke joinstyle=\"miter\"/>" +
            "                <v:formulas>" +
            "                    <v:f eqn=\"val #0\"/>" +
            "                </v:formulas>" +
            "                <v:path arrowok=\"t\" fillok=\"f\" o:connecttype=\"none\"/>" +
            "                <v:handles>" +
            "                    <v:h position=\"#0,center\"/>" +
            "                </v:handles>" +
            "                <o:lock v:ext=\"edit\" shapetype=\"t\"/>" +
            "            </v:shapetype>" +
            "            <v:shape id=\"${shapeId}\" o:spid=\"_x0000_s1026\" type=\"#_x0000_t34\" style=\"position:absolute;margin-left:13.9pt;margin-top:134.4pt;width:295.5pt;height:158.25pt;z-index:251659264;visibility:visible;mso-wrap-style:square;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;mso-position-horizontal-relative:text;mso-position-vertical:absolute;mso-position-vertical-relative:text\" strokecolor=\"#5b9bd5 [3204]\" strokeweight=\".5pt\"/>" +
            "        </w:pict>" +
            "    </mc:Fallback>" +
            "</mc:AlternateContent>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        mappings.put("shapeId", opcPackage.getNextMarkupId().toString());
        mappings.put("typeId", opcPackage.getNextMarkupId().toString());
        return (AlternateContent)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings);
    }

    public static AlternateContent createAlternateContentGroup(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<mc:AlternateContent " +
            alternateContentNS +
            "xmlns:w10=\"urn:schemas-microsoft-com:office:word\" " +
            "xmlns:wpg=\"http://schemas.microsoft.com/office/word/2010/wordprocessingGroup\" " +
            ">" +
            "    <mc:Choice Requires=\"wpg\">" +
            "        <w:drawing>" +
            "            <wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\">" +
            "                <wp:extent cx=\"100000\" cy=\"100000\"/>" +
            "                <wp:effectExtent l=\"0\" t=\"0\" r=\"1\" b=\"1\"/>" +
            "                <wp:docPr name=\"Group 1\"/>" +
            "                <wp:cNvGraphicFramePr/>" +
            "                <a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">" +
            "                    <a:graphicData uri=\"http://schemas.microsoft.com/office/word/2010/wordprocessingGroup\">" +
            "                        <wpg:wgp>" +
            "                            <wpg:cNvGrpSpPr/>" +
            "                            <wpg:grpSpPr>" +
            "                                <a:xfrm>" +
            "                                    <a:off x=\"0\" y=\"0\"/>" +
            "                                    <a:ext cx=\"100000\" cy=\"100000\"/>" +
            "                                    <a:chOff x=\"0\" y=\"0\"/>" +
            "                                    <a:chExt cx=\"19050\" cy=\"16510\"/>" +
            "                                </a:xfrm>" +
            "                            </wpg:grpSpPr>" +
            "                        </wpg:wgp>" +
            "                    </a:graphicData>" +
            "                </a:graphic>" +
            "            </wp:inline>" +
            "        </w:drawing>" +
            "    </mc:Choice>" +
            "    <mc:Fallback>" +
            "        <w:pict>" +
            "            <v:group id=\"${shapeId}\" o:spid=\"_x0000_s1026\" style=\"width:35.25pt;height:39pt;mso-position-horizontal-relative:char;mso-position-vertical-relative:line\">" +
            "                <w10:anchorlock/>" +
            "            </v:group>" +
            "        </w:pict>" +
            "    </mc:Fallback>" +
            "</mc:AlternateContent>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        mappings.put("shapeId", opcPackage.getNextMarkupId().toString());
        return (AlternateContent)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings);
    }

    public static CTWordprocessingGroup createDrawingMLGroup() {

        final org.docx4j.dml.wordprocessingGroup2010.ObjectFactory objectFactory = new org.docx4j.dml.wordprocessingGroup2010.ObjectFactory();
        final CTWordprocessingGroup group = objectFactory.createCTWordprocessingGroup();
        final CTNonVisualDrawingProps cNvPr = group.getNonVisualDrawingProperties(true);
        cNvPr.setName("Group 1");
        cNvPr.setId(null);
        group.getNonVisualDrawingShapeProperties(true);
        group.setGrpSpPr(Context.getDmlObjectFactory().createCTGroupShapeProperties());
        group.getXfrm(true);
        return group;
    }

    public static CTWordprocessingShape createDrawingMLShape(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<wps:wsp " +
            alternateContentNS +
            "xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\" " +
            ">" +
            "    <wps:cNvPr name=\"Rechteck 1\" />" +
            "    <wps:cNvSpPr />" +
            defaultSpPr +
            defaultSpStyle +
            "    <wps:bodyPr rot=\"0\" spcFirstLastPara=\"0\"" +
            "        vertOverflow=\"overflow\" horzOverflow=\"overflow\" vert=\"horz\"" +
            "        wrap=\"square\" lIns=\"91440\" tIns=\"45720\" rIns=\"91440\" bIns=\"45720\"" +
            "        numCol=\"1\" spcCol=\"0\" rtlCol=\"0\" fromWordArt=\"0\" anchor=\"ctr\"" +
            "        anchorCtr=\"0\" forceAA=\"0\" compatLnSpc=\"1\">" +
            "        <a:prstTxWarp prst=\"textNoShape\">" +
            "            <a:avLst />" +
            "        </a:prstTxWarp>" +
            "        <a:noAutofit />" +
            "    </wps:bodyPr>" +
            "</wps:wsp>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        return (CTWordprocessingShape)((JAXBElement<?>)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings)).getValue();
    }

    public static CTWordprocessingShape createDrawingMLConnectorShape(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<wps:wsp " +
            alternateContentNS +
            "xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\" " +
            ">" +
            "    <wps:cNvPr name=\"Connector 1\" />" +
            "    <wps:cNvCnPr />" +
            defaultSpConnectorPr +
            defaultSpStyle +
            "    <wps:bodyPr rot=\"0\" spcFirstLastPara=\"0\"" +
            "        vertOverflow=\"overflow\" horzOverflow=\"overflow\" vert=\"horz\"" +
            "        wrap=\"square\" lIns=\"91440\" tIns=\"45720\" rIns=\"91440\" bIns=\"45720\"" +
            "        numCol=\"1\" spcCol=\"0\" rtlCol=\"0\" fromWordArt=\"0\" anchor=\"ctr\"" +
            "        anchorCtr=\"0\" forceAA=\"0\" compatLnSpc=\"1\">" +
            "        <a:prstTxWarp prst=\"textNoShape\">" +
            "            <a:avLst />" +
            "        </a:prstTxWarp>" +
            "        <a:noAutofit />" +
            "    </wps:bodyPr>" +
            "</wps:wsp>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        return (CTWordprocessingShape)((JAXBElement<?>)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings)).getValue();
    }

    public static Pic createDrawingMLPic(OpcPackage opcPackage)
        throws JAXBException {

        final String ml =
            "<pic:pic " +
            alternateContentNS +
            "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" " +
            "xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">" +
            "    <pic:nvPicPr>" +
            "        <pic:cNvPr name=\"Grafik 1\" />" +
            "        <pic:cNvPicPr>" +
            "            <a:picLocks noChangeAspect=\"1\" />" +
            "        </pic:cNvPicPr>" +
            "    </pic:nvPicPr>" +
            "    <pic:blipFill>" +
            "        <a:blip r:embed=\"\" cstate=\"print\">" +
            "            <a:extLst>" +
            "                <a:ext uri=\"{28A0092B-C50C-407E-A947-70E740481C1C}\" />" +
            "            </a:extLst>" +
            "        </a:blip>" +
            "        <a:stretch>" +
            "            <a:fillRect />" +
            "        </a:stretch>" +
            "    </pic:blipFill>" +
            "    <pic:spPr>" +
            "        <a:xfrm>" +
            "            <a:off x=\"0\" y=\"0\" />" +
            "            <a:ext cx=\"1\" cy=\"1\" />" +
            "        </a:xfrm>" +
            "        <a:prstGeom prst=\"rect\">" +
            "            <a:avLst />" +
            "        </a:prstGeom>" +
            "    </pic:spPr>" +
            "</pic:pic>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        return (Pic)((JAXBElement<?>)org.docx4j.XmlUtils.unmarshallFromTemplate(ml, opcPackage, mappings)).getValue();
    }

    protected static abstract class DrawingML_base implements IShape {

        final DocxOperationDocument operationDocument;
        final protected IContentAccessor parentGroup;

        protected DrawingML_base(DocxOperationDocument operationDocument, IContentAccessor parent) {
            this.operationDocument = operationDocument;
            parentGroup = parent;
        }
        protected IShape getNext(Object o) {
            if(parentGroup!=null) {
                final DLList<Object> parentGroupContent = (DLList<Object>)parentGroup.getContent();
                final int index = parentGroupContent.indexOf(o)+1;
                if(index>0&&index<parentGroupContent.size()) {
                    final Object child = parentGroupContent.get(index);
                    if(child instanceof CTWordprocessingGroup) {
                        return new DrawingML_group(operationDocument, (CTWordprocessingGroup)child, parentGroup);
                    }
                    else if(child instanceof CTWordprocessingCanvas) {
                        return new DrawingML_canvas(operationDocument, (CTWordprocessingCanvas)child, parentGroup);
                    }
                    else if (child instanceof CTWordprocessingShape) {
                        return new DrawingML_shape(operationDocument, (CTWordprocessingShape)child, parentGroup);
                    }
                    else if (child instanceof Pic) {
                        return new DrawingML_pic(operationDocument, (Pic)child, parentGroup);
                    }
                }
            }
            return null;
        }
        @Override
        public Object getChild() {
            return null;
        }
        @Override
        public Object insertChild(int number, ComponentType type) throws Exception {
            return null;
        }
        protected void applyChildExtensionToParent(JSONObject attrs) {
            if(parentGroup instanceof CTWordprocessingGroup) {
                final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
                if(drawingAttrs!=null) {
                    final Object left = drawingAttrs.opt(OCKey.LEFT.value());
                    final Object top = drawingAttrs.opt(OCKey.TOP.value());
                    final Object width = drawingAttrs.opt(OCKey.WIDTH.value());
                    final Object height = drawingAttrs.opt(OCKey.HEIGHT.value());
                    if(width instanceof Integer&&height instanceof Integer) {
                        final CTGroupTransform2D groupTransform = ((CTWordprocessingGroup)parentGroup).getXfrm(true);
                        final Rectangle rect =
                           new Rectangle(left instanceof Integer ? (Integer)left : 0, top instanceof Integer ? (Integer)top : 0,
                               (Integer)width, (Integer)height);
                        final CTPoint2D groupOff = groupTransform.getChOff(true);
                        CTPositiveSize2D groupExt = groupTransform.getChExt(false);
                        if(groupExt==null) {
                            groupOff.setX((Integer)left);
                            groupOff.setY((Integer)top);
                            groupExt = groupTransform.getChExt(true);
                            groupExt.setCx((Integer)width);
                            groupExt.setCy((Integer)height);
                        }
                        else {
                            final Rectangle grpRect = new Rectangle((int)groupOff.getX(), (int)groupOff.getY(), (int)groupExt.getCx(), (int)groupExt.getCy());
                            final Rectangle aNewGrpRect = grpRect.union(rect);
                            groupOff.setX((long)aNewGrpRect.getX());
                            groupOff.setY((long)aNewGrpRect.getY());
                            groupExt.setCx((long)aNewGrpRect.getWidth());
                            groupExt.setCy((long)aNewGrpRect.getHeight());
                        }
                    }
                }
            }
        }
    }

    public static class DrawingML_shape extends DrawingML_base implements IShapeTxBxContentAccessor {

    	final private CTWordprocessingShape shape;

		protected DrawingML_shape(DocxOperationDocument operationDocument, CTWordprocessingShape o, IContentAccessor parent) {
			super(operationDocument, parent);
			shape = o;
		}

		@Override
		public ShapeType getType() {
		    return shape.getNonVisualConnectorProperties(false)!=null ? ShapeType.CONNECTOR : ShapeType.SHAPE;
		}

		@Override
		public IShape getNext() {
			return getNext(shape);
		}

		@Override
		public Object getChild() {
		    CTTxbxContent textboxContent = getTextboxContent(false);
		    if(textboxContent==null) {
		        return null;
		    }
			final RootComponent rootComponent = new RootComponent(operationDocument, textboxContent);
			return rootComponent.getNextChildComponent(null, null);
		}

	    @Override
	    public CTTxbxContent getTextboxContent(boolean forceCreate) {
            final CTTextboxInfo txBox = shape.getTxbx(forceCreate);
            if(txBox==null) {
                return null;
            }
            return txBox.getTxbxContent(forceCreate);
	    }

	    @Override
	    public void setTextboxContent(CTTxbxContent textBox) {
	        shape.getTxbx(true).setTxbxContent(textBox);
	    }

		@Override
    	public Object insertChild(int number, ComponentType type) {
		    final CTTxbxContent txBoxContent = getTextboxContent(true);
			final RootComponent rootComponent = new RootComponent(super.operationDocument, txBoxContent);
			IComponent<OfficeOpenXMLOperationDocument> c = rootComponent.getNextChildComponent(null, null);
        	if(c!=null) {
        		c = c.getComponent(number);
        	}
        	return rootComponent.insertChildComponent(rootComponent, new DLNode<Object>(txBoxContent), number, c, type, null);
		}

		@Override
    	public void applyAttrsFromJSON(JSONObject attrs)
    	    throws JSONException, InvalidFormatException, PartUnrecognisedException {

		    if(attrs.opt(OCKey.CONNECTOR.value())!=null) {
		        DMLHelper.applyConnectionPropertiesFromJson(attrs, shape);
		    }
		    applyChildExtensionToParent(attrs);
			Shape.applyAttrsFromJSON(operationDocument, attrs, shape, parentGroup==null, false);
		}

		@Override
    	public JSONObject createJSONAttrs(JSONObject attrs)
    		throws JSONException, FilterException {

		    if(getType()==ShapeType.CONNECTOR) {
		        DMLHelper.createJsonFromConnectionProperties(attrs, shape);
		    }
			Shape.createJSONAttrs(operationDocument, attrs, shape, parentGroup==null);
			return attrs;
    	}
    }

    public static class DrawingML_pic extends DrawingML_base {

    	final private Pic pic;

		protected DrawingML_pic(DocxOperationDocument operationDocument, Pic o, IContentAccessor parent) {
			super(operationDocument, parent);
			pic = o;
		}
		@Override
		public ShapeType getType() {
			return ShapeType.IMAGE;
		}
		@Override
		public IShape getNext() {
			return getNext(pic);
		}
		@Override
    	public void applyAttrsFromJSON(JSONObject attrs)
    	    throws JSONException, InvalidFormatException, PartUnrecognisedException {

            applyChildExtensionToParent(attrs);
			Picture.applyAttrsFromJSON(operationDocument, attrs, operationDocument.getContextPart(), pic, parentGroup==null);
		}
		@Override
		public JSONObject createJSONAttrs(JSONObject attrs)
    		throws JSONException, FilterException {

			Picture.createJSONAttrs(operationDocument, attrs, operationDocument.getContextPart(), pic, parentGroup==null);
            return attrs;
    	}
    }

    public static class DrawingML_group extends DrawingML_base {

    	final private CTWordprocessingGroup group;

		protected DrawingML_group(DocxOperationDocument operationDocument, CTWordprocessingGroup o, IContentAccessor parent) {
			super(operationDocument, parent);
			group = o;
		}
		@Override
		public ShapeType getType() {
			return ShapeType.GROUP;
		}
        @Override
        public IShape getNext() {
            return getNext(group);
        }
        @Override
        public Object getChild() {

            final DLList<Object> groupContent = group.getContent();
            if(!groupContent.isEmpty()) {
                final Object child = groupContent.get(0);
                if(child instanceof CTWordprocessingGroup) {
                    return new DrawingML_group(operationDocument, (CTWordprocessingGroup)child, group);
                }
                else if (child instanceof CTWordprocessingShape) {
                    return new DrawingML_shape(operationDocument, (CTWordprocessingShape)child, group);
                }
                else if (child instanceof Pic) {
                    return new DrawingML_pic(operationDocument, (Pic)child, group);
                }
            }
            return null;
        }
        @Override
        public Object insertChild(int number, ComponentType type)
            throws JAXBException {

            final DLList<Object> groupContent = group.getContent();
            if(type==ComponentType.AC_GROUP) {
                final CTWordprocessingGroup child = createDrawingMLGroup();
                groupContent.add(number, child);
                return new DrawingML_group(super.operationDocument, child, group);
            }
            else if(type==ComponentType.AC_SHAPE) {
                final CTWordprocessingShape child = createDrawingMLShape(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_shape(super.operationDocument, child, group);
            }
            else if(type==ComponentType.AC_CONNECTOR) {
                final CTWordprocessingShape child = createDrawingMLConnectorShape(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_shape(super.operationDocument, child, group);
            }
            else if(type==ComponentType.AC_IMAGE) {
                final Pic child = createDrawingMLPic(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_pic(super.operationDocument, child, group);
            }
            return null;
        }
		@Override
    	public void applyAttrsFromJSON(JSONObject attrs) throws Exception  {

		    applyChildExtensionToParent(attrs);
			GroupShape.applyAttrsFromJSON(operationDocument, attrs, group, parentGroup==null);
		}
		@Override
    	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

			GroupShape.createJSONAttrs(operationDocument, attrs, group, parentGroup==null);
    		return attrs;
    	}
    }

    public static class DrawingML_canvas extends DrawingML_base {

        final private CTWordprocessingCanvas canvas;

        protected DrawingML_canvas(DocxOperationDocument operationDocument, CTWordprocessingCanvas o, IContentAccessor parent) {
            super(operationDocument, parent);
            canvas = o;
        }
        @Override
        public ShapeType getType() {
            return ShapeType.GROUP;
        }
        @Override
        public IShape getNext() {
            return getNext(canvas);
        }
        @Override
        public Object getChild() {

            final DLList<Object> groupContent = canvas.getContent();
            if(!groupContent.isEmpty()) {
                final Object child = groupContent.get(0);
                if(child instanceof CTWordprocessingGroup) {
                    return new DrawingML_group(operationDocument, (CTWordprocessingGroup)child, canvas);
                }
                else if (child instanceof CTWordprocessingShape) {
                    return new DrawingML_shape(operationDocument, (CTWordprocessingShape)child, canvas);
                }
                else if (child instanceof Pic) {
                    return new DrawingML_pic(operationDocument, (Pic)child, canvas);
                }
            }
            return null;
        }
        @Override
        public Object insertChild(int number, ComponentType type)
            throws JAXBException {

            final DLList<Object> groupContent = canvas.getContent();
            if(type==ComponentType.AC_GROUP) {
                final CTWordprocessingGroup child = createDrawingMLGroup();
                groupContent.add(number, child);
                return new DrawingML_group(super.operationDocument, child, canvas);
            }
            else if(type==ComponentType.AC_SHAPE) {
                final CTWordprocessingShape child = createDrawingMLShape(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_shape(super.operationDocument, child, canvas);
            }
            else if(type==ComponentType.AC_CONNECTOR) {
                final CTWordprocessingShape child = createDrawingMLConnectorShape(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_shape(super.operationDocument, child, canvas);
            }
            else if(type==ComponentType.AC_IMAGE) {
                final Pic child = createDrawingMLPic(operationDocument.getPackage());
                groupContent.add(number, child);
                return new DrawingML_pic(super.operationDocument, child, canvas);
            }
            return null;
        }
        @Override
        public void applyAttrsFromJSON(JSONObject attrs) throws Exception {
            applyChildExtensionToParent(attrs);
//            GroupShape.applyAttrsFromJSON(operationDocument, attrs, canvas, true);
        }
        @Override
        public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

//            GroupShape.createJSONAttrs(createOperationHelper.getOperationDocument(), attrs, canvas, parentGroup==null);
            return attrs;
        }
    }

    public static class DrawingML_root implements IShape, IShapeTxBxContentAccessor {

    	private final Drawing rootObject;

    	// drawing is containing wps and wpg if available, otherwise it is null
    	private DrawingML_base drawing;

    	public DrawingML_root(DocxOperationDocument operationDocument, Drawing o) {
    		rootObject = o;
			drawing = null;
			final List<GraphicBase> anchorOrInlineList = rootObject.getAnchorOrInline();
            if(!anchorOrInlineList.isEmpty()) {
                final GraphicBase anchorOrInline = anchorOrInlineList.get(0);
                final Graphic graphic = anchorOrInline!=null?anchorOrInline.getGraphic():null;
    			if(graphic!=null) {
                    final GraphicData graphicData = graphic.getGraphicData();
                    if(graphicData!=null) {
                    	final String uri = graphicData.getUri();
                        final List<Object> any = graphicData.getAny();
                    	if(uri!=null&&!any.isEmpty()) {
	                        if(uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingGroup")) {
	                            Object group = any.get(0);
	                            if(group instanceof JAXBElement && ((JAXBElement<?>)group).getDeclaredType().getName().equals("org.docx4j.dml.wordprocessingGroup2010.CTWordprocessingGroup") ) {
	                            	group = ((JAXBElement<?>)group).getValue();
	                            	any.set(0, group);
	                            }
	                            if(group instanceof CTWordprocessingGroup) {
	                            	drawing = new DrawingML_group(operationDocument, (CTWordprocessingGroup)group, null);
	                            }
	                        }
	                        else if(uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingShape")) {
	                            Object shape = any.get(0);
	                            if(shape instanceof JAXBElement && ((JAXBElement<?>)shape).getDeclaredType().getName().equals("org.docx4j.dml.wordprocessingShape2010.CTWordprocessingShape") ) {
	                            	shape = ((JAXBElement<?>)shape).getValue();
	                            	any.set(0, shape);
	                            }
	                            if(shape instanceof CTWordprocessingShape) {
	                            	drawing = new DrawingML_shape(operationDocument, (CTWordprocessingShape)shape, null);
	                            }
	                        }
                            else if(uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas")) {
                                Object canvas = any.get(0);
                                if(canvas instanceof JAXBElement && ((JAXBElement<?>)canvas).getDeclaredType().getName().equals("org.docx4j.dml.wordprocessingCanvas2010.CTWordprocessingCanvas") ) {
                                    canvas = ((JAXBElement<?>)canvas).getValue();
                                    any.set(0, canvas);
                                }
                                if(canvas instanceof CTWordprocessingCanvas) {
                                    drawing = new DrawingML_canvas(operationDocument, (CTWordprocessingCanvas)canvas, null);
                                }
                            }
	                        else if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/picture")) {
	                            Object shape = any.get(0);
	                            if(shape instanceof JAXBElement && ((JAXBElement<?>)shape).getDeclaredType().getName().equals("org.docx4j.dml.picture.Pic") ) {
	                            	shape = ((JAXBElement<?>)shape).getValue();
	                            	any.set(0, shape);
	                            }
	                            if(shape instanceof Pic) {
	                            	drawing = new DrawingML_pic(operationDocument, (Pic)shape, null);
	                            }
	                        }
                    	}
                    }
    			}
            }
		}

        @Override
        public CTTxbxContent getTextboxContent(boolean forceCreate) {
            if(drawing instanceof IShapeTxBxContentAccessor) {
                return ((IShapeTxBxContentAccessor)drawing).getTextboxContent(forceCreate);
            }
            return null;
        }

        @Override
        public void setTextboxContent(CTTxbxContent textBoxContent) {
            if(drawing instanceof IShapeTxBxContentAccessor) {
                ((IShapeTxBxContentAccessor) drawing).setTextboxContent(textBoxContent);
            }
        }

        final static String namespaces = " xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" "
                + "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" "
                + "xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\"";

        public static Inline createInline(boolean linked, String relId, int cx, int cy)
            throws JAXBException {

            String type = linked ? "r:link" : "r:embed";

            String ml =
                "<wp:inline distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\"" + namespaces + ">"
                + "<wp:extent cx=\"${cx}\" cy=\"${cy}\"/>"
                + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>" + //l=\"19050\"
                "<wp:docPr name=\"${filenameHint}\" descr=\"${altText}\"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" noChangeAspect=\"1\"/></wp:cNvGraphicFramePr><a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">"
                + "<a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
                + "<pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\"><pic:nvPicPr><pic:cNvPr name=\"${filenameHint}\"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill>"
                + "<a:blip ${rEmbedId}/><a:stretch><a:fillRect/></a:stretch></pic:blipFill>"
                + "<pic:spPr><a:xfrm><a:off x=\"0\" y=\"0\"/><a:ext cx=\"${cx}\" cy=\"${cy}\"/></a:xfrm><a:prstGeom prst=\"rect\"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic>"
                + "</wp:inline>";

            java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();

            mappings.put("cx", Integer.toString(cx));
            mappings.put("cy", Integer.toString(cy));
            mappings.put("filenameHint", "");
            mappings.put("altText", "");
            mappings.put("rEmbedId", relId.isEmpty() ? "" : type + "=\"" + relId + "\"");

            Inline inline = null;
            Object o = org.docx4j.XmlUtils.unmarshallFromTemplate(ml, /*TODO provide package*/ null, mappings);
            inline = (Inline) ((JAXBElement<?>) o).getValue();
            return inline;
        }

        public static Anchor createAnchor(boolean linked, String relId, int cx, int cy)
            throws JAXBException {

            String type = linked ? "r:link" : "r:embed";

            String ml =
                "<wp:anchor simplePos=\"0\" distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\" allowOverlap=\"1\" layoutInCell=\"1\"" + namespaces + ">"
                + "<wp:simplePos x=\"0\" y=\"0\"/>"
                + "<wp:positionH relativeFrom=\"column\"><wp:posOffset>0</wp:posOffset></wp:positionH>"
                + "<wp:positionV relativeFrom=\"paragraph\"><wp:posOffset>0</wp:posOffset></wp:positionV>"
                + "<wp:extent cx=\"${cx}\" cy=\"${cy}\"/>"
                + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/><wp:wrapTopAndBottom/>"
                + "<wp:docPr name=\"${filenameHint}\" descr=\"${altText}\"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" noChangeAspect=\"1\"/></wp:cNvGraphicFramePr><a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">"
                + "<a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
                + "<pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\"><pic:nvPicPr><pic:cNvPr name=\"${filenameHint}\"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill>"
                + "<a:blip ${rEmbedId}/><a:stretch><a:fillRect/></a:stretch></pic:blipFill>"
                + "<pic:spPr><a:xfrm><a:off x=\"0\" y=\"0\"/><a:ext cx=\"${cx}\" cy=\"${cy}\"/></a:xfrm><a:prstGeom prst=\"rect\"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic>"
                + "</wp:anchor>";

            java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();

            mappings.put("cx", Integer.toString(cx));
            mappings.put("cy", Integer.toString(cy));
            mappings.put("filenameHint", "");
            mappings.put("altText", "");
            mappings.put("rEmbedId", relId.isEmpty() ? "" : type + "=\"" + relId + "\"");

            Anchor anchor = null;
            Object o = org.docx4j.XmlUtils.unmarshallFromTemplate(ml, /*TODO provide package*/ null, mappings);
            anchor = (Anchor) ((JAXBElement<?>) o).getValue();
            return anchor;
        }

        private GraphicBase getAnchorOrInline() {
            final List<GraphicBase> anchorOrInlineContent = rootObject.getAnchorOrInline();
            return anchorOrInlineContent.size() > 0 ? anchorOrInlineContent.get(0) : null;
        }

        private void setAnchorOrInline(GraphicBase o) {
            final List<GraphicBase> anchorOrInlineContent = rootObject.getAnchorOrInline();
            anchorOrInlineContent.clear();
            anchorOrInlineContent.add(o);
        }

        private Inline switchToInlineObject()
            throws JAXBException {

            Anchor anchor = (Anchor)getAnchorOrInline();
            Graphic graphic = anchor.getGraphic();
            Inline inline = createInline(true, "", 0, 0);
            inline.setParent(anchor.getParent());
            inline.setNonVisualDrawingProperties(anchor.getNonVisualDrawingProperties(false));
            inline.setNonVisualDrawingShapeProperties(anchor.getNonVisualDrawingShapeProperties(false));
            inline.setGraphic(graphic);
            inline.setExtent(anchor.getExtent());
            inline.setDistB(anchor.getDistB());
            inline.setDistL(anchor.getDistL());
            inline.setDistR(anchor.getDistR());
            inline.setDistT(anchor.getDistT());
            setAnchorOrInline(inline);
            return inline;
        }

        private Anchor switchToAnchorObject()
            throws JAXBException {

            Inline inline = (Inline)getAnchorOrInline();
            Graphic graphic = inline.getGraphic();
            Anchor anchor = createAnchor(true, "", 0, 0);
            anchor.setParent(inline.getParent());
            anchor.setNonVisualDrawingProperties(inline.getNonVisualDrawingProperties(false));
            anchor.setNonVisualDrawingShapeProperties(inline.getNonVisualDrawingShapeProperties(false));
            anchor.setGraphic(graphic);
            anchor.setExtent(inline.getExtent());
            anchor.setDistB(inline.getDistB());
            anchor.setDistL(inline.getDistL());
            anchor.setDistR(inline.getDistR());
            anchor.setDistT(inline.getDistT());
            anchor.setAllowOverlap(true);
            setAnchorOrInline(anchor);
            return anchor;
        }

    	@Override
		public ShapeType getType() {
            ShapeType type = ShapeType.UNDEFINED;
            final List<GraphicBase> anchorOrInlineList = rootObject.getAnchorOrInline();
            if(anchorOrInlineList.isEmpty()) {
            	return ShapeType.UNDEFINED;
            }
            final Object anchorOrInline = anchorOrInlineList.get(0);
            Graphic graphic = null;
			if(anchorOrInline instanceof Anchor)
                graphic = ((Anchor)anchorOrInline).getGraphic();
            else if (anchorOrInline instanceof Inline)
                graphic = ((Inline)anchorOrInline).getGraphic();
			if(graphic==null) {
				return ShapeType.UNDEFINED;
			}
            final GraphicData graphicData = graphic.getGraphicData();
            if(graphicData!=null) {
                final String uri = graphicData.getUri();
                if(uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingShape")) {
                    if(!graphicData.getAny().isEmpty()) {
                        type = ShapeType.SHAPE;
                        Object shape = graphicData.getAny().get(0);
                        if(shape instanceof JAXBElement && ((JAXBElement<?>)shape).getDeclaredType().getName().equals("org.docx4j.dml.wordprocessingShape2010.CTWordprocessingShape") ) {
                            shape = ((JAXBElement<?>)shape).getValue();
                        }
                        if(shape instanceof CTWordprocessingShape) {
                            if(((CTWordprocessingShape)shape).getNonVisualConnectorProperties(false)!=null) {
                                type = ShapeType.CONNECTOR;
                            }
                        }
                    }
                }
                else if (uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"))
                	type = ShapeType.GROUP;
                else if (uri.equalsIgnoreCase("http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"))
                    type = ShapeType.GROUP;
                else if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/picture"))
                    type = ShapeType.IMAGE;
                else if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/diagram"))
                    type = ShapeType.DIAGRAM;
                else if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/chart"))
                    type = ShapeType.CHART;
                else if (uri.equalsIgnoreCase("http://schemas.microsoft.com/office/drawing/2014/chartex"))
                    type = ShapeType.CHART;
            }
            return type;
		}

    	@Override
    	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

            if(drawing!=null) {
                drawing.applyAttrsFromJSON(attrs);
            }
            final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
	        if(drawingAttrs!=null) {
	            ObjectFactory dmlObjectFactory = new ObjectFactory();

	            GraphicBase anchorOrInline = getAnchorOrInline();
	            Object inlineObj = drawingAttrs.opt(OCKey.INLINE.value());
	            if(inlineObj!=null) {
	                boolean inline = inlineObj instanceof Boolean ? (Boolean)inlineObj : true;
	                if((anchorOrInline instanceof Anchor)&&inline)
	                    anchorOrInline = switchToInlineObject();
	                else if((anchorOrInline instanceof Inline)&&(inline==false))
	                    anchorOrInline = switchToAnchorObject();
	            }
	            Iterator<String> keys = drawingAttrs.keys();
	            while(keys.hasNext()) {
	                String attr = keys.next();
	                Object value = drawingAttrs.get(attr);
	                if(attr.equals(OCKey.WIDTH.value())) {
	                    if(value instanceof Integer) {
	                        CTPositiveSize2D extend = anchorOrInline.getExtent();
	                        if(extend==null) {
	                            extend = Context.getDmlObjectFactory().createCTPositiveSize2D();
	                            anchorOrInline.setExtent(extend);
	                        }
	                        extend.setCx(((Integer)value).intValue()*360);
	                    }
	                }
	                else if(attr.equals(OCKey.HEIGHT.value())) {
	                    if(value instanceof Integer) {
	                        CTPositiveSize2D extend = anchorOrInline.getExtent();
	                        if(extend==null) {
	                            extend = Context.getDmlObjectFactory().createCTPositiveSize2D();
	                            anchorOrInline.setExtent(extend);
	                        }
	                        extend.setCy(((Integer) value).intValue()*360);
	                    }
	                }
	                else if(attr.equals(OCKey.MARGIN_LEFT.value())) {
	                    if(value instanceof Integer) {
	                        anchorOrInline.setDistL(((Integer)value).longValue()*360);
	                    }
	                }
	                else if(attr.equals(OCKey.MARGIN_TOP.value())) {
	                    if(value instanceof Integer) {
	                        anchorOrInline.setDistT(((Integer)value).longValue()*360);
	                    }
	                }
	                else if(attr.equals(OCKey.MARGIN_RIGHT.value())) {
	                    if(value instanceof Integer) {
	                        anchorOrInline.setDistR(((Integer)value).longValue()*360);
	                    }
	                }
	                else if(attr.equals(OCKey.MARGIN_BOTTOM.value())) {
	                    if(value instanceof Integer) {
	                        anchorOrInline.setDistB(((Integer)value).longValue()*360);
	                    }
	                }
	                // !!!!.... only attributes for anchored drawings are following now !!!!
	                else if(anchorOrInline instanceof Anchor) {
	                    final Anchor anchor = (Anchor)anchorOrInline;
	                    if(attr.equals(OCKey.ANCHOR_HOR_BASE.value())) {
	                        if(value instanceof String) {
	                            CTPosH ctPosH = anchor.getPositionH();
	                            if(ctPosH==null)
	                                ctPosH = dmlObjectFactory.createCTPosH();
	                            ctPosH.setRelativeFrom(STRelFromH.fromValue((String)value));
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_HOR_ALIGN.value())) {
	                        if(value instanceof String) {
	                            CTPosH ctPosH = anchor.getPositionH();
	                            if(ctPosH==null)
	                                ctPosH = dmlObjectFactory.createCTPosH();
	                            if(((String)value).equals("offset"))
	                                ctPosH.setAlign(null);
	                            else
	                                ctPosH.setAlign(STAlignH.fromValue((String)value));
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_HOR_OFFSET.value())) {
	                        if(value instanceof Integer) {
	                            CTPosH ctPosH = anchor.getPositionH();
	                            if(ctPosH==null)
	                                ctPosH = dmlObjectFactory.createCTPosH();
	                            ctPosH.setPosOffset((Integer)value * 360);
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_VERT_BASE.value())) {
	                        if(value instanceof String) {
	                            CTPosV ctPosV = anchor.getPositionV();
	                            if(ctPosV==null)
	                                ctPosV = dmlObjectFactory.createCTPosV();
	                            ctPosV.setRelativeFrom(STRelFromV.fromValue((String)value));
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_VERT_ALIGN.value())) {
	                        if(value instanceof String) {
	                            CTPosV ctPosV = anchor.getPositionV();
	                            if(ctPosV==null)
	                                ctPosV = dmlObjectFactory.createCTPosV();
	                            if(((String)value).equals("offset"))
	                                ctPosV.setAlign(null);
	                            else
	                                ctPosV.setAlign(STAlignV.fromValue((String)value));
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_VERT_OFFSET.value())) {
	                        if(value instanceof Integer) {
	                            CTPosV ctPosV = anchor.getPositionV();
	                            if(ctPosV==null)
	                                ctPosV = dmlObjectFactory.createCTPosV();
	                            ctPosV.setPosOffset((Integer)value * 360);
	                        }
	                    }
	                    else if(attr.equals(OCKey.TEXT_WRAP_MODE.value())) {
	                        if(value instanceof String) {
	                            CTWrapNone      ctWrapNone = null;
	                            CTWrapSquare    ctWrapSquare = null;
	                            CTWrapTight     ctWrapTight = null;
	                            CTWrapThrough   ctWrapThrough = null;
	                            CTWrapTopBottom ctWrapTopBottom = null;
	                            if(((String)value).equals("none")) {
	                                ctWrapNone = anchor.getWrapNone();
	                                if(ctWrapNone==null)
	                                    ctWrapNone = dmlObjectFactory.createCTWrapNone();
	                            }
	                            else if(((String)value).equals("square")) {
	                                ctWrapSquare = anchor.getWrapSquare();
	                                if(ctWrapSquare==null) {
	                                    ctWrapSquare = dmlObjectFactory.createCTWrapSquare();
	                                    ctWrapSquare.setWrapText(STWrapText.BOTH_SIDES);
	                                }
	                            }
	                            else if(((String)value).equals("tight")) {
	                                ctWrapTight = anchor.getWrapTight();
	                                if(ctWrapTight==null) {
	                                    ctWrapTight = dmlObjectFactory.createCTWrapTight();
	                                    ctWrapTight.setWrapText(STWrapText.BOTH_SIDES);
	                                    ctWrapTight.setWrapPolygon(createWrapPath());
	                                }
	                            }
	                            else if(((String)value).equals("through")) {
	                                ctWrapThrough = anchor.getWrapThrough();
	                                if(ctWrapThrough==null) {
	                                    ctWrapThrough = dmlObjectFactory.createCTWrapThrough();
	                                    ctWrapThrough.setWrapText(STWrapText.BOTH_SIDES);
	                                    ctWrapThrough.setWrapPolygon(createWrapPath());
	                                }
	                            }
	                            else if(((String)value).equals("topAndBottom")) {
	                                ctWrapTopBottom = anchor.getWrapTopAndBottom();
	                                if(ctWrapTopBottom==null)
	                                    ctWrapTopBottom = dmlObjectFactory.createCTWrapTopBottom();
	                            }
	                            anchor.setWrapNone(ctWrapNone);
	                            anchor.setWrapSquare(ctWrapSquare);
	                            anchor.setWrapTight(ctWrapTight);
	                            anchor.setWrapThrough(ctWrapThrough);
	                            anchor.setWrapTopAndBottom(ctWrapTopBottom);
	                        }
	                    }
	                    else if(attr.equals(OCKey.ANCHOR_BEHIND_DOC.value())) {
	                        if(value instanceof Boolean){
	                            anchor.setBehindDoc((Boolean) value);
	                        }else{
	                            anchor.setBehindDoc(false);
	                        }
                        }
	                    //wp14 enhancements:
                        else if(attr.equals(OCKey.ANCHOR_LAYER_ORDER.value())) {
                            if (value instanceof Number) {
                                anchor.setRelativeHeight((((Number)value)).longValue());
                            } else {
                                anchor.setRelativeHeight(0);
                            }
                        }
                        else if(attr.equals(OCKey.SIZE_REL_H_FROM.value())) {
                            if(value instanceof String) {
                                anchor.getSizeRelH(true).setRelativeFrom(STSizeRelFromH.fromValue((String)value));
                            }
                            else {
                                anchor.setSizeRelH(null);
                            }
                        }
                        else if(attr.equals(OCKey.SIZE_REL_V_FROM.value())) {
                            if(value instanceof String) {
                                anchor.getSizeRelV(true).setRelativeFrom(STSizeRelFromV.fromValue((String)value));
                            }
                            else {
                                anchor.setSizeRelV(null);
                            }
                        }
                        else if(attr.equals(OCKey.PCT_WIDTH.value())) {
                            if(!(value instanceof Integer)) {
                                value = Integer.valueOf(100000);
                            }
                            anchor.getSizeRelH(true).setPctWidth(new STPositivePercentage((Integer)value));
                        }
                        else if(attr.equals(OCKey.PCT_HEIGHT.value())) {
                            if(!(value instanceof Integer)) {
                                value = Integer.valueOf(100000);
                            }
                            anchor.getSizeRelV(true).setPctHeight(new STPositivePercentage((Integer)value));
                        }
                        else if(attr.equals(OCKey.PCT_POS_H_OFFSET.value())) {
                            final CTPosH posH = anchor.getPositionH();
                            if(posH!=null) {
                                if(value instanceof Integer) {
                                    posH.setPctPosHOffset(new STPercentage((Integer)value));
                                }
                                else {
                                    posH.setPctPosHOffset(null);
                                }
                            }
                        }
                        else if(attr.equals(OCKey.PCT_POS_V_OFFSET.value())) {
                            final CTPosV posV = anchor.getPositionV();
                            if(posV!=null) {
                                if(value instanceof Integer) {
                                    posV.setPctPosVOffset(new STPercentage((Integer)value));
                                }
                                else {
                                    posV.setPctPosVOffset(null);
                                }
                            }
                        }
	                }
	            }

	            // either "align" or "position" is to be used, otherwise Word won't open the document
	            if(anchorOrInline instanceof Anchor) {
	                final Anchor anchor = (Anchor)anchorOrInline;
	                CTPosH ctPosH = anchor.getPositionH();
	                if(ctPosH!=null) {
	                    if(ctPosH.getAlign()!=null)
	                        ctPosH.setPosOffset(null);
	                    else {
	                        Integer pos = ctPosH.getPosOffset();
	                        if(pos==null)
	                            ctPosH.setPosOffset(0);
	                    }
	                }
	                CTPosV ctPosV = anchor.getPositionV();
	                if(ctPosV!=null) {
	                    if(ctPosV.getAlign()!=null)
	                        ctPosV.setPosOffset(null);
	                    else {
	                        Integer pos = ctPosV.getPosOffset();
	                        if(pos==null)
	                            ctPosV.setPosOffset(0);
	                    }
	                }
	                // textwrapside can't be set inside the above while loop, because it depends to the
	                // wrapmode that is used. late applying preserves us from taking care of the property order.
	                if (drawingAttrs.has(OCKey.TEXT_WRAP_SIDE.value())) {
	                    Object value = drawingAttrs.get(OCKey.TEXT_WRAP_SIDE.value());
	                    if(value instanceof String) {
	                        String wrapText = ((String)value).equals("both") ? "bothSides" : (String)value;
	                        if(anchor.getWrapSquare()!=null)
	                            anchor.getWrapSquare().setWrapText(STWrapText.fromValue(wrapText));
	                        else if (anchor.getWrapTight()!=null)
	                            anchor.getWrapTight().setWrapText(STWrapText.fromValue(wrapText));
	                        else if (anchor.getWrapThrough()!=null)
	                            anchor.getWrapThrough().setWrapText(STWrapText.fromValue(wrapText));
	                    }
	                }
	            }
                applyEffectExtent(anchorOrInline);
	        }
    	}

		private void applyEffectExtent(GraphicBase graphicBase) {
            final CTPositiveSize2D extent = graphicBase.getExtent();
            if(extent!=null) {
    		    final Graphic graphic = graphicBase.getGraphic();
    		    if(graphic!=null) {
    		        final GraphicData graphicData = graphic.getGraphicData();
    		        if(graphicData!=null) {
    		            final Pic pic = graphicData.getPic();
    		            if(pic!=null) {
    		                final CTTransform2D xfrm = pic.getXfrm(false);
    		                if(xfrm!=null) {
    		                    int rot = xfrm.getRot();

    		                    if(rot > 10800000) {            // rot > 180
                                    rot = 21600000 - rot;       // rot = 360 - rot
                                }
                                if(rot > 5400000) {             // rot > 90
                                    rot = 10800000 - rot;       // rot = 180 - rot
                                }
    		                    if(rot!=0) {
    		                        long cx = extent.getCx();
    		                        long cy = extent.getCy();

    		                        if(rot >= 2700000 && xfrm.getRot() != 8100000 && xfrm.getRot() != 18900000) {         // rot >= 45
    		                            rot = 5400000 - rot;

    		                            long t = cx;
    		                            cx = cy;
    		                            cy = t;

    		                        }

    		                        if(rot!=0) {
        		                        final CTEffectExtent effectExtent = graphicBase.getEffectExtent(true);
    		                            final double rad = (rot / 60000.0) * Math.PI / 180.0;
    		                            final double xDiffHalf = Math.abs(Math.sin(rad) * cy * 0.5);
    		                            final double yDiffHalf = Math.abs(Math.sin(rad) * cx * 0.5);
    		                            final long xExt = Double.valueOf(xDiffHalf).longValue();
    		                            final long yExt = Double.valueOf(yDiffHalf).longValue();
    		                            effectExtent.setL(xExt);
                                        effectExtent.setR(xExt);
                                        effectExtent.setT(yExt);
                                        effectExtent.setB(yExt);
    		                        }
    		                    }
    		                }
    		            }
    		        }
    		    }
            }
		}

		// creates a dummy wrap path that has to be used for wrapTight and wrapThrough wrapping
		public static CTWrapPath createWrapPath() {
			final CTWrapPath wrapPath = new CTWrapPath();
			wrapPath.setStart(new CTPoint2D(0, 0));
			wrapPath.getLineTo().add(new CTPoint2D(0, 100));
			wrapPath.getLineTo().add(new CTPoint2D(100, 100));
			wrapPath.getLineTo().add(new CTPoint2D(100, 0));
			wrapPath.getLineTo().add(new CTPoint2D(0, 0));
			return wrapPath;
		}

		final static String[] enumRelFromH = { "margin", "page", "column", "character", "leftMargin", "rightMargin", "insideMargin", "outsideMargin" };
	    final static String[] enumSTAlignH = { "left", "right", "center", "inside", "outside" };
	    final static String[] enumRelFromV = { "margin", "page", "paragraph", "line", "topMargin", "bottomMargin", "insideMargin", "outsideMargin" };
	    final static String[] enumSTAlignV = { "top", "bottom", "center", "inside", "outside" };
	    final static String[] enumSTWrapText = { "both", "left", "right", "largest" };

		@Override
    	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

            if(drawing!=null) {
                attrs = drawing.createJSONAttrs(attrs);
            }
	        final GraphicBase anchorOrInline = getAnchorOrInline();
            if(anchorOrInline!=null) {

                DMLHelper.createJsonFromNonVisualDrawingProperties(attrs, anchorOrInline);
    	        DMLHelper.createJsonFromNonVisualDrawingShapeProperties(attrs, anchorOrInline);

    	        final JSONObject initialDrawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
    	        final JSONObject drawingAttrs = initialDrawingAttrs!=null ? initialDrawingAttrs : new JSONObject();

    	        DMLHelper.createJsonFromPositiveSize2D(drawingAttrs, anchorOrInline.getExtent());

	            drawingAttrs.put(OCKey.INLINE.value(), anchorOrInline instanceof Inline);

	            // applying margin properties
	            final Long DistL = anchorOrInline.getDistL();
	            final Long DistT = anchorOrInline.getDistT();
	            final Long DistR = anchorOrInline.getDistR();
	            final Long DistB = anchorOrInline.getDistB();
	            if((DistL!=null) && (DistL.intValue() != 0))
	                drawingAttrs.put(OCKey.MARGIN_LEFT.value(), DistL.intValue() / 360);
	            if((DistT!=null) && (DistT.intValue() != 0))
	                drawingAttrs.put(OCKey.MARGIN_TOP.value(), DistT.intValue() / 360);
	            if((DistR!=null) && (DistR.intValue() != 0))
	                drawingAttrs.put(OCKey.MARGIN_RIGHT.value(), DistR.intValue() / 360);
	            if((DistB!=null) && (DistB.intValue() != 0))
	                drawingAttrs.put(OCKey.MARGIN_BOTTOM.value(), DistB.intValue() / 360);

	            // positioning in word
	            if(anchorOrInline instanceof Anchor) {
	                final Anchor anchor = (Anchor)anchorOrInline;
	                // z-index
                    boolean behindDoc = anchor.isBehindDoc();
                    if (behindDoc) {
                        drawingAttrs.put(OCKey.ANCHOR_BEHIND_DOC.value(), behindDoc );
                    }
                    long relHeight = anchor.getRelativeHeight();
                    if (relHeight != 0) {
                        drawingAttrs.put(OCKey.ANCHOR_LAYER_ORDER.value(), relHeight );
                    }
	                // horizontal
	                CTPosH ctPosH = anchor.getPositionH();
	                if(ctPosH!=null) {
	                    STRelFromH stRelFrom = ctPosH.getRelativeFrom();
	                    if(stRelFrom!=null) {
	                        int val = stRelFrom.ordinal();
	                        if(val>=enumRelFromH.length)
	                            val = 0;
	                        drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), enumRelFromH[val] );
	                    }
	                    STAlignH stAlignH = ctPosH.getAlign();
	                    if(stAlignH!=null) {
	                        int val = stAlignH.ordinal();
	                        if(val>=enumSTAlignH.length)
	                            val = 0;
	                        drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), enumSTAlignH[val]);
	                    }
	                    else {
	                        drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "offset");
	                    }
	                    Integer val = ctPosH.getPosOffset();
	                    if(val!=null) {
	                        drawingAttrs.put(OCKey.ANCHOR_HOR_OFFSET.value(), val.intValue() / 360);
	                    }
	                    final STPercentage pctPosHOffset = ctPosH.getPctPosHOffset();
	                    if(pctPosHOffset!=null && pctPosHOffset.getValue()!=null) {
	                        drawingAttrs.put(OCKey.PCT_POS_H_OFFSET.value(), pctPosHOffset.getValue());
	                    }
	                }
	                // vertical
	                CTPosV ctPosV = anchor.getPositionV();
	                if(ctPosV!=null) {
	                    STRelFromV stRelFrom = ctPosV.getRelativeFrom();
	                    if(stRelFrom!=null) {
	                        int val = stRelFrom.ordinal();
	                        if(val>=enumRelFromV.length)
	                            val = 0;
	                        drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), enumRelFromV[val] );
	                    }
	                    STAlignV stAlignV = ctPosV.getAlign();
	                    if(stAlignV!=null) {
	                        int val = stAlignV.ordinal();
	                        if(val>=enumSTAlignV.length)
	                            val = 0;
	                        drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), enumSTAlignV[val]);
	                    }
	                    else {
	                        drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "offset");
	                    }
	                    Integer val = ctPosV.getPosOffset();
	                    if(val!=null) {
	                        drawingAttrs.put(OCKey.ANCHOR_VERT_OFFSET.value(),val.intValue() / 360);
	                    }
                        final STPercentage pctPosVOffset = ctPosV.getPctPosVOffset();
                        if(pctPosVOffset!=null && pctPosVOffset.getValue()!=null) {
                            drawingAttrs.put(OCKey.PCT_POS_V_OFFSET.value(), pctPosVOffset.getValue());
                        }
	                }

	                // wp14 file format enhancements:
	                final CTSizeRelH sizeRelH = anchor.getSizeRelH(false);
	                if(sizeRelH!=null) {
	                    drawingAttrs.put(OCKey.SIZE_REL_H_FROM.value(), sizeRelH.getRelativeFrom().value());
	                    final STPositivePercentage positivePercentage = sizeRelH.getPctWidth();
	                    int pctWidth = positivePercentage != null && positivePercentage.getValue() != null ? positivePercentage.getValue().intValue() : 0;
	                    if(pctWidth > 0) {
	                    	drawingAttrs.put(OCKey.PCT_WIDTH.value(), pctWidth);
		                    if(sizeRelH.getRelativeFrom()==STSizeRelFromH.PAGE) {	// only page supported
		                        final SectPr sectPr = Utils.getDocumentProperties(drawing.operationDocument.getPackage().getMainDocumentPart(), false);
	                            if(sectPr.getPgSz(false)!=null) {
	                                adaptLengthToRel(drawingAttrs, OCKey.WIDTH, pctWidth, (sectPr.getPgSz(true).getW() * 2540) / 1440);
	                            }
		                    }
	                    }
	                }
	                final CTSizeRelV sizeRelV = anchor.getSizeRelV(false);
	                if(sizeRelV!=null) {
	                    drawingAttrs.put(OCKey.SIZE_REL_V_FROM.value(), sizeRelV.getRelativeFrom().value());
	                    final STPositivePercentage positivePercentage = sizeRelV.getPctHeight();
	                    int pctHeight = positivePercentage != null && positivePercentage.getValue() != null ? positivePercentage.getValue().intValue() : 0;
	                    if(pctHeight > 0) {
	                    	drawingAttrs.put(OCKey.PCT_HEIGHT.value(), pctHeight);
	                        if(sizeRelV.getRelativeFrom()==STSizeRelFromV.PAGE) {	// only page supported
	                            final SectPr sectPr = Utils.getDocumentProperties(drawing.operationDocument.getPackage().getMainDocumentPart(), false);
	                            if(sectPr.getPgSz(false)!=null) {
	                                adaptLengthToRel(drawingAttrs, OCKey.HEIGHT, pctHeight, (sectPr.getPgSz(true).getH() * 2540) / 1440);
	                            }
	                        }
	                    }
	                }

	                // textwrapmode
	                String textWrapMode = null;
	                STWrapText textWrapSide = null;
	                CTWrapNone wrapNone = anchor.getWrapNone();
	                if(wrapNone!=null)
	                    textWrapMode = "none";
	                else {
	                    CTWrapSquare wrapSquare = anchor.getWrapSquare();
	                    if(wrapSquare!=null) {
	                        textWrapMode = "square";
	                        textWrapSide = wrapSquare.getWrapText();
	                    }
	                    else {
	                        CTWrapTight wrapTight = anchor.getWrapTight();
	                        if(wrapTight!=null) {
	                            textWrapMode = "tight";
	                            textWrapSide = wrapTight.getWrapText();
	                        }
	                        else {
	                            CTWrapThrough wrapThrough = anchor.getWrapThrough();
	                            if(wrapThrough!=null) {
	                                textWrapMode = "through";
	                                textWrapSide = wrapThrough.getWrapText();
	                            }
	                            else {
	                                CTWrapTopBottom wrapTopAndBottom = anchor.getWrapTopAndBottom();
	                                if(wrapTopAndBottom!=null)
	                                    textWrapMode = "topAndBottom";
	                            }
	                        }
	                    }
	                }
	                if(textWrapMode!=null)
	                    drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), textWrapMode);
	                if(textWrapSide!=null) {
	                    int val = textWrapSide.ordinal();
	                    if(val>=enumSTWrapText.length)
	                        val = 0;
	                    drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), enumSTWrapText[val]);
	                }
	            }
	            if(initialDrawingAttrs==null&&!drawingAttrs.isEmpty()) {
	                attrs.put(OCKey.DRAWING.value(), drawingAttrs);
	            }
	        }
    		return attrs;
    	}

		private void adaptLengthToRel(JSONObject attrs, OCKey prop, long relDistance, long absDistance) throws JSONException {
		    if(absDistance!=0) {
		        final double d = ((absDistance * relDistance) / 100000.0) + 0.5;
		        attrs.put(prop.value(), Double.valueOf(d).longValue());
		    }
		}

		@Override
		public IShape getNext() {

		    return null;
		}

		@Override
		public Object getChild() {

		    return drawing!=null?drawing.getChild():null;
		}

		@Override
		public Object insertChild(int number, ComponentType type) throws Exception {
		    return drawing==null?null:drawing.insertChild(number, type);
		}
    }
}
