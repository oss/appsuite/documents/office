/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.List;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.CTMarkup;
import org.docx4j.wml.CTRPrChange;
import org.docx4j.wml.DelInstrText;
import org.docx4j.wml.DelText;
import org.docx4j.wml.InstrText;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RDBase;
import org.docx4j.wml.RIBase;
import org.docx4j.wml.RPr;
import org.docx4j.wml.RStyle;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblGrid;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.Text;
import org.docx4j.wml.TrPr;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class TextUtils{

    // resetting id which has to be unique
    public static void resetMarkupId(CTMarkup trackChange) {
    	if(trackChange!=null) {
    		trackChange.setId(null);
    	}
    }

    // resetting id which has to be unique
    public static void resetMarkupId(RPr rPr) {
    	if(rPr!=null) {
    		resetMarkupId(rPr.getRPrChange());
    	}
    }

	// resetting id which has to be unique
    public static void resetMarkupId(PPr pPr) {
    	if(pPr!=null) {
    		resetMarkupId(pPr.getPPrChange(false));
	        final ParaRPr rPr = pPr.getRPr(false);
	        if(rPr!=null) {
	        	resetMarkupId(rPr.getRPrChange(false));
	        	resetMarkupId(rPr.getDel(false));
	        	resetMarkupId(rPr.getIns(false));
	        	resetMarkupId(rPr.getMoveFrom(false));
	        	resetMarkupId(rPr.getMoveTo(false));
	        }
    	}
    }

	// resetting id which has to be unique
    public static void resetMarkupId(TrPr trPr) {
    	if(trPr!=null) {
    		resetMarkupId(trPr.getDel(false));
    		resetMarkupId(trPr.getIns(false));
    		resetMarkupId(trPr.getTrPrChange(false));
    	}
    }

	// resetting id which has to be unique
    public static void resetMarkupId(TcPr tcPr) {
    	if(tcPr!=null) {
    		resetMarkupId(tcPr.getCellDel(false));
    		resetMarkupId(tcPr.getCellIns(false));
    		resetMarkupId(tcPr.getCellMerge(false));
    		resetMarkupId(tcPr.getTcPrChange(false));
    	}
    }

	// resetting id which has to be unique (without tbl content)
    public static void resetMarkupId(Tbl tbl) {
    	if(tbl!=null) {
    		final TblGrid tblGrid = tbl.getTblGrid(false);
    		if(tblGrid!=null) {
    			resetMarkupId(tblGrid.getTblGridChange(false));
    		}
    		final TblPr tblPr = tbl.getTblPr(false);
    		if(tblPr!=null) {
    			resetMarkupId(tblPr.getTblPrChange());
    		}
    	}
    }

    private static void delTextToText(int index, Object o) {
    	if(o instanceof IContentAccessor) {
    		final List<Object> content = ((IContentAccessor)o).getContent();
    		for(int i=0; i<content.size(); i++) {
    			delTextToText(i, content.get(i));
    		}
    	}
    	else if(o instanceof DelText) {
			final Text text = Context.getWmlObjectFactory().createText();
			Object parent = ((DelText)o).getParent();
			text.setParent(parent);
			text.setSpace(((DelText)o).getSpace());
			text.setValue(((DelText)o).getValue());
			((IContentAccessor)parent).getContent().set(index, text);
    	}
    	else if(o instanceof DelInstrText) {
    	    final InstrText instrText = new InstrText();
    	    Object parent = ((DelInstrText)o).getParent();
    	    instrText.setParent(parent);
    	    instrText.setSpace(((DelInstrText)o).getSpace());
    	    instrText.setValue(((DelInstrText)o).getValue());
    	    ((IContentAccessor)parent).getContent().set(index, instrText);
    	}
    }

    public static JSONObject forceRemoveChangeTrack(JSONObject attrs)
    	throws JSONException {

    	if(attrs==null) {
    		attrs = new JSONObject(1);
    	}
		JSONObject changes = attrs.optJSONObject(OCKey.CHANGES.value());
		if(changes == null) {
			changes = new JSONObject(3);
  			changes.put(OCKey.INSERTED.value(), JSONObject.NULL);
    		changes.put(OCKey.REMOVED.value(), JSONObject.NULL);
    		changes.put(OCKey.MODIFIED.value(), JSONObject.NULL);
			attrs.put(OCKey.CHANGES.value(), changes);
		}
		else {
			if(!changes.has(OCKey.INSERTED.value())) {
				changes.put(OCKey.INSERTED.value(), JSONObject.NULL);
			}
			if(!changes.has(OCKey.REMOVED.value())) {
        		changes.put(OCKey.REMOVED.value(), JSONObject.NULL);
			}
			if(!changes.has(OCKey.MODIFIED.value())) {
				changes.put(OCKey.MODIFIED.value(), JSONObject.NULL);
			}
		}
    	return attrs;
    }

    public static void applyTextRunAttributes(DocxOperationDocument operationDocument, JSONObject attrs, ITextRunState textRunState)
        throws JSONException {

        if(attrs==null) {
            return;
        }
        final R textRun = textRunState.getTextRun();
        final RPr rPr = textRun.getRPr(true);
        final Object styleId = attrs.opt(OCKey.STYLE_ID.value());
        if(styleId!=null) {
            if (styleId instanceof String) {
                final RStyle rStyle = rPr.getrStyle(true);
                rStyle.setVal((String)styleId);
                rPr.setrStyle(rStyle);
            }
            else {
                rPr.setrStyle(null);
            }
        }

        // applying character attributes and hyperlinks
        final JSONObject characterAttributes = attrs.optJSONObject(OCKey.CHARACTER.value());
        if(characterAttributes!=null) {
            Character.applyCharacterProperties(operationDocument, characterAttributes, rPr);

            final Object url = characterAttributes.opt(OCKey.URL.value());
            final Object anchor = characterAttributes.opt(OCKey.ANCHOR.value());
            if(url!=null||anchor!=null) {
                final P.Hyperlink hyperlink = textRunState.getHyperlink(true);
                if(url instanceof String) {
                    hyperlink.setId(Commons.setUrl(operationDocument.getContextPart(), hyperlink.getId(), (String)url));
                }
                else if(url==JSONObject.NULL) {
                    hyperlink.setId(null);
                }
                if(anchor instanceof String) {
                    hyperlink.setAnchor((String)anchor);
                }
                else if(anchor==JSONObject.NULL) {
                    hyperlink.setAnchor(null);
                }
                if(hyperlink.isEmpty()) {
                    textRunState.removeHyperlink();
                }
            }
        }

        // applying change tracking
        final Object changes = attrs.opt(OCKey.CHANGES.value());
        if(changes!=null) {
            final Object modified = changes==JSONObject.NULL ? JSONObject.NULL : ((JSONObject)changes).opt(OCKey.MODIFIED.value());
            final Object inserted = changes==JSONObject.NULL ? JSONObject.NULL : ((JSONObject)changes).opt(OCKey.INSERTED.value());
            final Object removed = changes==JSONObject.NULL ? JSONObject.NULL : ((JSONObject)changes).opt(OCKey.REMOVED.value());

            // applying modified attributes
            if(modified!=null) {
                if(modified==JSONObject.NULL) {
                    rPr.setRPrChange(null);
                }
                else {
                    CTRPrChange rPrChange = rPr.getRPrChange();
                    if(rPrChange==null) {
                        rPrChange = new CTRPrChange();
                        // TODO: check if parent is necessary for CTRPrChange objects
                        rPrChange.setParent(rPr);
                        rPr.setRPrChange(rPrChange);
                    }
                    org.docx4j.wml.CTRPrChange.RPr rPrC = rPrChange.getRPr();
                    if(rPrC==null) {
                        rPrC = new org.docx4j.wml.CTRPrChange.RPr();
                        rPrChange.setRPr(rPrC);
                    }
                    Utils.applyTrackInfoFromJSON(operationDocument, (JSONObject)modified, rPrChange);
                    final Object changedAttrs = ((JSONObject)modified).opt(OCKey.ATTRS.value());
                    if(changedAttrs!=null) {
                        if(changedAttrs instanceof JSONObject) {
                            final Object changedCharacterAttrs = ((JSONObject)changedAttrs).opt(OCKey.CHARACTER.value());
                            if(changedCharacterAttrs!=null) {
                                if(changedCharacterAttrs instanceof JSONObject) {
                                    Character.applyCharacterProperties(operationDocument, ((JSONObject)changedAttrs).optJSONObject(OCKey.CHARACTER.value()), rPrC);
                                }
                                else {
                                    rPrChange.setRPr(null);
                                }
                            }
                        }
                        else {
                            rPrChange.setRPr(null);
                        }
                    }
                }
            }

            // applying deletions
            if(removed!=null) {
                if(removed==JSONObject.NULL) {
                    final RDBase runDel = textRunState.getRunDel(false);
                    if(runDel!=null) {
                        // delNode is to be removed
                        delTextToText(0, runDel);
                        textRunState.removeRunDel();
                    }
                }
                else {
                    RDBase runDel = textRunState.getRunDel(false);
                    if(runDel==null) {
                        runDel = textRunState.getRunDel(true);

                        // changing text to delText
                        final DLList<Object> textRunContent = textRun.getContent();
                        for(int i = 0; i<textRunContent.size(); i++) {
                            final Object o = textRunContent.get(i);
                            if(o instanceof Text) {
                                final Text text = (Text)o;
                                final DelText delText = Context.getWmlObjectFactory().createDelText();
                                delText.setParent(text.getParent());
                                delText.setSpace(text.getSpace());
                                delText.setValue(text.getValue());
                                textRunContent.set(i, delText);
                            }
                            else if(o instanceof InstrText) {
                                 final InstrText instrText = (InstrText)o;
                                 final DelInstrText delInstrText = new DelInstrText();
                                 delInstrText.setParent(instrText.getParent());
                                 delInstrText.setSpace(instrText.getSpace());
                                 delInstrText.setValue(instrText.getValue());
                                 textRunContent.set(i, delInstrText);
                            }
                        }
                    }
                    Utils.applyTrackInfoFromJSON(operationDocument, (JSONObject)removed, runDel);
                }
            }

            // applying insertions
            if(inserted!=null) {
                if(inserted==JSONObject.NULL) {
                    final RIBase runIns = textRunState.getRunIns(false);
                    if(runIns!=null) {
                        // insertNode is to be removed
                        textRunState.removeRunIns();
                    }
                }
                else {
                    // insertNode is needed
                    Utils.applyTrackInfoFromJSON(operationDocument, (JSONObject)inserted, textRunState.getRunIns(true));
                }
            }
        }
    }
}
