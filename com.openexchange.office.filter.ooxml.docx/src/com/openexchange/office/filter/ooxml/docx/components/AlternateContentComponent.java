/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.ArrayList;
import java.util.List;
import org.docx4j.mce.AlternateContent;
import org.docx4j.mce.AlternateContent.Choice;
import org.docx4j.mce.AlternateContent.Fallback;
import org.docx4j.wml.Drawing;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.MultiComponent;
import com.openexchange.office.filter.ooxml.components.VMLBase;
import com.openexchange.office.filter.ooxml.components.VMLRootComponent;
import com.openexchange.office.filter.ooxml.docx.tools.DrawingML;

public class AlternateContentComponent extends ShapeComponent_Root {

	public AlternateContentComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        final List<OfficeOpenXMLComponent> shapes = new ArrayList<OfficeOpenXMLComponent>(2);
		final AlternateContent alternateContent = (AlternateContent)getObject();
		final List<Choice> choices = alternateContent.getChoice();
		for (int i=0; i<choices.size(); i++) {
			final Choice choice = choices.get(i);
			final String requires = choice.getRequires();
			if(requires.equals("wps")||requires.equals("wpg")) {
				final List<Object> any = choice.getAny();
				final Object o = any.get(0);
				if(o instanceof Drawing) {
					shapes.add(new ShapeComponent(getOperationDocument(), new DrawingML.DrawingML_root(getOperationDocument(), (Drawing)o), getComponentNumber()));
				}
			}
			else if (requires.equals("wpc")) {
			    final List<Object> any = choice.getAny();
			    final Object o = any.get(0);
			    if(o instanceof Drawing) {
			        shapes.add(new ShapeComponent(getOperationDocument(), new DrawingML.DrawingML_root(getOperationDocument(), (Drawing)o), getComponentNumber()));
			    }
			}
			else {
				// we don't support changes of this type, so we remove it...
				// changes will be applied to the fallback
				choices.remove(i--);
			}
		}
		final Fallback fallback = alternateContent.getFallback();
		if(fallback!=null) {
			final List<Object> any = fallback.getAny();
			if(any.size()==1) {
				final Object o = any.get(0);
				if(o instanceof IContentAccessor) {
				    final VMLBase vmlBase = (VMLBase) new VMLRootComponent("text", operationDocument, (IContentAccessor)o).getNextChildComponent(null, null);
				    if(vmlBase!=null) {
				        shapes.add(new ShapeComponent(getOperationDocument(), vmlBase, getComponentNumber()));
				    }
				}
			}
		}
		if(!shapes.isEmpty()) {
		    shapeType = ((ShapeComponent)shapes.get(0)).getType();
            shapeComponent = MultiComponent.createComponent(parentContext.getOperationDocument(), shapes);
		}
    }
}
