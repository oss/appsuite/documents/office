/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.AttributeSet;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.IShape;
import com.openexchange.office.filter.ooxml.components.IShapeProvider;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;

public class ShapeComponent extends DocxComponent implements IShapeProvider, IShapeType, IDrawingCharacterAttributes {

	private final IShape shape;

	public ShapeComponent(DocxOperationDocument operationDocument, IShape shape, int _componentNumber) {
		super(operationDocument, new DLNode<Object>(null), _componentNumber);
		this.shape = shape;
	}

	@Override
	public IShape getShape() {
	    return shape;
	}

	@Override
	public ShapeType getType() {
		return shape.getType();
	}
	@Override
	public OfficeOpenXMLComponent getNextComponent() {
	    final IShape next = shape.getNext();
	    return next!=null?new ShapeComponent(getOperationDocument(), next, getComponentNumber()+1):null;
	}
	@Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
	    final Object child = shape.getChild();
	    if(child instanceof IShape) {
	        return new ShapeComponent(getOperationDocument(), (IShape)child, 0);
	    }
	    else if(child instanceof OfficeOpenXMLComponent) {
	        return (OfficeOpenXMLComponent)child;
	    }
	    return null;
	}
	@Override
	public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {

	    final Object child = shape.insertChild(number, type);
	    IComponent<OfficeOpenXMLOperationDocument> childComponent = null;
	    if(child instanceof IShape) {
            childComponent = new ShapeComponent(getOperationDocument(), (IShape)child, number);
        }
        else if(child instanceof OfficeOpenXMLComponent) {
            childComponent = (OfficeOpenXMLComponent)child;
        }
	    if(attrs!=null) {
	        childComponent.applyAttrsFromJSON(attrs);
	    }
	    return childComponent;
	}
	@Override
	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

		shape.applyAttrsFromJSON(attrs);
	}

	/*
	 * returning shape attributes without character & paragraph attrs.
	 */
	@Override
	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

	    final JSONObject shapeAttrs = shape.createJSONAttrs(new JSONObject());
	    shapeAttrs.remove(OCKey.CHARACTER.value());
	    shapeAttrs.remove(OCKey.PARAGRAPH.value());
	    AttributeSet.mergeAttributes(attrs, shapeAttrs);
		return attrs;
	}

    @Override
    public JSONObject createDrawingCharacterAttributes() throws Exception {
        final JSONObject attrs = shape.createJSONAttrs(new JSONObject());
        return attrs.optJSONObject(OCKey.CHARACTER.value());
    }
}
