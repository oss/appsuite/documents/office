/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;

abstract public class ShapeComponent_Root extends TextRun_Base implements IShapeType, IDrawingCharacterAttributes {

    protected ShapeType shapeType;
    protected OfficeOpenXMLComponent   shapeComponent;

    protected ShapeComponent_Root(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _rootShapeNode, int _componentNumber) {
        super(parentContext, _rootShapeNode, _componentNumber);
        shapeType = ShapeType.UNDEFINED;
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
	    return shapeComponent!=null?shapeComponent.getNextChildComponent(previousChildContext, previousChildComponent):null;
	}

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {
	    return shapeComponent!=null?shapeComponent.insertChildComponent(number, attrs, type):null;
	}

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {
        if(shapeComponent!=null) {
            shapeComponent.applyAttrsFromJSON(attrs);
        }
        super.applyAttrsFromJSON(attrs);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

        attrs = super.createJSONAttrs(attrs);
        if(shapeComponent!=null) {
            attrs = shapeComponent.createJSONAttrs(attrs);
        }
        return attrs;
    }

    @Override
    public ShapeType getType() {
    	return shapeType;
    }

    @Override
    public JSONObject createDrawingCharacterAttributes() throws Exception {
        return shapeComponent instanceof IDrawingCharacterAttributes ? ((IDrawingCharacterAttributes)shapeComponent).createDrawingCharacterAttributes() : null;
    }
}
