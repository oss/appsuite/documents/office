/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.CTTblGridChange;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.CTTblPrBase.TblStyle;
import org.docx4j.wml.CTTblPrChange;
import org.docx4j.wml.P;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.R;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblGrid;
import org.docx4j.wml.TblGridBase;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.Tr;
import org.docx4j.wml.TrPr;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.ITable;
import com.openexchange.office.filter.ooxml.docx.tools.Table;
import com.openexchange.office.filter.ooxml.docx.tools.TextUtils;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;

public class TableComponent extends DocxComponent implements ITable {

    public TableComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }
    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> tblNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)tblNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, tblNode.getData());
            if(o instanceof Tr) {
                nextComponent = new TrComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }
	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {
		if(attrs==null) {
			return;
		}
		try {
	        final Tbl tbl = (Tbl)getObject();
	        final TblPr tblPr = tbl.getTblPr(true);
    		if(attrs.has(OCKey.STYLE_ID.value())) {
	            Table.applyTableStyle(attrs.getString(OCKey.STYLE_ID.value()), tblPr);
	        }
    		final TblGrid tblGrid = tbl.getTblGrid(true);
	        Table.applyTableProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, attrs.optJSONObject(OCKey.TABLE.value()), this, tblGrid, tblPr);
        	final Object changes = attrs.opt(OCKey.CHANGES.value());
        	if(changes!=null) {
        		boolean removeTableChangeTrack = false;
        		if(changes instanceof JSONObject) {
        			final Object modified = ((JSONObject)changes).opt(OCKey.MODIFIED.value());
        			if(modified!=null) {
        				if(modified instanceof JSONObject) {
        					CTTblPrChange tblPrChange = tblPr.getTblPrChange();
        					if(tblPrChange==null) {
        						tblPrChange = Context.getWmlObjectFactory().createCTTblPrChange();
        						tblPr.setTblPrChange(tblPrChange);
        					}
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)modified, tblPrChange);
        					final Object attrsModified = ((JSONObject)modified).opt(OCKey.ATTRS.value());
        					if(attrsModified!=null) {
        						if(attrsModified instanceof JSONObject) {
        						    final CTTblPrBase tblPrChangePr = tblPrChange.getTblPr(true);
        							if(((JSONObject)attrsModified).has(OCKey.STYLE_ID.value())) {
        								Table.applyTableStyle(((JSONObject) attrsModified).getString(OCKey.STYLE_ID.value()), tblPrChangePr);
        							}
        							final JSONObject tableAttrChanges = ((JSONObject)attrsModified).optJSONObject(OCKey.TABLE.value());
        							if(tableAttrChanges!=null) {
        								TblGridBase tblGridBase = null;
        								Object tblGridObj = tableAttrChanges.opt(OCKey.TABLE_GRID.value());
        								if(tblGridObj!=null) {
        									if(tblGridObj instanceof JSONArray) {
    									        tblGrid.getTblGridChange(true).getTblGrid(true);
        									}
        									else {
        										tblGrid.setTblGridChange(null);
        									}
        								}
        								Table.applyTableProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, tableAttrChanges, null, tblGridBase, tblPrChangePr);
        							}
        						}
        						else {
        							tblPrChange.setTblPr(null);
        						}
        					}
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsModified, tblPrChange);
        				}
        				else {
        					removeTableChangeTrack = true;
        				}
        			}
        			// removed / inserted changetracking has to be applied to each row
        			if(((JSONObject)changes).opt(OCKey.INSERTED.value())!=null||((JSONObject)changes).opt(OCKey.REMOVED.value())!=null) {
        			    IComponent<OfficeOpenXMLOperationDocument> trComponent = getNextChildComponent(null, null);
        				while(trComponent!=null) {
        					trComponent.applyAttrsFromJSON(attrs);
        					trComponent = trComponent.getNextComponent();
        				}
        			}
        		}
        		else {
        			removeTableChangeTrack = true;
        		}
        		if(removeTableChangeTrack) {
    				tblPr.setTblPrChange(null);
    				tblGrid.setTblGridChange(null);
        		}
        	}
		}
		catch(Exception e) {
		    //
		}
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
		throws JSONException {

		final Tbl tbl = (Tbl)getObject();
        final int maxTableColumns = operationDocument.getIntegerOfficeConfigurationValue("//module/maxTableColumns", 15);
        final int maxTableRows = operationDocument.getIntegerOfficeConfigurationValue("//module/maxTableRows", 1500);
        final int maxTableCells = operationDocument.getIntegerOfficeConfigurationValue("//module/maxTableCells", 1500);

        final TblGrid tableGrid = tbl.getTblGrid(false);
        JSONArray tableGridArray = Table.getTableGrid(tableGrid);
        int gridCount = tableGridArray!=null?tableGridArray.length() : 0;
        int columns = 0;
        int rows = 0;
        int rowWidth = Table.getRowWidth(this);

        IComponent<OfficeOpenXMLOperationDocument> rowComponent = getNextChildComponent(null, null);
        while(rowComponent instanceof TrComponent) {
            rows++;
            if (rows == 1) {
                columns = ((IContentAccessor)rowComponent.getNode().getData()).getContent().size();
            }
            rowComponent = rowComponent.getNextComponent();
        }

        if(gridCount>columns) {
            columns = gridCount;
        }
        boolean maxTableSizeExceeded = (columns>maxTableColumns)||(rows>maxTableRows)||((columns*rows)>maxTableCells);

        if(tableGridArray==null) {
            tableGridArray = new JSONArray(columns);
            for (int i=0; i<columns; i++) {
                tableGridArray.put(100);
            }
        }

        JSONObject tableChanges = null;
        final TblPr tblPr = tbl.getTblPr(false);
        if(tblPr!=null) {
            // setting the table style that is used...
            TblStyle tblStyle = tblPr.getTblStyle(false);
            if(tblStyle!=null) {
                String style = tblStyle.getVal();
                if(style!=null&&style.length()>0) {
                    attrs.put(OCKey.STYLE_ID.value(), style);
                }
            }
        }
        final boolean isRootTable = !(tbl.getParent() instanceof Tc);
        Table.createTableProperties(getOperationDocument().getPackage(), tbl.getTblPr(false), tableGridArray, isRootTable, rowWidth, attrs);
        final CTTblPrChange tblPrChange = tbl.getTblPr(false).getTblPrChange();
        if(tblPrChange!=null) {
        	tableChanges = new JSONObject(2);
        	final JSONObject tableModified = Utils.createJSONFromTrackInfo(getOperationDocument(), tblPrChange);
        	JSONArray tableGridChangeArray = null;
        	if(tableGrid!=null) {
        		final CTTblGridChange tblGridChange = tableGrid.getTblGridChange(false);
        		if(tblGridChange!=null) {
        			tableGridChangeArray = Table.getTableGrid(tblGridChange.getTblGrid(false));
        		}
        	}
        	final CTTblPrBase tblChangePr = tblPrChange.getTblPr(false);
        	if(tblChangePr!=null) {
            	final JSONObject tableChangedAttrs = new JSONObject();
            	final TblStyle tblStyle = tblChangePr.getTblStyle(false);
            	if(tblStyle!=null) {
            		String style = tblStyle.getVal();
            		if(style!=null&&!style.isEmpty()) {
            			tableChangedAttrs.put(OCKey.STYLE_ID.value(), style);
            		}
            	}
            	Table.createTableProperties(getOperationDocument().getPackage(), tblPrChange.getTblPr(false), tableGridChangeArray, isRootTable, rowWidth, tableChangedAttrs);
        		if(!tableChangedAttrs.isEmpty()) {
        			tableModified.put(OCKey.ATTRS.value(), tableChangedAttrs);
        		}
        	}
        	if(!tableModified.isEmpty()) {
        		tableChanges.put(OCKey.MODIFIED.value(), tableModified);
        	}
        	if(!tableChanges.isEmpty()) {
        		attrs.put(OCKey.CHANGES.value(), tableChanges);
        	}
        }
        JSONObject sizeExceeded = null;
        if(maxTableSizeExceeded) {
            sizeExceeded = new JSONObject(2);
            sizeExceeded.put(OCKey.ROWS.value(), rows);
            sizeExceeded.put(OCKey.COLUMNS.value(), columns);
            attrs.put(OCKey.SIZE_EXCEEDED.value(), sizeExceeded);
        }
		return attrs;
	}

	// ITable interfaces
	@Override
	public void insertRows(int destinationRowComponent, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

    	final TrComponent trReferenceRow = referenceRow!=-1 ? (TrComponent)getChildComponent(referenceRow) : null;
    	final Tbl tbl = (Tbl)getObject();
        final DLList<Object> tblContent = tbl.getContent();
        final IComponent<OfficeOpenXMLOperationDocument> oldTr = getChildComponent(destinationRowComponent);
        DLNode<Object> referenceNode = oldTr!=null?oldTr.getNode():null;

        for(int i = 0; i<count; i++) {
            final Tr tr = Context.getWmlObjectFactory().createTr();
            tr.setParent(tbl);
            final DLNode<Object> rowNode = new DLNode<Object>(tr);
            tblContent.addNode(referenceNode, rowNode, true);
            referenceNode = rowNode;
            if(insertDefaultCells) {
                JSONArray tableGrid = Table.getTableGrid(tbl.getTblGrid(false));
                if(tableGrid!=null) {
                    for(int j = 0; j<tableGrid.length(); j++) {
                        final Tc tc = Context.getWmlObjectFactory().createTc();
                        tc.setParent(tr);
                        tr.getContent().add(tc);
                        final P p = Context.getWmlObjectFactory().createP();
                        p.setParent(tc);
                        tc.getContent().addNode(new DLNode<Object>(p));
                    }
                }
            }
            else if(trReferenceRow!=null) {       // we try to create the new row from the referenceRow
                final Tr trReference = (Tr)trReferenceRow.getObject();
                final TrPr sourceTrPr = trReference.getTrPr(false);
                if(sourceTrPr!=null) {
                    final TrPr newTrPr = XmlUtils.deepCopy(sourceTrPr, operationDocument.getPackage());
                    TextUtils.resetMarkupId(newTrPr);
                    tr.setTrPr(newTrPr);
                }
                TcComponent tcReferenceComponent = (TcComponent)trReferenceRow.getNextChildComponent(null, null);
                while(tcReferenceComponent!=null) {
                    final Tc tcReference = (Tc)tcReferenceComponent.getObject();
                    final Tc newTc = Context.getWmlObjectFactory().createTc();
                    newTc.setParent(tr);
                    tr.getContent().add(newTc);
                    final TcPr tcPrReference = tcReference.getTcPr(false);
                    if(tcPrReference!=null) {
                        final TcPr newTcPr = XmlUtils.deepCopy(tcPrReference, operationDocument.getPackage());
                        TextUtils.resetMarkupId(newTcPr);
                        newTc.setTcPr(newTcPr);
                    }
                    final P p = Context.getWmlObjectFactory().createP();
                    p.setParent(newTc);
                    newTc.getContent().addNode(new DLNode<Object>(p));
                    applyReferenceCellAttributes(p, tcReferenceComponent);
                    tcReferenceComponent = (TcComponent)tcReferenceComponent.getNextComponent();
                }
            }
        }
        if(attrs!=null) {
            IComponent<OfficeOpenXMLOperationDocument> c = getNextChildComponent(null, null).getComponent(destinationRowComponent);
	        for(int i=0; i<count; i++) {
	        	c.applyAttrsFromJSON(attrs);
	        	c = c.getNextComponent();
	        }
        }
	}

	private void applyReferenceCellAttributes(P p, TcComponent referenceCell) {
	    if(referenceCell!=null) {
    	    final ParagraphComponent referenceParagraphComponent = referenceCell.getFirstParagraphChildComponent();
    	    if(referenceParagraphComponent!=null) {
    	        final P referenceP = (P)referenceParagraphComponent.getObject();
                if(referenceP.getPPr(false)!=null) {
                    p.setPPr(XmlUtils.deepCopy(referenceP.getPPr(false), getOperationDocument().getPackage()));
                }
    	        final OfficeOpenXMLComponent c = (OfficeOpenXMLComponent)referenceParagraphComponent.getNextChildComponent(null, null);
    	        if(c instanceof TextRun_Base) {
                    final R r = ((TextRun_Base)c).getTextRun();
                    if(r.getRPr(false)!=null) {
                        p.getPPr(true).setRPr(ParaRPr.cloneFromRPrBase(r.getRPr(false)));
                    }
    	        }
    	    }
	    }
	}

	@Override
	public void splitTable(int rowPosition) {
    	final Tbl sourceTbl = (Tbl)getObject();
    	// to clone the table we temporarily remove the content
    	final DLList<Object> sourceTblContent = sourceTbl.getContent();
    	sourceTbl.setContent(null);
    	final Tbl destTbl = XmlUtils.deepCopy(sourceTbl, getOperationDocument().getPackage());
    	TextUtils.resetMarkupId(destTbl);
    	sourceTbl.setContent(sourceTblContent);
    	final IContentAccessor sourceTblParent = (IContentAccessor)getParentComponent().getNode().getData();
    	destTbl.setParent(sourceTblParent);
    	((DLList<Object>)sourceTblParent.getContent()).addNode(getNode(), new DLNode<Object>(destTbl), false);
    	sourceTblContent.moveNodes(sourceTblContent.getNode(rowPosition), -1, destTbl.getContent(), null, true, destTbl);
	}

	@Override
	public void mergeTable() {
    	final TableComponent sourceTable = (TableComponent)getNextComponent();
    	final DLList<Object> sourceContent = ((Tbl)sourceTable.getObject()).getContent();
    	final DLList<Object> destContent = ((Tbl)getObject()).getContent();
    	sourceContent.moveNodes(destContent, getObject());
    	((DLList<Object>)((IContentAccessor)((Child)sourceTable.getObject()).getParent()).getContent()).removeNode(sourceTable.getNode());
	}

	@Override
	public void insertColumn(JSONArray tableGrid, int gridPosition, String insertMode)
		throws JSONException {

		boolean before = insertMode.equals("before");
        TrComponent trComponent = (TrComponent)getNextChildComponent(null, null);
        while(trComponent!=null) {
            TcComponent tcReference = (TcComponent)trComponent.getNextChildComponent(null, null);
            TcComponent tcReferenceLast = null;
            TcComponent destination = null;
            while(tcReference!=null) {
                tcReferenceLast = tcReference;
                if(gridPosition>=tcReference.getGridPosition()&&gridPosition<tcReference.getNextGridPosition()) {
                    destination = tcReference;
                    break;
                }
                tcReference = (TcComponent)tcReference.getNextComponent();
            }
            final Tc tc = Context.getWmlObjectFactory().createTc();
            if(destination!=null) {
                final TcPr referenceTcPr = ((Tc)destination.getObject()).getTcPr(false);
                tc.setParent(trComponent.getObject());
                if(referenceTcPr!=null) {
                    TcPr newTcPr = XmlUtils.deepCopy(referenceTcPr, operationDocument.getPackage());
                    newTcPr.setGridSpan(null);
                    newTcPr.setTcW(new TblWidth(TblWidth.TYPE_DXA, 0));
                    TextUtils.resetMarkupId(newTcPr);
                    tc.setTcPr(newTcPr);
                }
            }
            final P p = Context.getWmlObjectFactory().createP();
            p.setParent(tc);
            tc.getContent().addNode(new DLNode<Object>(p));

            applyReferenceCellAttributes(p, destination!=null ? destination : tcReferenceLast);

            final DLList<Object> rowContent = ((Tr)trComponent.getObject()).getContent();
            if(destination==null) {
            	rowContent.add(tc);
            }
            else {
            	// context child can be a SdtRowContext or TcComponent
            	final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = destination.getContextChild(null);
            	rowContent.addNode(contextChild.getNode(), new DLNode<Object>(tc), before);
            }
            trComponent = (TrComponent)trComponent.getNextComponent();
        }
        Table.setTableGrid((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, this, ((Tbl)getObject()).getTblGrid(true), tableGrid);
	}

	@Override
    public void deleteColumns(int gridStart, int gridEnd)
    	throws JSONException {

	    IComponent<OfficeOpenXMLOperationDocument> trComponent = getNextChildComponent(null, null);
        while(trComponent!=null) {
        	DLNode<Object> startNode = null;
        	DLNode<Object> endNode = null;
            TcComponent tcComponent = (TcComponent)trComponent.getNextChildComponent(null, null);
            while(tcComponent!=null) {
            	final int x1 = tcComponent.getGridPosition();
            	final int x2 = tcComponent.getNextGridPosition();
            	if(Math.max(x2, gridEnd + 1) - Math.min(x1, gridStart) < (x2 - x1) + ((gridEnd+1)-gridStart)) {
            		final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = tcComponent.getContextChild(null);
                	if (startNode==null) {
                        startNode = contextChild.getNode();
                	}
                    endNode = contextChild.getNode();
                }
                if(tcComponent.getNextGridPosition()>gridEnd)
                    break;
                tcComponent = (TcComponent)tcComponent.getNextComponent();
            }
            if(startNode!=null) {
                ((Tr)trComponent.getObject()).getContent().removeNodes(startNode, endNode);
            }
            trComponent = trComponent.getNextComponent();
        }
        JSONArray tableGrid = Table.getTableGrid(((Tbl)getObject()).getTblGrid(false));
        JSONArray newTableGrid = new JSONArray();
        for(int i=0;i<tableGrid.length();i++) {
            if(i<gridStart||i>gridEnd) {
                newTableGrid.put(tableGrid.get(i));
            }
        }
        Table.setTableGrid((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, this, ((Tbl)getObject()).getTblGrid(true), newTableGrid);
	}
}
