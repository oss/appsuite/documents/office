/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.wml.CTTcPrChange;
import org.docx4j.wml.SdtBlock;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.TcPrInner;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.tools.Table;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;

public class TcComponent extends RootComponent {

    private final int gridPosition;

    public TcComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber, int _gridPosition) {
        super(parentContext, _node, _componentNumber);
        gridPosition = _gridPosition;
    }

    public int getGridPosition() {
        return gridPosition;
    }

    public int getNextGridPosition() {
        int gridSpan = 1;
        TcPr tcPr = ((Tc)getNode().getData()).getTcPr(false);
        if(tcPr!=null) {
            TcPrInner.GridSpan span = tcPr.getGridSpan();
            if (span!=null) {
                gridSpan = span.getVal().intValue();
                if (gridSpan==0) {
                    System.out.println("Error: empty gridSpan is zero... this results in problems with the component iterator");
                    gridSpan++; // increasing.. preventing an endless loop
                }
            }
        }
        return gridPosition + gridSpan;
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> cellNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)cellNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, cellNode.getData());
            if(ParagraphComponent.isRepresentableParagraph(o)) {
                nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Tbl) {
                nextComponent = new TableComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SdtBlock) {
                final SdtRootContext sdtRootContext = new SdtRootContext(this, childNode);
                nextComponent = sdtRootContext.getNextChildComponent(null, previousChildComponent);
            }
        }
        return nextComponent;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {
		if(attrs==null) {
			return;
		}
		try {
	        final Tc tc = (Tc)getObject();
	        final TcPr tcPr = tc.getTcPr(true);
	        Table.applyCellProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, attrs.optJSONObject(OCKey.CELL.value()), tcPr);
	        final Object changes = attrs.opt(OCKey.CHANGES.value());
	        if(changes!=null) {
		        if(changes instanceof JSONObject) {
		        	final Object attrsInserted = ((JSONObject)changes).opt(OCKey.INSERTED.value());
		        	if(attrsInserted!=null) {
		        		if(attrsInserted instanceof JSONObject) {
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsInserted, tcPr.getCellIns(true));
		        		}
			        	else {
			        		tcPr.setCellIns(null);
			        	}
		        	}
		        	final Object attrsRemoved = ((JSONObject)changes).opt(OCKey.REMOVED.value());
		        	if(attrsRemoved!=null) {
	            		if(attrsRemoved instanceof JSONObject) {
	            			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsRemoved, tcPr.getCellDel(true));
	            		}
	            		else {
	            			tcPr.setCellDel(null);
	            		}
		        	}
		        	final Object attrsModified = ((JSONObject)changes).opt(OCKey.MODIFIED.value());
		        	if(attrsModified!=null) {
		        		if(attrsModified instanceof JSONObject) {
		        		    final CTTcPrChange tcPrChange = tcPr.getTcPrChange(true);
	            			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsModified, tcPrChange);
        					final Object attrsModifiedAttrs = ((JSONObject)attrsModified).opt(OCKey.ATTRS.value());
        					if(attrsModifiedAttrs!=null) {
        						if(attrsModifiedAttrs instanceof JSONObject) {
        					        Table.applyCellProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, ((JSONObject)attrsModifiedAttrs).optJSONObject(OCKey.CELL.value()), tcPrChange.getTcPr(true));
        						}
        						else {
        							tcPrChange.setTcPr(null);
        						}
        					}
		        		}
		        		else {
		        			tcPr.setTcPrChange(null);
		        		}
		        	}
		        }
		        else {
		        	tcPr.setCellDel(null);
		        	tcPr.setCellIns(null);
		        	tcPr.setCellMerge(null);
		        }
	        }
		}
		catch(Exception e) {
		    //
		}
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
		throws JSONException {

        final Tc cell = (Tc)getObject();
        final TrComponent trComponent = (TrComponent)getParentComponent();
        final TableComponent tblComponent = (TableComponent)trComponent.getParentComponent();
        final Tbl tbl = (Tbl)tblComponent.getObject();
        final TcPr tcPr = cell.getTcPr(false);
        if(tcPr!=null) {
        	Table.createCellProperties(tcPr, tbl, attrs);
            if(tcPr.getCellDel(false)!=null||tcPr.getCellIns(false)!=null||tcPr.getTcPrChange(false)!=null||tcPr.getCellMerge(false)!=null) {
                final JSONObject jsonCellChanges = new JSONObject(2);
                if(tcPr.getCellDel(false)!=null) {
                	jsonCellChanges.put(OCKey.REMOVED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), tcPr.getCellDel(false)));
                }
                if(tcPr.getCellIns(false)!=null) {
                	jsonCellChanges.put(OCKey.INSERTED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), tcPr.getCellIns(false)));
                }
                if(tcPr.getTcPrChange(false)!=null) {
                	final JSONObject jsonCellModified = Utils.createJSONFromTrackInfo(getOperationDocument(), tcPr.getTcPrChange(false));
                	final JSONObject jsonCellAttrChanges = new JSONObject();
                	final TcPrInner tcPrInner = tcPr.getTcPrChange(false).getTcPr(false);
                	if(tcPrInner!=null) {
                    	Table.createCellProperties(tcPrInner, tbl, jsonCellAttrChanges);
                	}
                	if(!jsonCellAttrChanges.isEmpty()) {
                		jsonCellModified.put(OCKey.ATTRS.value(), jsonCellAttrChanges);
                	}
                	if(!jsonCellModified.isEmpty()) {
                		jsonCellChanges.put(OCKey.MODIFIED.value(), jsonCellModified);
                	}
                }
                if(tcPr.getCellMerge(false)!=null) {
                	jsonCellChanges.put(OCKey.MERGED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), tcPr.getCellMerge(false)));
                }
                if(!jsonCellChanges.isEmpty()) {
                	attrs.put(OCKey.CHANGES.value(), jsonCellChanges);
                }
            }
        }
		return attrs;
	}

	public ParagraphComponent getFirstParagraphChildComponent() {
	    IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
	    while(childComponent != null) {
	        if(childComponent instanceof ParagraphComponent) {
	            return (ParagraphComponent)childComponent;
	        }
	        childComponent = childComponent.getNextComponent();
	    }
	    return null;
	}
}
