/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.Iterator;
import java.util.List;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTTabStop;
import org.docx4j.wml.Color;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.PPrBase;
import org.docx4j.wml.PPrBase.Ind;
import org.docx4j.wml.PPrBase.OutlineLvl;
import org.docx4j.wml.PPrBase.PBdr;
import org.docx4j.wml.STBorder;
import org.docx4j.wml.STLineSpacingRule;
import org.docx4j.wml.STTabJc;
import org.docx4j.wml.STTabTlc;
import org.docx4j.wml.Tabs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class Paragraph {

	final private static Logger log = LoggerFactory.getLogger(Paragraph.class);

    public static void applyParagraphProperties(DocxOperationDocument operationDocument, JSONObject paragraphProperties, PPrBase pPr)
        throws FilterException, JSONException {

        if(paragraphProperties==null)
            return;

        final ObjectFactory objectFactory = Context.getWmlObjectFactory();
        final Iterator<String> keys = paragraphProperties.keys();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = paragraphProperties.get(attr);
            if(attr.equals(OCKey.LINE_HEIGHT.value())) {
                if(value instanceof JSONObject) {
                    Iterator<String> lineheightkeys = ((JSONObject)value).keys();
                    String lineheighttype = null;
                    int lineheightvalue = -1;
                    while(lineheightkeys.hasNext()) {
                        String lineheightattr = lineheightkeys.next();
                        if(lineheightattr.equals(OCKey.TYPE.value()))
                            lineheighttype = ((JSONObject)value).get(lineheightattr).toString();
                        else if (lineheightattr.equals(OCKey.VALUE.value()))
                            lineheightvalue = ((Integer)((JSONObject)value).get(lineheightattr)).intValue();
                    }
                    if(lineheighttype!=null&&lineheightvalue>-1) {
                        STLineSpacingRule lineSpacingRule = null;
                        Long lineSpacingValue = null;
                        if(lineheighttype.equals("fixed")) {
                            lineSpacingRule = STLineSpacingRule.fromValue("exact");
                            lineSpacingValue = Long.valueOf(((long)(lineheightvalue) * 144 ) / 254);
                        } else if (lineheighttype.equals("leading")) {
                            lineSpacingRule = STLineSpacingRule.fromValue("percent");
                            lineSpacingValue = Long.valueOf(((long)(100) * 24 ) / 10);
                        } else if (lineheighttype.equals("atLeast")) {
                            lineSpacingRule = STLineSpacingRule.fromValue("atLeast");
                            lineSpacingValue = Long.valueOf(((long)(lineheightvalue) * 144 ) / 254);
                        } else if (lineheighttype.equals("percent")) {
                            lineSpacingRule = STLineSpacingRule.fromValue("auto");
                            lineSpacingValue = Long.valueOf(((long)(lineheightvalue) * 24 ) / 10);
                        }
                        if(lineSpacingRule!=null&&lineSpacingValue!=null) {
                            PPrBase.Spacing spacing = pPr.getSpacing();
                            if(spacing==null) {
                                spacing = objectFactory.createPPrBaseSpacing();
                                pPr.setSpacing(spacing);
                            }
                            spacing.setLineRule(lineSpacingRule);
                            spacing.setLine(lineSpacingValue);
                        }
                    }
                }
                else
                    pPr.setSpacing(null);
            }
            else if(attr.equals(OCKey.MARGIN_TOP.value())) {
                PPrBase.Spacing spacing = pPr.getSpacing();
                if(value instanceof Number) {
                    if(spacing==null) {
                        spacing = objectFactory.createPPrBaseSpacing();
                        pPr.setSpacing(spacing);
                    }
                    spacing.setBefore(Utils.map100THMMToTwip(((Number)value).longValue()));
                }
                else if(spacing!=null)
                    spacing.setBefore(null);
            }
            else if(attr.equals(OCKey.MARGIN_BOTTOM.value())) {
                PPrBase.Spacing spacing = pPr.getSpacing();
                if(value instanceof Integer) {
                    if(spacing==null) {
                        spacing = objectFactory.createPPrBaseSpacing();
                        pPr.setSpacing(spacing);
                    }
                    spacing.setAfter(Utils.map100THMMToTwip((Integer)value));
                }
                else if(spacing!=null)
                    spacing.setAfter(null);
            }
            else if(attr.equals(OCKey.ALIGNMENT.value())) {
                if (value instanceof String ) {
                    Jc jc = pPr.getJc();
                    if(jc==null) {
                        jc = objectFactory.createJc();
                        pPr.setJc(jc);
                    }
                    if(((String)value).equals("left"))
                        jc.setVal(JcEnumeration.LEFT);
                    else if(((String)value).equals("center"))
                        jc.setVal(JcEnumeration.CENTER);
                    else if(((String)value).equals("right"))
                        jc.setVal(JcEnumeration.RIGHT);
                    if(((String)value).equals("justify"))
                        jc.setVal(JcEnumeration.BOTH);
                }
                else
                    pPr.setJc(null);
            }
            else if(attr.equals(OCKey.FILL_COLOR.value())) {
                if(value instanceof JSONObject) {
                    JSONObject shdObject = (JSONObject)value;
                    Utils.initShdFromJSONColor(operationDocument, shdObject, pPr.getShd(true));
                }
                else
                    pPr.setShd(null);
            }
            else if (attr.equals(OCKey.OUTLINE_LEVEL.value())) {
                if (value instanceof Number) {
                    final Long outlineLvl = ((Number)value).longValue();
                    OutlineLvl level = objectFactory.createPPrBaseOutlineLvl();
                    level.setVal(outlineLvl);
                    pPr.setOutlineLvl(level);
                }
                else
                    pPr.setOutlineLvl(null);
            }
            else if(attr.equals(OCKey.LIST_STYLE_ID.value())) {
                if(value instanceof String) {
                    String sValue = (String)value;
                    if(sValue.length() == 0){
                        //empty string is just the default state!
                        continue;
                    }

                    final PPrBase.NumPr numPr = pPr.getNumPr(true);
                    long numIdValue;
                    try {
                        if(!java.lang.Character.isDigit(sValue.charAt(0))){
                            numIdValue = Long.parseLong(sValue.substring(1));
                        } else {
                            numIdValue = Long.parseLong(sValue);
                        }
                    }
                    catch(NumberFormatException e) {
                        numIdValue = 0;
                        log.debug("docx export applyProperties: invalid listStyleId found!");
                    }
                    if(numIdValue >= 0) {
                        PPrBase.NumPr.NumId numId = new PPrBase.NumPr.NumId();
                        numId.setVal(Long.valueOf(numIdValue));
                        numPr.setNumId(numId);
                    } else {
                        pPr.setNumPr( null );
                    }
                }
                else {
                    pPr.setNumPr(null);
                }
            }
            else if(attr.equals(OCKey.LIST_LEVEL.value())) {
                if(value instanceof Number) {
                    final Long intValue = ((Number)value).longValue();
                    if(intValue<0)
                        pPr.setNumPr(null);
                    else {
                        final PPrBase.NumPr numPr = pPr.getNumPr(true);
                        PPrBase.NumPr.Ilvl ilvl = new PPrBase.NumPr.Ilvl();
                        ilvl.setVal(intValue);
                        numPr.setIlvl(ilvl);
                    }
                }
                else
                    pPr.setNumPr(null);
            }
            else if(attr.equals(OCKey.BORDER_LEFT.value()) || attr.equals(OCKey.BORDER_RIGHT.value()) ||
                    attr.equals(OCKey.BORDER_TOP.value()) || attr.equals(OCKey.BORDER_BOTTOM.value()) ||
                    attr.equals(OCKey.BORDER_INSIDE.value())) {

                boolean left = attr.equals(OCKey.BORDER_LEFT.value());
                boolean right = attr.equals(OCKey.BORDER_RIGHT.value());
                boolean top = attr.equals(OCKey.BORDER_TOP.value());
                boolean bottom = attr.equals(OCKey.BORDER_BOTTOM.value());

                if(value instanceof JSONObject) {
                    PBdr pBorder = pPr.getPBdr();
                    if(pBorder==null) {
                        pBorder = objectFactory.createPPrBasePBdr();
                        pPr.setPBdr(pBorder);
                    }
                    CTBorder border = left ? pBorder.getLeft() : right ? pBorder.getRight() : top ? pBorder.getTop() : bottom ? pBorder.getBottom() : pBorder.getBetween();

                    JSONObject borderObject = (JSONObject)value;
                    if( !borderObject.has(OCKey.STYLE.value()) ) {
                        border = null;
                    } else {
                        if( border == null ) {
                            border = objectFactory.createCTBorder();
                        }
                        String style = borderObject.getString(OCKey.STYLE.value());
                        border.setVal(STBorder.fromValue( style ));
                        if(borderObject.has(OCKey.COLOR.value()))
                            Utils.initBorderColorFromJSONColor(operationDocument, borderObject.optJSONObject(OCKey.COLOR.value()), border);
                        border.setFrame(false);
                        border.setShadow(false);
                        if(borderObject.has(OCKey.SPACE.value()))
                            border.setSpace(Utils.map100THMMToTwip((double)(borderObject.getLong(OCKey.SPACE.value()) +10 ) / 20 ));
                        if(borderObject.has(OCKey.WIDTH.value()))
                            border.setSz(Utils.map100THMMToTwip((double)(borderObject.getLong(OCKey.WIDTH.value()) + 2 )* 2 / 5 ));
                    }
                    if(left)
                        pBorder.setLeft(border);
                    else if(right)
                        pBorder.setRight(border);
                    else if(top)
                        pBorder.setTop(border);
                    else if(bottom)
                        pBorder.setBottom(border);
                    else
                        pBorder.setBetween(border);
                }
                else {
                    PBdr pBorder = pPr.getPBdr();
                    if(pBorder != null){
                        if(left)
                            pBorder.setLeft(null);
                        else if(right)
                            pBorder.setRight(null);
                        else if(top)
                            pBorder.setTop(null);
                        else if(bottom)
                            pBorder.setBottom(null);
                        else
                            pBorder.setBetween(null);
                        if(pBorder.getLeft() == null && pBorder.getRight() == null && pBorder.getTop() == null &&
                            pBorder.getBottom() == null && pBorder.getBetween() == null)
                                pPr.setPBdr(null);
                    }

                }
            } else if ((attr.equals(OCKey.INDENT_LEFT.value())) || (attr.equals(OCKey.INDENT_FIRST_LINE.value())) ||
                        (attr.equals(OCKey.INDENT_RIGHT.value()))) {

                boolean left = attr.equals(OCKey.INDENT_LEFT.value());
                boolean right = attr.equals(OCKey.INDENT_RIGHT.value());
                boolean firstLine = attr.equals(OCKey.INDENT_FIRST_LINE.value());

                if(value instanceof Integer) {
                    Ind indent = pPr.getInd();
                    if (indent == null) {
                        indent = objectFactory.createPPrBaseInd();
                        pPr.setInd(indent);
                    }
                    Long longValue = ((Integer)value).longValue();
                    if (left) {
                        indent.setLeft(Utils.map100THMMToTwip(longValue));
                    }
                    else if (right) {
                        indent.setRight(Utils.map100THMMToTwip(longValue));
                    }
                    else if (firstLine) {
                        indent.setFirstLine(Utils.map100THMMToTwip(longValue));
                    }
                }
            }
            else if(attr.equals(OCKey.CONTEXTUAL_SPACING.value())){
                if(value instanceof Boolean) {
                    BooleanDefaultTrue contextSpacing = pPr.getContextualSpacing();
                    if(contextSpacing == null){
                        contextSpacing = objectFactory.createBooleanDefaultTrue();
                        pPr.setContextualSpacing(contextSpacing);
                    }
                    contextSpacing.setVal(((Boolean)value).booleanValue());
                }else{
                	pPr.setContextualSpacing(null);
                }
            }
            else if(attr.equals(OCKey.PAGE_BREAK_BEFORE.value())) {
            	if(value instanceof Boolean) {
            		BooleanDefaultTrue pageBreakBefore = pPr.getContextualSpacing();
            		if(pageBreakBefore==null) {
            			pageBreakBefore = objectFactory.createBooleanDefaultTrue();
            			pPr.setPageBreakBefore(pageBreakBefore);
            		}
            		pageBreakBefore.setVal(((Boolean)value).booleanValue());
            	}
            	else {
            		pPr.setPageBreakBefore(null);
            	}
            }
            else if(attr.equals(OCKey.TAB_STOPS.value())) {
                if(value instanceof JSONArray) {
                    JSONArray newTabs = (JSONArray) value;
                    Tabs tabs = pPr.getTabs();
                    if (tabs == null) {
                        tabs = new Tabs();
                        pPr.setTabs(tabs);
                    }
                    List<CTTabStop> tabStops = tabs.getTab();
                    tabStops.clear();

                    for (int i = 0; i < newTabs.length(); i++)
                    {
                        JSONObject newTab = newTabs.getJSONObject(i);
                        CTTabStop tab = new CTTabStop();

                        String fillChar = newTab.optString(OCKey.FILL_CHAR.value(), null);
                        if(fillChar != null){
                            tab.setLeader(STTabTlc.fromValue(fillChar));
                        }
                        long pos = newTab.getLong(OCKey.POS.value());
                        tab.setPos(Utils.map100THMMToTwip(pos));

                        String val = newTab.getString(OCKey.VALUE.value());
                        tab.setVal(STTabJc.fromValue(val));

                        tabStops.add(tab);
                    }
                }
            }
        }
    }

    public static JSONObject createParagraphProperties(PPrBase paragraphProperties)
        throws JSONException {

        JSONObject jsonParagraphProperties = null;
        if(paragraphProperties!=null) {
            jsonParagraphProperties = new JSONObject();

            final BooleanDefaultTrue pageBreakBefore = paragraphProperties.getPageBreakBefore();
            if(pageBreakBefore!=null) {
            	jsonParagraphProperties.put(OCKey.PAGE_BREAK_BEFORE.value(), pageBreakBefore.isVal());
            }
            PPrBase.Spacing spacing = paragraphProperties.getSpacing();
            if(spacing!=null) {
                STLineSpacingRule lineSpacingRule = spacing.getLineRule();
                Long lineSpacingValue = spacing.getLine();                    // value is always in twip
                if (lineSpacingRule!=null&&lineSpacingValue!=null) {
                    String type = null;
                    int value = -1;
                    if(lineSpacingRule==STLineSpacingRule.AT_LEAST) {
                        type = "atLeast";
                        value = (int)(lineSpacingValue.doubleValue() * 254 / 144);  // twip to 1/100mm
                    } else if (lineSpacingRule==STLineSpacingRule.AUTO) {
                        type = "percent";
                        value = (int)(lineSpacingValue.doubleValue() / 2.4);
                    } else if (lineSpacingRule==STLineSpacingRule.EXACT) {
                        type = "fixed";
                        value = (int)(lineSpacingValue.doubleValue() * 254 /144);   // twip to 1/100mm
                    }
                    if(type!=null&&value>-1) {
                        JSONObject jsonObject = new JSONObject(2);
                        jsonObject.put(OCKey.TYPE.value(), type);
                        jsonObject.put(OCKey.VALUE.value(), value);
                        jsonParagraphProperties.put(OCKey.LINE_HEIGHT.value(), jsonObject);
                    }
                }
                Long before = spacing.getBefore();
                if(before != null)
                    jsonParagraphProperties.put(OCKey.MARGIN_TOP.value(), Utils.mapTwipTo100THMM(before));
                Long after = spacing.getAfter();
                if(after != null)
                    jsonParagraphProperties.put(OCKey.MARGIN_BOTTOM.value(), Utils.mapTwipTo100THMM(after));
            }

            BooleanDefaultTrue contextSpacing = paragraphProperties.getContextualSpacing();
            if(contextSpacing != null)
                jsonParagraphProperties.put(OCKey.CONTEXTUAL_SPACING.value(), contextSpacing.isVal());

            Jc jc = paragraphProperties.getJc();
            if(jc!=null) {
                if (jc.getVal()==JcEnumeration.LEFT)
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "left");
                else if (jc.getVal()==JcEnumeration.CENTER)
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "center");
                else if (jc.getVal()==JcEnumeration.RIGHT)
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "right");
                else if (jc.getVal()==JcEnumeration.DISTRIBUTE)
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "justify");
                else if (jc.getVal()==JcEnumeration.BOTH)
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "justify");
                else    // we do not support quasimodo alignments->defaulting to left
                    jsonParagraphProperties.put(OCKey.ALIGNMENT.value(), "left");
            }
            final CTShd shd = paragraphProperties.getShd(false);
            if(shd!=null){
                Commons.jsonPut(
                    jsonParagraphProperties,
                    OCKey.FILL_COLOR.value(),
                    Utils.createFillColor(shd));
            }
            final PPrBase.NumPr numPr = paragraphProperties.getNumPr(false);
            if(numPr!=null){
                Long ilvl = numPr.getIlvl() != null ? numPr.getIlvl().getVal() : null;
                final PPrBase.NumPr.NumId numId = numPr.getNumId();
                if(numId!=null) {
                    jsonParagraphProperties.put(OCKey.LIST_STYLE_ID.value(), 'L' + Integer.toString(numId.getVal().intValue()));
                    if ((ilvl==null) && (numId.getVal().intValue()!=0)) { // fix for docs-3388, setting default iLvl of 0 if numIdId is given
                        ilvl = Long.valueOf(0);
                    }
                }
                if(ilvl!=null) {
                    jsonParagraphProperties.put(OCKey.LIST_LEVEL.value(), ilvl.intValue());
                }
            }
            OutlineLvl outlineLvl = paragraphProperties.getOutlineLvl();
            if (outlineLvl != null){
                jsonParagraphProperties.put(OCKey.OUTLINE_LEVEL.value(), outlineLvl.getVal().intValue() );
            }
            Tabs tabs = paragraphProperties.getTabs();
            if (tabs != null){
                List<CTTabStop> tabList = tabs.getTab();
                if ((tabList != null) && !tabList.isEmpty()){
                    JSONArray jsonTabstopArray = new JSONArray();
                    Iterator<CTTabStop> iter = tabList.iterator();
                    while (iter.hasNext()) {
                        CTTabStop tabstop = iter.next();
                        JSONObject jsonTabstop = new JSONObject();

                        Long posValue = tabstop.getPos();
                        jsonTabstop.put(OCKey.VALUE.value(), tabstop.getVal().value());
                        jsonTabstop.put(OCKey.POS.value(), Utils.mapTwipTo100THMM(posValue));
                        if (tabstop.getLeader() != null) {
                            jsonTabstop.put(OCKey.FILL_CHAR.value(), tabstop.getLeader().value());
                        }
                        jsonTabstopArray.put(jsonTabstop);
                    }
                    jsonParagraphProperties.put(OCKey.TAB_STOPS.value(), jsonTabstopArray);
                }
            }
            PBdr pBorder = paragraphProperties.getPBdr();
            if (pBorder != null){
                CTBorder lBorder = pBorder.getLeft();
                if( lBorder != null )
                    jsonParagraphProperties.put(OCKey.BORDER_LEFT.value(), createBorderProperties( lBorder ));
                CTBorder rBorder = pBorder.getRight();
                if( rBorder != null )
                    jsonParagraphProperties.put(OCKey.BORDER_RIGHT.value(), createBorderProperties( rBorder ));
                CTBorder tBorder = pBorder.getTop();
                if( tBorder != null )
                    jsonParagraphProperties.put(OCKey.BORDER_TOP.value(), createBorderProperties( tBorder ));
                CTBorder bBorder = pBorder.getBottom();
                if( bBorder != null )
                    jsonParagraphProperties.put(OCKey.BORDER_BOTTOM.value(), createBorderProperties( bBorder ));
                CTBorder iBorder = pBorder.getBetween();
                if( iBorder != null )
                    jsonParagraphProperties.put(OCKey.BORDER_INSIDE.value(), createBorderProperties( iBorder ));
// not used
//                    CTBorder barBorder = pBorder.getBar();
//                    if( barBorder != null )
//                        jsonParagraphProperties.put("borderbar", createBorderProperties( barBorder ));
            }
            Ind indent = paragraphProperties.getInd();
            if (indent != null){
                Long hanging = indent.getHanging();
                if( hanging != null )
                    jsonParagraphProperties.put(OCKey.INDENT_FIRST_LINE.value(), - Utils.mapTwipTo100THMM(hanging));
                else{
                    Long firstLine = indent.getFirstLine();
                    if( firstLine != null )
                        jsonParagraphProperties.put(OCKey.INDENT_FIRST_LINE.value(), Utils.mapTwipTo100THMM(firstLine));
                }
                Long left = indent.getLeft();
                if( left != null )
                    jsonParagraphProperties.put(OCKey.INDENT_LEFT.value(), Utils.mapTwipTo100THMM(left));
                Long right = indent.getRight();
                if( right != null )
                    jsonParagraphProperties.put(OCKey.INDENT_RIGHT.value(), Utils.mapTwipTo100THMM(right));
            }
        }
        return jsonParagraphProperties!=null&&jsonParagraphProperties.length()>0 ? jsonParagraphProperties : null;
    }
    private static JSONObject createBorderProperties( CTBorder borderLine )
        throws JSONException {

        JSONObject line = new JSONObject();
        Color color = new Color();
        color.setVal(borderLine.getColor());
        color.setThemeColor(borderLine.getThemeColor());
        color.setThemeTint(borderLine.getThemeTint());
        color.setThemeShade(borderLine.getThemeShade());
        Commons.jsonPut(line, OCKey.COLOR.value(), Utils.createColor(color));
        Long sz = borderLine.getSz();
        if(sz != null)
            line.put(OCKey.WIDTH.value(), Utils.mapTwipTo100THMM(sz.longValue() * 5 / 2)); // Unit: 1/8 pt
        Long space = borderLine.getSpace();
        if(space != null)
            line.put(OCKey.SPACE.value(), Utils.mapTwipTo100THMM(space.longValue() * 20)); // Unit: pt
// not used
//            line.put("isframe",borderLine.isFrame());
//            line.put("isshadow", borderLine.isShadow());
        STBorder borderType = borderLine.getVal();
        if(borderType != null){
            line.put(OCKey.STYLE.value(), borderType.value());
        }
        return line;
    }
}
