/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.wml.IText;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class TextComponent extends TextRun_Base {

    public TextComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _textNode, int _componentNumber) {
        super(parentContext, _textNode, _componentNumber);
    }

    @Override
    public int getNextComponentNumber() {
        int textLength = ((IText)getNode().getData()).getValue().length();
        if (textLength==0) {
            System.out.println("Error: empty text string is no component... this results in problems with the component iterator");
            textLength++;       // increasing.. preventing an endless loop
        }
        return getComponentNumber() + textLength;
    }
    @Override
    public void splitStart(int componentPosition, SplitMode splitMode) {
    	final int splitPosition = componentPosition-getComponentNumber();
    	if(splitPosition>0) {
    		splitText(splitPosition, true);
    		setComponentNumber(getComponentNumber() + splitPosition);
    	}
    	ParagraphComponent.splitAccessor(this, true, splitMode);
    }
    @Override
    public void splitEnd(int componentPosition, SplitMode splitMode) {
    	if(getNextComponentNumber()>++componentPosition) {
    		splitText(componentPosition-getComponentNumber(), false);
    	}
    	ParagraphComponent.splitAccessor(this, false, splitMode);
    }
    public static void preserveSpace(IText t) {
        if(t!=null) {
            String s = t.getValue();
            if(s!=null&&s.length()>0) {
                if(s.charAt(0)==' '||s.charAt(s.length()-1)==' ') {
                    t.setSpace("preserve");
                    return;
                }
            }
            t.setSpace(null);
        }
    }
    private void splitText(int textSplitPosition, boolean splitStart) {
    	final IText text = (IText)getNode().getData();
        if(textSplitPosition>0&&textSplitPosition<text.getValue().length()) {
        	final IText newText = text.createText();
            newText.setParent(text.getParent());
            final DLList<Object> runContent = getTextRun().getContent();
            final StringBuffer s = new StringBuffer(text.getValue());
            if(splitStart) {
                runContent.addNode(getNode(), new DLNode<Object>(newText), true);
                newText.setValue(s.substring(0, textSplitPosition));
                text.setValue(s.substring(textSplitPosition));
            }
            else {
            	runContent.addNode(getNode(), new DLNode<Object>(newText), false);
                text.setValue(s.substring(0, textSplitPosition));
                newText.setValue(s.substring(textSplitPosition));
            }
            preserveSpace(newText);
            preserveSpace(text);
        }
    }
}
