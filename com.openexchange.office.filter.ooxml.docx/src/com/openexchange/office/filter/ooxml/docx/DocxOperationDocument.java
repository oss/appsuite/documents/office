/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.PeoplePart;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsExPart;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.DocumentSettingsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.FontTablePart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.w15.CTCommentEx;
import org.docx4j.w15.CTCommentsEx;
import org.docx4j.w15.CTPeople;
import org.docx4j.w15.CTPerson;
import org.docx4j.wml.CTLanguage;
import org.docx4j.wml.CTSettings;
import org.docx4j.wml.Comments;
import org.docx4j.wml.Comments.Comment;
import org.docx4j.wml.Fonts;
import org.docx4j.wml.Fonts.Font;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.SectPr.PgMar;
import org.docx4j.wml.Styles;
import org.docx4j.wml.Styles.LatentStyles;
import org.docx4j.wml.Styles.LatentStyles.LsdException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DocumentComplexity;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.RootComponent;
import com.openexchange.office.filter.ooxml.docx.operations.DocxApplyOperationHelper;
import com.openexchange.office.filter.ooxml.docx.operations.DocxCreateOperationHelper;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class DocxOperationDocument extends OfficeOpenXMLOperationDocument {

    private WordprocessingMLPackage opcPackage;

    public DocxOperationDocument(Session session, IResourceManager _resourceManager, DocumentProperties _documentProperties)
        throws FilterException {

        super(session, _resourceManager, _documentProperties);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.office.filter.ooxml.OperationDocument#_loadDocument(java.io.InputStream)
     *
     * returns if a  complexity check was successful and if the document could be loaded properly
     */
    @Override
    protected ByteArrayInputStream _loadDocument(InputStream _inputStream, boolean XMLReadOnly)
        throws Docx4JException {

        final long maxWordCount = getIntegerOfficeConfigurationValue( "//text/maxWordCount", 50000);
        final long maxAllowedXmlSizeMatchingComplexity = maxWordCount > 0 ? maxWordCount * 45 + 1000000 : 0;
        final ByteArrayInputStream documentInputStream = DocumentComplexity.checkDocumentComplexity(_inputStream, maxAllowedXmlSizeMatchingComplexity, 0);
        opcPackage = (WordprocessingMLPackage)OpcPackage.load(documentInputStream, XMLReadOnly);
        return documentInputStream;
    }

    // the constructor without inputStream creates an empty document
    @Override
    protected void _createPackage()
        throws Exception  {

        opcPackage = WordprocessingMLPackage.createPackage();

        final MainDocumentPart mainDocumentPart = getPackage().getMainDocumentPart();
        final ThemePart themePart = getDefaultThemePart(new PartName("/word/theme/theme1.xml"));
        mainDocumentPart.addTargetPart(themePart);
        final Styles styles = getStyles(true);
        resetDocumentLanguage(styles);
        insertPredefinedLatentStyles(styles);
        final DocumentSettingsPart documentSettingsPart = getDefaultSettingsPart(new PartName("/word/settings.xml"));
        mainDocumentPart.addTargetPart(documentSettingsPart);
        final SectPr sectPr = Utils.getDocumentProperties(mainDocumentPart, true);
        final PgMar pgMar = sectPr.getPgMar(true);
        pgMar.setLeft(Long.valueOf(1440));
        pgMar.setRight(Long.valueOf(1440));
        pgMar.setTop(Long.valueOf(1440));
        pgMar.setBottom(Long.valueOf(1440));
        pgMar.setHeader(Long.valueOf(708));
        pgMar.setFooter(Long.valueOf(708));

        // creating a fontlist (QuickOffice won't load documents without this part)
        getFontList(true);
    }

    @Override
    public void _applyDefaultDocumentProperties()
        throws Exception {

        final DocumentProperties documentProperties = getDocumentProperties();
        if(documentProperties==null) {
            return;
        }
        final DocxApplyOperationHelper applyOperationHelper = new DocxApplyOperationHelper(this);
        applyOperationHelper.setDocumentLanguage(documentProperties.optString(DocumentProperties.PROP_USER_LANGUAGE, null), false);
        final JSONObject attrs = new JSONObject(1);
        final JSONObject pageAttrs = new JSONObject(2);
        final Object pageSizeWidth = documentProperties.get(DocumentProperties.PROP_PAGESIZE_WIDTH);
        final Object pageSizeHeight = documentProperties.get(DocumentProperties.PROP_PAGESIZE_HEIGHT);
        if(pageSizeWidth instanceof Number) {
            pageAttrs.put(OCKey.WIDTH.value(), pageSizeWidth);
        }
        if(pageSizeHeight instanceof Number) {
            pageAttrs.put(OCKey.HEIGHT.value(), pageSizeHeight);
        }
        if(!pageAttrs.isEmpty()) {
            attrs.put(OCKey.PAGE.value(), pageAttrs);
        }
        if(!attrs.isEmpty()) {
            applyOperationHelper.setDocumentAttributes(attrs);
        }
    }

    @Override
    public JSONObject _getOperations()
        throws Exception {

        final JSONObject aOperations = new JSONObject();
        final JSONArray operationsArray = new JSONArray();

        final DocxCreateOperationHelper createOperationHelper = new DocxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createDocumentDefaults(documentAttributes, getUserLanguage());
        createOperationHelper.CreateFontDescriptions();
        createOperationHelper.CreateStyleOperations();
        createOperationHelper.createThemeOperations(null, null, null);
        createOperationHelper.CreateListOperations();
        createOperationHelper.CreateHeaderFooterOperations();
        createOperationHelper.createOperations();
        createOperationHelper.createDocumentAuthors(documentAttributes);
        createOperationHelper.createUsedLanguages(documentAttributes);
        aOperations.put("operations", operationsArray);
        return aOperations;
    }

    @Override
    public Map<String, Object> _getMetaData()
        throws Exception {

        final JSONArray operationsArray = new JSONArray();
        final DocxCreateOperationHelper createOperationHelper = new DocxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createDocumentDefaults(documentAttributes, getUserLanguage());
        createOperationHelper.CreateFontDescriptions();
        createOperationHelper.CreateStyleOperations();
        createOperationHelper.createThemeOperations(null, null, null);
        createOperationHelper.CreateListOperations();
        createOperationHelper.CreateHeaderFooterOperations();
        final Map<String, Object> operations = new HashMap<String, Object>(1);
        operations.put("operations", operationsArray);
        return operations;
    }

    @Override
    public Map<String, Object> _getActivePart()
        throws Exception {

        final JSONArray operationsArray = new JSONArray();
        final DocxCreateOperationHelper createOperationHelper = new DocxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createOperations();
        createOperationHelper.createDocumentAuthors(documentAttributes);
        createOperationHelper.createUsedLanguages(documentAttributes);
        final Map<String, Object> operations = new HashMap<String, Object>(1);
        operations.put("operations", operationsArray);
        return operations;
    }

    @Override
    public Map<String, Object> _getNextPart()
        throws Exception {

        return null;
    }

    private void setContext(String target) {
        if(target.isEmpty()) {
            setContextPart(null);
        }
        else {
            setContextPart(getPackage().getMainDocumentPart().getRelationshipsPart(false).getPart(target));
        }
    }

    @Override
    public void _applyOperations(String applyOperations) {

        if (applyOperations != null){
            int i = 0;
            JSONObject op = null;
            OCValue opName = null;
            try {
                DocxApplyOperationHelper applyOperationHelper = new DocxApplyOperationHelper(this);
                final JSONArray aOperations = new JSONArray(new JSONTokener(applyOperations));
                for (i = 0; i < aOperations.length(); i++) {
                    opcPackage.setSuccessfulAppliedOperations(i);
                    op = (JSONObject) aOperations.get(i);
                    final String target = op.optString(OCKey.TARGET.value(), "");
                    setContext(target);
                    opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
                    switch(opName) {
                        case INSERT_PARAGRAPH : {
                            applyOperationHelper.insertParagraph(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case DELETE : {
                            CUtil.delete(getRootComponent(), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                            break;
                        }
                        case MOVE : {
                            applyOperationHelper.move(op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                            break;
                        }
                        case SPLIT_PARAGRAPH : {
                            applyOperationHelper.splitParagraph(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case MERGE_PARAGRAPH : {
                            applyOperationHelper.mergeParagraph(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case INSERT_TEXT : {
                            applyOperationHelper.insertText(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_TAB : {
                            applyOperationHelper.insertTab(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_HARD_BREAK : {
                            applyOperationHelper.insertHardBreak(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_TABLE : {
                            applyOperationHelper.insertTable(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_ROWS : {
                            applyOperationHelper.insertRows(op.getJSONArray(OCKey.START.value()), op.optInt(OCKey.COUNT.value(), 1), op.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), op.optInt(OCKey.REFERENCE_ROW.value(), -1), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_CELLS : {
                            applyOperationHelper.insertCells(op.getJSONArray(OCKey.START.value()), op.optInt(OCKey.COUNT.value(), 1), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_COLUMN : {
                            applyOperationHelper.insertColumn(op.getJSONArray(OCKey.START.value()), op.getJSONArray(OCKey.TABLE_GRID.value()), op.getInt(OCKey.GRID_POSITION.value()), op.optString(OCKey.INSERT_MODE.value(), "before"));
                            break;
                        }
                        case DELETE_COLUMNS : {
                            applyOperationHelper.deleteColumns(op.getJSONArray(OCKey.START.value()), op.getInt(OCKey.START_GRID.value()), op.optInt(OCKey.END_GRID.value(), op.getInt(OCKey.START_GRID.value())));
                            break;
                        }
                        case SET_ATTRIBUTES : {
                            applyOperationHelper.setAttributes(op.getJSONObject(OCKey.ATTRS.value()), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                            break;
                        }
                        case INSERT_DRAWING : {
                            applyOperationHelper.insertDrawing(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_STYLE_SHEET : {
                            applyOperationHelper.insertStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value()), op.optBoolean(OCKey.HIDDEN.value(), false), op.optInt(OCKey.UI_PRIORITY.value()), op.optBoolean(OCKey.DEFAULT.value(), false), op.optBoolean(OCKey.CUSTOM.value(), false));
                            break;
                        }
                        case CHANGE_STYLE_SHEET : {
                            applyOperationHelper.changeStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.optString(OCKey.STYLE_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value()), op.optBoolean(OCKey.HIDDEN.value()), op.optInt(OCKey.UI_PRIORITY.value()));
                            break;
                        }
                        case DELETE_STYLE_SHEET : {
                            applyOperationHelper.deleteStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()));
                            break;
                        }
                        case INSERT_LIST_STYLE : {
                            applyOperationHelper.insertListStyle(op.getString(OCKey.LIST_STYLE_ID.value()), op.getJSONObject(OCKey.LIST_DEFINITION.value()));
                            break;
                        }
                        case SPLIT_TABLE : {
                            applyOperationHelper.splitTable(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case MERGE_TABLE : {
                            applyOperationHelper.mergeTable(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case SET_DOCUMENT_ATTRIBUTES : {
                            applyOperationHelper.setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_HEADER_FOOTER : {
                            applyOperationHelper.insertHeaderFooter(op.getString(OCKey.ID.value()), op.getString(OCKey.TYPE.value()));
                            break;
                        }
                        case DELETE_HEADER_FOOTER : {
                            applyOperationHelper.deleteHeaderFooter(op.getString(OCKey.ID.value()));
                            break;
                        }
                        case CHANGE_COMMENT : {
                            applyOperationHelper.changeComment(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TEXT.value()), op.optJSONArray(OCKey.MENTIONS.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_COMMENT : {
                            applyOperationHelper.insertComment(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TEXT.value()), op.optJSONArray(OCKey.MENTIONS.value()),
                                op.getString(OCKey.ID.value()), op.optString(OCKey.PARENT_ID.value(), null), op.optString(OCKey.USER_ID.value()), op.optString(OCKey.AUTHOR.value()), op.optString(OCKey.DATE.value()), op.optJSONArray(OCKey.CHILDREN.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_RANGE : {
                            applyOperationHelper.insertRange(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.ID.value(), null), op.optString(OCKey.TYPE.value(), "comment"), op.optString(OCKey.POSITION.value(), "start"), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_BOOKMARK : {
                            applyOperationHelper.insertBookmark(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.ID.value(), null), op.optString(OCKey.POSITION.value(), "start"), op.optString(OCKey.ANCHOR_NAME.value(), null));
                            break;
                        }
                        case INSERT_COMPLEX_FIELD : {
                            applyOperationHelper.insertComplexField(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.INSTRUCTION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case UPDATE_COMPLEX_FIELD : {
                            applyOperationHelper.updateComplexField(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.INSTRUCTION.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case DELETE_LIST_STYLE :
                            // PASSTHROUGH INTENDED
                        case NO_OP : {
                            break;
                        }
                        case CREATE_ERROR : {
                            throw new FilterException("createError operation detected: " + opName, ErrorCode.UNSUPPORTED_OPERATION_USED);
                        }
                        default: {
                            final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
                            OfficeOpenXMLOperationDocument.logMessage("warn", "Ignoring unsupported operation: " + unsupportedOps);
                        }
                    }
                }
                opcPackage.setSuccessfulAppliedOperations(aOperations.length());

                // creating fontListPart ...for QuickOffice
                getFontList(true);

                //
                createCommentsExAndIdsPart();

                // set user language if not already set in the document
                applyOperationHelper.setDocumentLanguage(getUserLanguage(), true);
            }
            catch(Exception e) {
                String message = e.getMessage();
                if(op!=null) {
                    try {
                        if(OCValue.INSERT_TEXT==opName) {
                            op.put(OCKey.TEXT.value(), "...");
                        }
                        message += ", operation:" + Integer.toString(i) + " " + op.toString();
                    }
                    catch(JSONException f) {
                        //
                    }
                }
                throw new FilterException(message, e, ErrorCode.CRITICAL_ERROR);
            }
        }
    }

    @Override
    public WordprocessingMLPackage getPackage() {
        return opcPackage;
    }

    @Override
    public void setPackage(OpcPackage p) {
       opcPackage = (WordprocessingMLPackage)p;
    }

    @Override
    public Part getContextPart() {
        return contextPart!=null ? contextPart : getPackage().getMainDocumentPart();
    }

    @Override
    public com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent getRootComponent() {
        return new RootComponent(this, (IContentAccessor)getContextPart());
    }

    @Override
    public String getImagePath() {
        return "word/media/";
    }

    @Override
    public ThemePart getThemePart(boolean createIfMissing)
        throws FilterException {

        ThemePart themePart = (ThemePart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.THEME);
        if(themePart==null&&createIfMissing) {
            try {
                themePart = getDefaultThemePart(new PartName("/word/theme/theme1.xml"));
                getPackage().getMainDocumentPart().addTargetPart(themePart);
            }
            catch(InvalidFormatException e) {
                throw new FilterException("docx filter, could not create default theme", ErrorCode.CRITICAL_ERROR, e);
            }
        }
        return themePart;
    }

    public StyleDefinitionsPart getStyleDefinitionsPart(boolean createIfMissing) {
        StyleDefinitionsPart styleDefinitionsPart = (StyleDefinitionsPart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.STYLES);
        if(styleDefinitionsPart==null&&createIfMissing) {
            try {
                styleDefinitionsPart = new StyleDefinitionsPart(new PartName("/word/styles.xml"));
                styleDefinitionsPart.setJaxbElement(Context.getWmlObjectFactory().createStyles());
                getPackage().getMainDocumentPart().addTargetPart(styleDefinitionsPart);
            }
            catch(InvalidFormatException e) {
                throw new FilterException("docx filter, could not create style definitions part", ErrorCode.CRITICAL_ERROR, e);
            }
        }
        return styleDefinitionsPart;
    }

    public Styles getStyles(boolean createIfMissing) {
        final StyleDefinitionsPart styleDefinitionsPart = getStyleDefinitionsPart(createIfMissing);
        if(styleDefinitionsPart==null) {
            return null;
        }
        return styleDefinitionsPart.getJaxbElement();
    }

    public CTSettings getSettings(boolean createIfMissing)
        throws InvalidFormatException {

        DocumentSettingsPart settingsPart = (DocumentSettingsPart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.SETTINGS);
        if(settingsPart==null&&createIfMissing) {
            settingsPart = new DocumentSettingsPart(new PartName("/word/settings.xml"));
            settingsPart.setJaxbElement(Context.getWmlObjectFactory().createCTSettings());
            getPackage().getMainDocumentPart().addTargetPart(settingsPart);
        }
        return settingsPart!=null?settingsPart.getJaxbElement():null;
    }

    public CommentsPart getCommentsPart(boolean createIfMissing) {
        CommentsPart commentsPart = (CommentsPart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.COMMENTS);
        if(commentsPart==null&&createIfMissing) {
            try {
                commentsPart = new CommentsPart();
                commentsPart.setJaxbElement(Context.getWmlObjectFactory().createComments());
                getPackage().getMainDocumentPart().addTargetPart(commentsPart);
            }
            catch(InvalidFormatException e) {
                // ohoh
            }
        }
        if(commentsPart!=null) {
            commentsPart.mergeParaIdToComment();
            final CommentsExPart commentsExPart = getCommentsExPart(false);
            if(commentsExPart!=null) {
                // the commentsEx are containing relationships to parent comments only, we will merge
                // this into the normal commentsPart and recreate commentsEx when saving
                commentsPart.mergeCommentsExPart(commentsExPart);
                getPackage().getMainDocumentPart().getRelationshipsPart(true).removeRelationshipByType(Namespaces.COMMENTS_EX);
                getPackage().getMainDocumentPart().getRelationshipsPart(true).removeRelationshipByType(Namespaces.COMMENTS_IDS);
            }
        }
        return commentsPart;
    }

    public void createCommentsExAndIdsPart() {
        final CommentsPart commentsPart = getCommentsPart(false);
        if(commentsPart!=null) {
            final Comments comments = commentsPart.getJaxbElement();
            for(Comment c:comments.getComments()) {
                c.setParaIdToParagraph(getPackage().getNextMarkupId());
            }
            final CTCommentsEx commentsEx = new CTCommentsEx();
            final Iterator<Comment> commentIter = comments.getComments().iterator();
            while(commentIter.hasNext()) {
                final Comment childComment = commentIter.next();
                if(childComment.getParentId()!=null) {
                    final Comment parentComment = comments.getComment(childComment.getParentId(), false);
                    if(parentComment!=null) {
                        final P childPara = childComment.getFirstParagraph();
                        final P parentPara = parentComment.getFirstParagraph();
                        if(childPara!=null&&parentPara!=null) {
                            if(childPara.getParaId()==null) {
                                childPara.setParaId(getPackage().getNextParaId());
                            }
                            if(parentPara.getParaId()==null) {
                                parentPara.setParaId(getPackage().getNextParaId());
                            }
                            final CTCommentEx commentEx = new CTCommentEx();
                            commentEx.setParaId(childPara.getParaId());
                            commentEx.setParaIdParent(parentPara.getParaId());
                            commentEx.setDone("0");
                            commentsEx.getContent().add(commentEx);
                        }
                    }
                }
            }
            if(!commentsEx.getContent().isEmpty()) {
                try {
                    final CommentsExPart commentsExPart = new CommentsExPart();
                    commentsExPart.setJaxbElement(commentsEx);
                    getPackage().getMainDocumentPart().addTargetPart(commentsExPart);
                }
                catch(InvalidFormatException e) {
                    //
                }
            }
        }
    }

/* The CommentIds part seems to be optional and its content is not documented, so we remove it if we change a comment.

    public CommentsIdsPart getCommentsIdsPart(boolean createIfMissing) {
        CommentsIdsPart commentsIdsPart = (CommentsIdsPart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.COMMENTS_IDS);
        if(commentsIdsPart==null&&createIfMissing) {
            try {
                commentsIdsPart = new CommentsIdsPart();
                commentsIdsPart.setJaxbElement(new CommentsIds());
                getPackage().getMainDocumentPart().addTargetPart(commentsIdsPart);
            }
            catch(InvalidFormatException e) {
                // ohoh
            }
        }
        return commentsIdsPart;
    }
*/

    public CommentsExPart getCommentsExPart(boolean createIfMissing) {
        CommentsExPart commentsExPart = (CommentsExPart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.COMMENTS_EX);
        if(commentsExPart==null&&createIfMissing) {
            try {
                commentsExPart = new CommentsExPart();
                commentsExPart.setJaxbElement(new CTCommentsEx());
                getPackage().getMainDocumentPart().addTargetPart(commentsExPart);
            }
            catch(InvalidFormatException e) {
                // ohoh
            }
        }
        return commentsExPart;
    }

    public Comment getComment(Integer id, boolean createIfMissing) {
        final CommentsPart commentsPart = getCommentsPart(createIfMissing);
        if(commentsPart!=null) {
            return commentsPart.getJaxbElement().getComment(id, createIfMissing);
        }
        return null;
    }

    public List<Font> getFontList(boolean createIfMissing)
        throws Exception {

        FontTablePart fontTablePart = (FontTablePart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.FONT_TABLE);
        if(fontTablePart==null&&createIfMissing) {
            fontTablePart = new FontTablePart(new PartName("/word/fontTable.xml"));
            fontTablePart.setJaxbElement(Context.getWmlObjectFactory().createFonts());
            getPackage().getMainDocumentPart().addTargetPart(fontTablePart);
        }
        if(fontTablePart!=null) {
            final Fonts fonts = fontTablePart.getJaxbElement();
            if(fonts!=null) {
                final List<Font> fontList = fonts.getFont();
                if(createIfMissing) {
                    int i;
                    // we put at least one dummy font into this list
                    for(i=0;i<fontList.size();i++) {
                        final String fontName = fontList.get(i).getName();
                        if(fontName!=null&&fontName.equals("Times New Roman")) {
                            break;
                        }
                    }
                    if(i==fontList.size()) {
                        final Font timesNewRoman = Context.getWmlObjectFactory().createFontsFont();
                        timesNewRoman.setName("Times New Roman");
                        fontList.add(timesNewRoman);
                    }
                }
            }
        }
        return null;
    }

    public CTPeople getPeopleList(boolean createIfMissing) {

        try {
            PeoplePart peoplePart = (PeoplePart)getPackage().getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.PEOPLE);
            if(peoplePart==null&&createIfMissing) {
                peoplePart = new PeoplePart(new PartName("/word/people.xml"));
                peoplePart.setJaxbElement(new org.docx4j.w15.ObjectFactory().createCTPeople());
                getPackage().getMainDocumentPart().addTargetPart(peoplePart);
            }
            return peoplePart!=null?peoplePart.getJaxbElement():null;
        }
        catch(InvalidFormatException e) {
            return null;
        }
    }

    public CTPerson getAuthorById(String id) {
        try {
            final CTPeople peoples = getPeopleList(false);
            final int index = Integer.parseInt(id);
            if(peoples!=null) {
                final List<CTPerson> peopleList = peoples.getPerson();
                if(peopleList.size() > index) {
                    return peopleList.get(index);
                }
            }
        }
        catch(NumberFormatException e) {
            //
        }
        return null;
    }

    /**
     * Reset document language.
     *
     * @param styles {Styles}
     *  The WordML styles with or without language settings
     */
    private void resetDocumentLanguage(Styles styles) {
        if (styles!=null) {
            final CTLanguage ctLanguage = styles.getDocDefaults(true).getRPrDefault(true).getRPr(true).getLang(true);
            ctLanguage.setVal("");
            ctLanguage.setEastAsia("");
            ctLanguage.setBidi("");
        }
    }

    @Override
    protected void switchToNonTemplateDocument() throws Exception {
        super.switchToNonTemplateDocument();
    }

    @Override
    protected void switchToTemplateDocument() throws Exception {
        super.switchToTemplateDocument();

        final MainDocumentPart mainDocumentPart = getPackage().getMainDocumentPart();
        final RelationshipsPart mainRelationshipsPart = mainDocumentPart.getRelationshipsPart();
        removePart(mainRelationshipsPart, "/word/stylesWithEffects.xml");
        removePart(mainRelationshipsPart, "/word/webSettings.xml");
        removePart(mainRelationshipsPart, "/word/printerSettings/printerSettings1.bin");
        removePart(mainRelationshipsPart, "/word/printerSettings/printerSettings2.bin");
        removePart(mainRelationshipsPart, "/word/printerSettings/printerSettings3.bin");

        // removing rsIds from settings if available
        final DocumentSettingsPart documentSettingsPart = (DocumentSettingsPart)mainDocumentPart.getRelationshipsPart(true).getPartByType(Namespaces.SETTINGS);
        if(documentSettingsPart!=null) {
            final CTSettings settings = documentSettingsPart.getJaxbElement();
            settings.setRsids(null);
        }
    }

    private void insertPredefinedLatentStyles(Styles styles) {

        final ObjectFactory objectFactory = new ObjectFactory();
        // name, semiHidden, uiPriority, unhideWhenUsed, qFormat
        final String[] predefinedLatentStyles = {
            "Normal, 0, 0, 0, 1",
            "heading 1, 0, 9, 0, 1",
            "heading 2,, 9,, 1",
            "heading 3,, 9,, 1",
            "heading 4,, 9,, 1",
            "heading 5,, 9,, 1",
            "heading 6,, 9,, 1",
            "heading 7,, 9,, 1",
            "heading 8,, 9,, 1",
            "heading 9,, 9,, 1",
            "toc 1,, 39",
            "toc 2,, 39",
            "toc 3,, 39",
            "toc 4,, 39",
            "toc 5,, 39",
            "toc 6,, 39",
            "toc 7,, 39",
            "toc 8,, 39",
            "toc 9,, 39",
            "caption,, 35,, 1",
            "Title, 0, 10, 0, 1",
            "Default Paragraph Font, , 1",
            "Subtitle, 0, 11, 0, 1",
            "Strong, 0, 22,, 0, 1",
            "Emphasis, 0, 20,, 0, 1",
            "Table Grid, 0, 59, 0",
            "Placeholder Text,,,0",
            "No Spacing, 0, 1, 0, 1",
            "Light Shading, 0, 60,0",
            "Light List, 0 ,61, 0",
            "Light Grid, 0 ,62, 0",
            "Medium Shading 1, 0 ,63, 0",
            "Medium Shading 2, 0 ,64, 0",
            "Medium List 1, 0 ,65, 0",
            "Medium List 2, 0 ,66, 0",
            "Medium Grid 1, 0 ,67, 0",
            "Medium Grid 2, 0 ,68, 0",
            "Medium Grid 3, 0 ,69, 0",
            "Dark List, 0 ,70, 0",
            "Colorful Shading, 0 ,71, 0",
            "Colorful List, 0 ,72, 0",
            "Colorful Grid, 0 ,73, 0",
            "Light Shading Accent 1, 0 ,60, 0",
            "Light List Accent 1, 0 ,61, 0",
            "Light Grid Accent 1, 0 ,62, 0",
            "Medium Shading 1 Accent 1, 0 ,63, 0",
            "Medium Shading 2 Accent 1, 0 ,64, 0",
            "Medium List 1 Accent 1, 0 ,65, 0",
            "Revision, 0",
            "List Paragraph, 0 ,34, 0, 1",
            "Quote, 0 ,29, 0, 1",
            "Intense Quote, 0 ,30, 0, 1",
            "Medium List 2 Accent 1, 0 ,66, 0",
            "Medium Grid 1 Accent 1, 0 ,67, 0",
            "Medium Grid 2 Accent 1, 0 ,68, 0",
            "Medium Grid 3 Accent 1, 0 ,69, 0",
            "Dark List Accent 1, 0 ,70, 0",
            "Colorful Shading Accent 1, 0 ,71, 0",
            "Colorful List Accent 1, 0 ,72, 0",
            "Colorful Grid Accent 1, 0 ,73, 0",
            "Light Shading Accent 2, 0 ,60, 0",
            "Light List Accent 2, 0 ,61, 0",
            "Light Grid Accent 2, 0 ,62, 0",
            "Medium Shading 1 Accent 2, 0 ,63, 0",
            "Medium Shading 2 Accent 2, 0 ,64, 0",
            "Medium List 1 Accent 2, 0 ,65, 0",
            "Medium List 2 Accent 2, 0 ,66, 0",
            "Medium Grid 1 Accent 2, 0 ,67, 0",
            "Medium Grid 2 Accent 2, 0 ,68, 0",
            "Medium Grid 3 Accent 2, 0 ,69, 0",
            "Dark List Accent 2, 0 ,70, 0",
            "Colorful Shading Accent 2, 0 ,71, 0",
            "Colorful List Accent 2, 0 ,72, 0",
            "Colorful Grid Accent 2, 0 ,73, 0",
            "Light Shading Accent 3, 0 ,60, 0",
            "Light List Accent 3, 0 ,61, 0",
            "Light Grid Accent 3, 0 ,62, 0",
            "Medium Shading 1 Accent 3, 0 ,63, 0",
            "Medium Shading 2 Accent 3, 0 ,64, 0",
            "Medium List 1 Accent 3, 0 ,65, 0",
            "Medium List 2 Accent 3, 0 ,66, 0",
            "Medium Grid 1 Accent 3, 0 ,67, 0",
            "Medium Grid 2 Accent 3, 0 ,68, 0",
            "Medium Grid 3 Accent 3, 0 ,69, 0",
            "Dark List Accent 3, 0 ,70, 0",
            "Colorful Shading Accent 3, 0 ,71, 0",
            "Colorful List Accent 3, 0 ,72, 0",
            "Colorful Grid Accent 3, 0 ,73, 0",
            "Light Shading Accent 4, 0 ,60, 0",
            "Light List Accent 4, 0 ,61, 0",
            "Light Grid Accent 4, 0 ,62, 0",
            "Medium Shading 1 Accent 4, 0 ,63, 0",
            "Medium Shading 2 Accent 4, 0 ,64, 0",
            "Medium List 1 Accent 4, 0 ,65, 0",
            "Medium List 2 Accent 4, 0 ,66, 0",
            "Medium Grid 1 Accent 4, 0 ,67, 0",
            "Medium Grid 2 Accent 4, 0 ,68, 0",
            "Medium Grid 3 Accent 4, 0 ,69, 0",
            "Dark List Accent 4, 0 ,70, 0",
            "Colorful Shading Accent 4, 0 ,71, 0",
            "Colorful List Accent 4, 0 ,72, 0",
            "Colorful Grid Accent 4, 0 ,73, 0",
            "Light Shading Accent 5, 0 ,60, 0",
            "Light List Accent 5, 0 ,61, 0",
            "Light Grid Accent 5, 0 ,62, 0",
            "Medium Shading 1 Accent 5, 0 ,63, 0",
            "Medium Shading 2 Accent 5, 0 ,64, 0",
            "Medium List 1 Accent 5, 0 ,65, 0",
            "Medium List 2 Accent 5, 0 ,66, 0",
            "Medium Grid 1 Accent 5, 0 ,67, 0",
            "Medium Grid 2 Accent 5, 0 ,68, 0",
            "Medium Grid 3 Accent 5, 0 ,69, 0",
            "Dark List Accent 5, 0 ,70, 0",
            "Colorful Shading Accent 5, 0 ,71, 0",
            "Colorful List Accent 5, 0 ,72, 0",
            "Colorful Grid Accent 5, 0 ,73, 0",
            "Light Shading Accent 6, 0 ,60, 0",
            "Light List Accent 6, 0 ,61, 0",
            "Light Grid Accent 6, 0 ,62, 0",
            "Medium Shading 1 Accent 6, 0 ,63, 0",
            "Medium Shading 2 Accent 6, 0 ,64, 0",
            "Medium List 1 Accent 6, 0 ,65, 0",
            "Medium List 2 Accent 6, 0 ,66, 0",
            "Medium Grid 1 Accent 6, 0 ,67, 0",
            "Medium Grid 2 Accent 6, 0 ,68, 0",
            "Medium Grid 3 Accent 6, 0 ,69, 0",
            "Dark List Accent 6, 0 ,70, 0",
            "Colorful Shading Accent 6, 0 ,71, 0",
            "Colorful List Accent 6, 0 ,72, 0",
            "Colorful Grid Accent 6, 0 ,73, 0",
            "Subtle Emphasis, 0 ,19, 0, 1",
            "Intense Emphasis, 0 ,21, 0, 1",
            "Subtle Reference, 0 ,31, 0, 1",
            "Intense Reference, 0 ,32, 0, 1",
            "Book Title, 0 ,33, 0, 1",
            "Bibliography,, 37",
            "TOC Heading,, 39,, 1"
        };

        if (styles != null) {
            // set attributes for the latent styles part
            LatentStyles latentStyles = styles.getLatentStyles();
            latentStyles.setDefLockedState(false);
            latentStyles.setDefUIPriority(Long.valueOf(99));
            latentStyles.setDefSemiHidden(true);
            latentStyles.setDefUnhideWhenUsed(true);
            latentStyles.setDefQFormat(false);

            // Extract predefined latent styles and put into the styles parts
            List<LsdException> styleList = latentStyles.getLsdException();
            // We will add all known latent MS styles therefore remove preset
            // to prevent duplicates
            styleList.clear();

            for (String style: predefinedLatentStyles) {
                String[] styleParts = style.split(",");

                LsdException lsdException = objectFactory.createStylesLatentStylesLsdException();
                lsdException.setParent(styleList);
                for (int i = 0; i < styleParts.length; i++) {

                    String value = styleParts[i].trim();
                    if (value.length() > 0) {
                        switch (i) {
                            case 0: lsdException.setName(value); break;
                            case 1: lsdException.setSemiHidden(Boolean.parseBoolean(value)); break;
                            case 2: lsdException.setUiPriority(Long.valueOf(Long.parseLong(value))); break;
                            case 3: lsdException.setUnhideWhenUsed(Boolean.parseBoolean(value)); break;
                            case 4: lsdException.setQFormat(Boolean.parseBoolean(value)); break;
                        }
                    }
                }
                styleList.add(lsdException);
            }
        }
    }

    // is converting a comment id "cmtffffffff" into an integer
    public static int commentOperationIdToId(String v) {
        return Integer.parseUnsignedInt(v.substring(3), 16);
    }

    public static String getCommentOperationId(int id) {
        final StringBuilder builder = new StringBuilder();
        builder.append("cmt");
        builder.append(Integer.toHexString(id));
        return builder.toString();
    }
}
