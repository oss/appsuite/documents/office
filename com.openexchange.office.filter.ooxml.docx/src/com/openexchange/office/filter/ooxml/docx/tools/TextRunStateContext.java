/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import org.docx4j.jaxb.Context;
import org.docx4j.wml.P;
import org.docx4j.wml.P.Hyperlink;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.docx.components.HyperlinkContext;
import com.openexchange.office.filter.ooxml.docx.components.ParagraphComponent;
import com.openexchange.office.filter.ooxml.docx.components.RunDelContext;
import com.openexchange.office.filter.ooxml.docx.components.RunInsContext;
import com.openexchange.office.filter.ooxml.docx.components.RunMoveFromContext;
import com.openexchange.office.filter.ooxml.docx.components.RunMoveToContext;
import com.openexchange.office.filter.ooxml.docx.components.SdtParagraphContext;
import com.openexchange.office.filter.ooxml.docx.components.TextRunContext;
import com.openexchange.office.filter.ooxml.docx.components.TextRun_Base;
import org.docx4j.wml.R;
import org.docx4j.wml.RDBase;
import org.docx4j.wml.RIBase;
import org.docx4j.wml.RunDel;
import org.docx4j.wml.RunIns;
import org.docx4j.wml.RunMoveFrom;
import org.docx4j.wml.RunMoveTo;

public class TextRunStateContext implements ITextRunState {

    final TextRun_Base textRunBase;
    private DLNode<Object> paragraphNode = null;
    private DLNode<Object> textRunNode = null;
    private DLNode<Object> hyperlinkNode = null;
    private DLNode<Object> runMoveToNode = null;
    private DLNode<Object> runMoveFromNode = null;
    private DLNode<Object> runInsNode = null;
    private DLNode<Object> runDelNode = null;

    public TextRunStateContext(TextRun_Base textRunBase) {
        this.textRunBase = textRunBase;
        ComponentContext parent = textRunBase;
        do {
            parent = parent.getParentContext();
            if(parent instanceof TextRunContext) {
                textRunNode = parent.getNode();
            }
            else if(parent instanceof HyperlinkContext) {
                hyperlinkNode = parent.getNode();
            }
            else if(parent instanceof RunMoveToContext) {
                runMoveToNode = parent.getNode();
            }
            else if(parent instanceof RunMoveFromContext) {
                runMoveFromNode = parent.getNode();
            }
            else if(parent instanceof RunInsContext) {
                runInsNode = parent.getNode();
            }
            else if(parent instanceof RunDelContext) {
                runDelNode = parent.getNode();
            }
        }
        while(!(parent instanceof ParagraphComponent));
        paragraphNode = ((ParagraphComponent)parent).getNode();
    }

    @Override
    public P getParagraph() {
        return paragraphNode!=null ? (P)paragraphNode.getData() : null;
    }

    @Override
    public Hyperlink getHyperlink() {
        return hyperlinkNode!=null ? (P.Hyperlink)hyperlinkNode.getData() : null;
    }

    @Override
    public R getTextRun() {
        return (R)textRunNode.getData();
    }

    public DLNode<Object> getTextRunNode() {
        return textRunNode;
    }

    @Override
    public boolean hasChangeTracking() {
        return runInsNode!=null||runDelNode!=null||runMoveToNode!=null||runMoveFromNode!=null;
    }

    @Override
    public Hyperlink getHyperlink(boolean forceCreate) {
        getHyperlinkNode(forceCreate);
        return hyperlinkNode!=null ? (P.Hyperlink)hyperlinkNode.getData() : null;
    }

    public DLNode<Object> getHyperlinkNode(boolean forceCreate) {
        if(hyperlinkNode==null&&forceCreate) {
            hyperlinkNode = new DLNode<Object>(Context.getWmlObjectFactory().createPHyperlink());
            textRunBase.insertContext(new HyperlinkContext(textRunBase.getOperationDocument(), hyperlinkNode), hyperlinkParentContextList);
        }
        return hyperlinkNode;
    }

    @Override
    public void removeHyperlink() {
        if(hyperlinkNode!=null) {
            textRunBase.removeContext(HyperlinkContext.class);
            hyperlinkNode = null;
        }
    }

    static final private ImmutableSet<Class<?>> hyperlinkParentContextList = ImmutableSet.<Class<?>> builder()
        .add(SdtParagraphContext.class)
        .build();

    @Override
    public RIBase getRunIns(boolean forceCreate) {
        getRunInsNode(forceCreate);
        return runInsNode!=null ? (RunIns)runInsNode.getData() : runMoveToNode!=null ? (RunMoveTo)runMoveToNode.getData() : null;
    }

    public DLNode<Object> getRunInsNode(boolean forceCreate) {
        if(runInsNode==null&&runMoveToNode==null&&forceCreate) {
            runInsNode = new DLNode<Object>(Context.getWmlObjectFactory().createRunIns());
            textRunBase.insertContext(new RunInsContext(textRunBase.getOperationDocument(), runInsNode), runInsParentContextList);
        }
        return runInsNode!=null ? runInsNode : runMoveToNode;
    }

    @Override
    public void removeRunIns() {
        if(runInsNode!=null) {
            textRunBase.removeContext(RunInsContext.class);
            runInsNode = null;
        }
        if(runMoveToNode!=null) {
            textRunBase.removeContext(RunMoveToContext.class);
            runMoveToNode = null;
        }
    }

    static final private ImmutableSet<Class<?>> runInsParentContextList = ImmutableSet.<Class<?>> builder()
        .add(SdtParagraphContext.class)
        .add(HyperlinkContext.class)
        .build();

    @Override
    public RDBase getRunDel(boolean forceCreate) {
        getRunDelNode(forceCreate);
        return runDelNode!=null ? (RunDel)runDelNode.getData() : runMoveFromNode!=null ? (RunMoveFrom)runMoveFromNode.getData() : null;
    }

    public DLNode<Object> getRunDelNode(boolean forceCreate) {
        if(runDelNode==null&&runMoveFromNode==null&&forceCreate) {
            runDelNode = new DLNode<Object>(Context.getWmlObjectFactory().createRunDel());
            textRunBase.insertContext(new RunDelContext(textRunBase.getOperationDocument(), runDelNode), runDelParentContextList);
        }
        return runDelNode!=null ? runDelNode : runMoveFromNode;
    }

    @Override
    public void removeRunDel() {
        if(runDelNode!=null) {
            textRunBase.removeContext(RunDelContext.class);
            runDelNode = null;
        }
        if(runMoveFromNode!=null) {
            textRunBase.removeContext(RunMoveFromContext.class);
            runMoveFromNode = null;
        }
    }

    static final private ImmutableSet<Class<?>> runDelParentContextList = ImmutableSet.<Class<?>> builder()
        .add(SdtParagraphContext.class)
        .add(HyperlinkContext.class)
        .add(RunMoveToContext.class)
        .add(RunMoveFromContext.class)
        .add(RunInsContext.class)
        .build();

    public void setTextRunNode(DLNode<Object> textRunNode) {
        this.textRunNode = textRunNode;
    }

    public void setHyperlinkNode(DLNode<Object> hyperlinkNode) {
        this.hyperlinkNode = hyperlinkNode;
    }

    public void setRunMoveToNode(DLNode<Object> runMoveToNode) {
        this.runMoveToNode = runMoveToNode;
    }

    public void setRunMoveFromNode(DLNode<Object> runMoveFromNode) {
        this.runMoveFromNode = runMoveFromNode;
    }

    public void setRunInsNode(DLNode<Object> runInsNode) {
        this.runInsNode = runInsNode;
    }

    public void setRunDelNode(DLNode<Object> runDelNode) {
        this.runDelNode = runDelNode;
    }
}
