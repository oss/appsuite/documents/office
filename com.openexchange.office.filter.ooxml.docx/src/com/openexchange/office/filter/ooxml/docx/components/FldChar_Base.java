/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTFFCheckBox;
import org.docx4j.wml.CTFFDDList;
import org.docx4j.wml.CTFFData;
import org.docx4j.wml.CTFFTextInput;
import org.docx4j.wml.FldChar;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

abstract class FldChar_Base extends TextRun_Base {

    final FldChar fldChar;

    public FldChar_Base(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        fldChar = (FldChar)getObject();
    }

    @Override
	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

		super.applyAttrsFromJSON(attrs);
		removeInvalidParents();
	}

    public void removeInvalidParents() {
        if(getHyperlink(false)!=null) {
            splitStart(getComponentNumber(), SplitMode.ATTRIBUTE);
            splitEnd(getComponentNumber(), SplitMode.ATTRIBUTE);
            if(getHyperlink(false)!=null) {
                removeHyperlink();
            }
        }
    }

    @Override
	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {
		super.createJSONAttrs(attrs);

		final CTFFData ffData = fldChar.getFfData();
		if(ffData!=null) {
		    JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
		    if(characterAttrs==null) {
		        characterAttrs = new JSONObject(1);
		        attrs.put(OCKey.CHARACTER.value(), characterAttrs);
		    }
		    final JSONObject field = new JSONObject();
		    final List<JAXBElement<?>> ffContent = ffData.getNameOrEnabledOrCalcOnExit();
		    final Iterator<JAXBElement<?>> ffIter = ffContent.iterator();
		    boolean enabled = true;
		    while(ffIter.hasNext()) {
		        final JAXBElement<?> ffEntry = ffIter.next();
		        final Object ffEntryValue = ffEntry.getValue();
		        if(ffEntryValue instanceof CTFFCheckBox) {
		            field.put(OCKey.FORM_FIELD_TYPE.value(), "checkBox");
		            final BooleanDefaultTrue defaultV = ((CTFFCheckBox)ffEntryValue).getDefault();
		            final BooleanDefaultTrue checkedV = ((CTFFCheckBox)ffEntryValue).getChecked();
		            boolean checked = true;
		            if(checkedV!=null) {
		                checked = checkedV.isVal();
		            }
		            else if(defaultV!=null) {
		                checked = defaultV.isVal();
		            }
                    field.put(OCKey.CHECKED.value(), checked);
		        }
		        else if(ffEntryValue instanceof CTFFTextInput) {
		            field.put(OCKey.FORM_FIELD_TYPE.value(), "textInput");
		        }
		        else if(ffEntryValue instanceof CTFFDDList) {
		            field.put(OCKey.FORM_FIELD_TYPE.value(), "dropDownList");
		        }
		        else if(ffEntryValue instanceof BooleanDefaultTrue) {
		            if(ffEntry.getName().getLocalPart().equals("enabled")) {
		                enabled = ((BooleanDefaultTrue)ffEntryValue).isVal();
		            }
		        }
		    }
		    field.put(OCKey.ENABLED.value(), enabled);
		    characterAttrs.put(OCKey.FIELD.value(), field);
		}
		return attrs;
	}
}
