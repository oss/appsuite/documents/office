/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.wml.P;
import org.docx4j.wml.P.Hyperlink;
import org.docx4j.wml.R;
import org.docx4j.wml.RDBase;
import org.docx4j.wml.RIBase;
import org.docx4j.wml.RPr;
import org.docx4j.wml.RStyle;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.tools.Character;
import com.openexchange.office.filter.ooxml.docx.tools.ITextRunState;
import com.openexchange.office.filter.ooxml.docx.tools.TextRunStateContext;
import com.openexchange.office.filter.ooxml.docx.tools.TextUtils;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class TextRun_Base extends DocxComponent implements ITextRunState {

    final TextRunStateContext textRunStateContext;

    protected TextRun_Base(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
        textRunStateContext = new TextRunStateContext(this);
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
		return null;
	}

    @Override
    public void delete(int count) {
        DLNode<Object> hyperlinkNode = textRunStateContext.getHyperlinkNode(false);
    	DLNode<Object> previousHyperlinkNode = null;
    	if (hyperlinkNode!=null) {
			final DLNode<Object > previousNode = hyperlinkNode.getPrev();
			previousHyperlinkNode = previousNode!=null&&previousNode.getData() instanceof P.Hyperlink ? previousNode : null;
    	}
    	super.delete(count);

    	if(previousHyperlinkNode!=null) {
    		combineWithNextHyperlink(previousHyperlinkNode);
    	}
    }

    @Override
	public void splitStart(int n, SplitMode splitMode) {
		ParagraphComponent.splitAccessor(this, true, splitMode);
	}

    @Override
	public void splitEnd(int n, SplitMode splitMode) {
		ParagraphComponent.splitAccessor(this, false, splitMode);
	}

    @Override
	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        // we want the textruns in this paragraph to be optimized when saved
        getParagraph().setModified(true);

        TextUtils.applyTextRunAttributes((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, attrs, this);
		DLNode<Object> hyperlinkNode = textRunStateContext.getHyperlinkNode(false);
		if(hyperlinkNode!=null) {
			combineWithPreviousHyperlink();
			combineWithNextHyperlink(textRunStateContext.getHyperlinkNode(false));
		}
	}

	private void combineWithPreviousHyperlink() {
		final ComponentContext<OfficeOpenXMLOperationDocument> componentContext = getContext(HyperlinkContext.class);
		if(componentContext!=null) {
		    DLNode<Object> hyperlinkNode = textRunStateContext.getHyperlinkNode(false);
			final IContentAccessor hyperlinkParent = (IContentAccessor)((Child)hyperlinkNode.getData()).getParent();
			final DLList<Object> hyperlinkParentContent = (DLList<Object>)hyperlinkParent.getContent();

			final DLNode<Object> previousNode = hyperlinkNode.getPrev();
			if(previousNode!=null && previousNode.getData() instanceof P.Hyperlink) {
				final P.Hyperlink currentHyperlink = (P.Hyperlink)hyperlinkNode.getData();
				final P.Hyperlink previousHyperlink = (P.Hyperlink)previousNode.getData();
				if(currentHyperlink.getId() == null ? previousHyperlink.getId() == null : currentHyperlink.getId().equals(previousHyperlink.getId())) {
					final DLList<Object> previousHyperlinkContent = previousHyperlink.getContent();
					final DLList<Object> currentHyperlinkContent = currentHyperlink.getContent();
					currentHyperlinkContent.moveNodes(previousHyperlinkContent, previousHyperlink);
					componentContext.setNode(previousNode);
					textRunStateContext.setHyperlinkNode(previousNode);
					// empty hyperlink can now be removed
					hyperlinkParentContent.remove(currentHyperlink);
				}
			}
		}
	}

	private ComponentContext<OfficeOpenXMLOperationDocument> getContext(Class<?> contextClass) {
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)&&(parentContext!=null)) {
            if(parentContext.getClass()==contextClass) {
                return parentContext;
            }
            parentContext = parentContext.getParentContext();
        }
        return null;
    }

	private static void combineWithNextHyperlink(DLNode<Object> hyperlinkNode) {
		final IContentAccessor hyperlinkParent = (IContentAccessor)((Child)hyperlinkNode.getData()).getParent();
		final DLList<Object> hyperlinkParentContent = (DLList<Object>)hyperlinkParent.getContent();
		final DLNode<Object> nextNode = hyperlinkNode.getNext();
		if(nextNode!=null && nextNode.getData() instanceof P.Hyperlink) {
			final P.Hyperlink currentHyperlink = (P.Hyperlink)hyperlinkNode.getData();
			final P.Hyperlink nextHyperlink = (P.Hyperlink)nextNode.getData();
			if(currentHyperlink.getId() == null ? nextHyperlink.getId() == null : currentHyperlink.getId().equals(nextHyperlink.getId())) {
				final DLList<Object> nextHyperlinkContent = nextHyperlink.getContent();
				final DLList<Object> currentHyperlinkContent = currentHyperlink.getContent();
				nextHyperlinkContent.moveNodes(currentHyperlinkContent, currentHyperlink);
				// empty hyperlink can now be removed
				hyperlinkParentContent.remove(nextHyperlink);
			}
		}
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

		JSONObject changes = null;
        JSONObject jsonCharacterProperties = null;

        final R textRun = textRunStateContext.getTextRun();
        final RPr rPr = textRun.getRPr(false);
        if(rPr!=null) {
            final RStyle rStyle = rPr.getrStyle(false);
            if(rStyle!=null)
                attrs.put(OCKey.STYLE_ID.value(), rStyle.getVal());
            jsonCharacterProperties = Character.createCharacterProperties((DocxOperationDocument)operationDocument, rPr);
            if(rPr.getRPrChange()!=null) {
            	changes = new JSONObject(2);
            	final JSONObject changedAttrs = new JSONObject();
            	final org.docx4j.wml.CTRPrChange.RPr rPrChange = rPr.getRPrChange().getRPr();
            	final JSONObject changedJsonCharacterAttributes = Character.createCharacterProperties((DocxOperationDocument)operationDocument,  rPrChange);
            	if(changedJsonCharacterAttributes!=null) {
            		changedAttrs.put(OCKey.CHARACTER.value(), changedJsonCharacterAttributes);
            	}
            	final JSONObject modifiedAttrs = Utils.createJSONFromTrackInfo(getOperationDocument(), rPr.getRPrChange());
            	modifiedAttrs.put(OCKey.ATTRS.value(), changedAttrs);
            	changes.put(OCKey.MODIFIED.value(), modifiedAttrs);
            }
        }
        if(textRunStateContext.hasChangeTracking()) {
        	if(changes==null) {
        		changes = new JSONObject(2);
        	}
        	if(textRunStateContext.getRunIns(false)!=null) {
        		changes.put(OCKey.INSERTED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), textRunStateContext.getRunIns(false)));
        	}
        	if(textRunStateContext.getRunDel(false)!=null) {
        		changes.put(OCKey.REMOVED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), textRunStateContext.getRunDel(false)));
        	}
        }
    	if(changes!=null&&!changes.isEmpty()) {
    		attrs.put(OCKey.CHANGES.value(), changes);
    	}
    	final P.Hyperlink hyperlink = textRunStateContext.getHyperlink(false);
        if(hyperlink!=null) {
            final String url = Commons.getUrl(operationDocument.getContextPart(), hyperlink.getId());
            if (url!=null&&!url.isEmpty()) {
                if(jsonCharacterProperties==null) {
                    jsonCharacterProperties = new JSONObject();
                }
                jsonCharacterProperties.put(OCKey.URL.value(), url);
            }
            final String anchor = hyperlink.getAnchor();
            if(anchor!=null&&!anchor.isEmpty()) {
                if(jsonCharacterProperties==null) {
                    jsonCharacterProperties = new JSONObject();
                }
                jsonCharacterProperties.put(OCKey.ANCHOR.value(),  anchor);
            }
        }
        if(jsonCharacterProperties!=null&&!jsonCharacterProperties.isEmpty()) {
            attrs.put(OCKey.CHARACTER.value(), jsonCharacterProperties);
        }
		return attrs;
	}

    @Override
    public P getParagraph() {
        return textRunStateContext.getParagraph();
    }

    @Override
    public Hyperlink getHyperlink() {
        return textRunStateContext.getHyperlink();
    }

    @Override
    public R getTextRun() {
        return textRunStateContext.getTextRun();
    }

    public DLNode<Object> getTextRunNode() {
        return textRunStateContext.getTextRunNode();
    }

    @Override
    public boolean hasChangeTracking() {
        return textRunStateContext.hasChangeTracking();
    }

    @Override
    public Hyperlink getHyperlink(boolean forceCreate) {
        return textRunStateContext.getHyperlink(forceCreate);
    }

    @Override
    public void removeHyperlink() {
        textRunStateContext.removeHyperlink();
    }

    @Override
    public RIBase getRunIns(boolean forceCreate) {
        return textRunStateContext.getRunIns(forceCreate);
    }

    @Override
    public void removeRunIns() {
        textRunStateContext.removeRunIns();
    }

    @Override
    public RDBase getRunDel(boolean forceCreate) {
        return textRunStateContext.getRunDel(forceCreate);
    }

    @Override
    public void removeRunDel() {
        textRunStateContext.removeRunDel();
    }

    public void setTextRunNode(DLNode<Object> textRunNode) {
        textRunStateContext.setTextRunNode(textRunNode);
    }

    public void setHyperlinkNode(DLNode<Object> hyperlinkNode) {
        textRunStateContext.setHyperlinkNode(hyperlinkNode);
    }

    public void setRunMoveToNode(DLNode<Object> runMoveToNode) {
        textRunStateContext.setRunMoveToNode(runMoveToNode);
    }

    public void setRunMoveFromNode(DLNode<Object> runMoveFromNode) {
        textRunStateContext.setRunMoveFromNode(runMoveFromNode);
    }

    public void setRunInsNode(DLNode<Object> runInsNode) {
        textRunStateContext.setRunInsNode(runInsNode);
    }

    public void setRunDelNode(DLNode<Object> runDelNode) {
        textRunStateContext.setRunDelNode(runDelNode);
    }
}
