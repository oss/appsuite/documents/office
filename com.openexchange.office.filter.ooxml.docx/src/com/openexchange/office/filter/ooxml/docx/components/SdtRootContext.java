/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.wml.SdtBlock;
import org.docx4j.wml.Tbl;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;

public class SdtRootContext extends ComponentContext<OfficeOpenXMLOperationDocument> {

	public SdtRootContext(ComponentContext<OfficeOpenXMLOperationDocument> p, DLNode<Object> n) {
        super(p, n);
    }
    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> sdtBlockNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)sdtBlockNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = OfficeOpenXMLComponent.getContentModel(childNode, sdtBlockNode.getData());
            if(ParagraphComponent.isRepresentableParagraph(o)) {
                nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Tbl) {
                nextComponent = new TableComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SdtBlock) {
                final SdtRootContext sdtRootContext = new SdtRootContext(this, childNode);
                nextComponent = sdtRootContext.getNextChildComponent(null, previousChildComponent);
            }
        }
        return nextComponent;
    }
}
