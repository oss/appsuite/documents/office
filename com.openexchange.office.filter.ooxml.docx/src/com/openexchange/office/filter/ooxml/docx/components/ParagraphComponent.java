/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import javax.xml.datatype.XMLGregorianCalendar;
import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.Br;
import org.docx4j.wml.CTBookmark;
import org.docx4j.wml.CTMarkupRange;
import org.docx4j.wml.CTPPrChange;
import org.docx4j.wml.CTParaRPrOriginal;
import org.docx4j.wml.CTTrackChange;
import org.docx4j.wml.CommentRangeEnd;
import org.docx4j.wml.CommentRangeStart;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.IText;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase;
import org.docx4j.wml.PPrBase.PStyle;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.ParaRPrChange;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.RunDel;
import org.docx4j.wml.RunIns;
import org.docx4j.wml.RunMoveFrom;
import org.docx4j.wml.RunMoveTo;
import org.docx4j.wml.STBrType;
import org.docx4j.wml.STFldCharType;
import org.docx4j.wml.SdtBlock;
import org.docx4j.wml.SdtRun;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.component.IComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.IParagraph;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.tools.Character;
import com.openexchange.office.filter.ooxml.docx.tools.DrawingML;
import com.openexchange.office.filter.ooxml.docx.tools.DrawingML.DrawingML_root;
import com.openexchange.office.filter.ooxml.docx.tools.Paragraph;
import com.openexchange.office.filter.ooxml.docx.tools.TextUtils;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;

public class ParagraphComponent extends DocxComponent implements IParagraph {

    public ParagraphComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> paragraphNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)paragraphNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, paragraphNode.getData());
            if(TextRunContext.isRepresentableTextRunContext(o)) {
                final TextRunContext textRunContext = new TextRunContext(this, childNode);
                nextComponent = textRunContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof P.Hyperlink) {
                final HyperlinkContext hyperlinkContext = new HyperlinkContext(this, childNode);
                nextComponent = hyperlinkContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunIns) {
                final RunInsContext runInsContext = new RunInsContext(this, childNode);
                nextComponent = runInsContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunDel) {
                final RunDelContext runDelContext = new RunDelContext(this, childNode);
                nextComponent = runDelContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunMoveTo) {
                final RunMoveToContext runMoveToContext = new RunMoveToContext(this, childNode);
                nextComponent = runMoveToContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof RunMoveFrom) {
                final RunMoveFromContext runMoveFromContext = new RunMoveFromContext(this, childNode);
                nextComponent = runMoveFromContext.getNextChildComponent(null, previousChildComponent);
            }
            else if(o instanceof CommentRangeStart) {
            	nextComponent = new CommentRangeStartComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CommentRangeEnd) {
            	nextComponent = new CommentRangeEndComponent(this, childNode, nextComponentNumber);
            }
            else if(o.getClass()==CTBookmark.class) {
                nextComponent = new BookmarkStartComponent(this, childNode, nextComponentNumber);
            }
            else if(o.getClass()==CTMarkupRange.class) {
                nextComponent = new BookmarkEndComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SdtRun) {
                final SdtParagraphContext sdtParagraphContext = new SdtParagraphContext(this, childNode);
                nextComponent = sdtParagraphContext.getNextChildComponent(null, previousChildComponent);
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int textPosition, JSONObject attrs, ComponentType childType) throws Exception {

        switch(childType) {
            case TAB : return insertChildWithTextRun(Context.getWmlObjectFactory().createRTab(), textPosition, attrs);
            case HARDBREAK_DEFAULT :
            case HARDBREAK_PAGE :
            case HARDBREAK_COLUMN : {
                final Child newChild = Context.getWmlObjectFactory().createBr();
                if(childType==ComponentType.HARDBREAK_PAGE) {
                    ((Br)newChild).setType(STBrType.PAGE);
                }
                else if(childType==ComponentType.HARDBREAK_COLUMN) {
                    ((Br)newChild).setType(STBrType.COLUMN);
                }
                return insertChildWithTextRun(newChild, textPosition, attrs);
            }
            case COMPLEX_FIELD_START :
            case COMPLEX_FIELD_SEPARATE :
            case COMPLEX_FIELD_END : {
            	final Child newChild = Context.getWmlObjectFactory().createFldChar();
            	((FldChar)newChild).setFldCharType(STFldCharType.BEGIN);
            	if(childType==ComponentType.COMPLEX_FIELD_END) {
                	((FldChar)newChild).setFldCharType(STFldCharType.END);
            	}
            	else if(childType==ComponentType.COMPLEX_FIELD_SEPARATE) {
                	((FldChar)newChild).setFldCharType(STFldCharType.SEPARATE);
            	}
            	else {
            		((FldChar)newChild).setFldLock(false);
            	}
            	final FldChar_Base component = (FldChar_Base)insertChildWithTextRun(newChild, textPosition, attrs);
            	component.removeInvalidParents();
            	return component;
            }
            case COMMENT_REFERENCE :        return insertChildWithTextRun(Context.getWmlObjectFactory().createRCommentReference(), textPosition, attrs);
            case COMMENT_RANGE_START :      return insertChildWithoutTextRun(Context.getWmlObjectFactory().createCommentRangeStart(), textPosition, attrs);
            case COMMENT_RANGE_END :        return insertChildWithoutTextRun(Context.getWmlObjectFactory().createCommentRangeEnd(), textPosition, attrs);
            case BOOKMARK_START:            return insertChildWithoutTextRun(new CTBookmark(), textPosition, attrs);
            case BOOKMARK_END:              return insertChildWithoutTextRun(new CTMarkupRange(), textPosition, attrs);
            case AC_IMAGE : {
                final Drawing drawing = Context.getWmlObjectFactory().createDrawing();
                drawing.getAnchorOrInline().add(DrawingML_root.createInline(false, "", 500000, 500000));
                return insertChildWithTextRun(drawing, textPosition, attrs);
            }
            case AC_SHAPE :                 return insertChildWithTextRun(DrawingML.createAlternateContentShape(operationDocument.getPackage()), textPosition, attrs);
            case AC_CONNECTOR :             return insertChildWithTextRun(DrawingML.createAlternateContentConnector(operationDocument.getPackage()), textPosition, attrs);
            case AC_GROUP :                 return insertChildWithTextRun(DrawingML.createAlternateContentGroup(operationDocument.getPackage()), textPosition, attrs);
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    private IComponent<OfficeOpenXMLOperationDocument> insertChildWithTextRun(Child newChild, int textPosition, JSONObject attrs) throws Exception {

        final P paragraph = (P)getObject();
        final DLList<Object> paragraphContent = paragraph.getContent();

        IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
        if(childComponent!=null) {
            if(textPosition>0) {
                childComponent = childComponent.getComponent(textPosition-1);
                childComponent.splitEnd(textPosition-1, SplitMode.ATTRIBUTE);
            }

            // the r element can't be shared with multiple components, office 365 for instance will not display comments if
            // following construct is used, the commentReference needs to be in its own text run -> always create a new r

/*
            <w:r>
                <w:commentReference w:id="0"/>
                <w:t>rtrhrt</w:t>
            </w:r>
*/

            DLNode<Object> referenceNode = childComponent.getNode();

            final R newRun = Context.getWmlObjectFactory().createR();
            newRun.setRPr(getClonedRPrInheritance(childComponent));
            newRun.getContent().add(newChild);

            if(childComponent instanceof TextRun_Base) {
                referenceNode = ((TextRun_Base)childComponent).getTextRunNode();
            }
            final IContentAccessor<Object> destination = (IContentAccessor<Object>)((Child)referenceNode.getData()).getParent();
            newRun.setParent(destination);
            ((DLList<Object>)destination.getContent()).addNode(referenceNode, new DLNode<Object>(newRun), textPosition == 0);

        }
        else {
            final R newRun = Context.getWmlObjectFactory().createR();
            newRun.setParent(paragraph);
            paragraphContent.add(newRun);
            newChild.setParent(newRun);
            newRun.getContent().add(newChild);
        }
        if(textPosition>0) {
            childComponent = childComponent.getNextComponent();
        }
        else {
            childComponent = getNextChildComponent(null, null);
        }
        if(childComponent instanceof TextRun_Base) {
            if(((TextRun_Base)childComponent).hasChangeTracking()) {
                attrs = TextUtils.forceRemoveChangeTrack(attrs);
            }
        }
        if(attrs!=null) {
            childComponent.splitStart(textPosition, SplitMode.ATTRIBUTE);
            childComponent.splitEnd(textPosition, SplitMode.ATTRIBUTE);
            childComponent.applyAttrsFromJSON(attrs);
        }
        return childComponent;
    }

    private IComponent<OfficeOpenXMLOperationDocument> insertChildWithoutTextRun(Child newChild, int textPosition, JSONObject attrs) throws Exception {

        final P paragraph = (P)getObject();
        final DLList<Object> paragraphContent = paragraph.getContent();

        IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
        if(childComponent!=null) {
            if(textPosition>0) {
                childComponent = childComponent.getComponent(textPosition-1);
                childComponent.splitEnd(textPosition-1, SplitMode.ATTRIBUTE);
            }
            final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = childComponent.getContextChild(parentContextList);
            final ComponentContext<OfficeOpenXMLOperationDocument> parentContext = contextChild.getParentContext();
            final IContentAccessor parentObject = (IContentAccessor)parentContext.getNode().getData();
            newChild.setParent(parentObject);
            ((DLList<Object>)parentObject.getContent()).addNode(contextChild.getNode(), new DLNode<Object>(newChild), textPosition==0);
        }
        else {
            newChild.setParent(paragraph);
            paragraphContent.add(newChild);
        }
        if(textPosition>0) {
            childComponent = childComponent.getNextComponent();
        }
        else {
            childComponent = getNextChildComponent(null, null);
        }
        if(childComponent instanceof TextRun_Base) {
            if(((TextRun_Base)childComponent).hasChangeTracking()) {
                attrs = TextUtils.forceRemoveChangeTrack(attrs);
            }
        }
        if(attrs!=null) {
            childComponent.splitStart(textPosition, SplitMode.ATTRIBUTE);
            childComponent.splitEnd(textPosition, SplitMode.ATTRIBUTE);
            childComponent.applyAttrsFromJSON(attrs);
        }
        return childComponent;
    }

    static final private ImmutableSet<Class<?>> parentContextList = ImmutableSet.<Class<?>> builder()
        .add(SdtParagraphContext.class)
        .add(HyperlinkContext.class)
        .add(RunMoveToContext.class)
        .add(RunMoveFromContext.class)
        .add(RunInsContext.class)
        .add(RunDelContext.class)
        .build();

    public static void splitAccessor(OfficeOpenXMLComponent component, boolean splitStart, SplitMode splitMode) {
        DLNode<Object> node = component.getNode();
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = component.getParentContext();
        while(parentContext!=null) {
        	if(parentContext instanceof ParagraphComponent) {
        		break;
        	}
        	if(splitMode==SplitMode.ATTRIBUTE) {
        		if(parentContext instanceof SdtParagraphContext) {
        			break;
        		}
        	}

            final DLNode<Object> parentNode = parentContext.getNode();
            if((splitStart&&!node.isFirst())||(!splitStart&&!node.isLast())) {
                DLNode<Object> newParentNode = null;
                if(parentContext instanceof TextRunContext) {
                    final R parent = (R)parentNode.getData();
                    final R newParent = Context.getWmlObjectFactory().createR();
                    newParentNode = new DLNode<Object>(newParent);
                    final RPr rPr = parent.getRPr(false);
                    if(rPr!=null) {
                    	final RPr rPrNew = XmlUtils.deepCopy(rPr, component.getOperationDocument().getPackage());
                    	TextUtils.resetMarkupId(rPrNew);
                        newParent.setRPr(rPrNew);
                    }
                    if(splitStart&&component instanceof TextRun_Base) {
                        ((TextRun_Base)component).setTextRunNode(newParentNode);
                    }
                }
                else if(parentContext instanceof HyperlinkContext) {
                    final P.Hyperlink parent = (P.Hyperlink)parentNode.getData();
                    final P.Hyperlink newParent = Context.getWmlObjectFactory().createPHyperlink();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setAnchor(parent.getAnchor());
                    newParent.setDocLocation(parent.getDocLocation());
                    newParent.setHistory(parent.isHistory());
                    newParent.setId(parent.getId());
                    newParent.setTgtFrame(parent.getTgtFrame());
                    newParent.setTooltip(parent.getTooltip());
                    if(splitStart&&component instanceof TextRun_Base) {
                    	((TextRun_Base)component).setHyperlinkNode(newParentNode);
                    }
                }
                else if(parentContext instanceof RunMoveToContext) {
                    final RunMoveTo parent = (RunMoveTo)parentNode.getData();
                    final RunMoveTo newParent = Context.getWmlObjectFactory().createRunMoveTo();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setAuthor(parent.getAuthor());
                    final XMLGregorianCalendar date = parent.getDate();
                    if(date!=null) {
                        newParent.setDate((XMLGregorianCalendar) date.clone());
                    }
                    if(splitStart&&component instanceof TextRun_Base) {
                        ((TextRun_Base)component).setRunMoveToNode(newParentNode);
                    }
                }
                else if(parentContext instanceof RunMoveFromContext) {
                    final RunMoveFrom parent = (RunMoveFrom)parentNode.getData();
                    final RunMoveFrom newParent = Context.getWmlObjectFactory().createRunMoveFrom();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setAuthor(parent.getAuthor());
                    final XMLGregorianCalendar date = parent.getDate();
                    if(date!=null) {
                        newParent.setDate((XMLGregorianCalendar) date.clone());
                    }
                    if(splitStart&&component instanceof TextRun_Base) {
                        ((TextRun_Base)component).setRunMoveFromNode(newParentNode);
                    }
                }
                else if(parentContext instanceof RunInsContext) {
                    final RunIns parent = (RunIns)parentNode.getData();
                    final RunIns newParent = Context.getWmlObjectFactory().createRunIns();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setAuthor(parent.getAuthor());
                    final XMLGregorianCalendar date = parent.getDate();
                    if(date!=null) {
                        newParent.setDate((XMLGregorianCalendar) date.clone());
                    }
                    if(splitStart&&component instanceof TextRun_Base) {
                    	((TextRun_Base)component).setRunInsNode(newParentNode);
                    }
                }
                else if(parentContext instanceof RunDelContext) {
                    final RunDel parent = (RunDel)parentNode.getData();
                    final RunDel newParent = Context.getWmlObjectFactory().createRunDel();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setAuthor(parent.getAuthor());
                    final XMLGregorianCalendar date = parent.getDate();
                    if(date!=null) {
                        newParent.setDate((XMLGregorianCalendar)date.clone());
                    }
                    if(splitStart&&component instanceof TextRun_Base) {
                    	((TextRun_Base)component).setRunDelNode(newParentNode);
                    }
                }
                else if(parentContext instanceof SdtParagraphContext&&splitMode==SplitMode.DELETE) {
                    final SdtRun parent = (SdtRun)parentNode.getData();
                    final SdtRun newParent = Context.getWmlObjectFactory().createSdtRun();
                    newParentNode = new DLNode<Object>(newParent);
                    newParent.setSdtEndPr(parent.getSdtEndPr());
                    newParent.setSdtPr(parent.getSdtPr());
                }
                if(newParentNode!=null) {
                    ((Child)newParentNode.getData()).setParent(parentNode.getData());
                    final DLList<Object> rContent = (DLList<Object>)((IContentAccessor)parentNode.getData()).getContent();
                    final DLList<Object> rContentNew = (DLList<Object>)((IContentAccessor)newParentNode.getData()).getContent();
                    final Object pParent = ((Child)parentNode.getData()).getParent();
                    final Object pNewParent = newParentNode.getData();
                    ((Child)pNewParent).setParent(pParent);
                    final DLList<Object> pParentNode = (DLList<Object>)((IContentAccessor)pParent).getContent();
                    pParentNode.addNode(parentNode, newParentNode, false);
                    final DLNode<Object> sourceRefNode = !splitStart ? node.getNext() : node;
                    if(sourceRefNode!=null) {
                        rContent.moveNodes(sourceRefNode, -1, rContentNew, rContentNew.getLastNode(), false, pNewParent);
                    }
                    if(splitStart) {
                        parentContext.setNode(newParentNode);
                    }
                }
            }
            node = parentContext.getNode();
            parentContext = parentContext.getParentContext();
        }
    }

    @Override
	public void applyAttrsFromJSON(JSONObject attrs) {
		if(attrs==null) {
			return;
		}
		try {
	        final P paragraph = (P)getObject();
	        final PPr pPr = paragraph.getPPr(true);
	        if(attrs.has(OCKey.STYLE_ID.value())) {
	            final Object styleId = attrs.get(OCKey.STYLE_ID.value());
	            if(styleId instanceof String) {
	                final PPrBase.PStyle pStyle = pPr.getPStyle(true);
	                pStyle.setVal((String)styleId);
	                pPr.setPStyle(pStyle);
	            }
	            else {
	                pPr.setPStyle(null);
	            }
	        }
	        Paragraph.applyParagraphProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, attrs.optJSONObject(OCKey.PARAGRAPH.value()), pPr);
	        final Object characterAttrs = attrs.opt(OCKey.CHARACTER.value());
	        if(characterAttrs instanceof JSONObject) {
	            com.openexchange.office.filter.ooxml.docx.tools.Character.applyCharacterProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)characterAttrs, pPr.getRPr(true));
	        }
            final Object changes = attrs.opt(OCKey.CHANGES.value());
            if(changes!=null) {
	            if(changes instanceof JSONObject) {
	                final ParaRPr paraRPr = pPr.getRPr(true);
	            	final Object attrsInserted = ((JSONObject)changes).opt(OCKey.INSERTED.value());
	            	if(attrsInserted!=null) {
	            		if(attrsInserted instanceof JSONObject) {
	            		    if(paraRPr.getIns(false)==null) {
	            		        paraRPr.setIns(paraRPr.getMoveTo(false));
	            		        paraRPr.setMoveTo(null);
	            		    }
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsInserted, paraRPr.getIns(true));
	            		}
	            		else {
	            			paraRPr.setIns(null);
	            		}
	            		paraRPr.setMoveTo(null);
	            	}
	            	final Object attrsRemoved = ((JSONObject)changes).opt(OCKey.REMOVED.value());
	            	if(attrsRemoved!=null) {
	            		if(attrsRemoved instanceof JSONObject) {
	            		    if(paraRPr.getDel(false)==null) {
	            		        paraRPr.setDel(paraRPr.getMoveFrom(false));
	            		        paraRPr.setMoveFrom(null);
	            		    }
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsRemoved, paraRPr.getDel(true));
	            		}
	            		else {
	            			paraRPr.setDel(null);
	            		}
	            		paraRPr.setMoveFrom(null);
	            	}
	            	final Object modified = ((JSONObject)changes).opt(OCKey.MODIFIED.value());
	            	if(modified!=null) {
	            		if(modified instanceof JSONObject) {
	            		    final ParaRPrChange rPrChange = paraRPr.getRPrChange(true);
	            		    final CTPPrChange pPrChange = pPr.getPPrChange(true);
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)modified, rPrChange);
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)modified, pPrChange);
                			final Object attrsModified = ((JSONObject)modified).opt(OCKey.ATTRS.value());
                			if(attrsModified!=null) {
                				if(attrsModified instanceof JSONObject) {
		                			// Style
                					if(((JSONObject) attrsModified).has(OCKey.STYLE_ID.value())) {
        					            final Object styleId = attrs.get(OCKey.STYLE_ID.value());
        					            final PPrBase pPrBase = pPrChange.getPPr(true);
         					            if(styleId instanceof String) {
         					                final PPrBase.PStyle pStyle = pPrBase.getPStyle(true);
        					                pStyle.setVal((String)styleId);
        					                pPrBase.setPStyle(pStyle);
        					            }
        					            else {
        					                pPrBase.setPStyle(null);
        					            }
                					}
                					final Object characterChanges = ((JSONObject)attrsModified).opt(OCKey.CHARACTER.value());
			            			if(characterChanges!=null) {
			            				if(characterChanges instanceof JSONObject) {
			            					Character.applyCharacterProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)characterChanges, rPrChange.getRPr(true));
			            				}
			            				else {
			            					rPrChange.setRPr(null);
			            				}
			            			}
			            			final Object paragraphChanges = ((JSONObject)attrsModified).opt(OCKey.PARAGRAPH.value());
			            			if(paragraphChanges!=null) {
			            				if(paragraphChanges instanceof JSONObject) {
	            							Paragraph.applyParagraphProperties((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)paragraphChanges, pPrChange.getPPr(true));
			            				}
			            				else {
			            					pPr.setPPrChange(null);
			            				}
			            			}
                				}
                				else {
                					paraRPr.setRPrChange(null);
                					pPr.setPPrChange(null);
                				}
                			}
	            		}
	            		else {
	            			paraRPr.setRPrChange(null);
	            			pPr.setPPrChange(null);
	            		}
	            	}
	            }
	            else {
	            	final ParaRPr paraRPr = pPr.getRPr(false);
	            	if(paraRPr!=null) {
	            		paraRPr.setIns(null);
	            		paraRPr.setDel(null);
	            		paraRPr.setRPrChange(null);
	            	}
	            }
            }
		}
        catch (Exception e) {
            //
		}
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
		throws JSONException {

		final PPr pPr = ((P)getObject()).getPPr(false);
        if(pPr!=null) {
            final PPrBase.PStyle pStyle = pPr.getPStyle(false);
            if(pStyle!=null) {
                attrs.put(OCKey.STYLE_ID.value(), pStyle.getVal());
            }
            final JSONObject paragraphProperties = Paragraph.createParagraphProperties(pPr);
            if(paragraphProperties!=null) {
                attrs.put(OCKey.PARAGRAPH.value(), paragraphProperties);
            }
            final ParaRPr paraRPr = pPr.getRPr(false);
            if(paraRPr!=null) {
                final JSONObject characterProperties = Character.createCharacterProperties((DocxOperationDocument)operationDocument, paraRPr);
                if(characterProperties!=null) {
                    attrs.put(OCKey.CHARACTER.value(), characterProperties);
                }
            }

            CTTrackChange rPrIns = null;
        	CTTrackChange rPrDel = null;
        	ParaRPrChange rPrChange = null;
        	if(paraRPr!=null) {
        		rPrIns = paraRPr.getIns(false);
                if(rPrIns==null) {
                    rPrIns = paraRPr.getMoveTo(false);
                }
        		rPrDel = paraRPr.getDel(false);
        		if(rPrDel==null) {
        		    rPrDel = paraRPr.getMoveFrom(false);
        		}
        		rPrChange = paraRPr.getRPrChange(false);
        	}
        	final CTPPrChange pPrChange = pPr.getPPrChange(false);
            if(rPrIns!=null||rPrDel!=null||rPrChange!=null||pPrChange!=null) {
            	final JSONObject changes = new JSONObject(2);
            	if(rPrIns!=null) {
            		changes.put(OCKey.INSERTED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), rPrIns));
                }
                if(rPrDel!=null) {
                	changes.put(OCKey.REMOVED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), rPrDel));
                }
                if(rPrChange!=null||pPrChange!=null) {
                	JSONObject modified = null;
                	JSONObject modifiedAttrs = new JSONObject(3);
                	if(rPrChange!=null) {
                		modified = Utils.createJSONFromTrackInfo(getOperationDocument(), rPrChange);
	                	final CTParaRPrOriginal rPrOriginal  = rPrChange.getRPr(false);
                    	if(rPrOriginal!=null) {
	                    	final JSONObject changedJsonCharacterAttributes = Character.createCharacterProperties((DocxOperationDocument)operationDocument, rPrOriginal);
	                    	if(changedJsonCharacterAttributes!=null) {
	                    		modifiedAttrs.put(OCKey.CHARACTER.value(), changedJsonCharacterAttributes);
	                    	}
                    	}
                	}
                	if(pPrChange!=null) {
 	                	if(modified==null) {
 	                		modified = Utils.createJSONFromTrackInfo(getOperationDocument(), pPrChange);
 	                	}
 	                	final PPrBase pPrOriginal = pPrChange.getPPr(false);
 	                	if(pPrOriginal!=null) {
	 	                	final PStyle originalStyle = pPrOriginal.getPStyle(false);
		 	   	            if(originalStyle!=null) {
		 		                modifiedAttrs.put(OCKey.STYLE_ID.value(), originalStyle.getVal());
		 		            }
 	                	}
 	                	final JSONObject changedJsonParagraphAttributes = Paragraph.createParagraphProperties(pPrOriginal);
 	                	if(changedJsonParagraphAttributes!=null) {
 	                		modifiedAttrs.put(OCKey.PARAGRAPH.value(), changedJsonParagraphAttributes);
 	                	}
                	}

                	if(!modifiedAttrs.isEmpty()) {
                		modified.put(OCKey.ATTRS.value(), modifiedAttrs);
                	}
                	if(!modified.isEmpty()) {
                		changes.put(OCKey.MODIFIED.value(), modified);
                	}
                }
                if(!changes.isEmpty()) {
                	attrs.put(OCKey.CHANGES.value(), changes);
                }
            }
        }
        return attrs;
	}

	@Override
	public void insertText(int textPosition, String text, JSONObject attrs) throws Exception {

    	if(text.length()>0) {
        	final P paragraph = (P)getObject();

        	// if setModified is true text of this pararagrpah is checked if adjacent text can be combined before marshaling
        	paragraph.setModified(true);

            IText t = null;
            IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
            IComponent<OfficeOpenXMLOperationDocument> cRet = null;

            if(childComponent!=null) {

                if(textPosition>0) {
            		childComponent = childComponent.getComponent(textPosition-1);
            	}
                // check if the character could be inserted into an existing text:
                if(childComponent instanceof TextComponent) {
                    t = (IText)childComponent.getObject();
                    final StringBuffer s = new StringBuffer(t.getValue());
                    s.insert(textPosition-((TextComponent)childComponent).getComponentNumber(), text);
                    t.setValue(s.toString());
                    cRet = childComponent;
                }
                else {

                    DLNode<Object> referenceNode = childComponent.getNode();

                    t = Context.getWmlObjectFactory().createText();
                    t.setValue(text);

                    final R newRun = Context.getWmlObjectFactory().createR();
                    newRun.setRPr(getClonedRPrInheritance(childComponent));
                    t.setParent(newRun);
                    newRun.getContent().add(t);

                    if(childComponent instanceof TextRun_Base) {
                        referenceNode = ((TextRun_Base)childComponent).getTextRunNode();
                    }
                    final IContentAccessor<Object> destination = (IContentAccessor<Object>)((Child)referenceNode.getData()).getParent();
                    newRun.setParent(destination);
                    ((DLList<Object>)destination.getContent()).addNode(referenceNode, new DLNode<Object>(newRun), textPosition == 0);

                    if(textPosition>0) {
                        cRet = childComponent.getNextComponent();
                    }
                    else {
                        cRet = getNextChildComponent(null, null);
                    }
                }
            }
            else {

            	// the paragraph is empty, we have to create R and its text
                final R newRun = Context.getWmlObjectFactory().createR();
                newRun.setParent(getObject());
                paragraph.getContent().add(newRun);
                t = Context.getWmlObjectFactory().createText();
                t.setParent(newRun);
                t.setValue(text);
                newRun.getContent().add(t);
                cRet = getNextChildComponent(null, null);
            }
			TextComponent.preserveSpace(t);
			if(cRet instanceof TextRun_Base&&((TextRun_Base)cRet).hasChangeTracking()) {
				attrs = TextUtils.forceRemoveChangeTrack(attrs);
			}
            if(attrs!=null) {
            	cRet.splitStart(textPosition, SplitMode.ATTRIBUTE);
            	cRet.splitEnd(textPosition+text.length()-1, SplitMode.ATTRIBUTE);
            	cRet.applyAttrsFromJSON(attrs);
            }
        }
	}

	@Override
    public void splitParagraph(int textPosition) {

    	final P paragraph = (P)getObject();

    	IComponent<OfficeOpenXMLOperationDocument> component = getNextChildComponent(null, null);
    	while(component!=null&&component.getNextComponentNumber()<=textPosition) {
    		component = component.getNextComponent();
    	}
        if(component!=null) {
        	component.splitStart(textPosition, SplitMode.ATTRIBUTE);
        }
        // creating our new paragraph
        final P destParagraph = Context.getWmlObjectFactory().createP();
        destParagraph.setParent(paragraph.getParent());
        final DLNode<Object> destParagraphNode = new DLNode<Object>(destParagraph);
        Utils.getContent(paragraph.getParent()).addNode(getNode(), destParagraphNode, false);
        final PPr sourceParagraphProperties = paragraph.getPPr(false);
        if(sourceParagraphProperties!=null){
            final PPr destParagraphProperties = XmlUtils.deepCopy(sourceParagraphProperties, getOperationDocument().getPackage());
            // no inheritance for pageBreakBefore attribute... this attribute is always false

            TextUtils.resetMarkupId(destParagraphProperties);

            destParagraphProperties.setPageBreakBefore(null);
            destParagraph.setPPr(destParagraphProperties);
        }
        while(component!=null) {
            if(textPosition>=component.getComponentNumber()&&textPosition<component.getNextComponentNumber()) {
                boolean skipSdt = false;
                IComponentContext<OfficeOpenXMLOperationDocument> childContext = component;
                ComponentContext<OfficeOpenXMLOperationDocument> parentContext = component.getParentContext();
                while(!(childContext instanceof ParagraphComponent)) {
                    if(parentContext instanceof SdtParagraphContext) {
                        if(!childContext.getNode().isFirst()) {
                            // moving end-part of the sdt into the next paragraph
                            final DLList<Object> sourceContent = (DLList<Object>)((IContentAccessor)parentContext.getNode().getData()).getContent();
                            sourceContent.moveNodes(childContext.getNode(), -1, destParagraph.getContent(), destParagraph.getContent().getLastNode(), false, destParagraph);
                            skipSdt = true;
                        }
                    }
                    else if(parentContext instanceof ParagraphComponent) {
                        final DLList<Object> parentContent = (DLList<Object>)((IContentAccessor)parentContext.getNode().getData()).getContent();
                        DLNode<Object> sourceRefNode = childContext.getNode();
                        if(skipSdt) {
                            sourceRefNode = sourceRefNode.getNext();
                        }
                        if(sourceRefNode!=null) {
                            parentContent.moveNodes(sourceRefNode, -1, destParagraph.getContent(), destParagraph.getContent().getLastNode(), false, destParagraph);
                        }
                    }
                    childContext = parentContext;
                    parentContext = childContext.getParentContext();
                }
                break;
            }
            component = component.getNextComponent();
        }
        
    }

    @Override
    public void mergeParagraph() {
    	final IComponent<OfficeOpenXMLOperationDocument> nextParagraphComponent = getNextComponent();
    	if(nextParagraphComponent instanceof ParagraphComponent) {
        	final P paragraph = (P)getObject();
        	final DLList<Object> sourceContent = ((P)nextParagraphComponent.getObject()).getContent();
            final DLList<Object> destContent = paragraph.getContent();
            if(sourceContent.size()>0) {
                sourceContent.moveNodes(destContent, paragraph);
            }
            DLNode<Object> parentContextNode = nextParagraphComponent.getParentContext().getNode();
        	Utils.getContent(parentContextNode.getData()).removeNode(nextParagraphComponent.getNode());
        	if(parentContextNode.getData() instanceof SdtBlock) {
        	    final ComponentContext<OfficeOpenXMLOperationDocument> sdtRootContext = nextParagraphComponent.getParentContext();
        	    final ComponentContext<OfficeOpenXMLOperationDocument> parentOfSdtRootContext = sdtRootContext.getParentContext();
                if(((IContentAccessor)sdtRootContext.getNode().getData()).getContent().isEmpty()) {
                    Utils.getContent(parentOfSdtRootContext.getNode().getData()).removeNode(sdtRootContext.getNode());
                }
        	}
    	}
    }

    // returns a clone of RPr attributes for the given referenceCompnent, it is searched for RPr attributes in the following order: the given
    // referenceComponent itself, components left of the reference, components right of the reference and last at the paragraph
    private RPr getClonedRPrInheritance(IComponent<OfficeOpenXMLOperationDocument> referenceComponent) {
        if(isTextComponent(referenceComponent)) {
            return RPr.cloneFromRPrBase(((R)(((TextRun_Base)referenceComponent).getTextRunNode()).getData()).getRPr(false));
        }
        TextRun_Base textRunBase = getPreviousTextRun(referenceComponent);
        if(textRunBase == null) {
            textRunBase = getNextTextRun(referenceComponent);
        }
        if(textRunBase!=null) {
            return RPr.cloneFromRPrBase(textRunBase.getTextRun().getRPr(false));
        }
        return null;
    }

    private TextRun_Base getPreviousTextRun(IComponent<OfficeOpenXMLOperationDocument> reference) {
        TextRun_Base lastRPr = null;
        IComponent<OfficeOpenXMLOperationDocument> component = getNextChildComponent(null, null);
        while(component!=null&&component.getNextComponentNumber()<=reference.getComponentNumber()) {
            if(isTextComponent(component)) {
                lastRPr = (TextRun_Base)component;
            }
            component = component.getNextComponent();
        }
        return lastRPr;
    }

    private TextRun_Base getNextTextRun(IComponent<OfficeOpenXMLOperationDocument> reference) {
        IComponent<OfficeOpenXMLOperationDocument> component = reference.getNextComponent();
        while(component!=null) {
            if(isTextComponent(component)) {
                return (TextRun_Base)component;
            }
            component = component.getNextComponent();
        }
        return null;
    }

    private boolean isTextComponent(IComponent<OfficeOpenXMLOperationDocument> component) {
        return component instanceof TextRun_Base && !(component instanceof ShapeComponent_Root);
    }

    public static boolean isRepresentableParagraph(Object o) {
        return o instanceof P && ((P)o).notVanished();
    }
}
