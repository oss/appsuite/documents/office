/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.operations;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Stack;
import jakarta.xml.bind.JAXBElement;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.URIHelper;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.WordprocessingML.FontTablePart;
import org.docx4j.openpackaging.parts.WordprocessingML.FooterPart;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.vml.CTImageData;
import org.docx4j.vml.CTShape;
import org.docx4j.w15.CTPeople;
import org.docx4j.w15.CTPerson;
import org.docx4j.w15.CTPresenceInfo;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.Br;
import org.docx4j.wml.CTBookmark;
import org.docx4j.wml.CTMarkupRange;
import org.docx4j.wml.CTRel;
import org.docx4j.wml.CTSettings;
import org.docx4j.wml.CTTabStop;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.CTTblStylePr;
import org.docx4j.wml.CTTwipsMeasure;
import org.docx4j.wml.CTZoom;
import org.docx4j.wml.Color;
import org.docx4j.wml.CommentRangeEnd;
import org.docx4j.wml.CommentRangeStart;
import org.docx4j.wml.Comments.Comment;
import org.docx4j.wml.DocDefaults;
import org.docx4j.wml.DocDefaults.PPrDefault;
import org.docx4j.wml.DocDefaults.RPrDefault;
import org.docx4j.wml.FontFamily;
import org.docx4j.wml.FontPanose;
import org.docx4j.wml.FontPitch;
import org.docx4j.wml.Fonts;
import org.docx4j.wml.Fonts.Font.AltName;
import org.docx4j.wml.FooterReference;
import org.docx4j.wml.Ftr;
import org.docx4j.wml.Hdr;
import org.docx4j.wml.HeaderReference;
import org.docx4j.wml.IText;
import org.docx4j.wml.Jc;
import org.docx4j.wml.Lvl;
import org.docx4j.wml.Lvl.LvlText;
import org.docx4j.wml.Lvl.Start;
import org.docx4j.wml.Lvl.Suff;
import org.docx4j.wml.NumFmt;
import org.docx4j.wml.NumberFormat;
import org.docx4j.wml.Numbering;
import org.docx4j.wml.Numbering.AbstractNum;
import org.docx4j.wml.Numbering.AbstractNum.NumStyleLink;
import org.docx4j.wml.Numbering.Num;
import org.docx4j.wml.Numbering.Num.AbstractNumId;
import org.docx4j.wml.Numbering.Num.LvlOverride;
import org.docx4j.wml.Numbering.Num.LvlOverride.StartOverride;
import org.docx4j.wml.Numbering.NumPicBullet;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase.Ind;
import org.docx4j.wml.PPrBase.NumPr;
import org.docx4j.wml.PPrBase.NumPr.NumId;
import org.docx4j.wml.Pict;
import org.docx4j.wml.R;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.STBrType;
import org.docx4j.wml.STPageOrientation;
import org.docx4j.wml.STPitch;
import org.docx4j.wml.STTblStyleOverrideType;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.SectPr.PgMar;
import org.docx4j.wml.SectPr.PgSz;
import org.docx4j.wml.Style;
import org.docx4j.wml.Style.Name;
import org.docx4j.wml.Style.Next;
import org.docx4j.wml.Style.UiPriority;
import org.docx4j.wml.Styles;
import org.docx4j.wml.Tabs;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.TrPr;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.MultiComponent;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.BookmarkEndComponent;
import com.openexchange.office.filter.ooxml.docx.components.BookmarkStartComponent;
import com.openexchange.office.filter.ooxml.docx.components.CommentRangeEndComponent;
import com.openexchange.office.filter.ooxml.docx.components.CommentRangeStartComponent;
import com.openexchange.office.filter.ooxml.docx.components.CommentReferenceComponent;
import com.openexchange.office.filter.ooxml.docx.components.FldCharBegin;
import com.openexchange.office.filter.ooxml.docx.components.FldCharEnd;
import com.openexchange.office.filter.ooxml.docx.components.FldCharSeparate;
import com.openexchange.office.filter.ooxml.docx.components.HardBreakComponent;
import com.openexchange.office.filter.ooxml.docx.components.IDrawingCharacterAttributes;
import com.openexchange.office.filter.ooxml.docx.components.ParagraphComponent;
import com.openexchange.office.filter.ooxml.docx.components.RootComponent;
import com.openexchange.office.filter.ooxml.docx.components.ShapeComponent;
import com.openexchange.office.filter.ooxml.docx.components.ShapeComponent_Root;
import com.openexchange.office.filter.ooxml.docx.components.TabComponent;
import com.openexchange.office.filter.ooxml.docx.components.TableComponent;
import com.openexchange.office.filter.ooxml.docx.components.TextComponent;
import com.openexchange.office.filter.ooxml.docx.components.TextRun_Base;
import com.openexchange.office.filter.ooxml.docx.tools.Character;
import com.openexchange.office.filter.ooxml.docx.tools.Paragraph;
import com.openexchange.office.filter.ooxml.docx.tools.Table;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;
import com.openexchange.office.filter.ooxml.operations.CreateOperationHelper;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class DocxCreateOperationHelper extends CreateOperationHelper {

    static final private ImmutableMap<String, String> conditionalTableAttrNames = ImmutableMap.<String, String> builder()
        .put("firstRow", OCKey.FIRST_ROW.value())
        .put("lastRow", OCKey.LAST_ROW.value())
        .put("firstCol", OCKey.FIRST_COL.value())
        .put("lastCol", OCKey.LAST_COL.value())
        .put("band1Vert", OCKey.BAND1_VERT.value())
        .put("band2Vert", OCKey.BAND2_VERT.value())
        .put("band1Horz", OCKey.BAND1_HORZ.value())
        .put("band2Horz", OCKey.BAND2_HORZ.value())
        .put("neCell", OCKey.NORTH_EAST_CELL.value())
        .put("nwCell", OCKey.NORTH_WEST_CELL.value())
        .put("seCell", OCKey.SOUTH_EAST_CELL.value())
        .put("swCell", OCKey.SOUTH_WEST_CELL.value())
        .build();

    private final WordprocessingMLPackage wordprocessingMLPackage;
    private final HashSet<Integer> createdComments = new HashSet<Integer>();

    private class ComplexFieldStackEntry {

    	final FldCharBegin fldCharBegin;

    	public ComplexFieldStackEntry(FldCharBegin component) {
    		fldCharBegin = component;
    	}
    	public FldCharBegin getFldStart() {
    		return fldCharBegin;
    	}
    }

    public DocxCreateOperationHelper(DocxOperationDocument _operationDocument, JSONArray mOperationsArray)
    	throws FilterException {

    	super(_operationDocument, mOperationsArray);
        wordprocessingMLPackage = _operationDocument.getPackage();
    }

    @Override
    public DocxOperationDocument getOperationDocument() {
        return (DocxOperationDocument)operationDocument;
    }

    public void createOperations() throws Exception {

        createOperations(wordprocessingMLPackage.getMainDocumentPart(), new ArrayList<Integer>(), null, new Stack<ComplexFieldStackEntry>());
    }

    public void CreateHeaderFooterOperations() throws Exception {

    	final SectPr sectPr = Utils.getDocumentProperties(wordprocessingMLPackage.getMainDocumentPart(), false);
        if(sectPr!=null) {
        	for(CTRel rel:sectPr.getEGHdrFtrReferences()) {
        		final String id = rel.getId();
        		if(id!=null&&!id.isEmpty()) {
        			final RelationshipsPart relationshipsPart = wordprocessingMLPackage.getMainDocumentPart().getRelationshipsPart(false);
        			if(relationshipsPart!=null) {
        				AddInsertHeaderFooterOperation(id, rel);
        				final Part part = relationshipsPart.getPart(id);
        				if(part instanceof HeaderPart) {
        					final Hdr hdr = ((HeaderPart)part).getJaxbElement();
        					operationDocument.setContextPart(part);
        			        createOperations(hdr, new ArrayList<Integer>(), id, new Stack<ComplexFieldStackEntry>());
        				}
        				else if(part instanceof FooterPart) {
        					final Ftr ftr = ((FooterPart)part).getJaxbElement();
        					operationDocument.setContextPart(part);
        					createOperations(ftr, new ArrayList<Integer>(), id, new Stack<ComplexFieldStackEntry>());
        				}
        			}
        		}
        	}
        	operationDocument.setContextPart(null);
        }
    }

    public void createDocumentDefaults(JSONObject attrs, String userLanguage)
    	throws InvalidFormatException, JSONException {

        final CTSettings settings = getOperationDocument().getSettings(false);
        final SectPr sectPr = Utils.getDocumentProperties(wordprocessingMLPackage.getMainDocumentPart(), false);
        // document defaults
        final JSONObject jsonDocumentSettings = createCoreProperties(attrs);
        if (settings!=null) {
            final BooleanDefaultTrue trackRevisions = settings.getTrackRevisions();
            if(trackRevisions!=null&&trackRevisions.isVal()) {
            	jsonDocumentSettings.put(OCKey.CHANGE_TRACKING.value(), true);
            }
            final CTTwipsMeasure defaultTabStop = settings.getDefaultTabStop();
            if (defaultTabStop != null) {
                jsonDocumentSettings.put(OCKey.DEFAULT_TABSTOP.value(), Utils.mapTwipTo100THMM(defaultTabStop.getVal()));
            }
            final CTZoom zoom = settings.getZoom();
            if (zoom!=null) {
                boolean addZoomAttr = false;
                JSONObject zoomAttributes = new JSONObject();
                if (zoom.getVal() != null) {
                    zoomAttributes.put(OCKey.TYPE.value(), zoom.getVal().toString());
                    addZoomAttr = true;
                }
                if (zoom.getPercent() != null) {
                    zoomAttributes.put(OCKey.VALUE.value(), zoom.getPercent().intValue());
                    addZoomAttr = true;
                }
                if (addZoomAttr) {
                    jsonDocumentSettings.put(OCKey.ZOOM.value(), zoomAttributes);
                }
            }
        }

        // page defaults
        JSONObject jsonPageSettings = new JSONObject();
        if(settings!=null) {
            final BooleanDefaultTrue evenAndOddHeaders = settings.getEvenAndOddHeaders();
            final boolean evenOddPages = evenAndOddHeaders!=null?evenAndOddHeaders.isVal():false;
            if(evenOddPages) {
            	jsonPageSettings.put(OCKey.EVEN_ODD_PAGES.value(), evenOddPages);
            }
        }
        if(sectPr!=null) {
            final PgSz pgSz = sectPr.getPgSz(false);
            if(pgSz!=null) {
                jsonPageSettings.put(OCKey.WIDTH.value(), Utils.mapTwipTo100THMM(pgSz.getW()));
                jsonPageSettings.put(OCKey.HEIGHT.value(), Utils.mapTwipTo100THMM(pgSz.getH()));
                if(pgSz.getOrient()==STPageOrientation.LANDSCAPE) {
                    jsonPageSettings.put(OCKey.ORIENTATION.value(), "landscape");
                }
            }
            final PgMar pgMar = sectPr.getPgMar(false);
            if(pgMar!=null) {
                jsonPageSettings.put(OCKey.MARGIN_LEFT.value(), Utils.mapTwipTo100THMM(pgMar.getLeft()));
                jsonPageSettings.put(OCKey.MARGIN_TOP.value(), Utils.mapTwipTo100THMM(pgMar.getTop()));
                jsonPageSettings.put(OCKey.MARGIN_RIGHT.value(), Utils.mapTwipTo100THMM(pgMar.getRight()));
                jsonPageSettings.put(OCKey.MARGIN_BOTTOM.value(), Utils.mapTwipTo100THMM(pgMar.getBottom()));
                jsonPageSettings.put(OCKey.MARGIN_HEADER.value(), Utils.mapTwipTo100THMM(pgMar.getHeader()));
                jsonPageSettings.put(OCKey.MARGIN_FOOTER.value(), Utils.mapTwipTo100THMM(pgMar.getFooter()));
            }
            final BooleanDefaultTrue titlePg = sectPr.getTitlePg();
            final boolean firstPage = titlePg!=null?titlePg.isVal():false;
            if(firstPage) {
            	jsonPageSettings.put(OCKey.FIRST_PAGE.value(), firstPage);
            }
        }
        if(!jsonPageSettings.isEmpty()) {
            attrs.put(OCKey.PAGE.value(), jsonPageSettings);
        }

        final StyleDefinitionsPart styleDefinitionsPart = getOperationDocument().getStyleDefinitionsPart(false);
        if(styleDefinitionsPart!=null) {
            final Styles styles = styleDefinitionsPart.getJaxbElement();
            final DocDefaults docDefaults = styles.getDocDefaults(false);
            if(docDefaults!=null) {
                final RPrDefault rDefault = docDefaults.getRPrDefault(false);
                if(rDefault!=null) {
                    final RPr rPrDefault = rDefault.getRPr(false);
                    if(rPrDefault!=null) {
                        final JSONObject jsonCharacterSettings = Character.createCharacterProperties(getOperationDocument(), rPrDefault);
                        if(jsonCharacterSettings!=null&&jsonCharacterSettings.length()>0) {

                            // set user language if it doesn't exists within the document
                            String value = jsonCharacterSettings.optString(OCKey.LANGUAGE.value());
                            if (value == null || value.length() == 0) {
                                if (userLanguage != null && userLanguage.length() > 0)
                                    jsonCharacterSettings.put(OCKey.LANGUAGE.value(), userLanguage);
                                else
                                    jsonCharacterSettings.put(OCKey.LANGUAGE.value(), "en-US"); // set default
                            }
                            value = jsonCharacterSettings.optString(OCKey.LANGUAGE_EA.value());
                            if (value == null || value.length() == 0) {
                                jsonCharacterSettings.put(OCKey.LANGUAGE_EA.value(), "en-US"); // set default
                            }
                            value = jsonCharacterSettings.optString(OCKey.LANGUAGE_BIDI.value());
                            if (value == null || value.length() == 0) {
                                jsonCharacterSettings.put(OCKey.LANGUAGE_BIDI.value(), "ar-SA"); // set default
                            }
                            attrs.put(OCKey.CHARACTER.value(), jsonCharacterSettings);
                        }
                    }
                }
                final PPrDefault pDefault = docDefaults.getPPrDefault();
                if(pDefault!=null) {
                    final PPr pPrDefault = pDefault.getPPr();
                    if(pPrDefault!=null) {
                        final JSONObject jsonParagraphSettings = Paragraph.createParagraphProperties(pPrDefault);
                        if(jsonParagraphSettings!=null&&jsonParagraphSettings.length()>0) {
                            attrs.put(OCKey.PARAGRAPH.value(), jsonParagraphSettings);
                        }
                    }
                }
            }
        }
    }

    public void createDocumentAuthors(JSONObject documentAttributes)
        throws JSONException {

        final CTPeople peoples = getOperationDocument().getPeopleList(false);
        if(peoples!=null) {
            final List<CTPerson> peopleList = peoples.getPerson();
            if(!peopleList.isEmpty()) {
                final JSONArray authors = new JSONArray(peopleList.size());
                Integer id = 0;
                for(CTPerson person:peopleList) {
                    final JSONObject author = new JSONObject();
                    author.put(OCKey.AUTHOR_ID.value(), (id++).toString());
                    author.put(OCKey.AUTHOR_NAME.value(), person.getAuthor());
                    final CTPresenceInfo presenceInfo = person.getPresenceInfo(false);
                    if(presenceInfo!=null) {
                        String providerId = presenceInfo.getProviderId();
                        String userId = presenceInfo.getUserId();
                        if("None".equals(providerId) && userId != null && userId.startsWith("ox_")) {
                            providerId = "ox";
                            userId = userId.substring(3);
                        }
                        author.put(OCKey.PROVIDER_ID.value(), providerId);
                        author.put(OCKey.USER_ID.value(), userId);
                    }
                    authors.put(author);
                }
                documentAttributes.put(OCKey.AUTHORS.value(), authors);
            }
        }
    }

    /**
     * @param aOperationsArray
     * @param nPara
     */
    public void AddInsertParagraphOperation(final List<Integer> start, String target, final JSONObject attrs)
        throws JSONException {

        final JSONObject insertParagraphObject = new JSONObject(4);
        insertParagraphObject.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        insertParagraphObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertParagraphObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
        	insertParagraphObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertParagraphObject);
    }

    /**
     * @param aOperationsArray
     * @param nPara
     * @param nPos
     * @param aText
     */
    public void AddInsertTextOperation(List<Integer> paragraphPosition, int textPosition, String target, TextComponent textComponent, JSONObject attrs)
        throws JSONException {

        String text = ((IText)textComponent.getObject()).getValue();
        if(!operationsArray.isEmpty()) {
            final JSONObject lastOperation = operationsArray.getJSONObject(operationsArray.length() - 1);
            if(lastOperation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_TEXT.value())) {
                final JSONArray lastStart = lastOperation.getJSONArray(OCKey.START.value());
                if(lastStart.length() == paragraphPosition.size() + 1) {
                    if(tryCombineTextOperation(lastOperation, text, attrs)) {
                        return;
                    }
                }
            }
        }

        final JSONObject insertTextObject = new JSONObject(4);
        List<Integer> startArray = new ArrayList<Integer>(paragraphPosition);
        startArray.add(textPosition);

        insertTextObject.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        insertTextObject.put(OCKey.START.value(), startArray);
        if(target!=null) {
        	insertTextObject.put(OCKey.TARGET.value(), target);
        }
        insertTextObject.put(OCKey.TEXT.value(), text);
        if(attrs!=null && !attrs.isEmpty()) {
            insertTextObject.put(OCKey.ATTRS.value(), attrs );
        }
        operationsArray.put(insertTextObject);
    }

    public void AddInsertHeaderFooterOperation(String id, CTRel rel)
    	throws JSONException {

    	final JSONObject headerFooterOperation = new JSONObject(4);
    	headerFooterOperation.put(OCKey.NAME.value(), OCValue.INSERT_HEADER_FOOTER.value());
    	headerFooterOperation.put(OCKey.ID.value(), id);
    	if(rel instanceof HeaderReference) {
			headerFooterOperation.put(OCKey.TYPE.value(), "header_" + ((HeaderReference)rel).getType().value());
    	}
		else if (rel instanceof FooterReference) {
			headerFooterOperation.put(OCKey.TYPE.value(), "footer_" + ((FooterReference)rel).getType().value());
    	}
    	operationsArray.put(headerFooterOperation);
    }

    // returns true if a oncoming insert text operation was combined with the last operation
    private boolean tryCombineTextOperation(JSONObject lastOperation, String text, JSONObject attrs)
        throws JSONException {

        final boolean hasAttrs = attrs != null && !attrs.isEmpty();

        /* it is not necessary to check target because operations are in order and the first insertText
           operation of a new target is always preceded by a insertParagraph operation */
        if(getOperationDocument().isCreateFastLoadOperations()) {
            final JSONObject lastAttrs = lastOperation.optJSONObject(OCKey.ATTRS.value());
            final boolean hasLastAttrs = lastAttrs != null && !lastAttrs.isEmpty();
            if(hasLastAttrs != hasAttrs) {
                return false;
            }
            if(hasLastAttrs) {
                // fastload text operations can only be combined if attrs has same attributes as used in the previous text operation
                if(!lastAttrs.isEqualTo(attrs)) {
                    return false;
                }
            }
        }
        else if(hasAttrs) { // without fastload attrs contains differences, so the operations can be combined if there are no attrs
            return false;
        }
        // combine text operations:
        lastOperation.put(OCKey.TEXT.value(), lastOperation.getString(OCKey.TEXT.value()) + text);
        return true;
    }

    public void AddInsertTabOperation(final List<Integer> start, String target, JSONObject attrs)
        throws JSONException {

        final JSONObject insertTabObject = new JSONObject(3);
        insertTabObject.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        insertTabObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertTabObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null) {
            insertTabObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertTabObject);
    }

    public void AddInsertCommentOperation(final List<Integer> start, String target, Comment comment, JSONObject attrs)
        throws JSONException {

        final JSONObject insertComment = Utils.createJSONFromTrackInfo(getOperationDocument(), comment);
        insertComment.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
        insertComment.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertComment.put(OCKey.TARGET.value(), target);
        }

        final String operationId = DocxOperationDocument.getCommentOperationId(comment.getId());
        insertComment.put(OCKey.ID.value(), operationId);

        final Integer parentId = comment.getParentId();
        if(parentId!=null) {
            final Comment parent = getOperationDocument().getComment(parentId, false);
            if(parent!=null) {
                insertComment.put(OCKey.PARENT_ID.value(), DocxOperationDocument.getCommentOperationId(parentId));
            }
        }

        final JSONArray mentions = new JSONArray();
        final MutableBoolean restorable = new MutableBoolean(true);
        insertComment.put(OCKey.TEXT.value(), getSimpleTextAndMentionsFromComment(comment, mentions, new Stack<ComplexFieldStackEntry>(), restorable));
        if(!restorable.booleanValue()) {
            insertComment.put(OCKey.RESTORABLE.value(), false);
        }
        if(!mentions.isEmpty()) {
            insertComment.put(OCKey.MENTIONS.value(), mentions);
        }
        if(attrs!=null) {
            insertComment.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertComment);
    }

    public String getSimpleTextAndMentionsFromComment(Comment comment, JSONArray mentions, Stack<ComplexFieldStackEntry> complexFieldStack, MutableBoolean restorable)
        throws JSONException {

        final StringBuilder text = new StringBuilder();
        if(!createdComments.contains(comment.getId())) {
            final Part oldContextPart = operationDocument.getContextPart();
            operationDocument.setContextPart(getOperationDocument().getCommentsPart(false));

            // iterate over the components of the comment
            final RootComponent rootComponent = new RootComponent(getOperationDocument(), comment);
            IComponent<OfficeOpenXMLOperationDocument> component = rootComponent.getNextChildComponent(null, null);
            boolean firstParagraph = true;
            while (component!=null) {
                // we are only interested in paragraphs, tables and other components are ignored
                if(component instanceof ParagraphComponent) {
                    appendSimpleTextAndMentionsFromParagraph(text, (ParagraphComponent)component, mentions, complexFieldStack, firstParagraph, restorable);
                }
                else {
                    restorable.setFalse(); // everything else seems to be created with versions prior Word 365
                }
                firstParagraph = false;
                component = component.getNextComponent();
            }

            operationDocument.setContextPart(oldContextPart);
            createdComments.add(comment.getId());
        }
        return text.toString();
    }

    private void appendSimpleTextAndMentionsFromParagraph(StringBuilder builder, ParagraphComponent paragraphComponent, JSONArray mentions, Stack<ComplexFieldStackEntry> complexFieldStack, boolean firstParagraph, MutableBoolean restorable)
        throws JSONException {

        // insert paragraph and apply paragraph attributes
        if(!firstParagraph) {
            builder.append('\n');   // new line except for the first paragraph
        }

        // insert components (text and or other drawing objects)
        IComponent<OfficeOpenXMLOperationDocument> component = paragraphComponent.getNextChildComponent(null, null);
        while(component!=null) {
            if (component instanceof TextComponent) {
                builder.append(((IText)component.getObject()).getValue());
            }
            else if(component instanceof FldCharBegin) {
                    component.setComponentNumber(builder.length());
                    complexFieldStack.push(new ComplexFieldStackEntry((FldCharBegin)component));
            }
            else if(component instanceof FldCharEnd) {
                if(!complexFieldStack.isEmpty()) {
                    final FldCharBegin fldStart = complexFieldStack.pop().getFldStart();
                    final String instruction = fldStart.getInstruction().trim();
                    if(instruction.startsWith("HYPERLINK")) {
                        final int startIndex = instruction.indexOf("\"mailto:");
                        final int endIndex = instruction.lastIndexOf('"');
                        if(startIndex!=-1 && endIndex!=-1) {
                            final String eMail = instruction.substring(startIndex + 8, endIndex);
                            if(!eMail.isEmpty() && eMail.indexOf('"') == -1) {
                                final JSONObject mention = new JSONObject();
                                final int position = fldStart.getComponentNumber();
                                mention.put(OCKey.POS.value(), position);
                                mention.put(OCKey.LENGTH.value(), builder.length() - position);
                                mention.put(OCKey.EMAIL.value(), eMail);
                                mentions.put(mention);
                            }
                        }
                    }
                }
            }
            else if (component instanceof FldCharSeparate || component instanceof BookmarkStartComponent || component instanceof BookmarkEndComponent) {
                //
            }
            else {
                restorable.setFalse(); // everything else seems to be created with versions prior Word 365
            }
            component = component.getNextComponent();
        }
    }

    public void AddInsertCommentRangeOperation(final List<Integer> start, String target, Integer commentId, String type, boolean startRange, JSONObject attrs)
        throws JSONException {

        final JSONObject insertRangeObject = new JSONObject(6);
        insertRangeObject.put(OCKey.NAME.value(), OCValue.INSERT_RANGE.value());
        insertRangeObject.put(OCKey.START.value(), start);
        insertRangeObject.put(OCKey.TYPE.value(), type);
        if(target!=null) {
        	insertRangeObject.put(OCKey.TARGET.value(), target);
        }
        insertRangeObject.put(OCKey.POSITION.value(), startRange ? "start" : "end");
        final Comment comment = getOperationDocument().getComment(commentId, false);
        insertRangeObject.put(OCKey.ID.value(), comment != null ? DocxOperationDocument.getCommentOperationId(comment.getId()) : "");
        if(attrs!=null) {
            insertRangeObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertRangeObject);
    }

    public void AddInsertBookmarkOperation(final List<Integer> start, String target, Integer id, String anchorName, boolean bookmarkStart, JSONObject attrs)
        throws JSONException {

        final JSONObject insertBookmarkObject = new JSONObject(6);
        insertBookmarkObject.put(OCKey.NAME.value(), OCValue.INSERT_BOOKMARK.value());
        insertBookmarkObject.put(OCKey.START.value(), start);
        if(anchorName!=null) {
            insertBookmarkObject.put(OCKey.ANCHOR_NAME.value(), anchorName);
        }
        if(target!=null) {
            insertBookmarkObject.put(OCKey.TARGET.value(), target);
        }
        insertBookmarkObject.put(OCKey.POSITION.value(), bookmarkStart ? "start" : "end");
        insertBookmarkObject.put(OCKey.ID.value(), "bm" + id.toString());
        insertBookmarkObject.put(OCKey.ATTRS.value(), attrs);
        operationsArray.put(insertBookmarkObject);
    }

    // TODO: complex fields within multi components ...
    public void AddInsertComplexFieldOperation(final List<Integer> start, String target, ComplexFieldStackEntry fieldStackEntry, JSONObject attrs)
        throws JSONException {

        final JSONObject insertComplexField = new JSONObject(4);
        insertComplexField.put(OCKey.NAME.value(), OCValue.INSERT_COMPLEX_FIELD.value());
        insertComplexField.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertComplexField.put(OCKey.TARGET.value(), target);
        }
        if(fieldStackEntry!=null) {
	        insertComplexField.put(OCKey.INSTRUCTION.value(), fieldStackEntry.getFldStart().getInstruction());
        }
        if(attrs!=null) {
            insertComplexField.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertComplexField);
    }

    public void AddInsertComplexFieldRangeOperation(final List<Integer> start, String target, boolean startRange, JSONObject attrs)
        throws JSONException {

        final JSONObject insertCommentRangeObject = new JSONObject(5);
        insertCommentRangeObject.put(OCKey.NAME.value(), OCValue.INSERT_RANGE.value());
        insertCommentRangeObject.put(OCKey.START.value(), start);
        insertCommentRangeObject.put(OCKey.TYPE.value(), "field");
        if(target!=null) {
        	insertCommentRangeObject.put(OCKey.TARGET.value(), target);
        }
        insertCommentRangeObject.put(OCKey.POSITION.value(), startRange?"start":"end");
        if(attrs!=null) {
            insertCommentRangeObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertCommentRangeObject);
    }

    public void AddInsertHardBreakOperation(List<Integer> start, String target, STBrType type, JSONObject attrs)
        throws JSONException {

        final JSONObject insertHardBreakObject = new JSONObject(4);
        insertHardBreakObject.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        insertHardBreakObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertHardBreakObject.put(OCKey.TARGET.value(), target);
        }
        if(type!=null) {
        	insertHardBreakObject.put(OCKey.TYPE.value(), type.value());
        }
        if(attrs!=null) {
            insertHardBreakObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertHardBreakObject);
    }

    public boolean AddInsertTableOperation(final List<Integer> start, String target, final JSONObject attrs)
        throws JSONException {

    	boolean tableSizeAllowed = true;
    	final JSONObject insertTableObject = new JSONObject(5);
        insertTableObject.put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value());
        insertTableObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertTableObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            if(attrs.has(OCKey.SIZE_EXCEEDED.value())) {
            	tableSizeAllowed = false;
            	insertTableObject.put(OCKey.SIZE_EXCEEDED.value(), attrs.getJSONObject(OCKey.SIZE_EXCEEDED.value()));
            	attrs.remove(OCKey.SIZE_EXCEEDED.value());
            }
        	insertTableObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertTableObject);
        return tableSizeAllowed;
    }

    public void AddInsertRowsOperation(List<Integer> start, String target, JSONObject attrs)
        throws JSONException {

        final JSONObject insertRowObject = new JSONObject(4);
        insertRowObject.put(OCKey.NAME.value(), OCValue.INSERT_ROWS.value());
        insertRowObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertRowObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            insertRowObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertRowObject);
    }

    public void AddInsertCellsOperation(List<Integer> start, String target, JSONObject attrs)
        throws JSONException {

        final JSONObject insertCellObject = new JSONObject(4);
        insertCellObject.put(OCKey.NAME.value(), OCValue.INSERT_CELLS.value());
        insertCellObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertCellObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            insertCellObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertCellObject);
    }

    public void AddInsertDrawingOperation(final List<Integer> start, String target, ShapeType graphicType, final JSONObject attrs)
        throws JSONException {

        final JSONObject insertDrawingObject = new JSONObject(5);
        insertDrawingObject.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        insertDrawingObject.put(OCKey.START.value(), start);
        insertDrawingObject.put(OCKey.TYPE.value(), graphicType.name().toLowerCase());
        if(target!=null) {
        	insertDrawingObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            insertDrawingObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertDrawingObject);
    }

    public void AddInsertFontDescription(String name, JSONObject attrs)
        throws JSONException {

        if(name!=null&&name.length()>0&&attrs!=null&&attrs.length()>0) {
            final JSONObject fontDescription = new JSONObject(3);
            fontDescription.put(OCKey.NAME.value(), OCValue.INSERT_FONT_DESCRIPTION.value());
            fontDescription.put(OCKey.FONT_NAME.value(), name);
            fontDescription.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(fontDescription);
        }
    }

    public void CreateFontDescriptions()
        throws JSONException {

        FontTablePart fontTablePart = (FontTablePart)wordprocessingMLPackage.getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.FONT_TABLE);
        if(fontTablePart!=null) {
            org.docx4j.wml.Fonts fonts = fontTablePart.getJaxbElement();
            List<Fonts.Font> fontList = fonts.getFont();
            for(Fonts.Font font: fontList) {
                JSONObject attrs = new JSONObject();
                AltName altNames = font.getAltName();
                if(altNames!=null) {
                    String names = altNames.getVal();
                    if(names!=null) {
                        JSONArray jsonAltNames = new JSONArray();
                        String[] tokens = names.split(",");
                        for(String token : tokens)
                            Commons.jsonPut(jsonAltNames, token);
                        Commons.jsonPut(attrs, OCKey.ALT_NAMES.value(), jsonAltNames);
                    }
                }
                FontFamily fontFamily = font.getFamily();
                if(fontFamily!=null)
                    Commons.jsonPut(attrs, OCKey.FAMILY.value(), fontFamily.getVal());
                FontPitch fontPitch = font.getPitch();
                if(fontPitch!=null) {
                    STPitch stPitch = fontPitch.getVal();
                    if(stPitch!=STPitch.DEFAULT)
                        Commons.jsonPut(attrs, OCKey.PITCH.value(), stPitch.toString().toLowerCase());
                }
                FontPanose fontPanose = font.getPanose1();
                if(fontPanose!=null) {
                    byte[] panose = fontPanose.getVal();
                    if(panose.length==10) {
                        JSONArray jsonPanose = new JSONArray();
                        for(byte entry : panose)
                            jsonPanose.put(entry);
                        Commons.jsonPut(attrs, OCKey.PANOSE1.value(), jsonPanose);
                    }
                }
                AddInsertFontDescription(font.getName(), attrs);
            }
        }
    }

    private void CreateConditionalTableStyle(JSONObject attrs, String conditionalTableStyle, CTTblPrBase tblPr, TrPr trPr, TcPr tcPr, PPr pPr, RPr rPr)
        throws JSONException {

        final JSONObject tableStyle = new JSONObject();
        Table.createTableProperties(wordprocessingMLPackage, tblPr, null, true, 0, tableStyle);
        Table.createRowProperties(trPr, tableStyle);
        Table.createCellProperties(tcPr, null, tableStyle);
        final JSONObject paragraphProperties = Paragraph.createParagraphProperties(pPr);
        if(paragraphProperties!=null) {
        	tableStyle.put(OCKey.PARAGRAPH.value(), paragraphProperties);
        }
        final JSONObject characterProperties = Character.createCharacterProperties(getOperationDocument(), rPr);
        if(characterProperties!=null&&!characterProperties.isEmpty()) {
        	tableStyle.put(OCKey.CHARACTER.value(), characterProperties);
        }
        if(!tableStyle.isEmpty()) {
            attrs.put(conditionalTableStyle, tableStyle);
        }
    }

    public void CreateStyleOperations()
        throws JSONException {

        final StyleDefinitionsPart styleDefinitionsPart = getOperationDocument().getStyleDefinitionsPart(false);
        if(styleDefinitionsPart!=null) {
            final Styles styles = styleDefinitionsPart.getJaxbElement();
            final List< Style > lStyle = styles.getStyle();
            for(Style style : lStyle) {
                String styleId = null;
                String styleName = null;
                String type = style.getType();
                Name name = style.getName();
                if(name!=null)
                    styleName = name.getVal();
                styleId = style.getStyleId();
                if(type!=null&&styleId!=null&&styleId.length()>0) {
                    String parentId = null;
                    Style.BasedOn basedOn = style.getBasedOn();
                    if (basedOn!=null)
                        parentId = basedOn.getVal();

                    Integer uipriority = null;
                    Boolean hidden = null;
                    Boolean isDefault = style.isDefault();
                    BooleanDefaultTrue defaultTrueHidden = style.getHidden();
                    Boolean custom = style.isCustomStyle();
                    if (defaultTrueHidden!=null)
                        hidden = defaultTrueHidden.isVal();
                    if(hidden==null||hidden==false)
                    {
                        UiPriority prio = style.getUiPriority();
                        if(prio!=null)
                            uipriority = prio.getVal().intValue();
                    }

                    RPr rPr = null;
                    PPr pPr = null;
                    if(type.equals("character")) {
                        rPr = style.getRPr(false);
                        JSONObject attrs = new JSONObject();
                        if (rPr!=null) {
                            JSONObject character = Character.createCharacterProperties(getOperationDocument(), rPr);
                            attrs.put(OCKey.CHARACTER.value(), character);
                        }
                        operationsArray.put(createInsertStyleSheetOperation("character", false, styleId, styleName, attrs, parentId, hidden, uipriority, isDefault, custom));
                    }
                    else if (type.equals("paragraph")) {
                        rPr = style.getRPr(false);
                        pPr = style.getPPr(false);
                        JSONObject attrs = new JSONObject();

                        if(rPr!=null) {
                            JSONObject character = Character.createCharacterProperties(getOperationDocument(), rPr);
                            attrs.put(OCKey.CHARACTER.value(), character);
                        }
                        JSONObject paragraph = null;
                        if(pPr!=null) {
                            paragraph = Paragraph.createParagraphProperties(pPr);
                        }
                        Next nextStyle = style.getNext();
                        if(nextStyle!=null) {
                            String nextStyleId = nextStyle.getVal();
                            if(nextStyleId!=null&&nextStyleId.length()>0) {
                                if (paragraph==null) {
                                    paragraph = new JSONObject();
                                }
                                paragraph.put(OCKey.NEXT_STYLE_ID.value(), nextStyleId);
                            }
                        }
                        if (paragraph!=null) {
                            attrs.put(OCKey.PARAGRAPH.value(), paragraph);
                        }
                        operationsArray.put(createInsertStyleSheetOperation("paragraph", false, styleId, styleName, attrs, parentId, hidden, uipriority, isDefault, custom));
                    }
                    else if (type.equals("table")) {
                        JSONObject attrs = new JSONObject();
                        CreateConditionalTableStyle(attrs, OCKey.WHOLE_TABLE.value(), style.getTblPr(true), style.getTrPr(false), style.getTcPr(false), style.getPPr(false), style.getRPr(false));
                        List<CTTblStylePr> conditionalStyles = style.getTblStylePr();
                        for(CTTblStylePr conditionalStyle : conditionalStyles) {
                            STTblStyleOverrideType conditionalType = conditionalStyle.getType();
                            String attrName = conditionalType.value();
                            if(conditionalTableAttrNames.containsKey(attrName))
                                attrName = conditionalTableAttrNames.get(attrName);
                            CreateConditionalTableStyle(attrs, attrName, conditionalStyle.getTblPr(false), conditionalStyle.getTrPr(false), conditionalStyle.getTcPr(false), conditionalStyle.getPPr(false), conditionalStyle.getRPr(false));
                        }
                        operationsArray.put(createInsertStyleSheetOperation("table", false, styleId, styleName, attrs, parentId, hidden, uipriority, isDefault, custom));
                    }
                }
            }
        }
    }

    private void createOperations(IContentAccessor<Object> parent, List<Integer> parentPosition, String target, Stack<ComplexFieldStackEntry> complexFieldStack)
        throws Exception {

        final RootComponent rootComponent = new RootComponent(getOperationDocument(), parent);
        IComponent<OfficeOpenXMLOperationDocument> component = rootComponent.getNextChildComponent(null, null);
        while (component!=null) {
        	if(component instanceof ParagraphComponent) {
        	    createParagraphOperations((ParagraphComponent)component, parentPosition, target, complexFieldStack, null);
            }
            else if(component instanceof TableComponent) {
                createTableOperations((TableComponent)component, parentPosition, target, complexFieldStack);
            }
        	component = component.getNextComponent();
        }
    }

    private void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> parentPosition, String target, Stack<ComplexFieldStackEntry> complexFieldStack, JSONObject drawingCharacterAttributes)
        throws Exception {

        // insert paragraph and apply paragraph attributes
        final List<Integer> paragraphPosition = new ArrayList<Integer>(parentPosition);
        paragraphPosition.add(paragraphComponent.getComponentNumber());
        AddInsertParagraphOperation(paragraphPosition, target, mergeCharacterAttributes(paragraphComponent.createJSONAttrs(new JSONObject()), drawingCharacterAttributes));

        // insert components (text and or other drawing objects)
        final boolean isFastLoad = getOperationDocument().isCreateFastLoadOperations();
        JSONObject currentAttrs = new JSONObject();
        IComponent<OfficeOpenXMLOperationDocument> component = paragraphComponent.getNextChildComponent(null, null);
        while(component!=null) {
            JSONObject attrs = isFastLoad
                ? mergeCharacterAttributes(((OfficeOpenXMLComponent)component).createJSONAttrs(new JSONObject()), drawingCharacterAttributes)   // fastload
                : createDifferenceAttrs(currentAttrs, (OfficeOpenXMLComponent)component, drawingCharacterAttributes);

            if (component instanceof TextComponent) {
                AddInsertTextOperation(paragraphPosition, component.getComponentNumber(), target, (TextComponent)component, attrs);
            }
            else {
                final List<Integer> startPosition = new ArrayList<Integer>(paragraphPosition);
                startPosition.add(component.getComponentNumber());
            	if (component instanceof ShapeComponent_Root){
                    createDrawingOperations(component, target, paragraphPosition, attrs);
                }
                else if (component instanceof TabComponent) {
                    AddInsertTabOperation(startPosition, target, attrs);
                }
                else if (component instanceof HardBreakComponent) {
                    AddInsertHardBreakOperation(startPosition, target, ((Br)component.getObject()).getType(), attrs);
                }
                else if(component instanceof CommentRangeStartComponent) {
                    if(isFastLoad && component.getComponentNumber()==0) {
                        // get attributes of first component that is supporting textrun attributes
                        IComponent<OfficeOpenXMLOperationDocument> componentTextRuntIter = component.getNextComponent();
                        while(componentTextRuntIter!=null) {
                            if(componentTextRuntIter instanceof TextRun_Base) {
                                attrs = mergeCharacterAttributes(((OfficeOpenXMLComponent)componentTextRuntIter).createJSONAttrs(new JSONObject()), drawingCharacterAttributes);
                                break;
                            }
                            componentTextRuntIter = componentTextRuntIter.getNextComponent();
                        }
                    }
                    AddInsertCommentRangeOperation(startPosition, target, ((CommentRangeStart)component.getNode().getData()).getId(), "comment", true, attrs);
                }
                else if(component instanceof CommentRangeEndComponent) {
                    AddInsertCommentRangeOperation(startPosition, target, ((CommentRangeEnd)component.getNode().getData()).getId(), "comment", false, attrs);
                }
                else if(component instanceof CommentReferenceComponent) {
                	final Integer id = ((R.CommentReference)component.getNode().getData()).getId();
                	final Comment comment = getOperationDocument().getComment(id, false);
                	AddInsertCommentOperation(startPosition, target, comment, attrs);
                }
                else if(component instanceof BookmarkStartComponent) {
                    AddInsertBookmarkOperation(startPosition, target, ((CTBookmark)component.getNode().getData()).getId(), ((CTBookmark)component.getNode().getData()).getName(), true, attrs);
                }
                else if(component instanceof BookmarkEndComponent) {
                    AddInsertBookmarkOperation(startPosition, target, ((CTMarkupRange)component.getNode().getData()).getId(), null, false, attrs);
                }
                else if(component instanceof FldCharBegin) {
                    complexFieldStack.push(new ComplexFieldStackEntry((FldCharBegin)component));
                    AddInsertComplexFieldRangeOperation(startPosition, target, true, attrs);
                }
                else if(component instanceof FldCharSeparate) {
                    ComplexFieldStackEntry fieldStackEntry = null;
                    if(!complexFieldStack.isEmpty()) {
                        fieldStackEntry = complexFieldStack.peek();
                    }
                    AddInsertComplexFieldOperation(startPosition, target, fieldStackEntry, attrs);
                }
                else if(component instanceof FldCharEnd) {
                    if(!complexFieldStack.isEmpty()) {
                        complexFieldStack.pop();
                    }
                    AddInsertComplexFieldRangeOperation(startPosition, target, false, attrs);
                }
                else {
                    AddInsertDrawingOperation(startPosition, target, ShapeType.UNDEFINED, attrs);
                }
            }
            component = component.getNextComponent();
        }
    }

    private JSONObject mergeCharacterAttributes(JSONObject attrs, JSONObject drawingChraracterAttributes) throws JSONException {
        if(drawingChraracterAttributes!=null) {
            JSONObject destJSONCharacterAttributes = attrs.optJSONObject(OCKey.CHARACTER.value());
            if(destJSONCharacterAttributes==null) {
                destJSONCharacterAttributes = new JSONObject();
                attrs.put(OCKey.CHARACTER.value(), destJSONCharacterAttributes);
            }
            final Iterator<Entry<String, Object>> iter = drawingChraracterAttributes.entrySet().iterator();
            while(iter.hasNext()) {
                final Entry<String, Object> entrySet = iter.next();
                if(!destJSONCharacterAttributes.has(entrySet.getKey())) {
                    destJSONCharacterAttributes.put(entrySet.getKey(), entrySet.getValue());
                }
            }
        }
        return attrs;
    }

    // -------------------------------------------------------------------------

    private JSONObject createDifferenceAttrs(JSONObject currentAttrs, OfficeOpenXMLComponent component, JSONObject drawingCharacterAttributes) throws JSONException, Exception {
        if(component instanceof ShapeComponent_Root) {
            return component.createJSONAttrs(new JSONObject()); // no attribute difference for drawing objects
        }
        JSONObject componentAttrs = component.createJSONAttrs(new JSONObject());
        if(component.getComponentNumber() == 0) {
            componentAttrs = mergeCharacterAttributes(componentAttrs, drawingCharacterAttributes);
        }
        return createDifference(currentAttrs, componentAttrs);
    }

    // -------------------------------------------------------------------------

    private void createDrawingOperations(IComponent<OfficeOpenXMLOperationDocument> component, String target, List<Integer> parentPosition, JSONObject attrs) throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(component.getComponentNumber());
        AddInsertDrawingOperation(position, target, ((IShapeType)component).getType(), attrs);

        Stack<ComplexFieldStackEntry> complexFieldStack = null;
        final JSONObject drawingCharacterAttributes = component instanceof IDrawingCharacterAttributes ? ((IDrawingCharacterAttributes)component).createDrawingCharacterAttributes() : null;

        IComponent<OfficeOpenXMLOperationDocument> childComponent = component.getNextChildComponent(null, null);
        while(childComponent!=null) {
            final IComponent<OfficeOpenXMLOperationDocument> c = childComponent instanceof MultiComponent ? ((MultiComponent)childComponent).getComponentList().get(0) : childComponent;
	    	if(c instanceof ShapeComponent) {
	    		createDrawingOperations(c, target, position, ((OfficeOpenXMLComponent)c).createJSONAttrs(new JSONObject()));
	    	}
	    	else if(c instanceof ParagraphComponent) {
	    	    if(complexFieldStack==null) {
	    	        complexFieldStack = new Stack<ComplexFieldStackEntry>();
	    	    }
                createParagraphOperations((ParagraphComponent)c, position, target, complexFieldStack, drawingCharacterAttributes);
	    	}
			else if(c instanceof TableComponent) {
                if(complexFieldStack==null) {
                    complexFieldStack = new Stack<ComplexFieldStackEntry>();
                }
                createTableOperations((TableComponent)c, position, target, complexFieldStack);
			}
	    	childComponent = childComponent.getNextComponent();
        }
    }

    private void createTableOperations(TableComponent tableComponent, List<Integer> parentPosition, String target, Stack<ComplexFieldStackEntry> complexFieldStack)
        throws Exception {

        final List<Integer> tablePosition = new ArrayList<Integer>(parentPosition);
        tablePosition.add(tableComponent.getComponentNumber());
        final JSONObject attrs = tableComponent.createJSONAttrs(new JSONObject());
        if(AddInsertTableOperation(tablePosition, target, attrs)) {
            IComponent<OfficeOpenXMLOperationDocument> rowComponent = tableComponent.getNextChildComponent(null, null);
            while(rowComponent!=null) {
                final List<Integer> rowPosition = new ArrayList<Integer>(tablePosition);
                rowPosition.add(rowComponent.getComponentNumber());
                AddInsertRowsOperation(rowPosition, target, ((OfficeOpenXMLComponent)rowComponent).createJSONAttrs(new JSONObject()));
                IComponent<OfficeOpenXMLOperationDocument> cellComponent = rowComponent.getNextChildComponent(null, null);
                while(cellComponent!=null) {
                    final List<Integer> cellPosition = new ArrayList<Integer>(rowPosition);
                    cellPosition.add(cellComponent.getComponentNumber());
                    AddInsertCellsOperation(cellPosition, target, ((OfficeOpenXMLComponent)cellComponent).createJSONAttrs(new JSONObject()));
                    createOperations((Tc)cellComponent.getNode().getData(), cellPosition, target, complexFieldStack);
                    cellComponent = cellComponent.getNextComponent();
                }
                rowComponent = rowComponent.getNextComponent();
            }
        }
    }

    public void AddInsertListOperation(long listIdentifier, String abstractNumId, JSONObject listDefinition)
        throws JSONException {

        final JSONObject insertListObject = new JSONObject(4);
        insertListObject.put(OCKey.NAME.value(), OCValue.INSERT_LIST_STYLE.value());
        insertListObject.put(OCKey.LIST_STYLE_ID.value(), 'L' + String.valueOf(listIdentifier));
        if(abstractNumId!=null) {
        	insertListObject.put(OCKey.BASE_STYLE_ID.value(), 'A' + abstractNumId);
        }
        insertListObject.put(OCKey.LIST_DEFINITION.value(), listDefinition);
        operationsArray.put(insertListObject);
    }

    /**
     * Puts all list level attributes into the level's JSONObject
     * Is used to load the levels of the abstract numbering as well
     * as for the levels of the LvlOverride
     */
    private void createLevel( JSONObject levelObject, Lvl level, final HashMap<Long, String> picMap )
        throws JSONException {

        Jc jc = level.getLvlJc();//start, end, center, both, ....
        if(jc!=null&&jc.getVal()!=null)
            levelObject.put( OCKey.TEXT_ALIGN.value(), jc.getVal().value() );
        Start start = level.getStart();
        if( start != null )
            levelObject.put(OCKey.LIST_START_VALUE.value(), start.getVal().longValue());
        NumFmt numFmt = level.getNumFmt();
        // contains the numbering type like
        // decimal, upperRoman, lowerRoman, upperLetter, lowerLetter, ordinal, cardinalText, ordinalText
        // hex, ...
        if( numFmt != null && numFmt.getVal() != null){
            NumberFormat numberFormat = numFmt.getVal();
            levelObject.put(OCKey.NUMBER_FORMAT.value(), numberFormat.value());
        }
        LvlText lvlText = level.getLvlText();
        if( lvlText != null )
            levelObject.put(OCKey.LEVEL_TEXT.value(), lvlText.getVal() );

        if( level.getLvlRestart() != null ) // determines whether a sub-level restarts on a 'higher' than the parent level
            levelObject.put(OCKey.LIST_START_VALUE.value(), level.getLvlRestart().getVal().longValue());
        Lvl.PStyle pstyle = level.getPStyle();
        if( pstyle != null ){
            levelObject.put(OCKey.PARA_STYLE.value(), pstyle.getVal());
        }
        Lvl.LvlPicBulletId lvlPicBulletId = level.getLvlPicBulletId();
        if( lvlPicBulletId != null ) {
            long bulletId = lvlPicBulletId.getVal().longValue();
            String picURI = picMap.get( Long.valueOf(bulletId));
            levelObject.put(OCKey.LEVEL_PIC_BULLET_URI.value(), picURI );
        }

        Suff suff = level.getSuff();
        if( suff != null){ //number suffix - could be 'tab', 'space' or 'nothing'
            levelObject.put(OCKey.LABEL_FOLLOWED_BY.value(), suff.getVal().equals("tab") ? "listtab" : suff.getVal());
        }
        PPr paraProperties = level.getPPr();//paragraph properties - indent, tab, ...
        if( paraProperties != null ){
            Ind ind = paraProperties.getInd();
            if( ind != null ){
                Long hanging = ind.getHanging();
                if( hanging != null && hanging.longValue() > 0) //either hanging or firstLine can be set, if hanging is set then firstLine is ignored
                    levelObject.put(OCKey.INDENT_FIRST_LINE.value(), - Utils.mapTwipTo100THMM(hanging) );
                else{
                    Long firstLine = ind.getFirstLine();
                    if( firstLine != null && firstLine.longValue() > 0 )
                        levelObject.put(OCKey.INDENT_FIRST_LINE.value(), Utils.mapTwipTo100THMM(firstLine));
                }
                if(ind.getLeft()!=null) {
                    levelObject.put(OCKey.INDENT_LEFT.value(), Utils.mapTwipTo100THMM(ind.getLeft()));
                }
            }
            Tabs tabs = paraProperties.getTabs();
            if( tabs != null ){
                List<CTTabStop> tabList = tabs.getTab();
                if( tabList != null && !tabList.isEmpty()){
                    // there is only one tab stop
                    levelObject.put( OCKey.TAB_STOP_POSITION.value(), Utils.mapTwipTo100THMM(tabList.get(0).getPos()) );
                }
            }
        }
        RPr rPr = level.getRPr();
        if( rPr != null ){
            /*
            <w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/><w:color w:val="auto"/>
            */
            final RFonts rFonts = rPr.getrFonts(false);
            if( rFonts != null){
                levelObject.put( OCKey.FONT_NAME.value(), rFonts.getAscii());
                //rFonts.getHAnsi();
                //rFonts.getHint();
            }
            final Color color = rPr.getColor(false);
            if(color != null){
                Commons.jsonPut(levelObject, OCKey.COLOR.value(), Utils.createColor(color));
            }
        }
    }

    /**
     * OOXML numbering parts contains two related arrays. One is the array of abstract numberings and on is the mapping from
     * numId to abstractNumId.
     * A numId of zero (assigned to a paragraph) switches the numbering off.
     *
     * The list operations create list definitions for a numId rather than the mapping numId->abstractNumId->definition
     *
     * The mapping also allows for overriding certain levels of the abstract numbering.
     *
     */
    public void CreateListOperations()
        throws JSONException {

        NumberingDefinitionsPart numDefPart = (NumberingDefinitionsPart)wordprocessingMLPackage.getMainDocumentPart().getRelationshipsPart(true).getPartByType(Namespaces.NUMBERING);
        if( numDefPart == null )
            return;
        Numbering numbering = numDefPart.getJaxbElement();
        // load bullet picture mapping
        HashMap<Long, String> picMap = new HashMap<Long, String>();
        List<NumPicBullet> numPicBulletList = numbering.getNumPicBullet();
        if( numPicBulletList != null ){
            Iterator<NumPicBullet> numPicBulletIter = numPicBulletList.iterator();
            while( numPicBulletIter.hasNext() ){
                NumPicBullet numPicBullet = numPicBulletIter.next();
                Pict pict = numPicBullet.getPict();
                if( pict != null ){
                    //
                    CTShape shape = null;
                    Iterator<Object> anyAndAnyIter = pict.getContent().iterator();
                     while( anyAndAnyIter.hasNext() ){
                         Object element = anyAndAnyIter.next();
                         if( element instanceof JAXBElement ) {
                             if( ((JAXBElement<?>)element).getValue().getClass().getName() == "org.docx4j.vml.CTShape"){
                                 shape = (CTShape)((JAXBElement<?>)element).getValue();
                                 break;
                            }
                         }
                         else if( element instanceof CTShape ) {
                             //
                             shape = (CTShape)element;
                             break;
                         }
                     }
                     if( shape != null ){
                         List<Object> shapeElements = shape.getContent();
                         CTImageData imageData = (CTImageData)XmlUtils.findElement(shapeElements, CTImageData.class, "imagedata");
                         if(imageData!=null){
                             String pictRelId = imageData.getId();
                             RelationshipsPart relPart = numDefPart.getRelationshipsPart();
                             Relationship rel = numDefPart.getRelationshipsPart().getRelationshipByID(pictRelId);
                             String imageUrl;
                             try {
                                 imageUrl = URIHelper. resolvePartUri(relPart.getSourceURI(), new URI(rel.getTarget())).getPath();
                                 if(imageUrl.charAt(0)=='/') {
                                     imageUrl = imageUrl.substring(1);
                                 }
                                 picMap.put( new Long( numPicBullet.getNumPicBulletId().longValue() ), imageUrl );
                             } catch (URISyntaxException e) {
                                // TODO Auto-generated catch block
                             }
                         }
                     }
                }
            }
        }


        //abstract numbering container
        HashMap<Long, JSONObject> abstractNumMap = new HashMap<Long, JSONObject>();
        //iterator for abstract definitions
        Iterator<AbstractNum> abstractIter = numbering.getAbstractNum().iterator();
        while( abstractIter.hasNext() ){
            AbstractNum abstractNum = abstractIter.next();
            // abstractNum.getMultiLevelType();//hybridMultilevel, multilevel, singleLevel
            long abstractNumId = abstractNum.getAbstractNumId().longValue();
            NumStyleLink styleLink = abstractNum.getNumStyleLink();
            if(styleLink!=null) {
                String numberingStyleId = styleLink.getVal();
                if(numberingStyleId!=null) {
                    // now trying to get the numbering style ...
                    final Styles styles = getOperationDocument().getStyles(true);
                    for(Style style : styles.getStyle()) {
                        if(style.getType().equals("numbering")) {
                            if(style.getStyleId().equals(numberingStyleId)) {
                                PPr paragraphProperties = style.getPPr(false);
                                if(paragraphProperties==null)
                                    break;
                                NumPr numPr = paragraphProperties.getNumPr(false);
                                if(numPr==null)
                                    break;
                                NumId numId = numPr.getNumId();
                                if(numId==null||numId.getVal()==null)
                                    break;
                                long id = numId.getVal().longValue();

                                // now trying to get the corresponding Numbering
                                for(Num num:numbering.getNum()) {
                                    if(num.getNumId()!=null&&num.getNumId().longValue()==id) {
                                        AbstractNumId abstractId = num.getAbstractNumId();
                                        if(abstractId==null||abstractId.getVal()==null)
                                            break;
                                        long abstractL = abstractId.getVal().longValue();
                                        // now searching for the correct AbstractNum to replace our
                                        // initial abstract numbering who was just a link
                                        for(AbstractNum an:numbering.getAbstractNum()) {
                                            if(an.getAbstractNumId()!=null&&an.getAbstractNumId().longValue()==abstractL) {
                                                abstractNum = an;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            final JSONObject listObject = new JSONObject();
            //now create an operation
            Iterator<Lvl> levelIter = abstractNum.getLvl().iterator();
            for( int index = 0; index < 9; ++index ){
                final JSONObject levelObject = new JSONObject();
                if( levelIter.hasNext() ){
                    Lvl level = levelIter.next();
                    createLevel( levelObject, level, picMap );
                }
                listObject.put(OCKey.LIST_LEVEL.value() + index, levelObject);
            }
            abstractNumMap.put(Long.valueOf(abstractNumId), listObject);
        }
        //
        Iterator<Num> numListIter = numbering.getNum().iterator();
        while( numListIter.hasNext() ){
            Num num = numListIter.next();

            JSONObject listObject = abstractNumMap.get(num.getAbstractNumId().getVal().longValue());
            List<LvlOverride> lvlOverride = num.getLvlOverride();

            if( lvlOverride != null && !lvlOverride.isEmpty()){
                listObject = new JSONObject(listObject.toString());
                Iterator<LvlOverride> lvlIter = lvlOverride.iterator();
                while( lvlIter.hasNext() ){
                    LvlOverride override = lvlIter.next();
                    long ilvlOverride = override.getIlvl().longValue();
                    JSONObject levelObject = listObject.getJSONObject(OCKey.LIST_LEVEL.value() + ilvlOverride);
                    final StartOverride startOverride = override.getStartOverride();
                    if(startOverride!=null) {
                        levelObject.put(OCKey.LIST_OVERRIDE_START_VALUE.value(), startOverride.getVal().longValue());
                    }
                    Lvl level = override.getLvl();
                    if( level != null ){
                        createLevel( levelObject, level, picMap );
                    }
                }
            }
            AddInsertListOperation(num.getNumId().longValue(), num.getAbstractNumId().getVal().toString(), listObject);
        }
    }
}
