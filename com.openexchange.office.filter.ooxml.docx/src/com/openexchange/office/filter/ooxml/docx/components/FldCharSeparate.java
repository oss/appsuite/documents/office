/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.List;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.R;
import org.docx4j.wml.STFldCharType;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.tools.TextRunState;
import com.openexchange.office.filter.ooxml.docx.tools.TextUtils;

public class FldCharSeparate extends FldChar_Base {

    private final FldCharBegin fldCharBegin;

    public FldCharSeparate(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber, FldCharBegin fldCharBegin) {
        super(parentContext, _node, _componentNumber);

        this.fldCharBegin = fldCharBegin;
    }

    public FldCharBegin getFldCharBegin() {
        return fldCharBegin;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        final Object characterAttrs = attrs.opt(OCKey.CHARACTER.value());
        if(characterAttrs instanceof JSONObject) {
            final Object autoDateField = ((JSONObject)characterAttrs).opt(OCKey.AUTO_DATE_FIELD.value());
            if(autoDateField instanceof Boolean) {
                applyAutoDateField((Boolean)autoDateField);
            }
            else {
                applyAutoDateField(true);
            }
        }
        final Object changes = attrs.opt(OCKey.CHANGES.value());
        if(changes!=null&&fldCharBegin!=null) {
            final R start = fldCharBegin.getTextRun();
            final R end = getTextRun();
            // now find each R between start and end and apply changetracking
            Object current = start;
            while(current!=null) {
                current = getNextObject(current);
                if(current instanceof DLNode) {
                    current = ((DLNode<?>)current).getData();
                }
                if(current==end) {
                    break;
                }
                if(current instanceof R) {
                    TextUtils.applyTextRunAttributes(getOperationDocument(), attrs, new TextRunState((R)current));
                }
            }
        }
        super.applyAttrsFromJSON(attrs);
    }

    private static Object getNextObject(Object currentObject) {
        DLNode<Object> next = getNextSibling(currentObject);
        if(next!=null) {
            DLNode<Object> child = null;
            do {
                child = getFirstChild(next);
                if(child!=null) {
                    next = child;
                }
            }
            while(child!=null);

            return next;
        }
        return getParent(currentObject);
    }

    private static DLNode<Object> getNextSibling(Object currentObject) {
        DLNode<Object> currentNode = null;
        if(currentObject instanceof DLNode) {
            currentNode = (DLNode<Object>)currentObject;
            currentObject = currentNode.getData();
        }
        if(!(currentObject instanceof Child)) {
            return null;
        }
        final Object parentObject = ((Child)currentObject).getParent();
        if(!(parentObject instanceof IContentAccessor)) {
            return null;
        }
        final Object content = ((IContentAccessor)parentObject).getContent();
        if(!(content instanceof DLList)) {
            return null;
        }
        if(currentNode==null) {
            currentNode = ((DLList<Object>)content).getNode(((DLList<Object>)content).indexOf(currentObject));
        }
        return currentNode.getNext();
    }

    private static Object getParent(Object currentObject) {
        if(currentObject instanceof DLNode) {
            currentObject = ((DLNode<?>)currentObject).getData();
        }
        if(!(currentObject instanceof Child)) {
            return null;
        }
        final Object parentObject = ((Child)currentObject).getParent();
        if(!(parentObject instanceof IContentAccessor)) {
            return null;
        }
        return parentObject;
    }

    private static DLNode<Object> getFirstChild(Object currentObject) {
        DLNode<Object> currentNode = null;
        if(currentObject instanceof DLNode) {
            currentNode = (DLNode<Object>)currentObject;
            currentObject = currentNode.getData();
        }
        if(!(currentObject instanceof IContentAccessor)) {
            return null;
        }
        final Object content = ((IContentAccessor)currentObject).getContent();
        if(!(content instanceof DLList)) {
            return null;
        }
        return ((DLList)content).getFirstNode();
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {
        if(getComponentNumber()-1>=0) {
            final Object prev = getParentComponent().getChildComponent(getComponentNumber()-1).getObject();
            if(prev instanceof FldChar) {
                JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
                if(characterAttrs==null) {
                    characterAttrs = new JSONObject(1);
                    attrs.put(OCKey.CHARACTER.value(), characterAttrs);
                }
                characterAttrs.put(OCKey.AUTO_DATE_FIELD.value(), !((FldChar)prev).isFldLock());
            }
        }
        super.createJSONAttrs(attrs);
        return attrs;
    }

    @Override
    public void delete(int count) {
        if(fldCharBegin!=null) {
            final List<DLNode<Object>> instructionNodes = fldCharBegin.getInstructionNodes();
            if(instructionNodes!=null) {
                for(int i=instructionNodes.size()-1; i>=0; i--) {
                    final DLNode<Object> instrNode = instructionNodes.get(i);
                    final Child instrText = (Child)instrNode.getData();
                    final Object instrTextParent = instrText.getParent();
                    if(instrTextParent instanceof R&&((R)instrTextParent).getContent().size()==1) { // removing R completely
                        ((IContentAccessor)(((R)instrTextParent).getParent())).getContent().remove(instrTextParent);
                    } else {
                        ((IContentAccessor)(instrText.getParent())).getContent().remove(instrText);
                    }
                }
            }
        }
        super.delete(count);
    }

    private void applyAutoDateField(Boolean val) {
        if(getComponentNumber()-1>=0) {
            final Object prev = getParentComponent().getChildComponent(getComponentNumber()-1).getObject();
            if(prev instanceof FldChar) {
                if(((FldChar)prev).getFldCharType() == STFldCharType.BEGIN) {
                    ((FldChar)prev).setFldLock(val!=null?!val.booleanValue():false);
                }
            }
        }
    }
}
