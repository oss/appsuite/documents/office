/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.List;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.VMLBase;
import com.openexchange.office.filter.ooxml.components.VMLRootComponent;

public class PictVMLComponent extends ShapeComponent_Root {
    // parent can either be a Pict or a CTObject

    public PictVMLComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
		final List<Object> pictContent = ((IContentAccessor)getObject()).getContent();
		if(!pictContent.isEmpty()) {
		    shapeComponent = (OfficeOpenXMLComponent)new VMLRootComponent("text", operationDocument, ((IContentAccessor)getObject())).getNextChildComponent(null, null);
		    if(shapeComponent!=null) {
		        shapeType = ((VMLBase)shapeComponent).getType();
		    }
		}
    }
}
