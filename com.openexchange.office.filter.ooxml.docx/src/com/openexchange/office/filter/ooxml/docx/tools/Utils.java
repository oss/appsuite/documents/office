/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.docx4j.dml.BaseStyles;
import org.docx4j.dml.CTColorScheme;
import org.docx4j.dml.Theme;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.w15.CTPeople;
import org.docx4j.w15.CTPerson;
import org.docx4j.w15.CTPresenceInfo;
import org.docx4j.wml.Body;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTTrackChange;
import org.docx4j.wml.Color;
import org.docx4j.wml.Document;
import org.docx4j.wml.STShd;
import org.docx4j.wml.STThemeColor;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.SectPr.PgMar;
import org.docx4j.wml.SectPr.PgSz;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.json.JSONHelper;

public class Utils {

    public static String themeColorshortName(Theme theme, STThemeColor themeColor) {
        String rgbValue = null;
        if(theme!=null) {
            BaseStyles themeElements = theme.getThemeElements();
            if(themeElements!=null) {
                CTColorScheme clrScheme = themeElements.getClrScheme();
                if(clrScheme!=null) {
                    org.docx4j.dml.CTColor ctColor = null;
                    switch(themeColor) {
                        case ACCENT_1 : ctColor = clrScheme.getAccent1(); break;
                        case ACCENT_2 : ctColor = clrScheme.getAccent2(); break;
                        case ACCENT_3 : ctColor = clrScheme.getAccent3(); break;
                        case ACCENT_4 : ctColor = clrScheme.getAccent4(); break;
                        case ACCENT_5 : ctColor = clrScheme.getAccent5(); break;
                        case ACCENT_6 : ctColor = clrScheme.getAccent6(); break;
                        case BACKGROUND_1 : ctColor = clrScheme.getLt1(); break;
                        case BACKGROUND_2 : ctColor = clrScheme.getLt2(); break;
                        case DARK_1 : ctColor = clrScheme.getDk1(); break;
                        case DARK_2 : ctColor = clrScheme.getDk2(); break;
                        case FOLLOWED_HYPERLINK : ctColor = clrScheme.getFolHlink(); break;
                        case HYPERLINK : ctColor = clrScheme.getHlink(); break;
                        case LIGHT_1 : ctColor = clrScheme.getLt1(); break;
                        case LIGHT_2 : ctColor = clrScheme.getLt2(); break;
                        case TEXT_1 : ctColor = clrScheme.getDk1(); break;
                        case TEXT_2 : ctColor = clrScheme.getDk2(); break;
                        case NONE : break;
                    }
                    if(ctColor!=null) {
                        rgbValue = Commons.ctColorToString(ctColor);
                    }
                }
            }
        }
        return rgbValue;
    }

    public static class TintShadeTransformation {
    	STThemeColor 	themeColor;
    	String 			themeTint;
    	String 			themeShade;
    	String 			color;

    	public TintShadeTransformation(DocxOperationDocument operationDocument, JSONObject jsonColor)
            throws FilterException, JSONException {

            final String type = jsonColor.getString(OCKey.TYPE.value());
            if (type.equals("rgb") && jsonColor.has(OCKey.VALUE.value())) {
                color = jsonColor.getString(OCKey.VALUE.value());
            } else if (type.equals("scheme") && jsonColor.has(OCKey.VALUE.value())) {
                themeColor = STThemeColor.fromValue(jsonColor.getString(OCKey.VALUE.value()));
                color = themeColorshortName(operationDocument.getThemePart(true).getJaxbElement(), themeColor);
            } else if (type.equals("auto")) {
                color = "auto";
            }

            final Double shadeTint = JSONHelper.jsonTransformationsToShadeTint(jsonColor);
            if (shadeTint != null) {
                final int value = (int)Math.round(shadeTint.doubleValue() * 255.0);
                if (value < 0) {
                    themeShade = Integer.toHexString(value + 255);
                } else if (value > 0) {
                    themeTint = Integer.toHexString(255 - value);
                }
            }
        }
    }

    public static void initBorderColorFromJSONColor(DocxOperationDocument operationDocument, JSONObject jsonColor, CTBorder border)
        throws FilterException, JSONException {

    	if(border==null||!jsonColor.has(OCKey.TYPE.value())) {
    		return;
    	}

    	final TintShadeTransformation tintShadeTransformation = new TintShadeTransformation(operationDocument, jsonColor);
    	border.setThemeColor(tintShadeTransformation.themeColor);
        border.setThemeTint(tintShadeTransformation.themeTint);
        border.setThemeShade(tintShadeTransformation.themeShade);
        border.setColor(tintShadeTransformation.color);
    }

    public static void initShdFromJSONColor(DocxOperationDocument operationDocument, JSONObject jsonColor, CTShd shd)
    	throws FilterException, JSONException {

    	if(shd==null||!jsonColor.has(OCKey.TYPE.value())) {
    		return;
    	}
    	final TintShadeTransformation tintShadeTransformation = new TintShadeTransformation(operationDocument, jsonColor);
    	shd.setThemeFill(tintShadeTransformation.themeColor);
    	shd.setThemeFillTint(tintShadeTransformation.themeTint);
    	shd.setThemeFillShade(tintShadeTransformation.themeShade);
    	shd.setFill(jsonColor.optString(OCKey.FALLBACK_VALUE.value(), tintShadeTransformation.color));
        shd.setVal(STShd.CLEAR);
    }

    public static void initColorFromJSONColor(DocxOperationDocument operationDocument, JSONObject jsonColor, Color color)
        throws FilterException, JSONException {

    	if(color==null||!jsonColor.has(OCKey.TYPE.value())) {
    		return;
    	}
    	final TintShadeTransformation tintShadeTransformation = new TintShadeTransformation(operationDocument, jsonColor);
    	color.setThemeColor(tintShadeTransformation.themeColor);
    	color.setThemeTint(tintShadeTransformation.themeTint);
    	color.setThemeShade(tintShadeTransformation.themeShade);
    	color.setVal(tintShadeTransformation.color);
    }

    public static boolean fillJsonColor(JSONObject jsonColor, STThemeColor themeColor, String STHexColor)
        throws JSONException {

        if(themeColor!=null&&themeColor.value()!=null&&themeColor.value().length()>0) {
            jsonColor.put(OCKey.TYPE.value(), "scheme");
            jsonColor.put(OCKey.VALUE.value(), themeColor.value());
        } else if (STHexColor!=null&&STHexColor.length()>0) {
            if(STHexColor.equals("auto"))
                jsonColor.put(OCKey.TYPE.value(), "auto");
            else if(Commons.highlightColorToRgb.containsKey(STHexColor)){
                jsonColor.put(OCKey.TYPE.value(), "rgb");
                jsonColor.put(OCKey.VALUE.value(), Commons.highlightColorToRgb.get(STHexColor));
            }
            else {
                jsonColor.put(OCKey.TYPE.value(), "rgb");
                jsonColor.put(OCKey.VALUE.value(), STHexColor);
            }
        }
        return jsonColor.length()>0;
    }

    public static JSONObject createColor(STThemeColor themeColor, String STHexColor)
        throws JSONException {

        JSONObject jsonColor = new JSONObject();
        return fillJsonColor(jsonColor, themeColor, STHexColor) ? jsonColor : null;
    }

    private static JSONArray createJsonTransformations(final String shade, final String tint) throws JSONException {
    	Double shadeTint = null;
        if (shade != null) {
        	shadeTint = Integer.parseInt(shade, 16) / 255.0 - 1.0;
        } else if (tint != null) {
        	shadeTint = 1.0 - Integer.parseInt(tint, 16) / 255.0;
        }
        return JSONHelper.shadeTintToJsonTransformations(shadeTint);
    }

    public static JSONObject createFillColor(CTShd shd)
        throws JSONException {

        if (shd!=null) {
            JSONObject jsonColor = new JSONObject();
            JSONArray transformations = Utils.createJsonTransformations(shd.getThemeFillShade(), shd.getThemeFillTint());
            if (transformations!=null) {
                jsonColor.put(OCKey.TRANSFORMATIONS.value(), transformations);
            }
            return fillJsonColor(jsonColor, shd.getThemeFill(), shd.getFill()) ? jsonColor : null;
        }
        return null;
    }

    public static JSONObject createColor(Color color)
        throws JSONException {

        if (color!=null) {
            JSONObject jsonColor = new JSONObject();
            JSONArray transformations = Utils.createJsonTransformations(color.getThemeShade(), color.getThemeTint());
            if (transformations!=null) {
                jsonColor.put(OCKey.TRANSFORMATIONS.value(), transformations);
            }
            return fillJsonColor(jsonColor, color.getThemeColor(), color.getVal()) ? jsonColor : null;
        }
        return null;
    }

    public static SectPr getDocumentProperties(MainDocumentPart mainDocPart, boolean createIfMissing) {
        SectPr sectPr = null;
        if(mainDocPart!=null) {
            final Document doc = mainDocPart.getJaxbElement();
            if(doc!=null) {
                final Body body = doc.getBody();
                if(body!=null) {
                    sectPr = body.getSectPr(createIfMissing);
                }
            }
        }
        return sectPr;
    }

    public static long getPageWidth(SectPr sectPr, long defaultWidth) {
    	if(sectPr!=null) {
    	    final PgMar pgMar = sectPr.getPgMar(false);
    	    final PgSz pgSz = sectPr.getPgSz(false);
	        if(pgSz!=null&&pgMar!=null) {
	            long sz = pgSz.getW().longValue();
	            sz -= pgMar.getLeft();
	            sz -= pgMar.getRight();
	            return sz * 2540 / 1440;
	        }
    	}
    	return defaultWidth;
    }

    public static long getPageHeight(SectPr sectPr, long defaultHeight) {
        if(sectPr!=null) {
            final PgMar pgMar = sectPr.getPgMar(false);
            final PgSz pgSz = sectPr.getPgSz(false);
            if(pgSz!=null&&pgMar!=null) {
                long sz = pgSz.getH().longValue();
                sz -= pgMar.getTop();
                sz -= pgMar.getBottom();
                return sz * 2540 / 1440;
            }
        }
        return defaultHeight;
    }

    public static long mapTwipTo100THMM(long longValue) {
        return ((longValue * 2540) / 72) / 20;
    }

    public static Long map100THMMToTwip(long longValue) {
        return Long.valueOf((longValue * 1440) / 2540);
    }

    public static Long map100THMMToTwip(double doubleValue) {
        return Long.valueOf((long)(doubleValue * 1440. /2540.));
    }

    public static boolean equals(final String s1, final String s2) {
        return (s1==null&&s2==null)||((s1!=null)&&s1.equals(s2));
    }

    public static DLList<Object> getContent(Object o) {
    	DLList<Object> r = null;
        if(o instanceof IContentAccessor) {
        	final List<Object> content = ((IContentAccessor)o).getContent();
        	if(content instanceof DLList) {
        		r = (DLList<Object>)content;
        	}
        }
        return r;
    }

    public static JSONObject createJSONFromTrackInfo(com.openexchange.office.filter.ooxml.docx.DocxOperationDocument operationDocument, CTTrackChange changeTrack)
    	throws JSONException {

		final JSONObject trackInfoDesc = new JSONObject(2);
		if(changeTrack!=null) {
			final String author = changeTrack.getAuthor();
			if(author!=null) {
	            operationDocument.getChangeTrackAuthors(true).add(changeTrack.getAuthor());

	            int authorId = 0;
				final CTPeople ctPeople = operationDocument.getPeopleList(true);
				final List<CTPerson> peopleList = ctPeople.getPerson();
				for( ; authorId < peopleList.size(); authorId++) {
				    final CTPerson person = peopleList.get(authorId);
					if(person.getAuthor()!=null&&person.getAuthor().equals(author)) {
						final CTPresenceInfo presenceInfo = person.getPresenceInfo(false);
						if(presenceInfo!=null) {
							final String providerId = presenceInfo.getProviderId();
							final String userId = presenceInfo.getUserId();
							if(providerId!=null&&userId!=null) {
								if(providerId.equals("None")&&userId.startsWith("ox_")) {
								    // trackInfoDesc.put(OCKey.UID.value(), userId.substring(3));
									// trackInfoDesc.put(OCKey.PROVIDER_ID.value(), "ox");
									break; // not required, the authorId is sufficient now
								}
							}
						}
					}
				}
				if(authorId == peopleList.size()) { // the author with correct provider id could not be found, we need to add the author
				    trackInfoDesc.put(OCKey.AUTHOR.value(), author);
				}
				else { // only transport the authorId
                    trackInfoDesc.put(OCKey.AUTHOR_ID.value(), Integer.valueOf(authorId).toString());
				}
			}
			if(changeTrack.getDate()!=null) {
				trackInfoDesc.put(OCKey.DATE.value(), changeTrack.getDate().toXMLFormat());
			}
		}
		return trackInfoDesc;
    }

    public static void applyTrackInfoFromJSON(com.openexchange.office.filter.ooxml.docx.DocxOperationDocument operationDocument, JSONObject json, CTTrackChange changeTrack) {
    	if(json!=null) {
    		applyTrackInfoFromJSON(operationDocument, json.optString(OCKey.AUTHOR_ID.value(), null), json.optString(OCKey.AUTHOR.value(), null), json.optString(OCKey.DATE.value(), null), json.optString(OCKey.USER_ID.value(), null), changeTrack);
    	}
    }

    public static void applyTrackInfoFromJSON(com.openexchange.office.filter.ooxml.docx.DocxOperationDocument operationDocument, String authorId, String author, String date, String uid, CTTrackChange changeTrack) {

        // first apply the changeTrack date
        changeTrack.setDate(stringToXMLGregorianCalendar(date));

        // check if a authorId which can be used as changeTrack author
        if(authorId!=null) {
            final CTPerson person = operationDocument.getAuthorById(authorId);
            if(person!=null) {
                changeTrack.setAuthor(person.getAuthor());
                return; // if a authorId is available the author is still available in the peopleList, no further actions are required
            }
        }

        if (author==null||author.isEmpty()) {
            author = changeTrack.getAuthor(); // no author was given in the operation, try if there is a author available in the given changeTrack
            if(author==null) {
                author = ""; // a author is a required field.. a empty string will provide error if marshaling the document.
            }
        }
        changeTrack.setAuthor(author);

		// add new entries to the peopleList
		// author = "max muster" <- uidAuthor
		// providerId = "None"
		// userId = "ox_4711" <- "ox_" + uid;
		if(uid!=null&&!author.isEmpty()) {
			final CTPeople ctPeople = operationDocument.getPeopleList(true);
			if(ctPeople!=null) {
				final List<CTPerson> peopleList = ctPeople.getPerson();
				if(peopleList!=null) {
					int i=0;
					for(;i<peopleList.size();i++) {
						final CTPerson person = peopleList.get(i);
						if(person.getAuthor()!=null&&person.getAuthor().equals(author)) {
							final CTPresenceInfo presenceInfo = person.getPresenceInfo(false);
							if(presenceInfo!=null) {
								final String providerId = presenceInfo.getProviderId();
								final String userId = presenceInfo.getUserId();
								if(providerId.equals("None")&&userId.startsWith("ox_")) {
								    final String id = getIdFromUserId(userId.substring(3));
								    if(id!=null && id.equals(uid)) {
    									break;
    								}
								}
							}
						}
					}
					if(i==peopleList.size()) {
						final CTPerson newPerson = new CTPerson();
						newPerson.setAuthor(author);
						peopleList.add(newPerson);
						final CTPresenceInfo presenceInfo = new CTPresenceInfo();
						presenceInfo.setProviderId("None");
						presenceInfo.setUserId("ox_" + uid);
						newPerson.setPresenceInfo(presenceInfo);
					}
				}
			}
			else {
				// TODO: removing uid for this author
			}
		}
	}

    // in case of a conversion problem null is returned
    public static XMLGregorianCalendar stringToXMLGregorianCalendar(String date) {
        if(date!=null) {
            try {
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            catch(IllegalArgumentException | DatatypeConfigurationException e) {
                // ignoring date
            }
        }
        return null;
    }

    public static String getIdFromUserId(String id) {
        final String[] s = id.split("::", -1);
        return s.length == 1 ? s[0] : s.length > 1 ? s[1] : null;
    }
}
