/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.sharedtypes.STOnOff;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTHeight;
import org.docx4j.wml.CTTblCellMar;
import org.docx4j.wml.CTTblLayoutType;
import org.docx4j.wml.CTTblLook;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.CTTblPrBase.TblStyle;
import org.docx4j.wml.CTTblPrBase.TblStyleColBandSize;
import org.docx4j.wml.CTTblPrBase.TblStyleRowBandSize;
import org.docx4j.wml.CTTrPrBase;
import org.docx4j.wml.CTVerticalJc;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.STBorder;
import org.docx4j.wml.STTblLayoutType;
import org.docx4j.wml.STVerticalJc;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblBorders;
import org.docx4j.wml.TblGridBase;
import org.docx4j.wml.TblGridCol;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcMar;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.TcPrInner;
import org.docx4j.wml.TcPrInner.GridSpan;
import org.docx4j.wml.TcPrInner.TcBorders;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.TableComponent;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class Table {

    // RowBandSize and ColBandSize needs to be set to 1, otherwise the
    // conditional table styles "band1Vert, band2Vert..." will be ignored
    // by word.
	public static void initLookAndRowBandSize(CTTblPrBase tblPr) {

		final ObjectFactory objectFactory = Context.getWmlObjectFactory();

		final CTTblLook tblLook = objectFactory.createCTTblLook();
        tblPr.setTblLook(tblLook);
        tblLook.setFirstColumn(STOnOff.TRUE);
        tblLook.setLastColumn(STOnOff.TRUE);
        tblLook.setFirstRow(STOnOff.TRUE);
        tblLook.setLastRow(STOnOff.TRUE);
        tblLook.setNoHBand(STOnOff.FALSE);
        tblLook.setNoHBand(STOnOff.FALSE);
        tblLook.setVal("1E0");

        final TblStyleRowBandSize rowBandSize = tblPr.getTblStyleRowBandSize(true);
        rowBandSize.setVal(Long.valueOf(1));
        final TblStyleColBandSize colBandSize = tblPr.getTblStyleColBandSize(true);
        colBandSize.setVal(Long.valueOf(1));
	}

    public static JSONArray getTableGrid(TblGridBase tblGridBase) {

        JSONArray tableGrid = null;
        if (tblGridBase!=null) {
            List<TblGridCol> gridCols = tblGridBase.getGridCol();
            if(gridCols!=null) {
                tableGrid = new JSONArray();
                for(TblGridCol gridCol:gridCols) {
                    Long w = gridCol.getW();
                    tableGrid.put(w!=null?w.longValue()*254/144:1);
                }
            }
        }
        return tableGrid;
    }

    public static void setTableGrid(DocxOperationDocument operationDocument, TableComponent tableComponent, TblGridBase tblGridBase, JSONArray tableGrid)
        throws JSONException {

        if (tblGridBase == null)
            return;

        if(tableGrid!=null) {
	        final List<TblGridCol> gridCols = tblGridBase.getGridCol();
	        gridCols.clear();
            for(int i=0; i<tableGrid.length(); i++) {
                TblGridCol tblGridCol = Context.getWmlObjectFactory().createTblGridCol();
                tblGridCol.setW(Long.valueOf(tableGrid.getInt(i)*144/254));
                gridCols.add(tblGridCol);
            }
	        if(tableComponent==null) {
	            return;
	        }

	        int rowWidth = Table.getRowWidth(tableComponent);
	        removeCellWidthProperties(tableComponent);

	        final Tbl tbl = (Tbl)tableComponent.getObject();
	        final TblPr tblPr = tbl.getTblPr(false);
	        if(tblPr==null) {
	        	return;
	        }

	        tblPr.getTblLayout(true).setType(STTblLayoutType.FIXED);
            final TblWidth tblW = tblPr.getTblW(true);
	        if(tblW.getType() == TblWidth.TYPE_AUTO) {
	            if(rowWidth != 0) {
	                setW100thmm(tblW, rowWidth);
	            } else {
	                tblPr.setTblW(new TblWidth(TblWidth.TYPE_PCT, 5000));
	            }
	        }

	        if(tblW.getW()!=null) {
		        Double tableWidth = null;
		        if(tblW.getType()==null||tblW.getType().equals("dxa")) {
		        	tableWidth = tblW.getW().doubleValue();
		        }
		        else if(!(tbl.getParent() instanceof Tc)) {
		        	if(tblW.getType().equals("auto")||tblW.getType().equals("pct")) {
		        		tableWidth = Double.valueOf(Utils.getPageWidth(Utils.getDocumentProperties(operationDocument.getPackage().getMainDocumentPart(), false), 19000));
		        		if(tblW.getType().equals("pct")) {
		                    tableWidth *= tblW.getW().doubleValue() / 5000;
		        		}
		        	}
		        }
		        // we could determine the width of the table, so it is possible to adjust also the gridValues to 1/20 pts
		        if(tableWidth!=null) {
			        // updating tableGrid to 1/20 points
			        double totalGridSize = 0;
			        for(int i=0; i<gridCols.size(); i++) {
			        	totalGridSize += gridCols.get(i).getW().doubleValue();
			        }
			        if(totalGridSize!=0.0) {
				        final double gridFactor = (tableWidth*1440.0d/2540.0d) / totalGridSize;
			        	for(int i=0; i<gridCols.size(); i++) {
			        	    final double w = gridCols.get(i).getW().doubleValue() * gridFactor;
			        		gridCols.get(i).setW(Double.valueOf(w).longValue());
			        	}
			        }
		        }
	        }
        }
    }

    public static void applyTableStyle(String styleId, CTTblPrBase tblPr) {
        if (styleId!=null) {
            final TblStyle tblStyle = tblPr.getTblStyle(true);
            tblStyle.setVal(styleId);
            tblPr.setTblStyle(tblStyle);
            if(tblPr.getTblLook()==null) {
                initLookAndRowBandSize(tblPr);
            }
        }
        else {
            tblPr.setTblStyle(null);
        }
    }

    public static void applyTableProperties(DocxOperationDocument operationDocument, JSONObject tableProperties, TableComponent tableComponent, TblGridBase tblGridBase, CTTblPrBase tblPr)
        throws FilterException, JSONException {

        if(tableProperties==null)
            return;

        final ObjectFactory objectFactory = Context.getWmlObjectFactory();
        final Iterator<String> keys = tableProperties.keys();

        Object newTableWidth = null;
        JSONArray newTableGrid = null;

        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = tableProperties.get(attr);

            if(attr.equals(OCKey.TABLE_GRID.value()) && value instanceof JSONArray) {
                newTableGrid = (JSONArray)value;
            } else if(attr.equals(OCKey.WIDTH.value())) {
                newTableWidth = value;
            } else if(attr.equals(OCKey.INDENT.value())) {
                tblPr.setTblInd(createTblWidthFromJSON(objectFactory, value));
            }
            else if(attr.equals(OCKey.BORDER_LEFT.value()))
                tblPr.getTblBorders(true).setLeft(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_TOP.value()))
                tblPr.getTblBorders(true).setTop(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_RIGHT.value()))
                tblPr.getTblBorders(true).setRight(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_BOTTOM.value()))
                tblPr.getTblBorders(true).setBottom(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_INSIDE_HOR.value()))
                tblPr.getTblBorders(true).setInsideH(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_INSIDE_VERT.value()))
                tblPr.getTblBorders(true).setInsideV(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.PADDING_LEFT.value()))
                tblPr.getTblCellMar(true).setLeft(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_TOP.value()))
                tblPr.getTblCellMar(true).setTop(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_RIGHT.value()))
                tblPr.getTblCellMar(true).setRight(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_BOTTOM.value()))
                tblPr.getTblCellMar(true).setBottom(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.FILL_COLOR.value())) {
                if(value instanceof JSONObject) {
                    Utils.initShdFromJSONColor(operationDocument, (JSONObject)value, tblPr.getShd(true));
                }
                else
                    tblPr.setShd(null);
            }
            else if(attr.equals(OCKey.EXCLUDE.value())) {
                final CTTblLook tblLook = objectFactory.createCTTblLook();
                if(value instanceof JSONArray) {
                    short val = 0x1e0;
                    Iterator<Object> jsonArray = ((JSONArray)value).iterator();
                    while(jsonArray.hasNext()) {
                        Object o = jsonArray.next();
                        if(o instanceof String) {
                            if(o.equals("firstRow")) {
                                val&=~0x20;
                            } else if(o.equals("lastRow")) {
                                val&=~0x40;
                            } else if(o.equals("firstCol")) {
                                val&=~0x80;
                            } else if(o.equals("lastCol")) {
                                val&=~0x100;
                            } else if(o.equals("bandsHor")) {
                                val|=0x200;
                            } else if(o.equals("bandsVert")) {
                                val|=0x400;
                            }
                        }
                    }
                    tblLook.setVal(Integer.toHexString(val));
                }
                else
                    tblLook.setVal("01E0");
                tblPr.setTblLook(tblLook);
            }
        }
        if(newTableGrid != null || newTableWidth != null) {

            if(newTableWidth != null) {
                tblPr.setTblW(createTblWidthFromJSON(objectFactory, newTableWidth));
            }
            setTableGrid(operationDocument, tableComponent, tblGridBase, newTableGrid);
    	}
    }

    public static void createTableProperties(WordprocessingMLPackage document, CTTblPrBase tableProperties, JSONArray tableGrid, boolean isRootTable, int firstRowWidth, JSONObject attrs)
        throws JSONException {

        final JSONObject jsonTableProperties = new JSONObject();

        if(tableGrid!=null)
            jsonTableProperties.put(OCKey.TABLE_GRID.value(), tableGrid);
        if(tableProperties!=null) {
            final CTTblLayoutType ctLayoutType = tableProperties.getTblLayout(false);
            @SuppressWarnings("unused")
            final STTblLayoutType tableLayoutType = ctLayoutType!=null ? ctLayoutType.getType() : STTblLayoutType.AUTOFIT;   // default = AUTOFIT
                                                                                                                             //           FIXED

            final long referenceWidth = isRootTable ? Utils.getPageWidth(Utils.getDocumentProperties(document.getMainDocumentPart(), false), 19000) : 0;
            Commons.jsonPut(jsonTableProperties, OCKey.WIDTH.value(), createJSONFromTblWidth(tableProperties.getTblW(false), referenceWidth, firstRowWidth));
            Object indent = createJSONFromTblWidth(tableProperties.getTblInd(), 0, 0);
            if("auto".equals(indent)) {
                indent = Integer.valueOf(0);
            }
            Commons.jsonPut(jsonTableProperties, OCKey.INDENT.value(), indent);

            // creating table borders
            TblBorders tblBorders = tableProperties.getTblBorders(false);
            if(tblBorders!=null) {
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_LEFT.value(), createJSONfromCTBorder(tblBorders.getLeft()));
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_TOP.value(), createJSONfromCTBorder(tblBorders.getTop()));
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_RIGHT.value(), createJSONfromCTBorder(tblBorders.getRight()));
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_BOTTOM.value(), createJSONfromCTBorder(tblBorders.getBottom()));
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_INSIDE_HOR.value(), createJSONfromCTBorder(tblBorders.getInsideH()));
                Commons.jsonPut(jsonTableProperties, OCKey.BORDER_INSIDE_VERT.value(), createJSONfromCTBorder(tblBorders.getInsideV()));
            }

            CTTblCellMar tblCellMar= tableProperties.getTblCellMar(false);
            if(tblCellMar!=null) {
                Commons.jsonPut(jsonTableProperties, OCKey.PADDING_LEFT.value(), createJSONFromTblWidth(tblCellMar.getLeft(), 0, 0));
                Commons.jsonPut(jsonTableProperties, OCKey.PADDING_TOP.value(), createJSONFromTblWidth(tblCellMar.getTop(), 0, 0));
                Commons.jsonPut(jsonTableProperties, OCKey.PADDING_RIGHT.value(), createJSONFromTblWidth(tblCellMar.getRight(), 0, 0));
                Commons.jsonPut(jsonTableProperties, OCKey.PADDING_BOTTOM.value(), createJSONFromTblWidth(tblCellMar.getBottom(), 0, 0));
            }

            Commons.jsonPut(jsonTableProperties, OCKey.FILL_COLOR.value(), Utils.createFillColor(tableProperties.getShd(false)));

            // taking care of used conditional styles..:
            final CTTblLook tblLook = tableProperties.getTblLook();
            if(tblLook!=null) {
                final String val = tblLook.getVal();
                final JSONArray jsonLook = new JSONArray();
                if(val!=null) {
                    int look = Integer.parseInt(val, 16);
                    if((look&0x20)==0)
                        jsonLook.put("firstRow");
                    if((look&0x40)==0)
                        jsonLook.put("lastRow");
                    if((look&0x80)==0)
                        jsonLook.put("firstCol");
                    if((look&0x100)==0)
                        jsonLook.put("lastCol");
                    if((look&0x200)>0) {
                        jsonLook.put("bandsHor");
                    }
                    if((look&0x400)>0) {
                        jsonLook.put("bandsVert");
                    }
                }
                else {
                    if(!STOnOff.getState(tblLook.getFirstRow(), false)) {
                        jsonLook.put("firstRow");
                    }
                    if(!STOnOff.getState(tblLook.getLastRow(), false)) {
                        jsonLook.put("lastRow");
                    }
                    if(!STOnOff.getState(tblLook.getFirstColumn(), false)) {
                        jsonLook.put("firstCol");
                    }
                    if(!STOnOff.getState(tblLook.getLastColumn(), false)) {
                        jsonLook.put("lastCol");
                    }
                    if(STOnOff.getState(tblLook.getNoHBand(), false)) {
                        jsonLook.put("bandsHor");
                    }
                    if(STOnOff.getState(tblLook.getNoVBand(), false)) {
                        jsonLook.put("bandsVert");
                    }
                }
                if(jsonLook.length()>0)
                    jsonTableProperties.put(OCKey.EXCLUDE.value(), jsonLook);
            }
        }
        if(!jsonTableProperties.isEmpty()) {
        	attrs.put(OCKey.TABLE.value(), jsonTableProperties);
        }
    }

    public static void applyRowProperties(JSONObject rowProperties, CTTrPrBase trPr)
        throws JSONException {

        if(rowProperties==null)
            return;

        final ObjectFactory objectFactory = Context.getWmlObjectFactory();
        final Iterator<String> keys = rowProperties.keys();
        final List<JAXBElement<?>> jaxbElements = trPr.getCnfStyleOrDivIdOrGridBefore();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = rowProperties.get(attr);
            if(attr.equals(OCKey.HEIGHT.value())) {
                if (value instanceof Number) {
                    JAXBElement<?> jaxbElement = findElement(jaxbElements, "trHeight");
                    CTHeight ctHeight;
                    if(jaxbElement!=null)
                        ctHeight = (CTHeight)jaxbElement.getValue();
                    else {
                        ctHeight = objectFactory.createCTHeight();
                    }
                    ctHeight.setVal(Long.valueOf(((Number)value).longValue() * 144 / 254));
                    if(jaxbElement!=null)
                        jaxbElement = objectFactory.createCTTrPrBaseTrHeight(ctHeight);
                    else
                        jaxbElements.add(objectFactory.createCTTrPrBaseTrHeight(ctHeight));
                }
            }
            else if(attr.equals(OCKey.HEADER_ROW.value())) {
                if(value instanceof Boolean) {
                    final JAXBElement<?> jaxbElement = findElement(jaxbElements, "tblHeader");
                    BooleanDefaultTrue tblHeader;
                    if(jaxbElement!=null) {
                        tblHeader = (BooleanDefaultTrue)jaxbElement.getValue();
                    }
                    else {
                        tblHeader = new BooleanDefaultTrue();
                    }
                    if(!((Boolean)value).booleanValue()) {
                        tblHeader.setVal(Boolean.FALSE);
                    }
                    if(jaxbElement==null) {
                        jaxbElements.add(objectFactory.createCTTrPrBaseTblHeader(tblHeader));
                    }
                }
                else if(value==JSONObject.NULL) {
                    removeElement(jaxbElements, "headerRow");
                }
            }
        }
    }

    public static JAXBElement<?> findElement(List<JAXBElement<?>> jaxbElements, String elementName) {
        for (int i=0; i<jaxbElements.size(); i++) {
            if(elementName.equals(jaxbElements.get(i).getName().getLocalPart())) {
                return jaxbElements.get(i);
            }
        }
        return null;
    }

    public static void removeElement(List<JAXBElement<?>> jaxbElements, String elementName) {
        for(int i=0; i<jaxbElements.size(); i++) {
            final JAXBElement<?> o = jaxbElements.get(i);
            if(elementName.equals(o.getName().getLocalPart())) {
                jaxbElements.remove(i);
                break;
            }
        }
    }

    public static void createRowProperties(CTTrPrBase rowProperties, JSONObject attrs)
        throws JSONException {

        JSONObject jsonRowProperties = new JSONObject();
        if(rowProperties!=null) {
            List<JAXBElement<?>> jaxbElements = rowProperties.getCnfStyleOrDivIdOrGridBefore();
            for(JAXBElement<?> jaxbElement:jaxbElements) {
                if(jaxbElement.getName().getLocalPart().equals("trHeight")) {
                    final Object o = jaxbElement.getValue();
                    if(o instanceof CTHeight) {
                        final Long height = ((CTHeight)o).getVal();
                        if(height!=null) {
                            jsonRowProperties.put(OCKey.HEIGHT.value(), height.longValue()*254/144);
                        }
                    }
                }
                if(jaxbElement.getName().getLocalPart().equals("tblHeader")) {
                    final Object o = jaxbElement.getValue();
                    if(o instanceof BooleanDefaultTrue) {
                        final boolean tblHeader = ((BooleanDefaultTrue)o).isVal();
                        if(tblHeader) {
                            jsonRowProperties.put(OCKey.HEADER_ROW.value(), tblHeader);
                        }
                    }
                }
            }
        }
        if(!jsonRowProperties.isEmpty()) {
        	attrs.put(OCKey.ROW.value(), jsonRowProperties);
        }
    }

    public static void applyCellProperties(DocxOperationDocument operationDocument, JSONObject cellProperties, TcPrInner tcPr)
        throws FilterException, JSONException {

        if(cellProperties==null)
            return;

        final ObjectFactory objectFactory = Context.getWmlObjectFactory();
        final Iterator<String> keys = cellProperties.keys();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = cellProperties.get(attr);
            if(attr.equals(OCKey.GRID_SPAN.value())) {
                if (value instanceof Number) {
                    TcPrInner.GridSpan span = tcPr.getGridSpan();
                    if (span==null) {
                        span = objectFactory.createTcPrInnerGridSpan();
                    }
                    span.setVal(Long.valueOf(((Number)value).longValue()));
                    tcPr.setGridSpan(span);
                }
                else
                    tcPr.setGridSpan(null);
            }
            else if(attr.equals(OCKey.MERGE_VERT.value())) {
                if(value==JSONObject.NULL||"noMerge".equals(value)) {
                    tcPr.setVMerge(null);
                }
                else if("restart".equals(value)) {
                    tcPr.setVMerge(new TcPrInner.VMerge("restart"));
                }
                else if("continue".equals(value)) {
                    tcPr.setVMerge(new TcPrInner.VMerge());
                }
            }
            else if(attr.equals(OCKey.BORDER_LEFT.value()))
                tcPr.getTcBorders(true).setLeft(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_TOP.value()))
                tcPr.getTcBorders(true).setTop(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_RIGHT.value()))
                tcPr.getTcBorders(true).setRight(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_BOTTOM.value()))
                tcPr.getTcBorders(true).setBottom(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_INSIDE_HOR.value()))
                tcPr.getTcBorders(true).setInsideH(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.BORDER_INSIDE_VERT.value()))
                tcPr.getTcBorders(true).setInsideV(createCTBorderFromJSON(operationDocument, objectFactory, value));
            else if(attr.equals(OCKey.PADDING_LEFT.value()))
                tcPr.getTcMar(true).setLeft(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_TOP.value()))
                tcPr.getTcMar(true).setTop(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_RIGHT.value()))
                tcPr.getTcMar(true).setRight(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.PADDING_BOTTOM.value()))
                tcPr.getTcMar(true).setBottom(createTblWidthFromJSON(objectFactory, value));
            else if(attr.equals(OCKey.FILL_COLOR.value())) {
                if(value instanceof JSONObject) {
                    Utils.initShdFromJSONColor(operationDocument, (JSONObject)value, tcPr.getShd(true));
                }
                else
                    tcPr.setShd(null);
            }
            else if(attr.equals(OCKey.ALIGN_VERT.value())) {
                if(value instanceof String) {
                    if("top".equals(value)) {
                        tcPr.getVAlign(true).setVal(STVerticalJc.TOP);
                    }
                    else if("center".equals(value)) {
                        tcPr.getVAlign(true).setVal(STVerticalJc.CENTER);
                    }
                    else if("bottom".equals(value)) {
                        tcPr.getVAlign(true).setVal(STVerticalJc.BOTTOM);
                    }
                    else if("justified".equals(value)||"distributed".equals(value)) {
                        tcPr.getVAlign(true).setVal(STVerticalJc.BOTH);
                    }
                }
                else if(value==JSONObject.NULL) {
                    tcPr.setVAlign(null);
                }
            }
        }
    }

    public static void createCellProperties(TcPrInner cellProperties, Tbl table, JSONObject attrs)
        throws JSONException {

        JSONObject jsonCellProperties = new JSONObject();
        if(cellProperties!=null) {

            GridSpan gridSpan = cellProperties.getGridSpan();
            if(gridSpan!=null) {
                jsonCellProperties.put(OCKey.GRID_SPAN.value(), gridSpan.getVal().intValue());
            }

            final CTVerticalJc vAlign = cellProperties.getVAlign(false);
            if(vAlign!=null) {
                final STVerticalJc vA = vAlign.getVal();
                String jsonVAlign = "top";
                if(vA==STVerticalJc.CENTER) {
                    jsonVAlign = "center";
                }
                else if(vA==STVerticalJc.BOTTOM) {
                    jsonVAlign = "bottom";

                }
                else if(vA==STVerticalJc.BOTH) {
                    jsonVAlign = "justified";
                }
                jsonCellProperties.put(OCKey.ALIGN_VERT.value(), jsonVAlign);
            }
            if(cellProperties.getVMerge()!=null) {
                final String vMerge = cellProperties.getVMerge().getVal();
                if(vMerge==null||vMerge.equals("continue")) {
                    jsonCellProperties.put(OCKey.MERGE_VERT.value(), "continue");
                }
                else if(vMerge.equals("restart")) {
                    jsonCellProperties.put(OCKey.MERGE_VERT.value(), "restart");
                }
            }

            // creating cell borders
            TcBorders tcBorder = cellProperties.getTcBorders(false);
            if(tcBorder!=null) {
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_LEFT.value(), createJSONfromCTBorder(tcBorder.getLeft()));
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_TOP.value(), createJSONfromCTBorder(tcBorder.getTop()));
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_RIGHT.value(), createJSONfromCTBorder(tcBorder.getRight()));
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_BOTTOM.value(), createJSONfromCTBorder(tcBorder.getBottom()));
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_INSIDE_HOR.value(), createJSONfromCTBorder(tcBorder.getInsideH()));
                Commons.jsonPut(jsonCellProperties, OCKey.BORDER_INSIDE_VERT.value(), createJSONfromCTBorder(tcBorder.getInsideV()));
            }

            // docs-3464, always create cell fill, there is no inheritance from table fill
            Commons.jsonPut(jsonCellProperties, OCKey.FILL_COLOR.value(), Utils.createFillColor(cellProperties.getShd(false)));

            TcMar tcMar = cellProperties.getTcMar(false);
            if(tcMar!=null) {
                Commons.jsonPut(jsonCellProperties, OCKey.PADDING_LEFT.value(), createJSONFromTblWidth(tcMar.getLeft(), 0, 0));
                Commons.jsonPut(jsonCellProperties, OCKey.PADDING_TOP.value(), createJSONFromTblWidth(tcMar.getTop(), 0, 0));
                Commons.jsonPut(jsonCellProperties, OCKey.PADDING_RIGHT.value(), createJSONFromTblWidth(tcMar.getRight(), 0, 0));
                Commons.jsonPut(jsonCellProperties, OCKey.PADDING_BOTTOM.value(), createJSONFromTblWidth(tcMar.getBottom(), 0, 0));
            }
        }
        if(!jsonCellProperties.isEmpty()) {
        	attrs.put(OCKey.CELL.value(), jsonCellProperties);
        }
    }

    private static CTBorder createCTBorderFromJSON(DocxOperationDocument operationDocument, ObjectFactory objectFactory, Object borderProperties )
        throws FilterException, JSONException {

        CTBorder ctBorder = null;
        if(borderProperties instanceof JSONObject) {
            ctBorder = objectFactory.createCTBorder();
            Iterator<String> keys = ((JSONObject)borderProperties).keys();
            while(keys.hasNext()) {
                String attr = keys.next();
                Object value = ((JSONObject)borderProperties).get(attr);
                if(attr.equals(OCKey.STYLE.value())) {
                    if(value instanceof String) {
                        STBorder borderStyle = STBorder.SINGLE;
                        if(((String)value).equals("none"))
                            borderStyle = STBorder.NONE;
                        else if (((String)value).equals("single"))
                            borderStyle = STBorder.SINGLE;
                        else if (((String)value).equals("none"))
                            borderStyle = STBorder.NONE;
                        else if (((String)value).equals("double"))
                            borderStyle = STBorder.DOUBLE;
                        else if (((String)value).equals("dotted"))
                            borderStyle = STBorder.DOTTED;
                        else if (((String)value).equals("dashed"))
                            borderStyle = STBorder.DASHED;
                        else if (((String)value).equals("outset"))
                            borderStyle = STBorder.OUTSET;
                        else if (((String)value).equals("inset"))
                            borderStyle = STBorder.INSET;
                        ctBorder.setVal(borderStyle);
                    }
                    else
                        ctBorder.setVal(STBorder.NONE);
                } else if(attr.equals(OCKey.WIDTH.value())) {
                    if(value instanceof Integer) {
                        int val = ((Integer)value + 2 )* 576 / 2540;
                        ctBorder.setSz(Long.valueOf(val));
                    }
                    else
                        ctBorder.setSz(null);
                } else if(attr.equals(OCKey.COLOR.value())) {
                    if(value instanceof JSONObject) {
                        Utils.initBorderColorFromJSONColor(operationDocument, (JSONObject)value, ctBorder);
                    }
                    else
                        ctBorder.setColor(null);
                }
            }
        }
        return ctBorder;
    }

    private static JSONObject createJSONfromCTBorder(CTBorder tblBorder)
        throws JSONException {

        if(tblBorder==null)
            return null;
        JSONObject jsonBorder = new JSONObject();
        STBorder borderStyle = tblBorder.getVal();
        if(borderStyle!=null) {
            String style = "single";
            if(borderStyle==STBorder.NONE)
                style = "none";
            else if(borderStyle==STBorder.NIL)
                style = "none";
            else if(borderStyle==STBorder.SINGLE)
                style = "single";
            else if(borderStyle==STBorder.DOUBLE)
                style = "double";
            else if(borderStyle==STBorder.DOTTED)
                style = "dotted";
            else if(borderStyle==STBorder.DASHED)
                style = "dashed";
            else if(borderStyle==STBorder.OUTSET)
                style = "outset";
            else if(borderStyle==STBorder.INSET)
                style = "inset";

            // TODO... each ooxml border style needs to be mapped to a corresponding html style... now they are all mapped to single border

            jsonBorder.put(OCKey.STYLE.value(), style);
        }

        // creating border color
        Commons.jsonPut(jsonBorder, OCKey.COLOR.value(), Utils.createColor(tblBorder.getThemeColor(), tblBorder.getColor()));

        // creating border with
        Long sz = tblBorder.getSz();
        if(sz!=null){
            int dv = (int)((sz.doubleValue() * 2540 + 288.)/ 576);
            jsonBorder.put(OCKey.WIDTH.value(), dv );    // 1/8 Pt to 1/100mm
        }
        return jsonBorder;
    }

    private static TblWidth createTblWidthFromJSON(ObjectFactory objectFactory, Object jsonWidth) {
        if(jsonWidth==JSONObject.NULL) {
            return null;
        }
        TblWidth tblWidth = null;
        if (jsonWidth instanceof Integer||jsonWidth instanceof String) {
            tblWidth = objectFactory.createTblWidth();
            if(jsonWidth instanceof Number) {
                setW100thmm(tblWidth, ((Number)jsonWidth).longValue());
            }
            else if (((String)jsonWidth).equals("auto")) {
                tblWidth.setType("pct");
                tblWidth.setW(Long.valueOf(5000));
            }
        }
        return tblWidth;
    }

    private static void setW100thmm(TblWidth tblWidth, long width) {
        tblWidth.setType("dxa");
        tblWidth.setW(Long.valueOf((width * 72 * 20) / 2540));
    }

    private static Object createJSONFromTblWidth(TblWidth tblWidth, long referenceWidth, int firstRowWidth) {

        if(tblWidth==null)
            return null;
        Object jsonObject = null;

        String tableWidthType=null;     // default = "dxa"; (1/20 point)
                                        //           "auto"
                                        //           "pct"  (if percent sign is present... then val W represents whole percent points,
                                        //                   otherwise ????)
                                        //           "nil"  (zero width)
        int tableWidth = 0;             // default = 0;

        tableWidthType = tblWidth.getType();
        Long l = tblWidth.getW();
        if(l!=null)
            tableWidth = l.intValue();

        if ((tableWidthType != null) && (tableWidthType.equals("auto")) && (tableWidth == 0) && (firstRowWidth > 0)) {
            if(referenceWidth < firstRowWidth)
                jsonObject = new String("auto");
            else
                jsonObject = Integer.valueOf(firstRowWidth);  // using the width, that was calculated from all cells in the first row
        }
        else if ((tableWidthType!=null&&tableWidthType.equals("auto"))) {
            jsonObject = new String("auto");
        }
        else if (tableWidthType!=null&&tableWidthType.equals("pct")) {   // relative to page width excluding margins ... 5000==100%
            if(referenceWidth!=0) {
                tableWidth = (int)(referenceWidth * tableWidth / 5000);
                jsonObject = Integer.valueOf(tableWidth);
            }
            else {
                // we do not have a referenceWidth, percentage to absolute width can't be calculated, so we set width = auto here...
                jsonObject = new String("auto");
            }
        }
        else { // default: if(tableWidthType==null||tableWidthType.equals("dxa"))
            tableWidth = tableWidth * 2540 / 72 / 20;
            jsonObject = Integer.valueOf(tableWidth);
        }
        return jsonObject;
    }

    private static void removeCellWidthProperties(TableComponent tableComponent) {
        if(tableComponent!=null) {
            IComponent<OfficeOpenXMLOperationDocument> tableRowComponent = tableComponent.getNextChildComponent(null,  null);
            while(tableRowComponent!=null) {
                IComponent<OfficeOpenXMLOperationDocument> tableCellComponent = tableRowComponent.getNextChildComponent(null, null);
                while(tableCellComponent!=null) {
                    final TcPr tcPr = ((Tc)tableCellComponent.getNode().getData()).getTcPr(false);
                    if(tcPr!=null) {
                        tcPr.setTcW(null);
                    }
                    tableCellComponent = tableCellComponent.getNextComponent();
                }
                tableRowComponent = tableRowComponent.getNextComponent();
            }
        }
    }

    /*
     * returns the absolute row width in 1/100thmm, if the full row width couldn't be determined 0 is returned.
     */
    public static int getRowWidth(TableComponent tableComponent) {

        if (tableComponent == null) {
            return 0;
        }

        final IComponent<OfficeOpenXMLOperationDocument> rowComponent = tableComponent.getNextChildComponent(null, null);
        if (rowComponent == null) {
            return 0;
        }

        int rowWidth = 0;

        IComponent<OfficeOpenXMLOperationDocument> cellComponent = rowComponent.getNextChildComponent(null, null);
        while(cellComponent!=null) {
            final TcPr cellProperties = ((Tc)cellComponent.getObject()).getTcPr(false);
            if (cellProperties == null) {
                return 0;
            }
            int cellWidth = 0;
            TblWidth cellWidthObject = cellProperties.getTcW();
            if (cellWidthObject == null) {
                return 0;
            }
            String cellWidthType = cellWidthObject.getType();
            Long l = cellWidthObject.getW();
            if (l == null) {
                return 0;
            }
            cellWidth = l.intValue();
            if ((cellWidthType != null) && (cellWidthType.equals("dxa"))) {
                rowWidth = rowWidth + cellWidth * 2540 / 72 / 20;
            } else {
                return 0;
            }
            cellComponent = cellComponent.getNextComponent();
        }
        return rowWidth;
    }
}
