/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.HashSet;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.SdtBlock;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblPr;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.tools.Table;

public class RootComponent extends DocxComponent {

    public RootComponent(DocxOperationDocument operationDocument, IContentAccessor contentAccessor) {
        super(operationDocument, new DLNode<Object>(contentAccessor), 0);
    }
    public RootComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> node, int componentNumber) {
    	super(parentContext, node, componentNumber);
    }
    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> rootNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)rootNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, rootNode.getData());
            if(ParagraphComponent.isRepresentableParagraph(o)) {
                nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Tbl) {
                nextComponent = new TableComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SdtBlock) {
                final SdtRootContext sdtRootContext = new SdtRootContext(this, childNode);
                nextComponent = sdtRootContext.getNextChildComponent(null, previousChildComponent);
            }
        }
        return nextComponent;
    }
    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

        DLList<Object> DLList;
        DLNode<Object> referenceNode = null;
        if(child!=null&&child.getComponentNumber()==number) {
            final HashSet<Class<?>> parentContextList = new HashSet<Class<?>>(1);
            parentContextList.add(SdtRootContext.class);
            final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = child.getContextChild(parentContextList);
            DLList = (DLList<Object>)((IContentAccessor)contextChild.getParentContext().getNode().getData()).getContent();
            referenceNode = contextChild.getNode();
        }
        else {
            DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        }
        switch(type) {
            case PARAGRAPH : {
                final Child newChild = Context.getWmlObjectFactory().createP();
                newChild.setParent(contextNode.getData());
                final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                DLList.addNode(referenceNode, newChildNode, true);
                return new ParagraphComponent(parentContext, newChildNode, number);
            }
            case TABLE : {
                final Tbl newTbl = Context.getWmlObjectFactory().createTbl();
                final TblPr tblPr = newTbl.getTblPr(true);

                // turning on each conditional Style, each of our conditional styles has to be used by default,
                // (they can be deactivated by using the exclude property)

                // TODO: tblLook changed in docx4j3.0.1...
                Table.initLookAndRowBandSize(tblPr);
                newTbl.setParent(contextNode.getData());
                final DLNode<Object> newChildNode = new DLNode<Object>(newTbl);
                DLList.addNode(referenceNode, newChildNode, true);
                return new TableComponent(parentContext, newChildNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }
    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {

    }
    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	return attrs;
    }
}
