/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import java.util.ArrayList;
import java.util.List;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTFFCheckBox;
import org.docx4j.wml.CTFFDDList;
import org.docx4j.wml.CTFFData;
import org.docx4j.wml.CTFFTextInput;
import org.docx4j.wml.DelInstrText;
import org.docx4j.wml.InstrText;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class FldCharBegin extends FldChar_Base {

    private List<DLNode<Object>> instructionNodes = new ArrayList<DLNode<Object>>();

    public FldCharBegin(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        final JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
        if(characterAttrs!=null) {
            final JSONObject fieldAttrs = characterAttrs.optJSONObject(OCKey.FIELD.value());
            if(fieldAttrs!=null) {
                final Object formFieldType = fieldAttrs.opt(OCKey.FORM_FIELD_TYPE.value());
                if(formFieldType!=null) {
                    if(formFieldType instanceof String) {
                        if(formFieldType.equals("checkBox")) {
                            final CTFFData ctFFData = new CTFFData();
                            fldChar.setFfData(ctFFData);
                            final CTFFCheckBox checkBox = new CTFFCheckBox();
                            final Object checked = fieldAttrs.opt(OCKey.CHECKED.value());
                            if(checked instanceof Boolean) {
                                checkBox.setChecked(new BooleanDefaultTrue((Boolean)checked));
                            }
                            ctFFData.getNameOrEnabledOrCalcOnExit().add(Context.getWmlObjectFactory().createCTFFDataCheckBox(checkBox));
                        }
                        else if(formFieldType.equals("textInput")) {
                            final CTFFData ctFFData = new CTFFData();
                            fldChar.setFfData(ctFFData);
                            final CTFFTextInput textInput = new CTFFTextInput();
                            ctFFData.getNameOrEnabledOrCalcOnExit().add(Context.getWmlObjectFactory().createCTFFDataTextInput(textInput));
                        }
                        else if(formFieldType.equals("dropDownList")) {
                            final CTFFData ctFFData = new CTFFData();
                            fldChar.setFfData(ctFFData);
                            final CTFFDDList ddList = new CTFFDDList();
                            ctFFData.getNameOrEnabledOrCalcOnExit().add(Context.getWmlObjectFactory().createCTFFDataDdList(ddList));
                        }
                    }
                    else {
                        fldChar.setFfData(null);
                    }
                }
            }
        }
        super.applyAttrsFromJSON(attrs);
    }

    public List<DLNode<Object>> getInstructionNodes() {
        return instructionNodes;
    }

    public String getInstruction() {
        final StringBuffer stringBuffer = new StringBuffer();
        for(DLNode<Object> node:instructionNodes) {
            if(stringBuffer.length()>0) {
                stringBuffer.append(' ');
            }
            final Object o = node.getData();
            if(o instanceof InstrText) {
                stringBuffer.append(((InstrText)o).getValue());
            }
            else if(o instanceof DelInstrText) {
                stringBuffer.append(((DelInstrText)o).getValue());
            }
        }
        return stringBuffer.toString();
    }
}
