/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.jaxb.Context;
import org.docx4j.wml.CTSdtCell;
import org.docx4j.wml.CTTrPrBase;
import org.docx4j.wml.CTTrPrChange;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Tr;
import org.docx4j.wml.TrPr;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IRow;
import com.openexchange.office.filter.ooxml.docx.tools.Table;
import com.openexchange.office.filter.ooxml.docx.tools.Utils;

public class TrComponent extends DocxComponent implements IRow {

    public TrComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }
    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> trNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)trNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        final int nextGridPosition = previousChildComponent instanceof TcComponent?((TcComponent)previousChildComponent).getNextGridPosition():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, trNode.getData());
            if(o instanceof Tc) {
                nextComponent = new TcComponent(this, childNode, nextComponentNumber, nextGridPosition);
            }
            else if(o instanceof CTSdtCell) {
                final SdtRowContext sdtRowContext = new SdtRowContext(this, childNode);
                nextComponent = sdtRowContext.getNextChildComponent(null, previousChildComponent);
            }
        }
        return nextComponent;
    }
	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {
		if(attrs==null) {
			return;
		}
		try {
	        final Tr tr = (Tr)getObject();
	        final TrPr trPr = tr.getTrPr(true);
	        Table.applyRowProperties(attrs.optJSONObject(OCKey.ROW.value()), trPr);
	        final Object changes = attrs.opt(OCKey.CHANGES.value());
	        if(changes!=null) {
		        if(changes instanceof JSONObject) {
		        	final Object attrsInserted = ((JSONObject)changes).opt(OCKey.INSERTED.value());
		        	if(attrsInserted!=null) {
		        		if(attrsInserted instanceof JSONObject) {
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsInserted, trPr.getIns(true));
		        		}
		        		else {
		        			trPr.setIns(null);
		        		}
		        	}
		        	final Object attrsRemoved = ((JSONObject)changes).opt(OCKey.REMOVED.value());
		        	if(attrsRemoved!=null) {
			        	if(attrsRemoved instanceof JSONObject) {
                			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsRemoved, trPr.getDel(true));
			        	}
			        	else {
			        		trPr.setDel(null);
			        	}
		        	}
		        	final Object attrsModified = ((JSONObject)changes).opt(OCKey.MODIFIED.value());
		        	if(attrsModified!=null) {
		        		if(attrsModified instanceof JSONObject) {
		        		    final CTTrPrChange trPrChange = trPr.getTrPrChange(true);
		        			Utils.applyTrackInfoFromJSON((com.openexchange.office.filter.ooxml.docx.DocxOperationDocument)operationDocument, (JSONObject)attrsModified, trPrChange);
        					final Object attrsModifiedAttrs = ((JSONObject)attrsModified).opt(OCKey.ATTRS.value());
        					if(attrsModifiedAttrs!=null) {
        						if(attrsModifiedAttrs instanceof JSONObject) {
        							Table.applyRowProperties(((JSONObject)attrsModifiedAttrs).optJSONObject(OCKey.ROW.value()), trPrChange.getTrPr(true));
        						}
        						else {
        							trPrChange.setTrPr(null);
        						}
        					}
		        		}
		        		else {
		        			trPr.setTrPrChange(null);
		        		}
		        	}
		        }
		        else {
		        	trPr.setIns(null);
		        	trPr.setDel(null);
		        	trPr.setTrPrChange(null);
		        }
	        }
		}
		catch(Exception e) {
		    //
		}
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
		throws JSONException {

		final Tr tr = (Tr)getObject();
        final TrPr trPr = tr.getTrPr(false);
        if(trPr!=null) {
            Table.createRowProperties(tr.getTrPr(false), attrs);
            if(trPr.getDel(false)!=null||trPr.getIns(false)!=null||trPr.getTrPrChange(false)!=null) {
                final JSONObject jsonRowChanges = new JSONObject(2);
                if(trPr.getDel(false)!=null) {
                	jsonRowChanges.put(OCKey.REMOVED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), trPr.getDel(false)));
                }
                if(trPr.getIns(false)!=null) {
                	jsonRowChanges.put(OCKey.INSERTED.value(), Utils.createJSONFromTrackInfo(getOperationDocument(), trPr.getIns(false)));
                }
                if(trPr.getTrPrChange(false)!=null) {
                	JSONObject rowModified = Utils.createJSONFromTrackInfo(getOperationDocument(), trPr.getTrPrChange(false));
                	final CTTrPrBase trPrChangePr = trPr.getTrPrChange(false).getTrPr(false);
                	if(trPrChangePr!=null) {
                		final JSONObject jsonRowAttrChanges = new JSONObject();
                		Table.createRowProperties(trPrChangePr, jsonRowAttrChanges);
                		if(!jsonRowAttrChanges.isEmpty()) {
                			rowModified.put(OCKey.ATTRS.value(), jsonRowAttrChanges);
                		}
                	}
                	if(!rowModified.isEmpty()) {
                    	jsonRowChanges.put(OCKey.MODIFIED.value(), rowModified);
                	}
                }
                if(!jsonRowChanges.isEmpty()) {
                	attrs.put(OCKey.CHANGES.value(), jsonRowChanges);
                }
            }
        }
        return attrs;
	}

	// IRow interface
	@Override
	public void insertCells(int cellPosition, int count, JSONObject attrs) throws Exception {

	    IComponent<OfficeOpenXMLOperationDocument> c = getChildComponent(cellPosition);
    	final Tr tr = (Tr)getObject();
        final DLList<Object> trContent = tr.getContent();
        for (int i=0; i<count; i++) {
            final Tc tc = Context.getWmlObjectFactory().createTc();
            tc.setParent(tr);
            trContent.addNode(c!=null?c.getNode():null, new DLNode<Object>(tc), true);
        }
        if(attrs!=null) {
            c = getChildComponent(cellPosition);
            for(int i=0; i<count; i++) {
            	c.applyAttrsFromJSON(attrs);
            	c = c.getNextComponent();
            }
        }
	}
}
