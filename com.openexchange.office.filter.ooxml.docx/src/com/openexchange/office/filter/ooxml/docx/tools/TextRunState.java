/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.tools;

import java.util.Set;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.RDBase;
import org.docx4j.wml.RIBase;
import org.docx4j.wml.RunDel;
import org.docx4j.wml.RunIns;
import org.docx4j.wml.RunMoveFrom;
import org.docx4j.wml.RunMoveTo;
import org.docx4j.wml.SdtRun;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.component.Child;

public class TextRunState implements ITextRunState {

    public P paragraph = null;
    public R textRun = null;
    public P.Hyperlink hyperlink = null;
    public RunMoveTo runMoveTo = null;
    public RunMoveFrom runMoveFrom = null;
    public RunIns runIns = null;
    public RunDel runDel = null;

    public TextRunState(R textRun) {
        this.textRun = textRun;
        Child parent = textRun;
        do {
            parent = (Child)parent.getParent();
            if(parent instanceof P.Hyperlink) {
                hyperlink = (P.Hyperlink)parent;
            }
            else if(parent instanceof RunMoveTo) {
                runMoveTo = (RunMoveTo)parent;
            }
            else if(parent instanceof RunMoveFrom) {
                runMoveFrom = (RunMoveFrom)parent;
            }
            else if(parent instanceof RunIns) {
                runIns = (RunIns)parent;
            }
            else if(parent instanceof RunDel) {
                runDel = (RunDel)parent;
            }
        }
        while(!(parent instanceof P));
        paragraph = (P)parent;
    }

    @Override
    public P getParagraph() {
        return paragraph;
    }

    @Override
    public P.Hyperlink getHyperlink() {
        return hyperlink;
    }

    @Override
    public R getTextRun() {
        return textRun;
    }

    @Override
    public boolean hasChangeTracking() {
        return runIns!=null||runDel!=null||runMoveFrom!=null||runMoveTo!=null;
    }

    @Override
    public P.Hyperlink getHyperlink(boolean forceCreate) {
        if(hyperlink==null&&forceCreate) {
            hyperlink = new P.Hyperlink();
            insertChild(hyperlink, hyperlinkParentContextList);
        }
        return hyperlink;
    }

    @Override
    public void removeHyperlink() {
        if(hyperlink!=null) {
            deleteChild(hyperlink);
            hyperlink = null;
        }
    }

    static final private ImmutableSet<Class<?>> hyperlinkParentContextList = ImmutableSet.<Class<?>> builder()
        .add(P.class)
        .add(SdtRun.class)
        .build();

    @Override
    public RIBase getRunIns(boolean forceCreate) {
        if(runIns==null&&runMoveTo==null&&forceCreate) {
            runIns = new RunIns();
            insertChild(runIns, runInsParentContextList);
        }
        return runIns!=null ? runIns : runMoveTo;
    }

    @Override
    public void removeRunIns() {
        if(runIns!=null) {
            deleteChild(runIns);
            runIns = null;
        }
        if(runMoveTo!=null) {
            deleteChild(runMoveTo);
            runMoveTo = null;
        }
    }

    static final private ImmutableSet<Class<?>> runInsParentContextList = ImmutableSet.<Class<?>> builder()
        .add(P.class)
        .add(SdtRun.class)
        .add(P.Hyperlink.class)
        .build();

    @Override
    public RDBase getRunDel(boolean forceCreate) {
        if(runDel==null&&runMoveFrom==null&&forceCreate) {
            runDel = new RunDel();
            insertChild(runDel, runDelParentContextList);
        }
        return runDel!=null ? runDel : runMoveFrom;
    }

    @Override
    public void removeRunDel() {
        if(runDel!=null) {
            deleteChild(runDel);
            runDel = null;
        }
        if(runMoveFrom!=null) {
            deleteChild(runMoveFrom);
            runMoveFrom = null;
        }
    }

    static final private ImmutableSet<Class<?>> runDelParentContextList = ImmutableSet.<Class<?>> builder()
        .add(P.class)
        .add(SdtRun.class)
        .add(P.Hyperlink.class)
        .add(RunMoveFrom.class)
        .add(RunMoveTo.class)
        .add(RunIns.class)
        .build();

    public void insertChild(Object c, Set<Class<?>> parentContextList) {
        Object child = textRun;
        Object parent = ((Child)child).getParent();
        while(parent!=null&&!parentContextList.contains(parent.getClass())) {
            child = parent;
            parent = ((Child)child).getParent();
        }
        if(parent!=null) {
            final int index = ((IContentAccessor)parent).getContent().indexOf(child);
            ((IContentAccessor)parent).getContent().set(index, c);
            ((IContentAccessor)c).getContent().add(child);
            ((Child)child).setParent(c);
            ((Child)c).setParent(parent);
        }
    }

    public void deleteChild(Object c) {
        final Object parent = ((Child)c).getParent();
        int index = ((IContentAccessor)parent).getContent().indexOf(c);
        ((IContentAccessor)parent).getContent().remove(index);
        for(Object o:((IContentAccessor)c).getContent()) {
            if(o instanceof Child) {
                ((Child)o).setParent(parent);
            }
            ((IContentAccessor)parent).getContent().add(index++, o);
        }
    }
}
