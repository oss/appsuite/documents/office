/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.docx.components;

import org.docx4j.mce.AlternateContent;
import org.docx4j.wml.Br;
import org.docx4j.wml.CTObject;
import org.docx4j.wml.DelInstrText;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.IText;
import org.docx4j.wml.InstrText;
import org.docx4j.wml.Pict;
import org.docx4j.wml.R;
import org.docx4j.wml.R.Ptab;
import org.docx4j.wml.R.Tab;
import org.docx4j.wml.STFldCharType;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;

public class TextRunContext extends ComponentContext<OfficeOpenXMLOperationDocument> {

    public TextRunContext(ComponentContext<OfficeOpenXMLOperationDocument> p, DLNode<Object> n) {
        super(p, n);
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> textRunNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)textRunNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        TextRun_Base nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = OfficeOpenXMLComponent.getContentModel(childNode, textRunNode.getData());
            if(o instanceof IText) {
                if(((IText)o).getValue().length()>0) {
                    nextComponent = new TextComponent(this, childNode, nextComponentNumber);
                }
            }
            else if(o instanceof AlternateContent)
                nextComponent = new AlternateContentComponent(this, childNode, nextComponentNumber);
            else if(o instanceof Drawing)
                nextComponent = new DrawingComponent(this, childNode, nextComponentNumber);
            else if(o instanceof Pict||o instanceof CTObject)
                nextComponent = new PictVMLComponent(this, childNode, nextComponentNumber);
            else if(o instanceof Tab)
                nextComponent = new TabComponent(this, childNode, nextComponentNumber);
            else if(o instanceof Ptab)
                nextComponent = new TabComponent(this, childNode, nextComponentNumber);
            else if(o instanceof Br)
                nextComponent = new HardBreakComponent(this, childNode, nextComponentNumber);
            else if(o instanceof R.CommentReference)
            	nextComponent = new CommentReferenceComponent(this, childNode, nextComponentNumber);
            else if(o instanceof FldChar) {
                final STFldCharType fldCharType = ((FldChar)o).getFldCharType();
                if(STFldCharType.BEGIN==fldCharType) {
                    nextComponent = new FldCharBegin(this, childNode, nextComponentNumber);
                }
                else if(STFldCharType.SEPARATE==fldCharType) {
                    nextComponent = new FldCharSeparate(this, childNode, nextComponentNumber, previousChildComponent instanceof FldCharBegin ? (FldCharBegin)previousChildComponent : null);
                }
                else if(STFldCharType.END==fldCharType) {
                    nextComponent = new FldCharEnd(this, childNode, nextComponentNumber);
                }
            }
            else if((o instanceof InstrText||o instanceof DelInstrText)&&previousChildComponent instanceof FldCharBegin) {
                ((FldCharBegin)previousChildComponent).getInstructionNodes().add(childNode);
            }
        }
        return nextComponent;
    }
    
    public static boolean isRepresentableTextRunContext(Object o) {
        return o instanceof R && ((R)o).notVanished();
    }
}
