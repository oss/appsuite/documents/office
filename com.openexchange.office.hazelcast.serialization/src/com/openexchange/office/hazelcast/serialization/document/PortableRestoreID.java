/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.tools.directory.DocRestoreID;



/**
 * {@link PortableResourceID} A {@link ID} implementation that can efficiently be serialized via Hazelcast's Portable mechanism.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 */
public class PortableRestoreID implements CustomPortable {

    public static final int CLASS_ID = 206;

    private static final String FIELD_ID = "id";

    private static final String FIELD_CREATED = "created";

    public static ClassDefinition CLASS_DEFINITION = null;

    private String id;

    private long created;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
            .addUTFField(FIELD_ID)
            .addLongField(FIELD_CREATED)
            .build();
    }

    public PortableRestoreID() {
        this.id = null;
        this.created = 0;
    }

    /**
     * Initializes a new {@link PortableID} based on an existing non portable ID.
     * @param id The non portable ID to use as base for this PortableID.
     */
    public PortableRestoreID(final DocRestoreID docRestoreID) {
        this.id = docRestoreID.getResourceID();
        this.created = docRestoreID.getCreated();
    }

    /**
     *
     * @return
     */
    final String getResourceID() { return this.id; }

    /**
     *
     * @return
     */
    final long getCreated() { return this.created; }

    @Override
    public final String toString() {
        return DocRestoreID.toString(id, created);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof PortableRestoreID)) return false;

        PortableRestoreID otherRestoreID = (PortableRestoreID)other;
        return ((StringUtils.equals(id, otherRestoreID.id)) && (otherRestoreID.created == this.created));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + Long.valueOf(created).hashCode();
        return result;
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_ID, id.toString());
        writer.writeLong(FIELD_CREATED, created);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        this.id = reader.readUTF(FIELD_ID);
        this.created = reader.readLong(FIELD_CREATED);
    }

    public static DocRestoreID createFrom(final PortableRestoreID portableRestoreID) {
        DocRestoreID docRestoreId = null;

        if (null != portableRestoreID) {
            docRestoreId = DocRestoreID.create(portableRestoreID.getResourceID(), portableRestoreID.getCreated());
        }

        return docRestoreId;
    }
}

