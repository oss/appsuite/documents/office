/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.tools.directory.DocResourceID;



/**
 * {@link PortableID} A {@link ID} implementation that can efficiently be serialized via Hazelcast's Portable mechanism.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.0
 */
public class PortableID implements CustomPortable {

    public static final int CLASS_ID = 200;

    private static final String FIELD_ID = "id";

    public static ClassDefinition CLASS_DEFINITION = null;

    private String id;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
            .addUTFField(FIELD_ID)
            .build();
    }

    public PortableID() {
        this.id = null;
    }

    /**
     * Initializes a new {@link PortableID} based on an existing non portable ID.
     * @param id The non portable ID to use as base for this PortableID.
     */
    public PortableID(final DocResourceID id) {
        this.id = id.toString();
    }

    /**
     * Initializes a new {@link PortableID}.
     * @param id
     */
    public PortableID(final String id) {
        this.id = id;
    }

    @Override
    public final String toString() {
        return this.id;
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_ID, id.toString());
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        this.id = reader.readUTF(FIELD_ID);
    }

}
