/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.Validate;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentResources;

/**
* {@link PortableDocumentResources} - Efficient serialization of default {@link Presence} fields via {@link Portable} mechanism. Use
* {@link PortableDocumentResources#getPresence()} to reconstruct the serialized Presence via the {@link Presence.Builder}.
*
* @author <a href="mailto:marc.arens@open-xchange.com">Carsten Driesner</a>
* @since 7.8.1
*/
public class PortableDocumentResources implements CustomPortable {

    public static final int CLASS_ID = 204;
    public static ClassDefinition CLASS_DEFINITION = null;

    private PortableRestoreID restoreId;
    private static final String FIELD_DOCRESTOREID = "docRestoreId";

    private long timeStamp;
    private static final String FIELD_TIMESTAMP = "timeStamp";

    private String uniqueInstanceId;
    private static final String FIELD_UNIQUEINSTANCEID = "uniqueInstanceId";

    private Set<String> resourceList;
    private static final String FIELD_RESOURCELIST = "resourceList";

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addPortableField(FIELD_DOCRESTOREID, PortableRestoreID.CLASS_DEFINITION)
        .addLongField(FIELD_TIMESTAMP)
        .addUTFField(FIELD_UNIQUEINSTANCEID)
        .addPortableArrayField(FIELD_RESOURCELIST, PortableResourceID.CLASS_DEFINITION)
        .build();
    }

    public PortableDocumentResources() {
        super();
    }

    public PortableDocumentResources(final DocumentResources doc) {
        Validate.notNull(doc, "Mandatory argument missing: doc");
        this.restoreId = new PortableRestoreID(doc.getDocRestoreId());
        this.resourceList = doc.getResources();
        this.uniqueInstanceId = doc.getUniqueInstanceId();
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writePortable(FIELD_DOCRESTOREID, this.restoreId);
        writer.writeLong(FIELD_TIMESTAMP, this.timeStamp);
        writer.writeUTF(FIELD_UNIQUEINSTANCEID, this.uniqueInstanceId);
        if (null != resourceList) {
            Set<PortableResourceID> portableResIds = new HashSet<PortableResourceID>(resourceList.size());
            for (String s : resourceList) {
                portableResIds.add(new PortableResourceID(s));
            }
            writer.writePortableArray(FIELD_RESOURCELIST, portableResIds.toArray(new Portable[portableResIds.size()]));
        } else {
            writer.writePortableArray(FIELD_RESOURCELIST, null);
        }
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        restoreId = reader.readPortable(FIELD_DOCRESTOREID);
        timeStamp = reader.readLong(FIELD_TIMESTAMP);
        uniqueInstanceId = reader.readUTF(FIELD_UNIQUEINSTANCEID);
        Portable[] portables = reader.readPortableArray(FIELD_RESOURCELIST);
        if (null != portables) {
            if (null == resourceList) { resourceList = new HashSet<String>(); }
            for (Portable portable : portables) {
                if (portable instanceof PortableResourceID) {
                    PortableResourceID portResId = (PortableResourceID)portable;
                    resourceList.add(portResId.toString());
                }
            }
        }
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    public DocRestoreID getRestoreId() {
        return DocRestoreID.create(this.restoreId.getResourceID(), this.restoreId.getCreated());
    }

    public void setRestoreId(DocRestoreID restoreId) {
        this.restoreId = new PortableRestoreID(restoreId);
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public String getUniqueInstanceId() {
        return this.uniqueInstanceId;
    }

    public void touch() {
        this.timeStamp = new Date().getTime();
    }

    public Set<String> getResourceList() {
        return this.resourceList;
    }

    @Override
    public String toString() {
        final StringBuffer strBuf = new StringBuffer();

        boolean first = true;
        if (null != resourceList) {
            for (String s : resourceList) {
                if (!first) { strBuf.append(","); }
                strBuf.append(s);
                first = false;
            }
        }

        return "PortableDocumentResources [restoreId=" + this.restoreId + ", timestamp=" + this.timeStamp + ", uniqueID=" + this.uniqueInstanceId + ", resources=[" + strBuf.toString() + "]" + "]";
    }

    public static DocumentResources createFrom(final PortableDocumentResources portableDocResources) {
        DocumentResources docResources = null;

        if (null != portableDocResources) {
            docResources = new DocumentResources(
                    portableDocResources.getRestoreId(),
                    portableDocResources.getTimeStamp(),
                    portableDocResources.getUniqueInstanceId(),
                    portableDocResources.getResourceList());
        }
        return docResources;
    }
}
