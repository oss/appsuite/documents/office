/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;

/**
 * {@link PortableCleanupStatus} - Holds data about a cluster wide cleanup for
 * Documents resources that can be used to decide if a cleanup was already started.
 * Additionally it holds data about who performed the cleanup at what time.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 */
public class PortableCleanupStatus implements CustomPortable {

    public static final int CLASS_ID = 207;

    private static String FIELD_CLEANING_MEMBER = "cleaningMember";
    private static String FIELD_MEMBER_TO_CLEAN = "memberToClean";
    private static String FIELD_CLEANING_START_TIME = "cleaningStartTime";
    private static String FIELD_CLEANING_FINISH_TIME = "cleaningFinishTime";
    public static ClassDefinition CLASS_DEFINITION = null;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addUTFField(FIELD_CLEANING_MEMBER)
        .addUTFField(FIELD_MEMBER_TO_CLEAN)
        .addLongField(FIELD_CLEANING_START_TIME)
        .addLongField(FIELD_CLEANING_FINISH_TIME)
        .build();
    }

    private String cleaningMember;
    private String memberToClean;
    private long cleaningStartTime;
    private long cleaningFinishTime;

    /**
     * Initializes a new {@link PortableCleanupStatus}.
     */
    public PortableCleanupStatus() {
        super();
    }

    /**
     * Initializes a new {@link PortableCleanupStatus}.
     *
     * @param cleaningMember The member that started the cleanup.
     * @param memberToClean The member that left the cluster.
     */
    public PortableCleanupStatus(String cleaningMemberUid, String memberToCleanUid) {
        super();
        this.cleaningMember = cleaningMemberUid;
        this.memberToClean = memberToCleanUid;
        this.cleaningStartTime = System.currentTimeMillis();
        this.cleaningFinishTime = -1;
    }

    /**
     * Gets the cleaningMember
     *
     * @return The cleaningMember
     */
    public final String getCleaningMember() {
        return cleaningMember;
    }

    /**
     * Sets the cleaningMember
     *
     * @param cleaningMember The cleaningMember to set
     */
    public void setCleaningMember(final String cleaningMember) {
        this.cleaningMember = cleaningMember;
    }

    /**
     * Gets the memberToClean
     *
     * @return The memberToClean
     */
    public final String getMemberToClean() {
        return memberToClean;
    }

    /**
     * Sets the memberToClean
     *
     * @param memberToClean The memberToClean to set
     */
    public void setMemberToClean(final String memberToClean) {
        this.memberToClean = memberToClean;
    }

    /**
     * Gets the cleaningStartTime
     *
     * @return The cleaningStartTime i milliseconds
     */
    public long getCleaningStartTime() {
        return cleaningStartTime;
    }

    /**
     * Sets the cleaningStartTime
     *
     * @param cleaningStartTime The cleaningStartTime to set in milliseconds.
     */
    public void setCleaningStartTime(long cleaningStartTime) {
        this.cleaningStartTime = cleaningStartTime;
    }

    /**
     * Gets the cleaningFinishTime
     *
     * @return The cleaningFinishTime in milliseconds, -1 if not finished.
     */
    public long getCleaningFinishTime() {
        return cleaningFinishTime;
    }

    /**
     * Sets the cleaningFinishTime
     *
     * @param cleaningFinishTime The cleaningFinishTime to set in milliseconds.
     */
    public void setCleaningFinishTime(long cleaningFinishTime) {
        this.cleaningFinishTime = cleaningFinishTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (cleaningFinishTime ^ (cleaningFinishTime >>> 32));
        result = prime * result + ((cleaningMember == null) ? 0 : cleaningMember.hashCode());
        result = prime * result + (int) (cleaningStartTime ^ (cleaningStartTime >>> 32));
        result = prime * result + ((memberToClean == null) ? 0 : memberToClean.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PortableCleanupStatus other = (PortableCleanupStatus) obj;
        if (cleaningFinishTime != other.cleaningFinishTime)
            return false;
        if (cleaningMember == null) {
            if (other.cleaningMember != null)
                return false;
        } else if (!cleaningMember.equals(other.cleaningMember))
            return false;
        if (cleaningStartTime != other.cleaningStartTime)
            return false;
        if (memberToClean == null) {
            if (other.memberToClean != null)
                return false;
        } else if (!memberToClean.equals(other.memberToClean))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PortableCleanupStatus [cleaningMember=" + cleaningMember + ", memberToClean=" + memberToClean + ", cleaningStartTime=" + cleaningStartTime + ", cleaningFinishTime=" + cleaningFinishTime + "]";
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_CLEANING_MEMBER, cleaningMember);
        writer.writeUTF(FIELD_MEMBER_TO_CLEAN, memberToClean);
        writer.writeLong(FIELD_CLEANING_START_TIME, cleaningStartTime);
        writer.writeLong(FIELD_CLEANING_FINISH_TIME, cleaningFinishTime);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        cleaningMember = reader.readUTF(FIELD_CLEANING_MEMBER);
        memberToClean = reader.readUTF(FIELD_MEMBER_TO_CLEAN);
        cleaningStartTime = reader.readLong(FIELD_CLEANING_START_TIME);
        cleaningFinishTime = reader.readLong(FIELD_CLEANING_FINISH_TIME);
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

}
