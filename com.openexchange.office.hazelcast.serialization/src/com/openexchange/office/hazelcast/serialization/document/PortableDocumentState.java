/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.Validate;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentState;

/**
 * {@link PortableDocumentState} - Efficient serialization of default {@link Presence} fields via {@link Portable} mechanism. Use
 * {@link PortableDocumentState#getPresence()} to reconstruct the serialized Presence via the {@link Presence.Builder}.
 *
 * @author <a href="mailto:marc.arens@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.0
 */
public class PortableDocumentState implements CustomPortable {

    public static final int CLASS_ID = 201;

    private PortableRestoreID resourceId;
    private static final String FIELD_DOCRESTOREID = "docRestoreId";

    private String uuid;
    private static final String FIELD_UNIQUEINSTANCEID = "uuid";

    private boolean active;
    private static final String FIELD_ACTIVE = "active";

    private long timeStamp;
    private static final String FIELD_TIMESTAMP = "timeStamp";

    private String fileName;
    private static final String FIELD_FILENAME = "filename";

    private String mimeType;
    private static final String FIELD_MIMETYPE = "mimeType";

    private String doc_file_id;
    private static final String FIELD_DOC_ID = "backup_doc_id";

    private String ops_file_id;
    private static final String FIELD_OPS_ID = "backup_ops_id";

    private int operationStateNumber = 0;
    private static final String FIELD_OSN = "base_osn";

    private String version;
    private static final String FIELD_VERSION = "base_version";

    private int currentOperationStateNumber = 0;
    private static final String FIELD_CURRENT_OSN = "current_osn";

    private String currentVersion;
    private static final String FIELD_CURRENT_VERSION = "current_version";

    public static ClassDefinition CLASS_DEFINITION = null;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addPortableField(FIELD_DOCRESTOREID, PortableRestoreID.CLASS_DEFINITION)
        .addUTFField(FIELD_UNIQUEINSTANCEID)
        .addBooleanField(FIELD_ACTIVE)
        .addLongField(FIELD_TIMESTAMP)
        .addUTFField(FIELD_FILENAME)
        .addUTFField(FIELD_MIMETYPE)
        .addUTFField(FIELD_DOC_ID)
        .addUTFField(FIELD_OPS_ID)
        .addIntField(FIELD_OSN)
        .addUTFField(FIELD_VERSION)
        .addIntField(FIELD_CURRENT_OSN)
        .addUTFField(FIELD_CURRENT_VERSION)
        .build();
    }

    public PortableDocumentState() {
        super();
    }

    public PortableDocumentState(final DocumentState doc) {
        Validate.notNull(doc, "Mandatory argument missing: doc");
        this.resourceId = new PortableRestoreID(doc.getRestoreId());
        this.uuid = doc.getUniqueInstanceId();
        this.active = doc.isActive();
        this.timeStamp = doc.getTimeStamp();
        this.fileName = doc.getFileName();
        this.mimeType = doc.getMimeType();
        this.doc_file_id = doc.getManagedDocFileId();
        this.ops_file_id = doc.getManagedOpsFileId();
        this.operationStateNumber = doc.getBaseOSN();
        this.version = doc.getBaseVersion();
        this.currentOperationStateNumber = doc.getCurrentOSN();
        this.currentVersion = doc.getCurrentVersion();
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writePortable(FIELD_DOCRESTOREID, this.resourceId);
        writer.writeUTF(FIELD_UNIQUEINSTANCEID, this.uuid);
        writer.writeBoolean(FIELD_ACTIVE, this.active);
        writer.writeLong(FIELD_TIMESTAMP, this.timeStamp);
        writer.writeUTF(FIELD_FILENAME, this.fileName);
        writer.writeUTF(FIELD_MIMETYPE, this.mimeType);
        writer.writeUTF(FIELD_DOC_ID, this.doc_file_id);
        writer.writeUTF(FIELD_OPS_ID, this.ops_file_id);
        writer.writeInt(FIELD_OSN, operationStateNumber);
        writer.writeUTF(FIELD_VERSION, version);
        writer.writeInt(FIELD_CURRENT_OSN, currentOperationStateNumber);
        writer.writeUTF(FIELD_CURRENT_VERSION, currentVersion);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        resourceId = reader.readPortable(FIELD_DOCRESTOREID);
        uuid = reader.readUTF(FIELD_UNIQUEINSTANCEID);
        active = reader.readBoolean(FIELD_ACTIVE);
        timeStamp = reader.readLong(FIELD_TIMESTAMP);
        fileName = reader.readUTF(FIELD_FILENAME);
        mimeType = reader.readUTF(FIELD_MIMETYPE);
        doc_file_id = reader.readUTF(FIELD_DOC_ID);
        ops_file_id = reader.readUTF(FIELD_OPS_ID);
        operationStateNumber = reader.readInt(FIELD_OSN);
        version = reader.readUTF(FIELD_VERSION);
        currentOperationStateNumber = reader.readInt(FIELD_CURRENT_OSN);
        currentVersion = reader.readUTF(FIELD_CURRENT_VERSION);
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    /**
     * Gets the document resource id
     *
     * @return The from
     */
    public DocRestoreID getRestoreId() {
        return DocRestoreID.create(this.resourceId.getResourceID(), this.resourceId.getCreated());
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(final String uniqueInstanceId) {
        this.uuid = uniqueInstanceId;
    }

    public boolean isActive() { return this.active; }

    public void setActive(boolean active) { this.active = active; }

    public long getTimeStamp() { return this.timeStamp; }

    public void setTimeStamp(long timeStamp) { this.timeStamp = timeStamp; }

    /**
     *
     * @return
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
    *
    * @return
    */
   public void setFileName(final String fileName) {
       this.fileName = fileName;
   }

   /**
     *
     * @return
     */
    public String getMimeType() {
        return this.mimeType;
    }

    /**
    *
    * @return
    */
   public void setMimeType(final String mimeType) {
       this.mimeType = mimeType;
   }

    /**
     * Gets the type
     *
     * @return The type
     */
    public String getBackupDocId() {
        return this.doc_file_id;
    }

    /**
     * Sets the backup id
     *
     * @param type The type to set
     */
    public void setBackupDocId(final String backupId) {
        this.doc_file_id = backupId;
    }

    /**
     * Gets the type
     *
     * @return The type
     */
    public String getBackupOpsId() {
        return this.ops_file_id;
    }

    /**
     * Sets the backup id
     *
     * @param type The type to set
     */
    public void setBackupOpsId(final String backupId) {
        this.ops_file_id = backupId;
    }

    /**
     * Gets the operation state number.
     *
     * @return
     */
    public int getBaseOSN() {
    	return this.operationStateNumber;
    }

    /**
     * Gets the version.
     *
     * @return
     */
    public String getBaseVersion() {
    	return this.version;
    }

    /**
     * Sets the operation state number.
     *
     * @param osn
     */
    public void setBaseOSN(int osn) {
    	this.operationStateNumber = osn;
    }

    /**
     * Sets the version.
     *
     * @param version
     */
    public void setBaseVersion(final String version) {
    	this.version = version;
    }

    /**
     * Gets the operation state number.
     *
     * @return
     */
    public int getCurrentOSN() {
    	return this.currentOperationStateNumber;
    }

    /**
     * Gets the version.
     *
     * @return
     */
    public String getCurrentVersion() {
    	return this.currentVersion;
    }

    /**
     * Sets the operation state number.
     *
     * @param osn
     */
    public void setCurrentOSN(int osn) {
    	this.currentOperationStateNumber = osn;
    }

    /**
     * Sets the version.
     *
     * @param version
     */
    public void setCurrentVersion(final String version) {
    	this.currentVersion = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((resourceId == null) ? 0 : resourceId.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result + ((doc_file_id == null) ? 0 : doc_file_id.hashCode());
        result = prime * result + ((ops_file_id == null) ? 0 : ops_file_id.hashCode());
        result = prime * result + operationStateNumber;
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result + currentOperationStateNumber;
        result = prime * result + ((currentVersion == null) ? 0 : currentVersion.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PortableDocumentState))
            return false;
        PortableDocumentState other = (PortableDocumentState) obj;
        if (resourceId == null) {
            if (other.resourceId != null)
                return false;
        } else if (!resourceId.equals(other.resourceId))
            return false;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        if (fileName == null) {
            if (other.fileName != null)
                return false;
        } else if (!fileName.equals(other.fileName))
            return false;
        if (mimeType == null) {
            if (other.mimeType != null)
                return false;
        } else if (!mimeType.equals(other.mimeType))
            return false;
        if (doc_file_id == null) {
            if (other.doc_file_id != null)
                return false;
        } else if (!doc_file_id.equals(other.doc_file_id))
            return false;
        if (ops_file_id == null) {
            if (other.ops_file_id != null)
                return false;
        } else if (!ops_file_id.equals(other.ops_file_id))
            return false;
        if (operationStateNumber != other.operationStateNumber)
        	return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        if (currentOperationStateNumber != other.currentOperationStateNumber)
        	return false;
        if (currentVersion == null) {
            if (other.currentVersion != null)
                return false;
        } else if (!currentVersion.equals(other.currentVersion))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PortableDocument [restoreId=" + this.resourceId +
                ", uuid=" + this.uuid +
                ", active=" + this.active +
                ", timestamp=" + (new Date(this.timeStamp)) +
                ", filename=" + this.fileName +
                ", mimetype=" + this.mimeType +
                ", doc_file_id=" + this.doc_file_id +
                ", ops_file_id=" + this.ops_file_id +
                ", osn=" + this.operationStateNumber +
                ", version=" + this.version +
                ", curr_osn=" + this.currentOperationStateNumber +
                ", curr_version=" + this.currentVersion + "]";
    }

    public static DocumentState createFrom(final PortableDocumentState portableDocState) {
        DocumentState docState = null;

        if (null != portableDocState) {
            docState = new DocumentState(
            		portableDocState.getRestoreId(),
            		portableDocState.getUuid(),
            		portableDocState.isActive(),
            		portableDocState.getTimeStamp(),
                    portableDocState.getFileName(),
                    portableDocState.getMimeType(),
                    portableDocState.getBackupDocId(),
                    portableDocState.getBackupOpsId(),
                    portableDocState.getBaseOSN(),
                    portableDocState.getBaseVersion(),
                    portableDocState.getCurrentOSN(),
                    portableDocState.getCurrentVersion());
        }

        return docState;
    }
}
