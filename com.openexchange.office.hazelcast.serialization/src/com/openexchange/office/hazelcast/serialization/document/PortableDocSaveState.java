/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.hazelcast.serialization.document;

import java.io.IOException;

import org.apache.commons.lang3.Validate;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.tools.directory.DocResourceID;
import com.openexchange.office.tools.directory.DocSaveState;

/**
 * {@link PortableDocumentState} - Efficient serialization of default {@link Presence} fields via {@link Portable} mechanism. Use
 * {@link PortableDocumentState#getPresence()} to reconstruct the serialized Presence via the {@link Presence.Builder}.
 *
 * @author <a href="mailto:marc.arens@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 */
public class PortableDocSaveState implements CustomPortable {

    public static final int CLASS_ID = 202;

    private PortableID resourceId;
    private static final String FIELD_DOCRESOURCEID = "docResourceId";

    private boolean saveInProgress;
    private static final String FIELD_SAVE_IN_PROGRESS = "saveInProgress";

    public static ClassDefinition CLASS_DEFINITION = null;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addPortableField(FIELD_DOCRESOURCEID, PortableID.CLASS_DEFINITION)
        .addBooleanField(FIELD_SAVE_IN_PROGRESS)
        .build();
    }

    public PortableDocSaveState() {
        super();
    }

    public PortableDocSaveState(final DocSaveState doc) {
        Validate.notNull(doc, "Mandatory argument missing: doc");
        this.resourceId = new PortableID(doc.getResourceId());
        this.saveInProgress = doc.getSaveInProgress();
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writePortable(FIELD_DOCRESOURCEID, this.resourceId);
        writer.writeBoolean(FIELD_SAVE_IN_PROGRESS, this.saveInProgress);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        resourceId = reader.readPortable(FIELD_DOCRESOURCEID);
        saveInProgress = reader.readBoolean(FIELD_SAVE_IN_PROGRESS);
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    /**
     * Gets the document resource id
     *
     * @return The from
     */
    public PortableID getResourceId() {
        return resourceId;
    }

    /**
     * Sets the from
     *
     * @param from The from to set
     */
    public void setFrom(PortableID resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * Gets the save state
     *
     * @return The state
     */
    public boolean getSaveState() {
        return this.saveInProgress;
    }

    /**
     * Sets the save state
     *
     * @param state The state to set
     */
    public void setSaveState(boolean state) {
        this.saveInProgress = state;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((resourceId == null) ? 0 : resourceId.hashCode());
        result = prime * result + (saveInProgress ? 1 : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PortableDocSaveState))
            return false;
        PortableDocSaveState other = (PortableDocSaveState) obj;
        if (resourceId == null) {
            if (other.resourceId != null)
                return false;
        } else if (!resourceId.equals(other.resourceId))
            return false;
        if (saveInProgress != other.saveInProgress)
            return false;
        if (saveInProgress != other.saveInProgress)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PortableSaveState [id=" + this.resourceId + ", state=" + this.saveInProgress+ "]";
    }

    public static DocSaveState createFrom(final PortableDocSaveState portableSaveState) {
        DocSaveState saveState = null;

        if (null != portableSaveState) {
        	saveState = new DocSaveState(DocResourceID.createDocResourceID(
        			portableSaveState.getResourceId().toString()),
        			portableSaveState.getSaveState());
        }
        return saveState;
    }
}
