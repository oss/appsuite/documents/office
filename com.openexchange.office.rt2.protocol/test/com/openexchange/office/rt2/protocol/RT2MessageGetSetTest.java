/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.UUID;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;

public class RT2MessageGetSetTest {

    //-------------------------------------------------------------------------
    public RT2MessageGetSetTest() {}

    //-------------------------------------------------------------------------
    @Test
    public void testGetAndSetUserAgent() throws JSONException {
        var type = RT2Protocol.REQUEST_JOIN;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var sessionId = "bb315e9cbf7082a9ccff360eb4696301";
        var fileId = "123456";
        var folderId = "789/123456";
        var userAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0";
        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);

        RT2MessageGetSet.setUserAgent(request, userAgent);

        var getUserAgent = RT2MessageGetSet.getUserAgent(request);
        assertEquals(userAgent, getUserAgent);
    }

    // -------------------------------------------------------------------------
    @Test
    public void testGetAndSetNodeUUID() throws JSONException {
        var type = RT2Protocol.REQUEST_JOIN;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var sessionId = "bb315e9cbf7082a9ccff360eb4696301";
        var fileId = "123456";
        var folderId = "789/123456";
        var nodeUuid = UUID.randomUUID().toString();
        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);

        RT2MessageGetSet.setNodeUUID(request, nodeUuid);

        var getNodeUuid = RT2MessageGetSet.getNodeUUID(request);
        assertEquals(nodeUuid, getNodeUuid);
    }

    // -------------------------------------------------------------------------
    @Test
    public void testGetAndSetOriginAsHostName() throws JSONException {
        var type = RT2Protocol.REQUEST_JOIN;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var sessionId = "bb315e9cbf7082a9ccff360eb4696301";
        var fileId = "123456";
        var folderId = "789/123456";
        var originAsHostName = "mw-node-1";
        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);

        RT2MessageGetSet.setOriginAsHostName(request, originAsHostName);

        var getOriginAsHostName = RT2MessageGetSet.getOriginAsHostName(request);
        assertEquals(originAsHostName, getOriginAsHostName);
    }

    // -------------------------------------------------------------------------
    @Test
    public void testErrors() throws JSONException {
        var type = RT2Protocol.REQUEST_JOIN;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var sessionId = "bb315e9cbf7082a9ccff360eb4696301";
        var fileId = "123456";
        var folderId = "789/123456";
        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);

        var errorResponse = RT2MessageFactory.createResponseFromMessage(request, RT2MessageType.RESPONSE_GENERIC_ERROR);

        var error = ErrorCode.HANGUP_INVALID_OPERATIONS_SENT_ERROR;
        RT2MessageGetSet.setError(errorResponse, error);

        assertTrue(RT2MessageGetSet.hasError(errorResponse));
        assertTrue(RT2MessageGetSet.isError(errorResponse));
        assertEquals(error, RT2MessageGetSet.getError(errorResponse));

        // does nothing - resetting an error is not possible
        RT2MessageGetSet.setError(errorResponse, null);
        assertEquals(error, RT2MessageGetSet.getError(errorResponse));

        assertEquals(ErrorCode.NO_ERROR, RT2MessageGetSet.getError(request));
        assertFalse(RT2MessageGetSet.hasError(request));
        assertFalse(RT2MessageGetSet.isError(request));
    }

    // -------------------------------------------------------------------------
    @Test
    public void testGetAndSetRecipients() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var broadcastReload = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_FILE_MOVED_RELOAD, clientUid, docUid);

        var recipient1 = UUID.randomUUID().toString();
        var recipient2 = UUID.randomUUID().toString();
        var recipient3 = UUID.randomUUID().toString();
        RT2MessageGetSet.setRecipients(broadcastReload, recipient1, recipient2, recipient3);

        var recipientsList = RT2MessageGetSet.getRecipients(broadcastReload);
        assertEquals(3, recipientsList.size());
        assertTrue(recipientsList.contains(recipient1));
        assertTrue(recipientsList.contains(recipient2));
        assertTrue(recipientsList.contains(recipient3));
    }

    // -------------------------------------------------------------------------
    @Test
    public void testGetAdvisoryLockAction() throws JSONException {
        var type = RT2Protocol.REQUEST_OPEN_DOC;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var sessionId = "bb315e9cbf7082a9ccff360eb4696301";
        var fileId = "123456";
        var folderId = "789/123456";
        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);

        var advisoryLockAction = "reset";
        request.setHeader(RT2Protocol.HEADER_ADVISORY_LOCK, advisoryLockAction);

        var getAdvisoryLockAction = RT2MessageGetSet.getAdvisoryLockAction(request);
        assertEquals(advisoryLockAction, getAdvisoryLockAction);

    }
}
