/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.HashMap;
import java.util.UUID;
import javax.jms.Message;
import javax.validation.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.internal.EDocState;
import com.openexchange.office.rt2.protocol.value.RT2AdminHzMemberUuidType;
import com.openexchange.office.rt2.protocol.value.RT2AdminHzUuidType;
import com.openexchange.office.rt2.protocol.value.RT2AdminIdType;
import com.openexchange.office.rt2.protocol.value.RT2AppActionType;
import com.openexchange.office.rt2.protocol.value.RT2AuthCodeType;
import com.openexchange.office.rt2.protocol.value.RT2AutoCloseType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocTypeType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2DriveDocIdType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2FastEmptyOsnType;
import com.openexchange.office.rt2.protocol.value.RT2FastEmptyType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2NoRestoreType;
import com.openexchange.office.rt2.protocol.value.RT2OldClientUuidType;
import com.openexchange.office.rt2.protocol.value.RT2PreviewType;
import com.openexchange.office.rt2.protocol.value.RT2RecipientListType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.rt2.protocol.value.RT2StorageOsnType;
import com.openexchange.office.rt2.protocol.value.RT2StorageVersionType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.DocumentType;

public class RT2MessageTest {

    private static final String TEST_JSON_BODY_STRING = "{\"selections\":[{\"type\":\"text\",\"start\":[1,0],\"end\":[1,0]}]}";

    //-------------------------------------------------------------------------
    public RT2MessageTest() {}

    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp() throws Exception {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown() throws Exception {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCleanHeaderBeforeSendToClient() throws ValidationException, JSONException {
        var adminTaskFilesMovedDeletedMsg = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_TASK_FILES_MOVED_DELETED);

        assertTrue(adminTaskFilesMovedDeletedMsg.isType(RT2MessageType.ADMIN_TASK_FILES_MOVED_DELETED));

        var headersBefore = adminTaskFilesMovedDeletedMsg.getHeader();
        RT2Message.cleanHeaderBeforeSendToClient(adminTaskFilesMovedDeletedMsg);
        var headersAfter = adminTaskFilesMovedDeletedMsg.getHeader();

        assertTrue(headersBefore.equals(headersAfter));

        var msgId = "6959ee74-7f8d-45ac-8d0a-81822787b051";
        var clientUid = "46af15b8-79fb-43cb-baf7-27713b02cf8a";
        var docUid = "99a66d8e3b945e9f38f5f03ff324d855";
        var seqNr = 10;
        var sessionId = "ab0a090332eeebc1d1df8d915926730a";
        var folderId = "1099";
        var fileId = "1099/384745";

        var openDocResponse = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId, fileId, folderId);

        headersBefore = openDocResponse.getHeader();
        RT2Message.cleanHeaderBeforeSendToClient(openDocResponse);
        headersAfter = openDocResponse.getHeader();

        assertEquals(msgId, headersBefore.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersBefore.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersBefore.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersBefore.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersBefore.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersBefore.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersBefore.get(RT2Protocol.HEADER_FILE_ID));

        assertNotEquals(headersBefore, headersAfter);
        assertEquals(msgId, headersAfter.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersAfter.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersAfter.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersAfter.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(null, headersAfter.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(null, headersAfter.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(null, headersAfter.get(RT2Protocol.HEADER_FILE_ID));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCleanHeaderBeforeSendToServer() throws JSONException {
        var msgId = "6959ee74-7f8d-45ac-8d0a-81822787b051";
        var clientUid = "46af15b8-79fb-43cb-baf7-27713b02cf8a";
        var docUid = "99a66d8e3b945e9f38f5f03ff324d855";
        var seqNr = 10;
        var sessionId = "ab0a090332eeebc1d1df8d915926730a";
        var folderId = "1099";
        var fileId = "1099/384745";

        var openDocRequestAsJSon = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId, fileId, folderId);
        var openDocRequest = RT2MessageFactory.fromJSON(openDocRequestAsJSon, false);

        var headersBefore = openDocRequest.getHeader();
        RT2Message.cleanHeaderBeforeSendToServer(openDocRequest);
        var headersAfter = openDocRequest.getHeader();

        assertEquals(msgId, headersBefore.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersBefore.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersBefore.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersBefore.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersBefore.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersBefore.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersBefore.get(RT2Protocol.HEADER_FILE_ID));

        assertEquals(headersBefore, headersAfter);

        var internalRecipients = "test";
        var perfClientIn = System.currentTimeMillis();
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, internalRecipients);
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN, perfClientIn);
        openDocRequest = RT2MessageFactory.fromJSON(openDocRequestAsJSon, false);

        headersBefore = openDocRequest.getHeader();
        RT2Message.cleanHeaderBeforeSendToServer(openDocRequest);
        headersAfter = openDocRequest.getHeader();

        assertEquals(msgId, headersBefore.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersBefore.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersBefore.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersBefore.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersBefore.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersBefore.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersBefore.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(internalRecipients, headersBefore.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));
        assertEquals(perfClientIn, headersBefore.get(RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN));

        assertNotEquals(headersBefore, headersAfter);
        assertEquals(msgId, headersAfter.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersAfter.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersAfter.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersAfter.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersAfter.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersAfter.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersAfter.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(null, headersAfter.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));
        assertEquals(perfClientIn, headersBefore.get(RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCleanInternalHeader() throws JSONException {
        var msgId = "6959ee74-7f8d-45ac-8d0a-81822787b051";
        var clientUid = "46af15b8-79fb-43cb-baf7-27713b02cf8a";
        var docUid = "99a66d8e3b945e9f38f5f03ff324d855";
        var seqNr = 10;
        var sessionId = "ab0a090332eeebc1d1df8d915926730a";
        var folderId = "1099";
        var fileId = "1099/384745";

        var internalRecipients = "test";
        var saveDocRequestAsJSon = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_SAVE_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.setFolderAndFileId(saveDocRequestAsJSon, folderId, fileId);
        RT2MessageHelper.addHeader(saveDocRequestAsJSon, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, internalRecipients);
        var saveDocRequest = RT2MessageFactory.fromJSON(saveDocRequestAsJSon, false);

        var headersBefore = saveDocRequest.getHeader();
        RT2Message.cleanInternalHeader(saveDocRequest);
        var headersAfter = saveDocRequest.getHeader();

        assertEquals(msgId, headersBefore.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersBefore.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersBefore.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersBefore.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersBefore.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersBefore.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersBefore.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(internalRecipients, headersBefore.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));

        assertNotEquals(headersBefore, headersAfter);
        assertEquals(msgId, headersAfter.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headersAfter.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headersAfter.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headersAfter.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, headersAfter.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headersAfter.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headersAfter.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(null, headersAfter.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCopyHeaderWithprefix() throws JSONException {
        var msgId = "6959ee74-7f8d-45ac-8d0a-81822787b051";
        var clientUid = "46af15b8-79fb-43cb-baf7-27713b02cf8a";
        var docUid = "99a66d8e3b945e9f38f5f03ff324d855";
        var seqNr = 10;
        var sessionId = "ab0a090332eeebc1d1df8d915926730a";
        var folderId = "1099";
        var fileId = "1099/384745";

        var internalRecipients = "test";
        var perfClientIn = System.currentTimeMillis();
        var openDocRequestAsJSon = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.setFolderAndFileId(openDocRequestAsJSon, folderId, fileId);
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, internalRecipients);
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN, perfClientIn);
        var openDocRequest = RT2MessageFactory.fromJSON(openDocRequestAsJSon, false);

        var openDocResponse = RT2MessageFactory.createResponseFromMessage(openDocRequest, RT2MessageType.RESPONSE_OPEN_DOC);

        var headerResponse = openDocResponse.getHeader();
        assertEquals(msgId, headerResponse.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, headerResponse.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, headerResponse.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, headerResponse.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(null, headerResponse.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, headerResponse.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, headerResponse.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(internalRecipients, headerResponse.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));
        assertEquals(perfClientIn, headerResponse.get(RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN));

        var openDocRequest2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        var headerRequest2Before = openDocRequest2.getHeader();
        var sizeBefore = headerRequest2Before.size();
        RT2Message.copyHeaderWithprefix(openDocRequest, openDocRequest2, "dbg_");
        var headerRequest2After = openDocRequest2.getHeader();
        var sizeAfter = headerRequest2After.size();

        assertEquals(perfClientIn, headerRequest2After.get(RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN));
        assertEquals(sizeBefore + 1, sizeAfter);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCopyAllHeader() throws JSONException {
        var msgId = "6959ee74-7f8d-45ac-8d0a-81822787b051";
        var clientUid = "46af15b8-79fb-43cb-baf7-27713b02cf8a";
        var docUid = "99a66d8e3b945e9f38f5f03ff324d855";
        var seqNr = 10;
        var sessionId = "ab0a090332eeebc1d1df8d915926730a";
        var folderId = "1099";
        var fileId = "1099/384745";

        var internalRecipients = "test";
        var perfClientIn = System.currentTimeMillis();
        var openDocRequestAsJSon = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.setFolderAndFileId(openDocRequestAsJSon, folderId, fileId);
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, internalRecipients);
        RT2MessageHelper.addHeader(openDocRequestAsJSon, RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN, perfClientIn);
        var openDocRequest = RT2MessageFactory.fromJSON(openDocRequestAsJSon, false);
        var header1 = openDocRequest.getHeader();

        var msgId2 = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid2 = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid2 = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr2 = 99;
        var sessionId2 = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var openDocRequest2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId2, clientUid2, docUid2, seqNr2, sessionId2);
        var header2Before = openDocRequest2.getHeader();

        assertNotEquals(header1, header2Before);
        RT2Message.copyAllHeader(openDocRequest, openDocRequest2);
        var header2After = openDocRequest2.getHeader();

        assertEquals(header1, header2After);

        var perfClientOut = System.currentTimeMillis();
        var openDocRequestAsJson3 = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId2, clientUid2, docUid2, seqNr2, sessionId2);
        RT2MessageHelper.addHeader(openDocRequestAsJson3, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, internalRecipients);
        RT2MessageHelper.addHeader(openDocRequestAsJson3, RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN, perfClientIn);
        RT2MessageHelper.addHeader(openDocRequestAsJson3, RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_OUT, perfClientOut);
        var openDocRequest3 = RT2MessageFactory.fromJSON(openDocRequestAsJson3, false);

        var header3Before = openDocRequest3.getHeader();
        assertNotEquals(header1, header3Before);

        RT2Message.copyAllHeader(openDocRequest, openDocRequest3);
        var header3After = openDocRequest3.getHeader();

        assertNotEquals(header1, header3After);
        assertEquals(msgId, header3After.get(RT2Protocol.HEADER_MSG_ID));
        assertEquals(clientUid, header3After.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, header3After.get(RT2Protocol.HEADER_DOC_UID));
        assertEquals(seqNr, header3After.get(RT2Protocol.HEADER_SEQ_NR));
        assertEquals(sessionId, header3After.get(RT2Protocol.HEADER_SESSION_ID));
        assertEquals(folderId, header3After.get(RT2Protocol.HEADER_FOLDER_ID));
        assertEquals(fileId, header3After.get(RT2Protocol.HEADER_FILE_ID));
        assertEquals(internalRecipients, header3After.get(RT2Protocol.HEADER_INTERNAL_RECIPIENTS));
        assertEquals(perfClientOut, header3After.get(RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_OUT));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCopySomeHeader() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        var openDocRequest2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, null, 0, null);

        RT2Message.copySomeHeader(openDocRequest, openDocRequest2, RT2Protocol.HEADER_DOC_UID, RT2Protocol.HEADER_SEQ_NR, RT2Protocol.HEADER_SESSION_ID);

        var headers = openDocRequest.getHeader();
        var headers2 = openDocRequest2.getHeader();
        assertEquals(headers, headers2);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCopyBody() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.addBodyString(applyOpsAsJson, TEST_JSON_BODY_STRING);
        var applyOpsRequest = RT2MessageFactory.fromJSON(applyOpsAsJson, false);

        assertEquals(TEST_JSON_BODY_STRING, applyOpsRequest.getBodyString());

        var applyOpsAsJson2 = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        var applyOpsRequest2 = RT2MessageFactory.fromJSON(applyOpsAsJson2, false);

        var emptyJson = new JSONObject();
        assertEquals(emptyJson, applyOpsRequest2.getBody());
        RT2Message.copyBody(applyOpsRequest, applyOpsRequest2);

        assertEquals(applyOpsRequest.getBodyString(), applyOpsRequest2.getBodyString());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsType() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));
        closeDocRequest.isType(RT2MessageType.REQUEST_CLOSE_DOC);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetType() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));
        var type = closeDocRequest.getType();
        assertEquals(RT2MessageType.REQUEST_CLOSE_DOC, type);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetMessageID() throws JSONException {
        var adminHealthCheckRequest = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_HEALTH_CHECK_REQUEST);
        assertNotNull(adminHealthCheckRequest.getMessageID().getValue());

        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        var applyOpsRequest = RT2MessageFactory.fromJSON(applyOpsAsJson, false);

        assertEquals(msgId, applyOpsRequest.getMessageID().getValue());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetMessageID() {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var adminHealthCheckRequest = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_HEALTH_CHECK_REQUEST);

        assertNotNull(adminHealthCheckRequest.getMessageID().getValue());
        assertNotEquals(msgId, adminHealthCheckRequest.getMessageID().getValue());

        adminHealthCheckRequest.setMessageID(new RT2MessageIdType(msgId));
        assertEquals(msgId, adminHealthCheckRequest.getMessageID().getValue());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewMessageID() {
        var adminHealthCheckRequest = RT2MessageFactory.newAdminMessage(RT2MessageType.ADMIN_HEALTH_CHECK_REQUEST);
        var autoGenMsgId = adminHealthCheckRequest.getMessageID().getValue();
        adminHealthCheckRequest.newMessageID();

        assertNotEquals(autoGenMsgId, adminHealthCheckRequest.getMessageID().getValue());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testListHeader() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));

        var headerPropNames = closeDocRequest.listHeader();
        var header = closeDocRequest.getHeader();
        assertEquals(headerPropNames, header.keySet());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetHeader() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));

        var header = closeDocRequest.getHeader();
        assertEquals(clientUid, header.get(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(docUid, header.get(RT2Protocol.HEADER_DOC_UID));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testHasHeader() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));
        assertTrue(closeDocRequest.hasHeader(RT2Protocol.HEADER_MSG_ID));
        assertTrue(closeDocRequest.hasHeader(RT2Protocol.HEADER_CLIENT_UID));
        assertTrue(closeDocRequest.hasHeader(RT2Protocol.HEADER_DOC_UID));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetHeaderArgWithCheck() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";

        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));

        var headerClientUid = closeDocRequest.getHeader(RT2Protocol.HEADER_CLIENT_UID);
        assertEquals(clientUid, headerClientUid);

        var headerDocUid = closeDocRequest.getHeader(RT2Protocol.HEADER_DOC_UID);
        assertEquals(docUid, headerDocUid);

        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = (String) null;
        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        headerClientUid = openDocRequest.getHeader(RT2Protocol.HEADER_CLIENT_UID);
        assertEquals(clientUid, headerClientUid);

        headerDocUid = openDocRequest.getHeader(RT2Protocol.HEADER_DOC_UID);
        assertEquals(docUid, headerDocUid);

        try {
            openDocRequest.getHeader(RT2Protocol.HEADER_SESSION_ID);
            fail("Session is missing, therefore exception has to be thrown");
        } catch (ValidationException ex) {
            // expected
        }

        var adminHzMasterUuid = openDocRequest.getHeader(RT2Protocol.HEADER_ADMIN_HZ_MASTER_UUID);
        assertNull(adminHzMasterUuid);

        openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        try {
            openDocRequest.getHeader(RT2Protocol.HEADER_OLD_CLIENT_UID);
        } catch (ValidationException ex) {
            // expected
        }

        var oldClientUid = "97e918a3-98ff-4ab1-a842-34f2bdda7b18";
        openDocRequest.setOldClientUID(new RT2OldClientUuidType(oldClientUid));
        var headerOldClientUid = openDocRequest.getHeader(RT2Protocol.HEADER_OLD_CLIENT_UID);
        assertEquals(oldClientUid, headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));

        // ATTENTION: set null value for old client uid which converts null to ""
        openDocRequest.setOldClientUID(new RT2OldClientUuidType(null));
        headerOldClientUid = openDocRequest.getHeader(RT2Protocol.HEADER_OLD_CLIENT_UID);
        assertEquals("", headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetFlags() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var leaveAndCloseRequest = RT2MessageFactory.newLeaveAndCloseRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), Cause.GARBAGE_COLLECTING);

        leaveAndCloseRequest.setFlags(RT2MessageFlags.FLAG_DOC_BASED, RT2MessageFlags.FLAG_SESSION_BASED);
        assertTrue(leaveAndCloseRequest.hasFlags(RT2MessageFlags.FLAG_DOC_BASED));
        assertTrue(leaveAndCloseRequest.hasFlags(RT2MessageFlags.FLAG_SESSION_BASED));
        assertTrue(leaveAndCloseRequest.hasFlags(RT2MessageFlags.FLAG_DOC_BASED, RT2MessageFlags.FLAG_SESSION_BASED));
        assertFalse(leaveAndCloseRequest.hasFlags(RT2MessageFlags.FLAG_SEQUENCE_NR_BASED));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetHeader2() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var leaveAndCloseRequest = RT2MessageFactory.newLeaveAndCloseRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), Cause.GARBAGE_COLLECTING);

        var headerClientUid = leaveAndCloseRequest.getHeader2(RT2Protocol.HEADER_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals(clientUid, headerClientUid);

        try {
            leaveAndCloseRequest.getHeader2(RT2Protocol.HEADER_CLIENT_UID, 0, RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
            fail(RT2Protocol.REQUEST_CLOSE_AND_LEAVE + " doesn't support flag");
        } catch (ValidationException ex) {
            // expected
        }

        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var seqNr = 2;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        // no old client uid set
        try {
            openDocRequest.getHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
            fail("Expected ValidationException");
        } catch (ValidationException ex) {
            // expected
        }

        // old client uid set
        var oldClientUid = "22676030-f115-4d3b-af2d-f7ffc88756b5";
        openDocRequest.setHeader(RT2Protocol.HEADER_OLD_CLIENT_UID, oldClientUid);
        var headerOldClientUid = openDocRequest.getHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals(oldClientUid, headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));

        // old client uid with null value
        try {
            openDocRequest.setHeader(RT2Protocol.HEADER_OLD_CLIENT_UID, null);
            fail("Not allowed to set HEADER_OLD_CLIENT_UID to null");
        } catch (ValidationException ex) {
            // expected
        }

        // using setOldClientUid with RT2OldClientUuidType (which internally converts null to "") works
        openDocRequest.setOldClientUID(new RT2OldClientUuidType(null));
        headerOldClientUid = openDocRequest.getHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals("", headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetHeaderUnchecked() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var leaveAndCloseRequest = RT2MessageFactory.newLeaveAndCloseRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), Cause.GARBAGE_COLLECTING);

        var headerClientUid = leaveAndCloseRequest.getHeaderUnchecked(RT2Protocol.HEADER_CLIENT_UID);
        assertEquals(clientUid, headerClientUid);

        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var seqNr = 2;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var openDocRequestAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        var openDocRequest = RT2MessageFactory.fromJSON(openDocRequestAsJson, false);

        var headerOldClientUid = openDocRequest.getHeaderUnchecked(RT2Protocol.HEADER_OLD_CLIENT_UID);
        assertNull(headerOldClientUid);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetHeader() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 2;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        var fileId = "12345";
        var folderId = "1009/12345";
        openDocRequest.setHeader(RT2Protocol.HEADER_FILE_ID, fileId);
        openDocRequest.setHeader(RT2Protocol.HEADER_FILE_ID, folderId);

        // internal header
        var nodeUuid = UUID.randomUUID().toString();
        openDocRequest.setHeader(RT2Protocol.HEADER_INTENAL_BACKEND_PROCESSING_NODE, nodeUuid);

        // old client uid with null value
        try {
            openDocRequest.setHeader(RT2Protocol.HEADER_OLD_CLIENT_UID, null);
            fail("Not allowed to set HEADER_OLD_CLIENT_UID to null");
        } catch (ValidationException ex) {
            // expected
        }
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetHeader2() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var seqNr = 2;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        // old client uid set
        var oldClientUid = "22676030-f115-4d3b-af2d-f7ffc88756b5";
        openDocRequest.setHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, oldClientUid, RT2MessageFlags.FLAG_DOC_BASED);

        var headerOldClientUid = openDocRequest.getHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals(oldClientUid, headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));

        // old client uid with null value
        try {
            openDocRequest.setHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, null, RT2MessageFlags.FLAG_DOC_BASED);
            fail("Not allowed to set HEADER_OLD_CLIENT_UID to null");
        } catch (ValidationException ex) {
            // expected
        }

        try {
            openDocRequest.setHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, null, RT2MessageFlags.FLAG_DOC_BASED);
            fail("Not allowed to set HEADER_OLD_CLIENT_UID to null");
        } catch (ValidationException ex) {
            // expected
        }

        try {
            openDocRequest.setHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, null, RT2MessageFlags.FLAG_CHUNK_BASED);
            fail("Not allowed to set HEADER_OLD_CLIENT_UID to null");
        } catch (ValidationException ex) {
            var msg = ex.getMessage();
            assertTrue(StringUtils.contains(msg, "dont support requested flags"));
        }

        // using empty string works
        openDocRequest.setHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, "", RT2MessageFlags.FLAG_DOC_BASED);
        headerOldClientUid = openDocRequest.getHeader2(RT2Protocol.HEADER_OLD_CLIENT_UID, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals("", headerOldClientUid);
        assertTrue(openDocRequest.hasHeader(RT2Protocol.HEADER_OLD_CLIENT_UID));

        // setting header to value null with flag none is always allowed
        openDocRequest.setHeader2(RT2Protocol.HEADER_USERAGENT, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, null, RT2MessageFlags.FLAG_NONE);
        try {
            openDocRequest.setHeader2(RT2Protocol.HEADER_USERAGENT, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, null, RT2MessageFlags.FLAG_DOC_BASED);
            fail("Not allowed to set HEADER_USERAGENT to null if flag is not FLAG_NONE");
        } catch (ValidationException ex) {
            // expected
        }
    }

    //----------------------------------------------------------------------
    @Test
    public void testMergeHeader() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var seqNr = 2;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var openDocRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);

        var fileId = "12345";
        var folderId = "1099/12345";
        var headerToMerge = new HashMap<String, Object>();
        headerToMerge.put(RT2Protocol.HEADER_FILE_ID, fileId);
        headerToMerge.put(RT2Protocol.HEADER_FOLDER_ID, folderId);

        openDocRequest.mergeHeader(headerToMerge, null);
        assertEquals(fileId, openDocRequest.getHeader(RT2Protocol.HEADER_FILE_ID));
        assertEquals(folderId, openDocRequest.getHeader(RT2Protocol.HEADER_FOLDER_ID));

        var jmsMessage = Mockito.mock(Message.class);
        headerToMerge.put("unknown_header", "test");
        openDocRequest.mergeHeader(headerToMerge, jmsMessage);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetBody() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.addBodyString(applyOpsAsJson, TEST_JSON_BODY_STRING);
        var applyOpsRequest = RT2MessageFactory.fromJSON(applyOpsAsJson, false);

        var body = applyOpsRequest.getBody();
        assertNotNull(body);
        assertNotNull(body.getJSONArray("selections"));
        assertEquals(TEST_JSON_BODY_STRING, body.toString());

        applyOpsAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        applyOpsRequest = RT2MessageFactory.fromJSON(applyOpsAsJson, false);

        body = applyOpsRequest.getBody();
        assertNotNull(body);
        assertTrue(body.length() == 0);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetBodyString() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsAsJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        RT2MessageHelper.addBodyString(applyOpsAsJson, TEST_JSON_BODY_STRING);
        var applyOpsRequest = RT2MessageFactory.fromJSON(applyOpsAsJson, false);

        var bodyString = applyOpsRequest.getBodyString();
        assertNotNull(bodyString);
        assertEquals(TEST_JSON_BODY_STRING, bodyString);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetBody() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);

        var jsonBody = new JSONObject(TEST_JSON_BODY_STRING);
        applyOpsRequest.setBody(jsonBody);
        var bodyString = applyOpsRequest.getBodyString();
        assertEquals(TEST_JSON_BODY_STRING, bodyString);

        applyOpsRequest.setBody(null);
        bodyString = applyOpsRequest.getBodyString();
        assertNotNull(bodyString);
        assertEquals(new JSONObject(), applyOpsRequest.getBody());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSetBodyString() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);

        applyOpsRequest.setBodyString(TEST_JSON_BODY_STRING);
        var bodyString = applyOpsRequest.getBodyString();
        assertEquals(TEST_JSON_BODY_STRING, bodyString);

        applyOpsRequest.setBodyString(null);
        bodyString = applyOpsRequest.getBodyString();
        assertNotNull(bodyString);
        assertEquals(new JSONObject(), applyOpsRequest.getBody());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsValid() throws ValidationException, JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";
        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);

        var reasons = new StringBuffer();
        var valid = applyOpsRequest.isValid(reasons);
        assertTrue(valid);
        assertTrue(reasons.isEmpty());

        var openDocRequestJson = RT2MessageHelper.createJsonObjectRT2Message(RT2Protocol.REQUEST_OPEN_DOC);
        var openDocRequest = RT2MessageFactory.fromJSON(openDocRequestJson, false);
        openDocRequest.setHeaderWithoutCheck(RT2Protocol.HEADER_MSG_ID, null);

        try {
            // TODO: as isValid() uses the method which use wrapper types
            // isValid does not work as expected - the wrapper types already
            // check for null values and throw a NullPointerException
            openDocRequest.isValid(reasons);
        } catch (NullPointerException ex) {
            // expected
        }
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsSimpleMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        assertFalse(applyOpsRequest.isSimpleMessage());

        // TODO: There is no RT2Protocol defined message which has the flag simple, therefore
        // testing for simple is not possible.
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsSessionMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        assertTrue(applyOpsRequest.isSessionMessage());

        var pingMsg = RT2MessageFactory.newPing();
        assertFalse(pingMsg.isSessionMessage());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsSequenceMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        assertTrue(applyOpsRequest.isSequenceMessage());

        var pingMsg = RT2MessageFactory.newPing();
        assertFalse(pingMsg.isSequenceMessage());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsDocMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var applyOpsRequest = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, msgId, clientUid, docUid, seqNr, sessionId);
        assertTrue(applyOpsRequest.isDocMessage());

        var pingMsg = RT2MessageFactory.newPing();
        assertFalse(pingMsg.isDocMessage());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testIsChunkMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        // TODO: There is no request/response defined in RT2Protocol which uses the flag
        // FLAG_CHUNK_BASED. Therefore no positive test is possible for isChunkMessage()

        var pingMsg = RT2MessageFactory.newPing();
        assertFalse(pingMsg.isChunkMessage());

        var openDocResponse = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        assertFalse(openDocResponse.isChunkMessage());
    }

    //-------------------------------------------------------------------------
    @Test
    public void isFinalMessage() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        // TODO: There is no request/response defined in RT2Protocol which uses the flag
        // FLAG_CHUNK_BASED. Therefore no negative test is possible for isFinalMessage()

        var pingMsg = RT2MessageFactory.newPing();
        assertTrue(pingMsg.isFinalMessage());

        var openDocResponse = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        assertTrue(openDocResponse.isFinalMessage());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testToString() throws JSONException {
        var msgId = "79bd200a-26e6-4f21-a6e8-922a6be60ec4";
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var seqNr = 99;
        var sessionId = "ef2f0bcfca0b8a052ebadc0ad4adc8a3";

        var pingMsg = RT2MessageFactory.newPing();
        var pingToString = pingMsg.toString();
        assertTrue(StringUtils.isNotEmpty(pingToString));

        var openDocResponse = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_OPEN_DOC, msgId, clientUid, docUid, seqNr, sessionId);
        var openDocResponseToString = openDocResponse.toString();
        assertTrue(StringUtils.isNotEmpty(openDocResponseToString));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCompareTo() {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var request0 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        var request1 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        var request2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        var request3 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);

        request1.setSeqNumber(new RT2SeqNumberType(10));
        request2.setSeqNumber(new RT2SeqNumberType(11));
        request3.setSeqNumber(new RT2SeqNumberType(12));

        assertTrue(request0.compareTo(request0) == 0);
        assertTrue(request1.compareTo(request1) == 0);
        assertTrue(request0.compareTo(request1) == -1);
        assertTrue(request1.compareTo(request2) == -1);
        assertTrue(request1.compareTo(request3) == -1);
        assertTrue(request2.compareTo(request3) == -1);
        assertTrue(request1.compareTo(request0) == 1);
        assertTrue(request2.compareTo(request1) == 1);
        assertTrue(request3.compareTo(request2) == 1);
        assertTrue(request3.compareTo(request1) == 1);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGetAndSet() throws Exception {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var sessionId = UUID.randomUUID().toString();

        // get/set/clearSessionID
        //-------------------------------------------------------------------------

        var openDocResponse = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_OPEN_DOC, clientUid, docUid);
        try {
            openDocResponse.getSessionID();
            fail("SessionId must be set for message with FLAG_SESSION_BASED");
        } catch (ValidationException ex) {
            // expected
        }

        var sessionIdType = new RT2SessionIdType(sessionId);
        openDocResponse.setSessionID(sessionIdType);
        var getSessionId = openDocResponse.getSessionID();
        assertEquals(sessionIdType, getSessionId);

        try {
            openDocResponse.clearSessionID();
            fail("SessionId must be set for message with FLAGS_4_DOC_OPS - clearSession not possible");
        } catch (ValidationException ex) {
            // expected
        }

        // TODO: clearSession() cannot be called without an exception, whether because the
        // session id must exists or a message doesn't have the session based flag

        //-------------------------------------------------------------------------
        // has/get/setSeqNumber

        assertFalse(openDocResponse.hasSeqNumber());

        // TODO: Should also throw a validation exception - seq numbers are mandatory in case the
        // flags are set (RESPONSE_OPEN_DOC has the flag set)
        var getSeqNr = openDocResponse.getSeqNumber();
        assertEquals(Integer.valueOf(Integer.MIN_VALUE), getSeqNr.getValue());

        var seqNr = Integer.valueOf(10);
        openDocResponse.setSeqNumber(new RT2SeqNumberType(seqNr));
        assertTrue(openDocResponse.hasSeqNumber());

        getSeqNr = openDocResponse.getSeqNumber();
        assertEquals(seqNr, getSeqNr.getValue());

        // sets the sequence number to 0
        openDocResponse.clearSeqNumber();

        //-------------------------------------------------------------------------
        // set/get client uid

        var getClientUid = openDocResponse.getClientUID();
        assertEquals(clientUid, getClientUid.getValue());

        var newClientUid = UUID.randomUUID().toString();
        openDocResponse.setClientUID(new RT2CliendUidType(newClientUid));
        getClientUid = openDocResponse.getClientUID();
        var emptyClientUidType = new RT2CliendUidType();
        assertEquals(newClientUid, getClientUid.getValue());
        assertFalse(RT2CliendUidType.isEmpty(getClientUid));
        assertFalse(RT2CliendUidType.isNotEmpty(emptyClientUidType));
        assertTrue(RT2CliendUidType.isEmpty(null));
        assertTrue(RT2CliendUidType.isEmpty(emptyClientUidType));
        assertTrue(RT2CliendUidType.isNotEmpty(getClientUid));
        assertFalse(RT2CliendUidType.isNotEmptyAndEqual(emptyClientUidType, emptyClientUidType));
        assertFalse(RT2CliendUidType.isNotEmptyAndEqual(getClientUid, emptyClientUidType));
        assertFalse(RT2CliendUidType.isNotEmptyAndEqual(emptyClientUidType, getClientUid));
        assertTrue(RT2CliendUidType.isNotEmptyAndEqual(getClientUid, getClientUid));
        assertNotEquals(getClientUid.hashCode(), emptyClientUidType.hashCode());
        assertNotEquals(getClientUid, emptyClientUidType);
        assertNotEquals(emptyClientUidType, getClientUid);
        assertEquals(emptyClientUidType, new RT2CliendUidType());
        assertEquals(getClientUid, new RT2CliendUidType(newClientUid));

        //-------------------------------------------------------------------------
        // set/get old client uid

        try {
            openDocResponse.getOldClientUID();
            fail("OldClientUid must available on OPEN_DOC_RESPONSE due to flags");
        } catch (ValidationException ex) {
            // expected
        }

        var oldClientUid = UUID.randomUUID().toString();
        openDocResponse.setOldClientUID(new RT2OldClientUuidType(oldClientUid));
        var getOldClientUid = openDocResponse.getOldClientUID();
        assertEquals(oldClientUid, getOldClientUid.getValue());

        //-------------------------------------------------------------------------
        // get/setDriveDocUid

        var driveDocId = "a50336686884e69a5c2c4347e02d773a";
        try {
            openDocResponse.getDriveDocID();
            fail("driveDocId must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }

        openDocResponse.setDriveDocID(new RT2DriveDocIdType(driveDocId));
        var getDriveDocID = openDocResponse.getDriveDocID();
        assertEquals(driveDocId, getDriveDocID.getValue());

        //-------------------------------------------------------------------------
        // get/setFolderID

        var folderId = "1234/95732";
        try {
            openDocResponse.getFolderID();
            fail("folderID must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }

        openDocResponse.setFolderID(new RT2FolderIdType(folderId));
        var getFolderId = openDocResponse.getFolderID();
        assertEquals(folderId, getFolderId.getValue());

        //-------------------------------------------------------------------------
        // get/setFileID

        var fileId = "95732";
        try {
            openDocResponse.getFileID();
            fail("fileID must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }

        openDocResponse.setFileID(new RT2FileIdType(fileId));
        var getFileId = openDocResponse.getFileID();
        assertEquals(fileId, getFileId.getValue());

        //-------------------------------------------------------------------------
        // get/setDocUid

        var docUid2 = UUID.randomUUID().toString();
        var getDocUid = openDocResponse.getDocUID();
        assertEquals(docUid, getDocUid.getValue());
        openDocResponse.setDocUID(new RT2DocUidType(docUid2));
        getDocUid = openDocResponse.getDocUID();
        assertEquals(docUid2, getDocUid.getValue());

        //-------------------------------------------------------------------------
        // get/setDocUid

        var docType = DocumentType.TEXT.toString();
        try {
            openDocResponse.getDocType();
            fail("docType must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }
        openDocResponse.setDocType(new RT2DocTypeType(docType));
        var getDocType = openDocResponse.getDocType();
        assertEquals(docType, getDocType.getValue());

        //-------------------------------------------------------------------------
        // get/setDocState

        var docState = EDocState.E_ONLINE;
        try {
            openDocResponse.getDocState();
            fail("docState must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }
        openDocResponse.setDocState(docState);
        var getDocState = openDocResponse.getDocState();
        assertEquals(docState, getDocState);

        //-------------------------------------------------------------------------
        // mark/isPreview

        var isPreview = new RT2PreviewType(true);
        openDocResponse.isPreview();
        openDocResponse.markAsPreview();
        var getIsPreview = openDocResponse.isPreview();
        assertEquals(isPreview, getIsPreview);

        //-------------------------------------------------------------------------
        // get/setFastEmpty

        var fastEmpty = new RT2FastEmptyType(true);
        openDocResponse.getFastEmpty();
        openDocResponse.setFastEmpty(fastEmpty);
        var getFastEmpty = openDocResponse.getFastEmpty();
        assertEquals(fastEmpty, getFastEmpty);

        //-------------------------------------------------------------------------
        // get/setFastEmptyOSN

        var fastEmptyOSN = new RT2FastEmptyOsnType(23);
        openDocResponse.getFastEmptyOSN();
        openDocResponse.setFastEmptyOSN(fastEmptyOSN);
        var getFastEmptyOSN = openDocResponse.getFastEmptyOSN();
        assertEquals(fastEmptyOSN, getFastEmptyOSN);

        //-------------------------------------------------------------------------
        // get/setAuthCode

        var authCode = new RT2AuthCodeType("a50336686884e69a5c2c4347e02d773a");
        // TODO: This looks strange as null is used as default value for the authcode
        // where RT2ValueType base class does not accept null (throws a NullPointerException).
        // The value itself must be set by the client, but throwing a NullPointerException on
        // an optional value is a strange solution.
        try {
            openDocResponse.getAuthCode();
        } catch (NullPointerException ex) {
            // expected
        }
        openDocResponse.setAuthCode(authCode);
        var getAuthCode = openDocResponse.getAuthCode();
        assertEquals(authCode, getAuthCode);

        //-------------------------------------------------------------------------
        // isNewDocState

        var getNewDocState = openDocResponse.isNewDocState();
        assertEquals(false, getNewDocState);

        //-------------------------------------------------------------------------
        // getStorageVersion

        // TODO: This looks strange as null is used as default value for the storage version
        // where RT2ValueType base class does not accept null (throws a NullPointerException).
        // The value itself must be set by the client, but throwing a NullPointerException on
        // an optional value is a strange solution.
        try {
            openDocResponse.getStorageVersion();
        } catch (NullPointerException ex) {
            // expected
        }
        var storageVersion = new RT2StorageVersionType("1");
        RT2MessageGetSet.setOptionalHeader(openDocResponse, RT2Protocol.HEADER_STORAGE_VERSION, storageVersion.getValue());
        var getStorageVersion = openDocResponse.getStorageVersion();
        assertEquals(storageVersion, getStorageVersion);

        //-------------------------------------------------------------------------
        // getStorageOSN
        var storageOSN = new RT2StorageOsnType(-1);
        var getStorageOSN = openDocResponse.getStorageOSN();
        assertEquals(storageOSN, getStorageOSN);

        //-------------------------------------------------------------------------
        // get/setUnvailableTime

        var unavailTime = new RT2UnavailableTimeType(30000L);
        openDocResponse.getUnvailableTime();
        openDocResponse.setUnvailableTime(unavailTime);
        var getUnavailTime = openDocResponse.getUnvailableTime();
        assertEquals(unavailTime, getUnavailTime);

        //-------------------------------------------------------------------------
        // get/setAppAction

        try {
            openDocResponse.getAppAction();
            fail("app action must be set for message with FLAGS_4_DOC_OPS");
        } catch (ValidationException ex) {
            // expected
        }
        var appAction = new RT2AppActionType("handleUpdateSlide");
        openDocResponse.setAppAction(appAction);
        var getAppAction = openDocResponse.getAppAction();
        assertEquals(appAction, getAppAction);

        //-------------------------------------------------------------------------
        // get/setAdminID

        try {
            openDocResponse.getAdminID();
            fail("adminID must be set for message with FLAGS_4_DOC_OPS");
        } catch (NullPointerException ex) {
            // expected
        }
        var adminId = new RT2AdminIdType(UUID.randomUUID().toString());
        openDocResponse.setAdminID(adminId);
        var getAdminId = openDocResponse.getAdminID();
        assertEquals(adminId, getAdminId);

        //-------------------------------------------------------------------------
        // get/setAdminHZMasterUUID

        // TODO: a not-set uuid results in a NullPointerException as UUID.toString()
        // is used in the RT2AdminHzUuidType class which cannot work with null.
        try {
            openDocResponse.getAdminHZMasterUUID();
        } catch (NullPointerException ex) {
            // expected
        }
        var hZMasterUUID = new RT2AdminHzUuidType(UUID.randomUUID());
        openDocResponse.setAdminHZMasterUUID(hZMasterUUID);
        var getHZMasterUUID = openDocResponse.getAdminHZMasterUUID();
        assertEquals(hZMasterUUID, getHZMasterUUID);

        //-------------------------------------------------------------------------
        // get/setAdminHZMemberUUID

        // TODO: a not-set uuid results in a NullPointerException as UUID.toString()
        // is used in the RT2AdminHzMemberUuidType class which cannot work with null.
        try {
            openDocResponse.getAdminHZMemberUUID();
        } catch (NullPointerException ex) {
            // expected
        }
        var hZAdminMemberUUID = new RT2AdminHzMemberUuidType(UUID.randomUUID());
        openDocResponse.setAdminHZMemberUUID(hZAdminMemberUUID);
        var getHzAdminMemberUUID = openDocResponse.getAdminHZMemberUUID();
        assertEquals(hZAdminMemberUUID, getHzAdminMemberUUID);

        //-------------------------------------------------------------------------
        // get/setAutoClose

        var notSetAutoCloseValue = openDocResponse.getAutoClose();
        assertFalse(notSetAutoCloseValue.getValue());
        var autoClose = new RT2AutoCloseType(true);
        openDocResponse.setAutoClose(autoClose);
        var getAutoClose = openDocResponse.getAutoClose();
        assertEquals(autoClose, getAutoClose);

        //-------------------------------------------------------------------------
        // get/setNoRestore

        var notSetNoRestoreValue = openDocResponse.getNoRestore();
        assertFalse(notSetNoRestoreValue.getValue());
        var noRestore = new RT2NoRestoreType(true);
        openDocResponse.setNoRestore(noRestore);
        var getNoRestore = openDocResponse.getNoRestore();
        assertEquals(noRestore, getNoRestore);

        //-------------------------------------------------------------------------
        // get/setError/isError

        var notSetErrorValue = openDocResponse.getError();
        assertEquals(ErrorCode.NO_ERROR, notSetErrorValue.getValue());
        assertFalse(openDocResponse.isError());
        var error = new RT2ErrorCodeType(ErrorCode.ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR);
        openDocResponse.setError(error);
        var getError = openDocResponse.getError();
        assertEquals(error, getError);

        // TODO: check this "optimization" which does not allow to set
        // a non-error (which makes it impossible to reset an existing
        // error).
        error = new RT2ErrorCodeType(ErrorCode.NO_ERROR);
        openDocResponse.setError(error);
        assertTrue(openDocResponse.isError());

        //-------------------------------------------------------------------------
        // get/setRecipients

        var notSetRecipientsValue = openDocResponse.getRecipients();
        assertEquals(new RT2RecipientListType(), notSetRecipientsValue);
        var recipients = new RT2RecipientListType(new RT2CliendUidType(UUID.randomUUID().toString()), new RT2CliendUidType(UUID.randomUUID().toString()));
        openDocResponse.setRecipients(recipients);
        var getRecipients = openDocResponse.getRecipients();
        assertEquals(recipients, getRecipients);

        //-------------------------------------------------------------------------
        // get/setRecipients

        var notSetInternalBackendProcessingNode = openDocResponse.getInternalBackendProcessingNode();
        assertEquals(null, notSetInternalBackendProcessingNode);
        var internalBackendProcessingNode = UUID.randomUUID().toString();
        openDocResponse.setInternalBackendProcessingNode(internalBackendProcessingNode);
        var getInternalBackendProcessingNode = openDocResponse.getInternalBackendProcessingNode();
        assertEquals(internalBackendProcessingNode, getInternalBackendProcessingNode);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testEquals() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var request1 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        var request2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        request1.setBody(new JSONObject());
        request2.setBody(new JSONObject());

        assertNotEquals(request1, request2);
        request2.setMessageID(request1.getMessageID());
        assertEquals(request1, request2);
        assertEquals(request1, request1);
        assertNotEquals(request1, null);
        assertNotEquals(request1, new Object());

        var json = new JSONObject();
        json.put("type", RT2Protocol.REQUEST_JOIN);
        var requestWithNullBody1 = RT2MessageFactory.fromJSON(json, false);
        var requestWithNullBody2 = RT2MessageFactory.fromJSON(json, false);
        json.put("type", RT2Protocol.REQUEST_OPEN_DOC);
        var requestOpenDocWithNullBody = RT2MessageFactory.fromJSON(json, false);

        assertNotEquals(requestWithNullBody1, requestWithNullBody2);
        assertNotEquals(requestWithNullBody1, request1);
        assertNotEquals(request1, requestWithNullBody1);
        assertNotEquals(requestOpenDocWithNullBody, requestWithNullBody1);
        assertNotEquals(requestWithNullBody1, requestOpenDocWithNullBody);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testHashCode() throws JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var request1 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        var request2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        request1.setBody(new JSONObject());
        request2.setBody(new JSONObject());

        assertNotEquals(request1.hashCode(), request2.hashCode());
        request2.setMessageID(request1.getMessageID());
        assertEquals(request1.hashCode(), request2.hashCode());

        var json = new JSONObject();
        json.put("type", RT2Protocol.REQUEST_JOIN);
        var requestWithNullBody1 = RT2MessageFactory.fromJSON(json, false);
        var requestWithNullBody2 = RT2MessageFactory.fromJSON(json, false);

        assertNotEquals(requestWithNullBody1.hashCode(), requestWithNullBody2.hashCode());
        assertNotEquals(requestWithNullBody1.hashCode(), request1.hashCode());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNonReachableCodeViaMockito() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, JSONException {
        var clientUid = "1f2c7fdb-5bc7-4c7c-aa30-15587316aa48";
        var docUid = "88739141f90a8e8c992ba9282d968e9d";
        var jsonBodyString = "{\"seqNr\":199}";
        var request = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        request.setBody(new JSONObject(jsonBodyString));

        // isValid() with internal null values
        var mockRequest = Mockito.spy(request);
        Mockito.when(mockRequest.getType()).thenReturn(null);

        var reasons = new StringBuffer();
        var result = mockRequest.isValid(reasons);
        assertFalse(result);

        Mockito.when(mockRequest.getType()).thenCallRealMethod();
        Mockito.when(mockRequest.getMessageID()).thenReturn(null);
        result = mockRequest.isValid(reasons);
        assertFalse(result);

        // hashCode() with internal null values
        var request2 = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_APPLY_OPS, clientUid, docUid);
        request2.setBody(new JSONObject(jsonBodyString));
        var hashCode1 = request2.hashCode();
        assertNotEquals(request, request2);
        assertNotEquals(request2, request);

        setFieldOfInstance(RT2Message.class, request2, "header", null);
        var hashCode2 = request2.hashCode();
        assertNotEquals(request, request2);
        assertNotEquals(request2, request);
        setFieldOfInstance(RT2Message.class, request, "header", null);
        assertNotEquals(request, request2);
        assertNotEquals(request2, request);

        setFieldOfInstance(RT2Message.class, request2, "type", null);
        var hashCode3 = request2.hashCode();
        assertNotEquals(request, request2);
        assertNotEquals(request2, request);
        setFieldOfInstance(RT2Message.class, request, "type", null);
        assertEquals(request, request2);
        assertEquals(request2, request);

        setFieldOfInstance(RT2Message.class, request2, "body", null);
        var hashCode4 = request2.hashCode();
        assertNotEquals(request, request2);
        assertNotEquals(request2, request);
        setFieldOfInstance(RT2Message.class, request, "body", null);
        assertEquals(request, request2);
        assertEquals(request2, request);

        assertNotEquals(hashCode1, hashCode2);
        assertNotEquals(hashCode2, hashCode3);
        assertNotEquals(hashCode3, hashCode4);
    }

    private <T> void setFieldOfInstance(Class<T> clazz, T instance, String memberName, Object value) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        var declaredField = clazz.getDeclaredField(memberName);
        var accessible = declaredField.isAccessible();

        declaredField.setAccessible(true);
        declaredField.set(instance, value);
        declaredField.setAccessible(accessible);
    }
}
