package com.openexchange.office.rt2.protocol.utils;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

public class JmsMessageMock implements TextMessage {

    private Map<String, Object> properties = new HashMap<String, Object>();

    private String text;

    public JmsMessageMock() {
        // nothing to do
    }

    public JmsMessageMock(Map<String, Object> properties) {
        this.properties.putAll(properties);
    }

    @Override
    public void acknowledge() throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clearBody() throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void clearProperties() throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean getBooleanProperty(String arg0) throws JMSException {
        return (boolean)properties.get(arg0);
    }

    @Override
    public byte getByteProperty(String arg0) throws JMSException {
        return (byte)properties.get(arg0);
    }

    @Override
    public double getDoubleProperty(String arg0) throws JMSException {
        return (double)properties.get(arg0);
    }

    @Override
    public float getFloatProperty(String arg0) throws JMSException {
        return (float)properties.get(arg0);
    }

    @Override
    public int getIntProperty(String arg0) throws JMSException {
        return (int)properties.get(arg0);
    }

    @Override
    public String getJMSCorrelationID() throws JMSException {
        return null;
    }

    @Override
    public byte[] getJMSCorrelationIDAsBytes() throws JMSException {
        return null;
    }

    @Override
    public int getJMSDeliveryMode() throws JMSException {
        return 0;
    }

    @Override
    public Destination getJMSDestination() throws JMSException {
        return null;
    }

    @Override
    public long getJMSExpiration() throws JMSException {
        return 0;
    }

    @Override
    public String getJMSMessageID() throws JMSException {
        return null;
    }

    @Override
    public int getJMSPriority() throws JMSException {
        return 0;
    }

    @Override
    public boolean getJMSRedelivered() throws JMSException {
        return false;
    }

    @Override
    public Destination getJMSReplyTo() throws JMSException {
        return null;
    }

    @Override
    public long getJMSTimestamp() throws JMSException {
        return 0;
    }

    @Override
    public String getJMSType() throws JMSException {
        return null;
    }

    @Override
    public long getLongProperty(String arg0) throws JMSException {
        return (long)properties.get(arg0);
    }

    @Override
    public Object getObjectProperty(String arg0) throws JMSException {
        return properties.get(arg0);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Enumeration getPropertyNames() throws JMSException {
        return Collections.enumeration(properties.keySet());
    }

    @Override
    public short getShortProperty(String arg0) throws JMSException {
        return (short)properties.get(arg0);
    }

    @Override
    public String getStringProperty(String arg0) throws JMSException {
        return (String)properties.get(arg0);
    }

    @Override
    public boolean propertyExists(String arg0) throws JMSException {
        return properties.containsKey(arg0);
    }

    @Override
    public void setBooleanProperty(String arg0, boolean arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setByteProperty(String arg0, byte arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setDoubleProperty(String arg0, double arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setFloatProperty(String arg0, float arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setIntProperty(String arg0, int arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setJMSCorrelationID(String arg0) throws JMSException {
        // TODO Auto-generated method stub
    }

    @Override
    public void setJMSCorrelationIDAsBytes(byte[] arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSDeliveryMode(int arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSDestination(Destination arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSExpiration(long arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSMessageID(String arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSPriority(int arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSRedelivered(boolean arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSReplyTo(Destination arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSTimestamp(long arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setJMSType(String arg0) throws JMSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setLongProperty(String arg0, long arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setObjectProperty(String arg0, Object arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setShortProperty(String arg0, short arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public void setStringProperty(String arg0, String arg1) throws JMSException {
        properties.put(arg0, arg1);
    }

    @Override
    public String getText() throws JMSException {
        return text;
    }

    @Override
    public void setText(String arg0) throws JMSException {
        this.text = arg0;
    }

    @Override
    public <T> T getBody(Class<T> arg0) throws JMSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getJMSDeliveryTime() throws JMSException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isBodyAssignableTo(Class arg0) throws JMSException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setJMSDeliveryTime(long arg0) throws JMSException {
        // TODO Auto-generated method stub
    }

}
