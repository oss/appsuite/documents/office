/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;

public class RT2MessageHelper {

    //-------------------------------------------------------------------------
    public static void checkRT2MessageHeader(RT2Message msg, String clientUid, String docUid) {
        assertNotNull(msg);

        var clientUidType = new RT2CliendUidType(clientUid);
        assertEquals(clientUid, msg.getHeader(RT2Protocol.HEADER_CLIENT_UID));
        assertEquals(clientUidType, msg.getClientUID());
        checkRT2MessageHeader(msg, docUid);
    }

    //-------------------------------------------------------------------------
    public static void checkRT2MessageHeader(RT2Message msg, String docUid) {
        assertNotNull(msg);

        var docUidType = new RT2DocUidType(docUid);
        assertEquals(docUid, msg.getHeader(RT2Protocol.HEADER_DOC_UID));
        assertEquals(docUidType, msg.getDocUID());
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRT2Message(String type, String clientUid, String docUid) {
        return RT2MessageFactory.newMessage(RT2MessageType.valueOfType(type), new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRT2Message(String type, String msgId, String clientUid, String docUid, String sessionId, String fileId, String folderId) throws JSONException {
        var json = createJsonObjectRT2Message(type, msgId, clientUid, docUid, sessionId, fileId, folderId);
        return RT2MessageFactory.fromJSON(json, false);
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRT2Message(String type, String msgId, String clientUid, String docUid, int seqNr, String sessionId, String fileId, String folderId) throws JSONException {
        var json = createJsonObjectRT2Message(type, msgId, clientUid, docUid, seqNr, sessionId);
        setFolderAndFileId(json, folderId, fileId);
        return RT2MessageFactory.fromJSON(json, false);
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRT2Message(String type, String msgId, String clientUid, String docUid, int seqNr, String sessionId) throws JSONException {
        var json = createJsonObjectRT2Message(type, msgId, clientUid, docUid, seqNr, sessionId);
        return RT2MessageFactory.fromJSON(json, false);
    }

    //-------------------------------------------------------------------------
    public static JSONObject createJsonObjectRT2Message(String type, String msgId, String clientUid, String docUid, int seqNr, String sessionId) throws JSONException {
        var rt2JsonObj = new JSONObject();
        var header = new JSONObject();
        header.put(RT2Protocol.HEADER_MSG_ID, msgId);
        header.put(RT2Protocol.HEADER_CLIENT_UID, clientUid);
        header.put(RT2Protocol.HEADER_DOC_UID, docUid);
        header.put(RT2Protocol.HEADER_SEQ_NR, seqNr);
        header.put(RT2Protocol.HEADER_SESSION_ID, sessionId);
        rt2JsonObj.put("header", header);
        rt2JsonObj.put("type", type);
        rt2JsonObj.put("body", new JSONObject());
        return rt2JsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createJsonObjectRT2Message(String type, String msgId, String clientUid, String docUid, String sessionId, String fileId, String folderId) throws JSONException {
        var rt2JsonObj = new JSONObject();
        var header = new JSONObject();
        header.put(RT2Protocol.HEADER_MSG_ID, msgId);
        header.put(RT2Protocol.HEADER_CLIENT_UID, clientUid);
        header.put(RT2Protocol.HEADER_DOC_UID, docUid);
        header.put(RT2Protocol.HEADER_SESSION_ID, sessionId);
        header.put(RT2Protocol.HEADER_FILE_ID, fileId);
        header.put(RT2Protocol.HEADER_FOLDER_ID, folderId);
        rt2JsonObj.put("header", header);
        rt2JsonObj.put("type", type);
        rt2JsonObj.put("body", new JSONObject());
        return rt2JsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createJsonObjectRT2Message(String type, String msgId, String clientUid, String docUid, int seqNr, String sessionId, String fileId, String folderId) throws JSONException {
        var rt2JsonObj = new JSONObject();
        var header = new JSONObject();
        header.put(RT2Protocol.HEADER_MSG_ID, msgId);
        header.put(RT2Protocol.HEADER_CLIENT_UID, clientUid);
        header.put(RT2Protocol.HEADER_DOC_UID, docUid);
        header.put(RT2Protocol.HEADER_SEQ_NR, seqNr);
        header.put(RT2Protocol.HEADER_SESSION_ID, sessionId);
        header.put(RT2Protocol.HEADER_FILE_ID, fileId);
        header.put(RT2Protocol.HEADER_FOLDER_ID, folderId);
        rt2JsonObj.put("header", header);
        rt2JsonObj.put("type", type);
        rt2JsonObj.put("body", new JSONObject());
        return rt2JsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createJsonObjectRT2Message(String type) throws JSONException {
        var rt2JsonObj = new JSONObject();
        var header = new JSONObject();
        rt2JsonObj.put("header", header);
        rt2JsonObj.put("type", type);
        rt2JsonObj.put("body", new JSONObject());
        return rt2JsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject addBodyString(JSONObject rt2JsonObj, String jsonBodyAsString) throws JSONException {
        var jsonBody = new JSONObject(jsonBodyAsString);
        rt2JsonObj.put("body", jsonBody);
        return rt2JsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject setFolderAndFileId(JSONObject jsonObj, String folderId, String fileId) throws JSONException {
        var setHeader = false;
        var header = jsonObj.optJSONObject("header");
        if (header == null) {
            header = new JSONObject();
            setHeader = true;
        }
        header.put(RT2Protocol.HEADER_FOLDER_ID, folderId);
        header.put(RT2Protocol.HEADER_FILE_ID, fileId);
        if (setHeader) {
            jsonObj.put("header", header);
        }
        return jsonObj;
    }

    //-------------------------------------------------------------------------
    public static JSONObject addHeader(JSONObject jsonObj, String headerName, Object value) throws JSONException {
        var setHeader = false;
        var header = jsonObj.optJSONObject("header");
        if (header == null) {
            header = new JSONObject();
            setHeader = true;
        }

        header.put(headerName, value);

        if (setHeader) {
            jsonObj.put("header", header);
        }
        return jsonObj;
    }
}
