/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.UUID;
import javax.jms.JMSException;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BodyType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageType;
import com.openexchange.office.rt2.protocol.utils.JmsMessageMock;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;

public class GpbMessageJmsPostProcessorTest {

    //-------------------------------------------------------------------------
    @Test
    public void testGpbMessageJmsPostProcessor() throws JMSException {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";

        var sMsgType = RT2MessageType.BROADCAST_OT_RELOAD;
        var broadcast = RT2MessageFactory.newBroadcastMessage(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), sMsgType);

        BroadcastMessage.Builder broadcastMsgBuilder = BroadcastMessage.newBuilder().setBody(BodyType.newBuilder().setValue(broadcast.getBodyString()).build())
            .setDocUid(DocUidType.newBuilder().setValue(broadcast.getDocUID().getValue()))
            .setMessageId(MessageIdType.newBuilder().setValue(broadcast.getMessageID().getValue()))
            .setMsgType(MessageType.BROADCAST_OT_RELOAD);
        var broadcastGpbMessage = broadcastMsgBuilder.build();

        var jmsMessageMock = new JmsMessageMock();
        var gpbMessageJmsPostProcessor = new GpbMessageJmsPostProcessor(broadcastGpbMessage);
        var jmsMessage = gpbMessageJmsPostProcessor.postProcessMessage(jmsMessageMock);
        var internalMsgType = RT2Protocol.HEADER_PREFIX_INTERNAL + ".type";

        assertNotNull(jmsMessage);
        assertEquals(broadcastGpbMessage.getMsgType().name(), jmsMessage.getStringProperty(internalMsgType));
        assertTrue(jmsMessage.getBooleanProperty(RT2MessagePostProcessor.HEADER_GPB_MSG));

        var jmsMessageMock2 = new JmsMessageMock();
        var gpbMessageJmsPostProcessor2 = new GpbMessageJmsPostProcessor();
        var jmsMessage2 = gpbMessageJmsPostProcessor2.postProcessMessage(jmsMessageMock2);

        assertNotNull(jmsMessage2);
        assertTrue(jmsMessage2.getBooleanProperty(RT2MessagePostProcessor.HEADER_GPB_MSG));
    }
}
