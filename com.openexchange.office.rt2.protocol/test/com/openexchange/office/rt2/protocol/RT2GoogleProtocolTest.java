/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import javax.validation.ValidationException;
import org.junit.jupiter.api.Test;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.AdminMessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BodyType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessage;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.BroadcastMessageReceiver;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ClientUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.DocUidType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ErrorCodeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageTimeType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.MessageType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.SequenceNrType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.ServerIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.SessionIdType;
import com.openexchange.office.rt2.protocol.RT2GoogleProtocol.VersionMessageResponse;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;

public class RT2GoogleProtocolTest {

    //-------------------------------------------------------------------------
    @Test
    public void testCreateGoogleProtocolInstances() throws ValidationException {
        var clientUid = UUID.randomUUID().toString();
        var docUid = UUID.randomUUID().toString();

        var rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_CRASHED, clientUid, docUid);
        var broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        var broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_HANGUP, clientUid, docUid);
        rMsg.setError(new RT2ErrorCodeType(ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR));
        var errorCodeAsString = rMsg.getError().getValue().getAsJSON().toString();
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMsgBuilder.setErrorCode(ErrorCodeType.newBuilder().setValue(errorCodeAsString));
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_RENAMED_RELOAD, clientUid, docUid);
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_SHUTDOWN, clientUid, docUid);
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_UPDATE, clientUid, docUid);
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_UPDATE_CLIENTS, clientUid, docUid);
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);

        rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_OT_RELOAD, clientUid, docUid);
        broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testAddReceiversToBroadcastMsg() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = UUID.randomUUID().toString();

        var rMsg = RT2MessageHelper.createRT2Message(RT2Protocol.BROADCAST_CRASHED, clientUid, docUid);
        var broadcastMsgBuilder = createBroadcastMsgBuilder(rMsg);
        broadcastMsgBuilder.addReceivers(
            BroadcastMessageReceiver.newBuilder()
                .setSeqNr(SequenceNrType.newBuilder().setValue(rMsg.hasSeqNumber() ? rMsg.getSeqNumber().getValue() : -1))
                .setReceiver(ClientUidType.newBuilder().setValue(rMsg.getClientUID().getValue())));

        var broadcastMessage = broadcastMsgBuilder.build();
        assertNotNull(broadcastMessage);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testAdminMessages() {
        var nodeUuid = UUID.randomUUID().toString();
        var adminRequest = AdminMessage.newBuilder()
            .setMsgType(AdminMessageType.UPDATE_CURR_DOC_PROCESSORS_REQUEST)
            .setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
            .setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
            .setOriginator(ServerIdType.newBuilder().setValue(nodeUuid))
            .build();

        assertNotNull(adminRequest);

        adminRequest = AdminMessage.newBuilder()
            .setMsgType(AdminMessageType.UPDATE_CURR_CLIENT_LIST_OF_DOC_PROCESSOR_REQUEST)
            .setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
            .setServerId(ServerIdType.newBuilder().setValue(nodeUuid))
            .setOriginator(ServerIdType.newBuilder().setValue(nodeUuid))
            .build();

        assertNotNull(adminRequest);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testAdminMessageDocProxyRemove() {
        var sessionId = "164ee5b0cc764e3d66d6da0dd3b1815a";
        var localMemberUuid = UUID.randomUUID().toString();
        var docUidsGpb = new HashSet<DocUidType>();
        var docUids = new ArrayList<RT2DocUidType>();
        docUids.add(new RT2DocUidType("3821ac4610c76353150d38de853b41ec"));
        docUids.add(new RT2DocUidType("3d396124ccfae05680939c38645cdc39"));

        docUids.forEach(d -> docUidsGpb.add(DocUidType.newBuilder().setValue(d.getValue()).build()));
        var adminRequestDocProxyRemoveBuilder = AdminMessage.newBuilder()
                .setMsgType(AdminMessageType.ADMIN_TASK_REMOVE_DOC_PROXIES_WITH_DOC_UUID)
                .setMessageTime(MessageTimeType.newBuilder().setValue(System.currentTimeMillis()))
                .setServerId(ServerIdType.newBuilder().setValue(localMemberUuid))
                .setSessionId(SessionIdType.newBuilder().setValue(sessionId).build())
                .setOriginator(ServerIdType.newBuilder().setValue(localMemberUuid));
        docUidsGpb.forEach(d -> adminRequestDocProxyRemoveBuilder.addDocProcessorsRemoved(d));
        var adminRequestDocProxyRemove = adminRequestDocProxyRemoveBuilder.build();

        assertNotNull(adminRequestDocProxyRemove);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testReadWriteAdminMessage() throws UnknownHostException {
        var localMemberUuid = UUID.randomUUID().toString();
        var originator = UUID.randomUUID().toString();
        var version = "unknown";
        VersionMessageResponse versMsg = VersionMessageResponse.newBuilder()
                .setHost(InetAddress.getLocalHost().getHostName())
                .setVersion(version)
                .setHostId(localMemberUuid)
                .setOriginator(originator)
                .setCountBackends(3)
                .build();

        var baOut = new ByteArrayOutputStream();
        try {
            versMsg.writeTo(baOut);
        } catch (IOException ex) {
            fail("Failed to write Google Protocol Buffer msg to output stream");
        }
    }

    //-------------------------------------------------------------------------
    private BroadcastMessage.Builder createBroadcastMsgBuilder(RT2Message rMsg) {
        return BroadcastMessage.newBuilder().setBody(BodyType.newBuilder().setValue(rMsg.getBodyString()).build())
            .setDocUid(DocUidType.newBuilder().setValue(rMsg.getDocUID().getValue()))
            .setMessageId(MessageIdType.newBuilder().setValue(rMsg.getMessageID().getValue()))
            .setMsgType(getMessageTypeOfRT2Message(rMsg.getType()));
    }

    //-------------------------------------------------------------------------
    private MessageType getMessageTypeOfRT2Message(RT2MessageType msgType) {
        switch (msgType) {
            case BROADCAST_CRASHED: return MessageType.BROADCAST_CRASHED;
            case BROADCAST_EDITREQUEST_STATE: return MessageType.BROADCAST_EDITREQUEST_STATE;
            case BROADCAST_HANGUP: return MessageType.BROADCAST_HANGUP;
            case BROADCAST_RENAMED_RELOAD: return MessageType.BROADCAST_RENAMED_RELOAD;
            case BROADCAST_SHUTDOWN: return MessageType.BROADCAST_SHUTDOWN;
            case BROADCAST_UPDATE: return MessageType.BROADCAST_UPDATE;
            case BROADCAST_UPDATE_CLIENTS: return MessageType.BROADCAST_UPDATE_CLIENTS;
            case BROADCAST_OT_RELOAD: return MessageType.BROADCAST_OT_RELOAD;
            default: throw new RuntimeException("Not a broadcast message type: " + msgType);
        }
    }
}
