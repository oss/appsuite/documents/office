/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.utils.JmsMessageMock;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;

public class RT2MessageFactoryTest {

    //-------------------------------------------------------------------------
    public RT2MessageFactoryTest() {}

    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp() throws Exception {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown() throws Exception {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewBroadcastMessage() {
        var docUid = "47541c5d87953415d9f646587ed40785";
        var broadcastShutdown = RT2MessageFactory.newBroadcastMessage(RT2MessageType.BROADCAST_SHUTDOWN, new RT2DocUidType(docUid));

        RT2MessageHelper.checkRT2MessageHeader(broadcastShutdown, docUid);

        var clientUid = UUID.randomUUID().toString();
        var broadcastCrashed = RT2MessageFactory.newBroadcastMessage(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), RT2MessageType.BROADCAST_CRASHED);

        RT2MessageHelper.checkRT2MessageHeader(broadcastCrashed, clientUid, docUid);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewLeaveRequest() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var leaveRequest = RT2MessageFactory.newLeaveRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));

        RT2MessageHelper.checkRT2MessageHeader(leaveRequest, clientUid, docUid);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewCloseDocRequest() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var closeDocRequest = RT2MessageFactory.newCloseDocRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid));

        RT2MessageHelper.checkRT2MessageHeader(closeDocRequest, clientUid, docUid);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewLeaveAndCloseRequest() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var cause = Cause.SESSION_INVALID;
        var leaveAndCloseRequest = RT2MessageFactory.newLeaveAndCloseRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), cause);

        RT2MessageHelper.checkRT2MessageHeader(leaveAndCloseRequest, clientUid, docUid);
        assertEquals(cause.name(), leaveAndCloseRequest.getHeader(RT2Protocol.HEADER_CAUSE));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewSimpleNackRequest() throws JSONException {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var seqNrs = createSeqNumbers(5, 6, 8, 9);

        var nackMsg = RT2MessageFactory.newSimpleNackRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), seqNrs);

        RT2MessageHelper.checkRT2MessageHeader(nackMsg, clientUid, docUid);
        var jsonBody = nackMsg.getBody();
        assertNotNull(jsonBody);
        var nackSeqNrs = jsonBody.getJSONArray(RT2Protocol.BODYPART_NACKS);
        assertEquals(seqNrs, nackSeqNrs.asList());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewUnavailabilityRequest() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var unavailTime = 30000L;
        var unavailRequest = RT2MessageFactory.newUnavailabilityRequest(new RT2CliendUidType(clientUid), new RT2DocUidType(docUid), new RT2UnavailableTimeType(unavailTime));

        RT2MessageHelper.checkRT2MessageHeader(unavailRequest, clientUid, docUid);
        assertEquals(Long.valueOf(unavailTime), unavailRequest.getHeader(RT2Protocol.HEADER_UNAVAIL_TIME));
        assertEquals(new RT2UnavailableTimeType(unavailTime), unavailRequest.getUnvailableTime());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testNewPong() {
        var pongMsg = RT2MessageFactory.newPong();

        assertNotNull(pongMsg);
        assertEquals(RT2MessageType.PONG, pongMsg.getType());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testFromJmsMessage() throws JSONException, JMSException {
        var jmsMessageProps = new HashMap<String, Object>();
        jmsMessageProps.put(RT2MessageJmsPostProcessor.HEADER_MSG_TYPE, RT2Protocol.REQUEST_JOIN);

        var body = new JSONObject();
        var jmsMessage = new JmsMessageMock(jmsMessageProps);
        jmsMessage.setText(body.toString());

        var rt2Msg = RT2MessageFactory.fromJmsMessage(jmsMessage);
        assertNotNull(rt2Msg);
        assertEquals(RT2MessageType.REQUEST_JOIN, rt2Msg.getType());
        assertEquals(new JSONObject(), rt2Msg.getBody());

        var badJsonJmsMessage = new JmsMessageMock(jmsMessageProps);
        badJsonJmsMessage.setText("");
        try {
            RT2MessageFactory.fromJmsMessage(badJsonJmsMessage);
            fail("bad json must throw JSONException");
        } catch (JSONException e) {
            // expected
        }

        var badJmsMessage = Mockito.mock(TextMessage.class);
        Mockito.when(badJmsMessage.getStringProperty(RT2MessageJmsPostProcessor.HEADER_MSG_TYPE)).thenReturn(RT2Protocol.REQUEST_JOIN);
        Mockito.when(badJmsMessage.getPropertyNames()).thenReturn(Collections.emptyEnumeration());
        Mockito.when(badJmsMessage.getText()).thenThrow(JMSException.class);
        try {
            RT2MessageFactory.fromJmsMessage(badJmsMessage);
            fail("JMSException must be thrown by mock code");
        } catch (JMSException e) {
            // expected
        }

        var rt2MsgOrig = RT2MessageFactory.newAdminMessage(RT2MessageType.BROADCAST_CRASHED);
        rt2MsgOrig.setBody(body);
        var rt2MsgToJmsPostProcessor = new RT2MessageJmsPostProcessor(rt2MsgOrig);
        var jmsMsg = new JmsMessageMock();

        jmsMsg = (JmsMessageMock)rt2MsgToJmsPostProcessor.postProcessMessage(jmsMsg);

        var rt2MsgFromJms = RT2MessageFactory.fromJmsMessage(jmsMsg);
        assertEquals(rt2MsgOrig.getMessageID(), rt2MsgFromJms.getMessageID());
        assertEquals(rt2MsgOrig.getBody(), rt2MsgFromJms.getBody());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCreateRequestFromMessage() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";

        var response = RT2MessageHelper.createRT2Message(RT2Protocol.RESPONSE_APPLY_OPS, clientUid, docUid);
        var request = RT2MessageFactory.createRequestFromMessage(response, RT2MessageType.REQUEST_APPLY_OPS);

        assertNotNull(request);
        assertEquals(RT2MessageType.REQUEST_APPLY_OPS, request.getType());
        assertEquals(new RT2CliendUidType(clientUid), request.getClientUID());
        assertEquals(new RT2DocUidType(docUid), request.getDocUID());
        assertNotEquals(request.getMessageID(), response.getMessageID());
    }

    //-------------------------------------------------------------------------
    @Test
    public void testCreateResponseFromMessage() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var reqSeqnr = 10;

        var request = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APPLY_OPS, clientUid, docUid);
        request.setSeqNumber(new RT2SeqNumberType(reqSeqnr));
        var errorResponse = RT2MessageFactory.createResponseFromMessage(request, RT2MessageType.RESPONSE_GENERIC_ERROR);

        var msgID = errorResponse.getHeader(RT2Protocol.HEADER_MSG_ID);
        var seqNr = errorResponse.getHeader(RT2Protocol.HEADER_SEQ_NR);

        // Response must share msgId to enable client to map response to request
        // TODO: check if this makes sense
        // The sequence number is also copied although receive/send seq numbers do
        // normally not correlate.
        assertEquals(msgID, request.getHeader(RT2Protocol.HEADER_MSG_ID));
        assertEquals(seqNr, request.getHeader(RT2Protocol.HEADER_SEQ_NR));

        RT2MessageGetSet.clearSeqNumber(errorResponse);
        seqNr = errorResponse.getHeader(RT2Protocol.HEADER_SEQ_NR);
        assertEquals(0, seqNr);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testToAndFromJSONString() throws JSONException {
        var type = RT2Protocol.REQUEST_OPEN_DOC;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var seqNr = 2;
        var sessionId = "c7151cbc1363cb8225a951cd791fdd63";
        var fileId = "1234";
        var folderId = "567";

        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, seqNr, sessionId, fileId, folderId);
        var requestAsJsonString = RT2MessageFactory.toJSONString(request);

        var requestFromJsonString = RT2MessageFactory.fromJSONString(requestAsJsonString);
        assertEquals(request, requestFromJsonString);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testToAndFromJSON() throws Exception {
        var type = RT2Protocol.REQUEST_OPEN_DOC;
        var msgId = UUID.randomUUID().toString();
        var clientUid = UUID.randomUUID().toString();
        var docUid = "47541c5d87953415d9f646587ed40785";
        var seqNr = 2;
        var sessionId = "c7151cbc1363cb8225a951cd791fdd63";
        var fileId = "1234";
        var folderId = "567";

        var request = RT2MessageHelper.createRT2Message(type, msgId, clientUid, docUid, seqNr, sessionId, fileId, folderId);
        var requestAsJsonEncoded = RT2MessageFactory.toJSON(request);
        var requestAsJsonDecoded = RT2MessageFactory.toJSON(request, false);

        var requestFromJson = RT2MessageFactory.fromJSON(requestAsJsonEncoded, true);
        assertEquals(request, requestFromJson);

        requestFromJson = RT2MessageFactory.fromJSON(requestAsJsonDecoded, false);
        assertEquals(request, requestFromJson);

        request.setHeader(RT2Protocol.HEADER_INTERNAL_FORCE, true);
        request.setHeader("JMSMessageId", UUID.randomUUID().toString());
        requestAsJsonEncoded = RT2MessageFactory.toJSON(request);
        requestFromJson = RT2MessageFactory.fromJSON(requestAsJsonEncoded, true);

        // fromJSON filters out internal and jms header automatically
        assertNull(requestFromJson.getHeader(RT2Protocol.HEADER_INTERNAL_FORCE));
        assertNull(requestFromJson.getHeader("JMSMessageId"));
    }

    //-------------------------------------------------------------------------
    private Collection<Integer> createSeqNumbers(int... seqNrs) {
        var result = new ArrayList<Integer>();
        if (seqNrs != null) {
            for (int i=0; i < seqNrs.length; i++) {
                result.add(Integer.valueOf(seqNrs[i]));
            }
        }
        return result;
    }

}
