/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

public class RT2MessageFormatterTest {

    //-------------------------------------------------------------------------
    @Test
    public void testFormatForDefaultOutput() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var rt2Msg = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_EMERGENCY_LEAVE, clientUid, docUid);

        var output = RT2MessageFormatter.formatForDefaultOutput(rt2Msg);
        assertNotNull(output);
        assertTrue(StringUtils.isNotEmpty(output));

        var rt2Msgs = new ArrayList<RT2Message>();
        rt2Msgs.add(rt2Msg);
        var outputColl = RT2MessageFormatter.formatForDefaultOutput(rt2Msgs);
        assertNotNull(outputColl);
        assertTrue(StringUtils.isNotEmpty(outputColl));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testFormatForOutput() {
        var clientUid = UUID.randomUUID().toString();
        var docUid = "ac27e16bae61e2953725de45f6c05338";
        var rt2Msg = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_EMERGENCY_LEAVE, clientUid, docUid);

        var outputSomeProps = RT2MessageFormatter.formatForOutput(rt2Msg, RT2Protocol.HEADER_MSG_ID, RT2Protocol.HEADER_CLIENT_UID);
        assertNotNull(outputSomeProps);
        assertTrue(StringUtils.isNotEmpty(outputSomeProps));

        var rt2Msgs = new ArrayList<RT2Message>();
        rt2Msgs.add(rt2Msg);
        var outputCollSomeProps = RT2MessageFormatter.formatForOutput(rt2Msgs, RT2Protocol.HEADER_MSG_ID, RT2Protocol.HEADER_CLIENT_UID);
        assertNotNull(outputCollSomeProps);
        assertTrue(StringUtils.isNotEmpty(outputCollSomeProps));

        var clientUid2 = UUID.randomUUID().toString();
        var docUid2 = "ac27e16bae61e2953725de45f6c05338";
        var rt2Msg2 = RT2MessageHelper.createRT2Message(RT2Protocol.REQUEST_APP_ACTION, clientUid2, docUid2);
        rt2Msgs.add(rt2Msg2);
        var outputCollSomeMsgsWithSomeProps = RT2MessageFormatter.formatForOutput(rt2Msgs, 1, RT2Protocol.HEADER_MSG_ID, RT2Protocol.HEADER_CLIENT_UID);
        assertNotNull(outputCollSomeMsgsWithSomeProps);
        assertTrue(StringUtils.isNotEmpty(outputCollSomeMsgsWithSomeProps));
    }
}
