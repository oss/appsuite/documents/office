/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.HashSet;
import org.junit.jupiter.api.Test;

public class RT2MessageFlagsTest {

    //-------------------------------------------------------------------------
    @Test
    public void testMapFlagToString() {
        var flagNone = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_NONE);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_NONE), flagNone);

        var flagAll = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_ALL);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_ALL), flagAll);

        var flagSimpleMsg = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_SIMPLE_MSG);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_SIMPLE_MSG), flagSimpleMsg);

        var flagSessionBased = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_SESSION_BASED);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_SESSION_BASED), flagSessionBased);

        var flagSeqNrBased = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_SEQUENCE_NR_BASED), flagSeqNrBased);

        var flagChunkBased = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_CHUNK_BASED);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_CHUNK_BASED), flagChunkBased);

        var flagDocBased = RT2MessageFlags.mapFlagsToString(RT2MessageFlags.FLAG_DOC_BASED);
        assertEquals(toFlagString(RT2MessageFlags.FLAG_STR_DOC_BASED), flagDocBased);

        try {
            RT2MessageFlags.mapFlagsToString(256);
            fail("Unknown flag must throw!");
        } catch (UnsupportedOperationException ex) {
            // expected
        }
    }

    //-------------------------------------------------------------------------
    @Test
    public void testMapFlagFieldToString() throws Exception {
        var flagFieldString = RT2MessageFlags.mapFlagFieldToString(RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
        verifyStringContainsSubStrings(flagFieldString, RT2MessageFlags.FLAG_STR_SIMPLE_MSG, RT2MessageFlags.FLAG_STR_SEQUENCE_NR_BASED);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSplitFlagFieldToFlags() throws Exception {
        var flags = RT2MessageFlags.splitFlagFieldToFlags(RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED);
        assertEquals(2, flags.length);
        assertTrue((flags[0] & RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED) != 0);
        assertFalse((flags[0] & RT2MessageFlags.FLAG_CHUNK_BASED+RT2MessageFlags.FLAG_DOC_BASED) != 0);
        assertTrue((flags[1] & RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED) != 0);
        assertFalse((flags[1] & RT2MessageFlags.FLAG_CHUNK_BASED+RT2MessageFlags.FLAG_DOC_BASED) != 0);

        flags = RT2MessageFlags.splitFlagFieldToFlags(RT2MessageFlags.FLAG_SESSION_BASED+RT2MessageFlags.FLAG_CHUNK_BASED);
        assertEquals(2, flags.length);
        assertTrue((flags[0] & RT2MessageFlags.FLAG_SESSION_BASED+RT2MessageFlags.FLAG_CHUNK_BASED) != 0);
        assertFalse((flags[0] & RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED) != 0);
        assertTrue((flags[1] & RT2MessageFlags.FLAG_SESSION_BASED+RT2MessageFlags.FLAG_CHUNK_BASED) != 0);
        assertFalse((flags[1] & RT2MessageFlags.FLAG_SIMPLE_MSG+RT2MessageFlags.FLAG_SEQUENCE_NR_BASED) != 0);

        flags = RT2MessageFlags.splitFlagFieldToFlags(RT2MessageFlags.FLAG_ALL);
        assertEquals(1, flags.length);
        assertEquals(flags[0], RT2MessageFlags.FLAG_ALL);

        flags = RT2MessageFlags.splitFlagFieldToFlags(RT2MessageFlags.FLAG_NONE);
        assertEquals(1, flags.length);
        assertEquals(flags[0], RT2MessageFlags.FLAG_NONE);

        var allFlagsSet =
            RT2MessageFlags.FLAG_SIMPLE_MSG + RT2MessageFlags.FLAG_SESSION_BASED +
            RT2MessageFlags.FLAG_SEQUENCE_NR_BASED + RT2MessageFlags.FLAG_CHUNK_BASED +
            RT2MessageFlags.FLAG_DOC_BASED;

        var useAllFlags = RT2MessageFlags.splitFlagFieldToFlags(allFlagsSet);
        assertEquals(5, useAllFlags.length);
        verifyIntArrayContains(useAllFlags,
            RT2MessageFlags.FLAG_SIMPLE_MSG, RT2MessageFlags.FLAG_SESSION_BASED,
            RT2MessageFlags.FLAG_SEQUENCE_NR_BASED, RT2MessageFlags.FLAG_CHUNK_BASED,
            RT2MessageFlags.FLAG_DOC_BASED);
    }

    //-------------------------------------------------------------------------
    private String toFlagString(String flagName) {
        return "[" + flagName + "]";
    }

    //-------------------------------------------------------------------------
    private void verifyStringContainsSubStrings(String flagField, String...expectedFlagNames) {
        for (var flagName : expectedFlagNames) {
            if (!flagField.contains(flagName)) {
                fail("Flag " + flagName + " not found!");
            }
        }
    }

    //-------------------------------------------------------------------------
    private void verifyIntArrayContains(int[] numbers, int...expectedNumbers) {
        var numSet1 = new HashSet<Integer>();
        for (var n : numbers) {
            numSet1.add(n);
        }
        var numSet2 = new HashSet<Integer>();
        for (var n : expectedNumbers) {
            numSet2.add(n);
        }
        assertEquals(numSet1, numSet2);
    }

}
