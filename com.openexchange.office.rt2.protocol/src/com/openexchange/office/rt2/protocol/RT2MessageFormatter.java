/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RT2MessageFormatter {

	public static final String MSG_TYPE = "msg_type";

    public static final List<String> DEFAULT_LOG_FORMAT;
    static {
        final List<String> tmp = new ArrayList<>();
        tmp.add(RT2Protocol.HEADER_MSG_ID);
        tmp.add(RT2Protocol.HEADER_CLIENT_UID);
        tmp.add(RT2Protocol.HEADER_DOC_UID);
        tmp.add(MSG_TYPE);
        tmp.add(RT2Protocol.HEADER_SEQ_NR);

        DEFAULT_LOG_FORMAT = Collections.unmodifiableList(tmp);
    }


	private RT2MessageFormatter() {
		// nothing to do
	}

	public static String formatForDefaultOutput(RT2Message msg) {
		final StringBuilder tmp = new StringBuilder("RT2Message: ");
		DEFAULT_LOG_FORMAT.stream().forEach(p -> {
			tmp.append(p);
			tmp.append(": ");
			tmp.append(getMessageValueFromName(msg, p));
			tmp.append(", ");
		});
		return tmp.toString();
	}

	public static String formatForDefaultOutput(Collection<RT2Message> msgs) {
		final StringBuilder tmp = new StringBuilder("List of RT2Messages: ");
		msgs.stream().forEach(m -> {
			tmp.append(formatForDefaultOutput(m));
		});
		return tmp.toString();
	}

	public static String formatForOutput(RT2Message msg, String... propsToOutput) {
		StringBuilder tmp = new StringBuilder("RT2Message: ");
		for (String p : propsToOutput)
			appendMessageValue(tmp, msg, p);
		return tmp.toString();
	}

	public static String formatForOutput(Collection<RT2Message> msgs, String... propsToOutput) {
		final StringBuilder tmp = new StringBuilder("List of RT2Messages: ");
		msgs.stream().forEach(m -> {
			tmp.append(formatForOutput(m, propsToOutput));
		});
		return tmp.toString();
	}

	public static String formatForOutput(Collection<RT2Message> msgs, int maxCount, String... propsToOutput) {
		final StringBuilder tmp = new StringBuilder("List of non-ACKed RT2Messages: ");
		msgs.stream().limit(Math.min(msgs.size(), maxCount))	
		             .forEach(m -> { 
		                  tmp.append(formatForOutput(m, propsToOutput));
		              });
		return tmp.toString();
	}

	private static void appendMessageValue(StringBuilder b, RT2Message msg, String propName) {
		b.append(propName);
		b.append(": ");
		b.append(getMessageValueFromName(msg, propName));
		b.append(", ");
	}

	private static String getMessageValueFromName(RT2Message msg, String propName) {
		if (MSG_TYPE.equalsIgnoreCase(propName))
			return msg.getType().toString();

		return toString(msg.getHeaderUnchecked(propName));
	}

	private static String toString(Object o) {
		return (o == null) ? "null" : o.toString();
	}
}
