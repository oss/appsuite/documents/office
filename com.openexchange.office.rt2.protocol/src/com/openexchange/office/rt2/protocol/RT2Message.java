/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.jms.Message;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.internal.EDocState;
import com.openexchange.office.rt2.protocol.value.RT2AdminHzMemberUuidType;
import com.openexchange.office.rt2.protocol.value.RT2AdminHzUuidType;
import com.openexchange.office.rt2.protocol.value.RT2AdminIdType;
import com.openexchange.office.rt2.protocol.value.RT2AppActionType;
import com.openexchange.office.rt2.protocol.value.RT2AuthCodeType;
import com.openexchange.office.rt2.protocol.value.RT2AutoCloseType;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocTypeType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2DriveDocIdType;
import com.openexchange.office.rt2.protocol.value.RT2ErrorCodeType;
import com.openexchange.office.rt2.protocol.value.RT2FastEmptyOsnType;
import com.openexchange.office.rt2.protocol.value.RT2FastEmptyType;
import com.openexchange.office.rt2.protocol.value.RT2FileIdType;
import com.openexchange.office.rt2.protocol.value.RT2FolderIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageIdType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2NoRestoreType;
import com.openexchange.office.rt2.protocol.value.RT2OldClientUuidType;
import com.openexchange.office.rt2.protocol.value.RT2PreviewType;
import com.openexchange.office.rt2.protocol.value.RT2RecipientListType;
import com.openexchange.office.rt2.protocol.value.RT2SeqNumberType;
import com.openexchange.office.rt2.protocol.value.RT2SessionIdType;
import com.openexchange.office.rt2.protocol.value.RT2StorageOsnType;
import com.openexchange.office.rt2.protocol.value.RT2StorageVersionType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;

//=============================================================================
public class RT2Message implements Comparable<RT2Message>
{
	private static final Logger log = LoggerFactory.getLogger(RT2Message.class);
	
    //-------------------------------------------------------------------------
    private final String type;

    //-------------------------------------------------------------------------
    private int flags = 0;

    //-------------------------------------------------------------------------
    private Map< String, Object > header = new HashMap<>();

    //-------------------------------------------------------------------------
    private JSONObject body = null;

    //-------------------------------------------------------------------------
    // force using of RT2MessageFactory methods for creating new message instances
    protected RT2Message (final RT2MessageType type) {
        this.type = type.name();
    }

    //-------------------------------------------------------------------------
    public static void cleanHeaderBeforeSendToClient (final RT2Message aMessage) {
        cleanInternalHeader (aMessage);

        aMessage.setHeaderWithoutCheck("breadcrumbId"                 , null); // camel specific header
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_SESSION_ID  , null);
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_DOC_STATE   , null);
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_DRIVE_DOC_ID, null);
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_FOLDER_ID   , null);
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_FILE_ID     , null);
    }

    //-------------------------------------------------------------------------
    public static void cleanHeaderBeforeSendToServer (final RT2Message aMessage) {
        cleanInternalHeader (aMessage);
    }

    //-------------------------------------------------------------------------
    public static void cleanInternalHeader (final RT2Message aMessage) {
        aMessage.setHeaderWithoutCheck(RT2Protocol.HEADER_INTERNAL_RECIPIENTS, null);
    }

    //-------------------------------------------------------------------------
    public static void copyHeaderWithprefix (final RT2Message aFrom, final RT2Message aTo, final String sPrefix) {
    	final Set< String > lHeader = aFrom.listHeader();
    	for (final String sHeader : lHeader) {
    		if ( ! StringUtils.startsWith(sHeader, sPrefix))
    			continue;

    		final Object aValue = aFrom.getHeader(sHeader);
    		aTo.setHeader(sHeader, aValue);
    	}
    }

    //-------------------------------------------------------------------------
    public static void copyAllHeader (final RT2Message aFrom, final RT2Message aTo  ) {
        final Set< String > lHeader = aFrom.listHeader();
        for (final String sHeader : lHeader) {
            final Object aValue = aFrom.getHeader(sHeader);
            aTo.setHeader(sHeader, aValue);
        }
    }

    //-------------------------------------------------------------------------
    public static void copySomeHeader (final RT2Message aFrom, final RT2Message aTo, final String...  lHeader) {
        for (final String sHeader : lHeader) {
        	final Object aValue = aFrom.getHeader (sHeader);
        	aTo.setHeader (sHeader, aValue);
        }
    }

    //-------------------------------------------------------------------------
    public static void copyBody (final RT2Message aFrom, final RT2Message aTo) {
        final JSONObject aBody = aFrom.getBody();
        aTo.setBody (aBody);
    }

    //-------------------------------------------------------------------------
    public boolean isType (final RT2MessageType otherType) {
        final boolean bIsType = getType().equals(otherType);
        return bIsType;
    }

    //-------------------------------------------------------------------------
    public RT2MessageType getType () {
    	return RT2MessageType.valueOf(type);
    }

    //-------------------------------------------------------------------------
    public RT2MessageIdType getMessageID () {
        final String sMsgId = impl_getHeader(RT2Protocol.HEADER_MSG_ID, null);
        return new RT2MessageIdType(sMsgId);
    }

    //-------------------------------------------------------------------------
    public void setMessageID (final RT2MessageIdType sMsgId) {
        setHeaderWithoutCheck(RT2Protocol.HEADER_MSG_ID, sMsgId.getValue());
    }

    //-------------------------------------------------------------------------
    public void newMessageID () {
        setMessageID(new RT2MessageIdType());
    }

    //-------------------------------------------------------------------------
    public void setFlags (final int... lFlags) {
        int nFlags = 0;
        for (int nFlag : lFlags)
            nFlags |= nFlag;
        flags = nFlags;
    }

    //-------------------------------------------------------------------------
    public boolean hasFlags (final int... lFlags) {
        Validate.notNull(lFlags, "Invalid argument 'flags'.");

        final int nFlags = flags;

        for (int nFlag : lFlags)
        {
            final boolean bIsFlag = RT2MessageFlags.isFlag(nFlags, nFlag);
            if ( ! bIsFlag)
                return false;
        }

        return true;
    }

    //-------------------------------------------------------------------------
    public Set< String > listHeader () {
    	final Set< String > lHeader = new HashSet<> ();
  		lHeader.addAll (header.keySet());
    	return lHeader;
    }

    //-------------------------------------------------------------------------    
    public Map<String, Object> getHeader() {
    	return new HashMap<>(header);
    }
    
    //-------------------------------------------------------------------------
    public boolean hasHeader (final String sHeader) {
        return this.header.containsKey(sHeader);
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public < T > T getHeader (final String sHeader) {
        final int nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        final T   aValue = (T) getHeader2 (sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlags);
    	return aValue;
    }

    //-------------------------------------------------------------------------
    public < T > T getHeader2 (final String sHeader, final int nMissCheck, final int... lFlags    ) {
        if ( ! hasFlags(lFlags))
            throw new ValidationException ("Message ["+getType()+"] dont support requested flags ["+RT2MessageFlags.mapFlagsToString(lFlags)+"] on getting header ["+sHeader+"].");

        final T aValue = impl_getHeader(sHeader, null);
        if (aValue != null)
            return aValue;

        if ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY)
            throw new ValidationException ("Message ["+getType()+"] miss mandatory header ["+sHeader+"]. message={"+this+"}");

        if (
            ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) &&
            ( ! RT2MessageFlags.areFlagsNone(lFlags)                      )
           )
        {
            throw new ValidationException ("Message ["+getType()+"] miss header ["+sHeader+"] forced by flags. message={"+this+"}");
        }

        return (T) null;
    }

    //-------------------------------------------------------------------------
    public <T> T getHeaderUnchecked(final String header) {
        return impl_getHeader(header, null);
    }

    //-------------------------------------------------------------------------
    public < T > void setHeader (final String sHeader, final T      aValue ) {
        final int nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        setHeader2 (sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlags);
    }
    
    //-------------------------------------------------------------------------
    public < T > void setHeader2 (final String sHeader, final int    nMissCheck, final T aValue, final int... lFlags) {
        if ( ! hasFlags(lFlags))
            throw new ValidationException("Message ["+getType()+"] dont support requested flags ["+RT2MessageFlags.mapFlagsToString(lFlags)+"] on setting header ["+sHeader+"].");

        if (aValue != null) {
            setHeaderWithoutCheck(sHeader, aValue);
            return;
        }

        if ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY)
            throw new ValidationException ("Message ["+getType()+"] miss value for setting mandatory header ["+sHeader+"].");

        if (
            ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) &&
            ( ! RT2MessageFlags.areFlagsNone(lFlags)                            )
           )
        {
            throw new ValidationException ("Message ["+getType()+"] miss value for setting header ["+sHeader+"] forced by flags.");
        }
    }

    //----------------------------------------------------------------------
    public void mergeHeader (final Map< String, Object > lOutsideHeader, Message jmsMessage) {
        final Iterator< String > rHeader = lOutsideHeader.keySet ().iterator ();
        while (rHeader.hasNext ())
        {
            final String sHeader = rHeader       .next ();
            final Object aValue  = lOutsideHeader.get  (sHeader);
            setHeaderWithoutCheck(sHeader, aValue);
            try {
            	RT2Protocol.get().getFlag4Header(sHeader);
            } catch (Exception ex) {
            	log.warn("Unknow header {} with value {} found in jms message {}.", sHeader, aValue, jmsMessage.toString());
            	setHeaderWithoutCheck(sHeader, null);
            }
        }
    };

    //-------------------------------------------------------------------------
    public JSONObject getBody () {
    	return impl_getBodyJSON();
    }

    //-------------------------------------------------------------------------
    public String getBodyString () {
        return impl_getBodyString();
    }

    //-------------------------------------------------------------------------
    public void setBody (final JSONObject aBody) {
    	impl_setBodyJSON (aBody);
    }

    //-------------------------------------------------------------------------
    public void setBodyString (final String sBody) throws JSONException {
        final String     sNormBody = StringUtils.defaultString(sBody, "{}");
        final JSONObject aBody     = new JSONObject (sNormBody);
        impl_setBodyJSON (aBody);
    }

    //-------------------------------------------------------------------------
    public boolean isValid (final StringBuffer sReason) {
    	if (getType() == null)
    	{
    		sReason.append ("'type' not defined");
    		return false;
    	}

    	if (getMessageID() == null)
    	{
    		sReason.append ("'msg_id' not defined");
    		return false;
    	}

    	return true;
    }

    //-------------------------------------------------------------------------
    public boolean isSimpleMessage () {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SIMPLE_MSG));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isSessionMessage ()
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SESSION_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isSequenceMessage ()
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SEQUENCE_NR_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isDocMessage ()
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_DOC_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isChunkMessage ()
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_CHUNK_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isFinalMessage ()
    {
        final boolean bIs = ( ! hasFlags (RT2MessageFlags.FLAG_CHUNK_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    @Override
    public String toString ()
    {
    	return getHeader().toString();
    }

	//-------------------------------------------------------------------------
	@Override
	public int compareTo(RT2Message o)
	{
		try
		{
			final Integer nSeq1Obj = RT2MessageGetSet.getSeqNumber(this);
			final Integer nSeq2Obj = RT2MessageGetSet.getSeqNumber(o);
			final int nSeq1 = (nSeq1Obj == null) ? 0 : nSeq1Obj;
			final int nSeq2 = (nSeq2Obj == null) ? 0 : nSeq2Obj;
			return (nSeq1 == nSeq2) ? 0 : ((nSeq1 < nSeq2) ? -1 : 1);
		}
		catch (Exception e)
		{
			throw new RuntimeException("RT2: Exception caught while trying to compare sequence numbers!", e);
		}
	}

    //-------------------------------------------------------------------------
    public < T > void setHeaderWithoutCheck (final String sHeader, final T      aValue ) {
        final Map< String, Object > lHeader = header;
        if (aValue == null)
            lHeader.remove(sHeader);
        else
            lHeader.put(sHeader, aValue);
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    private < T > T impl_getHeader (final String sHeader ,
                                    final T      aDefault)
    {
        T aValue = null;

//        if (m_aCamelMsg != null)
//            aValue = (T) m_aCamelMsg.getHeader(sHeader);
//        else
            aValue = (T) header.get(sHeader);

        if (aValue == null)
            aValue = aDefault;

        return aValue;
    }

    //-------------------------------------------------------------------------
    private void impl_setBodyJSON (final JSONObject aBody)
    {
    	body = aBody;
    }

    //-------------------------------------------------------------------------
    private JSONObject impl_getBodyJSON ()
    {
    	return mem_BodyJSON();
    }

    //-------------------------------------------------------------------------
    private String impl_getBodyString ()
    {
    	return mem_BodyJSON().toString();
    }

    //-------------------------------------------------------------------------
    private JSONObject mem_BodyJSON ()
    {
        if (body == null)
        {
            // use camel message to initialize body json object
//            if (m_aCamelMsg != null)
//                m_aBodyJSON = new JSONObject((String)m_aCamelMsg.getBody());
//            else
                body = new JSONObject();
        }

        return body;
    }

    public void setSessionID(final RT2SessionIdType sessionId) {
        RT2MessageGetSet.setSessionID(this, sessionId.getValue());
    }

    //-------------------------------------------------------------------------
    public void clearSessionID() {
    	RT2MessageGetSet.setSessionID(this, null);
    }
    
    public RT2SessionIdType getSessionID() {
        return new RT2SessionIdType(RT2MessageGetSet.getSessionID(this));
    }

    //-------------------------------------------------------------------------
    public RT2SeqNumberType getSeqNumber() {
        return new RT2SeqNumberType(RT2MessageGetSet.getSeqNumber(this));
    }

    //-------------------------------------------------------------------------
    public boolean hasSeqNumber() {
        return RT2MessageGetSet.hasSeqNumber(this);
    }

    //-------------------------------------------------------------------------
    public void setSeqNumber(RT2SeqNumberType nr) {
        RT2MessageGetSet.setSeqNumber(this, nr.getValue());
    }

    //-------------------------------------------------------------------------
    public void clearSeqNumber() {
        RT2MessageGetSet.clearSeqNumber(this);
    }

     //-------------------------------------------------------------------------
    public void setClientUID(final RT2CliendUidType clientUid) {
        RT2MessageGetSet.setClientUID(this, clientUid.getValue());
    }

     //-------------------------------------------------------------------------
    public RT2CliendUidType getClientUID() {
        return new RT2CliendUidType(RT2MessageGetSet.getClientUID(this));
    }

    //-------------------------------------------------------------------------
    public void setOldClientUID(final RT2OldClientUuidType clientUID) {
        RT2MessageGetSet.setOldClientUID(this, clientUID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2OldClientUuidType getOldClientUID() {
        return new RT2OldClientUuidType(RT2MessageGetSet.getOldClientUID(this));
    }

    //-------------------------------------------------------------------------
    public void setDriveDocID(final RT2DriveDocIdType driveDocID) {
        RT2MessageGetSet.setDriveDocID(this, driveDocID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2DriveDocIdType getDriveDocID() {
        return new RT2DriveDocIdType(RT2MessageGetSet.getDriveDocID(this));
    }

    //-------------------------------------------------------------------------
    public void setFolderID(final RT2FolderIdType folderID) {
        RT2MessageGetSet.setFolderID(this, folderID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2FolderIdType getFolderID() {
        return new RT2FolderIdType(RT2MessageGetSet.getFolderID(this));
    }

    //-------------------------------------------------------------------------
    public void setFileID(final RT2FileIdType fileID) {
        RT2MessageGetSet.setFileID(this, fileID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2FileIdType getFileID() {
        return new RT2FileIdType(RT2MessageGetSet.getFileID(this));
    }

    //-------------------------------------------------------------------------
    public void setDocUID(final RT2DocUidType docUID) {
        RT2MessageGetSet.setDocUID(this, docUID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2DocUidType getDocUID() {
        return new RT2DocUidType(RT2MessageGetSet.getDocUID(this));
    }

    //-------------------------------------------------------------------------
    public void setDocType(final RT2DocTypeType docType) {
        RT2MessageGetSet.setDocType(this, docType.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2DocTypeType getDocType() {
        return new RT2DocTypeType(RT2MessageGetSet.getDocType(this));
    }

    //-------------------------------------------------------------------------
    public void setDocState(final EDocState state) {
        RT2MessageGetSet.setDocState(this, state);
    }

    //-------------------------------------------------------------------------
    public EDocState getDocState() {
        return RT2MessageGetSet.getDocState(this);
    }

    //-------------------------------------------------------------------------
    public void markAsPreview() {
        RT2MessageGetSet.markAsPreview(this);
    }

    //-------------------------------------------------------------------------
    public RT2PreviewType isPreview() {
        return new RT2PreviewType(RT2MessageGetSet.isPreview(this));
    }

    //-------------------------------------------------------------------------
    public void setFastEmpty(final RT2FastEmptyType fastEmpty) {
        RT2MessageGetSet.setFastEmpty(this, fastEmpty.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2FastEmptyType getFastEmpty() {
        return new RT2FastEmptyType(RT2MessageGetSet.getFastEmpty(this));
    }

    //-------------------------------------------------------------------------
    public void setFastEmptyOSN(final RT2FastEmptyOsnType fastEmptyOSN) {
        RT2MessageGetSet.setFastEmptyOSN(this, fastEmptyOSN.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2AuthCodeType getAuthCode() {
        return new RT2AuthCodeType(RT2MessageGetSet.getAuthCode(this));
    }

    //-------------------------------------------------------------------------
    public void setAuthCode(final RT2AuthCodeType authCode) {
        RT2MessageGetSet.setAuthCode(this, authCode.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2FastEmptyOsnType getFastEmptyOSN() {
        return new RT2FastEmptyOsnType(RT2MessageGetSet.getFastEmptyOSN(this));
    }

    //-------------------------------------------------------------------------
    public Boolean isNewDocState() {
        return RT2MessageGetSet.getNewDocState(this);
    }

    //-------------------------------------------------------------------------
    public RT2StorageVersionType getStorageVersion() {
        return new RT2StorageVersionType(RT2MessageGetSet.getStorageVersion(this));
    }

    //-------------------------------------------------------------------------
    public RT2StorageOsnType getStorageOSN() {
        return new RT2StorageOsnType(RT2MessageGetSet.getStorageOSN(this));
    }

    //-------------------------------------------------------------------------
    public RT2UnavailableTimeType getUnvailableTime() {
        return new RT2UnavailableTimeType(RT2MessageGetSet.getUnvailableTime(this));
    }

    //-------------------------------------------------------------------------
    public void setUnvailableTime(final RT2UnavailableTimeType unavailableTime) {
        RT2MessageGetSet.setUnvailableTime(this, unavailableTime.getValue());
    }

    //-------------------------------------------------------------------------
    public void setAppAction(final RT2AppActionType action) {
        RT2MessageGetSet.setAppAction(this, action.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2AppActionType getAppAction() {
        return new RT2AppActionType(RT2MessageGetSet.getAppAction(this));
    }

    //-------------------------------------------------------------------------
    public void setAdminID(final RT2AdminIdType adminID) {
        RT2MessageGetSet.setAdminID(this, adminID.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2AdminIdType getAdminID() {
        return new RT2AdminIdType(RT2MessageGetSet.getAdminID(this));
    }

    //-------------------------------------------------------------------------
    public void setAdminHZMasterUUID(final RT2AdminHzUuidType hZMasterUUID) {
        RT2MessageGetSet.setAdminHZMasterUUID(this, hZMasterUUID.getValue().toString());
    }

    //-------------------------------------------------------------------------
    public RT2AdminHzUuidType getAdminHZMasterUUID() {
        return new RT2AdminHzUuidType(UUID.fromString(RT2MessageGetSet.getAdminHZMasterUUID(this)));
    }

    //-------------------------------------------------------------------------
    public void setAdminHZMemberUUID(final RT2AdminHzMemberUuidType hZMemberUUID) {
        RT2MessageGetSet.setAdminHZMemberUUID(this, hZMemberUUID.getValue().toString());
    }

    //-------------------------------------------------------------------------
    public RT2AdminHzMemberUuidType getAdminHZMemberUUID() {
        return new RT2AdminHzMemberUuidType(UUID.fromString(RT2MessageGetSet.getAdminHZMemberUUID(this)));
    }

    //-------------------------------------------------------------------------
    public void setAutoClose(RT2AutoCloseType autoClose) {
        RT2MessageGetSet.setAutoClose(this, autoClose.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2AutoCloseType getAutoClose() {
        return new RT2AutoCloseType(RT2MessageGetSet.getAutoClose(this));
    }

    //-------------------------------------------------------------------------
    public void setNoRestore(RT2NoRestoreType noRestore) {
        RT2MessageGetSet.setNoRestore(this, noRestore.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2NoRestoreType getNoRestore() {
        return new RT2NoRestoreType(RT2MessageGetSet.getNoRestore(this));
    }

    //-------------------------------------------------------------------------
    public void setError(final RT2ErrorCodeType errorCode) {
        RT2MessageGetSet.setError(this, errorCode.getValue());
    }

    //-------------------------------------------------------------------------
    public RT2ErrorCodeType getError() {
        return new RT2ErrorCodeType(RT2MessageGetSet.getError(this));
    }

    //-------------------------------------------------------------------------
    public boolean isError() {
        return RT2MessageGetSet.isError(this);
    }

    //-------------------------------------------------------------------------
    public void setRecipients(final RT2RecipientListType recipients) {
        List<String> recipientsAsStr = recipients.getValue().stream().map(r -> r.getValue()).collect(Collectors.toList());
        RT2MessageGetSet.setRecipients(this, recipientsAsStr);
    }

    //-------------------------------------------------------------------------
    public RT2RecipientListType getRecipients () {
        List<RT2CliendUidType> recipients = RT2MessageGetSet.getRecipients(this).stream().map(r -> new RT2CliendUidType(r)).collect(Collectors.toList());
        return new RT2RecipientListType(recipients);
    }

    public String getInternalBackendProcessingNode() throws Exception {
        return RT2MessageGetSet.getInternalBackendProcessingNode(this);
    }

    public void setInternalBackendProcessingNode(String nodeId) {
        RT2MessageGetSet.setInternalBackendProcessingNode(this, nodeId);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + flags;
		result = prime * result + ((header == null) ? 0 : header.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RT2Message other = (RT2Message) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (flags != other.flags)
			return false;
		if (header == null) {
			if (other.header != null)
				return false;
		} else if (!header.equals(other.header))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}
