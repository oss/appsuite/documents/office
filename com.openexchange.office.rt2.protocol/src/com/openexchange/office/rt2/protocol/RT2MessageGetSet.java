/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.openexchange.office.rt2.protocol.internal.EDocState;
import com.openexchange.office.tools.common.error.ErrorCode;

//=============================================================================
public class RT2MessageGetSet {

	// -------------------------------------------------------------------------
	private RT2MessageGetSet() {
	}

	// -------------------------------------------------------------------------
	public static void setSessionID(final RT2Message aMessage, final String sSessionID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SESSION_ID, sSessionID);
	}

	// -------------------------------------------------------------------------
	public static String getSessionID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SESSION_ID);
	}

	// -------------------------------------------------------------------------
	public static Integer getSeqNumber(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, Integer.MIN_VALUE);
	}

	// -------------------------------------------------------------------------
	public static boolean hasSeqNumber(final RT2Message aMessage) {
		final Integer nSeqNrCheck = getOptionalHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, null);
		final boolean bHasSeqNr = (nSeqNrCheck != null);
		return bHasSeqNr;
	}

	// -------------------------------------------------------------------------
	public static void setSeqNumber(final RT2Message aMessage, final Integer nNr) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, nNr);
	}

	// -------------------------------------------------------------------------
	public static void clearSeqNumber(final RT2Message aMessage) {
		setMandatoryHeader(aMessage, RT2Protocol.HEADER_SEQ_NR, 0);
	}

	// -------------------------------------------------------------------------
	public static void setClientUID(final RT2Message aMessage, final String sClientUID) {
		setMandatoryHeader(aMessage, RT2Protocol.HEADER_CLIENT_UID, sClientUID);
	}

	// -------------------------------------------------------------------------
	public static String getClientUID(final RT2Message aMessage) {
		return getMandatoryHeader(aMessage, RT2Protocol.HEADER_CLIENT_UID);
	}

	// -------------------------------------------------------------------------
	public static void setOldClientUID(final RT2Message aMessage, final String sClientUID) {
	    setMandatoryHeader(aMessage, RT2Protocol.HEADER_OLD_CLIENT_UID, sClientUID);
	}

	// -------------------------------------------------------------------------
	public static String getOldClientUID(final RT2Message aMessage) {
		return getMandatoryHeader(aMessage, RT2Protocol.HEADER_OLD_CLIENT_UID);
	}

	// -------------------------------------------------------------------------
	public static void setDriveDocID(final RT2Message aMessage, final String sDriveDocID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DRIVE_DOC_ID, sDriveDocID);
	}

	// -------------------------------------------------------------------------
	public static String getDriveDocID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DRIVE_DOC_ID);
	}

	// -------------------------------------------------------------------------
	public static void setFolderID(final RT2Message aMessage, final String sFolderID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FOLDER_ID, sFolderID);
	}

	// -------------------------------------------------------------------------
	public static String getFolderID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FOLDER_ID);
	}

	// -------------------------------------------------------------------------
	public static void setFileID(final RT2Message aMessage, final String sFileID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FILE_ID, sFileID);
	}

	// -------------------------------------------------------------------------
	public static String getFileID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_FILE_ID);
	}

	// -------------------------------------------------------------------------
	public static void setDocUID(final RT2Message aMessage, final String sDocUID) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_DOC_UID, sDocUID);
	}

	// -------------------------------------------------------------------------
	public static String getDocUID(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_DOC_UID, null);
	}

	// -------------------------------------------------------------------------
	public static void setDocType(final RT2Message aMessage, final String sDocType) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_TYPE, sDocType);
	}

	// -------------------------------------------------------------------------
	public static String getDocType(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_TYPE);
	}

	// -------------------------------------------------------------------------
	public static void setDocState(final RT2Message aMessage, final EDocState eState) {
		final String sState = EDocState.toString(eState);
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_STATE, sState);
	}

	// -------------------------------------------------------------------------
	public static EDocState getDocState(final RT2Message aMessage) {
		final String sState = getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_DOC_STATE);
		final EDocState eState = EDocState.fromString(sState);
		return eState;
	}

	// -------------------------------------------------------------------------
	public static void markAsPreview(final RT2Message aMessage) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_PREVIEW, Boolean.TRUE);
	}

	// -------------------------------------------------------------------------
	public static boolean isPreview(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_PREVIEW, Boolean.FALSE);
	}

	// -------------------------------------------------------------------------
	public static void setFastEmpty(final RT2Message aMessage, final boolean bFastEmpty) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY, bFastEmpty);
	}

	// -------------------------------------------------------------------------
	public static Boolean getFastEmpty(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY, Boolean.FALSE);
	}

	// -------------------------------------------------------------------------
	public static void setFastEmptyOSN(final RT2Message aMessage, final int nFastEmptyOSN) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY_OSN, nFastEmptyOSN);
	}

	// -------------------------------------------------------------------------
	public static String getUserAgent(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_USERAGENT, null);
	}

	// -------------------------------------------------------------------------
	public static void setUserAgent(final RT2Message aMessage, final String userAgent) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_USERAGENT, userAgent);
	}

	// -------------------------------------------------------------------------
	public static String getAuthCode(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_AUTH_CODE, null);
	}

	// -------------------------------------------------------------------------
	public static void setAuthCode(final RT2Message aMessage, final String sAuthCode) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_AUTH_CODE, sAuthCode);
	}

	// -------------------------------------------------------------------------
	public static Integer getFastEmptyOSN(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_FAST_EMPTY_OSN, -1);
	}

	// -------------------------------------------------------------------------
	public static Boolean getNewDocState(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_NEW_DOC, Boolean.FALSE);
	}

	// -------------------------------------------------------------------------
	public static String getStorageVersion(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_STORAGE_VERSION, null);
	}

	// -------------------------------------------------------------------------
	public static int getStorageOSN(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_STORAGE_OSN, -1);
	}

	// -------------------------------------------------------------------------
	public static long getUnvailableTime(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_UNAVAIL_TIME, 0L);
	}

	// -------------------------------------------------------------------------
	public static void setUnvailableTime(final RT2Message aMessage, final long nUnavailableTime) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_UNAVAIL_TIME, nUnavailableTime);
	}

	// -------------------------------------------------------------------------
	public static void setAppAction(final RT2Message aMessage, final String sAction) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_APP_ACTION, sAction);
	}

	// -------------------------------------------------------------------------
	public static String getAppAction(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_APP_ACTION);
	}

	// -------------------------------------------------------------------------
	public static void setNodeUUID(final RT2Message aMessage, final String sUUID) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_NODE_UID, sUUID);
	}

	// -------------------------------------------------------------------------
	public static String getNodeUUID(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_NODE_UID, null);
	}

	// -------------------------------------------------------------------------
	public static void setAdminID(final RT2Message aMessage, final String sAdminID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_ID, sAdminID);
	}

	// -------------------------------------------------------------------------
	public static String getAdminID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_ID);
	}

	// -------------------------------------------------------------------------
	public static void setAdminHZMasterUUID(final RT2Message aMessage, final String sHZMasterUUID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_HZ_MASTER_UUID, sHZMasterUUID);
	}

	// -------------------------------------------------------------------------
	public static String getAdminHZMasterUUID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_HZ_MASTER_UUID);
	}

	// -------------------------------------------------------------------------
	public static void setAdminHZMemberUUID(final RT2Message aMessage, final String sHZMemberUUID) {
		setFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_HZ_MEMBER_UUID, sHZMemberUUID);
	}

	// -------------------------------------------------------------------------
	public static String getAdminHZMemberUUID(final RT2Message aMessage) {
		return getFlagMandatoryHeader(aMessage, RT2Protocol.HEADER_ADMIN_HZ_MEMBER_UUID);
	}

	// -------------------------------------------------------------------------
	public static void setAutoClose(final RT2Message aMessage, boolean bAutoClose) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_AUTO_CLOSE, bAutoClose);
	}

	// -------------------------------------------------------------------------
	public static Boolean getAutoClose(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_AUTO_CLOSE, Boolean.FALSE);
	}

	// -------------------------------------------------------------------------
	public static void setNoRestore(final RT2Message aMessage, boolean bNoRestore) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_NO_RESTORE, bNoRestore);
	}

	// -------------------------------------------------------------------------
	public static Boolean getNoRestore(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_NO_RESTORE, Boolean.FALSE);
	}

	// -------------------------------------------------------------------------
	public static void setOriginAsHostName(final RT2Message aMessage, String originAsHostName) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_ORIGIN_HOSTNAME, originAsHostName);
	}

	// -------------------------------------------------------------------------
	public static String getOriginAsHostName(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_ORIGIN_HOSTNAME, null);
	}

	public static String getInternalBackendProcessingNode(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_INTENAL_BACKEND_PROCESSING_NODE, null);
	}

	public static void setInternalBackendProcessingNode(final RT2Message aMessage, String backId) {
		setOptionalHeader(aMessage, RT2Protocol.HEADER_INTENAL_BACKEND_PROCESSING_NODE, backId);
	}

	// -------------------------------------------------------------------------
	public static boolean hasError(RT2Message msg) {
		final Integer errorCode = getOptionalHeader(msg, RT2Protocol.HEADER_ERROR_CODE, null);
		return (errorCode != null);
	}

	// -------------------------------------------------------------------------
	public static void setError(final RT2Message aMessage, final ErrorCode aError) {
		if (aError == null)
			return;

		// TODO: This optimization is not error free - it's not possible to reset
		// an error with a non-error. Could be possible in some specific situations
		// e.g. recoverable error.
		if (aError.isNoError() && !aError.isWarning())
			return;

		setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CLASS, aError.getErrorClass());
		setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CODE, aError.getCode());
		setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_NAME, aError.getCodeAsStringConstant());
		setOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_VALUE, aError.getValue());
	}

	// -------------------------------------------------------------------------
	public static ErrorCode getError(final RT2Message aMessage) {
		final Integer nErrorClass = getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CLASS,
				ErrorCode.NO_ERROR.getErrorClass());
		final Integer nErrorCode = getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_CODE,
				ErrorCode.NO_ERROR.getCode());
		final String sErrorName = getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_NAME,
				ErrorCode.NO_ERROR.getCodeAsStringConstant());
		final Object sErrorValue = getOptionalHeader(aMessage, RT2Protocol.HEADER_ERROR_VALUE, new Object());

		final ErrorCode aError = new ErrorCode(nErrorCode, sErrorName, "", nErrorClass);
		if (sErrorValue != null) {
			aError.setValue(sErrorValue.toString());
		}

		return aError;
	}

	// -------------------------------------------------------------------------
	public static boolean isError(final RT2Message aMessage) {
		final ErrorCode aError = getError(aMessage);
		if (aError == null)
			return false;
		if (aError.equals(ErrorCode.NO_ERROR))
			return false;
		return true;
	}

	// -------------------------------------------------------------------------
	public static void setRecipients(final RT2Message aMessage, final String... lRecipients) {
		String sRecipients = StringUtils.join(lRecipients, ",");
		sRecipients = StringUtils.removeStart(sRecipients, ",");
		sRecipients = StringUtils.removeEnd(sRecipients, ",");
		Validate.notEmpty(sRecipients, "Recipient list is empty. Routing will produce unpredictable results !");

		setInternalHeader(aMessage, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, sRecipients);
	}

	// -------------------------------------------------------------------------
	public static void setRecipients(final RT2Message aMessage, final List<String> lRecipients) {
		String[] strArray = {};
		String sRecipients = StringUtils.join(lRecipients.toArray(strArray), ",");
		sRecipients = StringUtils.removeStart(sRecipients, ",");
		sRecipients = StringUtils.removeEnd(sRecipients, ",");
		Validate.notEmpty(sRecipients, "Recipient list is empty. Routing will produce unpredictable results !");

		setInternalHeader(aMessage, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, sRecipients);
	}

	// -------------------------------------------------------------------------
	public static List<String> getRecipients(final RT2Message aMessage) {
		final List<String> lRecipients = new ArrayList<String>();
		final String sRecipients = getInternalHeader(aMessage, RT2Protocol.HEADER_INTERNAL_RECIPIENTS, "");

		final String[] lToken = StringUtils.split(sRecipients, ',');
		for (final String sRecipient : lToken)
			lRecipients.add(sRecipient);

		return lRecipients;
	}

	// -------------------------------------------------------------------------
	public static String getAdvisoryLockAction(final RT2Message aMessage) {
		return getOptionalHeader(aMessage, RT2Protocol.HEADER_ADVISORY_LOCK, "");
	}

	// -------------------------------------------------------------------------
	public static <T> void setMandatoryHeader(final RT2Message aMessage, final String sHeader, final T aValue) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, aValue, nFlag);
	}

	// -------------------------------------------------------------------------
	public static <T> T getMandatoryHeader(final RT2Message aMessage, final String sHeader) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		T aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY, nFlag);
		return aValue;
	}

	// -------------------------------------------------------------------------
	public static <T> void setFlagMandatoryHeader(final RT2Message aMessage, final String sHeader, final T aValue) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlag);
	}

	// -------------------------------------------------------------------------
	public static <T> T getFlagMandatoryHeader(final RT2Message aMessage, final String sHeader) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		T aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlag);
		return aValue;
	}

	// -------------------------------------------------------------------------
	public static <T> void setOptionalHeader(final RT2Message aMessage, final String sHeader, final T aValue) {
		aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_OPTIONAL, aValue);
	}

	// -------------------------------------------------------------------------
	public static <T> T getOptionalHeader(final RT2Message aMessage, final String sHeader, final T aDefault) {
		T aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_OPTIONAL);
		if (aValue == null)
			aValue = aDefault;
		return aValue;
	}

	// -------------------------------------------------------------------------
	public static <T> void setInternalHeader(final RT2Message aMessage, final String sHeader, final T aValue) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		aMessage.setHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlag);
	}

	// -------------------------------------------------------------------------
	public static <T> T getInternalHeader(final RT2Message aMessage, final String sHeader, final T aDefault) {
		final int nFlag = RT2Protocol.get().getFlag4Header(sHeader);
		T aValue = aMessage.getHeader2(sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlag);
		if (aValue == null)
			aValue = aDefault;
		return aValue;
	}
}
