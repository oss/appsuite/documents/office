/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol.internal;

import org.apache.commons.lang3.StringUtils;

public enum Cause {

    SESSION_INVALID,
    GARBAGE_COLLECTING,
    FATAL_WEBSOCKET_ERROR;

    //-------------------------------------------------------------------------
    public static String toString (final Cause cause)
    {
        switch (cause) {
            case SESSION_INVALID: return SESSION_INVALID.toString();
            case GARBAGE_COLLECTING: return GARBAGE_COLLECTING.toString();
            case FATAL_WEBSOCKET_ERROR: return FATAL_WEBSOCKET_ERROR.toString();
            default: throw new UnsupportedOperationException ("Unknown '"+cause+"' detected.");
        }
    }

    //-------------------------------------------------------------------------
    public static Cause fromString (final String cause)
    {
        if (StringUtils.equalsIgnoreCase(cause, SESSION_INVALID.toString()))
            return Cause.SESSION_INVALID;

        if (StringUtils.equalsIgnoreCase(cause, GARBAGE_COLLECTING.toString()))
            return Cause.GARBAGE_COLLECTING;

        if (StringUtils.equalsIgnoreCase(cause, FATAL_WEBSOCKET_ERROR.toString()))
            return Cause.FATAL_WEBSOCKET_ERROR;

        throw new UnsupportedOperationException ("Unknown '"+cause+"' detected.");
    }
}
