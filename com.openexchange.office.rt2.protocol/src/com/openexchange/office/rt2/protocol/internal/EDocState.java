/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol.internal;

import org.apache.commons.lang3.StringUtils;

//=============================================================================
public enum EDocState
{
    //-------------------------------------------------------------------------
    E_OFFLINE,
    E_ONLINE ;

    //-------------------------------------------------------------------------
    public static final String STR_OFFLINE = "offline";
    public static final String STR_ONLINE  = "online" ;

    //-------------------------------------------------------------------------
    public static String toString (final EDocState eState)
    {
        if (eState == EDocState.E_OFFLINE)
            return STR_OFFLINE;

        if (eState == EDocState.E_ONLINE)
            return STR_ONLINE;

        throw new UnsupportedOperationException ("No support for '"+eState+"' implemented yet.");
    }

    //-------------------------------------------------------------------------
    public static EDocState fromString (final String sState)
    {
        if (StringUtils.equalsIgnoreCase(sState, STR_OFFLINE))
            return EDocState.E_OFFLINE;

        if (StringUtils.equalsIgnoreCase(sState, STR_ONLINE))
            return EDocState.E_ONLINE;

        throw new UnsupportedOperationException ("No support for '"+sState+"' implemented yet.");
    }
}
