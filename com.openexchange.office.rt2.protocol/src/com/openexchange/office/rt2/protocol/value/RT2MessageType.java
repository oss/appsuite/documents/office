/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol.value;

import java.util.HashMap;
import java.util.Map;

public enum RT2MessageType {
	
    ADMIN_TASK_CLEANUP_FOR_CRASHED_NODE("cleanup_for_crashed_node_admintask", null),
    ADMIN_TASK_COMPLETED_CLEANUP_FOR_CRASHED_NODE("cleanup_for_crashed_node_admintask_completed", null),
    ADMIN_HEALTH_CHECK_REQUEST("health_check_adminrequest", null),
    ADMIN_HEALTH_CHECK_RESPONSE("health_check_adminresponse", null),
    ADMIN_TASK_FILES_MOVED_DELETED("files_moved_deleted_admintask", null),

    PING("ping", null),
    PONG("pong", null),
    ACK_SIMPLE("simple_ack", null),
    ACK_ERROR("error_ack", null),
    NACK_SIMPLE("simple_nack", null),

    RESPONSE_GENERIC_ERROR("generic_error_response", null),
    RESPONSE_JOIN("join_response", null),
    RESPONSE_LEAVE("leave_response", null),
    RESPONSE_OPEN_DOC("open_doc_response", null),
    RESPONSE_OPEN_DOC_CHUNK("open_doc_chunk_response", null),
    RESPONSE_SAVE_DOC("save_doc_response", null),
    RESPONSE_CLOSE_DOC("close_doc_response", null),
    RESPONSE_CLOSE_AND_LEAVE("close_and_leave_response", null),
    RESPONSE_APPLY_OPS("apply_ops_response", null),
    RESPONSE_APP_ACTION("app_action_response", null),
    RESPONSE_UPDATE_SLIDE("update_slide_response", null), // --> Application specific msg
    RESPONSE_RESET("reset_response", null),  // --> Not used, only for compatibility to RT2 protocol
    RESPONSE_SYNC("sync_response", null),
    RESPONSE_EDITRIGHTS("editrights_response", null),
    RESPONSE_UNAVAILABILITY("unavailability_response", null),
    RESPONSE_ABORT_OPEN("abort_open_response", null),
    RESPONSE_EMERGENCY_LEAVE("emergency_leave_response", null),
    RESPONSE_SYNC_STABLE("sync_stable_response", null),

    REQUEST_JOIN("join_request", RESPONSE_JOIN),
    REQUEST_LEAVE("leave_request", RESPONSE_LEAVE),
    REQUEST_OPEN_DOC("open_doc_request", RESPONSE_OPEN_DOC),
    REQUEST_SAVE_DOC("save_doc_request", RESPONSE_SAVE_DOC),
    REQUEST_CLOSE_DOC("close_doc_request", RESPONSE_CLOSE_DOC),
    REQUEST_CLOSE_AND_LEAVE("close_and_leave_request", RESPONSE_CLOSE_AND_LEAVE),
    REQUEST_APPLY_OPS("apply_ops_request", RESPONSE_APPLY_OPS),
    REQUEST_APP_ACTION("app_action_request", RESPONSE_APP_ACTION),		// --> Application specific msg 
    REQUEST_UPDATE_SLIDE("update_slide_request", RESPONSE_UPDATE_SLIDE),
    REQUEST_RESET("reset_request", RESPONSE_RESET), // --> Not used, only for compatibility to RT2 protocol
    REQUEST_SYNC("sync_request", RESPONSE_SYNC),
    REQUEST_EDITRIGHTS("editrights_request", RESPONSE_EDITRIGHTS),
    REQUEST_UNAVAILABILITY("unavailability_request", RESPONSE_UNAVAILABILITY),
    REQUEST_ABORT_OPEN("abort_open_request", RESPONSE_ABORT_OPEN),
    REQUEST_EMERGENCY_LEAVE("emergency_leave_request", RESPONSE_EMERGENCY_LEAVE),
    REQUEST_SYNC_STABLE("sync_stable_request", RESPONSE_SYNC_STABLE),

    BROADCAST_UPDATE("update_broadcast", null),
    BROADCAST_UPDATE_CLIENTS("update_clients_broadcast", null),
    BROADCAST_EDITREQUEST_STATE("editrequest_state_broadcast", null),
    BROADCAST_SHUTDOWN("shutdown_broadcast", null),
    BROADCAST_CRASHED("crashed_broadcast", null),
    BROADCAST_RENAMED_RELOAD("renamed_reload_broadcast", null),
    BROADCAST_HANGUP("hangup_broadcast", null),
    BROADCAST_OT_RELOAD("ot_reload_broadcast", null),
    BROADCAST_FILE_MOVED_RELOAD("file_moved_reload_broadcast", null);

    private static Map<String, RT2MessageType> enumMap = new HashMap<>();

    private final String value;
    private final RT2MessageType responseType;

    private RT2MessageType(String value, RT2MessageType responseType) {
        this.value = value;
        this.responseType = responseType;
    }

    public String getValue() {
        return value;
    }

    public RT2MessageType getResponseType() {
        return responseType;
    }

    public static RT2MessageType valueOfType(String type) {
        if (enumMap.isEmpty()) {
            synchronized (enumMap) {
                for (RT2MessageType enumType : RT2MessageType.values()) {
                    enumMap.put(enumType.getValue(), enumType);
                }
            }
        }
        return enumMap.get(type);
    }
}
