/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol.value;

import org.apache.commons.lang3.StringUtils;

public class RT2CliendUidType extends RT2ValueType<String> {

    public RT2CliendUidType() {
        super("");
    }

    public RT2CliendUidType(String type) {
        super(type);
    }

    public static boolean isNotEmptyAndEqual(final RT2CliendUidType r1, final RT2CliendUidType r2) {
    	if ((r1 != null) && (r2 != null) && StringUtils.isNotEmpty(r1.getValue()) && StringUtils.isNotEmpty(r2.getValue())) {
			return (r1.getValue()).equals(r2.getValue());
		}

		return false;
	}

    public static boolean isEmpty(final RT2CliendUidType r1) {
    	return ((r1 == null) || (r1.getValue().isEmpty()));
    }

    public static boolean isNotEmpty(final RT2CliendUidType r1) {
    	return !isEmpty(r1);
    }
}
