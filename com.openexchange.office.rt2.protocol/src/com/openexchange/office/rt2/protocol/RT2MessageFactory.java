/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.internal.Cause;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.rt2.protocol.value.RT2UnavailableTimeType;

//=============================================================================
public class RT2MessageFactory
{
	private static final Logger log = LoggerFactory.getLogger(RT2MessageFactory.class);
	
     //-------------------------------------------------------------------------
    private RT2MessageFactory ()
    {}

    public static RT2Message newAdminMessage (final RT2MessageType type)
    {
        final RT2Message aMessage = new RT2Message (type);

        impl_defineMessageFlags  (aMessage, type);
        aMessage.newMessageID();

        return aMessage;
    }

    public static RT2Message newBroadcastMessage (final RT2MessageType type, RT2DocUidType docUid)
    {
        final RT2Message aMessage = new RT2Message (type);

        impl_defineMessageFlags  (aMessage, type);
        aMessage.newMessageID();
        aMessage.setDocUID(docUid);
        return aMessage;
    }

    public static RT2Message newBroadcastMessage(RT2CliendUidType clientUid, RT2DocUidType docUid, RT2MessageType type) {
        final RT2Message aMessage = newMessage(type, clientUid, docUid);

        impl_defineMessageFlags  (aMessage, type);
        aMessage.newMessageID();

        return aMessage;
	}

    public static RT2Message newLeaveRequest(final RT2CliendUidType clientUid, final RT2DocUidType docUid) {
        return newMessage(RT2MessageType.REQUEST_LEAVE, clientUid, docUid);
    }

    public static RT2Message newCloseDocRequest(final RT2CliendUidType clientUid, final RT2DocUidType docUid) {
        return newMessage(RT2MessageType.REQUEST_CLOSE_DOC, clientUid, docUid);
    }

    public static RT2Message newLeaveAndCloseRequest(final RT2CliendUidType clientUid, final RT2DocUidType docUid, Cause cause) {
    	RT2Message res = newMessage(RT2MessageType.REQUEST_CLOSE_AND_LEAVE, clientUid, docUid);
    	res.setHeader(RT2Protocol.HEADER_CAUSE, cause.name());
    	return res;
    }
    
    public static RT2Message newSimpleNackRequest(final RT2CliendUidType clientUid, final RT2DocUidType docUid, Collection<Integer> seqNrs) throws JSONException {
        RT2Message res = newMessage(RT2MessageType.NACK_SIMPLE, clientUid, docUid);
		res.getBody().put(RT2Protocol.BODYPART_NACKS, new JSONArray(seqNrs));
        return res;
    }

    public static RT2Message newUnavailabilityRequest(final RT2CliendUidType clientUid, final RT2DocUidType docUid, final RT2UnavailableTimeType unvailableTime) {
        RT2Message res = newMessage(RT2MessageType.REQUEST_UNAVAILABILITY, clientUid, docUid);
        res.setUnvailableTime(unvailableTime);
        return res;
    }

    public static RT2Message newPing() {
        final RT2Message aMessage = new RT2Message(RT2MessageType.PING);
        aMessage.newMessageID();

        return aMessage;
    }

    public static RT2Message newPong() {
        final RT2Message aMessage = new RT2Message(RT2MessageType.PONG);
        aMessage.newMessageID();

        return aMessage;
    }

    //-------------------------------------------------------------------------
    public static RT2Message newMessage (final RT2MessageType type, final RT2CliendUidType clientUid, final RT2DocUidType docUid)
    {
        final RT2Message aMessage = new RT2Message(type);
        impl_defineMessageFlags(aMessage, type);
        aMessage.newMessageID();
        aMessage.setClientUID(clientUid);
        aMessage.setDocUID(docUid);

        return aMessage;
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJmsMessage(Message jmsMessage) throws JSONException, JMSException {
    	try {
	        String type = jmsMessage.getStringProperty(RT2MessageJmsPostProcessor.HEADER_MSG_TYPE);
	        RT2Message res = new RT2Message(RT2MessageType.valueOfType(type));
	        impl_defineMessageFlags(res, res.getType());
	        TextMessage txtMsg = (TextMessage) jmsMessage;
	        	        
	        Map<String, Object> header = new HashMap<>();
	        @SuppressWarnings("unchecked")
	        Enumeration<String> enumeration = txtMsg.getPropertyNames();
	        while (enumeration.hasMoreElements()) {
	            String next = enumeration.nextElement();
	            header.put(next, txtMsg.getObjectProperty(next));
	        }
	        res.mergeHeader(header, jmsMessage);
	        String text = txtMsg.getText();
	        try {	        	
	        	res.setBodyString(text);
	        } catch (JSONException ex) {
	        	log.error("Cannot parse json body {} of jms message {}.", text, jmsMessage, ex);
	        	throw ex;
	        }
	        return res;
    	} catch (JMSException ex) {
    		log.error("Error reading data of jms message {}", jmsMessage, ex);
    		throw ex;
    	}
    }
    
    //-------------------------------------------------------------------------
    public static RT2Message cloneMessage (final RT2Message aOriginal, final RT2MessageType cloneType)
    {
        final RT2Message aClone = RT2MessageFactory.newMessage(cloneType);

        RT2Message.copyAllHeader(aOriginal, aClone);
        RT2Message.copyBody     (aOriginal, aClone);

        // msg flags are bound to message type.
        // Dont copy it - generate it new for new type always !
        impl_defineMessageFlags  (aClone, cloneType);

        return aClone;
    }

    //-------------------------------------------------------------------------
    public static RT2Message createResponseFromMessage (final RT2Message aMessage, final RT2MessageType responseType)
    {
        final RT2Message aResponse = cloneMessage(aMessage, responseType);
        aResponse.setHeader("internal.type", aMessage.getType().getResponseType());
        // message id of response has to be the same !
        // but body needs to be fresh and new !
        aResponse.setBody(null);
        aResponse.setHeaderWithoutCheck(RT2Protocol.HEADER_SESSION_ID, null);
        aResponse.setHeaderWithoutCheck(RT2Protocol.HEADER_AUTO_CLOSE, null);
        return aResponse;
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRequestFromMessage (final RT2Message aMessage, final RT2MessageType requestType)
    {
        final RT2Message aRequest = cloneMessage(aMessage, requestType);
        // new request needs new message id !
        aRequest.newMessageID();
        // but body needs to be fresh and new !
        aRequest.setBody(null);
        return aRequest;
    }

    //-------------------------------------------------------------------------
    private static void impl_defineMessageFlags (final RT2Message aMessage,
                                                 final RT2MessageType     sType   )
    {
        final Integer nFlags4Type = RT2Protocol.get().getFlags4Type(sType.getValue());
        aMessage.setFlags(nFlags4Type);
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSONString (final String sJSON) throws JSONException, ValidationException
    {
        final RT2Message aRTMsg = fromJSONString(sJSON, /*decode*/true);
        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSONString (final String  sJSON , boolean bDecode) throws JSONException, ValidationException
    {
        final JSONObject aJSON = new JSONObject ();
        aJSON.parseJSONString(sJSON);

        final RT2Message aRTMsg = fromJSON(aJSON, bDecode);
        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSON (final JSONObject aJSON, boolean bDecode) throws JSONException, ValidationException
    {
        final String      KEY_TYPE   = bDecode ? "t" : "type"  ;
        final String      KEY_HEADER = bDecode ? "h" : "header";
        final String      KEY_BODY   = bDecode ? "b" : "body"  ;

        final RT2Protocol aProtocol  = RT2Protocol.get ();

        String sType = (String)aJSON.getString(KEY_TYPE);
        if (bDecode)
            sType = aProtocol.decode (sType);

        final RT2Message aRTMsg = newMessage(RT2MessageType.valueOfType(sType));

        if (aJSON.hasAndNotNull(KEY_HEADER))
        {
            final Map< String, Object >              lHeader = RT2MessageFactory.impl_JSONObject2Map(aJSON.getJSONObject(KEY_HEADER));
            final Iterator< Entry< String, Object >> rHeader = lHeader.entrySet().iterator();

            while (rHeader.hasNext())
            {
                final Entry< String, Object > aHeader     = rHeader.next();
                      String                  sHeader     = aHeader.getKey();
                final Object                  aValue      = aHeader.getValue();

                if (bDecode)
                    sHeader = aProtocol.decode(sHeader);

                aRTMsg.setHeader(sHeader, aValue);
            }
        }

        final JSONObject aBody = aJSON.optJSONObject(KEY_BODY);
        aRTMsg.setBody(aBody);

        final StringBuffer sValidation = new StringBuffer (256);
        if ( ! aRTMsg.isValid(sValidation))
            throw new ValidationException("Msg not valid : "+sValidation.toString ());

        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    private static RT2Message newMessage (final RT2MessageType sType)
    {
        Validate.notEmpty (sType.getValue(), "Invalid argument 'type'.");

        final RT2Message aMessage = new RT2Message (sType);

        impl_defineMessageFlags  (aMessage, sType);
        aMessage.newMessageID();

        return aMessage;
    }

    //-------------------------------------------------------------------------
    public static String toJSONString (final RT2Message aRTMsg)
    {
        final String sJSON = RT2MessageFactory.toJSONString(aRTMsg, /*encode*/true);
        return sJSON;
    }

    //-------------------------------------------------------------------------
    public static JSONObject toJSON (final RT2Message aRTMsg)
        throws Exception
    {
        final JSONObject aJSON = RT2MessageFactory.toJSON(aRTMsg, /*encode*/true);
        return aJSON;
    }

    //-------------------------------------------------------------------------
    public static String toJSONString (final RT2Message aRTMsg ,
                                       final boolean    bEncode)
    {
        final JSONObject aJSON = RT2MessageFactory.toJSON (aRTMsg, bEncode);
        final String     sJSON = aJSON.toString ();
        return sJSON;
    }

    //-------------------------------------------------------------------------
    public static JSONObject toJSON (final RT2Message aRTMsg ,
                                     final boolean    bEncode)
    {
        try {
            final String      KEY_TYPE   = bEncode ? "t" : "type"  ;
            final String      KEY_HEADER = bEncode ? "h" : "header";
            final String      KEY_BODY   = bEncode ? "b" : "body"  ;

            final JSONObject  aJSON      = new JSONObject ();
            final RT2Protocol aProtocol  = RT2Protocol.get ();

            String sType = aRTMsg.getType().getValue();
            if (bEncode)
                sType = aProtocol.encode (sType);
            aJSON.put(KEY_TYPE, sType);

            final JSONObject    aJSONHeader = new JSONObject ();
            final Set< String > lHeader     = aRTMsg.listHeader();
            for (String sHeader : lHeader)
            {
                // filter out 'internal' header so they do not reach the client
                if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_JMS))
                    continue;

                if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_INTERNAL))
                    continue;

                // note: value needs to be retrieved from internal map,
                // before we might encode the header name ...
                // otherwise we will get "null" as value always ...
                // because the might encoded header is not known ;-)
                final Object aValue = aRTMsg.getHeader(sHeader);

                if (bEncode)
                    sHeader = aProtocol.encode(sHeader);

                aJSONHeader.put (sHeader, aValue);
            }

            aJSON.put(KEY_HEADER, aJSONHeader);
            aJSON.put(KEY_BODY  , aRTMsg.getBody());

            return aJSON;
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    //-------------------------------------------------------------------------
    private static Map< String, Object > impl_JSONObject2Map (final JSONObject aJSON)
    {
        final Map< String, Object > aMap = new HashMap< String, Object > ();
        final Set< Entry< String, Object >> lEntries = aJSON.entrySet();
        if (lEntries == null)
            return aMap;

        final Iterator< Entry< String, Object >> rEntries = lEntries.iterator();
        while (rEntries.hasNext())
        {
            final Entry< String, Object > rEntry = rEntries.next();
            final String                  sEntry = rEntry.getKey();
            final Object                  aValue = rEntry.getValue();
            aMap.put (sEntry, aValue);
        }

        return aMap;
    }

}
