/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.rt2.protocol.value.RT2MessageType;

public class RT2MessageJmsPostProcessor implements RT2MessagePostProcessor {

    private static final Logger log = LoggerFactory.getLogger(RT2MessageJmsPostProcessor.class);

    public static final String HEADER_MSG_TYPE  = RT2Protocol.HEADER_PREFIX_INTERNAL + ".type";

    private final RT2Message rt2Message;

    public RT2MessageJmsPostProcessor(RT2Message rt2Message) {
        this.rt2Message = rt2Message;
    }

    @Override
    public Message postProcessMessage(Message msg) throws JMSException {
    	msg.setBooleanProperty(HEADER_VERSION_MSG_RCV, false);
    	msg.setBooleanProperty(HEADER_VERSION_MSG_SND, false);
        msg.setStringProperty(HEADER_MSG_TYPE, rt2Message.getType().getValue());
        msg.setBooleanProperty( HEADER_GPB_MSG, false);
        Map<String, Object> debugMap = null;
        if (log.isDebugEnabled()) {
            debugMap = new HashMap<>();
        }
        for (String header : rt2Message.listHeader()) {
            if (!header.equals("internal.type")) {
                Object value = rt2Message.getHeader(header);
                if (value instanceof RT2MessageType) {
                    RT2MessageType rt2MsgType = (RT2MessageType) value;
                    value = rt2MsgType.getValue();
                }
                if (debugMap != null) {
                    debugMap.put(header, value);
                }
                msg.setObjectProperty(header, value);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Header: {}", debugMap);
        }
        return msg;
    }
}
