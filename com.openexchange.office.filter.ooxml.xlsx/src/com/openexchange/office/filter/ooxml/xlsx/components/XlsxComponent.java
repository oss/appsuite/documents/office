/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public abstract class XlsxComponent extends OfficeOpenXMLComponent {

    public XlsxComponent(XlsxOperationDocument operationDocument, DLNode<Object> _node, int _componentNumber) {
        super(operationDocument, _node, _componentNumber);
    }

    public XlsxComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }

    @Override
    public XlsxOperationDocument getOperationDocument() {
        return (XlsxOperationDocument)operationDocument;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {
        return null;
    }

    @Override
    public void delete(int count) {

    	final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = getContextChild(null);

    	final int endComponent = (getComponentNumber()+count)-1;
    	IComponent<OfficeOpenXMLOperationDocument> component = this;
        while(true) {
         	if(component.getNextComponentNumber()>endComponent) {
        		break;
        	}
        	component = component.getNextComponent();
        }
        component.splitEnd(endComponent, SplitMode.DELETE);
        final DLList<Object> content = (DLList<Object>)((IContentAccessor)contextChild.getParentContext().getNode().getData()).getContent();
        content.removeNodes(contextChild.getNode(), component.getContextChild(null).getNode());
    }
}
