/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.Row;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;

public class RowUtils {

    public static void applyRowProperties(JSONObject attrs, String style, Row row)
        throws FilterException, JSONException {

        if(attrs!=null) {
            // Row properties
            final JSONObject jsonRowProperties = attrs.optJSONObject(OCKey.ROW.value());
            if(jsonRowProperties!=null) {
                Iterator<String> keys = jsonRowProperties.keys();
                while(keys.hasNext()) {
                    String attr = keys.next();
                    Object value = jsonRowProperties.get(attr);
                    if(attr.equals(OCKey.HEIGHT.value())) {
                        Double ht = null;
                        if(value instanceof Number) {
                            ht = ((Number)value).doubleValue();
                        }
                        row.setHt(ht);
                    }
                    else if(attr.equals(OCKey.CUSTOM_HEIGHT.value())) {
                        Boolean customHeight = null;
                        if(value instanceof Boolean) {
                            customHeight = (Boolean)value;
                        }
                        row.setCustomHeight(customHeight);
                    }
                    else if(attr.equals(OCKey.VISIBLE.value())) {
                        Boolean hidden = null;
                        if(value instanceof Boolean) {
                            hidden = !(Boolean)value;
                        }
                        row.setHidden(hidden);
                    }
                    else if(attr.equals(OCKey.CUSTOM_FORMAT.value())) {
                        Boolean customFormat = null;
                        if(value instanceof Boolean) {
                            customFormat = (Boolean)value;
                        }
                        row.setCustomFormat(customFormat);
                    }
                }
            }

            // grouping attributes
            final JSONObject jsonGroupAttrs = attrs.optJSONObject(OCKey.GROUP.value());
            if (jsonGroupAttrs!=null) {
                final int level = jsonGroupAttrs.optInt(OCKey.LEVEL.value(), 0);
                if (level>=1 && level<=7) {
                    row.setOutlineLevel((short)level);
                } else {
                    row.setOutlineLevel(null);
                }
                row.setCollapsed(jsonGroupAttrs.optBoolean(OCKey.COLLAPSED.value()) ? true : null);
            }
        }

        // set new auto-style, or delete the 's' attribute for the default auto-style
        if (style != null) {
            Long index = Utils.parseAutoStyleId(style);
            if (index != null) {
                row.setS(index.longValue() == 0 ? null : index);
            }
        }
    }

    private static boolean equals(Double d1, Double d2, double def) {
        final double v1 = (d1==null) ? def : d1.doubleValue();
        final double v2 = (d2==null) ? def : d2.doubleValue();
        return v1==v2;
    }

    private static boolean equals(Short s1, Short s2, short def) {
        final short v1 = (s1==null) ? def : s1.shortValue();
        final short v2 = (s2==null) ? def : s2.shortValue();
        return v1==v2;
    }

    public static boolean compareRowProperties(Row r1, Row r2) {
        return
            (r1.getStyle() == r2.getStyle()) &&
            equals(r1.getHt(), r2.getHt(), 0.0) &&
            (r1.isCustomHeight() == r2.isCustomHeight()) &&
            (r1.isHidden() == r2.isHidden()) &&
            (r1.isCustomFormat() == r2.isCustomFormat()) &&
            equals(r1.getOutlineLevel(), r2.getOutlineLevel(), (short)0) &&
            (r1.isCollapsed() == r2.isCollapsed());
    }

    public static JSONObject createRowProperties(Row row)
        throws JSONException {

        final JSONObject rowProperties = new JSONObject(4);

        final Double rowHeight = row.getHt();
        if(rowHeight!=null) {
            rowProperties.put(OCKey.HEIGHT.value(), rowHeight);
        }
        if(row.isCustomHeight()!=null&&row.isCustomHeight().booleanValue()) {
            rowProperties.put(OCKey.CUSTOM_HEIGHT.value(), true);
        }
        rowProperties.put(OCKey.VISIBLE.value(), row.isHidden()!=null ? !row.isHidden().booleanValue() : true);
        if(row.isCustomFormat()!=null&&row.isCustomFormat().booleanValue()) {
            rowProperties.put(OCKey.CUSTOM_FORMAT.value(), true);
        }

        // grouping
        final JSONObject groupProperties = new JSONObject(2);
        final Short level = row.getOutlineLevel();
        if (level != null && level >= 1 && level <= 7) {
            groupProperties.put(OCKey.LEVEL.value(), level);
        }
        final Boolean collapsed = row.isCollapsed();
        if (collapsed != null && collapsed.booleanValue()) {
            groupProperties.put(OCKey.COLLAPSE.value(), true);
        }

        // create the resulting attribute set
        JSONObject attrs = new JSONObject();
        if (rowProperties.length()>0) {
            attrs.put(OCKey.ROW.value(), rowProperties);
        }
        if (groupProperties.length()>0) {
            attrs.put(OCKey.GROUP.value(), groupProperties);
        }

        return (attrs.length()>0) ? attrs : null;
    }
}
