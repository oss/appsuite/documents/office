/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTCellAlignment;
import org.xlsx4j.sml.CTCellProtection;
import org.xlsx4j.sml.CTCfRule;
import org.xlsx4j.sml.CTDxf;
import org.xlsx4j.sml.CTNumFmt;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTXf;
import org.xlsx4j.sml.CfRule;
import org.xlsx4j.sml.CfRule.CfRuleIter;
import org.xlsx4j.sml.ICfRule;
import org.xlsx4j.sml.STCfType;
import org.xlsx4j.sml.STConditionalFormattingOperator;
import org.xlsx4j.sml.STTimePeriod;
import org.xlsx4j.sml.Worksheet;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public final class ConditionalFormattings {

    public static void createOperations(XlsxOperationDocument operationDocument, JSONArray operationsArray, Worksheet worksheet, int sheetIndex)
        throws JSONException {

        final Iterator<Entry<String, CfRule>> conditionalFormattingRuleIter = worksheet.getConditionalFormattingRules().entrySet().iterator();
        while(conditionalFormattingRuleIter.hasNext()) {
            final Entry<String, CfRule> conditionalFormattingRuleEntry = conditionalFormattingRuleIter.next();
            operationsArray.put(getInsertCondFormatRuleOperation(sheetIndex, conditionalFormattingRuleEntry.getKey(), conditionalFormattingRuleEntry.getValue(), operationDocument.getStylesheet(true)));
        }
    }

    private static JSONObject getInsertCondFormatRuleOperation(int sheetIndex, String id, CfRule cf, CTStylesheet stylesheet)
        throws JSONException {

        final ICfRule cfRule = cf.getR2() != null ? cf.getR2() : cf.getR1();
        final JSONObject insertCondFormatRuleObject = new JSONObject();
        insertCondFormatRuleObject.put(OCKey.NAME.value(), OCValue.INSERT_CF_RULE.value());
        insertCondFormatRuleObject.put(OCKey.SHEET.value(), sheetIndex);
        insertCondFormatRuleObject.put(OCKey.ID.value(), cf.getType() + id);
        String ranges = "";
        for(String r:cf.getRanges()) {
            if(!ranges.isEmpty()) {
                ranges = ranges + " ";
            }
            ranges = ranges + r;
        }
        insertCondFormatRuleObject.put(OCKey.RANGES.value(), ranges);

        final String formula1 = cfRule.getFormula().isEmpty() ? "" : cfRule.getFormula().get(0);
        final String formula2 = cfRule.getFormula().size() < 2 ? "" : cfRule.getFormula().get(1);

        String opType = "formula";
        Object value1 = "";
        String value2 = "";

        switch (cfRule.getType()) {
        case ABOVE_AVERAGE:
            value1 =  cfRule.getStdDev();
            if (cfRule.isAboveAverage() && !cfRule.isEqualAverage()) {
                opType = "aboveAverage";
            } else if (cfRule.isAboveAverage() && cfRule.isEqualAverage()) {
                opType = "atLeastAverage";
            } else if (!cfRule.isAboveAverage() && !cfRule.isEqualAverage()) {
                opType = "belowAverage";
            } else {
                opType = "atMostAverage";
            }
            break;
        case BEGINS_WITH:
            opType = "beginsWith";
                value1 = formulashortName(cf, formula2);
            break;
        case CELL_IS:
            switch (cfRule.getOperator()) {
            case BEGINS_WITH:
                opType = "beginsWith";
                value1 = formula2;
                break;
            case BETWEEN:
                opType = "between";
                value1 = formula1;
                value2 = formula2;
                break;
            case CONTAINS_TEXT:
                opType = "contains";
                value1 = formula2;
                break;
            case ENDS_WITH:
                opType = "endsWith";
                value1 = formula2;
                break;
            case EQUAL:
                opType = "equal";
                value1 = formula1;
                break;
            case GREATER_THAN:
                opType = "greater";
                value1 = formula1;
                break;
            case GREATER_THAN_OR_EQUAL:
                opType = "greaterEqual";
                value1 = formula1;
                break;
            case LESS_THAN:
                opType = "less";
                value1 = formula1;
                break;
            case LESS_THAN_OR_EQUAL:
                opType = "lessEqual";
                value1 = formula1;
                break;
            case NOT_BETWEEN:
                opType = "notBetween";
                value1 = formula1;
                value2 = formula2;
                break;
            case NOT_CONTAINS:
                opType = "notContains";
                value1 = cfRule.getText();
                // value1 = formula2;
                break;
            case NOT_EQUAL:
                opType = "notEqual";
                value1 = formula1;
                break;
            }
            break;
        case COLOR_SCALE:
            opType = "colorScale";
            insertCondFormatRuleObject.put(OCKey.COLOR_SCALE.value(), Utils.createColorScaleSteps(cfRule.getColorScale(), 2, 3));
            break;
        case CONTAINS_BLANKS:
            opType = "blank";
            break;
        case CONTAINS_ERRORS:
            opType = "error";
            break;
        case CONTAINS_TEXT:
            opType = "contains";
                value1 = formulashortName(cf, formula2);
            break;
        case DATA_BAR:
            opType = "dataBar";
            ICfRule cfRule1 =  cf.getR2() == null ? null : cf.getR1();
            insertCondFormatRuleObject.put(OCKey.DATA_BAR.value(), Utils.createDataBarOperation(cfRule.getDataBar(), cfRule1 == null ? null : cfRule1.getDataBar()));
            break;
        case DUPLICATE_VALUES:
            opType = "duplicate";
            break;
        case ENDS_WITH:
            opType = "endsWith";
                value1 = formulashortName(cf, formula2);
            break;
        case EXPRESSION:
            opType = "formula";
            value1 = formula1;
            break;
        case ICON_SET:
            opType = "iconSet";
            insertCondFormatRuleObject.put(OCKey.ICON_SET.value(), Utils.createIconSet(cfRule.getIconSet()));
            break;
        case NOT_CONTAINS_BLANKS:
            opType = "notBlank";
            break;
        case NOT_CONTAINS_ERRORS:
            opType = "noError";
            break;
        case NOT_CONTAINS_TEXT:
            opType = "notContains";
                value1 = formulashortName(cf, formula2);
            break;
        case TIME_PERIOD:
            switch (cfRule.getTimePeriod()) {
            case LAST_7_DAYS:
                opType = "last7Days";
                break;
            case LAST_MONTH:
                opType = "lastMonth";
                break;
            case LAST_WEEK:
                opType = "lastWeek";
                break;
            case NEXT_MONTH:
                opType = "nextMonth";
                break;
            case NEXT_WEEK:
                opType = "nextWeek";
                break;
            case THIS_MONTH:
                opType = "thisMonth";
                break;
            case THIS_WEEK:
                opType = "thisWeek";
                break;
            case TODAY:
                opType = "today";
                break;
            case TOMORROW:
                opType = "tomorrow";
                break;
            case YESTERDAY:
                opType = "yesterday";
                break;
            }
            break;
        case TOP_10:
            if (cfRule.getRank() != null) {
                value1 = cfRule.getRank();
            }
            if (!cfRule.isBottom() && !cfRule.isPercent()) {
                opType = "topN";
            } else if (!cfRule.isBottom() && cfRule.isPercent()) {
                opType = "topPercent";
            } else if (cfRule.isBottom() && !cfRule.isPercent()) {
                opType = "bottomN";
            } else {
                opType = "bottomPercent";
            }
            break;
        case UNIQUE_VALUES:
            opType = "unique";
            break;
        }
        insertCondFormatRuleObject.put(OCKey.TYPE.value(), opType);
        if(value1!=null) {
            insertCondFormatRuleObject.put(OCKey.VALUE1.value(), value1);
        }
        insertCondFormatRuleObject.put(OCKey.VALUE2.value(), value2);
        insertCondFormatRuleObject.put(OCKey.PRIORITY.value(), cf.getR1() != null && cf.getR1().getPriority() != null ? cf.getR1().getPriority() : cf.getR2().getPriority());
        insertCondFormatRuleObject.put(OCKey.STOP.value(), cfRule.isStopIfTrue());

        final CTDxf dxf = cfRule.getDxf(stylesheet);
        if(dxf!=null) {
            insertCondFormatRuleObject.put(OCKey.ATTRS.value(), Utils.createDxfAttrs(dxf, stylesheet));
        }
        return insertCondFormatRuleObject;
    }

    public static void changeOrInsertCondFormatRule(XlsxOperationDocument operationDocument, boolean inserted, int sheet, String rTypeId, String ranges, String type, Object value1, String value2, Integer priority, Boolean stop, JSONObject op)
        throws FilterException, JSONException {

        final HashMap<String, CfRule> ruleMap = operationDocument.getWorksheet(sheet).getConditionalFormattingRules();
        final List<String> rangeList = !ranges.isEmpty() ? new ArrayList<String>(Arrays.asList(ranges.split(" "))) : null;
        final String id = rTypeId.substring(1);
        JSONObject attrs = op.optJSONObject(OCKey.ATTRS.value());
        CfRule rule;
        if(inserted) {
            final char rType = rTypeId.charAt(0);
            if(rType=='A') {
                rule = new CfRule(new CTCfRule(), rangeList);
            }
            else if(rType=='B') {
                rule = new CfRule(new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule(), rangeList);
            }
            else {
                rule = new CfRule(new CTCfRule(), rangeList);
                rule.setR2(new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule());
            }
            final CfRuleIter ruleIter = rule.iterator();
            while(ruleIter.hasNext()) {
                ruleIter.next().setId(id);
            }
            ruleMap.put(id, rule);
        }
        else {
            rule = ruleMap.get(id);
            if(rangeList!=null) {
                rule.setRanges(rangeList);
            }
        }
        boolean hasMultipleRules = rule.getR1() != null && rule.getR2() != null;
        final CfRuleIter ruleIter = rule.iterator();
        while(ruleIter.hasNext()) {
            final ICfRule cfRule = ruleIter.next();
            if(type!=null) {
                if(!inserted) {
                    // resetting cfRule
                    cfRule.setText(null);
                    cfRule.setTimePeriod(null);
                    cfRule.setAboveAverage(null);
                    cfRule.setEqualAverage(null);
                    cfRule.setStdDev(null);
                    cfRule.setBottom(null);
                    cfRule.setPercent(null);
                    cfRule.setRank(null);
                    cfRule.getFormula().clear();
                    cfRule.setOperator(null);
                }
                switch(type) {
                    case "formula" : {
                        cfRule.setType(STCfType.EXPRESSION);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "between" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.BETWEEN);
                        cfRule.getFormula().add((String)value1);
                        cfRule.getFormula().add(value2);
                        break;
                    }
                    case "notBetween" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.NOT_BETWEEN);
                        cfRule.getFormula().add((String)value1);
                        cfRule.getFormula().add(value2);
                        break;
                    }
                    case "equal" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.EQUAL);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "notEqual" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.NOT_EQUAL);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "less" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.LESS_THAN);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "lessEqual" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.LESS_THAN_OR_EQUAL);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "greater" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.GREATER_THAN);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "greaterEqual" : {
                        cfRule.setType(STCfType.CELL_IS);
                        cfRule.setOperator(STConditionalFormattingOperator.GREATER_THAN_OR_EQUAL);
                        cfRule.getFormula().add((String)value1);
                        break;
                    }
                    case "contains" : {
                        // what to do with the first formula and text ?
                        cfRule.setType(STCfType.CONTAINS_TEXT);
                        cfRule.setOperator(STConditionalFormattingOperator.CONTAINS_TEXT);
                        stringToFormula(cfRule.getFormula(), rule, (String) value1);
                        cfRule.setText((String)value1);
                        break;
                    }
                    case "notContains" : {
                        cfRule.setType(STCfType.NOT_CONTAINS_TEXT);
                        cfRule.setOperator(STConditionalFormattingOperator.NOT_CONTAINS);
                        stringToFormula(cfRule.getFormula(), rule, (String) value1);
                        cfRule.setText((String)value1);
                        break;
                    }
                    case "beginsWith" : {
                        cfRule.setType(STCfType.BEGINS_WITH);
                        cfRule.setOperator(STConditionalFormattingOperator.BEGINS_WITH);
                        stringToFormula(cfRule.getFormula(), rule, (String) value1);
                        cfRule.setText((String)value1);
                        break;
                    }
                    case "endsWith" : {
                        cfRule.setType(STCfType.ENDS_WITH);
                        cfRule.setOperator(STConditionalFormattingOperator.ENDS_WITH);
                        stringToFormula(cfRule.getFormula(), rule, (String) value1);
                        cfRule.setText((String)value1);
                        break;
                    }
                    case "yesterday" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.YESTERDAY);
                        break;
                    }
                    case "today" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.TODAY);
                        break;
                    }
                    case "tomorrow" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.TOMORROW);
                        break;
                    }
                    case "last7Days" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.LAST_7_DAYS);
                        break;
                    }
                    case "lastWeek" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.LAST_WEEK);
                        break;
                    }
                    case "thisWeek" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.THIS_WEEK);
                        break;
                    }
                    case "nextWeek" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.NEXT_WEEK);
                        break;
                    }
                    case "lastYear" :
                    case "lastMonth" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.LAST_MONTH);
                        break;
                    }
                    case "thisYear" :
                    case "thisMonth" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.THIS_MONTH);
                        break;
                    }
                    case "nextYear" :
                    case "nextMonth" : {
                        cfRule.setType(STCfType.TIME_PERIOD);
                        cfRule.setTimePeriod(STTimePeriod.NEXT_MONTH);
                        break;
                    }
                    case "aboveAverage" : {
                        cfRule.setType(STCfType.ABOVE_AVERAGE);
                        cfRule.setAboveAverage(Boolean.TRUE);
                        cfRule.setEqualAverage(Boolean.FALSE);
                        if(value1 instanceof Number) {
                            cfRule.setStdDev(((Number)value1).intValue());
                        }
                        break;
                    }
                    case "atLeastAverage" : {
                        cfRule.setType(STCfType.ABOVE_AVERAGE);
                        cfRule.setAboveAverage(Boolean.TRUE);
                        cfRule.setEqualAverage(Boolean.TRUE);
                        if(value1 instanceof Number) {
                            cfRule.setStdDev(((Number)value1).intValue());
                        }
                        break;
                    }
                    case "belowAverage" : {
                        cfRule.setType(STCfType.ABOVE_AVERAGE);
                        cfRule.setAboveAverage(Boolean.FALSE);
                        cfRule.setEqualAverage(Boolean.FALSE);
                        if(value1 instanceof Number) {
                            cfRule.setStdDev(((Number)value1).intValue());
                        }
                        break;
                    }
                    case "atMostAverage" : {
                        cfRule.setType(STCfType.ABOVE_AVERAGE);
                        cfRule.setAboveAverage(Boolean.FALSE);
                        cfRule.setEqualAverage(Boolean.TRUE);
                        if(value1 instanceof Number) {
                            cfRule.setStdDev(((Number)value1).intValue());
                        }
                        break;
                    }
                    case "topN" : {
                        cfRule.setType(STCfType.TOP_10);
                        cfRule.setBottom(Boolean.FALSE);
                        cfRule.setPercent(Boolean.FALSE);
                        cfRule.setRank(((Integer)value1).longValue());
                        break;
                    }
                    case "topPercent" : {
                        cfRule.setType(STCfType.TOP_10);
                        cfRule.setBottom(Boolean.FALSE);
                        cfRule.setPercent(Boolean.TRUE);
                        cfRule.setRank(((Integer)value1).longValue());
                        break;
                    }
                    case "bottomN" : {
                        cfRule.setType(STCfType.TOP_10);
                        cfRule.setBottom(Boolean.TRUE);
                        cfRule.setPercent(Boolean.FALSE);
                        cfRule.setRank(((Integer)value1).longValue());
                        break;
                    }
                    case "bottomPercent" : {
                        cfRule.setType(STCfType.TOP_10);
                        cfRule.setBottom(Boolean.TRUE);
                        cfRule.setPercent(Boolean.TRUE);
                        cfRule.setRank(((Integer)value1).longValue());
                        break;
                    }
                    case "unique" : {
                        cfRule.setType(STCfType.UNIQUE_VALUES);
                        break;
                    }
                    case "duplicate" : {
                        cfRule.setType(STCfType.DUPLICATE_VALUES);
                        break;
                    }
                    case "blank" : {
                        cfRule.setType(STCfType.CONTAINS_BLANKS);
                        break;
                    }
                    case "notBlank" : {
                        cfRule.setType(STCfType.NOT_CONTAINS_BLANKS);
                        break;
                    }
                    case "error" : {
                        cfRule.setType(STCfType.CONTAINS_ERRORS);
                        break;
                    }
                    case "noError" : {
                        cfRule.setType(STCfType.NOT_CONTAINS_ERRORS);
                        break;
                    }
                    case "colorScale" : {
                        cfRule.setType(STCfType.COLOR_SCALE);
                        Utils.addColorScaleObjectToCfRule(operationDocument, cfRule, op.getJSONArray(OCKey.COLOR_SCALE.value()));
                        break;
                    }
                    case "dataBar" : {
                        cfRule.setType(STCfType.DATA_BAR);
                        Utils.addDataBarObjectToCfRule(operationDocument, cfRule, op.getJSONObject(OCKey.DATA_BAR.value()), hasMultipleRules, priority);
                        break;
                    }
                    case "iconSet" :{
                        cfRule.setType(STCfType.ICON_SET);
                        Utils.addIconSetObjectCfRule(operationDocument, cfRule, op.getJSONObject(OCKey.ICON_SET.value()));
                        break;
                    }
                }
            }
            if(priority!=null && !STCfType.DATA_BAR.equals(cfRule.getType())) {
                cfRule.setPriority(priority.intValue());
            }
            if(stop!=null) {
                cfRule.setStopIfTrue(stop.booleanValue());
            }
            if(attrs!=null) {
                final CTStylesheet stylesheet = operationDocument.getStylesheet(true);
                if(attrs instanceof JSONObject) {
                    final CTDxf ctDxf = Context.getsmlObjectFactory().createCTDxf();
                    applyJsonAttrsToDxf(operationDocument, attrs, ctDxf);
                    cfRule.setDxf(stylesheet, ctDxf);
                }
                else {
                    cfRule.setDxf(stylesheet, null);
                }
            }
        }
    }

    private static void stringToFormula(List<String> formulas, CfRule parentRule, String string) {
        String range = parentRule.getRanges().get(0);

        //irrational behavior in xlsx
        formulas.add("NOT(ISERROR(SEARCH(" + string + "," + range + ")))");
        formulas.add(string);
    }

    private static String formulashortName(CfRule parentRule, String formula2) {
        if (!formula2.isEmpty()) {
            return formula2;
        }

        final ICfRule cfRule = parentRule.getR2() != null ? parentRule.getR2() : parentRule.getR1();
        return "\"" + cfRule.getText().replace("\"", "\"\"") + "\"";
    }

    private static void applyJsonAttrsToDxf(XlsxOperationDocument operationDocument, JSONObject attrs, CTDxf ctDxf)
        throws FilterException, JSONException {

        final CTXf cellXf = Context.getsmlObjectFactory().createCTXf();
        final JSONObject cellProperties = attrs.optJSONObject(OCKey.CELL.value());
        CTStylesheet stylesheet = operationDocument.getStylesheet(true);
        if(cellProperties!=null) {
            CellUtils.applyCellProperties(operationDocument, cellProperties, cellXf, stylesheet, true);
        }
        final JSONObject characterProperties = attrs.optJSONObject(OCKey.CHARACTER.value());
        if(characterProperties!=null) {
            CellUtils.applyCharacterProperties(operationDocument, characterProperties, cellXf, stylesheet);
        }
        final Long fontId = cellXf.getFontId();
        if(fontId!=null) {
            ctDxf.setFont(stylesheet.getFontByIndex(fontId).clone());
        }
        final Long borderId = cellXf.getBorderId();
        if(borderId!=null) {
            ctDxf.setBorder(stylesheet.getBorderByIndex(borderId).clone());
        }
        final Long fillId = cellXf.getFillId();
        if(fillId!=null) {
            ctDxf.setFill(stylesheet.getFillByIndex(fillId).clone());
        }
        final Long numFmtId = cellXf.getNumFmtId();

        if(numFmtId!=null) {
            CTNumFmt numFmt = stylesheet.getNumberFormatById(numFmtId);
            if(numFmt!=null) {
                numFmt = numFmt.clone();
            }
            else {
                numFmt = new CTNumFmt();
                numFmt.setNumFmtId(numFmtId);
                numFmt.setFormatCode(cellProperties == null ? "" : cellProperties.optString(OCKey.FORMAT_CODE.value(), ""));
            }
            ctDxf.setNumFmt(numFmt);
        }
        final CTCellProtection protection = cellXf.getProtection();
        if(protection!=null) {
            ctDxf.setProtection(cellXf.getProtection().clone());
        }
        final CTCellAlignment alignment = cellXf.getAlignment();
        if(alignment!=null) {
            ctDxf.setAlignment(alignment);
        }
    }

    public static void deleteCondFormatRule(XlsxOperationDocument operationDocument, int sheet, String id) {
        operationDocument.getWorksheet(sheet).getConditionalFormattingRules().remove(id.substring(1));
    }
}
