/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.CTTextLineBreak;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;

public class HardBreakComponent extends XlsxComponent {

	HardBreakComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }

    @Override
    public void delete(int count) {
        if(getComponentNumber()==0&&getNextComponent()==null) {
            getParagraphComponent().getParagraph().setEndParaRPr(((CTTextLineBreak)getObject()).getRPr(false));
        }
        super.delete(count);
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
	    throws JSONException {

	    if(attrs!=null) {
            DMLHelper.applyTextCharacterPropertiesFromJson(((CTTextLineBreak)getObject()).getRPr(true), attrs, operationDocument.getContextPart());
        }
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
		throws FilterException, JSONException {

        DMLHelper.createJsonFromTextCharacterProperties(getOperationDocument(), attrs, ((CTTextLineBreak)getObject()).getRPr(false));
		return attrs;
	}

    public ParagraphComponent getParagraphComponent() {
        return (ParagraphComponent)getParentComponent();
    }
}
