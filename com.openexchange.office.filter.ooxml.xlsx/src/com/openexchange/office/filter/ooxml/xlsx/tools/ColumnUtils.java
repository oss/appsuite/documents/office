/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.Col;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class ColumnUtils {

    public enum ColRow {
        Column, Row;
    }

    public static void applyColumnProperties(JSONObject attrs, String style, Col col, long baseColWidth)
        throws FilterException, JSONException {

        if(attrs!=null) {
            // Column properties
            final JSONObject jsonColumnProperties = attrs.optJSONObject(OCKey.COLUMN.value());
            if(jsonColumnProperties!=null) {
                Iterator<String> keys = jsonColumnProperties.keys();
                while(keys.hasNext()) {
                    String attr = keys.next();
                    Object value = jsonColumnProperties.get(attr);
                    if(attr.equals(OCKey.WIDTH.value())) {
                        col.setWidth((value instanceof Number) ? ((Number)value).doubleValue() : null);
                        // mac excel is ignoring the width if customWidth is false
                        col.setCustomWidth(true);
                    }
                    else if(attr.equals(OCKey.VISIBLE.value())) {
                        Boolean hidden = null;
                        if(value instanceof Boolean) {
                            hidden = !(Boolean)value;
                        }
                        col.setHidden(hidden);
                    }
    /*
                    else if(attr.equals("customWidth")) {
                        Boolean customWidth = null;
                        if(value instanceof Boolean) {
                            customWidth = (Boolean)value;
                        }
                        col.setCustomWidth(customWidth);
                    }
    */
                }
            }

            // grouping attributes
            final JSONObject jsonGroupAttrs = attrs.optJSONObject(OCKey.GROUP.value());
            if (jsonGroupAttrs!=null) {
                final int level = jsonGroupAttrs.optInt(OCKey.LEVEL.value(), 0);
                if (level>=1 && level<=7) {
                    col.setOutlineLevel((short)level);
                } else {
                    col.setOutlineLevel(null);
                }
                col.setCollapsed(jsonGroupAttrs.optBoolean(OCKey.COLLAPSED.value()) ? true : null);
            }
        }

        // set new auto-style, or delete the 'style' attribute for the default auto-style
        if (style!=null) {
            Long index = Utils.parseAutoStyleId(style);
            if (index != null) {
                col.setStyle(index.longValue() == 0 ? null : index);
            }
        }

        // Excel 2013 is having problems with empty columns
        if(col.getWidth()==null) {
            col.setWidth(Long.valueOf(baseColWidth).doubleValue());
        }
    }

    public static JSONObject createColumnProperties(XlsxOperationDocument operationDocument, Col col)
        throws JSONException {

        final JSONObject columnProperties = new JSONObject(3);

        final Double columnWidth = col.getWidth();
        if(columnWidth!=null) {
            columnProperties.put(OCKey.WIDTH.value(), columnWidth);
        }
        if(col.isHidden()) {
            columnProperties.put(OCKey.VISIBLE.value(), false);
        }
        if(col.isCustomWidth()) {
            columnProperties.put(OCKey.CUSTOM_WIDTH.value(), true);
        }

        // grouping
        final JSONObject groupProperties = new JSONObject(2);
        final short level = col.getOutlineLevel();
        if (level >= 1 && level <= 7) {
            groupProperties.put(OCKey.LEVEL.value(), level);
        }
        if (col.isCollapsed()) {
            groupProperties.put(OCKey.COLLAPSE.value(), true);
        }

        // create the resulting attribute set
        JSONObject attrs = new JSONObject();
        if (columnProperties.length()>0) {
            attrs.put(OCKey.COLUMN.value(), columnProperties);
        }
        if (groupProperties.length()>0) {
            attrs.put(OCKey.GROUP.value(), groupProperties);
        }

        return (attrs.length()>0) ? attrs : null;
    }
}
