/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import java.util.HashMap;
import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTGraphicalObjectFrame;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.ChartWrapper;
import com.openexchange.office.filter.ooxml.drawingml.DMLGraphic;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;
import com.openexchange.office.filter.ooxml.xlsx.tools.Utils;

public class ShapeGraphicComponent extends XlsxComponent implements IShapeType {

	final private CTGraphicalObjectFrame graphicalObjectFrame;
	private AnchorBase cellAnchor;
	private ShapeType shapeType = ShapeType.UNDEFINED;
	private String shape2png;

	final HashMap<String, Object> drawingProperties = new HashMap<String, Object>();

	public ShapeGraphicComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, CTGraphicalObjectFrame graphicalObjectFrame, AnchorBase cellAnchor, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.graphicalObjectFrame = graphicalObjectFrame;
        this.cellAnchor = cellAnchor;

        DMLGraphic.getGraphicProperties(operationDocument, operationDocument.getContextPart(), graphicalObjectFrame.getGraphic(), drawingProperties);
        if(shapeType == ShapeType.UNDEFINED) {
            if(drawingProperties.containsKey(OCKey.TYPE.value())&&drawingProperties.get(OCKey.TYPE.value()).equals("chart")) {
                shapeType = ShapeType.CHART;
            }
        }
        if(shapeType == ShapeType.UNDEFINED) {
            shape2png = DMLHelper.getReplacementUrlFromGraphicFrame(parentContext, graphicalObjectFrame, graphicalObjectFrame.getXfrm(false), Utils.getParentSheetIndex(this), this.getComponentNumber(), "xlsx");
            if(shape2png!=null) { // the documentconverter needs to be used to create png replacement for this shape
                getOperationDocument().registerShape2pngReplacementImage(shape2png);
            }
        }
	}

	public CTGraphicalObjectFrame getObjectFrame() {
	    return graphicalObjectFrame;
	}

	public HashMap<String, Object> getDrawingProperties() {
	    return drawingProperties;
	}

	@Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
		return null;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {
        if(cellAnchor!=null) {
            final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
            if(drawingAttrs!=null) {
                getNode().setData(Drawings.setDrawingAnchor(drawingAttrs, cellAnchor));
            }
        }
        DMLHelper.applyTransform2DFromJson(graphicalObjectFrame, attrs, cellAnchor!=null);
        DMLHelper.applyNonVisualDrawingProperties(graphicalObjectFrame, attrs.optJSONObject(OCKey.DRAWING.value()));
        DMLHelper.applyNonVisualDrawingShapeProperties(graphicalObjectFrame, attrs.optJSONObject(OCKey.DRAWING.value()));
        if(shapeType==ShapeType.CHART) {
            final JSONObject chartProperties = attrs.optJSONObject(OCKey.CHART.value());
            if(chartProperties!=null) {
                try {
                    Drawings.getChart(drawingProperties).applyChartSpaceProperties(null, chartProperties, attrs.optJSONObject(OCKey.FILL.value()));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
        throws JSONException {

        if(cellAnchor!=null) {
            Drawings.createAnchorOperations(cellAnchor, attrs);
        }
        DMLHelper.createJsonFromTransform2D(attrs, graphicalObjectFrame.getXfrm(false), cellAnchor!=null);
        if(shapeType == ShapeType.CHART) {
            final JSONObject chartProperties = new JSONObject();
            chartProperties.put(OCKey.TYPE.value(), drawingProperties.get(OCKey.CHART_TYPE.value()));
            attrs.put(OCKey.CHART.value(), chartProperties);

            ChartWrapper chart = Drawings.getChart(drawingProperties);
            chart.createJSONAttrs(chartProperties, drawingProperties, attrs);

        }
        else if(shape2png != null) {
            Tools.addFamilyAttribute(attrs, OCKey.IMAGE, OCKey.IMAGE_URL, shape2png);
            Tools.addFamilyAttribute(attrs, OCKey.DRAWING, OCKey.NOT_RESTORABLE, Boolean.TRUE);
        }
        return attrs;
    }

	@Override
	public ShapeType getType() {
		return shapeType;
	}
}
