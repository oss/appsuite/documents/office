/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTDataValidation;
import org.xlsx4j.sml.CTDataValidations;
import org.xlsx4j.sml.IDataValidation;
import org.xlsx4j.sml.IDataValidations;
import org.xlsx4j.sml.STDataValidationErrorStyle;
import org.xlsx4j.sml.STDataValidationOperator;
import org.xlsx4j.sml.STDataValidationType;
import org.xlsx4j.sml.Worksheet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class ValidationUtils {

    final static String[] enumValidationType = { "all", "integer", "number", "list", "date", "time", "length", "custom", "source" };
    final static String[] enumCompare        = { "between", "notBetween", "equal", "notEqual", "less", "lessEqual", "greater", "greaterEqual" };
    final static String[] enumErrorType      = { "error", "warning", "info" };

    // transforming string token "a,b,c" to our style "a","b","c"
    public static String formulaAddQuotations(String value) {
	    if(value.length()>2&&value.charAt(0)=='"'&&value.charAt(value.length()-1)=='"') {
	    	final StringTokenizer tokenizer = new StringTokenizer(value.substring(1, value.length()-1), ",");
	    	final StringBuffer buffer = new StringBuffer();
	    	while(tokenizer.hasMoreTokens()) {
	    		if(buffer.length()>0) {
	    			buffer.append(',');
	    		}
	    		buffer.append('"').append(tokenizer.nextToken()).append('"');
	    	}
	    	value = buffer.toString();
	    }
	    return value;
    }

	// transforming string token "a","b","c" to "a,b,c"
    public static String formulaRemoveQuotations(String value) {
        if(value.length()>2&&value.charAt(0)=='"'&&value.charAt(value.length()-1)=='"') {
        	final StringBuffer buffer = new StringBuffer(value);
        	for(int i=buffer.length()-2; i>1; i--) {
        		if(buffer.charAt(i)=='"') {
        			buffer.deleteCharAt(i);
        		}
        	}
        	return value = buffer.toString();
        }
    	return value;
    }

    public static void addInsertValidationOperation(JSONArray operationsArray, int sheetIndex, int validationIndex, List<CellRefRange> rangeList, STDataValidationType type, STDataValidationOperator operator, String value1, String value2,
                                            boolean showInfo, String infoTitle, String infoText, boolean showError, String errorTitle, String errorText, STDataValidationErrorStyle errorStyle, boolean showDropDown, boolean ignoreBlank)
        throws JSONException {

        final JSONObject insertValidationObject = new JSONObject();
        insertValidationObject.put(OCKey.NAME.value(), OCValue.INSERT_DV_RULE.value());
        insertValidationObject.put(OCKey.SHEET.value(), sheetIndex);
        insertValidationObject.put(OCKey.INDEX.value(), validationIndex);
        final StringBuffer rangesBuffer = new StringBuffer();
        for(CellRefRange cellRefRange:rangeList) {
            if(rangesBuffer.length()!=0) {
                rangesBuffer.append(" ");
            }
            rangesBuffer.append(CellRefRange.getCellRefRange(cellRefRange));
        }
        insertValidationObject.put(OCKey.RANGES.value(), rangesBuffer.toString());
        int val = type.ordinal();
        if(value1!=null && type==STDataValidationType.LIST) {
            if(!value1.isEmpty()&&value1.charAt(0)!='"') {
                val = enumValidationType.length - 1;    // using type source if value does not start with "
            }
        }
        if(val>=enumValidationType.length)
            val = 0;
        insertValidationObject.put(OCKey.TYPE.value(), enumValidationType[val]);
        if(operator!=STDataValidationOperator.BETWEEN) {
            val = operator.ordinal();
            if(val>=enumCompare.length)
                val = 0;
            insertValidationObject.put(OCKey.COMPARE.value(), enumCompare[val]);
        }

        if (value1!=null) {
        	insertValidationObject.put(OCKey.VALUE1.value(), formulaAddQuotations(value1));
        }

        if(value2!=null) {
            insertValidationObject.put(OCKey.VALUE2.value(), formulaAddQuotations(value2));
        }
        if(showInfo==false) {
            insertValidationObject.put(OCKey.SHOW_INFO.value(), showInfo);
        }
        if(infoTitle!=null) {
            insertValidationObject.put(OCKey.INFO_TITLE.value(), infoTitle);
        }
        if(infoText!=null) {
            insertValidationObject.put(OCKey.INFO_TEXT.value(),  infoText);
        }
        if(showError==false) {
            insertValidationObject.put(OCKey.SHOW_ERROR.value(), showError);
        }
        if(errorTitle!=null) {
            insertValidationObject.put(OCKey.ERROR_TITLE.value(), errorTitle);
        }
        if(errorText!=null) {
            insertValidationObject.put(OCKey.ERROR_TEXT.value(), errorText);
        }
        if(errorStyle!=STDataValidationErrorStyle.STOP) {
            val = errorStyle.ordinal();
            if(val>=enumErrorType.length)
                val = 0;
            insertValidationObject.put(OCKey.ERROR_TYPE.value(), enumErrorType[val]);
        }
        if(type==STDataValidationType.LIST&&showDropDown==false) {
            insertValidationObject.put(OCKey.SHOW_DROP_DOWN.value(), showDropDown);
        }
        if(ignoreBlank==false) {
            insertValidationObject.put(OCKey.IGNORE_EMPTY.value(), ignoreBlank);
        }
        operationsArray.put(insertValidationObject);
    }

    private static List<IDataValidations> getDataValidations(XlsxOperationDocument operationDocument, int sheetIndex) {
        return XlsxOperationDocument.getDataValidations(operationDocument.getWorksheet(sheetIndex));
    }

    private static IDataValidation getDataValidation(XlsxOperationDocument operationDocument, int sheetIndex, int index) {

        int i = 0;
        final List<IDataValidations> dataValidationsList = getDataValidations(operationDocument, sheetIndex);
    	for(IDataValidations iDataValidations:dataValidationsList) {
    		if(iDataValidations.getDataValidation().size()+i<=index) {
    			i+=iDataValidations.getDataValidation().size();
    		}
    		else {
    			return iDataValidations.getDataValidation().get(index - i);
    		}
    	}
    	return null;
	}

	private static void fillValidation(IDataValidation iDataValidation, JSONObject op, boolean insert)
		throws JSONException {

		// TODO: check for required attributes if "insert" is true

		if(iDataValidation!=null) {
		    Boolean showInfo = insert ? Boolean.TRUE : null;
		    Boolean showError = insert ? Boolean.TRUE : null;
		    Boolean allowEmpty = insert ? Boolean.TRUE : null;
	        final Iterator<String> keys = op.keys();
	        while(keys.hasNext()) {
	            final String attr = keys.next();
	            final Object value = op.get(attr);
	            if(attr.equals(OCKey.RANGES.value())) {
	            	Utils.applyRangesToSqrefRange(iDataValidation.getsqref(), (String)value);
	            }
	            else if(attr.equals(OCKey.TYPE.value())) {
            		STDataValidationType dataValidationType = STDataValidationType.NONE;
	            	if(value instanceof String) {
	            		final String type = (String)value;
	            		if(type.equals("all")) {
	            			dataValidationType = STDataValidationType.NONE;
	            		} else if(type.equals("integer")) {
	            			dataValidationType = STDataValidationType.WHOLE;
	            		} else if(type.equals("number")) {
	            			dataValidationType = STDataValidationType.DECIMAL;
	            		} else if(type.equals("list")) {
	            			dataValidationType = STDataValidationType.LIST;
	            		} else if(type.equals("date")) {
	            			dataValidationType = STDataValidationType.DATE;
	            		} else if(type.equals("time")) {
	            			dataValidationType = STDataValidationType.TIME;
	            		} else if(type.equals("length")) {
	            			dataValidationType = STDataValidationType.TEXT_LENGTH;
	            		} else if(type.equals("custom")) {
	            			dataValidationType = STDataValidationType.CUSTOM;
	            		} else if(type.equals("source")) {
	            			dataValidationType = STDataValidationType.LIST;
	            		}
	               	}
            		iDataValidation.setType(dataValidationType);
	            }
	            else if(attr.equals(OCKey.COMPARE.value())) {
	            	STDataValidationOperator dataValidationOperator = STDataValidationOperator.BETWEEN;
	            	if(value instanceof String) {
	            		final String operator = (String)value;
	            		if(operator.equals("between")) {
	            			dataValidationOperator = STDataValidationOperator.BETWEEN;
	            		} else if(operator.equals("notBetween")) {
	            			dataValidationOperator = STDataValidationOperator.NOT_BETWEEN;
	            		} else if(operator.equals("equal")) {
	            			dataValidationOperator = STDataValidationOperator.EQUAL;
	            		} else if(operator.equals("notEqual")) {
	            			dataValidationOperator = STDataValidationOperator.NOT_EQUAL;
	            		} else if(operator.equals("less")) {
	            			dataValidationOperator = STDataValidationOperator.LESS_THAN;
	            		} else if(operator.equals("lessEqual")) {
	            			dataValidationOperator = STDataValidationOperator.LESS_THAN_OR_EQUAL;
	            		} else if(operator.equals("greater")) {
	            			dataValidationOperator = STDataValidationOperator.GREATER_THAN;
	            		} else if(operator.equals("greaterEqual")) {
	            			dataValidationOperator = STDataValidationOperator.GREATER_THAN_OR_EQUAL;
	            		}
	            	}
	            	iDataValidation.setOperator(dataValidationOperator);
	            }
	            else if(attr.equals(OCKey.VALUE1.value())) {
            		iDataValidation.setFormula1(formulaRemoveQuotations(value instanceof String?(String)value:null));
	            }
	            else if(attr.equals(OCKey.VALUE2.value())) {
            		iDataValidation.setFormula2(formulaRemoveQuotations(value instanceof String?(String)value:null));
	            }
	            else if(attr.equals(OCKey.SHOW_INFO.value())) {
	                if(value instanceof Boolean) {
	                    showInfo = (Boolean)value;
	                }
	                else {
	                    showInfo = Boolean.TRUE;
	                }
	            }
	            else if(attr.equals(OCKey.INFO_TITLE.value())) {
	            	iDataValidation.setPromptTitle(value instanceof String?(String)value:null);
	            }
	            else if(attr.equals(OCKey.INFO_TEXT.value())) {
	            	iDataValidation.setPrompt(value instanceof String?(String)value:null);
	            }
	            else if(attr.equals(OCKey.SHOW_ERROR.value())) {
                    if(value instanceof Boolean) {
                        showError = (Boolean)value;
                    }
                    else {
                        showError = Boolean.TRUE;
                    }
	            }
	            else if(attr.equals(OCKey.ERROR_TITLE.value())) {
	            	iDataValidation.setErrorTitle(value instanceof String?(String)value:null);
	            }
	            else if(attr.equals(OCKey.ERROR_TEXT.value())) {
	            	iDataValidation.setError(value instanceof String?(String)value:null);
	            }
	            else if(attr.equals(OCKey.ERROR_TYPE.value())) {
	            	STDataValidationErrorStyle dataValidationErrorStyle = STDataValidationErrorStyle.STOP;
	            	if(value instanceof String) {
	            		final String errorStyle = (String)value;
	            		if(errorStyle.equals("error")) {
	            			dataValidationErrorStyle = STDataValidationErrorStyle.STOP;
	            		} else if(errorStyle.equals("warning")) {
	            			dataValidationErrorStyle = STDataValidationErrorStyle.WARNING;
	            		} else if(errorStyle.equals("info")) {
	            			dataValidationErrorStyle = STDataValidationErrorStyle.INFORMATION;
	            		}
	            	}
	            	iDataValidation.setErrorStyle(dataValidationErrorStyle);
	            }
	            else if(attr.equals(OCKey.SHOW_DROP_DOWN.value())) {
	            	iDataValidation.setShowDropDown(value instanceof Boolean?!(Boolean)value:false);
	            }
	            else if(attr.equals(OCKey.IGNORE_EMPTY.value())) {
                    if(value instanceof Boolean) {
                        allowEmpty = (Boolean)value;
                    }
                    else {
                        allowEmpty = Boolean.TRUE;
                    }
	            }
	        }
	        if(showInfo!=null) {
	            iDataValidation.setShowInputMessage(showInfo.booleanValue() ? Boolean.TRUE : null);
	        }
	        if(showError!=null) {
	            iDataValidation.setShowErrorMessage(showError.booleanValue() ? Boolean.TRUE : null);
	        }
	        if(allowEmpty!=null) {
	            iDataValidation.setAllowBlank(allowEmpty.booleanValue() ? Boolean.TRUE :  null);
	        }
		}
	}


	public static void insertValidation(XlsxOperationDocument operationDocument, JSONObject op)
		throws JSONException {

		final int sheet = op.getInt(OCKey.SHEET.value());
		final int index = op.optInt(OCKey.INDEX.value(), -1);

		// the dataValidations might exist within two different lists, so we have to create and insert the correct type
		IDataValidation  iDataValidation = null;
		IDataValidations iDataValidations = null;
		if(index!=-1) {
			iDataValidation = getDataValidation(operationDocument, sheet, index);
			if(iDataValidation!=null) {
				iDataValidations = (IDataValidations)iDataValidation.getParent();
			}
		}
		if(iDataValidations==null) {
			// append the dataValidation
			final List<IDataValidations> dataValidationsList = getDataValidations(operationDocument, sheet);
			if(!dataValidationsList.isEmpty()) {
				iDataValidations = dataValidationsList.get(dataValidationsList.size()-1);
			}
			else {
				final Worksheet worksheet = operationDocument.getWorksheet(sheet);
				final CTDataValidations dataValidations = Context.getsmlObjectFactory().createCTDataValidations();
				dataValidations.setParent(worksheet);
				worksheet.setDataValidations(dataValidations);
				iDataValidations = dataValidations;
			}
		}
		if(iDataValidations instanceof CTDataValidations) {
			final CTDataValidations dataValidations = (CTDataValidations)iDataValidations;
			final List<CTDataValidation> validationList = dataValidations.getDataValidation();
			final int listIndex = iDataValidation!=null?validationList.indexOf(iDataValidation):validationList.size();
			final CTDataValidation dataValidation = Context.getsmlObjectFactory().createCTDataValidation();
			dataValidation.setParent(dataValidations);
			validationList.add(listIndex, dataValidation);
			iDataValidation = dataValidation;
		} else if (iDataValidations instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations) {
			final org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations dataValidations = (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations)iDataValidations;
			final List<org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidation> validationList = dataValidations.getDataValidation();
			final int listIndex = iDataValidation!=null?validationList.indexOf(iDataValidation):validationList.size();
			final org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidation dataValidation = new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidation();
			dataValidation.setParent(dataValidations);
			validationList.add(listIndex, dataValidation);
			iDataValidation = dataValidation;
		}
		if(iDataValidation!=null) {
			fillValidation(iDataValidation, op, true);
		}
	}

    public static void changeValidation(XlsxOperationDocument operationDocument, JSONObject op)
		throws JSONException {

		fillValidation(getDataValidation(operationDocument, op.getInt(OCKey.SHEET.value()), op.getInt(OCKey.INDEX.value())), op, false);
	}

	public static void deleteValidation(XlsxOperationDocument operationDocument, int sheet, int index) {
		final IDataValidation iDataValidation = getDataValidation(operationDocument, sheet, index);
		if(iDataValidation!=null) {
			Object parent = iDataValidation.getParent();
			if(parent instanceof IDataValidations) {
				((IDataValidations)parent).getDataValidation().remove(iDataValidation);
			}
		}
	}
}
