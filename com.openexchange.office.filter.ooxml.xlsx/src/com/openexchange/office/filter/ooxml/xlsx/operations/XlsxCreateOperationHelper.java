/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.operations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTextField;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.vml.VmlCore;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.spreadsheetDrawing.STObjectType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.BookViews;
import org.xlsx4j.sml.CTAuthors;
import org.xlsx4j.sml.CTBookView;
import org.xlsx4j.sml.CTCalcPr;
import org.xlsx4j.sml.CTCellFormula;
import org.xlsx4j.sml.CTCellStyle;
import org.xlsx4j.sml.CTCellStyleXfs;
import org.xlsx4j.sml.CTCellStyles;
import org.xlsx4j.sml.CTCellXfs;
import org.xlsx4j.sml.CTComment;
import org.xlsx4j.sml.CTCommentList;
import org.xlsx4j.sml.CTComments;
import org.xlsx4j.sml.CTDefinedName;
import org.xlsx4j.sml.CTDxf;
import org.xlsx4j.sml.CTExtension;
import org.xlsx4j.sml.CTFont;
import org.xlsx4j.sml.CTFutureMetadata;
import org.xlsx4j.sml.CTFutureMetadataBlock;
import org.xlsx4j.sml.CTHyperlink;
import org.xlsx4j.sml.CTHyperlinks;
import org.xlsx4j.sml.CTMetadata;
import org.xlsx4j.sml.CTMetadataBlocks;
import org.xlsx4j.sml.CTMetadataRecord;
import org.xlsx4j.sml.CTMetadataType;
import org.xlsx4j.sml.CTMetadataTypes;
import org.xlsx4j.sml.CTNumFmt;
import org.xlsx4j.sml.CTNumFmts;
import org.xlsx4j.sml.CTRst;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTTableStyle;
import org.xlsx4j.sml.CTTableStyleElement;
import org.xlsx4j.sml.CTTableStyles;
import org.xlsx4j.sml.CTXf;
import org.xlsx4j.sml.Cell;
import org.xlsx4j.sml.Col;
import org.xlsx4j.sml.Cols;
import org.xlsx4j.sml.DefinedNames;
import org.xlsx4j.sml.IDataValidation;
import org.xlsx4j.sml.IDataValidations;
import org.xlsx4j.sml.Row;
import org.xlsx4j.sml.STCellFormulaType;
import org.xlsx4j.sml.STCellType;
import org.xlsx4j.sml.STTableStyleType;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.SheetData;
import org.xlsx4j.sml.Sheets;
import org.xlsx4j.sml.Workbook;
import org.xlsx4j.sml.WorkbookPr;
import org.xlsx4j.sml.Worksheet;
import org.xlsx4j.sml_2017.CTDynamicArrayProperties;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ColumnRef;
import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.filter.core.spreadsheet.RowRef;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.components.VMLBase;
import com.openexchange.office.filter.ooxml.components.VMLRootComponent;
import com.openexchange.office.filter.ooxml.operations.CreateOperationHelper;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.components.ChartsheetComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.HardBreakComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.ParagraphComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.ShapeGraphicComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.SheetComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.TextComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.TextFieldComponent;
import com.openexchange.office.filter.ooxml.xlsx.tools.AutoFilterHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.CellUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.ColumnUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.ConditionalFormattings;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;
import com.openexchange.office.filter.ooxml.xlsx.tools.MergeCellHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.RowUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.SheetUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.TableHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.Utils;
import com.openexchange.office.filter.ooxml.xlsx.tools.ValidationUtils;
import org.xlsx4j.sml_2018.*;

public class XlsxCreateOperationHelper extends CreateOperationHelper {

    private final SpreadsheetMLPackage spreadsheetMLPackage;
    private final Map<Long, String> indexToStyleId = new HashMap<Long, String>();
    final int maxCellCount;
    final int maxSheetCount;
    int currentCellCount = 0;

    // List of threaded comments, needed to send the threaded comment not as a simple comment (note)
    private List<String> threadedCommentIds = new ArrayList<>();
    private Map<String, JSONObject> threadedCommentOperations = new HashMap<String, JSONObject>();

    public XlsxCreateOperationHelper(XlsxOperationDocument _operationDocument, JSONArray mOperationsArray)
        throws FilterException {

        super(_operationDocument, mOperationsArray);
        spreadsheetMLPackage = _operationDocument.getPackage();
        maxCellCount = operationDocument.getIntegerOfficeConfigurationValue("//spreadsheet/maxCells", 500000);
        maxSheetCount = operationDocument.getIntegerOfficeConfigurationValue("//spreadsheet/maxSheets", 256);

        final WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();
        final Sheets sheets = workbook.getSheets();
        if(sheets!=null) {
            final DLList<Sheet> sheetList = sheets.getContent();
            if(maxSheetCount>=0) {
                if(sheetList.size()>maxSheetCount) {
                    final FilterException filterException = new FilterException("", ErrorCode.COMPLEXITY_TOO_HIGH);
                    filterException.setSubType(FilterException.ST_MAX_SHEET_COUNT_EXCEEDED);
                    throw filterException;
                }
            }
        }
        final CTStylesheet stylesheet = getOperationDocument().getStylesheet(false);
        if(stylesheet!=null) {
            // creating styles
            final CTCellStyleXfs cellStyleXfs = stylesheet.getCellStyleXfs();
            if(cellStyleXfs!=null) {
                BuiltinIds.getUsedStyles(stylesheet, indexToStyleId, null);
            }
        }
    }

    @Override
    public XlsxOperationDocument getOperationDocument() {
        return (XlsxOperationDocument)operationDocument;
    }

    public void createStyleOperations()
        throws Exception {

        CTStylesheet stylesheet = getOperationDocument().getStylesheet(false);
        if(stylesheet!=null) {

            // create number formats
            final CTNumFmts numFmts = stylesheet.getNumberFormats(false);
            if(numFmts!=null) {
                for (CTNumFmt numFmt: numFmts.getNumFmt()) {
                    final JSONObject operation = new JSONObject(3);
                    operation.put(OCKey.NAME.value(), OCValue.INSERT_NUMBER_FORMAT.value());
                    operation.put(OCKey.ID.value(), numFmt.getNumFmtId());
                    operation.put(OCKey.CODE.value(), numFmt.getFormatCode());
                    operationsArray.put(operation);
                }
            }

            // creating styles
            final CTCellStyleXfs cellStyleXfs = stylesheet.getCellStyleXfs();
            if(cellStyleXfs!=null) {
                final List<CTXf> cellStyleXfList = cellStyleXfs.getXf();
                for(int i=0;i<cellStyleXfList.size();i++) {
                    final String styleId = indexToStyleId.get(Long.valueOf(i));
                    if(styleId!=null) {
                        final CTXf cellStyleXf = cellStyleXfList.get(i);
                        String styleName = styleId;
                        JSONObject jsonProperties = new JSONObject();
                        Commons.mergeJsonObjectIfUsed(OCKey.CELL.value(), jsonProperties, CellUtils.createCellProperties(cellStyleXf, stylesheet, true));
                        final CTFont ctFont = stylesheet.getFontByIndex(CellUtils.getAttributeIndex(cellStyleXf.getFontId(), cellStyleXf.isApplyFont(), true));
                        Commons.mergeJsonObjectIfUsed(OCKey.CHARACTER.value(), jsonProperties, CellUtils.createCharacterProperties(new JSONObject(), stylesheet, ctFont));
                        final JSONObject applyProperties = createApplyProperties(cellStyleXf, true);
                        if(!applyProperties.isEmpty()) {
                            jsonProperties.put(OCKey.APPLY.value(), applyProperties);
                        }
                        JSONObject attrs = jsonProperties.length()>0?jsonProperties:null;
                        Boolean hidden = null;
                        Integer uiPriority = null;
                        Boolean isDefault = null;

                        // check for proper UI styleName
                        CTCellStyles cellStyles = stylesheet.getCellStyles();
                        if(cellStyles!=null) {
                            for(CTCellStyle cellStyle:cellStyles.getCellStyle()) {
                                if(cellStyle.getXfId()==i) {
                                    final String name = cellStyle.getName();
                                    if(name!=null&&!name.isEmpty()) {
                                        styleName = name;
                                    }
                                    if(i==0) {
                                        isDefault = Boolean.TRUE;
                                    }
                                    hidden = cellStyle.isHidden();
                                    break;
                                }
                            }
                        }
                        if(hidden==null||!hidden.booleanValue()) {
                            operationsArray.put(createInsertStyleSheetOperation("cell", true, styleId, styleName, attrs, null, hidden, uiPriority, isDefault, null));
                        }
                    }
                }
            }

            // creating autostyles
            final CTCellXfs cellXfs = stylesheet.getCellXfs();
            if(cellXfs!=null) {
                final List<CTXf> cellXfsList = cellXfs.getXf();
                for(int i=0;i<cellXfsList.size();i++) {
                    final CTXf xf = cellXfsList.get(i);
                    final String styleId = "a" + Integer.toString(i);
                    JSONObject jsonProperties = new JSONObject();
                    Commons.mergeJsonObjectIfUsed(OCKey.CELL.value(), jsonProperties, CellUtils.createCellProperties(xf, stylesheet, false));
                    final CTFont ctFont = stylesheet.getFontByIndex(CellUtils.getAttributeIndex(xf.getFontId(), xf.isApplyFont(), false));
                    Commons.mergeJsonObjectIfUsed(OCKey.CHARACTER.value(), jsonProperties, CellUtils.createCharacterProperties(new JSONObject(), stylesheet, ctFont));
                    final JSONObject applyProperties = createApplyProperties(xf, false);
                    if(!applyProperties.isEmpty()) {
                        jsonProperties.put(OCKey.APPLY.value(), applyProperties);
                   }
                   final Long xfId = xf.getXfId();
                    if(xfId!=null) {
                        final String parentId = indexToStyleId.get(xfId);
                        if(parentId!=null) {
                            jsonProperties.put(OCKey.STYLE_ID.value(), parentId);
                        }
                    }
                    operationsArray.put(createInsertAutoStyleOperation(styleId, jsonProperties.length()>0?jsonProperties:null, i==0));
                }
            }
            final CTTableStyles tableStyles = stylesheet.getTableStyles(false);
            if(tableStyles!=null) {
                final Iterator<CTTableStyle> tableStyleIter = tableStyles.getTableStyle().iterator();
                while(tableStyleIter.hasNext()) {
                    final CTTableStyle tableStyle = tableStyleIter.next();
                    createInsertTableStyleOperation(stylesheet, tableStyle);
                }
            }
        }
    }

    final void createApplyProperty(JSONObject jsonApplyProps, String propName, Boolean xfProperty, boolean defaultValue)
        throws JSONException {

        // OOXML default value depends on XF type (true in styles, false in cells); operation default is always true
        boolean applyValue = (xfProperty == null) ? defaultValue : xfProperty.booleanValue();
        if (!applyValue) { jsonApplyProps.put(propName, false); }
    }

    final JSONObject createApplyProperties(CTXf cellStyleXf, boolean defaultValue)
        throws JSONException {

        final JSONObject jsonApplyProperties = new JSONObject(0);
        createApplyProperty(jsonApplyProperties, OCKey.BORDER.value(), cellStyleXf.isApplyBorder(), defaultValue);
        createApplyProperty(jsonApplyProperties, OCKey.ALIGN.value(), cellStyleXf.isApplyAlignment(), defaultValue);
        createApplyProperty(jsonApplyProperties, OCKey.NUMBER.value(), cellStyleXf.isApplyNumberFormat(), defaultValue);
        createApplyProperty(jsonApplyProperties, OCKey.PROTECT.value(), cellStyleXf.isApplyProtection(), defaultValue);
        createApplyProperty(jsonApplyProperties, OCKey.FONT.value(), cellStyleXf.isApplyFont(), defaultValue);
        createApplyProperty(jsonApplyProperties, OCKey.FILL.value(), cellStyleXf.isApplyFill(), defaultValue);
        return jsonApplyProperties;
    }

    final void createInsertTableStyleOperation(CTStylesheet stylesheet, CTTableStyle tableStyle)
        throws JSONException {

        final JSONObject insertStylesheetObject = new JSONObject(6);
        insertStylesheetObject.put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
        insertStylesheetObject.put(OCKey.TYPE.value(), "table");
        insertStylesheetObject.put(OCKey.STYLE_ID.value(), tableStyle.getName());
        insertStylesheetObject.put(OCKey.STYLE_NAME.value(), tableStyle.getName());

        final JSONObject attrs = new JSONObject();
        final Iterator<CTTableStyleElement> tableStyleElementIter = tableStyle.getTableStyleElement().iterator();
        while(tableStyleElementIter.hasNext()) {
            final CTTableStyleElement tableStyleElement = tableStyleElementIter.next();
            final STTableStyleType tableStyleElementType = tableStyleElement.getType();
            if(tableStyleElementType!=null) {
                switch(tableStyleElementType) {
                    case WHOLE_TABLE: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.WHOLE_TABLE.value(), attrs);
                        break;
                    }
                    case FIRST_COLUMN_STRIPE: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.BAND1_VERT.value(), attrs);
                        break;
                    }
                    case SECOND_COLUMN_STRIPE: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.BAND2_VERT.value(), attrs);
                        break;
                    }
                    case FIRST_ROW_STRIPE: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.BAND1_HORZ.value(), attrs);
                        break;
                    }
                    case SECOND_ROW_STRIPE: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.BAND2_HORZ.value(), attrs);
                        break;
                    }
                    case LAST_COLUMN: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.LAST_COL.value(), attrs);
                        break;
                    }
                    case FIRST_COLUMN: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.FIRST_COL.value(), attrs);
                        break;
                    }
                    case HEADER_ROW: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.FIRST_ROW.value(), attrs);
                        break;
                    }
                    case TOTAL_ROW: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.LAST_ROW.value(), attrs);
                        break;
                    }
                    case FIRST_HEADER_CELL: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.NORTH_WEST_CELL.value(), attrs);
                        break;
                    }
                    case LAST_HEADER_CELL: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.NORTH_EAST_CELL.value(), attrs);
                        break;
                    }
                    case FIRST_TOTAL_CELL: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.SOUTH_WEST_CELL.value(), attrs);
                        break;
                    }
                    case LAST_TOTAL_CELL: {
                        createTableStyleElementOperation(stylesheet, tableStyleElement, OCKey.SOUTH_EAST_CELL.value(), attrs);
                        break;
                    }

                    // ignoring, used for pivot tables only:
                    case BLANK_ROW:
                    case FIRST_COLUMN_SUBHEADING:
                    case FIRST_ROW_SUBHEADING:
                    case FIRST_SUBTOTAL_COLUMN:
                    case FIRST_SUBTOTAL_ROW:
                    case PAGE_FIELD_LABELS:
                    case PAGE_FIELD_VALUES:
                    case SECOND_COLUMN_SUBHEADING:
                    case SECOND_ROW_SUBHEADING:
                    case SECOND_SUBTOTAL_COLUMN:
                    case SECOND_SUBTOTAL_ROW:
                    case THIRD_COLUMN_SUBHEADING:
                    case THIRD_ROW_SUBHEADING:
                    case THIRD_SUBTOTAL_COLUMN:
                    case THIRD_SUBTOTAL_ROW:
                    default:
                        break;

                }
            }
        }
        if(!attrs.isEmpty()) {
            insertStylesheetObject.put(OCKey.ATTRS.value(), attrs);
        }
        insertStylesheetObject.put(OCKey.CUSTOM.value(), true);
        operationsArray.put(insertStylesheetObject);
    }

    public void createTableStyleElementOperation(CTStylesheet stylesheet, CTTableStyleElement tableStyleElement, String conditionalTableStyle, JSONObject attrs)
        throws JSONException {

        final Long dxfId = tableStyleElement.getDxfId();
        if(dxfId!=null) {
            final CTDxf dxf = stylesheet.getDxfsByIndex(dxfId);
            if(dxf!=null) {
                final JSONObject jsonObject = Utils.createDxfAttrs(dxf, stylesheet);
                if(tableStyleElement.getSize()!=1) {
                    jsonObject.put(OCKey.STRIPSET_SIZE.value(), tableStyleElement.getSize());
                }
                attrs.put(conditionalTableStyle, jsonObject);
            }
        }
    }

    public void createOperations()
        throws Exception {

        final WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();

        createDocumentSettings(workbook);
        createThemeOperations(null, null, null);
        createStyleOperations();
        createInsertNameOperations(workbook, null);
        final Sheets sheets = workbook.getSheets();
        final DLList<Sheet> sheetList = sheets.getContent();
        for(int sheetIndex = 0; sheetIndex < sheetList.size(); sheetIndex++) {
            createSheetOperations(true, true, workbook, sheetList.get(sheetIndex), sheetIndex);
        }
    }

    public void createMetaOperations(DocumentProperties documentProperties)
        throws Exception {

        final WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();

        final int activeSheetIndex = createDocumentSettings(workbook);
        createThemeOperations(null, null, null);
        createStyleOperations();
        createInsertNameOperations(workbook, null);
        documentProperties.put(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX, activeSheetIndex);
        documentProperties.put(DocumentProperties.PROP_SPREADHSEET_CURRENT_SHEET_INDEX, Integer.valueOf(0));
    }

    public void createActivePartOperations(DocumentProperties documentProperties)
        throws Exception {

        final WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();

        final int activeSheetIndex = (Integer)documentProperties.get(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX);
        final Sheets sheets = workbook.getSheets();
        final DLList<Sheet> sheetList = sheets.getContent();
        for(int sheetIndex = 0; sheetIndex < sheetList.size(); sheetIndex++) {
            createSheetOperations(true, activeSheetIndex==sheetIndex, workbook, sheetList.get(sheetIndex), sheetIndex);
        }
        documentProperties.put(DocumentProperties.PROP_SPREADSHEET_SHEET_COUNT, sheetList.size());
    }

    public boolean createNextPartOperations(DocumentProperties documentProperties)
        throws Exception {

        final WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();

        final int activeSheetIndex = (Integer)documentProperties.get(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX);
        int currentSheetIndex = (Integer)documentProperties.get(DocumentProperties.PROP_SPREADHSEET_CURRENT_SHEET_INDEX);

        final Sheets sheets = workbook.getSheets();
        final DLList<Sheet> sheetList = sheets.getContent();
        for(; currentSheetIndex < sheetList.size();) {
            createSheetOperations(false, activeSheetIndex!=currentSheetIndex, workbook, sheetList.get(currentSheetIndex), currentSheetIndex);
            documentProperties.put(DocumentProperties.PROP_SPREADHSEET_CURRENT_SHEET_INDEX, Integer.valueOf(currentSheetIndex+1));
            return true;
        }
        return false;
    }

    private void updateCellCount(Worksheet worksheet)
        throws FilterException {

        if(maxCellCount>0) {
            final SheetData sheetData = worksheet.getSheetData();
            if(sheetData!=null) {
                final Iterator<Row> rowIterator = sheetData.createRowIterator();
                while(rowIterator.hasNext()) {
                    final Row row = rowIterator.next();
                    currentCellCount += row.getCellCount();
                    if(currentCellCount>maxCellCount) {
                        throw new FilterException("", ErrorCode.COMPLEXITY_TOO_HIGH);
                    }
                }
            }
        }
    }

    /* returns the active sheet */

    private int createDocumentSettings(Workbook workbook)
        throws JSONException {

        long activeSheet = 0;
        // core settings (modified/created)
        final JSONObject attrs = new JSONObject(1);
        final JSONObject documentAttrs = createCoreProperties(attrs);
        documentAttrs.put(OCKey.FILE_FORMAT.value(), "ooxml");
        documentAttrs.put(OCKey.COLS.value(), 16384);
        documentAttrs.put(OCKey.ROWS.value(), 1048576);
        final BookViews bookViews = workbook.getBookViews();
        if(bookViews!=null) {
            final List<CTBookView> bookViewList = bookViews.getWorkbookView();
            if(!bookViewList.isEmpty()) {
                final CTBookView bookView = bookViewList.get(0);
                activeSheet = bookView.getActiveTab();
                documentAttrs.put(OCKey.ACTIVE_SHEET.value(), activeSheet);
            }
        }

        // property nullDate: serial number 0 represents 1904-01-01 in "date1904" mode
        final WorkbookPr wbPr = workbook.getWorkbookPr(false);
        if (wbPr!=null && wbPr.isDate1904()) {
            documentAttrs.put(OCKey.NULL_DATE.value(), "1904-01-01");
        }

        // property calcOnLoad: recalculate all formulas once after import
        final CTCalcPr calcPr = workbook.getCalcPr(false);
        if (calcPr!=null && calcPr.isFullCalcOnLoad()) {
            documentAttrs.put(OCKey.CALC_ON_LOAD.value(), true);
        }
        addSetDocumentAttributesOperation(attrs);
        return (int)activeSheet;
    }

    private void createInsertNameOperations(Workbook workbook, Long sheetIndex)
        throws JSONException {

        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames==null) {
            return;
        }
        final HashMap<String, CTDefinedName> definedNameMap = definedNames.getDefinedNameMap();
        for(CTDefinedName definedName:definedNameMap.values()) {
            Long localSheetId = definedName.getLocalSheetId();
            if ((sheetIndex == null) ? (localSheetId == null) : sheetIndex.equals(localSheetId)) {
                addInsertNameOperation(sheetIndex, definedName.getName(), definedName.getValue()!=null ? definedName.getValue() : "", definedName.isHidden());
            }
        }
    }

    private Object getCellObject(Cell cell)
        throws Exception {

        STCellType cellType = cell.getT();
        if(cellType==null) {
            cellType = STCellType.N;
        }

        final JSONObject cellObject = new JSONObject();
        Object v = null;

        final CTCellFormula cellFormula = cell.getF();
        if(cellFormula!=null) {
            STCellFormulaType cellFormulaType = cellFormula.getT();
            final String formulaExpr = cellFormula.getValue();
            if (formulaExpr!=null && !formulaExpr.isEmpty()) {
                cellObject.put(OCKey.F.value(), formulaExpr);
            }
            if(cellFormulaType==STCellFormulaType.SHARED) {
                final Long si = cellFormula.getSi();
                if(si != null) {
                    cellObject.put(OCKey.SI.value(), si);
                }
                final String sr = cellFormula.getRef();
                if (sr != null) {
                    cellObject.put(OCKey.SR.value(), sr);
                }
            } else if (cellFormulaType==STCellFormulaType.ARRAY) {
                final String mr = cellFormula.getRef();
                if (mr != null) {
                    cellObject.put(OCKey.MR.value(), mr);
                }
            }
        }
        if(cellType==STCellType.INLINE_STR) {
            final CTRst is = cell.getIs();
            if(is!=null) {
                if(is.getT()!=null) {
                    v = new String(is.getT().getValue());
                }
            }
        }
        else if(cell.getV()!=null) {
            if(cellType==STCellType.N) {
                try {
                    v = Double.valueOf(Double.parseDouble(cell.getV()));
                }
                catch(NumberFormatException e) {
                    // TODO: ups
                }
            }
            else if(cellType==STCellType.S) {
                v = getOperationDocument().getSharedString(cell.getV());
            }
            else if(cellType==STCellType.B) {
                if(cell.getV().equals("0")||cell.getV().toLowerCase().equals("false")) {
                    v = Boolean.FALSE;;
                }
                else if(cell.getV().equals("1")||cell.getV().toLowerCase().equals("true")) {
                    v = Boolean.TRUE;
                }
            }
            else if(cellType==STCellType.E) {
                cellObject.put(OCKey.E.value(), cell.getV());
            }
            else {  // STCellType.STR, STCellType.F (ARRAY or DATA_TABLE format)
                v = cell.getV();
            }
        }
        final Long cm = cell.getCm();
        if(cm!=null && cm.intValue() >= 1) {
            final int cmIndex = cm.intValue() - 1;
            final CTMetadata metadata = getOperationDocument().getMetadata(false);
            if(metadata!=null) {
                final CTMetadataBlocks cellMetadataBlocks = metadata.getCellMetadata();
                if(cellMetadataBlocks!=null && cellMetadataBlocks.getCount() > cmIndex && cellMetadataBlocks.getBk().size() > cmIndex) {
                    final Iterator<CTMetadataRecord> metadataRecordIter = cellMetadataBlocks.getBk().get(cmIndex).getRc().iterator();

                    Boolean fDynamic = null;
                    while(metadataRecordIter.hasNext()) {
                        final CTMetadataRecord metadataRecord = metadataRecordIter.next();
                        final int tIndex = Long.valueOf(metadataRecord.getT()).intValue() - 1;
                        final int vIndex = Long.valueOf(metadataRecord.getV()).intValue();
                        if(tIndex >= 0 && vIndex >= 0) {
                            CTMetadataTypes metadataTypes = metadata.getMetadataTypes();
                            if(metadataTypes.getCount() > tIndex && metadataTypes.getMetadataType().size() > tIndex) {
                                final CTMetadataType type = metadataTypes.getMetadataType().get(tIndex);
                                if("XLDAPR".equals(type.getName())) {   // we are only interested in the XLDAPR type
                                    final Iterator<CTFutureMetadata> futureMetadataIter = metadata.getFutureMetadata().iterator();
                                    while(futureMetadataIter.hasNext()) {
                                        final CTFutureMetadata futureMetadata = futureMetadataIter.next();
                                        if(type.getName().equals(futureMetadata.getName())) {   // check that we have the correct futureMetadata by name
                                            if(futureMetadata.getCount() > vIndex && futureMetadata.getBk().size() > vIndex) {
                                                final CTFutureMetadataBlock futureMetadataBlock  = futureMetadata.getBk().get(vIndex);
                                                final CTExtension ext = futureMetadataBlock.getExtLst().getExtensionByUri("{bdbb8cdc-fa1e-496e-a857-3c3f30c029c3}", false);
                                                final Object any = ext.getAny();
                                                if(any instanceof JAXBElement) {
                                                    final Object o = ((JAXBElement<?>)any).getValue();
                                                    if(o instanceof CTDynamicArrayProperties) {
                                                        fDynamic = ((CTDynamicArrayProperties)o).getfDynamic();
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if(fDynamic!=null) {
                        cellObject.put(OCKey.MD.value(), fDynamic);
                    }
                }
            }
        }
        final long s = cell.getStyle();
        if ((s > 0) || cellObject.isEmpty()) {
            cellObject.put(OCKey.S.value(), "a" + Long.toString(s));
        }
        if(cellObject.isEmpty() && v!=null) {
            return v;
        }
        if(v!=null) {
            cellObject.put(OCKey.V.value(), v);
        }
        return cellObject;
    }

    private void createSheetOperations(boolean insertsheet, boolean sheetcontent, Workbook workbook, Sheet sheet, int sheetIndex)
        throws Exception {

        final Part part = XlsxOperationDocument.getSheetPart(spreadsheetMLPackage, sheet);
        final String sheetType = XlsxOperationDocument.getSheetType(part);

        if(sheetType.equals("worksheet")) {
            updateCellCount(((WorksheetPart)part).getJaxbElement());
        }
        if(insertsheet) {
            final JSONObject attrs = new JSONObject();
            final JSONObject sheetProps = SheetUtils.createSheetProperties(sheet);
            if(sheetProps!=null&&!sheetProps.isEmpty()) {
                attrs.put(OCKey.SHEET.value(), sheetProps);
            }
            addInsertSheetOperation(sheetIndex, sheet.getName(), sheetType, attrs);
        }
        if(sheetcontent) {
            createInsertNameOperations(workbook, Long.valueOf(sheetIndex));
            if(sheetType.equals("worksheet")) {
                final Worksheet worksheet = ((WorksheetPart)part).getJaxbElement();
                createChangeSheetOperation(sheet, worksheet, sheetIndex);
                createWorksheetOperations(sheet, (WorksheetPart)part, sheetIndex);
                createThreadedCommentOperations((WorksheetPart)part, sheetIndex);
                createCommentOperations((WorksheetPart)part, sheetIndex);
            }
            else if(sheetType.equals("chartsheet")) {
                createChartsheetOperations(sheet, sheetIndex);
            }
        }
        if(part instanceof SerializationPart) {
            ((SerializationPart<?>)part).setJaxbElement(null);
        }
    }

    private void createThreadedCommentOperations(WorksheetPart worksheetPart, int sheetIndex) throws InvalidFormatException, JSONException {
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);

        if (threadedComments != null) {
            final CTPersonList personList = getOperationDocument().getPersons(false);
            final Map<String, Integer> idToIdx = new HashMap<>();
            final List<CellRef> cellRefs = new ArrayList<CellRef>();
            for (CTThreadedComment comment : threadedComments.getThreadedComments()) {

                final JSONObject jsonOperation = new JSONObject();

                jsonOperation.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
                jsonOperation.put(OCKey.SHEET.value(), sheetIndex);

                CellRef cellRef = comment.getCellRef(true);
                jsonOperation.put(OCKey.ANCHOR.value(), CellRef.getCellRef(cellRef));
                jsonOperation.put(OCKey.TEXT.value(), createMentions(comment, personList, jsonOperation));

                if (comment.getDateTime() != null) {
                    String date = comment.getDateTime().toXMLFormat();
                    if (!date.toLowerCase().endsWith("z")) {
                        date += "Z";
                    }
                    jsonOperation.put(OCKey.DATE.value(), date);
                }

                if (personList != null) {
                    CTPerson person = personList.getPersonById(comment.getPersonId());
                    if (person != null) {
                        jsonOperation.put(OCKey.AUTHOR.value(), person.getDisplayName());
                        jsonOperation.put(OCKey.AUTHOR_ID.value(), person.getUserId());
                        jsonOperation.put(OCKey.AUTHOR_PROVIDER.value(), person.getProviderId());
                    }
                }
                if (comment.getParentId() == null) {

                    if (cellRefs.contains(cellRef)) {
                        throw new FilterException("an threaded comment on this address exists. " + cellRef.toString(), ErrorCode.CRITICAL_ERROR);
                    }
                    idToIdx.put(comment.getId(), 0);
                    jsonOperation.put(OCKey.INDEX.value(), 0);

                    threadedCommentOperations.put(cellRef.toString(), jsonOperation);
                }
                else {
                    int index = idToIdx.get(comment.getParentId()) + 1;
                    idToIdx.put(comment.getParentId(), index);

                    jsonOperation.put(OCKey.INDEX.value(), index);
                }
                cellRefs.add(cellRef);
                if (comment.getDone() != null) {
                    jsonOperation.put(OCKey.DONE.value(), comment.getDone());
                }
                threadedCommentIds.add("tc=" + comment.getId().toLowerCase());
                operationsArray.put(jsonOperation);
            }
        }
    }

    private String createMentions(CTThreadedComment comment, CTPersonList personList, JSONObject operation) throws JSONException {
        // Mentions
        final CTThreadedCommentMentions _mentions = comment.getMentions();
        if (personList==null || _mentions==null || _mentions.getMentions().isEmpty()) {
            return comment.getText();
        }
        final List<CTMention> mentions = _mentions.getMentions();
        Collections.sort(mentions, new Comparator<CTMention>() {
            @Override
            public int compare(CTMention d0, CTMention d1) {
                return (int)d0.getStartIndex() - (int)d1.getStartIndex();
            }
        });
        // This was only needed when removing @-signs
        // int offset = 0;
        final StringBuilder builder = new StringBuilder(comment.getText());
        final JSONArray jsonMentions = new JSONArray(mentions.size());
        for (CTMention mention : mentions) {
            final CTPerson person = personList.getPersonById(mention.getPersonId());
            final int index = (int)mention.getStartIndex() /* - offset */;
            if(person!=null && mention.getLength() > 0 && builder.charAt(index) == '@') {
                // removing '@'
                // builder.deleteCharAt(index);

                final JSONObject jsonMention = new JSONObject();
                jsonMention.put(OCKey.DISPLAY_NAME.value(), person.getDisplayName());
                jsonMention.put(OCKey.USER_ID.value(), person.getUserId());
                jsonMention.put(OCKey.PROVIDER_ID.value(), person.getProviderId());
                jsonMention.put(OCKey.POS.value(), index);
                // jsonMention.put(OCKey.LENGHT.value(), mention.getLength() - 1);
                jsonMention.put(OCKey.LENGTH.value(), mention.getLength());
                jsonMentions.put(jsonMention);
                // offset++;
            }
        }
        if(!jsonMentions.isEmpty()) {
            operation.put(OCKey.MENTIONS.value(), jsonMentions);
        }
        return builder.toString();
    }

    private void createCommentOperations(WorksheetPart worksheetPart, int sheetIndex) throws Exception {
        final CTComments comments = worksheetPart.getComments(false);
        if(comments!=null) {
            final CTCommentList commentList = comments.getCommentList();
            final CTAuthors authors = comments.getAuthors();
            final List<String> authorList = authors!=null ? authors.getAuthor() : null;
            final List<CTComment> cList = commentList.getComment();
            for(int i=0; i<cList.size(); i++) {

            	final CTComment comment = cList.get(i);
                final CellRef cellRef = comment.getCellRef(true);

            	String author = null;
                final int authorId = Long.valueOf(comment.getAuthorId()).intValue();
                if(authorList!=null&&authorId>=0&&authorId<authorList.size()) {
                    author = authorList.get(authorId);
                    if (author != null && threadedCommentIds.contains(author.toLowerCase())) {
                        JSONObject threadedCommpentOperation = threadedCommentOperations.get(cellRef.toString());
                        if (threadedCommpentOperation != null) {
                            JSONObject vmlAttributes = getVMLCommentAttributes(worksheetPart, cellRef);
                            if (vmlAttributes != null && vmlAttributes.has(OCKey.DRAWING.value())) {
                                JSONObject drawing = new JSONObject();
                                drawing.put(OCKey.DRAWING.value(), vmlAttributes.get(OCKey.DRAWING.value()));
                                threadedCommpentOperation.put(OCKey.ATTRS.value(), drawing);
                            }
                        }
                        continue;
                    }
                }

                final JSONObject jsonInsertNoteOperation = new JSONObject(6);
                jsonInsertNoteOperation.put(OCKey.NAME.value(), OCValue.INSERT_NOTE.value());
                jsonInsertNoteOperation.put(OCKey.SHEET.value(), sheetIndex);

                jsonInsertNoteOperation.put(OCKey.ANCHOR.value(), CellRef.getCellRef(cellRef));

                if (author!=null && author.length()>0) {
                    jsonInsertNoteOperation.put(OCKey.AUTHOR.value(), author);
                }

                final String text = comment.toString();
                if (text!=null && text.length()>0) {
                	jsonInsertNoteOperation.put(OCKey.TEXT.value(), comment.toString());
                }
                JSONObject vmlAttributes = getVMLCommentAttributes(worksheetPart, cellRef);

                if (vmlAttributes != null) {
                    jsonInsertNoteOperation.put(OCKey.ATTRS.value(), vmlAttributes);
                }

                operationsArray.put(jsonInsertNoteOperation);
            }
        }
    }

    private JSONObject getVMLCommentAttributes(WorksheetPart worksheetPart, CellRef cellRef) throws Exception {

        final VMLPart vmlPart = worksheetPart.getVMLPart(false);

        if(vmlPart!=null) {
            IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
            while(component!=null) {
                if(component instanceof VMLBase) {
                    final VMLBase vmlBase = (VMLBase)component;
                    final VmlCore vmlCore = vmlBase.getVMLCoreObject();
                    final CTClientData clientData = vmlCore.getClientData(false);
                    if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                        final Integer row = clientData.getRow();
                        final Integer column = clientData.getColumn();
                        if(row!=null&&column!=null) {
                            if(cellRef.getColumn()==column.intValue()&&cellRef.getRow()==row.intValue()) {
                                final JSONObject attrs = new JSONObject();
                                ((OfficeOpenXMLComponent)component).createJSONAttrs(attrs);
                                JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
                                final String visible = clientData.getVisible();
                                if (visible==null) {
                                    if (drawingAttrs==null) {
                                        drawingAttrs = new JSONObject(1);
                                        attrs.put(OCKey.DRAWING.value(), drawingAttrs);
                                    }
                                    drawingAttrs.put(OCKey.HIDDEN.value(), true);
                                } else if (drawingAttrs!=null) {
                                    drawingAttrs.remove(OCKey.HIDDEN.value());
                                }
                                if(!attrs.isEmpty()) {
                                    return attrs;
                                }
                                return null;
                            }
                        }
                    }
                }
                component = component.getNextComponent();
            }
        }
        return null;
    }

    private void createChangeSheetOperation(Sheet sheet, Worksheet worksheet, int sheetIndex)
        throws JSONException {

        final JSONObject properties = new JSONObject();
        properties.put(OCKey.SHEET.value(), SheetUtils.createWorksheetProperties(sheet, worksheet));
        properties.put(OCKey.COLUMN.value(), SheetUtils.createWorksheetColumnProperties(getOperationDocument(), worksheet));
        final JSONObject rowProperties = SheetUtils.createWorksheetRowProperties(worksheet);
        if(rowProperties!=null&&!rowProperties.isEmpty()) {
            properties.put(OCKey.ROW.value(), rowProperties);
        }
        addChangeSheetOperation(sheetIndex, properties);
    }

    private void createWorksheetOperations(Sheet sheet, final WorksheetPart worksheetPart, int sheetIndex)
        throws Exception {

        final Worksheet worksheet = worksheetPart.getJaxbElement();

        // column attributes... row attributes ... cells...
        createColumnOperations(worksheet.getCols(), sheetIndex);
        createRowOperations(worksheet, sheetIndex);

        MergeCellHelper.createMergeCellOperations(operationsArray, sheetIndex, worksheet.getMergeCells());
        AutoFilterHelper.createAutoFilterOperations(operationsArray, sheetIndex, worksheet.getAutoFilter(false));
        TableHelper.createTableOperations(operationsArray, worksheetPart, sheetIndex, worksheet.getTableParts(false));

        // hyperlinks
        final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
        if(hyperlinks!=null) {
            final List<CTHyperlink> hyperlinkList = hyperlinks.getHyperlink();
            for(int i=0; i<hyperlinkList.size();i++) {
                final CTHyperlink hyperlink = hyperlinkList.get(i);
                final CellRefRange cellRefRange = hyperlink.getCellRefRange(false);
                if(cellRefRange!=null) {
                    String hyperlinkUrl = Commons.getUrl(worksheetPart, hyperlink.getId());
                    if(hyperlinkUrl!=null&&!hyperlinkUrl.isEmpty()) {
                        final JSONObject insertHyperlinkOperation = new JSONObject(5);
                        insertHyperlinkOperation.put(OCKey.NAME.value(), OCValue.INSERT_HYPERLINK.value());
                        insertHyperlinkOperation.put(OCKey.SHEET.value(), sheetIndex);
                        insertHyperlinkOperation.put(OCKey.RANGES.value(), CellRefRange.getCellRefRange(cellRefRange));
                        final String location = hyperlink.getLocation();
                        if(location!=null && !location.isEmpty()) {
                            if(!hyperlinkUrl.contains(String.valueOf('#')) && !location.contains(String.valueOf('#'))) {
                                hyperlinkUrl = hyperlinkUrl + '#' + location;   // DOCS-4505
                            }
                        }
                        insertHyperlinkOperation.put(OCKey.URL.value(), hyperlinkUrl);
                        operationsArray.put(insertHyperlinkOperation);
                    }
                }
            }
        }
        final SheetComponent sheetComponent = new SheetComponent(sheet, getOperationDocument(), sheetIndex);
        final List<Integer> sheetPosition = new ArrayList<Integer>(1);
        sheetPosition.add(sheetIndex);
        IComponent<OfficeOpenXMLOperationDocument> childComponent = sheetComponent.getNextChildComponent(null, null);
        while(childComponent!=null) {
            createDrawingOperations(childComponent, sheetPosition, null);
            childComponent = childComponent.getNextComponent();
        }

/*
        //drawings
        final CTDrawing drawing = worksheet.getDrawing();
        if(drawing!=null) {
            final String drawingId = drawing.getId();
            if(drawingId!=null&&!drawingId.isEmpty()) {
                final RelationshipsPart sourceRelationships = worksheetPart.getRelationshipsPart();
                if(sourceRelationships!=null) {
                    final Part part = sourceRelationships.getPart(drawingId);
                    if(part instanceof org.docx4j.openpackaging.parts.DrawingML.Drawing) {
                        Drawings.createDrawingOperations(this.getOperationDocument(), operationsArray, (org.docx4j.openpackaging.parts.DrawingML.Drawing)part, sheetIndex);
                    }
                }
            }
        }
*/

        // validations
        final List<IDataValidations> validations = XlsxOperationDocument.getDataValidations(worksheet);
        int validationIndex = 0;
        for(IDataValidations dataValidations:validations) {
            for(IDataValidation dataValidation:dataValidations.getDataValidation()) {
                createDataValidation(sheetIndex, validationIndex++, dataValidation);
            }
        }

        // conditional formattings
        ConditionalFormattings.createOperations(getOperationDocument(), operationsArray, worksheet, sheetIndex);
    }

    private void createChartsheetOperations(Sheet sheet, int sheetIndex)
        throws Exception {

        final ChartsheetComponent sheetComponent = new ChartsheetComponent(sheet, getOperationDocument(), sheetIndex);
        final List<Integer> sheetPosition = new ArrayList<Integer>(1);
        sheetPosition.add(sheetIndex);
        IComponent<OfficeOpenXMLOperationDocument> childComponent = sheetComponent.getNextChildComponent(null, null);
        while(childComponent!=null) {
            createDrawingOperations(childComponent, sheetPosition, null);
            childComponent = childComponent.getNextComponent();
        }
    }

    private void createDrawingOperations(IComponent<OfficeOpenXMLOperationDocument> drawingComponent, List<Integer> parentPosition, String target)
        throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(drawingComponent.getComponentNumber());
        final JSONObject jsonInsertOperation = new JSONObject(5);
        jsonInsertOperation.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        jsonInsertOperation.put(OCKey.TYPE.value(), ((IShapeType)drawingComponent).getType().toString());
        jsonInsertOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = ((OfficeOpenXMLComponent)drawingComponent).createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertOperation);
        if(((IShapeType)drawingComponent).getType()==ShapeType.CHART) {
            Drawings.getChart(((ShapeGraphicComponent) drawingComponent).getDrawingProperties()).createChartSpaceOperations(operationsArray, position);
        }
        IComponent<OfficeOpenXMLOperationDocument> shapeChildComponent = drawingComponent.getNextChildComponent(null, null);

        while(shapeChildComponent!=null) {
            if(shapeChildComponent instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)shapeChildComponent, position, null);
            }
            else {
                createDrawingOperations(shapeChildComponent, position, target);
            }
            shapeChildComponent = shapeChildComponent.getNextComponent();
        }
    }

    public void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(paragraphComponent.getComponentNumber());
        final JSONObject jsonInsertParagraphOperation = new JSONObject(4);
        jsonInsertParagraphOperation.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        jsonInsertParagraphOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertParagraphOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = paragraphComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertParagraphOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertParagraphOperation);
        IComponent<OfficeOpenXMLOperationDocument> paragraphChild = paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            if(paragraphChild instanceof TextComponent) {
                int n = paragraphChild.getComponentNumber();
                final String[] tokens =
                    (((CTRegularTextRun)(((TextComponent)paragraphChild).getObject())).getT()).split("\\t", -1);
                for(int i=0; i < tokens.length; i++) {
                    final String t = tokens[i];
                    if(!t.isEmpty()) {
                        createTextInsertOperation(t, n, position, target);
                        n += t.length();
                    }
                    if(tokens.length!=1&&i!=tokens.length-1) {
                        createInsertTabOperation(n, position, target);
                        n++;
                    }
                }
            }
            else if(paragraphChild instanceof TextFieldComponent) {
                createInsertFieldOperation((TextFieldComponent)paragraphChild, position, target);
            }
            else if(paragraphChild instanceof HardBreakComponent) {
                createHardBreakOperation((HardBreakComponent)paragraphChild, position, target);
            }
            paragraphChild = paragraphChild.getNextComponent();
        }
        paragraphChild = paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            if(paragraphChild instanceof TextComponent) {
                createTextAttributesOperation((TextComponent)paragraphChild, position, target);
            }
            else if(paragraphChild instanceof TextFieldComponent) {
                createTextFieldAttributesOperation((TextFieldComponent)paragraphChild, position, target);
            }
            else if(paragraphChild instanceof HardBreakComponent) {
                //
            }
            paragraphChild = paragraphChild.getNextComponent();
        }
    }

    public void createTextInsertOperation(String text, int textPosition, List<Integer> parentPosition, String target)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textPosition);

        final JSONObject jsonInsertTextOperation = new JSONObject(4);
        jsonInsertTextOperation.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        jsonInsertTextOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTextOperation.put(OCKey.TARGET.value(), target);
        }
        jsonInsertTextOperation.put(OCKey.TEXT.value(), text);
        operationsArray.put(jsonInsertTextOperation);
    }

    public void createInsertTabOperation(int textPosition, List<Integer> parentPosition, String target)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textPosition);

        final JSONObject jsonInsertTabOperation = new JSONObject(3);
        jsonInsertTabOperation.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        jsonInsertTabOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTabOperation.put(OCKey.TARGET.value(), target);
        }
        operationsArray.put(jsonInsertTabOperation);
    }

    public void createTextAttributesOperation(TextComponent textComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final JSONObject attrs = textComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {

            final JSONObject jsonSetAttributesOperation = new JSONObject(5);
            jsonSetAttributesOperation.put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());

            final List<Integer> start = new ArrayList<Integer>(parentPosition);
            start.add(textComponent.getComponentNumber());
            jsonSetAttributesOperation.put(OCKey.START.value(), start);

            final CTRegularTextRun textRun = (CTRegularTextRun)textComponent.getObject();
            if(textRun.getT().length()>1) {
                start.set(start.size()-1, (textComponent.getComponentNumber()+textRun.getT().length())-1);
                jsonSetAttributesOperation.put(OCKey.END.value(), start);
            }

            if(target!=null) {
                jsonSetAttributesOperation.put(OCKey.TARGET.value(), target);
            }
            jsonSetAttributesOperation.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(jsonSetAttributesOperation);
        }
    }

    public void createTextFieldAttributesOperation(TextFieldComponent textFieldComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final JSONObject attrs = textFieldComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {

            final JSONObject jsonSetAttributesOperation = new JSONObject(4);
            jsonSetAttributesOperation.put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());

            final List<Integer> start = new ArrayList<Integer>(parentPosition);
            start.add(textFieldComponent.getComponentNumber());
            jsonSetAttributesOperation.put(OCKey.START.value(), start);
            if(target!=null) {
                jsonSetAttributesOperation.put(OCKey.TARGET.value(), target);
            }
            jsonSetAttributesOperation.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(jsonSetAttributesOperation);
        }
    }

    public void createHardBreakAttributesOperation(HardBreakComponent hardBreakComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final JSONObject attrs = hardBreakComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {

            final JSONObject jsonSetAttributesOperation = new JSONObject(5);
            jsonSetAttributesOperation.put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());

            final List<Integer> start = new ArrayList<Integer>(parentPosition);
            start.add(hardBreakComponent.getComponentNumber());
            jsonSetAttributesOperation.put(OCKey.START.value(), start);

            if(target!=null) {
                jsonSetAttributesOperation.put(OCKey.TARGET.value(), target);
            }
            jsonSetAttributesOperation.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(jsonSetAttributesOperation);
        }
    }

    public void createInsertFieldOperation(TextFieldComponent textFieldComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textFieldComponent.getComponentNumber());

        final JSONObject jsonInsertSimpleFieldOperation = new JSONObject(6);
        jsonInsertSimpleFieldOperation.put(OCKey.NAME.value(), OCValue.INSERT_FIELD.value());
        jsonInsertSimpleFieldOperation.put(OCKey.START.value(), position);
        final CTTextField textField = (CTTextField)textFieldComponent.getObject();
        jsonInsertSimpleFieldOperation.put(OCKey.TYPE.value(), textField.getType());
        jsonInsertSimpleFieldOperation.put(OCKey.REPRESENTATION.value(), textField.getT());
        if(target!=null) {
            jsonInsertSimpleFieldOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = textFieldComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertSimpleFieldOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertSimpleFieldOperation);
    }

    public void createHardBreakOperation(HardBreakComponent hardBreakComponent, List<Integer> parentPosition, String target)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(hardBreakComponent.getComponentNumber());

        final JSONObject jsonInsertHardBreakOperation = new JSONObject(4);
        jsonInsertHardBreakOperation.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        jsonInsertHardBreakOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertHardBreakOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = hardBreakComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertHardBreakOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertHardBreakOperation);
    }

    private void createDataValidation(int sheetIndex, int validationIndex, IDataValidation dataValidation)
        throws JSONException {

        final List<CellRefRange> cellRefRangeList = Utils.createCellRefRangeListFromSqrefList(dataValidation.getsqref());
        ValidationUtils.addInsertValidationOperation(operationsArray, sheetIndex, validationIndex, cellRefRangeList, dataValidation.getType(), dataValidation.getOperator(), dataValidation.getFormula1(), dataValidation.getFormula2(), dataValidation.isShowInputMessage(),
            dataValidation.getPromptTitle(), dataValidation.getPrompt(), dataValidation.isShowErrorMessage(), dataValidation.getErrorTitle(), dataValidation.getError(), dataValidation.getErrorStyle(),
                dataValidation.isShowDropDown()==false, dataValidation.isAllowBlank());
    }

    private boolean compareCell(Cell source, Cell dest) {
        if(source.getF()!=null||dest.getF()!=null)
            return false;
        if(source.getIs()!=null||dest.getIs()!=null)
            return false;
        if(source.getT()!=dest.getT())
            return false;
        if(source.getVm()!=dest.getVm())
            return false;
        if(source.getCm()!=dest.getCm())
            return false;
        if(source.getStyle()!=dest.getStyle())
            return false;
        if(source.getV()==null&&dest.getV()==null)
            return true;
        if(source.getV()!=null&&dest.getV()!=null) {
            return source.getV().equals(dest.getV());
        }
        return false;
    }

    private void createRowOperations(Worksheet worksheet, int sheetIndex)
        throws Exception {

        final SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            return;
        }

        // first we will create row attributes
        Iterator<Row> rowIterator = sheetData.createRowIterator();

        Row lastRow = null;
        int lastRowNumber = 0;
        int rowCount = 0;

        while(rowIterator.hasNext()) {
            final Row row = rowIterator.next();

            if(lastRow!=null&&row.getRow()==lastRowNumber+rowCount&&RowUtils.compareRowProperties(lastRow, row)) {
                rowCount++;
            }
            else {
                if(lastRow!=null) {
                    final JSONObject attrs = RowUtils.createRowProperties(lastRow);
                    final Long style = lastRow.getS();
                    if(attrs!=null || style!=null) {
                        addChangeRows(sheetIndex, lastRowNumber, (lastRowNumber - 1) + rowCount, attrs, style);
                    }
                }
                lastRow = row;
                lastRowNumber = row.getRow();
                rowCount = 1;
            }
            if(!rowIterator.hasNext()&&lastRow!=null) {
                final JSONObject attrs = RowUtils.createRowProperties(lastRow);
                final Long style = lastRow.getS();
                if(attrs!=null || style!=null) {
                    addChangeRows(sheetIndex, lastRowNumber, (lastRowNumber - 1) + rowCount, attrs, style);
                }
            }
        }

        JSONObject contents = new JSONObject();

        rowIterator = sheetData.createRowIterator();
        while(rowIterator.hasNext()) {
            final Row row = rowIterator.next();

            CellRefRange lastCellRefRange = null;
            Cell lastCell = null;

            Iterator<Cell> cellIterator = row.createCellIterator();
            while(cellIterator.hasNext()) {
                final Cell cell = cellIterator.next();
                if(lastCellRefRange==null) {
                    lastCellRefRange = new CellRefRange(cell.getColumn(), row.getRow(), cell.getColumn(), row.getRow());
                    lastCell = cell;
                }
                else {
                    if(cell.getColumn() == lastCellRefRange.getEnd().getColumn() + 1 && compareCell(cell, lastCell)) {
                        lastCellRefRange.getEnd().setColumn(cell.getColumn());
                    }
                    else {
                        contents = addCellContent(contents, sheetIndex, lastCellRefRange, lastCell);
                        lastCellRefRange = new CellRefRange(cell.getColumn(), row.getRow(), cell.getColumn(), row.getRow());
                        lastCell = cell;
                    }
                }
            }
            if(lastCellRefRange!=null) {
                contents = addCellContent(contents, sheetIndex, lastCellRefRange, lastCell);
            }
        }
        addChangeCellsOperation(sheetIndex, contents);
    }

     private JSONObject addCellContent(JSONObject contents, int sheetIndex, CellRefRange cellRefRange, Cell cell) throws JSONException, Exception {
         if(contents.length() == getOperationDocument().getMaxJsonSize()) {
             addChangeCellsOperation(sheetIndex, contents);
             contents = new JSONObject();
         }
        final String key = cellRefRange.getStart().equals(cellRefRange.getEnd()) ? CellRef.getCellRef(cellRefRange.getStart()) : CellRefRange.getCellRefRange(cellRefRange);
        contents.put(key, getCellObject(cell));
        return contents;
    }

    private void createColumnOperations(List<Cols> colsIter, int sheetIndex)
        throws Exception {

        if(colsIter==null)
            return;

        for(Cols cols:colsIter) {
            List<Col> colIter = cols.getCol();
            if(colIter!=null) {
                for(Col col:colIter) {
                    final JSONObject attrs = ColumnUtils.createColumnProperties(getOperationDocument(), col);
                    final long style = col.getStyle();
                    if(attrs!=null) {
                        addChangeColumns(sheetIndex, (int)col.getMin()-1, (int)col.getMax()-1, attrs, style);
                    }
                }
            }
        }
    }

    public void addSetDocumentAttributesOperation(JSONObject attrs)
        throws JSONException {

        if(attrs!=null&&!attrs.isEmpty()) {
            final JSONObject setDocumentAttributesObject = new JSONObject(2);
            setDocumentAttributesObject.put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
            setDocumentAttributesObject.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(setDocumentAttributesObject);
        }
    }

    public void addInsertSheetOperation(int sheetIndex, final String sheetName, final String type, final JSONObject attrs)
        throws JSONException {

        final JSONObject insertSheetObject = new JSONObject(5);
        insertSheetObject.put(OCKey.NAME.value(), OCValue.INSERT_SHEET.value());
        insertSheetObject.put(OCKey.SHEET.value(), sheetIndex);
        if(type!=null&&!type.equals("worksheet")) {
            insertSheetObject.put(OCKey.TYPE.value(), type);
        }
        insertSheetObject.put(OCKey.SHEET_NAME.value(), sheetName);
        if(attrs!=null&&!attrs.isEmpty()) {
            insertSheetObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(insertSheetObject);
    }

    public void addChangeSheetOperation(int sheetIndex, final JSONObject attrs)
        throws JSONException {

        final JSONObject setSheetAttributesObject = new JSONObject(3);
        setSheetAttributesObject.put(OCKey.NAME.value(), OCValue.CHANGE_SHEET.value());
        setSheetAttributesObject.put(OCKey.SHEET.value(), sheetIndex);
        setSheetAttributesObject.put(OCKey.ATTRS.value(), attrs);
        operationsArray.put(setSheetAttributesObject);
    }

    final static List<Object> objectList = new ArrayList<Object>();

    public void addChangeCellsOperation(int sheetIndex, JSONObject contents)
        throws JSONException {

        if(!contents.isEmpty()) {
            final JSONObject insertCellsObject = new JSONObject(4);
            insertCellsObject.put(OCKey.NAME.value(), OCValue.CHANGE_CELLS.value());
            insertCellsObject.put(OCKey.SHEET.value(), sheetIndex);
            insertCellsObject.put(OCKey.CONTENTS.value(), contents);
            operationsArray.put(insertCellsObject);
        }
    }

    public void addChangeRows(int sheetIndex, int start, int end, JSONObject attrs, Long style)
        throws JSONException {

        final JSONObject operation = new JSONObject(6);
        if (attrs!=null && attrs.length()>0) {
            operation.put(OCKey.ATTRS.value(), attrs);
        }
        if (style!=null && style.longValue()>0) {
            operation.put(OCKey.S.value(), "a" + Long.toString(style));
        }

        if (operation.length()>0) {
            operation.put(OCKey.NAME.value(), OCValue.CHANGE_ROWS.value());
            operation.put(OCKey.SHEET.value(), sheetIndex);
            operation.put(OCKey.INTERVALS.value(), new Interval(new RowRef(start), new RowRef(end)).toString());
            operationsArray.put(operation);
        }
    }

    public void addChangeColumns(int sheetIndex, int start, int end, JSONObject attrs, Long style)
        throws JSONException {

        final JSONObject operation = new JSONObject(6);
        if (attrs!=null && attrs.length()>0) {
            operation.put(OCKey.ATTRS.value(), attrs);
        }
        if (style!=null && style.longValue()>0) {
            operation.put(OCKey.S.value(), "a" + Long.toString(style));
        }

        if (operation.length()>0) {
            operation.put(OCKey.NAME.value(), OCValue.CHANGE_COLUMNS.value());
            operation.put(OCKey.SHEET.value(), sheetIndex);
            operation.put(OCKey.INTERVALS.value(), new Interval(new ColumnRef(start), new ColumnRef(end)).toString());
            operationsArray.put(operation);
        }
    }

    public void addInsertNameOperation(Long sheetIndex, String name, String formula, boolean hidden)
        throws JSONException {

        final JSONObject addInsertNameObject = new JSONObject(5);
        addInsertNameObject.put(OCKey.NAME.value(), OCValue.INSERT_NAME.value());
        if(sheetIndex!=null) {
            addInsertNameObject.put(OCKey.SHEET.value(), sheetIndex);
        }
        if(hidden) {
            addInsertNameObject.put(OCKey.HIDDEN.value(), true);
        }
        addInsertNameObject.put(OCKey.LABEL.value(), name);
        addInsertNameObject.put(OCKey.FORMULA.value(), formula);

        operationsArray.put(addInsertNameObject);
    }
}
