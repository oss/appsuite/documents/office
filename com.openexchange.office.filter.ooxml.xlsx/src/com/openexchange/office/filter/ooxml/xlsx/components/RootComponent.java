/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.json.JSONObject;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.Sheets;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class RootComponent extends XlsxComponent {

	public RootComponent(XlsxOperationDocument operationDocument, Sheets sheets) {
		super(operationDocument, new DLNode<Object>(sheets), 0);
    }

    @Override
    public OfficeOpenXMLComponent getNextComponent() {
        return null;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> rootNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)rootNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        OfficeOpenXMLComponent nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, rootNode.getData());
            if(o instanceof Sheet) {
            	nextComponent = new SheetComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {
        //
    }
    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) {
        return attrs;
    }
}
