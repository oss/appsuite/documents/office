/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.IShapeBasic;
import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTShape;
import org.docx4j.jaxb.Context;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;
import com.openexchange.office.filter.ooxml.xlsx.tools.Utils;

public class ShapeComponent extends XlsxComponent implements IShapeType {

	final private CTShape shape;
	private AnchorBase cellAnchor;
    private String imageReplacementUrl = null;
    private ShapeType shapeType = ShapeType.UNDEFINED;

	/*
	 * cellAnchor needs to be set for root shapes otherwise it has to be null
	 */

	public ShapeComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, CTShape shape, AnchorBase cellAnchor, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.shape = shape;
        this.cellAnchor = cellAnchor;

        if(shape.isWordArt()) {
            imageReplacementUrl = DMLHelper.getReplacementUrl(DMLHelper.getHashFromShape(org.docx4j.jaxb.Context.getJc(), getOperationDocument().getPackage(), shape), Utils.getParentSheetIndex(this), getComponentNumber(), "xlsx");
            if(imageReplacementUrl != null) {
                getOperationDocument().registerShape2pngReplacementImage(imageReplacementUrl);
            }
        }
        if(imageReplacementUrl == null) {
            shapeType = ShapeType.SHAPE;
        }
	}

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        // no child shapes if we are using an image replacement
        if(imageReplacementUrl != null) {
            return null;
        }

        final DLList<Object> nodeList = shape.getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, shape.getTxBody(false));
            if(o instanceof CTTextParagraph) {
            	nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = shape.getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
        	case PARAGRAPH: {
	            final Child newChild = Context.getDmlObjectFactory().createCTTextParagraph();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ParagraphComponent(parentContext, newChildNode, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        if(cellAnchor!=null) {
            final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
            if(drawingAttrs!=null) {
                getNode().setData(Drawings.setDrawingAnchor(drawingAttrs, cellAnchor));
            }
        }
        com.openexchange.office.filter.ooxml.drawingml.Shape.applyAttrsFromJSON(operationDocument, attrs, shape, cellAnchor!=null, true);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

        if(cellAnchor!=null) {
            Drawings.createAnchorOperations(cellAnchor, attrs);
        }
        if(imageReplacementUrl != null) {
            com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(operationDocument, attrs, (IShapeBasic)shape, cellAnchor!=null);
            Tools.addFamilyAttribute(attrs, OCKey.IMAGE, OCKey.IMAGE_URL, imageReplacementUrl);
            Tools.addFamilyAttribute(attrs, OCKey.DRAWING, OCKey.NOT_RESTORABLE, Boolean.TRUE);
        }
        else {
            com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(operationDocument, attrs, shape, cellAnchor!=null);
        }
        return attrs;
    }

	@Override
	public ShapeType getType() {
		return shapeType;
	}

	public CTTextBody getTextBody(boolean forceCreate) {
		return shape.getTxBody(forceCreate);
	}
}
