/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTConnector;
import org.docx4j.dml.spreadsheetDrawing.CTGraphicalObjectFrame;
import org.docx4j.dml.spreadsheetDrawing.CTGroupShape;
import org.docx4j.dml.spreadsheetDrawing.CTPicture;
import org.docx4j.dml.spreadsheetDrawing.CTShape;
import org.docx4j.dml.spreadsheetDrawing.CTTwoCellAnchor;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.Sheet;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;

public class ChartsheetComponent extends XlsxComponent {

    public ChartsheetComponent(Sheet sheet, XlsxOperationDocument operationDocument, int _componentNumber) {
        super(operationDocument, new DLNode<Object>(new ChartsheetWrapper(new DLNode<Object>(sheet), operationDocument)), _componentNumber);
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> sheetNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)sheetNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, sheetNode.getData());
            if(o instanceof AnchorBase) {
                final AnchorBase cellAnchor = (AnchorBase)o;
                if(cellAnchor.getGrpSp()!=null) {
                    nextComponent = new ShapeGroupComponent(this, childNode, cellAnchor.getGrpSp(), cellAnchor, nextComponentNumber);
                }
                else if(cellAnchor.getSp()!=null) {
                    nextComponent = new ShapeComponent(this, childNode, cellAnchor.getSp(), cellAnchor, nextComponentNumber);
                }
                else if(cellAnchor.getGraphicFrame()!=null) {
                    nextComponent = new ShapeGraphicComponent(this, childNode, cellAnchor.getGraphicFrame(), cellAnchor, nextComponentNumber);
                }
                else if(cellAnchor.getCxnSp()!=null) {
                    nextComponent = new ShapeConnectorComponent(this, childNode, cellAnchor.getCxnSp(), cellAnchor, nextComponentNumber);
                }
                else if(cellAnchor.getPic()!=null) {
                    nextComponent = new ShapePicComponent(this, childNode, cellAnchor.getPic(), cellAnchor, nextComponentNumber);
                } else if (cellAnchor.getAlternateContent() != null) {
                    //System.out.println("ChartsheetComponent.getNextChildComponent " + cellAnchor.getAlternateContent());
                }
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        final CTTwoCellAnchor cellAnchor = new CTTwoCellAnchor();
        switch(type) {
        	case AC_SHAPE: {
	            final CTShape newChild = Drawings.createShape();
	            cellAnchor.setSp(newChild);
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeComponent(parentContext, newChildNode, newChild, cellAnchor, number);
        	}
        	case AC_GROUP: {
	            final CTGroupShape newChild = Drawings.createGroupShape();
	            cellAnchor.setGrpSp(newChild);
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeGroupComponent(parentContext, newChildNode, newChild, cellAnchor, number);
        	}
        	case AC_CHART: {
                try {
                    final CTGraphicalObjectFrame newChild = Drawings.createChart(getOperationDocument(), getOperationDocument().getContextPart(), new JSONObject());
                    cellAnchor.setGraphicFrame(newChild);
                    newChild.setParent(contextNode.getData());
                    final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                    DLList.addNode(referenceNode, newChildNode, true);
                    return new ShapeGraphicComponent(parentContext, newChildNode, newChild, cellAnchor, number);
                }
                catch(Exception e) {
                    throw new UnsupportedOperationException();
                }
        	}
            case AC_CONNECTOR: {
                final CTConnector newChild = Drawings.createConnectorShape();
                cellAnchor.setCxnSp(newChild);
                newChild.setParent(contextNode.getData());
                final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                DLList.addNode(referenceNode, newChildNode, true);
                return new ShapeConnectorComponent(parentContext, newChildNode, newChild, cellAnchor, number);
            }
            case AC_IMAGE: {
                final CTPicture newChild = Drawings.createImage();
                cellAnchor.setPic(newChild);
                newChild.setParent(contextNode.getData());
                final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                DLList.addNode(referenceNode, newChildNode, true);
                return new ShapePicComponent(parentContext, newChildNode, newChild, cellAnchor, number);
            }
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {
        //
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

		return attrs;
    }
}
