/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.Iterator;
import org.docx4j.sharedtypes.STVerticalAlignRun;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTBooleanProperty;
import org.xlsx4j.sml.CTBorder;
import org.xlsx4j.sml.CTBorderPr;
import org.xlsx4j.sml.CTCellAlignment;
import org.xlsx4j.sml.CTCellProtection;
import org.xlsx4j.sml.CTColor;
import org.xlsx4j.sml.CTFill;
import org.xlsx4j.sml.CTFont;
import org.xlsx4j.sml.CTFontName;
import org.xlsx4j.sml.CTFontScheme;
import org.xlsx4j.sml.CTFontSize;
import org.xlsx4j.sml.CTGradientFill;
import org.xlsx4j.sml.CTPatternFill;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTUnderlineProperty;
import org.xlsx4j.sml.CTVerticalAlignFontProperty;
import org.xlsx4j.sml.CTXf;
import org.xlsx4j.sml.ObjectFactory;
import org.xlsx4j.sml.STBorderStyle;
import org.xlsx4j.sml.STFontScheme;
import org.xlsx4j.sml.STHorizontalAlignment;
import org.xlsx4j.sml.STPatternType;
import org.xlsx4j.sml.STUnderlineValues;
import org.xlsx4j.sml.STVerticalAlignment;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.ThemeFonts;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;


public class CellUtils {

	private final static Logger log = LoggerFactory.getLogger(CellUtils.class);

    static public Long getAttributeIndex(Long index, Boolean isApply, boolean createStyle) {
        if(createStyle) {
            if((isApply==null||isApply.booleanValue())&&index!=null) {
                return index;
            }
            return Long.valueOf(0);
        }
        return index!=null?index:Long.valueOf(0);
    }

    static private CTFill createDetachedFill(CTStylesheet stylesheet, CTXf xf) {

        Long fillId = xf.getFillId();
        if(fillId==null||fillId.longValue()==0) {
            return Context.getsmlObjectFactory().createCTFill();
        }
        CTFill ctFill = stylesheet.getFillByIndex(fillId);
        if(ctFill!=null) {
            return ctFill.clone();
        }
        return Context.getsmlObjectFactory().createCTFill();
    }

    static private CTBorder createDetachedBorder(CTStylesheet stylesheet, CTXf xf) {

        Long borderId = xf.getBorderId();
        if(borderId==null||borderId.longValue()==0) {
            return Context.getsmlObjectFactory().createCTBorder();
        }
        CTBorder ctBorder = stylesheet.getBorderByIndex(borderId);
        if(ctBorder!=null) {
            return ctBorder.clone();
        }
        return Context.getsmlObjectFactory().createCTBorder();
    }

    private static CTBorderPr applyBorder(XlsxOperationDocument operationDocument, JSONObject borderProperties, CTStylesheet stylesheet, CTBorderPr _borderPr)
        throws FilterException, JSONException {

        CTBorderPr borderPr = _borderPr!=null?_borderPr:Context.getsmlObjectFactory().createCTBorderPr();

        JSONObject jsonColor = borderProperties.optJSONObject(OCKey.COLOR.value());
        if(jsonColor!=null) {
            CTColor color = Context.getsmlObjectFactory().createCTColor();
            Utils.applyColor(operationDocument, color, jsonColor);
            borderPr.setColor(color);
        }

        int width = borderProperties.optInt(OCKey.WIDTH.value(), Utils.THIN_WIDTH_HMM);
        double pixelWidth = Utils.hmmToPixel(width);

        String style = borderProperties.optString(OCKey.STYLE.value(), null);
        if(style!=null) {

            STBorderStyle borderStyle = STBorderStyle.THIN;
            if(style.equals("none")) {
                borderStyle = STBorderStyle.NONE;
            }
            else if(style.equals("single")) {
                if (pixelWidth >= 2.5) {
                    borderStyle = STBorderStyle.THICK;
                } else if (pixelWidth >= 1.5) {
                    borderStyle = STBorderStyle.MEDIUM;
                } else if (pixelWidth >= 0.75) {
                    borderStyle = STBorderStyle.THIN;
                } else {
                    borderStyle = STBorderStyle.HAIR;
                }
            }
            else if(style.equals("double")) {
                borderStyle = STBorderStyle.DOUBLE;
            }
            else if(style.equals("dotted")) {
                if (pixelWidth >= 1.5) {
                    borderStyle = STBorderStyle.MEDIUM_DASH_DOT_DOT;
                } else {
                	borderStyle = STBorderStyle.DOTTED;
                }
            }
            else if(style.equals("dashed")) {
                if (pixelWidth >= 1.5) {
                    borderStyle = STBorderStyle.MEDIUM_DASHED;
                } else {
                    borderStyle = STBorderStyle.DASHED;
                }
            }
            else if(style.equals("dashDot")) {
                if (pixelWidth >= 1.5) {
                    borderStyle = STBorderStyle.MEDIUM_DASH_DOT;
                } else {
                	borderStyle = STBorderStyle.DASH_DOT;
                }
            }
            else if(style.equals("dashDotDot")) {
                if (pixelWidth >= 1.5) {
                    borderStyle = STBorderStyle.MEDIUM_DASH_DOT_DOT;
                } else {
                	borderStyle = STBorderStyle.DASH_DOT_DOT;
                }
            }
            borderPr.setStyle(borderStyle);
        }
        return borderPr;
    }

    private static STPatternType convertDMLPatternToXFPattern(String pattern) {

        if (pattern==null) {
            return null;
        }

        switch (pattern) {
        case "dkDnDiag":    return STPatternType.DARK_DOWN;
        case "dkHorz":      return STPatternType.DARK_HORIZONTAL;
        case "dkUpDiag":    return STPatternType.DARK_UP;
        case "dkVert":      return STPatternType.DARK_VERTICAL;
        case "ltDnDiag":    return STPatternType.LIGHT_DOWN;
        case "ltHorz":      return STPatternType.LIGHT_HORIZONTAL;
        case "ltUpDiag":    return STPatternType.LIGHT_UP;
        case "ltVert":      return STPatternType.LIGHT_VERTICAL;
        case "none":        return STPatternType.NONE;
        case "pct10":       return STPatternType.GRAY_0625;
        case "pct20":       return STPatternType.GRAY_125;
        case "pct25":       return STPatternType.LIGHT_GRAY;
        case "pct30":       return STPatternType.LIGHT_TRELLIS;
        case "pct50":       return STPatternType.MEDIUM_GRAY;
        case "pct70":       return STPatternType.DARK_GRAY;
        case "smCheck":     return STPatternType.DARK_GRID;
        case "smGrid":      return STPatternType.LIGHT_GRID;
        case "solid":       return STPatternType.SOLID;
        case "trellis":     return STPatternType.DARK_TRELLIS;
        }

        return null;
    }

    public static void applyCellProperties(XlsxOperationDocument operationDocument, JSONObject cellProperties, CTXf xf, CTStylesheet stylesheet, boolean dxf)
        throws FilterException, JSONException {

        final ObjectFactory objectFactory = Context.getsmlObjectFactory();

        // detached property sets are only created if needed
        CTBorder detachedBorder = null;

        // create a fill object from all properties occurring in the passed attributes
        String fillType = null;
        JSONObject fillColor = null;
        JSONObject foreColor = null;
        STPatternType patternType = null;
        boolean createFill = false;

        Iterator<String> keys = cellProperties.keys();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = cellProperties.get(attr);

            // CTNumFmt
            if(attr.equals(OCKey.FORMAT_ID.value())) {
            	if(value instanceof Number) {
                    xf.setNumFmtId(((Number)value).longValue());
                } else {
                    xf.setNumFmtId(null);
                }
            }
            // CTFill
            else if(attr.equals(OCKey.FILL_TYPE.value())) {
                if (value instanceof String) {
                    createFill = true;
                    fillType = (String)value;
                }
            }
            else if(attr.equals(OCKey.FILL_COLOR.value())) {
                if (value instanceof JSONObject) {
                    createFill = true;
                    fillColor = (JSONObject)value;
                }
            }
            else if(attr.equals(OCKey.FORE_COLOR.value())) {
                if (value instanceof JSONObject) {
                    createFill = true;
                    foreColor = (JSONObject)value;
                }
            }
            else if (attr.equals(OCKey.PATTERN.value())) {
                if (value instanceof String) {
                    createFill = true;
                    patternType = convertDMLPatternToXFPattern((String)value);
                }
            }
            // CTBorder
            else if(attr.equals(OCKey.BORDER_LEFT.value())) {
                if(detachedBorder==null) {
                    detachedBorder = createDetachedBorder(stylesheet, xf);
                }
                if(value instanceof JSONObject) {
                    detachedBorder.setLeft(applyBorder(operationDocument, (JSONObject)value, stylesheet, detachedBorder.getLeft()));
                } else {
                    detachedBorder.setLeft(null);
                }
            }
            else if(attr.equals(OCKey.BORDER_RIGHT.value())) {
                if(detachedBorder==null) {
                    detachedBorder = createDetachedBorder(stylesheet, xf);
                }
                if(value instanceof JSONObject) {
                    detachedBorder.setRight(applyBorder(operationDocument, (JSONObject)value, stylesheet, detachedBorder.getRight()));
                } else {
                    detachedBorder.setRight(null);
                }
            }
            else if(attr.equals(OCKey.BORDER_TOP.value())) {
                if(detachedBorder==null) {
                    detachedBorder = createDetachedBorder(stylesheet, xf);
                }
                if(value instanceof JSONObject) {
                    detachedBorder.setTop(applyBorder(operationDocument, (JSONObject)value, stylesheet, detachedBorder.getTop()));
                } else {
                    detachedBorder.setTop(null);
                }
            }
            else if(attr.equals(OCKey.BORDER_BOTTOM.value())) {
                if(detachedBorder==null) {
                    detachedBorder = createDetachedBorder(stylesheet, xf);
                }
                if(value instanceof JSONObject) {
                    detachedBorder.setBottom(applyBorder(operationDocument, (JSONObject)value, stylesheet, detachedBorder.getBottom()));
                } else {
                    detachedBorder.setBottom(null);
                }
            }
            // CTCellAlignment
            else if(attr.equals(OCKey.ALIGN_HOR.value())) {
                CTCellAlignment cellAlignment = xf.getAlignment();
                if(cellAlignment==null) {
                    cellAlignment = objectFactory.createCTCellAlignment();
                    xf.setAlignment(cellAlignment);
                }
                if(value instanceof String) {
                    String alignment = (String)value;
                    if (alignment.equals("left")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.LEFT);
                    }
                    else if (alignment.equals("center")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.CENTER);
                    }
                    else if (alignment.equals("right")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.RIGHT);
                    }
                    else if (alignment.equals("justify")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.JUSTIFY);
                    }
                    else if (alignment.equals("distribute")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.DISTRIBUTED);
                    }
                    else if (alignment.equals("fillAcross")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.FILL);
                    }
                    else if (alignment.equals("centerAcross")) {
                        cellAlignment.setHorizontal(STHorizontalAlignment.CENTER_CONTINUOUS);
                    }
                    else {
                        cellAlignment.setHorizontal(STHorizontalAlignment.GENERAL);
                    }
                }
                else {
                    cellAlignment.setHorizontal(STHorizontalAlignment.GENERAL);
                }
            }
            else if(attr.equals(OCKey.ALIGN_VERT.value())) {
                CTCellAlignment cellAlignment = xf.getAlignment();
                if(cellAlignment==null) {
                    cellAlignment = objectFactory.createCTCellAlignment();
                    xf.setAlignment(cellAlignment);
                }
                if(value instanceof String) {
                    String alignment = (String)value;
                    if(alignment.equals("top")) {
                        cellAlignment.setVertical(STVerticalAlignment.TOP);
                    }
                    else if(alignment.equals("middle")) {
                        cellAlignment.setVertical(STVerticalAlignment.CENTER);
                    }
                    else if(alignment.equals("justify")) {
                        cellAlignment.setVertical(STVerticalAlignment.JUSTIFY);
                    }
                    else {
                        cellAlignment.setVertical(STVerticalAlignment.BOTTOM);
                    }
                }
                else {
                    cellAlignment.setVertical(STVerticalAlignment.BOTTOM);
                }
            }
            else if(attr.equals(OCKey.WRAP_TEXT.value())) {
                CTCellAlignment cellAlignment = xf.getAlignment();
                if(cellAlignment==null) {
                    cellAlignment = objectFactory.createCTCellAlignment();
                    xf.setAlignment(cellAlignment);
                }
                if(value instanceof Boolean) {
                    cellAlignment.setWrapText((Boolean)value);
                }
                else {
                    cellAlignment.setWrapText(null);
                }
            }
            //CTCellProtection
            else if(attr.equals(OCKey.UNLOCKED.value())) {
                CTCellProtection cellProtection = xf.getProtection();
                if(cellProtection==null) {
                    cellProtection = objectFactory.createCTCellProtection();
                    xf.setProtection(cellProtection);
                }
                if(value instanceof Boolean) {
                    cellProtection.setLocked(!(Boolean)value);
                }
                else {
                    cellProtection.setLocked(null);
                }
            }
            else if (attr.equals(OCKey.HIDDEN.value())) {
                CTCellProtection cellProtection = xf.getProtection();
                if(cellProtection==null) {
                    cellProtection = objectFactory.createCTCellProtection();
                    xf.setProtection(cellProtection);
                }
                if(value instanceof Boolean) {
                    cellProtection.setHidden((Boolean)value);
                } else {
                    cellProtection.setHidden(null);
                }
            }
        }

        // CTFill
        xf.setFillId(null);
        if (createFill && (fillType!=null)) {

            CTPatternFill patternFill = null;
            CTGradientFill gradientFill = null;

            // in DXF mode, put the existing properties only, but with special behavior for pattern/solid fill type
            if (dxf) {

                switch (fillType) {
                case "solid":
                    patternFill = objectFactory.createCTPatternFill();
                    if ((fillColor!=null) && "auto".equals(fillColor.optString(OCKey.TYPE.value()))) {
                        // "solid" fill type with auto color represents "no fill" (transparent)
                        patternFill.setPatternType(STPatternType.NONE);
                        CTColor color = objectFactory.createCTColor();
                        color.setAuto(true);
                        patternFill.setBgColor(color);
                    } else {
                        patternFill.setPatternType(STPatternType.SOLID);
                        if (fillColor!=null) {
                            CTColor bgColor = objectFactory.createCTColor();
                            Utils.applyColor(operationDocument, bgColor, fillColor);
                            patternFill.setBgColor(bgColor);
                        }
                    }
                    break;
                case "pattern":
                case "inherit":
                    // "inherit" mode allows to change the pattern colors without changing "solid" or "pattern" mode
                    patternFill = objectFactory.createCTPatternFill();
                    patternFill.setPatternType(patternType);
                    if (fillColor!=null) {
                        CTColor bgColor = objectFactory.createCTColor();
                        Utils.applyColor(operationDocument, bgColor, fillColor);
                        patternFill.setBgColor(bgColor);
                    }
                    if (foreColor!=null) {
                        CTColor fgColor = objectFactory.createCTColor();
                        Utils.applyColor(operationDocument, fgColor, foreColor);
                        patternFill.setFgColor(fgColor);
                    }
                    break;
                }

            } else { // normal cell XF mode

                switch (fillType) {
                case "solid":
                    patternFill = objectFactory.createCTPatternFill();
                    if ((fillColor==null) || "auto".equals(fillColor.optString(OCKey.TYPE.value()))) {
                        // "solid" fill type with auto color (also implicit) represents "no fill" (transparent)
                        patternFill.setPatternType(STPatternType.NONE);
                        // <bgColor> element not needed in cell XF mode
                    } else {
                        patternFill.setPatternType(STPatternType.SOLID);
                        // solid fill color must be stored into <fgColor> element due to "solid" pattern
                        CTColor fgColor = objectFactory.createCTColor();
                        Utils.applyColor(operationDocument, fgColor, fillColor);
                        patternFill.setFgColor(fgColor);
                    }
                    break;
                case "pattern":
                    patternFill = objectFactory.createCTPatternFill();
                    patternFill.setPatternType(patternType);
                    if ((fillColor!=null) && !"auto".equals(fillColor.optString(OCKey.TYPE.value()))) {
                        CTColor bgColor = objectFactory.createCTColor();
                        Utils.applyColor(operationDocument, bgColor, fillColor);
                        patternFill.setBgColor(bgColor);
                    }
                    if ((foreColor!=null) && !"auto".equals(foreColor.optString(OCKey.TYPE.value()))) {
                        CTColor fgColor = objectFactory.createCTColor();
                        Utils.applyColor(operationDocument, fgColor, foreColor);
                        patternFill.setFgColor(fgColor);
                    }
                    break;
                }
            }

            CTFill ctFill = createDetachedFill(stylesheet, xf);
            ctFill.setPatternFill(patternFill);
            ctFill.setGradientFill(gradientFill);
            xf.setFillId(stylesheet.getOrApplyFill(ctFill));
        }

        // CTBorder
        if(detachedBorder!=null) {
            xf.setBorderId(stylesheet.getOrApplyBorder(detachedBorder));
        }
    }

    public static JSONObject createCellProperties(CTXf xf, CTStylesheet stylesheet, boolean createStyle)
        throws JSONException {

        JSONObject jsonCellProperties = new JSONObject();
        // CTNumFmt
        Long formatIdObj = getAttributeIndex(xf.getNumFmtId(), xf.isApplyNumberFormat(), createStyle);
        long formatId = formatIdObj==null ? 0 : formatIdObj.longValue();
        if (formatId >= 0) {
        	jsonCellProperties.put(OCKey.FORMAT_ID.value(), formatId);
        }

        // CTFILL
        Utils.createFill(jsonCellProperties, stylesheet, stylesheet.getFillByIndex(getAttributeIndex(xf.getFillId(), xf.isApplyFill(), createStyle)), false);

        // CTBorders
        Utils.createBorders(jsonCellProperties, stylesheet, stylesheet.getBorderByIndex(getAttributeIndex(xf.getBorderId(), xf.isApplyBorder(), createStyle)));

        // CTCellAlignment
        if(createStyle) {
	        if(xf.isApplyAlignment()!=null&&xf.isApplyAlignment().booleanValue()) {
	            Utils.createAlignment(jsonCellProperties, xf.getAlignment());
	        }
	        // CTCellProtection
	        if(xf.isApplyProtection()!=null&&xf.isApplyProtection().booleanValue()) {
	            Utils.createProtection(jsonCellProperties, xf.getProtection());
	        }
        }
        else {
        	Utils.createAlignment(jsonCellProperties, xf.getAlignment());
        	Utils.createProtection(jsonCellProperties, xf.getProtection());
        }
        return jsonCellProperties.length()>0?jsonCellProperties:null;
    }

    public static void applyCharacterProperties(XlsxOperationDocument operationDocument, JSONObject characterProperties, CTXf xf, CTStylesheet stylesheet)
        throws FilterException, JSONException {

        final ObjectFactory objectFactory = Context.getsmlObjectFactory();

        CTFont font = stylesheet.getFontByIndex(xf.getFontId());
        if(font==null) {
            log.debug("xlsx export: could not get font for Id");
            font = objectFactory.createCTFont();
        }
        final CTFont detachedFont = font.clone();

        Iterator<String> keys = characterProperties.keys();
        while(keys.hasNext()) {
            String attr = keys.next();
            Object value = characterProperties.get(attr);
            if(attr.equals(OCKey.FONT_NAME.value())) {
                if(value instanceof String) {

                    String name = (String)value;
                	STFontScheme scheme = null;

                    if (ThemeFonts.MAJOR_ID.equals(name)) {
                        name = operationDocument.getThemeFonts().getMajorFont();
                    	scheme = STFontScheme.MAJOR;
                    } else if (ThemeFonts.MINOR_ID.equals(name)) {
                        name = operationDocument.getThemeFonts().getMinorFont();
                    	scheme = STFontScheme.MINOR;
                    }

                    final CTFontName nameObj = objectFactory.createCTFontName();
                    nameObj.setVal(name);
                	detachedFont.setName(nameObj);

                	if (scheme != null) {
                    	final CTFontScheme schemeObj = objectFactory.createCTFontScheme();
                    	schemeObj.setVal(scheme);
                        detachedFont.setScheme(schemeObj);
                    } else {
                        // the scheme property has higher priority, so we have to remove it, otherwise
                        // the font name would not be used
                        detachedFont.setScheme(null);
                    }
                }
                else {
                	detachedFont.setName(null);
                    detachedFont.setScheme(null);
                }
            }
            else if (attr.equals(OCKey.FONT_SIZE.value())) {
                if(value instanceof Number) {
                    CTFontSize fontSize = detachedFont.getSz();
                    if(fontSize==null) {
                    	fontSize = objectFactory.createCTFontSize();
                    	detachedFont.setSz(fontSize);
                    }
                    fontSize.setVal(((Number)value).doubleValue());
                }
                else {
                	detachedFont.setSz(null);
                }
            }
            else if(attr.equals(OCKey.BOLD.value())) {
                if(value instanceof Boolean) {
                    CTBooleanProperty b = detachedFont.getB();
                    if(b==null) {
                    	b = objectFactory.createCTBooleanProperty();
                    	detachedFont.setB(b);
                    }
                    b.setVal((Boolean)value);
                }
                else {
                	detachedFont.setB(null);
                }
            }
            else if(attr.equals(OCKey.ITALIC.value())) {
                if(value instanceof Boolean) {
                    CTBooleanProperty i = detachedFont.getI();
                    if(i==null) {
                    	i = objectFactory.createCTBooleanProperty();
                    	detachedFont.setI(i);
                    }
                    i.setVal((Boolean)value);
                }
                else {
                	detachedFont.setI(null);
                }
            }
            else if(attr.equals(OCKey.STRIKE.value())) {
                if(value instanceof String) {
                    CTBooleanProperty strike = detachedFont.getStrike();
                    if(strike==null) {
                    	strike = objectFactory.createCTBooleanProperty();
                    	detachedFont.setStrike(strike);
                    }
                    strike.setVal(((String)value).equals("none")==false);
                }
                else {
                	detachedFont.setStrike(null);
                }

            }
            else if(attr.equals(OCKey.COLOR.value())) {
                if(value instanceof JSONObject) {
                    CTColor color = detachedFont.getColor();
                    if(color==null) {
                    	color = objectFactory.createCTColor();
                    	detachedFont.setColor(color);
                    }
                    Utils.applyColor(operationDocument, color, (JSONObject)value);
                }
                else {
                	detachedFont.setColor(null);
                }
            }
            else if(attr.equals(OCKey.VERT_ALIGN.value())) {
                if(value instanceof String) {
                    CTVerticalAlignFontProperty vertAlign = detachedFont.getVertAlign();
                    if(vertAlign==null) {
                    	vertAlign = objectFactory.createCTVerticalAlignFontProperty();
                    	detachedFont.setVertAlign(vertAlign);
                    }
                    STVerticalAlignRun align = STVerticalAlignRun.BASELINE;
                    if(((String)value).equals("sup")) {
                        align = STVerticalAlignRun.SUPERSCRIPT;
                    }
                    else if(((String)value).equals("sub")) {
                        align = STVerticalAlignRun.SUBSCRIPT;
                    }
                    vertAlign.setVal(align);
                }
                else {
                	detachedFont.setVertAlign(null);
                }
            }
            else if(attr.equals(OCKey.UNDERLINE.value())) {
                if(value instanceof Boolean) {
                    CTUnderlineProperty u = detachedFont.getU();
                    if(u==null) {
                    	u = objectFactory.createCTUnderlineProperty();
                    	detachedFont.setU(u);
                    }
                    u.setVal(((Boolean)value).booleanValue() ? STUnderlineValues.SINGLE : STUnderlineValues.NONE);
                }
                else {
                	detachedFont.setU(null);
                }
            }
        }
        long fontIndex = stylesheet.getOrApplyFont(detachedFont);
        xf.setFontId(fontIndex);
    }

    public static JSONObject createCharacterProperties(JSONObject jsonCharacterProperties, CTStylesheet stylesheet, CTFont font)
    	throws JSONException {

    	if(font!=null) {
        	final CTFontName fontName = font.getName();
            if(fontName!=null) {
                Commons.jsonPut(jsonCharacterProperties, OCKey.FONT_NAME.value(), fontName.getVal());
            }
            final CTFontScheme fontScheme = font.getScheme();
            if(fontScheme!=null) {
            	final STFontScheme scheme = fontScheme.getVal();
            	if (STFontScheme.MAJOR.equals(scheme)) {
                    Commons.jsonPut(jsonCharacterProperties, OCKey.FONT_NAME.value(), ThemeFonts.MAJOR_ID);
            	} else if(STFontScheme.MINOR.equals(scheme)) {
                    Commons.jsonPut(jsonCharacterProperties, OCKey.FONT_NAME.value(), ThemeFonts.MINOR_ID);
            	}
            }
            final CTFontSize fontSize = font.getSz();
            if(fontSize!=null) {
                Commons.jsonPut(jsonCharacterProperties, OCKey.FONT_SIZE.value(), fontSize.getVal());
            }
            final CTBooleanProperty b = font.getB();
            if(b!=null) {
            	Commons.jsonPut(jsonCharacterProperties, OCKey.BOLD.value(), b.isVal());
            }
            final CTBooleanProperty i = font.getI();
            if(i!=null) {
            	Commons.jsonPut(jsonCharacterProperties, OCKey.ITALIC.value(), i.isVal());
            }
            final CTBooleanProperty strike = font.getStrike();
            if(strike!=null) {
            	Commons.jsonPut(jsonCharacterProperties, OCKey.STRIKE.value(), strike.isVal() ? "single" : "none");
            }
            final CTColor color = font.getColor();
            if(color!=null) {
            	Commons.jsonPut(jsonCharacterProperties, OCKey.COLOR.value(), Utils.createColor(stylesheet, color));
            }
            final CTVerticalAlignFontProperty vertAlign = font.getVertAlign();
            String align = "baseline";
            if(vertAlign!=null) {
                switch(vertAlign.getVal()) {
                    case BASELINE :    break;
                    case SUPERSCRIPT : align = "sup"; break;
                    case SUBSCRIPT :   align = "sub"; break;
                }
                Commons.jsonPut(jsonCharacterProperties, OCKey.VERT_ALIGN.value(), align);
            }
            final CTUnderlineProperty u = font.getU();
            if(u!=null) {
                final STUnderlineValues underline = u.getVal();
                if(underline!=null) {
                    Commons.jsonPut(jsonCharacterProperties, OCKey.UNDERLINE.value(), underline!=STUnderlineValues.NONE);
                }
            }
        }
        return jsonCharacterProperties.length()>0?jsonCharacterProperties:null;
    }

    private static CTXf createDetachedDefaultXf() {
        final CTXf detachedDefaultXf = Context.getsmlObjectFactory().createCTXf();
        detachedDefaultXf.setXfId(Long.valueOf(0));
        detachedDefaultXf.setFillId(Long.valueOf(0));
        detachedDefaultXf.setFontId(Long.valueOf(0));
        detachedDefaultXf.setNumFmtId(Long.valueOf(0));
        detachedDefaultXf.setBorderId(Long.valueOf(0));
        return detachedDefaultXf;
    }
    public static CTXf getDetachedXfFromStyle(CTStylesheet stylesheet, Long styleIndex) {

        CTXf detachedXf = null;

        // we assume that the first style is an empty default style and is containing only
        // zero indexes, for this case we don't clone and are using the objectFactory
        if(styleIndex.longValue()>0) {
            final CTXf cellStyleXfSource = stylesheet.getCellStyleXfsByIndex(styleIndex);
            if(cellStyleXfSource!=null) {
                detachedXf = cellStyleXfSource.clone(false);
                detachedXf.setXfId(styleIndex);
            }
        }
        if(detachedXf==null) {
            detachedXf = createDetachedDefaultXf();
        }
        return detachedXf;
    }

    public static CTXf getDetachedXfFromAutoStyle(CTStylesheet stylesheet, long cellXfIndex, Long newStyleIndex) {

        CTXf detachedXf;

        final CTXf xfSource = stylesheet.getCellXfByIndex(cellXfIndex);         // current auto-attributes
        if(newStyleIndex==null) {
            detachedXf = xfSource==null?createDetachedDefaultXf():xfSource.clone(true);
        }
        else {
            final CTXf cellStyleXfSource = stylesheet.getCellStyleXfsByIndex(newStyleIndex);
            if(cellStyleXfSource==null) {
                detachedXf = createDetachedDefaultXf();
            }
            else {
                detachedXf = cellStyleXfSource.clone(true);
                detachedXf.setXfId(newStyleIndex);
            }
            if(detachedXf.isApplyBorder()!=null&&!detachedXf.isApplyBorder().booleanValue()) {
                detachedXf.setApplyBorder(true);
                detachedXf.setBorderId(xfSource.getBorderId());
            }
            if(detachedXf.isApplyAlignment()!=null&&!detachedXf.isApplyAlignment().booleanValue()) {
                detachedXf.setApplyAlignment(true);
                final CTCellAlignment cellAlignmentSource = xfSource.getAlignment();
                detachedXf.setAlignment(cellAlignmentSource!=null?cellAlignmentSource.clone():null);
            }
            if(detachedXf.isApplyFill()!=null&&!detachedXf.isApplyFill().booleanValue()) {
                detachedXf.setApplyFill(true);
                detachedXf.setFillId(xfSource.getFillId());
            }
            if(detachedXf.isApplyFont()!=null&&!detachedXf.isApplyFont().booleanValue()) {
                detachedXf.setApplyFont(true);
                detachedXf.setFontId(xfSource.getFontId());
            }
            if(detachedXf.isApplyNumberFormat()!=null&&!detachedXf.isApplyNumberFormat().booleanValue()) {
                detachedXf.setApplyNumberFormat(true);
                detachedXf.setNumFmtId(xfSource.getNumFmtId());
            }
            if(detachedXf.isApplyProtection()!=null&&!detachedXf.isApplyProtection().booleanValue()) {
                detachedXf.setApplyProtection(true);
                final CTCellProtection cellProtectionSource = xfSource.getProtection();
                detachedXf.setProtection(cellProtectionSource!=null?cellProtectionSource.clone():null);
            }
            detachedXf.setPivotButton(xfSource.isPivotButton());
            detachedXf.setQuotePrefix(xfSource.isQuotePrefix());
        }
        return detachedXf;
    }
}
