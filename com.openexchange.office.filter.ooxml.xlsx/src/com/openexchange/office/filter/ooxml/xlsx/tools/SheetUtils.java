/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.Iterator;
import java.util.List;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.BookViews;
import org.xlsx4j.sml.CTBookView;
import org.xlsx4j.sml.CTColor;
import org.xlsx4j.sml.CTOutlinePr;
import org.xlsx4j.sml.CTPane;
import org.xlsx4j.sml.CTSelection;
import org.xlsx4j.sml.CTSheetFormatPr;
import org.xlsx4j.sml.CTSheetPr;
import org.xlsx4j.sml.CTSheetProtection;
import org.xlsx4j.sml.ISheetAccess;
import org.xlsx4j.sml.STPane;
import org.xlsx4j.sml.STPaneState;
import org.xlsx4j.sml.STSheetState;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.SheetView;
import org.xlsx4j.sml.SheetViews;
import org.xlsx4j.sml.Workbook;
import org.xlsx4j.sml.Worksheet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class SheetUtils {

    public static Sheet getSheet(SpreadsheetMLPackage spreadsheetMLPackage, int index) {
        WorkbookPart workbookPart = spreadsheetMLPackage.getWorkbookPart();
        Workbook workbook = workbookPart.getJaxbElement();
        return workbook.getSheets().getContent().get(index);
    }

    private static Double getSplitSize(final CTPane pane, boolean width, final Object srcSize) {
        if(srcSize instanceof Number) {
            double size = ((Number)srcSize).doubleValue();
            if (pane.getState() == STPaneState.SPLIT) {
                size *= 1440.0 / 2540.0; // 100thmm to TWIP
                if(!width) {
                    size += 285.0;
                }
            }
            return Double.valueOf((int)size);
        }
        return null;
    }

    public static void applySheetProperties(Workbook workbook, int sheetIndex, ISheetAccess sheetAccess, JSONObject sheetProperties)
        throws JSONException {

        if(sheetProperties==null)
            return;

        final List<Sheet> sheetList = workbook.getSheets().getContent();
        final Sheet sheet = sheetList.get(sheetIndex);
        final Object ov = sheetProperties.opt(OCKey.VISIBLE.value());
        if(ov != null) {
            if(ov instanceof Boolean) {
                sheet.setState(((Boolean)ov).booleanValue() ? STSheetState.VISIBLE : STSheetState.HIDDEN);
            }
            else {
                sheet.setState(STSheetState.VISIBLE);
            }
        }

        if(sheetAccess instanceof Worksheet) {
            final Worksheet worksheet = (Worksheet)sheetAccess;
            final Object locked = sheetProperties.opt(OCKey.LOCKED.value());
            if(locked!=null) {
                CTSheetProtection sheetProtection = worksheet.getSheetProtection();
                if(locked instanceof Boolean) {
                    if(sheetProtection==null) {
                        sheetProtection = Context.getsmlObjectFactory().createCTSheetProtection();
                        worksheet.setSheetProtection(sheetProtection);
                    }
                    sheetProtection.setSheet((Boolean)locked);
                    sheetProtection.setObjects((Boolean)locked);
                    sheetProtection.setScenarios((Boolean)locked);
                }
                else if(locked==JSONObject.NULL&&sheetProtection!=null) {
                    worksheet.setSheetProtection(null);
                }
            }
            final SheetView sheetView = getOrCreateSheetView(worksheet);
            final Object zoom = sheetProperties.opt(OCKey.ZOOM.value());
            if(zoom!=null) {
                sheetView.setZoomScale(zoom instanceof Number
                        ? (long)((((Number)zoom).doubleValue()*100.0)+0.5)
                        : null);
            }
            final Object showGrid = sheetProperties.opt(OCKey.SHOW_GRID.value());
            if(showGrid!=null) {
                sheetView.setShowGridLines(showGrid instanceof Boolean
                        ? (Boolean)showGrid
                        : null);
            }
            final Object scrollLeft = sheetProperties.opt(OCKey.SCROLL_LEFT.value());
            final Object scrollTop  = sheetProperties.opt(OCKey.SCROLL_TOP.value());
            if(scrollLeft!=null||scrollTop!=null) {
                CellRef topLeft = CellRef.createCellRef(sheetView.getTopLeftCell());
                if(topLeft==null) {
                    topLeft = new CellRef(0, 0);
                }
                if(scrollLeft!=null) {
                    topLeft.setColumn(scrollLeft instanceof Number
                        ? ((Number)scrollLeft).intValue()
                        : 0);
                }
                if(scrollTop!=null) {
                    topLeft.setRow(scrollTop instanceof Number
                        ? ((Number)scrollTop).intValue()
                        : 0);
                }
                if(topLeft.getColumn()==0&&topLeft.getRow()==0) {
                    sheetView.setTopLeftCell(null);
                }
                else {
                    sheetView.setTopLeftCell(CellRef.getCellRef(topLeft));
                }
            }

            CTPane pane = sheetView.getPane();
            final Object splitMode = sheetProperties.opt(OCKey.SPLIT_MODE.value());
            final Object splitWidth = sheetProperties.opt(OCKey.SPLIT_WIDTH.value());
            final Object splitHeight = sheetProperties.opt(OCKey.SPLIT_HEIGHT.value());
            if(splitMode!=null||splitWidth!=null||splitHeight!=null) {
                if(pane==null) {
                    pane = getOrCreatePane(worksheet);
                }
                if(splitMode!=null) {
                    if(splitMode instanceof String) {
                        if(((String)splitMode).equals("split")) {
                            pane.setState(STPaneState.SPLIT);
                        } else if(((String)splitMode).equals("frozen")) {
                            pane.setState(STPaneState.FROZEN);
                        } else if(((String)splitMode).equals("frozenSplit")) {
                            pane.setState(STPaneState.FROZEN_SPLIT);
                        }
                    }
                    else {
                        pane.setState(STPaneState.SPLIT);
                    }
                }
                if(splitWidth!=null) {
                    pane.setXSplit(getSplitSize(pane, true, splitWidth));
                }
                if(splitHeight!=null) {
                    pane.setYSplit(getSplitSize(pane, false, splitHeight));
                }
            }

            if(pane==null||pane.getXSplit()==0.0) {
                // remove selection topRight & bottomRight
                removeSelection(sheetView, STPane.TOP_RIGHT);
                removeSelection(sheetView, STPane.BOTTOM_RIGHT);
            }
            if(pane==null||pane.getYSplit()==0.0) {
                // remove selection bottomLeft & bottomRight
                removeSelection(sheetView, STPane.BOTTOM_LEFT);
                removeSelection(sheetView, STPane.BOTTOM_RIGHT);
            }

            boolean hasPane = pane!=null&&(pane.getXSplit()!=0.0D||pane.getYSplit()!=0.0D);
            if(!hasPane) {
                sheetView.setPane(null);
            } else if (pane!=null) {
                final Object aPane = sheetProperties.opt(OCKey.ACTIVE_PANE.value());
                if(aPane!=null) {
                    STPane activePane = STPane.BOTTOM_RIGHT;
                    if(aPane instanceof String) {
                        if(((String)aPane).equals("topLeft")) {
                            activePane = STPane.TOP_LEFT;
                        } else if(((String)aPane).equals("topRight")) {
                            activePane = STPane.TOP_RIGHT;
                        } else if(((String)aPane).equals("bottomLeft")) {
                            activePane = STPane.BOTTOM_LEFT;
                        }
                    }
                    pane.setActivePane(activePane);
                }
                final Object scrollRight = sheetProperties.opt(OCKey.SCROLL_RIGHT.value());
                final Object scrollBottom  = sheetProperties.opt(OCKey.SCROLL_BOTTOM.value());
                if(scrollRight!=null||scrollBottom!=null) {
                    CellRef topLeft = CellRef.createCellRef(pane.getTopLeftCell());
                    if(topLeft==null) {
                        topLeft = new CellRef(0, 0);
                    }
                    if(scrollRight!=null) {
                        topLeft.setColumn(scrollRight instanceof Number
                            ? ((Number)scrollRight).intValue()
                            : 0);
                    }
                    if(scrollBottom!=null) {
                        topLeft.setRow(scrollBottom instanceof Number
                            ? ((Number)scrollBottom).intValue()
                            : 0);
                    }
                    if(topLeft.getColumn()==0&&topLeft.getRow()==0) {
                        pane.setTopLeftCell(null);
                    }
                    else {
                        pane.setTopLeftCell(CellRef.getCellRef(topLeft));
                    }
                }
            }

            // excel needs an activeCell and also a sqRef... the active cell must be placed within the sqref range...
            // otherwise excel will report an error in the viewSettings
            final Object selectedRanges = sheetProperties.opt(OCKey.SELECTED_RANGES.value());
            final Object activeIndex = sheetProperties.opt(OCKey.ACTIVE_INDEX.value());
            final Object activeCell = sheetProperties.opt(OCKey.ACTIVE_CELL.value());
            if(selectedRanges!=null||activeIndex!=null||activeCell!=null) {
                final CTSelection selection=getSelection(sheetView, true);
                if(activeCell!=null) {
                    if(activeCell instanceof String) {
                        final CellRef cellRef = CellRef.createCellRef((String)activeCell);
                        selection.setActiveCell(CellRef.getCellRef(cellRef));
                    }
                    else {
                        selection.setActiveCell(null);
                    }
                }
                if(selectedRanges!=null) {
                    if(selectedRanges instanceof String) {
                        final String[] ranges = ((String) selectedRanges).split(" ", -1);
                        final List<String> sqrefs = selection.getSqref();
                        sqrefs.clear();
                        for(int i=0;i<ranges.length;i++) {
                            sqrefs.add(ranges[i]);
                        }
                    }
                    else {
                        selection.getSqref().clear();
                    }
                }
                if(activeIndex!=null) {
                    if(activeIndex instanceof Number) {
                        selection.setActiveCellId(((Number)activeIndex).longValue());
                    }
                    else {
                        selection.setActiveCellId(null);
                    }
                }
                // now check if our selection is valid
                if(selection.getActiveCell()==null) {
                    selection.setActiveCell("A1");
                }
                final CellRef activeCellRef = CellRef.createCellRef(selection.getActiveCell());
                final List<String> sel = selection.getSqref();
                if(sel.isEmpty()) {
                    sel.add(CellRef.getCellRef(activeCellRef));
                    selection.setActiveCellId(null);
                }
                else {
                    final long cellId = selection.getActiveCellId();
                    boolean activeCellIsPartOfSelection = false;
                    // now check if the active cell is part of the sqref
                    if(cellId>=0&&cellId<sel.size()) {
                        final CellRefRange cellRefRange = CellRefRange.createCellRefRange(sel.get((int)cellId));
                        activeCellIsPartOfSelection = cellRefRange.isInside(activeCellRef.getColumn(), activeCellRef.getRow());
                    }
                    if(activeCellIsPartOfSelection==false) {
                        selection.setActiveCellId(null);
                        sel.clear();
                        sel.add(CellRef.getCellRef(activeCellRef));
                    }
                }
            }
        }
    }

    private static CTSelection getSelection(SheetView sheetView, boolean forceCreate) {

        final STPane activePane = sheetView.getPane()!=null?sheetView.getPane().getActivePane():STPane.TOP_LEFT;
        final List<CTSelection> selectionList = sheetView.getSelection();
        for(CTSelection selection:selectionList) {
            if(selection.getPane()==activePane) {
                return selection;
            }
        }
        if(forceCreate) {
            CTSelection selection = Context.getsmlObjectFactory().createCTSelection();
            selection.setPane(activePane);
            selectionList.add(selection);
            return selection;
        }
        return null;
    }

    private static void removeSelection(SheetView sheetView, STPane paneToBeRemoved) {
        final Iterator<CTSelection> selectionIter = sheetView.getSelection().iterator();
        while(selectionIter.hasNext()) {
            final CTSelection selection = selectionIter.next();
            if(selection.getPane().equals(paneToBeRemoved)) {
                selectionIter.remove();
                return;
            }
        }
    }

    private static SheetView getOrCreateSheetView(Worksheet worksheet) {

        SheetViews sheetViews = worksheet.getSheetViews();
        if(sheetViews==null) {
            sheetViews = Context.getsmlObjectFactory().createSheetViews();
            worksheet.setSheetViews(sheetViews);
        }
        final List<SheetView> sheetViewList = sheetViews.getSheetView();
        if(sheetViewList.isEmpty()) {
            sheetViewList.add(Context.getsmlObjectFactory().createSheetView());
        }
        return sheetViewList.get(0);
    }

    private static CTPane getOrCreatePane(Worksheet worksheet) {

        final SheetView sheetView = getOrCreateSheetView(worksheet);
        CTPane pane = sheetView.getPane();
        if(pane==null) {
            pane = Context.getsmlObjectFactory().createCTPane();
            sheetView.setPane(pane);
        }
        return pane;
    }

    // check if the first visible sheet matches the bookView and removes each tabSelected from sheet
    public static void updateBookView(Workbook workbook) {

        final List<Sheet> sheetList = workbook.getSheets().getContent();
        Long firstVisible = null;
        for(int i=0; i<sheetList.size(); i++) {
            final Sheet sh = sheetList.get(i);
            if(firstVisible==null&&sh.getState()==STSheetState.VISIBLE) {
                firstVisible = Long.valueOf(i);
            }
        }
        if(firstVisible==null) {
            firstVisible = Long.valueOf(0);
        }
        BookViews bookViews = workbook.getBookViews();
        if(bookViews==null) {
            bookViews = Context.getsmlObjectFactory().createBookViews();
            workbook.setBookViews(bookViews);
        }
        final List<CTBookView> workbookViewList = bookViews.getWorkbookView();
        if(workbookViewList.isEmpty()) {
            final CTBookView bookView = Context.getsmlObjectFactory().createCTBookView();
            workbookViewList.add(bookView);
        }
        for(CTBookView bookView:workbookViewList) {
            final int activeTab = (int)bookView.getActiveTab();
            final int firstView = (int)bookView.getFirstSheet();
            if(activeTab<0||activeTab>=sheetList.size()||sheetList.get(activeTab).getState()!=STSheetState.VISIBLE) {
                bookView.setActiveTab(firstVisible);
            }
            if(firstView<0||firstView>=sheetList.size()||sheetList.get(firstView).getState()!=STSheetState.VISIBLE) {
                bookView.setFirstSheet(firstVisible);
            }
        }
    }

    public static JSONObject createSheetProperties(Sheet sheet)
        throws JSONException {

        if(sheet.getState()==STSheetState.VISIBLE) {
            return null;
        }
        JSONObject jsonSheetProperties = new JSONObject();
        jsonSheetProperties.put(OCKey.VISIBLE.value(), false);
        return jsonSheetProperties;
    }

    public static JSONObject createWorksheetProperties(Sheet sheet, Worksheet worksheet)
        throws JSONException {

        JSONObject attrs = createSheetProperties(sheet);
        if(attrs==null) {
            attrs = new JSONObject();
        }

        final CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        final double baseColWidth = (sheetFormatPr == null) ? 8.0 : sheetFormatPr.getBaseColWidth();
        if (baseColWidth!=8.0) {
            attrs.put(OCKey.BASE_COL_WIDTH.value(), baseColWidth);
        }

        final CTSheetPr sheetPr = worksheet.getSheetPr();
        final CTOutlinePr outlinePr = (sheetPr!=null) ? sheetPr.getOutlinePr() : null;
        // position of total column of column groups (OOXML: true=right-of; but in operations: true=left-of)
        final boolean summaryRight = (outlinePr!=null) ? outlinePr.isSummaryRight() : true; // default is true
        if (!summaryRight) { attrs.put(OCKey.COL_TOTAL_BEFORE.value(), true); }
        // position of total row of row groups (OOXML: true=below; but in operations: true=above)
        final boolean summaryBelow = (outlinePr!=null) ? outlinePr.isSummaryBelow() : true; // default is true
        if (!summaryBelow) { attrs.put(OCKey.ROW_TOTAL_BEFORE.value(), true); }

        final CTSheetProtection sheetProtection = worksheet.getSheetProtection();
        if ((sheetProtection!=null) && sheetProtection.isSheet()) {
            attrs.put(OCKey.LOCKED.value(), true);
        }

        final SheetViews sheetViews = worksheet.getSheetViews();
        if(sheetViews!=null) {
            final List<SheetView> sheetViewList = sheetViews.getSheetView();
            if(!sheetViewList.isEmpty()) {
                final SheetView sheetView = sheetViewList.get(0);
                final CTPane pane = sheetView.getPane();
                final double zoomScale = sheetView.getZoomScale() / 100.0;
                attrs.put(OCKey.ZOOM.value(), zoomScale);
                if(sheetView.isShowGridLines()==false) {
                    attrs.put(OCKey.SHOW_GRID.value(), false);
                }
                if(sheetView.isDefaultGridColor()==false) {
                    final long colorIndex = sheetView.getColorId();
                    if(colorIndex!=64) {
                        CTColor color = new CTColor();
                        color.setIndexed(new Long(colorIndex));
                        Commons.jsonPut(attrs, OCKey.GRID_COLOR.value(), Utils.createColor(null, color));
                    }
                }
                final CellRef topLeft = CellRef.createCellRef(sheetView.getTopLeftCell());
                if(topLeft!=null) {
                    final int column = topLeft.getColumn();
                    final int row = topLeft.getRow();
                    if(column>0) {
                        attrs.put(OCKey.SCROLL_LEFT.value(), topLeft.getColumn());
                    }
                    if(row>0) {
                        attrs.put(OCKey.SCROLL_TOP.value(), topLeft.getRow());
                    }
                }

                String p;
                final STPane activePane = pane!=null?pane.getActivePane():STPane.TOP_LEFT;
                switch(activePane) {
                    case BOTTOM_LEFT :  p = "bottomLeft"; break;
                    case BOTTOM_RIGHT : p = "bottomRight"; break;
                    case TOP_RIGHT :    p = "topRight"; break;
                    default: p = "topLeft";
                }
                attrs.put(OCKey.ACTIVE_PANE.value(), p);

                if(pane!=null) {
                    final CellRef topLeftP = CellRef.createCellRef(pane.getTopLeftCell());
                    if(topLeftP!=null) {
                        if(topLeftP.getColumn()>0) {
                            attrs.put(OCKey.SCROLL_RIGHT.value(), topLeftP.getColumn());
                        }
                        if(topLeftP.getRow()>0) {
                            attrs.put(OCKey.SCROLL_BOTTOM.value(), topLeftP.getRow());
                        }
                    }
                    double xSplit = pane.getXSplit();
                    double ySplit = pane.getYSplit();
                    switch(pane.getState()) {
                    case FROZEN:
                        attrs.put(OCKey.SPLIT_MODE.value(), "frozen");
                        if(xSplit!=0.0) {
                            attrs.put(OCKey.SPLIT_WIDTH.value(), (int)xSplit);
                        }
                        if(ySplit!=0.0) {
                            attrs.put(OCKey.SPLIT_HEIGHT.value(), (int)ySplit);
                        }
                        break;
                    case FROZEN_SPLIT:
                        attrs.put(OCKey.SPLIT_MODE.value(),  "frozenSplit");
                        if(xSplit!=0.0) {
                            attrs.put(OCKey.SPLIT_WIDTH.value(), (int)xSplit);
                        }
                        if(ySplit!=0.0) {
                            attrs.put(OCKey.SPLIT_HEIGHT.value(), (int)ySplit);
                        }
                        break;
                    case SPLIT:
                        if(xSplit!=0.0) {
                            attrs.put(OCKey.SPLIT_WIDTH.value(), (int)((xSplit * 2540.0) / 1440 + 0.5));     // TWIP to 100thmm
                        }
                        if(ySplit>=285.0) {
                            attrs.put(OCKey.SPLIT_HEIGHT.value(), (int)(((ySplit - 285.0) * 2540.0) / 1440 + 0.5));
                        }
                        break;
                    default:
                        break;
                    }
                }
                final List<CTSelection> selections = sheetView.getSelection();
                if(!selections.isEmpty()) {
                    for(CTSelection selection:selections) {
                        if(selection.getPane()==activePane) {
                            final StringBuffer ranges = new StringBuffer();
                            for(String sqref:selection.getSqref()) {
                                if(ranges.length()!=0) {
                                    ranges.append(" ");
                                }
                                ranges.append(sqref);
                            }
                            if(ranges.length()>0) {
                                attrs.put(OCKey.SELECTED_RANGES.value(), ranges);
                                attrs.put(OCKey.ACTIVE_INDEX.value(), selection.getActiveCellId());
                                if(selection.getActiveCell()!=null) {
                                    final CellRef activeCell = CellRef.createCellRef(selection.getActiveCell());
                                    if(activeCell!=null) {
                                        attrs.put(OCKey.ACTIVE_CELL.value(), CellRef.getCellRef(activeCell));
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        return attrs;
    }

    public static CTSheetFormatPr getOrCreateSheetFormatPr(Worksheet worksheet) {
        CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        if(sheetFormatPr==null) {
            sheetFormatPr = Context.getsmlObjectFactory().createCTSheetFormatPr();
            worksheet.setSheetFormatPr(sheetFormatPr);
        }
        return sheetFormatPr;
    }

    public static void applyWorksheetColumnProperties(XlsxOperationDocument operationDocument, Worksheet worksheet, JSONObject columnProperties)
            throws JSONException {

        if(columnProperties==null)
            return;
        final CTSheetFormatPr sheetFormatPr = getOrCreateSheetFormatPr(worksheet);
        final Object width = columnProperties.opt(OCKey.WIDTH.value());
        if (width!=null) {
            sheetFormatPr.setDefaultColWidth((width instanceof Number) ? ((Number)width).doubleValue() : null);
        }
/*
        final Object customWidth = columnProperties.opt("customWidth");
        if(customWidth!=null) {
        }
*/
        final Object visible = columnProperties.opt(OCKey.VISIBLE.value());
        if(visible!=null) {
            final Double defaultColWidth = sheetFormatPr.getDefaultColWidth();
            if(visible instanceof Boolean) {
                if(!(Boolean)visible) {
                    // changing to invisible
                    sheetFormatPr.setDefaultColWidth(new Double(0.0d));
                    if(defaultColWidth!=null) {
                        sheetFormatPr.setBaseColWidth(defaultColWidth.longValue());
                    }
                }
                else {  // changing to visible -> defaultColWidth of "0" is not allowed
                    if(defaultColWidth!=null&&defaultColWidth==0) {
                        sheetFormatPr.setDefaultColWidth(null);
                    }
                }
            }
            else {  // changing to visible -> defaultColWidth of "0" is not allowed
                if(defaultColWidth!=null&&defaultColWidth==0) {
                    sheetFormatPr.setDefaultColWidth(null);
                }
            }
        }
    }

    public static void applyWorksheetRowProperties(XlsxOperationDocument operationDocument, Worksheet worksheet, JSONObject rowProperties)
            throws JSONException {

        if(rowProperties==null)
            return;

        final CTSheetFormatPr sheetFormatPr = getOrCreateSheetFormatPr(worksheet);
        final Object height = rowProperties.opt(OCKey.HEIGHT.value());
        if(height!=null) {
            if(height instanceof Number) {
                sheetFormatPr.setDefaultRowHeight(((Number)height).doubleValue());
            }
            else {
                sheetFormatPr.setDefaultRowHeight(15);
            }
        }
        final Object customHeight = rowProperties.opt(OCKey.CUSTOM_HEIGHT.value());
        if(customHeight!=null) {
            if(customHeight instanceof Boolean) {
                sheetFormatPr.setCustomHeight((Boolean)customHeight);
            }
            else {
                sheetFormatPr.setCustomHeight(null);
            }
        }
        final Object visible = rowProperties.opt(OCKey.VISIBLE.value());
        if(visible!=null) {
            if(visible instanceof Boolean) {
                sheetFormatPr.setZeroHeight(!(Boolean)visible);
            }
            else {
                sheetFormatPr.setZeroHeight(null);
            }
        }
    }

    public static JSONObject createWorksheetColumnProperties(XlsxOperationDocument operationDocument, Worksheet worksheet)
        throws JSONException {

        final JSONObject columnProperties = new JSONObject();
        final CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        if(sheetFormatPr!=null) {
            final Double defaultColWidth = sheetFormatPr.getDefaultColWidth();
            if(defaultColWidth!=null) {
                columnProperties.put(OCKey.WIDTH.value(), defaultColWidth);
                columnProperties.put(OCKey.CUSTOM_WIDTH.value(), true);
                if(defaultColWidth.doubleValue()==0) {
                    columnProperties.put(OCKey.VISIBLE.value(), true);
                }
            }
        }
        return columnProperties;
    }

    public static JSONObject createWorksheetRowProperties(Worksheet worksheet)
        throws JSONException {

        final JSONObject rowProperties = new JSONObject();
        final CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        if(sheetFormatPr!=null) {
            final boolean customHeight = sheetFormatPr.isCustomHeight();
            if(customHeight) {
                rowProperties.put(OCKey.CUSTOM_HEIGHT.value(), true);
            }
            rowProperties.put(OCKey.HEIGHT.value(), sheetFormatPr.getDefaultRowHeight());
            if(sheetFormatPr.isZeroHeight()) {
                rowProperties.put(OCKey.VISIBLE.value(), false);
            }
        }
        return rowProperties;
    }
}
