/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.CTAutoFilter;
import org.xlsx4j.sml.CTFilterColumn;
import org.xlsx4j.sml.CTSortCondition;
import org.xlsx4j.sml.CTSortState;
import org.xlsx4j.sml.Worksheet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.ooxml.xlsx.operations.XlsxApplyOperationHelper;

public class AutoFilterHelper {

    enum FilterType {
        NONE,
        DISCRETE
    };

    public static void createAutoFilterOperations(JSONArray operationsArray, int sheetIndex, CTAutoFilter autoFilter)
        throws JSONException {

        if(autoFilter==null) {
            return;
        }
        CTSortState sortState = autoFilter.getSortState(false);
        final CellRefRange cellRefRange = autoFilter.getCellRefRange(false);
        if(cellRefRange==null) {
            return;
        }
        JSONObject attrs = new JSONObject(1);
        final JSONObject tableAttrs = new JSONObject(1);
        tableAttrs.put(OCKey.HEADER_ROW.value(), true);
        if(sortState!=null&&sortState.isCaseSensitive()) {
            tableAttrs.put(OCKey.CASE_SENSITIVE.value(), true);
        }
        attrs.put(OCKey.TABLE.value(), tableAttrs);
        addInsertTableOperation(operationsArray, sheetIndex, cellRefRange, "", attrs);
        TableHelper.createChangeTableColumnOperations(operationsArray, sheetIndex, "", cellRefRange.getStart().getColumn(), autoFilter, sortState);
    }

    public static void addInsertTableOperation(JSONArray operationsArray, int sheetIndex, CellRefRange range, String tableName, JSONObject attrs)
            throws JSONException {

        final JSONObject addInsertTableObject = new JSONObject(6);
        addInsertTableObject.put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value());
        addInsertTableObject.put(OCKey.SHEET.value(), sheetIndex);
        addInsertTableObject.put(OCKey.RANGE.value(), CellRefRange.getCellRefRange(range));
        if(!attrs.isEmpty()) {
            addInsertTableObject.put(OCKey.ATTRS.value(), attrs);
        }
        if(tableName!=null && tableName.length()>0) {
            addInsertTableObject.put(OCKey.TABLE.value(), tableName);
        }
        operationsArray.put(addInsertTableObject);
    }

    public static void changeOrInsertAutoFilter(XlsxApplyOperationHelper applyOperationHelper, int sheetIndex, String range, JSONObject attrs) {
        final Worksheet worksheet = (Worksheet)applyOperationHelper.getWorksheet(sheetIndex);
        final CTAutoFilter autoFilter = worksheet.getAutoFilter(true);
        if(range!=null) {
            autoFilter.setCellRefRange(CellRefRange.createCellRefRange(range));
        }
        if(attrs!=null) {
            applyTableAttributes(autoFilter, attrs);
        }
    }

    public static void deleteAutoFilter(XlsxApplyOperationHelper applyOperationHelper, int sheetIndex) {

        final Worksheet worksheet = (Worksheet)applyOperationHelper.getWorksheet(sheetIndex);
        worksheet.setAutoFilter(null);
    }

    private static void applyTableAttributes(CTAutoFilter autoFilter, JSONObject attrs) {
        final JSONObject tableAttrs = attrs.optJSONObject(OCKey.TABLE.value());
        if(tableAttrs!=null) {
            final Object caseSensitive = tableAttrs.opt(OCKey.CASE_SENSITIVE.value());
            if(caseSensitive instanceof Boolean&&((Boolean)caseSensitive).booleanValue()) {
                autoFilter.getSortState(true).setCaseSensitive(Boolean.TRUE);
            }
            else {
                final CTSortState sortState = autoFilter.getSortState(false);
                if(sortState!=null) {
                    sortState.setCaseSensitive(null);
                }
            }
        }

    }

    /*
     * returns the new autoFilter cellRefRange or null if the autoFilter has to be deleted
     */
    public static CellRefRange handleAutoFilter(CTAutoFilter autoFilter, int start, int count, boolean column, int rel) {
        final CellRefRange oldCellRefRange = autoFilter.getCellRefRange(false);
        final CellRefRange newCellRefRange = CellRefRange.modifyCellRefRange(oldCellRefRange, start, count, column, rel, false);
        if(newCellRefRange==null) {
            return null;
        }
        // if the cellRefRange is unchanged then also the complete autoFilter remains unchanged
        if(!newCellRefRange.equals(oldCellRefRange)) {
            autoFilter.setCellRefRange(newCellRefRange);

            // update filter columns
            final List<CTFilterColumn> filterColumnList = autoFilter.getFilterColumn(false);
            if(filterColumnList!=null) {
                if(column) {
                    if(start <= oldCellRefRange.getEnd().getColumn()) {
                        if(rel>0) {
                            if(start > oldCellRefRange.getStart().getColumn()) {
                                for(int i = filterColumnList.size() - 1; i >= 0; i--) {
                                    final CTFilterColumn filterColumn = filterColumnList.get(i);
                                    final long oldColumn = oldCellRefRange.getStart().getColumn() + filterColumn.getColId();
                                    if(start<=oldColumn) {
                                        final long newColumn = oldColumn + count;
                                        if(newColumn >= 16384) {
                                            filterColumnList.remove(i);
                                        }
                                        else {
                                            filterColumn.setColId(filterColumn.getColId() + count);
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if(start >= oldCellRefRange.getStart().getColumn()) {
                                for(int i = filterColumnList.size() - 1; i >= 0; i--) {
                                    final CTFilterColumn filterColumn = filterColumnList.get(i);
                                    final long oldColumn = oldCellRefRange.getStart().getColumn() + filterColumn.getColId();
                                    if(start<=oldColumn) {
                                        if((oldColumn - count) >= start) {
                                            filterColumn.setColId(filterColumn.getColId() - count);
                                        }
                                        else {
                                            filterColumnList.remove(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            final CTSortState sortState = autoFilter.getSortState(false);
            if(sortState!=null) {
                final CellRefRange sortRange = sortState.getCellRefRange(false);
                if(sortRange!=null) {
                    final CellRefRange newSortRange = CellRefRange.modifyCellRefRange(sortRange, start, count, column, rel, false);
                    if(newSortRange==null) {
                        autoFilter.setSortState(null);
                    }
                    else {
                        sortState.setCellRefRange(newSortRange);
                        final List<CTSortCondition> sortConditions = sortState.getSortCondition();
                        for(int j = sortConditions.size() - 1; j >= 0; j--) {
                            final CTSortCondition sortCondition = sortConditions.get(j);
                            final CellRefRange conditionRange = sortCondition.getCellRefRange(false);
                            if(conditionRange!=null) {
                                final CellRefRange newConditionRange = CellRefRange.modifyCellRefRange(conditionRange, start, count, column, rel, false);
                                if(newConditionRange!=null) {
                                    sortCondition.setCellRefRange(newConditionRange);
                                }
                                else {
                                    sortConditions.remove(j);
                                }
                            }
                        }
                    }
                }
            }
        }
        return newCellRefRange;
    }
}
