/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTConnector;
import org.docx4j.dml.spreadsheetDrawing.CTGraphicalObjectFrame;
import org.docx4j.dml.spreadsheetDrawing.CTGroupShape;
import org.docx4j.dml.spreadsheetDrawing.CTPicture;
import org.docx4j.dml.spreadsheetDrawing.CTShape;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;

public class ShapeGroupComponent extends XlsxComponent implements IShapeType {

	final private CTGroupShape groupShape;
	private AnchorBase cellAnchor;

	public ShapeGroupComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, CTGroupShape groupShape, AnchorBase cellAnchor, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.groupShape = groupShape;
        this.cellAnchor = cellAnchor;
	}

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> groupShapeNode = getNode();
        final DLList<Object> nodeList = groupShape.getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        OfficeOpenXMLComponent nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, groupShapeNode.getData());
            if(o instanceof CTGroupShape) {
            	nextComponent = new ShapeGroupComponent(this, childNode, (CTGroupShape)o, null, nextComponentNumber);
            }
            else if(o instanceof CTShape) {
            	nextComponent = new ShapeComponent(this, childNode, (CTShape)o, null, nextComponentNumber);
            }
            else if(o instanceof CTGraphicalObjectFrame) {
            	nextComponent = new ShapeGraphicComponent(this, childNode, (CTGraphicalObjectFrame)o, null, nextComponentNumber);
            }
            else if(o instanceof CTConnector) {
            	nextComponent = new ShapeConnectorComponent(this, childNode, (CTConnector)o, null, nextComponentNumber);
            }
            else if(o instanceof CTPicture) {
            	nextComponent = new ShapePicComponent(this, childNode, (CTPicture)o, null, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = groupShape.getContent();
    	DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
        	case AC_SHAPE: {
	            final CTShape newChild = Drawings.createShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeComponent(parentContext, newChildNode, newChild, null, number);
        	}
        	case AC_GROUP: {
	            final CTGroupShape newChild = Drawings.createGroupShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeGroupComponent(parentContext, newChildNode, newChild, null, number);
        	}
        	case AC_CHART: {
        	    try {
        	        final CTGraphicalObjectFrame newChild = Drawings.createChart(getOperationDocument(), getOperationDocument().getContextPart(), new JSONObject());
                    newChild.setParent(contextNode.getData());
                    final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                    DLList.addNode(referenceNode, newChildNode, true);
                    return new ShapeGraphicComponent(parentContext, newChildNode, newChild, null, number);
        	    }
        	    catch(Exception e) {
        	        throw new UnsupportedOperationException();
        	    }
        	}
        	case AC_CONNECTOR: {
	            final CTConnector newChild = Drawings.createConnectorShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeConnectorComponent(parentContext, newChildNode, newChild, null, number);
        	}
        	case AC_IMAGE: {
	            final CTPicture newChild = Drawings.createImage();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapePicComponent(parentContext, newChildNode, newChild, null, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {
        if(cellAnchor!=null) {
            final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
            if(drawingAttrs!=null) {
                getNode().setData(Drawings.setDrawingAnchor(drawingAttrs, cellAnchor));
            }
        }
        com.openexchange.office.filter.ooxml.drawingml.GroupShape.applyAttrsFromJSON(operationDocument, attrs, groupShape, cellAnchor!=null);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
        throws JSONException {

        if(cellAnchor!=null) {
            Drawings.createAnchorOperations(cellAnchor, attrs);
        }
        com.openexchange.office.filter.ooxml.drawingml.GroupShape.createJSONAttrs(operationDocument, attrs, groupShape, cellAnchor!=null);
        return attrs;
    }

	@Override
	public ShapeType getType() {
		return ShapeType.GROUP;
	}
}
