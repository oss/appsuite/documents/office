/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.lang3.mutable.MutableInt;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.SpreadsheetML.JaxbSmlPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.TablePart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTAutoFilter;
import org.xlsx4j.sml.CTCellFormula;
import org.xlsx4j.sml.CTDateGroupItem;
import org.xlsx4j.sml.CTFilter;
import org.xlsx4j.sml.CTFilterColumn;
import org.xlsx4j.sml.CTFilters;
import org.xlsx4j.sml.CTSortCondition;
import org.xlsx4j.sml.CTSortState;
import org.xlsx4j.sml.CTTable;
import org.xlsx4j.sml.CTTableColumn;
import org.xlsx4j.sml.CTTableColumns;
import org.xlsx4j.sml.CTTableFormula;
import org.xlsx4j.sml.CTTablePart;
import org.xlsx4j.sml.CTTableParts;
import org.xlsx4j.sml.CTTableStyleInfo;
import org.xlsx4j.sml.Cell;
import org.xlsx4j.sml.IAutoFilterAccessor;
import org.xlsx4j.sml.ISortStateAccessor;
import org.xlsx4j.sml.Row;
import org.xlsx4j.sml.STCellFormulaType;
import org.xlsx4j.sml.STCellType;
import org.xlsx4j.sml.STSortBy;
import org.xlsx4j.sml.STTotalsRowFunction;
import org.xlsx4j.sml.SheetData;
import org.xlsx4j.sml.Worksheet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.operations.XlsxApplyOperationHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.AutoFilterHelper.FilterType;

public class TableHelper {

    public static void createTableOperations(JSONArray operationsArray, WorksheetPart worksheetPart, int sheetIndex, CTTableParts tableParts)
        throws JSONException {

        if(tableParts==null) {
            return;
        }
    	final List<CTTablePart> tablePartList = tableParts.getTablePart();
    	for(CTTablePart tablePartId:tablePartList) {
    		final CTTable table = getTable(worksheetPart, tablePartId);
    		if(table!=null) {
    	        final CellRefRange cellRefRange = table.getCellRefRange(false);
    			final String tableName = table.getDisplayName();
    	        if (cellRefRange!=null && tableName!=null && !tableName.isEmpty()) {
    	            final JSONObject attrs = new JSONObject(1);
    	            final JSONObject tableAttrs = new JSONObject();
    	            if (table.getHeaderRowCount() == 1) { tableAttrs.put(OCKey.HEADER_ROW.value(), true); }
    	            if (table.getTotalsRowCount() == 1) { tableAttrs.put(OCKey.FOOTER_ROW.value(), true); }
    	            final CTTableStyleInfo styleInfo = table.getTableStyleInfo(false);
    	            if (styleInfo != null) {
    	            	String styleId = styleInfo.getName();
    	            	if (styleId != null) { attrs.put(OCKey.STYLE_ID.value(), styleId); }
    	            	Boolean firstCol = styleInfo.isShowFirstColumn();
    	            	if (firstCol != null && firstCol.booleanValue()) { tableAttrs.put(OCKey.FIRST_COL.value(), true); }
    	            	Boolean lastCol = styleInfo.isShowLastColumn();
    	            	if (lastCol != null && lastCol.booleanValue()) { tableAttrs.put(OCKey.LAST_COL.value(), true); }
    	            	Boolean rowStripes = styleInfo.isShowRowStripes();
    	            	if (rowStripes != null && rowStripes.booleanValue()) { tableAttrs.put(OCKey.BANDS_HOR.value(), true); }
    	            	Boolean colStripes = styleInfo.isShowColumnStripes();
    	            	if (colStripes != null && colStripes.booleanValue()) { tableAttrs.put(OCKey.BANDS_VERT.value(), true); }
    	            }
    	            final CTSortState sortState = table.getSortState(false);
    	            if(sortState!=null&&sortState.isCaseSensitive()) {
    	                tableAttrs.put(OCKey.CASE_SENSITIVE.value(), true);
    	            }
                    final CTAutoFilter autoFilter = table.getAutoFilter(false);
                    if(autoFilter==null) {
                        tableAttrs.put(OCKey.HIDE_BUTTONS.value(), true);
                    }
    	            if (!tableAttrs.isEmpty()) {
    	            	attrs.put(OCKey.TABLE.value(), tableAttrs);
    	            }
    	            addInsertTableOperation(operationsArray, sheetIndex, cellRefRange, tableName, attrs);
    	            createChangeTableColumnOperations(operationsArray, sheetIndex, tableName, cellRefRange.getStart().getColumn(), autoFilter, table.getSortState(false));
    	        }
    		}
    	}
    }

    public static void createChangeTableColumnOperations(JSONArray operationsArray, int sheetIndex, String tableName, int tableStartColumn, CTAutoFilter autoFilter, CTSortState sortState)
        throws JSONException {

        if(autoFilter==null&&sortState==null) {
            return;
        }
        final LinkedHashMap<Integer, JSONObject> columns = new LinkedHashMap<Integer, JSONObject>();
        if(sortState!=null) {
            final Iterator<CTSortCondition> sortConditionIter = sortState.getSortCondition().iterator();
            while(sortConditionIter.hasNext()) {
                final CTSortCondition sortCondition = sortConditionIter.next();
                final String ref = sortCondition.getRef();
                if(ref!=null) {
                    final JSONObject sortAttrs = new JSONObject();
                    final Integer column = Integer.valueOf(CellRef.createCellRef(ref).getColumn()-tableStartColumn);
                    if(column.intValue()>=0) {
                        final STSortBy sortBy = sortCondition.getSortBy();
                        String type = "value";
                        if(sortBy==STSortBy.CELL_COLOR) {
                            type = "fill";
                        }
                        else if(sortBy==STSortBy.FONT_COLOR) {
                            type = "font";
                        }
                        else if(sortBy==STSortBy.ICON) {
                            type = "icon";
                        }
                        sortAttrs.put(OCKey.TYPE.value(), type);
                        final Boolean descending = sortCondition.getDescending();
                        if(descending!=null) {
                            sortAttrs.put(OCKey.DESCENDING.value(), descending);
                        }
                        final Long dxfId = sortCondition.getDxfId();
                        if(dxfId!=null) {
                            sortAttrs.put(OCKey.DXF.value(), dxfId);
                        }
                        final String is = sortCondition.getIconSet();
                        if(is!=null) {
                            sortAttrs.put(OCKey.IS.value(), is);
                        }
                        final Long id = sortCondition.getIconId();
                        if(id!=null) {
                            sortAttrs.put(OCKey.ID.value(), id);
                        }
                        final String customList = sortCondition.getCustomList();
                        if(customList!=null) {
                            sortAttrs.put(OCKey.LIST.value(), customList);
                        }
                        final JSONObject attrs = new JSONObject();
                        columns.put(column, attrs);
                        attrs.put(OCKey.SORT.value(), sortAttrs);
                    }
                }
            }
        }

        // collecting filter columns ...
        if(autoFilter!=null) {
            final List<CTFilterColumn> filterColumns = autoFilter.getFilterColumn(true);
            for(CTFilterColumn filterColumn:filterColumns) {
                final JSONObject filterAttrs = new JSONObject(3);
                final CTFilters filters = filterColumn.getFilters();
                if(tableName!=null&&!tableName.isEmpty()) {
                    if(filterColumn.isHiddenButton()) {
                        filterAttrs.put(OCKey.HIDE_BUTTON.value(), true);
                    }
                }
                else {
                    if(!filterColumn.isShowButton()) {
                        filterAttrs.put(OCKey.HIDE_BUTTON.value(), true);
                    }
                };
                if(filters!=null) {
                    filterAttrs.put(OCKey.TYPE.value(), "discrete");
                    final List<CTFilter> filterList = filters.getFilter();
                    final JSONArray filterArray = new JSONArray(filterList.size()+1);
                    for(CTFilter filter:filterList) {
                        filterArray.put(filter.getVal());
                    }
                    final List<CTDateGroupItem> dateGroupList = filters.getDateGroupItem();
                    for(CTDateGroupItem dateGroupItem:dateGroupList) {
                        filterArray.put(createDateGroupEntry(dateGroupItem));
                    }
                    if(filters.isBlank()) {
                        filterArray.put("");
                    }
                    filterAttrs.put(OCKey.ENTRIES.value(), filterArray);
                }
                if(!filterAttrs.isEmpty()) {
                    final long id = filterColumn.getColId();
                    if(id>=0&&id<16384) {
                        JSONObject attrs = columns.get(Integer.valueOf((int)id));
                        if(attrs==null) {
                            attrs = new JSONObject();
                            columns.put(Integer.valueOf((int)id), attrs);
                        }
                        attrs.put(OCKey.FILTER.value(), filterAttrs);
                    }
                }
            }
        }

        final Iterator<Entry<Integer, JSONObject>> columnIter = columns.entrySet().iterator();
        while(columnIter.hasNext()) {
            final Entry<Integer, JSONObject> columnEntry = columnIter.next();
            addChangeTableColumnOperation(operationsArray, sheetIndex, tableName, columnEntry.getKey().intValue(), columnEntry.getValue());
        }
    }

    private static JSONObject createDateGroupEntry(CTDateGroupItem dateGroupItem) throws JSONException {
        final JSONObject entry = new JSONObject(2);
        entry.put(OCKey.T.value(), OCValue.DATE.value());
        final StringBuilder val = new StringBuilder();
        val.append(dateGroupItem.getYear());
        if(dateGroupItem.getMonth()!=null) {
            val.append(' ');
            val.append(dateGroupItem.getMonth().intValue());
            if(dateGroupItem.getDay()!=null) {
                val.append(' ');
                val.append(dateGroupItem.getDay().intValue());
                if(dateGroupItem.getHour()!=null) {
                    val.append(' ');
                    val.append(dateGroupItem.getHour().intValue());
                    if(dateGroupItem.getMinute()!=null) {
                        val.append(' ');
                        val.append(dateGroupItem.getMinute().intValue());
                        if(dateGroupItem.getSecond()!=null) {
                            val.append(' ');
                            val.append(dateGroupItem.getSecond().intValue());
                        }
                    }
                }
            }
        }
        return entry.put(OCKey.V.value(), val.toString());
    }

    public static void addChangeTableColumnOperation(JSONArray operationsArray, int sheetIndex, String tableName, int columnIndex, JSONObject attrs)
        throws JSONException {
        if(!attrs.isEmpty()) {
            final JSONObject addInsertChangeTableColumnObject = new JSONObject();
            addInsertChangeTableColumnObject.put(OCKey.NAME.value(), OCValue.CHANGE_TABLE_COLUMN.value());
            addInsertChangeTableColumnObject.put(OCKey.SHEET.value(), sheetIndex);
            if(tableName!=null && tableName.length()>0) {
                addInsertChangeTableColumnObject.put(OCKey.TABLE.value(), tableName);
            }
            addInsertChangeTableColumnObject.put(OCKey.COL.value(), columnIndex);
            addInsertChangeTableColumnObject.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(addInsertChangeTableColumnObject);
        }
    }

    public static void addInsertTableOperation(JSONArray operationsArray, int sheetIndex, CellRefRange range, String tableName, JSONObject attrs)
            throws JSONException {

        final JSONObject addInsertTableObject = new JSONObject(6);
        addInsertTableObject.put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value());
        addInsertTableObject.put(OCKey.SHEET.value(), sheetIndex);
        addInsertTableObject.put(OCKey.RANGE.value(), CellRefRange.getCellRefRange(range));
        if(!attrs.isEmpty()) {
            addInsertTableObject.put(OCKey.ATTRS.value(), attrs);
        }
        addInsertTableObject.put(OCKey.TABLE.value(), tableName);
        operationsArray.put(addInsertTableObject);
    }

    public static void changeOrInsertTable(XlsxApplyOperationHelper applyOperationHelper, int sheetIndex, String tableName, String range, JSONObject attrs, boolean insert)
        throws InvalidFormatException {

    	final WorksheetPart worksheetPart = (WorksheetPart)applyOperationHelper.getSheetPart(sheetIndex);
        CTTable table;
    	if(insert) {
    		final CellRefRange cellRefRange = CellRefRange.createCellRefRange(range);
            final CTTableParts tableParts = worksheetPart.getJaxbElement().getTableParts(true);
            final TablePart tablePart = new TablePart();
            table = new CTTable();
            tablePart.setJaxbElement(table);
            final Relationship tableRelation = worksheetPart.addTargetPart(tablePart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
            final CTTablePart ctTablePart = new CTTablePart();
            ctTablePart.setId(tableRelation.getId());
            tableParts.getTablePart().add(ctTablePart);
            table.setDisplayName(tableName);
            table.setCellRefRange(cellRefRange);
            final CTAutoFilter autoFilter = new CTAutoFilter();
            autoFilter.setCellRefRange(cellRefRange);
            table.setAutoFilter(autoFilter);
            final CTTableColumns tableColumns = table.getTableColumns(true);
            for(int i=cellRefRange.getStart().getColumn(); i<cellRefRange.getEnd().getColumn(); i++) {
            	tableColumns.getTableColumn().add(new CTTableColumn());
            }
    	}
    	else {
    		table = getTable(worksheetPart, tableName);
    		if(range!=null) {
    		    table.setCellRefRange(CellRefRange.createCellRefRange(range));
    		}
        	final CTAutoFilter autoFilter = table.getAutoFilter(false);
        	if(autoFilter!=null) {
        	    autoFilter.setCellRefRange(table.getCellRefRange(true).clone());
        	}
        	final CTSortState sortState = table.getSortState(false);
        	if(sortState!=null) {
        	    sortState.setCellRefRange(table.getCellRefRange(true).clone());
        	}
    	}

    	// apply table attributes
    	if (attrs!=null) {
    	    final Object styleId = attrs.opt(OCKey.STYLE_ID.value());
    	    if(styleId instanceof String) {
    	        table.getTableStyleInfo(true).setName((String)styleId);
    	    }
    	    else if (styleId==JSONObject.NULL) {
    	        table.getTableStyleInfo(true).setName(null);
    	    }
    		final JSONObject tableAttrs = attrs.optJSONObject(OCKey.TABLE.value());
    		if (tableAttrs!=null) {
				table.setHeaderRowCount(tableAttrs.optBoolean(OCKey.HEADER_ROW.value()) ? null : Long.valueOf(0)); // default is "1"
				table.setTotalsRowCount(tableAttrs.optBoolean(OCKey.FOOTER_ROW.value()) ? Long.valueOf(1) : null); // default is "0"

				final CTTableStyleInfo styleInfo = table.getTableStyleInfo(true);
				styleInfo.setShowFirstColumn(tableAttrs.optBoolean(OCKey.FIRST_COL.value()) ? true : null);
				styleInfo.setShowLastColumn(tableAttrs.optBoolean(OCKey.LAST_COL.value()) ? true : null);
				styleInfo.setShowRowStripes(tableAttrs.optBoolean(OCKey.BANDS_HOR.value()) ? true : null);
				styleInfo.setShowColumnStripes(tableAttrs.optBoolean(OCKey.BANDS_VERT.value()) ? true : null);

	            final Object caseSensitive = tableAttrs.opt(OCKey.CASE_SENSITIVE.value());
	            if(caseSensitive instanceof Boolean&&((Boolean)caseSensitive).booleanValue()) {
	                final CTSortState sortState = table.getSortState(true);
	                sortState.setCaseSensitive(Boolean.TRUE);
	                if(sortState.getRef()==null) {
	                    final CellRefRange tableRef = table.getCellRefRange(true);
	                    final CellRefRange sortRef = new CellRefRange(tableRef.getStart().getColumn(), tableRef.getStart().getRow()+1, tableRef.getEnd().getColumn(), tableRef.getEnd().getRow()+1);
	                    sortState.setRef(CellRefRange.getCellRefRange(sortRef));
	                }
	            }
	            else {
	                final CTSortState sortState = table.getSortState(false);
	                if(sortState!=null) {
	                    sortState.setCaseSensitive(null);
	                }
	            }
    		}
    	}
    }

    public static void deleteTable(XlsxApplyOperationHelper applyOperationHelper, int sheetIndex, String tableName) {
    	deleteTable((WorksheetPart)applyOperationHelper.getSheetPart(sheetIndex), tableName);
    }

    private static void deleteTable(WorksheetPart worksheetPart, String tableName) {
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        final RelationshipsPart worksheetRelationships = worksheetPart.getRelationshipsPart();
        if(worksheetRelationships!=null) {
        	final CTTableParts tableParts = worksheet.getTableParts(false);
        	final Iterator<CTTablePart> tablePartIter = tableParts.getTablePart().iterator();
        	while(tablePartIter.hasNext()) {
        		final CTTablePart tablePartId = tablePartIter.next();
        		final CTTable table = getTable(worksheetPart, tablePartId);
        		if(table!=null) {
        			if(tableName.equals(table.getDisplayName())) {
        				worksheetRelationships.removeRelationship(worksheetRelationships.getRelationshipByID(tablePartId.getId()));
        				tablePartIter.remove();
        				break;
        			}
        		}
        	}
        }
    }

    public static void changeTableColumn(XlsxApplyOperationHelper applyOperationHelper, boolean sort, int sheetIndex, String tableName, int col, JSONObject attrs)
        throws JSONException {

        final IAutoFilterAccessor autoFilterAccessor;
        final ISortStateAccessor sortStateAccessor;

        final JaxbSmlPart<?> worksheetPart = applyOperationHelper.getSheetPart(sheetIndex);
        final CellRefRange refRange;
        if(tableName==null||tableName.isEmpty()) {
            final Worksheet worksheet = (Worksheet)worksheetPart.getJaxbElement();
            autoFilterAccessor = worksheet;
            final CTAutoFilter autoFilter = autoFilterAccessor.getAutoFilter(false);
            if(autoFilter!=null) {
                sortStateAccessor = autoFilter;
                refRange = autoFilter.getCellRefRange(true);
            }
            else {
                sortStateAccessor = worksheet;
                refRange = null;
            }
        }
        else {
            final CTTable table = getTable((WorksheetPart)worksheetPart, tableName);
            refRange = table.getCellRefRange(true);
            autoFilterAccessor = table;
            sortStateAccessor = table;
        }

        if(refRange!=null) {
            Object o= attrs.opt(OCKey.FILTER.value());
            if(o!=null) {
                if(o==JSONObject.NULL) {
                    autoFilterAccessor.setAutoFilter(null);
                }
                else if(o instanceof JSONObject) {
                    applyAutoFilter(autoFilterAccessor.getAutoFilter(true), col, (JSONObject)o, sort);
                }
            }
            o = attrs.opt(OCKey.SORT.value());
            if(o!=null) {
                if(o==JSONObject.NULL) {
                    sortStateAccessor.setSortState(null);
                }
                else if(o instanceof JSONObject) {
                    final CTSortState sortState = sortStateAccessor.getSortState(true);
                    if(sortState.getRef()==null) {
                        final CellRefRange sortRef = new CellRefRange(refRange.getStart().getColumn(), refRange.getStart().getRow()+1, refRange.getEnd().getColumn(), refRange.getEnd().getRow()+1);
                        sortState.setRef(CellRefRange.getCellRefRange(sortRef));
                    }
                    applySortState(refRange, sortState, col, (JSONObject)o, sort);
                }
            }
        }
    }

    public static void applyAutoFilter(CTAutoFilter autoFilter, long col, JSONObject autoFilterAttrs, boolean sort)
        throws JSONException {

        final JSONArray entries = autoFilterAttrs.optJSONArray(OCKey.ENTRIES.value());
        FilterType filterType = FilterType.NONE;
        final String v = autoFilterAttrs.optString(OCKey.TYPE.value(), null);
        if(v!=null) {
            if(v.equals("none")) {
                filterType = FilterType.NONE;
            }
            else if(v.equals("discrete")) {
                filterType = FilterType.DISCRETE;
            }
        }
        final List<CTFilterColumn> filterColumns = autoFilter.getFilterColumn(true);
        int filterColumnIndex = 0;
        CTFilterColumn filterColumn = null;
        for(;filterColumnIndex<filterColumns.size();filterColumnIndex++) {
            if(filterColumns.get(filterColumnIndex).getColId()==col) {
                filterColumn = filterColumns.remove(filterColumnIndex);
                break;
            }
        }
        if(filterColumn==null) {
            filterColumn = Context.getsmlObjectFactory().createCTFilterColumn();
            filterColumn.setColId(col);
        }
        filterColumns.add(sort ? filterColumns.size() : filterColumnIndex, filterColumn);
        if(filterType==FilterType.NONE) {
            final Object hideButton = autoFilterAttrs.opt(OCKey.HIDE_BUTTON.value());
            if(hideButton!=null) {
                filterColumn.setShowButton(hideButton instanceof Boolean?!((Boolean)hideButton).booleanValue():null);
            }
            filterColumn.setFilters(null);
        }
        else if(filterType==FilterType.DISCRETE) {
            CTFilters filters = filterColumn.getFilters();
            if(filters==null) {
                filters = Context.getsmlObjectFactory().createCTFilters();
                filterColumn.setFilters(filters);
            }
            if(entries!=null) {
                final List<CTFilter> filterList = filters.getFilter();
                final List<CTDateGroupItem> dateGroupList = filters.getDateGroupItem();
                filterList.clear();
                dateGroupList.clear();
                for(int i=0; i<entries.length(); i++) {
                    final Object filterValue = entries.get(i);
                    if(filterValue instanceof String) {
                        if(((String)filterValue).isEmpty()) {
                            filters.setBlank(Boolean.TRUE);
                        }
                        else {
                            filterList.add(new CTFilter((String)filterValue));
                        }
                    }
                    else if(filterValue instanceof JSONObject) {
                        final String type = ((JSONObject)filterValue).getString(OCKey.T.value());
                        if(type.equals(OCValue.DATE.value())) {
                            dateGroupList.add(new CTDateGroupItem(((JSONObject)filterValue).getString(OCKey.V.value())));
                        }
                    }
                }
            }
        }
    }

    /*
     * the parameter refRange descibes the range of the whole table.
     */
    public static void applySortState(CellRefRange refRange, CTSortState sortState, int col, JSONObject sortStateAttrs, boolean sort) {
        CTSortCondition currentCondition = null;
        final List<CTSortCondition> sortConditionList = sortState.getSortCondition();
        int sortIndex = 0;
        for(; sortIndex<sortConditionList.size(); sortIndex++) {
            final CTSortCondition ci = sortConditionList.get(sortIndex);
            if(CellRef.createCellRef(ci.getRef()).getColumn()-refRange.getStart().getColumn()==col) {
                currentCondition = sortConditionList.remove(sortIndex);
                break;
            }
        }
        final String type = sortStateAttrs.optString(OCKey.TYPE.value(), "none");
        if(type.equals("none")) {
            return;
        }
        if(currentCondition==null) {
            currentCondition = new CTSortCondition();
            final int c = refRange.getStart().getColumn() + col;
            final CellRefRange newCellRefRange =  new CellRefRange(c, refRange.getStart().getRow()+1, c, refRange.getEnd().getRow());
            currentCondition.setRef(CellRefRange.getCellRefRange(newCellRefRange));
        }
        sortConditionList.add(sort ? sortConditionList.size() : sortIndex, currentCondition);
        if(type.equals("value")) {
            currentCondition.setSortBy(null);
        }
        else if(type.equals("font")) {
            currentCondition.setSortBy(STSortBy.FONT_COLOR);
        }
        else if(type.equals("fill")) {
            currentCondition.setSortBy(STSortBy.CELL_COLOR);
        }
        else if(type.equals("icon")) {
            currentCondition.setSortBy(STSortBy.ICON);
        }
        final Object descending = sortStateAttrs.opt(OCKey.DESCENDING.value());
        if(descending!=null) {
            currentCondition.setDescending(descending==JSONObject.NULL ? null : ((Boolean)descending).booleanValue());
        }
        final Object dxf = sortStateAttrs.opt(OCKey.DXF.value());
        if(dxf!=null) {
            currentCondition.setDxfId(dxf instanceof Number ? ((Number)dxf).longValue() :  null);
        }
        final Object list = sortStateAttrs.opt(OCKey.LIST.value());
        if(list!=null) {
            currentCondition.setCustomList(list instanceof String ? (String)list : null);
        }
        final Object is = sortStateAttrs.opt(OCKey.IS.value());
        if(is!=null) {
            currentCondition.setIconSet(is instanceof String ? (String)is : null);
        }
        final Object id = sortStateAttrs.opt(OCKey.ID.value());
        if(id!=null) {
            currentCondition.setIconId(id instanceof Number ? ((Number)id).longValue() : null);
        }
    }

    public static CTTable getTable(WorksheetPart worksheetPart, String tableName) {
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
	    	final List<CTTablePart> tablePartList = tableParts.getTablePart();
	    	for(CTTablePart tablePartId:tablePartList) {
	    		final CTTable table = getTable(worksheetPart, tablePartId);
	    		if(table!=null) {
	    	        final CellRefRange cellRefRange = table.getCellRefRange(false);
	    	        if(tableName.equals(table.getDisplayName())&&cellRefRange!=null) {
	    	        	return table;
	    	        }
	    		}
	    	}
        }
    	return null;
    }

    private static CTTable getTable(WorksheetPart worksheetPart, CTTablePart tablePart) {
    	final String id = tablePart.getId();
    	if(id!=null&&!id.isEmpty()) {
	        final RelationshipsPart worksheetRelationships = worksheetPart.getRelationshipsPart();
	        if(worksheetRelationships!=null) {
	        	final Part part = worksheetRelationships.getPart(id);
	        	if(part.getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/table")&&part instanceof TablePart) {
	        		return ((TablePart)part).getJaxbElement();
	        	}
	        }
    	}
    	return null;
    }

    public static void insertRows(WorksheetPart worksheetPart, CTTablePart tablePart, int start, int count) {
    	final CTTable table = getTable(worksheetPart, tablePart);
    	final CellRefRange tableRef = CellRefRange.insertRowRange(table.getCellRefRange(true), start, count, false);
    	final CellRefRange newAutoFilterRef = table.getAutoFilter(false) != null ? AutoFilterHelper.handleAutoFilter(table.getAutoFilter(true), start, count, false, 1) : null;
    	final CTSortState sortState = table.getSortState(false);
    	if(sortState!=null) {
    	    final CellRefRange sortRef = CellRefRange.insertRowRange(table.getSortState(false).getCellRefRange(true), start, count, false);
    	    if(sortRef!=null) {
    	        sortState.setCellRefRange(sortRef);
    	        CellRefRange.insertRows(sortState.getSortCondition(), start, count, false, null);
    	    }
    	    else {
    	        table.setSortState(null);
    	    }
    	}
    	applyTableRefChange(worksheetPart, table, tableRef, newAutoFilterRef);
    }

    public static void deleteRows(WorksheetPart worksheetPart, CTTablePart tablePart, int start, int count) {
    	final CTTable table = getTable(worksheetPart, tablePart);
    	final CellRefRange tableRef = CellRefRange.deleteRowRange(table.getCellRefRange(true), start, count);
        final CellRefRange newAutoFilterRef = table.getAutoFilter(false) != null ? AutoFilterHelper.handleAutoFilter(table.getAutoFilter(true), start, count, false, -1) : null;
    	final CTSortState sortState = table.getSortState(false);
    	if(sortState!=null) {
    	    final CellRefRange sortRef = CellRefRange.deleteRowRange(table.getSortState(false).getCellRefRange(true), start, count);
    	    if(sortRef!=null) {
    	        sortState.setCellRefRange(sortRef);
    	        CellRefRange.deleteRows(sortState.getSortCondition(), start, count, false, null);
    	    }
    	    else {
    	        table.setSortState(null);
    	    }
    	}
    	applyTableRefChange(worksheetPart, table, tableRef, newAutoFilterRef);
    }

    public static void insertColumns(WorksheetPart worksheetPart, CTTablePart tablePart, int start, int count) {
    	final CTTable table = getTable(worksheetPart, tablePart);
    	final CellRefRange tableRef = CellRefRange.insertColumnRange(table.getCellRefRange(true), start, count, false);
    	final CellRefRange newAutofilterRef = table.getAutoFilter(false) != null ? AutoFilterHelper.handleAutoFilter(table.getAutoFilter(true), start, count, true, 1) : null;
    	final CTSortState sortState = table.getSortState(false);
    	if(sortState!=null) {
    	    final CellRefRange sortRef = CellRefRange.insertColumnRange(table.getSortState(false).getCellRefRange(true), start, count, false);
    	    if(sortRef!=null) {
    	        sortState.setCellRefRange(sortRef);
    	        CellRefRange.insertColumns(sortState.getSortCondition(), start, count, false, null);
    	    }
    	    else {
    	        table.setSortState(null);
    	    }
    	}
    	applyTableRefChange(worksheetPart, table, tableRef, newAutofilterRef);
    }

    public static void deleteColumns(WorksheetPart worksheetPart, CTTablePart tablePart, int start, int count) {
    	final CTTable table = getTable(worksheetPart, tablePart);
    	final CellRefRange tableRef = CellRefRange.deleteColumnRange(table.getCellRefRange(true), start, count);
    	final CellRefRange newAutofilterRef = table.getAutoFilter(false) != null ? AutoFilterHelper.handleAutoFilter(table.getAutoFilter(true), start, count, true, -1) : null;
    	final CTSortState sortState = table.getSortState(false);
    	if(sortState!=null) {
            final CellRefRange sortRef = CellRefRange.deleteColumnRange(table.getSortState(false).getCellRefRange(true), start, count);
            if(sortRef!=null) {
                sortState.setCellRefRange(sortRef);
                CellRefRange.deleteColumns(sortState.getSortCondition(), start, count, false, null);

            }
            else {
                table.setSortState(null);
            }
    	}
    	applyTableRefChange(worksheetPart, table, tableRef, newAutofilterRef);
    }

    private static void applyTableRefChange(WorksheetPart worksheetPart, CTTable table, CellRefRange tableRef, CellRefRange autofilterRef) {
    	if(tableRef!=null) {
    		table.setCellRefRange(tableRef);
    		if(autofilterRef!=null) {
    			table.getAutoFilter(true).setCellRefRange(autofilterRef);
    		}
    		else {
    			table.setAutoFilter(null);
    		}
    	}
    	else {
    		deleteTable(worksheetPart, table.getDisplayName());
    	}
    }

    // this method is called after all operations were applied and the document is about to be saved.
    // we take care that column names matches the corresponding cell values of the worksheet and that each
    // table is getting its own unique id
    public static void finalizeTables(XlsxOperationDocument operationDocument, WorksheetPart worksheetPart, MutableInt uniqueTableId) {
    	final Worksheet worksheet = worksheetPart.getJaxbElement();
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
        	final SheetData sheetData = worksheet.getSheetData();
        	final List<CTTablePart> tablePartList = tableParts.getTablePart();
        	for(CTTablePart tablePart:tablePartList) {
        		final CTTable table = getTable(worksheetPart, tablePart);
        		if(table!=null) {
        			table.setId(uniqueTableId.longValue());
        			uniqueTableId.increment();

    				// ensure the correct number of column entries
        			final CellRefRange cellRefRange = table.getCellRefRange(true);
    				final int columns = (cellRefRange.getEnd().getColumn()-cellRefRange.getStart().getColumn())+1;
    				final CTTableColumns tableColumns = table.getTableColumns(true);
        			tableColumns.setCount(Long.valueOf(columns));
        			final List<CTTableColumn> tableColumnList = tableColumns.getTableColumn();
        			while(tableColumnList.size()<columns) {
        				tableColumnList.add(Context.getsmlObjectFactory().createCTTableColumn());
        			}
        			while(tableColumnList.size()>columns) {
        				tableColumnList.remove(tableColumnList.size()-1);
        			}

        			// applying unique column id
        			long uniqueColumnId = 1;
        			for(CTTableColumn tableColumn:tableColumnList) {
        				tableColumn.setId(uniqueColumnId++);
        			}

        			final long headerRowCount = table.getHeaderRowCount();
        			if(headerRowCount==1) {
            			// take care that the column header matches the cell content
        				// and that the cell content is of type string and that the column name is unique
        				final HashSet<String> nameSet = new HashSet<String>(columns);
        				final Row row = sheetData.getRow(cellRefRange.getStart().getRow(), true);
                        org.docx4j.jaxb.Context.abortOnLowMemory(operationDocument.getPackage());
        				int c1 = cellRefRange.getStart().getColumn();
        				while(c1<=cellRefRange.getEnd().getColumn()) {
        					final Cell cell = row.getCell(c1, true);
        		            org.docx4j.jaxb.Context.abortOnLowMemory(operationDocument.getPackage());
        					final STCellType cellType = cell.getT();

        					String preferedColumnName = null;
        					if(cellType==STCellType.S) {
        						preferedColumnName = operationDocument.getSharedString(cell.getV());
        					}
        					else if(cellType==STCellType.STR) {
        						preferedColumnName = cell.getV();
        					}
        					final String uniqueName = getCheckUniqueName(nameSet, preferedColumnName, (c1-cellRefRange.getStart().getColumn())+1);
        					tableColumnList.get(c1-cellRefRange.getStart().getColumn()).setName(uniqueName);
        					cell.setT(STCellType.STR);
        					cell.setV(uniqueName);
        					cell.setF(null);
        					c1++;
        				}
        			}
        			else {
        				// just take care of unique column entries.
        				final HashSet<String> nameSet = new HashSet<String>(columns);
        				for(CTTableColumn tableColumn:tableColumnList) {
        					final String preferedColumnName = tableColumn.getName();
        					tableColumn.setName(getCheckUniqueName(nameSet, preferedColumnName, table.getId()));
        				}
        			}

        			final long totalsRowCount = table.getTotalsRowCount();
        			if(totalsRowCount==1) {
        				final Row row = sheetData.getRow(cellRefRange.getEnd().getRow(), true);
                        org.docx4j.jaxb.Context.abortOnLowMemory(operationDocument.getPackage());
        				int c1 = cellRefRange.getStart().getColumn();
        				while(c1<=cellRefRange.getEnd().getColumn()) {
        					final Cell cell = row.getCell(c1, false);
        		            org.docx4j.jaxb.Context.abortOnLowMemory(operationDocument.getPackage());
    						final CTTableColumn tableColumn = tableColumnList.get(c1-cellRefRange.getStart().getColumn());
    						tableColumn.setTotalsRowFormula(null);
    						tableColumn.setTotalsRowFunction(null);
    						tableColumn.setTotalsRowLabel(null);
        					if(cell!=null) {
        						final STCellType cellType = cell.getT()!=null?cell.getT():STCellType.N;
        						final CTCellFormula cellFormula = cell.getF();
        						final boolean isFormula = cellFormula!=null&&(cellFormula.getT()==STCellFormulaType.NORMAL||cellFormula.getT()==STCellFormulaType.SHARED);
        						if(isFormula) {
        							final CTTableFormula tableFormula = Context.getsmlObjectFactory().createCTTableFormula();
        							tableFormula.setValue(cellFormula.getValue());
        							tableColumn.setTotalsRowFunction(STTotalsRowFunction.CUSTOM);
        							tableColumn.setTotalsRowFormula(tableFormula);
        						}
        						else if(cellType==STCellType.S) {
        							tableColumn.setTotalsRowLabel(operationDocument.getSharedString(cell.getV()));
        						}
        						else if(cellType==STCellType.STR) {
        							tableColumn.setTotalsRowLabel(cell.getV());
        						}
        					}
        					c1++;
        				}
        			}
        		}
        	}
        }
    }

    private static String getCheckUniqueName(HashSet<String> nameSet, String preferedName, long column) {
    	if(preferedName==null||preferedName.isEmpty()) {
    		preferedName = "column" + Long.toString(column);
    	}
    	while(nameSet.contains(preferedName)) {
    		preferedName = preferedName + "0";
    	}
    	nameSet.add(preferedName);
    	return preferedName;
    }
}
