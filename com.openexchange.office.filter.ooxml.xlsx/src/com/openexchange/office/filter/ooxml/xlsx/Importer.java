/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.xlsx4j.jaxb.Context;

import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.IPartImporter;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.session.Session;

/**
 * {@link Importer}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
@Service
@RegisteredService
public class Importer implements IImporter, IPartImporter, OsgiBundleContextAware {

	private OsgiBundleContextAndActivator bundleCtx;

    /**
     * Initializes a new {@link Importer}.
     */
    public Importer() {
        Context.getsmlObjectFactory();
    }

    @Override
    public JSONObject createOperations(Session session, InputStream inputDocumentStm, DocumentProperties documentProperties, boolean createFastLoadOperations) {
        boolean debugGetDefaultDocument = false;
        if(debugGetDefaultDocument) {   // just a simple helper method to store our in memory created
            try(XlsxOperationDocument emptyDoc = new XlsxOperationDocument(null, null, documentProperties)) {
            	bundleCtx.prepareObject(emptyDoc);
                documentProperties.put(DocumentProperties.PROP_INITIAL_SHEETNAME, "empty_sheet");
                emptyDoc.createPackage();
                emptyDoc.writeFile("/tmp/default.oxt");    // template documents...
            }
            catch(Throwable e) {
                OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
            }
        }

        JSONObject operations = null;
        try(XlsxOperationDocument operationDocument = new XlsxOperationDocument(session, null, documentProperties)) {
        	bundleCtx.prepareObject(operationDocument);
            operationDocument.loadDocument(inputDocumentStm, true);
            operationDocument.setCreateFastLoadOperations(createFastLoadOperations);
            operations = operationDocument.getOperations();
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
        return operations;
    }

    @Override
    public InputStream getDefaultDocument(InputStream templateDocument, DocumentProperties documentProperties) {
        InputStream defaultDocument = null;
        try(XlsxOperationDocument operationDocument = new XlsxOperationDocument(null, null, documentProperties)) {
        	bundleCtx.prepareObject(operationDocument);
            if(templateDocument!=null) {
                operationDocument.loadDocument(templateDocument, false);
            }
            else {
                operationDocument.createPackage();
            }
            operationDocument.applyDefaultDocumentProperties();
            defaultDocument = operationDocument.save();
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
        finally {
            IOUtils.closeQuietly(templateDocument);
        }
        return defaultDocument;
    }

    @Override
    public void initPartitioning(Session session, InputStream inputDocument, DocumentProperties documentProperties, boolean createFastLoadOperations) {
        try(XlsxOperationDocument operationDocument = new XlsxOperationDocument(session, null, documentProperties)) {
        	bundleCtx.prepareObject(operationDocument);
            operationDocument.loadDocument(inputDocument, true);
            operationDocument.setCreateFastLoadOperations(createFastLoadOperations);
            documentProperties.put(DocumentProperties.PROP_DOCUMENT, operationDocument);
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
    }

    @Override
    public Map<String, Object> getMetaData(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
        final XlsxOperationDocument operationDocument = (XlsxOperationDocument)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
        try {
            operationDocument.registerMemoryListener();
            metaData = operationDocument.getMetaData();
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, operationDocument.getPackage());
        }
        finally {
            operationDocument.close();
        }
        return metaData;
    }

    @Override
    public Map<String, Object> getActivePart(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
        final XlsxOperationDocument operationDocument = (XlsxOperationDocument)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
        try {
            operationDocument.registerMemoryListener();
            metaData = operationDocument.getActivePart();
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, operationDocument.getPackage());
        }
        finally {
            operationDocument.close();
        }
        return metaData;
    }

    @Override
    public Map<String, Object> getNextPart(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
        final XlsxOperationDocument operationDocument = (XlsxOperationDocument)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
        try {
            operationDocument.registerMemoryListener();
            metaData = operationDocument.getNextPart();
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, operationDocument.getPackage());
        }
        finally {
            operationDocument.close();
        }
        return metaData;
    }

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
