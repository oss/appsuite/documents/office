/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.List;
import java.util.Map;
import org.docx4j.dml.CTBlipFillProperties;
import org.docx4j.dml.CTGroupShapeProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualPictureProperties;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.dml.CTPresetGeometry2D;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTAbsoluteAnchor;
import org.docx4j.dml.spreadsheetDrawing.CTConnector;
import org.docx4j.dml.spreadsheetDrawing.CTConnectorNonVisual;
import org.docx4j.dml.spreadsheetDrawing.CTGraphicalObjectFrame;
import org.docx4j.dml.spreadsheetDrawing.CTGroupShape;
import org.docx4j.dml.spreadsheetDrawing.CTGroupShapeNonVisual;
import org.docx4j.dml.spreadsheetDrawing.CTMarker;
import org.docx4j.dml.spreadsheetDrawing.CTOneCellAnchor;
import org.docx4j.dml.spreadsheetDrawing.CTPicture;
import org.docx4j.dml.spreadsheetDrawing.CTShape;
import org.docx4j.dml.spreadsheetDrawing.CTShapeNonVisual;
import org.docx4j.dml.spreadsheetDrawing.CTTwoCellAnchor;
import org.docx4j.dml.spreadsheetDrawing.ICellAnchor;
import org.docx4j.dml.spreadsheetDrawing.ObjectFactory;
import org.docx4j.dml.spreadsheetDrawing.STEditAs;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.parts.Part;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.ChartWrapper;
import com.openexchange.office.filter.ooxml.drawingml.DMLGraphic;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.imagemgr.DocumentImageHelper;

public class Drawings {

	public static void createAnchorOperations(ICellAnchor anchor, JSONObject attrs)
	    throws JSONException {

	    JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
	    if(drawingAttrs==null) {
	        drawingAttrs = new JSONObject();
	        attrs.put(OCKey.DRAWING.value(), drawingAttrs);
	    }
        if(anchor instanceof CTAbsoluteAnchor) {
            final CTPoint2D position = ((CTAbsoluteAnchor)anchor).getPos(true);
            drawingAttrs.put(OCKey.LEFT.value(), Commons.coordinateTo100TH_MM(position.getX()));
            drawingAttrs.put(OCKey.TOP.value(), Commons.coordinateTo100TH_MM(position.getY()));
            final CTPositiveSize2D size = ((CTAbsoluteAnchor)anchor).getExt(true);
            drawingAttrs.put(OCKey.WIDTH.value(), Commons.coordinateTo100TH_MM(size.getCx()));
            drawingAttrs.put(OCKey.HEIGHT.value(), Commons.coordinateTo100TH_MM(size.getCy()));
            drawingAttrs.put(OCKey.ANCHOR.value(), "a");
        }
        else {
            final StringBuilder builder = new StringBuilder();
            if(anchor instanceof CTTwoCellAnchor) {
                final CTMarker from = ((CTTwoCellAnchor) anchor).getFrom(true);
                final CellRef startRef = new CellRef(from.getCol(), from.getRow());
                final long startColumnOffset = Commons.coordinateTo100TH_MM(from.getColOff());
                final long startRowOffset = Commons.coordinateTo100TH_MM(from.getRowOff());
                final CTMarker to = ((CTTwoCellAnchor) anchor).getTo(true);
                final CellRef endRef = new CellRef(to.getCol(), to.getRow());
                final long endColumnOffset = Commons.coordinateTo100TH_MM(to.getColOff());
                final long endRowOffset = Commons.coordinateTo100TH_MM(to.getRowOff());

                char aType = '2';
                if(((CTTwoCellAnchor) anchor).getEditAs()==STEditAs.ABSOLUTE) {
                    aType = 'A';
                }
                else if (((CTTwoCellAnchor) anchor).getEditAs()==STEditAs.ONE_CELL) {
                    aType = 'O';
                }
                builder.append(aType)
                       .append(' ')
                       .append(CellRef.getCellRef(startRef))
                       .append(' ')
                       .append(startColumnOffset)
                       .append(' ')
                       .append(startRowOffset)
                       .append(' ')
                       .append(CellRef.getCellRef(endRef))
                       .append(' ')
                       .append(endColumnOffset)
                       .append(' ')
                       .append(endRowOffset);
            }
            else if(anchor instanceof CTOneCellAnchor) {
                final CTMarker from = ((CTOneCellAnchor) anchor).getFrom(true);
                final CellRef startRef = new CellRef(from.getCol(), from.getRow());
                final long startColumnOffset = Commons.coordinateTo100TH_MM(from.getColOff());
                final long startRowOffset = Commons.coordinateTo100TH_MM(from.getRowOff());
                builder.append('1')
                        .append(' ')
                        .append(CellRef.getCellRef(startRef))
                        .append(' ')
                        .append(startColumnOffset)
                        .append(' ')
                        .append(startRowOffset);

                final CTPositiveSize2D size = ((CTOneCellAnchor)anchor).getExt(true);
                drawingAttrs.put(OCKey.WIDTH.value(), Commons.coordinateTo100TH_MM(size.getCx()));
                drawingAttrs.put(OCKey.HEIGHT.value(), Commons.coordinateTo100TH_MM(size.getCy()));
            }
            drawingAttrs.put(OCKey.ANCHOR.value(), builder.toString());
        }
	}

    public static void insertDrawingOperation(JSONArray operationsArray, List<Integer> drawingPosition, String drawingType, JSONObject attrs)
        throws JSONException {

        final JSONObject insertDrawingOperation = new JSONObject(4);
        insertDrawingOperation.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        insertDrawingOperation.put(OCKey.START.value(), drawingPosition);
        insertDrawingOperation.put(OCKey.TYPE.value(), drawingType);
        insertDrawingOperation.put(OCKey.ATTRS.value(), attrs);
        operationsArray.put(insertDrawingOperation);
    }

    public static AnchorBase applyAnchorAttributes(int drawingIndex, org.docx4j.openpackaging.parts.DrawingML.Drawing drawingsPart, JSONObject attrs)
        throws Exception {

        final org.docx4j.dml.spreadsheetDrawing.CTDrawing drawings = drawingsPart.getJaxbElement();
        final DLList<AnchorBase> anchors = drawings.getContent();
        if (drawingIndex>=anchors.size()) {
            throw new RuntimeException("xlsx::applyDrawingOperation: drawing not available");
        }
        final JSONObject drawingProperties = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingProperties==null) {
            return anchors.get(drawingIndex);
        }
        final AnchorBase cellAnchor = setDrawingAnchor(drawingProperties, anchors.get(drawingIndex));
        anchors.set(drawingIndex, cellAnchor);
        return cellAnchor;
    }

    public static void applyDrawingAttributes(OfficeOpenXMLOperationDocument operationDocument, ICellAnchor anchor, Part part, JSONObject attrs)
    	throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if(anchor.getPic()!=null) {
            DMLHelper.applyNonVisualDrawingProperties(anchor.getPic(), attrs.optJSONObject(OCKey.DRAWING.value()));
            DMLHelper.applyNonVisualDrawingShapeProperties(anchor.getPic(), attrs.optJSONObject(OCKey.DRAWING.value()));
    	    anchor.getPic().setSpPr(DMLHelper.applyShapePropertiesFromJson(anchor.getPic().getSpPr(), attrs, operationDocument, part, true));
        }
        else if(anchor.getGraphicFrame()!=null) {
            DMLHelper.applyNonVisualDrawingProperties(anchor.getGraphicFrame(), attrs.optJSONObject(OCKey.DRAWING.value()));
        	DMLHelper.applyNonVisualDrawingShapeProperties(anchor.getGraphicFrame(), attrs.optJSONObject(OCKey.DRAWING.value()));
        } else if (anchor.getAlternateContent() != null) {
            // System.out.println("Drawings.applyDrawingAttributes() " + anchor.getAlternateContent());
        }
    }

    public static void insertDrawing(XlsxOperationDocument operationDocument, int drawingIndex, org.docx4j.openpackaging.parts.DrawingML.Drawing drawingPart, String drawingType, JSONObject attrs)
        throws Exception {

        AnchorBase anchor = Context.getDmlSpreadsheetDrawingObjectFactory().createCTTwoCellAnchor();
        anchor.setClientData(Context.getDmlSpreadsheetDrawingObjectFactory().createCTAnchorClientData());
        drawingPart.getJaxbElement().getContent().add(drawingIndex, anchor);

        anchor = applyAnchorAttributes(drawingIndex, drawingPart, attrs);

        // we have to create a valid drawing otherwise excel wont open... at least we are creating an default empty rect shape
        final JSONObject imageAttrs = attrs.optJSONObject(OCKey.IMAGE.value());
        if(drawingType.equals("image")) {
            Object imageUrl = null;
            if(imageAttrs!=null) {
                imageUrl = DocumentImageHelper.getImageUrlFromImageData(operationDocument.getResourceManager(), imageAttrs.optString(OCKey.IMAGE_DATA.value()), "xl/media/");
                if(imageUrl==null) {
                    imageUrl = imageAttrs.opt(OCKey.IMAGE_URL.value());
                }
            }
            if(imageUrl instanceof String) {
                anchor.setPic(createImage(operationDocument, drawingPart, (String)imageUrl));
            }
            else {
                anchor.setSp(createDummyShape());
            }
        }
        else if(drawingType.equals("chart")) {
            anchor.setGraphicFrame(createChart(operationDocument, drawingPart, attrs.optJSONObject(OCKey.CHART.value())));
        }
        else {
            anchor.setSp(createDummyShape());
        }
        applyDrawingAttributes(operationDocument, anchor, drawingPart, attrs);
    }

    public static CTGraphicalObjectFrame createChart(XlsxOperationDocument operationDocument, Part drawingPart, JSONObject chartAttrs)
        throws Exception {

        final CTGraphicalObjectFrame graphicalObjectFrame = Context.getDmlSpreadsheetDrawingObjectFactory().createCTGraphicalObjectFrame();
        graphicalObjectFrame.getNonVisualDrawingShapeProperties(true);
        final CTNonVisualDrawingProps nonVisualDrawingProperties = graphicalObjectFrame.getNonVisualDrawingProperties(true);
        nonVisualDrawingProperties.setName("chart");
        DMLHelper.createTransform2D(graphicalObjectFrame.getXfrm(true), 0, 0, 500, 500);
        graphicalObjectFrame.setGraphic(DMLGraphic.createDrawingMLChart(operationDocument, drawingPart, "/xl/charts/", chartAttrs));
        return graphicalObjectFrame;
    }

    public static CTPicture createImage(XlsxOperationDocument operationDocument, Part drawingPart, String imageUrl)
        throws Exception {

        final CTPicture picture = Context.getDmlSpreadsheetDrawingObjectFactory().createCTPicture();

        // general shape properties
        final CTShapeProperties shapeProperties = Context.getDmlObjectFactory().createCTShapeProperties();
        picture.setSpPr(shapeProperties);
        DMLHelper.createTransform2D(picture.getXfrm(true), 0, 0, 500, 500);
        shapeProperties.setPrstGeom(DMLHelper.createPresetGeometry2D(STShapeType.RECT));
        picture.getNonVisualDrawingShapeProperties(true);
        final CTNonVisualDrawingProps nonVisualDrawingProperties = picture.getNonVisualDrawingProperties(true);
        nonVisualDrawingProperties.setName("image");

        // picture
        final CTBlipFillProperties blipFillProperties = Context.getDmlObjectFactory().createCTBlipFillProperties();
        blipFillProperties.setBlip(DMLHelper.createCTBlip(operationDocument, drawingPart, imageUrl));
        blipFillProperties.setStretch(Context.getDmlObjectFactory().createCTStretchInfoProperties());
        picture.setBlipFill(blipFillProperties);
        return picture;
    }

    private static CTShape createDummyShape() {

        final CTShape shape = Context.getDmlSpreadsheetDrawingObjectFactory().createCTShape();

        // general shape Properties
        final CTShapeProperties shapeProperties = Context.getDmlObjectFactory().createCTShapeProperties();
        shape.setSpPr(shapeProperties);
        DMLHelper.createTransform2D(shape.getXfrm(true), 0, 0, 500, 500);
        shape.setNvSpPr(createShapeNonVisual("shape"));

        // shape properties (preset)
        shapeProperties.setPrstGeom(DMLHelper.createPresetGeometry2D(STShapeType.RECT));
        return shape;
    }

    private static CTShapeNonVisual createShapeNonVisual(String name) {
        final CTShapeNonVisual shapeNonVisual = Context.getDmlSpreadsheetDrawingObjectFactory().createCTShapeNonVisual();
        shapeNonVisual.setCNvSpPr(Context.getDmlObjectFactory().createCTNonVisualDrawingShapeProps());
        final CTNonVisualDrawingProps nonVisualDrawingProps = Context.getDmlObjectFactory().createCTNonVisualDrawingProps();
        nonVisualDrawingProps.setName(name);
        shapeNonVisual.setCNvPr(nonVisualDrawingProps);
        return shapeNonVisual;
    }

    private static CTGroupShapeNonVisual createGrpShapeNonVisual(String name) {
        final CTGroupShapeNonVisual shapeNonVisual = Context.getDmlSpreadsheetDrawingObjectFactory().createCTGroupShapeNonVisual();
        shapeNonVisual.setCNvGrpSpPr(Context.getDmlObjectFactory().createCTNonVisualGroupDrawingShapeProps());
        final CTNonVisualDrawingProps nonVisualDrawingProps = Context.getDmlObjectFactory().createCTNonVisualDrawingProps();
        nonVisualDrawingProps.setName(name);
        shapeNonVisual.setCNvPr(nonVisualDrawingProps);
        return shapeNonVisual;
    }

    private static CTConnectorNonVisual createConnectorShapeNonVisual(String name) {
        final CTConnectorNonVisual nvSpPr = Context.getDmlSpreadsheetDrawingObjectFactory().createCTConnectorNonVisual();
        nvSpPr.setCNvCxnSpPr(Context.getDmlObjectFactory().createCTNonVisualConnectorProperties());
        final CTNonVisualDrawingProps nonVisualDrawingProps = Context.getDmlObjectFactory().createCTNonVisualDrawingProps();
        nonVisualDrawingProps.setName(name);
        nvSpPr.setCNvPr(nonVisualDrawingProps);
        return nvSpPr;
    }

    public static AnchorBase setDrawingAnchor(JSONObject drawingProperties, AnchorBase sourceAnchor) {

        final String anchor = drawingProperties.optString(OCKey.ANCHOR.value(), null);
        AnchorBase cellAnchor = sourceAnchor;

        if(anchor!=null) {
            final char anchorType = anchor.charAt(0);
            final String[] anchorAttrs = anchor.substring(2).split(" ", -1);

            if(anchorType == '2' || anchorType == 'O' || anchorType == 'A') {
                final CellRef startRef = CellRef.createCellRef(anchorAttrs[0]);
                final long startColumnOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[1]));
                final long startRowOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[2]));
                final CellRef endRef = CellRef.createCellRef(anchorAttrs[3]);
                final long endColumnOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[4]));
                final long endRowOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[5]));

                cellAnchor = new CTTwoCellAnchor(new CTMarker(startRef.getColumn(), startColumnOffset, startRef.getRow(), startRowOffset),
                    new CTMarker(endRef.getColumn(), endColumnOffset, endRef.getRow(), endRowOffset), sourceAnchor);

                if(anchorType == '2') {
                    ((CTTwoCellAnchor)cellAnchor).setEditAs(org.docx4j.dml.spreadsheetDrawing.STEditAs.TWO_CELL);
                }
                else if(anchorType == 'O') {
                    ((CTTwoCellAnchor)cellAnchor).setEditAs(org.docx4j.dml.spreadsheetDrawing.STEditAs.ONE_CELL);
                }
                else if(anchorType == 'A') {
                    ((CTTwoCellAnchor)cellAnchor).setEditAs(org.docx4j.dml.spreadsheetDrawing.STEditAs.ABSOLUTE);
                }
            }
            else if(anchorType == '1') {
                final CellRef startRef = CellRef.createCellRef(anchorAttrs[0]);
                final long startColumnOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[1]));
                final long startRowOffset = Commons.coordinateFrom100TH_MM(Integer.parseInt(anchorAttrs[2]));
                final long width = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.WIDTH.value(), 0));
                final long height = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.HEIGHT.value(), 0));

                cellAnchor = new CTOneCellAnchor(new CTMarker(startRef.getColumn(), startColumnOffset, startRef.getRow(), startRowOffset), new CTPositiveSize2D(width, height), sourceAnchor);
            }
            else if(anchorType == 'a') {
                final long left = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.LEFT.value(), 0));
                final long top = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.TOP.value(), 0));
                final long width = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.WIDTH.value(), 0));
                final long height = Commons.coordinateFrom100TH_MM(drawingProperties.optInt(OCKey.HEIGHT.value(), 0));

                cellAnchor = new CTAbsoluteAnchor(new CTPoint2D(left, top), new CTPositiveSize2D(width, height), sourceAnchor);
            }
        }
        return cellAnchor;
    }

    public static ChartWrapper getChart(Map<String, Object> drawingPropertyMap){
        return (ChartWrapper) drawingPropertyMap.get(OCKey.CHART_PART.value());
    }

    public static CTShape createShape() {
        return createDummyShape();
    }

    public static CTGroupShape createGroupShape() {
        final CTGroupShape shape = Context.getDmlSpreadsheetDrawingObjectFactory().createCTGroupShape();

        // general shape Properties
        final CTGroupShapeProperties shapeProperties = Context.getDmlObjectFactory().createCTGroupShapeProperties();
        shape.setGrpSpPr(shapeProperties);
        DMLHelper.createTransform2D(shape.getXfrm(true), 0, 0, 500, 500);
        shape.setNvGrpSpPr(createGrpShapeNonVisual("shape"));
        return shape;
    }

    public static CTConnector createConnectorShape() {
        final ObjectFactory pmlObjectFactory = Context.getDmlSpreadsheetDrawingObjectFactory();
        final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();
        final CTConnector cxnSp = pmlObjectFactory.createCTConnector();
        cxnSp.setNvCxnSpPr(createConnectorShapeNonVisual("connector"));
        cxnSp.setSpPr(dmlObjectFactory.createCTShapeProperties());
        DMLHelper.createTransform2D(cxnSp.getSpPr().getXfrm(true), 0, 0, 500, 500);
        return cxnSp;
    }

    public static CTPicture createImage() {
        final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();

        final CTPicture pic = Context.getDmlSpreadsheetDrawingObjectFactory().createCTPicture();
        final CTNonVisualDrawingProps nvPr = pic.getNonVisualDrawingProperties(true);
        final CTNonVisualPictureProperties nvPicPr = pic.getNonVisualDrawingShapeProperties(true);
        nvPicPr.getLocks(true).setNoChangeAspect(Boolean.TRUE);

        final CTShapeProperties spPr = dmlObjectFactory.createCTShapeProperties();
        final CTPresetGeometry2D presetGeometry = dmlObjectFactory.createCTPresetGeometry2D();
        presetGeometry.setPrst(STShapeType.RECT);
        presetGeometry.setAvLst(dmlObjectFactory.createCTGeomGuideList());
        spPr.setPrstGeom(presetGeometry);
        pic.setSpPr(spPr);
        final CTBlipFillProperties blipFillProperties = pic.getBlipFill(true);
        blipFillProperties.getStretch(true).setFillRect(dmlObjectFactory.createCTRelativeRect());
        return pic;
    }

}
