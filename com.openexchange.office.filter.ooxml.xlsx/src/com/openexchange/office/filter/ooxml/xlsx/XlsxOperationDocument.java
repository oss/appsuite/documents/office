/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import org.apache.commons.lang3.mutable.MutableInt;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.SpreadsheetML.MetadataPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.PersonsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.SharedStrings;
import org.docx4j.openpackaging.parts.SpreadsheetML.Styles;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTDataValidations;
import org.xlsx4j.sml.CTExtension;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTFontSize;
import org.xlsx4j.sml.CTFonts;
import org.xlsx4j.sml.CTMetadata;
import org.xlsx4j.sml.CTRElt;
import org.xlsx4j.sml.CTRst;
import org.xlsx4j.sml.CTSheetFormatPr;
import org.xlsx4j.sml.CTSst;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTXstringWhitespace;
import org.xlsx4j.sml.Cell;
import org.xlsx4j.sml.IDataValidations;
import org.xlsx4j.sml.Row;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.SheetData;
import org.xlsx4j.sml.SheetView;
import org.xlsx4j.sml.SheetViews;
import org.xlsx4j.sml.Sheets;
import org.xlsx4j.sml.Workbook;
import org.xlsx4j.sml.Worksheet;
import org.xlsx4j.sml_2018.CTPersonList;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DocumentComplexity;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.IntervalArray;
import com.openexchange.office.filter.core.spreadsheet.SUtil;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.RootComponent;
import com.openexchange.office.filter.ooxml.xlsx.operations.XlsxApplyOperationHelper;
import com.openexchange.office.filter.ooxml.xlsx.operations.XlsxCreateOperationHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.AutoFilterHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.ConditionalFormattings;
import com.openexchange.office.filter.ooxml.xlsx.tools.SheetUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.TableHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.ValidationUtils;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.session.Session;

public class XlsxOperationDocument extends com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument {

    final private long FACTOR_FOR_REQUIRED_HEAPSPACE = 32;
    private SpreadsheetMLPackage opcPackage;
    protected CTStylesheet stylesheet = null;
    protected CTSst sharedStrings = null;
    private XlsxApplyOperationHelper applyOperationHelper = null;
    private XlsxCreateOperationHelper _createOperationHelper = null;
    private Double defaultFontSize = null;
    private ThemePart themePart = null;
    private CTPersonList personList;

	private final static Logger log = LoggerFactory.getLogger(XlsxOperationDocument.class);

    public XlsxOperationDocument(Session session, IResourceManager _resourceManager, DocumentProperties _documentProperties)
        throws FilterException {

        super(session,_resourceManager, _documentProperties);
    }

    @Override
    protected ByteArrayInputStream _loadDocument(InputStream _inputStream, boolean XMLReadOnly)
        throws Docx4JException {

    	final long maxAllowedXmlSizeMatchingComplexity = getIntegerOfficeConfigurationValue("//spreadsheet/maxCells", 500000) * 100;
    	final long maxAllowedXmlSizeMatchingMemory = MemoryObserver.calcMaxHeapSize(50) / FACTOR_FOR_REQUIRED_HEAPSPACE;
    	final ByteArrayInputStream documentInputStream = DocumentComplexity.checkDocumentComplexity(_inputStream, maxAllowedXmlSizeMatchingComplexity, maxAllowedXmlSizeMatchingMemory);
        opcPackage = (SpreadsheetMLPackage)OpcPackage.load(documentInputStream, XMLReadOnly);
		final PartName themePartName = new PartName("/xl/theme/theme1.xml");
		themePart = (ThemePart)getPackage().getParts().get(themePartName);

        // styles
        final Styles styles = getPackage().getWorkbookPart().getStyles();
        if (styles != null) {
            stylesheet = styles.getJaxbElement();
            // apply the default number formats passed in the document properties
            stylesheet.insertNumberFormats(getInitialFormats());
        }

        // shared strings
        final SharedStrings _sharedStrings = getPackage().getWorkbookPart().getSharedStrings();
        if (_sharedStrings != null) {
            sharedStrings = _sharedStrings.getJaxbElement();
        }

        // persons
        final PersonsPart persons = getPackage().getWorkbookPart().getPersons();
        if (persons != null) {
            personList = persons.getJaxbElement();
        }

        final String initialSheetName = getInitialSheetName();
        final Workbook workbook = getPackage().getWorkbookPart().getJaxbElement();
        final Sheet sheet = workbook.getSheets().getContent().get(0);
        if(sheet.getName().equals("empty_sheet")) {
            sheet.setName(initialSheetName);
        }
        return documentInputStream;
    }

    @Override
    protected void _createPackage()
        throws Exception  {

        opcPackage = SpreadsheetMLPackage.createPackage();
        themePart = getDefaultThemePart(new PartName("/xl/theme/theme1.xml"));
        getPackage().getWorkbookPart().addTargetPart(themePart);
        getPackage().createWorksheetPart(new PartName("/xl/worksheets/sheet1.xml"), getInitialSheetName(), 1);

        // applying the default column width...
        final Sheet sheet = SheetUtils.getSheet(opcPackage, 0);
        final RelationshipsPart relationshipPart = opcPackage.getWorkbookPart().getRelationshipsPart();
        final WorksheetPart worksheetPart = (WorksheetPart)relationshipPart.getPart(sheet.getId());
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        if(sheetFormatPr==null) {
            sheetFormatPr = Context.getsmlObjectFactory().createCTSheetFormatPr();
            worksheet.setSheetFormatPr(sheetFormatPr);
            sheetFormatPr.setBaseColWidth(new Long(10L));
            sheetFormatPr.setDefaultRowHeight(14.5);
        }
        getStylesheet(true);
    }

    @Override
    public void _applyDefaultDocumentProperties() {
        final DocumentProperties documentProperties = getDocumentProperties();
        if(documentProperties==null) {
            return;
        }
    }

    @Override
    public JSONObject _getOperations()
        throws Exception {

        final XlsxCreateOperationHelper createOperationHelper = getCreateOperationHelper();
        createOperationHelper.createOperations();

/*
        Writer writer = new FileWriter(new File("/tmp/jsonout.txt"));
        for(int i=0;i<createOperationHelper.getOperationsArray().length();i++) {
            createOperationHelper.getOperationsArray().getJSONObject(i).write(writer);
            writer.append(System.getProperty("line.separator"));
        }
        writer.close();
*/
        final JSONObject aOperations = new JSONObject();
        aOperations.put("operations", createOperationHelper.getOperationsArray());
        return aOperations;
    }

    @Override
    public Map<String, Object> _getMetaData()
        throws Exception {

        final XlsxCreateOperationHelper createOperationHelper = getCreateOperationHelper();
        createOperationHelper.createMetaOperations(getDocumentProperties());
        final HashMap<String, Object> aOperations = new HashMap<String, Object>(1);
        aOperations.put("operations", createOperationHelper.getOperationsArray());
        return aOperations;
    }

    @Override
    public Map<String, Object> _getActivePart()
        throws Exception {

        final XlsxCreateOperationHelper createOperationHelper = getCreateOperationHelper();
        createOperationHelper.createActivePartOperations(getDocumentProperties());
        final HashMap<String, Object> aOperations = new HashMap<String, Object>(1);
        aOperations.put("operations", createOperationHelper.getOperationsArray());
        return aOperations;
    }

    @Override
    public Map<String, Object> _getNextPart()
        throws Exception {

        final XlsxCreateOperationHelper createOperationHelper = getCreateOperationHelper();
        if(!createOperationHelper.createNextPartOperations(getDocumentProperties())) {
            return null;
        }
        final HashMap<String, Object> aOperations = new HashMap<String, Object>(1);
        aOperations.put("operations", createOperationHelper.getOperationsArray());
        return aOperations;
    }

	/**
	 * Returns the value of the document property "InitialSheetName".
	 *
	 * @return
	 * 	The value of the document property "InitialSheetName" (falls back to "Sheet1").
	 */
	private String getInitialSheetName() {
        return getDocumentProperties().optString(DocumentProperties.PROP_INITIAL_SHEETNAME, "Sheet1");
	}

	/**
	 * Returns the value of the document property "InitialFormats".
	 *
	 * @return
	 * 	The value of the document property "InitialFormats".
	 */
	private Map<Long, String> getInitialFormats() {

		Map<Long, String> formatsMap = new HashMap<Long, String>();

		final String formatsStr = getDocumentProperties().optString(DocumentProperties.PROP_INITIAL_FORMATS, null);
        if (formatsStr != null && !formatsStr.isEmpty()) {
        	try {
        		final JSONObject formatsObj = new JSONObject(formatsStr);
        		for(Iterator<String> iterator = formatsObj.keySet().iterator(); iterator.hasNext(); ) {
        		    String key = iterator.next();
        		    long formatId = Long.parseLong(key, 10);
        		    String formatCode = formatsObj.getString(key);
        		    formatsMap.put(formatId, formatCode);
        		}
        	} catch (Exception e) {
        	    //
        	}
        }

        return formatsMap;
	}

    @Override
	public Part getContextPart() {
    	return contextPart!=null ? contextPart : getPackage().getWorkbookPart();
    }

    @Override
    public String getImagePath() {
    	return "xl/media/";
    }

    public CTStylesheet getStylesheet(boolean createIfMissing) {

        if(stylesheet==null&&createIfMissing) {
            try {
                final Styles styles = new Styles(new PartName("/xl/styles.xml"));
                stylesheet = createStylesheet(styles.getPackage());
                styles.setJaxbElement(stylesheet);
                getPackage().getWorkbookPart().addTargetPart(styles);
            }
            catch(final Exception e) {
                stylesheet = null;
            }
        }
        return stylesheet;
    }

    public CTSst getSharedStrings(boolean createIfMissing)
        throws Exception {

        if(sharedStrings==null&&createIfMissing) {
            final SharedStrings _sharedStrings = new SharedStrings(new PartName("/xl/sharedStrings.xml"));
            sharedStrings = Context.getsmlObjectFactory().createCTSst();
            _sharedStrings.setJaxbElement(sharedStrings);
            getPackage().getWorkbookPart().addTargetPart(_sharedStrings);
        }
        return sharedStrings;
    }

    public CTPersonList getPersons(boolean createIfMissing) {
        if (personList == null && createIfMissing) {
            try {
                final PersonsPart persons = new PersonsPart();
                personList = Context.sml2018ObjectFactory().createCTPersonList();
                getPackage().getWorkbookPart().addTargetPart(persons);
                persons.setJaxbElement(personList);
            } catch (InvalidFormatException e) {
                personList = null;
            }
        }

        return personList;
    }

    public CTMetadata getMetadata(boolean createIfMissing) {
        MetadataPart metadataPart = (MetadataPart)getPackage().getWorkbookPart().getRelationshipsPart(true).getPartByType(Namespaces.SPREADSHEET_METADATA);
        if(metadataPart == null && createIfMissing) {
            try {
                metadataPart = new MetadataPart();
                metadataPart.setJaxbElement(new CTMetadata());
                getPackage().getWorkbookPart().addTargetPart(metadataPart);
            }
            catch(InvalidFormatException e) {
                //
            }
        }
        return metadataPart!=null ? metadataPart.getJaxbElement() : null;
    }

    // returns null if something goes wrong ...
    public String getSharedString(String index) {

        if(sharedStrings!=null) {
            final List<CTRst> si = sharedStrings.getSi();
            try  {
                final int i = Integer.parseInt(index);
                if(i>=0&&i<si.size()) {
                    final CTRst ctRst = si.get(i);
                    final CTXstringWhitespace ctxString = ctRst.getT();
                    if(ctxString!=null) {
                        return ctxString.getValue();
                    }
                    final List<CTRElt> ctREltList = ctRst.getR();
                    if(ctREltList!=null) {
                        final StringBuffer stringBuffer = new StringBuffer();
                        for(final CTRElt ctRElt:ctREltList) {
                            final String t = ctRElt.getT().getValue();
                            if(t!=null) {
                                stringBuffer.append(t);
                            }
                        }
                        return stringBuffer.toString();
                    }
                }
            }
            catch(final NumberFormatException e) {
                // TODO: what to do now ?
            }
        }
        return null;
    }

    public String applySharedString(String v)
    	throws InvalidFormatException {

    	if(sharedStrings==null) {
    		final SharedStrings sharedStringsPart = new SharedStrings();
    		sharedStrings = Context.getsmlObjectFactory().createCTSst();
    		sharedStringsPart.setJaxbElement(sharedStrings);
            getPackage().getWorkbookPart().addTargetPart(sharedStringsPart);
    	}
    	int i;
        final List<CTRst> si = sharedStrings.getSi();
        for(i=0;i<si.size();i++) {
        	final CTRst crst = si.get(i);
        	if(crst.getT()!=null&&crst.getT().getValue()!=null&&crst.getT().getValue().equals(v)) {
        		return String.valueOf(i);
        	}
        }
        final CTRst rst = Context.getsmlObjectFactory().createCTRst();
        final CTXstringWhitespace n = Context.getsmlObjectFactory().createCTXstringWhitespace();
        n.setValue(v);
        if(v.charAt(0)==' '||v.charAt(v.length()-1)==' ') {
            n.setSpace("preserve");
        }
        rst.setT(n);
        si.add(rst);
        return String.valueOf(si.size()-1);
    }

    public static List<IDataValidations> getDataValidations(Worksheet worksheet) {
        final List<IDataValidations> dataValidations = new ArrayList<IDataValidations>();

        final CTDataValidations validations = worksheet.getDataValidations();
        if(validations!=null) {
            dataValidations.add(validations);
        }
        final CTExtensionList extLst = worksheet.getExtLst(false);
        if(extLst!=null) {
            for(final CTExtension extension:extLst.getExt()) {
                final Object any = extension.getAny();
                if(any instanceof JAXBElement<?>) {
                    if(extension.getUri().equals("{CCE6A557-97BC-4b89-ADB6-D9C93CAAB3DF}")) {
                        final Object value = ((JAXBElement<?>)any).getValue();
                        if(value instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations) {
                            dataValidations.add((org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations)value);
                        }
                    }
                }
            }
        }
        return dataValidations;
    }

    //
    public String escapeString(String s) {
        if(s==null||s.length()==0)
            return s;
        final char char0 = s.charAt(0);
        return ((char0=='=')||(char0=='#')) ? "'" + s : s;
    }

    public XlsxApplyOperationHelper getApplyOperationHelper() {
        return applyOperationHelper;
    }

    public XlsxCreateOperationHelper getCreateOperationHelper()
   		throws FilterException {

    	if(_createOperationHelper==null) {
    		_createOperationHelper = new XlsxCreateOperationHelper(this, new JSONArray());
    	}
    	_createOperationHelper.setOperationsArray(new JSONArray());
    	return _createOperationHelper;
    }

    @Override
    public void _applyOperations(String applyOperations) throws FilterException {
    	if (applyOperations != null){
            int i = 0;
            JSONObject op = null;
            try {
                applyOperationHelper = new XlsxApplyOperationHelper(this);
                final JSONArray aOperations = new JSONArray(new JSONTokener(applyOperations));
                for (i = 0; i < aOperations.length(); i++) {
                	opcPackage.setSuccessfulAppliedOperations(i);
                    op = (JSONObject) aOperations.get(i);
                    final OCValue opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
                    switch(opName) {
                        case INSERT_PARAGRAPH : {
                            applyOperationHelper.insertParagraph(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case SPLIT_PARAGRAPH : {
                            applyOperationHelper.splitParagraph(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case MERGE_PARAGRAPH : {
                            applyOperationHelper.mergeParagraph(op.getJSONArray(OCKey.START.value()));
                            break;
                        }
                        case INSERT_TEXT : {
                            applyOperationHelper.insertText(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_HARD_BREAK : {
                            applyOperationHelper.insertHardBreak(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_TAB : {
                            applyOperationHelper.insertTab(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case INSERT_FIELD : {
                            applyOperationHelper.insertField(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TYPE.value()), op.getString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case DELETE : {
                            CUtil.delete(getRootComponent(), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                            break;
                        }
                        case SET_ATTRIBUTES : {
                            applyOperationHelper.setAttributes(op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()), op.getJSONObject(OCKey.ATTRS.value()));
                            break;
                        }
                        case MOVE : {
                            applyOperationHelper.move(op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                            break;
                        }
                        case INSERT_NUMBER_FORMAT : {
	                        applyOperationHelper.insertNumberFormat(op.getLong(OCKey.ID.value()), op.getString(OCKey.CODE.value()));
                    		break;
                    	}
                    	case DELETE_NUMBER_FORMAT : {
	                        applyOperationHelper.deleteNumberFormat(op.getLong(OCKey.ID.value()));
                    		break;
                    	}
	                    case CHANGE_CELLS : {
	                        applyOperationHelper.changeCells(op.getInt(OCKey.SHEET.value()), op.getJSONObject(OCKey.CONTENTS.value()));
	                        break;
	                    }
	                    case CHANGE_ROWS : {
	                        applyOperationHelper.changeRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
	                        break;
	                    }
	                    case CHANGE_COLUMNS : {
	                        applyOperationHelper.changeColumns(op.getInt(OCKey.SHEET.value()),  IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
	                        break;
	                    }
	                    case INSERT_ROWS : {
	                        applyOperationHelper.insertRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
	                        break;
	                    }
	                    case DELETE_ROWS : {
	                        applyOperationHelper.deleteRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())));
	                        break;
	                    }
	                    case INSERT_COLUMNS : {
	                        applyOperationHelper.insertColumns(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
	                        break;
	                    }
	                    case DELETE_COLUMNS : {
	                        applyOperationHelper.deleteColumns(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())));
	                        break;
	                    }
	                    case INSERT_SHEET : {
	                        applyOperationHelper.insertSheet(op.getInt(OCKey.SHEET.value()), op.getString(OCKey.SHEET_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case MOVE_SHEET : {
	                        applyOperationHelper.moveSheet(op.getInt(OCKey.SHEET.value()), op.getInt(OCKey.TO.value()));
	                        break;
	                    }
                        case MOVE_SHEETS : {
                            SUtil.moveSheets(applyOperationHelper, op.getJSONArray(OCKey.SHEETS.value()));
                            break;
                        }
	                    case DELETE_SHEET : {
	                        applyOperationHelper.deleteSheet(op.getInt(OCKey.SHEET.value()));
	                        break;
	                    }
	                    case COPY_SHEET : {
	                    	applyOperationHelper.copySheet(op.getInt(OCKey.SHEET.value()), op.getInt(OCKey.TO.value()), op.getString(OCKey.SHEET_NAME.value()), op.optJSONObject(OCKey.TABLE_NAMES.value()));
	                    	break;
	                    }
	                    case CHANGE_SHEET : {
	                        applyOperationHelper.changeSheet(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.SHEET_NAME.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case MERGE_CELLS : {
	                        applyOperationHelper.mergeCells(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())), op.optBoolean(OCKey.KEEP_CONTENT.value(), false), op.optString(OCKey.TYPE.value(), "merge"));
	                        break;
	                    }
	                    case INSERT_DRAWING : {
	                        applyOperationHelper.insertDrawing(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case DELETE_DRAWING : {
	                        applyOperationHelper.deleteDrawing(op.getJSONArray(OCKey.START.value()));
	                        break;
	                    }
	                    case CHANGE_DRAWING : {
	                        applyOperationHelper.changeDrawing(op.getJSONArray(OCKey.START.value()), op.getJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
                        case MOVE_DRAWING : {
                            applyOperationHelper.move(op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                            break;
                        }
	                    case INSERT_STYLE_SHEET : {
	                        applyOperationHelper.insertStyleSheet(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optBoolean(OCKey.HIDDEN.value(), false), op.optInt(OCKey.UI_PRIORITY.value(), 0), op.optBoolean(OCKey.DEFAULT.value(), false));
	                        break;
	                    }
	                    case INSERT_AUTO_STYLE : {
	                        applyOperationHelper.insertAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optBoolean(OCKey.DEFAULT.value(), false));
	                        break;
	                    }
	                    case CHANGE_AUTO_STYLE : {
	                        applyOperationHelper.changeAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.getJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case DELETE_AUTO_STYLE : {
	                        applyOperationHelper.deleteAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()));
	                        break;
	                    }
	                    case INSERT_CHART_SERIES : {
	                        applyOperationHelper.insertChartSeries(op.getJSONArray(OCKey.START.value()), op.getInt(OCKey.SERIES.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case CHANGE_CHART_SERIES : {
	                        applyOperationHelper.changeChartSeries(op.getJSONArray(OCKey.START.value()), op.getInt(OCKey.SERIES.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case DELETE_CHART_SERIES : {
	                        applyOperationHelper.deleteChartSeries(op.getJSONArray(OCKey.START.value()), op.getInt(OCKey.SERIES.value()));
	                        break;
	                    }
	                    case INSERT_NAME : {
	                        applyOperationHelper.insertName(op.optLong(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()), op.getString(OCKey.FORMULA.value()), op.optBoolean(OCKey.HIDDEN.value(), false));
	                        break;
	                    }
	                    case CHANGE_NAME : {
	                        applyOperationHelper.changeName(op.optLong(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()), op.optString(OCKey.FORMULA.value(), null), op.optString(OCKey.NEW_LABEL.value(), null));
	                        break;
	                    }
	                    case DELETE_NAME : {
	                        applyOperationHelper.deleteName(op.optLong(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()));
	                        break;
	                    }
	                    case CHANGE_CHART_AXIS : {
	                        applyOperationHelper.changeChartAxis(op.getJSONArray(OCKey.START.value()), op.getLong(OCKey.AXIS.value()), op.getLong(OCKey.CROSS_AX.value()), op.getString(OCKey.AX_POS.value()), op.optBoolean(OCKey.Z_AXIS.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case CHANGE_CHART_GRID : {
	                        applyOperationHelper.changeChartGrid(op.getJSONArray(OCKey.START.value()), op.getLong(OCKey.AXIS.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case CHANGE_CHART_TITLE : {
	                        applyOperationHelper.changeChartTitle(op.getJSONArray(OCKey.START.value()), op.has(OCKey.AXIS.value()) ? op.getLong(OCKey.AXIS.value()) : null, op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case CHANGE_CHART_LEGEND : {
	                        applyOperationHelper.changeChartLegend(op.getJSONArray(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case INSERT_DV_RULE : {
	                    	ValidationUtils.insertValidation(this, op);
	                    	break;
	                    }
	                    case CHANGE_DV_RULE : {
	                    	ValidationUtils.changeValidation(this, op);
	                    	break;
	                    }
	                    case DELETE_DV_RULE : {
	                    	ValidationUtils.deleteValidation(this, op.getInt(OCKey.SHEET.value()), op.getInt(OCKey.INDEX.value()));
	                    	break;
	                    }
	                    case SET_DOCUMENT_ATTRIBUTES : {
	                    	applyOperationHelper.setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
	                    	break;
	                    }
	                    case INSERT_TABLE : {
	                    	final String tableName = op.optString(OCKey.TABLE.value(), null);
	                    	if(tableName==null||tableName.isEmpty()) {
	                    		AutoFilterHelper.changeOrInsertAutoFilter(applyOperationHelper, op.getInt(OCKey.SHEET.value()), op.getString(OCKey.RANGE.value()), op.optJSONObject(OCKey.ATTRS.value()));
	                    	}
	                    	else {
	                    		TableHelper.changeOrInsertTable(applyOperationHelper, op.getInt(OCKey.SHEET.value()), tableName, op.getString(OCKey.RANGE.value()), op.optJSONObject(OCKey.ATTRS.value()), true);
	                    	}
	                        break;
	                    }
	                    case CHANGE_TABLE : {
	                    	final String tableName = op.optString(OCKey.TABLE.value(), null);
	                    	if(tableName==null||tableName.isEmpty()) {
	                    		AutoFilterHelper.changeOrInsertAutoFilter(applyOperationHelper, op.getInt(OCKey.SHEET.value()), op.optString(OCKey.RANGE.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
	                    	}
	                    	else {
	                    		TableHelper.changeOrInsertTable(applyOperationHelper, op.getInt(OCKey.SHEET.value()), tableName, op.optString(OCKey.RANGE.value(), null), op.optJSONObject(OCKey.ATTRS.value()), false);
	                    	}
	                        break;
	                    }
	                    case DELETE_TABLE : {
	                    	final String tableName = op.optString(OCKey.TABLE.value(), null);
	                    	if(tableName==null||tableName.isEmpty()) {
	                    		AutoFilterHelper.deleteAutoFilter(applyOperationHelper, op.getInt(OCKey.SHEET.value()));
	                    	}
	                    	else {
	                    		TableHelper.deleteTable(applyOperationHelper, op.getInt(OCKey.SHEET.value()), tableName);
	                    	}
	                        break;
	                    }
	                    case CHANGE_TABLE_COLUMN : {
                    		TableHelper.changeTableColumn(applyOperationHelper, op.optBoolean(OCKey.SORT.value(), false), op.getInt(OCKey.SHEET.value()), op.optString(OCKey.TABLE.value(), null), op.getInt(OCKey.COL.value()), op.getJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    }
	                    case INSERT_CF_RULE : {
	                    	ConditionalFormattings.changeOrInsertCondFormatRule(this, true, op.getInt(OCKey.SHEET.value()), op.getString(OCKey.ID.value()), op.getString(OCKey.RANGES.value()), op.optString(OCKey.TYPE.value(), "formula"),
	                    		op.has(OCKey.VALUE1.value()) ? op.get(OCKey.VALUE1.value()) : "", op.optString(OCKey.VALUE2.value(), ""), op.optInt(OCKey.PRIORITY.value()), op.optBoolean(OCKey.STOP.value(), false), op);
	                    	break;
	                    }
	                    case CHANGE_CF_RULE : {
	                    	ConditionalFormattings.changeOrInsertCondFormatRule(this, false, op.getInt(OCKey.SHEET.value()), op.getString(OCKey.ID.value()), op.optString(OCKey.RANGES.value()), op.optString(OCKey.TYPE.value(), null),
	                    		op.opt(OCKey.VALUE1.value()), op.optString(OCKey.VALUE2.value(), null), op.optInt(OCKey.PRIORITY.value()), op.optBoolean(OCKey.STOP.value()), op);
	                    	break;
	                    }
	                    case DELETE_CF_RULE : {
	                    	ConditionalFormattings.deleteCondFormatRule(this, op.getInt(OCKey.SHEET.value()), op.getString(OCKey.ID.value()));
	                    	break;
	                    }
	                    case INSERT_HYPERLINK : {
	                    	applyOperationHelper.insertHyperlink(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())), op.getString(OCKey.URL.value()));
	                    	break;
	                    }
	                    case DELETE_HYPERLINK : {
	                    	applyOperationHelper.deleteHyperlink(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())));
	                    	break;
	                    }
	                    case INSERT_NOTE :
	                        applyOperationHelper.insertNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.optString(OCKey.TEXT.value(), ""), op.optString(OCKey.AUTHOR.value(), null), op.optString(OCKey.USER_ID.value(), null), op.optString(OCKey.DATE.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    case CHANGE_NOTE :
	                        applyOperationHelper.changeNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.optString(OCKey.TEXT.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    case DELETE_NOTE :
	                        applyOperationHelper.deleteNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())));
	                        break;
	                    case MOVE_NOTES :
	                        applyOperationHelper.moveNotes(op.getInt(OCKey.SHEET.value()), op.getString(OCKey.FROM.value()).split(" ", -1), op.getString(OCKey.TO.value()).split(" ", -1));
	                        break;
	                    case INSERT_COMMENT:
	                        applyOperationHelper.insertComment(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.optString(OCKey.TEXT.value(), ""), op.optJSONArray(OCKey.MENTIONS.value()), op.optString(OCKey.DATE.value(), null), op.optString(OCKey.AUTHOR.value()), op.optString(OCKey.AUTHOR_ID.value()), op.optString(OCKey.AUTHOR_PROVIDER.value()), op.has(OCKey.DONE.value()) ? op.getBoolean(OCKey.DONE.value()) : null, op.optJSONObject(OCKey.ATTRS.value()));
	                        break;
	                    case DELETE_COMMENT:
	                        applyOperationHelper.deleteComment(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.getInt(OCKey.INDEX.value()));
	                        break;
                        case CHANGE_COMMENT:
                            applyOperationHelper.changeComment(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.has(OCKey.INDEX.value()) ? op.getInt(OCKey.INDEX.value()) : null, op.optString(OCKey.TEXT.value(), null), op.optJSONArray(OCKey.MENTIONS.value()), op.has(OCKey.DONE.value()) ? op.getBoolean(OCKey.DONE.value()) : null, op.optJSONObject(OCKey.ATTRS.value()));
                            break;
                        case MOVE_COMMENTS:
                            applyOperationHelper.moveComments(op.getInt(OCKey.SHEET.value()), op.getString(OCKey.FROM.value()).split(" ", -1), op.getString(OCKey.TO.value()).split(" ", -1));
                            break;
	                    case NO_OP : {
	                        break;
	                    }
	                    case CREATE_ERROR : {
	                        throw new FilterException("createError operation detected: " + opName.value(true), ErrorCode.UNSUPPORTED_OPERATION_USED);
	                    }
	                    default: {
	                        final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
	                        OfficeOpenXMLOperationDocument.logMessage("warn", "Ignoring unsupported operation: " + unsupportedOps);
	                    }
                    }
                }
                opcPackage.setSuccessfulAppliedOperations(aOperations.length());

                final WorkbookPart workbookPart = getPackage().getWorkbookPart();
                final Workbook workbook = workbookPart.getJaxbElement();

                if(getDocumentProperties().optBoolean(DocumentProperties.PROP_CALCULATE_ON_LOAD, false)) {
                	workbook.getCalcPr(true).setCalcCompleted(Boolean.TRUE);
                }

                // our formula updates should be always up to date now, so it is no longer necessary to
                if(log.isInfoEnabled()) {
                	checkFormulaResults();
                }

                // removing calcChain which might be invalid now
                final RelationshipsPart workbookRelationshipsPart = workbookPart.getRelationshipsPart();
                if(workbookRelationshipsPart!=null) {
                    workbookRelationshipsPart.removeRelationshipByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/calcChain");
                    workbookRelationshipsPart.removeRelationshipByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/volatileDependencies");
                }

                // removing tabSelected from other worksheets and finalizing tables
                final Sheets sheets = workbook.getSheets();
                final List<Sheet> sheetList = sheets.getContent();
                final MutableInt uniqueTableId = new MutableInt(1000);
                for(int sheetIndex = 0; sheetIndex < sheetList.size(); sheetIndex++) {
                    final Sheet sheet = sheetList.get(sheetIndex);
                    final Part part = getSheetPart(getPackage(), sheet);
                    final String sheetType = getSheetType(part);
                    if(sheetType.equals("worksheet")) {
                        final Worksheet worksheet = ((WorksheetPart)part).getJaxbElement();
                        TableHelper.finalizeTables(this, (WorksheetPart)part, uniqueTableId);
                        final SheetViews sheetViews = worksheet.getSheetViews();
                        if(sheetViews!=null) {
                            final List<SheetView> sheetViewList = sheetViews.getSheetView();
                            for(SheetView sheetView:sheetViewList) {
                                sheetView.setTabSelected(null);
                            }
                        }
                    }
                }
            }
            catch(final Exception e) {
                String message = e.getMessage();
                if(op!=null) {
                    message += ", operation:" + Integer.toString(i) + " " + op.toString();
                }
                throw new FilterException(message, e, ErrorCode.CRITICAL_ERROR);
            }
        }
    }

    /**
     * checkFormulaResults is logging the formula cells that do not have a result
     */
    private void checkFormulaResults() {
        final WorkbookPart workbookPart = getPackage().getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();
        final RelationshipsPart workbookRelationshipsPart = workbookPart.getRelationshipsPart();
        for (final Sheet sheet:workbook.getSheets().getContent()) {
            final Part part = workbookRelationshipsPart.getPart(sheet.getId());
            if(part instanceof WorksheetPart) {
                final WorksheetPart sheetPart = (WorksheetPart)part;
                if(sheetPart.isUnmarshalled()) {
	                final Worksheet worksheet = sheetPart.getJaxbElement();
	                final SheetData sheetData = worksheet.getSheetData();
	                if(sheetData!=null) {
	                    final Iterator<Row> rowIterator = sheetData.createRowIterator();
	                    while(rowIterator.hasNext()) {
	                        final Row row = rowIterator.next();
	                        final Iterator<Cell> cellIterator = row.createCellIterator();
	                        while(cellIterator.hasNext()) {
	                            final Cell cell = cellIterator.next();
	                            if(cell.getF()!=null&&cell.getV()==null) {
	                            	log.debug("XLSX: Cell:" + cell.getColumn() + " formula without result!");
	                            }
	                        }
	                    }
	                }
                }
            }
        }
    }

    @Override
    public SpreadsheetMLPackage getPackage() {
        return opcPackage;
    }

    @Override
    public void setPackage(OpcPackage p) {
       opcPackage = (SpreadsheetMLPackage)p;
    }

    @Override
    public ThemePart getThemePart(boolean createIfMissing)
    	throws FilterException {

    	if(themePart==null&&createIfMissing) {
    		try {
    			themePart = getDefaultThemePart(new PartName("/xl/theme/theme1.xml"));
    			getPackage().getWorkbookPart().addTargetPart(themePart);
    		}
    		catch(InvalidFormatException e) {
    			throw new FilterException("xlsx filter, could not create default theme", ErrorCode.CRITICAL_ERROR, e);
    		}
    	}
    	return themePart;
    }

    public double getDefaultFontSize() {

        if(defaultFontSize==null) {

            try {
                final CTStylesheet stylesheet = getStylesheet(false);
                if(stylesheet!=null) {
                    final CTFonts fonts = stylesheet.getFonts();
                    if(fonts!=null&&!fonts.getFont().isEmpty()) {
                        final CTFontSize fontSize = fonts.getFont().get(0).getSz();
                        if(fontSize!=null) {
                            defaultFontSize = fontSize.getVal();
                        }
                    }
                }
            }
            catch(final Exception e) {
                // nothing ...
            }
            if(defaultFontSize==null) {
                defaultFontSize = new Double(11.0);
            }
        }
        return defaultFontSize.doubleValue();
    }

    public Worksheet getWorksheet(int sheetIndex) {
        final Sheet sheet = SheetUtils.getSheet(getPackage(), sheetIndex);
        final RelationshipsPart relationshipPart = getPackage().getWorkbookPart().getRelationshipsPart();
        return ((WorksheetPart)relationshipPart.getPart(sheet.getId())).getJaxbElement();
    }

    public double getDefaultRowHeight(Worksheet worksheet) {

        final CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        return sheetFormatPr!=null ? sheetFormatPr.getDefaultRowHeight() : 14.5d;
    }

    public static Part getSheetPart(SpreadsheetMLPackage spreadsheetPackage, final Sheet sheet) {
        final RelationshipsPart relationshipPart = spreadsheetPackage.getWorkbookPart().getRelationshipsPart();
        return relationshipPart.getPart(sheet.getId());
    }

    public static String getSheetType(final Part part)
        throws FilterException {

        String sheetType = null;
        if(part.getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet")) {
            sheetType = "worksheet";
        }
        else if(part.getRelationshipType().equals("http://schemas.microsoft.com/office/2006/relationships/xlMacrosheet")) {
            sheetType = "macrosheet";
        }
        else if(part.getRelationshipType().equals("http://schemas.microsoft.com/office/2006/relationships/xlIntlMacrosheet")) {
            sheetType = "macrosheet";
        }
        else if(part.getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/dialogsheet")) {
            sheetType = "dialogsheet";
        }
        else if(part.getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/chartsheet")) {
            sheetType = "chartsheet";
        }
        if(sheetType==null) {
            throw new FilterException("xlsx import: unknown sheettype detected.", ErrorCode.CRITICAL_ERROR);
        }
        return sheetType;
    }

    private CTStylesheet createStylesheet(OpcPackage opcPackage) throws JAXBException {

        final String defaultStylesheet = "<styleSheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\"" +
        		"    xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\"" +
        		"    mc:Ignorable=\"x14ac\"" +
        		"    xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\">" +
        		"    <fonts count=\"1\" x14ac:knownFonts=\"1\">" +
        		"        <font>" +
        		"            <sz val=\"11\" />" +
        		"            <color theme=\"1\" />" +
        		"            <name val=\"Arial\" />" +
        		"            <family val=\"2\" />" +
        		"            <scheme val=\"minor\" />" +
        		"        </font>" +
        		"    </fonts>" +
        		"    <fills count=\"2\">" +
        		"        <fill>" +
        		"            <patternFill patternType=\"none\" />" +
        		"        </fill>" +
        		"        <fill>" +
        		"            <patternFill patternType=\"gray125\" />" +
        		"        </fill>" +
        		"    </fills>" +
        		"    <borders count=\"1\">" +
        		"        <border>" +
        		"            <left />" +
        		"            <right />" +
        		"            <top />" +
        		"            <bottom />" +
        		"            <diagonal />" +
        		"        </border>" +
        		"    </borders>" +
        		"    <cellStyleXfs count=\"1\">" +
        		"        <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\" />" +
        		"    </cellStyleXfs>" +
        		"    <cellXfs count=\"2\">" +
        		"        <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\" xfId=\"0\" />" +
        		"    </cellXfs>" +
        		"    <cellStyles count=\"1\">" +
        		"        <cellStyle name=\"Standard\" xfId=\"0\" builtinId=\"0\" />" +
        		"    </cellStyles>" +
        		"    <dxfs count=\"0\" />" +
        		"    <tableStyles count=\"0\" defaultTableStyle=\"TableStyleMedium2\"" +
        		"        defaultPivotStyle=\"PivotStyleLight16\" />" +
        		"</styleSheet>";

        final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
        final Object o = org.docx4j.XmlUtils.unmarshallFromTemplate(defaultStylesheet, opcPackage, mappings, getPackage().getWorkbookPart().getJAXBContext());
        return o instanceof JAXBElement ? (CTStylesheet)((JAXBElement<?>)o).getValue() : (CTStylesheet)o;
    }

	@Override
	public OfficeOpenXMLComponent getRootComponent() {
        final WorkbookPart workbookPart = getPackage().getWorkbookPart();
        final Workbook workbook = workbookPart.getJaxbElement();
        return new RootComponent(this, workbook.getSheets());
	}
}
