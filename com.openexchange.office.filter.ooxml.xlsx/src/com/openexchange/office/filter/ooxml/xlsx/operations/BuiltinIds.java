/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.filter.ooxml.xlsx.operations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xlsx4j.sml.CTCellStyle;
import org.xlsx4j.sml.CTCellStyleXfs;
import org.xlsx4j.sml.CTCellStyles;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTXf;

public final class BuiltinIds {

    private final static String[] builtins = new String[54];

    static {
        for (int i = 1; i <= 6; i++) {
            final String accentName = "Accent " + i;

            final int normalId = 25 + (i * 4);

            builtins[normalId + 0] = accentName;
            builtins[normalId + 1] = "20\u0025 - " + accentName;
            builtins[normalId + 2] = "40\u0025 - " + accentName;
            builtins[normalId + 3] = "60\u0025 - " + accentName;
        }

        builtins[0] = "Standard";

        builtins[3] = "Comma";
        builtins[4] = "Currency";
        builtins[5] = "Percent";
        builtins[6] = "Decimal";
        builtins[7] = "Currency [0]";
        builtins[8] = "Hyperlink";
        builtins[9] = "Followed Hyperlink";
        builtins[10] = "Note";
        builtins[11] = "Warning Text";

        builtins[15] = "Title";
        builtins[16] = "Heading 1";
        builtins[17] = "Heading 2";
        builtins[18] = "Heading 3";
        builtins[19] = "Heading 4";
        builtins[20] = "Input";
        builtins[21] = "Output";
        builtins[22] = "Calculation";
        builtins[23] = "Check Cell";
        builtins[24] = "Linked Cell";
        builtins[25] = "Total";
        builtins[26] = "Positive";
        builtins[27] = "Negative";
        builtins[28] = "Neutral";


        builtins[53] = "Explanatory Text";
    }

    public final static Long getBuiltinId(String style) {
        for(int i=0; i<builtins.length; i++) {
            final String res = builtins[i];
            if(res!=null&&res.equals(style)) {
                return new Long(i);
            }
        }
        return null;
    }

    private final static String getName(long builtinId) {
        String res = null;
        if(builtinId>=0&&builtinId<builtins.length) {
            res = builtins[(int) builtinId];
        }
        if (res == null || res.isEmpty()) {
            res = "BuiltIn" + builtinId;
        }
        return res;
    }

    public final static Map<String, CTXf> getUsedStyles(CTStylesheet styleSheet) {
        return getUsedStyles(styleSheet, null, null);
    }

    public final static Map<String, CTXf> getUsedStyles(CTStylesheet styleSheet, Map<Long, String> indexToStyleId, Map<String, Long> styleIdToIndex) {
        final Map<String, CTXf> usedStyles = new HashMap<String, CTXf>();
        if(styleSheet!=null) {
            final CTCellStyleXfs cellStyleXfs = styleSheet.getCellStyleXfs();
            if(cellStyleXfs!=null) {
                final List<CTXf> cellStyleXfsList = cellStyleXfs.getXf();
                for(int i = 0; i < cellStyleXfsList.size(); i++) {

                    String styleId = null;
                    final CTXf ctXf = cellStyleXfsList.get(i);

                    // first check if this style is a builtin style...
                    final CTCellStyles cellStyles = styleSheet.getCellStyles();
                    if(cellStyles!=null) {
                        for(CTCellStyle cellStyle:cellStyles.getCellStyle()) {
                            if(cellStyle.getXfId()==i) {
                                final Long builtInId = cellStyle.getBuiltinId();
                                if(builtInId!=null) {
                                    final String builtInString = getName(builtInId);
                                    if(builtInString!=null) {
                                        styleId = builtInString;
                                    }
                                }

                                // this style is no builtin style, we are using the styleName as Id
                                if(styleId==null) {
                                    final String name = cellStyle.getName();
                                    if(name!=null&&!name.isEmpty()) {
                                        styleId = name;
                                    }
                                }
                            }
                        }
                    }

                    if(styleId==null) {
                        styleId = new Integer(i).toString();
                    }
                    usedStyles.put(styleId, ctXf);
                    if(indexToStyleId!=null) {
                        indexToStyleId.put(new Long(i), styleId);
                    }
                    if(styleIdToIndex!=null) {
                        styleIdToIndex.put(styleId, new Long(i));
                    }
                }
            }
        }
        return usedStyles;
    }
}
