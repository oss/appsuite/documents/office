/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.docx4j.dml.BaseStyles;
import org.docx4j.dml.CTColorScheme;
import org.docx4j.dml.Theme;
import org.docx4j.openpackaging.parts.ThemePart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfIcon;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfvo;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTColorScale;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.STCfvoType;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.STDataBarAxisPosition;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.STDataBarDirection;
import org.xlsx4j.sml.CTBorder;
import org.xlsx4j.sml.CTBorderPr;
import org.xlsx4j.sml.CTCellAlignment;
import org.xlsx4j.sml.CTCellProtection;
import org.xlsx4j.sml.CTCfRule;
import org.xlsx4j.sml.CTColor;
import org.xlsx4j.sml.CTColors;
import org.xlsx4j.sml.CTDataBar;
import org.xlsx4j.sml.CTDxf;
import org.xlsx4j.sml.CTFill;
import org.xlsx4j.sml.CTFont;
import org.xlsx4j.sml.CTGradientFill;
import org.xlsx4j.sml.CTGradientStop;
import org.xlsx4j.sml.CTIconSet;
import org.xlsx4j.sml.CTIndexedColors;
import org.xlsx4j.sml.CTNumFmt;
import org.xlsx4j.sml.CTPatternFill;
import org.xlsx4j.sml.CTRgbColor;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.Col;
import org.xlsx4j.sml.Cols;
import org.xlsx4j.sml.ICfRule;
import org.xlsx4j.sml.ICfvo;
import org.xlsx4j.sml.IColorScale;
import org.xlsx4j.sml.IDataBar;
import org.xlsx4j.sml.IIconSet;
import org.xlsx4j.sml.STBorderStyle;
import org.xlsx4j.sml.STHorizontalAlignment;
import org.xlsx4j.sml.STPatternType;
import org.xlsx4j.sml.STVerticalAlignment;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.json.JSONHelper;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.components.SheetComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.XlsxComponent;

public class Utils {

    public static JSONObject createDxfAttrs(CTDxf dxf, CTStylesheet stylesheet)
        throws JSONException {

        final JSONObject cellAttrs = new JSONObject();
        final CTFill ctFill = dxf.getFill();
        if(ctFill!=null) {
            Utils.createFill(cellAttrs, stylesheet, ctFill, true);
        }
        final CTCellAlignment ctCellAlignment = dxf.getAlignment();
        if(ctCellAlignment!=null) {
            Utils.createAlignment(cellAttrs, ctCellAlignment);
        }
        final CTBorder ctBorder = dxf.getBorder();
        if(ctBorder!=null) {
            Utils.createBorders(cellAttrs, stylesheet, ctBorder);
        }
        final CTCellProtection ctCellProtection = dxf.getProtection();
        if(ctCellProtection!=null) {
            Utils.createProtection(cellAttrs, ctCellProtection);
        }
        final CTNumFmt ctNumFmt = dxf.getNumFmt();
        if(ctNumFmt!=null) {
            cellAttrs.put(OCKey.FORMAT_ID.value(), ctNumFmt.getNumFmtId());
            cellAttrs.put(OCKey.FORMAT_CODE.value(), ctNumFmt.getFormatCode());
        }
        final JSONObject charAttrs = new JSONObject();
        final CTFont ctFont = dxf.getFont();
        if(ctFont!=null) {
            CellUtils.createCharacterProperties(charAttrs, stylesheet, ctFont);
        }
        final JSONObject attrs = new JSONObject(2);
        if(!cellAttrs.isEmpty()) {
            attrs.put(OCKey.CELL.value(), cellAttrs);
        }
        if(!charAttrs.isEmpty()) {
            attrs.put(OCKey.CHARACTER.value(), charAttrs);
        }
        return attrs.isEmpty()?null:attrs;
    }

    public static void createFill(JSONObject dest, CTStylesheet stylesheet, CTFill fill, boolean dxf)
        throws JSONException {

        if (fill==null) { return; }

        final CTPatternFill patternFill = fill.getPatternFill();
        if (patternFill!=null) {
            createPatternFill(dest, stylesheet, patternFill, dxf);
            return;
        }

        final CTGradientFill gradientFill = fill.getGradientFill();
        if (gradientFill!=null) {
            createGradientFill(dest, stylesheet, gradientFill);
        }
    }

    private static void createGradientFill(JSONObject dest, CTStylesheet stylesheet, CTGradientFill gradientFill)
        throws JSONException {

        final List<CTGradientStop> stopList = gradientFill.getStop();
        if (stopList!=null && stopList.size()>=2 && stopList.size()<=3) {
            final JSONObject jsonColor1 = createColor(stylesheet, stopList.get(0).getColor());
            final JSONObject jsonColor2 = createColor(stylesheet, stopList.get(1).getColor());
            if (jsonColor1!=null && jsonColor2!=null) {
                dest.put(OCKey.FILL_TYPE.value(), "gradient");
                dest.put(OCKey.FORE_COLOR.value(), jsonColor1);
                dest.put(OCKey.FILL_COLOR.value(), jsonColor2);
            }
        }
    }

    private static String convertXFPatternToDMLPattern(STPatternType patternType) {

        if (patternType==null) {
            return null;
        }

        switch (patternType) {
        case DARK_DOWN:         return "dkDnDiag";
        case DARK_HORIZONTAL:   return "dkHorz";
        case DARK_UP:           return "dkUpDiag";
        case DARK_VERTICAL:     return "dkVert";
        case LIGHT_DOWN:        return "ltDnDiag";
        case LIGHT_HORIZONTAL:  return "ltHorz";
        case LIGHT_UP:          return "ltUpDiag";
        case LIGHT_VERTICAL:    return "ltVert";
        case NONE:              return "none";
        case GRAY_0625:         return "pct10";
        case GRAY_125:          return "pct20";
        case LIGHT_GRAY:        return "pct25";
        case LIGHT_TRELLIS:     return "pct30";
        case MEDIUM_GRAY:       return "pct50";
        case DARK_GRAY:         return "pct70";
        case DARK_GRID:         return "smCheck";
        case LIGHT_GRID:        return "smGrid";
        case SOLID:             return "solid";
        case DARK_TRELLIS:      return "trellis";
        }

        return null;
    }

    private static void createPatternFill(JSONObject dest, CTStylesheet stylesheet, CTPatternFill patternFill, boolean dxf)
        throws JSONException {

        // the pattern type (convert to DML pattern types as used in all document operations)
        final STPatternType patternType = patternFill.getPatternType();
    	final String jsonPattern = convertXFPatternToDMLPattern(patternType);

        // the solid fill color, or pattern background color
    	final CTColor bgColor = patternFill.getBgColor();
    	final JSONObject jsonBgColor = (bgColor!=null) ? createColor(stylesheet, bgColor) : null;

        // the pattern foreground color
        final CTColor fgColor = patternFill.getFgColor();
        final JSONObject jsonFgColor = (fgColor!=null) ? createColor(stylesheet, fgColor) : null;

        // JSON object for the "auto color" needed in various places
        final JSONObject jsonAutoColor = new JSONObject(1);
        jsonAutoColor.put(OCKey.TYPE.value(), "auto");

        // in DXF mode, put the existing properties only, but with special behavior for pattern/solid fill type
    	if (dxf) {

    	    if (patternType==STPatternType.NONE) {
    	        // pattern type "none" switches cell to transparent
        	    dest.put(OCKey.FILL_TYPE.value(), "solid");
                dest.put(OCKey.FILL_COLOR.value(), jsonAutoColor);
    	    } else if (patternType == STPatternType.SOLID) {
    	        // pattern type "solid" switches to solid fill color
                dest.put(OCKey.FILL_TYPE.value(), "solid");
                // without explicit fill color, cells keep their own fill color of a pattern fill
                if (jsonBgColor!=null) {
                    dest.put(OCKey.FILL_COLOR.value(), jsonBgColor);
                }
    	    } else {
    	        if (jsonPattern!=null) {
    	            // explicit pattern type switches fill mode to "pattern" (with current colors)
    	            dest.put(OCKey.FILL_TYPE.value(), "pattern");
    	            dest.put(OCKey.PATTERN.value(), jsonPattern);
    	        } else {
    	            // special type identifier that switches from "gradient" to "pattern", but sticks to "solid"
                    dest.put(OCKey.FILL_TYPE.value(), "inherit");
    	        }
    	        if (jsonBgColor!=null) {
                    dest.put(OCKey.FILL_COLOR.value(), jsonBgColor);
    	        }
                if (jsonFgColor!=null) {
                    dest.put(OCKey.FORE_COLOR.value(), jsonFgColor);
                }
    	    }

    	} else { // normal cell XF mode

        	if (jsonPattern==null || patternType==STPatternType.NONE) {
                // pattern type "none" switches cell to transparent
                dest.put(OCKey.FILL_TYPE.value(), "solid");
                dest.put(OCKey.FILL_COLOR.value(), jsonAutoColor);
        	} else if (patternType==STPatternType.SOLID) {
                // pattern type "solid" switches to solid fill color (<fgColor> element used as fill color!)
                dest.put(OCKey.FILL_TYPE.value(), "solid");
                dest.put(OCKey.FILL_COLOR.value(), (jsonFgColor!=null) ? jsonFgColor : jsonAutoColor);
        	} else {
        	    // switch cell to pattern fill
                dest.put(OCKey.FILL_TYPE.value(), "pattern");
                dest.put(OCKey.PATTERN.value(), jsonPattern);
                dest.put(OCKey.FILL_COLOR.value(), (jsonBgColor!=null) ? jsonBgColor : jsonAutoColor);
                dest.put(OCKey.FORE_COLOR.value(), (jsonFgColor!=null) ? jsonFgColor : jsonAutoColor);
        	}
        }
    }

    final static String themeColors[] = {

/* OOXML specification at a glance... if accessing theme colors
 * as described in the specification, then at least black and
 * white are swapped. Some says that only the first two pairs
 * within the index are swapped, other think (like in poi) that each
 * non alpha rbg white and black color is switched
 *
 * TODO: I have to test this. For now I only switch the index into
 * the color scheme.
 *

        "dark1",
        "light1",
        "dark2",
        "light2",
*/
        "light1",
        "dark1",
        "light2",
        "dark2",

        "accent1",
        "accent2",
        "accent3",
        "accent4",
        "accent5",
        "accent6",
        "hyperlink",
        "followedHyperlink"
    };

    final static String[] indexedColors = {
        "000000","FFFFFF","FF0000","00FF00","0000FF","FFFF00","FF00FF","00FFFF",
        "000000","FFFFFF","FF0000","00FF00","0000FF","FFFF00","FF00FF","00FFFF",
        "800000","008000","000080","808000","800080","008080","C0C0C0","808080",
        "9999FF","993366","FFFFCC","CCFFFF","660066","FF8080","0066CC","CCCCFF",
        "000080","FF00FF","FFFF00","00FFFF","800080","800000","008080","0000FF",
        "00CCFF","CCFFFF","CCFFCC","FFFF99","99CCFF","FF99CC","CC99FF","FFCC99",
        "3366FF","33CCCC","99CC00","FFCC00","FF9900","FF6600","666699","969696",
        "003366","339966","003300","333300","993300","993366","333399","333333"
    };

	public static JSONArray createColorScaleSteps(IColorScale iColorScale, int minColorScaleSize, int maxColorScaleSize)
			throws JSONException {
		JSONArray jsonColorScaleSteps = new JSONArray();
		if (iColorScale != null && iColorScale.getCfvo().size() == iColorScale.getColor().size()
				&& iColorScale.getCfvo().size() >= minColorScaleSize
				&& iColorScale.getCfvo().size() <= maxColorScaleSize) {

			for (int i = 0; i < iColorScale.getCfvo().size(); i++) {
				final JSONObject colorScaleStep = createRuleOperation(iColorScale.getCfvo().get(i));
				colorScaleStep.put(OCKey.C.value(), createColor(null, iColorScale.getColor().get(i)));
				jsonColorScaleSteps.put(colorScaleStep);
			}
		}

		return jsonColorScaleSteps;
	}

	/**
	 * Add the CTColorScale Object to the given CfRule.
	 * @throws JSONException
	 */
	public static void addColorScaleObjectToCfRule(XlsxOperationDocument operationDocument, ICfRule iCfRule,  JSONArray jsonColorScales) throws JSONException {
		if (iCfRule instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) {
			CTColorScale colorScale = new CTColorScale();
			for (int i = 0; i < jsonColorScales.length(); i++) {
				JSONObject jsonRule = jsonColorScales.getJSONObject(i);
				colorScale.getCfvo().add((CTCfvo) createRuleObject(new CTCfvo(), jsonRule));
				colorScale.getColor().add(getColorByJson(operationDocument, jsonRule.getJSONObject(OCKey.C.value())));
			}
			((org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) iCfRule).setColorScale(colorScale);
		} else {
			org.xlsx4j.sml.CTColorScale colorScale = new org.xlsx4j.sml.CTColorScale();
			for (int i = 0; i < jsonColorScales.length(); i++) {
				JSONObject jsonRule = jsonColorScales.getJSONObject(i);
				colorScale.getCfvo().add((org.xlsx4j.sml.CTCfvo) createRuleObject(new org.xlsx4j.sml.CTCfvo(), jsonRule));
				colorScale.getColor().add(getColorByJson(operationDocument, jsonRule.getJSONObject(OCKey.C.value())));
			}
			((CTCfRule) iCfRule).setColorScale(colorScale);
		}
	}

	public static JSONObject createDataBarOperation(IDataBar dataBar, IDataBar dataBar1) throws JSONException {
		JSONObject jsonDataBar = new JSONObject();
		if (dataBar != null) {
			jsonDataBar.put(OCKey.R1.value(), createRuleOperation(dataBar.getCfvo().get(0)));
			if (dataBar.getCfvo().size() > 1) {
				jsonDataBar.put(OCKey.R2.value(), createRuleOperation(dataBar.getCfvo().get(1)));
			}

			JSONObject color;
			if (dataBar.getColor() == null && dataBar1 != null) {
				color = createColor(null, dataBar1.getColor());
			} else {
				color = createColor(null, dataBar.getColor());
			}
			jsonDataBar.put(OCKey.C.value(), color);
			jsonDataBar.put(OCKey.S.value(), dataBar.isShowValue() && (dataBar1 == null || dataBar1.isShowValue()));
			jsonDataBar.put(OCKey.MIN.value(), dataBar.getMinLength());
			jsonDataBar.put(OCKey.MAX.value(), dataBar.getMaxLength());

			if (dataBar instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataBar) {
				org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataBar dataBar2009 = (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataBar) dataBar;

				jsonDataBar.put(OCKey.B.value(), dataBar2009.isBorder());
				jsonDataBar.put(OCKey.BC.value(), createColor(null, dataBar2009.getBorderColor()));

				jsonDataBar.put(OCKey.NCS.value(), dataBar2009.isNegativeBarColorSameAsPositive());
				jsonDataBar.put(OCKey.NC.value(), createColor(null, dataBar2009.getNegativeFillColor()));

				jsonDataBar.put(OCKey.NBS.value(), dataBar2009.isNegativeBarBorderColorSameAsPositive());
			    jsonDataBar.put(OCKey.NBC.value(), createColor(null, dataBar2009.getNegativeBorderColor()));

				jsonDataBar.put(OCKey.AP.value(), dataBar2009.getAxisPosition().value());
				jsonDataBar.put(OCKey.AC.value(), createColor(null, dataBar2009.getAxisColor()));

				jsonDataBar.put(OCKey.G.value(), dataBar2009.isGradient());
				jsonDataBar.put(OCKey.D.value(), dataBar2009.getDirection().value());
			}
		}

		return jsonDataBar;
	}

	private static CTColor getColorByJson(XlsxOperationDocument operationDocument, JSONObject jsonColor) throws FilterException, JSONException {
		CTColor color = Context.getsmlObjectFactory().createCTColor();
		if (jsonColor != null) {
			applyColor(operationDocument, color, jsonColor);
		}
		return color;
	}

	/**
	 * Add the CTDataBar to the CfRule Object.
	 * @throws JSONException
	 */
	public static void addDataBarObjectToCfRule(XlsxOperationDocument operationDocument, ICfRule iCfRule,  JSONObject jsonDataBar, boolean hasBothRules, Integer priority) throws JSONException {

		if (jsonDataBar != null) {
			if (iCfRule instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) {
				/**
				 * Documentation for DataBar.
				 * 	https://msdn.microsoft.com/en-us/library/dd948147%28v=office.12%29.aspx
				 */
				org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataBar dataBar = new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataBar();
				addDefaultDataBarAttributes(dataBar, jsonDataBar);
				dataBar.getCfvo().add((CTCfvo) createRuleObject(new CTCfvo(), jsonDataBar.getJSONObject(OCKey.R1.value())));
				dataBar.getCfvo().add((CTCfvo) createRuleObject(new CTCfvo(), jsonDataBar.getJSONObject(OCKey.R2.value())));

				// Fill Color and Priority only if org.xlsx4j.sml.CTCfRule not exists.
				if (!hasBothRules) {
					dataBar.setFillColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.C.value())));
			  		if(priority!=null) {
			  			iCfRule.setPriority(priority.intValue());
			  		}
				}

				if (!jsonDataBar.isNull(OCKey.B.value())) {
					dataBar.setBorder(jsonDataBar.getBoolean(OCKey.B.value()));
				}

				if (dataBar.isBorder()) {
				    if (!jsonDataBar.isNull(OCKey.BC.value())) {
						dataBar.setBorderColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.BC.value())));
				    }
					if (!jsonDataBar.isNull(OCKey.NBS.value())) {
						dataBar.setNegativeBarBorderColorSameAsPositive(jsonDataBar.getBoolean(OCKey.NBS.value()));
					}
				}

				if (!dataBar.isNegativeBarBorderColorSameAsPositive()) {
					if (!jsonDataBar.isNull(OCKey.NBC.value())) {
						dataBar.setNegativeBorderColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.NBC.value())));
					}
				}

				if (!jsonDataBar.isNull(OCKey.NCS.value())) {
					dataBar.setNegativeBarBorderColorSameAsPositive(jsonDataBar.getBoolean(OCKey.NCS.value()));
				}

				if (!dataBar.isNegativeBarColorSameAsPositive()) {
					if (!jsonDataBar.isNull(OCKey.NC.value())) {
						dataBar.setNegativeFillColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.NC.value())));
					}
				}

				if (!jsonDataBar.isNull(OCKey.AP.value())) {
					dataBar.setAxisPosition(STDataBarAxisPosition.fromValue(jsonDataBar.getString(OCKey.AP.value())));
				}

				if (!jsonDataBar.isNull(OCKey.AC.value())) {
					dataBar.setAxisColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.AC.value())));
				}


				if (!jsonDataBar.isNull(OCKey.G.value())) {
					dataBar.setGradient(jsonDataBar.getBoolean(OCKey.G.value()));
				}

				if (!jsonDataBar.isNull(OCKey.D.value())) {
					dataBar.setDirection(STDataBarDirection.fromValue(jsonDataBar.getString(OCKey.D.value())));
				}

				((org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) iCfRule).setDataBar(dataBar);
			} else {
				CTDataBar dataBar = new CTDataBar();
		  		if(priority!=null) {
		  			iCfRule.setPriority(priority.intValue());
		  		}
				dataBar.getCfvo().add((org.xlsx4j.sml.CTCfvo) createRuleObject(new org.xlsx4j.sml.CTCfvo(), jsonDataBar.getJSONObject(OCKey.R1.value())));
		  		dataBar.getCfvo().add((org.xlsx4j.sml.CTCfvo) createRuleObject(new org.xlsx4j.sml.CTCfvo(), jsonDataBar.getJSONObject(OCKey.R2.value())));

				dataBar.setColor(getColorByJson(operationDocument, jsonDataBar.getJSONObject(OCKey.C.value())));

				addDefaultDataBarAttributes(dataBar, jsonDataBar);

				((org.xlsx4j.sml.CTCfRule)iCfRule).setDataBar(dataBar);
			}
		}

	}

	private static void addDefaultDataBarAttributes(IDataBar dataBar, JSONObject jsonDataBar) throws JSONException {
		if (!jsonDataBar.isNull(OCKey.S.value())) {
			dataBar.setShowValue(jsonDataBar.getBoolean(OCKey.S.value()));
		}
		if (!jsonDataBar.isNull(OCKey.MIN.value())) {
			dataBar.setMinLength(jsonDataBar.getLong(OCKey.MIN.value()));
		}
		if (!jsonDataBar.isNull(OCKey.MAX.value())) {
			dataBar.setMaxLength(jsonDataBar.getLong(OCKey.MAX.value()));
		}
	}


	public static JSONObject createIconSet(IIconSet iconSet)  throws JSONException {
		org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTIconSet iconSet2009;
		JSONObject jsonIconSet = new JSONObject();
		jsonIconSet.put(OCKey.IS.value(), iconSet.getIconSet());
		jsonIconSet.put(OCKey.S.value(), iconSet.isShowValue());
		jsonIconSet.put(OCKey.R.value(), iconSet.isReverse());
		jsonIconSet.put(OCKey.P.value(), iconSet.isPercent());
		if (iconSet instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTIconSet) {
			iconSet2009 = (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTIconSet)iconSet;
			jsonIconSet.put(OCKey.C.value(), iconSet2009.isCustom());
		} else {
			iconSet2009 = null;
		}

		JSONArray jsonRules = new JSONArray();
		List<CTCfIcon> cfIcons = null;
		if (iconSet2009 != null) {
			cfIcons = iconSet2009.getCfIcon();
		}

		for (int i = 0; i < iconSet.getCfvo().size(); i++) {
			ICfvo cfvo = iconSet.getCfvo().get(i);
			final JSONObject rule = createRuleOperation(cfvo);
			rule.put(OCKey.GTE.value(), cfvo.isGte());
			if (cfIcons != null && i < cfIcons.size()) {
				CTCfIcon icon = iconSet2009.getCfIcon().get(i);
				rule.put(OCKey.IS.value(), icon.getIconSet());
				rule.put(OCKey.ID.value(), icon.getIconId());
			}
			jsonRules.put(rule);
		}

		jsonIconSet.put(OCKey.IR.value(), jsonRules);

		return jsonIconSet;
	}

	/**
	 * Add CTIconSet to the CfRule Object.
	 * @throws JSONException
	 */
	public static void addIconSetObjectCfRule(XlsxOperationDocument operationDocument, ICfRule iCfRule,  JSONObject jsonIconSet) throws JSONException {
		if (jsonIconSet != null) {
			if (iCfRule instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) {
				org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTIconSet iconSet = new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTIconSet();
				if (!jsonIconSet.isNull(OCKey.C.value())) {
					iconSet.setCustom(jsonIconSet.getBoolean(OCKey.C.value()));
				}
				addDefaultIconSetAttributes(iconSet, jsonIconSet);

				JSONArray jsonRules = jsonIconSet.getJSONArray(OCKey.IR.value());
				for (int i = 0; i < jsonRules.length(); i++) {
					JSONObject jsonRule = jsonRules.getJSONObject(i);
					CTCfvo rule = (CTCfvo) createRuleObject(new CTCfvo(), jsonRule);
					iconSet.getCfvo().add(rule);
					if (jsonRule.has(OCKey.IS.value()) && jsonRule.has(OCKey.ID.value())) {
						CTCfIcon icon = new CTCfIcon();
						icon.setIconSet(jsonRule.getString(OCKey.IS.value()));
						icon.setIconId(jsonRule.getInt(OCKey.ID.value()));
						iconSet.getCfIcon().add(icon);
					}
				}

				((org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) iCfRule).setIconSet(iconSet);
			} else {
				CTIconSet iconSet = new CTIconSet();

				((org.xlsx4j.sml.CTCfRule)iCfRule).setIconSet(iconSet);
				addDefaultIconSetAttributes(iconSet, jsonIconSet);


				JSONArray jsonRules = jsonIconSet.getJSONArray(OCKey.IR.value());
				for (int i = 0; i < jsonRules.length(); i++) {
					JSONObject jsonRule = jsonRules.getJSONObject(i);
					org.xlsx4j.sml.CTCfvo rule = (org.xlsx4j.sml.CTCfvo) createRuleObject(new org.xlsx4j.sml.CTCfvo(), jsonRule);
					iconSet.getCfvo().add(rule);
				}
			}
		}
	}

	private static void addDefaultIconSetAttributes(IIconSet iconSet, JSONObject jsonIconSet) throws JSONException {
		iconSet.setIconSet(jsonIconSet.getString(OCKey.IS.value()));
		if (!jsonIconSet.isNull(OCKey.S.value())) {
			iconSet.setShowValue(jsonIconSet.getBoolean(OCKey.S.value()));
		}
		if (!jsonIconSet.isNull(OCKey.R.value())) {
			iconSet.setReverse(jsonIconSet.getBoolean(OCKey.R.value()));
		}
		if (!jsonIconSet.isNull(OCKey.P.value())) {
			iconSet.setPercent(jsonIconSet.getString(OCKey.P.value()).equals("percent"));
		}
	}

	private static JSONObject createRuleOperation(ICfvo cfvo) throws JSONException {
		JSONObject jsonRule = new JSONObject();

		if (cfvo.getType() != null) {
			String type;
			switch (cfvo.getType().ordinal()) {
			case 0 :
				type = "formula";
				break;
			case 1 :
				type = "percent";
				break;
			case 2:
				type = "max";
				break;
			case 3:
				type = "min";
				break;
			case 4:
				type = "formula";
				break;
			case 5:
				type = "percentile";
				break;
			case 6:
				type = "automin";
				break;
			case 7:
				type = "automax";
				break;
			default:
				type = null;
			}
			if (type != null) {
				jsonRule.put(OCKey.T.value(), type);
				jsonRule.put(OCKey.V.value(), cfvo.getVal());
			}
		}

		return jsonRule;
	}

	private static ICfvo createRuleObject(ICfvo iCfvo, JSONObject jsonRule) throws JSONException {
		if (jsonRule != null) {
			String typeName = jsonRule.getString(OCKey.T.value());
			if (iCfvo  instanceof CTCfvo) {
				((CTCfvo)iCfvo).setType(STCfvoType.fromValue(typeName));
			} else {
				org.xlsx4j.sml.STCfvoType type = org.xlsx4j.sml.STCfvoType.fromValue(typeName);
				if (type == null) {
					if (typeName.equalsIgnoreCase("automin")) {
						typeName = "min";
					} else if (typeName.equalsIgnoreCase("automax")) {
						typeName = "max";
					}
				}
				((org.xlsx4j.sml.CTCfvo)iCfvo).setType(org.xlsx4j.sml.STCfvoType.fromValue(typeName));
			}
			if (jsonRule.has(OCKey.V.value())) {
				iCfvo.setVal(jsonRule.getString(OCKey.V.value()));
			}
			if (!jsonRule.isNull(OCKey.GTE.value())) {
				iCfvo.setGte(jsonRule.getBoolean(OCKey.GTE.value()));
			}

		}
		return iCfvo;
	}

    public static JSONObject createColor(CTStylesheet stylesheet, CTColor color)
        throws JSONException {

        if (color!=null) {

            JSONObject jsonColor = new JSONObject(3);

            Boolean auto    = color.isAuto();
            Long    theme   = color.getTheme();
            Long    indexed = color.getIndexed();
            byte[]  rgb     = color.getRgb();

            if(theme!=null&&theme.longValue()<themeColors.length) {
                jsonColor.put(OCKey.TYPE.value(),  "scheme");
                jsonColor.put(OCKey.VALUE.value(), themeColors[theme.intValue()]);
            }
            else if(auto!=null&&auto.booleanValue()) {
                jsonColor.put(OCKey.TYPE.value(), "auto");
            }
            else if(rgb!=null&&rgb.length==3) {
                jsonColor.put(OCKey.TYPE.value(), "rgb");
                jsonColor.put(OCKey.VALUE.value(), Commons.bytesToHexString(rgb, 0, 3));
            }
            else if(rgb!=null&&rgb.length==4) {
            	// alpha channel will be ignored (Excel and Calc ignore it too)
                jsonColor.put(OCKey.TYPE.value(), "rgb");
                jsonColor.put(OCKey.VALUE.value(), Commons.bytesToHexString(rgb, 1, 3));
            }
            else if(indexed!=null) {
                if(indexed.longValue()>=indexedColors.length) {
                    jsonColor.put(OCKey.TYPE.value(), "auto");
                }
                else {
                    jsonColor.put(OCKey.TYPE.value(), "rgb");
                    String colorValue = indexedColors[indexed.intValue()];
                    if(stylesheet!=null) {
                        // check if indexedColor is overwritten
	                    final CTColors ctColors = stylesheet.getColors();
	                    if(ctColors!=null) {
	                        CTIndexedColors _indexedColors = ctColors.getIndexedColors();
	                        if(_indexedColors!=null) {
	                            List<CTRgbColor> _indexedColorList = _indexedColors.getRgbColor();
	                            if(_indexedColorList.size()>indexed.intValue()) {
	                                CTRgbColor _indexedRgbColor = _indexedColorList.get(indexed.intValue());
	                                if(_indexedRgbColor!=null) {
	                                    byte[] _indexedRgbColorArray = _indexedRgbColor.getRgb();
	                                    if(_indexedRgbColorArray!=null&&_indexedRgbColorArray.length==4) {
	                                        // alpha = indRgbArray[0]; don't know if alpha is used here, value seems to be zero always
	                                        colorValue = Commons.bytesToHexString(_indexedRgbColorArray, 1, 3);
	                                    }
	                                }
	                            }
	                        }
	                    }
                    }
                    jsonColor.put(OCKey.VALUE.value(), colorValue);
                }
            }

            final JSONArray transformations = JSONHelper.shadeTintToJsonTransformations(color.getTint());
            if (transformations != null) {
                jsonColor.put(OCKey.TRANSFORMATIONS.value(), transformations);
            }

            return jsonColor.isEmpty() ? null : jsonColor;
        }
        return null;
    }

    public static JSONObject createDefaultColor()
    	throws JSONException {

    	JSONObject jsonColor = new JSONObject(2);
        jsonColor.put(OCKey.TYPE.value(),  "scheme");
        jsonColor.put(OCKey.VALUE.value(), themeColors[1]);
        return jsonColor;
    }

    // CTBorder

    public static void createBorders(JSONObject jsonDest, CTStylesheet stylesheet, CTBorder borders)
        throws JSONException {

        if(borders!=null) {
            Commons.jsonPut(jsonDest, OCKey.BORDER_LEFT.value(), createJSONfromCTBorder(stylesheet, borders.getLeft()));
            Commons.jsonPut(jsonDest, OCKey.BORDER_TOP.value(), createJSONfromCTBorder(stylesheet, borders.getTop()));
            Commons.jsonPut(jsonDest, OCKey.BORDER_RIGHT.value(), createJSONfromCTBorder(stylesheet, borders.getRight()));
            Commons.jsonPut(jsonDest, OCKey.BORDER_BOTTOM.value(), createJSONfromCTBorder(stylesheet, borders.getBottom()));
            Commons.jsonPut(jsonDest, OCKey.BORDER_INSIDE_HOR.value(), createJSONfromCTBorder(stylesheet, borders.getHorizontal()));
            Commons.jsonPut(jsonDest, OCKey.BORDER_INSIDE_VERT.value(), createJSONfromCTBorder(stylesheet, borders.getVertical()));
        }
    }

    public static int pixelToHmm(double pixel) {
    	return (int) Math.round(pixel / 96.0 * 2540.0);
    }

    public static double hmmToPixel(int hmm) {
    	return hmm * 96.0 / 2540.0;
    }

    public static int HAIR_WIDTH_HMM= pixelToHmm(0.5);
    public static int THIN_WIDTH_HMM = pixelToHmm(1);
    public static int MEDIUM_WIDTH_HMM = pixelToHmm(2);
    public static int THICK_WIDTH_HMM = pixelToHmm(3);

    public static JSONObject createJSONfromCTBorder(CTStylesheet stylesheet, CTBorderPr borderPr)
        throws JSONException {

        if (borderPr==null)
            return null;

        JSONObject jsonBorder = new JSONObject();
        String style = "single";
        double width = THIN_WIDTH_HMM;
        STBorderStyle borderStyle = borderPr.getStyle();
        if(borderStyle!=null) {
            if(borderStyle==STBorderStyle.NONE) {
                style = "none";
            }
            else if(borderStyle==STBorderStyle.HAIR) {
                style = "single";
                width = HAIR_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.THIN) {
                style = "single";
                width = THIN_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.DOTTED) {
                style = "dotted";
                width = THIN_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.DASH_DOT_DOT) {
                style = "dashDotDot";
                width = THIN_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.DASH_DOT) {
                style = "dashDot";
                width = THIN_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.DASHED) {
                style = "dashed";
                width = THIN_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.MEDIUM) {
                style = "single";
                width = MEDIUM_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.MEDIUM_DASH_DOT_DOT) {
                style = "dashDotDot";
                width = MEDIUM_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.MEDIUM_DASH_DOT) {
                style = "dashDot";
                width = MEDIUM_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.MEDIUM_DASHED) {
                style = "dashed";
                width = MEDIUM_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.SLANT_DASH_DOT) {
                style = "dashDot";
                width = MEDIUM_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.THICK) {
                style = "single";
                width = THICK_WIDTH_HMM;
            }
            else if(borderStyle==STBorderStyle.DOUBLE) {
                style = "double";
                width = THICK_WIDTH_HMM;
            }
        }
        if(borderStyle!=STBorderStyle.NONE) {
            jsonBorder.put(OCKey.WIDTH.value(), width);
        }
        jsonBorder.put(OCKey.STYLE.value(), style);

        // creating border color
        Commons.jsonPut(jsonBorder, OCKey.COLOR.value(), createColor(stylesheet, borderPr.getColor()));

        return jsonBorder;
    }

    public static void createAlignment(JSONObject jsonDest, CTCellAlignment cellAlignment)
        throws JSONException {

        if(cellAlignment!=null) {
            STHorizontalAlignment horzAlignment = cellAlignment.getHorizontal();
            if(horzAlignment!=null) {
                String hAlign;
                switch(horzAlignment) {
                    case LEFT:              hAlign = "left";         break;
                    case CENTER:            hAlign = "center";       break;
                    case RIGHT:             hAlign = "right";        break;
                    case JUSTIFY:           hAlign = "justify";      break;
                    case DISTRIBUTED:       hAlign = "distribute";   break;
                    case CENTER_CONTINUOUS: hAlign = "centerAcross"; break;
                    case FILL:              hAlign = "fillAcross";   break;
                    default:                hAlign = "auto";
                }
                jsonDest.put(OCKey.ALIGN_HOR.value(), hAlign);
            }

            STVerticalAlignment vertAlignment = cellAlignment.getVertical();
            if(vertAlignment!=null) {
                String vAlign;
                switch(vertAlignment) {
                    case TOP:     vAlign = "top";     break;
                    case CENTER:  vAlign = "middle";  break;
                    case JUSTIFY: vAlign = "justify"; break;
                    default:      vAlign = "bottom";
                }
                jsonDest.put(OCKey.ALIGN_VERT.value(), vAlign);
            }
            Boolean wrapText = cellAlignment.isWrapText();
            if(wrapText!=null) {
                jsonDest.put(OCKey.WRAP_TEXT.value(), wrapText.booleanValue());
            }
        }
    }

    public static void createProtection(JSONObject jsonDest, CTCellProtection cellProtection)
        throws JSONException {

        if(cellProtection!=null) {
            if(cellProtection.isHidden()!=null && cellProtection.isHidden().booleanValue()) {
                jsonDest.put(OCKey.HIDDEN.value(), true);
            }
            if(cellProtection.isLocked()!=null&&!cellProtection.isLocked().booleanValue()) {
                jsonDest.put(OCKey.UNLOCKED.value(), true);
            }
        }
        else
            jsonDest.put(OCKey.UNLOCKED.value(), false);
    }

    public static byte[] themeColorToBytes(ThemePart themePart, String themeColor) {
        byte[] rgbValue = null;
        if(themePart!=null) {
        	final Theme theme = themePart.getJaxbElement();
        	if(theme!=null) {
	            BaseStyles themeElements = theme.getThemeElements();
	            if(themeElements!=null) {
	                CTColorScheme clrScheme = themeElements.getClrScheme();
	                if(clrScheme!=null) {
	                    org.docx4j.dml.CTColor ctColor = null;
	                    if(themeColor.equals("light1")) {
	                        ctColor = clrScheme.getLt1();
	                    }
	                    else if(themeColor.equals("dark1")) {
	                        ctColor = clrScheme.getDk1();
	                    }
	                    else if(themeColor.equals("light2")) {
	                        ctColor = clrScheme.getLt1();
	                    }
	                    else if(themeColor.equals("dark2")) {
	                        ctColor = clrScheme.getDk2();
	                    }
	                    else if(themeColor.equals("accent1")) {
	                        ctColor = clrScheme.getAccent1();
	                    }
	                    else if(themeColor.equals("accent2")) {
	                        ctColor = clrScheme.getAccent2();
	                    }
	                    else if(themeColor.equals("accent3")) {
	                        ctColor = clrScheme.getAccent3();
	                    }
	                    else if(themeColor.equals("accent4")) {
	                        ctColor = clrScheme.getAccent4();
	                    }
	                    else if(themeColor.equals("accent5")) {
	                        ctColor = clrScheme.getAccent5();
	                    }
	                    else if(themeColor.equals("accent6")) {
	                        ctColor = clrScheme.getAccent6();
	                    }
	                    else if(themeColor.equals("hyperlink")) {
	                        ctColor = clrScheme.getHlink();
	                    }
	                    else if(themeColor.equals("followedHyperlink")) {
	                        ctColor = clrScheme.getFolHlink();
	                    }
	                    else if(themeColor.equals("text1")) {
	                        ctColor = clrScheme.getDk1();
	                    }
	                    else if(themeColor.equals("text2")) {
	                        ctColor = clrScheme.getDk2();
	                    }
	                    else if(themeColor.equals("background1")) {
	                        ctColor = clrScheme.getLt1();
	                    }
	                    else if(themeColor.equals("background2")) {
	                        ctColor = clrScheme.getLt2();
	                    }
	                    if(ctColor!=null) {
	                        rgbValue = Commons.ctColorToBytes(ctColor);
	                    }
	                }
	            }
            }
        }
        return rgbValue;
    }

    public static void applyColor(XlsxOperationDocument operationDocument, CTColor color, JSONObject jsonColor)
        throws FilterException, JSONException {

        if(color!=null&&jsonColor.has(OCKey.TYPE.value())) {
            String type = jsonColor.getString(OCKey.TYPE.value());
            color.setTint(null);
            if(type.equals("rgb")&&jsonColor.has(OCKey.VALUE.value())) {
                color.setRgb(Commons.hexStringToBytes(jsonColor.getString(OCKey.VALUE.value())));
                color.setTheme(null);
            } else if (type.equals("scheme")&&jsonColor.has(OCKey.VALUE.value())) {
                String themeColor = jsonColor.getString(OCKey.VALUE.value());

                long themeColorIndex = 0;
                for(String c:themeColors) {
                    if(c.equals(themeColor)) {
                        break;
                    }
                    themeColorIndex++;
                }
                if(themeColorIndex >= themeColors.length) {
                    if(themeColor.equals("text1")) {
                        themeColorIndex = 1;
                    }
                    else if(themeColor.equals("text2")) {
                        themeColorIndex = 3;
                    }
                    else if(themeColor.equals("background1")) {
                        themeColorIndex = 0;
                    }
                    else if(themeColor.equals("background2")) {
                        themeColorIndex = 2;
                    }
                    else {
                        themeColorIndex = 1;
                    }
                }
                color.setTheme(themeColorIndex);
                color.setRgb(themeColorToBytes(operationDocument.getThemePart(true), themeColor));
            } else if (type.equals("auto")) {
                color.setAuto(true);
                color.setTheme(null);
                return;
            }

            // calculate value of the "tint" attribute from the JSON color transformations
			color.setTint(JSONHelper.jsonTransformationsToShadeTint(jsonColor));
        }
    }

    public static void createColumnAttributeRanges(Cols cols, long start, long size) {
        List<Col> colList = cols.getCol();

        int i=0;
        for(;i<colList.size();i++) {
            Col col = colList.get(i);

            if(col.getMax()<start) {
                continue;
            }
            if(col.getMin()>((start+size)-1))
                break;
            long colLength = col.getMax() - col.getMin() + 1;
            if(col.getMin()>start) {
                Col newCol = Context.getsmlObjectFactory().createCol();
                newCol.setMin(start);
                long newEnd = start + size - 1;
                if(newEnd>=col.getMin()) {
                    newEnd = col.getMin() - 1;
                }
                newCol.setMax(newEnd);
                start = newEnd + 1;
                size -= (newCol.getMax() - newCol.getMin()) + 1;
                colList.add(i, newCol);
            }
            else if(col.getMin()==start) {
                if(colLength<size) {
                    start += colLength;
                    size -= colLength;
                }
                else if(colLength>size) {
                    final Col newCol = col.clone();
                    col.setMax(col.getMin()+size-1);
                    newCol.setMin(col.getMax()+1);
                    colList.add(i+1, newCol);
                    size = 0;
                    break;
                }
                else {
                    size = 0;
                    break;
                }
            }
            // colMin() < start
            else if(col.getMax()>=start) {
                final Col newCol = col.clone();
                newCol.setMin(start);
                newCol.setMax(col.getMax());
                col.setMax(start-1);
                colList.add(i+1, newCol);
            }
        }
        if(size>0) {
            Col newCol = Context.getsmlObjectFactory().createCol();
            newCol.setMin(start);
            newCol.setMax((start+size)-1);
            colList.add(i, newCol);
        }
    }

    public static void applyRangesToSqrefRange(List<String> sqrefList, String value) {
        sqrefList.clear();
        if(value!=null) {
            final String[] refs = value.split(" ", -1);
            for(String r:refs) {
                sqrefList.add(r);
            }
        }
    }

    public static List<CellRefRange> createCellRefRangeListFromSqrefList(List<String> sqrefList) {
		final List<CellRefRange> cellRefRangeList = new ArrayList<CellRefRange>();
	    for(String sqRef:sqrefList) {
	        final CellRefRange cellRefRange = CellRefRange.createCellRefRange(sqRef);
	        if(cellRefRange!=null) {
	            cellRefRangeList.add(cellRefRange);
	        }
	    }
	    return cellRefRangeList;
    }

	private static Pattern autoStyleIdPattern = Pattern.compile("a\\d+");

    /**
     * Parses the passed identifier of an auto-style, and returns the array index of the
     * corresponding cell XF element.
     *
     * @param styleId
     * 	The identifier of an auto-style, as used in document operations. MUST NOT be null!
     *  Can be the empty string to refer to the default auto-style "a0".
     *
     * @return
     * 	The zero-based index of the specified auto-style (e.g. the number 3 for the style
     * 	identifier "a3"); or null, if the passed string is not a valid auto-style identifier.
     */
    public static Long parseAutoStyleId(String styleId) {
    	if (styleId.length() == 0) { return 0L; }
    	Matcher matcher = autoStyleIdPattern.matcher(styleId);
    	return matcher.matches() ? Long.parseLong(styleId.substring(1)) : null;
    }

    public static int getParentSheetIndex(XlsxComponent component) {
        ComponentContext<OfficeOpenXMLOperationDocument> context = component.getParentContext();
        while(context != null && !(context instanceof SheetComponent)) {
            context = context.getParentContext();
        }
        return context instanceof SheetComponent ? ((SheetComponent)context).getComponentNumber() : 0;
    }
}
