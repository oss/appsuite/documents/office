/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.CTDrawing;
import org.xlsx4j.sml.ISheetAccess;
import org.xlsx4j.sml.Sheet;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class SheetWrapper implements INodeAccessor<AnchorBase> {

    private SerializationPart<ISheetAccess> worksheetPart;
    final XlsxOperationDocument operationDocument;
    private DLList<AnchorBase> content;

    public SheetWrapper(Sheet sheet, XlsxOperationDocument operationDocument) {
        this.operationDocument = operationDocument;

        final RelationshipsPart relationshipPart = operationDocument.getPackage().getWorkbookPart().getRelationshipsPart();
        final Part part = relationshipPart.getPart(sheet.getId());
        // not every sheet is a jaxbXmlPart, some sheets like macrosheet will be loaded as default xml. JaxbXmlParts needs to be added for them.
        if(part instanceof SerializationPart) {
            final Object e = ((SerializationPart<?>)part).getJaxbElement();
            if(e instanceof ISheetAccess) {
                worksheetPart = (SerializationPart<ISheetAccess>)part;
                final ISheetAccess worksheet = (ISheetAccess)e;
                final CTDrawing drawing = worksheet.getDrawing();
                if(drawing!=null) {
                    final String drawingId = drawing.getId();
                    if(drawingId!=null&&!drawingId.isEmpty()) {
                        final RelationshipsPart sourceRelationships = worksheetPart.getRelationshipsPart();
                        if(sourceRelationships!=null) {
                            final Part drawingsPart = sourceRelationships.getPart(drawingId);
                            if(drawingsPart instanceof org.docx4j.openpackaging.parts.DrawingML.Drawing) {
                                final org.docx4j.dml.spreadsheetDrawing.CTDrawing drawings = ((org.docx4j.openpackaging.parts.DrawingML.Drawing)drawingsPart).getJaxbElement();
                                content = drawings.getContent();
                                operationDocument.setContextPart(drawingsPart);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public DLList<AnchorBase> getContent() {
        return getContent(true);
    }

    public DLList<AnchorBase> getContent(boolean forceCreate) {
        if(content==null&&forceCreate) {
            final ISheetAccess worksheet = worksheetPart.getJaxbElement();
            CTDrawing drawing = worksheet.getDrawing();
            if(drawing==null) {
                drawing = Context.getsmlObjectFactory().createCTDrawing();
                worksheet.setDrawing(drawing);
            }
            final org.docx4j.openpackaging.parts.DrawingML.Drawing drawingsPart;
            try {
                drawingsPart = new org.docx4j.openpackaging.parts.DrawingML.Drawing();
                final org.docx4j.dml.spreadsheetDrawing.CTDrawing drawings = org.docx4j.jaxb.Context.getDmlSpreadsheetDrawingObjectFactory().createCTDrawing();
                drawingsPart.setJaxbElement(drawings);
                final Relationship relationship = worksheetPart.addTargetPart(drawingsPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
                content = drawings.getContent();
                operationDocument.setContextPart(drawingsPart);
                drawing.setId(relationship.getId());
            } catch (InvalidFormatException e) {
                throw new FilterException("XLSX Export: could not create drawing part", ErrorCode.CRITICAL_ERROR, e);
            }
        }
        return content;
    }
}
