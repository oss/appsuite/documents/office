/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.XmlUtils;
import org.docx4j.dml.CTRegularTextRun;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;

public class TextComponent extends XlsxComponent {

    private final CTRegularTextRun textRun;

    public TextComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _textNode, int _componentNumber) {
        super(parentContext, _textNode, _componentNumber);
        textRun = (CTRegularTextRun)_textNode.getData();
    }

    @Override
    public void delete(int count) {
        if(getComponentNumber()==0&&getNextComponent()==null) {
            getParagraphComponent().getParagraph().setEndParaRPr(textRun.getRPr(false));
        }
        super.delete(count);
    }

    @Override
    public int getNextComponentNumber() {
        int textLength = textRun.getT().length();
        if (textLength==0) {
            System.out.println("Error: empty text string is no component... this results in problems with the component iterator");
            textLength++;       // increasing.. preventing an endless loop
        }
        return getComponentNumber() + textLength;
    }

    @Override
    public void splitStart(int componentPosition, SplitMode splitMode) {
    	final int splitPosition = componentPosition-getComponentNumber();
    	if(splitPosition>0) {
    		splitText(splitPosition, true);
    		setComponentNumber(getComponentNumber() +splitPosition);
    	}
    }

    @Override
    public void splitEnd(int componentPosition, SplitMode splitMode) {
    	if(getNextComponentNumber()>++componentPosition) {
    		splitText(componentPosition-getComponentNumber(), false);
    	}
    }

    private void splitText(int textSplitPosition, boolean splitStart) {
        if(textSplitPosition>0&&textSplitPosition<textRun.getT().length()) {
        	final CTRegularTextRun newTextRun = XmlUtils.deepCopy(textRun, getOperationDocument().getPackage());
        	newTextRun.setParent(textRun.getParent());
            final StringBuffer s = new StringBuffer(textRun.getT());
        	final DLList<Object> parentContent = (DLList<Object>)((IContentAccessor)textRun.getParent()).getContent();
            if(splitStart) {
            	parentContent.addNode(getNode(), new DLNode<Object>(newTextRun), true);
                newTextRun.setT(s.substring(0, textSplitPosition));
                textRun.setT(s.substring(textSplitPosition));
            }
            else {
            	parentContent.addNode(getNode(), new DLNode<Object>(newTextRun), false);
                textRun.setT(s.substring(0, textSplitPosition));
                newTextRun.setT(s.substring(textSplitPosition));
            }
        }
    }

	@Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
			throws JSONException {

	    if(attrs!=null) {
            DMLHelper.applyTextCharacterPropertiesFromJson(textRun.getRPr(true), attrs, operationDocument.getContextPart());
        }
	}

	@Override
	public JSONObject createJSONAttrs(JSONObject attrs)
			throws JSONException, FilterException {

		DMLHelper.createJsonFromTextCharacterProperties(getOperationDocument(), attrs, textRun.getRPr(false));
		return attrs;
	}

	public ParagraphComponent getParagraphComponent() {
		return (ParagraphComponent)getParentComponent();
	}
}
