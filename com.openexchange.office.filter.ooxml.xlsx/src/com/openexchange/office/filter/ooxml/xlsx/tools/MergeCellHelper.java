/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xlsx4j.sml.CTMergeCell;
import org.xlsx4j.sml.CTMergeCells;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;

public class MergeCellHelper {

    private static class MergeCellWithDirectRefAccess implements Comparable<MergeCellWithDirectRefAccess> {

        MergeCellWithDirectRefAccess(CTMergeCell mergeCell) {
            final CellRefRange cellRefRange = mergeCell.getCellRefRange(false);
            topRow = cellRefRange.getStart().getRow();
            leftColumn = cellRefRange.getStart().getColumn();
            bottomRow = cellRefRange.getEnd().getRow();
            rightColumn = cellRefRange.getEnd().getColumn();
            alive = true;
        }

        int getTopRow() {
            return topRow;
        }
        int getLeftColumn() {
            return leftColumn;
        }
        int getBottomRow() {
            return bottomRow;
        }
        int getRightColumn() {
            return rightColumn;
        }
        CellRef getStart() {
            return new CellRef(leftColumn, topRow);
        }
        CellRef getEnd() {
            return new CellRef(rightColumn, bottomRow);
        }
        boolean isAlive() {
            return alive;
        }
        void kill() {
            alive = false;
        }
        @Override
        public int compareTo(MergeCellWithDirectRefAccess mergeCellWithDirectRefAccess) {
            final int diff = topRow-mergeCellWithDirectRefAccess.getTopRow();
            if (diff!=0)
                return diff;        // the rows are unequal... that is sufficient... otherwise if equal, we have to take care of the column position (next line)
            return leftColumn-mergeCellWithDirectRefAccess.getLeftColumn();
        }
        private final int topRow;
        private final int leftColumn;
        private final int bottomRow;
        private final int rightColumn;
        private boolean alive;
    }

    public static void createMergeCellOperations(JSONArray operationsArray, int sheetIndex, CTMergeCells mergeCells)
        throws JSONException {

        if(mergeCells==null)
            return;

        final List<CTMergeCell> mergeCellList = mergeCells.getMergeCell();
        if(mergeCellList==null||mergeCellList.isEmpty())
            return;

        // we create a sorted list to be able to properly iterate from top left to bottom right (we imply that there are no intersections between mergeCellRanges and topLeft is our Key)
        final List<MergeCellWithDirectRefAccess> mergeCellSortedList = new ArrayList<MergeCellWithDirectRefAccess>(mergeCellList.size());
        // next we create a hashMap to gain direct access to each topLeft mergeCellRange
        final HashMap<Integer, HashMap<Integer, MergeCellWithDirectRefAccess>> rowMap = new HashMap<Integer, HashMap<Integer, MergeCellWithDirectRefAccess>>();

        for(CTMergeCell mergeCell:mergeCellList) {
            final MergeCellWithDirectRefAccess mergeCellSorted = new MergeCellWithDirectRefAccess(mergeCell);
            mergeCellSortedList.add(mergeCellSorted);

            final int r = mergeCellSorted.getTopRow();
            final int c = mergeCellSorted.getLeftColumn();
            HashMap<Integer, MergeCellWithDirectRefAccess> columnMap = rowMap.get(r);
            if(columnMap==null) {
                columnMap = new HashMap<Integer, MergeCellWithDirectRefAccess>();
                rowMap.put(r,  columnMap);
            }
            columnMap.put(c, mergeCellSorted);
        }

        Collections.sort(mergeCellSortedList);

        // the sortedList and HashMap above is now initialized ...
        for(MergeCellWithDirectRefAccess mergeCellSorted:mergeCellSortedList) {

            // now iterating from top left to bottom right
            if(mergeCellSorted.isAlive()) { // maybe this cellRange has already been taken for an vertical/horizontal merge
                final CellRef start = mergeCellSorted.getStart();
                CellRef end = mergeCellSorted.getEnd();
                String mergeType = null;                                            // default for simple merge

                // first we will check for horizontal merges....
                if(mergeCellSorted.getTopRow()==mergeCellSorted.getBottomRow()) {                           // horizontal merge requires that top and bottom row is equal
                    int count = 1;
                    int r = mergeCellSorted.getTopRow();
                    while(true) {
                        final HashMap<Integer, MergeCellWithDirectRefAccess> columnMap = rowMap.get(r+1);
                        if(columnMap==null) {
                            break;
                        }
                        final MergeCellWithDirectRefAccess compare = columnMap.get(mergeCellSorted.getLeftColumn());
                        if(compare==null||!compare.isAlive()) {
                            break;
                        }
                        if(compare.getRightColumn()!=mergeCellSorted.getRightColumn()) {
                            break;
                        }
                        if(compare.getTopRow()!=compare.getBottomRow()) {
                            break;
                        }
                        end = compare.getEnd();
                        compare.kill();
                        count++;
                        r++;
                    }
                    if (count>1) {
                        mergeType = "horizontal";
                    }
                }
                // check for vertical merges... (only if not already a horizontal merge was found)
                if(mergeType==null&&mergeCellSorted.getLeftColumn()==mergeCellSorted.getRightColumn()) {    // vertical merge requires that left and right column is equal
                    int count = 1;
                    int c = mergeCellSorted.getLeftColumn();
                    while(true) {
                        final HashMap<Integer, MergeCellWithDirectRefAccess> columnMap = rowMap.get(mergeCellSorted.getTopRow());
                        // columnMap can't be zero as we are part of it
                        final MergeCellWithDirectRefAccess compare = columnMap.get(c+1);
                        if(compare==null||!compare.isAlive()) {
                            break;
                        }
                        if(compare.getBottomRow()!=mergeCellSorted.getBottomRow()) {
                            break;
                        }
                        if(compare.getLeftColumn()!=compare.getRightColumn()) {
                            break;
                        }
                        end = compare.getEnd();
                        compare.kill();
                        count++;
                        c++;
                    }
                    if (count>1) {
                        mergeType = "vertical";
                    }
                }
                addMergeCells(operationsArray, sheetIndex, mergeType, start, end);
            }
        }
    }

    public static void addMergeCells(JSONArray operationsArray, int sheetIndex, String mergeType, CellRef start, CellRef end)
        throws JSONException {

        final JSONObject addMergeCellsObject = new JSONObject(5);
        addMergeCellsObject.put(OCKey.NAME.value(), OCValue.MERGE_CELLS.value());
        addMergeCellsObject.put(OCKey.SHEET.value(), sheetIndex);
        addMergeCellsObject.put(OCKey.RANGES.value(), CellRefRange.getCellRefRange(new CellRefRange(start, end)));
        addMergeCellsObject.putOpt(OCKey.TYPE.value(), mergeType==null ? "merge" : mergeType);
        operationsArray.put(addMergeCellsObject);
    }
}
