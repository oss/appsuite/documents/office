/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.operations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import jakarta.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLStreamException;
import org.docx4j.XmlUtils;
import org.docx4j.dml.CTTextField;
import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.DefaultXmlPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.DrawingML.Drawing;
import org.docx4j.openpackaging.parts.SpreadsheetML.JaxbSmlPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.SpreadsheetCommentsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.ThreadedCommentsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.docx4j.relationships.Relationships;
import org.docx4j.vml.VmlCore;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.spreadsheetDrawing.STObjectType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.BookViews;
import org.xlsx4j.sml.CTAutoFilter;
import org.xlsx4j.sml.CTBookView;
import org.xlsx4j.sml.CTCalcPr;
import org.xlsx4j.sml.CTCellFormula;
import org.xlsx4j.sml.CTCellStyle;
import org.xlsx4j.sml.CTCellStyleXfs;
import org.xlsx4j.sml.CTCellStyles;
import org.xlsx4j.sml.CTCellXfs;
import org.xlsx4j.sml.CTChartsheet;
import org.xlsx4j.sml.CTComment;
import org.xlsx4j.sml.CTComments;
import org.xlsx4j.sml.CTDefinedName;
import org.xlsx4j.sml.CTExtension;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTHyperlink;
import org.xlsx4j.sml.CTHyperlinks;
import org.xlsx4j.sml.CTLegacyDrawing;
import org.xlsx4j.sml.CTMergeCell;
import org.xlsx4j.sml.CTMergeCells;
import org.xlsx4j.sml.CTRElt;
import org.xlsx4j.sml.CTRPrElt;
import org.xlsx4j.sml.CTSheetFormatPr;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.CTTablePart;
import org.xlsx4j.sml.CTTableParts;
import org.xlsx4j.sml.CTXf;
import org.xlsx4j.sml.CTXstringWhitespace;
import org.xlsx4j.sml.Cell;
import org.xlsx4j.sml.Col;
import org.xlsx4j.sml.Cols;
import org.xlsx4j.sml.DefinedNames;
import org.xlsx4j.sml.ISheetAccess;
import org.xlsx4j.sml.Row;
import org.xlsx4j.sml.STCellFormulaType;
import org.xlsx4j.sml.STCellType;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.SheetData;
import org.xlsx4j.sml.Sheets;
import org.xlsx4j.sml.Workbook;
import org.xlsx4j.sml.WorkbookPr;
import org.xlsx4j.sml.Worksheet;
import org.xlsx4j.sml_2018.CTMention;
import org.xlsx4j.sml_2018.CTPerson;
import org.xlsx4j.sml_2018.CTPersonList;
import org.xlsx4j.sml_2018.CTThreadedComment;
import org.xlsx4j.sml_2018.CTThreadedCommentMentions;
import org.xlsx4j.sml_2018.CTThreadedComments;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ColumnRef;
import com.openexchange.office.filter.core.spreadsheet.ISpreadsheet;
import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IParagraph;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.VMLBase;
import com.openexchange.office.filter.ooxml.components.VMLRootComponent;
import com.openexchange.office.filter.ooxml.drawingml.ChartWrapper;
import com.openexchange.office.filter.ooxml.drawingml.DMLGraphic;
import com.openexchange.office.filter.ooxml.operations.ApplyOperationHelper;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;
import com.openexchange.office.filter.ooxml.xlsx.components.RootComponent;
import com.openexchange.office.filter.ooxml.xlsx.components.ShapeGraphicComponent;
import com.openexchange.office.filter.ooxml.xlsx.tools.AutoFilterHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.CellUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.ColumnUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;
import com.openexchange.office.filter.ooxml.xlsx.tools.RowUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.SheetUtils;
import com.openexchange.office.filter.ooxml.xlsx.tools.TableHelper;
import com.openexchange.office.filter.ooxml.xlsx.tools.Utils;

public class XlsxApplyOperationHelper extends ApplyOperationHelper implements ISpreadsheet<Sheet> {

    private final static Logger log = LoggerFactory.getLogger(ApplyOperationHelper.class);

    private final XlsxOperationDocument operationDocument;
    private final SpreadsheetMLPackage spreadsheetMLPackage;
    private final WorkbookPart workbookPart;
    private final Map<String, CTXf> styleXfMap;
    private final Map<String, Long> styleIdToIndex = new HashMap<String, Long>();

    public XlsxApplyOperationHelper(XlsxOperationDocument _operationDocument) {
        super();

        operationDocument = _operationDocument;
        spreadsheetMLPackage = _operationDocument.getPackage();
        workbookPart = spreadsheetMLPackage.getWorkbookPart();
        styleXfMap = BuiltinIds.getUsedStyles(operationDocument.getStylesheet(false), null, styleIdToIndex);
    }

    @Override
    public XlsxOperationDocument getOperationDocument() {
        return operationDocument;
    }

    @Override
    public DLList<Sheet> getContent() {
        return workbookPart.getJaxbElement().getSheets().getContent();
    }

    private CTCellFormula createCellFormula(Cell cell, STCellFormulaType type) {
        final CTCellFormula cellFormula = Context.getsmlObjectFactory().createCTCellFormula();
        cell.setF(cellFormula);
        if (type != STCellFormulaType.NORMAL) {
            cellFormula.setT(type);
        }
        return cellFormula;
    }

    private CTCellFormula createCellFormula(Cell cell) {
        return createCellFormula(cell, STCellFormulaType.NORMAL);
    }

    public void setCellContent(Row row, int cellIndex, Object o) {
        if(o instanceof JSONObject) {
            final JSONObject cellObject = (JSONObject)o;
            if(cellObject.optBoolean(OCKey.U.value(), false)) {
                row.clearCellRange(cellIndex, 1);
                if(cellObject.length()==1) {
                    return;
                }
            }
            final Cell cell = row.getCell(cellIndex, true);

            final Object v = cellObject.opt(OCKey.V.value());
            final Object e = cellObject.opt(OCKey.E.value());

            if (v instanceof String) {
                cell.setT(STCellType.STR);
                cell.setV((String)v);
                cell.setIs(null);
            }
            else if (v instanceof Boolean) {
                cell.setT(STCellType.B);
                cell.setV(((Boolean)v).booleanValue() ? "1" : "0");
                cell.setIs(null);
            }
            else if (v instanceof Number) {
                cell.setT(STCellType.N);
                cell.setV(((Number)v).toString());
                cell.setIs(null);
            }
            else if (v==JSONObject.NULL) {
                cell.setT(null);
                cell.setV(null);
                cell.setIs(null);
            }
            else if (e instanceof String) {
                cell.setT(STCellType.E);
                cell.setV((String)e);
                cell.setIs(null);
            }

            CTCellFormula formula = cell.getF();
            final Object jsonF = cellObject.opt(OCKey.F.value());
            final Object jsonSi = cellObject.opt(OCKey.SI.value());
            final Object jsonSr = cellObject.opt(OCKey.SR.value());
            final Object jsonMr = cellObject.opt(OCKey.MR.value());

            // the effective (old or new) formula expression
            final String f =
                (jsonF instanceof String) ? (String)jsonF :
                (jsonF == JSONObject.NULL) ? null :
                (formula != null) ? formula.getValue() :
                null;

            // the effective (old or new) shared index:
            // - always delete the shared index when setting a matrix formula (property "mr")
            // - "si" with "sr" represents the anchor cell of a shared formula
            // - "si" without "sr" represents any other child of a shared formula
            Long si =
                (jsonMr instanceof String) ? null :
                (jsonSi instanceof Number) ? (Long)((Number)jsonSi).longValue() :
                (jsonSi == JSONObject.NULL) ? null :
                (formula != null) ? formula.getSi() :
                null;

            // the effective (old or new) bounding range of a shared formula or matrix formula:
            // - both operation properties "sr" and "mr" will be mapped to the attribute @ref of the <formula> element
            // - if both properties are contained in the operation, string value wins over null value
            final String ref =
                (jsonMr instanceof String) ? (String)jsonMr :
                (jsonSr instanceof String) ? (String)jsonSr :
                (jsonMr == JSONObject.NULL || jsonSr == JSONObject.NULL) ? null :
                (formula != null) ? formula.getRef() :
                null;

            // whether the cell becomes or remains a formula cell
            final boolean isFormula = (f != null) || (si != null) || (ref != null);

            // create a <formula> element on demand; or remove an existing <formula> element
            if (isFormula) {
                if (formula == null) { formula = createCellFormula(cell); }
                formula.setT((si != null) ? STCellFormulaType.SHARED : (ref != null) ? STCellFormulaType.ARRAY : null);
                formula.setValue(f);
                formula.setSi(si);
                formula.setRef(ref);
            } else if (formula != null) {
                cell.setF(null);
            }

            final String s = cellObject.optString(OCKey.S.value(), null);
            if(s != null) {
                Long index = Utils.parseAutoStyleId(s);
                if (index != null) {
                    cell.setS(index.longValue() == 0 ? null : index);
                }
            }
        }
        else {
            final Cell cell = row.getCell(cellIndex, true);
            if(o == JSONObject.NULL) {
                row.getCell(cellIndex, true);
                cell.setT(null);
                cell.setV(null);
                cell.setIs(null);
            }
            else if(o instanceof String) {
                cell.setT(STCellType.STR);
                cell.setV((String)o);
                cell.setIs(null);
            }
            else if(o instanceof Number) {
                cell.setT(STCellType.N);
                cell.setV(((Number)o).toString());
                cell.setIs(null);
            }
            else if(o instanceof Boolean) {
                cell.setT(STCellType.B);
                cell.setV(((Boolean)o).booleanValue() ? "1" : "0");
                cell.setIs(null);
            }
        }
    }

    public JaxbSmlPart<?> getSheetPart(int sheetIndex) {
        final Sheet sheet = SheetUtils.getSheet(spreadsheetMLPackage, sheetIndex);
        final RelationshipsPart relationshipPart = spreadsheetMLPackage.getWorkbookPart().getRelationshipsPart();
        return (JaxbSmlPart<?>)relationshipPart.getPart(sheet.getId());
    }

    public ISheetAccess getWorksheet(int sheetIndex) {
        return (ISheetAccess)getSheetPart(sheetIndex).getJaxbElement();
    }

    public void changeCells(int sheetIndex, JSONObject contents)
        throws Exception {

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        worksheet.setDimension(null);
        SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            sheetData = Context.getsmlObjectFactory().createSheetData();
            worksheet.setSheetData(sheetData);
        }
        final Iterator<Entry<String, Object>> iter = contents.entrySet().iterator();
        while(iter.hasNext()) {
            final Entry<String, Object> entry = iter.next();
            final CellRefRange cellRefRange = CellRefRange.createCellRefRange(entry.getKey());
            for(int r = cellRefRange.getStart().getRow(); r <= cellRefRange.getEnd().getRow(); r++) {
                final Row row = sheetData.getRow(r, true);
                for(int c = cellRefRange.getStart().getColumn(); c <= cellRefRange.getEnd().getColumn(); c++) {
                    org.docx4j.jaxb.Context.abortOnLowMemory(getOperationDocument().getPackage());
                    setCellContent(row, c, entry.getValue());
                }
            }
        }
    }

    private boolean nullOnlyAttributes(JSONObject attrs) {
        final Iterator<Entry<String, Object>> iter = attrs.entrySet().iterator();
        while(iter.hasNext()) {
            final Entry<String, Object> entry = iter.next();
            if(entry.getValue() instanceof JSONObject) {
                if(nullOnlyAttributes((JSONObject)entry.getValue())==false) {
                    return false;
                }
            }
            else if(entry.getValue()!=JSONObject.NULL) {
                return false;
            }
        }
        return true;
    }

    public void changeRows(int sheetIndex, List<Interval> intervals, JSONObject attrs, String style)
        throws Exception {

        for(Interval interval:intervals) {
            changeRows(sheetIndex, interval, attrs, style);
        }
    }

    public void changeRows(int sheetIndex, Interval interval, JSONObject attrs, String style)
        throws Exception {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            sheetData = Context.getsmlObjectFactory().createSheetData();
            worksheet.setSheetData(sheetData);
        }

        final boolean nullOnly = (attrs != null) && nullOnlyAttributes(attrs);
        for(int y=start; y<=end; y++) {
            final Row row = sheetData.getRow(y, !nullOnly);
            org.docx4j.jaxb.Context.abortOnLowMemory(getOperationDocument().getPackage());
            if(row!=null) {
                RowUtils.applyRowProperties(attrs, style, row);
                sheetData.applyCachedRowData(row);
            }
        }

        // applying rowHidden attribute to comments if necessary
        if(attrs!=null) {
            final JSONObject optRowAttributes = attrs.optJSONObject(OCKey.ROW.value());
            if(optRowAttributes!=null) {
                final Object v = optRowAttributes.opt(OCKey.VISIBLE.value());
                if(v!=null) {
                    updateCommentVisibility(worksheetPart, new CellRefRange(0, start, 16383, end));
                }
            }
        }
    }

    public void changeColumns(int sheetIndex, List<Interval> intervals, JSONObject attrs, String style)
        throws Exception {

        for(Interval interval:intervals) {
            changeColumns(sheetIndex, interval, attrs, style);
        }
    }

    public void changeColumns(int sheetIndex, Interval interval, JSONObject attrs, String style)
        throws Exception {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        final List<Cols> colsList = worksheet.getCols();
        if(colsList.size()==0) {
            colsList.add(Context.getsmlObjectFactory().createCols());
        }

        // TODO: we only apply our attributes to the first Cols within the cols list, so additional
        // entries have to be merged ...
        start++;
        end++;

        final Cols cols = colsList.get(0);
        Utils.createColumnAttributeRanges(cols, Long.valueOf(start), Long.valueOf(end-start+1));
        for(final Col col:cols.getCol()) {
            if(col.getMin()>end)
                break;
            if(col.getMax()<start)
                continue;
            ColumnUtils.applyColumnProperties(attrs, style, col, worksheet.getBaseColWidth());
        }

        // applying colHidden attribute to comments if necessary
        if(attrs!=null) {
            final JSONObject optColumnAttributes = attrs.optJSONObject(OCKey.COLUMN.value());
            if(optColumnAttributes!=null) {
                final Object v = optColumnAttributes.opt(OCKey.VISIBLE.value());
                if(v!=null) {
                    updateCommentVisibility(worksheetPart, new CellRefRange(start-1, 0, end-1, 1048575));
                }
            }
        }
    }

    private void updateCommentVisibility(WorksheetPart worksheetPart, CellRefRange columnRowRange)
        throws InvalidFormatException {

        final VMLPart vmlPart = worksheetPart.getVMLPart(false);
        if(vmlPart!=null) {
            IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
            while(component!=null) {
                if(component instanceof VMLBase) {
                    final VmlCore vmlCore = ((VMLBase)component).getVMLCoreObject();
                    final CTClientData clientData = vmlCore.getClientData(false);
                    if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                        final CellRef cellRef = clientData.getCellRef(false);
                        if(cellRef!=null) {
                            CellRefRange cellRefRange = null;
                            final Worksheet worksheet = worksheetPart.getJaxbElement();
                            final CTMergeCells mergeCells = worksheet.getMergeCells();
                            if(mergeCells!=null) {
                                for(CTMergeCell mergeCell:mergeCells.getMergeCell()) {
                                    final CellRefRange mergeCellRange = mergeCell.getCellRefRange(true);
                                    if(mergeCellRange.intersects(columnRowRange)) {
                                        if(mergeCellRange.getStart().equals(cellRef)) {
                                            cellRefRange = mergeCellRange;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(cellRefRange==null&&columnRowRange.isInside(cellRef.getColumn(), cellRef.getRow())) {
                                cellRefRange = new CellRefRange(cellRef, cellRef);
                            }
                            if(cellRefRange!=null) {
                                boolean first = true;
                                Boolean isVisible = null;
                                for(int row = cellRefRange.getStart().getRow(); isVisible==null && row <= cellRefRange.getEnd().getRow(); row++) {
                                    for(int column = cellRefRange.getStart().getColumn(); isVisible==null && column <= cellRefRange.getEnd().getColumn(); column++) {
                                        final boolean isRowVisible = worksheet.isRowVisible(row);
                                        final boolean isColumnVisible = worksheet.isColumnVisible(column);
                                        if(first) {
                                            clientData.setRowHidden(!isRowVisible?new String():null);
                                            clientData.setColHidden(!isColumnVisible?new String():null);
                                        }
                                        if(isRowVisible&&isColumnVisible) {
                                            isVisible = Boolean.TRUE;
                                        }
                                    }
                                }
                                vmlCore.getStyleMap().put("visibility", Boolean.TRUE==isVisible ? "visible" : "hidden");
                            }
                        }
                    }
                }
                component = component.getNextComponent();
            }
        }
    }

    public void insertRows(int sheetIndex, List<Interval> intervals, JSONObject attrs, String style)
        throws Exception {

        for(Interval interval:intervals) {
            insertRows(sheetIndex, interval, attrs, style);
        }
    }

    public void insertRows(int sheetIndex, Interval interval, JSONObject attrs, String style)
        throws Exception {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();
        int count = end-start+1;

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        worksheet.setDimension(null);
        SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            sheetData = Context.getsmlObjectFactory().createSheetData();
            worksheet.setSheetData(sheetData);
        }
        sheetData.insertRows(start, count);

        // set attributes and auto-style passed with the operation
        if (attrs!=null || style!=null) {
            changeRows(sheetIndex, interval, attrs, style);
        }

        // taking care of merged cells
        final CTMergeCells mergeCells = worksheet.getMergeCells();
        if(mergeCells!=null) {
            CellRefRange.insertRows(mergeCells.getMergeCell(), start, count, false, 1048576);
            if(mergeCells.getMergeCell().isEmpty()) {
                worksheet.setMergeCells(null);
            }
        }
        // taking care of hyperlinks
        final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
        if(hyperlinks!=null) {
            CellRefRange.insertRows(hyperlinks.getHyperlink(), start, count, false, null);
            if(hyperlinks.getHyperlink().isEmpty()) {
                worksheet.setHyperlinks(null);
            }
        }
        // taking care of tables
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
            final List<CTTablePart> tablePartList = tableParts.getTablePart();
            for(int i=tablePartList.size()-1;i>=0;i--) {
                TableHelper.insertRows(worksheetPart, tablePartList.get(i), start, count);
            }
        }
        // taking care of comments
        final CTComments comments = worksheetPart.getComments(false);
        if(comments!=null) {
            CellRef.insertRows(comments.getCommentList().getComment(), start, count);
            final VMLPart vmlPart = worksheetPart.getVMLPart(false);
            if(vmlPart!=null) {
                IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
                while(component!=null) {
                    final IComponent<OfficeOpenXMLOperationDocument> nextComponent = component.getNextComponent();
                    if(component instanceof VMLBase) {
                        final VmlCore vmlCore = ((VMLBase)component).getVMLCoreObject();
                        final CTClientData clientData = vmlCore.getClientData(false);
                        if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                            if(CellRef.insertRows(clientData.getCellRef(false), start, count)) {
                                component.delete(1);
                            }
                            else {
                                if(clientData.getMoveWithCells()==null) {
                                    final int[] anchor = clientData.getAnchorArray(false);
                                    if(anchor!=null) {
                                        if(anchor[6]>=start) {
                                            anchor[6]+=count;
                                            anchor[6] = Math.min(anchor[6], 1048575);
                                            clientData.setSizeWithCells(new String());
                                            if(anchor[2]>=start) {
                                                anchor[2]+=count;
                                                anchor[2] = Math.min(anchor[2], 1048575);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    component = nextComponent;
                }
            }
        }

        // taking care of threaded comments
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);

        if(threadedComments!=null) {
            CellRef.insertRows(threadedComments.getThreadedComments(), start, count);
            worksheetPart.deleteThreadedCommentsPartIfEmpty();
        }

        handleInsertDeleteColRow(sheetIndex, start, count, false, 1);
    }

    public void deleteRows(int sheetIndex, List<Interval> intervals)
        throws InvalidFormatException {

        for(Interval interval:intervals) {
            deleteRows(sheetIndex, interval);
        }
    }

    public void deleteRows(int sheetIndex, Interval interval)
        throws InvalidFormatException {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();
        int count = end-start+1;

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        worksheet.setDimension(null);
        final SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            log.warn("xlsx export: deleteRows, no sheetData available");
            return;
        }
        sheetData.deleteRows(start, count);

        // taking care of merged cells
        final CTMergeCells mergeCells = worksheet.getMergeCells();
        if(mergeCells!=null) {
            CellRefRange.deleteRows(mergeCells.getMergeCell(), start, count, true, 1048576);
            if(mergeCells.getMergeCell().isEmpty()) {
                worksheet.setMergeCells(null);
            }
        }
        // taking care of hyperlinks
        final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
        if(hyperlinks!=null) {
            CellRefRange.deleteRows(hyperlinks.getHyperlink(), start, count, false, null);
            if(hyperlinks.getHyperlink().isEmpty()) {
                worksheet.setHyperlinks(null);
            }
        }
        // taking care of tables
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
            final List<CTTablePart> tablePartList = tableParts.getTablePart();
            for(int i=tablePartList.size()-1;i>=0;i--) {
                TableHelper.deleteRows(worksheetPart, tablePartList.get(i), start, count);
            }
        }
        // taking care of comments
        final CTComments comments = worksheetPart.getComments(false);
        if(comments!=null) {
            CellRef.deleteRows(comments.getCommentList().getComment(), start, count);;
            final VMLPart vmlPart = worksheetPart.getVMLPart(false);
            if(vmlPart!=null) {
                IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
                while(component!=null) {
                    final IComponent<OfficeOpenXMLOperationDocument> nextComponent = component.getNextComponent();
                    if(component instanceof VMLBase) {
                        final VmlCore vmlCore = ((VMLBase)component).getVMLCoreObject();
                        final CTClientData clientData = vmlCore.getClientData(false);
                        if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                            if(CellRef.deleteRows(clientData.getCellRef(false), start, count)) {
                                component.delete(1);
                            }
                            else {
                                if(clientData.getMoveWithCells()==null) {
                                    final int[] anchor = clientData.getAnchorArray(false);
                                    if(anchor!=null) {
                                        if(start<=anchor[6]) {
                                            anchor[6]-=count;
                                            clientData.setSizeWithCells(new String());
                                            if(start<=anchor[2]) {
                                                anchor[2]-=count;
                                                anchor[2] = Math.max(anchor[2], start);
                                            }
                                            anchor[6] = Math.max(anchor[6], anchor[2]);
                                        }
                                    }
                                    clientData.setSizeWithCells(new String());
                                }
                            }
                        }
                    }
                    component = nextComponent;
                }
            }
        }

        // taking care of threaded comments
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);
        if(threadedComments != null) {
            CellRef.deleteRows(threadedComments.getThreadedComments(), start, count);
            worksheetPart.deleteThreadedCommentsPartIfEmpty();
        }

        handleInsertDeleteColRow(sheetIndex, start, count, false, -1);
    }

    public void insertColumns(int sheetIndex, List<Interval> intervals, JSONObject attrs, String style)
        throws Exception {

        for(Interval interval:intervals) {
            insertColumns(sheetIndex, interval, attrs, style);
        }
    }

    public void insertColumns(int sheetIndex, Interval interval, JSONObject attrs, String style)
        throws Exception {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();
        int count = end-start+1;

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        worksheet.setDimension(null);
        SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            sheetData = Context.getsmlObjectFactory().createSheetData();
            worksheet.setSheetData(sheetData);
        }
        sheetData.insertColumns(start, count);

        // move following columns (XML uses one-based indexes!)
        int cstart = start + 1;
        final List<Cols> cols = worksheet.getCols();
        for(int ci = cols.size()-1; ci>=0; ci--) {
            final List<Col> colList = cols.get(ci).getCol();
            for (int i=colList.size()-1; i>=0; --i) {
                final Col col = colList.get(i);
                long min = col.getMin();
                long max = col.getMax();
                if (cstart<=min) {
                    // column will be moved completely (or deleted if it shifts outside the sheet)
                    min += count;
                    max = Math.min(max+count, 16384);
                    if (min<=max) {
                        col.setMin(min);
                        col.setMax(max);
                    } else {
                        colList.remove(i);
                        if(colList.isEmpty()) {
                            cols.remove(ci);
                        }
                    }
                } else if (cstart<=max) {
                    // column will be split into two parts
                    col.setMax(cstart-1);
                    final Col newCol = col.clone();
                    min = cstart+count;
                    max = Math.min(max+count, 16384);
                    if (min<=max) {
                        newCol.setMin(min);
                        newCol.setMax(max);
                        colList.add(i+1, newCol);
                    }
                }
            }
        }

        // taking care of merged cells
        final CTMergeCells mergeCells = worksheet.getMergeCells();
        if(mergeCells!=null) {
            CellRefRange.insertColumns(mergeCells.getMergeCell(), start, count, false, 16384);
            if(mergeCells.getMergeCell().isEmpty()) {
                worksheet.setMergeCells(null);
            }
        }
        // taking care of hyperlinks
        final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
        if(hyperlinks!=null) {
            CellRefRange.insertColumns(hyperlinks.getHyperlink(), start, count, false, null);
            if(hyperlinks.getHyperlink().isEmpty()) {
                worksheet.setHyperlinks(null);
            }
        }
        // taking care of tables
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
            final List<CTTablePart> tablePartList = tableParts.getTablePart();
            for(int i=tablePartList.size()-1;i>=0;i--) {
                TableHelper.insertColumns(worksheetPart, tablePartList.get(i), start, count);
            }
        }
        // taking care of comments
        final CTComments comments = worksheetPart.getComments(false);
        if(comments!=null) {
            CellRef.insertColumns(comments.getCommentList().getComment(), start, count);;
            final VMLPart vmlPart = worksheetPart.getVMLPart(false);
            if(vmlPart!=null) {
                IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
                while(component!=null) {
                    final IComponent<OfficeOpenXMLOperationDocument> nextComponent = component.getNextComponent();
                    if(component instanceof VMLBase) {
                        final VmlCore vmlCore = ((VMLBase)component).getVMLCoreObject();
                        final CTClientData clientData = vmlCore.getClientData(false);
                        if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                            if(CellRef.insertColumns(clientData.getCellRef(false), start, count)) {
                                component.delete(1);
                            }
                            else {
                                if(clientData.getMoveWithCells()==null) {
                                    final int[] anchor = clientData.getAnchorArray(false);
                                    if(anchor!=null) {
                                        if(anchor[4]>=start) {
                                            anchor[4]+=count;
                                            anchor[4] = Math.min(anchor[4], 16383);
                                            clientData.setSizeWithCells(new String());
                                            if(anchor[0]>=start) {
                                                anchor[0]+=count;
                                                anchor[0] = Math.min(anchor[0], 16383);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    component = nextComponent;
                }
            }
        }

        // taking care of threaded comments
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);
        if(threadedComments!=null) {
            CellRef.insertColumns(threadedComments.getThreadedComments(), start, count);
            worksheetPart.deleteThreadedCommentsPartIfEmpty();
        }

        handleInsertDeleteColRow(sheetIndex, start, count, true, 1);

        // set attributes and auto-style passed with the operation
        if (attrs!=null || style!=null) {
            changeColumns(sheetIndex, new Interval(new ColumnRef(start), new ColumnRef(end)), attrs, style);
        }
    }

    public void deleteColumns(int sheetIndex, List<Interval> intervals)
        throws InvalidFormatException {

        for(Interval interval:intervals) {
            deleteColumns(sheetIndex, interval);
        }
    }

    public void deleteColumns(int sheetIndex, Interval interval)
        throws InvalidFormatException {

        int start = interval.getMin().getValue();
        int end = interval.getMax().getValue();
        int count = end-start+1;

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        worksheet.setDimension(null);
        final SheetData sheetData = worksheet.getSheetData();
        if(sheetData==null) {
            log.warn("xlsx export: deleteColumns, no sheetData available");
            return;
        }
        sheetData.deleteColumns(start, count);

        // taking care of column attributes
        final List<Cols> cols = worksheet.getCols();
        for(int ci = cols.size()-1; ci>=0; ci--) {
            final List<Col> colList = cols.get(ci).getCol();
            final int cstart = start + 1;
            final int cend = start + count;
            for(int i = colList.size()-1; i>=0; i--) {

                final Col col = colList.get(i);
                long min = col.getMin();
                long max = col.getMax();

                if(max<cstart) {
                    break;
                }
                if(min>cend) {
                    col.setMin(min-count);
                    col.setMax(max-count);
                }
                else if (min>=cstart&&max<=cend) {
                    colList.remove(i);
                    if(colList.isEmpty()) {
                        cols.remove(ci);
                    }
                }
                else if (cstart>=min&&cend<=max) {
                    col.setMax(max-count);
                }
                else if (cend>max) {
                    col.setMax(cstart-1);
                }
                else if (cstart<min) {
                    col.setMin(cstart);
                    col.setMax(max-count);
                }
            }
        }

        // taking care of merged cells
        final CTMergeCells mergeCells = worksheet.getMergeCells();
        if(mergeCells!=null) {
            CellRefRange.deleteColumns(mergeCells.getMergeCell(), start, count, true, 16384);
            if(mergeCells.getMergeCell().isEmpty()) {
                worksheet.setMergeCells(null);
            }
        }
        // taking care of hyperlinks
        final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
        if(hyperlinks!=null) {
            CellRefRange.deleteColumns(hyperlinks.getHyperlink(), start, count, false, null);
            if(hyperlinks.getHyperlink().isEmpty()) {
                worksheet.setHyperlinks(null);
            }
        }
        // taking care of tables
        final CTTableParts tableParts = worksheet.getTableParts(false);
        if(tableParts!=null) {
            final List<CTTablePart> tablePartList = tableParts.getTablePart();
            for(int i=tablePartList.size()-1;i>=0;i--) {
                TableHelper.deleteColumns(worksheetPart, tablePartList.get(i), start, count);
            }
        }
        // taking care of comments
        final CTComments comments = worksheetPart.getComments(false);
        if(comments!=null) {
            CellRef.deleteColumns(comments.getCommentList().getComment(), start, count);;
            final VMLPart vmlPart = worksheetPart.getVMLPart(false);
            if(vmlPart!=null) {
                IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
                while(component!=null) {
                    final IComponent<OfficeOpenXMLOperationDocument> nextComponent = component.getNextComponent();
                    if(component instanceof VMLBase) {
                        final VmlCore vmlCore = ((VMLBase)component).getVMLCoreObject();
                        final CTClientData clientData = vmlCore.getClientData(false);
                        if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                            if(CellRef.deleteColumns(clientData.getCellRef(false), start, count)) {
                                component.delete(1);
                            }
                            else {
                                if(clientData.getMoveWithCells()==null) {
                                    final int[] anchor = clientData.getAnchorArray(false);
                                    if(anchor!=null) {
                                        if(start<=anchor[4]) {
                                            anchor[4]-=count;
                                            clientData.setSizeWithCells(new String());
                                            if(start<=anchor[0]) {
                                                anchor[0]-=count;
                                                anchor[0] = Math.max(anchor[0], start);
                                            }
                                            anchor[4] = Math.max(anchor[4], anchor[0]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    component = nextComponent;
                }
            }
        }

        // taking care of threaded comments
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);
        if(threadedComments != null) {
            CellRef.deleteColumns(threadedComments.getThreadedComments(), start, count);
            worksheetPart.deleteThreadedCommentsPartIfEmpty();
        }

        handleInsertDeleteColRow(sheetIndex, start, count, true, -1);
    }

    private void handleInsertDeleteColRow(int sheetIndex, int start, int count, boolean column, int rel){

        final Workbook workbook = workbookPart.getJaxbElement();
        final List<Sheet> allSheets = workbook.getSheets().getContent();
        final RelationshipsPart workbookRelationshipsPart = workbookPart.getRelationshipsPart();

        for (int i = 0; i < allSheets.size(); i++) {
            final Sheet sheet = allSheets.get(i);
            final Part part = workbookRelationshipsPart.getPart(sheet.getId());
            if(part instanceof WorksheetPart) {
                final WorksheetPart sheetPart = (WorksheetPart)part;
                final RelationshipsPart sheetRelationshipsPart = sheetPart.getRelationshipsPart();
                if(sheetRelationshipsPart!=null) {
                    final Worksheet worksheet = sheetPart.getJaxbElement();

                    if(sheetIndex==i) {

                        // check autoFilter
                        final CTAutoFilter autoFilter = worksheet.getAutoFilter(false);
                        if(autoFilter!=null) {
                            if(AutoFilterHelper.handleAutoFilter(autoFilter, start, count, column, rel)==null) {
                                worksheet.setAutoFilter(null);
                            }
                        }
                    }

                    // for each worksheet -->
                }
            }
        }
    }

    public void insertSheet(int sheetIndex, String sheetName, JSONObject attrs)
        throws InvalidFormatException, JSONException {

        // first we have to get a free relationship that can be used for our new sheet
        int sheetNumber = 1;

        Relationship relShip = null;
        PartName partName = null;
        do {
            partName = new PartName("/xl/worksheets/sheet" + Integer.toString(sheetNumber++) + ".xml");
            relShip = workbookPart.getRelationshipsPart().getRel(partName);
        }
        while(relShip!=null);

        // now we have a free partName... and we will look for a free SheetId
        final Workbook workbook = workbookPart.getJaxbElement();
        final Sheets sheets = workbook.getSheets();
        final List<Sheet> sheetList = sheets.getContent();

        long sheetId = 1;
        do {
            boolean idFound = false;
            for(int i = 0; i < sheetList.size(); i++) {
                if(sheetList.get(i).getSheetId()==sheetId) {
                    idFound = true;
                    break;
                }
            }
            if(idFound==false)
                break;
            sheetId++;
        }
        while(true);

        final WorksheetPart worksheetPart = spreadsheetMLPackage.createWorksheetPart(partName, sheetName, sheetId);
        final Worksheet worksheet = worksheetPart.getJaxbElement();
        CTSheetFormatPr sheetFormatPr = worksheet.getSheetFormatPr();
        if(sheetFormatPr==null) {
            sheetFormatPr = Context.getsmlObjectFactory().createCTSheetFormatPr();
            worksheet.setSheetFormatPr(sheetFormatPr);
            sheetFormatPr.setBaseColWidth(new Long(10L));
            sheetFormatPr.setDefaultRowHeight(14.5d);
        }

        // setting the correct index position
        final Sheet sheet = sheetList.remove(sheetList.size() - 1);
        sheetList.add(sheetIndex, sheet);
        if(attrs!=null) {
            SheetUtils.applySheetProperties(workbook, sheetIndex, worksheet, attrs.optJSONObject(OCKey.SHEET.value()));
            SheetUtils.applyWorksheetColumnProperties(operationDocument, worksheet, attrs.optJSONObject(OCKey.COLUMN.value()));
            SheetUtils.applyWorksheetRowProperties(operationDocument, worksheet, attrs.optJSONObject(OCKey.ROW.value()));
        }

        SheetUtils.updateBookView(workbook);

        // correcting definedName local sheet ids
        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames!=null) {
            for(final CTDefinedName definedName:definedNames.getDefinedName()) {
                final Long localSheetId = definedName.getLocalSheetId();
                if(localSheetId!=null) {
                    if(localSheetId>=sheetIndex) {
                        definedName.setLocalSheetId(localSheetId+1);
                    }
                }
            }
        }
    }

    @Override
    public void moveSheet(int sheetIndex, int to) {
        final Workbook workbook = workbookPart.getJaxbElement();
        final Sheets sheets = workbook.getSheets();
        final List<Sheet> sheetList = sheets.getContent();
        sheetList.add(to, sheetList.remove(sheetIndex));

        SheetUtils.updateBookView(workbook);

        // correcting definedName local sheet ids
        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames!=null) {
            final List<CTDefinedName> definedNameList = definedNames.getDefinedName();
            for(final CTDefinedName definedName:definedNameList) {
                final Long localSheetId = definedName.getLocalSheetId();
                if(localSheetId!=null) {
                    long newSheetId;
                    if(localSheetId==sheetIndex) {
                        newSheetId = to;
                    }
                    else {
                        newSheetId = localSheetId;
                        if(sheetIndex<newSheetId) {
                            newSheetId--;
                        }
                        if(newSheetId>=to) {
                            newSheetId++;
                        }
                    }
                    definedName.setLocalSheetId(newSheetId);
                }
            }
        }
    }

    public void deleteSheet(int sheetIndex) {

        final Workbook workbook = workbookPart.getJaxbElement();
        final Sheets sheets = workbook.getSheets();
        final List<Sheet> sheetList = sheets.getContent();
        final Sheet sheet = sheetList.remove(sheetIndex);
        final String relId = sheet.getId();
        final RelationshipsPart relationshipsPart = workbookPart.getRelationshipsPart();
        relationshipsPart.removeRelationship(relationshipsPart.getRelationshipByID(relId));

        SheetUtils.updateBookView(workbook);

        // correcting definedName local sheet ids
        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames!=null) {
            final List<CTDefinedName> definedNameList = definedNames.getDefinedName();
            for(int i=0;i<definedNameList.size();i++) {
                final CTDefinedName definedName = definedNameList.get(i);
                final Long localSheetId = definedName.getLocalSheetId();
                if(localSheetId!=null) {
                    if(localSheetId==sheetIndex) {
                        definedNameList.remove(i--);
                    }
                    else if(localSheetId>sheetIndex) {
                        definedName.setLocalSheetId(localSheetId-1);
                    }
                }
            }
        }
    }

    public void copySheet(int sheet, int to, String sheetName, JSONObject tableNames)
        throws Docx4JException, InvalidFormatException, JAXBException, JSONException, XMLStreamException {

        insertSheet(to, sheetName, null);
        if(to<=sheet) {
            sheet++;
        }
        // create and insert a new copy of our worksheet
        final WorksheetPart sourceWorksheetPart = (WorksheetPart)getSheetPart(sheet);
        final WorksheetPart destWorksheetPart   = (WorksheetPart)getSheetPart(to);
        final Worksheet sourceWorksheet = sourceWorksheetPart.getJaxbElement();
        final Worksheet destWorksheet = XmlUtils.deepCopy(sourceWorksheet, getOperationDocument().getPackage(), Context.getJcSML());
        destWorksheetPart.setJaxbElement(destWorksheet);

        // now we have to clone the relationships
        final Set<String> types = ImmutableSet.<String>builder()
            .add(Namespaces.SPREADSHEETML_DRAWING)
            .add(Namespaces.SPREADSHEETML_TABLE)
            .add(Namespaces.SPREADSHEETML_COMMENTS)
            .add(Namespaces.VML)
            .add(Namespaces.SPREADSHEETML_THREADED_COMMENTS)
            .build();
        final Set<String> ignoreTypes = ImmutableSet.<String>builder()
                .add("http://schemas.microsoft.com/office/2007/relationships/slicer")
                .build();

        cloneRelationshipsPart(destWorksheetPart, sourceWorksheetPart, types, ignoreTypes);

        final Workbook workbook = workbookPart.getJaxbElement();

        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames!=null) {
            // duplicating definedNames with localSheetId
            final List<CTDefinedName> definedNameList = definedNames.getDefinedName();
            for(int i=definedNameList.size()-1;i>=0;i--) {
                final CTDefinedName sourceName = definedNameList.get(i);
                if(sourceName.getLocalSheetId()!=null&&sourceName.getLocalSheetId().longValue()==sheet) {
                    final CTDefinedName destName = sourceName.clone();
                    destName.setLocalSheetId(Long.valueOf(to));
                    definedNameList.add(destName);
                }
            }
        }

        final CTExtensionList extLst = destWorksheet.getExtLst(false);
        if(extLst!=null) {
            final CTExtension ext = extLst.getExtensionByUri("{A8765BA9-456A-4dab-B4F3-ACF838C121DE}", false);
            if(ext!=null) {
                extLst.getExt().remove(ext);
            }
        }

        if(tableNames!=null) {
            final Iterator<Entry<String, Object>> nameEntryIter = tableNames.entrySet().iterator();
            while(nameEntryIter.hasNext()) {
                final Entry<String, Object> nameEntry = nameEntryIter.next();
                TableHelper.getTable(destWorksheetPart, nameEntry.getKey()).setDisplayName((String)nameEntry.getValue());
            }
        }

        final CTLegacyDrawing legacyDrawing = sourceWorksheet.getLegacyDrawing(false);
        if(legacyDrawing!=null) {
            final RelationshipsPart sourceRelationshipsPart = sourceWorksheetPart.getRelationshipsPart(false);
            if(sourceRelationshipsPart!=null) {
                final Relationship rel = sourceRelationshipsPart.getRelationshipByID(legacyDrawing.getId());
                if(rel!=null) {
                    final RelationshipsPart destRelationshipsPart = destWorksheetPart.getRelationshipsPart(true);
                    destWorksheet.getLegacyDrawing(true).setId(destRelationshipsPart.getRelationshipByType(rel.getType()).getId());
                }
            }
        }

        ThreadedCommentsPart threadedCommentsPart = destWorksheetPart.getThreadedCommentsPart(false);
        if (threadedCommentsPart != null) {
            CTThreadedComments threadedComments = threadedCommentsPart.getJaxbElement();
            List<CTThreadedComment> threadedCommentList = threadedComments.getThreadedComments();

            SpreadsheetCommentsPart commentsPart = destWorksheetPart.getCommentsPart(false);
            CTComments comments = commentsPart.getJaxbElement();
            List<CTComment> commentList = comments.getCommentList().getComment();

            Map<String, String> oldnewId = new  HashMap<String, String>();
            for (CTThreadedComment threadedComment : threadedCommentList) {
                String guid = getGUID();
                oldnewId.put(threadedComment.getId(), guid);

                if (oldnewId.containsKey(threadedComment.getParentId())) {
                    threadedComment.setParentId(oldnewId.get(threadedComment.getParentId()));
                }

                for (CTComment comment: commentList) {
                    if (threadedComment.getId().equals(comment.getGuid())) {
                        comment.setGuid(guid);
                        break;
                    }
                }

                String threadedCommentAuthorId = getThreadedCommentAuthorId(threadedComment.getId());
                List<String> authorList = comments.getAuthors().getAuthor();
                for (int i = 0; i < authorList.size(); i++) {
                    String author = authorList.get(i);

                    if (author.equals(threadedCommentAuthorId)) {
                        authorList.set(i, getThreadedCommentAuthorId(guid));
                        break;
                    }
                }

                threadedComment.setId(guid);
            }
        }

    }

    private void cloneRelationshipsPart(Part destPart, Part sourcePart, Set<String> types, Set<String> ignoreTypes)
        throws Docx4JException, InvalidFormatException, JAXBException, PartUnrecognisedException, XMLStreamException {

        final RelationshipsPart sourceRelationshipsPart = sourcePart.getRelationshipsPart(false);
        if(sourceRelationshipsPart!=null) {
            final Relationships sourceRelationships = sourceRelationshipsPart.getRelationships();
            final List<Relationship> sourceRelationshipList = sourceRelationships.getRelationship();
            for(int relIndex = 0; relIndex<sourceRelationshipList.size(); relIndex++) {
                final Relationship sourceRelationship = sourceRelationshipList.get(relIndex);
                final String sourceType = sourceRelationship.getType();
                final String targetMode = sourceRelationship.getTargetMode();
                if(ignoreTypes==null||!ignoreTypes.contains(sourceType)) {
                    if((targetMode==null||targetMode.equalsIgnoreCase("Internal"))&&!sourceType.equals(Namespaces.HYPERLINK)) {
                        if(types==null||types.contains(sourceType)) {
                            try {
                                final Part part = sourceRelationshipsPart.getPart(sourceRelationship.getId());
                                if(part instanceof SerializationPart) {
                                    cloneXmlPart(destPart, (SerializationPart<?>)part, sourceRelationship.getId());
                                    continue;
                                }
                                else if(part instanceof DefaultXmlPart) {
                                    cloneDefaultXmlPart(destPart, (DefaultXmlPart)part, sourceRelationship.getId());
                                    continue;
                                }
                            }
                            catch(InvalidFormatException e) {
                                //
                            }
                        }
                    }
                    destPart.getRelationshipsPart(true).getRelationships().getRelationship().add(XmlUtils.deepCopy(sourceRelationship, sourcePart.getPackage(), org.docx4j.jaxb.Context.getJcRelationships()));
                }
            }
        }
    }

    public void insertHyperlink(int sheetIndex, CellRefRange[] ranges, String hlink) {
        for(CellRefRange cellRefRange:ranges) {
            final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
            final Worksheet worksheet = worksheetPart.getJaxbElement();
            CTHyperlinks hyperlinks = worksheet.getHyperlinks();
            if(hyperlinks==null) {
                hyperlinks = Context.getsmlObjectFactory().createCTHyperlinks();
                worksheet.setHyperlinks(hyperlinks);
            }
            final List<CTHyperlink> hyperlinkList = hyperlinks.getHyperlink();
            for(int i=hyperlinkList.size()-1;i>=0;i--) {
                final CTHyperlink hyperlink = hyperlinkList.get(i);
                if((hyperlink.getCellRefRange(false)==null)||cellRefRange.contains(hyperlink.getCellRefRange(false))) {
                    hyperlinkList.remove(i);
                }
            }
            final CTHyperlink hyperlink = Context.getsmlObjectFactory().createCTHyperlink();
            hyperlink.setCellRefRange(cellRefRange);
            final String[] parts = hlink.split("#", 2);
            hyperlink.setId(Commons.setUrl(worksheetPart, hyperlink.getId(), parts[0]));
            if (parts.length > 1) {
                hyperlink.setLocation(parts[1]);
            }
            hyperlinks.getHyperlink().add(hyperlink);
        }
    }

    public void deleteHyperlink(int sheetIndex, CellRefRange[] ranges) {
        for(CellRefRange cellRefRange:ranges) {
            final Worksheet worksheet = (Worksheet)getWorksheet(sheetIndex);
            final CTHyperlinks hyperlinks = worksheet.getHyperlinks();
            if(hyperlinks!=null) {
                final List<CTHyperlink> hyperlinkList = hyperlinks.getHyperlink();
                for(int i=hyperlinkList.size()-1;i>=0;i--) {
                    final CTHyperlink hyperlink = hyperlinkList.get(i);
                    if((hyperlink.getCellRefRange(false)==null)||cellRefRange.equals(hyperlink.getCellRefRange(false))) {
                        hyperlinkList.remove(i);
                    }
                }
                if(hyperlinkList.isEmpty()) {
                    worksheet.setHyperlinks(null);
                }
            }
        }
    }

    private void cloneXmlPart(Part destPart, SerializationPart<?> sourcePart, String relationshipId)
        throws Docx4JException, InvalidFormatException, JAXBException, PartUnrecognisedException, XMLStreamException {

        final Part part = spreadsheetMLPackage.getContentTypeManager().newPartForContentType(sourcePart.getContentType(), sourcePart.getPartName(), null);
        if(part instanceof SerializationPart) {
            final SerializationPart<?> clonedPart = (SerializationPart<?>)part;
            clonedPart.setPackage(sourcePart.getPackage());
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            sourcePart.marshal(baos);
            clonedPart.unmarshal(new ByteArrayInputStream(baos.toByteArray()));
            destPart.addTargetPart(clonedPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS, relationshipId);
            cloneRelationshipsPart(clonedPart, sourcePart, null, null);
        }
    }

    private void cloneDefaultXmlPart(Part destPart, DefaultXmlPart sourcePart, String relationshipId)
        throws Docx4JException, JAXBException, XMLStreamException {

        final Document destDoc = (Document) sourcePart.getDocument().cloneNode(true);
        final DefaultXmlPart clonedPart = new DefaultXmlPart(sourcePart.getPartName());
        clonedPart.setContentType(new ContentType(sourcePart.getContentType()));
        clonedPart.setRelationshipType(sourcePart.getRelationshipType());
        destPart.addTargetPart(clonedPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS, relationshipId);
        clonedPart.setDocument(destDoc);
        cloneRelationshipsPart(clonedPart, sourcePart, null, null);
    }

    public void setSheetName(int sheetIndex, String newSheetName) {

        final Workbook workbook = workbookPart.getJaxbElement();
        final Sheet _sheet = workbook.getSheets().getContent().get(sheetIndex);
        _sheet.setName(newSheetName);
    }

    public static int getNameOffset(String content, int startOffset) {

        boolean stringLiteral = false;
        boolean complexString = false;
        for(int i=startOffset; i<content.length();i++) {
            final char n = content.charAt(i);
            if(complexString) {
                if(n=='\'') {
                    if(i+1<content.length()&&content.charAt(i+1)=='\'') {
                        i++;
                    }
                    else {
                        complexString = false;
                    }
                }
            }
            else if(stringLiteral) {
                if(n=='"') {
                    if(i+1<content.length()&&content.charAt(i+1)=='"') {
                        i++;
                    }
                    else {
                        stringLiteral = false;
                    }
                }
            }
            else if(n=='\'') {
                complexString = true;
            }
            else if(n=='"') {
                stringLiteral = true;
            }
            else if(n=='!') {
                return i;
            }
        }
        return -1;
    }

    private static String simpleCharPattern ="^[\\w.\\xa1-\\u2027\\u202a-\\uffff]+$";

    public static String updateSheetName(String content, String oldName, String newName) {
        String newContent = content;
        List<Integer> replaceOffsets = null;
        if(content!=null&&content.length()>oldName.length()) {
            for(int startOffset = 0; startOffset<content.length();) {
                final int nameOffset = getNameOffset(content, startOffset);
                if(nameOffset<0) {
                    break;
                }
                final int possibleLength = nameOffset - startOffset;
                if(possibleLength>=oldName.length()) {
                    boolean replace = false;
                    final boolean isComplex = content.charAt(nameOffset-1)=='\'';
                    final String subString = content.substring(nameOffset-oldName.length(), nameOffset);

                    final int mOffset = nameOffset - oldName.length();
                    if(isComplex&&oldName.charAt(0)=='\'') {
                        if(subString.equals(oldName)) {
                            replace = true;
                        }
                    }
                    else if(oldName.charAt(0)!='\'') {
                        if(subString.equals(oldName)) {
                            if(mOffset>startOffset) {
                                final String prev = content.substring(mOffset-1, mOffset);
                                if(!prev.matches(simpleCharPattern)) {
                                    replace = true;
                                }
                            }
                            else {
                                replace = true;
                            }
                        }
                    }
                    if(replace) {
                        if(replaceOffsets==null) {
                            replaceOffsets = new ArrayList<Integer>();
                        }
                        replaceOffsets.add(mOffset);
                    }
                }
                startOffset = nameOffset + 1;
            }
        }
        if(replaceOffsets!=null) {
            final StringBuffer buffer = new StringBuffer(content);
            for(int i=replaceOffsets.size()-1;i>=0;i--) {
                final int offset = replaceOffsets.get(i);
                buffer.replace(offset, offset + oldName.length(), newName);
            }
            newContent = buffer.toString();
        }
        return newContent;
    }

    public void changeSheet(int sheetIndex, String sheetName, JSONObject attrs)
        throws JSONException {

        if(attrs!=null) {
            final Workbook workbook = workbookPart.getJaxbElement();
            final ISheetAccess sheetAccess = getWorksheet(sheetIndex);
            final JSONObject sheetProperties = attrs.optJSONObject(OCKey.SHEET.value());
            if(sheetProperties!=null) {
                SheetUtils.applySheetProperties(workbook, sheetIndex, sheetAccess, sheetProperties);
                final boolean visible = sheetProperties.optBoolean(OCKey.VISIBLE.value(), true);
                if(!visible) {
                    SheetUtils.updateBookView(workbook);
                }
            }
            if(sheetAccess instanceof Worksheet) {
                final JSONObject columnProperties = attrs.optJSONObject(OCKey.COLUMN.value());
                if(columnProperties!=null) {
                    SheetUtils.applyWorksheetColumnProperties(getOperationDocument(), (Worksheet)sheetAccess, columnProperties);
                }
                final JSONObject rowProperties = attrs.optJSONObject(OCKey.ROW.value());
                if(rowProperties!=null) {
                    SheetUtils.applyWorksheetRowProperties(getOperationDocument(), (Worksheet)sheetAccess, rowProperties);
                }
            }
        }

        if(sheetName!=null) {
            setSheetName(sheetIndex, sheetName);
        }
    }

    public void mergeCells(int sheetIndex, CellRefRange[] ranges, boolean keepContent, String type) {
        for(CellRefRange cellRefRange:ranges) {
            final Worksheet worksheet = (Worksheet)getWorksheet(sheetIndex);
            CTMergeCells mergeCells = worksheet.getMergeCells();
            if(mergeCells==null) {
                mergeCells = Context.getsmlObjectFactory().createCTMergeCells();
                worksheet.setMergeCells(mergeCells);
            }
            // first we will remove each mergeCell that is covering our new mergeCellRange
            final List<CTMergeCell> mergeCellList = mergeCells.getMergeCell();
            for(int i=mergeCellList.size()-1;i>=0;i--) {
                final CTMergeCell mergeCell = mergeCellList.get(i);
                final CellRefRange mergedRefRange = mergeCell.getCellRefRange(false);
                if(mergedRefRange==null) {
                    log.warn("xlsx export: mergeRange is invalid");
                }
                else {
                    if(cellRefRange.intersects(mergedRefRange)) {
                        mergeCellList.remove(i);
                    }
                }
            }
            if(type.equals("merge")) {
                final CTMergeCell mergeCell = Context.getsmlObjectFactory().createCTMergeCell();
                mergeCell.setCellRefRange(cellRefRange);
                mergeCellList.add(mergeCell);
            }
            else if(type.equals("horizontal")) {
                for(int row=cellRefRange.getStart().getRow();row<=cellRefRange.getEnd().getRow();row++) {
                    final CTMergeCell mergeCell = Context.getsmlObjectFactory().createCTMergeCell();
                    final CellRefRange mergedCellRange = new CellRefRange(new CellRef(cellRefRange.getStart().getColumn(), row), new CellRef(cellRefRange.getEnd().getColumn(), row));
                    mergeCell.setCellRefRange(mergedCellRange);
                    mergeCellList.add(mergeCell);
                }
            }
            else if(type.equals("vertical")) {
                for(int column=cellRefRange.getStart().getColumn();column<=cellRefRange.getEnd().getColumn();column++) {
                    final CTMergeCell mergeCell = Context.getsmlObjectFactory().createCTMergeCell();
                    final CellRefRange mergedCellRange = new CellRefRange(new CellRef(column, cellRefRange.getStart().getRow()), new CellRef(column, cellRefRange.getEnd().getRow()));
                    mergeCell.setCellRefRange(mergedCellRange);
                    mergeCellList.add(mergeCell);
                }
            }
            if(mergeCells.getMergeCell().isEmpty()) {
                worksheet.setMergeCells(null);
            }
        }
    }

    public void insertNumberFormat(long formatId, String formatCode) throws Exception {
        operationDocument.getStylesheet(true).insertNumberFormat(formatId, formatCode);
    }

    public void deleteNumberFormat(long formatId) throws Exception {
        final CTStylesheet stylesheet = operationDocument.getStylesheet(false);
        if (stylesheet != null) {
            stylesheet.deleteNumberFormat(formatId);
        }
    }

    public Long getStyleIndex(String styleId) {
        if(styleId==null||styleId.isEmpty()) {
            return Long.valueOf(0);
        }
        return styleIdToIndex.get(styleId);
    }

    public void insertStyleSheet(String type, String styleId, String styleName, JSONObject attrs, boolean hidden, int uiPriority, boolean defaultStyle)
        throws Exception {

        final CTStylesheet stylesheet = operationDocument.getStylesheet(true);
        CTXf cellStyleXf = null;
        CTCellStyleXfs cellStyleXfs = stylesheet.getCellStyleXfs();
        if(cellStyleXfs==null) {
            cellStyleXfs = Context.getsmlObjectFactory().createCTCellStyleXfs();
        }
        cellStyleXf = styleXfMap.get(styleId);
        if(cellStyleXf==null) {
            cellStyleXf = Context.getsmlObjectFactory().createCTXf();
            final List<CTXf> ctXfList = cellStyleXfs.getXf();
            final Long ctXfIndex = new Long(ctXfList.size());
            ctXfList.add(cellStyleXf);

            CTCellStyles cellStyles = stylesheet.getCellStyles();
            if(cellStyles==null) {
                cellStyles = Context.getsmlObjectFactory().createCTCellStyles();
            }
            final List<CTCellStyle> cellStyleList = cellStyles.getCellStyle();
            final CTCellStyle cellStyle = Context.getsmlObjectFactory().createCTCellStyle();
            cellStyleList.add(cellStyle);
            cellStyle.setBuiltinId(BuiltinIds.getBuiltinId(styleName));
            cellStyle.setName(styleName);
            cellStyle.setHidden(hidden);
            cellStyle.setXfId(ctXfIndex);
            styleXfMap.put(styleId, cellStyleXf);
            styleIdToIndex.put(styleId, ctXfIndex);
        }
        final JSONObject cellApplyProperties = attrs.optJSONObject(OCKey.APPLY.value());
        if(cellApplyProperties!=null) {
            // default for the "apply" flags in style XFs is "true"
            final Object applyBorder = cellApplyProperties.opt(OCKey.BORDER.value());
            if(applyBorder!=null) {
                cellStyleXf.setApplyBorder(applyBorder instanceof Boolean&&!((Boolean)applyBorder).booleanValue()?false:null);
            }
            final Object applyAlign = cellApplyProperties.opt(OCKey.ALIGN.value());
            if(applyAlign!=null) {
                cellStyleXf.setApplyAlignment(applyAlign instanceof Boolean&&!((Boolean)applyAlign).booleanValue()?false:null);
            }
            final Object applyNumber = cellApplyProperties.opt(OCKey.NUMBER.value());
            if(applyNumber!=null) {
                cellStyleXf.setApplyNumberFormat(applyNumber instanceof Boolean&&!((Boolean)applyNumber).booleanValue()?false:null);
            }
            final Object applyProtect = cellApplyProperties.opt(OCKey.PROTECT.value());
            if(applyProtect!=null) {
                cellStyleXf.setApplyProtection(applyProtect instanceof Boolean&&!((Boolean)applyProtect).booleanValue()?false:null);
            }
            final Object applyFont = cellApplyProperties.opt(OCKey.FONT.value());
            if(applyFont!=null) {
                cellStyleXf.setApplyProtection(applyFont instanceof Boolean&&!((Boolean)applyFont).booleanValue()?false:null);
            }
            final Object applyFill = cellApplyProperties.opt(OCKey.FILL.value());
            if(applyFill!=null) {
                cellStyleXf.setApplyFill(applyFill instanceof Boolean&&!((Boolean)applyFill).booleanValue()?false:null);
            }
        }
        if(cellStyleXf!=null) {
            final JSONObject cellProperties = attrs.optJSONObject(OCKey.CELL.value());
            if(cellProperties!=null) {
                CellUtils.applyCellProperties(operationDocument, cellProperties, cellStyleXf, stylesheet, false);
            }
            final JSONObject characterProperties = attrs.optJSONObject(OCKey.CHARACTER.value());
            if(characterProperties!=null) {
                CellUtils.applyCharacterProperties(operationDocument, characterProperties, cellStyleXf, stylesheet);
            }
        }
    }

    private Boolean getAutoStyleApplyFlag(final JSONObject applyAttribs, String attrName) {
        // default for the "apply" flags in cell XFs is "false"; default in operations is true (!)
        final Object jsonValue = applyAttribs.opt(attrName);
        return ((jsonValue == null) || ((jsonValue instanceof Boolean) && ((Boolean)jsonValue).booleanValue())) ? true : null;
    }

    private void applyAutoStyleAttributes(CTXf cellXf, JSONObject attrs, CTStylesheet stylesheet)
        throws Exception {

        final JSONObject cellProperties = attrs.optJSONObject(OCKey.CELL.value());
        if(cellProperties!=null) {
            CellUtils.applyCellProperties(operationDocument, cellProperties, cellXf, stylesheet, false);
        }

        final JSONObject characterProperties = attrs.optJSONObject(OCKey.CHARACTER.value());
        if(characterProperties!=null) {
            CellUtils.applyCharacterProperties(operationDocument, characterProperties, cellXf, stylesheet);
        }

        final JSONObject cellApplyProperties = attrs.optJSONObject(OCKey.APPLY.value());
        if (cellApplyProperties != null) {
            cellXf.setApplyBorder(getAutoStyleApplyFlag(cellApplyProperties, OCKey.BORDER.value()));
            cellXf.setApplyAlignment(getAutoStyleApplyFlag(cellApplyProperties, OCKey.ALIGN.value()));
            cellXf.setApplyNumberFormat(getAutoStyleApplyFlag(cellApplyProperties, OCKey.NUMBER.value()));
            cellXf.setApplyProtection(getAutoStyleApplyFlag(cellApplyProperties, OCKey.PROTECT.value()));
            cellXf.setApplyFont(getAutoStyleApplyFlag(cellApplyProperties, OCKey.FONT.value()));
            cellXf.setApplyFill(getAutoStyleApplyFlag(cellApplyProperties, OCKey.FILL.value()));
        }
    }

    public void insertAutoStyle(String type, String styleId, JSONObject attrs, boolean defaultStyle)
        throws Exception {

        Long index = Utils.parseAutoStyleId(styleId);
        if (index == null) {
            throw new FilterException("invalid auto-style identifier", ErrorCode.CRITICAL_ERROR);
        }

        final CTStylesheet stylesheet = operationDocument.getStylesheet(true);
        CTXf cellXf = null;
        final CTCellXfs ctXfs = stylesheet.getCellXfs();
        if(ctXfs != null) {
            final List<CTXf> ctXfList = ctXfs.getXf();
            if (index!=ctXfList.size()) {
                if(index==0&&ctXfList.size()==1) {
                    // empty documents might already have a default autostyle, so we make an exception for the first
                    cellXf = ctXfList.get(0);
                }
                else {
                    throw new FilterException("auto-style must be appended", ErrorCode.CRITICAL_ERROR);
                }
            }
            else {
                cellXf = Context.getsmlObjectFactory().createCTXf();
                ctXfList.add(cellXf);
            }
            applyAutoStyleAttributes(cellXf, attrs, stylesheet);
            final String parentId = attrs.optString(OCKey.STYLE_ID.value(), "");
            if(!parentId.isEmpty()) {
                final CTCellStyles cellStyles = stylesheet.getCellStyles();
                if(cellStyles!=null) {
                    for(CTCellStyle cellStyle:cellStyles.getCellStyle()) {
                        if(parentId.equals(cellStyle.getName())) {
                            cellXf.setXfId(cellStyle.getXfId());
                            break;
                        }
                    }
                }
            }
        }
    }

    public void changeAutoStyle(String type, String styleId, JSONObject attrs)
        throws Exception {

        Long index = Utils.parseAutoStyleId(styleId);
        if (index == null) {
            throw new FilterException("invalid auto-style identifier", ErrorCode.CRITICAL_ERROR);
        }

        final CTStylesheet stylesheet = operationDocument.getStylesheet(true);
        final CTCellXfs ctXfs = stylesheet.getCellXfs();
        if(ctXfs != null) {
            final List<CTXf> ctXfList = ctXfs.getXf();
            int i = index.intValue();
            if ((i < 0) || (i >= ctXfList.size())) {
                throw new FilterException("auto-style does not exist", ErrorCode.CRITICAL_ERROR);
            }
            applyAutoStyleAttributes(ctXfList.get(i), attrs, stylesheet);
        }
    }

    public void deleteAutoStyle(String type, String styleId)
        throws Exception {

        final Long index = Utils.parseAutoStyleId(styleId);
        if (index == null) {
            throw new FilterException("invalid auto-style identifier", ErrorCode.CRITICAL_ERROR);
        }

        final CTStylesheet stylesheet = operationDocument.getStylesheet(false);
        final CTCellXfs ctXfs = stylesheet.getCellXfs();
        if(ctXfs != null) {
            final List<CTXf> ctXfList = ctXfs.getXf();
            int i = index.intValue();
            if (i == 0) {
                throw new FilterException("default auto-style cannot be deleted", ErrorCode.CRITICAL_ERROR);
            }
            if (index + 1 != ctXfList.size()) {
                throw new FilterException("only the last auto-style can be deleted", ErrorCode.CRITICAL_ERROR);
            }
            ctXfList.remove(i);
        }
    }

    private Drawing getDrawing(int sheetIndex)
        throws Exception {

        final JaxbSmlPart<?> sheetPart = getSheetPart(sheetIndex);
        final Object sheetElement = sheetPart.getJaxbElement();
        org.xlsx4j.sml.CTDrawing drawing = null;
        if(sheetElement instanceof Worksheet) {
            drawing = ((Worksheet)sheetElement).getDrawing();
        }
        else if(sheetElement instanceof CTChartsheet) {
            drawing = ((CTChartsheet)sheetElement).getDrawing();
        }
        if(drawing==null) {
            throw new RuntimeException("xlsx:getDrawing: could not get drawing for sheet");
        }
        final String drawingsId = drawing.getId();
        if(drawingsId==null||drawingsId.isEmpty()) {
            throw new RuntimeException("xlsx:getDrawing: could not get drawing for sheet");
        }
        final RelationshipsPart worksheetRelationships = sheetPart.getRelationshipsPart();
        return (Drawing)worksheetRelationships.getPart(drawingsId);
    }

    public void insertDrawing(JSONArray start, String type, JSONObject attrs)
        throws Exception {

        ComponentType cType = ComponentType.AC_SHAPE;
        if(type.equals("image")) {
            cType = ComponentType.AC_IMAGE;
        }
        else if(type.equals("group")) {
            cType = ComponentType.AC_GROUP;
        }
        else if(type.equals("connector")) {
            cType = ComponentType.AC_CONNECTOR;
        }
        else if(type.equals("table")) {
            cType = ComponentType.TABLE;
        }
        else if(type.equals("chart")) {
            cType = ComponentType.AC_CHART;
        }
        new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, cType);
    }

    public void changeDrawing(JSONArray start, JSONObject attrs)
        throws Exception {

        new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length())
            .applyAttrsFromJSON(attrs);
    }

    public void deleteDrawing(JSONArray start)
        throws Exception {

        final int sheetIndex = start.getInt(0);
        final Drawing drawing = getDrawing(sheetIndex);
        final DLList<AnchorBase> egAnchor = drawing.getJaxbElement().getContent();
        egAnchor.remove(start.getInt(1));
        if(egAnchor.isEmpty()) {
            // do not remove the drawing part here, because it might be that the drawing is used
            // twice, an unused part will be removed when saving in Save.checkUnusedRelations
            getWorksheet(start.getInt(0)).setDrawing(null);
        }
    }

    public void insertChartSeries(JSONArray start, int series, JSONObject attrs)
        throws Exception {

        final ShapeGraphicComponent shapeGraphicComponent = (ShapeGraphicComponent)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length());
        final HashMap<String, Object> drawingPropertyMap = new HashMap<String, Object>();
        DMLGraphic.getGraphicProperties(operationDocument, getDrawing(start.getInt(0)), shapeGraphicComponent.getObjectFrame().getGraphic(), drawingPropertyMap);
        Drawings.getChart(drawingPropertyMap).insertChartSpace(series, attrs);
    }

    public void changeChartSeries(JSONArray start, int series, JSONObject attrs) throws Exception {

        final ShapeGraphicComponent shapeGraphicComponent = (ShapeGraphicComponent)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length());
        final HashMap<String, Object> drawingPropertyMap = new HashMap<String, Object>();
        DMLGraphic.getGraphicProperties(operationDocument, getDrawing(start.getInt(0)), shapeGraphicComponent.getObjectFrame().getGraphic(), drawingPropertyMap);
        Drawings.getChart(drawingPropertyMap).setDataSeriesAttributes(series, attrs);
    }

    public void deleteChartSeries(JSONArray start, int series)
        throws Exception {

        final ShapeGraphicComponent shapeGraphicComponent = (ShapeGraphicComponent)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length());
        final HashMap<String, Object> drawingPropertyMap = new HashMap<String, Object>();
        DMLGraphic.getGraphicProperties(operationDocument, getDrawing(start.getInt(0)), shapeGraphicComponent.getObjectFrame().getGraphic(), drawingPropertyMap);
        Drawings.getChart(drawingPropertyMap).deleteChartSpace(series);
    }

    public void insertName(Long sheet, String label, String formula, Boolean hidden) {

        if(sheet<0) {
            sheet = null;
        }

        final Workbook workbook = workbookPart.getJaxbElement();
        DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames==null) {
            definedNames = Context.getsmlObjectFactory().createDefinedNames();
            workbook.setDefinedNames(definedNames);
        }
        final HashMap<String, CTDefinedName> definedNameMap = definedNames.getDefinedNameMap();
        final CTDefinedName definedName = Context.getsmlObjectFactory().createCTDefinedName();
        definedName.setName(label);
        definedName.setLocalSheetId(sheet);
        definedName.setValue(formula);
        if(hidden!=null) {
            definedName.setHidden(hidden.booleanValue()?true:null);
        }
        definedNameMap.put(DefinedNames.getDefinedNameKey(sheet, label), definedName);
    }

    public void changeName(Long sheet, String label, String formula, String newLabel) {

        if(sheet<0) {
            sheet = null;
        }
        final Workbook workbook = workbookPart.getJaxbElement();
        final DefinedNames definedNames = workbook.getDefinedNames();
        final CTDefinedName oldName = definedNames.getDefinedNameMap().get(DefinedNames.getDefinedNameKey(sheet, label));
        definedNames.getDefinedNameMap().remove(DefinedNames.getDefinedNameKey(sheet, label));
        if(newLabel!=null) {
            oldName.setName(newLabel);
        }
        if(formula!=null) {
            oldName.setValue(formula);
        }
        definedNames.getDefinedNameMap().put(DefinedNames.getDefinedNameKey(sheet, newLabel!=null?newLabel:label), oldName);
    }

    public void deleteName(Long sheet, String name) {

        final Workbook workbook = workbookPart.getJaxbElement();
        final DefinedNames definedNames = workbook.getDefinedNames();
        if(definedNames!=null) {
            definedNames.getDefinedNameMap().remove(DefinedNames.getDefinedNameKey(sheet, name));
        }
        log.debug("xlsx:deleteName ... could not find name within definedNameList");
    }

    public void changeChartAxis(JSONArray start, long axisId, long crossAxId, String axPos, boolean zAxis, JSONObject attrs) throws Exception {
        getChart(start).setAxisAttributes(axisId, crossAxId, axPos, zAxis, attrs);
    }

    public void changeChartGrid(JSONArray start, long axisId, JSONObject attrs) throws Exception {
        getChart(start).setChartGridlineAttributes(axisId, attrs);
    }

    public void changeChartTitle(JSONArray start, Long axisId, JSONObject attrs) throws Exception {
        getChart(start).setChartTitleAttributes(axisId, attrs);
    }

    public void changeChartLegend(JSONArray start, JSONObject attrs) throws Exception {
        getChart(start).setLegendFromAttrs(attrs);
    }

    private ChartWrapper getChart(JSONArray start) throws Exception {

        final ShapeGraphicComponent shapeGraphicComponent = (ShapeGraphicComponent)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length());
        final HashMap<String, Object> drawingPropertyMap = new HashMap<String, Object>();
        DMLGraphic.getGraphicProperties(operationDocument, getDrawing(start.getInt(0)), shapeGraphicComponent.getObjectFrame().getGraphic(), drawingPropertyMap);
        return Drawings.getChart(drawingPropertyMap);
    }

    public void setDocumentAttributes(JSONObject attrs) {
        final JSONObject documentAttrs = attrs.optJSONObject(OCKey.DOCUMENT.value());
        if(documentAttrs!=null) {
            final Workbook workbook = workbookPart.getJaxbElement();
            BookViews bookViews = workbook.getBookViews();
            if(bookViews==null) {
                bookViews = Context.getsmlObjectFactory().createBookViews();
                workbook.setBookViews(bookViews);
            }
            final List<CTBookView> bookViewList = bookViews.getWorkbookView();
            if(bookViewList.isEmpty()) {
                final CTBookView newBookView = Context.getsmlObjectFactory().createCTBookView();
                bookViewList.add(newBookView);
            }
            final int activeSheet = documentAttrs.optInt(OCKey.ACTIVE_SHEET.value(), -1);
            if(activeSheet>=0) {
                final CTBookView bookView = bookViewList.get(0);
                bookView.setActiveTab(new Long(activeSheet));
            }

            // property nullDate: serial number 0 represents 1904-01-01 in "date1904" mode
            if (documentAttrs.has(OCKey.NULL_DATE.value())) {
                final String nullDate = documentAttrs.optString(OCKey.NULL_DATE.value(), null);
                final boolean date1904 = "1904-01-01".equals(nullDate);
                // do not force to create the <workbookPr> element when resetting the flag
                final WorkbookPr wbPr = workbook.getWorkbookPr(date1904);
                if (wbPr!=null) { wbPr.setDate1904(date1904 ? true : null); }
            }

            // property calcOnLoad: recalculate all formulas when loading the document
            if (documentAttrs.has(OCKey.CALC_ON_LOAD.value())) {
                final boolean calcOnLoad = documentAttrs.optBoolean(OCKey.CALC_ON_LOAD.value(), false);
                // do not force to create the <calcPr> element when resetting the flag
                final CTCalcPr calcPr = workbook.getCalcPr(calcOnLoad);
                if (calcPr!=null) { calcPr.setFullCalcOnLoad(calcOnLoad ? true : null); }
            }
        }
    }

    public void insertParagraph(JSONArray start, JSONObject attrs) throws Exception {

        new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
    }

    public void splitParagraph(JSONArray start) throws Exception {

        ((IParagraph)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1)).splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start) {

        ((IParagraph)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length())).mergeParagraph();
    }

    public void insertText(JSONArray start, String text, JSONObject attrs) throws Exception {

        if(text.length()>0) {
            final IComponent<OfficeOpenXMLOperationDocument> component = new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1);
            ((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
        }
    }

    public void insertTab(JSONArray start, JSONObject attrs) throws Exception {

        insertText(start, "\t", attrs);
    }

    public void insertHardBreak(JSONArray start, JSONObject attrs) throws Exception {

        new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.HARDBREAK_DEFAULT);
    }

    public void insertField(JSONArray start, String type, String representation, JSONObject attrs) throws Exception {

        final CTTextField textField = (CTTextField)new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.SIMPLE_FIELD).getObject();

        textField.setId("{2640DB9D-575D-4FF8-9117-D1A6111C2CD0}");
        textField.setType(type);
        textField.setT(representation);
    }

    public void setAttributes(JSONArray start, JSONArray end, JSONObject attrs) throws Exception {

        if(attrs==null) {
            return;
        }
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OfficeOpenXMLOperationDocument> component = new RootComponent(getOperationDocument(), workbookPart.getJaxbElement().getSheets()).getComponent(start, start.length());
        component.splitStart(startIndex, SplitMode.ATTRIBUTE);
        while(component!=null&&component.getComponentNumber()<=endIndex) {
            if(component.getNextComponentNumber()>=endIndex+1) {
                component.splitEnd(endIndex, SplitMode.ATTRIBUTE);
            }
            component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
    }

    public void move(JSONArray start, JSONArray end, JSONArray to)
        throws JSONException {

        OfficeOpenXMLComponent.move(operationDocument, start, end, to);
    }

    public void insertNote(int sheetIndex, CellRef ref, String text, String author, String uid, String date, JSONObject attrs) throws Exception {

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTComments comments = worksheetPart.getComments(true);

        int authorId = 0;

        if (author != null) {

            final List<String> authorList = comments.getAuthors().getAuthor();

            for(authorId = 0;  authorId < authorList.size(); authorId++) {
                if(authorList.get(authorId).equals(author)) {
                    break;
                }
            }

            if(authorId == authorList.size()) {
                authorList.add(author);
            }

        }

        final List<CTComment> commentList = comments.getCommentList().getComment();
        final CTComment comment = new CTComment();
        if (author != null) {
            comment.setAuthorId(authorId);
        }
        commentList.add(comment);
        comment.setCellRef(ref);
        final VMLRootComponent vmlRootComponent = new VMLRootComponent("spreadsheet", operationDocument, worksheetPart.getVMLPart(true).getJaxbElement());
        final VMLBase vmlBase = ((VMLBase)vmlRootComponent.insertChildComponent(Integer.MAX_VALUE, attrs, ComponentType.AC_COMMENT));
        final CTClientData clientData = vmlBase.getVMLCoreObject().getClientData(true);
        clientData.setColumn(ref.getColumn());
        clientData.setRow(ref.getRow());
        applyCommentAttrs(worksheetPart.getJaxbElement(), comment, ref, vmlBase, text, attrs);
    }

    public void changeNote(int sheetIndex, CellRef ref, String text, JSONObject attrs) throws Exception {
        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTComments comments = worksheetPart.getComments(false);
        final List<CTComment> commentList = comments.getCommentList().getComment();
        for(int i=commentList.size()-1; i>=0; i--) {
            final CTComment comment = commentList.get(i);
            if(ref.equals(comment.getCellRef(false))) {
                applyCommentAttrs(worksheetPart.getJaxbElement(), comment, ref, getVMLForComment(worksheetPart, ref.getColumn(), ref.getRow()), text, attrs);
                break;
            }
        }
    }

    private VMLBase getVMLForComment(WorksheetPart worksheetPart, int column, int row)
        throws InvalidFormatException {

        final VMLPart vmlPart = worksheetPart.getVMLPart(false);
        if(vmlPart!=null) {
            IComponent<OfficeOpenXMLOperationDocument> component = new VMLRootComponent("spreadsheet", operationDocument, vmlPart.getJaxbElement()).getNextChildComponent(null, null);
            while(component!=null) {
                if(component instanceof VMLBase) {
                    final VMLBase vmlBase = (VMLBase)component;
                    final VmlCore vmlCore = vmlBase.getVMLCoreObject();
                    final CTClientData clientData = vmlCore.getClientData(false);
                    if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                        final Integer r = clientData.getRow();
                        final Integer c = clientData.getColumn();
                        if(r!=null&&c!=null) {
                            if(column==c.intValue()&&row==r.intValue()) {
                                return vmlBase;
                            }
                        }
                    }
                }
                component = component.getNextComponent();
            }
        }
        return null;
    }

    private void applyCommentAttrs(Worksheet worksheet, CTComment comment, CellRef ref, VMLBase vmlBase, String text, JSONObject attrs) throws Exception {
        if(attrs!=null||text!=null) {
            final String t = text!=null ? text : comment.toString();
            final List<CTRElt> reltList = comment.getText(true).getR();
            comment.getText(true).setT(null);
            final CTRElt n = new CTRElt();
            reltList.clear();   // we only have one text run, so we will only use the first
            reltList.add(n);
            n.setT(new CTXstringWhitespace(t, true));
            if(attrs!=null) {
                final CTRPrElt rPr = n.getRPr(true);
                // TODO: character attributes ...
                if(vmlBase!=null) {
                    vmlBase.applyAttrsFromJSON(attrs);
                    final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
                    if(drawingAttrs!=null) {
                        final Object hidden = drawingAttrs.opt(OCKey.HIDDEN.value());
                        if(hidden!=null) {
                            final VmlCore vmlCore = vmlBase.getVMLCoreObject();
                            final HashMap<String, String> vmlStyle = vmlCore.getStyleMap();

                            final boolean h = hidden instanceof Boolean ? ((Boolean)hidden).booleanValue() : false;
                            final CTClientData clientData = vmlCore.getClientData(false);
                            if(h) {
                                if(clientData!=null) {
                                    clientData.setVisible(null);
                                }
                            }
                            else {
                                if(clientData!=null) {
                                    clientData.setVisible(new String());
                                }
                            }
                            vmlStyle.put("visibility", worksheet.isColumnVisible(ref.getColumn())&&worksheet.isRowVisible(ref.getRow())&&!h ? "visible" : "hidden");
                        }
                    }
                }
            }
        }
    }

    public void deleteNote(int sheetIndex, CellRef ref) throws Exception {

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final SpreadsheetCommentsPart commentPart = worksheetPart.getCommentsPart(false);
        final CTComments comments = commentPart.getJaxbElement();
        final List<CTComment> commentList = comments.getCommentList().getComment();
        for(int i=commentList.size()-1; i>=0; i--) {
            final CTComment comment = commentList.get(i);
            if(ref.equals(comment.getCellRef(false))) {
                commentList.remove(i);
                break;
            }
        }
        if(commentList.isEmpty()) {
            worksheetPart.getRelationshipsPart().removeRelationshipByType(Namespaces.COMMENTS);
        }
        final VMLBase vmlBase = getVMLForComment(worksheetPart, ref.getColumn(), ref.getRow());
        if(vmlBase!=null) {
            vmlBase.delete(1);
        }
    }

    private String getCommentString(List<CTThreadedComment> comments, CellRef ref) {
        String commentsText = "[Threaded comment]\n";

        for (CTThreadedComment comment: comments) {
            if (ref.equals(comment.getCellRef(false))) {
                commentsText += "Comment:\n" + comment.getText() + "\n";
            }
        }
        return commentsText;
    }

    private String getGUID() {
        UUID uuid = UUID.randomUUID();
        return "{" + uuid.toString() + "}";

    }

    public void deleteComment(int sheetIndex, CellRef ref, int index) throws Exception {
        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);

        Iterator<CTThreadedComment> commentsIterator = threadedComments.getThreadedComments().iterator();

        int i = 0;

        while(commentsIterator.hasNext()) {
            CTThreadedComment comment = commentsIterator.next();

            if (ref.equals(comment.getCellRef(false))) {
                if (index == 0) {
                    commentsIterator.remove();
                } else if (index == i) {
                    commentsIterator.remove();
                    break;
                }
                i++;
            }

        }

        // delete the complete comment from the comment part
        if (index == 0) {
            deleteNote(sheetIndex, ref.clone());
        } else {
            // update the comment in the comment part
            String commentsText = getCommentString(threadedComments.getThreadedComments(), ref);

            //String text, String author, String uid, String date, JSONObject attrs
            changeNote(sheetIndex, ref.clone(), commentsText, null);
        }
        worksheetPart.deleteThreadedCommentsPartIfEmpty();
    }

    public void insertComment(int sheetIndex, CellRef ref , String _text, JSONArray mentions, String date, String author, String authorId, String authorProvider, Boolean done, JSONObject attrs) throws Exception {
        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(true);

        final String text = applyMentions(_text, mentions);

        CTThreadedComment parentComment = null;
        for (CTThreadedComment comment : threadedComments.getThreadedComments()) {
            if (ref.equals(comment.getCellRef(false))) {
                parentComment = comment;
                break;
            }
        }

        CTThreadedComment comment = new CTThreadedComment();
        comment.setText(text);
        comment.setCellRef(ref);

        if (date != null) {
            XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(date.replace("Z", ""));
            comment.setDateTime(calendar);
        }

        comment.setDone(done);

        comment.setId(getGUID());

        if (parentComment != null) {
            comment.setParentId(parentComment.getId());
        }

        if (author.isEmpty()) {
            comment.setPersonId("{00000000-0000-0000-0000-000000000000}");
        } else {
            CTPersonList personList = getOperationDocument().getPersons(true);
            CTPerson person = personList.getPerson(author, authorId, authorProvider);
            if (person == null) {
                person = new CTPerson();
                person.setDisplayName(author);
                person.setId(getGUID());
                person.setUserId(authorId);
                person.setProviderId(authorProvider);
                personList.getPersons().add(person);
            }
            comment.setPersonId(person.getId());
        }
        createMentions(comment, mentions);
        threadedComments.getThreadedComments().add(comment);

        // update the comment in the comment part
        String commentsText = getCommentString(threadedComments.getThreadedComments(), ref);
        if (parentComment == null) {
            insertNote(sheetIndex, ref.clone(), text, getThreadedCommentAuthorId(comment.getId()), comment.getId(), null, attrs);
        } else {
            //String text, String author, String uid, String date, JSONObject attrs
            changeNote(sheetIndex, ref.clone(), commentsText, null);
        }
    }

    private void createMentions(CTThreadedComment comment, JSONArray jsonMentions) throws JSONException {
        if(jsonMentions!=null && !jsonMentions.isEmpty()) {
            final CTPersonList personList = getOperationDocument().getPersons(true);
            final CTThreadedCommentMentions mentions = new CTThreadedCommentMentions();
            comment.setMentions(mentions);

            // int offset = 0;
            int mentionCount = jsonMentions.length();

            for(int i = 0; i < mentionCount; i++) {
                final JSONObject jsonMention = jsonMentions.getJSONObject(i);

                // create personId from name
                final String displayName = jsonMention.optString(OCKey.DISPLAY_NAME.value(), "");
                final String userId = jsonMention.optString(OCKey.USER_ID.value(), "");
                final String providerId = jsonMention.optString(OCKey.PROVIDER_ID.value(), "");
                if(!displayName.isEmpty() && !userId.isEmpty() && !providerId.isEmpty()) {
                    final CTMention mention = new CTMention();
                    CTPerson person = personList.getPerson(displayName, userId, providerId);
                    if(person==null) {
                        person = new CTPerson(); // create new person if not already available
                        personList.getPersons().add(person);
                        person.setId(getGUID());
                        person.setDisplayName(displayName);
                        person.setUserId(userId);
                        person.setProviderId(providerId);
                    }
                    mention.setPersonId(person.getId());

                    final String mentionId = jsonMention.optString(OCKey.MENTION_ID.value());
                    if(mentionId!=null && !mentionId.isEmpty()) {
                        mention.setId(mentionId);
                    }
                    else {
                        mention.setId(getGUID());
                    }
                    mention.setStartIndex(jsonMention.getInt(OCKey.POS.value()) /* + offset */);
                    mention.setLength(jsonMention.getInt(OCKey.LENGTH.value()) /* + 1 */);
                    mentions.addMention(mention);
                }
                // offset++;
            }
        }
    }

    private String getThreadedCommentAuthorId(String commentId) {
        return "tc=" + commentId;
    }


    public void changeComment(int sheetIndex, CellRef ref , Integer index, String _text, JSONArray mentions, Boolean done, JSONObject attrs) throws Exception {
        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);

        final String text = applyMentions(_text, mentions);

        int i = 0;
        Iterator<CTThreadedComment> commentsIterator = threadedComments.getThreadedComments().iterator();
        CTThreadedComment changeComment = null;
        while(commentsIterator.hasNext()) {
            CTThreadedComment comment = commentsIterator.next();

            if (ref.equals(comment.getCellRef(false))) {

                if (index == i) {
                    changeComment = comment;
                    break;
                }

                i++;
            }

        }

        if (changeComment != null) {
            if (text != null || mentions != null) {
                changeComment.setMentions(null);
            }
            if (text != null) {
                changeComment.setText(text);
            }
            if(mentions != null) {
                createMentions(changeComment, mentions);
            }
            if (done != null) {
                changeComment.setDone(done);
            }

            // update the comment in the comment part
            String commentsText = getCommentString(threadedComments.getThreadedComments(), ref);

            //String text, String author, String uid, String date, JSONObject attrs
            changeNote(sheetIndex, ref.clone(), commentsText, attrs);
        }
    }

    public static String applyMentions(String text, JSONArray mentions) throws JSONException {
        if(text == null || mentions == null || mentions.length() == 0) {
            return text;
        }
        final StringBuilder builder = new StringBuilder(text);
        // for(int i = mentions.length() - 1; i >= 0; i--) {
        //     builder.insert(mentions.getJSONObject(i).getInt(OCKey.POS.value()), "@");
        // }
        return builder.toString();
    }

    public void moveComments(int sheetIndex, String[] from, String[] to) throws Exception {
        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final CTThreadedComments threadedComments = worksheetPart.getThreadedComments(false);
        Map<CTThreadedComment, CellRef> moveComment = new HashMap<CTThreadedComment, CellRef>();

        for(int i = 0; i < from.length; i++) {
            final CellRef cellRefFrom = CellRef.createCellRef(from[i]);
            final CellRef cellRefTo = CellRef.createCellRef(to[i]);

            for (CTThreadedComment comment : threadedComments.getThreadedComments()) {
                if (cellRefFrom.equals(comment.getCellRef(false))) {
                    moveComment.put(comment, cellRefTo);
                }
            }
        }

        for (CTThreadedComment comment : moveComment.keySet()) {
            comment.setCellRef(moveComment.get(comment));
        }
    }

    public void moveNotes(int sheetIndex, String[] from, String[] to)
        throws InvalidFormatException {

        final WorksheetPart worksheetPart = (WorksheetPart)getSheetPart(sheetIndex);
        final SpreadsheetCommentsPart commentPart = worksheetPart.getCommentsPart(false);
        final CTComments comments = commentPart.getJaxbElement();
        final List<CTComment> commentList = comments.getCommentList().getComment();

        final CTComment[] cArray = new CTComment[from.length];
        final VMLBase[] vArray = new VMLBase[from.length];

        // collecting comments (from)
        int a, i;
        for(a=0; a < from.length; a++) {
            final CellRef f = CellRef.createCellRef(from[a]);
            for(i=0; i<commentList.size(); i++) {
                final CTComment comment = commentList.get(i);
                if(f.equals(comment.getCellRef(false))) {
                    cArray[a] = comment;
                    vArray[a] = getVMLForComment(worksheetPart, f.getColumn(), f.getRow());
                    break;
                }
            }
            if(i==commentList.size()) {
                // oh no, comment could not be found;
                throw new RuntimeException("xlsx::moveComments: comment could not be found");
            }
        }
        for(a=0; a < from.length; a++) {
            final CellRef t = CellRef.createCellRef(to[a]);
            cArray[a].setCellRef(t);
            if(vArray[a]!=null) {
                final VmlCore vmlCore = vArray[a].getVMLCoreObject();
                final CTClientData clientData = vmlCore.getClientData(false);
                if(clientData!=null&&clientData.getObjectType()==STObjectType.NOTE) {
                    final Integer r = clientData.getRow();
                    final Integer c = clientData.getColumn();
                    if(r!=null&&c!=null) {
                        clientData.setRow(Integer.valueOf(t.getRow()));
                        clientData.setColumn(Integer.valueOf(t.getColumn()));
                    }
                }
            }
        }
    }
}
