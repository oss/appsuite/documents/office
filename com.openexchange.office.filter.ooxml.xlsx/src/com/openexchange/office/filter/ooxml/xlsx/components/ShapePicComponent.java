/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.dml.spreadsheetDrawing.CTPicture;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.Picture;
import com.openexchange.office.filter.ooxml.xlsx.tools.Drawings;

public class ShapePicComponent extends XlsxComponent implements IShapeType {

	final private CTPicture pic;
	private AnchorBase cellAnchor;

	public ShapePicComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, CTPicture pic, AnchorBase cellAnchor, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.pic = pic;
        this.cellAnchor = cellAnchor;
	}

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        return null;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {
        if(cellAnchor!=null) {
            final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
            if(drawingAttrs!=null) {
                getNode().setData(Drawings.setDrawingAnchor(drawingAttrs, cellAnchor));
            }
        }
        Picture.applyAttrsFromJSON(operationDocument, attrs, operationDocument.getContextPart(), pic, cellAnchor!=null);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
        throws JSONException {

        if(cellAnchor!=null) {
            Drawings.createAnchorOperations(cellAnchor, attrs);
        }
        Picture.createJSONAttrs(operationDocument, attrs, operationDocument.getContextPart(), pic, cellAnchor!=null);
        return attrs;
    }

	@Override
	public ShapeType getType() {
		return ShapeType.IMAGE;
	}
}
