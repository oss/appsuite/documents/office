/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.xlsx.components;

import org.docx4j.dml.spreadsheetDrawing.AnchorBase;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.SpreadsheetML.ChartsheetPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.xlsx4j.sml.CTChartsheet;
import org.xlsx4j.sml.CTDrawing;
import org.xlsx4j.sml.Sheet;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.xlsx.XlsxOperationDocument;

public class ChartsheetWrapper implements INodeAccessor<AnchorBase> {

    final private ChartsheetPart chartsheetPart;
    private DLList<AnchorBase> content;

    public ChartsheetWrapper(DLNode<Object> sheetNode, XlsxOperationDocument operationDocument) {

        final Sheet sheet = (Sheet)sheetNode.getData();
        final RelationshipsPart relationshipPart = operationDocument.getPackage().getWorkbookPart().getRelationshipsPart();
        chartsheetPart = (ChartsheetPart)relationshipPart.getPart(sheet.getId());
        final CTChartsheet worksheet = chartsheetPart.getJaxbElement();
        final CTDrawing drawing = worksheet.getDrawing();
        if(drawing!=null) {
            final String drawingId = drawing.getId();
            if(drawingId!=null&&!drawingId.isEmpty()) {
                final RelationshipsPart sourceRelationships = chartsheetPart.getRelationshipsPart();
                if(sourceRelationships!=null) {
                    final Part drawingsPart = sourceRelationships.getPart(drawingId);
                    if(drawingsPart instanceof org.docx4j.openpackaging.parts.DrawingML.Drawing) {
                        final org.docx4j.dml.spreadsheetDrawing.CTDrawing drawings = ((org.docx4j.openpackaging.parts.DrawingML.Drawing)drawingsPart).getJaxbElement();
                        content = drawings.getContent();
                        operationDocument.setContextPart(drawingsPart);
                    }
                }
            }
        }
    }

    @Override
    public DLList<AnchorBase> getContent() {
        if(content==null) {
            content = new DLList<AnchorBase>();
        }
        return content;
    }
}
