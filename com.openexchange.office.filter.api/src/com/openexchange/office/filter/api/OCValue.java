/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum OCValue {

    CHANGE_AUTO_STYLE("changeAutoStyle", "cas"),
    CHANGE_CELLS("changeCells", "cc"),
    CHANGE_CF_RULE("changeCFRule", "ccf"),
    CHANGE_CHART_AXIS("changeChartAxis", "ccha"),
    CHANGE_CHART_GRID("changeChartGrid", "cchg"),
    CHANGE_CHART_LEGEND("changeChartLegend", "cchl"),
    CHANGE_CHART_SERIES("changeChartSeries", "cchs"),
    CHANGE_CHART_TITLE("changeChartTitle", "ccht"),
    CHANGE_COLUMNS("changeColumns", "ccol"),
    CHANGE_COMMENT("changeComment", "cco"),
    CHANGE_DRAWING("changeDrawing", "cdr"),
    CHANGE_DV_RULE("changeDVRule", "cva"),
    CHANGE_LAYOUT("changeLayout", "cla"),
    CHANGE_MASTER("changeMaster", "cma"),
    CHANGE_NAME("changeName", "cn"),
    CHANGE_NOTE("changeNote", "cno"),
    CHANGE_ROWS("changeRows", "cro"),
    CHANGE_SHEET("changeSheet", "changeSheet"),
    CHANGE_STYLE_SHEET("changeStyleSheet", "css"),
    CHANGE_TABLE("changeTable", "ct"),
    CHANGE_TABLE_COLUMN("changeTableColumn", "ctc"),
    COPY_SHEET("copySheet", "cs"),
    CREATE_ERROR("createError", "ce"),
    DATE("date", null),
    DELETE("delete", "d"),
    DELETE_AUTO_STYLE("deleteAutoStyle", "das"),
    DELETE_CF_RULE("deleteCFRule", "dcf"),
    DELETE_CHART_AXIS("deleteChartAxis", null),
    DELETE_CHART_SERIES("deleteChartSeries", "dchs"),
    DELETE_COLUMNS("deleteColumns", "dcol"),
    DELETE_COMMENT("deleteComment", "dco"),
    DELETE_DRAWING("deleteDrawing", "dd"),
    DELETE_DV_RULE("deleteDVRule", "dva"),
    DELETE_HEADER_FOOTER("deleteHeaderFooter", "dhf"),
    DELETE_HYPERLINK("deleteHyperlink", "dhy"),
    DELETE_LIST_STYLE("deleteListStyle", "dls"),
    DELETE_NAME("deleteName", "dn"),
    DELETE_NOTE("deleteNote", "dno"),
    DELETE_NUMBER_FORMAT("deleteNumberFormat", "dnf"),
    DELETE_ROWS("deleteRows", "dro"),
    DELETE_SHEET("deleteSheet", "dsh"),
    DELETE_STYLE_SHEET("deleteStyleSheet", "dss"),
    DELETE_TABLE("deleteTable", "dta"),
    DELETE_TARGET_SLIDE("deleteTargetSlide", null),
    DOWN("down", "down"),
    GROUP("group", "g"),
    INSERT_AUTO_STYLE("insertAutoStyle", "ias"),
    INSERT_BOOKMARK("insertBookmark", "ibm"),
    INSERT_CELLS("insertCells", "ic"),
    INSERT_CF_RULE("insertCFRule", "icr"),
    INSERT_CHART_SERIES("insertChartSeries", "ichs"),
    INSERT_COLUMN("insertColumn", "ico"),
    INSERT_COLUMNS("insertColumns", "icol"),
    INSERT_COMMENT("insertComment", "icom"),
    INSERT_COMPLEX_FIELD("insertComplexField", "icf"),
    INSERT_DRAWING("insertDrawing", "id"),
    INSERT_DV_RULE("insertDVRule", "iva"),
    INSERT_FIELD("insertField", "if"),
    INSERT_FONT_DESCRIPTION("insertFontDescription", "ifd"),
    INSERT_HARD_BREAK("insertHardBreak", "ihb"),
    INSERT_HEADER_FOOTER("insertHeaderFooter", "ihf"),
    INSERT_HYPERLINK("insertHyperlink", "ihl"),
    INSERT_LAYOUT_SLIDE("insertLayoutSlide", "ils"),
    INSERT_LIST_STYLE("insertListStyle", "ili"),
    INSERT_MASTER_SLIDE("insertMasterSlide", "ims"),
    INSERT_NAME("insertName", "in"),
    INSERT_NOTE("insertNote", "ino"),
    INSERT_NUMBER_FORMAT("insertNumberFormat", "inf"),
    INSERT_PARAGRAPH("insertParagraph", "ip"),
    INSERT_RANGE("insertRange", "ira"),
    INSERT_ROWS("insertRows", "iro"),
    INSERT_SHEET("insertSheet", "ish"),
    INSERT_SLIDE("insertSlide", "isl"),
    INSERT_STYLE_SHEET("insertStyleSheet", "iss"),
    INSERT_TAB("insertTab", "itb"),
    INSERT_TABLE("insertTable", "ita"),
    INSERT_TEXT("insertText", "it"),
    INSERT_THEME("insertTheme", "ith"),
    LEFT("left", "left"),
    MERGE_CELLS("mergeCells", "mc"),
    MERGE_PARAGRAPH("mergeParagraph", "mp"),
    MERGE_TABLE("mergeTable", "mt"),
    MOVE("move", "mo"),
    MOVE_CELLS("moveCells", null),
    MOVE_COMMENTS("moveComments", null),
    MOVE_DRAWING("moveDrawing", "md"),
    MOVE_LAYOUT_SLIDE("moveLayoutSlide", "mls"),
    MOVE_NOTES("moveNotes", "mno"),
    MOVE_SHEET("moveSheet", "msh"),
    MOVE_SHEETS("moveSheets", null),
    MOVE_SLIDE("moveSlide", "msl"),
    NO_OP("noOp", "no"),
    RIGHT("right", "right"),
    SET_ATTRIBUTES("setAttributes", "sa"),
    SET_DOCUMENT_ATTRIBUTES("setDocumentAttributes", "sda"),
    SPLIT_PARAGRAPH("splitParagraph", "sp"),
    SPLIT_TABLE("splitTable", "st"),
    UNGROUP("ungroup", "ug"),
    UNKNOWN_VALUE("unknownValue", "uk"),
    UP("up", "up"),
    UPDATE_COMPLEX_FIELD("updateComplexField", "ucf"),
    UPDATE_FIELD("updateField", "uf");

    private String value;
    final private String longName;
    final private String shortName;

    private static Map<String, OCValue> nameToEnumMap = Collections.unmodifiableMap(initializeNameToEnumMap(true));

    private OCValue(String longName, String shortName) {
        this.longName = longName;
        this.shortName = shortName;
        this.value = shortName!=null ? shortName : longName;
    }

    public String value() {
        return value;
    }

    public String value(boolean useLongName) {
        return useLongName ? longName : value;
    }

    public static OCValue fromValue(String val) {
        final OCValue value = nameToEnumMap.get(val);
        if(value==null) {
            return UNKNOWN_VALUE;
        }
        return value;
    }

    private static Map<String, OCValue> initializeNameToEnumMap(boolean useShortName) {
        final Map<String, OCValue> mMap = new HashMap<String, OCValue>();
        for (OCValue s : OCValue.values()) {
            s.value = s.longName;
            if(useShortName && s.shortName!=null) {
                s.value = s.shortName;
            }
            mMap.put(s.value, s);
        }
        return mMap;
    }

    public static void debugOnly(boolean useShortName) {
        nameToEnumMap = Collections.unmodifiableMap(initializeNameToEnumMap(useShortName));
    }

    public static OCValue fromShortNameValue(String val) {
        for(OCValue value:OCValue.values()) {
            if(value.shortName.equals(val)) {
                return value;
            }
        }
        return OCValue.UNKNOWN_VALUE;
    }

    public static OCValue fromLongNameValue(String val) {
        for(OCValue value:OCValue.values()) {
            if(value.longName.equals(val)) {
                return value;
            }
        }
        return OCValue.UNKNOWN_VALUE;
    }

    @Override
    public String toString() {
        return value(true);
    }
}
