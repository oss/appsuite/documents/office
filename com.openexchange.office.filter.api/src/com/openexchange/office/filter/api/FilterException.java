/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

public class FilterException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public enum ErrorCode {
        CRITICAL_ERROR,
        WRONG_PASSWORD,
        UNSUPPORTED_ENCRYPTION_USED,
        COMPLEXITY_TOO_HIGH,
        MEMORY_USAGE_TOO_HIGH,
        MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED,
        UNSUPPORTED_OPERATION_USED,
        STRICT_OOXML_NOT_SUPPORTED,
        FEATURE_NOT_SUPPORTED
    };

    public static String ST_MAX_SHEET_COUNT_EXCEEDED = "MAX_SHEET_COUNT_EXCEEDED";
    public static String ST_FRAME_ATTACHED_TO_FRAME_NOT_SUPPORTED = "FRAME_ATTACHED_TO_FRAME_IS_NOT_SUPPORTED";
    public static String ST_MEDIATYPE_NOT_SUPPORTED = "MEDIATYPE_NOT_SUPPORTED";

    final private ErrorCode errorCode;
    final private Object value;
    private String subType = null;
    private int operationCount = 0;

    public FilterException(String message, ErrorCode eCode) {
        super(message);
        errorCode = eCode;
        value = null;
    }

    public FilterException(String message, ErrorCode eCode, Object value) {
        super(message);
        errorCode = eCode;
        this.value = value;
    }

    public FilterException(String message, Throwable e, ErrorCode eCode) {
        super(message, e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
        value = null;
    }

    public FilterException(String message, Throwable e, ErrorCode eCode, Object value) {
        super(message, e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
        this.value = value;
    }

    public FilterException(Throwable e, ErrorCode eCode) {
        super(e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
        value = null;
    }

    public FilterException(Throwable e, ErrorCode eCode, Object value) {
        super(e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
        this.value = value;
    }

    public ErrorCode getErrorcode() {
        return errorCode;
    }

    public Object getValue() {
        return value;
    }

    public String getSubType() {
    	return subType;
    }

    public void setSubType(String v) {
    	subType = v;
    }
    public int getOperationCount() {
    	return operationCount;
    }
    public void setOperationCount(int count) {
    	operationCount = count;
    }
}
