/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

import java.io.InputStream;
import java.util.Map;
import com.openexchange.session.Session;

/**
 * A partitioning importer can provide the operations and meta data
 * of a document stream in parts. The importer must be initialized
 * with the necessary data and meta data, the active part and all
 * remaining parts can be retrieved. The importer must provide the
 * number of parts within the meta data.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner
 * @since 7.8.2
 */
public interface IPartImporter {

	/**
	 * Initializes the importer with the necessary data to provide
	 * partition support for the document.
	 *
	 * @param session the session of the client requesting to import the document
	 * @param inputDocument the document stream which must be seekable
	 * @param documentProperties the document properties that specifies certain
	 *                           information to the filter. E.g. how much memory
	 *                           can be used, etc.
	 * @throws FilterException
	 */
    public void initPartitioning(final Session session, final InputStream inputDocument, final DocumentProperties documentProperties, boolean createFastLoadOperations)
		throws FilterException;

    /**
     * Provides the meta data for the provided document. One of the meta data
     * properties must specify in how many parts the document will be sliced.
     *
	 * @param documentProperties the document properties that specifies certain
	 *                           information to the filter. E.g. how much memory
	 *                           can be used, etc.
	 *
     * @return a map containing the meta data of the document as properties. The
     *         must contain the property "numberOfParts" which provides the number
     *         of parts including the active part.
     */
    public Map<String, Object> getMetaData(DocumentProperties documentProperties)
    	throws FilterException;

    /**
     * Provides the active part of the provided document stream. It's up to
     * the implementation to decide which is the active part. Normally this
     * would be the active sheet, active slide or the first part of a text
     * document.
     *
	 * @param documentProperties the document properties that specifies certain
	 *                           information to the filter. E.g. how much memory
	 *                           can be used, etc.
	 *
     * @return the active part of the provided document stream. The map must
     *         at least provide the property "operations" which is a JSONArray
     *         containing operations. The array may be empty.
     */
    public Map<String, Object> getActivePart(DocumentProperties documentProperties)
    	throws FilterException;

    /**
     * Provides the next part of the provided document stream. The filter should
     * chose a logical way to provide the next part. E.g. for a text document the next
     * part after the active part would be the following elements, for spreadsheet the
     * following sheet or the previous one and the same would be true for presentation
     * documents.
     *
	 * @param documentProperties the document properties that specifies certain
	 *                           information to the filter. E.g. how much memory
	 *                           can be used, etc.
	 *
     *
     * @return the next part of the provided document stream or null if there is
     *         no part. The map must at least provide the property "operations"
     *         which is a JSONArray containing operations. The array may be empty.
     */
    public Map<String, Object> getNextPart(DocumentProperties documentProperties)
    	throws FilterException;
}
