/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

import java.util.HashMap;

import org.w3c.dom.DocumentType;

/**
 * {@link DocumentType}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
public class DocumentProperties extends HashMap<String, Object> {

	private static final long serialVersionUID = 4912557177778757666L;

	// TYPE String
    static final public String PROP_CREATOR = "Creator";

    // TYPE String
    static final public String PROP_LAST_MODIFIED_BY = "LastModifiedBy";

    // TYPE String
    static final public String PROP_USER_LANGUAGE = "UserLanguage";

    // TYPE String
    static final public String PROP_REVISION = "Revision";

    // TYPE String
    static final public String PROP_VERSION = "Version";

    // TYPE String
    static final public String PROP_INITIAL_SHEETNAME = "InitialSheetName";

    // TYPE String (stringified JSON object)
    static final public String PROP_INITIAL_FORMATS = "InitialFormats";

    // TYPE String
    static final public String PROP_UNIQUE_DOCUMENT_IDENTIFIER = "UniqueDocumentIdentifier";

    // TYPE Boolean
    static final public String PROP_SAVE_TEMPLATE_DOCUMENT = "SaveTemplateDocument";

    static final public String PROP_PREVIEWDATA = "PreviewData";

    static final public String PROP_DOCUMENT = "Document";

    // TYPE Integer
    static final public String PROP_SPREADHSEET_ACTIVE_SHEET_INDEX = "ActiveSheetIndex";

    // TYPE Integer
    static final public String PROP_SPREADHSEET_CURRENT_SHEET_INDEX = "CurrentSheetIndex";

    // TYPE Integer
    static final public String PROP_SPREADSHEET_SHEET_COUNT = "SheetCount";

    // TYPE Boolean
    static final public String PROP_CALCULATE_ON_LOAD = "CalculateOnLoad";

    // TYPE Long
    static final public String PROP_PAGESIZE_WIDTH = "PageSizeWidth";

    static final public String PROP_PAGESIZE_HEIGHT = "PageSizeHeight";

    // TYPE Boolean
    static final public String PROP_CREATED_BY_DEFAULT_TEMPLATE = "DefaultTemplate";

    // TYPE Boolean
    static final public String PROP_SAVE_DOC_HISTORY = "SaveDocumentHistory";

    // TYPE HashMap<String (Hash), String (url)>
    static final public String PROP_SHAPE2PNG_REPLACEMENT_IMAGES = "Shape2pngReplacementImages";

    public String optString(String key, String _default) {
        final Object val = get(key);
        if (val instanceof String) {
            return (String) val;
        }
        return _default;
    }

    public boolean optBoolean(String key, boolean _default) {
        final Object val = get(key);
        if (val instanceof Boolean) {
            return ((Boolean) val).booleanValue();
        }
        return _default;
    }

    public Integer optInteger(String key, int _default) {
        final Object val = get(key);
        if (val instanceof Integer) {
            return (Integer) val;
        }
        return _default;
    }

    public long optLong(String key, long _default) {
        final Object val = get(key);
        if (val instanceof Long) {
            return ((Long) val).longValue();
        }
        return _default;
    }
}
