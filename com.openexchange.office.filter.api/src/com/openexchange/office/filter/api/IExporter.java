/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

import java.io.InputStream;

import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

/**
 * {@link IExporter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IExporter {


    /**
     * @param originalOrEmptyTemplateDocument
     * @param mergeOperations
     * @param resourceManager
     * @param documentProperties
     * @param createFinalDocument (if true the export filter is allowed to remove unused graphics as they can't be accessed via undo operation etc.)
     *
     * @return the InputStream containing the created document with all mergeOperations applied
     */
    public InputStream createDocument(Session session, InputStream originalOrEmptyTemplateDocument, String mergeOperations, IResourceManager resourceManager, DocumentProperties documentProperties, boolean createFinalDocument)
        throws FilterException;
}
