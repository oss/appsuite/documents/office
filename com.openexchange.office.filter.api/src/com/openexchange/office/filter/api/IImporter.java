/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.api;

import java.io.InputStream;

import org.json.JSONObject;

import com.openexchange.session.Session;


/**
 * {@link IImporter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IImporter {

    /**
     * @param inputDocument
     * @return the JSON Object containing all operations from the given document
     */
    public JSONObject createOperations(Session session, InputStream inputDocument, DocumentProperties documentProperties, boolean createFastLoadOperations)
        throws FilterException;

    /**
     * @param templateDocument a optional template document can be given to allow the filter to modify templates (e.g. applying documentProperties, language etc.)
     *                         this template document is returned by the filter if no changes are applied
     * @param documentProperties
     * @return the InputStream containing the requested default document
     */
    public InputStream getDefaultDocument(InputStream templateDocument, DocumentProperties documentProperties)
        throws FilterException;
}
