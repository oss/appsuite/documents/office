pipeline {
    agent {
        kubernetes {
            label "documents-office-${UUID.randomUUID().toString()}"
            defaultContainer 'jnlp'
            yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: gradle
    image: gradle:5.6.4-jdk8
    command:
    - cat
    tty: true
"""
        }
    }
    options {
        buildDiscarder(logRotator(daysToKeepStr: '30'))
        checkoutToSubdirectory('office')
        disableConcurrentBuilds()
        gitLabConnection('Gitlab')
        gitlabBuilds(builds: ['Integration build'])
    }
    stages {
        stage('Integration build') {
            when {
               expression { 'main' == env.BRANCH_NAME }
            }
            options {
                gitlabCommitStatus(name: 'Integration build')
            }
            steps {
                script {
                    build job: 'appsuite/integration/main', parameters: [text(name: 'OVERWRITE_COMPONENTS', value: '')]
                }
            }
        }
    }
    post {
        failure {
            emailext attachLog: true,
                body: "${env.BUILD_URL} failed.\n\nFull log at: ${env.BUILD_URL}console\n\n",
                subject: "${env.JOB_NAME} (#${env.BUILD_NUMBER}) - ${currentBuild.result}",
                to: 'cc_full_stack_product_development@open-xchange.com'
        }
    }
}

String version4Documentation(String branchName) {
    if ('main' == branchName)
        return branchName
    if (branchName.startsWith('master-'))
        return branchName.substring(7)
    if (branchName.startsWith('release-'))
        return branchName.substring(8)
    return null
}
