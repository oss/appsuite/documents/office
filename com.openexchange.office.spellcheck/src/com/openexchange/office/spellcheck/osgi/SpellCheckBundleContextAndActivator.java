/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.spellcheck.osgi;

import com.openexchange.exception.OXException;
import com.openexchange.office.spellcheck.impl.SpellCheck;
import com.openexchange.office.spellcheck.impl.client.SpellCheckClient;
import com.openexchange.office.spellcheck.impl.client.SpellCheckHttpConfigProvider;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

//=============================================================================
public class SpellCheckBundleContextAndActivator extends OsgiBundleContextAndActivator {

	private static SpellCheckBundleContextAndActivator INSTANCE;

	public static SpellCheckBundleContextAndActivator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new SpellCheckBundleContextAndActivator();
		}
		return INSTANCE;
	}

	public SpellCheckBundleContextAndActivator() {
		super(new SpellCheckBundleContextIdentificator());
		INSTANCE = this;
	}

	@Override
	protected void onAfterCreateInstances() throws OXException {
		try {
            registerService(new SpellCheckHttpConfigProvider(), false);
		    registerService(new SpellCheckClient(), false);
			registerService(new SpellCheck(), false);
		} catch (IllegalArgumentException ex) {
			//...do nothing!!! The 6 lines fixes the problem with package installation but the try catch fixes the problem with eclipse
		}
	}

}
