/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.spellcheck.impl;


import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.office.spellcheck.api.ISpellCheck;
import com.openexchange.office.spellcheck.api.IValidator;
import com.openexchange.office.spellcheck.impl.client.SpellCheckClient;
import com.openexchange.office.tools.annotation.NonNull;
import com.openexchange.office.tools.annotation.RegisteredService;

/**
 * {@link SpellCheck}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@Service
@RegisteredService(registeredClass = ISpellCheck.class)
public class SpellCheck implements InitializingBean, DisposableBean, IValidator, ISpellCheck {

    /**
     * Initializes a new {@link SpellCheckClient}
     */
    public SpellCheck() {
        super();
    }

    // - InitializingBean ------------------------------------------------------

    /**
     *
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        m_enabled.set(m_configService.getBoolProperty("io.ox/office//module/spellingEnabled", true));

        LOG.info(new StringBuilder(128).append("SpellChecking is ").append(m_enabled.get() ? "enabled" : "disabled").append(" by configuration").toString());
    }

    // - DisposableBean --------------------------------------------------------

    /**
     *
     */
    @Override
    public void destroy() throws Exception {
        // Nothing to do
    }

    // - IValidator ------------------------------------------------------------

    /**
    *
    */
    @Override
    public boolean isEnabled() {
        return m_enabled.get();
    }

    /**
     *
     */
    @Override
    public boolean isValid() {
        return isEnabled() && (implGetValidSpellCheck() != null);
    }

    // - ISpellCheck ---------------------------------------------------------

    /**
     *
     */
    @Override
    public JSONObject checkSpelling(@NonNull final JSONObject jsonParams) throws Exception {
        final ISpellCheck spellCheck = implGetValidSpellCheck();

        return ((null != spellCheck) ? spellCheck.checkSpelling(jsonParams) : implGetErrorJsonObject());
    }

    /**
     *
     */
    @Override
    public JSONObject checkParagraphSpelling(@NonNull final JSONObject jsonParams) throws Exception {
        ISpellCheck spellCheck = implGetValidSpellCheck();

        return ((null != spellCheck) ? spellCheck.checkParagraphSpelling(jsonParams) : implGetErrorJsonObject());
    }

    /**
     *
     */
    @Override
    public JSONObject suggestReplacements(@NonNull final JSONObject jsonParams) throws Exception {
        final ISpellCheck spellCheck = implGetValidSpellCheck();

        return ((null != spellCheck) ? spellCheck.suggestReplacements(jsonParams) : implGetErrorJsonObject());
    }

    /**
     *
     */
    @Override
    public JSONObject isMisspelled(@NonNull final JSONObject jsonParams) throws Exception {
        final ISpellCheck spellCheck = implGetValidSpellCheck();

        return ((null != spellCheck) ? spellCheck.isMisspelled(jsonParams) : implGetErrorJsonObject());
    }

    /**
     *
     */
    @Override
    public JSONObject getSupportedLocales() throws Exception {
        final ISpellCheck spellCheck = implGetValidSpellCheck();

        return ((null != spellCheck) ? spellCheck.getSupportedLocales() : implGetErrorJsonObject());
    }

    // - Implementation --------------------------------------------------------

    /**
     * implGetValidSpellCheck
     *
     * @return a valid ISpellCheck instance or <code>null</code>
     */
    private ISpellCheck implGetValidSpellCheck() {
        return m_spellCheckClient.isEnabled() ? m_spellCheckClient : null;
    }

    /**
     * @return
     */
    private JSONObject implGetErrorJsonObject() {
        return new JSONObject();
    }

    // - Static Members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(SpellCheck.class);

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_enabled = new AtomicBoolean(false);

    @Autowired
    private ConfigurationService m_configService;

    @Autowired
    private SpellCheckClient m_spellCheckClient;
}
