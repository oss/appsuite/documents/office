/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.spellcheck.impl.client;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Throwables;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.config.Reloadables;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.office.spellcheck.api.ISpellCheck;
import com.openexchange.office.spellcheck.api.IValidator;
import com.openexchange.office.tools.annotation.NonNull;
import com.openexchange.office.tools.annotation.Nullable;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.rest.client.httpclient.HttpClients;
import com.openexchange.timer.TimerService;
import io.micrometer.core.instrument.Tags;

/**
 * {@link SpellCheckClient}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 *
 */
@Service
@RegisteredService(registeredClass = Reloadable.class)
public class SpellCheckClient implements InitializingBean, Reloadable, IValidator, ISpellCheck {

    /**
     * {@link ConnectionDown}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    @SuppressWarnings("serial")
    private static class ConnectionDown extends Exception {

        /**
         * Initializes a new {@link ConnectionDown}.
         */
        public ConnectionDown() {
            super();
        }
    }

    // -------------------------------------------------------------------------

    final public static String CONFIG_KEY_SPELLCHECK_SERVICE_URL = "com.openexchange.office.spellcheck.remoteURL";

    final private static String HTTPSERVER_ID_SPELLCHECK = "77fdc062-570b-11ea-91c3-8bd6e5445cb5";

    final private static String CONFIG_KEY_SPELLCHECK_ENABLED = "io.ox/office//module/spellingEnabled";

    final private static String CONFIG_DEFAULT_SPELLCHECK_SERVICE_URL = "http://localhost:8003";

    final private static String DEFAULT_ENCODING = "UTF-8";
    final private static String DEFAULT_LOCALE = "en_US";

    final private static String PATH_SPELLCHECK_SERVICE_CHECK_SPELLING = "/check-spelling";
    final private static String PATH_SPELLCHECK_SERVICE_CHECK_PARAGRAPH_SPELLING = "/check-paragraph-spelling";
    final private static String PATH_SPELLCHECK_SERVICE_GET_SUPPORTED_LOCALES = "/supported-locales";
    final private static String PATH_SPELLCHECK_SERVICE_HEALTH = "/health";
    final private static String PATH_SPELLCHECK_SERVICE_IS_MISSPELLED = "/is-misspelled";
    final private static String PATH_SPELLCHECK_SERVICE_SUGGEST_REPLACEMENTS = "/suggest-replacements";
    final private static String PATH_SPELLCHECK_SERVICE_READY = "/ready";

    final private static String KEY_ENCODING = "encoding";
    final private static String KEY_HEALTH = "health";
    final private static String KEY_LOCALE = "locale";
    final private static String KEY_PARAGRAPH = "paragraph";
    final private static String KEY_REPLACEMENTS = "replacements";
    final private static String KEY_SERVERAPI = "serverAPI";
    final private static String KEY_SERVERID = "serverId";
    final private static String KEY_SPELLRESULT = "spellResult";
    final private static String KEY_SUPPORTEDLOCALES = "supportedLocales";
    final private static String KEY_SUPPORTEDLOCALES_ALT = "SupportedLocales";
    final private static String KEY_TEXT = "text";
    final private static String KEY_WORD = "word";

    final private static String MIME_TYPE_JSON = "application/json";

    final private static JSONArray EMPTY_JSON_ARRAY = new JSONArray();

    final private static int SC_TOO_MANY_REQUESTS = 429;

    final private static long LOG_CONNECTION_STATUS_PERIOD_MILLIS = 300 * 1000L;

    final private static String LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR = Long.toString(LOG_CONNECTION_STATUS_PERIOD_MILLIS / 1000L);

    final private static String STR_VALIDATION = "validation";
    final private static String STR_SPELLCHECK = "spellcheck";
    final private static String STR_LOG_CONNECTION_ERROR = "SpellCheck client received exception during remote {} call";

    final private static ConnectionDown CONNECTION_DOWN_EXCEPTION = new ConnectionDown();

    /**
     * Initializes a new {@link SpellCheckClient}.
     */
    public SpellCheckClient() {
        super();

        m_clientMetrics = new SpellCheckClientMetrics();
        m_versionValidator = implCreateVersionValidator();

        m_logTimerExecutor.scheduleWithFixedDelay(() -> implLogConnectionStatus(),
            LOG_CONNECTION_STATUS_PERIOD_MILLIS, LOG_CONNECTION_STATUS_PERIOD_MILLIS, TimeUnit.MILLISECONDS);
    }

    // - InitializingBean ------------------------------------------------------

    @Override
    public void afterPropertiesSet() throws Exception {
        reloadConfiguration(m_configService);
   }

    // - Reloadable ------------------------------------------------------------

    /**
     *
     */
    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        m_versionValidator.setVersionInvalid(false);
        m_versionValidator.setEnabled(false);

        implResetLocalesAndEncodings();

        LOG.info("SpellCheck client (re)loads configuration properties: [{}]",
            CONFIG_KEY_SPELLCHECK_SERVICE_URL);

        final boolean isEnabled = (null != (m_spellCheckURL = implReadConfig()));

        m_versionValidator.setEnabled(isEnabled);

        if (isEnabled) {
            // try connection in 15s
            if (null != m_timerService) {
                m_timerService.schedule(() -> {
                    m_versionValidator.getVersion(false);
                }, 15000);
            }
        }
     }

    /**
     *
     */
    @Override
    public Interests getInterests() {
        return Reloadables.interestsForProperties(new String[] {
            CONFIG_KEY_SPELLCHECK_SERVICE_URL
        });
    }

    // - IValidator ------------------------------------------------------------

    /**
     *
     */
    @Override
    public boolean isEnabled() {
        return m_versionValidator.isEnabled();
    }

    /**
     *
     */
    @Override
    public boolean isValid() {
        return m_versionValidator.isValid();
    }

    // - ISpellCheck ---------------------------------------------------------

    /**
     *
     */
    @Override
    @Nullable public JSONObject checkSpelling(@NonNull final JSONObject jsonParams) throws Exception {
        Map<String, String> localesAndEncodings = null;
        JSONObject ret = null;

        if (isValid()) {
            if (jsonParams.isObject() && (null != (localesAndEncodings = impGetLocalesAndEncodings()))) {
                if (null == (ret = implGetEmptyJSONArray(jsonParams, KEY_TEXT))) {
                    // Preprocess request
                    final String word = jsonParams.optString(KEY_TEXT);
                    final String locale = jsonParams.optString(KEY_LOCALE, DEFAULT_LOCALE);

                    jsonParams.put(KEY_TEXT, implStringCodec(word, locale, localesAndEncodings, true));

                    if (null != (ret = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_CHECK_SPELLING, jsonParams))) {
                        implPostProcessResponse(localesAndEncodings, ret, locale, KEY_WORD, KEY_REPLACEMENTS);
                    }
                }
            }
        } else {
            m_clientMetrics.incrementConnectionStatusCount(SpellCheckClientMetrics.ConnectionType.SPELLCHECK, CONNECTION_DOWN_EXCEPTION);
        }

        return ret;
    }

    /**
     *
     */
    @Override
    @Nullable public JSONObject checkParagraphSpelling(@NonNull final JSONObject jsonParams) throws Exception {
        Map<String, String> localesAndEncodings = null;
        JSONObject ret = null;

        if (isValid()) {
            if (jsonParams.isObject() && (null != (localesAndEncodings = impGetLocalesAndEncodings()))) {
                // Preprocess request
                boolean isEmptyRequest = true;

                try {
                    // perform shortcut check for empty 'word' paragraph
                    // array param to avoid unnecessary remote call and encode
                    // all characters to the required dictionary encoding
                    JSONArray jsonArray = jsonParams.optJSONArray(KEY_PARAGRAPH);

                    // set encoded strings according to dictionary encoding
                    for (int i = 0, len = ((null != jsonArray) ? jsonArray.length() : 0); i < len; ++i) {
                        final JSONObject jsonObj = jsonArray.optJSONObject(i);

                        if (null != jsonObj) {
                            final String word = jsonObj.optString(KEY_WORD);
                            final String locale = jsonObj.optString(KEY_LOCALE, DEFAULT_LOCALE);

                            if (StringUtils.isNotBlank(word)) {
                                jsonObj.put(KEY_WORD, implStringCodec(word, locale, localesAndEncodings, true));
                                isEmptyRequest = false;
                            }
                        }
                    }

                    if (isEmptyRequest) {
                        ret = new JSONObject().put(KEY_SPELLRESULT, EMPTY_JSON_ARRAY);
                    }
                } catch (Exception e) {
                    LOG.warn("SpellCheck client received exception in shortcut check for empty request: {}", Throwables.getRootCause(e));
                }

                // execute request in case the requested words are not empty at all
                if (!isEmptyRequest && (null != (ret = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_CHECK_PARAGRAPH_SPELLING, jsonParams)))) {
                    final JSONArray jsonArray = ret.optJSONArray(KEY_SPELLRESULT);

                    for (int pos = 0, len = ((null != jsonArray) ? jsonArray.length() : 0); pos < len; ++pos) {
                        final JSONObject jsonObj = jsonArray.optJSONObject(pos);

                        if (null != jsonObj) {
                            implPostProcessResponse(localesAndEncodings, jsonObj, jsonObj.optString(KEY_LOCALE), KEY_WORD, KEY_REPLACEMENTS);
                        }
                    }
                }
            }
        } else {
            m_clientMetrics.incrementConnectionStatusCount(SpellCheckClientMetrics.ConnectionType.SPELLCHECK, CONNECTION_DOWN_EXCEPTION);
        }

        return ret;
    }

    /**
     *
     */
    @Override
    @Nullable public JSONObject suggestReplacements(@NonNull final JSONObject jsonParams) throws Exception {
        Map<String, String> localesAndEncodings = null;
        JSONObject ret = null;

        if (isValid()) {
            if (jsonParams.isObject() && (null != (localesAndEncodings = impGetLocalesAndEncodings()))) {
                if (null == (ret = implGetEmptyJSONArray(jsonParams, KEY_WORD))) {
                    // Preprocess request
                    final String word = jsonParams.optString(KEY_WORD);
                    final String locale = jsonParams.optString(KEY_LOCALE, DEFAULT_LOCALE);

                    jsonParams.put(KEY_WORD, implStringCodec(word, locale, localesAndEncodings, true));

                    if (null != (ret = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_SUGGEST_REPLACEMENTS, jsonParams))) {
                        implPostProcessResponse(localesAndEncodings, ret, locale, null, KEY_SPELLRESULT);
                    }
                }
            }
        } else {
            m_clientMetrics.incrementConnectionStatusCount(SpellCheckClientMetrics.ConnectionType.SPELLCHECK, CONNECTION_DOWN_EXCEPTION);
        }

        return ret;
    }

    /**
    *
    */
    @Override
    @Nullable public JSONObject isMisspelled(@NonNull final JSONObject jsonParams) throws Exception {
        Map<String, String> localesAndEncodings = null;
        JSONObject ret = null;

        if (isValid()) {
            if (jsonParams.isObject() && (null != (localesAndEncodings = impGetLocalesAndEncodings()))) {
                if (null == implGetEmptyJSONArray(jsonParams, KEY_WORD)) {
                    // Preprocess request
                    final String word = jsonParams.optString(KEY_WORD);
                    final String locale = jsonParams.optString(KEY_LOCALE, DEFAULT_LOCALE);

                    jsonParams.put(KEY_WORD, implStringCodec(word, locale, localesAndEncodings, true));

                    ret = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_IS_MISSPELLED, jsonParams);
                } else {
                    ret = new JSONObject().put(KEY_SPELLRESULT, false);
                }
            }
        } else {
            m_clientMetrics.incrementConnectionStatusCount(SpellCheckClientMetrics.ConnectionType.SPELLCHECK, CONNECTION_DOWN_EXCEPTION);
        }

        return ret;
    }

    /**
    *
    */
    @Override
    @Nullable public JSONObject getSupportedLocales() throws Exception {
        Map<String, String> localesAndEncodings = null;

        if (isValid()) {
            if (null != (localesAndEncodings = impGetLocalesAndEncodings())) {
                return new JSONObject().put("SupportedLocales", new JSONArray(localesAndEncodings.keySet()));
            }
        } else {
            m_clientMetrics.incrementConnectionStatusCount(SpellCheckClientMetrics.ConnectionType.SPELLCHECK, CONNECTION_DOWN_EXCEPTION);
        }

        return null;
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        // TODO (KA): check if nothing is to be done!
    }

    /**
     * @return
     */
    public String getSpellCheckURL() {
        return m_spellCheckURL;
    }

    /**
     * @return
     */
    public boolean isServerReady() {
        return m_versionValidator.isReady();
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param configService
     * @return The URL string for the remote server or <code>null</code> if URL is not valid or available
     */
    @Nullable private String implReadConfig() {
        final ConfigurationService configService = m_configService;
        String ret = null;

        if ((null != configService) && configService.getBoolProperty(CONFIG_KEY_SPELLCHECK_ENABLED, true)) {
            // evaluate SpellChecking service URL
            String spellCheckServiceUrlStr = configService.getProperty(CONFIG_KEY_SPELLCHECK_SERVICE_URL, null);
            String errorMsg = null;

            if (StringUtils.isBlank(spellCheckServiceUrlStr)) {
                spellCheckServiceUrlStr = CONFIG_DEFAULT_SPELLCHECK_SERVICE_URL;

                LOG.info("SpellCheck client service URL is not set via config item {}. Using default service URL {}",
                    CONFIG_KEY_SPELLCHECK_SERVICE_URL, CONFIG_DEFAULT_SPELLCHECK_SERVICE_URL);
            }

            if (StringUtils.isNotBlank(spellCheckServiceUrlStr)) {
                try {
                    final URI spellCheckURI = new URI(spellCheckServiceUrlStr);
                    final String usedScheme = spellCheckURI.getScheme().toLowerCase().trim();

                    if ("http".equals(usedScheme) || "https".equals(usedScheme)) {
                        ret = spellCheckServiceUrlStr;
                    } else {
                        errorMsg = "SpellCheck client service URL has no 'http' or 'https' protocol specified (" + spellCheckServiceUrlStr + ")";
                    }
                } catch (Exception e) {
                    errorMsg = "SpellCheck client service URL not valid (" + spellCheckServiceUrlStr + "): " + Throwables.getRootCause(e);
                }
            }

            if (null != errorMsg) {
                LOG.error(errorMsg);
            }
        }

        return ret;
    }

    /**
     * @return
     */
    private VersionValidator implCreateVersionValidator() {
        return new VersionValidator(
            () -> {
                // getVersionHandler
                try {
                    final JSONObject jsonResponse = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_HEALTH, null, SpellCheckClientMetrics.ConnectionType.VALIDATION);

                    if (null != jsonResponse) {
                        final JSONObject jsonHealth = jsonResponse.optJSONObject(KEY_HEALTH);
                        final String serverId = (null != jsonHealth) ? jsonHealth.optString(KEY_SERVERID) : null;

                        if (HTTPSERVER_ID_SPELLCHECK.equalsIgnoreCase(serverId)) {
                            return implUpdateLocalesAndEncodings() ?
                                jsonHealth.optInt(KEY_SERVERAPI, VersionValidator.VERSION_INVALID) :
                                    VersionValidator.VERSION_INVALID;
                        }
                    } else if (m_subsequentRemoteCall.compareAndSet(false, true)) {
                        LOG.trace("SpellCheck client is not able to get remote connection to {}. Please install SpellCheck service and set appropriate config item '{}'.",
                            m_spellCheckURL, CONFIG_KEY_SPELLCHECK_SERVICE_URL);
                    }
                } catch (IOException e) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("SpellCheck client received exception while validating server response: {} [{}]",
                            m_spellCheckURL, Throwables.getRootCause(e));
                    }
                } catch (Exception e) {
                    LOG.trace("SpellCheck client received exception in #getVersion handler call: {} [{}]",
                        m_spellCheckURL, Throwables.getRootCause(e));
                }

                return VersionValidator.VERSION_INVALID;
            },
            (final int oldVersion, final int newVersion) -> {
                // gotValidVersionHandler
                LOG.info("SpellCheck client established remote connection to: {} [serverAPIVersion: {}]",
                    (null != m_spellCheckURL) ? m_spellCheckURL : "n/a", newVersion);
            },
            (final int oldVersion, final int newVersion) -> {
                // lostValidVersionHandler
                implResetLocalesAndEncodings();

                LOG.trace("SpellCheck client lost remote connection to: {}",
                    (null != m_spellCheckURL) ? m_spellCheckURL : "n/a");
            },
            () -> {
                // isReadyHandler
                try {
                    return (null != implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_READY, null, SpellCheckClientMetrics.ConnectionType.VALIDATION));
                } catch (Exception e) {
                    LOG.trace("SpellCheck client received exception in #isReady handler call: {} [{}]",
                        m_spellCheckURL, Throwables.getRootCause(e));
                }

                return false;
            });
    }

    /**
     * @return
     * @throws Exception
     */
    boolean implUpdateLocalesAndEncodings() throws Exception {
        final JSONObject jsonLocales = implExecuteHttpRequest(PATH_SPELLCHECK_SERVICE_GET_SUPPORTED_LOCALES, null);

        if (null != jsonLocales) {
            JSONArray jsonLocalesArray = jsonLocales.optJSONArray(KEY_SUPPORTEDLOCALES);

            if (null == jsonLocalesArray) {
                jsonLocalesArray = jsonLocales.optJSONArray(KEY_SUPPORTEDLOCALES_ALT);
            }

            if (null != jsonLocalesArray) {
                final Map<String, String> localesMap = new HashMap<>(jsonLocalesArray.length());

                for (final Object curObj : jsonLocalesArray.asList()) {
                    if (curObj instanceof Map) {
                        final Map<?, ?> curMap = (Map<?, ?>) curObj;
                        final String curLocale = (String) curMap.get(KEY_LOCALE);
                        final String curEncoding = (String) curMap.get(KEY_ENCODING);

                        localesMap.put(implGetNormalizedLocale(curLocale), (null != curEncoding) ? curEncoding : DEFAULT_ENCODING);
                    } else if (curObj instanceof String) {
                        localesMap.put(implGetNormalizedLocale((String) curObj), DEFAULT_ENCODING);
                    } else {
                        LOG.error("SpellCheck client updating locales received no valid object");
                    }
                }

                synchronized (m_localesAndEncodings) {
                    m_localesAndEncodings.set(localesMap);
                }

                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    void implResetLocalesAndEncodings() {
        synchronized (m_localesAndEncodings) {
            final Map<String, String> localesMap = m_localesAndEncodings.get();

            if (null != localesMap) {
                localesMap.clear();
            }
        }
    }

    /**
     * @param retLocalesAndEncodings
     * @return <code>true</code> if the LocalesAndEncodings member contains valid entries and can be used
     */
    Map<String, String> impGetLocalesAndEncodings() {
        Map<String, String> ret = new HashMap<>(16);

        synchronized (m_localesAndEncodings) {
            final Map<String, String> localesMap = m_localesAndEncodings.get();

            if (null != localesMap) {
                ret.putAll(localesMap);
            }
        }

        return (ret.size() > 0) ? ret : null;
    }

    /**
     * @param locale
     * @return
     */
    String implGetNormalizedLocale(final String locale) {
        return StringUtils.isNotBlank(locale) ?
            locale.replace('-', '_') :
                DEFAULT_LOCALE;
    }

    /**
     * @param str
     * @param locale
     * @param localeAndEncodingMap
     * @return
     */
    String implStringCodec(String str, final String locale, final Map<String, String> localeAndEncodingMap, boolean encode) {
        if (StringUtils.isBlank(str)) {
            return str;
        }

        final String encoding = localeAndEncodingMap.get(implGetNormalizedLocale(locale));

        if ((null == encoding) || "utf-8".equalsIgnoreCase(encoding)) {
            return str;
        }

        try {
            if (encode) {
                // The Unicode codepoints of the given string need to be encoded to
                // values in the range 0x00-0xFF with the given language encoding;
                // the final string is nevertheless represented as Unicode for streaming
                final byte[] byteArray = str.getBytes(encoding);
                final StringBuilder strBuilder = new StringBuilder(byteArray.length);

                for (byte curByte : byteArray) {
                    strBuilder.append((char) (curByte & 0xFF));
                }

                return strBuilder.toString();
            } else {
                // The given string is represented as Unicode codepoints;
                // it is expected that the given codepoints are in the range 0x00-0xFF
                // The single bytes of the string are then reencoded to a Unicode string
                // with the given language encoding
                final int len =  str.length();

                if (len > 0) {
                    final byte[] codePoints = new byte[len];

                    for (int i = 0; i < len; ++i) {
                        final char curChar = str.charAt(i);
                        codePoints[i] = (curChar < 256) ? (byte) curChar : 32;
                    }

                    return new String(codePoints, encoding);
                }
            }
        } catch (Exception e) {
            LOG.warn("SpellCheck client received exception while encoding string {} (locale: {}, encoding  {}): {}",
                str, locale, encoding, Throwables.getRootCause(e));
        }

        return str;
    }

    /**
     * @param path HTTP request resource path, used with the SpellChecking
     * service Endpoint
     * @param jsonRequestBody if empty, execute GET request with given path,
     * otherwiese execute POST request with given jsonData
     * @return JSON response string for all request pathes
     */
    @Nullable JSONObject implExecuteHttpRequest(
        @NonNull final String path,
        @Nullable JSONObject optionalJsonRequestData
        ) throws Exception {

        return implExecuteHttpRequest(path, optionalJsonRequestData, SpellCheckClientMetrics.ConnectionType.SPELLCHECK);
    }

    /**
     * @param path HTTP request resource path, used with the SpellChecking
     * service Endpoint
     * @param jsonRequestBody if empty, execute GET request with given path,
     * otherwiese execute POST request with given jsonData
     * @param isValidationRequest if set to true this indicates a server status validation request
     * otherwiese execute POST request with given jsonData
     * @return JSON response string for all request pathes
     */
    @Nullable JSONObject implExecuteHttpRequest(
        @NonNull final String path,
        @Nullable JSONObject optionalJsonRequestData,
        @NonNull final SpellCheckClientMetrics.ConnectionType connectionType) throws Exception {

        final SSLContext sslContext = (null != m_sslSocketFactoryProvider) ?
            m_sslSocketFactoryProvider.getOriginatingDefaultContext() :
                null;

        if (null == sslContext) {
            throw new Exception("SpellCheck client could not get SSLContext to create HttpClient instance");
        }

        final CloseableHttpClient managedHttpClient = org.apache.http.impl.client.HttpClients.
            custom().setSSLContext(sslContext).build();

        if (null == managedHttpClient) {
            throw new Exception("SpellCheck client could not get managed HttpClient instance");
        }

        HttpRequestBase httpRequest = null;
        HttpResponse httpResponse = null;

        try {
            final HttpClientContext context = HttpClientContext.create();
            final URI targetURI = new URIBuilder(m_spellCheckURL + (path.startsWith("/") ? path : ('/' + path))).build();
            final RequestConfig requestConfig = RequestConfig.custom().
                setConnectionRequestTimeout(SpellCheckHttpConfigProvider.getConnectTimeoutMillis()).
                setConnectTimeout(SpellCheckHttpConfigProvider.getConnectTimeoutMillis()).
                setSocketTimeout(SpellCheckHttpConfigProvider.getSocketReadTimeoutMillis()).build();

            httpRequest = (null != optionalJsonRequestData) ? new HttpPost(targetURI) : new HttpGet(targetURI);
            httpRequest.setConfig(requestConfig);
            httpRequest.addHeader(HttpHeaders.ACCEPT, MIME_TYPE_JSON);

            // in case of valid optionalJsonRequestData use a POST request type
            if (null != optionalJsonRequestData) {
                httpRequest.addHeader(HttpHeaders.CONTENT_TYPE, MIME_TYPE_JSON);
                ((HttpPost) httpRequest).setEntity(
                    new StringEntity(optionalJsonRequestData.toString(), ContentType.APPLICATION_JSON));


            }

            httpResponse = managedHttpClient.execute(httpRequest, context);

            final HttpEntity responseEntity = httpResponse.getEntity();
            final StatusLine statusLine = httpResponse.getStatusLine();
            final int statusCode = statusLine.getStatusCode();

            if ((HttpStatus.SC_OK == statusCode) && (null != responseEntity)) {
                final Header contentTypeHeader = responseEntity.getContentType();
                final String responseContentType = (null != contentTypeHeader) ? contentTypeHeader.getValue() : null;

                m_clientMetrics.incrementConnectionStatusCount(connectionType, null);

                return (StringUtils.startsWithIgnoreCase(responseContentType, MIME_TYPE_JSON)) ?
                    new JSONObject(EntityUtils.toString(responseEntity, StandardCharsets.UTF_8)) :
                        JSONObject.EMPTY_OBJECT;
            } else if (HttpStatus.SC_REQUEST_TIMEOUT == statusCode) {
                LOG.error("SpellCheck client received 'Request timeout' server error response: [code: {}]", statusCode);
            } else if (SC_TOO_MANY_REQUESTS == statusCode) {
                LOG.error("SpellCheck client received 'Too many requests' server error response: [code: {}]", statusCode);
            } else {
                LOG.warn("SpellCheck client received invalid server response: [code: {}, reason: {}]", statusCode, statusLine.getReasonPhrase());
            }

            EntityUtils.consume(httpResponse.getEntity());

            m_clientMetrics.incrementConnectionStatusCount(connectionType, null);
        } catch (Exception e) {
            final boolean isValidation = SpellCheckClientMetrics.ConnectionType.VALIDATION == connectionType;
            final String connectionTypeStr = isValidation ? STR_VALIDATION : STR_SPELLCHECK;
            final boolean isWarnLog = (isValidation && m_observationPeriodValidationLoggingEnabled.compareAndSet(true, false)) ||
                (!isValidation && m_observationPeriodSpellCheckLoggingEnabled.compareAndSet(true, false));

            if (isWarnLog) {
                LOG.warn(STR_LOG_CONNECTION_ERROR, connectionTypeStr, e);
            } else {
                LOG.trace(STR_LOG_CONNECTION_ERROR, connectionTypeStr, e);
            }

            m_clientMetrics.incrementConnectionStatusCount(connectionType, e);
            m_versionValidator.setVersionInvalid(true);
        } finally {
            HttpClients.close(httpRequest, httpResponse);

            try {
                managedHttpClient.close();
            } catch (IOException e) {
                LOG.error("SpellCheck client received exception when closing HttpClient: {}", Throwables.getRootCause(e));
            }
        }

        return null;
    }

    /**
     * @param localesAndEncodings
     * @param jsonObj
     * @param locale
     * @param wordKey
     * @param arrayKey
     * @throws Exception
     */
    void implPostProcessResponse(
        @NonNull final Map<String, String> localesAndEncodings,
        @Nullable final JSONObject jsonObj,
        @Nullable final String locale,
        @Nullable final String wordKey,
        @Nullable final String arrayKey ) throws Exception {

        if (null != jsonObj) {
            final String localeToUse = StringUtils.isEmpty(locale) ? DEFAULT_LOCALE : locale;

            if (null != wordKey) {
                final String word = jsonObj.optString(wordKey);

                jsonObj.put(wordKey, implStringCodec(word, localeToUse, localesAndEncodings, false));
            }

            if (null != arrayKey) {
                final JSONArray jsonArray = jsonObj.optJSONArray(arrayKey);

                for (int pos = 0, len = ((null != jsonArray) ? jsonArray.length() : 0); pos < len; ++pos) {
                    jsonArray.put(pos, implStringCodec(jsonArray.optString(pos), localeToUse, localesAndEncodings, false));
                }
            }
        }
    }

    /**
     * @param jsonParams
     * @param key
     * @return
     */
    @Nullable private JSONObject implGetEmptyJSONArray(@NonNull final JSONObject jsonParams, @NonNull final String key) {
        try {
            // perform shortcut check for empty 'text' param to avoid unnecessary remote call
            if (StringUtils.isBlank(jsonParams.optString(key))) {
                return new JSONObject().put(KEY_SPELLRESULT, EMPTY_JSON_ARRAY);
            }
        } catch (Exception e) {
            LOG.warn("SpellCheck client received exception in shortcut check for empty request: {}", Throwables.getRootCause(e));
        }

        return null;
    }

    /**
     *
     */
    private synchronized void implLogConnectionStatus() {
        final StringBuilder logDataBuilder = new StringBuilder(512);
        final Map<Tags, AtomicLong> curValidationConnectionCounts = m_clientMetrics.
            getConnectionStatusCountMap(SpellCheckClientMetrics.ConnectionType.VALIDATION);
        final Map<Tags, AtomicLong> curSpellCheckConnectionCounts = m_clientMetrics.
            getConnectionStatusCountMap(SpellCheckClientMetrics.ConnectionType.SPELLCHECK);
        final boolean validationPeriodErrorOccured = implAddConnectionLogData(STR_VALIDATION,
            curValidationConnectionCounts, m_lastValidationConnectionCounts, logDataBuilder);
        final boolean spellCheckPeriodErrorOccured = implAddConnectionLogData(STR_SPELLCHECK,
            curValidationConnectionCounts, m_lastValidationConnectionCounts, logDataBuilder);

        if (validationPeriodErrorOccured || spellCheckPeriodErrorOccured) {
            LOG.warn("SpellCheck client lost remote connection to {} during last observation period of {}s: {}",
                (null != m_spellCheckURL) ? m_spellCheckURL : "n/a",
                LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR, logDataBuilder.toString());
        } else {
            LOG.info("SpellCheck client connection overview during last observation period of {}s: {}",
                LOG_CONNECTION_STATUS_PERIOD_SECONDS_STR, logDataBuilder.toString());
        }

        m_observationPeriodValidationLoggingEnabled.set(true);
        m_observationPeriodSpellCheckLoggingEnabled.set(true);
    }

    /**
     * @param curCounts
     * @param lastCounts
     * @param logDataBuilder
     * @return
     */
    private boolean implAddConnectionLogData(
        @NonNull final String groupName,
        @NonNull final Map<Tags, AtomicLong> curCounts,
        @NonNull final Map<Tags, Long> lastCounts,
        @NonNull final StringBuilder logDataBuilder) {

        boolean periodErrorOccured = false;

        for (final Tags curTags : curCounts.keySet()) {
            final long totalCount = curCounts.get(curTags).get();
            final Long lastValue = lastCounts.get(curTags);
            final long periodCount = totalCount - ((null != lastValue) ? lastValue.longValue() : 0);
            final String tagsStr = curTags.toString();
            final Matcher statusMatcher = CONNECTION_STATUS_PATTERN.matcher(tagsStr);
            final String statusString = statusMatcher.find() ? statusMatcher.group(1) : tagsStr;


            if (logDataBuilder.length() > 0) {
                logDataBuilder.append(", ");
            }

            logDataBuilder.
                append(statusString + "_" + groupName + "_total: ").append(totalCount).append(", ").
                append(statusString + "_" + groupName + "_period: ").append(periodCount);

            if (curTags != SpellCheckClientMetrics.TAGS_CONNECTION_OK) {
                if (!periodErrorOccured && (periodCount > 0)) {
                    periodErrorOccured = true;
                }
            }

            lastCounts.put(curTags, Long.valueOf(totalCount));
        }

        return periodErrorOccured;
    }

    // - Members ---------------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(SpellCheckClient.class);

    final private static Pattern CONNECTION_STATUS_PATTERN = Pattern.compile("^.*(status=[a-zA-Z0-9]+)");

    final private SpellCheckClientMetrics m_clientMetrics;

    final private VersionValidator m_versionValidator;

    final private AtomicBoolean m_subsequentRemoteCall = new AtomicBoolean(false);

    final private Map<Tags, Long> m_lastValidationConnectionCounts = new HashMap<>();

    final private Map<Tags, Long> m_lastSpellCheckConnectionCounts = new HashMap<>();

    final private ScheduledExecutorService m_logTimerExecutor = Executors.newScheduledThreadPool(1);

    final private AtomicBoolean m_observationPeriodValidationLoggingEnabled = new AtomicBoolean(true);

    final private AtomicBoolean m_observationPeriodSpellCheckLoggingEnabled = new AtomicBoolean(true);

    @Autowired
    private ConfigurationService m_configService;

    @Autowired
    private SSLSocketFactoryProvider m_sslSocketFactoryProvider;

    @Autowired
    private TimerService m_timerService;

    private String m_spellCheckURL = null;

    private AtomicReference<Map<String, String>> m_localesAndEncodings = new AtomicReference<>();
}
