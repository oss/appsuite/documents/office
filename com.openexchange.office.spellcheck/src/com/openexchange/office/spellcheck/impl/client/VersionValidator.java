/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.spellcheck.impl.client;

import java.security.InvalidParameterException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import com.openexchange.office.tools.annotation.NonNull;
import com.openexchange.office.tools.annotation.Nullable;

/**
 * {@link VersionValidator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
public class VersionValidator {

    final public  static int VERSION_INVALID = -1;

    /**
     * {@link GetVersionFunc}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.4
     */
    public interface GetVersionHandler {
        int getVersion();
    }

    /**
     * VersionChangedHandler
     */
    public interface VersionChangedHandler {
        void versionChanged(int oldVersion, int newVersion);
    }

    /**
     * IsReadyHandler
     */
    public interface IsReadyHandler {
        boolean execute();
    }

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private VersionValidator() {
        super();

        m_getVersionHandler = null;
        m_gotValidVersionHandler = null;
        m_lostValidVersionHandler = null;
        m_isReadyHandler = null;
    }

    /**
     * Initializes a new {@link VersionValidator}.
     */
    public VersionValidator(
        @NonNull final GetVersionHandler getVersionHandler,
        @Nullable final VersionChangedHandler gotValidVersionHandler,
        @Nullable final VersionChangedHandler lostValidVersionHandler,
        @Nullable final IsReadyHandler isReadyHandler) throws InvalidParameterException {

        super();

        if (null == getVersionHandler) {
            throw new InvalidParameterException("GetVersionHandler needs to be valid");
        }

        m_getVersionHandler = getVersionHandler;
        m_gotValidVersionHandler = gotValidVersionHandler;
        m_lostValidVersionHandler = lostValidVersionHandler;
        m_isReadyHandler = isReadyHandler;
    }

    /**
     * @param enabled
     * @return
     */
    public void setEnabled(final boolean enabled) {
        m_enabled.set(enabled);
    }

    /**
     * @param enabled
     * @return
     */
    public boolean isEnabled() {
        return m_enabled.get();
    }

    /**
     * @return
     */
    public int getVersion(boolean force) {
        if (isEnabled()) {
            int version = m_version.get();

            // DOCS-4541: Allow only one thread at a time to retrieve version from server in
            // order to prevent accumulation of requests until memory gets exhausted (=> OOM)
            if ((force || (VERSION_INVALID == version)) && m_lock.tryLock()) {
                try {
                    final int newVersion = Math.max(m_getVersionHandler.getVersion(), VERSION_INVALID);

                    if ((newVersion > VERSION_INVALID) && (version != newVersion) && (null != m_gotValidVersionHandler)) {
                        m_gotValidVersionHandler.versionChanged(version, newVersion);
                    }

                    m_version.set(version = newVersion);
                } finally {
                    m_lock.unlock();
                }
            }

            return version;
        }

        return VERSION_INVALID;
    }

    /**
     *
     */
    public void setVersionInvalid(boolean notifyLostValidVersion) {
        final int version = m_version.get();

        if (version > VERSION_INVALID) {
            m_version.set(VERSION_INVALID);

            if (notifyLostValidVersion && (null != m_lostValidVersionHandler)) {
                m_lostValidVersionHandler.versionChanged(version, VERSION_INVALID);
            }
        }
    }

    /**
     * @return The current status of the connection, without performing
     *  an expensive check of calling  the remote service if not necessary.
     */
    public boolean isValid() {
        return getVersion(false) > VERSION_INVALID;
    }

    /**
     * @return The current readiness status of the connection, performing
     *  a physical call if possible.
     *  This is done either via the {@link IsReadyHandler},
     *  if set, or via the {@link GetVersionHandler}, that needs to be set in every case
     *  as fallback.
     */
    public boolean isReady() {
        return (null != m_isReadyHandler) ?
            m_isReadyHandler.execute() :
                (getVersion(true) > VERSION_INVALID);
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_enabled = new AtomicBoolean(true);

    final private AtomicInteger m_version = new AtomicInteger(VERSION_INVALID);

    final private ReentrantLock m_lock = new ReentrantLock(true);

    final private GetVersionHandler m_getVersionHandler;

    final private VersionChangedHandler m_gotValidVersionHandler;

    final private VersionChangedHandler m_lostValidVersionHandler;

    final private IsReadyHandler m_isReadyHandler;

}
