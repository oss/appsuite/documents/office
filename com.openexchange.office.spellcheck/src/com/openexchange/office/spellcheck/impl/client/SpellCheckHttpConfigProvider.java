/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.spellcheck.impl.client;

import org.springframework.stereotype.Service;
import com.openexchange.config.DefaultInterests;
import com.openexchange.config.Interests;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.rest.client.httpclient.DefaultHttpClientConfigProvider;
import com.openexchange.rest.client.httpclient.HttpBasicConfig;
import com.openexchange.rest.client.httpclient.SpecificHttpClientConfigProvider;

/**
 * {@link SpellCheckHttpConfigProvider}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@Service
@RegisteredService(registeredClass=SpecificHttpClientConfigProvider.class)
public class SpellCheckHttpConfigProvider extends DefaultHttpClientConfigProvider {

    public static final String HTTP_CLIENT_ID = "spellcheck";
    public static final String HTTP_CLIENT_USER_AGENT = "OX App Suite Spellcheck HTTP client";

    final private static int CONNECT_TIMEOUT_MILLIS = 5000;
    final private static int READ_TIMEOUT_MILLIS = 30000;
    final private static int TOTAL_CONNECTIONS = 24;
    final private static int TOTAL_CONNECTIONS_PER_ROUTE = 12;

    /**
     * Initializes a new {@link SpellCheckHttpConfigProvider}.
     */
    public SpellCheckHttpConfigProvider() {
        super(HTTP_CLIENT_ID, HTTP_CLIENT_USER_AGENT);
    }

    @Override
    public Interests getAdditionalInterests() {
        return DefaultInterests.builder().propertiesOfInterest("com.openexchange.spellcheck.*").build();
    }

    @Override
    public HttpBasicConfig configureHttpBasicConfig(HttpBasicConfig config) {
        config.setConnectTimeout(CONNECT_TIMEOUT_MILLIS);
        config.setSocketReadTimeout(READ_TIMEOUT_MILLIS);
        config.setMaxTotalConnections(TOTAL_CONNECTIONS);
        config.setMaxConnectionsPerRoute(TOTAL_CONNECTIONS_PER_ROUTE);

        return config;
    }

    /**
     * @return
     */
    public static int getConnectTimeoutMillis() {
        return CONNECT_TIMEOUT_MILLIS;
    }

    /**
     * @return
     */
    public static int getSocketReadTimeoutMillis() {
        return READ_TIMEOUT_MILLIS;
    }
}