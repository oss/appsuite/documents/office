/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.office.spellcheck.impl.client;

import java.util.Dictionary;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.health.Constants;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.IOsgiBundleDictionaryProvider;

@Service
@RegisteredService(registeredClass=HealthCheck.class)
public class SpellCheckHealthCheck implements HealthCheck, IOsgiBundleDictionaryProvider {
    final private static Logger LOG = LoggerFactory.getLogger(SpellCheckHealthCheck.class);
    private static final String SPELLCHECK_CLIENT_HEALTH_CHECK = "SpellCheckHealthCheck";
    private static final String SPELLCHECK_CLIENT_REMOTE_URL = "remoteSpellCheckUrl";
    private static final String SPELLCHECK_CLIENT_REMOTE_URL_NOT_AVAILABLE = "n/a";
    private static final String SPELLCHECK_SERVICE_AVAILABLE = "serviceAvailable";
    private static final int SPELLCHECK_HEALTH_RESULT_TIME_TO_LIVE_MILLIS = 15000;

    @Autowired
    private SpellCheckClient client;

    @Override
    public Dictionary<String, Object> getDictionary() {
        return Constants.withTimeToLive(SPELLCHECK_HEALTH_RESULT_TIME_TO_LIVE_MILLIS);
    }

    @Override
    public HealthCheckResponse call() {
        boolean serviceAvailable = false;

        // check service availability
        if (client != null) {
            try {
                serviceAvailable = client.isServerReady();
            } catch (Exception e) {
                LOG.warn("Spellcheck health check recevied exception trying to determine spellcheck service status", e);
            }
        }

        final String remoteURLStr = client.getSpellCheckURL();
        final HealthCheckResponseBuilder healthResponseBuilder = HealthCheckResponse.named(SPELLCHECK_CLIENT_HEALTH_CHECK).
            withData(SPELLCHECK_CLIENT_REMOTE_URL, (null != remoteURLStr) ? remoteURLStr : SPELLCHECK_CLIENT_REMOTE_URL_NOT_AVAILABLE).
            withData(SPELLCHECK_SERVICE_AVAILABLE, serviceAvailable);

        // UP/DOWN status is set to UP in every case!
        healthResponseBuilder.up();

        return healthResponseBuilder.build();
    }
}
