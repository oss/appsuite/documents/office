/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.operations;

import org.docx4j.dml.BaseStyles.FontScheme;
import org.docx4j.docProps.core.CoreProperties;
import org.docx4j.docProps.core.dc.elements.SimpleLiteral;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import org.docx4j.dml.CTColorMapping;
import org.docx4j.dml.CTColorScheme;
import org.docx4j.dml.FontCollection;
import org.docx4j.dml.Theme;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.ThemePart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.UnmodifiableIterator;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.ThemeFonts;

abstract public class CreateOperationHelper {

    protected final OfficeOpenXMLOperationDocument operationDocument;
    protected JSONArray operationsArray;
    protected int jsonLogSize;

    protected CreateOperationHelper(OfficeOpenXMLOperationDocument _operationDocument, JSONArray _operationsArray)
    	throws FilterException {

    	operationDocument = _operationDocument;
        operationsArray = _operationsArray;
        jsonLogSize = 0;
    }

    abstract public OfficeOpenXMLOperationDocument getOperationDocument();

    private void putSchemeColor(final JSONObject jsonColorScheme, final String colorKey, final CTColorScheme colorScheme, final CTColorMapping colorMapping)
        throws JSONException {
        jsonColorScheme.put(colorKey, Commons.ctColorToString(colorScheme.getByColorKey(colorKey, colorMapping)));
    }

    private void putSchemeFont(final JSONObject jsonFontScheme, final String fontKey, final FontCollection schemeFont)
        throws JSONException {
        if (schemeFont != null) { jsonFontScheme.put(fontKey, schemeFont.getLatin().getTypeface()); }
    }

    public void createThemeOperations(String target, JSONArray start, CTColorMapping colorMapping)
        throws FilterException, JSONException {

        final ThemePart themePart = getOperationDocument().getThemePart(true);
    	final Theme theme = themePart.getJaxbElement();

        final CTColorScheme colorScheme = theme.getThemeElements().getClrScheme();
        final JSONObject jsonColorScheme = new JSONObject();
        putSchemeColor(jsonColorScheme, "dark1", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "dark2", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "light1", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "light2", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "text1", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "text2", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "background1", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "background2", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent1", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent2", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent3", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent4", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent5", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "accent6", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "hyperlink", colorScheme, colorMapping);
        putSchemeColor(jsonColorScheme, "followedHyperlink", colorScheme, colorMapping);

        final FontScheme fontScheme = theme.getThemeElements().getFontScheme();
        final JSONObject jsonFontScheme = new JSONObject(2);
        putSchemeFont(jsonFontScheme, ThemeFonts.MAJOR_ID, fontScheme.getMajorFont());
        putSchemeFont(jsonFontScheme, ThemeFonts.MINOR_ID, fontScheme.getMinorFont());

        addInsertThemeOperation(themePart.getPartName().getName(), jsonColorScheme, jsonFontScheme, target, start);
    }

    public JSONArray getOperationsArray() {
    	return operationsArray;
    }

    public void setOperationsArray(JSONArray _operationsArray) {
    	operationsArray = _operationsArray;
    }

    public void addInsertThemeOperation(String themeName, JSONObject clrScheme, JSONObject fontScheme, String target, JSONArray start)
        throws JSONException {

        final JSONObject insertThemeObject = new JSONObject(5);
        insertThemeObject.put(OCKey.NAME.value(), OCValue.INSERT_THEME.value());
        insertThemeObject.put(OCKey.THEME_NAME.value(), themeName);
        insertThemeObject.put(OCKey.COLOR_SCHEME.value(), clrScheme);
        insertThemeObject.put(OCKey.FONT_SCHEME.value(), fontScheme);
        if(target!=null) {
        	insertThemeObject.put(OCKey.TARGET.value(), target);
        }
        if(start!=null) {
            insertThemeObject.put(OCKey.START.value(), start);
        }
        operationsArray.put(insertThemeObject);
    }

    public JSONObject createInsertStyleSheetOperation(String type, boolean isDefaultType, String styleId, String styleName, JSONObject attrs, String parentId, Boolean hidden, Integer uipriority, Boolean isDefault, Boolean custom)
        throws JSONException {

        final JSONObject insertStylesheetObject = new JSONObject(10);
        insertStylesheetObject.put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
        if(!isDefaultType) {
            insertStylesheetObject.put(OCKey.TYPE.value(), type);
        }
        insertStylesheetObject.put(OCKey.STYLE_ID.value(), styleId);
        insertStylesheetObject.put(OCKey.STYLE_NAME.value(), styleName);
        if(attrs!=null)
            insertStylesheetObject.put(OCKey.ATTRS.value(), attrs);
        if(parentId!=null&&parentId.length()>0)
            insertStylesheetObject.put(OCKey.PARENT.value(), parentId);
        if(hidden!=null)
            insertStylesheetObject.put(OCKey.HIDDEN.value(), hidden);
        if(uipriority!=null)
            insertStylesheetObject.put(OCKey.UI_PRIORITY.value(), uipriority);
        if(isDefault!=null&&isDefault)
            insertStylesheetObject.put(OCKey.DEFAULT.value(), true);
        if(custom!=null) {
            insertStylesheetObject.put(OCKey.CUSTOM.value(), custom);
        }
        return insertStylesheetObject;
    }

    public JSONObject createInsertAutoStyleOperation(String styleId, JSONObject attrs, boolean isDefault)
        throws JSONException {

        final JSONObject insertStylesheetObject = new JSONObject(5);
        insertStylesheetObject.put(OCKey.NAME.value(), OCValue.INSERT_AUTO_STYLE.value());
        insertStylesheetObject.put(OCKey.STYLE_ID.value(), styleId);
        if(attrs!=null)
            insertStylesheetObject.put(OCKey.ATTRS.value(), attrs);
        if(isDefault)
            insertStylesheetObject.put(OCKey.DEFAULT.value(), true);
        return insertStylesheetObject;
    }

    public JSONObject addSetDocumentAttributesOperation()
        throws JSONException {

        final JSONObject documentAttributes = new JSONObject();
        final JSONObject setDocumentAttributesObject = new JSONObject(2);
        setDocumentAttributesObject.put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        setDocumentAttributesObject.put(OCKey.ATTRS.value(), documentAttributes);
        operationsArray.put(setDocumentAttributesObject);
        return documentAttributes;
    }

    public JSONObject createCoreProperties(JSONObject attrs) throws JSONException {
        final JSONObject documentProperties = Tools.writeFilterVersion(attrs);
        final DocPropsCorePart corePropertiesPart = operationDocument.getPackage().getDocPropsCorePart(false);
        if(corePropertiesPart!=null) {
            final CoreProperties coreProperties = corePropertiesPart.getJaxbElement();
            if(coreProperties!=null) {
                final SimpleLiteral modified = coreProperties.getModified();
                if(modified!=null && !modified.getContent().isEmpty()) {
                    documentProperties.put(OCKey.MODIFIED.value(), modified.getContent().get(0));
                }
                final SimpleLiteral created = coreProperties.getCreated();
                if(created!=null && !created.getContent().isEmpty()) {
                    documentProperties.put(OCKey.CREATED.value(), created.getContent().get(0));
                }
            }
        }
        return documentProperties;
    }

    // usedLanguages are only available after creating operations for the whole document (via _createOperations)
    public void createUsedLanguages(JSONObject documentAttributes) throws JSONException {
        applyLanguages(documentAttributes, OCKey.USED_LANGUAGES, getOperationDocument().getUsedLanguages());
        applyLanguages(documentAttributes, OCKey.USED_LANGUAGES_EA, getOperationDocument().getUsedLanguagesEA());
        applyLanguages(documentAttributes, OCKey.USED_LANGUAGES_BIDI, getOperationDocument().getUsedLanguagesBidi());
    }

    private void applyLanguages(JSONObject documentAttributes, OCKey attributeName, Set<String> usedLanguages) throws JSONException {
        if(!usedLanguages.isEmpty()) {
            JSONObject jsonDocumentSettings = documentAttributes.optJSONObject(OCKey.DOCUMENT.value());
            if(jsonDocumentSettings==null) {
                documentAttributes.put(OCKey.DOCUMENT.value(), jsonDocumentSettings = new JSONObject());
            }
            jsonDocumentSettings.put(attributeName.value(), new JSONArray(usedLanguages));
        }
    }

    static final private ImmutableSet<String> familiesWithDeepAttrsInheritance = ImmutableSet.<String> builder()
        .add(OCKey.CHARACTER.value())
        .add(OCKey.PARAGRAPH.value())
        .build();

    protected JSONObject createDifference(JSONObject currentAttrs, JSONObject newAttrs) throws JSONException {
        final JSONObject difference = new JSONObject();
        final UnmodifiableIterator<String> iter = familiesWithDeepAttrsInheritance.iterator();
        while(iter.hasNext()) {
            final String family = iter.next();
            final JSONObject c = currentAttrs.optJSONObject(family);
            if (c!=null) {
                final JSONObject result = createDifferenceWithDeepInheritance(c, newAttrs!= null ? newAttrs.optJSONObject(family) : null);
                if(result!=null) {
                    difference.put(family, result);
                }
            }
            else if (newAttrs!=null) {
                final JSONObject n = newAttrs.optJSONObject(family);
                if (n!=null) {
                    difference.put(family, n);
                    currentAttrs.put(family, cloneJSONObject(n));
                }
            }
        }
        String newStyleId = null;
        if(newAttrs!=null) {
            newStyleId = newAttrs.optString(OCKey.STYLE_ID.value(), null);
            final Iterator<Entry<String, Object>> newAttrsIter = newAttrs.entrySet().iterator();
            while(newAttrsIter.hasNext()) {
                final Entry<String, Object> newAttrsEntry = newAttrsIter.next();
                if(!familiesWithDeepAttrsInheritance.contains(newAttrsEntry.getKey())) {
                    difference.put(newAttrsEntry.getKey(), newAttrsEntry.getValue());
                }
            }
        }
        if(newStyleId==null) {
            difference.put(OCKey.STYLE_ID.value(), JSONObject.NULL);
        }
        return difference.isEmpty() ? null : difference;
    }

    private JSONObject createDifferenceWithDeepInheritance(JSONObject currentAttrs, JSONObject newAttrs) throws JSONException {

        final JSONObject difference = new JSONObject();
        final Iterator<Entry<String, Object>> currentAttrsIter = currentAttrs.entrySet().iterator();
        while(currentAttrsIter.hasNext()) {

            final Entry<String, Object> currentAttrsEntry = currentAttrsIter.next();
            final Object currentValue = currentAttrsEntry.getValue();
            final String key = currentAttrsEntry.getKey();
            final Object newValue = newAttrs !=null ? newAttrs.opt(key) : null;

            if(newValue!=null) {
                // key is available on both sides
                if(!currentValue.equals(newValue)) {
                    difference.put(key, newValue);
                    currentAttrs.put(key, newValue);
                }
            }
            else { // key is available only in current attributes
                difference.put(key, JSONObject.NULL);
                currentAttrsIter.remove();
            }
        }

        if(newAttrs!=null) {
            // now only applying everything that is not part in currentAttrs
            final Iterator<Entry<String, Object>> newAttrsIter = newAttrs.entrySet().iterator();
            while(newAttrsIter.hasNext()) {
                final Entry<String, Object> newAttrsEntry = newAttrsIter.next();
                final String key = newAttrsEntry.getKey();
                if(!currentAttrs.has(key)) {
                    Object value = newAttrsEntry.getValue();
                    difference.put(key,  value);
                    currentAttrs.put(key, value);
                }
            }
        }
        return difference.isEmpty() ? null : difference;
    }

    // -------------------------------------------------------------------------

    public static JSONArray cloneJSONArray(JSONArray source) throws JSONException {
        final JSONArray dest = new JSONArray(source.length());
        for(int i=0; i < source.length(); i++) {
            final Object s = source.get(i);
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(i, d);
        }
        return dest;
    }

    public static JSONObject cloneJSONObject(JSONObject source) throws JSONException {
        final JSONObject dest = new JSONObject(source.length());
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            final Object s = sourceEntry.getValue();
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(sourceEntry.getKey(), d);
        }
        return dest;
    }
}
