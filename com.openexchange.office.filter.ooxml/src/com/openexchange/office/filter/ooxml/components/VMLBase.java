/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.components;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import org.docx4j.vml.CTFill;
import org.docx4j.vml.CTGroup;
import org.docx4j.vml.CTImage;
import org.docx4j.vml.CTImageData;
import org.docx4j.vml.CTRect;
import org.docx4j.vml.CTShape;
import org.docx4j.vml.CTShapetype;
import org.docx4j.vml.CTStroke;
import org.docx4j.vml.CTTextPath;
import org.docx4j.vml.CTTextbox;
import org.docx4j.vml.VmlCore;
import org.docx4j.vml.VmlShapeCore;
import org.docx4j.vml.officedrawing.STHrAlign;
import org.docx4j.vml.officedrawing.STTrueFalse;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.wordprocessingDrawing.CTWrap;
import org.docx4j.vml.wordprocessingDrawing.STWrapType;
import org.docx4j.wml.CTTxbxContent;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.EnhancedGeometryType;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;

public abstract class VMLBase extends OfficeOpenXMLComponent implements IShape, IShapeTxBxContentAccessor {

    private final VmlCore vmlCoreObject;
    private final CTShapetype referenceShape;
    private final HashMap<String, String> vmlStyle;
    private final VMLGroup parentGroup;
    protected final String documentType;

    public VmlCore getVMLCoreObject() {
        return vmlCoreObject;
    }

    @Override
    public ShapeType getType() {
        if(vmlCoreObject instanceof CTImage) {
            return ShapeType.IMAGE;
        }
        else if(vmlCoreObject instanceof CTGroup) {
            return ShapeType.GROUP;
        }
        else if(vmlCoreObject instanceof CTRect) {
            final STTrueFalse stTrueFalse = ((CTRect)vmlCoreObject).getHr();
            if(stTrueFalse == STTrueFalse.T||stTrueFalse == STTrueFalse.TRUE) {
                return ShapeType.HORIZONTAL_LINE;
            }
            return ShapeType.SHAPE;
        }
        else if(vmlCoreObject instanceof VmlShapeCore) {
            return ((VmlShapeCore)vmlCoreObject).getOle()!=null?ShapeType.OLE:ShapeType.SHAPE;
        }
        return ShapeType.SHAPE;
    }

    public VMLBase(String documentType, OfficeOpenXMLOperationDocument operationDocument, IContentAccessor contentAccessor) {
        super(operationDocument, new DLNode<Object>(contentAccessor), 0);
        this.documentType = documentType;
        vmlCoreObject = null;
        vmlStyle = null;
        referenceShape = null;
        parentGroup = null;
    }

    public VMLBase(String documentType, ComponentContext<OfficeOpenXMLOperationDocument> componentContext, DLNode<Object> _node, int componentNumber) {
        super(componentContext, _node, componentNumber);
        this.documentType = documentType;
        vmlCoreObject = (VmlCore)_node.getData();
        vmlStyle = vmlCoreObject.getStyleMap();
        referenceShape = initializeReferenceShape(componentContext.getOperationDocument(), vmlCoreObject);
        parentGroup = (componentContext instanceof VMLGroup) ? (VMLGroup)componentContext : null;
    }

    private static CTShapetype initializeReferenceShape(OfficeOpenXMLOperationDocument operationDocument, VmlCore vmlCoreObject) {
        CTShapetype shapeType = null;
        if(vmlCoreObject instanceof CTShape) {
            String type = ((CTShape)vmlCoreObject).getType();
            if(type!=null&&type.length()>1) {
                type = type.substring(1);
                shapeType = operationDocument.getVMLShapeTypes().get(type);
            }
        }
        return shapeType;
    }

    @Override
    public void delete(int count) {
        final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = getContextChild(null);

        final int endComponent = (getComponentNumber()+count)-1;
        IComponent<OfficeOpenXMLOperationDocument> component = this;
        while(true) {
            if(component.getNextComponentNumber()>endComponent) {
                break;
            }
            component = component.getNextComponent();
        }
        component.splitEnd(endComponent, SplitMode.DELETE);
        final DLList<Object> content = (DLList<Object>)((IContentAccessor)contextChild.getParentContext().getNode().getData()).getContent();
        content.removeNodes(contextChild.getNode(), component.getContextChild(null).getNode());
    }

    @Override
    public IShape getNext() {
        return (VMLBase)getNextComponent();
    }

    @Override
    public Object getChild() {
        final CTTxbxContent textboxContent = getTextboxContent(false);
        if(textboxContent==null) {
            return null;
        }
        final OfficeOpenXMLComponent rootComponent = operationDocument.getRootComponent();
        rootComponent.setNode(new DLNode<Object>(textboxContent));
        return rootComponent.getNextChildComponent(null, null);
    }

    @Override
    public CTTxbxContent getTextboxContent(boolean forceCreate) {
        if(documentType.equals("text")) {
            final CTTextbox textbox = vmlCoreObject.getTextBox(forceCreate);
            if(textbox!=null) {
                return textbox.getTxbxContent(forceCreate);
            }
        }
        return null;
    }

    @Override
    public void setTextboxContent(CTTxbxContent textBox) {
        if(documentType.equals("text")) {
            vmlCoreObject.getTextBox(true).setTxbxContent(textBox);
        }
    }

    @Override
    public Object insertChild(int number, ComponentType type) throws Exception {
        if(documentType.equals("text")) {
            final CTTxbxContent textboxContent = getTextboxContent(true);
            final OfficeOpenXMLComponent rootComponent = operationDocument.getRootComponent();
            rootComponent.setNode(new DLNode<Object>(textboxContent));
            IComponent<OfficeOpenXMLOperationDocument> c = rootComponent.getNextChildComponent(null, null);
            if(c!=null) {
                c = c.getComponent(number);
            }
            return rootComponent.insertChildComponent(rootComponent, new DLNode<Object>(textboxContent), number, c, type, null);
        }
        return null;
    }

    private static String createVMLColorFromJSON(JSONObject jsonColor) {
        String color = null;
        if(jsonColor!=null) {
            final String fallBack = jsonColor.optString(OCKey.FALLBACK_VALUE.value(), null);
            if(fallBack!=null) {
                color = "#" + fallBack.toUpperCase();
            }
            else if(jsonColor.optString(OCKey.TYPE.value(), "").equals("rgb")) {
                final String rgb = jsonColor.optString(OCKey.VALUE.value());
                if(rgb!=null) {
                    color = "#" + rgb.toUpperCase();
                }
            }
        }
        return color;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        final ShapeType shapeType = getType();

        final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingAttrs!=null) {
            final int optWidth = drawingAttrs.optInt(OCKey.WIDTH.value(), Integer.MIN_VALUE);
            final int optHeight = drawingAttrs.optInt(OCKey.HEIGHT.value(), Integer.MIN_VALUE);
            final int width = optWidth != Integer.MIN_VALUE ? optWidth : 0;
            final int height = optHeight != Integer.MIN_VALUE ? optHeight : 0;
            final int optCoordWidth = drawingAttrs.optInt(OCKey.COORD_WIDTH.value(), Integer.MIN_VALUE);
            final int optCoordHeight = drawingAttrs.optInt(OCKey.COORD_HEIGHT.value(), Integer.MIN_VALUE);

            if(optCoordWidth!=Integer.MIN_VALUE||optCoordHeight!=Integer.MIN_VALUE) {
                int coordWidth = optCoordWidth != Integer.MIN_VALUE ? optCoordWidth : 0;
                int coordHeight = optCoordHeight != Integer.MIN_VALUE ? optCoordHeight : 0;
                vmlCoreObject.setCoordsize(Integer.toString(coordWidth) + "," + Integer.toString(coordHeight));
            }
            else if(shapeType==ShapeType.GROUP) {
                vmlCoreObject.setCoordsize(Integer.toString(width) + "," + Integer.toString(height));
            }

            if(parentGroup==null) {
                if(optWidth!=Integer.MIN_VALUE||optHeight!=Integer.MIN_VALUE) {
                    setLength("width", width);
                    setLength("height", height);
                }
                if(documentType.equals("text")) {
                    int marginLeft = drawingAttrs.optInt(OCKey.MARGIN_LEFT.value(), 0);
                    int marginTop = drawingAttrs.optInt(OCKey.MARGIN_TOP.value(), 0);
                    int marginRight = drawingAttrs.optInt(OCKey.MARGIN_RIGHT.value(), 0);
                    int marginBottom = drawingAttrs.optInt(OCKey.MARGIN_BOTTOM.value(), 0);

                    boolean isInline = true;
                    final String pos = vmlStyle.get("position");
                    if(pos!=null)
                        isInline = !pos.equals("absolute");
                    Object inline = drawingAttrs.opt(OCKey.INLINE.value());
                    if(inline!=null) {
                        if(inline instanceof Boolean) {
                            isInline = ((Boolean)inline).booleanValue();
                        }
                        else {
                            isInline = true;
                        }
                    }
                    if (isInline) {
                        vmlStyle.put("position", "static");
                        vmlStyle.remove("mso-position-horizontal");
                        vmlStyle.remove("mso-position-horizontal-relative");
                        vmlStyle.remove("mso-position-vertical");
                        vmlStyle.remove("mso-position-vertical-relative");
                    } else {
                        vmlStyle.put("position",  "absolute");
                        if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_HOR_BASE.value())) {
                            setHorzBase(drawingAttrs.getString(OCKey.ANCHOR_HOR_BASE.value()));
                        }
                        String anchorHorAlign = null;
                        if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_HOR_ALIGN.value())) {
                            anchorHorAlign = drawingAttrs.getString(OCKey.ANCHOR_HOR_ALIGN.value());
                            setHorzAlign(anchorHorAlign);
                        }

                        if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_VERT_BASE.value())) {
                            setVertBase(drawingAttrs.getString(OCKey.ANCHOR_VERT_BASE.value()));
                        }
                        String anchorVertAlign = null;
                        if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_VERT_ALIGN.value())) {
                            anchorVertAlign = drawingAttrs.getString(OCKey.ANCHOR_VERT_ALIGN.value());
                            setVertAlign(anchorVertAlign);
                        }

                        if(anchorHorAlign!=null&&anchorHorAlign.equals("offset")) {
                            if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_HOR_OFFSET.value()))
                                marginLeft += drawingAttrs.getInt(OCKey.ANCHOR_HOR_OFFSET.value());
                        }
                        if(anchorVertAlign!=null&&anchorVertAlign.equals("offset")) {
                            if(drawingAttrs.hasAndNotNull(OCKey.ANCHOR_VERT_OFFSET.value()))
                                marginTop += drawingAttrs.getInt(OCKey.ANCHOR_VERT_OFFSET.value());
                        }
                        if(drawingAttrs.hasAndNotNull(OCKey.TEXT_WRAP_MODE.value())) {
                            CTWrap ctWrap = vmlCoreObject.getWrap(true);
                            String wrapMode = drawingAttrs.getString(OCKey.TEXT_WRAP_MODE.value());
                            if(wrapMode.equals("square"))
                                ctWrap.setType(STWrapType.SQUARE);
                            else if(wrapMode.equals("tight"))
                                ctWrap.setType(STWrapType.TIGHT);
                            else if(wrapMode.equals("topAndBottom"))
                                ctWrap.setType(STWrapType.TOP_AND_BOTTOM);
                            else if(wrapMode.equals("through"))
                                ctWrap.setType(STWrapType.THROUGH);
                            else
                                ctWrap.setType(STWrapType.NONE);
                        }
                    }

                    if(marginLeft!=0)
                        setLength("margin-left", marginLeft);
                    else
                        vmlStyle.remove("margin-left");
                    if(marginTop!=0)
                        setLength("margin-top", marginTop);
                    else
                        vmlStyle.remove("margin-top");
                    if(marginRight!=0)
                        setLength("margin-right", marginRight);
                    else
                        vmlStyle.remove("margin-right");
                    if(marginBottom!=0)
                        setLength("margin-bottom", marginBottom);
                    else
                        vmlStyle.remove("margin-bottom");
                }
                else {
                    final Object left = drawingAttrs.opt(OCKey.LEFT.value());
                    if(left instanceof Number) {
                        setLength("margin-left", ((Number)left).intValue());
                    }
                    final Object top = drawingAttrs.opt(OCKey.TOP.value());
                    if(top instanceof Number) {
                        setLength("margin-top", ((Number)top).intValue());
                    }
                    if(documentType.equals("spreadsheet")) {
                        final String anchor = drawingAttrs.optString(OCKey.ANCHOR.value(), null);
                        if(anchor!=null) {
                            final CTClientData clientData = vmlCoreObject.getClientData(true);
                            final char anchorType = anchor.charAt(0);
                            switch(anchorType) {
                                case 'a' :
                                case 'A' : {
                                    clientData.setMoveWithCells("");
                                    clientData.setSizeWithCells("");
                                    break;
                                }
                                case '1' :
                                case 'O' : {
                                    clientData.setMoveWithCells(null);
                                    clientData.setSizeWithCells("");
                                    break;
                                }
                                case '2' : {
                                    clientData.setMoveWithCells(null);
                                    clientData.setSizeWithCells(null);
                                    break;
                                }
                            }
                            final String[] anchorAttrs = anchor.substring(2).split(" ", -1);
                            final int pC = anchorAttrs.length;
                            if(pC>0) {
                                final CellRef startRef = CellRef.createCellRef(anchorAttrs[0]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[0] = startRef.getColumn();
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[2] = startRef.getRow();
                            }
                            if(pC>1) {
                                final long startColumnOffset = Integer.parseInt(anchorAttrs[1]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[1] = (int)((((Long)startColumnOffset).doubleValue()*0.037795)+0.5);
                            }
                            if(pC>2) {
                                final long startRowOffset = Integer.parseInt(anchorAttrs[2]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[3] = (int)((((Long)startRowOffset).doubleValue()*0.037795)+0.5);
                            }
                            if(pC>3) {
                                final CellRef endRef = CellRef.createCellRef(anchorAttrs[3]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[4] = endRef.getColumn();
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[6] = endRef.getRow();
                            }
                            if(pC>4) {
                                final long endColumnOffset = Integer.parseInt(anchorAttrs[4]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[5] = (int)((((Long)endColumnOffset).doubleValue()*0.037795)+0.5);
                            }
                            if(pC>5) {
                                final long endRowOffset = Integer.parseInt(anchorAttrs[5]);
                                vmlCoreObject.getClientData(true).getAnchorArray(true)[7] = (int)((((Long)endRowOffset).doubleValue()*0.037795)+0.5);
                            }
                        }
                    }
                }
            }
            else {
                // if parentGroup != null
                if(optWidth!=Integer.MIN_VALUE||optHeight!=Integer.MIN_VALUE) {
                    vmlStyle.put("width", Integer.toString(width));
                    vmlStyle.put("height", Integer.toString(height));
                }
                if((drawingAttrs.has(OCKey.LEFT.value())||drawingAttrs.has(OCKey.TOP.value()))) {
                    vmlStyle.put("left", Integer.toString(drawingAttrs.optInt(OCKey.LEFT.value(), 0)));
                    vmlStyle.put("top", Integer.toString(drawingAttrs.optInt(OCKey.TOP.value(), 0)));
                }
            }
            Iterator<String> keys = drawingAttrs.keys();
            while(keys.hasNext()) {
                String attr = keys.next();
                Object value = drawingAttrs.get(attr);
                if(shapeType == ShapeType.HORIZONTAL_LINE && (attr.equals(OCKey.ANCHOR_HOR_ALIGN.value()) || attr.equals(OCKey.WIDTH.value()))) {
                    if(vmlCoreObject instanceof CTRect) {
                        CTRect rect = (CTRect)vmlCoreObject;
                        try{
                            if(attr.equals(OCKey.ANCHOR_HOR_ALIGN.value()) ){
                                STHrAlign hrAlign = STHrAlign.fromValue((String)value);
                                rect.setHralign(hrAlign);
                            } else {
                                rect.setHrpct(Float.valueOf(0));
                            }
                        } catch( IllegalArgumentException i ){
                            //
                        }
                    }
                }
            }
        }

        // fill attributes
        final JSONObject fillAttrs = attrs.optJSONObject(OCKey.FILL.value());
        if(fillAttrs!=null) {
            final CTFill fill = vmlCoreObject.getFill(false);
            final String type = fillAttrs.optString(OCKey.TYPE.value(), null);
            if(type!=null) {
                boolean filled = false;
                if(type.equals("solid")) {
                    filled = true;
                }
                final org.docx4j.vml.STTrueFalse trueFalse = filled ? org.docx4j.vml.STTrueFalse.T : org.docx4j.vml.STTrueFalse.F;
                if(fill!=null) {
                    fill.setOn(trueFalse);
                }
                else {
                    vmlCoreObject.setFilled(trueFalse);
                }
            }
            final String color = createVMLColorFromJSON(fillAttrs.optJSONObject(OCKey.COLOR.value()));
            if(color!=null) {
                if(fill!=null) {
                    fill.setColor(color);
                }
                else {
                    vmlCoreObject.setFillcolor(color);
                }
            }
        }

        // line attributes
        final JSONObject lineAttrs = attrs.optJSONObject(OCKey.LINE.value());
        if(lineAttrs!=null) {
            CTStroke stroke = vmlCoreObject.getStroke(false);
            final String type = lineAttrs.optString(OCKey.TYPE.value(), null);
            if(type!=null) {
                boolean stroked = false;
                if(type.equals("solid")) {
                    stroked = true;
                }
                final org.docx4j.vml.STTrueFalse trueFalse = stroked ? org.docx4j.vml.STTrueFalse.T : org.docx4j.vml.STTrueFalse.F;
                if(stroke!=null) {
                    stroke.setOn(trueFalse);
                }
                else if(vmlCoreObject instanceof VmlShapeCore) {
                    ((VmlShapeCore)vmlCoreObject).setStroked(trueFalse);
                }
            }
            final String color = createVMLColorFromJSON(lineAttrs.optJSONObject(OCKey.COLOR.value()));
            if(color!=null) {
                if(stroke!=null) {
                    stroke.setColor(color);
                }
                else if(vmlCoreObject instanceof VmlShapeCore) {
                    ((VmlShapeCore)vmlCoreObject).setStrokecolor(color);
                }
            }
            final Object weight = lineAttrs.opt(OCKey.WIDTH.value());
            if(weight!=null&&weight instanceof Number) {
                final long w = ((Number)weight).longValue();
                String newWidth = getVMLLength(w);
                if(stroke!=null) {
                    stroke.setWeight(newWidth);
                }
                else if(vmlCoreObject instanceof VmlShapeCore) {
                    ((VmlShapeCore)vmlCoreObject).setStrokeweight(newWidth);
                }
            }
            // dash style
            final Object style = lineAttrs.opt(OCKey.STYLE.value());
            if(style!=null) {
                if(stroke==null) {
                    stroke = vmlCoreObject.getStroke(true);
                    if(vmlCoreObject instanceof VmlShapeCore) {
                        stroke.setOn(((VmlShapeCore)vmlCoreObject).getStroked());
                    }
                }
                if(style instanceof String) {
                    if(((String)style).equals("solid")) {
                        stroke.setDashstyle("Solid");
                    }
                    else if(((String)style).equals("dotted")) {
                        stroke.setDashstyle("Dot");
                    }
                    else if(((String)style).equals("dashed")) {
                        stroke.setDashstyle("Dash");
                    }
                    else if(((String)style).equals("dashDot")) {
                        stroke.setDashstyle("LongDashDot");
                    }
                    else if(((String)style).equals("dashDotDot")) {
                        stroke.setDashstyle("LongDashDotDot");
                    }
                }
                else {
                    stroke.setDashstyle(null);
                }
            }
        }

        // line attributes
        final JSONObject shapeAttrs = attrs.optJSONObject(OCKey.SHAPE.value());
        if(shapeAttrs!=null) {
            final Object paddingLeft = shapeAttrs.opt(OCKey.PADDING_LEFT.value());
            final Object paddingTop = shapeAttrs.opt(OCKey.PADDING_TOP.value());
            final Object paddingRight = shapeAttrs.opt(OCKey.PADDING_RIGHT.value());
            final Object paddingBottom = shapeAttrs.opt(OCKey.PADDING_BOTTOM.value());
            CTTextbox textbox = null;
            if(paddingLeft!=null||paddingTop!=null||paddingRight!=null||paddingBottom!=null) {
                textbox = vmlCoreObject.getTextBox(true);
                final String[] paddingArray = new String[4];
                final String oldInset = textbox.getInset();
                if(oldInset!=null) {
                    final String[] pArray = oldInset.split(",");
                    for (int i=0; i<pArray.length&&i<4; i++) {
                        final String pEntry = pArray[i];
                        if(pEntry!=null&&pArray.length!=0) {
                            paddingArray[i] = pArray[i];
                        }
                    }
                }
                if(paddingLeft!=null) {
                    if(paddingLeft instanceof Number) {
                        final int pLeft = ((Number)paddingLeft).intValue();
                        paddingArray[0] = pLeft==250?null:getVMLLength(pLeft);
                    }
                    else {
                        paddingArray[0] = null;
                    }
                }
                if(paddingTop!=null) {
                    if(paddingTop instanceof Number) {
                        final int pTop = ((Number)paddingTop).intValue();
                        paddingArray[1] = pTop==125?null:getVMLLength(pTop);
                    }
                    else {
                        paddingArray[1] = null;
                    }
                }
                if(paddingRight!=null) {
                    if(paddingRight instanceof Number) {
                        final int pRight = ((Number)paddingRight).intValue();
                        paddingArray[2] = pRight==250?null:getVMLLength(pRight);
                    }
                    else {
                        paddingArray[2] = null;
                    }
                }
                if(paddingBottom!=null) {
                    if(paddingBottom instanceof Number) {
                        final int pBottom = ((Number)paddingBottom).intValue();
                        paddingArray[3] = pBottom==125?null:getVMLLength(pBottom);
                    }
                    else {
                        paddingArray[3] = null;
                    }
                }
                if(paddingArray[0]==null&&paddingArray[1]==null&&paddingArray[2]==null&&paddingArray[3]==null) {
                    textbox.setInset(null);
                }
                else {
                    final StringBuilder stringBuilder = new StringBuilder();
                    for(int i=3;i>=0;i--) {
                        if(paddingArray[i]!=null) {
                            if(stringBuilder.length()>0) {
                                stringBuilder.insert(0, paddingArray[i] + ",");
                            }
                            else {
                                stringBuilder.insert(0, paddingArray[i]);
                            }
                        }
                    }
                }
            }
            HashMap<String, String> textboxStyle = null;
            final Object autoResizeHeight = shapeAttrs.opt(OCKey.AUTO_RESIZE_HEIGHT.value());
            if(autoResizeHeight!=null) {
                if(textbox==null) {
                    textbox = vmlCoreObject.getTextBox(true);
                }
                textboxStyle = textbox.getStyleMap();
                if((autoResizeHeight instanceof Boolean)&&((Boolean)autoResizeHeight).booleanValue()) {
                    textboxStyle.put("mso-fit-shape-to-text", "t");
                }
                else {
                    textboxStyle.remove("mso-fit-shape-to-text");
                }
            }
        }
    }

    private static JSONObject createJSONColorFromVML(String vmlColor, String rgbDefault) throws Exception {

        final JSONObject color = new JSONObject(2);
        if(vmlColor!=null) {
            if(vmlColor.startsWith("#")) {
                if(vmlColor.length()==4||(vmlColor.length()>4&&vmlColor.charAt(4)==' ')) {
                    color.put(OCKey.TYPE.value(), "rgb");
                    final StringBuffer rgb = new StringBuffer(6);
                    for(int i=1; i<4; i++) {
                        final char c = vmlColor.charAt(i);
                        rgb.append(c);
                        rgb.append(c);
                    }
                    color.put(OCKey.VALUE.value(), rgb.toString());
                }
                else if(vmlColor.length()>=7) {
                    color.put(OCKey.TYPE.value(), "rgb");
                    color.put(OCKey.VALUE.value(), vmlColor.substring(1, 7).toUpperCase());
                }
            }
            else {
                boolean isPresetColor = false;
                final int i = vmlColor.indexOf('[');
                if(i>0) {
                    vmlColor = vmlColor.substring(0, i-1);
                }
                if(vmlColor.equals("none")) {
                    if(i>0) {
                        color.put(OCKey.TYPE.value(), "auto");
                    }
                    else {
                        color.put(OCKey.TYPE.value(), "rgb");
                        color.put(OCKey.VALUE.value(), "000000");
                        final JSONArray transformations = new JSONArray(1);
                        final JSONObject colorTransformation = new JSONObject(2);
                        colorTransformation.put(OCKey.TYPE.value(), "alpha");
                        colorTransformation.put(OCKey.VALUE.value(), (Integer)0);
                        transformations.put(colorTransformation);
                        color.put("transformations", transformations);
                    }
                }
                else {
                    switch(vmlColor.toLowerCase()) {
                        case "black":
                        case "silver":
                        case "gray":
                        case "white":
                        case "maroon":
                        case "red":
                        case "purple":
                        case "fuchsia":
                        case "green":
                        case "lime":
                        case "olive":
                        case "yellow":
                        case "navy":
                        case "blue":
                        case "teal":
                        case "aqua":
                            isPresetColor = true;
                        break;
                    }
                    if(isPresetColor) {
                        color.put(OCKey.TYPE.value(), "preset");
                    }
                    else {
                        switch(vmlColor.toLowerCase()) {
                            // translating old vml system colors ..
                            case "threeddarkshadow": vmlColor = "3dDkShadow"; break;
                            case "activecaption": vmlColor = "activeCaption"; break;
                            case "appworkspace": vmlColor = "appWorkspace"; break;
                            case "background": vmlColor = "background"; break;
                            case "buttonface": vmlColor = "btnFace"; break;
                            case "buttonhighlight": vmlColor = "btnHighlight"; break;
                            case "buttonshadow": vmlColor = "btnShadow"; break;
                            case "buttontext": vmlColor = "btnText"; break;
                            case "captiontext": vmlColor = "captionText"; break;
                            case "graytext": vmlColor = "grayText"; break;
                            case "highlight": vmlColor = "highlight"; break;
                            case "highlighttext": vmlColor = "highlightText"; break;
                            case "inactiveborder": vmlColor = "inactiveBorder"; break;
                            case "inactivecaption": vmlColor = "inactiveCaption"; break;
                            case "inactivecaptiontext": vmlColor = "inactiveCaptionText"; break;
                            case "infobackground": vmlColor = "infoBk"; break;
                            case "infotext": vmlColor = "infoText"; break;
                            case "menu": vmlColor = "menu"; break;
                            case "menutext": vmlColor = "menuText"; break;
                            case "scrollbar": vmlColor = "scrollBar"; break;
                            case "window": vmlColor = "window"; break;
                            case "windowframe": vmlColor = "windowFrame"; break;
                            case "windowtext": vmlColor = "windowText"; break;
                        }
                        color.put(OCKey.TYPE.value(), "system");
                    }
                    color.put(OCKey.VALUE.value(), vmlColor);
                }
            }
        }
        if(color.isEmpty()) {
            color.put(OCKey.TYPE.value(), "rgb");
            color.put(OCKey.VALUE.value(), rgbDefault);
        }
        return color;
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {

        final ShapeType shapeType = getType();

        final JSONObject initialDrawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        final JSONObject drawingAttrs = initialDrawingAttrs != null ? initialDrawingAttrs : new JSONObject();
        final String vmlId = vmlCoreObject.getVmlId();
        if(vmlId!=null&&!vmlId.isEmpty()) {
            drawingAttrs.put(OCKey.ID.value(), vmlId);
            if(vmlId.toLowerCase().contains("watermark")) {
                drawingAttrs.put(OCKey.NO_SELECT.value(), true);
            }
        }
        if("hidden".equals(vmlStyle.get("visibility"))) {
            drawingAttrs.put(OCKey.HIDDEN.value(), Boolean.TRUE);
        }
        if (initialDrawingAttrs == null && !drawingAttrs.isEmpty()) {
            attrs.put(OCKey.DRAWING.value(), drawingAttrs);
        }
        final String coordSize = vmlCoreObject.getCoordsize();
        if(coordSize!=null) {
            String[] result = coordSize.split(",");
            if(result.length==2&&!result[0].isEmpty()&&!result[1].isEmpty()) {
                drawingAttrs.put(OCKey.COORD_WIDTH.value(), (int)getLengthAndUnitType(result[0], UnitType.COORDSIZE));
                drawingAttrs.put(OCKey.COORD_HEIGHT.value(), (int)getLengthAndUnitType(result[1], UnitType.COORDSIZE));
            }
        }
        if(parentGroup==null) {
            String width = vmlStyle.get("width");
            if(getDimensionType(width)==DimensionType.LENGTH) {
                drawingAttrs.put(OCKey.WIDTH.value(), (int)getLengthAndUnitType(width, UnitType.PX));
            }
            String height = vmlStyle.get("height");
            if(getDimensionType(height)==DimensionType.LENGTH) {
                drawingAttrs.put(OCKey.HEIGHT.value(), (int)getLengthAndUnitType(height, UnitType.PX));
            }

            if(documentType.equals("text")) {
                boolean inline = true;
                String position = vmlStyle.get("position");
                if(position!=null) {
                    if(position.equals("absolute"))
                        inline = false;
                }
                drawingAttrs.put(OCKey.INLINE.value(), inline);
                if(!inline) {
                    drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), getHorzBase() );
                    String anchorHAlign = getHorzAlign();
                    drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), anchorHAlign);
                    if(anchorHAlign.equals("offset")) {
                        int horzOffset = 0;
                        String marginLeft = vmlStyle.get("margin-left");
                        if(getDimensionType(marginLeft)==DimensionType.LENGTH) {
                            horzOffset = (int)getLengthAndUnitType(marginLeft, UnitType.PX);
                        }
                        drawingAttrs.put(OCKey.ANCHOR_HOR_OFFSET.value(), (Integer)horzOffset);
                    }
                    drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), getVertBase() );
                    String anchorVAlign = getVertAlign();
                    drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), anchorVAlign);
                    if(anchorVAlign.equals("offset")) {
                        int vertOffset = 0;
                        String marginTop = vmlStyle.get("margin-top");
                        if(getDimensionType(marginTop)==DimensionType.LENGTH) {
                            vertOffset = (int)getLengthAndUnitType(marginTop, UnitType.PX);
                        }
                        drawingAttrs.put(OCKey.ANCHOR_VERT_OFFSET.value(), (Integer)vertOffset);
                    }
                    CTWrap ctWrap = vmlCoreObject.getWrap(false);
                    if(ctWrap!=null) {
                        String wrapMode = "none";
                        if(ctWrap.getType()==STWrapType.SQUARE)
                            wrapMode = "square";
                        else if(ctWrap.getType()==STWrapType.THROUGH)
                            wrapMode = "through";
                        else if(ctWrap.getType()==STWrapType.TIGHT)
                            wrapMode = "tight";
                        else if(ctWrap.getType()==STWrapType.TOP_AND_BOTTOM)
                            wrapMode = "topAndBottom";
                        drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), wrapMode);
                    }
                }
                if(shapeType == ShapeType.HORIZONTAL_LINE) {
                    //convert o:hr-properties to shape properties
                    //o:hralign, o:hrpct, o:hrnoshade, fillcolor, stroked
                    if(!drawingAttrs.has(OCKey.WIDTH.value()))
                        drawingAttrs.put(OCKey.WIDTH.value(), 0);

                    drawingAttrs.put( OCKey.TEXT_WRAP_MODE.value(), "square" );
                    drawingAttrs.put( OCKey.ANCHOR_HOR_BASE.value(), "column" );
                    drawingAttrs.put( OCKey.ANCHOR_HOR_OFFSET.value(), 0 );
                    drawingAttrs.put( OCKey.INLINE.value(), false );

                    JSONObject topBorder = new JSONObject();
                    topBorder.put(OCKey.WIDTH.value(), 10);
                    topBorder.put(OCKey.STYLE.value(), "solid");

                    if(vmlCoreObject instanceof CTRect) {
                        CTRect rect = (CTRect)vmlCoreObject;
                        STHrAlign hrAlign = rect.getHralign();
                        if(hrAlign != null){
                            drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), hrAlign.value());
                            if( hrAlign.value().equals("right") )
                                drawingAttrs.put( OCKey.TEXT_WRAP_SIDE.value(), "left" );
                        }
                        final String fillColor = rect.getFillcolor();
                        if(fillColor != null){
                            Commons.jsonPut(topBorder, OCKey.COLOR.value(), createJSONColorFromVML(fillColor, "000000"));
                            drawingAttrs.put(OCKey.BORDER_TOP.value(), topBorder);
                        }
                    }
                    drawingAttrs.put(OCKey.BORDER_TOP.value(), topBorder);
                }
            }
            else if(documentType.equals("spreadsheet")) {
                final CTClientData clientData = vmlCoreObject.getClientData(false);
                if(clientData!=null) {
                	final int[] anchorArray = clientData.getAnchorArray(false);
                    if (anchorArray!=null && anchorArray.length==8) {
                        final StringBuilder builder = new StringBuilder();
                        if (clientData.getMoveWithCells()!=null) {
                            builder.append('A');
                        } else if (clientData.getSizeWithCells()!=null) {
                            builder.append('O');
                        } else {
                            builder.append('2');
                        }
                        final CellRef startRef = new CellRef(anchorArray[0], anchorArray[2]);
                        final CellRef endRef = new CellRef(anchorArray[4], anchorArray[6]);
                        builder.append(' ')
                               .append(CellRef.getCellRef(startRef))
                               .append(' ')
                               .append(Integer.valueOf((int)getLength(anchorArray[1], UnitType.PX)).toString())
                               .append(' ')
                               .append(Integer.valueOf((int)getLength(anchorArray[3], UnitType.PX)).toString())
                               .append(' ')
                               .append(CellRef.getCellRef(endRef))
                               .append(' ')
                               .append(Integer.valueOf((int)getLength(anchorArray[5], UnitType.PX)).toString())
                               .append(' ')
                               .append(Integer.valueOf((int)getLength(anchorArray[7], UnitType.PX)).toString());
                        drawingAttrs.put(OCKey.ANCHOR.value(), builder.toString());
                    }
                }
                else {
                    drawingAttrs.put(OCKey.ANCHOR.value(), "a");

                    final String marginLeft = vmlStyle.get("margin-left");
                    final String marginTop = vmlStyle.get("margin-top");

                    int horzOffset = 0;
                    if(getDimensionType(marginLeft)==DimensionType.LENGTH) {
                        horzOffset = (int)getLengthAndUnitType(marginLeft, UnitType.PX);
                    }
                    drawingAttrs.put(OCKey.LEFT.value(), (Integer)horzOffset);

                    int vertOffset = 0;
                    if(getDimensionType(marginTop)==DimensionType.LENGTH) {
                        vertOffset = (int)getLengthAndUnitType(marginTop, UnitType.PX);
                    }
                    drawingAttrs.put(OCKey.TOP.value(), (Integer)vertOffset);
                }
           }
        }
        else {
            // children are absolute positioned relative to their parent
            final String width = vmlStyle.get("width");
            if(width!=null) {
                drawingAttrs.put(OCKey.WIDTH.value(), (int)getLengthAndUnitType(width, UnitType.COORDSIZE));
            }
            final String height = vmlStyle.get("height");
            if(height!=null) {
                drawingAttrs.put(OCKey.HEIGHT.value(), (int)getLengthAndUnitType(height, UnitType.COORDSIZE));
            }
            final String left = vmlStyle.get("left");
            if(left!=null) {
                drawingAttrs.put(OCKey.LEFT.value(), (int)getLengthAndUnitType(left, UnitType.COORDSIZE));
            }
            final String top = vmlStyle.get("top");
            if(top!=null) {
                drawingAttrs.put(OCKey.TOP.value(), (int)getLengthAndUnitType(top, UnitType.COORDSIZE));
            }
        }
        if(initialDrawingAttrs==null&&!drawingAttrs.isEmpty()) {
            attrs.put(OCKey.DRAWING.value(), drawingAttrs);
        }

        // fill attributes
        // ---------------
        final JSONObject initialFillAttrs = attrs.optJSONObject(OCKey.FILL.value());
        final JSONObject fillAttrs = initialFillAttrs!=null ? initialFillAttrs : new JSONObject();

        // type
        Boolean filled = null;
        CTFill fill = vmlCoreObject.getFill(false);
        if(fill!=null&&fill.getOn()!=null) {
            filled = fill.getOn().is();
        }
        if(filled==null&&vmlCoreObject.getFilled()!=null) {
            filled = vmlCoreObject.getFilled().is();
        }
        if(filled==null&&referenceShape!=null) {
            fill = referenceShape.getFill(false);
            if(fill!=null&&fill.getOn()!=null) {
                filled = fill.getOn().is();
            }
            if(filled==null&&referenceShape.getFilled()!=null) {
                filled = referenceShape.getFilled().is();
            }
        }
        if(filled==null||filled.booleanValue()) {
            fillAttrs.put(OCKey.TYPE.value(), "solid");
        }

        // color
        String fillColor = null;
        if(fill!=null) {
            fillColor = fill.getColor();
        }
        if(fillColor==null) {
            fillColor = vmlCoreObject.getFillcolor();
        }
        fillAttrs.put(OCKey.COLOR.value(), createJSONColorFromVML(fillColor, "FFFFFF"));

        // line attributes
        // ---------------
        final JSONObject initialLineAttrs = attrs.optJSONObject(OCKey.LINE.value());
        final JSONObject lineAttrs = initialLineAttrs!=null ? initialLineAttrs : new JSONObject();

        // type...
        // on/off inheritance order:
        // this Shape.stroke.on != null
        // this Shape.stroked != null
        // referenced Shape.stroke.on != null
        // referenced Shape.stroked !=null
        // otherwise true..
        Boolean stroked = null;
        CTStroke stroke = vmlCoreObject.getStroke(false);
        if(stroke!=null&&stroke.getOn()!=null) {
            stroked = stroke.getOn().is();
        }
        if(stroked==null&&vmlCoreObject instanceof VmlShapeCore&&((VmlShapeCore)vmlCoreObject).getStroked()!=null) {
            stroked = ((VmlShapeCore)vmlCoreObject).getStroked().is();
        }
        if(stroked==null&&referenceShape!=null) {
            stroke = referenceShape.getStroke(false);
            if(stroke!=null&&stroke.getOn()!=null) {
                stroked = stroke.getOn().is();
            }
            if(stroked==null&&referenceShape.getStroked()!=null) {
                stroked = referenceShape.getStroked().is();
            }
        }
        if(stroked==null||stroked.booleanValue()) {
            lineAttrs.put(OCKey.TYPE.value(), "solid");
        }
        // TODO: the following code is superfluous if line type "auto" is removed and the default is "none"
        else {
            lineAttrs.put(OCKey.TYPE.value(), "none");
        }
        // dashStyle
        if(stroke!=null) {
            final String dashStyle = stroke.getDashstyle();
            if(dashStyle!=null) {
                String style = null;
                if(dashStyle.contains("DashDotDot")) {
                    style = "dashDotDot";
                }
                else if(dashStyle.contains("DashDot")) {
                    style = "dashDot";
                }
                else if(dashStyle.contains("Dash")) {
                    style = "dashed";
                }
                else if(dashStyle.contains("Dot")) {
                    style = "dotted";
                }
                if(style!=null) {
                    lineAttrs.put(OCKey.STYLE.value(), style);
                }
            }
        }
        // color
        String strokeColor = null;
        String strokeWeight = null;
        if(stroke!=null) {
            strokeColor = stroke.getColor();
            strokeWeight = stroke.getWeight();
        }
        if(vmlCoreObject instanceof VmlShapeCore) {
            if(strokeColor==null) {
                strokeColor = ((VmlShapeCore)vmlCoreObject).getStrokecolor();
            }
            if(strokeWeight==null) {
                strokeWeight = ((VmlShapeCore)vmlCoreObject).getStrokeweight();
            }
        }
        lineAttrs.put(OCKey.COLOR.value(), createJSONColorFromVML(strokeColor, "000000"));
        lineAttrs.put(OCKey.WIDTH.value(), Math.round(getLengthAndUnitType(strokeWeight==null ? "1px" : strokeWeight, UnitType.EMU)));

        // geometry attributes
        final CTTextPath textPath = vmlCoreObject.getTextPath(false);
        if(textPath!=null) {
            final String textPathString = textPath.getString();
            if(textPathString!=null&&!textPathString.isEmpty()) {
                // ---------------
                Tools.addFamilyAttribute(attrs, OCKey.GEOMETRY, OCKey.TEXT_PATH, textPathString);
            }
        }

        // Shape attributes
        //-----------------
        if(shapeType==ShapeType.SHAPE) {
            // geometry attributes
            if(vmlCoreObject instanceof CTShape) {
                Integer spt = ((VmlShapeCore)vmlCoreObject).getSpt();
                if(spt==null && referenceShape!=null) {
                    spt = referenceShape.getSpt();
                }
                if(spt!=null) {
                    final STShapeType presetShapeType = EnhancedGeometryType.getPresetShape("mso-spt" + spt.toString());
                    if(presetShapeType!=null) {
                        Tools.addFamilyAttribute(attrs, OCKey.GEOMETRY, OCKey.PRESET_SHAPE, presetShapeType.value());
                    }
                }

                /* TODO: not only using the spt type, instead if available the vml geometry should be used.
                    String path = ((CTShape)vmlCoreObject).getPath();
                    if(path==null && referenceShape!=null) {
                        path = referenceShape.getPath();
                    }
                 */
            }
            final CTTextbox textbox = vmlCoreObject.getTextBox(false);
            if(textbox!=null) {
                final JSONObject initialShapeAttrs = attrs.optJSONObject(OCKey.SHAPE.value());
                final JSONObject shapeAttrs = initialShapeAttrs!=null ? initialShapeAttrs : new JSONObject();

                final String inset = textbox.getInset();
                if(inset!=null) {
                    final String[] paddings = inset.split(",");
                    if(paddings.length>=1&&!paddings[0].isEmpty()) {
                        shapeAttrs.put(OCKey.PADDING_LEFT.value(), Double.valueOf(Math.round(getLengthAndUnitType(paddings[0], UnitType.EMU))).intValue());
                    }
                    if(paddings.length>=2&&!paddings[1].isEmpty()) {
                        shapeAttrs.put(OCKey.PADDING_TOP.value(), Double.valueOf(Math.round(getLengthAndUnitType(paddings[1], UnitType.EMU))).intValue());
                    }
                    if(paddings.length>=3&&!paddings[2].isEmpty()) {
                        shapeAttrs.put(OCKey.PADDING_RIGHT.value(), Double.valueOf(Math.round(getLengthAndUnitType(paddings[2], UnitType.EMU))).intValue());
                    }
                    if(paddings.length>=4&&!paddings[3].isEmpty()) {
                        shapeAttrs.put(OCKey.PADDING_BOTTOM.value(), Double.valueOf(Math.round(getLengthAndUnitType(paddings[3], UnitType.EMU))).intValue());
                    }
                }

                final HashMap<String, String> textboxStyle = textbox.getStyleMap();
                if(textboxStyle.containsKey("mso-fit-shape-to-text")) {
                    final String val = textboxStyle.get("mso-fit-shape-to-text");
                    if(val.equals("t")) {
                        shapeAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), true);
                    }
                }
                if(initialShapeAttrs==null&&!shapeAttrs.isEmpty()) {
                    attrs.put(OCKey.SHAPE.value(), shapeAttrs);
                }
            }
        }

        // image / fill bitmap attributes
        //-----------------
        final String imageUrl = getImageUrl(operationDocument, vmlCoreObject);
        if(imageUrl!=null && !imageUrl.isEmpty()) {
            if(shapeType==ShapeType.OLE || shapeType==ShapeType.IMAGE) {
                JSONObject imageAttrs = attrs.optJSONObject(OCKey.IMAGE.value());
                if(imageAttrs==null) {
                    imageAttrs = new JSONObject(1);
                    attrs.put(OCKey.IMAGE.value(), imageAttrs);
                }
                imageAttrs.put(OCKey.IMAGE_URL.value(), imageUrl);
            }
            else {
                JSONObject bitmapAttrs = attrs.optJSONObject(OCKey.BITMAP.value());
                if(bitmapAttrs==null) {
                    bitmapAttrs = new JSONObject(1);
                    fillAttrs.put(OCKey.BITMAP.value(), bitmapAttrs);
                }
                bitmapAttrs.put(OCKey.IMAGE_URL.value(), imageUrl);
                fillAttrs.put(OCKey.TYPE.value(), "bitmap");
            }
        }

        if(initialFillAttrs==null&&!fillAttrs.isEmpty()) {
            attrs.put(OCKey.FILL.value(), fillAttrs);
        }

        if(initialLineAttrs==null&&!lineAttrs.isEmpty()) {
            attrs.put(OCKey.LINE.value(), lineAttrs);
        }
        return attrs;
    }

    private enum DimensionType {
        UNDEFINED,
        AUTO,
        LENGTH,
        PERCENT,
        INHERIT
    }

    private enum UnitType {
        UNDEFINED,
        PT,
        PC,
        IN,
        MM,
        CM,
        PX,
        EM,
        EX,
        PERCENT,
        EMU,
        COORDSIZE
    }

    private String getHorzBase() {
        String value = vmlStyle.get("mso-position-horizontal-relative"); // margin, page, text or char possible
        if(value==null)
            return "column";
        else if(value.equals("page"))
            return "page";
        else if(value.equals("text"))
            return "column";
        else if(value.equals("char"))
            return "character";
        return "margin";
    }

    private String getHorzAlign() {
        String value = vmlStyle.get("mso-position-horizontal");          // absolute, left, center, right, inside or outside possible
        if(value==null)
            return "offset";
        else if(value.equals("absolute"))
            return "offset";
        else if(value.equals("center"))
            return "center";
        else if(value.equals("right"))
            return "right";
        else if(value.equals("inside"))
            return "inside";
        else if(value.equals("outside"))
            return "outside";
        return "left";
    }

    private void setHorzBase(final String horzBase) {                   // page, column, character, leftMargin, insideMargin or outsideMargin
        if(horzBase.equals("page")) {
            vmlStyle.put("mso-position-horizontal-relative", "page");
        }
        else if(horzBase.equals("column")) {
            vmlStyle.put("mso-position-horizontal-relative", "text");
        }
        else if(horzBase.equals("character")) {
            vmlStyle.put("mso-position-horizontal-relative", "char");
        }
        else if(horzBase.equals("leftMargin")) {
            vmlStyle.put("mso-position-horizontal-relative", "margin");
        }
        else if(horzBase.equals("insideMargin")) {
            vmlStyle.put("mso-position-horizontal-relative", "text");
        }
        else if(horzBase.equals("outsideMargin")) {
            vmlStyle.put("mso-position-horizontal-relative", "text");
        }
    }

    private void setHorzAlign(final String horzAlign) {                 // left, right, center, inside, outside or offset
        if(horzAlign.equals("left")) {
            vmlStyle.put("mso-position-horizontal", "left");
        }
        if(horzAlign.equals("center")) {
            vmlStyle.put("mso-position-horizontal", "center");
        }
        if(horzAlign.equals("right")) {
            vmlStyle.put("mso-position-horizontal", "right");
        }
        if(horzAlign.equals("inside")) {
            vmlStyle.put("mso-position-horizontal", "inside");
        }
        if(horzAlign.equals("outside")) {
            vmlStyle.put("mso-position-horizontal", "outside");
        }
        if(horzAlign.equals("offset")) {                        // offset, we have to add the offset to the left margin..
            vmlStyle.remove("mso-position-horizontal");
        }
    }

    private String getVertBase() {
        String value = vmlStyle.get("mso-position-vertical-relative");   // margin, page, text or line possible
        if(value==null)
            return "paragraph";
        else if(value.equals("page"))
            return "page";
        else if(value.equals("text"))
            return "paragraph";
        else if(value.equals("line"))
            return "line";
        return "margin";
    }

    private String getVertAlign() {

        String value = vmlStyle.get("mso-position-vertical");            // absolute, top, center, bottom, inside or outside possible
        if(value==null)
            return "offset";
        else if(value.equals("absolute"))
            return "offset";
        else if(value.equals("center"))
            return "center";
        else if(value.equals("bottom"))
            return "bottom";
        else if(value.equals("inside"))
            return "inside";
        else if(value.equals("outside"))
            return "outside";
        return "top";
    }

    private void setVertBase(final String vertBase) {                   // margin, page, paragraph, line, topMargin, bottomMargin, insideMargin or outsideMargin
        if(vertBase.equals("margin")) {
            vmlStyle.put("mso-position-vertical-relative", "margin");
        }
        else if(vertBase.equals("page")) {
            vmlStyle.put("mso-position-vertical-relative", "page");
        }
        else if(vertBase.equals("paragraph")) {
            vmlStyle.put("mso-position-vertical-relative", "text");
        }
        else if(vertBase.equals("line")) {
            vmlStyle.put("mso-position-vertical-relative", "line");
        }
        else if(vertBase.equals("topMargin")) {
            vmlStyle.put("mso-position-vertical-relative", "margin");
        }
        else if(vertBase.equals("bottomMargin")) {
            vmlStyle.put("mso-position-vertical-relative", "margin");    // TODO: offset has to be added to the margin
        }
        else if(vertBase.equals("insideMargin")) {
            vmlStyle.put("mso-position-vertical-relative", "inside");
        }
        else if(vertBase.equals("outsideMargin")) {
            vmlStyle.put("mso-position-vertical-relative", "outside");
        }
    }

    private void setVertAlign(final String vertAlign) {                 // top, bottom, center, inside, outside or offset
        if(vertAlign.equals("top")) {
            vmlStyle.put("mso-position-vertical", "top");
        }
        else if(vertAlign.equals("bottom")) {
            vmlStyle.put("mso-position-vertical", "bottom");
        }
        else if(vertAlign.equals("center")) {
            vmlStyle.put("mso-position-vertical", "center");
        }
        else if(vertAlign.equals("inside")) {
            vmlStyle.put("mso-position-vertical", "inside");
        }
        else if(vertAlign.equals("outside")) {
            vmlStyle.put("mso-position-vertical", "outside");
        }
        else if(vertAlign.equals("offset")) {
            vmlStyle.remove("mso-position-vertical");
        }
    }

    private static DimensionType getDimensionType(String value) {
        if(value==null)
            return DimensionType.UNDEFINED;
        else if(value.equals("auto"))
            return DimensionType.AUTO;
        else if (value.equals("inherit"))
            return DimensionType.INHERIT;
        else if (value.contains("%"))
            return DimensionType.PERCENT;
        else if(value.matches(".*?pt|.*?pc|.*?in|.*?mm|.*?cm|.*?px"))
            return DimensionType.LENGTH;
        return DimensionType.UNDEFINED;
    }
    private static UnitType getUnitType(String value, UnitType defaultUnit) {
        if(value==null)
            return defaultUnit;
        else if(value.contains("pt"))
            return UnitType.PT;
        else if(value.contains("pc"))
            return UnitType.PC;
        else if(value.contains("in"))
            return UnitType.IN;
        else if(value.contains("mm"))
            return UnitType.MM;
        else if(value.contains("cm"))
            return UnitType.CM;
        else if(value.contains("px"))
            return UnitType.PX;
        else if(value.contains("em"))
            return UnitType.EM;
        else if(value.contains("ex"))
            return UnitType.EX;
        else if(value.contains("%"))
            return UnitType.PERCENT;
        else if(value.contains("emu"))
            return UnitType.EMU;
        else
            return defaultUnit;
    }
    // returns the length in 1/100mm
    private static double getLengthAndUnitType(String value, UnitType defaultUnit) throws ParseException {
        return getLength(value, getUnitType(value, defaultUnit));
    }

    private static double getLength(String value, UnitType unitType) throws ParseException {

        return getLength(NumberFormat.getInstance(Locale.US).parse(value).doubleValue(), unitType);
    }

    private static double getLength(double length, UnitType unitType) {
        switch(unitType) {
            case COORDSIZE: {
                return length;
            }
            case EM :
            case EX :
            case PERCENT :
                return 1;

            default:
            case UNDEFINED :
            case EMU : {
                length /= 360.0;
            }
            break;
            case PT : length = (length/72)*2540; break;
            case PC : length = (length/6)*2540; break;
            case IN : length = length*2540; break;
            case MM : length = length*100; break;
            case CM : length = length*1000; break;
            case PX : length = (length/96)*2540; break;
        }
        return length;
    }

    private static String getVMLLength(long length100ThMM) {
        BigDecimal dec = new BigDecimal(((double)length100ThMM*72/2540));
        dec = dec.setScale(2, RoundingMode.HALF_UP);
        return dec.toString() + "pt";
    }

    // sets length which is assumed to be in 1/100mm
    private void setLength(String key, int length) {
        vmlStyle.put(key, getVMLLength(length));
    }

    public static String getImageUrl(OfficeOpenXMLOperationDocument operationDocument, VmlCore vmlCore) {
        String imageUrl = null;
        final CTImageData imageData = vmlCore.getImageData(false);
        if(imageData!=null) {
            final String rId = imageData.getId();
            if(rId!=null) {
                imageUrl = Commons.getUrl(operationDocument.getContextPart(), rId);
            }
        }
        return imageUrl;
    }
}
