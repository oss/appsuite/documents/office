/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.components;

import org.json.JSONObject;
import com.openexchange.office.filter.core.component.ComponentType;

public interface IShape extends IShapeType {

	public void applyAttrsFromJSON(JSONObject attrs) throws Exception;

	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception;

	// returns the next shape (the parent must be a group shape)
	public IShape getNext();

	// returns the first child shape (the parent must be a group shape)
	public Object getChild();

	// inserts a new shape (image, shape and or group into the parent group)
	// returns either IShape or Component
    public Object insertChild(int number, ComponentType type) throws Exception;
}
