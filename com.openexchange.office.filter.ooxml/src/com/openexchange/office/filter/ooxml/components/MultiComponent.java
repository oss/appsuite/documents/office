/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.components;

import java.util.ArrayList;
import java.util.List;
import org.docx4j.wml.CTTxbxContent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class MultiComponent extends OfficeOpenXMLComponent implements IParagraph, ITable, IRow {

	final private List<OfficeOpenXMLComponent> componentList;

	public MultiComponent(OfficeOpenXMLOperationDocument operationDocument, List<OfficeOpenXMLComponent> componentList) {
		super(operationDocument, new DLNode<Object>(componentList), 0);

		this.componentList = componentList;
	}
	public List<OfficeOpenXMLComponent> getComponentList() {
		return componentList;
	}
	@Override
	public OfficeOpenXMLComponent getComponent(int number) {
		final List<OfficeOpenXMLComponent> nextComponents = new ArrayList<OfficeOpenXMLComponent>(componentList.size());
		for(OfficeOpenXMLComponent component:componentList) {
			final OfficeOpenXMLComponent nextComponent = (OfficeOpenXMLComponent)component.getComponent(number);
			if(nextComponent!=null) {
				nextComponents.add(nextComponent);
			}
		}
		return createComponent(operationDocument, nextComponents);
    }
	@Override
    public int getComponentNumber() {
        return componentList.get(0).getComponentNumber();
    }
    @Override
    public int getNextComponentNumber() {
		return componentList.get(0).getNextComponentNumber();
    }
	@Override
	public OfficeOpenXMLComponent getNextComponent() {
		final List<OfficeOpenXMLComponent> nextComponents = new ArrayList<OfficeOpenXMLComponent>(componentList.size());
		for(OfficeOpenXMLComponent component:componentList) {
			final OfficeOpenXMLComponent nextComponent = (OfficeOpenXMLComponent)component.getNextComponent();
			if(nextComponent!=null) {
				nextComponents.add(nextComponent);
			}
		}
		return createComponent(operationDocument, nextComponents);
	}
	@Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
		final List<OfficeOpenXMLComponent> childComponents = new ArrayList<OfficeOpenXMLComponent>(componentList.size());
		for(OfficeOpenXMLComponent component:componentList) {
			final OfficeOpenXMLComponent childComponent = (OfficeOpenXMLComponent)component.getNextChildComponent(previousChildContext, previousChildComponent);
			if(childComponent!=null) {
				childComponents.add(childComponent);
			}
		}
		return createComponent(operationDocument, childComponents);
	}
	@Override
	public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {
		return null;
	}
	@Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {
	    final List<OfficeOpenXMLComponent> childComponents = new ArrayList<OfficeOpenXMLComponent>(componentList.size());
		for(OfficeOpenXMLComponent component:componentList) {
			childComponents.add((OfficeOpenXMLComponent)component.insertChildComponent(number, attrs, type));
		}
		return new MultiComponent(operationDocument, childComponents);
	}
	@Override
	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

		for(OfficeOpenXMLComponent component:componentList) {
			component.applyAttrsFromJSON(attrs);
		}
	}
	@Override
	public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {
		return componentList.get(0).createJSONAttrs(attrs);
	}
	@Override
	public void delete(int count) {
		for(OfficeOpenXMLComponent component:componentList) {
	        component.delete(count);
	    }
	}
	@Override
    public void splitStart(int n, SplitMode splitMode) {
		for(OfficeOpenXMLComponent component:componentList) {
			component.splitStart(n, splitMode);
		}
    }
	@Override
    public void splitEnd(int n, SplitMode splitMode) {
		for(OfficeOpenXMLComponent component:componentList) {
			component.splitEnd(n, splitMode);
		}
    }
	// IParagraph interfaces
	@Override
	public void insertText(int textPosition, String text, JSONObject attrs) throws Exception {
		for(OfficeOpenXMLComponent component:componentList) {
			((IParagraph)component).insertText(textPosition, text, attrs);
		}
	}
	@Override
    public void splitParagraph(int textPosition) {
		for(OfficeOpenXMLComponent component:componentList) {
			((IParagraph)component).splitParagraph(textPosition);
		}
	}
	@Override
    public void mergeParagraph() {
		for(OfficeOpenXMLComponent component:componentList) {
			((IParagraph)component).mergeParagraph();
		}
	}
	// ITable interface
	@Override
	public void insertRows(int rowPosition, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {
	    for(OfficeOpenXMLComponent component:componentList) {
			((ITable)component).insertRows(rowPosition, count, insertDefaultCells, referenceRow, attrs);
		}
	}
	@Override
	public void splitTable(int rowPosition) {
		for(OfficeOpenXMLComponent component:componentList) {
			((ITable)component).splitTable(rowPosition);
		}
	}
	@Override
	public void mergeTable() {
		for(OfficeOpenXMLComponent component:componentList) {
			((ITable)component).mergeTable();
		}
	}
	@Override
	public void insertColumn(JSONArray tableGrid, int gridPosition, String insertMode)
		throws JSONException {

		for(OfficeOpenXMLComponent component:componentList) {
			((ITable)component).insertColumn(tableGrid, gridPosition, insertMode);
		}
	}
	@Override
    public void deleteColumns(int gridStart, int gridEnd)
    	throws JSONException {

		for(OfficeOpenXMLComponent component:componentList) {
			((ITable)component).deleteColumns(gridStart, gridEnd);
		}
	}
	// IRow interface
	@Override
	public void insertCells(int cellPosition, int count, JSONObject attrs) throws Exception {
		for(OfficeOpenXMLComponent component:componentList) {
			((IRow)component).insertCells(cellPosition, count, attrs);
		}
	}

	public static OfficeOpenXMLComponent createComponent(OfficeOpenXMLOperationDocument operationDocument, List<OfficeOpenXMLComponent> componentList) {
	    if(componentList.isEmpty()) {
	        return null;
	    }
	    if(componentList.size()==1) {
	        return componentList.get(0);
	    }
	    final OfficeOpenXMLComponent a = componentList.get(0);
	    final OfficeOpenXMLComponent b = componentList.get(1);
	    if(a instanceof IShapeProvider && b instanceof IShapeProvider) {
	        final IShape shapea = ((IShapeProvider)a).getShape();
            final IShape shapeb = ((IShapeProvider)b).getShape();
            if(shapea instanceof IShapeTxBxContentAccessor && shapeb instanceof IShapeTxBxContentAccessor) {
    	        final CTTxbxContent txa = ((IShapeTxBxContentAccessor)shapea).getTextboxContent(false);
    	        final CTTxbxContent txb = ((IShapeTxBxContentAccessor)shapeb).getTextboxContent(false);
    	        if(txa!=null&&txb!=null) {
    	            // provide one instance for text only
    	            ((IShapeTxBxContentAccessor)shapeb).setTextboxContent(txa);
    	            return a;
    	        }
            }
	    }
	    return new MultiComponent(operationDocument, componentList);
	}
}
