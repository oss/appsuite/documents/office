/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.components;

import java.util.Map;
import org.docx4j.vml.CTArc;
import org.docx4j.vml.CTCurve;
import org.docx4j.vml.CTGroup;
import org.docx4j.vml.CTImage;
import org.docx4j.vml.CTLine;
import org.docx4j.vml.CTOval;
import org.docx4j.vml.CTPath;
import org.docx4j.vml.CTPolyLine;
import org.docx4j.vml.CTRect;
import org.docx4j.vml.CTRoundRect;
import org.docx4j.vml.CTShape;
import org.docx4j.vml.CTShapetype;
import org.docx4j.vml.CTTextbox;
import org.docx4j.vml.STStrokeJoinStyle;
import org.docx4j.vml.STTrueFalse;
import org.docx4j.vml.officedrawing.STConnectType;
import org.docx4j.vml.officedrawing.STInsetMode;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.spreadsheetDrawing.STObjectType;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;

public class VMLGroup extends VMLBase {

    public VMLGroup(String documentType, ComponentContext<OfficeOpenXMLOperationDocument> componentContext, DLNode<Object> node, int componentNumber) {
        super(documentType, componentContext, node, componentNumber);
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> paragraphNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)paragraphNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        OfficeOpenXMLComponent nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, paragraphNode.getData());
            if(o instanceof CTShapetype) {
                final String id = ((CTShapetype)o).getVmlId();
                if(id!=null&&!id.isEmpty()) {
                    getOperationDocument().getVMLShapeTypes().put(id, (CTShapetype)o);
                }
            }
            else if(o instanceof CTGroup) {
                nextComponent = new VMLGroup(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTShape) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTArc) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTCurve) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTImage) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTLine) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTOval) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTPolyLine) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTRect) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTRoundRect) {
                nextComponent = new VMLShape(documentType, this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public Object getChild() {
        return getNextChildComponent(null, null);
    }

    @Override
    public Object insertChild(int number, ComponentType type) {
        try {
            return insertChildComponent(number, null, type);
        }
        catch(Exception e) {
            return null;
        }
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

        final DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        final DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
            case AC_COMMENT : {
                final Map<String, CTShapetype> shapeTypes = getOperationDocument().getVMLShapeTypes();
                if(!shapeTypes.containsKey("_x0000_t202")) {
                    final CTShapetype shapeType = new CTShapetype();
                    shapeType.setCoordsize("21600,21600");
                    shapeType.setSpt(202);
                    shapeType.setPath("m,l,21600r21600,l21600,xe");
                    shapeType.setVmlId("_x0000_t202");
                    shapeType.getStroke(true).setJoinstyle(STStrokeJoinStyle.MITER);
                    final CTPath path = shapeType.getPathElement(true);
                    path.setGradientshapeok(STTrueFalse.TRUE);
                    path.setConnecttype(STConnectType.RECT);
                    DLList.addNode(referenceNode, new DLNode<Object>(shapeType), true);
                    shapeTypes.put(shapeType.getVmlId(), shapeType);
                }
                final CTShape newShape = new CTShape();
                newShape.setParent(getObject());
                newShape.setType("#_x0000_t202");
                newShape.setStyle("position:absolute;margin-left:251.25pt;margin-top:52.5pt;width:96pt;height:64.5pt;visibility:visible");
                newShape.setFillcolor("infoBackground [80]");
                newShape.setStrokecolor("none [81]");
                newShape.setInsetmode(STInsetMode.AUTO);
                newShape.getFill(true).setColor2("infoBackground [80]");
                newShape.getPathElement(true).setConnecttype(STConnectType.NONE);
                final CTTextbox textBox = newShape.getTextBox(true);
                textBox.setStyle("mso-direction-alt:auto");
                final CTClientData clientData = newShape.getClientData(true);
                clientData.setObjectType(STObjectType.NOTE);
                clientData.setMoveWithCells(new String());
                clientData.setSizeWithCells(new String());
                clientData.setAutoFill("False");
                clientData.setVisible(new String());
                final DLNode<Object> newChildNode = new DLNode<Object>(newShape);
                DLList.addNode(referenceNode, newChildNode, true);
                return new VMLShape(documentType, parentContext, newChildNode, number);
            }
            case AC_GROUP: {
                final CTGroup newGroup = new CTGroup();
                newGroup.setParent(getObject());
                newGroup.setVmlId("Group 1");
                newGroup.setStyle("position:absolute;width:100;height:100");
                newGroup.setSpid(operationDocument.getPackage().getNextMarkupId().toString());
                final DLNode<Object> newChildNode = new DLNode<Object>(newGroup);
                DLList.addNode(referenceNode, newChildNode, true);
                return new VMLGroup(documentType, parentContext, newChildNode, number);
            }
            case AC_CONNECTOR:
                // PASSTHROUGH INTENDED
            case AC_SHAPE: {
                final CTRect newShape = new CTRect();
                newShape.setParent(getObject());
                newShape.setVmlId("Rectangle 1");
                newShape.setStyle("position:absolute;width:100;height:100;visibility:visible;mso-wrap-style:square;v-text-anchor:middle");
                newShape.setFilled(org.docx4j.vml.STTrueFalse.F);
                newShape.setStroked(org.docx4j.vml.STTrueFalse.F);
                newShape.setStrokeweight("1pt");
                newShape.setSpid(operationDocument.getPackage().getNextMarkupId().toString());
                final DLNode<Object> newChildNode = new DLNode<Object>(newShape);
                DLList.addNode(referenceNode, newChildNode, true);
                return new VMLShape(documentType, parentContext, newChildNode, number);
            }
            case AC_IMAGE: {
                final CTImage newImage = new CTImage();
                newImage.setParent(getObject());
                newImage.setVmlId("Image 1");
                newImage.setStyle("position:absolute;width:100;height:100;visibility:visible;mso-wrap-style:square");
                newImage.setSpid(operationDocument.getPackage().getNextMarkupId().toString());
                final DLNode<Object> newChildNode = new DLNode<Object>(newImage);
                DLList.addNode(referenceNode, newChildNode, true);
                return new VMLShape(documentType, parentContext, newChildNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {
        super.applyAttrsFromJSON(attrs);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) throws Exception {
        return super.createJSONAttrs(attrs);
    }
}
