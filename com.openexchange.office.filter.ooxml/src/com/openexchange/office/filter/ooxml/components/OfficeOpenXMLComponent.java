/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.components;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public abstract class OfficeOpenXMLComponent extends ComponentContext<OfficeOpenXMLOperationDocument> implements IComponent<OfficeOpenXMLOperationDocument> {

    private int componentNumber;

    @Override
    public int getComponentNumber() {
        return componentNumber;
    }

    @Override
    public void setComponentNumber(int componentNumber) {
        this.componentNumber = componentNumber;
    }

    @Override
    public int getNextComponentNumber() {
        return componentNumber + 1;
    }

    private static HashSet<Class<?>> rootElements = new HashSet<Class<?>>();

    /*
     * simply returns o, but in case o is a JAXBElement and a RootElement annotation is available
     * then the parent content list is updated and the contentModel is returned.
     * */
    public static Object getContentModel(DLNode<Object> node, Object parent) {

        if (node.getData() instanceof jakarta.xml.bind.JAXBElement) {
            boolean hasRootElement = rootElements.contains(((jakarta.xml.bind.JAXBElement<?>)node.getData()).getDeclaredType());
            if(!hasRootElement) {
                final Annotation[] annotations = ((jakarta.xml.bind.JAXBElement<?>)node.getData()).getDeclaredType().getAnnotations();
                for(Annotation annotation : annotations) {
                    if(annotation instanceof XmlRootElement) {
                        rootElements.add(((jakarta.xml.bind.JAXBElement<?>)node.getData()).getDeclaredType());
                        hasRootElement = true;
                        break;
                    }
                }
            }
            if(hasRootElement) {
                node.setData(((JAXBElement<?>)node.getData()).getValue());
            }
        }
        if(node.getData() instanceof Child) {
        	((Child)node.getData()).setParent(parent);
        }
        return node.getData();
    }

    public OfficeOpenXMLComponent(OfficeOpenXMLOperationDocument operationDocument, DLNode<Object> _node, int componentNumber) {
        super(operationDocument, _node);
        this.componentNumber = componentNumber;
    }

    public OfficeOpenXMLComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int componentNumber) {
        super(parentContext, _node);
        this.componentNumber = componentNumber;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {
        final IComponent<OfficeOpenXMLOperationDocument> c = insertChildComponent(this, getNode(), number, getChildComponent(number), type, attrs);
        if(attrs!=null) {
            c.applyAttrsFromJSON(attrs);
        }
        return c;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextComponent() {
        if(getParentContext()==null) {
            return null;
        }
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = this;
        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        do {
            ComponentContext<OfficeOpenXMLOperationDocument> previousContext = parentContext;
            parentContext = parentContext.getParentContext();
            nextComponent = parentContext.getNextChildComponent(previousContext, this);
        }
        while(nextComponent==null&&!(parentContext instanceof IComponent));
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getComponent(int number) {
        IComponent<OfficeOpenXMLOperationDocument> c = this;
        while(c!=null&&c.getNextComponentNumber()<=number) {
            c = c.getNextComponent();
        }
        return c;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getComponent(JSONArray position, int positionCount) {
        try {
            if(positionCount==0) {
                return this;
            }
            // a part has to implement the ContentAccessor interface, otherwise no component can be accessed
            IComponent<OfficeOpenXMLOperationDocument> c = getNextChildComponent(null, null);
            for(int i=0; c!=null&&i<positionCount;) {
                c = c.getComponent(position.getInt(i++));
                if(i!=positionCount) {
                    c = c.getNextChildComponent(null, null);
                }
            }
            return c;
        }
        catch(JSONException e) {
            // ups
        }
        return null;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getParentComponent() {
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)) {
            parentContext = parentContext.getParentContext();
        }
        return (IComponent<OfficeOpenXMLOperationDocument>)parentContext;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getChildComponent(int childComponentNumber) {
        IComponent<OfficeOpenXMLOperationDocument> c = getNextChildComponent(null, null);
        if(c!=null) {
            c = c.getComponent(childComponentNumber);
        }
        return c;
    }

    @Override
    public ComponentContext<OfficeOpenXMLOperationDocument> getContextChild(Set<Class<?>> parentContextList) {
        ComponentContext<OfficeOpenXMLOperationDocument> childContext = this;
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)&&((parentContextList==null)||(!parentContextList.contains(parentContext.getClass())))) {
            childContext = parentContext;
            parentContext = childContext.getParentContext();
        }
        return childContext;
    }

    public abstract JSONObject createJSONAttrs(JSONObject attrs) throws Exception;

    public static void move(OfficeOpenXMLOperationDocument operationDocument, JSONArray startPosition, JSONArray endPosition, JSONArray toPosition)
        throws JSONException {

    	if(endPosition==null) {
    		endPosition = startPosition;
    	}
    	else if(startPosition.length()!=endPosition.length())
            throw new JSONException("ooxml export: move operation, size of startPosition != size of endPosition");

        final IComponent<OfficeOpenXMLOperationDocument> sourceBegComponent = operationDocument.getRootComponent().getComponent(startPosition, startPosition.length());
        final IComponent<OfficeOpenXMLOperationDocument> sourceEndComponent = sourceBegComponent.getComponent(endPosition.getInt(startPosition.length()-1));
        final DLList<Object> sourceContent = (DLList<Object>)((IContentAccessor)sourceBegComponent.getParentComponent().getNode().getData()).getContent();
        sourceBegComponent.splitStart(startPosition.getInt(startPosition.length()-1), SplitMode.DELETE);
        sourceEndComponent.splitEnd(endPosition.getInt(startPosition.length()-1), SplitMode.DELETE);

        // destComponent is empty if the content is to be appended
        final IComponent<OfficeOpenXMLOperationDocument> destComponent = operationDocument.getRootComponent().getComponent(toPosition, toPosition.length());
        IComponent<OfficeOpenXMLOperationDocument> destParentComponent;
        if(destComponent!=null) {
        	destComponent.splitStart(toPosition.getInt(toPosition.length()-1), SplitMode.DELETE);
        	destParentComponent = destComponent.getParentComponent();
        }
        else {
        	destParentComponent = operationDocument.getRootComponent().getComponent(toPosition, toPosition.length()-1);
        }
        final DLList<Object> destContent = (DLList<Object>)((IContentAccessor)destParentComponent.getNode().getData()).getContent();
        final boolean before = sourceContent==destContent && startPosition.getInt(startPosition.length()-1) <= toPosition.getInt(toPosition.length()-1) ? false : true;
        if(destComponent!=null) {
            sourceContent.moveNodes(sourceBegComponent.getContextChild(null).getNode(), sourceEndComponent.getContextChild(null).getNode(), destContent,
                destComponent.getContextChild(null).getNode(), before, destParentComponent.getNode().getData());
        }
        else {
            sourceContent.moveNodes(sourceBegComponent.getContextChild(null).getNode(), sourceEndComponent.getContextChild(null).getNode(), destContent,
                destContent.getLastNode(), false, destParentComponent.getNode().getData());
        }
    }

    @Override
    public String simpleName() {
        // TODO Auto-generated method stub
        return "";
    }

    // the ComponentContext c is inserted behind one of the classes in the
    // parentContextList, it is inserted at least behind the parent Component,
    // so the parentContextList is allowed to be null
    //
    // the parentContext of c is inserted, so c does not need to have a parentContext
    public void insertContext(ComponentContext<OfficeOpenXMLOperationDocument> c, Set<Class<?>> parentContextList) {
        ComponentContext<OfficeOpenXMLOperationDocument> childContext = this;
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)&&((parentContextList==null)||(!parentContextList.contains(parentContext.getClass())))) {
            childContext = parentContext;
            parentContext = childContext.getParentContext();
        }

        final DLList<Object> parentContent = (DLList<Object>)((IContentAccessor)parentContext.getNode().getData()).getContent();
        parentContent.setNode(childContext.getNode(), c.getNode());
        final DLList<Object> content = (DLList<Object>)((IContentAccessor)c.getNode().getData()).getContent();
        content.addNode(childContext.getNode());
        c.setParentContext(parentContext);
        childContext.setParentContext(c);
        ((Child)c.getNode().getData()).setParent(parentContext.getNode().getData());
        ((Child)childContext.getNode().getData()).setParent(c.getNode().getData());
    }

    public void removeContext(Class<?> contextClass) {
        ComponentContext<OfficeOpenXMLOperationDocument> childContext = this;
        ComponentContext<OfficeOpenXMLOperationDocument> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)) {
            if(parentContext.getClass()==contextClass) {
                final ComponentContext<OfficeOpenXMLOperationDocument> newParentContext = parentContext.getParentContext();
                ((DLList<Object>)((IContentAccessor)newParentContext.getNode().getData()).getContent()).setNodes(parentContext.getNode(), (DLList<Object>)((IContentAccessor)parentContext.getNode().getData()).getContent());
                childContext.setParentContext(newParentContext);
                ((Child)childContext.getNode().getData()).setParent(newParentContext.getNode().getData());
                break;
            }
            childContext = parentContext;
            parentContext = childContext.getParentContext();
        }
    }
}
