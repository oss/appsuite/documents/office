/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import org.docx4j.dml.CTSchemeColor;
import org.docx4j.dml.STSchemeColorVal;
import org.docx4j.dml.chartStyle2012.CTChartStyle;
import org.docx4j.dml.chartStyle2012.CTStyleEntry;
import org.docx4j.dml.chartStyle2012.CTStyleReference;

public class ChartStyleDefaults
{
    /**
     *
     * search string "Table 1: Chart element defaults" Page 3897
     *
     */

    private final static CTChartStyle DUMMY;

    static
    {
        DUMMY = new CTChartStyle();
        DUMMY.setCategoryAxis(get(true, false));
        DUMMY.setSeriesAxis(get(true, false));
        DUMMY.setAxisTitle(get(false, false));
        DUMMY.setChartArea(get(true, true));
        DUMMY.setTitle(get(false, false));
        DUMMY.setDataLabel(get(false, false));
        DUMMY.setDataTable(get(true, false));
        DUMMY.setDownBar(get(true, true));
        DUMMY.setDataPoint(get(true, true));
        DUMMY.setDataPoint3D(get(true, true));
        DUMMY.setSeriesLine(get(true, false));
        DUMMY.setFloor(get(true, true));
        DUMMY.setLegend(get(false, true));
        DUMMY.setDataPointLine(get(true, false));
        DUMMY.setGridlineMajor(get(true, false));
        DUMMY.setDataPointMarker(get(true, true));
        DUMMY.setGridlineMinor(get(true, false));
        DUMMY.setLeaderLine(get(true, false));
        DUMMY.setPlotArea(get(false, true));
        DUMMY.setPlotArea3D(get(false, false));
        DUMMY.setTrendlineLabel(get(false, false));
        DUMMY.setUpBar(get(true, true));
        DUMMY.setWall(get(false, true));
    }

    private static CTStyleEntry get(
        boolean line,
        boolean fill)
    {
        CTStyleEntry entry = new CTStyleEntry();
        entry.setFillRef(get(fill));
        entry.setLnRef(get(line));
        return entry;
    }

    private static CTStyleReference get(
        boolean style)
    {
        CTStyleReference ref = new CTStyleReference();

        if (style)
        {
            ref.setIdx(1);
            CTSchemeColor placeHolder = new CTSchemeColor();
            placeHolder.setVal(STSchemeColorVal.PH_CLR);
            ref.setSchemeClr(placeHolder);
        }
        else
        {
            ref.setIdx(0);
        }
        return ref;
    }

    public static CTChartStyle get(
        /*int styleId*/)
    {
        return DUMMY;
    }
}
