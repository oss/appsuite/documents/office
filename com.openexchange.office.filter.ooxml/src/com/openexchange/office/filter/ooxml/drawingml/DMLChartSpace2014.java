/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.util.HashMap;
import java.util.List;
import org.docx4j.dml.chartex2014.CTChartSpace;
import org.docx4j.dml.chartex2014.CTFormula;
import org.docx4j.dml.chartex2014.CTSeries;
import org.docx4j.dml.chartex2014.IDimension;
import org.docx4j.dml.chartex2014.STSeriesLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.ooxml.tools.Commons;

public class DMLChartSpace2014 {

    public static void getChartType(ChartWrapper chart, HashMap<String, Object> map) {
        CTChartSpace space = chart.getChartPart2014().getJaxbElement();
        List<CTSeries> series = space.getChart().getPlotArea().getPlotAreaRegion().getSeries();

        STSeriesLayout type = series.get(0).getLayoutId();
        map.put(OCKey.CHART_TYPE.value(), type.value());
    }

    public static void createChartSpaceOperations(JSONArray operationsArray, List<Integer> position, ChartWrapper chart) throws JSONException {
        CTChartSpace space = chart.getChartPart2014().getJaxbElement();
        List<CTSeries> series = space.getChart().getPlotArea().getPlotAreaRegion().getSeries();

        List<Object> data = space.getChartData().getData().get(0).getNumDimOrStrDim();

        String names = null;
        String values = null;

        for (Object d : data) {
            if (d instanceof IDimension) {
                IDimension dim = (IDimension) d;

                CTFormula f = (CTFormula) dim.getContent().get(0).getValue();

                if ("cat".equals(dim.getDimensionType())) {
                    names = f.getValue();
                } else if ("size".equals(dim.getDimensionType())) {
                    values = f.getValue();
                } else if ("val".equals(dim.getDimensionType())) {
                    values = f.getValue();
                }
            }
        }

        JSONObject shape = new JSONObject();
        shape.put(OCKey.TYPE.value(), "solid");

        for (int i = 0; i < series.size(); i++) {
            CTSeries serie = series.get(i);

            JSONObject json = new JSONObject(3);
            json.putSafe(OCKey.TYPE.value(), serie.getLayoutId().value());
            json.putSafe(OCKey.NAMES.value(), names);
            json.putSafe(OCKey.VALUES.value(), values);

            final JSONObject attrs = Commons.surroundJSONObject(OCKey.SERIES.value(), json);

            attrs.put(OCKey.FILL.value(), shape);
            attrs.put(OCKey.LINE.value(), shape);

            JSONObject op = new JSONObject(4);
            op.put(OCKey.NAME.value(), OCValue.INSERT_CHART_SERIES.value());
            op.put(OCKey.START.value(), position);
            op.put(OCKey.SERIES.value(), i);
            op.put(OCKey.ATTRS.value(), attrs);
            operationsArray.put(op);
        }

    }

    public static void setDataSeriesAttributes(ChartWrapper chart, int seriesIndex, JSONObject attrs) throws JSONException {

        final JSONObject attrSeries = attrs.optJSONObject(OCKey.SERIES.value());
        if (attrSeries != null) {

            if (seriesIndex == 0) {
                CTChartSpace space = chart.getChartPart2014().getJaxbElement();
                List<Object> data = space.getChartData().getData().get(0).getNumDimOrStrDim();


                for (Object d : data) {
                    if (d instanceof IDimension) {
                        IDimension dim = (IDimension) d;

                        CTFormula f = (CTFormula) dim.getContent().get(0).getValue();

                        if ("cat".equals(dim.getDimensionType())) {
                            if (attrSeries.has(OCKey.NAMES.value())) {
                                final Object names = attrSeries.get(OCKey.NAMES.value());
                                if (names != null) {
                                    f.setValue(names.toString());
                                } else {
                                    f.setValue("");
                                }
                            }
                        } else if ("size".equals(dim.getDimensionType())) {
                            if (attrSeries.has(OCKey.VALUES.value())) {
                                final Object values = attrSeries.get(OCKey.VALUES.value());
                                if (values != null) {
                                    f.setValue(values.toString());
                                } else {
                                    f.setValue("");
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}
