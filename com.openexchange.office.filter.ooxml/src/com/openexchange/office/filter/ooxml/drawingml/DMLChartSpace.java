/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import jakarta.xml.bind.annotation.XmlElements;
import org.docx4j.dml.CTEffectList;
import org.docx4j.dml.CTLineJoinRound;
import org.docx4j.dml.CTLineProperties;
import org.docx4j.dml.CTNoFillProperties;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTextCharacterProperties;
import org.docx4j.dml.CTTextField;
import org.docx4j.dml.CTTextLineBreak;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.CTTextParagraphProperties;
import org.docx4j.dml.ITextCharacterProperties;
import org.docx4j.dml.STLineCap;
import org.docx4j.dml.chart.CTArea3DChart;
import org.docx4j.dml.chart.CTAreaChart;
import org.docx4j.dml.chart.CTAxDataSource;
import org.docx4j.dml.chart.CTAxPos;
import org.docx4j.dml.chart.CTBar3DChart;
import org.docx4j.dml.chart.CTBarChart;
import org.docx4j.dml.chart.CTBarDir;
import org.docx4j.dml.chart.CTBarSer;
import org.docx4j.dml.chart.CTBoolean;
import org.docx4j.dml.chart.CTBubbleChart;
import org.docx4j.dml.chart.CTBubbleSer;
import org.docx4j.dml.chart.CTCatAx;
import org.docx4j.dml.chart.CTChart;
import org.docx4j.dml.chart.CTChartLines;
import org.docx4j.dml.chart.CTChartSpace;
import org.docx4j.dml.chart.CTCrossBetween;
import org.docx4j.dml.chart.CTCrosses;
import org.docx4j.dml.chart.CTDLbl;
import org.docx4j.dml.chart.CTDLblPos;
import org.docx4j.dml.chart.CTDLbls;
import org.docx4j.dml.chart.CTDPt;
import org.docx4j.dml.chart.CTDoughnutChart;
import org.docx4j.dml.chart.CTFirstSliceAng;
import org.docx4j.dml.chart.CTGapAmount;
import org.docx4j.dml.chart.CTHoleSize;
import org.docx4j.dml.chart.CTLayout;
import org.docx4j.dml.chart.CTLegend;
import org.docx4j.dml.chart.CTLegendPos;
import org.docx4j.dml.chart.CTLine3DChart;
import org.docx4j.dml.chart.CTLineChart;
import org.docx4j.dml.chart.CTLineSer;
import org.docx4j.dml.chart.CTMarker;
import org.docx4j.dml.chart.CTMarkerStyle;
import org.docx4j.dml.chart.CTNumData;
import org.docx4j.dml.chart.CTNumDataSource;
import org.docx4j.dml.chart.CTNumFmt;
import org.docx4j.dml.chart.CTNumRef;
import org.docx4j.dml.chart.CTNumVal;
import org.docx4j.dml.chart.CTOfPieChart;
import org.docx4j.dml.chart.CTOrientation;
import org.docx4j.dml.chart.CTOverlap;
import org.docx4j.dml.chart.CTPie3DChart;
import org.docx4j.dml.chart.CTPieChart;
import org.docx4j.dml.chart.CTPlotArea;
import org.docx4j.dml.chart.CTRadarChart;
import org.docx4j.dml.chart.CTRadarStyle;
import org.docx4j.dml.chart.CTRotX;
import org.docx4j.dml.chart.CTRotY;
import org.docx4j.dml.chart.CTScaling;
import org.docx4j.dml.chart.CTScatterChart;
import org.docx4j.dml.chart.CTScatterSer;
import org.docx4j.dml.chart.CTScatterStyle;
import org.docx4j.dml.chart.CTSerAx;
import org.docx4j.dml.chart.CTSerTx;
import org.docx4j.dml.chart.CTStockChart;
import org.docx4j.dml.chart.CTStrData;
import org.docx4j.dml.chart.CTStrRef;
import org.docx4j.dml.chart.CTStrVal;
import org.docx4j.dml.chart.CTStyle;
import org.docx4j.dml.chart.CTSurface3DChart;
import org.docx4j.dml.chart.CTSurfaceChart;
import org.docx4j.dml.chart.CTTickLblPos;
import org.docx4j.dml.chart.CTTickMark;
import org.docx4j.dml.chart.CTTitle;
import org.docx4j.dml.chart.CTTx;
import org.docx4j.dml.chart.CTUnsignedInt;
import org.docx4j.dml.chart.CTValAx;
import org.docx4j.dml.chart.CTView3D;
import org.docx4j.dml.chart.IAxisDescription;
import org.docx4j.dml.chart.IListSer;
import org.docx4j.dml.chart.ISerContent;
import org.docx4j.dml.chart.ISerErrorBarListContent;
import org.docx4j.dml.chart.ISerSingleErrorBarContent;
import org.docx4j.dml.chart.ObjectFactory;
import org.docx4j.dml.chart.RotationChart;
import org.docx4j.dml.chart.STAxPos;
import org.docx4j.dml.chart.STCrossBetween;
import org.docx4j.dml.chart.STCrosses;
import org.docx4j.dml.chart.STDLblPos;
import org.docx4j.dml.chart.STGrouping;
import org.docx4j.dml.chart.STLegendPos;
import org.docx4j.dml.chart.STMarkerStyle;
import org.docx4j.dml.chart.STOrientation;
import org.docx4j.dml.chart.STRadarStyle;
import org.docx4j.dml.chart.STScatterStyle;
import org.docx4j.dml.chart.STTickLblPos;
import org.docx4j.dml.chart.STTickMark;
import org.docx4j.dml.chart.layer.NoAxIdChart;
import org.docx4j.dml.chartStyle2012.CTChartStyle;
import org.docx4j.dml.chartStyle2012.CTColorStyle;
import org.docx4j.dml.chartStyle2012.CTColorStyleVariation;
import org.docx4j.dml.chartStyle2012.CTStyleEntry;
import org.docx4j.dml.chartStyle2012.CTStyleReference;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.parts.ThemePart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.Commons.Pair;

final public class DMLChartSpace {

    // In
    public static void applyChartSpaceProperties(String chartType, JSONObject chartProperties, ChartWrapper chartWrapper, JSONObject fill) throws Exception {
        final CTPlotArea plotArea = chartWrapper.getPlotArea();
        final CTChartSpace chartSpace = chartWrapper.getChartSpace();

        if (chartProperties == null) {
            chartProperties = new JSONObject();
        }

        String stacking = chartProperties.optString(OCKey.STACKING.value());
        if (stacking == null || stacking.isEmpty()) {
            stacking = getStackingType(chartWrapper);
        }
        if (stacking == null || stacking.isEmpty()) {
            stacking = STGrouping.STANDARD.value();
        }

        if (chartType == null && chartProperties.optBoolean(OCKey.CURVED.value())) {
            final IListSer oldListSer = plotArea.getAreaChartOrArea3DChartOrLineChart().get(0);
            if (!(oldListSer instanceof CTLineChart) && !(oldListSer instanceof CTScatterChart)) {
                chartType = "line2d";
            }
        }

        if (chartType == null && chartProperties.has(OCKey.ROTATION.value())) {
            final IListSer oldListSer = plotArea.getAreaChartOrArea3DChartOrLineChart().get(0);
            if (!(oldListSer instanceof CTLineChart) && !(oldListSer instanceof CTScatterChart)) {
                chartType = "pie";
            }
        }

        if (chartType != null) {
            // indirect call of getThemePart(createIfMissing=true) (Bug 46806)
            chartWrapper.getThemePart();

            List<IListSer> allCharts = plotArea.getAreaChartOrArea3DChartOrLineChart();

            //workaround for Bug 47969
            while (allCharts.size() > 1) {
                allCharts.remove(1);
            }

            final IListSer oldListSer = allCharts.get(0);

            final IListSer newListSer = createChartType(chartType, stacking);

            final Class<? extends ISerContent> type = getNewContentType(newListSer);

            if (newListSer != null) {
                plotArea.getValAxOrCatAxOrDateAx().clear();

                allCharts.set(0, newListSer);

                newListSer.setVaryColors(oldListSer.getVaryColors());

                setRotation(newListSer, chartProperties.optInt(OCKey.ROTATION.value(), getRotation(oldListSer)));
                for (final ISerContent serContent : oldListSer.getSer()) {
                    final ISerContent newSeriesContent;
                    if (serContent.getClass().equals(type)) {
                        newSeriesContent = serContent;
                        newSeriesContent.setTx(null);
                        newSeriesContent.setCat(null);
                        newSeriesContent.setSpPr(null);
                        newSeriesContent.setVal(null);
                    } else {
                        newSeriesContent = newListSer.createSer();
//                        newSeriesContent.setCat(serContent.getCat());
                        newSeriesContent.setExtLst(serContent.getExtLst());
                        newSeriesContent.setIdx(serContent.getIdx());
                        newSeriesContent.setOrder(serContent.getOrder());
//                        newSeriesContent.setSpPr(serContent.getSpPr());
//                        newSeriesContent.setTx(serContent.getTx());
//                        newSeriesContent.setVal(serContent.getVal());
                        newSeriesContent.setDLbls(serContent.getDLbls());

                        if (newSeriesContent instanceof CTDoughnutChart) {
                            handleStandardDonutChart((CTDoughnutChart) newSeriesContent);
                        } else if (newSeriesContent instanceof CTPieChart) {
                            handleStandardRotationChart((CTPieChart) newSeriesContent);
                        }
                    }
                    newListSer.getSer().add(newSeriesContent);
                }
            }
            refreshTitleRotations(chartWrapper);
        }

        final IListSer listSer = plotArea.getAreaChartOrArea3DChartOrLineChart().get(0);
        deleteEmptyContentLists(listSer);

        if (chartProperties.has(OCKey.CHART_STYLE_ID.value())) {
            CTStyle chartStyle = chartSpace.getStyle();
            if (chartStyle == null) {
                chartStyle = new CTStyle();
                chartSpace.setStyle(chartStyle);
            }
            chartStyle.setVal((short) chartProperties.getInt(OCKey.CHART_STYLE_ID.value()));

            /*
             * // FIXME: this should come with the series-operation
             * // delete all shapeproperties, but keep the Line-NoFill-Property
             * for (final ISerContent serContent : listSer.getSer()) {
             * CTShapeProperties sppr = serContent.getSpPr();
             * if (sppr != null ){
             * CTLineProperties ln = sppr.getLn();
             * if (null != ln && null != ln.getNoFill()) {
             * sppr = new CTShapeProperties();
             * sppr.setLn(ln);
             * serContent.setSpPr(sppr);
             * } else {
             * serContent.setSpPr(null);
             * }
             * }
             * final List<CTDPt> dpt = serContent.getDPt();
             * if (dpt != null) {
             * for (final CTDPt point : serContent.getDPt()) {
             * point.setSpPr(null);
             * }
             * }
             * }
             */
        }
        listSer.setStacking(stacking, STGrouping.STANDARD.value());

        if (listSer instanceof CTBarChart) {
            handleOverlap((CTBarChart) listSer, stacking);
        }

        if (chartProperties.has(OCKey.VARY_COLORS.value())) {
            listSer.setVaryColors(new CTBoolean(chartProperties.optBoolean(OCKey.VARY_COLORS.value(), false)));
        }

        if (listSer instanceof CTScatterChart) {
            handleStandardScatterChart((CTScatterChart) listSer);

        }

        if (chartProperties.has(OCKey.ROTATION.value())) {
            setRotation(listSer, chartProperties.optInt(OCKey.ROTATION.value(), 0));
        }
        setRotation3DFromJSON(chartWrapper.getChartSpace().getChart(), chartProperties);


        // reset generic axis numbers
        // for Bug 46441
        if (chartProperties.has(OCKey.STACKING.value()) || chartProperties.has(OCKey.TYPE.value())) {
//            for (IAxisDescription axis : plotArea.getValAxOrCatAxOrDateAx()) {
//                if (axis instanceof CTDateAx) {
//                    ((CTDateAx) axis).setMajorUnit(null);
//                    ((CTDateAx) axis).setMinorUnit(null);
//                    ((CTDateAx) axis).setMajorTimeUnit(null);
//                    ((CTDateAx) axis).setMinorTimeUnit(null);
//                } else if (axis instanceof CTValAx) {
//                    ((CTValAx) axis).setMajorUnit(null);
//                    ((CTValAx) axis).setMinorUnit(null);
//                }
//
//                CTScaling scaling = axis.getScaling();
//                if (null != scaling) {
//                    scaling.setMin(null);
//                    scaling.setMax(null);
//                }
//            }
        }

        if (fill != null) {
            final JSONObject fillHolder = new JSONObject();
            fillHolder.put(OCKey.FILL.value(), fill);

            chartSpace.setSpPr(DMLHelper.applyShapePropertiesFromJson(plotArea.getSpPr(), fillHolder, chartWrapper.getOperationDocument(), chartWrapper.getChartPart(), true));
            if (isShapePropertyFillNoSet(plotArea.getSpPr())) {
                plotArea.setSpPr(new CTShapeProperties());
                plotArea.getSpPr().setNoFill(new CTNoFillProperties());
            }


        }
        chartSpace.setRoundedCorners(new CTBoolean(false));
        
        if (chartProperties.has(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value())) {
            chartSpace.getChart().setPlotVisOnly(new CTBoolean(chartProperties.getBoolean(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value())));   
        }

    }
    // IN
    public static CTChartSpace createChartSpace(JSONObject chartAttrs) {
        final org.docx4j.dml.chart.ObjectFactory chartObjectFactory = Context.getDmlChartObjectFactory();
        final CTChartSpace chartSpace = chartObjectFactory.createCTChartSpace();
        final CTChart chart = chartObjectFactory.createCTChart();
        chartSpace.setChart(chart);
        final CTPlotArea plotArea = chartObjectFactory.createCTPlotArea();
        chart.setPlotArea(plotArea);
        final CTLegend legend = chartObjectFactory.createCTLegend();
        chart.setLegend(legend);
        final CTLegendPos legendPos = chartObjectFactory.createCTLegendPos();
        legend.setLegendPos(legendPos);
        legendPos.setVal(STLegendPos.R);
        chart.setPlotVisOnly(new CTBoolean(true));
        chart.setAutoTitleDeleted(new CTBoolean(false));
        final IListSer listSer = createChartType("bar2d", chartAttrs.optString(OCKey.STACKING.value()));
        plotArea.getAreaChartOrArea3DChartOrLineChart().add(listSer);

        return chartSpace;
    }

    // Out
    public static void createChartSpaceOperations(JSONArray operationsArray, List<Integer> position, ChartWrapper chartWrapper) throws Exception {

        final CTChartSpace chartSpace = chartWrapper.getChartSpace();

        // MSO2012 chartStyles can be zero...
        final CTChartStyle chartStyle = chartWrapper.getChartStyle();
        // final CTColorStyle chartColorStyle = chartPart.getChartColorStyle();
        final ThemePart themePart = chartWrapper.getThemePart();

        final CTChart chart = chartSpace.getChart();
        if (chart == null) {
            return;
        }

        final CTPlotArea plotArea = chart.getPlotArea();
        List<Long> zAxes = new ArrayList<Long>();
        if (plotArea != null && !plotArea.getAreaChartOrArea3DChartOrLineChart().isEmpty()) {
            final List<IListSer> chartList = plotArea.getAreaChartOrArea3DChartOrLineChart();

            Boolean varyColors = null;
            boolean hasRotation = false;

            final JSONObject insertDrawingOperation = operationsArray.getJSONObject(operationsArray.length() - 1);
            boolean hasErrorBars = false;
            int seriesIndex = 0;

            // TODO: what if there is more than one chart in this list ?
            for (final IListSer chartSer : chartList) {

                varyColors = varyColors == null ? getBoolean(chartSer.getVaryColors(), null) : varyColors;
                hasRotation = hasRotation || chartSer instanceof RotationChart;

                String chartType = getSeriesType(chartSer);

                Long axisXIndex = null;
                Long axisYIndex = null;
                Long axisZIndex = null;
                if (chartSer.getAxId() != null) {
                    final List<IAxisDescription> axes = plotArea.getValAxOrCatAxOrDateAx();
                    for (IAxisDescription axis : axes) {
                        if (chartSer.getAxId().contains(axis.getAxId())) {
                            if (axis instanceof CTSerAx) {
                                axisZIndex = axis.getAxId().getVal();
                            } else if (axis.getAxPos().getVal().equals(STAxPos.B) || axis.getAxPos().getVal().equals(STAxPos.T)) {
                                axisXIndex = axis.getAxId().getVal();
                            } else if (axis.getAxPos().getVal().equals(STAxPos.L) || axis.getAxPos().getVal().equals(STAxPos.R)) {
                                axisYIndex = axis.getAxId().getVal();
                            }
                        }
                    }
                }
                // we are taking only the first chart ...
                for (final ISerContent series : chartSer.getSer()) {

                    if (!hasErrorBars) {
                        if (series instanceof ISerErrorBarListContent) {
                            hasErrorBars = !((ISerErrorBarListContent)series).getErrBars().isEmpty();
                        } else if (series instanceof ISerSingleErrorBarContent) {
                            hasErrorBars = ((ISerSingleErrorBarContent)series).getErrBars() != null;
                        }
                    }


                    // title
                    final JSONObject seriesJSON = new JSONObject();

                    seriesJSON.put(OCKey.AXIS_X_INDEX.value(), axisXIndex);
                    seriesJSON.put(OCKey.AXIS_Y_INDEX.value(), axisYIndex);

                    if (axisZIndex != null) {
                        seriesJSON.put(OCKey.AXIS_Z_INDEX.value(), axisZIndex);
                        zAxes.add(axisZIndex);
                    }
                    final CTSerTx tx = series.getTx();
                    Object title = null;
                    if (tx != null) {
                        if (tx.getStrRef() != null) {
                            title = tx.getStrRef().getF();
                        } else {
                            title = new JSONArray();
                            ((JSONArray) title).put(tx.getV());
                        }
                    }
                    if (title != null) {
                        seriesJSON.put(OCKey.TITLE.value(), title);
                    }

                    // names
                    Object names = null;
                    final CTAxDataSource cat = series.getCat();
                    if (cat != null) {
                        if (cat.getStrRef() != null) {
                            names = cat.getStrRef().getF();
                        } else if (cat.getStrLit() != null) {
                            final CTStrData strLit = cat.getStrLit();
                            final List<CTStrVal> strLitPtList = strLit.getPt();
                            if (strLitPtList != null && !strLitPtList.isEmpty()) {
                                names = new JSONArray();
                                for (final CTStrVal strLitPtEntry : strLitPtList) {
                                    if (strLitPtEntry != null) {
                                        ((JSONArray) names).put(strLitPtEntry.getV());
                                    } else {
                                        ((JSONArray) names).put((String) null);
                                    }
                                }
                            }
                        } else if (cat.getNumRef() != null) {
                            names = cat.getNumRef().getF();
                        } else if (cat.getNumLit() != null) {
                            final CTNumData numLit = cat.getNumLit();
                            final List<CTNumVal> numLitPtList = numLit.getPt();
                            if (numLitPtList != null && !numLitPtList.isEmpty()) {
                                JSONArray array = new JSONArray();
                                for (final CTNumVal numLitPtEntry : numLitPtList) {
                                    if (numLitPtEntry != null) {
                                        String v = numLitPtEntry.getV();
                                        try {
                                            array.put(Double.parseDouble(v));
                                        } catch (NumberFormatException e) {
                                            array.put(v);
                                        }
                                    } else {
                                        array.put((String) null);
                                    }
                                }
                                names = array;
                            }
                        }
                        else if (cat.getMultiLvlStrRef() != null) {
                            names = cat.getMultiLvlStrRef().getF();
                        }
                    }
                    if (names != null) {
                        seriesJSON.put(OCKey.NAMES.value(), names);
                    }

                    // values
                    final CTNumDataSource numDataSource = series.getVal();
                    final Object values = getData(numDataSource);

                    if (values != null) {
                        seriesJSON.put(OCKey.VALUES.value(), values);
                    }

                    if (series instanceof CTBubbleSer) {
                        final CTBubbleSer serBB = (CTBubbleSer) series;

                        final CTNumDataSource bubblesizes = serBB.getBubbleSize();
                        if (bubblesizes != null) {
                            final Object bubbles = getData(bubblesizes);
                            if (bubbles != null) {
                                seriesJSON.put(OCKey.BUBBLES.value(), bubbles);
                            }
                        }
                        if (getBoolean(serBB.getBubble3D(), false)) {
                            seriesJSON.put(OCKey.BUBBLE3D.value(), true);
                        }
                    }

                    seriesJSON.put(OCKey.TYPE.value(), chartType);
                    if (chartType.contains("3d")) {
                        seriesJSON.put(OCKey.CHART3D.value(), chartType.contains("3d"));
                    }

                    addDataPointsToJSON(seriesJSON, chartWrapper, series, chartSer, chartStyle, themePart);

                    addDataLabelToJSON(seriesJSON, series);

                    CTDLbls dLbl = series.getDLbls();
                    if (dLbl != null && dLbl.getNumFmt() != null && !dLbl.getNumFmt().isSourceLinked()) {
                        seriesJSON.put(OCKey.FORMAT.value(), dLbl.getNumFmt().getFormatCode());
                    }

                    final JSONObject attrsJSON = Commons.surroundJSONObject(OCKey.SERIES.value(), seriesJSON);

                    addShapeFillLineOperationsToJSON(seriesJSON, attrsJSON, chartWrapper, series.getSpPr(), chartStyle, themePart);
                    addMarkerOperationsToJSON(attrsJSON, chartWrapper, chartSer, series.getMarker(), chartStyle);

                    if (isCurved(series, false)) {
                        seriesJSON.put(OCKey.CURVED.value(), true);
                    }

                    // TODO: chartColorStyle needs to be supported... line and fill styles should be created by following call (text, txtBody has to be added)
                    // DMLHelper.createJsonFromShapeProperties(attrs, ser.getSpPr(), theme, chartStyle!=null?chartStyle.getChartArea():null, false);
                    if (seriesJSON.length() > 0) {
                        final JSONObject insertChartDataSeriesOperation = new JSONObject(4);
                        insertChartDataSeriesOperation.put(OCKey.NAME.value(), OCValue.INSERT_CHART_SERIES.value());
                        insertChartDataSeriesOperation.put(OCKey.START.value(), position);
                        insertChartDataSeriesOperation.put(OCKey.SERIES.value(), seriesIndex++);
                        insertChartDataSeriesOperation.put(OCKey.ATTRS.value(), attrsJSON);
                        operationsArray.put(insertChartDataSeriesOperation);
                    }

                }
            }


            final JSONObject attrsJSON = insertDrawingOperation.getJSONObject(OCKey.ATTRS.value());
            final JSONObject chartPropertiesJSON = attrsJSON.getJSONObject(OCKey.CHART.value());

            if (hasRotation) {
                final RotationChart rotationChart = (RotationChart)chartList.get(0);
                final int rotation = rotationChart.getFirstSliceAng() != null ? rotationChart.getFirstSliceAng().getVal() : 0;
                chartPropertiesJSON.put(OCKey.ROTATION.value(), rotation);
            }

            if (hasErrorBars) {
                chartPropertiesJSON.put(OCKey.ERROR_BAR.value(), true);
            }

            CTView3D view3D = chart.getView3D();
            if (view3D != null) {
                if (view3D.getRotX() != null) {
                    chartPropertiesJSON.put(OCKey.ROTATION_X.value(), view3D.getRotX().getVal());
                }
                if (view3D.getRotY() != null) {
                    chartPropertiesJSON.put(OCKey.ROTATION_Y.value(), view3D.getRotY().getVal());
                }
            }

            if (varyColors != null) {
                chartPropertiesJSON.put(OCKey.VARY_COLORS.value(), varyColors);
            }
            
            // Show data in hidden rows and columns
            chartPropertiesJSON.put(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value(), chart.getPlotVisOnly().isVal());

            final CTShapeProperties shapePr = chartSpace.getSpPr();
            if (shapePr != null) {
                final CTStyleEntry styleRef = chartStyle == null ? null : chartStyle.getDataPointMarker();
                final CTStyleReference fillRef = styleRef == null ? null : styleRef.getFillRef();
                DMLHelper.createJsonFromFillProperties(attrsJSON, shapePr, themePart, fillRef, chartWrapper.getChartPart());
            }

            addAxisAndGridOperations(chartWrapper, operationsArray, position, zAxes);

            // Add main Title
            addAxisTitleOperation(chart.getTitle(), null, operationsArray, position);

            addLegendOperation(chart, operationsArray, position);
        }
    }

    private static void addDataPointsToJSON(JSONObject seriesJSON, ChartWrapper chartWrapper, ISerContent series, IListSer chartSer, CTChartStyle chartStyle, ThemePart themePart) throws JSONException {
        JSONArray dataPointsJSON = new JSONArray();

        if (series.getDPt() != null) {
            for (CTDPt dpt : series.getDPt()) {
                final JSONObject attrs = new JSONObject();
                addShapeFillLineOperationsToJSON(attrs, attrs, chartWrapper, dpt.getSpPr(), chartStyle, themePart);
                addMarkerOperationsToJSON(attrs, chartWrapper, chartSer, dpt.getMarker(), chartStyle);
                attrs.put(OCKey.INDEX.value(), Long.valueOf(dpt.getIdx().getVal()).intValue());
                dataPointsJSON.put(attrs);
            }
            if (!dataPointsJSON.isEmpty()) {
                seriesJSON.put(OCKey.DATA_POINTS.value(), dataPointsJSON);
            }
        }
    }

    private static void addDataLabelToJSON(JSONObject seriesJSON, ISerContent serContent) throws JSONException {
        CTDLbls lbls = serContent.getDLbls();
        if (lbls != null) {
            JSONArray showOptions = new JSONArray();
            addIfTrue(showOptions, lbls.getShowVal(), "value");
            addIfTrue(showOptions, lbls.getShowSerName(), "series");
            addIfTrue(showOptions, lbls.getShowBubbleSize(), "size");
            addIfTrue(showOptions, lbls.getShowCatName(), "cat");
            addIfTrue(showOptions, lbls.getShowLegendKey(), "legend");
            addIfTrue(showOptions, lbls.getShowPercent(), "percent");
            addIfTrue(showOptions, lbls.getShowLeaderLines(), "leader");

            JSONObject dataLabelJSON = new JSONObject();
            if (showOptions.length() > 0) {
                dataLabelJSON.put(OCKey.DATA_LABEL_TEXT.value(), showOptions);    
            }
            
            String defaultPosition = getDataLabelPosition(lbls.getDLblPos());
            if (defaultPosition != null) {
                dataLabelJSON.put(OCKey.DATA_LABEL_POS.value(), defaultPosition);
            }
            if (lbls.getDLbl() != null && !lbls.getDLbl().isEmpty()) {
                JSONArray dataLabels = new JSONArray();
                for (CTDLbl dataLabel : lbls.getDLbl()) {
                    String position = getDataLabelPosition(dataLabel.getDLblPos());
                    if (position != null && dataLabel.getIdx() != null) {
                        JSONObject labelJSON = new JSONObject();
                        labelJSON.put(OCKey.DATA_LABEL_POS.value(), position);

                        dataLabels.add((int)dataLabel.getIdx().getVal(), labelJSON);
                    }
                }
                dataLabelJSON.put(OCKey.DATA_LABELS.value(), dataLabels);
            }
            
            seriesJSON.put(OCKey.DATA_LABEL.value(), dataLabelJSON);
        }
    }
    
    private static String getDataLabelPosition(CTDLblPos dataLabelPos) {
        String position = null;
        if (dataLabelPos != null && dataLabelPos.getVal() != null) {
            STDLblPos pos = dataLabelPos.getVal();
            
            switch(pos) {
                case B:
                    position = OCKey.DATA_LABEL_POS_BOTTOM.value();
                    break;
                case BEST_FIT:
                    position = OCKey.DATA_LABEL_POS_BEST_FIT.value();
                    break;
                case CTR:
                    position = OCKey.DATA_LABEL_POS_CENTER.value();
                    break;
                case IN_BASE:
                    position = OCKey.DATA_LABEL_POS_INSIDE_BASE.value();
                    break;
                case IN_END:
                    position = OCKey.DATA_LABEL_POS_INSIDE.value();
                    break;
                case L:
                    position = OCKey.DATA_LABEL_POS_LEFT.value();
                    break;
                case OUT_END:
                    position = OCKey.DATA_LABEL_POS_OUTSIDE.value();
                    break;
                case R:
                    position = OCKey.DATA_LABEL_POS_RIGHT.value();
                    break;
                case T:
                    position = OCKey.DATA_LABEL_POS_ABOVE.value();
                    break;   
            }
        }
        return position;
    }
    
    private static STDLblPos getDataLabelPosition(String dataLabelPos) {
        STDLblPos position = null;
        if (dataLabelPos != null) {
           
            OCKey pos = OCKey.fromValue(dataLabelPos);
            
            switch(pos) {
                case DATA_LABEL_POS_BOTTOM:
                    position = STDLblPos.B;
                    break;
                case DATA_LABEL_POS_BEST_FIT:
                    position = STDLblPos.BEST_FIT;
                    break;
                case DATA_LABEL_POS_CENTER:
                    position = STDLblPos.CTR;
                    break;
                case DATA_LABEL_POS_INSIDE_BASE:
                    position = STDLblPos.IN_BASE;
                    break;
                case DATA_LABEL_POS_INSIDE:
                    position = STDLblPos.IN_END;
                    break;
                case DATA_LABEL_POS_LEFT:
                    position = STDLblPos.L;
                    break;
                case DATA_LABEL_POS_OUTSIDE:
                    position = STDLblPos.OUT_END;
                    break;
                case DATA_LABEL_POS_RIGHT:
                    position = STDLblPos.R;
                    break;
                case DATA_LABEL_POS_ABOVE:
                    position = STDLblPos.T;
                    break; 
                default:
                    // Do nothing
            }
        }
        return position;
    }

    private static void addShapeFillLineOperationsToJSON(JSONObject excludeImportAttributeJSON, JSONObject attrsJSON, ChartWrapper chartWrapper, CTShapeProperties shapeProps, CTChartStyle chartStyle, ThemePart themePart) throws JSONException {
        DMLHelper.createJsonFromShapeProperties(attrsJSON, shapeProps, themePart, chartStyle.getDataPoint(), chartWrapper.getChartPart(), false, true);

        if (shapeProps == null || shapeProps.getLn() == null) {
            excludeImportAttributeJSON.put(OCKey.NO_LINE.value(), true);
        }
        if (isShapePropertyFillNoSet(shapeProps)) {
            excludeImportAttributeJSON.put(OCKey.NO_FILL.value(), true);
        }
    }

    private static boolean isShapePropertyFillNoSet(CTShapeProperties shapeProperties) {
        return shapeProperties == null || (shapeProperties.getBlipFill() == null && shapeProperties.getGradFill() == null && shapeProperties.getGrpFill() == null && shapeProperties.getNoFill() == null && shapeProperties.getPattFill() == null && shapeProperties.getSolidFill() == null);
    }

    private static void addMarkerOperationsToJSON(JSONObject attrsJSON, ChartWrapper chartWrapper, IListSer chart, CTMarker marker, CTChartStyle chartStyle) throws FilterException, JSONException {
        boolean hasMarker = false;
        if (marker != null) {
            final CTShapeProperties shapePr = marker.getSpPr();
            if (shapePr != null) {
                final CTStyleEntry styleRef = chartStyle.getDataPointMarker();
                final CTStyleReference fillRef = styleRef != null ? styleRef.getFillRef() : null;
                DMLHelper.createJsonFromMarkerFillProperties(attrsJSON, shapePr, chartWrapper.getThemePart(), fillRef, chartWrapper.getChartPart());

                final CTStyleReference lineRef = styleRef != null ? styleRef.getLnRef() : null;
                DMLHelper.createJsonFromMarkerLineProperties(attrsJSON, chartWrapper.getThemePart(), lineRef, shapePr.getLn(), chartWrapper.getChartPart());

            } else if (marker.getSymbol() == null || marker.getSymbol().getVal() != STMarkerStyle.NONE) {
                hasMarker = true;
            }
        } else if (chart instanceof CTLineChart && getBoolean(((CTLineChart) chart).getMarker(), false)) {
            hasMarker = true;
        } else if (chart instanceof CTScatterChart && ((CTScatterChart) chart).getScatterStyle().getVal().value().toLowerCase().contains("marker")) {
            hasMarker = true;
        }

        if (hasMarker) {
            attrsJSON.put(OCKey.MARKER_FILL.value(), new JSONObject(new JSONTokener("{ type:'solid', onlyMarker: true }")));
        }
    }

    // Out
    public static JSONObject createJsonFromChartColor(CTColorStyle color) throws JSONException {

        JSONObject jsonChartColor = null;
        if (color != null) {
            jsonChartColor = new JSONObject();
            jsonChartColor.put(OCKey.ID.value(), color.getId());
            jsonChartColor.put(OCKey.METH.value(), color.getMeth());
            final List<Object> schemeClrList = color.getEGColorChoice();
            ;
            if (schemeClrList != null) {
                final JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < schemeClrList.size(); i++) {
                    final ColorChoiceListEntry listEntry = new ColorChoiceListEntry(schemeClrList, i);
                    if (listEntry.getSchemeClr() != null) {
                        final JSONObject jsonColor = DMLHelper.createJsonColorFromSolidColorFillProperties(listEntry);
                        if (jsonColor != null) {
                            jsonArray.put(jsonColor);
                        }
                    }
                }
                jsonChartColor.put("schemeClr", jsonArray);
            }
            final List<CTColorStyleVariation> variationList = color.getVariation();
            if (variationList != null) {

                /*
                 * TODO: variationList needs to be implemented...
                 *
                 * final JSONArray jsonArray = new JSONArray();
                 * for(final CTColorStyleVariation schemeClr:variationList) {
                 * final JSONObject resColor = new JSONObject();
                 * createJsonFromSchemeClr(resColor, schemeClr);
                 * if (resColor != null){
                 * jsonArray.put(resColor);
                 * }
                 * }
                 * if (!jsonArray.isEmpty()){
                 * jsonChartColor.put("variation", jsonArray);
                 * }
                 */
            }
        }
        return jsonChartColor != null && !jsonChartColor.isEmpty() ? jsonChartColor : null;
    }

    // In
    public static void deleteChartSpace(ChartWrapper chart, int series) {
        final Pair<Integer, Pair<ISerContent, IListSer>> local = globalToLocal(chart.getPlotArea(), series);
        if(local == null) {
            return;
        }
        final IListSer listSer = local.getSecond().getSecond();
        listSer.getSer().remove(local.getSecond().getFirst());

        // fix for Bug 53212 (delete all series)
        if (listSer.getSer().isEmpty() && chart.getPlotArea().getAreaChartOrArea3DChartOrLineChart().size() > 1) {
            chart.getPlotArea().getAreaChartOrArea3DChartOrLineChart().remove(listSer);
        }
        updateIndexAndOrder(chart.getPlotArea());
    }

    // In/Out
    public static void getChartType(ChartWrapper chart, HashMap<String, Object> map) {
        final CTPlotArea plotArea = chart.getPlotArea();
        if (plotArea == null) {
            return;
        }
        final List<IListSer> chartList = plotArea.getAreaChartOrArea3DChartOrLineChart();
        if (chartList.isEmpty()) {
            return;
        }

        final IListSer listSer = chartList.get(0);
        String chartType = getSeriesType(listSer);
        if (chartType != null) {
            map.put(OCKey.CHART_TYPE.value(), chartType);
        }
        final String chartGroup = listSer.getStacking();
        if (chartGroup != null) {
            map.put(OCKey.CHART_GROUP.value(), chartGroup);
        }
    }

    // In
    public static void insertChartSpace(ChartWrapper chartWrapper, int series, JSONObject attrs) throws Exception {

        JSONObject attrSeries = attrs.optJSONObject(OCKey.SERIES.value());
        String type = opToOOXMLType(attrSeries.optString(OCKey.TYPE.value()));

        List<IListSer> allCharts = chartWrapper.getPlotArea().getAreaChartOrArea3DChartOrLineChart();
        boolean newSeries = false;
        IListSer listSer =  allCharts.get(allCharts.size() - 1);
        ISerContent serContent;
        if (series > 0) {
            final IListSer newListSer = createChartType(type, null);
            for (IListSer chart : allCharts) {
                if (chart.getClass().equals(newListSer.getClass()) && chart.getAxId() != null) {
                    if (chart.getAxId() != null && (!chart.getAxId().contains(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_X_INDEX.value()))) || !chart.getAxId().contains(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_Y_INDEX.value()))))) {
                        newSeries = true;
                        break;
                    }
                }
            }
            if (newSeries) {
                chartWrapper.getPlotArea().getAreaChartOrArea3DChartOrLineChart().add(newListSer);
                listSer = newListSer;
            }
            serContent = listSer.createSer();
            listSer.getSer().add(serContent);

        } else {
            applyChartSpaceProperties(type, attrs.optJSONObject(OCKey.CHART.value()), chartWrapper, null);
            newSeries = true;
            listSer =  allCharts.get(allCharts.size() - 1);
            if (listSer.getSer().isEmpty()) {
                serContent = listSer.createSer();
                listSer.getSer().add(serContent);
            } else {
                serContent = listSer.getSer().get(0);
                listSer.getSer().clear();
                listSer.getSer().add(serContent);
            }
        }

        if (newSeries) {
            if (!(listSer instanceof NoAxIdChart)) {
                List<CTUnsignedInt> axId = listSer.getAxId();
                axId.clear();
                if (type.startsWith("bar")) {
                    axId.add(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_Y_INDEX.value())));
                    axId.add(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_X_INDEX.value())));
                } else {
                    axId.add(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_X_INDEX.value())));
                    axId.add(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_Y_INDEX.value())));
                }

                if (attrSeries.has(OCKey.AXIS_Z_INDEX.value())) {
                    axId.add(new CTUnsignedInt(attrSeries.getInt(OCKey.AXIS_Z_INDEX.value())));
                }
            }
        }

        disableInvertIfNegative(serContent);
        setCurved(serContent, attrSeries.optBoolean(OCKey.CURVED.value()));

        JSONObject noneType = new JSONObject();
        noneType.putSafe(OCKey.TYPE.value() , "none");

        if (!attrs.has(OCKey.FILL.value())) {
            // no fill -> no marker info -> visible marker, but we dont want that!
            attrs.put(OCKey.FILL.value(), noneType);
        }
        if (!attrs.has(OCKey.MARKER_FILL.value())) {
            attrs.put(OCKey.MARKER_FILL.value(), noneType);
        }
        if (!attrs.has(OCKey.MARKER_BORDER.value())) {
            attrs.put(OCKey.MARKER_BORDER.value(), noneType);
        }

        setDataseriesAttrs(chartWrapper, serContent, attrs);

        updateIndexAndOrder(chartWrapper.getPlotArea());
    }

    // In
    public static void setAxisAttributes(ChartWrapper chart, long axisId, long crossAxId, String axPos, boolean zAxis, JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {

        final IAxisDescription ax = getAxis(chart.getPlotArea(), axisId, crossAxId, axPos, zAxis);
        if (ax == null) {
            return;
        }
        final CTShapeProperties shape = DMLHelper.applyShapePropertiesFromJson(ax.getSpPr(), attrs, chart.getOperationDocument(), chart.getChartPart(), true);
        ax.setSpPr(shape);
        CTTickMark tickmark = ax.getMajorTickMark();
        if (tickmark == null) {
            tickmark = new CTTickMark();
            ax.setMajorTickMark(tickmark);
        }
        if (shape.getLn() != null && shape.getLn().getNoFill() != null) {
            tickmark.setVal(STTickMark.NONE);
        } else {
            tickmark.setVal(STTickMark.OUT);
        }

        final JSONObject axis = attrs.optJSONObject(OCKey.AXIS.value());
        if (axis != null) {
            CTTickLblPos ticklblPos = ax.getTickLblPos();
            if (ticklblPos == null) {
                ticklblPos = new CTTickLblPos();
                ax.setTickLblPos(ticklblPos);
            }
            final boolean label = axis.optBoolean(OCKey.LABEL.value(), false);

            if (label) {
                ticklblPos.setVal(STTickLblPos.NEXT_TO);
                ax.setTxPr(setTextBodyFromAttrs(ax.getTxPr(), attrs));
            } else {
                ticklblPos.setVal(STTickLblPos.NONE);
            }
        }
    }

    // In
    public static void setCharacterFromAttrs(CTTextParagraph p, JSONObject attrs) throws JSONException {

        final JSONObject character = attrs.optJSONObject(OCKey.CHARACTER.value());

        if (character != null) {
            final CTTextParagraphProperties prop = p.getPPr(true);
            prop.setDefRPr(setCharacterFromAttrs(prop.getDefRPr(false), attrs));
        }
    }

    // In
    public static void setChartGridlineAttributes(ChartWrapper chart, long axisId, JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {

        final IAxisDescription ax = getAxis(chart.getPlotArea(), axisId);
        if (ax == null) {
            return;
        }

        CTChartLines grids = ax.getMajorGridlines();
        if (grids == null) {
            grids = new CTChartLines();
            ax.setMajorGridlines(grids);
        }
        grids.setSpPr(DMLHelper.applyShapePropertiesFromJson(grids.getSpPr(), attrs, chart.getOperationDocument(), chart.getChartPart(), true));
    }

    // In
    public static void setChartTitleAttributes(ChartWrapper chart, long axisId, JSONObject attrs) throws JSONException {
        final IAxisDescription ax = getAxis(chart.getPlotArea(), axisId);
        if (ax == null) {
            return;
        }

        CTTitle title = ax.getTitle();
        if (title == null) {
            title = new CTTitle();
            ax.setTitle(title);
        }
        setTitleFromAttrs(title, attrs);
        refreshTitleRotations(chart);
    }

    // In
    public static void setDataSeriesAttributes(ChartWrapper chart, int seriesId, JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {
        final Pair<Integer, Pair<ISerContent, IListSer>> series = globalToLocal(chart.getPlotArea(), seriesId);
        if(series != null) {
            disableInvertIfNegative(series.getSecond().getFirst());
            setDataseriesAttrs(chart, series.getSecond().getFirst(), attrs);
        }
    }

    // In
    public static void setLegendFromAttrs(ChartWrapper chart, JSONObject attrs) throws JSONException {
        final CTChart ctChart = chart.getChartSpace().getChart();
        final String pos = attrs.getJSONObject(OCKey.LEGEND.value()).optString(OCKey.POS.value(), "off");
        if (pos.equals("off")) {
            ctChart.setLegend(null);
        } else {
            CTLegend legend = ctChart.getLegend();
            if (legend == null) {
                legend = new CTLegend();
                ctChart.setLegend(legend);
            }

            legend.setTxPr(setTextBodyFromAttrs(legend.getTxPr(), attrs));

            legend.setOverlay(new CTBoolean(false));

            CTLegendPos legendPos = legend.getLegendPos();
            if (legendPos == null) {
                legendPos = new CTLegendPos();
                legend.setLegendPos(legendPos);
            }

            final STLegendPos stpos;
            if (pos.equals("left")) {
                stpos = STLegendPos.L;
            } else if (pos.equals("top")) {
                stpos = STLegendPos.T;
            } else if (pos.equals("right")) {
                stpos = STLegendPos.R;
            } else if (pos.equals("bottom")) {
                stpos = STLegendPos.B;
            } else if (pos.equals("topRight")) {
                stpos = STLegendPos.TR;
            } else {
                Logger.getAnonymousLogger().info("No STLegendPost-enum found for " + pos);
                stpos = STLegendPos.L;
            }
            legendPos.setVal(stpos);
        }
    }

    // In
    public static void setTitleFromAttrs(CTTitle title, JSONObject attrs) throws JSONException {
        handleStandardTypes(title);

        JSONArray links = attrHasTitle(attrs);
        String ref = attrHasRef(attrs);
        if (links == null && ref == null) {

            title.setTx(null);
            title.setTxPr(setTextBodyFromAttrs(title.getTxPr(), attrs));
        } else {

            final CTTx tx = title.getTx();

            final CTTextBody rich = tx.getRich();
            final CTTextBodyProperties body = rich.getBodyPr();
            body.setRot(0);

            final List<Object> paras = rich.getContent();
            paras.clear();

            //delete cell ref, fix for Bug 47761
            tx.setStrRef(null);
            if (ref != null && ref.length() > 0) {

                CTStrRef strRef = new CTStrRef();
                strRef.setF(ref);
                tx.setStrRef(strRef);
                tx.setRich(null);
                return;
            }

            title.setTxPr(null);

            if (links != null && links.length() > 0) {
                for (int i = 0; i < links.length(); i++) {
                    final CTTextParagraph p = Context.getDmlObjectFactory().createCTTextParagraph();
                    setCharacterFromAttrs(p, attrs);
                    paras.add(p);
                    final List<Object> textHolder = p.getContent();
                    textHolder.clear();

                    final String link = links.getString(i);
                    final String[] textes = link.split("\n");
                    for (int j = 0; j < textes.length; j++) {
                        final CTRegularTextRun reg = Context.getDmlObjectFactory().createCTRegularTextRun();
                        reg.setRPr(setCharacterFromAttrs(reg.getRPr(false), attrs));
                        reg.setT(textes[j]);
                        textHolder.add(reg);
                        if (j < textes.length - 1) {
                            final CTTextLineBreak lb = Context.getDmlObjectFactory().createCTTextLineBreak();
                            lb.setRPr(setCharacterFromAttrs(lb.getRPr(false), attrs));
                            textHolder.add(lb);
                        }
                    }
                }
            }
        }
    }

    private static void addIfTrue(JSONArray res, CTBoolean b, String name) {
        if (getBoolean(b, false)) {
            res.put(name);
        }
    }

    private static void addTo(CTRegularTextRun textRun, StringBuffer stringBuffer) {
        stringBuffer.append(textRun.getT());
    }

    private static void addTo(CTTextField textRun, StringBuffer stringBuffer) {
        stringBuffer.append(textRun.getT());
    }

    private static void addTo(StringBuffer stringBuffer) {
        stringBuffer.append("\n");
    }

    private static void addAxisAndGridOperations(ChartWrapper chart, JSONArray operations, List<Integer> position, List<Long> zAxes) throws Exception {
        CTPlotArea plotArea = chart.getPlotArea();
        final List<IAxisDescription> axes = plotArea.getValAxOrCatAxOrDateAx();
        if (axes != null && axes.size() > 0) {
            for (int i = 0; i < axes.size(); i++) {
                final IAxisDescription axis = axes.get(i);

                addAxisOperation(chart, axis, operations, position, zAxes);
                addGridOperation(chart, axis, operations, position);
            }
        }
    }

    private static String attrHasRef(JSONObject attrs) {
        final JSONObject text = attrs.optJSONObject(OCKey.TEXT.value());
        if (text == null) {
            return null;
        }

        final Object olink = text.opt(OCKey.LINK.value());

        if (olink instanceof String) {
            return (String) olink;
        }

        return null;
    }

    private static JSONArray attrHasTitle(JSONObject attrs) {
        final JSONObject text = attrs.optJSONObject(OCKey.TEXT.value());
        if (text == null) {
            return null;
        }

        final Object olink = text.opt(OCKey.LINK.value());

        if (olink == null || !(olink instanceof JSONArray)) {

            if (olink == null || olink.equals(JSONObject.NULL)) {
                return null;
            }
            Logger.getAnonymousLogger().warning("title text has no correct content " + text.opt(OCKey.LINK.value()));
            return null;
        }

        final JSONArray links = (JSONArray) olink;
        if (links.length() > 0) {
            return links;
        }
        return null;
    }

    private static IListSer createBarChartType(String chartType, String stacking, ObjectFactory chartObjectFactory) {
        IListSer listSer = null;
        String barDir = null;
        if (chartType.equals("bar3d")) {
            listSer = chartObjectFactory.createCTBar3DChart();
            barDir = "bar";
        } else if (chartType.equals("column2d")) {
            listSer = chartObjectFactory.createCTBarChart();
            barDir = "col";
        } else if (chartType.equals("column3d")) {
            listSer = chartObjectFactory.createCTBar3DChart();
            barDir = "col";
        } else {
            listSer = chartObjectFactory.createCTBarChart();
            barDir = "bar";
        }
        listSer.setBarDirection(barDir);

        if (listSer instanceof CTBarChart) {
            handleOverlap((CTBarChart) listSer, stacking);
        }

        if (listSer instanceof CTBar3DChart) {
            handleOverlap((CTBar3DChart) listSer);
        }

        return listSer;
    }

    private static CTCatAx createCatAx() {
        final org.docx4j.dml.chart.ObjectFactory chartObjectFactory = Context.getDmlChartObjectFactory();
        final CTCatAx catAx = chartObjectFactory.createCTCatAx();
        final CTScaling scaling = chartObjectFactory.createCTScaling();
        final CTOrientation orientation = chartObjectFactory.createCTOrientation();
        orientation.setVal(STOrientation.MIN_MAX);
        scaling.setOrientation(orientation);
        catAx.setScaling(scaling);
        catAx.setNumFmt(null);
        catAx.setSpPr(getHiddenShapeProperties());

        catAx.setTickLblPos(new CTTickLblPos());
        catAx.getTickLblPos().setVal(STTickLblPos.NONE);

        return catAx;
    }

    private static IListSer createChartType(String type, String stacking) {

        IListSer listSer = null;
        final ObjectFactory chartObjectFactory = Context.getDmlChartObjectFactory();
        String chartType = type != null ? type : "bar2d";

        chartType = opToOOXMLType(chartType);

        if (chartType.equals("bar3d")) {
            listSer = createBarChartType(chartType, stacking, chartObjectFactory);
        } else if (chartType.equals("column2d")) {
            listSer = createBarChartType(chartType, stacking, chartObjectFactory);
        } else if (chartType.equals("column3d")) {
            listSer = createBarChartType(chartType, stacking, chartObjectFactory);
        } else if (chartType.equals("line2d")) {
            listSer = chartObjectFactory.createCTLineChart();
        } else if (chartType.equals("line3d")) {
            listSer = chartObjectFactory.createCTLine3DChart();
        } else if (chartType.equals("area2d")) {
            listSer = chartObjectFactory.createCTAreaChart();
        } else if (chartType.equals("area3d")) {
            listSer = chartObjectFactory.createCTArea3DChart();
        } else if (chartType.equals("radar2d")) {
            CTRadarChart radar = chartObjectFactory.createCTRadarChart();
            radar.setRadarStyle(new CTRadarStyle());
            radar.getRadarStyle().setVal(STRadarStyle.STANDARD);
            listSer = radar;
        } else if (chartType.equals("scatter2d")) {
            final CTScatterChart scatter = chartObjectFactory.createCTScatterChart();
            handleStandardScatterChart(scatter);
            listSer = scatter;
        } else if (chartType.equals("bubble2d")) {
            listSer = chartObjectFactory.createCTBubbleChart();
        } else if (chartType.equals("pie2d")) {
            final CTPieChart pie = chartObjectFactory.createCTPieChart();
            handleStandardRotationChart(pie);
            listSer = pie;
        } else if (chartType.equals("donut2d")) {
            final CTDoughnutChart donut = chartObjectFactory.createCTDoughnutChart();
            handleStandardDonutChart(donut);
            listSer = donut;
        } else if (chartType.equals("pie3d")) {
            final CTPie3DChart pie = chartObjectFactory.createCTPie3DChart();
            listSer = pie;
        } else { // bar2d -> default
            listSer = createBarChartType(chartType, stacking, chartObjectFactory);
        }
        listSer.setStacking(STGrouping.STANDARD.value(), STGrouping.STANDARD.value());

        return listSer;
    }

    private static void addLegendOperation(CTChart chart, JSONArray operations, List<Integer> position) throws JSONException {
        final JSONObject ljset = new JSONObject(2);
        final JSONObject lj = new JSONObject(3);
        lj.put(OCKey.POS.value(), "off");

        final CTLegend legend = chart.getLegend();
        if (legend != null) {
            //right is default, if legend exists
            lj.put(OCKey.POS.value(), "right");
            if (legend.getLegendPos() != null && legend.getLegendPos().getVal() != null) {

                final String pos;
                switch (legend.getLegendPos().getVal()) {
                    case R:
                        pos = "right";
                        break;
                    case T:
                        pos = "top";
                        break;
                    case B:
                        pos = "bottom";
                        break;
                    case TR:
                        pos = "topRight";
                        break;
                    default:
                        pos = "left";
                        break;
                }

                lj.put(OCKey.POS.value(), pos);

                final JSONObject character = DMLHelper.createJsonFromProperties(legend.getTxPr());
                if (character != null) {
                    ljset.put(OCKey.CHARACTER.value(), character);
                }
            }
        }
        ljset.put(OCKey.LEGEND.value(), lj);

        final JSONObject op = new JSONObject(3);
        op.put(OCKey.NAME.value(), OCValue.CHANGE_CHART_LEGEND.value());
        op.put(OCKey.START.value(), position);
        op.put(OCKey.ATTRS.value(), ljset);
        operations.put(op);

    }

    // TODO: creator for pie3d needs to be implemented...

    private static CTSerAx createSerAx() {
        final org.docx4j.dml.chart.ObjectFactory chartObjectFactory = Context.getDmlChartObjectFactory();
        final CTSerAx serAx = chartObjectFactory.createCTSerAx();
        final CTScaling scaling = chartObjectFactory.createCTScaling();
        final CTOrientation orientation = chartObjectFactory.createCTOrientation();
        orientation.setVal(STOrientation.MIN_MAX);
        scaling.setOrientation(orientation);
        serAx.setScaling(scaling);
        serAx.setNumFmt(null);
        serAx.setSpPr(getHiddenShapeProperties());
        serAx.setTickLblPos(new CTTickLblPos());
        serAx.getTickLblPos().setVal(STTickLblPos.NONE);

        return serAx;
    }

    private static CTValAx createValAx() {
        final org.docx4j.dml.chart.ObjectFactory chartObjectFactory = Context.getDmlChartObjectFactory();
        final CTValAx valAx = chartObjectFactory.createCTValAx();
        final CTScaling scaling = chartObjectFactory.createCTScaling();
        final CTOrientation orientation = chartObjectFactory.createCTOrientation();
        orientation.setVal(STOrientation.MIN_MAX);
        scaling.setOrientation(orientation);
        valAx.setScaling(scaling);
        valAx.setNumFmt(null);
        valAx.setSpPr(getHiddenShapeProperties());

        valAx.setTickLblPos(new CTTickLblPos());
        valAx.getTickLblPos().setVal(STTickLblPos.NONE);

        return valAx;
    }

    private static void deleteEmptyContentLists(IListSer listSer) {
        for (final ISerContent serContent : listSer.getSer()) {
            if (hasEmptyContents(serContent.getCat())) {
                serContent.setCat(null);
            }
            if (hasEmptyContents(serContent.getTx())) {
                serContent.setTx(null);
            }
            if (serContent instanceof CTBubbleSer) {
                final CTBubbleSer serBub = ((CTBubbleSer) serContent);
                if (hasEmptyContents(serBub.getBubbleSize())) {
                    serBub.setBubbleSize(null);
                }
            }
        }
    }

    private static void disableInvertIfNegative(ISerContent serContent) {
        if (serContent instanceof CTBarSer) {
            ((CTBarSer) serContent).setInvertIfNegative(new CTBoolean(false));
        } else if (serContent instanceof CTBubbleSer) {
            ((CTBubbleSer) serContent).setInvertIfNegative(new CTBoolean(false));
        }
        if (serContent.getDPt() != null) {
            for (CTDPt dpt : serContent.getDPt()) {
                dpt.setInvertIfNegative(new CTBoolean(false));
            }
        }
    }

    private static final void addAxisOperation(ChartWrapper chart, IAxisDescription axis, JSONArray operations, List<Integer> startPosition, List<Long> zAxes) throws Exception {

        final JSONObject axisOperation = new JSONObject();

        /*
         * disabled for Bug 46441, if we want to support scaling,w e also must support major & minor units!
         * final CTScaling scaling = axis.getScaling();
         * if(scaling!=null) {
         * if (scaling.getMin()!=null) {
         * axis.put("min", scaling.getMin().getVal());
         * }
         * if(scaling.getMax()!=null) {
         * axis.put("max", scaling.getMax().getVal());
         * }
         * }
         */
        JSONObject character = null;
        final JSONObject axisOp = new JSONObject();
        final CTBoolean del = axis.getDelete();
        if (getBoolean(del, false)) {
            final JSONObject line = new JSONObject(1);
            line.put(OCKey.TYPE.value(), "none");
            axisOp.put(OCKey.LINE.value(), line);
        } else {
            final CTShapeProperties shape = axis.getSpPr();
            if (shape != null) {
                DMLHelper.createJsonFromShapeProperties(axisOp, shape, chart.getThemePart(), chart.getChartStyle().getSeriesAxis(), chart.getChartPart(), false, true);
            }
            if (!axisOp.has(OCKey.LINE.value())) {
                //FIXME: nessecary?
                axisOp.put(OCKey.LINE.value(), makeStandardShapeType());
            }

            final CTTickLblPos label = axis.getTickLblPos();
            if (label != null && label.getVal() != STTickLblPos.NONE) {
                axisOperation.put(OCKey.LABEL.value(), true);
                character = DMLHelper.createJsonFromProperties(axis.getTxPr());
            }

        }
        if (axis.getNumFmt() != null && !axis.getNumFmt().isSourceLinked()) {
            axisOperation.put("format", axis.getNumFmt().getFormatCode());
        }

        if (axisOperation.length() > 0) {
            axisOp.put(OCKey.AXIS.value(), axisOperation);
        }
        if (character != null) {
            axisOp.put(OCKey.CHARACTER.value(), character);
        }

        if (axisOp.length() > 0) {
            final JSONObject op = new JSONObject(7);
            op.put(OCKey.NAME.value(), OCValue.CHANGE_CHART_AXIS.value());
            op.put(OCKey.START.value(), startPosition);
            op.put(OCKey.AXIS.value(), axis.getAxId().getVal());
            op.put(OCKey.CROSS_AX.value(), axis.getCrossAx().getVal());
            op.put(OCKey.AX_POS.value(), axis.getAxPos().getVal().value());

            if (zAxes.contains(axis.getAxId().getVal())) {
                op.put(OCKey.Z_AXIS.value(), true);
            }
            op.put(OCKey.ATTRS.value(), axisOp);

            operations.put(op);
        }

        addAxisTitleOperation(axis.getTitle(), axis.getAxId().getVal(), operations, startPosition);

    }

    private static void addAxisTitleOperation(CTTitle title, Long axisId, JSONArray operations, List<Integer> position) throws JSONException {

        final JSONObject attrs = getAxisTitleAttributes(title);
        if (attrs != null) {

            final JSONObject op = new JSONObject(4);
            op.put(OCKey.NAME.value(), OCValue.CHANGE_CHART_TITLE.value());
            op.put(OCKey.START.value(), position);
            op.put(OCKey.AXIS.value(), axisId);
            op.put(OCKey.ATTRS.value(), attrs);
            operations.put(op);
        }

    }

    private static JSONObject getAxisTitleAttributes(CTTitle title) throws JSONException {
        if (title == null) {
            return null;
        }
        final CTTx tx = title.getTx();
        if (tx == null) {
            return null;
        }

        final CTTextBody rich = tx.getRich();
        final CTStrRef ref = tx.getStrRef();

        if (null == ref && null == rich) {
            return null;
        }

        CTTextCharacterProperties characterProb = null;
        CTTextParagraphProperties textParagraphProperties = null;

        final JSONObject attrs = new JSONObject();
        final JSONObject textObj = new JSONObject();
        attrs.put(OCKey.TEXT.value(), textObj);

        if (rich != null) {

            final StringBuffer buf = new StringBuffer();

            final List<Object> paras = rich.getContent();
            for (int i = 0; i < paras.size(); i++) {
                final CTTextParagraph p = (CTTextParagraph) paras.get(i);


                if (textParagraphProperties == null) {
                    textParagraphProperties = p.getPPr(false);
                }

                final List<Object> textes = p.getContent();
                for (final Object text : textes) {
                    if (text instanceof CTRegularTextRun) {
                        addTo((CTRegularTextRun) text, buf);
                    } else if (text instanceof CTTextField) {
                        addTo((CTTextField) text, buf);
                    } else if (text instanceof CTTextLineBreak) {
                        addTo(buf);
                    }

                    if (characterProb == null) {
                        characterProb = ((ITextCharacterProperties) text).getRPr(false);
                    }

                }
                if (i < paras.size() - 1) {
                    buf.append("\n");
                }
            }

            final JSONArray link = new JSONArray();
            link.put(buf.toString());
            textObj.put(OCKey.LINK.value(), link);
        }

        if (ref != null) {
            textObj.put(OCKey.LINK.value(), ref.getF());
        }

        DMLHelper.createJsonFromTextCharacterProperties(null, attrs, characterProb);
        DMLHelper.createJsonFromTextParagraphProperties(null, attrs, textParagraphProperties);
        return attrs;
    }

    private static IAxisDescription getAxis(CTPlotArea plotArea, long axisId) {
        List<IAxisDescription> axes = plotArea.getValAxOrCatAxOrDateAx();
          for (IAxisDescription axis : axes) {
              if (axis.getAxId().getVal() == axisId) {
                  return axis;
              }
          }

        return null;
    }

    // In
    private static IAxisDescription getAxis(CTPlotArea plotArea, long axisId, long crossAxId, String axPos, boolean zAxis) {
        if (plotArea.getAreaChartOrArea3DChartOrLineChart().get(0) instanceof NoAxIdChart) {
            return null;
        }

        List<IAxisDescription> axes = plotArea.getValAxOrCatAxOrDateAx();

        STAxPos staxPos = STAxPos.valueOf(axPos.toUpperCase());

        IAxisDescription res = null;
        for (IAxisDescription other : axes) {

            if (axisId == other.getAxId().getVal()) {
                res = other;
                break;
            }
        }
//
        if (res == null) {
            String chartType = getSeriesType(plotArea.getAreaChartOrArea3DChartOrLineChart().get(0));

            boolean horizontal = "t".equals(axPos) || "b".equals(axPos);
            boolean vertical = "l".equals(axPos) || "r".equals(axPos);
            boolean barChart = chartType.startsWith("bar");

            STCrosses crossMinMax = STCrosses.AUTO_ZERO;
            boolean xyChart = false;
            if (chartType.contains("bubble") || chartType.contains("scatter")) {
                crossMinMax = STCrosses.MIN;
                xyChart = true;
            }
            if ("r".equals(axPos) || "t".equals(axPos)) {
                crossMinMax = STCrosses.MAX;
            }

            if (zAxis) {
                final CTSerAx ser = createSerAx();
                ser.setMinorGridlines(getHiddenChartLines());
                ser.setMinorTickMark(getHiddenTickMark());
                ser.setNumFmt(null);

                ser.setCrosses(new CTCrosses());
                ser.getCrosses().setVal(crossMinMax);

                res = ser;
            } else if (!xyChart && ((horizontal && !barChart) || (vertical && barChart))) {
                final CTCatAx cat = createCatAx();
                cat.setMinorGridlines(getHiddenChartLines());
                cat.setMinorTickMark(getHiddenTickMark());
                cat.setNumFmt(null);

                cat.setCrosses(new CTCrosses());
                cat.getCrosses().setVal(crossMinMax);

                res = cat;
            } else {
                final CTValAx val = createValAx();

                val.setMinorGridlines(getHiddenChartLines());
                val.setMinorTickMark(getHiddenTickMark());
                val.setNumFmt(null);

                val.setCrosses(new CTCrosses());
                val.getCrosses().setVal(crossMinMax);
                val.setCrossBetween(new CTCrossBetween());
                val.getCrossBetween().setVal(STCrossBetween.BETWEEN);

                res = val;
            }
            res.setCrossAx(getInteger(crossAxId));
            res.setAxPos(new CTAxPos(staxPos));
            res.setAxId(getInteger(axisId));

            axes.add(res);

        }
        // fix for Bug 49940
        if (getBoolean(res.getDelete(), false)) {
            hideAxis(res);
        } else {
            // workaround for Bug 49959
            res.setDelete(new CTBoolean(false));
        }
        return res;
    }

    private static Boolean getBoolean(CTBoolean value, Boolean def) {
        if (value == null) {
            return def;
        }
        return value.isVal();
    }

    private static Object getData(CTNumDataSource numDataSource) throws JSONException {
        if (numDataSource == null) {
            Logger.getAnonymousLogger().warning("CTNumDataSource is null");
            return null;
        }
        if (numDataSource.getNumRef() != null) {
            return numDataSource.getNumRef().getF();
        } else if (numDataSource.getNumLit() != null) {
            final CTNumData numLit = numDataSource.getNumLit();
            if (numLit != null) {
                final List<CTNumVal> numLitPtList = numLit.getPt();
                if (numLitPtList != null && !numLitPtList.isEmpty()) {
                    final JSONArray values = new JSONArray();
                    for (final CTNumVal numLitPt : numLitPtList) {
                        if (numLitPt != null) {
                            String v = numLitPt.getV();
                            try {
                                values.put(Float.parseFloat(v));
                            } catch (NumberFormatException e) {
                                values.put(v);
                            }
                        } else {
                            values.put((String) null);
                        }
                    }
                    return values;
                }
            }
        }
        return null;
    }

    private static final void addGridOperation(ChartWrapper chart, IAxisDescription axis, JSONArray operations, List<Integer> startPosition) throws Exception {

        final CTChartLines grid = axis.getMajorGridlines();
        if (grid != null) {
            final JSONObject gridOp = new JSONObject();
            final CTShapeProperties shape = grid.getSpPr();
            if (shape != null) {
                DMLHelper.createJsonFromShapeProperties(gridOp, shape, chart.getThemePart(), chart.getChartStyle().getGridlineMajor(), chart.getChartPart(), false, true);
            }
            if (!gridOp.has(OCKey.LINE.value())) {
                //FIXME: nessecary?
                gridOp.put(OCKey.LINE.value(), makeStandardShapeType());
            }
            final JSONObject op = new JSONObject(4);
            op.put(OCKey.NAME.value(), OCValue.CHANGE_CHART_GRID.value());
            op.put(OCKey.START.value(), startPosition);
            op.put(OCKey.AXIS.value(), axis.getAxId().getVal());
            op.put(OCKey.ATTRS.value(), gridOp);
            operations.put(op);
        }
    }

    private static CTChartLines getHiddenChartLines() {
        final CTChartLines line = new CTChartLines();
        line.setSpPr(getHiddenShapeProperties());
        return line;
    }

    private static CTTickLblPos getHiddenLblPos() {
        final CTTickLblPos lbl = new CTTickLblPos();
        lbl.setVal(STTickLblPos.NONE);
        return lbl;
    }

    private static CTShapeProperties getHiddenShapeProperties() {
        final CTShapeProperties shape = new CTShapeProperties();
        shape.setNoFill(new CTNoFillProperties());
        final CTLineProperties line = new CTLineProperties();
        line.setNoFill(new CTNoFillProperties());
        shape.setLn(line);
        return shape;
    }

    private static CTTickMark getHiddenTickMark() {
        final CTTickMark mark = new CTTickMark();
        mark.setVal(STTickMark.NONE);
        return mark;
    }

    private static CTUnsignedInt getInteger(int value) {
        final CTUnsignedInt res = new CTUnsignedInt();
        res.setVal(value);
        return res;
    }

    private static CTUnsignedInt getInteger(long value) {
        final CTUnsignedInt res = new CTUnsignedInt();
        res.setVal(value);
        return res;
    }

    @SuppressWarnings("unchecked")
    private static Class<? extends ISerContent> getNewContentType(IListSer listSer) throws Exception {
        final XmlElements serType = listSer.getClass().getDeclaredField("ser").getAnnotation(XmlElements.class);
        return serType.value()[0].type();
    }

    private static Integer getRotation(IListSer chart) {
        if (chart instanceof RotationChart) {
            return ((RotationChart) chart).getFirstSliceAng().getVal();
        }
        return 0;
    }

    private static String getSeriesType(IListSer listSer) {
        String chartType = null;
        if (listSer instanceof CTBubbleChart) {
            chartType = "bubble2d";
        } else if (listSer instanceof CTRadarChart) {
            chartType = "radar2d";
        } else if (listSer instanceof CTSurface3DChart) {
            chartType = "surface3d";
        } else if (listSer instanceof CTArea3DChart) {
            chartType = "area3d";
        } else if (listSer instanceof CTOfPieChart) {
            chartType = "ofPie2d";
        } else if (listSer instanceof CTAreaChart) {
            chartType = "area2d";
        } else if (listSer instanceof CTBarChart) {
            chartType = handleBarDir(listSer);
        } else if (listSer instanceof CTSurfaceChart) {
            chartType = "surface";
        } else if (listSer instanceof CTLine3DChart) {
            chartType = "line3d";
        } else if (listSer instanceof CTDoughnutChart) {
            chartType = "donut2d";
        } else if (listSer instanceof CTLineChart) {
            chartType = "line2d";
        } else if (listSer instanceof CTScatterChart) {
            chartType = "scatter2d";
        } else if (listSer instanceof CTBar3DChart) {
            chartType = handleBarDir(listSer);
        } else if (listSer instanceof CTPieChart) {
            chartType = "pie2d";
        } else if (listSer instanceof CTPie3DChart) {
            chartType = "pie3d";
        } else if (listSer instanceof CTStockChart) {
            chartType = "bar2d";
        }
        return chartType;
    }

    private static String getStackingType(ChartWrapper chartPart) {
        HashMap<String, Object> chartMap = new HashMap<>();
        getChartType(chartPart, chartMap);
        return (String) chartMap.get(OCKey.CHART_GROUP.value());
    }

    private static Pair<Integer, Pair<ISerContent, IListSer>> globalToLocal(CTPlotArea plotArea, int series) {
        List<IListSer> all = plotArea.getAreaChartOrArea3DChartOrLineChart();

        int index = 0;
        for (IListSer listSer : all) {
            int localIndex = 0;
            for (ISerContent content : listSer.getSer()) {
                if (series == index) {
                    return new Pair<Integer, Pair<ISerContent, IListSer>>(localIndex, new Pair<>(content, listSer));
                }
                localIndex++;
                index++;
            }
        }
        return null;
    }

    private static String handleBarDir(IListSer listSer) {
        final CTBarDir dir;
        final String view;
        if (listSer instanceof CTBar3DChart) {
            view = "3d";
            dir = ((CTBar3DChart) listSer).getBarDir();
        } else {
            view = "2d";
            dir = ((CTBarChart) listSer).getBarDir();
        }
        final String dirString;
        switch (dir.getVal()) {
            case BAR:
                dirString = "bar";
                break;
            default:
                dirString = "column";
                break;
        }
        return dirString + view;
    }

    private static void handleOverlap(CTBarChart barSer, String stacking) {
        final CTGapAmount gap = new CTGapAmount();
        gap.setVal(150);
        final CTOverlap overlap;
        if (stacking == null || stacking.equals("standard") || stacking.equals("clustered")) {
            overlap = null;
        } else {
            overlap = new CTOverlap();
            overlap.setVal((byte) 100);
        }
        barSer.setGapWidth(gap);
        barSer.setOverlap(overlap);

    }

    private static void handleOverlap(CTBar3DChart barSer) {
        final CTGapAmount gap = new CTGapAmount();
        gap.setVal(150);
        barSer.setGapWidth(gap);
        barSer.setGapDepth(gap);
    }

    private static void handleStandardDonutChart(CTDoughnutChart donut) {
        final CTHoleSize holeSize = new CTHoleSize();
        holeSize.setVal((short) 60);
        donut.setHoleSize(holeSize);
        handleStandardRotationChart(donut);
    }

    private static void handleStandardRotationChart(RotationChart chart) {
        chart.setFirstSliceAng(new CTFirstSliceAng());
    }

    private static void handleStandardScatterChart(CTScatterChart scatter) {
        if (scatter.getScatterStyle() == null || scatter.getScatterStyle().getVal() == null) {
            final CTScatterStyle style = new CTScatterStyle();
            style.setVal(STScatterStyle.LINE_MARKER);
            scatter.setScatterStyle(style);
        }

        for (final ISerContent cont : scatter.getSer()) {
            handleStandardScatterSer((CTScatterSer) cont);
        }
    }

    private static void handleStandardScatterSer(CTScatterSer serContent) {
        CTShapeProperties shape = serContent.getSpPr();
        if (null == shape) {
            shape = new CTShapeProperties();
            serContent.setSpPr(shape);
        }
        CTLineProperties line = shape.getLn();
        if (null == line) {
            line = new CTLineProperties();
            shape.setLn(line);
            line.setW(19050);
            line.setCap(STLineCap.RND);
            line.setRound(new CTLineJoinRound());
            shape.getEffectLst();
        }

    }

    private static void handleStandardTypes(CTTitle title) {
        CTTx tx = title.getTx();
        if (tx == null) {
            tx = new CTTx();
            title.setTx(tx);
        }
        CTTextBody rich = tx.getRich();
        if (rich == null) {
            rich = new CTTextBody();
            tx.setRich(rich);
        }
        DMLHelper.handleStandardTypes(rich);

        CTLayout layout = title.getLayout();
        if (layout == null) {
            layout = new CTLayout();
            title.setLayout(layout);
        }
        title.setOverlay(new CTBoolean(false));

        CTShapeProperties spr = title.getSpPr();
        if (spr == null) {
            spr = getHiddenShapeProperties();
        }

        final List<Object> ps = rich.getContent();
        ps.clear();

        final CTTextParagraph p = new CTTextParagraph();
        p.getPPr(true);
        ps.add(p);

    }

    private static boolean hasEmptyContents(CTAxDataSource data) {
        if (data == null) {
            return true;
        }
        if (data.getMultiLvlStrRef() != null || data.getNumLit() != null || data.getNumRef() != null || data.getStrLit() != null || data.getStrRef() != null) {
            return false;
        }
        return true;
    }

    private static boolean hasEmptyContents(CTNumDataSource data) {
        if (data == null) {
            return true;
        }
        if (data.getNumLit() != null || data.getNumRef() != null) {
            return false;
        }
        return true;
    }

    private static boolean hasEmptyContents(CTSerTx data) {
        if (data == null) {
            return true;
        }
        if (data.getStrRef() != null || data.getV() != null) {
            return false;
        }
        return true;
    }

    private static void hideAxis(IAxisDescription axis) {
        final CTBoolean delete = new CTBoolean(false);
        axis.setDelete(delete);

        axis.setSpPr(getHiddenShapeProperties());

        axis.setMajorGridlines(getHiddenChartLines());
        axis.setTickLblPos(getHiddenLblPos());

        axis.setTitle(null);
        axis.setTxPr(null);

    }


    private static void insertSeriesAttribute(CTNumDataSource source, Object attribute) {
        if (attribute instanceof String) {
            final CTNumRef numRef = source.getNumRef(true);
            numRef.setF((String) attribute);

            // empty cache for Net-Charts! (Bug 49821)
            numRef.setNumCache(Context.getDmlChartObjectFactory().createCTNumData());
        } else if (attribute instanceof JSONArray) {
            final JSONArray bubblesArray = (JSONArray) attribute;
            final CTNumData numData = Context.getDmlChartObjectFactory().createCTNumData();
            source.setNumLit(numData);
            numData.setPtCount(getInteger(bubblesArray.length()));
            for (int i = 0; i < bubblesArray.length(); i++) {
                final CTNumVal numVal = Context.getDmlChartObjectFactory().createCTNumVal();
                numVal.setIdx(i);
                numVal.setV(bubblesArray.optString(i, ""));
                numData.getPt().add(numVal);
            }
        }
    }

    private static boolean isCurved(ISerContent serContent, boolean def) {
        if (serContent instanceof CTLineSer) {
            final CTBoolean smooth = ((CTLineSer) serContent).getSmooth();
            //no smooth in line chart means it is activated!!!
            if (getBoolean(smooth, def)) {
                return true;
            }
        } else if (serContent instanceof CTScatterSer) {
            final CTBoolean smooth = ((CTScatterSer) serContent).getSmooth();
            //no smooth in line chart means it is activated!!!
            if (getBoolean(smooth, def)) {
                return true;
            }
        }
        return def;
    }

    //FIXME: nessecary?
    private static JSONObject makeStandardShapeType() throws JSONException {
        final JSONObject shape = new JSONObject();
        shape.put(OCKey.TYPE.value(), "solid");
        final JSONObject color = new JSONObject(1);
        color.put(OCKey.TYPE.value(), "auto");
        shape.put(OCKey.COLOR.value(), color);
        return shape;
    }

    private static CTDLbls newDataLabel() {
        final CTDLbls lbl = new CTDLbls();
        lbl.setShowLegendKey(new CTBoolean(false));
        lbl.setShowCatName(new CTBoolean(false));
        lbl.setShowSerName(new CTBoolean(false));
        lbl.setShowPercent(new CTBoolean(false));
        lbl.setShowBubbleSize(new CTBoolean(false));
        lbl.setShowLeaderLines(new CTBoolean(false));
        lbl.setShowVal(new CTBoolean(false));

        CTShapeProperties spr = lbl.getSpPr();
        if (spr == null) {
            spr = new CTShapeProperties();
            lbl.setSpPr(spr);
            spr.setNoFill(new CTNoFillProperties());
            final CTLineProperties ln = new CTLineProperties();
            ln.setNoFill(new CTNoFillProperties());
            spr.setLn(ln);

            spr.setEffectLst(new CTEffectList());
        }
        CTTextBody txPr = lbl.getTxPr();
        if (txPr == null) {
            txPr = new CTTextBody();
            lbl.setTxPr(txPr);
            DMLHelper.handleStandardTypes(txPr);
        }

        final CTTextParagraph p = new CTTextParagraph();
        final CTTextParagraphProperties prop = p.getPPr(true);
        final CTTextCharacterProperties def = prop.getDefRPr(true);

        def.setSz(1000);

        final List<Object> paras = txPr.getContent();
        paras.clear();

        paras.add(p);

        return lbl;
    }

    private static String opToOOXMLType(String chartType) {
        if (!chartType.endsWith("2d") && !chartType.endsWith("3d")) {
            chartType = chartType + "2d";
        }
        return chartType;
    }

    private static void refreshTitleRotations(ChartWrapper chart) {
        CTPlotArea plotArea = chart.getPlotArea();
        final List<IAxisDescription> axes = plotArea.getValAxOrCatAxOrDateAx();

        for (int i = 0; i < axes.size(); i++) {

            IAxisDescription axis = axes.get(i);
            boolean vertical = axis.getAxPos().getVal().equals(STAxPos.L) || axis.getAxPos().getVal().equals(STAxPos.R);
            int rot = 0;

            if (vertical) {
                rot = -5400000;
            }

            final CTTitle title = axis.getTitle();

            if (title != null) {
                if (title.getTx() != null && title.getTx().getRich() != null && title.getTx().getRich().getBodyPr() != null) {
                    final CTTx tx = title.getTx();
                    final CTTextBody rich = tx.getRich();
                    final CTTextBodyProperties body = rich.getBodyPr();
                    body.setRot(rot);

                }

                final CTTextBody txPr = title.getTxPr();
                if (null != txPr && null != txPr.getBodyPr()) {
                    final CTTextBodyProperties body2 = txPr.getBodyPr();
                    body2.setRot(rot);
                }
            }

            final CTTextBody lbl = axis.getTxPr();
            if (lbl != null && lbl.getBodyPr() != null) {
                final CTTextBodyProperties body = lbl.getBodyPr();
                body.setRot(0);
            }
        }
    }

    private static void setCharacterFromAttrs(CTTextBody txPr, JSONObject attrs) throws JSONException {
        final List<Object> ps = txPr.getContent();
        ps.clear();
        final CTTextParagraph p = Context.getDmlObjectFactory().createCTTextParagraph();
        ps.add(p);
        setCharacterFromAttrs(p, attrs);
    }

    private static CTTextCharacterProperties setCharacterFromAttrs(CTTextCharacterProperties t, JSONObject attrs) throws JSONException {
        final JSONObject character = attrs.optJSONObject(OCKey.CHARACTER.value());

        if (character != null) {
            if (t == null) {
                t = Context.getDmlObjectFactory().createCTTextCharacterProperties();
            }
            DMLHelper.applyTextCharacterPropertiesFromJson(t, attrs, null);

        }
        return t;
    }

    private static void setCurved(ISerContent serContent, boolean curved) {
        if (serContent instanceof CTLineSer) {
            ((CTLineSer) serContent).setSmooth(new CTBoolean(curved));
        } else if (serContent instanceof CTScatterSer) {
            ((CTScatterSer) serContent).setSmooth(new CTBoolean(curved));
        }
    }

    private static void setDataLabel(ISerContent serContent, ArrayList<String> dataLabelText, String dataLabelPos) {
        CTDLbls lbl = serContent.getDLbls();
        if (lbl == null) {
            lbl = newDataLabel();
            serContent.setDLbls(lbl);
        }
        
        lbl.setShowBubbleSize(setIfContains(dataLabelText, "size"));
        lbl.setShowCatName(setIfContains(dataLabelText, "cat"));
        lbl.setShowLegendKey(setIfContains(dataLabelText, "legend"));
        lbl.setShowPercent(setIfContains(dataLabelText, "percent"));
        lbl.setShowSerName(setIfContains(dataLabelText, "series"));
        lbl.setShowVal(setIfContains(dataLabelText, "value"));
        
        STDLblPos pos = getDataLabelPosition(dataLabelPos);
        
        if (pos == null) {
            lbl.setDLblPos(null);
        } else {
            CTDLblPos lPos = new CTDLblPos();
            lPos.setVal(pos);
            lbl.setDLblPos(lPos);
        }
        
        
//        lbl.setDLblPos(CTDLblPos.);
    }

    private static void setDataseriesAttrs(ChartWrapper chartWrapper, ISerContent serContent, JSONObject attrs) throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if (attrs != null) {
            final JSONObject attrSeries = attrs.optJSONObject(OCKey.SERIES.value());
            if (attrSeries != null) {

                // title
                if (attrSeries.has(OCKey.TITLE.value())) {
                    CTSerTx serTx = null;
                    final Object title = attrSeries.get(OCKey.TITLE.value());
                    if (title != null) {
                        serTx = Context.getDmlChartObjectFactory().createCTSerTx();

                        if (title instanceof String) {
                            final CTStrRef strRef = serTx.getStrRef(true);
                            strRef.setF((String) title);

                            // empty cache for Net-Charts! (Bug 49821)
                            strRef.setStrCache(Context.getDmlChartObjectFactory().createCTStrData());
                        } else if (title instanceof JSONArray && !((JSONArray) title).isEmpty()) {
                            final Object o = ((JSONArray) title).get(0);
                            if (o instanceof String) {
                                serTx.setV((String) o);
                            }
                        } else {
                            Logger.getAnonymousLogger().warning("Cant handle Chart Title " + title);
                            serTx = null;
                        }
                    }
                    serContent.setTx(serTx);
                }
                // names
                if (attrSeries.has(OCKey.NAMES.value())) {
                    CTAxDataSource cat = null;
                    final Object names = attrSeries.get(OCKey.NAMES.value());
                    if (names != null) {
                        cat = Context.getDmlChartObjectFactory().createCTAxDataSource();

                        if (names instanceof String) {
                            final CTStrRef strRef = cat.getStrRef(true);
                            strRef.setF((String) names);

                            // empty cache for Net-Charts! (Bug 49821)
                            strRef.setStrCache(Context.getDmlChartObjectFactory().createCTStrData());
                        } else if (names instanceof JSONArray) {
                            final JSONArray titleArray = (JSONArray) names;
                            final CTStrData strData = cat.getStrLit(true);
                            strData.setPtCount(getInteger(titleArray.length()));
                            for (int i = 0; i < titleArray.length(); i++) {
                                final CTStrVal strVal = Context.getDmlChartObjectFactory().createCTStrVal();
                                strVal.setV(titleArray.optString(i, ""));
                                strData.getPt().add(strVal);
                            }
                        } else {
                            Logger.getAnonymousLogger().warning("Cant handle Chart Names " + names);
                            cat = null;
                        }
                    }
                    serContent.setCat(cat);
                }
                // values
                if (attrSeries.has(OCKey.VALUES.value())) {
                    CTNumDataSource val = null;
                    final Object values = attrSeries.get(OCKey.VALUES.value());
                    if (values != null) {
                        val = Context.getDmlChartObjectFactory().createCTNumDataSource();
                        insertSeriesAttribute(val, values);
                    }
                    serContent.setVal(val);
                }

                // bubbles
                if (attrSeries.has(OCKey.BUBBLES.value()) && serContent instanceof CTBubbleSer) {
                    CTNumDataSource bub = null;
                    final Object bubbles = attrSeries.opt(OCKey.BUBBLES.value());
                    if (bubbles != null) {
                        bub = Context.getDmlChartObjectFactory().createCTNumDataSource();
                        insertSeriesAttribute(bub, bubbles);
                    }
                    ((CTBubbleSer) serContent).setBubbleSize(bub);
                    if (attrSeries.optBoolean(OCKey.BUBBLE3D.value())) {
                        ((CTBubbleSer) serContent).setBubble3D(new CTBoolean(true));
                    }
                }
                if (serContent instanceof CTScatterSer) {
                    handleStandardScatterSer((CTScatterSer) serContent);
                }

                if (attrSeries.has(OCKey.DATA_LABEL.value())) {
                    JSONObject dataLabel = attrSeries.optJSONObject(OCKey.DATA_LABEL.value());
                    ArrayList<String> dataLabelText = new ArrayList<String>();
                    if (dataLabel != null && dataLabel.has(OCKey.DATA_LABEL_TEXT.value())) {
                        Iterator<Object> dataLabelTextJSON = dataLabel.getJSONArray(OCKey.DATA_LABEL_TEXT.value()).iterator();
                        while (dataLabelTextJSON.hasNext()) {
                            dataLabelText.add(dataLabelTextJSON.next().toString());
                        }
                        
                    }
                    String dataLabelPos = dataLabel != null && dataLabel.has(OCKey.DATA_LABEL_POS.value()) ? dataLabel.getString(OCKey.DATA_LABEL_POS.value()) : null;
                    
                    setDataLabel(serContent, dataLabelText, dataLabelPos);
                }

                if (attrSeries.hasAny(OCKey.MARKER_FILL.value(), OCKey.MARKER_BORDER.value())) {
                    CTMarker marker = Context.getDmlChartObjectFactory().createCTMarker();
                    serContent.setMarker(marker);
                    marker.setSpPr(new CTShapeProperties());
                    setMarkerFillLineAttributesFromJSON(marker, attrSeries, chartWrapper);
                }

                if (attrSeries.has(OCKey.DATA_POINTS.value())) {

                    JSONArray dataPoints = attrSeries.optJSONArray(OCKey.DATA_POINTS.value());
                    List<CTDPt> points = serContent.getDPt();

                    if (points != null) {
                        points.clear();
                        if (dataPoints != null) {
                            int nextIndex = 0;
                            for (int i = 0; i < dataPoints.length(); i++) {
                                final JSONObject dataPoint = dataPoints.optJSONObject(i);
                                if (dataPoint != null && !dataPoint.isEmpty()) {
                                    nextIndex = dataPoint.optInt(OCKey.INDEX.value(), nextIndex);
                                    CTDPt point = Context.getDmlChartObjectFactory().createCTDPt();
                                    point.setInvertIfNegative(new CTBoolean(false));
                                    point.setIdx(getInteger(nextIndex++));

                                    // Test if the fill or line attributes exist in the original doc, if not remove it
                                    if (dataPoint.optBoolean(OCKey.NO_FILL.value())) {
                                        dataPoint.remove(OCKey.FILL.value());
                                    }
                                    if (dataPoint.optBoolean(OCKey.NO_LINE.value())) {
                                        dataPoint.remove(OCKey.LINE.value());
                                    }
                                    CTShapeProperties dataPointShapeProperties = DMLHelper.applyShapePropertiesFromJson(point.getSpPr(), dataPoint, chartWrapper.getOperationDocument(), chartWrapper.getChartPart(), false);
                                    if (!dataPointShapeProperties.isEmpty()) {
                                        point.setSpPr(dataPointShapeProperties);
                                    }

                                    if (dataPoint.hasAny(OCKey.MARKER_FILL.value(), OCKey.MARKER_BORDER.value())) {
                                        CTMarker marker = Context.getDmlChartObjectFactory().createCTMarker();
                                        point.setMarker(marker);
                                        marker.setSpPr(new CTShapeProperties());
                                        setMarkerFillLineAttributesFromJSON(marker, dataPoint, chartWrapper);
                                    }
                                    points.add(point);
                                }
                            }
                        }

                    } else {
                        Logger.getAnonymousLogger().warning("no DPt! for " + serContent.getClass().getSimpleName());
                    }
                }

                if (attrSeries.has(OCKey.FORMAT.value())) {
                    CTDLbls lbl = serContent.getDLbls();
                    if (lbl == null) {
                        lbl = newDataLabel();
                        serContent.setDLbls(lbl);
                    }
                    String format = attrSeries.getString(OCKey.FORMAT.value());
                    if (format != null && !format.isEmpty()) {
                        CTNumFmt nf = new CTNumFmt();
                        nf.setSourceLinked(false);
                        nf.setFormatCode(format);
                        lbl.setNumFmt(nf);
                    } else {
                        lbl.setNumFmt(null);
                    }
                }
            }
            boolean hasAnyMarker = attrs.hasAny(OCKey.MARKER_FILL.value(), OCKey.MARKER_BORDER.value());
            if ((hasAnyMarker || attrs.hasAny(OCKey.LINE.value(), OCKey.FILL.value())) && (serContent instanceof CTLineSer || serContent instanceof CTScatterSer)) {

                CTMarker marker = serContent.getMarker();
                if (marker == null) {
                    marker = Context.getDmlChartObjectFactory().createCTMarker();
                    if (serContent instanceof CTLineSer) {
                        ((CTLineSer) serContent).setMarker(marker);
                    } else {
                        ((CTScatterSer) serContent).setMarker(marker);
                    }
                }

                if (hasAnyMarker) {
                    if (marker.getSpPr() == null) {
                        marker.setSpPr(new CTShapeProperties());
                    }
                    setMarkerFillLineAttributesFromJSON(marker, attrs, chartWrapper);
                } else {
                    marker.setSymbol(new CTMarkerStyle());
                    marker.getSymbol().setVal(STMarkerStyle.NONE);
                }
            }

            // Test if the fill or line attributes exist in the original doc, if not remove it
            if (attrSeries != null) {
                if (attrSeries.optBoolean(OCKey.NO_FILL.value())) {
                    attrs.remove(OCKey.FILL.value());
                }
                if (attrSeries.optBoolean(OCKey.NO_LINE.value())) {
                    attrs.remove(OCKey.LINE.value());
                }
            }

            serContent.setSpPr(DMLHelper.applyShapePropertiesFromJson(serContent.getSpPr(), attrs, chartWrapper.getOperationDocument(), chartWrapper.getChartPart(), false));
        }
    }

    private static void setMarkerFillLineAttributesFromJSON (CTMarker marker, JSONObject attrs, ChartWrapper chartWrapper) throws InvalidFormatException, JSONException {
        JSONObject markerFill = attrs.optJSONObject(OCKey.MARKER_FILL.value());
        if (markerFill != null) {
            DMLHelper.applyMarkerFillPropertiesFromJson(marker.getSpPr(), attrs, chartWrapper.getOperationDocument(), chartWrapper.getChartPart());
        }

        if (attrs.has(OCKey.MARKER_BORDER.value()) && (markerFill == null || !markerFill.optBoolean(OCKey.ONLY_MARKER.value(), false))) {
            marker.getSpPr().setLn(DMLHelper.applyLinePropertiesFromJson(marker.getSpPr().getLn(), attrs.optJSONObject(OCKey.MARKER_BORDER.value())));
        }
    }

    private static CTBoolean setIfContains(ArrayList<String> source, String search) {
        return new CTBoolean(source.contains(search));
    }

    private static void setRotation(IListSer chart, int angle) {
        if (angle > 0 && chart instanceof RotationChart) {
            ((RotationChart) chart).getFirstSliceAng().setVal(angle);
        }
    }

    private static void setRotation3DFromJSON(CTChart chart, JSONObject attributes) {
        if (attributes.has(OCKey.ROTATION_X.value()) && attributes.has(OCKey.ROTATION_Y.value())) {
            CTView3D view3D  = chart.getView3D();
            if (view3D == null) {
                view3D = new CTView3D();
                chart.setView3D(view3D);
            }
            view3D.setRotX(new CTRotX(attributes.optInt(OCKey.ROTATION_X.value(), 0)));
            view3D.setRotY(new CTRotY(attributes.optInt(OCKey.ROTATION_Y.value(), 0)));
        }
    }

    // workaround for Bug 43959
    private static CTTextBody setTextBodyFromAttrs(CTTextBody textBody, JSONObject attrs) throws JSONException {
        JSONObject character = attrs.optJSONObject(OCKey.CHARACTER.value());
        if (character == null || character.length() == 0) {
            //nothing
        } else {
            if (textBody == null) {
                textBody = new CTTextBody();
                DMLHelper.handleStandardTypes(textBody);
            }
            setCharacterFromAttrs(textBody, attrs);
        }
        return textBody;
    }

    /*

    Idx and Order are required elements.

    The order attribute needs to be unique over all chart elements. The order itself does not matter and it
    also does not need to be linear. so order of "8", "5", "1", "4" is valid
        <c:lineChart>
            <c:ser>
                <order="8">
            </c:ser>
            <c:ser>
                <order="5">
            </c:ser>
        </c:lineChart>
        <c:lineChart>
            <c:ser>
                <idx="1">
            </c:ser>
            <c:ser>
                <idx="4">
            </c:ser>
        </c:lineChart>

    The idx attribute is allowed to be double. It must not start with "0" so following is valid

        <c:lineChart>
            <c:ser>
                <idx="1">
            </c:ser>
            <c:ser>
                <idx="1">
            </c:ser>
        </c:lineChart>
        <c:lineChart>
            <c:ser>
                <idx="2">
            </c:ser>
        </c:lineChart>
      */
    private static void updateIndexAndOrder(CTPlotArea plotArea) {
        int index = 0;
        for (IListSer listSer : plotArea.getAreaChartOrArea3DChartOrLineChart()) {
            for (ISerContent serCont : listSer.getSer()) {
                serCont.setIdx(getInteger(index));
                serCont.setOrder(getInteger(index));
                index++;
            }
        }
    }
}
