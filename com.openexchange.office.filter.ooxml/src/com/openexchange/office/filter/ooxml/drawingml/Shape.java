/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import org.docx4j.dml.IShape;
import org.docx4j.dml.IShapeBasic;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class Shape {

    public static void applyAttrsFromJSON(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, IShape shape, boolean rootShape, boolean applyStyle)
    	    throws JSONException, InvalidFormatException, PartUnrecognisedException {

    	DMLHelper.applyTextBodyPropertiesFromJson(shape.getTextBodyProperties(true), attrs);
        DMLHelper.applyNonVisualDrawingProperties(shape, attrs.optJSONObject(OCKey.DRAWING.value()));
        DMLHelper.applyNonVisualDrawingShapeProperties(shape, attrs.optJSONObject(OCKey.DRAWING.value()));
	    shape.setSpPr(DMLHelper.applyShapePropertiesFromJson(shape.getSpPr(), attrs, operationDocument, operationDocument.getContextPart(), rootShape));
	    if(applyStyle) {
    	    final JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
    	    if(characterAttrs!=null) {
    	    	final JSONObject color = characterAttrs.optJSONObject(OCKey.COLOR.value());
    	    	if(color!=null) {
        			DMLHelper.createSolidColorFillPropertiesFromJson(shape.getStyle(true).getFontRef(), color);
    	    	}
    	    }
	    }
    }

    public static void createJSONAttrs(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, IShape shape, boolean rootShape)
    	throws JSONException, FilterException {

    	DMLHelper.createJsonFromTextBodyProperties(attrs, shape.getTextBodyProperties(false));
    	createJSONAttrs(operationDocument, attrs, (IShapeBasic)shape, rootShape);
    }

    public static void createJSONAttrs(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, IShapeBasic shape, boolean rootShape)
        throws JSONException, FilterException {

        DMLHelper.createJsonFromNonVisualDrawingProperties(attrs, shape);
        DMLHelper.createJsonFromNonVisualDrawingShapeProperties(attrs, shape);
        DMLHelper.createJsonFromShapeProperties(attrs, shape.getSpPr(), operationDocument.getThemePart(true), shape.getStyle(false), operationDocument.getContextPart(), true, rootShape);
    }
}
