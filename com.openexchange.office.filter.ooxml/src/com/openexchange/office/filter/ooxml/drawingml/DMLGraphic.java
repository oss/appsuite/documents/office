/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.util.HashMap;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.GraphicData;
import org.docx4j.dml.chart.CTChartSpace;
import org.docx4j.dml.chart.CTRelId;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

final public class DMLGraphic {

    /**
     * @param part where the graphic is placed in
     * @param graphic
     * @return map may contain following properties:
     *
     * "type"        the drawingType as it is used within the insertDrawing operation
     * "chartObject" org.docx4j.dml.chart.CTChartSpace (Chart)
     *
     * @throws JSONException
     */
    public static void getGraphicProperties(OfficeOpenXMLOperationDocument operationDocument, Part part, Graphic graphic, HashMap<String, Object> map) {

        if(graphic!=null) {
            final GraphicData graphicData = graphic.getGraphicData();
            if(graphicData!=null) {
                if (graphicData.getUri().equals("http://schemas.openxmlformats.org/drawingml/2006/chart")/* || graphicData.getUri().equals("http://schemas.microsoft.com/office/drawing/2014/chartex") */) {
                    final List<Object> data = graphicData.getAny();
                    if(!data.isEmpty()) {
                        final Object o = data.get(0);
                        if(o instanceof JAXBElement<?>&&((JAXBElement<?>) o).getDeclaredType().getName().equals("org.docx4j.dml.chart.CTRelId")) {
                            final Object value = ((JAXBElement<?>)o ).getValue();
                            if(value instanceof CTRelId) {
                                final String relId = ((CTRelId) value).getId();
                                if(relId!=null&&!relId.isEmpty()) {
                                    final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
                                    if(relationshipsPart!=null) {
                                        final Part relPart = relationshipsPart.getPart(relId);
                                        if (relPart != null && relPart.getRelationshipType().equals("http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart")) {
                                            final ChartWrapper chart = new ChartWrapper((Chart) relPart, operationDocument);
                                            map.put(OCKey.CHART_PART.value(), chart);
                                            map.put(OCKey.TYPE.value(), "chart");
                                            chart.getChartType(map);
                                        }
                                    }
                                }
                            }
                        } /*
                           * else if(o instanceof JAXBElement<?>&&((JAXBElement<?>) o).getDeclaredType().getName().equals("org.docx4j.dml.chartex2014.CTRelId")) {
                           * final Object value = ((JAXBElement<?>)o ).getValue();
                           * if(value instanceof org.docx4j.dml.chartex2014.CTRelId) {
                           * final String relId = ((org.docx4j.dml.chartex2014.CTRelId) value).getId();
                           * if(relId!=null&&!relId.isEmpty()) {
                           * final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
                           * if(relationshipsPart!=null) {
                           * final Part relPart = relationshipsPart.getPart(relId);
                           * if (relPart != null && relPart.getRelationshipType().equals("http://schemas.microsoft.com/office/2014/relationships/chartEx")) {
                           * map.put("type", "undefined");
                           * final ChartWrapper chart = new ChartWrapper((ChartExPart) relPart, operationDocument);
                           * map.put(OCKey.CHART_PART.toString(), chart);
                           * map.put("type", "chart");
                           * chart.getChartType(map);
                           * }
                           * }
                           * }
                           * }
                           * }
                           */
                    }
                }
            }
        }
    }

    // baseUrl depends to the current application... "/xl/charts/" for xlsx
    public static Graphic createDrawingMLChart(OfficeOpenXMLOperationDocument operationDocument, Part part, String baseUrl, JSONObject chartAttrs)
        throws Exception {

        final Graphic graphic = Context.getDmlObjectFactory().createGraphic();
        final GraphicData graphicData = Context.getDmlObjectFactory().createGraphicData();
        graphicData.setUri("http://schemas.openxmlformats.org/drawingml/2006/chart");
        final org.docx4j.dml.chart.ObjectFactory chartObjectFactory = new org.docx4j.dml.chart.ObjectFactory();
        final CTRelId CTRelId = chartObjectFactory.createCTRelId();
        final JAXBElement<?> chart = chartObjectFactory.createChart(CTRelId);
        graphicData.getAny().add(chart);
        final Chart chartPart = new Chart(new PartName(baseUrl + "chart1.xml"));
        final CTChartSpace chartSpace = DMLChartSpace.createChartSpace(chartAttrs);
        chartPart.setJaxbElement(chartSpace);
        final Relationship relationship = part.addTargetPart(chartPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
        CTRelId.setId(relationship.getId());
        graphic.setGraphicData(graphicData);
        return graphic;
    }
}
