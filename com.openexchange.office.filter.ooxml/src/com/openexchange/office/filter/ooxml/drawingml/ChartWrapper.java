/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.filter.ooxml.drawingml;

import java.util.HashMap;
import java.util.List;
import org.docx4j.dml.chart.CTBoolean;
import org.docx4j.dml.chart.CTChart;
import org.docx4j.dml.chart.CTChartSpace;
import org.docx4j.dml.chart.CTPlotArea;
import org.docx4j.dml.chart.CTStyle;
import org.docx4j.dml.chart.CTTitle;
import org.docx4j.dml.chartStyle2012.CTChartStyle;
import org.docx4j.dml.chartStyle2012.CTColorStyle;
import org.docx4j.dml.chartex2014.ChartExPart;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class ChartWrapper {

    private final Chart chart;
    private final ChartExPart chart2014;
    private final OfficeOpenXMLOperationDocument operationDocument;

    
    public OfficeOpenXMLOperationDocument getOperationDocument() {
        return operationDocument;
    }

    public ChartWrapper(Chart chart, OfficeOpenXMLOperationDocument operationDocument) {
        if (chart == null) {
            throw new RuntimeException("chart cant be null");
        }
        if (operationDocument == null) {
            throw new RuntimeException("operationDocument cant be null");
        }

        this.chart = chart;
        this.chart2014 = null;
        this.operationDocument = operationDocument;
    }

    public ChartWrapper(ChartExPart chart2014, OfficeOpenXMLOperationDocument operationDocument) {
        if (chart2014 == null) {
            throw new RuntimeException("chart cant be null");
        }
        if (operationDocument == null) {
            throw new RuntimeException("operationDocument cant be null");
        }
        this.chart = null;
        this.chart2014 = chart2014;
        this.operationDocument = operationDocument;
    }

    public CTChartStyle getChartStyle() {
        CTChartStyle result = chart.getChartStyle();
        if (result == null) {
            /* final CTChartSpace chartSpace = getChartSpace(); */
            result = ChartStyleDefaults.get(/* chartSpace.getStyle().getVal() */);
        }
        return result;
    }

    public CTColorStyle getChartColorStyle() {
        return chart.getChartColorStyle();
    }

    public CTChartSpace getChartSpace() {
        return chart.getJaxbElement();
    }

    public Chart getChartPart() {
        return chart;
    }

    public ThemePart getThemePart() throws FilterException {
        return operationDocument.getThemePart(true);
    }

    public CTPlotArea getPlotArea() {
        return getChartSpace().getChart().getPlotArea();
    }

    public ChartExPart getChartPart2014() {
        return chart2014;
    }

    public void insertChartSpace(int series, JSONObject attrs) throws Exception {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.insertChartSpace()");
        } else {
            DMLChartSpace.insertChartSpace(this, series, attrs);
        }
    }

    public void setDataSeriesAttributes(int series, JSONObject attrs) throws Exception {
        if (chart2014 != null) {
            DMLChartSpace2014.setDataSeriesAttributes(this, series, attrs);
        } else {
            DMLChartSpace.setDataSeriesAttributes(this, series, attrs);
        }
    }

    public void deleteChartSpace(int series) {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.deleteChartSpace()");
        } else {
            DMLChartSpace.deleteChartSpace(this, series);
        }
    }

    public void setAxisAttributes(long axisId, long crossAxId, String axPos, boolean zAxis, JSONObject attrs) throws InvalidFormatException, PartUnrecognisedException, JSONException {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.setAxisAttributes()");
        } else {
            DMLChartSpace.setAxisAttributes(this, axisId, crossAxId, axPos, zAxis, attrs);
        }

    }

    public void setChartGridlineAttributes(long axisId, JSONObject attrs) throws Exception {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.setChartGridlineAttributes()");
        } else {
            DMLChartSpace.setChartGridlineAttributes(this, axisId, attrs);
        }
    }

    public void setChartTitleAttributes(Long axisId, JSONObject attrs) throws JSONException {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.setChartTitleAttributes()");
        } else {
            if (axisId == null) {
                final CTChartSpace chartSpace = this.getChartSpace();
                final CTChart ctChart = chartSpace.getChart();
                CTTitle title = ctChart.getTitle();
                if (title == null) {
                    title = new CTTitle();
                    ctChart.setTitle(title);
                }
                DMLChartSpace.setTitleFromAttrs(title, attrs);
                ctChart.setAutoTitleDeleted(new CTBoolean(false));
            } else {
                DMLChartSpace.setChartTitleAttributes(this, axisId, attrs);
            }
        }
    }

    public void setLegendFromAttrs(JSONObject attrs) throws JSONException {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.setLegendFromAttrs()");
        } else {
            DMLChartSpace.setLegendFromAttrs(this, attrs);
        }

    }

    public void createChartSpaceOperations(JSONArray operationsArray, List<Integer> position) throws Exception {
        if (chart2014 != null) {
            DMLChartSpace2014.createChartSpaceOperations(operationsArray, position, this);
        } else {
            DMLChartSpace.createChartSpaceOperations(operationsArray, position, this);
        }
    }

    public void applyChartSpaceProperties(String chartType, JSONObject chartProperties, JSONObject fill) throws Exception {
        if (chart2014 != null) {
            System.out.println("ChartWrapper.applyChartSpaceProperties()");
        } else {
            DMLChartSpace.applyChartSpaceProperties(chartType, chartProperties, this, fill);
        }
    }

    public void createJSONAttrs(JSONObject chartProperties, HashMap<String, Object> drawingProperties, JSONObject attrs) throws JSONException {
        if (chart2014 != null) {

            // https://bugs.open-xchange.com/show_bug.cgi?id=39376 change styleset!!!!!!

            chartProperties.put(OCKey.CHART_STYLE_ID.value(), 2);
            chartProperties.put(OCKey.VARY_COLORS.value(), true);
        } else {
            final CTChartSpace cs = getChartSpace();
            final CTStyle chartStyle = cs.getStyle();
            if (chartStyle != null) {
                chartProperties.put(OCKey.CHART_STYLE_ID.value(), chartStyle.getVal());
            } else {
                //2 is the default style of excel
                chartProperties.put(OCKey.CHART_STYLE_ID.value(), 2);
            }
            final String chartGroup = (String) drawingProperties.get(OCKey.CHART_GROUP.value());
            if (chartGroup != null) {
                chartProperties.putOpt(OCKey.STACKING.value(), chartGroup);
            }
            final CTColorStyle colorStyle = getChartColorStyle();
            if (colorStyle != null) {
                final JSONObject jsonChartColors = DMLChartSpace.createJsonFromChartColor(colorStyle);
                if (jsonChartColors != null) {
                    chartProperties.put(OCKey.CHART_COLORS.value(), jsonChartColors);
                }
            }

            DMLHelper.createJsonFromShapeProperties(attrs, cs.getChart().getPlotArea().getSpPr(), getThemePart(), getChartStyle().getPlotArea(), getChartPart(), false, true);

            if (attrs.has(OCKey.FILL.value())) {
                // phClr color while load leads to black color while saving!
                JSONObject fill = attrs.optJSONObject(OCKey.FILL.value());
                if (fill.has(OCKey.COLOR.value())) {
                    JSONObject color = fill.optJSONObject(OCKey.COLOR.value());
                    if (color.optString(OCKey.TYPE.value()).equals("scheme") && color.optString(OCKey.VALUE.value()).equals("phClr")) {
                        attrs.remove(OCKey.FILL.value());
                    }
                }
            }
        }
    }

    public void getChartType(HashMap<String, Object> map) {
        if (chart2014 != null) {
            DMLChartSpace2014.getChartType(this, map);
        } else {
            DMLChartSpace.getChartType(this, map);
        }
    }
}
