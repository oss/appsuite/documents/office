/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.docx4j.dml.CTAdjPoint2D;
import org.docx4j.dml.CTAdjustHandleList;
import org.docx4j.dml.CTConnectionSite;
import org.docx4j.dml.CTConnectionSiteList;
import org.docx4j.dml.CTCustomGeometry2D;
import org.docx4j.dml.CTGeomGuide;
import org.docx4j.dml.CTGeomGuideList;
import org.docx4j.dml.CTGeomRect;
import org.docx4j.dml.CTPath2D;
import org.docx4j.dml.CTPath2DArcTo;
import org.docx4j.dml.CTPath2DClose;
import org.docx4j.dml.CTPath2DCubicBezierTo;
import org.docx4j.dml.CTPath2DLineTo;
import org.docx4j.dml.CTPath2DList;
import org.docx4j.dml.CTPath2DMoveTo;
import org.docx4j.dml.CTPath2DQuadBezierTo;
import org.docx4j.dml.CTPolarAdjustHandle;
import org.docx4j.dml.CTPresetGeometry2D;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTXYAdjustHandle;
import org.docx4j.dml.IAvList;
import org.docx4j.dml.STPathFillMode;
import org.docx4j.jaxb.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.core.Tools;

final public class DMLGeometry {

    public static void applyGeometryPropertiesFromJson(JSONObject geometryAttrs, CTShapeProperties shapeProperties) throws JSONException {
        final String presetShape = geometryAttrs.optString(OCKey.PRESET_SHAPE.value(), null);
        if(presetShape!=null) {
            if(presetShape==JSONObject.NULL) {
                shapeProperties.setPrstGeom(null);
            }
            else {
                shapeProperties.getPrstGeom(true).setPrst(STShapeType.fromValue(presetShape));
                shapeProperties.setCustGeom(null);
            }
        }
        final JSONArray pathList = geometryAttrs.optJSONArray(OCKey.PATH_LIST.value());
        if((pathList==JSONObject.NULL||pathList==null)&&shapeProperties.getPrstGeom(false)!=null) {
            applyPresetGeometryPropertiesFromJson(geometryAttrs, shapeProperties.getPrstGeom(true));
        }
        else {
            applyCustomGeometryPropertiesFromJson(geometryAttrs, shapeProperties.getCustGeom(true));
        }
    }

	public static void applyPresetGeometryPropertiesFromJson(JSONObject geometryAttrs, CTPresetGeometry2D presetGeometry) {
		final Object avList = geometryAttrs.opt(OCKey.AV_LIST.value());
		if(avList!=null) {
			applyAvListFromJson(avList, presetGeometry);
		}
	}

	public static void applyCustomGeometryPropertiesFromJson(JSONObject geometryAttrs, CTCustomGeometry2D customGeometry) throws JSONException {
		final Object avList = geometryAttrs.opt(OCKey.AV_LIST.value());
		if(avList!=null) {
			applyAvListFromJson(avList, customGeometry);
		}
		final Object ahList = geometryAttrs.opt(OCKey.AH_LIST.value());
		if(ahList!=null) {
		    applyAhListFromJson(ahList, customGeometry);
		}
		final Object gdList = geometryAttrs.opt(OCKey.GD_LIST.value());
		if(gdList!=null) {
		    applyGuideListFromJson(gdList, customGeometry);
		}
		final Object pathList = geometryAttrs.opt(OCKey.PATH_LIST.value());
		if(pathList!=null) {
		    applyPathListFromJson(pathList, customGeometry);
		}
		final Object connectList = geometryAttrs.opt(OCKey.CXN_LIST.value());
		if(connectList!=null) {
		    applyCxnListFromJson(connectList, customGeometry);
		}
		final Object textRect = geometryAttrs.opt(OCKey.TEXT_RECT.value());
		if(textRect!=null) {
		    applyTextRectFromJson(textRect, customGeometry);
		}
	}

	public static void applyAvListFromJson(Object jsonObject, IAvList avList) {
		if(jsonObject instanceof JSONObject) {
			CTGeomGuideList geometryList = avList.getAvLst();
			if(geometryList==null) {
				geometryList = Context.getDmlObjectFactory().createCTGeomGuideList();
				avList.setAvLst(geometryList);
			}
			final List<CTGeomGuide> gdList = geometryList.getGd();
			final Iterator<Entry<String, Object>> avValueIter = ((JSONObject)jsonObject).entrySet().iterator();
			while(avValueIter.hasNext()) {
				final Entry<String, Object> avValueEntry = avValueIter.next();
				final String avName = avValueEntry.getKey();
				final Object avValue = avValueEntry.getValue();

				int i;
				for(i = 0; i < gdList.size(); i++) {
					final CTGeomGuide geomGuide = gdList.get(i);
					if(geomGuide.getName().equals(avName)) {
						if(avValue instanceof Number) {
							geomGuide.setFmla("val " + Long.valueOf(((Number)avValue).longValue()).toString());
						}
						else if(avValue == JSONObject.NULL) {
							gdList.remove(i);
						}
						break;
					}
				}
				if(i == gdList.size()) {
					if(avValue instanceof Number) {
						final CTGeomGuide geomGuide = Context.getDmlObjectFactory().createCTGeomGuide();
						geomGuide.setName(avName);
						geomGuide.setFmla("val " + Long.valueOf(((Number)avValue).longValue()).toString());
						gdList.add(geomGuide);
					}
				}
			}
		}
		else if(jsonObject == JSONObject.NULL) {
			avList.setAvLst(null);
		}
	}

	public static void applyAhListFromJson(Object json, CTCustomGeometry2D customGeometry)
	    throws JSONException {

	    if(json instanceof JSONArray) {
	        final CTAdjustHandleList adjustHandleList = new CTAdjustHandleList();
	        customGeometry.setAhLst(adjustHandleList);
	        final JSONArray lst = (JSONArray)json;
	        for(Object o:lst) {
	            if(o instanceof JSONObject) {
	                final JSONObject ahObject = (JSONObject)o;
                    final CTAdjPoint2D point2D = new CTAdjPoint2D();
                    point2D.setX(ahObject.get(OCKey.X.value()).toString());
                    point2D.setY(ahObject.get(OCKey.Y.value()).toString());
	                final String type = ahObject.getString(OCKey.TYPE.value());
	                if(type.equals("xy")) {
	                    final CTXYAdjustHandle xyAdjustHandle = new CTXYAdjustHandle();
	                    xyAdjustHandle.setPos(point2D);
	                    final Object gdRefX = ahObject.opt(OCKey.GD_REF_X.value());
	                    if(gdRefX instanceof String) {
	                        xyAdjustHandle.setGdRefX((String)gdRefX);
	                    }
	                    else if(gdRefX==JSONObject.NULL) {
	                        xyAdjustHandle.setGdRefX(null);
	                    }
	                    final Object minX = ahObject.opt(OCKey.MIN_X.value());
	                    if(minX==JSONObject.NULL) {
	                        xyAdjustHandle.setMinX(null);
	                    }
	                    else if(minX!=null) {
	                        xyAdjustHandle.setMinX(minX.toString());
	                    }
                        final Object maxX = ahObject.opt(OCKey.MAX_X.value());
                        if(maxX==JSONObject.NULL) {
                            xyAdjustHandle.setMaxX(null);
                        }
                        else if(maxX!=null) {
                            xyAdjustHandle.setMaxX(maxX.toString());
                        }
                        final Object gdRefY = ahObject.opt(OCKey.GD_REF_Y.value());
                        if(gdRefY instanceof String) {
                            xyAdjustHandle.setGdRefY((String)gdRefY);
                        }
                        else if(gdRefY==JSONObject.NULL) {
                            xyAdjustHandle.setGdRefY(null);
                        }
                        final Object minY = ahObject.opt(OCKey.MIN_Y.value());
                        if(minY==JSONObject.NULL) {
                            xyAdjustHandle.setMinY(null);
                        }
                        else if(minY!=null) {
                            xyAdjustHandle.setMinY(minY.toString());
                        }
                        final Object maxY = ahObject.opt(OCKey.MAX_Y.value());
                        if(maxY==JSONObject.NULL) {
                            xyAdjustHandle.setMaxY(null);
                        }
                        else if(maxY!=null) {
                            xyAdjustHandle.setMaxY(maxY.toString());
                        }

	                }
	                else if(type.equals("polar")) {
	                    final CTPolarAdjustHandle polarAdjustHandle = new CTPolarAdjustHandle();
	                    polarAdjustHandle.setPos(point2D);
                        final Object gdRefR = ahObject.opt(OCKey.GD_REF_R.value());
                        if(gdRefR instanceof String) {
                            polarAdjustHandle.setGdRefR((String)gdRefR);
                        }
                        else if(gdRefR==JSONObject.NULL) {
                            polarAdjustHandle.setGdRefR(null);
                        }
                        final Object minR = ahObject.opt(OCKey.MIN_R.value());
                        if(minR==JSONObject.NULL) {
                            polarAdjustHandle.setMinR(null);
                        }
                        else if(minR!=null) {
                            polarAdjustHandle.setMinR(minR.toString());
                        }
                        final Object maxR = ahObject.opt(OCKey.MAX_R.value());
                        if(maxR==JSONObject.NULL) {
                            polarAdjustHandle.setMaxR(null);
                        }
                        else if(maxR!=null) {
                            polarAdjustHandle.setMaxR(maxR.toString());
                        }
                        final Object gdRefAng = ahObject.opt(OCKey.GD_REF_ANG.value());
                        if(gdRefAng instanceof String) {
                            polarAdjustHandle.setGdRefAng((String)gdRefAng);
                        }
                        else if(gdRefAng==JSONObject.NULL) {
                            polarAdjustHandle.setGdRefAng(null);
                        }
                        final Object minAng = ahObject.opt(OCKey.MIN_ANG.value());
                        if(minAng==JSONObject.NULL) {
                            polarAdjustHandle.setMinAng(null);
                        }
                        else if(minAng!=null) {
                            polarAdjustHandle.setMinAng(minAng.toString());
                        }
                        final Object maxAng = ahObject.opt(OCKey.MAX_ANG.value());
                        if(maxAng==JSONObject.NULL) {
                            polarAdjustHandle.setMaxAng(null);
                        }
                        else if(maxAng!=null) {
                            polarAdjustHandle.setMaxAng(maxAng.toString());
                        }
	                }
	            }
	        }
	    }
	    else if(json==JSONObject.NULL) {
	        customGeometry.setAhLst(null);
	    }
	}

	public static void applyGuideListFromJson(Object json, CTCustomGeometry2D customGeometry) throws JSONException {
        if(json instanceof JSONArray) {
            final List<CTGeomGuide> geomGuideList = new ArrayList<CTGeomGuide>(((JSONArray)json).length());
            for(int i=0; i<((JSONArray)json).length(); i++) {
                final CTGeomGuide geomGuide = new CTGeomGuide();
                final JSONObject o = ((JSONArray)json).getJSONObject(i);
                geomGuide.setName(o.getString(OCKey.NAME.value()));
                final StringBuffer buffer = new StringBuffer();
                buffer.append(o.getString(OCKey.OP.value()));
                final Object p0 = o.get(OCKey.P0.value());
                if(p0!=null) {
                    buffer.append(' ');
                    buffer.append(p0);
                    final Object p1 = o.get(OCKey.P1.value());
                    if(p1!=null) {
                        buffer.append(' ');
                        buffer.append(p1);
                        final Object p2 = o.get(OCKey.P2.value());
                        if(p2!=null) {
                            buffer.append(' ');
                            buffer.append(p2);
                        }
                    }
                }
                geomGuide.setFmla(buffer.toString());
                geomGuideList.add(geomGuide);
            }
            customGeometry.setGdLst(new CTGeomGuideList(geomGuideList));
        }
        else if(json == JSONObject.NULL) {
            customGeometry.setGdLst(null);
        }
    }

    public static void applyPathListFromJson(Object json, CTCustomGeometry2D customGeometry) throws JSONException {
        if(json instanceof JSONArray) {
            final List<CTPath2D> path2DList = new ArrayList<CTPath2D>(((JSONArray)json).length());
            for(int i=0; i<((JSONArray)json).length(); i++) {
                final JSONObject o = ((JSONArray)json).getJSONObject(i);
                final CTPath2D path2D = new CTPath2D();
                path2DList.add(path2D);
                final String fillMode = o.optString(OCKey.FILL_MODE.value());
                if(fillMode!=null) {
                	try {
                		path2D.setFill(STPathFillMode.fromValue(fillMode));
                	}
                	catch(IllegalArgumentException e) {
                		//
                	}
                }
                final Object width = o.opt(OCKey.WIDTH.value());
                if(width instanceof Number) {
                	path2D.setW(((Number)width).longValue());
                }
                final Object height = o.opt(OCKey.HEIGHT.value());
                if(height instanceof Number) {
                	path2D.setH(((Number)height).longValue());
                }
                final boolean isStroke = o.optBoolean(OCKey.IS_STROKE.value(), true);
                if(!isStroke) {
                	path2D.setStroke(false);
                }
                final boolean isExtrusionOk = o.optBoolean(OCKey.IS_EXTRUSION_OK.value(), true);
    			if(!isExtrusionOk) {
    				path2D.setExtrusionOk(false);
    			}
    			final JSONArray commands = o.optJSONArray(OCKey.COMMANDS.value());
    			if(commands!=null) {
    				final List<Object> cList = path2D.getCloseOrMoveToOrLnTo();
    				for(Object c:commands) {
    					if(c instanceof JSONObject) {
    						final JSONObject cObj = (JSONObject)c;
    						final String command = cObj.optString(OCKey.C.value());
    						if(command!=null) {
    							switch(command) {
    								case "lineTo" : {
    									final CTPath2DLineTo path2DLineTo = new CTPath2DLineTo();
    									final CTAdjPoint2D point2D = new CTAdjPoint2D();
    									path2DLineTo.setPt(point2D);
    									point2D.setX(cObj.get(OCKey.X.value()).toString());
    									point2D.setY(cObj.get(OCKey.Y.value()).toString());
    									cList.add(path2DLineTo);
    									break;
    								}
    								case "arcTo" : {
    									final CTPath2DArcTo path2DArcTo = new CTPath2DArcTo();
    									path2DArcTo.setHR(cObj.get(OCKey.HR.value()).toString());
    									path2DArcTo.setWR(cObj.get(OCKey.WR.value()).toString());
    									path2DArcTo.setStAng(cObj.get(OCKey.ST_ANG.value()).toString());
    									path2DArcTo.setSwAng(cObj.get(OCKey.SW_ANG.value()).toString());
    									cList.add(path2DArcTo);
    									break;
    								}
    								case "moveTo" : {
    									final CTPath2DMoveTo path2DMoveTo = new CTPath2DMoveTo();
    									final CTAdjPoint2D point2D = new CTAdjPoint2D();
    									path2DMoveTo.setPt(point2D);
    									point2D.setX(cObj.get(OCKey.X.value()).toString());
    									point2D.setY(cObj.get(OCKey.Y.value()).toString());
    									cList.add(path2DMoveTo);
    									break;
    								}
    								case "close" : {
    									cList.add(new CTPath2DClose());
    									break;
    								}
    								case "quadBezierTo" : {
    									final CTPath2DQuadBezierTo path2DQuadBezierTo = new CTPath2DQuadBezierTo();
    									final JSONArray pts = cObj.getJSONArray(OCKey.PTS.value());
    									final List<CTAdjPoint2D> ptList = path2DQuadBezierTo.getPt();
    									for(Object pt:pts) {
        									final CTAdjPoint2D point2D = new CTAdjPoint2D();
        									point2D.setX(((JSONObject)pt).get(OCKey.X.value()).toString());
        									point2D.setY(((JSONObject)pt).get(OCKey.Y.value()).toString());
        									ptList.add(point2D);
    									}
    									cList.add(path2DQuadBezierTo);
    									break;
    								}
    								case "cubicBezierTo" : {
    									final CTPath2DCubicBezierTo path2DCubicBezierTo = new CTPath2DCubicBezierTo();
    									final JSONArray pts = cObj.getJSONArray(OCKey.PTS.value());
    									final List<CTAdjPoint2D> ptList = path2DCubicBezierTo.getPt();
    									for(Object pt:pts) {
        									final CTAdjPoint2D point2D = new CTAdjPoint2D();
        									point2D.setX(((JSONObject)pt).get(OCKey.X.value()).toString());
        									point2D.setY(((JSONObject)pt).get(OCKey.Y.value()).toString());
        									ptList.add(point2D);
    									}
    									cList.add(path2DCubicBezierTo);
    									break;
    								}
    							}
    						}
    					}
    				}
    			}
            }
            customGeometry.setPathLst(new CTPath2DList(path2DList));
        }
        else if(json==JSONObject.NULL) {
            customGeometry.setPathLst(null);
        }
    }

    public static void applyCxnListFromJson(Object json, CTCustomGeometry2D customGeometry)
        throws JSONException {

        if(json instanceof JSONArray) {
            final CTConnectionSiteList connectionList = new CTConnectionSiteList();
            customGeometry.setCxnLst(connectionList);
            for(Object o:(JSONArray)json) {
                final CTConnectionSite connectionSite = new CTConnectionSite();
                connectionList.getCxn().add(connectionSite);
                final CTAdjPoint2D point2D = new CTAdjPoint2D();
                point2D.setX(((JSONObject)o).get(OCKey.X.value()).toString());
                point2D.setY(((JSONObject)o).get(OCKey.Y.value()).toString());
                connectionSite.setPos(point2D);
                final Object ang = ((JSONObject)o).opt(OCKey.ANG.value());
                if(ang==JSONObject.NULL) {
                    connectionSite.setAng(null);
                }
                else {
                    connectionSite.setAng(ang.toString());
                }
            }
        }
        else if(json==JSONObject.NULL) {
            customGeometry.setCxnLst(null);
        }
    }

    public static void applyTextRectFromJson(Object json, CTCustomGeometry2D customGeometry) throws JSONException {
        if(json instanceof JSONObject) {
        	final CTGeomRect rect = new CTGeomRect();
			rect.setB(((JSONObject)json).get(OCKey.B.value()).toString());
			rect.setL(((JSONObject)json).get(OCKey.L.value()).toString());
			rect.setR(((JSONObject)json).get(OCKey.R.value()).toString());
			rect.setT(((JSONObject)json).get(OCKey.T.value()).toString());
        	customGeometry.setRect(rect);
        }
        else if(json == JSONObject.NULL) {
            customGeometry.setRect(null);
        }
    }

    public static void createJsonFromPresetGeometry(JSONObject attrs, CTPresetGeometry2D presetGeometry)
    	throws JSONException {

        if(presetGeometry!=null) {
            final STShapeType presetShapeType = presetGeometry.getPrst();
            if(presetShapeType!=null) {
                Tools.addFamilyAttribute(attrs, OCKey.GEOMETRY, OCKey.PRESET_SHAPE, presetShapeType.value());
            }
            if(presetShapeType!=null) {
                createJsonFromAvList(attrs.optJSONObject(OCKey.GEOMETRY.value()), presetGeometry.getAvLst(), false);
            }
        }
    }

    public static void createJsonFromCustomGeometry(JSONObject attrs, CTCustomGeometry2D customGeometry)
    	throws JSONException {

    	if(customGeometry!=null) {

    		final JSONObject initialGeometryAttrs = attrs.optJSONObject(OCKey.GEOMETRY.value());
    		final JSONObject geometryAttrs = initialGeometryAttrs!=null ? initialGeometryAttrs : new JSONObject(4);
    		createJsonFromAvList(geometryAttrs, customGeometry.getAvLst(), false);
            createJsonFromAhList(geometryAttrs, customGeometry.getAhLst(), false);
    		createJsonFromGuideList(geometryAttrs, customGeometry.getGdLst(), false);
    		createJsonFromPathList(geometryAttrs, customGeometry.getPathLst(), false);
            createJsonFromConnectList(geometryAttrs, customGeometry.getCxnLst(), false);
    		createJsonFromRect(geometryAttrs, customGeometry.getRect(), false);
    		if(initialGeometryAttrs==null&&!geometryAttrs.isEmpty()) {
    			attrs.put(OCKey.GEOMETRY.value(), geometryAttrs);
    		}
    	}
    }

    public static JSONObject createJSPresetGeometries(boolean useLongKeyName)
    	throws JSONException {

    	final JSONObject allPresets = new JSONObject(1);
    	final Iterator<Entry<STShapeType, CTCustomGeometry2D>> customShapePresetIter = Context.getCustomShapePresets().entrySet().iterator();
    	while(customShapePresetIter.hasNext()) {
    		final Entry<STShapeType, CTCustomGeometry2D> customShapePreset = customShapePresetIter.next();
    		final JSONObject preset = new JSONObject(6);
    		final CTCustomGeometry2D customGeometry = customShapePreset.getValue();
    		createJsonFromAvList(preset, customGeometry.getAvLst(), useLongKeyName);
    		createJsonFromAhList(preset, customGeometry.getAhLst(), useLongKeyName);
    		createJsonFromGuideList(preset, customGeometry.getGdLst(), useLongKeyName);
    		createJsonFromPathList(preset, customGeometry.getPathLst(), useLongKeyName);
    		createJsonFromConnectList(preset, customGeometry.getCxnLst(), useLongKeyName);
    		createJsonFromRect(preset, customGeometry.getRect(), useLongKeyName);
    		allPresets.put(customShapePreset.getKey().value(), preset);
    	}
    	return allPresets;
    }

    public static void createJsonFromAvList(JSONObject geometryAttrs, CTGeomGuideList geomGuides, boolean useLongKeyName)
    	throws JSONException {

        if(geomGuides!=null) {
    		final List<CTGeomGuide> geomGuideList = geomGuides.getGd();
    		JSONObject lst = geometryAttrs.optJSONObject(OCKey.AV_LIST.value(useLongKeyName));
    		if(lst==null) {
    			lst = new JSONObject(geomGuideList.size());
        		geometryAttrs.put(OCKey.AV_LIST.value(useLongKeyName), lst);
    		}
    		for(CTGeomGuide geomGuide:geomGuideList) {
    			if(geomGuide.getName()!=null&&geomGuide.getFmla()!=null) {
    				lst.put(geomGuide.getName(), Long.valueOf(geomGuide.getFmla().substring(4)));
    			}
    		}
    	}
    }

    public static void createJsonFromAhList(JSONObject geometryAttrs, CTAdjustHandleList adjustHandles, boolean useLongKeyName)
        throws JSONException {

        if(adjustHandles!=null) {
            final List<Object> adjustHandleList = adjustHandles.getAhXYOrAhPolar();
            if(adjustHandleList!=null&&!adjustHandleList.isEmpty()) {
                final JSONArray lst = new JSONArray(adjustHandleList.size());
                for(Object o:adjustHandleList) {
                    if(o instanceof CTXYAdjustHandle) {
                        final CTXYAdjustHandle xYAdjustHandle = (CTXYAdjustHandle)o;
                        final JSONObject xy = new JSONObject();
                        xy.put(OCKey.TYPE.value(useLongKeyName), "xy");
                        xy.put(OCKey.X.value(useLongKeyName), xYAdjustHandle.getPos().getParsedX());
                        xy.put(OCKey.Y.value(useLongKeyName), xYAdjustHandle.getPos().getParsedY());
                        if(xYAdjustHandle.getGdRefX()!=null) {
                            xy.put(OCKey.GD_REF_X.value(useLongKeyName), xYAdjustHandle.getGdRefX());
                        }
                        if(xYAdjustHandle.getMinX()!=null) {
                            xy.put(OCKey.MIN_X.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(xYAdjustHandle.getMinX()));
                        }
                        if(xYAdjustHandle.getMaxX()!=null) {
                            xy.put(OCKey.MAX_X.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(xYAdjustHandle.getMaxX()));
                        }
                        if(xYAdjustHandle.getGdRefY()!=null) {
                            xy.put(OCKey.GD_REF_Y.value(useLongKeyName), xYAdjustHandle.getGdRefY());
                        }
                        if(xYAdjustHandle.getMinY()!=null) {
                            xy.put(OCKey.MIN_Y.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(xYAdjustHandle.getMinY()));
                        }
                        if(xYAdjustHandle.getMaxY()!=null) {
                            xy.put(OCKey.MAX_Y.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(xYAdjustHandle.getMaxY()));
                        }
                        lst.put(xy);
                    }
                    else if(o instanceof CTPolarAdjustHandle) {
                        final CTPolarAdjustHandle polarAdjustHandle = (CTPolarAdjustHandle)o;
                        final JSONObject polar = new JSONObject();
                        polar.put(OCKey.TYPE.value(useLongKeyName), "polar");
                        polar.put(OCKey.X.value(useLongKeyName), polarAdjustHandle.getPos().getParsedX());
                        polar.put(OCKey.Y.value(useLongKeyName), polarAdjustHandle.getPos().getParsedY());
                        if(polarAdjustHandle.getGdRefR()!=null) {
                            polar.put(OCKey.GD_REF_R.value(useLongKeyName), polarAdjustHandle.getGdRefR());
                        }
                        if(polarAdjustHandle.getMinR()!=null) {
                            polar.put(OCKey.MIN_R.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(polarAdjustHandle.getMinR()));
                        }
                        if(polarAdjustHandle.getMaxR()!=null) {
                            polar.put(OCKey.MAX_R.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(polarAdjustHandle.getMaxR()));
                        }
                        if(polarAdjustHandle.getGdRefAng()!=null) {
                            polar.put(OCKey.GD_REF_ANG.value(useLongKeyName), polarAdjustHandle.getGdRefAng());
                        }
                        if(polarAdjustHandle.getMinAng()!=null) {
                            polar.put(OCKey.MIN_ANG.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(polarAdjustHandle.getMinAng()));
                        }
                        if(polarAdjustHandle.getMaxAng()!=null) {
                            polar.put(OCKey.MAX_ANG.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(polarAdjustHandle.getMaxAng()));
                        }
                        lst.put(polar);
                    }
                }
                geometryAttrs.put(OCKey.AH_LIST.value(useLongKeyName), lst);
            }
        }
    }

    public static void createJsonFromGuideList(JSONObject geometryAttrs, CTGeomGuideList geomGuides, boolean useLongKeyName)
    	throws JSONException {

    	if(geomGuides!=null) {
    		final List<CTGeomGuide> geomGuideList = geomGuides.getGd();
    		final JSONArray lst = new JSONArray(geomGuideList.size());
    		for(CTGeomGuide geomGuide:geomGuideList) {
    			final Object[] p = geomGuide.getParsedFmla();
    			final JSONObject gd = new JSONObject(1 + p.length);
    			if(geomGuide.getName()!=null) {
    				gd.put(OCKey.NAME.value(useLongKeyName), geomGuide.getName());
    			}
   				gd.put(OCKey.OP.value(useLongKeyName), p[0]);
   				if(p.length>1) {
   				    gd.put(OCKey.P0.value(useLongKeyName), p[1]);
   				}
   				if(p.length>2) {
   				    gd.put(OCKey.P1.value(useLongKeyName), p[2]);
   				}
   				if(p.length>3) {
   				    gd.put(OCKey.P2.value(useLongKeyName), p[3]);
   				}
   				lst.put(gd);
    		}
    		geometryAttrs.put(OCKey.GD_LIST.value(useLongKeyName), lst);
    	}
    }

    public static void createJsonFromPathList(JSONObject geometryAttrs, CTPath2DList pathes, boolean useLongKeyName)
    	throws JSONException {

    	if(pathes!=null) {
    		final List<CTPath2D> pathList = pathes.getPath();
    		final JSONArray jsonPathList = new JSONArray(pathList.size());
    		for(CTPath2D path:pathList) {
    			final JSONObject jsonPath = new JSONObject();
    			final STPathFillMode pathFillMode = path.getFill();
    			if(pathFillMode!=null) {
    				jsonPath.put(OCKey.FILL_MODE.value(useLongKeyName), pathFillMode.value());
    			}
    			jsonPath.put(OCKey.WIDTH.value(useLongKeyName), path.getW());
    			jsonPath.put(OCKey.HEIGHT.value(useLongKeyName), path.getH());
    			if(path.isStroke()==false) {
    				jsonPath.put(OCKey.IS_STROKE.value(useLongKeyName), false);
    			}
    			if(path.isExtrusionOk()==false) {
    				jsonPath.put(OCKey.IS_EXTRUSION_OK.value(useLongKeyName), false);
    			}
    			final List<Object> commands = path.getCloseOrMoveToOrLnTo();
    			final JSONArray jsonCommands = new JSONArray(commands.size());
    			for(Object command:commands) {
    				if(command instanceof CTPath2DLineTo) {
    					final CTPath2DLineTo c = (CTPath2DLineTo)command;
    					final CTAdjPoint2D pt2D = c.getPt();
    					final JSONObject jsonLineTo = new JSONObject(3);
    					jsonLineTo.put(OCKey.C.value(useLongKeyName), "lineTo");
    					jsonLineTo.put(OCKey.X.value(useLongKeyName), pt2D.getParsedX());
    					jsonLineTo.put(OCKey.Y.value(useLongKeyName), pt2D.getParsedY());
    					jsonCommands.put(jsonLineTo);
    				}
    				else if(command instanceof CTPath2DArcTo) {
    					final CTPath2DArcTo c = (CTPath2DArcTo)command;
    					final JSONObject jsonArcTo = new JSONObject(5);
    					jsonArcTo.put(OCKey.C.value(useLongKeyName), "arcTo");
    					jsonArcTo.put(OCKey.HR.value(useLongKeyName), c.getParsedHR());
    					jsonArcTo.put(OCKey.WR.value(useLongKeyName), c.getParsedWR());
    					jsonArcTo.put(OCKey.ST_ANG.value(useLongKeyName), c.getParsedStAng());
    					jsonArcTo.put(OCKey.SW_ANG.value(useLongKeyName), c.getParsedSwAng());
    					jsonCommands.put(jsonArcTo);
    				}
    				else if(command instanceof CTPath2DMoveTo) {
    					final CTPath2DMoveTo c = (CTPath2DMoveTo)command;
    					final CTAdjPoint2D pt2D = c.getPt();
    					final JSONObject jsonMoveTo = new JSONObject(3);
    					jsonMoveTo.put(OCKey.C.value(useLongKeyName), "moveTo");
    					jsonMoveTo.put(OCKey.X.value(useLongKeyName), pt2D.getParsedX());
    					jsonMoveTo.put(OCKey.Y.value(useLongKeyName), pt2D.getParsedY());
    					jsonCommands.put(jsonMoveTo);
    				}
    				else if(command instanceof CTPath2DClose) {
    					final JSONObject jsonClose = new JSONObject(1);
    					jsonClose.put(OCKey.C.value(useLongKeyName), "close");
    					jsonCommands.put(jsonClose);
    				}
    				else if(command instanceof CTPath2DQuadBezierTo) {
    					final CTPath2DQuadBezierTo c = (CTPath2DQuadBezierTo)command;
    					final List<CTAdjPoint2D> pts2D = c.getPt();
    					final JSONObject jsonQuadBezierTo = new JSONObject(2);
    					jsonQuadBezierTo.put(OCKey.C.value(useLongKeyName), "quadBezierTo");
    					final JSONArray jsonPts = new JSONArray(pts2D.size());
    					for(CTAdjPoint2D pt:pts2D) {
        					final JSONObject jsonPt = new JSONObject(2);
    						jsonPt.put(OCKey.X.value(useLongKeyName), pt.getParsedX());
    						jsonPt.put(OCKey.Y.value(useLongKeyName), pt.getParsedY());
    						jsonPts.put(jsonPt);
    					}
    					jsonQuadBezierTo.put(OCKey.PTS.value(useLongKeyName), jsonPts);
    					jsonCommands.put(jsonQuadBezierTo);
    				}
    				else if(command instanceof CTPath2DCubicBezierTo) {
    					final CTPath2DCubicBezierTo c = (CTPath2DCubicBezierTo)command;
    					final List<CTAdjPoint2D> pts2D = c.getPt();
    					final JSONObject jsonCubicBezierTo = new JSONObject(2);
    					jsonCubicBezierTo.put(OCKey.C.value(useLongKeyName), "cubicBezierTo");
    					final JSONArray jsonPts = new JSONArray(pts2D.size());
    					for(CTAdjPoint2D pt:pts2D) {
        					final JSONObject jsonPt = new JSONObject(2);
    						jsonPt.put(OCKey.X.value(useLongKeyName), pt.getParsedX());
    						jsonPt.put(OCKey.Y.value(useLongKeyName), pt.getParsedY());
    						jsonPts.put(jsonPt);
    					}
    					jsonCubicBezierTo.put(OCKey.PTS.value(useLongKeyName), jsonPts);
    					jsonCommands.put(jsonCubicBezierTo);
    				}
    			}
    			jsonPath.put(OCKey.COMMANDS.value(useLongKeyName), jsonCommands);
    			jsonPathList.put(jsonPath);
    		}
    		geometryAttrs.put(OCKey.PATH_LIST.value(useLongKeyName), jsonPathList);
    	}
    }

    public static void createJsonFromConnectList(JSONObject geometryAttrs, CTConnectionSiteList connectionList, boolean useLongKeyName)
        throws JSONException {

        if(connectionList!=null) {
            final List<CTConnectionSite> connectionSiteList = connectionList.getCxn();
            if(!connectionSiteList.isEmpty()) {
                final JSONArray cxArray = new JSONArray(connectionSiteList.size());
                for(int i=0; i<connectionSiteList.size(); i++) {
                    final CTConnectionSite connectionSite = connectionSiteList.get(i);
                    final JSONObject c = new JSONObject(3);
                    c.put(OCKey.X.value(useLongKeyName), connectionSite.getPos().getParsedX());
                    c.put(OCKey.Y.value(useLongKeyName), connectionSite.getPos().getParsedY());
                    c.put(OCKey.ANG.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(connectionSite.getAng()));
                    cxArray.put(c);
                }
                geometryAttrs.put(OCKey.CXN_LIST.value(useLongKeyName), cxArray);
            }
        }
    }

    public static void createJsonFromRect(JSONObject geometryAttrs, CTGeomRect rect, boolean useLongKeyName)
    	throws JSONException {

    	if(rect!=null) {
    		final JSONObject textRect = new JSONObject(4);
    		if(rect.getL()!=null) {
    			textRect.put(OCKey.L.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(rect.getL()));
    		}
    		if(rect.getT()!=null) {
    			textRect.put(OCKey.T.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(rect.getT()));
    		}
    		if(rect.getR()!=null) {
    			textRect.put(OCKey.R.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(rect.getR()));
    		}
    		if(rect.getB()!=null) {
    			textRect.put(OCKey.B.value(useLongKeyName), CTAdjPoint2D.getLongIfPossible(rect.getB()));
    		}
    		if(!textRect.isEmpty()) {
    			geometryAttrs.put(OCKey.TEXT_RECT.value(useLongKeyName), textRect);
    		}
    	}
    }
}
