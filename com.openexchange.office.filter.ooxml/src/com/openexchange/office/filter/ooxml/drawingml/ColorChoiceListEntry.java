/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.util.List;

import org.docx4j.dml.CTHslColor;
import org.docx4j.dml.CTPresetColor;
import org.docx4j.dml.CTSRgbColor;
import org.docx4j.dml.CTScRgbColor;
import org.docx4j.dml.CTSchemeColor;
import org.docx4j.dml.CTSystemColor;
import org.docx4j.dml.IColorChoice;

public class ColorChoiceListEntry implements IColorChoice {

	final private List<Object> egChoiceList;
	final private int index;
	private Object o;

	public ColorChoiceListEntry(List<Object> egChoiceList, int index) {
		this.egChoiceList = egChoiceList;
		this.index = index;
		while(index >= egChoiceList.size()) {
		    egChoiceList.add(null);
		}
		o = egChoiceList.get(index);
	}

	@Override
	public CTScRgbColor getScrgbClr() {
		return o instanceof CTScRgbColor ? (CTScRgbColor)o : null;
	}

	@Override
	public void setScrgbClr(CTScRgbColor value) {
		o = value;
		egChoiceList.set(index, value);
	}

	@Override
	public CTSRgbColor getSrgbClr() {
		return o instanceof CTSRgbColor ? (CTSRgbColor)o : null;
	}
	@Override
	public void setSrgbClr(CTSRgbColor value) {
		o = value;
		egChoiceList.set(index, value);
	}

	@Override
	public CTHslColor getHslClr() {
		return o instanceof CTHslColor ? (CTHslColor)o : null;
	}

	@Override
	public void setHslClr(CTHslColor value) {
		o = value;
		egChoiceList.set(index, value);
	}

	@Override
	public CTSystemColor getSysClr() {
		return o instanceof CTSystemColor ? (CTSystemColor)o : null;
	}

	@Override
	public void setSysClr(CTSystemColor value) {
		o = value;
		egChoiceList.set(index, value);
	}

	@Override
	public CTSchemeColor getSchemeClr() {
		return o instanceof CTSchemeColor ? (CTSchemeColor)o : null;
	}

	@Override
	public void setSchemeClr(CTSchemeColor value) {
		o = value;
		egChoiceList.set(index, value);
	}

	@Override
	public CTPresetColor getPrstClr() {
		return o instanceof CTPresetColor ? (CTPresetColor)o : null;
	}

	@Override
	public void setPrstClr(CTPresetColor value) {
		o = value;
		egChoiceList.set(index, value);
	}
}
