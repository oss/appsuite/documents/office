/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import org.docx4j.dml.CTGroupShapeProperties;
import org.docx4j.dml.CTGroupTransform2D;
import org.docx4j.dml.IGroupShape;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;

public class GroupShape {

	public static void applyAttrsFromJSON(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, IGroupShape group, boolean rootShape)
		throws JSONException, InvalidFormatException, PartUnrecognisedException {

        DMLHelper.applyNonVisualDrawingProperties(group, attrs.optJSONObject(OCKey.DRAWING.value()));
        DMLHelper.applyNonVisualDrawingShapeProperties(group, attrs.optJSONObject(OCKey.DRAWING.value()));
	    CTGroupShapeProperties groupProperties = group.getGrpSpPr();
		if(groupProperties==null) {
			groupProperties = Context.getDmlObjectFactory().createCTGroupShapeProperties();
			group.setGrpSpPr(groupProperties);
		}
		DMLHelper.applyTransform2DFromJson(groupProperties, attrs, rootShape);
		DMLHelper.applyFillPropertiesFromJson(groupProperties, attrs, null, null);
	}

    public static void createJSONAttrs(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, IGroupShape groupShape, boolean rootShape)
    	throws JSONException {

        DMLHelper.createJsonFromNonVisualDrawingProperties(attrs, groupShape);
        DMLHelper.createJsonFromNonVisualDrawingShapeProperties(attrs, groupShape);

        final CTGroupShapeProperties groupProperties = groupShape.getGrpSpPr();
		if(groupProperties!=null) {
	        final CTGroupTransform2D groupTransform =groupProperties.getXfrm(false);
	        if(groupTransform!=null) {
	        	DMLHelper.createJsonFromTransform2D(attrs, groupTransform, rootShape);
	        }
		}
    }
}
