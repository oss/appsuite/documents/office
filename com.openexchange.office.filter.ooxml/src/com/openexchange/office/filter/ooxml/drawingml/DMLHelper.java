/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.drawingml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import org.apache.commons.codec.digest.MurmurHash3;
import org.docx4j.XmlUtils;
import org.docx4j.dml.*;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.URIHelper;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.io3.stores.PartStore;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.tools.Commons;
import com.openexchange.office.filter.ooxml.tools.ThemeFonts;
import com.openexchange.office.filter.ooxml.tools.json.JSONHelper;
import com.openexchange.office.imagemgr.DocumentImageHelper;

final public class DMLHelper {

    public static void createJsonFromPositiveSize2D(JSONObject drawingProperties, CTPositiveSize2D positiveSize2D)
            throws JSONException {

        if (positiveSize2D != null) {
            drawingProperties.put(OCKey.WIDTH.value(), Commons.coordinateTo100TH_MM(positiveSize2D.getCx()));
            drawingProperties.put(OCKey.HEIGHT.value(), Commons.coordinateTo100TH_MM(positiveSize2D.getCy()));
        }
    }

    public static JSONObject createJsonFromBlipFillProperties(Part contextPart, JSONObject bitmapAttrs,
            CTBlipFillProperties blipFillProperties, IStyleMatrixReference styleMatrixReference) throws JSONException {

        if (blipFillProperties != null) {
            bitmapAttrs.put(OCKey.IMAGE_URL.value(), getBlipUrl(contextPart, blipFillProperties.getBlip(false)));

            final Boolean rotateWithShape = blipFillProperties.isRotWithShape();
            if (rotateWithShape != null) {
                bitmapAttrs.put(OCKey.ROTATE_WITH_SHAPE.value(), rotateWithShape);
            }
            final CTRelativeRect srcRect = blipFillProperties.getSrcRect(false);
            if (srcRect != null) {
                final int l = srcRect.getL() / 1000;
                final int t = srcRect.getT() / 1000;
                final int r = srcRect.getR() / 1000;
                final int b = srcRect.getB() / 1000;

                // add possible crop properties
                if (l != 0)
                    bitmapAttrs.put(OCKey.CROP_LEFT.value(), l);
                if (t != 0)
                    bitmapAttrs.put(OCKey.CROP_TOP.value(), t);
                if (r != 0)
                    bitmapAttrs.put(OCKey.CROP_RIGHT.value(), r);
                if (b != 0)
                    bitmapAttrs.put(OCKey.CROP_BOTTOM.value(), b);
            }

            // blip effects
            final CTBlip blip = blipFillProperties.getBlip(false);
            if (blip != null) {
                final List<Object> blipEffects = blip.getAlphaBiLevelOrAlphaCeilingOrAlphaFloor();
                if(!blipEffects.isEmpty()) {
                    createJsonFromBlipFillEffects(blipEffects, bitmapAttrs, styleMatrixReference);
                }
            }

            // blip tiling
            if (blipFillProperties.getTile(false) != null) {
                final CTTileInfoProperties tiling = blipFillProperties.getTile(false);
                final JSONObject json = new JSONObject(6);
                final STRectAlignment align = tiling.getAlgn();
                if (align != null) {
                    String a = null;
                    switch (align) {
                    case B:
                        a = "bottom";
                        break;
                    case BL:
                        a = "bottomLeft";
                        break;
                    case BR:
                        a = "bottomRight";
                        break;
                    case CTR:
                        a = "center";
                        break;
                    case L:
                        a = "left";
                        break;
                    case R:
                        a = "right";
                        break;
                    case T:
                        a = "top";
                        break;
                    case TL:
                        a = "topLeft";
                        break;
                    case TR:
                        a = "topRight";
                        break;
                    }
                    if (a != null) {
                        json.put(OCKey.RECT_ALIGNMENT.value(), a);
                    }
                }
                final STTileFlipMode flip = tiling.getFlip();
                String flipMode = null;
                if (flip == STTileFlipMode.X) {
                    flipMode = "x";
                } else if (flip == STTileFlipMode.Y) {
                    flipMode = "y";
                } else if (flip == STTileFlipMode.XY) {
                    flipMode = "xy";
                } else if (flip == STTileFlipMode.NONE) {
                    flipMode = "none";
                }
                if (flipMode != null) {
                    json.put(OCKey.FLIP_MODE.value(), flipMode);
                }
                if (tiling.getSx() != null) {
                    json.put(OCKey.STRETCH_X.value(), ((double) tiling.getSx()) / 1000.0);
                }
                if (tiling.getSy() != null) {
                    json.put(OCKey.STRETCH_Y.value(), ((double) tiling.getSx()) / 1000.0);
                }
                if (tiling.getTx() != null) {
                    json.put(OCKey.OFF_X.value(), (int) convertEmuTo100thmm(tiling.getTx()));
                }
                if (tiling.getTy() != null) {
                    json.put(OCKey.OFF_Y.value(), (int) convertEmuTo100thmm(tiling.getTy()));
                }
                if (!json.isEmpty()) {
                    bitmapAttrs.put(OCKey.TILING.value(), json);
                }
            } else if (blipFillProperties.getStretch(false) != null) {
                final CTStretchInfoProperties stretching = blipFillProperties.getStretch(false);
                final JSONObject json = new JSONObject(4);
                final CTRelativeRect fillRect = stretching.getFillRect();
                if (fillRect != null) {
                    if (fillRect.getL() != 0) {
                        json.put(OCKey.LEFT.value(), fillRect.getL() / 1000.0);
                    }
                    if (fillRect.getT() != 0) {
                        json.put(OCKey.TOP.value(), fillRect.getT() / 1000.0);
                    }
                    if (fillRect.getR() != 0) {
                        json.put(OCKey.RIGHT.value(), fillRect.getR() / 1000.0);
                    }
                    if (fillRect.getB() != 0) {
                        json.put(OCKey.BOTTOM.value(), fillRect.getB() / 1000.0);
                    }
                }
                if (!json.isEmpty()) {
                    bitmapAttrs.put(OCKey.STRETCHING.value(), json);
                }
            }
        }
        return bitmapAttrs;
    }

    public static void createJsonFromBlipFillEffects(List<Object> blipEffects, JSONObject bitmapAttrs, IStyleMatrixReference styleMatrixReference) throws JSONException {
        final JSONArray effects = new JSONArray(blipEffects.size());
        final Iterator<Object> blipEffectIter = blipEffects.iterator();
        while (blipEffectIter.hasNext()) {
            final Object blipEffect = blipEffectIter.next();
            switch (blipEffect.getClass().getSimpleName()) {
                case "CTAlphaModulateFixedEffect": {
                    bitmapAttrs.put(OCKey.TRANSPARENCY.value(),
                            (100000 - ((CTAlphaModulateFixedEffect) blipEffect).getAmt()) / 100000.0);
                    break;
                }
                case "CTDuotoneEffect": {
                    final CTDuotoneEffect duotone = (CTDuotoneEffect)blipEffect;
                    if(duotone.getEGColorChoice().size() == 2) {
                        final JSONObject effect = new JSONObject();
                        effect.put(OCKey.TYPE.value(), "duotone");
                        JSONObject color1 = createJsonColorFromSolidColorFillProperties(new ColorChoiceListEntry(duotone.getEGColorChoice(),0));
                        JSONObject color2 = createJsonColorFromSolidColorFillProperties(new ColorChoiceListEntry(duotone.getEGColorChoice(),1));
                        if (styleMatrixReference != null) {
                            color1 = createJsonColorFromSolidColorFillProperties(color1, styleMatrixReference);
                            color2 = createJsonColorFromSolidColorFillProperties(color2, styleMatrixReference);
                        }
                        effect.put(OCKey.COLOR.value(), color1);
                        effect.put(OCKey.COLOR2.value(), color2);
                        effects.put(effect);
                    }
                    break;
                }
            }
        }
        if(!effects.isEmpty()) {
            bitmapAttrs.put(OCKey.EFFECTS.value(), effects);
        }
    }

    public static void applyGradFillPropertiesFromJson(CTGradientFillProperties gradientFill, JSONObject gradientAttrs) throws JSONException {
        final String gradientType = gradientAttrs.optString(OCKey.TYPE.value(), null);
        if (gradientType != null) {
            if (gradientType.equals("linear")) {
                if (gradientFill.getLin() == null) {
                    gradientFill.setLin(Context.getDmlObjectFactory().createCTLinearShadeProperties());
                }
                gradientFill.setPath(null);
            } else if (gradientType.equals("path")) {
                if (gradientFill.getPath() == null) {
                    gradientFill.setPath(Context.getDmlObjectFactory().createCTPathShadeProperties());
                }
                gradientFill.setLin(null);
            }
        }
        if (gradientFill.getLin() != null) {
            final Object gradientRotate = gradientAttrs.opt(OCKey.ROTATION.value());
            if (gradientRotate instanceof Number) {
                final Double angle = ((Number) gradientRotate).doubleValue() * 60000.0;
                gradientFill.getLin().setAng(angle.intValue());
            } else if (gradientRotate != null) {
                gradientFill.getLin().setAng(null);
            }
            final Object gradientScaled = gradientAttrs.opt(OCKey.IS_SCALED.value());
            if (gradientScaled instanceof Boolean) {
                gradientFill.getLin().setScaled((Boolean) gradientScaled);
            } else if (gradientScaled != null) {
                gradientFill.getLin().setScaled(null);
            }
        } else if (gradientFill.getPath() != null) {
            final CTPathShadeProperties pathShadeProperties = gradientFill.getPath();
            final Object pathType = gradientAttrs.opt(OCKey.PATH_TYPE.value());
            if (pathType != null) {
                if (pathType instanceof String) {
                    if (((String) pathType).equals("circle")) {
                        pathShadeProperties.setPath(STPathShadeType.CIRCLE);
                    } else if (((String) pathType).equals("rect")) {
                        pathShadeProperties.setPath(STPathShadeType.RECT);
                    } else {
                        pathShadeProperties.setPath(STPathShadeType.SHAPE);
                    }
                } else {
                    pathShadeProperties.setPath(STPathShadeType.SHAPE);
                }
            }
            final CTRelativeRect relativeRect = pathShadeProperties.getFillToRect(true);
            final Object pathFillTop = gradientAttrs.opt(OCKey.PATH_FILL_TOP.value());
            if (pathFillTop != null) {
                relativeRect.setT(pathFillTop instanceof Number ? convertPercentToRel((Number) pathFillTop) : null);
            }
            final Object pathFillLeft = gradientAttrs.opt(OCKey.PATH_FILL_LEFT.value());
            if (pathFillLeft != null) {
                relativeRect.setL(pathFillLeft instanceof Number ? convertPercentToRel((Number) pathFillLeft) : null);
            }
            final Object pathFillRight = gradientAttrs.opt(OCKey.PATH_FILL_RIGHT.value());
            if (pathFillRight != null) {
                relativeRect.setR(pathFillRight instanceof Number ? convertPercentToRel((Number) pathFillRight) : null);
            }
            final Object pathFillBottom = gradientAttrs.opt(OCKey.PATH_FILL_BOTTOM.value());
            if (pathFillBottom != null) {
                relativeRect
                        .setB(pathFillBottom instanceof Number ? convertPercentToRel((Number) pathFillBottom) : null);
            }
            if (relativeRect.getT() == 0 && relativeRect.getL() == 0 && relativeRect.getR() == 0
                    && relativeRect.getB() == 0) {
                pathShadeProperties.setFillToRect(null);
            }
        }
        final CTRelativeRect tileRect = gradientFill.getTileRect(true);
        final Object tileRectTop = gradientAttrs.opt(OCKey.TOP.value());
        if (tileRectTop != null) {
            tileRect.setT(tileRectTop instanceof Number ? convertPercentToRel((Number) tileRectTop) : null);
        }
        final Object tileRectLeft = gradientAttrs.opt(OCKey.LEFT.value());
        if (tileRectLeft != null) {
            tileRect.setL(tileRectLeft instanceof Number ? convertPercentToRel((Number) tileRectLeft) : null);
        }
        final Object tileRectRight = gradientAttrs.opt(OCKey.RIGHT.value());
        if (tileRectRight != null) {
            tileRect.setR(tileRectRight instanceof Number ? convertPercentToRel((Number) tileRectRight) : null);
        }
        final Object tileRectBottom = gradientAttrs.opt(OCKey.BOTTOM.value());
        if (tileRectBottom != null) {
            tileRect.setB(tileRectBottom instanceof Number ? convertPercentToRel((Number) tileRectBottom) : null);
        }
        if (tileRect.getT() == 0 && tileRect.getL() == 0 && tileRect.getR() == 0 && tileRect.getB() == 0) {
            gradientFill.setTileRect(null);
        }
        final Object flipH = gradientAttrs.opt(OCKey.FLIP_H.value());
        final Object flipV = gradientAttrs.opt(OCKey.FLIP_V.value());
        if (flipH != null || flipV != null) {
            boolean fH = false;
            boolean fV = false;
            final STTileFlipMode flipMode = gradientFill.getFlip();
            if (flipMode == STTileFlipMode.X) {
                fH = true;
            } else if (flipMode == STTileFlipMode.Y) {
                fV = true;
            } else if (flipMode == STTileFlipMode.XY) {
                fH = true;
                fV = true;
            }
            if (flipH != null) {
                if (flipH instanceof Boolean) {
                    fH = ((Boolean) flipH).booleanValue();
                } else {
                    fH = false;
                }
            }
            if (flipV != null) {
                if (flipV instanceof Boolean) {
                    fV = ((Boolean) flipV).booleanValue();
                } else {
                    fV = false;
                }
            }
            if (fH && fV) {
                gradientFill.setFlip(STTileFlipMode.XY);
            } else if (fH) {
                gradientFill.setFlip(STTileFlipMode.X);
            } else if (fV) {
                gradientFill.setFlip(STTileFlipMode.Y);
            } else {
                gradientFill.setFlip(STTileFlipMode.NONE);
            }
        }
        final Object gradientRotateWithShape = gradientAttrs.opt(OCKey.IS_ROTATE_WITH_SHAPE.value());
        if (gradientRotateWithShape instanceof Boolean) {
            gradientFill.setRotWithShape((Boolean) gradientRotateWithShape);
        } else if (gradientRotateWithShape == JSONObject.NULL) {
            gradientFill.setRotWithShape(true);
        }

        final Object gradientStops = gradientAttrs.opt(OCKey.COLOR_STOPS.value());
        if (gradientStops instanceof JSONArray) {
            gradientFill.setGsLst(Context.getDmlObjectFactory().createCTGradientStopList());
            final CTGradientStopList gs = gradientFill.getGsLst();
            final List<CTGradientStop> gsLst = gs.getGs();
            final JSONArray stops = (JSONArray) gradientStops;
            for (int i = 0; i < stops.length(); i++) {
                final JSONObject stop = (JSONObject) stops.get(i);
                final Double p = (((Number) stop.opt(OCKey.POSITION.value())).doubleValue() * 100000);
                final JSONObject color = stop.optJSONObject(OCKey.COLOR.value());
                if (color != null) {
                    final CTGradientStop gradientStop = Context.getDmlObjectFactory().createCTGradientStop();
                    gradientStop.setPos(p.intValue());
                    gsLst.add(gradientStop);
                    createSolidColorFillPropertiesFromJson(gradientStop, color);
                }
            }
        } else if (gradientStops != null) {
            gradientFill.setGsLst(null);
        }
    }

    public static void applyPattFillPropertiesFromJson(CTPatternFillProperties pattFillProperties, JSONObject fillAttrs) {
        try {
            STPresetPatternVal pattType = STPresetPatternVal.fromValue(fillAttrs.getString(OCKey.PATTERN.value()));
            pattFillProperties.setPrst(pattType);

            CTColor bgClr = new CTColor();
            createSolidColorFillPropertiesFromJson(bgClr, fillAttrs.getJSONObject(OCKey.COLOR2.value()));
            pattFillProperties.setBgClr(bgClr);

            CTColor fgClr = new CTColor();
            createSolidColorFillPropertiesFromJson(fgClr, fillAttrs.getJSONObject(OCKey.COLOR.value()));
            pattFillProperties.setFgClr(fgClr);
        } catch (Exception ex) {
            //
        }
    }

    public static void applyBlipFillPropertiesFromJson(CTBlipFillProperties blipFillProperties,
            OfficeOpenXMLOperationDocument operationDocument, Part part, JSONObject imageAttrs)
            throws InvalidFormatException, JSONException {

        if(imageAttrs==null) {
            return;
        }
        Object imageUrl = null;
        final Object imageData = imageAttrs.opt(OCKey.IMAGE_DATA.value());
        if(imageData instanceof String) {
            try {
                imageUrl = DocumentImageHelper.getImageUrlFromImageData(operationDocument.getResourceManager(), (String)imageData, operationDocument.getImagePath());
            } catch (Exception e) {
                //
            }
        }
        if(imageUrl==null) {
            imageUrl = imageAttrs.opt(OCKey.IMAGE_URL.value());
        }
        if(imageUrl instanceof String) {
            final Relationship relationship = Commons.createGraphicRelation(operationDocument, part, (String)imageUrl);
            if(relationship != null) {
                final CTBlip blip = blipFillProperties.getBlip(true);
                final boolean linked = relationship.getTargetMode()!=null && relationship.getTargetMode().equals("External");
                if (linked) {
                    blip.setEmbed(null);
                    blip.setLink(relationship.getId());
                } else {
                    blip.setLink(null);
                    blip.setEmbed(relationship.getId());
                }
            }
        }
        else if(imageUrl==JSONObject.NULL) {
            blipFillProperties.setBlip(null);
        }
        final Object rotateWithShape = imageAttrs.opt(OCKey.ROTATE_WITH_SHAPE.value());
        if (rotateWithShape != null) {
            if (rotateWithShape == JSONObject.NULL) {
                blipFillProperties.setRotWithShape(null);
            } else {
                blipFillProperties.setRotWithShape((Boolean) rotateWithShape);
            }
        }
        final CTRelativeRect cropRect = blipFillProperties.getSrcRect(true);
        Object cropLeft = imageAttrs.opt(OCKey.CROP_LEFT.value());
        if (cropLeft instanceof Number) {
            cropRect.setL(((Number) cropLeft).intValue() * 1000);
        } else if (cropLeft == JSONObject.NULL) {
            cropRect.setL(null);
        }
        final Object cropTop = imageAttrs.opt(OCKey.CROP_TOP.value());
        if (cropTop instanceof Number) {
            cropRect.setT(((Number) cropTop).intValue() * 1000);
        } else if (cropTop == JSONObject.NULL) {
            cropRect.setT(null);
        }
        final Object cropRight = imageAttrs.opt(OCKey.CROP_RIGHT.value());
        if (cropRight instanceof Number) {
            cropRect.setR(((Number) cropRight).intValue() * 1000);
        } else if (cropRight == JSONObject.NULL) {
            cropRect.setR(null);
        }
        final Object cropBottom = imageAttrs.opt(OCKey.CROP_BOTTOM.value());
        if (cropBottom instanceof Number) {
            cropRect.setB(((Number) cropBottom).intValue() * 1000);
        } else if (cropBottom == JSONObject.NULL) {
            cropRect.setB(null);
        }
        if (cropRect.isEmpty()) {
            blipFillProperties.setSrcRect(null);
        }

        applyBlipFillEffectsFromJson(blipFillProperties.getBlip(true), imageAttrs);

        final JSONObject tilingAttrs = imageAttrs.optJSONObject(OCKey.TILING.value());
        if (tilingAttrs != null) {
            if (tilingAttrs == JSONObject.NULL) {
                blipFillProperties.setTile(null);
            } else {
                final CTTileInfoProperties tileInfoProperties = blipFillProperties.getTile(true);
                Object a = tilingAttrs.opt(OCKey.RECT_ALIGNMENT.value());
                if (a != null) {
                    STRectAlignment rectAlign = null;
                    if (a instanceof String) {
                        switch ((String) a) {
                        case "bottom":
                            rectAlign = STRectAlignment.B;
                            break;
                        case "bottomLeft":
                            rectAlign = STRectAlignment.L;
                            break;
                        case "bottomRight":
                            rectAlign = STRectAlignment.BR;
                            break;
                        case "center":
                            rectAlign = STRectAlignment.CTR;
                            break;
                        case "left":
                            rectAlign = STRectAlignment.L;
                            break;
                        case "right":
                            rectAlign = STRectAlignment.R;
                            break;
                        case "top":
                            rectAlign = STRectAlignment.T;
                            break;
                        case "topLeft":
                            rectAlign = STRectAlignment.TL;
                            break;
                        case "topRight":
                            rectAlign = STRectAlignment.TR;
                            break;
                        }
                    }
                    tileInfoProperties.setAlgn(rectAlign);
                }
                a = tilingAttrs.opt(OCKey.FLIP_MODE.value());
                if (a != null) {
                    STTileFlipMode flipMode = null;
                    if (a instanceof String) {
                        switch ((String) a) {
                        case "x":
                            flipMode = STTileFlipMode.X;
                            break;
                        case "y":
                            flipMode = STTileFlipMode.Y;
                            break;
                        case "xy":
                            flipMode = STTileFlipMode.XY;
                            break;
                        case "none":
                            flipMode = STTileFlipMode.NONE;
                        }
                    }
                    tileInfoProperties.setFlip(flipMode);
                }
                a = tilingAttrs.opt(OCKey.STRETCH_X.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        tileInfoProperties.setSx(null);
                    } else {
                        tileInfoProperties.setSx(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
                a = tilingAttrs.opt(OCKey.STRETCH_Y.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        tileInfoProperties.setSy(null);
                    } else {
                        tileInfoProperties.setSy(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
                a = tilingAttrs.opt(OCKey.OFF_X.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        tileInfoProperties.setTx(null);
                    } else {
                        tileInfoProperties.setTx(Double.valueOf(((Number) a).doubleValue() * 1000.0).longValue());
                    }
                }
                a = tilingAttrs.opt(OCKey.OFF_Y.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        tileInfoProperties.setTy(null);
                    } else {
                        tileInfoProperties.setTy(Double.valueOf(((Number) a).doubleValue() * 1000.0).longValue());
                    }
                }
            }
        }
        final JSONObject stretchingAttrs = imageAttrs.optJSONObject(OCKey.STRETCHING.value());
        if (stretchingAttrs != null) {
            if (stretchingAttrs == JSONObject.NULL) {
                blipFillProperties.setStretch(null);
            } else {
                final CTStretchInfoProperties stretchInfoProperties = blipFillProperties.getStretch(true);
                CTRelativeRect fillRect = stretchInfoProperties.getFillRect();
                if (fillRect == null) {
                    fillRect = new CTRelativeRect();
                    stretchInfoProperties.setFillRect(fillRect);
                }
                Object a = stretchingAttrs.opt(OCKey.LEFT.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        fillRect.setL(null);
                    } else {
                        fillRect.setL(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
                a = stretchingAttrs.opt(OCKey.TOP.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        fillRect.setT(null);
                    } else {
                        fillRect.setT(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
                a = stretchingAttrs.opt(OCKey.BOTTOM.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        fillRect.setB(null);
                    } else {
                        fillRect.setB(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
                a = stretchingAttrs.opt(OCKey.RIGHT.value());
                if (a != null) {
                    if (a == JSONObject.NULL) {
                        fillRect.setR(null);
                    } else {
                        fillRect.setR(Double.valueOf(((Number) a).doubleValue() * 1000.0).intValue());
                    }
                }
            }
        }
        // either tiling or stretch is possible
        if (blipFillProperties.getTile(false) == null || blipFillProperties.getTile(false).isEmpty()) {
            blipFillProperties.setTile(null);
            blipFillProperties.getStretch(true);
        } else {
            blipFillProperties.setStretch(null);
        }
    }

    public static void applyBlipFillEffectsFromJson(CTBlip blip, JSONObject imageAttrs) throws JSONException {
        final Object transparency = imageAttrs.opt(OCKey.TRANSPARENCY.value());
        if (transparency != null) {
            if (transparency == JSONObject.NULL) {
                final Iterator<Object> blipEffectIter = blip.getAlphaBiLevelOrAlphaCeilingOrAlphaFloor().iterator();
                while (blipEffectIter.hasNext()) {
                    if (blipEffectIter.next() instanceof CTAlphaModulateFixedEffect) {
                        blipEffectIter.remove();
                        break;
                    }
                }
            } else {
                CTAlphaModulateFixedEffect alphaModulateFixedEffect = null;
                final Iterator<Object> blipEffectIter = blip.getAlphaBiLevelOrAlphaCeilingOrAlphaFloor().iterator();
                while (blipEffectIter.hasNext()) {
                    final Object blipEffect = blipEffectIter.next();
                    if (blipEffect instanceof CTAlphaModulateFixedEffect) {
                        alphaModulateFixedEffect = (CTAlphaModulateFixedEffect) blipEffect;
                        break;
                    }
                }
                if (alphaModulateFixedEffect == null) {
                    alphaModulateFixedEffect = new CTAlphaModulateFixedEffect();
                    blip.getAlphaBiLevelOrAlphaCeilingOrAlphaFloor().add(alphaModulateFixedEffect);
                }
                alphaModulateFixedEffect
                        .setAmt(Double.valueOf(100000 - ((Number) transparency).doubleValue() * 100000.0).intValue());
            }
        }

        // blip effects
        final JSONArray effects = imageAttrs.optJSONArray(OCKey.EFFECTS.value());
        if(effects!=null) {
            for(int i = 0; i < effects.length(); i++) {
                final JSONObject effect = effects.getJSONObject(i);
                final String type = effect.getString(OCKey.TYPE.value());
                switch(type) {
                    case "duotone" : {
                        final CTDuotoneEffect duotoneEffect = new CTDuotoneEffect();
                        createSolidColorFillPropertiesFromJson(new ColorChoiceListEntry(duotoneEffect.getEGColorChoice(), 0), effect.getJSONObject(OCKey.COLOR.value()));
                        createSolidColorFillPropertiesFromJson(new ColorChoiceListEntry(duotoneEffect.getEGColorChoice(), 1), effect.getJSONObject(OCKey.COLOR2.value()));
                        blip.getAlphaBiLevelOrAlphaCeilingOrAlphaFloor().add(duotoneEffect);
                        break;
                    }
                }
            }
        }
    }

    public static void createJsonFromTransform2D(JSONObject attrs, ITransform2D xFrm, boolean convertTo100thmm)
            throws JSONException {

        if (xFrm != null) {
            final JSONObject initialDrawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
            final JSONObject drawingAttrs = initialDrawingAttrs != null ? initialDrawingAttrs : new JSONObject();

            final CTPoint2D point2D = xFrm.getOff(false);
            if (point2D != null) {
                if (!drawingAttrs.hasAndNotNull(OCKey.LEFT.value())) {
                    drawingAttrs.put(OCKey.LEFT.value(),
                            convertTo100thmm ? Commons.coordinateTo100TH_MM(point2D.getX()) : point2D.getX());
                }
                if (!drawingAttrs.hasAndNotNull(OCKey.TOP.value())) {
                    drawingAttrs.put(OCKey.TOP.value(),
                            convertTo100thmm ? Commons.coordinateTo100TH_MM(point2D.getY()) : point2D.getY());
                }
            }
            final CTPositiveSize2D positiveSize2D = xFrm.getExt(false);
            if (positiveSize2D != null) {
                if (!drawingAttrs.hasAndNotNull(OCKey.WIDTH.value())) {
                    drawingAttrs.put(OCKey.WIDTH.value(), convertTo100thmm ? Commons.coordinateTo100TH_MM(positiveSize2D.getCx())
                            : positiveSize2D.getCx());
                }
                if (!drawingAttrs.hasAndNotNull(OCKey.HEIGHT.value())) {
                    drawingAttrs.put(OCKey.HEIGHT.value(), convertTo100thmm ? Commons.coordinateTo100TH_MM(positiveSize2D.getCy())
                            : positiveSize2D.getCy());
                }
            }
            if (xFrm.isFlipH()) {
                if (!drawingAttrs.hasAndNotNull(OCKey.FLIP_H.value())) {
                    drawingAttrs.put(OCKey.FLIP_H.value(), true);
                }
            }
            if (xFrm.isFlipV()) {
                if (!drawingAttrs.hasAndNotNull(OCKey.FLIP_V.value())) {
                    drawingAttrs.put(OCKey.FLIP_V.value(), true);
                }
            }
            final int rotation = xFrm.getRot();
            if (rotation != 0) {
                drawingAttrs.put(OCKey.ROTATION.value(), rotation / 60000.0);
            }
            if (xFrm instanceof CTGroupTransform2D) {
                final CTPoint2D chOff = ((CTGroupTransform2D) xFrm).getChOff(false);
                if (chOff != null) {
                    if (!drawingAttrs.hasAndNotNull(OCKey.CHILD_LEFT.value())) {
                        drawingAttrs.put(OCKey.CHILD_LEFT.value(), chOff.getX());
                    }
                    if (!drawingAttrs.hasAndNotNull(OCKey.CHILD_TOP.value())) {
                        drawingAttrs.put(OCKey.CHILD_TOP.value(), chOff.getY());
                    }
                }
                final CTPositiveSize2D chExt = ((CTGroupTransform2D) xFrm).getChExt(false);
                if (chExt != null) {
                    if (!drawingAttrs.hasAndNotNull(OCKey.CHILD_WIDTH.value())) {
                        drawingAttrs.put(OCKey.CHILD_WIDTH.value(), chExt.getCx());
                    }
                    if (!drawingAttrs.hasAndNotNull(OCKey.CHILD_HEIGHT.value())) {
                        drawingAttrs.put(OCKey.CHILD_HEIGHT.value(), chExt.getCy());
                    }
                }
            }
            if (initialDrawingAttrs == null && !drawingAttrs.isEmpty()) {
                attrs.put(OCKey.DRAWING.value(), drawingAttrs);
            }
        }
    }

    public static void applyTransform2DFromJson(ITransform2DAccessor transformAccessor, JSONObject attrs,
            boolean convertToEmu) {

        final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        if (drawingAttrs != null) {

            final Object width = drawingAttrs.opt(OCKey.WIDTH.value());
            if (width instanceof Number) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.getExt(true)
                        .setCx(convertToEmu ? ((Number) width).intValue() * 360 : ((Number) width).intValue());
            }
            final Object height = drawingAttrs.opt(OCKey.HEIGHT.value());
            if (height instanceof Number) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.getExt(true)
                        .setCy(convertToEmu ? ((Number) height).intValue() * 360 : ((Number) height).intValue());
            }
            if (width == JSONObject.NULL && height == JSONObject.NULL) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.setExt(null);
            }
            final Object left = drawingAttrs.opt(OCKey.LEFT.value());
            if (left instanceof Number) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.getOff(true)
                        .setX(convertToEmu ? ((Number) left).longValue() * 360 : ((Number) left).longValue());
            }
            final Object top = drawingAttrs.opt(OCKey.TOP.value());
            if (top instanceof Number) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.getOff(true)
                        .setY(convertToEmu ? ((Number) top).longValue() * 360 : ((Number) top).longValue());
            }
            if (left == JSONObject.NULL && top == JSONObject.NULL) {
                ITransform2D transform2D = transformAccessor.getXfrm(true);
                transform2D.setOff(null);
            }
            final Object flipH = drawingAttrs.opt(OCKey.FLIP_H.value());
            if (flipH != null) {
                if (flipH instanceof Boolean) {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setFlipH((Boolean) flipH);
                } else {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setFlipH(null);
                }
            }
            final Object flipV = drawingAttrs.opt(OCKey.FLIP_V.value());
            if (flipV != null) {
                if (flipV instanceof Boolean) {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setFlipV((Boolean) flipV);
                } else {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setFlipV(null);
                }
            }
            final Object rotation = drawingAttrs.opt(OCKey.ROTATION.value());
            if (rotation != null) {
                if (rotation instanceof Number) {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setRot(Integer.valueOf((int) ((((Number) rotation).doubleValue() * 60000) + 0.5)));
                } else {
                    ITransform2D transform2D = transformAccessor.getXfrm(true);
                    transform2D.setRot(null);
                }
            }
            if (transformAccessor instanceof CTGroupShapeProperties) {
                final CTGroupTransform2D groupTransform = (CTGroupTransform2D) transformAccessor.getXfrm(true);
                final Object childWidth = drawingAttrs.opt(OCKey.CHILD_WIDTH.value());
                if (childWidth instanceof Number) {
                    final CTPositiveSize2D chExt = groupTransform.getChExt(true);
                    chExt.setCx(((Number) childWidth).intValue());
                }
                final Object childHeight = drawingAttrs.opt(OCKey.CHILD_HEIGHT.value());
                if (childHeight instanceof Number) {
                    final CTPositiveSize2D chExt = groupTransform.getChExt(true);
                    chExt.setCy(((Number) childHeight).intValue());
                }
                if (childWidth == JSONObject.NULL && childHeight == JSONObject.NULL) {
                    groupTransform.setChExt(null);
                }
                final Object childLeft = drawingAttrs.opt(OCKey.CHILD_LEFT.value());
                if (childLeft instanceof Number) {
                    final CTPoint2D chOff = groupTransform.getChOff(true);
                    chOff.setX(((Number) childLeft).longValue());
                }
                final Object childTop = drawingAttrs.opt(OCKey.CHILD_TOP.value());
                if (childTop instanceof Number) {
                    final CTPoint2D chOff = groupTransform.getChOff(true);
                    chOff.setY(((Number) childTop).longValue());
                }
                if (childLeft == JSONObject.NULL && childTop == JSONObject.NULL) {
                    groupTransform.setChOff(null);
                }
            } else {
                final ITransform2D transform2D = transformAccessor.getXfrm(false);
                if (transform2D != null && transform2D.getExt(false) == null && transform2D.getOff(false) == null) {
                    transformAccessor.removeXfrm();
                }
            }
        }
    }

    public static void applyFillPropertiesFromJsonColor(IFillProperties fillProperties, Object jsonColor)
            throws JSONException {

        fillProperties.setNoFill(null);
        fillProperties.setBlipFill(null);
        fillProperties.setGradFill(null);
        fillProperties.setPattFill(null);
        fillProperties.setGrpFill(null);

        if (jsonColor instanceof JSONObject) {
            final CTSolidColorFillProperties fillColor = Context.getDmlObjectFactory()
                    .createCTSolidColorFillProperties();
            fillProperties.setSolidFill(fillColor);
            createSolidColorFillPropertiesFromJson(fillColor, (JSONObject) jsonColor);
        } else if (jsonColor == JSONObject.NULL) {
            fillProperties.setSolidFill(null);
        }
    }

    public static void applyFillPropertiesFromJson(IFillProperties fillProperties, JSONObject attrs,
        OfficeOpenXMLOperationDocument operationDocument, Part part)
        throws JSONException, InvalidFormatException {
        applyFillPropertiesFromJson(fillProperties, attrs, operationDocument, part, OCKey.FILL.value());
    }

    public static void applyMarkerFillPropertiesFromJson(IFillProperties fillProperties, JSONObject attrs,
        OfficeOpenXMLOperationDocument operationDocument, Part part)
        throws JSONException, InvalidFormatException {
        applyFillPropertiesFromJson(fillProperties, attrs, operationDocument, part, OCKey.MARKER_FILL.value());
    }

    private static void applyFillPropertiesFromJson(IFillProperties fillProperties, JSONObject attrs,
            OfficeOpenXMLOperationDocument operationDocument, Part part, String attributeName)
            throws JSONException, InvalidFormatException {

        final JSONObject fillAttrs = attrs.optJSONObject(attributeName);
        if (fillAttrs != null) {

            // applying fill type first
            final Object fillType = fillAttrs.opt(OCKey.TYPE.value());
            if (fillType instanceof String) {
                if (fillType.equals("none")) {
                    fillProperties.setNoFill(Context.getDmlObjectFactory().createCTNoFillProperties());
                } else {
                    fillProperties.setNoFill(null);
                }
                if (fillType.equals("solid")) {
                    if (fillProperties.getSolidFill() == null) {
                        fillProperties.setSolidFill(Context.getDmlObjectFactory().createCTSolidColorFillProperties());
                    }
                } else {
                    fillProperties.setSolidFill(null);
                }
                if (fillType.equals("gradient")) {
                    if (fillProperties.getGradFill() == null) {
                        fillProperties.setGradFill(Context.getDmlObjectFactory().createCTGradientFillProperties());
                    }
                } else {
                    fillProperties.setGradFill(null);
                }
                if (fillType.equals("pattern")) {
                    if (fillProperties.getPattFill() == null) {
                        fillProperties.setPattFill(Context.getDmlObjectFactory().createCTPatternFillProperties());
                    }
                } else {
                    fillProperties.setPattFill(null);
                }
                if (fillType.equals("bitmap")) {
                    if (fillProperties.getBlipFill() == null) {
                        fillProperties.setBlipFill(Context.getDmlObjectFactory().createCTBlipFillProperties());
                    }
                } else {
                    fillProperties.setBlipFill(null);
                }
                fillProperties.setGrpFill(null);
            } else if (fillType == JSONObject.NULL) {
                fillProperties.setNoFill(null);
                fillProperties.setSolidFill(null);
                fillProperties.setGradFill(null);
                fillProperties.setPattFill(null);
                fillProperties.setBlipFill(null);
                fillProperties.setGrpFill(null);
            }
            if (fillProperties != null) {
                if (fillProperties.getSolidFill() != null) {
                    final JSONObject color = fillAttrs.optJSONObject(OCKey.COLOR.value());
                    if (color != null) {
                        createSolidColorFillPropertiesFromJson(fillProperties.getSolidFill(), color);
                    }
                } else if (fillProperties.getGradFill() != null) {
                    final JSONObject gradientAttrs = fillAttrs.optJSONObject(OCKey.GRADIENT.value());
                    if (gradientAttrs != null) {
                        applyGradFillPropertiesFromJson(fillProperties.getGradFill(), gradientAttrs);
                    }
                } else if (fillProperties.getPattFill() != null) {
                    applyPattFillPropertiesFromJson(fillProperties.getPattFill(), fillAttrs);
                } else if (fillProperties.getBlipFill() != null) {
                    applyBlipFillPropertiesFromJson(fillProperties.getBlipFill(), operationDocument, part,
                            fillAttrs.optJSONObject(OCKey.BITMAP.value()));
                }
            }
        }
    }

    // styleMatrixReference can be zero, if available then the color from the
    // styleMatrix is used
    private static void createJsonFromFillStyle(Object source, IStyleMatrixReference styleMatrixReference,
            JSONObject dest, Part part) throws JSONException {

        if (source instanceof CTSolidColorFillProperties) {
            dest.put(OCKey.TYPE.value(), "solid");
            JSONObject color = createJsonColorFromSolidColorFillProperties((CTSolidColorFillProperties) source);
            if (styleMatrixReference != null) {
                color = createJsonColorFromSolidColorFillProperties(color, styleMatrixReference);
            }
            if (color != null) {
                dest.put(OCKey.COLOR.value(), color);
            }
        } else if (source instanceof CTNoFillProperties) {
            dest.put(OCKey.TYPE.value(), "none");
        } else if (source instanceof CTGradientFillProperties) {
            dest.put(OCKey.TYPE.value(), "gradient");
            final JSONObject gradientAttrs = new JSONObject(5);

            final CTGradientFillProperties gradientFillProperties = (CTGradientFillProperties) source;
            final CTLinearShadeProperties linearShadeProperties = gradientFillProperties.getLin();
            if (linearShadeProperties != null) {
                gradientAttrs.put(OCKey.TYPE.value(), "linear");
                final Integer angle = linearShadeProperties.getAng();
                if (angle != null) {
                    gradientAttrs.put(OCKey.ROTATION.value(), angle.doubleValue() / 60000.0);
                }
                final Boolean isScaled = linearShadeProperties.isScaled();
                if (isScaled != null) {
                    gradientAttrs.put(OCKey.IS_SCALED.value(), isScaled);
                }
            } else if (gradientFillProperties.getPath() != null) {
                gradientAttrs.put(OCKey.TYPE.value(), "path");
                final CTPathShadeProperties pathShadeProperties = gradientFillProperties.getPath();
                if (pathShadeProperties.getPath() == STPathShadeType.CIRCLE) {
                    gradientAttrs.put(OCKey.PATH_TYPE.value(), "circle");
                } else if (pathShadeProperties.getPath() == STPathShadeType.RECT) {
                    gradientAttrs.put(OCKey.PATH_TYPE.value(), "rect");
                }
                final CTRelativeRect relativeRect = pathShadeProperties.getFillToRect(false);
                if (relativeRect != null) {
                    if (relativeRect.getB() != 0) {
                        gradientAttrs.put(OCKey.PATH_FILL_BOTTOM.value(), convertRelToPercent(relativeRect.getB()));
                    }
                    if (relativeRect.getL() != 0) {
                        gradientAttrs.put(OCKey.PATH_FILL_LEFT.value(), convertRelToPercent(relativeRect.getL()));
                    }
                    if (relativeRect.getR() != 0) {
                        gradientAttrs.put(OCKey.PATH_FILL_RIGHT.value(), convertRelToPercent(relativeRect.getR()));
                    }
                    if (relativeRect.getT() != 0) {
                        gradientAttrs.put(OCKey.PATH_FILL_TOP.value(), convertRelToPercent(relativeRect.getT()));
                    }
                }
            }
            final CTRelativeRect tileRect = gradientFillProperties.getTileRect(false);
            if (tileRect != null) {
                if (tileRect.getB() != 0) {
                    gradientAttrs.put(OCKey.BOTTOM.value(), convertRelToPercent(tileRect.getB()));
                }
                if (tileRect.getL() != 0) {
                    gradientAttrs.put(OCKey.LEFT.value(), convertRelToPercent(tileRect.getL()));
                }
                if (tileRect.getR() != 0) {
                    gradientAttrs.put(OCKey.RIGHT.value(), convertRelToPercent(tileRect.getR()));
                }
                if (tileRect.getT() != 0) {
                    gradientAttrs.put(OCKey.TOP.value(), convertRelToPercent(tileRect.getT()));
                }
            }
            final STTileFlipMode flipMode = gradientFillProperties.getFlip();
            if (flipMode != null) {
                if (flipMode == STTileFlipMode.X) {
                    gradientAttrs.put(OCKey.FLIP_H.value(), true);
                } else if (flipMode == STTileFlipMode.Y) {
                    gradientAttrs.put(OCKey.FLIP_V.value(), true);
                } else if (flipMode == STTileFlipMode.XY) {
                    gradientAttrs.put(OCKey.FLIP_H.value(), true);
                    gradientAttrs.put(OCKey.FLIP_V.value(), true);
                }
            }
            if (gradientFillProperties.isRotWithShape() == null
                    || gradientFillProperties.isRotWithShape().booleanValue()) {
                gradientAttrs.put(OCKey.IS_ROTATE_WITH_SHAPE.value(), true);
            } else {
                // Fix: Bug DOCS-1847
                // This attribute has to be sent to the server,
                // even if it's empty. Why? ODF doesn't has such
                // a feature and fills are rotated always with
                // the shape. By sending the attribute with false
                // we can use true in the client in case that the
                // attribute isn't available like it is in ODF.
                gradientAttrs.put(OCKey.IS_ROTATE_WITH_SHAPE.value(), false);
            }
            final CTGradientStopList gradientStopList = gradientFillProperties.getGsLst();
            if (gradientStopList != null) {
                final List<CTGradientStop> list = gradientStopList.getGs();
                final JSONArray gradientStopArray = new JSONArray(list.size());
                for (CTGradientStop gradientStop : list) {
                    final JSONObject colorStop = new JSONObject(2);
                    colorStop.put(OCKey.POSITION.value(), gradientStop.getPos() / 100000.0);
                    JSONObject color = createJsonColorFromSolidColorFillProperties(gradientStop);
                    if (styleMatrixReference != null) {
                        color = createJsonColorFromSolidColorFillProperties(color, styleMatrixReference);
                    }
                    if (color != null) {
                        colorStop.put(OCKey.COLOR.value(), color);
                        gradientStopArray.put(colorStop);
                    }
                }
                if (!gradientStopArray.isEmpty()) {
                    gradientAttrs.put(OCKey.COLOR_STOPS.value(), gradientStopArray);
                }
            }
            if (!gradientAttrs.isEmpty()) {
                dest.put(OCKey.GRADIENT.value(), gradientAttrs);
            }
        } else if (source instanceof CTPatternFillProperties) {
            final CTPatternFillProperties pattSource = (CTPatternFillProperties) source;
            final CTColor fgFill = pattSource.getFgClr();
            dest.put(OCKey.TYPE.value(), "pattern");
            if (fgFill != null) {
                JSONObject color = createJsonColorFromSolidColorFillProperties(fgFill);
                if (styleMatrixReference != null) {
                    color = createJsonColorFromSolidColorFillProperties(color, styleMatrixReference);
                }
                if (color != null) {
                    dest.put(OCKey.COLOR.value(), color);
                }
            }
            final CTColor bgFill = pattSource.getBgClr();
            if (bgFill != null) {
                JSONObject color = createJsonColorFromSolidColorFillProperties(bgFill);
                if (styleMatrixReference != null) {
                    color = createJsonColorFromSolidColorFillProperties(color, styleMatrixReference);
                }
                if (color != null) {
                    dest.put(OCKey.COLOR2.value(), color);
                }
            }
            final STPresetPatternVal pattType = pattSource.getPrst();
            dest.put(OCKey.PATTERN.value(), (pattType != null) ? pattType.value() : "none");
        } else if (source instanceof CTBlipFillProperties) {
            dest.put(OCKey.TYPE.value(), "bitmap");
            final JSONObject bitmapProps = new JSONObject();
            createJsonFromBlipFillProperties(part, bitmapProps, (CTBlipFillProperties) source, styleMatrixReference);
            dest.put(OCKey.BITMAP.value(), bitmapProps);
        }
    }

    // styleMatrixReference can be zero, if available then the color from the
    // styleMatrix is used
    private static void createJsonFromLineStyle(CTLineProperties lineProperties,
            IStyleMatrixReference styleMatrixReference, JSONObject dest, Part part) throws JSONException {

        if (lineProperties.getNoFill() != null) {
            createJsonFromFillStyle(lineProperties.getNoFill(), styleMatrixReference, dest, part);
        } else if (lineProperties.getSolidFill() != null) {
            createJsonFromFillStyle(lineProperties.getSolidFill(), styleMatrixReference, dest, part);
        } else if (lineProperties.getGradFill() != null) {
            createJsonFromFillStyle(lineProperties.getGradFill(), styleMatrixReference, dest, part);
        } else if (lineProperties.getPattFill() != null) {
            createJsonFromFillStyle(lineProperties.getPattFill(), styleMatrixReference, dest, part);
        }
        if (lineProperties.getW() != null) {
            dest.put(OCKey.WIDTH.value(), convertEmuTo100thmm(lineProperties.getW()));
        }
        if (lineProperties.getPrstDash() != null && lineProperties.getPrstDash().getVal() != null) {
            switch (lineProperties.getPrstDash().getVal()) {
            case SYS_DOT:
            case DOT:
                dest.put(OCKey.STYLE.value(), "dotted");
                break;
            case LG_DASH:
            case SYS_DASH:
            case DASH:
                dest.put(OCKey.STYLE.value(), "dashed");
                break;
            case LG_DASH_DOT:
            case SYS_DASH_DOT:
            case DASH_DOT:
                dest.put(OCKey.STYLE.value(), "dashDot");
                break;
            case LG_DASH_DOT_DOT:
            case SYS_DASH_DOT_DOT:
                dest.put(OCKey.STYLE.value(), "dashDotDot");
                break;
            case SOLID:
            default:
                break;
            }
        }
        final CTLineEndProperties headEnd = lineProperties.getHeadEnd(false);
        if (headEnd != null) {
            final STLineEndType headEndType = headEnd.getType();
            if (headEndType != null) {
                dest.put(OCKey.HEAD_END_TYPE.value(), headEndType.value());
            }
            final STLineEndLength headEndLength = headEnd.getLen();
            if (headEndLength != null) {
                if (headEndLength == STLineEndLength.SM) {
                    dest.put(OCKey.HEAD_END_LENGTH.value(), "small");
                } else if (headEndLength == STLineEndLength.MED) {
                    dest.put(OCKey.HEAD_END_LENGTH.value(), "medium");
                } else if (headEndLength == STLineEndLength.LG) {
                    dest.put(OCKey.HEAD_END_LENGTH.value(), "large");
                }
            }
            final STLineEndWidth headEndWidth = headEnd.getW();
            if (headEndWidth != null) {
                if (headEndWidth == STLineEndWidth.SM) {
                    dest.put(OCKey.HEAD_END_WIDTH.value(), "small");
                } else if (headEndWidth == STLineEndWidth.MED) {
                    dest.put(OCKey.HEAD_END_WIDTH.value(), "medium");
                } else if (headEndWidth == STLineEndWidth.LG) {
                    dest.put(OCKey.HEAD_END_WIDTH.value(), "large");
                }
            }
        }
        final CTLineEndProperties tailEnd = lineProperties.getTailEnd(false);
        if (tailEnd != null) {
            final STLineEndType tailEndType = tailEnd.getType();
            if (tailEndType != null) {
                dest.put(OCKey.TAIL_END_TYPE.value(), tailEndType.value());
            }
            final STLineEndLength tailEndLength = tailEnd.getLen();
            if (tailEndLength != null) {
                if (tailEndLength == STLineEndLength.SM) {
                    dest.put(OCKey.TAIL_END_LENGTH.value(), "small");
                } else if (tailEndLength == STLineEndLength.MED) {
                    dest.put(OCKey.TAIL_END_LENGTH.value(), "medium");
                } else if (tailEndLength == STLineEndLength.LG) {
                    dest.put(OCKey.TAIL_END_LENGTH.value(), "large");
                }
            }
            final STLineEndWidth tailEndWidth = tailEnd.getW();
            if (tailEndWidth != null) {
                if (tailEndWidth == STLineEndWidth.SM) {
                    dest.put(OCKey.TAIL_END_WIDTH.value(), "small");
                } else if (tailEndWidth == STLineEndWidth.MED) {
                    dest.put(OCKey.TAIL_END_WIDTH.value(), "medium");
                } else if (tailEndWidth == STLineEndWidth.LG) {
                    dest.put(OCKey.TAIL_END_WIDTH.value(), "large");
                }
            }
        }
    }

    private static void createJsonFromFillStyleMatrix(ThemePart themePart, IStyleMatrixReference fillStyleReference,
            JSONObject dest) throws JSONException {

        if (fillStyleReference == null || themePart == null) {
            return;
        }
        final Theme theme = themePart.getJaxbElement();
        if (theme == null) {
            return;
        }
        final BaseStyles themeElements = theme.getThemeElements();
        if (themeElements == null) {
            return;
        }
        final CTStyleMatrix fmtScheme = themeElements.getFmtScheme();
        if (fmtScheme == null) {
            return;
        }
        final long idx = fillStyleReference.getIdx();
        if (idx == 0 || idx == 1000) {
            dest.put(OCKey.TYPE.value(), "none");
            return;
        }
        List<Object> fillMatrix = null;
        if (idx >= 1 && idx < 1000) {
            final CTFillStyleList fillStyleList = fmtScheme.getFillStyleLst();
            if (fillStyleList != null) {
                fillMatrix = fillStyleList.getContent();
            }
        } else if (idx > 1000) {
            final CTBackgroundFillStyleList bgFillStyleList = fmtScheme.getBgFillStyleLst();
            if (bgFillStyleList != null) {
                fillMatrix = bgFillStyleList.getContent();
            }
        }
        if (fillMatrix != null) {
            final int index = (int) (idx > 1000 ? idx - 1001 : idx - 1);
            if (index < fillMatrix.size()) {
                createJsonFromFillStyle(fillMatrix.get(index), fillStyleReference, dest, themePart);
            }
        }
        return;
    }

    private static void createJsonFromLineStyleMatrix(ThemePart themePart, IStyleMatrixReference lineStyleReference,
            JSONObject dest) throws JSONException {

        if (lineStyleReference == null || themePart == null) {
            return;
        }
        final Theme theme = themePart.getJaxbElement();
        if (theme == null) {
            return;
        }
        final BaseStyles themeElements = theme.getThemeElements();
        if (themeElements == null) {
            return;
        }
        final CTStyleMatrix fmtScheme = themeElements.getFmtScheme();
        if (fmtScheme == null) {
            return;
        }
        final int idx = (int) lineStyleReference.getIdx() - 1;
        final CTLineStyleList lineStyleList = fmtScheme.getLnStyleLst();
        if (idx >= 0 && lineStyleList != null) {
            final List<CTLineProperties> lineMatrix = lineStyleList.getLn();
            if (idx < lineMatrix.size()) {
                createJsonFromLineStyle(lineMatrix.get(idx), lineStyleReference, dest, themePart);
            }
        }
        return;
    }

    public static void createJsonFromShapeProperties(JSONObject attrs, CTShapeProperties shapeProperties,
            ThemePart themePart, IShapeStyle shapeStyle, Part part, boolean createTransform, boolean rootShape)
            throws JSONException {

        if (shapeProperties != null) {
            if (createTransform) {
                createJsonFromTransform2D(attrs, shapeProperties.getXfrm(false), rootShape);
            }
            if (shapeProperties.getPrstGeom(false) != null) {
                DMLGeometry.createJsonFromPresetGeometry(attrs, shapeProperties.getPrstGeom(false));
            } else if (shapeProperties.getCustGeom(false) != null) {
                DMLGeometry.createJsonFromCustomGeometry(attrs, shapeProperties.getCustGeom(false));
            }
        }
        if (shapeStyle != null && shapeStyle.getFontRef() != null) {
            createJsonFromFontRef(attrs, shapeStyle.getFontRef());
        }
        createJsonFromLineProperties(attrs, themePart, shapeStyle != null ? shapeStyle.getLnRef() : null,
                shapeProperties != null ? shapeProperties.getLn() : null, part);
        createJsonFromFillProperties(attrs, shapeProperties, themePart,
                shapeStyle != null ? shapeStyle.getFillRef() : null, part);
    }

    public static void applyTableGridFromJson(CTTable table, long tableWidth, JSONArray tableGrid) {
        final CTTableGrid grid = table.getTblGrid(true);
        long totalGridWidth = 0;

        if (tableGrid != null) {
            // applying new tableGrid
            grid.getGridCol().clear();
            Iterator<Object> gridIter = tableGrid.iterator();
            while (gridIter.hasNext()) {
                final Object o = gridIter.next();
                if (o instanceof Number) {
                    totalGridWidth += ((Number) o).longValue();
                }
            }
            long remainingTableWidth = tableWidth;
            gridIter = tableGrid.iterator();
            while (gridIter.hasNext()) {
                final Object o = gridIter.next();
                if (o instanceof Number) {
                    final CTTableCol tableCol = new CTTableCol();
                    long destGrid = 0;
                    if (!gridIter.hasNext()) {
                        destGrid = remainingTableWidth;
                    } else if (totalGridWidth != 0) {
                        double w = ((Number) o).doubleValue();
                        w = (w / totalGridWidth) * tableWidth;
                        destGrid = Double.valueOf(w).longValue();
                        remainingTableWidth -= destGrid;
                    }
                    tableCol.setW(destGrid);
                    grid.getGridCol().add(tableCol);
                }
            }
        } else { // the table width might have changed, we have to adapt the
                 // existing tableGrid
            Iterator<CTTableCol> tableColIter = grid.getGridCol().iterator();
            while (tableColIter.hasNext()) {
                totalGridWidth += tableColIter.next().getW();
            }
            if (tableWidth != totalGridWidth) {
                long remainingTableWidth = tableWidth;
                tableColIter = grid.getGridCol().iterator();
                while (tableColIter.hasNext()) {
                    final CTTableCol tableCol = tableColIter.next();
                    long destGrid = 0;
                    if (!tableColIter.hasNext()) {
                        destGrid = remainingTableWidth;
                    } else if (totalGridWidth != 0) {
                        double w = tableCol.getW();
                        w = (w / totalGridWidth) * tableWidth;
                        destGrid = Double.valueOf(w).longValue();
                        remainingTableWidth -= destGrid;
                    }
                    tableCol.setW(destGrid);
                }
            }
        }
    }

    public static void applyTableFromJson(JSONObject attrs, CTTable table, long width,
            OfficeOpenXMLOperationDocument operationDocument)
            throws InvalidFormatException, JSONException {

        if (attrs == null) {
            return;
        }
        final Object styleId = attrs.opt(OCKey.STYLE_ID.value());
        if (styleId != null) {
            final CTTableProperties tblPr = table.getTblPr(true);
            if (styleId instanceof String) {
                tblPr.setTableStyleId((String) styleId);
            } else if (styleId == JSONObject.NULL) {
                tblPr.setTableStyleId(null);
            }
        }
        final JSONObject tableAttrs = attrs.optJSONObject(OCKey.TABLE.value());
        applyTableGridFromJson(table, width, tableAttrs != null ? tableAttrs.optJSONArray(OCKey.TABLE_GRID.value()) : null);
        if (tableAttrs != null) {
            final CTTableProperties tblPr = table.getTblPr(true);
            applyFillPropertiesFromJson(tblPr, tableAttrs, operationDocument, operationDocument.getContextPart());
            final JSONArray tableLook = tableAttrs.optJSONArray(OCKey.EXCLUDE.value());
            if (tableLook != null) {
                tblPr.setBandCol(true);
                tblPr.setBandRow(true);
                tblPr.setFirstCol(true);
                tblPr.setFirstRow(true);
                tblPr.setLastCol(true);
                tblPr.setLastRow(true);
                for (int i = 0; i < tableLook.length(); i++) {
                    final Object o = tableLook.get(i);
                    if (o instanceof String) {
                        switch ((String) o) {
                        case "bandsVert": {
                            tblPr.setBandCol(null);
                            break;
                        }
                        case "bandsHor": {
                            tblPr.setBandRow(null);
                            break;
                        }
                        case "firstCol": {
                            tblPr.setFirstCol(null);
                            break;
                        }
                        case "firstRow": {
                            tblPr.setFirstRow(null);
                            break;
                        }
                        case "lastCol": {
                            tblPr.setLastCol(null);
                            break;
                        }
                        case "lastRow": {
                            tblPr.setLastRow(null);
                            break;
                        }
                        }
                    }
                }
            }
        }
    }

    public static void createJsonFromTable(JSONObject attrs, CTTable table, ThemePart themePart, Part part)
            throws JSONException {

        final CTTableGrid tableGrid = table.getTblGrid(false);
        if (tableGrid != null && !tableGrid.getGridCol().isEmpty()) {
            final JSONArray jsonTableGrid = new JSONArray(tableGrid.getGridCol().size());
            final Iterator<CTTableCol> gridIter = tableGrid.getGridCol().iterator();
            while (gridIter.hasNext()) {
                jsonTableGrid.put(gridIter.next().getW());
            }
            Tools.addFamilyAttribute(attrs, OCKey.TABLE, OCKey.TABLE_GRID, jsonTableGrid);
        }
        final CTTableProperties tableProperties = table.getTblPr(false);
        if (tableProperties != null) {
            final String tableStyleId = tableProperties.getTableStyleId();
            if (tableStyleId != null && !tableStyleId.isEmpty()) {
                attrs.put(OCKey.STYLE_ID.value(), tableStyleId);
            }

            createJsonFromFillProperties(attrs, tableProperties, themePart, null, part);

            final JSONArray jsonLook = new JSONArray();
            if (!tableProperties.isBandCol()) {
                jsonLook.put("bandsVert");
            }
            if (!tableProperties.isBandRow()) {
                jsonLook.put("bandsHor");
            }
            if (!tableProperties.isFirstCol()) {
                jsonLook.put("firstCol");
            }
            if (!tableProperties.isFirstRow()) {
                jsonLook.put("firstRow");
            }
            if (!tableProperties.isLastCol()) {
                jsonLook.put("lastCol");
            }
            if (!tableProperties.isLastRow()) {
                jsonLook.put("lastRow");
            }
            if (!jsonLook.isEmpty()) {
                Tools.addFamilyAttribute(attrs, OCKey.TABLE, OCKey.EXCLUDE, jsonLook);
            }

            final CTTableStyle tableStyle = tableProperties.getTableStyle();
            if (tableStyle != null) {
                // TODO ...
            }
        }
    }

    // returns true if the table height needs to be recalculated
    public static boolean applyTableRowFromJson(CTTableRow tableRow, JSONObject attrs) {
        boolean tableHeightRecalcRequired = false;
        final JSONObject rowAttrs = attrs.optJSONObject(OCKey.ROW.value());
        if (rowAttrs != null) {
            final Object height = rowAttrs.opt(OCKey.HEIGHT.value());
            if (height instanceof Number) {
                tableRow.setH(convert100thmmToEmu((Number) height));
                tableHeightRecalcRequired = true;
            }
        }
        return tableHeightRecalcRequired;
    }

    public static void createJsonFromTableRow(JSONObject attrs, CTTableRow row)
            throws JSONException {

        final JSONObject jsonRowAttrs = new JSONObject(1);
        jsonRowAttrs.put(OCKey.HEIGHT.value(), (int) convertEmuTo100thmm(row.getH()));
        attrs.put(OCKey.ROW.value(), jsonRowAttrs);
    }

    public static void createJsonFromTableCell(JSONObject attrs, CTTableCell cell, ThemePart themePart, Part part)
            throws JSONException {

        final JSONObject jsonCellObject = new JSONObject();
        final CTTableCellProperties cellProperties = cell.getTcPr(false);
        if (cellProperties != null) {
            createJsonFromFillProperties(jsonCellObject, cellProperties, themePart, null, part);
            final Object o = jsonCellObject.remove(OCKey.FILL.value());
            if (o instanceof JSONObject) {
                final Object c = ((JSONObject) o).remove(OCKey.COLOR.value());
                if (c != null) {
                    jsonCellObject.put(OCKey.FILL_COLOR.value(), c);
                }
                final String fillType = ((JSONObject)o).optString(OCKey.TYPE.value(), null);
                if("none".equals(fillType)) {
                    jsonCellObject.put(OCKey.FILL_TYPE.value(), fillType);
                }
                else if(c != null) {
                    jsonCellObject.put(OCKey.FILL_TYPE.value(), "solid");
                }
            }
            createJsonBorderFromLineStyle(jsonCellObject, OCKey.BORDER_LEFT.value(), cellProperties.getLnL(false), themePart, part);
            createJsonBorderFromLineStyle(jsonCellObject, OCKey.BORDER_TOP.value(), cellProperties.getLnT(false), themePart, part);
            createJsonBorderFromLineStyle(jsonCellObject, OCKey.BORDER_RIGHT.value(), cellProperties.getLnR(false), themePart, part);
            createJsonBorderFromLineStyle(jsonCellObject, OCKey.BORDER_BOTTOM.value(), cellProperties.getLnB(false), themePart, part);

            final STTextAnchoringType anchorType = cellProperties.getAnchor();
            if(anchorType!=null) {
                if(anchorType.equals(STTextAnchoringType.T)) {
                    jsonCellObject.put(OCKey.ALIGN_VERT.value(), "top");
                }
                else if(anchorType.equals(STTextAnchoringType.CTR)) {
                    jsonCellObject.put(OCKey.ALIGN_VERT.value(), "center");
                }
                else if(anchorType.equals(STTextAnchoringType.B)) {
                    jsonCellObject.put(OCKey.ALIGN_VERT.value(), "bottom");
                }
                else if(anchorType.equals(STTextAnchoringType.JUST)) {
                    jsonCellObject.put(OCKey.ALIGN_VERT.value(), "justified");
                }
                else if(anchorType.equals(STTextAnchoringType.DIST)) {
                    jsonCellObject.put(OCKey.ALIGN_VERT.value(), "distributed");
                }
            }
            final Integer marL = cellProperties.getMarL();
            if (marL != null) {
                jsonCellObject.put(OCKey.PADDING_LEFT.value(), marL.intValue() / 360);
            }
            final Integer marR = cellProperties.getMarR();
            if (marR != null) {
                jsonCellObject.put(OCKey.PADDING_RIGHT.value(), marR.intValue() / 360);
            }
            final Integer marT = cellProperties.getMarT();
            if (marT != null) {
                jsonCellObject.put(OCKey.PADDING_TOP.value(), marT.intValue() / 360);
            }
            final Integer marB = cellProperties.getMarB();
            if (marB != null) {
                jsonCellObject.put(OCKey.PADDING_BOTTOM.value(), marB.intValue() / 360);
            }
        }
        if (cell.getGridSpan() > 1) {
            jsonCellObject.put(OCKey.GRID_SPAN.value(), cell.getGridSpan());
        }
        if (!jsonCellObject.isEmpty()) {
            attrs.put(OCKey.CELL.value(), jsonCellObject);
        }
    }

    public static void applyTableCellFromJson(CTTableCell tableCell, JSONObject attrs)
            throws JSONException {

        final JSONObject cellAttrs = attrs.optJSONObject(OCKey.CELL.value());
        if (cellAttrs != null) {
            final Object fillColor = cellAttrs.opt(OCKey.FILL_COLOR.value());
            if (fillColor != null) {
                CTTableCellProperties tableCellProperties = tableCell.getTcPr(false);
                if (tableCellProperties != null) {
                    tableCellProperties.setBlipFill(null);
                    tableCellProperties.setGradFill(null);
                    tableCellProperties.setNoFill(null);
                    tableCellProperties.setGrpFill(null);
                    tableCellProperties.setPattFill(null);
                    tableCellProperties.setSolidFill(null);
                }
                if (fillColor instanceof JSONObject) {
                    tableCellProperties = tableCell.getTcPr(true);

                    final CTSolidColorFillProperties solidFill = new CTSolidColorFillProperties();
                    tableCellProperties.setSolidFill(solidFill);
                    createSolidColorFillPropertiesFromJson(solidFill, (JSONObject) fillColor);
                }
            }
            final CTTableCellProperties cellPr = tableCell.getTcPr(true);
            cellPr.setLnL(
                    applyLinePropertiesFromJsonBorder(cellAttrs, OCKey.BORDER_LEFT.value(), tableCell.getTcPr(true).getLnL(false)));
            cellPr.setLnT(
                    applyLinePropertiesFromJsonBorder(cellAttrs, OCKey.BORDER_TOP.value(), tableCell.getTcPr(true).getLnT(false)));
            cellPr.setLnR(
                    applyLinePropertiesFromJsonBorder(cellAttrs, OCKey.BORDER_RIGHT.value(), tableCell.getTcPr(true).getLnR(false)));
            cellPr.setLnB(applyLinePropertiesFromJsonBorder(cellAttrs, OCKey.BORDER_BOTTOM.value(),
                    tableCell.getTcPr(true).getLnB(false)));
            final Object gridSpan = cellAttrs.opt(OCKey.GRID_SPAN.value());
            if (gridSpan != null) {
                if (gridSpan == JSONObject.NULL) {
                    removeGridSpan(tableCell);
                } else {
                    removeGridSpan(tableCell);
                    if ((Integer) gridSpan != 1) {
                        applyGridSpan(tableCell, (Integer) gridSpan);
                    }
                }
            }
            final Object marL = cellAttrs.opt(OCKey.PADDING_LEFT.value());
            if (marL != null) {
                if (marL instanceof Number) {
                    tableCell.getTcPr(true).setMarL(Integer.valueOf(((Number) marL).intValue() * 360));
                } else {
                    tableCell.getTcPr(true).setMarL(null);
                }
            }
            final Object marR = cellAttrs.opt(OCKey.PADDING_RIGHT.value());
            if (marR != null) {
                if (marR instanceof Number) {
                    tableCell.getTcPr(true).setMarR(Integer.valueOf(((Number) marR).intValue() * 360));
                } else {
                    tableCell.getTcPr(true).setMarR(null);
                }
            }
            final Object marT = cellAttrs.opt(OCKey.PADDING_TOP.value());
            if (marT != null) {
                if (marT instanceof Number) {
                    tableCell.getTcPr(true).setMarT(Integer.valueOf(((Number) marT).intValue() * 360));
                } else {
                    tableCell.getTcPr(true).setMarT(null);
                }
            }
            final Object marB = cellAttrs.opt(OCKey.PADDING_BOTTOM.value());
            if (marB != null) {
                if (marB instanceof Number) {
                    tableCell.getTcPr(true).setMarB(Integer.valueOf(((Number) marB).intValue() * 360));
                } else {
                    tableCell.getTcPr(true).setMarB(null);
                }
            }
            final Object alignVert = cellAttrs.opt(OCKey.ALIGN_VERT.value());
            if(alignVert!=null) {
                if(alignVert==JSONObject.NULL) {
                    tableCell.getTcPr(true).setAnchor(null);
                }
                else {
                    if("top".equals(alignVert)) {
                        tableCell.getTcPr(true).setAnchor(STTextAnchoringType.T);
                    }
                    else if("center".equals(alignVert)) {
                        tableCell.getTcPr(true).setAnchor(STTextAnchoringType.CTR);
                    }
                    else if("bottom".equals(alignVert)) {
                        tableCell.getTcPr(true).setAnchor(STTextAnchoringType.B);
                    }
                    else if("justified".equals(alignVert)) {
                        tableCell.getTcPr(true).setAnchor(STTextAnchoringType.JUST);
                    }
                    else if("distributed".equals(alignVert)) {
                        tableCell.getTcPr(true).setAnchor(STTextAnchoringType.DIST);
                    }
                }
            }
        }
    }

    private static CTLineProperties applyLinePropertiesFromJsonBorder(JSONObject cell, String borderName,
            CTLineProperties lineProperties) throws JSONException {

        final Object o = cell.opt(borderName);
        if (o != null) {
            if (o instanceof JSONObject) {
                final JSONObject cellBorder = (JSONObject) o;
                if (lineProperties == null) {
                    lineProperties = new CTLineProperties();
                }
                applyBorderPropertiesFromJson(lineProperties, cellBorder);
            } else {
                lineProperties = null;
            }
        }
        return lineProperties;
    }

    private static void createJsonBorderFromLineStyle(JSONObject cell, String borderName,
            CTLineProperties lineProperties, ThemePart themePart, Part part) throws JSONException {

        if (lineProperties != null) {
            final JSONObject border = new JSONObject();
            createJsonFromLineProperties(border, themePart, null, lineProperties, part);
            final Object o = border.remove(OCKey.LINE.value());
            if (o instanceof JSONObject) {
                Object c = ((JSONObject) o).remove(OCKey.COLOR.value());
                if (c != null) {
                    border.put(OCKey.COLOR.value(), c);
                }
                c = ((JSONObject) o).remove(OCKey.TYPE.value());
                if (c != null) {
                    border.put(OCKey.STYLE.value(), c);
                }
                c = ((JSONObject) o).remove(OCKey.WIDTH.value());
                if (c != null) {
                    border.put(OCKey.WIDTH.value(), c);
                }
            }
            if (!border.isEmpty()) {
                cell.put(borderName, border);
            }
        }
    }

    private static void removeGridSpan(CTTableCell cell) {
        int gridCount = cell.getGridSpan() - 1;
        if (gridCount > 0) {
            final CTTableRow row = (CTTableRow) cell.getParent();
            final DLList<Object> tableContent = ((CTTable) row.getParent()).getContent();
            DLNode<Object> rowNode = tableContent.getNode(tableContent.indexOf(row));
            final int cellIndex = (((CTTableRow) rowNode.getData()).getContent()).indexOf(cell);
            final DLList<Object> rowContent = ((CTTableRow) rowNode.getData()).getContent();
            if (cellIndex >= 0) {
                rowContent.removeNodes(rowContent.getNode(cellIndex).getNext(), gridCount);
            }
        }
        cell.setGridSpan(null);
    }

    private static void applyGridSpan(CTTableCell cell, int gridSpan) {
        int gridCount = gridSpan - 1;
        if (gridCount > 0) {
            final CTTableRow row = (CTTableRow) cell.getParent();
            final DLList<Object> tableContent = ((CTTable) row.getParent()).getContent();
            DLNode<Object> rowNode = tableContent.getNode(tableContent.indexOf(row));
            final int cellIndex = (((CTTableRow) rowNode.getData()).getContent()).indexOf(cell);
            final DLList<Object> rowContent = ((CTTableRow) rowNode.getData()).getContent();
            for (int i = 0; i < gridCount; i++) {
                final CTTableCell mergedCell = new CTTableCell();
                mergedCell.setHMerge(true);
                mergedCell.setParent(row);
                rowContent.add(cellIndex + 1, mergedCell);
            }
        }
        cell.setGridSpan(gridSpan);
    }

    final static public String[] tableStyleOperationNames = {OCKey.WHOLE_TABLE.value(), OCKey.BAND1_HORZ.value(), OCKey.BAND2_HORZ.value(), OCKey.BAND1_VERT.value(),
            OCKey.BAND2_VERT.value(), OCKey.LAST_COL.value(), OCKey.FIRST_COL.value(), OCKey.LAST_ROW.value(), OCKey.SOUTH_EAST_CELL.value(), OCKey.SOUTH_WEST_CELL.value(), OCKey.FIRST_ROW.value(),
            OCKey.NORTH_EAST_CELL.value(), OCKey.NORTH_WEST_CELL.value()};

    public static void applyTableStyleFromJson(CTTableStyle tableStyle, JSONObject attrs,
            OfficeOpenXMLOperationDocument operationDocument, Part part)
            throws JSONException, InvalidFormatException {

        final JSONObject backgroundStyle = attrs.optJSONObject(OCKey.BACKGROUND_STYLE.value());
        if (backgroundStyle != null) {
            final CTTableBackgroundStyle tableBackgroundStyle = new CTTableBackgroundStyle();
            tableStyle.setTblBg(tableBackgroundStyle);
            final CTFillProperties tableFill = new CTFillProperties();
            tableBackgroundStyle.setFill(tableFill);
            applyFillPropertiesFromJson(tableFill, backgroundStyle, operationDocument, part);
        }
        for (int i = 0; i < CTTableStyle.tableStyleNames.length; i++) {
            tableStyle.setTableStyleByName(CTTableStyle.tableStyleNames[i],
                    applyTablePartStyleFromJson(attrs, tableStyleOperationNames[i],
                            tableStyle.getTableStyleByName(CTTableStyle.tableStyleNames[i], false)));
        }
    }

    public static void createJsonFromTableStyle(JSONObject attrs, CTTableStyle tableStyle, ThemePart themePart,
            Part part) throws JSONException {

        final CTTableBackgroundStyle backgroundStyle = tableStyle.getTblBg();
        if (backgroundStyle != null) {
            if (backgroundStyle.getFill() != null || backgroundStyle.getFillRef() != null) {
                final JSONObject back = new JSONObject();
                createJsonFromFillProperties(back, backgroundStyle.getFill(), themePart, backgroundStyle.getFillRef(),
                        part);
                if (!back.isEmpty()) {
                    attrs.put(OCKey.BACKGROUND_STYLE.value(), back);
                }
            }
        }
        for (int i = 0; i < CTTableStyle.tableStyleNames.length; i++) {
            createJsonFromTablePartStyle(attrs, tableStyleOperationNames[i],
                    tableStyle.getTableStyleByName(CTTableStyle.tableStyleNames[i], false), themePart, part);
        }
    }

    private static CTTablePartStyle applyTablePartStyleFromJson(JSONObject attrs, String condName,
            CTTablePartStyle tablePartStyle) throws JSONException {

        Object o = attrs.optJSONObject(condName);
        if (o != null) {
            if (o instanceof JSONObject) {
                final JSONObject condTableStyle = (JSONObject) o;
                final JSONObject cellAttrs = condTableStyle.optJSONObject(OCKey.CELL.value());
                if (cellAttrs != null) {
                    if (tablePartStyle == null) {
                        tablePartStyle = new CTTablePartStyle();
                    }
                    final CTTableStyleCellStyle tableStyleCellStyle = tablePartStyle.getTcStyle(true);

                    boolean checkForFillColor = true;
                    final Object fillType = cellAttrs.opt(OCKey.FILL_TYPE.value());
                    if(fillType!=null) {
                        if(fillType.equals("none")) {
                            final CTFillProperties fillProperties = new CTFillProperties();
                            fillProperties.setNoFill(new CTNoFillProperties());
                            checkForFillColor = false;
                        }
                    }
                    if(checkForFillColor) {
                        final Object color = cellAttrs.opt(OCKey.FILL_COLOR.value());
                        if (color != null) {
                            tableStyleCellStyle.setFillRef(null);
                            if (color instanceof JSONObject) {
                                final CTFillProperties fillProperties = new CTFillProperties();
                                tableStyleCellStyle.setFill(fillProperties);
                                CTSolidColorFillProperties solidFill = new CTSolidColorFillProperties();
                                fillProperties.setSolidFill(solidFill);
                                createSolidColorFillPropertiesFromJson(solidFill, (JSONObject) color);
                            } else {
                                tableStyleCellStyle.setFill(null);
                            }
                        }
                    }
                    final CTTableCellBorderStyle tableCellBorderStyle = tableStyleCellStyle.getTcBdr(true);
                    tableCellBorderStyle.setLeft(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_LEFT.value(),
                            tableCellBorderStyle.getLeft()));
                    tableCellBorderStyle.setRight(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_RIGHT.value(),
                            tableCellBorderStyle.getRight()));
                    tableCellBorderStyle.setBottom(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_BOTTOM.value(),
                            tableCellBorderStyle.getBottom()));
                    tableCellBorderStyle.setTop(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_TOP.value(),
                            tableCellBorderStyle.getTop()));
                    tableCellBorderStyle.setInsideH(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_INSIDE_HOR.value(),
                            tableCellBorderStyle.getInsideH()));
                    tableCellBorderStyle.setInsideV(applyThemeableLineStyleFromJsonBorder(cellAttrs, OCKey.BORDER_INSIDE_VERT.value(),
                            tableCellBorderStyle.getInsideV()));
                    if (tableCellBorderStyle.getLeft() == null && tableCellBorderStyle.getRight() == null
                            && tableCellBorderStyle.getBottom() == null && tableCellBorderStyle.getTop() == null
                            && tableCellBorderStyle.getInsideH() == null && tableCellBorderStyle.getInsideV() == null) {
                        tableStyleCellStyle.setTcBdr(null);
                    }
                }
                final JSONObject characterAttrs = condTableStyle.optJSONObject(OCKey.CHARACTER.value());
                if (characterAttrs != null) {
                    if (tablePartStyle == null) {
                        tablePartStyle = new CTTablePartStyle();
                    }
                    applyTableStyleTextStyleFromJson(characterAttrs, tablePartStyle.getTcTxStyle(true));
                }
            } else {
                tablePartStyle = null;
            }
        }
        return tablePartStyle;
    }

    private static void createJsonFromTablePartStyle(JSONObject attrs, String condName, CTTablePartStyle tablePartStyle,
            ThemePart themePart, Part part) throws JSONException {

        if (tablePartStyle != null) {
            final JSONObject condTableStyle = new JSONObject();
            final CTTableStyleCellStyle tableStyleCellStyle = tablePartStyle.getTcStyle(false);
            if (tableStyleCellStyle != null) {
                final JSONObject cell = new JSONObject();
                final CTFillProperties fillProperties = tableStyleCellStyle.getFill();
                final CTStyleMatrixReference fillStyleMatrixRef = tableStyleCellStyle.getFillRef();
                if (fillProperties != null || fillStyleMatrixRef != null) {
                    createJsonFromFillProperties(cell, fillProperties, themePart, fillStyleMatrixRef, part);
                    final Object o = cell.remove(OCKey.FILL.value());
                    if (o instanceof JSONObject) {
                        final Object c = ((JSONObject) o).remove(OCKey.COLOR.value());
                        if (c != null) {
                            cell.put(OCKey.FILL_COLOR.value(), c);
                        }
                        final Object t = ((JSONObject)o).remove(OCKey.TYPE.value());
                        if(t!=null) {
                            cell.put(OCKey.FILL_TYPE.value(), t);
                        }
                    }
                }
                final CTTableCellBorderStyle tableCellBorderStyle = tableStyleCellStyle.getTcBdr(false);
                if (tableCellBorderStyle != null) {
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_LEFT.value(), tableCellBorderStyle.getLeft(),
                            themePart, part);
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_RIGHT.value(), tableCellBorderStyle.getRight(),
                            themePart, part);
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_BOTTOM.value(), tableCellBorderStyle.getBottom(),
                            themePart, part);
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_TOP.value(), tableCellBorderStyle.getTop(), themePart,
                            part);
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_INSIDE_HOR.value(), tableCellBorderStyle.getInsideH(),
                            themePart, part);
                    createJsonBorderFromThemeableLineStyle(cell, OCKey.BORDER_INSIDE_VERT.value(), tableCellBorderStyle.getInsideV(),
                            themePart, part);
                }
                if (!cell.isEmpty()) {
                    condTableStyle.put(OCKey.CELL.value(), cell);
                }
            }
            final CTTableStyleTextStyle tableStyleTextStyle = tablePartStyle.getTcTxStyle(false);
            if (tableStyleTextStyle != null) {
                createJsonFromTableStyleTextStyle(condTableStyle, tableStyleTextStyle, themePart);
            }
            if (!condTableStyle.isEmpty()) {
                attrs.put(condName, condTableStyle);
            }
        }
    }

    private static CTThemeableLineStyle applyThemeableLineStyleFromJsonBorder(JSONObject cell, String borderName,
            CTThemeableLineStyle lineStyle) throws JSONException {

        final Object o = cell.opt(borderName);
        if (o != null) {
            if (o instanceof JSONObject) {
                final JSONObject cellBorder = (JSONObject) o;
                if (lineStyle == null) {
                    lineStyle = new CTThemeableLineStyle();
                }
                lineStyle.setLn(applyBorderPropertiesFromJson(lineStyle.getLn(false), cellBorder));
            } else {
                return null;
            }
        }
        if (lineStyle != null && lineStyle.getLn(false) == null && lineStyle.getLnRef() == null) {
            return null;
        }
        return lineStyle;
    }

    private static void createJsonBorderFromThemeableLineStyle(JSONObject cell, String borderName,
            CTThemeableLineStyle lineStyle, ThemePart themePart, Part part) throws JSONException {

        if (lineStyle != null) {
            final JSONObject border = new JSONObject();
            createJsonFromLineProperties(border, themePart, lineStyle.getLnRef(), lineStyle.getLn(false), part);
            final Object o = border.remove(OCKey.LINE.value());
            if (o instanceof JSONObject) {
                Object c = ((JSONObject) o).remove(OCKey.COLOR.value());
                if (c != null) {
                    border.put(OCKey.COLOR.value(), c);
                }
                c = ((JSONObject) o).remove(OCKey.TYPE.value());
                if (c != null) {
                    border.put(OCKey.STYLE.value(), c);
                }
                c = ((JSONObject) o).remove(OCKey.WIDTH.value());
                if (c != null) {
                    border.put(OCKey.WIDTH.value(), c);
                }
            }
            if (!border.isEmpty()) {
                cell.put(borderName, border);
            }
        }
    }

    private static void applyTableStyleTextStyleFromJson(JSONObject characterAttrs, CTTableStyleTextStyle textStyle)
            throws JSONException {

        final Iterator<Entry<String, Object>> entries = characterAttrs.entrySet().iterator();
        while (entries.hasNext()) {
            final Entry<String, Object> entry = entries.next();
            final Object value = entry.getValue();
            switch (OCKey.fromValue(entry.getKey())) {
            case BOLD : {
                if (value == JSONObject.NULL) {
                    textStyle.setB(null);
                } else {
                    textStyle.setB(((Boolean) value).booleanValue() ? STOnOffStyleType.ON : STOnOffStyleType.OFF);
                }
                break;
            }
            case COLOR : {
                createSolidColorFillPropertiesFromJson(textStyle, (JSONObject) value);
                break;
            }
            }
        }
    }

    private static void createJsonFromTableStyleTextStyle(JSONObject attrs, CTTableStyleTextStyle textStyle,
            ThemePart themePart) throws JSONException {

        final JSONObject character = new JSONObject();
        final CTFontReference fontReference = textStyle.getFontRef(false);
        if (fontReference != null && themePart != null) {
            final STFontCollectionIndex fontCollectionIndex = fontReference.getIdx();
            if (fontCollectionIndex != null) {
                if (fontCollectionIndex == STFontCollectionIndex.MAJOR) {
                    character.put(OCKey.FONT_NAME.value(), new ThemeFonts(themePart).getMajorFont());
                } else if (fontCollectionIndex == STFontCollectionIndex.MINOR) {
                    character.put(OCKey.FONT_NAME.value(), new ThemeFonts(themePart).getMajorFont());
                }
            }
/*
  #61523# The file format of the tcTxStyle seems to be broken somehow. A text color can be defined at two different places ...
  It seems that MS is not using the color defined in the fontRef instead it only uses the color defined in tcTxStyle.
   <a:tcTxStyle b="on">
        <a:fontRef idx="minor">
           <a:prstClr val="black"/>
       </a:fontRef>
       <a:schemeClr val="lt1"/>
   </a:tcTxStyle>
*/

/*
            final JSONObject refColor = createJsonColorFromSolidColorFillProperties(fontReference);
            if (refColor != null) {
                character.put(OCKey.COLOR.value(), refColor);
            }
*/
        }
        final STOnOffStyleType bold = textStyle.getB();
        if (bold != null) {
            character.put(OCKey.BOLD.value(), bold == STOnOffStyleType.ON);
        }
        final FontCollection fontCollection = textStyle.getFont();
        if (fontCollection != null) {
            //
        }
        final JSONObject color = createJsonColorFromSolidColorFillProperties(textStyle);
        if (color != null) {
            character.put(OCKey.COLOR.value(), color);
        }
        if (!character.isEmpty()) {
            attrs.put(OCKey.CHARACTER.value(), character);
        }
    }

    public static void createJsonFromFillProperties(JSONObject attrs, IFillProperties fillProperties,
        ThemePart themePart, IStyleMatrixReference fillStyleReference, Part part) throws JSONException {
        createJsonFromFillProperties(attrs, fillProperties, themePart, fillStyleReference, part, OCKey.FILL.value());
    }

    public static void createJsonFromMarkerFillProperties(JSONObject attrs, IFillProperties fillProperties,
        ThemePart themePart, IStyleMatrixReference fillStyleReference, Part part) throws JSONException {
        createJsonFromFillProperties(attrs, fillProperties, themePart, fillStyleReference, part, OCKey.MARKER_FILL.value());
    }

    public static void createJsonFromFillProperties(JSONObject attrs, IFillProperties fillProperties,
            ThemePart themePart, IStyleMatrixReference fillStyleReference, Part part, String attributeName) throws JSONException {

        final JSONObject initialFillAttrs = attrs.optJSONObject(attributeName);
        final JSONObject fillAttrs = initialFillAttrs != null ? initialFillAttrs : new JSONObject(1);

        if (fillStyleReference != null) {
            // try to apply the style first...
            createJsonFromFillStyleMatrix(themePart, fillStyleReference, fillAttrs);
        }

        if (fillProperties != null) {

            // then each hard applied fill attribute
            if (fillProperties.getNoFill() != null) {
                createJsonFromFillStyle(fillProperties.getNoFill(), null, fillAttrs, part);
            } else if (fillProperties.getSolidFill() != null) {
                createJsonFromFillStyle(fillProperties.getSolidFill(), null, fillAttrs, part);
            } else if (fillProperties.getBlipFill() != null) {
                createJsonFromFillStyle(fillProperties.getBlipFill(), null, fillAttrs, part);
            } else if (fillProperties.getGradFill() != null) {
                createJsonFromFillStyle(fillProperties.getGradFill(), null, fillAttrs, part);
            } else if (fillProperties.getPattFill() != null) {
                createJsonFromFillStyle(fillProperties.getPattFill(), null, fillAttrs, part);
            } else if (fillProperties.getGrpFill() != null) {
                Object p = fillProperties.getParent();
                while(p!=null) {
                    if(p instanceof IGroupShape) {
                        final CTGroupShapeProperties grpProperties = ((IGroupShape)p).getGrpSpPr();
                        if(grpProperties!=null&&grpProperties.getGrpFill()==null) {
                            createJsonFromFillProperties(attrs, grpProperties, themePart, fillStyleReference, part, attributeName);
                            return;
                        }
                    }
                    p = p instanceof Child ? ((Child)p).getParent() : null;
                }
            }
        }
        if (initialFillAttrs == null && !fillAttrs.isEmpty()) {
            attrs.put(attributeName, fillAttrs);
        }
    }

    public static void applyConnectionPropertiesFromJson(JSONObject attrs, IConnectionAccessor connectionAccessor) {

        final JSONObject connector = attrs.optJSONObject(OCKey.CONNECTOR.value());
        if (connector != null) {
            final Object startId = connector.opt(OCKey.START_ID.value());
            if (startId != null) {
                if (startId == JSONObject.NULL || ((String) startId).isEmpty()) {
                    connectionAccessor.setStartConnection(null);
                } else {
                    connectionAccessor.getStartConnection(true).setId(Long.parseLong(((String) startId)));
                }
            }
            final Object startIndex = connector.opt(OCKey.START_INDEX.value());
            if (startIndex != null) {
                if (startIndex == JSONObject.NULL) {
                    connectionAccessor.setStartConnection(null);
                } else {
                    connectionAccessor.getStartConnection(true).setIdx(((Number) startIndex).longValue());
                }
            }
            final Object endId = connector.opt(OCKey.END_ID.value());
            if (endId != null) {
                if (endId == JSONObject.NULL || ((String) endId).isEmpty()) {
                    connectionAccessor.setEndConnection(null);
                } else {
                    connectionAccessor.getEndConnection(true).setId(Long.parseLong(((String) endId)));
                }
            }
            final Object endIndex = connector.opt(OCKey.END_INDEX.value());
            if (endIndex != null) {
                if (endIndex == JSONObject.NULL) {
                    connectionAccessor.setEndConnection(null);
                } else {
                    connectionAccessor.getEndConnection(true).setIdx(((Number) endIndex).longValue());
                }
            }
        }
    }

    public static void createJsonFromConnectionProperties(JSONObject attrs, IConnectionAccessor connectionAccessor)
            throws JSONException {

        final CTConnection startConnection = connectionAccessor.getStartConnection(false);
        final CTConnection endConnection = connectionAccessor.getEndConnection(false);
        if (startConnection != null || endConnection != null) {
            final JSONObject connector = new JSONObject(2);
            if (startConnection != null) {
                connector.put(OCKey.START_ID.value(), Long.toString(startConnection.getId()));
                connector.put(OCKey.START_INDEX.value(), startConnection.getIdx());
            }
            if (endConnection != null) {
                connector.put(OCKey.END_ID.value(), Long.toString(endConnection.getId()));
                connector.put(OCKey.END_INDEX.value(), endConnection.getIdx());
            }
            attrs.put(OCKey.CONNECTOR.value(), connector);
        }
    }

    public static CTShapeProperties applyShapePropertiesFromJson(CTShapeProperties shapeProperties, JSONObject attrs,
            OfficeOpenXMLOperationDocument operationDocument, Part part, boolean rootShape)
            throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if (shapeProperties == null) {
            shapeProperties = Context.getDmlObjectFactory().createCTShapeProperties();
        }
        applyTransform2DFromJson(shapeProperties, attrs, rootShape);
        final JSONObject lineAttrs = attrs.optJSONObject(OCKey.LINE.value());
        if (lineAttrs != null) {
            shapeProperties.setLn(applyLinePropertiesFromJson(shapeProperties.getLn(), lineAttrs));
        }
        applyFillPropertiesFromJson(shapeProperties, attrs, operationDocument, part);

        final JSONObject geometryAttrs = attrs.optJSONObject(OCKey.GEOMETRY.value());
        if (geometryAttrs != null) {
            DMLGeometry.applyGeometryPropertiesFromJson(geometryAttrs, shapeProperties);
        }
        return shapeProperties;
    }

    public static void createJsonFromFontRef(JSONObject attrs, IFontReference fontStyleReference) throws JSONException {

        final JSONObject fontColor = new JSONObject();
        createJsonColorFromSolidColorFillProperties(fontColor, fontStyleReference);
        if (!fontColor.isEmpty()) {
            Tools.addFamilyAttribute(attrs, OCKey.CHARACTER, OCKey.COLOR, fontColor);
        }
    }

    public static void createJsonFromLineProperties(JSONObject attrs, ThemePart themePart,
        IStyleMatrixReference lineStyleReference, CTLineProperties lineProperties, Part part) throws JSONException {
        createJsonFromLineProperties(attrs, themePart, lineStyleReference, lineProperties, part, OCKey.LINE.value());
    }

    public static void createJsonFromMarkerLineProperties(JSONObject attrs, ThemePart themePart,
        IStyleMatrixReference lineStyleReference, CTLineProperties lineProperties, Part part) throws JSONException {
        createJsonFromLineProperties(attrs, themePart, lineStyleReference, lineProperties, part, OCKey.MARKER_BORDER.value());
    }

    public static void createJsonFromLineProperties(JSONObject attrs, ThemePart themePart,
            IStyleMatrixReference lineStyleReference, CTLineProperties lineProperties, Part part, String lineAttribute) throws JSONException {

        final JSONObject initialLineAttrs = attrs.optJSONObject(lineAttribute);
        final JSONObject lineAttrs = initialLineAttrs != null ? initialLineAttrs : new JSONObject(2);

        if (lineStyleReference != null) {
            // try to apply the style first...
            createJsonFromLineStyleMatrix(themePart, lineStyleReference, lineAttrs);
        }
        if (lineProperties != null) {
            createJsonFromLineStyle(lineProperties, null, lineAttrs, part);
        }

        if (initialLineAttrs == null && !lineAttrs.isEmpty()) {
            attrs.put(lineAttribute, lineAttrs);
        }
    }

    public static CTLineProperties applyLinePropertiesFromJson(CTLineProperties lineProperties, JSONObject lineAttrs)
            throws JSONException {

        if (lineProperties == null) {
            lineProperties = Context.getDmlObjectFactory().createCTLineProperties();
        }
        final Object typeO = lineAttrs.opt(OCKey.TYPE.value());
        if (null != typeO) {
            final String type = typeO instanceof String ? (String) typeO : "none";
            final CTSolidColorFillProperties oldSolidFill = lineProperties.getSolidFill();
            lineProperties.setNoFill(null);
            lineProperties.setSolidFill(null);
            lineProperties.setGradFill(null);
            lineProperties.setPattFill(null);
            if (type.equals("none")) {
                lineProperties.setNoFill(Context.getDmlObjectFactory().createCTNoFillProperties());
            } else if (type.equals("solid")) {
                if (oldSolidFill != null) {
                    lineProperties.setSolidFill(oldSolidFill);
                } else {
                    lineProperties.setSolidFill(Context.getDmlObjectFactory().createCTSolidColorFillProperties());
                }
            }
        }
        final Object color = lineAttrs.opt(OCKey.COLOR.value());
        if (color != null) {
            // applying color only possible if fill type is solid
            CTSolidColorFillProperties solidFill = lineProperties.getSolidFill();
            if (solidFill == null && lineProperties.getNoFill() == null && lineProperties.getGradFill() == null
                    && lineProperties.getPattFill() == null) {
                // solid fill is default in drawingML we have to create it
                solidFill = Context.getDmlObjectFactory().createCTSolidColorFillProperties();
                lineProperties.setSolidFill(solidFill);
            }
            if (solidFill != null) {
                if (color instanceof JSONObject) {
                    createSolidColorFillPropertiesFromJson(solidFill, (JSONObject) color);
                } else {
                    final CTPresetColor presetColor = Context.getDmlObjectFactory().createCTPresetColor();
                    presetColor.setVal(STPresetColorVal.BLACK);
                    solidFill.setPrstClr(presetColor);
                }
            }
        }
        final Object width = lineAttrs.opt(OCKey.WIDTH.value());
        if (width != null) {
            if (width instanceof Number) {
                lineProperties.setW(convert100thmmToEmu((Number) width));
            } else {
                lineProperties.setW(null);
            }
        }
        final Object style = lineAttrs.opt(OCKey.STYLE.value());
        if (style != null) {
            CTPresetLineDashProperties presetDash = null;
            if (style instanceof String) {
                if (((String) style).equals("dotted")) {
                    presetDash = Context.getDmlObjectFactory().createCTPresetLineDashProperties();
                    presetDash.setVal(STPresetLineDashVal.DOT);
                } else if (((String) style).equals("dashed")) {
                    presetDash = Context.getDmlObjectFactory().createCTPresetLineDashProperties();
                    presetDash.setVal(STPresetLineDashVal.DASH);
                } else if (((String) style).equals("dashDot")) {
                    presetDash = Context.getDmlObjectFactory().createCTPresetLineDashProperties();
                    presetDash.setVal(STPresetLineDashVal.DASH_DOT);
                } else if (((String) style).equals("dashDotDot")) {
                    presetDash = Context.getDmlObjectFactory().createCTPresetLineDashProperties();
                    presetDash.setVal(STPresetLineDashVal.LG_DASH_DOT_DOT);
                }
            }
            lineProperties.setPrstDash(presetDash);
        }
        final Object headEndType = lineAttrs.opt(OCKey.HEAD_END_TYPE.value());
        final Object headEndLength = lineAttrs.opt(OCKey.HEAD_END_LENGTH.value());
        final Object headEndWidth = lineAttrs.opt(OCKey.HEAD_END_WIDTH.value());
        if (headEndType != null || headEndLength != null || headEndWidth != null) {
            applyLineEndType(lineProperties.getHeadEnd(true), headEndType, headEndLength, headEndWidth);
        }
        final Object tailEndType = lineAttrs.opt(OCKey.TAIL_END_TYPE.value());
        final Object tailEndLength = lineAttrs.opt(OCKey.TAIL_END_LENGTH.value());
        final Object tailEndWidth = lineAttrs.opt(OCKey.TAIL_END_WIDTH.value());
        if (tailEndType != null || tailEndLength != null || tailEndWidth != null) {
            applyLineEndType(lineProperties.getTailEnd(true), tailEndType, tailEndLength, tailEndWidth);
        }
        return lineProperties;
    }

    private static void applyLineEndType(CTLineEndProperties lineEndProperties, Object endType, Object endLength,
            Object endWidth) {
        if (endType != null) {
            if (endType == JSONObject.NULL) {
                lineEndProperties.setType(STLineEndType.NONE);
            } else {
                STLineEndType lineEndType = STLineEndType.NONE;
                if (((String) endType).equals("triangle")) {
                    lineEndType = STLineEndType.TRIANGLE;
                } else if (((String) endType).equals("stealth")) {
                    lineEndType = STLineEndType.STEALTH;
                } else if (((String) endType).equals("diamond")) {
                    lineEndType = STLineEndType.DIAMOND;
                } else if (((String) endType).equals("oval")) {
                    lineEndType = STLineEndType.OVAL;
                } else if (((String) endType).equals("arrow")) {
                    lineEndType = STLineEndType.ARROW;
                }
                lineEndProperties.setType(lineEndType);
            }
        }
        if (endLength != null) {
            if (endLength == JSONObject.NULL) {
                lineEndProperties.setLen(STLineEndLength.MED);
            } else {
                STLineEndLength lineEndLength = STLineEndLength.MED;
                if (((String) endLength).equals("small")) {
                    lineEndLength = STLineEndLength.SM;
                } else if (((String) endLength).equals("stealth")) {
                    lineEndLength = STLineEndLength.LG;
                }
                lineEndProperties.setLen(lineEndLength);
            }
        }
        if (endWidth != null) {
            if (endWidth == JSONObject.NULL) {
                lineEndProperties.setW(STLineEndWidth.MED);
            } else {
                STLineEndWidth lineEndWidth = STLineEndWidth.MED;
                if (((String) endWidth).equals("small")) {
                    lineEndWidth = STLineEndWidth.SM;
                } else if (((String) endWidth).equals("stealth")) {
                    lineEndWidth = STLineEndWidth.LG;
                }
                lineEndProperties.setW(lineEndWidth);
            }
        }
    }

    public static CTLineProperties applyBorderPropertiesFromJson(CTLineProperties lineProperties,
            JSONObject borderAttrs) throws JSONException {

        if (lineProperties == null) {
            lineProperties = Context.getDmlObjectFactory().createCTLineProperties();
        }
        final Object typeO = borderAttrs.opt(OCKey.STYLE.value());
        if (null != typeO) {
            final String type = typeO instanceof String ? (String) typeO : "none";
            final CTSolidColorFillProperties oldSolidFill = lineProperties.getSolidFill();
            lineProperties.setNoFill(null);
            lineProperties.setSolidFill(null);
            lineProperties.setGradFill(null);
            lineProperties.setPattFill(null);
            if (type.equals("none")) {
                lineProperties.setNoFill(Context.getDmlObjectFactory().createCTNoFillProperties());
            } else {
                if (oldSolidFill != null) {
                    lineProperties.setSolidFill(oldSolidFill);
                } else {
                    lineProperties.setSolidFill(Context.getDmlObjectFactory().createCTSolidColorFillProperties());
                }
            }
        }
        final Object color = borderAttrs.opt(OCKey.COLOR.value());
        if (color != null) {
            // applying color only possible if fill type is solid
            final CTSolidColorFillProperties solidFill = lineProperties.getSolidFill();
            if (solidFill != null) {
                if (color instanceof JSONObject) {
                    createSolidColorFillPropertiesFromJson(solidFill, (JSONObject) color);
                } else {
                    final CTPresetColor presetColor = Context.getDmlObjectFactory().createCTPresetColor();
                    presetColor.setVal(STPresetColorVal.BLACK);
                    solidFill.setPrstClr(presetColor);
                }
            }
        }
        final Object width = borderAttrs.opt(OCKey.WIDTH.value());
        if (width != null) {
            if (width instanceof Number) {
                lineProperties.setW(convert100thmmToEmu((Number) width));
            } else {
                lineProperties.setW(null);
            }
        }
        return lineProperties;
    }

    public static JSONObject createJsonColorFromSolidColorFillProperties(IColorChoice fillProperties)
            throws JSONException {

        return createJsonColorFromSolidColorFillProperties(null, fillProperties);
    }

    public static JSONObject createJsonColorFromSolidColorFillProperties(JSONObject sourceColor,
            IColorChoice fillProperties) throws JSONException {

        if (fillProperties != null) {
            if (fillProperties.getScrgbClr() != null) {
                final CTScRgbColor color = fillProperties.getScrgbClr();
                return JSONHelper.makeColor(sourceColor, "crgb",
                		JSONHelper.makeJSON(OCKey.R.value(), color.getR(), OCKey.G.value(), color.getG(), OCKey.B.value(), color.getB()),
                        color.getEGColorTransform());
            } else if (fillProperties.getSrgbClr() != null) {
                final CTSRgbColor color = fillProperties.getSrgbClr();
                final byte[] value = color.getVal();
                return JSONHelper.makeColor(sourceColor, "rgb", Commons.bytesToHexString(value, 0, 3),
                        color.getEGColorTransform());
            } else if (fillProperties.getHslClr() != null) {
                final CTHslColor color = fillProperties.getHslClr();
                return JSONHelper.makeColor(sourceColor, "hsl",
                        JSONHelper.makeJSON(OCKey.H.value(), color.getHue(), OCKey.S.value(), color.getSat(), OCKey.L.value(), color.getLum()),
                        color.getEGColorTransform());
            } else if (fillProperties.getSchemeClr() != null) {
                final CTSchemeColor color = fillProperties.getSchemeClr();
                String value = null;
                final STSchemeColorVal sourveVal = color.getVal();
                if (sourveVal != null) {
                    switch (sourveVal) {
                    case ACCENT_1:
                    case ACCENT_2:
                    case ACCENT_3:
                    case ACCENT_4:
                    case ACCENT_5:
                    case ACCENT_6:
                        value = sourveVal.value();
                        break;
                    case BG_1:
                        value = "background1";
                        break;
                    case BG_2:
                        value = "background2";
                        break;
                    case DK_1:
                        value = "dark1";
                        break;
                    case DK_2:
                        value = "dark2";
                        break;
                    case TX_1:
                        value = "text1";
                        break;
                    case TX_2:
                        value = "text2";
                        break;
                    case LT_1:
                        value = "light1";
                        break;
                    case LT_2:
                        value = "light2";
                        break;
                    case HLINK:
                        value = "hyperlink";
                        break;
                    case FOL_HLINK:
                        value = "followedHyperlink";
                        break;
                    case PH_CLR:
                        value = "phClr";
                        break;
                    }
                }
                return JSONHelper.makeColor(sourceColor, "scheme", value, color.getEGColorTransform());
            } else if (fillProperties.getSysClr() != null) {
                final CTSystemColor color = fillProperties.getSysClr();
                return JSONHelper.makeColor(sourceColor, "system", color.getVal(), color.getEGColorTransform());
            } else if (fillProperties.getPrstClr() != null) {
                final CTPresetColor color = fillProperties.getPrstClr();
                return JSONHelper.makeColor(sourceColor, "preset", color.getVal().value(), color.getEGColorTransform());
            }
        }
        return null;
    }

    public static boolean createSolidColorFillPropertiesFromJson(IColorChoice docx, JSONObject json)
            throws JSONException {
        docx.setSysClr(null);
        docx.setScrgbClr(null);
        docx.setSrgbClr(null);
        docx.setHslClr(null);
        docx.setPrstClr(null);
        docx.setSchemeClr(null);

        final String type = json.getString(OCKey.TYPE.value());

        if (type.equals("auto")) {
            return false;
        } else if (type.equals("scheme")) {
            final CTSchemeColor schemeClr = new CTSchemeColor();
            createSchemeColorFromJson(schemeClr, json);
            docx.setSchemeClr(schemeClr);
        } else if (type.equals("rgb")) {
            final CTSRgbColor srgbClr = new CTSRgbColor();
            createSRgbColorFromJson(srgbClr, json);
            docx.setSrgbClr(srgbClr);
        } else if (type.equals("crgb")) {
            final CTScRgbColor crgbClr = new CTScRgbColor();
            final JSONObject value = json.getJSONObject(OCKey.VALUE.value());
            crgbClr.setR(value.getInt(OCKey.R.value()));
            crgbClr.setG(value.getInt(OCKey.G.value()));
            crgbClr.setB(value.getInt(OCKey.B.value()));
            JSONHelper.saveTransformations(crgbClr.getEGColorTransform(), json);
            docx.setScrgbClr(crgbClr);
        } else if (type.equals("hsl")) {
            final CTHslColor hslClr = new CTHslColor();
            final JSONObject value = json.getJSONObject(OCKey.VALUE.value());
            hslClr.setHue(value.getInt(OCKey.H.value()));
            hslClr.setSat(value.getInt(OCKey.S.value()));
            hslClr.setLum(value.getInt(OCKey.L.value()));
            JSONHelper.saveTransformations(hslClr.getEGColorTransform(), json);
            docx.setHslClr(hslClr);
        } else if (type.equals("system")) {
            final CTSystemColor systemClr = new CTSystemColor();
            systemClr.setVal(json.getString(OCKey.VALUE.value()));
            JSONHelper.saveTransformations(systemClr.getEGColorTransform(), json);
            docx.setSysClr(systemClr);
        } else if (type.equals("preset")) {
            final CTPresetColor presetClr = new CTPresetColor();
            presetClr.setVal(STPresetColorVal.fromValue(json.getString(OCKey.VALUE.value())));
            JSONHelper.saveTransformations(presetClr.getEGColorTransform(), json);
            docx.setPrstClr(presetClr);
        } else {
            Logger.getAnonymousLogger().warning("colortype is not implemented " + type);
            // TODO: other colors?
        }
        return true;
    }

    public static void createSchemeColorFromJson(CTSchemeColor docx, JSONObject json) throws JSONException {
        final String value = json.getString(OCKey.VALUE.value());
        final STSchemeColorVal colorVal;
        if (value.equals("background1")) {
            colorVal = STSchemeColorVal.BG_1;
        } else if (value.equals("background2")) {
            colorVal = STSchemeColorVal.BG_2;
        } else if (value.equals("dark1")) {
            colorVal = STSchemeColorVal.DK_1;
        } else if (value.equals("dark2")) {
            colorVal = STSchemeColorVal.DK_2;
        } else if (value.equals("text1")) {
            colorVal = STSchemeColorVal.TX_1;
        } else if (value.equals("text2")) {
            colorVal = STSchemeColorVal.TX_2;
        } else if (value.equals("light1")) {
            colorVal = STSchemeColorVal.LT_1;
        } else if (value.equals("light2")) {
            colorVal = STSchemeColorVal.LT_2;
        } else if (value.equals("hyperlink")) {
            colorVal = STSchemeColorVal.HLINK;
        } else if (value.equals("followedHyperlink")) {
            colorVal = STSchemeColorVal.FOL_HLINK;
        } else {
            STSchemeColorVal tmpVal;
            try {
                tmpVal = STSchemeColorVal.fromValue(value);
            } catch (final IllegalArgumentException e) {
                Logger.getAnonymousLogger().warning("could not find STSchemeColorVal for " + value);
                tmpVal = STSchemeColorVal.TX_1;
            }
            colorVal = tmpVal;
        }
        docx.setVal(colorVal);
        JSONHelper.saveTransformations(docx.getEGColorTransform(), json);
    }

    public static void createSRgbColorFromJson(CTSRgbColor docx, JSONObject json) throws JSONException {
        docx.setVal(Commons.hexStringToBytes(json.getString(OCKey.VALUE.value())));
        JSONHelper.saveTransformations(docx.getEGColorTransform(), json);
    }

    public static void createJsonFromNonVisualDrawingShapeProperties(JSONObject attrs,
            INonVisualDrawingShapePropertyAccess iNonVisualDrawingShapePropertyAccess) throws JSONException {

        final ILocksAccess iLocksAccess = iNonVisualDrawingShapePropertyAccess
                .getNonVisualDrawingShapeProperties(false);
        if (iLocksAccess == null) {
            return;
        }
        final CTLockingBase lockingBase = iLocksAccess.getLocks(false);
        if (lockingBase == null) {
            return;
        }
        final JSONObject initialDrawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        final JSONObject drawingAttrs = initialDrawingAttrs != null ? initialDrawingAttrs : new JSONObject();

        if (lockingBase.isNoChangeAspect()) {
            drawingAttrs.put(OCKey.ASPECT_LOCKED.value(), true);
        }
        if (lockingBase.isNoMove()) {
            drawingAttrs.put(OCKey.NO_MOVE.value(), true);
        }
        if (lockingBase.isNoResize()) {
            drawingAttrs.put(OCKey.NO_RESIZE.value(), true);
        }
        if (lockingBase.isNoSelect()) {
            drawingAttrs.put(OCKey.NO_SELECT.value(), true);
        }
        if (lockingBase.isNoGrp()) {
            drawingAttrs.put(OCKey.NO_GROUP.value(), true);
        }
        if (lockingBase instanceof CTLockingBase.INoRot) {
            if (((CTLockingBase.INoRot) lockingBase).isNoRot()) {
                drawingAttrs.put(OCKey.NO_ROTATION.value(), true);
            }
        }
        if (initialDrawingAttrs == null && !drawingAttrs.isEmpty()) {
            attrs.put(OCKey.DRAWING.value(), drawingAttrs);
        }
    }

    public static void applyNonVisualDrawingShapeProperties(
            INonVisualDrawingShapePropertyAccess iNonVisualDrawingShapePropertyAccess, JSONObject drawingProperties) {
        if (drawingProperties == null) {
            return;
        }
        Object o = drawingProperties.opt(OCKey.ASPECT_LOCKED.value());
        if (o != null) {
            if (o instanceof Boolean) {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoChangeAspect((Boolean) o);
            } else {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoChangeAspect(null);
            }
        }
        o = drawingProperties.opt(OCKey.NO_MOVE.value());
        if (o != null) {
            if (o instanceof Boolean) {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoMove((Boolean) o);
            } else {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoMove(null);
            }
        }
        o = drawingProperties.opt(OCKey.NO_RESIZE.value());
        if (o != null) {
            if (o instanceof Boolean) {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoResize((Boolean) o);
            } else {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoResize(null);
            }
        }
        o = drawingProperties.opt(OCKey.NO_SELECT.value());
        if (o != null) {
            if (o instanceof Boolean) {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoSelect((Boolean) o);
            } else {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoSelect(null);
            }
        }
        o = drawingProperties.opt(OCKey.NO_GROUP.value());
        if (o != null) {
            if (o instanceof Boolean) {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoGrp((Boolean) o);
            } else {
                getLockingBase(iNonVisualDrawingShapePropertyAccess).setNoGrp(null);
            }
        }
        o = drawingProperties.opt(OCKey.NO_ROTATION.value());
        if (o != null) {
            final CTLockingBase lockingBase = getLockingBase(iNonVisualDrawingShapePropertyAccess);
            if (lockingBase instanceof CTLockingBase.INoRot) {
                if (o instanceof Boolean) {
                    ((CTLockingBase.INoRot) lockingBase).setNoRot((Boolean) o);
                } else {
                    ((CTLockingBase.INoRot) lockingBase).setNoRot(null);
                }
            }
        }
    }

    private static CTLockingBase getLockingBase(
            INonVisualDrawingShapePropertyAccess iNonVisualDrawingShapePropertyAccess) {
        return iNonVisualDrawingShapePropertyAccess.getNonVisualDrawingShapeProperties(true).getLocks(true);
    }

    public static void createJsonFromNonVisualDrawingProperties(JSONObject attrs,
            INonVisualDrawingPropertyAccess iNonVisualDrawingProps) throws JSONException {

        final CTNonVisualDrawingProps nonVisualDrawingProps = iNonVisualDrawingProps
                .getNonVisualDrawingProperties(false);
        if (nonVisualDrawingProps == null) {
            return;
        }
        final JSONObject initialDrawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        final JSONObject drawingAttrs = initialDrawingAttrs != null ? initialDrawingAttrs : new JSONObject();

        final String name = nonVisualDrawingProps.getName();
        if (name != null && !name.isEmpty()) {
            drawingAttrs.put(OCKey.NAME.value(), name);
        }
        final Integer id = nonVisualDrawingProps.getId();
        if(id!=null) {
            drawingAttrs.put(OCKey.ID.value(), id.toString());
        }
        final String description = nonVisualDrawingProps.getDescr();
        if (description != null && !description.isEmpty()) {
            drawingAttrs.put(OCKey.DESCRIPTION.value(), description);
        }
        if (nonVisualDrawingProps.isHidden()) {
            drawingAttrs.put(OCKey.HIDDEN.value(), true);
        }
        if (initialDrawingAttrs == null && !drawingAttrs.isEmpty()) {
            attrs.put(OCKey.DRAWING.value(), drawingAttrs);
        }
    }

    public static void applyNonVisualDrawingProperties(INonVisualDrawingPropertyAccess iNonVisualDrawingProperties,
            JSONObject drawingProperties) {
        if (drawingProperties == null) {
            return;
        }
        final Object optName = drawingProperties.opt(OCKey.NAME.value());
        final Object optDescription = drawingProperties.opt(OCKey.DESCRIPTION.value());
        final Object optId = drawingProperties.opt(OCKey.ID.value());
        if (optName!=null || optDescription!=null || optId!=null) {
            final CTNonVisualDrawingProps nonVisualDrawingProperties = iNonVisualDrawingProperties.getNonVisualDrawingProperties(true);
            if (optName!=null) {
                if (optName instanceof String) {
                    nonVisualDrawingProperties.setName((String) optName);
                } else {
                    // name is a required xml attribute... so we are setting a
                    // dummy
                    nonVisualDrawingProperties.setName("...");
                }
            }
            if (optDescription!=null) {
                if (optDescription instanceof String) {
                    nonVisualDrawingProperties.setDescr((String) optDescription);
                } else {
                    nonVisualDrawingProperties.setDescr(null);
                }
            }
            if (optId!=null) {
                if(optId==JSONObject.NULL) {
                    nonVisualDrawingProperties.setId(null);
                }
                else if (optId instanceof String) {
                    try {
                        nonVisualDrawingProperties.setId(Integer.parseInt((String)optId));
                    }
                    catch (NumberFormatException e) {
                        nonVisualDrawingProperties.setId(null);
                    }
                }
            }
        }
    }

    public static void createTransform2D(ITransform2D transform2D, int x100thmm, int y100thmm, int width100thmm,
            int height100thmm) {
        final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();
        final CTPoint2D point2D = dmlObjectFactory.createCTPoint2D();
        point2D.setX(Commons.coordinateFrom100TH_MM(x100thmm));
        point2D.setY(Commons.coordinateFrom100TH_MM(y100thmm));
        transform2D.setOff(point2D);
        final CTPositiveSize2D positiveSize2D = dmlObjectFactory.createCTPositiveSize2D();
        positiveSize2D.setCx(Commons.coordinateFrom100TH_MM(width100thmm));
        positiveSize2D.setCy(Commons.coordinateFrom100TH_MM(height100thmm));
    }

    public static CTPresetGeometry2D createPresetGeometry2D(STShapeType presetType) {
        final CTPresetGeometry2D presetGeometry2D = Context.getDmlObjectFactory().createCTPresetGeometry2D();
        presetGeometry2D.setPrst(presetType);
        return presetGeometry2D;
    }

    public static CTBlipFillProperties createBlipFillProperties(CTBlipFillProperties sourceBlipFill,
            OfficeOpenXMLOperationDocument operationDocument, Part part, String imageUrl)
            throws InvalidFormatException, PartUnrecognisedException {

        final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();
        final CTBlipFillProperties blipFillProperties = sourceBlipFill != null ? sourceBlipFill
                : dmlObjectFactory.createCTBlipFillProperties();
        blipFillProperties.setBlip(createCTBlip(operationDocument, part, imageUrl));
        blipFillProperties.setStretch(dmlObjectFactory.createCTStretchInfoProperties());
        return blipFillProperties;
    }

    public static CTBlip createCTBlip(OfficeOpenXMLOperationDocument operationDocument, Part part, String imageUrl)
            throws InvalidFormatException, PartUnrecognisedException {

        final CTBlip blip = Context.getDmlObjectFactory().createCTBlip();
        final Relationship relationship = Commons.createGraphicRelation(operationDocument, part, imageUrl);
        if (relationship != null) {
            final boolean linked = relationship.getTargetMode() != null
                    && relationship.getTargetMode().equals("External");
            if (linked) {
                blip.setLink(relationship.getId());
            } else {
                blip.setEmbed(relationship.getId());
            }
        }
        return blip;
    }

    public static void handleStandardTypes(CTTextBody txPr) {
        CTTextBodyProperties body = txPr.getBodyPr();
        if (body == null) {
            body = new CTTextBodyProperties();
            txPr.setBodyPr(body);
        }

        CTTextListStyle lst2 = txPr.getLstStyle();
        if (lst2 == null) {
            lst2 = new CTTextListStyle();
            txPr.setLstStyle(lst2);
        }
    }

    /**
     * @param part
     *            (partName Uri should be /word/document.xml)
     * @return the imageUrl in following form: word/media/xxxx.png
     */
    public static String getBlipUrl(Part part, CTBlip blip) {
        String imageUrl = "";
        if (blip != null) {
            imageUrl = Commons.getUrl(part, blip.getEmbed().length() > 0 ? blip.getEmbed() : blip.getLink());
        }
        return imageUrl;
    }

    public static float fontSizeToPt(Integer sz) {
        if (sz == null) {
            return 10;
        }
        return ((float) sz) / 100f;
    }

    public static Integer ptTofontSize(double pt) {
        return (int) (pt * 100);
    }

    public static int lineWidthToThmm(Integer w) {
        if (w == null) {
            return 1;
        }
        return (int) ((w / 360f) + 0.5);
    }

    public static int convert100thmmToEmu(Number e) {
        return Double.valueOf(e.doubleValue() * 360.0).intValue();
    }

    public static double convertEmuTo100thmm(long e) {
        return new BigDecimal(Double.valueOf(e / 360.0).toString()).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double convertRelToPercent(long e) {
        return new BigDecimal(Double.valueOf(e / 100000.0).toString()).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static int convertPercentToRel(Number e) {
        return Double.valueOf(e.doubleValue() * 100000.0 + 0.5).intValue();
    }

    private static Integer thmmToLineWidth(Long w) {
        return (int) (w * 360);
    }

    public static JSONObject createJsonFromProperties(CTTextBody props) throws JSONException {
        JSONObject character = null;
        if (props != null) {

            final List<Object> paras = props.getContent();
            if (paras != null && paras.size() > 0) {
                final CTTextParagraph para = (CTTextParagraph) paras.get(0);

                final CTTextParagraphProperties prop = para.getPPr(false);
                if (prop != null) {
                    final CTTextCharacterProperties defPr = prop.getDefRPr(false);
                    if (defPr != null) {

                        // color
                        if (defPr.getSolidFill() != null) {
                            character = new JSONObject();
                            final JSONObject color = createJsonColorFromSolidColorFillProperties(defPr.getSolidFill());
                            if (color != null) {
                                character.put(OCKey.COLOR.value(), color);
                            }
                        } else if (defPr.getNoFill() != null) {
                            character = new JSONObject();
                            Logger.getAnonymousLogger()
                                    .warning("CtTextbody has a no fill property, result is set to null");
                        }
                        if (character == null) {
                            character = new JSONObject();
                        }

                        // font-family
                        final TextFont latn = defPr.getLatin();
                        if (latn != null) {
                            final String ff = latn.getTypeface();
                            if (ff != null && !ff.equals("+mn-lt")) {
                                character.put(OCKey.FONT_NAME.value(), ff);
                            }

                        }

                        final float size = fontSizeToPt(defPr.getSz());
                        character.put(OCKey.FONT_SIZE.value(), size);
                        final Boolean bold = defPr.isB();
                        final Boolean italic = defPr.isI();
                        if (bold != null && bold.booleanValue()) {
                            character.put(OCKey.BOLD.value(), true);
                        }
                        if (italic != null && italic.booleanValue()) {
                            character.put(OCKey.ITALIC.value(), true);
                        }
                    }

                }
            }
        }
        return character;
    }

    public static void createJsonFromTextParagraphProperties(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs,
            CTTextParagraphProperties textParagraphProperties) throws JSONException {

        if (textParagraphProperties != null) {
            final JSONObject initialParagraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
            final JSONObject paragraphAttrs = initialParagraphAttrs != null ? initialParagraphAttrs : new JSONObject();

            createJsonFromTextCharacterProperties(operationDocument, attrs, textParagraphProperties.getDefRPr(false));

            final Integer level = textParagraphProperties.getLvl();
            if (level != null) {
                paragraphAttrs.put(OCKey.LEVEL.value(), level.intValue());
            }
            final JSONObject jsonLineHeight = createJsonFromCTTextSpacing(textParagraphProperties.getLnSpc());
            if (jsonLineHeight != null) {
                paragraphAttrs.put(OCKey.LINE_HEIGHT.value(), jsonLineHeight);
            }
            final JSONObject jsonSpacingBefore = createJsonFromCTTextSpacing(textParagraphProperties.getSpcBef());
            if (jsonSpacingBefore != null) {
                paragraphAttrs.put(OCKey.SPACING_BEFORE.value(), jsonSpacingBefore);
            }
            final JSONObject jsonSpacingAfter = createJsonFromCTTextSpacing(textParagraphProperties.getSpcAft());
            if (jsonSpacingAfter != null) {
                paragraphAttrs.put(OCKey.SPACING_AFTER.value(), jsonSpacingAfter);
            }
            final Integer defTabSize = textParagraphProperties.getDefTabSz();
            if (defTabSize != null) {
                paragraphAttrs.put(OCKey.DEFAULT_TAB_SIZE.value(), lineWidthToThmm(defTabSize.intValue()));
            }

            // bullet
            final JSONObject jsonBullet = new JSONObject(2);
            if (textParagraphProperties.getBuNone() != null) {
                jsonBullet.put(OCKey.TYPE.value(), "none");
            } else if (textParagraphProperties.getBuAutoNum() != null) {
                final CTTextAutonumberBullet buAutoNumber = textParagraphProperties.getBuAutoNum();
                jsonBullet.put(OCKey.TYPE.value(), "numbering");
                if (buAutoNumber.getStartAt() != null) {
                    jsonBullet.put(OCKey.START_AT.value(), buAutoNumber.getStartAt());
                }
                if (buAutoNumber.getType() != null) {
                    jsonBullet.put(OCKey.NUM_TYPE.value(), buAutoNumber.getType().value());
                }
            } else if (textParagraphProperties.getBuChar() != null) {
                final CTTextCharBullet buCharacter = textParagraphProperties.getBuChar();
                final String buChar = buCharacter.getChar();
                jsonBullet.put(OCKey.TYPE.value(), "character");
                if (buChar != null && buChar.length() == 1) {
                    jsonBullet.put(OCKey.CHARACTER.value(), buChar);
                }
            } else if (textParagraphProperties.getBuBlip() != null) {
                final CTTextBlipBullet buBlip = textParagraphProperties.getBuBlip();
                jsonBullet.put(OCKey.TYPE.value(), "bitmap");
                jsonBullet.put(OCKey.IMAGE_URL.value(), getBlipUrl(operationDocument!=null ? operationDocument.getContextPart() : null, buBlip.getBlip()));
            }
            if (!jsonBullet.isEmpty()) {
                paragraphAttrs.put(OCKey.BULLET.value(), jsonBullet);
            }

            // bullet size
            if (textParagraphProperties.getBuSzTx() != null) {
                final JSONObject buSize = new JSONObject(1);
                buSize.put(OCKey.TYPE.value(), "followText");
                paragraphAttrs.put(OCKey.BULLET_SIZE.value(), buSize);
            } else if (textParagraphProperties.getBuSzPct() != null) {
                final JSONObject buSize = new JSONObject(2);
                buSize.put(OCKey.TYPE.value(), "percent");
                final Integer size = textParagraphProperties.getBuSzPct().getVal();
                if (size != null) {
                    buSize.put(OCKey.SIZE.value(), (size.intValue() + 500) / 1000);
                }
                paragraphAttrs.put(OCKey.BULLET_SIZE.value(), buSize);
            } else if (textParagraphProperties.getBuSzPts() != null) {
                final JSONObject buSize = new JSONObject(2);
                buSize.put(OCKey.TYPE.value(), "point");
                final Integer size = textParagraphProperties.getBuSzPts().getVal();
                if (size != null) {
                    buSize.put(OCKey.SIZE.value(), (size.intValue() + 500) / 100);
                }
                paragraphAttrs.put(OCKey.BULLET_SIZE.value(), buSize);
            }

            // bullet color
            if (textParagraphProperties.getBuClrTx() != null) {
                final JSONObject bulletColor = new JSONObject(1);
                bulletColor.put(OCKey.FOLLOW_TEXT.value(), true);
                paragraphAttrs.put(OCKey.BULLET_COLOR.value(), bulletColor);
            } else if (textParagraphProperties.getBuClr() != null) {
                final JSONObject bulletColor = new JSONObject(2);
                bulletColor.put(OCKey.FOLLOW_TEXT.value(), false);
                final JSONObject color = createJsonColorFromSolidColorFillProperties(
                        textParagraphProperties.getBuClr());
                if (color != null) {
                    bulletColor.put(OCKey.COLOR.value(), color);
                }
                paragraphAttrs.put(OCKey.BULLET_COLOR.value(), bulletColor);
            }

            // bullet font
            if (textParagraphProperties.getBuFontTx() != null) {
                final JSONObject bulletFont = new JSONObject(1);
                bulletFont.put(OCKey.FOLLOW_TEXT.value(), true);
                paragraphAttrs.put(OCKey.BULLET_FONT.value(), bulletFont);
            } else if (textParagraphProperties.getBuFont() != null) {
                final JSONObject bulletFont = new JSONObject(2);
                bulletFont.put(OCKey.FOLLOW_TEXT.value(), false);
                final String bulletFontName = textParagraphProperties.getBuFont().getTypeface();
                if (bulletFontName != null) {
                    bulletFont.put(OCKey.NAME.value(), bulletFontName);
                }
                paragraphAttrs.put(OCKey.BULLET_FONT.value(), bulletFont);
            }

            final STTextAlignType paragraphAlign = textParagraphProperties.getAlgn();
            if (paragraphAlign != null) {
                String a = null;
                switch (paragraphAlign) {
                case CTR: {
                    a = "center";
                    break;
                }

                case THAI_DIST:
                case DIST:
                case JUST:
                case JUST_LOW: {
                    a = "justify";
                    break;
                }
                case L: {
                    a = "left";
                    break;
                }
                case R: {
                    a = "right";
                    break;
                }
                }
                if (a != null) {
                    paragraphAttrs.put(OCKey.ALIGNMENT.value(), a);
                }
            }
            final Integer marL = textParagraphProperties.getMarL();
            if (marL != null) {
                paragraphAttrs.put(OCKey.INDENT_LEFT.value(), lineWidthToThmm(marL.intValue()));
            }
            final Integer marR = textParagraphProperties.getMarR();
            if (marR != null) {
                paragraphAttrs.put(OCKey.INDENT_RIGHT.value(), lineWidthToThmm(marR.intValue()));
            }
            final Integer indent = textParagraphProperties.getIndent();
            if (indent != null) {
                paragraphAttrs.put(OCKey.INDENT_FIRST_LINE.value(), lineWidthToThmm(indent.intValue()));
            }
            if (initialParagraphAttrs == null && !paragraphAttrs.isEmpty()) {
                attrs.put(OCKey.PARAGRAPH.value(), paragraphAttrs);
            }
        }
    }

    public static void applyTextParagraphPropertiesFromJson(CTTextParagraphProperties paragraphProperties,
            JSONObject attrs, OfficeOpenXMLOperationDocument operationDocument)
            throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if (attrs == null) {
            return;
        }
        final JSONObject paragraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());

        // change of paragraph level has to be applied first
        if (paragraphAttrs != null) {
            if (paragraphAttrs.has(OCKey.LEVEL.value())) {
                final Object level = paragraphAttrs.get(OCKey.LEVEL.value());
                if (level instanceof Integer) {
                    paragraphProperties.setLvl((Integer) level);
                } else if (level == JSONObject.NULL) {
                    paragraphProperties.setLvl(null);
                }
            }
        }
        if (attrs.hasAndNotNull(OCKey.CHARACTER.value())) {
            applyTextCharacterPropertiesFromJson(paragraphProperties.getDefRPr(true), attrs,
                    operationDocument.getContextPart());
        }
        if (paragraphAttrs == null) {
            return;
        }
        final Iterator<Entry<String, Object>> entries = paragraphAttrs.entrySet().iterator();
        while (entries.hasNext()) {
            final Entry<String, Object> entry = entries.next();
            final Object value = entry.getValue();
            switch (OCKey.fromValue(entry.getKey())) {
                case LINE_HEIGHT : {
                    paragraphProperties.setLnSpc(createCTTextSpacingFromJson(value));
                    break;
                }
                case SPACING_BEFORE : {
                    paragraphProperties.setSpcBef(createCTTextSpacingFromJson(value));
                    break;
                }
                case SPACING_AFTER : {
                    paragraphProperties.setSpcAft(createCTTextSpacingFromJson(value));
                    break;
                }
                case DEFAULT_TAB_SIZE : {
                    if (value instanceof Number) {
                        paragraphProperties.setDefTabSz(thmmToLineWidth(((Number) value).longValue()));
                    } else if (value == JSONObject.NULL) {
                        paragraphProperties.setDefTabSz(null);
                    }
                    break;
                }
                case BULLET : {
                    createBulletFromJson(paragraphProperties, value, operationDocument);
                    break;
                }
                case BULLET_SIZE : {
                    createBulletSizeFromJson(paragraphProperties, value);
                    break;
                }
                case BULLET_COLOR : {
                    createBulletColorFromJson(paragraphProperties, value);
                    break;
                }
                case BULLET_FONT : {
                    createBulletFontFromJson(paragraphProperties, value);
                    break;
                }
                case ALIGNMENT : {
                    if (value instanceof String) {
                        if (((String) value).equals("center")) {
                            paragraphProperties.setAlgn(STTextAlignType.CTR);
                        } else if (((String) value).equals("justify")) {
                            paragraphProperties.setAlgn(STTextAlignType.JUST);
                        } else if (((String) value).equals("right")) {
                            paragraphProperties.setAlgn(STTextAlignType.R);
                        } else {
                            paragraphProperties.setAlgn(STTextAlignType.L);
                        }
                    } else if (value == JSONObject.NULL) {
                        paragraphProperties.setAlgn(null);
                    }
                    break;
                }
                case INDENT_LEFT : {
                    if (value instanceof Number) {
                        paragraphProperties.setMarL(Math.max(thmmToLineWidth(((Number) value).longValue()), 0));
                    } else if (value == JSONObject.NULL) {
                        paragraphProperties.setMarL(null);
                    }
                    break;
                }
                case INDENT_RIGHT : {
                    if (value instanceof Number) {
                        paragraphProperties.setMarR(Math.max(thmmToLineWidth(((Number) value).longValue()), 0));
                    } else if (value == JSONObject.NULL) {
                        paragraphProperties.setMarR(null);
                    }
                    break;
                }
                case INDENT_FIRST_LINE : {
                    if (value instanceof Number) {
                        paragraphProperties.setIndent(thmmToLineWidth(((Number) value).longValue()));
                    } else if (value == JSONObject.NULL) {
                        paragraphProperties.setIndent(null);
                    }
                    break;
                }
            }
        }
    }

    // applying the properties to the listStyle
    public static void applyTextParagraphPropertiesFromJson(CTTextBody textBody, int level, JSONObject attrs,
            OfficeOpenXMLOperationDocument operationDocument)
            throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if (attrs == null) {
            return;
        }
        CTTextListStyle textListStyle = textBody.getLstStyle();
        if (textListStyle == null) {
            textListStyle = Context.getDmlObjectFactory().createCTTextListStyle();
            textBody.setLstStyle(textListStyle);
        }
        applyTextParagraphPropertiesFromJson(textListStyle.getPPr(level, true), attrs, operationDocument);
    }

    public static JSONObject createJsonFromCTTextSpacing(CTTextSpacing textSpacing) throws JSONException {

        if (textSpacing == null) {
            return null;
        }

        final JSONObject jsonLineHeight = new JSONObject(2);
        if (textSpacing.getSpcPct() != null) {
            final CTTextSpacingPercent spacing = textSpacing.getSpcPct();
            jsonLineHeight.put(OCKey.TYPE.value(), "percent");
            jsonLineHeight.put(OCKey.VALUE.value(), (int) (spacing.getVal() / 1000.0));
        } else if (textSpacing.getSpcPts() != null) {
            final CTTextSpacingPoint spacing = textSpacing.getSpcPts();
            jsonLineHeight.put(OCKey.TYPE.value(), "fixed");
            jsonLineHeight.put(OCKey.VALUE.value(), (int) ((spacing.getVal() * 25.4) / 72.0));
        }
        return jsonLineHeight;
    }

    public static CTTextSpacing createCTTextSpacingFromJson(Object attrs) {
        if (attrs instanceof JSONObject) {
            final String type = ((JSONObject) attrs).optString(OCKey.TYPE.value(), null);
            final Object value = ((JSONObject) attrs).opt(OCKey.VALUE.value());
            if (type != null && value instanceof Integer) {
                final CTTextSpacing spacing = Context.getDmlObjectFactory().createCTTextSpacing();
                if (type.equals("percent")) {
                    final CTTextSpacingPercent textSpacingPercent = Context.getDmlObjectFactory()
                            .createCTTextSpacingPercent();
                    spacing.setSpcPct(textSpacingPercent);
                    textSpacingPercent.setVal(((Integer) value).intValue() * 1000);
                    return spacing;
                } else if (type.equals("fixed")) {
                    final CTTextSpacingPoint textSpacingFixed = Context.getDmlObjectFactory()
                            .createCTTextSpacingPoint();
                    spacing.setSpcPts(textSpacingFixed);
                    textSpacingFixed.setVal((int) ((((Integer) value).intValue() * 72.0) / 25.4));
                    return spacing;
                }
                return spacing;
            }
        }
        return null;
    }

    public static void createBulletFromJson(CTTextParagraphProperties paragraphProperties, Object bulletAttrs,
            OfficeOpenXMLOperationDocument operationDocument) throws InvalidFormatException, PartUnrecognisedException {

        paragraphProperties.setBuNone(null);
        paragraphProperties.setBuAutoNum(null);
        paragraphProperties.setBuChar(null);
        paragraphProperties.setBuBlip(null);
        if (bulletAttrs instanceof JSONObject) {
            final JSONObject bullet = (JSONObject) bulletAttrs;
            final String buType = bullet.optString(OCKey.TYPE.value(), "");
            if (buType.equals("none")) {
                paragraphProperties.setBuNone(Context.getDmlObjectFactory().createCTTextNoBullet());
            } else if (buType.equals("numbering")) {
                paragraphProperties.setBuAutoNum(Context.getDmlObjectFactory().createCTTextAutonumberBullet());
                final Object startAt = bullet.opt(OCKey.START_AT.value());
                if (startAt instanceof Integer) {
                    paragraphProperties.getBuAutoNum().setStartAt((Integer) startAt);
                }
                final Object numType = bullet.opt(OCKey.NUM_TYPE.value());
                if (numType instanceof String) {
                    paragraphProperties.getBuAutoNum().setType(STTextAutonumberScheme.fromValue((String) numType));
                }
            } else if (buType.equals("character")) {
                paragraphProperties.setBuChar(Context.getDmlObjectFactory().createCTTextCharBullet());
                final String buChar = bullet.optString(OCKey.CHARACTER.value(), "");
                if (buChar.length() == 1) {
                    paragraphProperties.getBuChar().setChar(buChar);
                }
            } else if (buType.equals("bitmap")) {
                paragraphProperties.setBuBlip(Context.getDmlObjectFactory().createCTTextBlipBullet());
                final Object imageUrl = bullet.opt(OCKey.IMAGE_URL.value());
                if (imageUrl instanceof String) {
                    if(!((String)imageUrl).isEmpty()) {
                        paragraphProperties.getBuBlip().setBlip(createCTBlip(operationDocument, operationDocument.getContextPart(), (String)imageUrl));
                    }
                }
            }
        }
    }

    public static void createBulletSizeFromJson(CTTextParagraphProperties paragraphProperties, Object buSize) {
        paragraphProperties.setBuSzTx(null);
        paragraphProperties.setBuSzPct(null);
        paragraphProperties.setBuSzPts(null);
        if (buSize instanceof JSONObject) {
            final String type = ((JSONObject) buSize).optString(OCKey.TYPE.value(), "");
            if (type.equals("followText")) {
                paragraphProperties.setBuSzTx(Context.getDmlObjectFactory().createCTTextBulletSizeFollowText());
            } else if (type.equals("percent")) {
                final Object value = ((JSONObject) buSize).opt(OCKey.SIZE.value());
                if (value instanceof Number) {
                    paragraphProperties.setBuSzPct(Context.getDmlObjectFactory().createCTTextBulletSizePercent());
                    paragraphProperties.getBuSzPct().setVal((int) (((Number) value).doubleValue() * 1000));
                }
            } else if (type.equals("point")) {
                final Object value = ((JSONObject) buSize).opt(OCKey.SIZE.value());
                if (value instanceof Number) {
                    paragraphProperties.setBuSzPts(Context.getDmlObjectFactory().createCTTextBulletSizePoint());
                    paragraphProperties.getBuSzPts().setVal((int) (((Number) value).doubleValue() * 100));
                }
            }
        }
    }

    public static void createBulletColorFromJson(CTTextParagraphProperties paragraphProperties, Object buColor)
            throws JSONException {

        paragraphProperties.setBuClr(null);
        paragraphProperties.setBuClrTx(null);
        if (buColor instanceof JSONObject) {
            final boolean followText = ((JSONObject) buColor).optBoolean(OCKey.FOLLOW_TEXT.value(), false);
            if (followText) {
                paragraphProperties.setBuClrTx(Context.getDmlObjectFactory().createCTTextBulletColorFollowText());
            } else {
                final Object color = ((JSONObject) buColor).opt(OCKey.COLOR.value());
                if (color instanceof JSONObject) {
                    paragraphProperties.setBuClr(Context.getDmlObjectFactory().createCTColor());
                    createSolidColorFillPropertiesFromJson(paragraphProperties.getBuClr(), (JSONObject) color);
                }
            }
        }
    }

    public static void createBulletFontFromJson(CTTextParagraphProperties paragraphProperties, Object buFont) {

        paragraphProperties.setBuFont(null);
        paragraphProperties.setBuFontTx(null);
        if (buFont instanceof JSONObject) {
            final boolean followText = ((JSONObject) buFont).optBoolean(OCKey.FOLLOW_TEXT.value(), false);
            if (followText) {
                paragraphProperties.setBuFontTx(Context.getDmlObjectFactory().createCTTextBulletTypefaceFollowText());
            } else {
                final String name = ((JSONObject) buFont).optString(OCKey.NAME.value(), "");
                paragraphProperties.setBuFont(Context.getDmlObjectFactory().createTextFont());
                paragraphProperties.getBuFont().setTypeface(name);
            }
        }
    }

    public static void createJsonFromTextCharacterProperties(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs,
            CTTextCharacterProperties textCharacterProperties) throws JSONException {

        if (textCharacterProperties != null) {
            final JSONObject initialCharacterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
            final JSONObject characterAttrs = initialCharacterAttrs != null ? initialCharacterAttrs : new JSONObject();

            final String lang = textCharacterProperties.getLang();
            if (lang != null) {
                characterAttrs.put(OCKey.LANGUAGE.value(), lang);
                if(operationDocument!=null) {
                    operationDocument.getUsedLanguages().add(lang);
                }
            }
            final Integer fontSize = textCharacterProperties.getSz();
            if (fontSize != null) {
                characterAttrs.put(OCKey.FONT_SIZE.value(), new BigDecimal(fontSize.floatValue() / 100.0f)
                    .setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
            }
            final TextFont latinFont = textCharacterProperties.getLatin();
            if (latinFont != null && latinFont.getTypeface() != null) {
                characterAttrs.put(OCKey.FONT_NAME.value(), latinFont.getTypeface());
            }
            final TextFont eastAsiaFont = textCharacterProperties.getEa();
            if (eastAsiaFont != null) {
                characterAttrs.put(OCKey.FONT_NAME_EAST_ASIA.value(), eastAsiaFont.getTypeface());
            }
            final TextFont complexFont = textCharacterProperties.getCs();
            if (complexFont != null) {
                characterAttrs.put(OCKey.FONT_NAME_COMPLEX.value(), complexFont.getTypeface());
            }
            final TextFont symFont = textCharacterProperties.getSym();
            if (symFont != null) {
                characterAttrs.put(OCKey.FONT_NAME_SYMBOL.value(), symFont.getTypeface());
            }
            if (textCharacterProperties.getNoFill() != null) {
                // TODO: we need a fill object instead of a plain color
                // createJsonFromFillStyle(textCharacterProperties.getNoFill(),
                // null, characterAttrs, part);
            } else if (textCharacterProperties.getSolidFill() != null) {
                final JSONObject color = createJsonColorFromSolidColorFillProperties(
                        textCharacterProperties.getSolidFill());
                if (color != null) {
                    characterAttrs.put(OCKey.COLOR.value(), color);
                }
            } else if (textCharacterProperties.getBlipFill() != null) {
                // TODO:
                // createJsonFromFillStyle(textCharacterProperties.getBlipFill(),
                // null, characterAttrs, part);
            } else if (textCharacterProperties.getGradFill() != null) {
                // TODO:
                // createJsonFromFillStyle(textCharacterProperties.getGradFill(),
                // null, characterAttrs, part);
            } else if (textCharacterProperties.getPattFill() != null) {
                // TODO:
                // createJsonFromFillStyle(textCharacterProperties.getPattFill(),
                // null, characterAttrs, part);
            }
            final Boolean bold = textCharacterProperties.isB();
            if (bold != null) {
                characterAttrs.put(OCKey.BOLD.value(), bold.booleanValue());
            }
            final STTextCapsType capsType = textCharacterProperties.getCap();
            if (capsType != null) {
                characterAttrs.put(OCKey.CAPS.value(), capsType.value());
            }
            final Boolean italic = textCharacterProperties.isI();
            if (italic != null) {
                characterAttrs.put(OCKey.ITALIC.value(), italic.booleanValue());
            }
            final STTextUnderlineType underline = textCharacterProperties.getU();
            if (underline != null) {
                switch (underline) {
                case DASH:
                case DASH_HEAVY:
                case DASH_LONG:
                case DASH_LONG_HEAVY:
                case DBL:
                case DOTTED:
                case DOTTED_HEAVY:
                case DOT_DASH:
                case DOT_DASH_HEAVY:
                case DOT_DOT_DASH:
                case DOT_DOT_DASH_HEAVY:
                case HEAVY:
                case SNG:
                case WAVY:
                case WAVY_DBL:
                case WAVY_HEAVY:
                case WORDS:
                    characterAttrs.put(OCKey.UNDERLINE.value(), true);
                    break;
                default:
                case NONE:
                    characterAttrs.put(OCKey.UNDERLINE.value(), false);
                    break;
                }
            }
            final STTextStrikeType strike = textCharacterProperties.getStrike();
            if (strike != null) {
                switch (strike) {
                case NO_STRIKE:
                    characterAttrs.put(OCKey.STRIKE.value(), "none");
                    break;
                case DBL_STRIKE:
                    characterAttrs.put(OCKey.STRIKE.value(), "double");
                    break;
                case SNG_STRIKE:
                    characterAttrs.put(OCKey.STRIKE.value(), "single");
                    break;
                }
            }
            final Integer baseline = textCharacterProperties.getBaseline();
            if (baseline != null) {
                characterAttrs.put(OCKey.BASELINE.value(), baseline / 1000);
            }
            final CTHyperlink hyperlink = textCharacterProperties.getHlinkClick(false);
            if(operationDocument!=null && hyperlink!=null) {
                final Part part = operationDocument.getContextPart();
                if(part!=null) {
                    final String rId = hyperlink.getId();
                    if (rId != null && !rId.isEmpty()) {
                        final String hLink = Commons.getUrl(part, rId);
                        if (hLink != null && !hLink.isEmpty()) {
                            characterAttrs.put(OCKey.URL.value(), hLink);
                        }
                    }
                }
            }
            if (initialCharacterAttrs == null && !characterAttrs.isEmpty()) {
                attrs.put(OCKey.CHARACTER.value(), characterAttrs);
            }
        }
    }

    public static void applyTextCharacterPropertiesFromJson(CTTextCharacterProperties characterProperties,
            JSONObject attrs, Part part) throws JSONException {

        if (attrs == null) {
            return;
        }
        final JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
        if (characterAttrs == null) {
            return;
        }
        final Iterator<Entry<String, Object>> entries = characterAttrs.entrySet().iterator();
        while (entries.hasNext()) {
            final Entry<String, Object> entry = entries.next();
            final Object value = entry.getValue();
            switch (OCKey.fromValue(entry.getKey())) {
                case COLOR : {
                    applyFillPropertiesFromJsonColor(characterProperties, value);
                    break;
                }
                case FONT_SIZE : {
                    if (value instanceof Number) {
                        characterProperties.setSz(new Float(((Number) value).floatValue() * 100.0f).intValue());
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setSz(null);
                    }
                    break;
                }
                case FONT_NAME : {
                    if (value instanceof String) {
                        characterProperties.setLatin(Context.getDmlObjectFactory().createTextFont());
                        characterProperties.getLatin().setTypeface((String) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setLatin(null);
                    }
                    break;
                }
                case FONT_NAME_EAST_ASIA : {
                    if (value instanceof String) {
                        characterProperties.setEa(Context.getDmlObjectFactory().createTextFont());
                        characterProperties.getEa().setTypeface((String) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setEa(null);
                    }
                    break;
                }
                case FONT_NAME_COMPLEX : {
                    if (value instanceof String) {
                        characterProperties.setCs(Context.getDmlObjectFactory().createTextFont());
                        characterProperties.getCs().setTypeface((String) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setCs(null);
                    }
                    break;
                }
                case FONT_NAME_SYMBOL : {
                    if (value instanceof String) {
                        characterProperties.setSym(Context.getDmlObjectFactory().createTextFont());
                        characterProperties.getSym().setTypeface((String) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setSym(null);
                    }
                    break;
                }
                case BOLD : {
                    if (value instanceof Boolean) {
                        characterProperties.setB((Boolean) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setB(null);
                    }
                    break;
                }
                case ITALIC : {
                    if (value instanceof Boolean) {
                        characterProperties.setI((Boolean) value);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setI(null);
                    }
                    break;
                }
                case UNDERLINE : {
                    if (value instanceof Boolean) {
                        characterProperties.setU(
                                ((Boolean) value).booleanValue() ? STTextUnderlineType.SNG : STTextUnderlineType.NONE);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setU(null);
                    }
                    break;
                }
                case CAPS : {
                    if (value instanceof String) {
                        characterProperties.setCap(STTextCapsType.fromValue((String) value));
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setCap(null);
                    }
                    break;
                }
                case STRIKE : {
                    if (value instanceof String) {
                        if (((String) value).equals("none")) {
                            characterProperties.setStrike(STTextStrikeType.NO_STRIKE);
                        } else if (((String) value).equals("single")) {
                            characterProperties.setStrike(STTextStrikeType.SNG_STRIKE);
                        } else if (((String) value).equals("double")) {
                            characterProperties.setStrike(STTextStrikeType.DBL_STRIKE);
                        }
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setStrike(null);
                    }
                    break;
                }
                case BASELINE : {
                    if (value instanceof Number) {
                        characterProperties.setBaseline(((Number) value).intValue() * 1000);
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setBaseline(null);
                    }
                    break;
                }
                case URL : {
                    if (value instanceof String) {
                        if (part != null) {
                            final CTHyperlink hyperlink = characterProperties.getHlinkClick(true);
                            hyperlink.setId(Commons.setUrl(part, null, (String) value));
                        }
                    } else if (value == JSONObject.NULL) {
                        characterProperties.setHlinkClick(null);
                    }
                    break;
                }
                case LANGUAGE : {
                    if (value instanceof String) {
                        characterProperties.setLang((String) value);
                    }
                    break;
                }
            }
        }
    }

    public static void applyTextListStyleFromJson(CTTextBody textBody, JSONObject listStyle,
            OfficeOpenXMLOperationDocument operationDocument)
            throws JSONException, InvalidFormatException, PartUnrecognisedException {

        applyTextParagraphPropertiesFromJson(textBody, 0, listStyle.optJSONObject(OCKey.L1.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 1, listStyle.optJSONObject(OCKey.L2.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 2, listStyle.optJSONObject(OCKey.L3.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 3, listStyle.optJSONObject(OCKey.L4.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 4, listStyle.optJSONObject(OCKey.L5.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 5, listStyle.optJSONObject(OCKey.L6.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 6, listStyle.optJSONObject(OCKey.L7.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 7, listStyle.optJSONObject(OCKey.L8.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 8, listStyle.optJSONObject(OCKey.L9.value()), operationDocument);
        applyTextParagraphPropertiesFromJson(textBody, 9, listStyle.optJSONObject(OCKey.DEFAULT.value()), operationDocument);
    }

    // applying the properties to the listStyle
    public static void applyTextCharacterPropertiesFromJson(CTTextBody textBody, int level, JSONObject attrs, Part part)
            throws JSONException {

        if (attrs == null || attrs.optJSONObject(OCKey.CHARACTER.value()) == null) {
            return;
        }
        CTTextListStyle textListStyle = textBody.getLstStyle();
        if (textListStyle == null) {
            textListStyle = Context.getDmlObjectFactory().createCTTextListStyle();
            textBody.setLstStyle(textListStyle);
        }
        applyTextCharacterPropertiesFromJson(textListStyle.getPPr(level, true).getDefRPr(true), attrs, part);
    }

    public static JSONObject createJsonFromTextListStyle(OfficeOpenXMLOperationDocument operationDocument, CTTextListStyle textListStyle)
            throws JSONException {

        if (textListStyle == null) {
            return null;
        }
        final JSONObject style = new JSONObject();
        createTextParagraphProperties(operationDocument, textListStyle.getDefPPr(), style, OCKey.DEFAULT.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl1PPr(), style, OCKey.L1.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl2PPr(), style, OCKey.L2.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl3PPr(), style, OCKey.L3.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl4PPr(), style, OCKey.L4.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl5PPr(), style, OCKey.L5.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl6PPr(), style, OCKey.L6.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl7PPr(), style, OCKey.L7.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl8PPr(), style, OCKey.L8.value());
        createTextParagraphProperties(operationDocument, textListStyle.getLvl9PPr(), style, OCKey.L9.value());
        return style;
    }

    private static JSONObject createTextParagraphProperties(OfficeOpenXMLOperationDocument operationDocument, CTTextParagraphProperties textParagraphProperties,
            JSONObject destStyle, String level) throws JSONException {

        if (textParagraphProperties == null) {
            return null;
        }

        final JSONObject properties = new JSONObject(2);
        DMLHelper.createJsonFromTextParagraphProperties(operationDocument, properties, textParagraphProperties);
        if (!properties.isEmpty()) {
            destStyle.put(level, properties);
        }
        return null;
    }

    public static JSONObject createJsonFromTextBodyProperties(JSONObject attrs, CTTextBodyProperties textBodyProperties)
            throws JSONException {

        if (textBodyProperties != null) {
            final JSONObject initialTextBodyAttrs = attrs.optJSONObject(OCKey.SHAPE.value());
            final JSONObject textBodyAttrs = initialTextBodyAttrs != null ? initialTextBodyAttrs : new JSONObject();

            // top is default
            final STTextAnchoringType anchor = textBodyProperties.getAnchor();
            if (anchor != null) {
                String a = null;
                switch (anchor) {
                case B: {
                    a = "bottom";
                    break;
                }
                case CTR: {
                    a = "centered";
                    break;
                }
                case DIST: {
                    a = "distributed";
                    break;
                }
                case JUST: {
                    a = "justified";
                    break;
                }
                case T: {
                    a = "top";
                    break;
                }
                }
                if (a != null) {
                    textBodyAttrs.put(OCKey.ANCHOR.value(), a);
                }
            }

            // false is default
            final Boolean anchorCentered = textBodyProperties.isAnchorCtr();
            if (anchorCentered != null) {
                textBodyAttrs.put(OCKey.ANCHOR_CENTERED.value(), anchorCentered.booleanValue());
            }

            // default 2,5mm
            final Integer marginLeft = textBodyProperties.getLIns();
            if (marginLeft != null) {
                textBodyAttrs.put(OCKey.PADDING_LEFT.value(), marginLeft.intValue() / 360);
            }

            // default 2,5mm
            final Integer marginRight = textBodyProperties.getRIns();
            if (marginRight != null) {
                textBodyAttrs.put(OCKey.PADDING_RIGHT.value(), marginRight.intValue() / 360);
            }

            // default 1,25mm
            final Integer marginTop = textBodyProperties.getTIns();
            if (marginTop != null) {
                textBodyAttrs.put(OCKey.PADDING_TOP.value(), marginTop.intValue() / 360);
            }

            // default 1,25mm
            final Integer marginBottom = textBodyProperties.getBIns();
            if (marginBottom != null) {
                textBodyAttrs.put(OCKey.PADDING_BOTTOM.value(), marginBottom.intValue() / 360);
            }

            // default is square
            final STTextWrappingType wrap = textBodyProperties.getWrap();
            if (wrap != null) {
                textBodyAttrs.put(OCKey.WORD_WRAP.value(), wrap == STTextWrappingType.NONE ? false : true);
            }

            // default is overflow
            final STTextHorzOverflowType horzOverflow = textBodyProperties.getHorzOverflow();
            if (horzOverflow != null) {
                textBodyAttrs.put(OCKey.HORZ_OVERFLOW.value(), horzOverflow == STTextHorzOverflowType.CLIP ? "clip" : "overflow");
            }

            if (textBodyProperties.getSpAutoFit()!=null) {
                textBodyAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), true);
            }
            else if(textBodyProperties.getNoAutofit()!=null) {
                textBodyAttrs.put(OCKey.NO_AUTO_RESIZE.value(), true);
            }
            else if(textBodyProperties.getNormAutofit()!=null) {
                final CTTextNormalAutofit normAutofit = textBodyProperties.getNormAutofit();
                textBodyAttrs.put(OCKey.AUTO_RESIZE_TEXT.value(), true);
                textBodyAttrs.put(OCKey.FONT_SCALE.value(), normAutofit.getFontScale() / 100000.0);
                textBodyAttrs.put(OCKey.LINE_REDUCTION.value(), normAutofit.getLnSpcReduction() / 100000.0);
            }

            final STTextVerticalType textVerticalType = textBodyProperties.getVert();
            if (textVerticalType != null) {
                textBodyAttrs.put(OCKey.VERT.value(), textVerticalType.value());
            }
            // default is overflow
            final STTextVertOverflowType vertOverflow = textBodyProperties.getVertOverflow();
            if (vertOverflow != null) {
                String o = null;
                switch (vertOverflow) {
                case CLIP: {
                    o = "clip";
                    break;
                }
                case ELLIPSIS: {
                    o = "ellipsis";
                    break;
                }
                case OVERFLOW: {
                    o = "overflow";
                    break;
                }
                }
                if (o != null) {
                    textBodyAttrs.put(OCKey.VERT_OVERFLOW.value(), o);
                }
            }
            if (initialTextBodyAttrs == null && !textBodyAttrs.isEmpty()) {
                attrs.put(OCKey.SHAPE.value(), textBodyAttrs);
            }
        }
        return attrs;
    }

    public static void applyTextBodyPropertiesFromJson(CTTextBodyProperties bodyPr, JSONObject attrs) {

        if (bodyPr == null) {
            return;
        }
        final JSONObject shapeAttrs = attrs.optJSONObject(OCKey.SHAPE.value());
        if (shapeAttrs == null) {
            return;
        }
        final Iterator<Entry<String, Object>> iter = shapeAttrs.entrySet().iterator();
        while (iter.hasNext()) {
            final Entry<String, Object> entry = iter.next();
            final Object value = entry.getValue();
            switch (OCKey.fromValue(entry.getKey())) {
                case PADDING_LEFT : {
                    if (value instanceof Number) {
                        bodyPr.setLIns(Integer.valueOf(((Number) value).intValue() * 360));
                    } else {
                        bodyPr.setLIns(null);
                    }
                    break;
                }
                case PADDING_RIGHT : {
                    if (value instanceof Number) {
                        bodyPr.setRIns(Integer.valueOf(((Number) value).intValue() * 360));
                    } else {
                        bodyPr.setRIns(null);
                    }
                    break;
                }
                case PADDING_TOP : {
                    if (value instanceof Number) {
                        bodyPr.setTIns(Integer.valueOf(((Number) value).intValue() * 360));
                    } else {
                        bodyPr.setTIns(null);
                    }
                    break;
                }
                case PADDING_BOTTOM : {
                    if (value instanceof Number) {
                        bodyPr.setBIns(Integer.valueOf(((Number) value).intValue() * 360));
                    } else {
                        bodyPr.setBIns(null);
                    }
                    break;
                }
                case AUTO_RESIZE_HEIGHT : {
                    if (value instanceof Boolean && ((Boolean) value).booleanValue()) {
                        bodyPr.setSpAutoFit(Context.getDmlObjectFactory().createCTTextShapeAutofit());
                    } else {
                        bodyPr.setSpAutoFit(null);
                    }
                    break;
                }
                case AUTO_RESIZE_TEXT : {
                    if (value instanceof Boolean && ((Boolean) value).booleanValue()) {
                        if (null == bodyPr.getNormAutofit()) {
                            bodyPr.setNormAutofit(Context.getDmlObjectFactory().createCTTextNormalAutofit());
                        }
                    } else {
                        bodyPr.setNormAutofit(null);
                    }
                    break;
                }
                case NO_AUTO_RESIZE : {
                    if (value instanceof Boolean && ((Boolean) value).booleanValue()) {
                        bodyPr.setNoAutofit(Context.getDmlObjectFactory().createCTTextNoAutofit());
                    } else {
                        bodyPr.setNoAutofit(null);
                    }
                    break;
                }
                case FONT_SCALE : {
                    CTTextNormalAutofit fit = bodyPr.getNormAutofit();
                    if (null == fit) {
                        fit = Context.getDmlObjectFactory().createCTTextNormalAutofit();
                        bodyPr.setNormAutofit(fit);
                    }
                    if (value instanceof Number) {
                        fit.setFontScale((int) (((Number) value).doubleValue() * 100000.0));
                    } else {
                        fit.setFontScale(null);
                    }
                    break;
                }
                case LINE_REDUCTION : {
                    CTTextNormalAutofit fit = bodyPr.getNormAutofit();
                    if (null == fit) {
                        fit = Context.getDmlObjectFactory().createCTTextNormalAutofit();
                        bodyPr.setNormAutofit(fit);
                    }
                    if (value instanceof Number) {
                        fit.setLnSpcReduction((int) (((Number) value).doubleValue() * 100000.0));
                    } else {
                        fit.setLnSpcReduction(null);
                    }
                    break;
                }
                case ANCHOR : {
                    STTextAnchoringType anchor = null;
                    if (value instanceof String) {
                        switch ((String) value) {
                        case "top": {
                            anchor = STTextAnchoringType.T;
                            break;
                        }
                        case "centered": {
                            anchor = STTextAnchoringType.CTR;
                            break;
                        }
                        case "distributed": {
                            anchor = STTextAnchoringType.DIST;
                            break;
                        }
                        case "justified": {
                            anchor = STTextAnchoringType.JUST;
                            break;
                        }
                        case "bottom": {
                            anchor = STTextAnchoringType.B;
                            break;
                        }
                        }
                    }
                    bodyPr.setAnchor(anchor);
                    break;
                }
                case ANCHOR_CENTERED : {
                    Boolean anchorCentered = null;
                    if (value instanceof Boolean) {
                        anchorCentered = (Boolean) value;
                    }
                    bodyPr.setAnchorCtr(anchorCentered);
                    break;
                }
                case WORD_WRAP : {
                    STTextWrappingType wordWrap = null;
                    if (value instanceof Boolean) {
                        wordWrap = ((Boolean) value).booleanValue() ? STTextWrappingType.SQUARE : STTextWrappingType.NONE;
                    }
                    bodyPr.setWrap(wordWrap);
                    break;
                }
                case VERT : {
                    STTextVerticalType textVerticalType = null;
                    if (value instanceof String) {
                        textVerticalType = STTextVerticalType.fromValue((String) value);
                    }
                    bodyPr.setVert(textVerticalType);
                    break;
                }
                case HORZ_OVERFLOW : {
                    STTextHorzOverflowType horzOverflowType = null;
                    if (value instanceof String) {
                        if (((String) value).equals("clip")) {
                            horzOverflowType = STTextHorzOverflowType.CLIP;
                        } else if (((String) value).equals("overflow")) {
                            horzOverflowType = STTextHorzOverflowType.OVERFLOW;
                        }
                    }
                    bodyPr.setHorzOverflow(horzOverflowType);
                    break;
                }
                case VERT_OVERFLOW : {
                    STTextVertOverflowType vertOverflowType = null;
                    if (value instanceof String) {
                        if (((String) value).equals("clip")) {
                            vertOverflowType = STTextVertOverflowType.CLIP;
                        } else if (((String) value).equals("overflow")) {
                            vertOverflowType = STTextVertOverflowType.OVERFLOW;
                        } else if (((String) value).equals("ellipsis")) {
                            vertOverflowType = STTextVertOverflowType.ELLIPSIS;
                        }
                    }
                    bodyPr.setVertOverflow(vertOverflowType);
                    break;
                }
            }
        }
    }

    /*
     * charts and diagrams are nowadays saved without alternate replacement
     * graphics. we use the DC with a shape2png conversion for these cases.
     */
    static final List<String> graphicUris = new ArrayList<>(Arrays.asList(
        "http://schemas.openxmlformats.org/drawingml/2006/chart",
        "http://schemas.openxmlformats.org/drawingml/2006/diagram"
    ));

    static public String getReplacementUrlFromGraphicFrame(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, IGraphicalObjectFrame graphicalObjectFrame, CTTransform2D xForm, int slideNumber, int shapeNumber, String inputType) {
        final Graphic graphic = graphicalObjectFrame.getGraphic();
        if(graphic != null && xForm != null) {
            final GraphicData graphicData = graphic.getGraphicData();
            if(graphicData != null && graphicData.hasUri(graphicUris)) {
                try {
                    return getReplacementUrl(getHashFromGraphicFrameRelations(parentContext, graphicalObjectFrame), slideNumber, shapeNumber, inputType);
                } catch (URISyntaxException | Docx4JException | IOException e) {
                    //
                }
            }
        }
        return null;
    }

    static public String getReplacementUrl(String hash, int slideNumber, int shapeNumber, String inputType) {
        String url = null;

       if(hash != null && !hash.isEmpty()) {
           url = "replacement"
               // .h hash parameter e.g. 8069F5DC-76BB-0395-EFB3-78555DB6F778-11109325-4298950
               + ".h" + hash
               // .p page index
               + ".p" + Integer.valueOf(slideNumber).toString()
               // shape index
               + ".s" + Integer.valueOf(shapeNumber).toString()
               // input type
               + ".i" + inputType
               + ".shape2png";
       }
       return url;
    }

    /*
     * Is returning the hash of embedded SmartArt and Chart objects, in case of an error null is returned
     */
    public static String getHashFromGraphicFrameRelations(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, IGraphicalObjectFrame graphicFrame) throws URISyntaxException, Docx4JException, IOException {

        long[] hash = null;

        final OfficeOpenXMLOperationDocument operationDocument = parentContext.getOperationDocument();
        final Part contextPart = operationDocument.getContextPart();
        final RelationshipsPart rels = contextPart.getRelationshipsPart(false);
        if(rels != null) {
            final Set<String> targets = new HashSet<String>();
            final String[] absoluteSourcePaths = getTargetsFromGraphicFrame(contextPart, rels, graphicFrame);
            for(String absoluteSourcePath : absoluteSourcePaths) {
                addTargetRelations(contextPart.getPackage().getSourcePartStore(), absoluteSourcePath, targets);
            }
            if(!targets.isEmpty()) {
                final ITransform2D xForm = ((ITransform2DAccessor)graphicFrame).getXfrm(false);
                if(xForm != null) {
                    final CTPositiveSize2D ext = xForm.getExt(false);
                    if(ext != null) {
                        hash = new long[2];
                        hash[ 0 ] = ext.getCx();
                        hash[ 1 ] = ext.getCy();
                    }
                }
                final Iterator<String> targetIter = targets.iterator();
                while(targetIter.hasNext()) {
                    final String targetPath = targetIter.next();
                    final PartStore partStore = contextPart.getPackage().getSourcePartStore();
                    try(InputStream rawPart = getRawBytes(partStore, targetPath, operationDocument)) {
                        if(rawPart == null) {
                            Logger.getAnonymousLogger().warning("ooxml: could not get hash for graphic frame replacement");
                            return null;
                        }
                        hash = getHash128FromByteArray(rawPart.readAllBytes(), hash);
                    }
                }
            }
        }
        return hash != null ? getStringFromHash128(hash) : null;
    }

    /*
     * Is returning the hash of the given shape
     */
    public static String getHashFromShape(JAXBContext jc, OpcPackage opcPackage, IShape shape) {
        final byte[] bytes = XmlUtils.marshalToByteArray(opcPackage, shape, true, jc);
        return (bytes != null && bytes.length > 0) ? getStringFromHash128(MurmurHash3.hash128(bytes)) : null;
    }

    private static long[] getHash128FromByteArray(byte[] buffer, long[] oldHash) {
        final long[] hash = MurmurHash3.hash128(buffer);
        if(oldHash != null) {
            hash[0] ^= oldHash[0];
            hash[1] ^= oldHash[1];
        }
        return hash;
    }

    private static String getStringFromHash128(long[] l) {
        final StringBuilder b = new StringBuilder(32);
        Arrays.stream(l).forEach(v -> b.append(Long.toHexString(v)));
        while(b.length() < 32) {
            b.append('0');
        }
        return b.toString();
    }

    private static InputStream getRawBytes(PartStore partStore, String targetPath, OfficeOpenXMLOperationDocument operationDocument) throws Docx4JException, IOException {
        InputStream is = partStore.loadPart(targetPath.substring(1));
        if(is == null) { // media files are not loaded when creating operations (read only)
            is = getRawBytesFromZip(operationDocument.getDebugDocument(), targetPath.substring(1));
        }
        return is;
    }

    private static InputStream getRawBytesFromZip(InputStream zip, String targetPath) throws IOException {
        try (ZipInputStream zipInputStream = new ZipInputStream(zip)) {
            ZipEntry zipEntry = null;
            while((zipEntry = zipInputStream.getNextEntry()) != null) {
                final String zipEntryName = zipEntry.getName();
                if(zipEntryName.equals(targetPath)) {
                    return new ByteArrayInputStream(zipInputStream.readAllBytes());
                }
                zipInputStream.closeEntry();
            }
        }
        return null;
    }

    /*
     * returns an array of absolute source paths that relates to this graphic frame
     */
    private static String[] getTargetsFromGraphicFrame(Part contextPart, RelationshipsPart rels, IGraphicAccess graphicAccess) throws URISyntaxException {
        final List<String> list = new ArrayList<String>();
        final Graphic graphic = graphicAccess.getGraphic();
        if(graphic != null) {
            final GraphicData graphicData = graphic.getGraphicData();
            if(graphicData != null) {
                final List<Object> anyList = graphicData.getAny();
                final Iterator<Object> iter = anyList.iterator();
                while(iter.hasNext()) {
                    final Object o = iter.next();
                    if(o instanceof JAXBElement<?>&&((JAXBElement<?>) o).getDeclaredType().getName().equals("org.docx4j.dml.chart.CTRelId")) {
                        final Object value = ((JAXBElement<?>)o ).getValue();
                        if(value instanceof org.docx4j.dml.chart.CTRelId) {
                            addTargetPath(contextPart, rels, ((org.docx4j.dml.chart.CTRelId)value).getId(), list);
                        }
                        break;
                    }
                    else if(o instanceof JAXBElement<?>&&((JAXBElement<?>) o).getDeclaredType().getName().equals("org.docx4j.dml.diagram.CTRelIds")) {
                        final Object value = ((JAXBElement<?>)o ).getValue();
                        if(value instanceof org.docx4j.dml.diagram.CTRelIds) {
                            final org.docx4j.dml.diagram.CTRelIds relIds = (org.docx4j.dml.diagram.CTRelIds)value;
                            addTargetPath(contextPart, rels, relIds.getCs(), list);
                            addTargetPath(contextPart, rels, relIds.getDm(), list);
                            addTargetPath(contextPart, rels, relIds.getLo(), list);
                            addTargetPath(contextPart, rels, relIds.getQs(), list);
                        }
                        break;
                    }
                }
            }
        }
        return list.toArray(new String[list.size()]);
    }

    /*
     * The absolute target path from sourcePart and relId is created and added to the targets list
     */
    private static void addTargetPath(Part contextPart, RelationshipsPart rels, String relId, List<String> targets) throws URISyntaxException {
        final Relationship rel = rels.getRelationshipByID(relId);
        if(rel.getTargetMode() == null || rel.getTargetMode().equalsIgnoreCase("Internal")) {
            targets.add(URIHelper.resolvePartUri(contextPart.getPartName().getURI(), new URI(rel.getTarget())).getPath());
        }
    }

    /*
     * The targets set is filled with absolute paths (absoluteSourcePath and all its relations)
     */
    private static void addTargetRelations(PartStore partStore, String absoluteSourcePath, Set<String> targets) throws IOException, Docx4JException, URISyntaxException {
        if(!targets.contains(absoluteSourcePath)) {
            targets.add(absoluteSourcePath);

            // try to get a RelationshipsPart for this target
            final String relPartName = PartName.getRelationshipsPartName(absoluteSourcePath.substring(1));
            try(InputStream is = partStore.loadPart(relPartName)) {
                if (is!=null) {
                    final RelationshipsPart rp = new RelationshipsPart(new PartName("/" + relPartName));
                    final BinaryPart sourcePart = new BinaryPart(new PartName(absoluteSourcePath));
                    rp.setSourceP(sourcePart);
                    rp.unmarshal(is);
                    for(Relationship rel : rp.getRelationships().getRelationship()) {
                        if(rel.getTargetMode() == null || rel.getTargetMode().equalsIgnoreCase("Internal")) {
                            addTargetRelations(partStore, URIHelper.resolvePartUri(sourcePart.getPartName().getURI(), new URI(rel.getTarget())).getPath(), targets);
                        }
                    }
                }
            }
        }
    }
}
