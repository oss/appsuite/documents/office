/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.tools;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.xml.bind.DatatypeConverter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.docx4j.dml.CTColor;
import org.docx4j.openpackaging.URIHelper;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.io3.stores.PartStore;
import org.docx4j.openpackaging.io3.stores.ZipPartStore;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.relationships.Relationships;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.tools.common.files.FileHelper;

final public class Commons {

	final private static Logger log = LoggerFactory.getLogger(Commons.class);

    final static String hexArray = "0123456789ABCDEF";

    final static String fillString = "000000";

    public static String bytesToHexString(byte[] bytes, int offset, int size) {
        final StringBuilder hex = new StringBuilder(2 * size);
        for (int i=0; i<size; i++) {
            final byte b = bytes[offset + i];
            hex.append(hexArray.charAt((b & 0xF0) >> 4))
               .append(hexArray.charAt(b&0x0F));
        }
        return hex.toString();
    }

    public static String ctColorToString( CTColor color ){
        byte[] rgbVal = color.getSrgbClr() != null ? color.getSrgbClr().getVal() : color.getSysClr().getLastClr();
        String ret = Integer.toHexString((((rgbVal[0]& 0xFF)<< 16) + ((rgbVal[1]& 0xFF)<< 8) + (rgbVal[2]& 0xFF)));
        if( ret.length() < 6 )
            ret = fillString.substring( ret.length() ) + ret;
        return ret;
    }

    public static String intToHexString(int color) {
        String ret = Integer.toHexString(color);
        if(ret.length() < 6)
            ret = fillString.substring( ret.length() ) + ret;
        return ret;
    }

    public static byte[] ctColorToBytes(CTColor color){
        return color.getSrgbClr() != null ? color.getSrgbClr().getVal() : color.getSysClr().getLastClr();
    }

    public static byte[] hexStringToBytes(String color) {
        return DatatypeConverter.parseHexBinary(color);
    }

    public static JSONObject surroundJSONObject(String type, JSONObject source) {
        if(source==null||source.length()==0)
            return null;
        JSONObject newJSON = new JSONObject();
        try {
            newJSON.put(type,  source);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return newJSON;
    }

    public static void mergeJsonObjectIfUsed(String destinationPropertyName, JSONObject destination, JSONObject source)
        throws JSONException {

        if(source!=null&&source.length()>0) {
            if(destination.has(destinationPropertyName)) {
                Iterator<String> keys = source.keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    Object val = source.get(key);
                    destination.put(key, val);
                }
           } else
                destination.put(destinationPropertyName, source);
        }
    }

    public static void jsonPut(JSONObject destination, String destinationPropertyName, JSONObject source)
        throws JSONException {
        if(source!=null&&source.length()>0) {
            destination.put(destinationPropertyName, source);
        }
    }

    public static void jsonPut(JSONObject destination, String destinationPropertyName, JSONArray source)
        throws JSONException {
        if(source!=null&&source.length()>0) {
            destination.put(destinationPropertyName, source);
        }
    }

    public static void jsonPut(JSONObject destination, String destinationPropertyName, String source)
        throws JSONException {
        if(source!=null&&source.length()>0) {
            destination.put(destinationPropertyName, source);
        }
    }

    public static void jsonPut(JSONObject destination, String destinationPropertyName, Object source)
        throws JSONException {
        if(source!=null) {
            destination.put(destinationPropertyName, source);
        }
    }

    public static void jsonPut(JSONArray destination, String source) {
        if(source!=null&&source.length()>0) {
            destination.put(source);
        }
    }

    public static final Map<String, String> highlightColorToRgb = createHighlightColorsMap();

    private static Map<String, String> createHighlightColorsMap() {
        // highlight color map according to ooxml specification
        Map<String, String> result = new HashMap<String, String>();
        result.put( "black", "000000");
        result.put( "blue", "0000FF");
        result.put( "cyan", "00FFFF");
        result.put( "darkBlue", "00008B");
        result.put( "darkCyan", "008B8B");
        result.put( "darkGray", "A9A9A9");
        result.put( "darkGreen", "006400");
        result.put( "darkMagenta", "800080");
        result.put( "darkRed", "8B0000");
        result.put( "darkYellow", "808000");
        result.put( "green", "00FF00");
        result.put( "lightGray", "D3D3D3");
        result.put( "magenta", "FF00FF");
        result.put( "none", "");
        result.put( "red", "FF0000");
        result.put( "white", "FFFFFF");
        result.put( "yellow", "FFFF00");
        return Collections.unmodifiableMap(result);
    }

    private static int rgbStringToInt(String rgbColor) {
        return Integer.parseInt(rgbColor, 16);
    }

    public static String mapHightlightColorToRgb(String highlightColor) {
        String rgbColor = null;

        Map<String, String> map = Commons.highlightColorToRgb;
        rgbColor = map.get(highlightColor);

        return rgbColor;
    }

    private static int colorDiff(int rgbColor1, int rgbColor2) {
        int r1 = ((rgbColor1 & 0x00ff0000) >> 16);
        int g1 = ((rgbColor1 & 0x0000ff00) >> 8);
        int y1 = (rgbColor1 & 0x000000ff);
        int r2 = ((rgbColor2 & 0x00ff0000) >> 16);
        int g2 = ((rgbColor2 & 0x0000ff00) >> 8);
        int y2 = (rgbColor2 & 0x000000ff);

        return Math.abs(r1-r2) + Math.abs(g1-g2) + Math.abs(y1-y2);
    }

    public static String mapRgbToPredefined(String rgbColor, Map<String, String> mappings) {
        String predefinedColor = null;
        int diff = Integer.MAX_VALUE;;
        int rgbColorValue = Integer.parseInt(rgbColor);

        if (mappings != null) {
            Set<String> keys = mappings.keySet();
            for (String color : keys) {
                int colorValue = Commons.rgbStringToInt(color);
                int currDiff = Commons.colorDiff(rgbColorValue, colorValue);
                if (currDiff < diff) {
                    diff = currDiff;
                    predefinedColor = color;
                    if (diff == 0)
                        return predefinedColor;
                }
            }
        }
        return predefinedColor;
    }

    public static class Pair<T, E> {

        private T first;
        private E second;

        public Pair(T first, E second) {
            this.first = first;
            this.second = second;
        }
        public T getFirst() {
            return first;
        }
        public E getSecond() {
            return second;
        }
        public void setFirst(T first) {
            this.first = first;
        }
        public void setSecond(E second) {
            this.second = second;
        }
    }

    public static long coordinateTo100TH_MM(long coordinate) {
        return (coordinate + 180)/360;
    }
    public static long coordinateTo100TH_MM(String coordinate) {

        try {
            // ST_CoordinateUnqualified
            return (Long.parseLong(coordinate)+180)/360;
        }
        catch(NumberFormatException e) {
            // TODO: ST_UniversalMeasure -> 'cm', 'mm', 'in', 'pt', 'pc' and 'pi'
            return 0;
        }
    }

    public static long coordinateFrom100TH_MM(long mm100th) {
        return mm100th*360;
    }

    /**
     * @param part
     * @param rId
     * @return the targetUrl of the corresponding relationship
     */
    public static String getUrl(Part part, String rId) {

        String url = "";

        if(part != null && rId != null && rId.length() > 0) {

            final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
            if(relationshipsPart != null) {

                final Relationship relationShip = relationshipsPart.getRelationshipByID(rId);
                if(relationShip != null) {

                    try {
                        final String targetMode = relationShip.getTargetMode();
                        if(targetMode != null && targetMode.equals("External")) {
                            final String target = relationShip.getTarget();
                            if(target != null) {
                                url = relationShip.getTarget();
                            }
                        }
                        else {
                            url = URIHelper. resolvePartUri(part.getPartName().getURI(), new URI(relationShip.getTarget())).getPath();
                        }

                        if(url.charAt(0) == '/') {
                            url = url.substring(1);
                        }

                    } catch(Exception e) {
                        //
                    }
                }
            }
        }
        return url;
    }

    /**
     * sets an external target url to the relationship. if rId is null or empty, a new hyperlink relationship
     * will be created, otherwise the existing relationship will be reused.
     *
     * @param part
     * @param rId
     * @param Url
     * @return rId
     */
    public static String setUrl(Part part, String rId, String Url) {

        if(part!=null) {

            final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
            if(relationshipsPart!=null) {
                Relationship relationShip = null;
                if(rId==null||rId.length()==0) {
                	if(Url!=null&&!Url.isEmpty()) {
                		final Relationships relationships = relationshipsPart.getJaxbElement();
                		if(relationships!=null) {
                			final List<Relationship> relationshipsList = relationships.getRelationship();
                			for(Relationship relationship:relationshipsList) {
                				if(relationship.getType()==Namespaces.HYPERLINK) {
                					if(relationship.getTarget()==Url) {
                						return relationship.getId();
                					}
                				}
                			}
                		}
                	}
                    final org.docx4j.relationships.ObjectFactory factory =
                        new org.docx4j.relationships.ObjectFactory();
                    relationShip = factory.createRelationship();
                    relationShip.setType(Namespaces.HYPERLINK);
                    relationshipsPart.addRelationship(relationShip);
                    rId = relationShip.getId();
                }
                else
                    relationShip = relationshipsPart.getRelationshipByID(rId);

                if(relationShip!=null) {
                    relationShip.setTarget(Url);
                    relationShip.setTargetMode("External");
                }
            }
        }
        return rId;
    }

    public static byte[] retrieveResource(String imageUrl, IResourceManager resourceManager) {

        byte[] buf = null;
        final String filename = FilenameUtils.getBaseName(imageUrl);
        if(filename.startsWith(Resource.RESOURCE_UID_PREFIX)) {
            try {
                String uid = filename.substring(Resource.RESOURCE_UID_PREFIX.length());
                buf = resourceManager.getResourceBuffer(uid);
            }
            catch(NumberFormatException e) {
                //
            }
            if(buf==null) {
                log.error("ooxml export: could not get buffer of resource " + filename);
            }
        }
        return buf;
    }

    /**
    *
    * @param part (for docx the uri of the part is something like: word/document.xml)
    * @param url (should be like word/media/xxx.png)
    * @return the relationship of an url, the relationship is null if there is no relationship available
    */
   public static Relationship getRelationship(Part part, String _url) {

       if(part==null||_url==null||_url.isEmpty())
           return null;

       try {

           final boolean isAbsolute = new URI(_url).isAbsolute();
           final String url = isAbsolute ? _url : '/' + _url;

           final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
           if(relationshipsPart!=null) {
               final List<Relationship> relationships = relationshipsPart.getRelationships().getRelationship();
               for(Relationship rel:relationships) {
                   try {
                       if(rel.getTarget()!=null) {
                           if(!isAbsolute&&(rel.getTargetMode()==null||rel.getTargetMode().equalsIgnoreCase("Internal"))) {
                               final String targetPath = URIHelper.resolvePartUri(part.getPartName().getURI(), new URI(rel.getTarget())).getPath();
                               if(targetPath!=null&&targetPath.equals(url)) {
                                   return rel;
                               }
                           }
                           else if(isAbsolute&&rel.getTarget().equals(url)) {
                               return rel;
                           }
                       }
                   } catch (Exception e) {
                   // TODO Auto-generated catch block
                   }
               }
           }
       } catch (URISyntaxException e1) {
           // TODO Auto-generated catch block
       }
       return null;
   }

   public static Relationship createGraphicRelation(OfficeOpenXMLOperationDocument operationDocument, Part part, String imageUrl)
       throws InvalidFormatException {

       // check if the image is already part of the document
       Relationship rel = Commons.getRelationship(part, imageUrl);
       if(rel==null) {
           // Ensure the relationships part exists
           RelationshipsPart relationshipsPart = part.getRelationshipsPart();
           if(relationshipsPart==null) {
               relationshipsPart = RelationshipsPart.createRelationshipsPartForPart(part);
           }

           final String proposedRelId = part.getRelationshipsPart().getNextId();
           final ContentTypeManager ctm = operationDocument.getPackage().getContentTypeManager();

           String contentType = ContentTypes.IMAGE_JPEG;
           final String extension = FileHelper.getExtension(imageUrl);
           if(extension.length()>0) {
               if (extension.equals(ContentTypes.EXTENSION_JPG_1))
                   contentType = ContentTypes.IMAGE_JPEG;
               else if(extension.equals(ContentTypes.EXTENSION_JPG_2))
                   contentType = ContentTypes.IMAGE_JPEG;
               else if(extension.equals(ContentTypes.EXTENSION_PNG))
                   contentType = ContentTypes.IMAGE_PNG;
               else if(extension.equals(ContentTypes.EXTENSION_GIF))
                   contentType = ContentTypes.IMAGE_GIF;
               else if(extension.equals(ContentTypes.EXTENSION_BMP))
                   contentType = ContentTypes.IMAGE_BMP;
               else if(extension.equals(ContentTypes.EXTENSION_EMF))
                   contentType = ContentTypes.IMAGE_EMF;
               else if(extension.equals(ContentTypes.EXTENSION_WMF))
                   contentType = ContentTypes.IMAGE_WMF;
               else if(extension.equals(ContentTypes.EXTENSION_TIFF))
                   contentType = ContentTypes.IMAGE_TIFF;
               else if(extension.equals(ContentTypes.EXTENSION_PICT))
                   contentType = ContentTypes.IMAGE_PICT;
           }

           byte[] imageData = Commons.retrieveResource(imageUrl, operationDocument.getResourceManager());
           if(imageData==null) {
        	   // check if we can find the image in the source part store
        	   final PartStore sourcePartStore = operationDocument.getPackage().getSourcePartStore();
        	   if(sourcePartStore instanceof ZipPartStore) {
        		   final String url = imageUrl.charAt(0)=='/' ? imageUrl.substring(1) : imageUrl;
        		   if(((ZipPartStore)sourcePartStore).partExists(url)) {
        			   try {
        				   final InputStream in = ((ZipPartStore)sourcePartStore).loadPart(url);
        				   if(in!=null) {
        					   imageData = IOUtils.toByteArray(in);
        				   }
        			   }
        			   catch (IOException e) {
        			       //
        			   }
        		   }
        	   }
           }
           if(imageData==null) {

        	   // we will create an external link to the graphic
               Part imagePart = ctm.newPartForContentType(contentType, new PartName(BinaryPartAbstractImage.createImageName(operationDocument.getPackage(), part, proposedRelId, "ext")), null);
               rel = part.addTargetPart(imagePart);
               rel.setTargetMode("External");
               if(imagePart instanceof BinaryPart)
                   operationDocument.getPackage().getExternalResources().put(((BinaryPart)imagePart).getExternalTarget(), imagePart);
               rel.setTarget(imageUrl.toString());
           }
           else {

        	   // the graphic is embedded and will be inserted added to the zip archive
               Part imagePart = ctm.newPartForContentType(contentType, new PartName("/" + imageUrl), null);
               if(imagePart instanceof BinaryPart)
                   ((BinaryPart)imagePart).setBinaryData(imageData);
               if(imagePart!=null)
                   rel = part.addTargetPart(imagePart, proposedRelId);
           }
       }
       return rel;
   }
}
