/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.tools;

import org.docx4j.dml.BaseStyles;
import org.docx4j.dml.BaseStyles.FontScheme;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.dml.FontCollection;
import org.docx4j.dml.TextFont;
import org.docx4j.dml.Theme;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.STTheme;


public class ThemeFonts {

    private String minorFont = "Calibri";
    private String majorFont = "Cambria";

    /**
     * The internal identifier of the major theme font (headings) used in OOXML.
     */
    public static final String MAJOR_ID = "+mj-lt";

    /**
     * The internal identifier of the minor theme font (body text) used in OOXML.
     */
    public static final String MINOR_ID = "+mn-lt";

    public ThemeFonts(ThemePart themePart) {
        if(themePart!=null) {
        	final Theme theme = themePart.getJaxbElement();
        	if(theme!=null) {
	            BaseStyles themeElements = theme.getThemeElements();
	            if(themeElements!=null) {
	                FontScheme fontScheme = themeElements.getFontScheme();
	                if(fontScheme!=null) {
	                    FontCollection minorFontCollection = fontScheme.getMinorFont();
	                    if(minorFontCollection!=null) {
	                        TextFont minorTextFont = minorFontCollection.getLatin();
	                        if(minorTextFont!=null)
	                            minorFont = minorTextFont.getTypeface();
	                    }
	                    FontCollection majorFontCollection = fontScheme.getMajorFont();
	                    if(majorFontCollection!=null) {
	                        TextFont majorTextFont = majorFontCollection.getLatin();
	                        if(majorTextFont!=null)
	                            majorFont = majorTextFont.getTypeface();
	                    }
	                }
	            }
        	}
        }
    }

    public String getMinorFont() {
        return minorFont;
    }

    public String getMajorFont() {
        return majorFont;
    }

    private String getTheme(STTheme theme) {
        String font = null;
        if(theme==STTheme.MAJOR_H_ANSI)
            font = getMajorFont();
        else if(theme==STTheme.MINOR_H_ANSI)
            font = getMinorFont();
        else if(theme==STTheme.MAJOR_ASCII)
            font = getMajorFont();
        else if(theme==STTheme.MINOR_ASCII)
            font = getMinorFont();
        return font;
    }

    public String getFont(RFonts rFonts) {
        String font = null;
        if(rFonts!=null) {
            // first we have to try themes...
            font = getTheme(rFonts.getHAnsiTheme());
            if(font==null)
                font = getTheme(rFonts.getAsciiTheme());

            // no theme found...
            if(font==null)
                font = rFonts.getHAnsi();
            if(font==null||font.length()==0)
                font = rFonts.getAscii();
        }
        return font;
    }

    public RFonts setFont(String fontName) {
        final RFonts rFonts = new RFonts();
        if(MINOR_ID.equals(fontName)) {
            rFonts.setAsciiTheme(STTheme.MINOR_H_ANSI);
            rFonts.setHAnsiTheme(STTheme.MINOR_H_ANSI);
        }
        else if(MAJOR_ID.equals(fontName)) {
            rFonts.setAsciiTheme(STTheme.MAJOR_H_ANSI);
            rFonts.setHAnsiTheme(STTheme.MAJOR_H_ANSI);
        }
        else {
            rFonts.setAscii(fontName);
            rFonts.setHAnsi(fontName);
            rFonts.setCs(fontName);
            rFonts.setEastAsia(fontName);
        }
        return rFonts;
    }
}
