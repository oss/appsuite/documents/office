/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.filter.ooxml.tools.json;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import org.docx4j.dml.CTAngle;
import org.docx4j.dml.CTComplementTransform;
import org.docx4j.dml.CTFixedPercentage;
import org.docx4j.dml.CTGammaTransform;
import org.docx4j.dml.CTGrayscaleTransform;
import org.docx4j.dml.CTInverseGammaTransform;
import org.docx4j.dml.CTInverseTransform;
import org.docx4j.dml.CTPercentage;
import org.docx4j.dml.CTPositiveFixedAngle;
import org.docx4j.dml.CTPositiveFixedPercentage;
import org.docx4j.dml.CTPositivePercentage;
import org.docx4j.dml.IIntValueAccess;
import org.docx4j.dml.ObjectFactory;
import org.docx4j.jaxb.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class JSONHelper {

	public static void saveTransformations(List<JAXBElement<?>> transform, JSONObject color) throws JSONException {
		transform.clear();
		final JSONArray transformations = color.optJSONArray(OCKey.TRANSFORMATIONS.value());
		if (transformations == null || transformations.length() == 0) {
			return;
		}
		for (int i = 0; i < transformations.length(); i++) {
			final JSONObject trans = transformations.getJSONObject(i);
			transform.add(getTrans(trans.getString(OCKey.TYPE.value()), trans.optInt(OCKey.VALUE.value())));
		}
	}

	private static JAXBElement<?> getTrans(String name, Integer value) {
		final ObjectFactory fact = Context.getDmlObjectFactory();
		switch(name) {
		    // CTPercentage
            case "blue"    : return fact.createCTSchemeColorBlue(new CTPercentage(value));
            case "blueMod" : return fact.createCTSchemeColorBlueMod(new CTPercentage(value));
            case "blueOff" : return fact.createCTSchemeColorBlueOff(new CTPercentage(value));
            case "green"   : return fact.createCTSchemeColorGreen(new CTPercentage(value));
            case "greenMod": return fact.createCTSchemeColorGreenMod(new CTPercentage(value));
            case "greenOff": return fact.createCTSchemeColorGreenOff(new CTPercentage(value));
            case "red"     : return fact.createCTSchemeColorRed(new CTPercentage(value));
            case "redMod"  : return fact.createCTSchemeColorRedMod(new CTPercentage(value));
            case "redOff"  : return fact.createCTSchemeColorRedOff(new CTPercentage(value));
            case "lum"     : return fact.createCTSchemeColorLum(new CTPercentage(value));
            case "lumMod"  : return fact.createCTSchemeColorLumMod(new CTPercentage(value));
            case "lumOff"  : return fact.createCTSchemeColorLumOff(new CTPercentage(value));
            case "sat"     : return fact.createCTSchemeColorSat(new CTPercentage(value));
            case "satMod"  : return fact.createCTSchemeColorSatMod(new CTPercentage(value));
            case "satOff"  : return fact.createCTSchemeColorSatOff(new CTPercentage(value));

            // CTPositivePercentage
            case "alphaMod": return fact.createCTSchemeColorAlphaMod(new CTPositivePercentage(value));
            case "hueMod"  : return fact.createCTSchemeColorHueMod(new CTPositivePercentage(value));
                

            // CTPositiveFixedPercentage
            case "shade"   : return fact.createCTSchemeColorShade(new CTPositiveFixedPercentage(value));
            case "tint"    : return fact.createCTSchemeColorTint(new CTPositiveFixedPercentage(value));
            case "alpha"   : return fact.createCTSchemeColorAlpha(new CTPositiveFixedPercentage(value));

            // CTFixedPercentage
            case "alphaOff": return fact.createCTSchemeColorAlphaOff(new CTFixedPercentage(value));

            // STPositiveFixedAngle
            case "hue"     : return fact.createCTSchemeColorHue(new CTPositiveFixedAngle(value));
            
            // STAngle
            case "hueOff"  : return fact.createCTSchemeColorHueOff(new CTAngle(value));
                
            // no value
            case "gamma"   : return fact.createCTSchemeColorGamma(new CTGammaTransform());
            case "gray"    : return fact.createCTSchemeColorGray(new CTGrayscaleTransform());
            case "inv"     : return fact.createCTSchemeColorInv(new CTInverseTransform());
            case "invGamma": return fact.createCTSchemeColorInvGamma(new CTInverseGammaTransform());
            case "comp"    : return fact.createCTSchemeColorComp(new CTComplementTransform());

            default: return fact.createCTScRgbColorTint(new CTPositiveFixedPercentage(0));
		}
	}

    public static String toHex(int one, int two, int three) {
        return toHex(one) + toHex(two) + toHex(three);
    }

    public static String toHex(int value) {
        String res = Integer.toHexString(value);
        while (res.length() < 2) {
            res = "0" + res;
        }
        return res;
    }

    private static void appendTransformation(JSONArray transformations, String type) throws JSONException {
        JSONObject transform = new JSONObject(1);
        transform.put(OCKey.TYPE.value(), type);
        transformations.put(transform);
    }

    private static void appendTransformation(JSONArray transformations, String type, double value) throws JSONException {
        JSONObject transform = new JSONObject(2);
        transform.put(OCKey.TYPE.value(), type);
        transform.put(OCKey.VALUE.value(), value);
        transformations.put(transform);
    }

    public static JSONObject makeColor(JSONObject sourceColor, String type, Object value, List<JAXBElement<?>> egColorTransform)
    	throws JSONException {

        final JSONObject destColor = sourceColor!=null ? sourceColor : new JSONObject(3);
        if(value!=null) {
        	destColor.put(OCKey.TYPE.value(), type);
        	destColor.put(OCKey.VALUE.value(), value);
        }
        if (egColorTransform!=null&&!egColorTransform.isEmpty()) {
        	JSONArray transformations = null;
        	if(sourceColor!=null) {
        		transformations = sourceColor.optJSONArray(OCKey.TRANSFORMATIONS.value());
        	}
        	if(transformations==null) {
        		transformations = new JSONArray(egColorTransform.size());
        		destColor.put(OCKey.TRANSFORMATIONS.value(), transformations);
        	}
            for (final JAXBElement<?> jax : egColorTransform) {
                final QName name = jax.getName();
                final Object perc = jax.getValue();
                if (perc instanceof IIntValueAccess) {
                	appendTransformation(transformations, name.getLocalPart(), ((IIntValueAccess)perc).getVal());
                } else {
                	appendTransformation(transformations, name.getLocalPart());
                }
            }
            if(transformations.isEmpty()) {
            	destColor.remove(OCKey.TRANSFORMATIONS.value());
            }
        }
        return destColor;
    }

    public static JSONObject makeJSON(String key1, Object value1, String key2, Object value2, String key3, Object value3) throws JSONException {
        final JSONObject res = new JSONObject(3);
        res.put(key1, value1);
        res.put(key2, value2);
        res.put(key3, value3);
        return res;
    }

    /**
     * Converts the passed shade/tint value as used in various OOXML elements outside DrawingML, e.g.
     * the attributes "themeShade", "themeTint", "themeFillShade", and "themeFillTint" used in DOCX,
     * or the signed "tint" attribute of color elements in XLSX, to JSON color transformations.
     *
     * @param shadeTint
     * 	The shade/tint value, in the interval [-1;1]. A negative value represents a shade transformation
     * (darkened colors), a positive value represents a tint transformation (lightened colors).
     *
     * @return
     * 	The JSON transformations that are equivalent to the passed shade/tint value.
     *
     * @throws JSONException
     */
    public static JSONArray shadeTintToJsonTransformations(Double shadeTint) throws JSONException {

    	// ignore invalid input values
    	double value = (shadeTint == null) ? 0.0 : Math.round(shadeTint.doubleValue() * 100000.0);
    	if (value == 0.0 || Math.abs(value) > 100000.0) { return null; }

        JSONArray transformations = new JSONArray();
        if (value < 0.0) {
            appendTransformation(transformations, "lumMod", value + 100000.0);
        } else {
        	appendTransformation(transformations, "lumMod", 100000.0 - value);
        	appendTransformation(transformations, "lumOff", value);
        }

        return transformations;
    }

    /**
     * Converts the passed JSON color transformations to a simple shade/tint value as used in various
     * OOXML elements outside DrawingML, e.g. the attributes "themeShade", "themeTint", "themeFillShade",
     * and "themeFillTint" used in DOCX, or the signed "tint" attribute of color elements in XLSX.
     * 
     * @param jsonColor
     * 	The JSON color with color transformations, as used in document operations. A single "lumMod"
     * 	transformation with a value less than 100% will be interpreted as shade (this method will return
     * 	a negative value). A combination of "lumMod" and "lumOff" transformations with a value sum of
     * 	100% (e.g. lumMod=20%, lumOff=80%) will be interpreted as tint (this method will return a
     * 	negative value). All other transformations in any other combinations are considered to be
     * 	unsupported (this method will return zero).
     *
     * @return
     * 	The shade/tint value extracted from the color transformations, in the interval [-1;1]. A negative
     *  value represents a shade transformation (darkened colors), a positive value represents a tint
     *  transformation (lightened colors).
     */
    public static Double jsonTransformationsToShadeTint(final JSONObject jsonColor) {

    	// missing or empty array
		final JSONArray transformations = jsonColor.optJSONArray(OCKey.TRANSFORMATIONS.value());
    	if (transformations == null || transformations.isEmpty()) { return null; }

    	// the luminance transformation values extracted from the array
    	double lumMod = -1.0;
    	double lumOff = -1.0;

    	// process all color transformations in the array
    	for (int i = 0, l = transformations.length(); i < l; ++i) {

    		// array elements must be JSON objects
    		JSONObject transform = transformations.optJSONObject(i);
            if (transform == null) { return null; }

            // the type specifier of the transformation
            String type = transform.optString(OCKey.TYPE.value());
            if (type == null || type.isEmpty()) { return null; }

            // the value of the transformations (transformations without value are considered unsupported)
            Double value = transform.optDouble(OCKey.VALUE.value());
            if (value == null || value.doubleValue() < 0.0) { return null; }

	    	// first transformation must be "lumMod"
        	if (type.equals("lumMod")) {
        		if (lumMod < 0.0) {
        			lumMod = value.doubleValue() / 100000.0;
        			continue;
        		}
        	}

	    	// second transformation must be "lumOff"
        	if (type.equals("lumOff")) {
        		if (lumMod >= 0.0 && lumOff < 0.0) {
        			lumOff = value.doubleValue() / 100000.0;
        			continue;
        		}
        	}

        	// anything else is unsupported
        	return null;
    	}

    	// return if "lumMod" transformation has not been found or is invalid
    	if (lumMod < 0.0 || lumMod >= 1.0) { return null; }

    	// "lumMod" without "lumOff": darkened color
		if (lumOff < 0.0) {
			return lumMod - 1.0;
		}

    	// "lumMod" and "lumOff": lightened color (only if sum of the values is 1)
		if (Math.abs(lumMod + lumOff - 1.0) < 0.001) {
			return lumOff;
		}

		return null;
    }
}
