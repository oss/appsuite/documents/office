/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.io.IOUtils;
import org.docx4j.dml.Theme;
import org.docx4j.docProps.core.CoreProperties;
import org.docx4j.docProps.core.dc.elements.SimpleLiteral;
import org.docx4j.docProps.core.dc.terms.W3CDTF;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.contenttype.ContentTypeManager.NonPersistContent;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.io3.Save;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.WordprocessingML.DocumentSettingsPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.vml.CTShapetype;
import org.docx4j.wml.CTSettings;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.InitializingBean;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.OperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.tools.ThemeFonts;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

abstract public class OfficeOpenXMLOperationDocument extends OperationDocument implements InitializingBean {

	private static final String MODULE = "//module/";
	private ByteArrayInputStream debugDocument;
    protected boolean saveDebugOperations;
    private TreeSet<String> changeTrackAuthors = null;
    protected Part contextPart = null;
    protected ThemeFonts themeFonts = null;
    private Map<String, CTShapetype> vmlShapeTypes = null;

    static final int DEFAULT_BUFFER_SIZE = 8192;

    protected OfficeOpenXMLOperationDocument(Session session, IResourceManager resourceManager, DocumentProperties documentProperties) {
        super(session, resourceManager, documentProperties);
    }

    private boolean createFastLoadOperations = false;

    public void setCreateFastLoadOperations(boolean createFastLoadOperations) {
        this.createFastLoadOperations = createFastLoadOperations;
    }

    public boolean isCreateFastLoadOperations() {
        return createFastLoadOperations;
    }

    @Override
	public void afterPropertiesSet() throws Exception {
        saveDebugOperations = Boolean.parseBoolean(getOfficeConfigurationValue(MODULE + "debugoperations", "false"));
        if(documentProperties!=null) {
            documentProperties.optBoolean("useConfiguration", true);
        }
	}

	final public void loadDocument(InputStream inputStream, boolean XMLReadOnly) throws Exception {
        try {
            debugDocument = _loadDocument(inputStream, XMLReadOnly);
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
    }
    abstract protected ByteArrayInputStream _loadDocument(InputStream inputStream, boolean XMLReadOnly) throws Exception;

    final public void createPackage() throws Exception {
        debugDocument = null;
        _createPackage();
    }
    abstract protected void _createPackage() throws Exception;

    final public InputStream getDebugDocument() {
        debugDocument.reset();
        return debugDocument;
    }

    final public void applyDefaultDocumentProperties() throws Exception {
        try {
            _applyDefaultDocumentProperties();
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
    }
    abstract public void _applyDefaultDocumentProperties() throws Exception;

    final public JSONObject getOperations() throws Exception {
        JSONObject operations = null;
        try {
            operations = _getOperations();
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
        return operations;
    }
    abstract protected JSONObject _getOperations() throws Exception;

   final public Map<String, Object> getMetaData() throws Exception {
       Map<String, Object> metaData = null;
       try {
           metaData = _getMetaData();
       }
       catch(Throwable e) {
           rethrowFilterException(e, getPackage());
       }
       return metaData;
    }
    abstract protected Map<String, Object> _getMetaData() throws Exception;

    final public Map<String, Object> getActivePart() throws Exception {
        Map<String, Object> metaData = null;
        try {
            metaData = _getActivePart();
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
        return metaData;
    }
    abstract protected Map<String, Object> _getActivePart() throws Exception;

    final public Map<String, Object> getNextPart() throws Exception {
        Map<String, Object> metaData = null;
        try {
            metaData = _getNextPart();
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
        return metaData;
    }
    abstract protected Map<String, Object> _getNextPart() throws Exception;

    final public void applyOperations(String applyOperations) throws Exception {
        try {
            _applyOperations(applyOperations);
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
    }
    abstract public void _applyOperations(String applyOperations) throws Exception;

    final public void updateDocumentProperties() throws Exception {
        // applying some document properties...
        OpcPackage opcPackage = getPackage();
        CoreProperties coreProperties = opcPackage.getDocPropsCorePart(true).getJaxbElement();
        org.docx4j.docProps.core.dc.elements.ObjectFactory of = new org.docx4j.docProps.core.dc.elements.ObjectFactory();
        String userName = documentProperties.optString(DocumentProperties.PROP_LAST_MODIFIED_BY, "");
        coreProperties.setLastModifiedBy(userName);
        coreProperties.setModified(new W3CDTF(currentDate()));
        coreProperties.setRevision(documentProperties.optString(DocumentProperties.PROP_REVISION, null));
        coreProperties.setVersion(documentProperties.optString(DocumentProperties.PROP_VERSION, null));

        if(userName.length()>0) {
            SimpleLiteral creatorLiteral = coreProperties.getCreator();
            if(creatorLiteral==null) {
                creatorLiteral = of.createSimpleLiteral();
                creatorLiteral.getContent().add(userName);
                coreProperties.setCreator(creatorLiteral);
            }
        }
        final String uniqueDocumentIdentifier = documentProperties.optString(DocumentProperties.PROP_UNIQUE_DOCUMENT_IDENTIFIER, null);
        if (null != uniqueDocumentIdentifier) {
            putUniqueDocumentIdentifier(uniqueDocumentIdentifier);
        }
    }

    public static String currentDate() throws DatatypeConfigurationException {
        final DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        final XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(new GregorianCalendar(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC")));
        return now.toXMLFormat();
    }

    final public InputStream save() {
        return debugSave(null);
    }

    /* For debug purposes we create a folder (debug) within the destination document
     * that stores each information necessary to repeat the last save operation that was done.
     * (original document, the operations just applied and the content of the resource manager. */

    final public InputStream debugSave(String debugOperations) {

        ByteArrayInputStream inputStream = null;
        try {
            boolean contentTypeChanged = true;
            if (documentProperties.optBoolean(DocumentProperties.PROP_SAVE_TEMPLATE_DOCUMENT, false)) {
                if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.WORDPROCESSINGML_DOCUMENT)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/word/document.xml").getURI(), ContentTypes.WORDPROCESSINGML_TEMPLATE);
                }
                else if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.SPREADSHEETML_WORKBOOK)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/xl/workbook.xml").getURI(), ContentTypes.SPREADSHEETML_TEMPLATE);
                }
                else if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.PRESENTATIONML_MAIN)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/ppt/presentation.xml").getURI(), ContentTypes.PRESENTATIONML_TEMPLATE);
                }
                else {
                    contentTypeChanged = false;
                }
                if(contentTypeChanged) {
                    switchToTemplateDocument();
                }
            }
            else {
                if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.WORDPROCESSINGML_TEMPLATE)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/word/document.xml").getURI(), ContentTypes.WORDPROCESSINGML_DOCUMENT);
                }
                else if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.SPREADSHEETML_TEMPLATE)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/xl/workbook.xml").getURI(), ContentTypes.SPREADSHEETML_WORKBOOK);
                }
                else if(getPackage().getContentTypeManager().getPartNameOverridenByContentType(ContentTypes.PRESENTATIONML_TEMPLATE)!=null) {
                    getPackage().getContentTypeManager().addOverrideContentType(new PartName("/ppt/presentation.xml").getURI(), ContentTypes.PRESENTATIONML_MAIN);
                }
                else {
                    contentTypeChanged = false;
                }
                if(contentTypeChanged) {    // switching from document from template to non template -> we have to remove the author
                    switchToNonTemplateDocument();
                }
            }
            if(!contentTypeChanged&&documentProperties.optBoolean(DocumentProperties.PROP_CREATED_BY_DEFAULT_TEMPLATE, false)) {
                resetCoreProperties();
            }

            // always removing thumbnails when saving
            removePart(getPackage().getRelationshipsPart(), "/docProps/thumbnail.jpeg");

            if(saveDebugOperations)  {
                int currentVersion = getCurrentDebugDocumentVersion(getDebugDocument());
                if(debugOperations!=null) {
                    getPackage().getContentTypeManager().addNonPersistContentFile("debug/v" + String.format ("%05d", currentVersion) + "-operationUpdates.dbg",
                        debugOperations.getBytes("UTF-8"),"dbg", "application/octet-stream");
                }
                final String extension = getExtension("dbg");
                getPackage().getContentTypeManager().addNonPersistContentFile("debug/v" + String.format ("%05d", currentVersion) + "-documentStream." + extension,
                    getSourceDocumentWithoutDebugFolder(getDebugDocument()), extension, "application/octet-stream");
                final boolean saveDocHistory = this.documentProperties.optBoolean(DocumentProperties.PROP_SAVE_DOC_HISTORY, false);
                if(saveDocHistory) {
                    addPreviousDebugVersions(getDebugDocument());
                }
            }
            final Save saver = new Save(getPackage());
            saver.setCheckUnusedRelations(createFinalDocument);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            saver.save(out);
            inputStream = new ByteArrayInputStream(out.toByteArray());
        }
        catch(Throwable e) {
            rethrowFilterException(e, getPackage());
        }
        return inputStream;
    }

    private void addPreviousDebugVersions(InputStream is) throws IOException {
        try(ZipInputStream zipInputStream = new ZipInputStream(is)) {
            ZipEntry zipEntry = null;
            byte[] buffer = new byte[0x10000];  // 64kb buffer
            while((zipEntry = zipInputStream.getNextEntry())!=null) {
                final String name = zipEntry.getName();
                if(!zipEntry.isDirectory()) {
                    if(name.startsWith("debug/")) {
                        int read;
                        final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                        while ((read = zipInputStream.read(buffer))>=0){
                            outStream.write(buffer,0,read);
                        }
                        getPackage().getContentTypeManager().addNonPersistContentFile(name,
                            outStream.toByteArray(), "dbg", "application/octet-stream");
                    }
                }
                zipInputStream.closeEntry();
            }
        }
    }

    private int getCurrentDebugDocumentVersion(InputStream is) throws IOException {
        int currentVersion = 1;
        try(final ZipInputStream zipInputStream = new ZipInputStream(is)) {
            while(true) {
                try {
                    ZipEntry zipEntry = zipInputStream.getNextEntry();
                    if(zipEntry==null) {
                        break;
                    }
                    final String name = zipEntry.getName();
                    if(name.startsWith("debug/")) {
                        int s = name.indexOf('v');
                        int e = name.lastIndexOf('-');
                        if(e!=-1&&s!=-1&&s<e) {
                            final String number = name.substring(s+1, e);
                            currentVersion = Math.max(currentVersion, Integer.parseUnsignedInt(number) + 1);
                        }
                    }
                }
                catch(ZipException e) {
                    //
                }
            }
        }
        return currentVersion;
    }

    private byte[] getSourceDocumentWithoutDebugFolder(InputStream is) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try(ZipInputStream zipInputStream = new ZipInputStream(is);
            ZipOutputStream zipOutputStream = new ZipOutputStream(outStream)) {

            byte[] buffer = new byte[0x10000];  // 64kb buffer
            while(true) {
                try {
                    ZipEntry zipEntry = zipInputStream.getNextEntry();
                    if(zipEntry==null) {
                        break;
                    }
                    // if the source document is containing a debug folder, we will
                    // skip the old debug information and not include it within our copy
                    if(!zipEntry.getName().startsWith("debug/")) {
                        ZipEntry newEntry = new ZipEntry(zipEntry.getName());
                        zipOutputStream.putNextEntry(newEntry);
                        int read;
                        while ((read = zipInputStream.read(buffer))>=0){
                            zipOutputStream.write(buffer,0,read);
                        }
                        zipOutputStream.closeEntry();
                    }
                    zipInputStream.closeEntry();
                }
                catch(ZipException e) {
                    //
                }
            }
        }
        return outStream.toByteArray();
    }

    private String getExtension(String _default) {
        try {
            String contentType = getPackage().getContentTypeManager().getContentType(new PartName("/word/document.xml"));
            if(contentType==null) {
                contentType = getPackage().getContentTypeManager().getContentType(new PartName("/xl/workbook.xml"));
            }
            if(contentType==null) {
                contentType = getPackage().getContentTypeManager().getContentType(new PartName("/ppt/presentation.xml"));
            }
            if(contentType!=null) {
                switch(contentType) {
                    case ContentTypes.WORDPROCESSINGML_DOCUMENT : return "docx";
                    case ContentTypes.WORDPROCESSINGML_TEMPLATE : return "dotx";
                    case ContentTypes.SPREADSHEETML_WORKBOOK: return "xlsx";
                    case ContentTypes.SPREADSHEETML_TEMPLATE: return "xltx";
                    case ContentTypes.PRESENTATIONML_MAIN: return "pptx";
                    case ContentTypes.PRESENTATIONML_TEMPLATE: return "xltx";
                }
            }
        }
        catch(InvalidFormatException e) {
            //
        }
        return _default;
    }

    public static void logMessage(final String msgClass, final String logMessage) {
        if ("fatal".equalsIgnoreCase(msgClass))
            log.error(logMessage);
        else if ("error".equalsIgnoreCase(msgClass))
            log.error(logMessage);
        else if ("warning".equalsIgnoreCase(msgClass))
            log.warn(logMessage);
        else if ("debug".equalsIgnoreCase(msgClass))
            log.debug(logMessage);
        else // "info"
            log.info(logMessage);
    }

    /**
     * Add the unique document identifier to the document.
     *
     * @param uniqueIdentifier
     *  The unique document identifier to be saved into the document stream.
     */
    private boolean putUniqueDocumentIdentifier(String uniqueIdentifier) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("uniqueDocumentIdentifier", uniqueIdentifier);

            getPackage().getContentTypeManager().addNonPersistContentFile("oxoffice/extendedData.json",
                jsonObject.toString().getBytes("UTF-8"),"json", "application/octet-stream");
        } catch (UnsupportedEncodingException e) {
            log.error("OOXML Filter could not save the unique document indentifier:", e);
            return false;
        } catch (JSONException e) {
            log.error("OOXML Filter could not create JSON object to store the unique document indentifier:", e);
            return false;
        }

        return true;
    }

    public void writeFile(String path) throws FilterException {
        InputStream  inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = save();

            if (null != inputStream) {
                IOUtils.copy(inputStream, outputStream = new FileOutputStream(new File(path)));
                outputStream.flush();
            }
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            throw new FilterException("OOXML Filter could not save the given document:", e, ErrorCode.CRITICAL_ERROR);
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    abstract public OpcPackage getPackage();

    abstract public void setPackage(OpcPackage opcPackage);

    abstract public ThemePart getThemePart(boolean createIfMissing)
        throws FilterException;

    public ThemeFonts getThemeFonts() {
        if(themeFonts==null) {
            themeFonts = new ThemeFonts(getThemePart(true));
        }
        return themeFonts;
    }

    /*
     * Returns the vmlPart that belongs to the given sourcePart. If the vmlId is null or empty then it is searched for
     * the type "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing" only.
     */
    public static VMLPart getVMLPart(Part sourcePart, String vmlId) {
        VMLPart vmlPart = null;
        final RelationshipsPart sourceRelationships = sourcePart.getRelationshipsPart();
        if(sourceRelationships!=null) {
            if(vmlId==null||vmlId.isEmpty()) {
                final Relationship drawingRelation = sourceRelationships.getRelationshipByType(Namespaces.VML);
                if(drawingRelation!=null) {
                    final Part part = sourceRelationships.getPart(drawingRelation);
                    if(part instanceof VMLPart) {
                        vmlPart = (VMLPart)part;
                    }
                }
            } else if(!vmlId.isEmpty()) {
                final Part part = sourceRelationships.getPart(vmlId);
                if(part.getRelationshipType().equals(Namespaces.VML)&&part instanceof VMLPart) {
                    vmlPart = (VMLPart)part;
                }
            }
        }
        return vmlPart;
    }

    @Override
    public void notifyLowMemory() {
        log.debug("OOXML Filter: Low memory notification received");
        final OpcPackage opcPackage = getPackage();
        if(opcPackage!=null) {
            opcPackage.setLowMemoryAbort();
            opcPackage.setSourcePartStore(null);
            opcPackage.setContentTypeManager(null);
        }
    }

    public static void rethrowFilterException(Throwable e, OpcPackage opcPackage) {

        ExceptionUtils.handleThrowable(e);

        FilterException ret;
        if(Context.isLowMemoryAbort(opcPackage)) {
            ret = new FilterException("FilterException...", ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED);
        }
        else if(e instanceof FilterException) {
            ret = (FilterException)e;
        }
        else if (e instanceof OutOfMemoryError) {
            ret = new FilterException(e, ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED);
        }
        else {
            ret = new FilterException(e, ErrorCode.CRITICAL_ERROR);
        }
        if(opcPackage!=null) {
            ret.setOperationCount(opcPackage.getSuccessfulAppliedOperations());
        }
        throw ret;
    }

    protected void switchToTemplateDocument() throws Exception {

        resetCoreProperties();
        removePart(getPackage().getRelationshipsPart(), "/docProps/app.xml");

        // removing extendedData
        final List<NonPersistContent> nonPersistContentList = getPackage().getContentTypeManager().getNonPersistContent();
        for(int i=0; i<nonPersistContentList.size(); i++) {
            final NonPersistContent nonPersistContent = nonPersistContentList.get(i);
            if(nonPersistContent.zipEntryName.equals("oxoffice/extendedData.json")) {
                nonPersistContentList.remove(i);
                break;
            }
        }
    }

    protected void switchToNonTemplateDocument() throws Exception {

        resetCoreProperties();
        removePart(getPackage().getRelationshipsPart(), "/docProps/app.xml");
    }

    private void resetCoreProperties() throws DatatypeConfigurationException {
        final DocPropsCorePart corePart = getPackage().getDocPropsCorePart(false);
        if(corePart!=null) {
            final CoreProperties coreProperties = corePart.getJaxbElement();
            coreProperties.setCategory(removeEmptyElement(coreProperties.getCategory()));
            coreProperties.setContentStatus(removeEmptyElement(coreProperties.getContentStatus()));
            coreProperties.setContentType(removeEmptyElement(coreProperties.getContentType()));
            coreProperties.setCreated(new W3CDTF(currentDate()));
            coreProperties.setCreator(null);
            coreProperties.setDescription(removeEmptyElement(coreProperties.getDescription()));
            coreProperties.setIdentifier(removeEmptyElement(coreProperties.getIdentifier()));
            coreProperties.setKeywords(removeEmptyElement(coreProperties.getKeywords()));
            coreProperties.setLanguage(removeEmptyElement(coreProperties.getLanguage()));
            coreProperties.setLastModifiedBy(null);
            coreProperties.setLastPrinted(null);
            coreProperties.setModified(null);
            coreProperties.setRevision(null);
            coreProperties.setSubject(removeEmptyElement(coreProperties.getSubject()));
            coreProperties.setTitle(removeEmptyElement(coreProperties.getTitle()));
        }
    }

    protected void removePart(final RelationshipsPart relationshipsPart, final String partName) {
        if(relationshipsPart!=null) {
            try {
                relationshipsPart.removePart(new PartName(partName));
            }
            catch(InvalidFormatException e) {
                //
            }
        }
    }

    private <T> T removeEmptyElement(T value) {
        if(value instanceof String) {
            if(((String) value).isEmpty()) {
                return null;
            }
        }
        else if(value instanceof SimpleLiteral) {
            if((((SimpleLiteral)value).getLang()==null||((SimpleLiteral)value).getLang().isEmpty())&&
                (((SimpleLiteral)value).getContent().isEmpty())) {
                    return null;
            }
        }
        else if(value instanceof JAXBElement<?>) {
            if(((JAXBElement<?>)value).getDeclaredType().equals("org.docx4j.docProps.core.dc.elements.SimpleLiteral")) {
                final SimpleLiteral o = (SimpleLiteral)((JAXBElement<?>)value).getValue();
                if((o.getLang()==null||o.getLang().isEmpty())&&
                    (o.getContent().isEmpty())) {
                    return null;
                }
            }
        }
        return value;
    }

    public TreeSet<String> getChangeTrackAuthors(boolean forceCreate) {
        if(changeTrackAuthors == null && forceCreate) {
            changeTrackAuthors = new TreeSet<>();
        }
        return changeTrackAuthors;
    }

    public boolean hasChangeTrackAuthor(String author) {
        return changeTrackAuthors == null ? false : changeTrackAuthors.contains(author);
    }

    public abstract Part getContextPart();

    public abstract OfficeOpenXMLComponent getRootComponent();

    public Part setContextPart(Part part) {
        contextPart = part;
        return part;
    }

    public abstract String getImagePath();

    protected ThemePart getDefaultThemePart(PartName partName) {

        final String themeStr =

            "<a:theme xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\"" +
            "    name=\"${themeName}\">" +
            "    <a:themeElements>" +
            "        <a:clrScheme name=\"${themeName}\">" +
            "            <a:dk1>" + "${dk1}" + " </a:dk1>" +
            "            <a:lt1>" + "${lt1}" + " </a:lt1>" +
            "            <a:dk2>" + "${dk2}" + " </a:dk2>" +
            "            <a:lt2>" + "${lt2}" + " </a:lt2>" +
            "            <a:accent1>" + "${accent1}" + " </a:accent1>" +
            "            <a:accent2>" + "${accent2}" + " </a:accent2>" +
            "            <a:accent3>" + "${accent3}" + " </a:accent3>" +
            "            <a:accent4>" + "${accent4}" + " </a:accent4>" +
            "            <a:accent5>" + "${accent5}" + " </a:accent5>" +
            "            <a:accent6>" + "${accent6}" + " </a:accent6>" +
            "            <a:hlink>"   + "${hlink}"   + " </a:hlink>"   +
            "            <a:folHlink>"+ "${folHlink}"+ " </a:folHlink>"+
            "        </a:clrScheme>" +
            "        <a:fontScheme name=\"${themeName}\">" +
            "            <a:majorFont>" +
            "                <a:latin typeface=\"${majorFont}\" />" +
            "                <a:ea typeface=\"\" />" +
            "                <a:cs typeface=\"\" />" +
            "            </a:majorFont>" +
            "            <a:minorFont>" +
            "                <a:latin typeface=\"${minorFont}\" />" +
            "                <a:ea typeface=\"\" />" +
            "                <a:cs typeface=\"\" />" +
            "            </a:minorFont>" +
            "        </a:fontScheme>" +
            "        <a:fmtScheme name=\"${themeName}\">" +
            "            <a:fillStyleLst>" +
            "                <a:solidFill>" +
            "                    <a:schemeClr val=\"phClr\" />" +
            "                </a:solidFill>" +
            "                <a:gradFill rotWithShape=\"1\">" +
            "                    <a:gsLst>" +
            "                        <a:gs pos=\"0\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"50000\" />" +
            "                                <a:satMod val=\"300000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"35000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"37000\" />" +
            "                                <a:satMod val=\"300000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"100000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"15000\" />" +
            "                                <a:satMod val=\"350000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                    </a:gsLst>" +
            "                    <a:lin ang=\"16200000\" scaled=\"1\" />" +
            "                </a:gradFill>" +
            "                <a:gradFill rotWithShape=\"1\">" +
            "                    <a:gsLst>" +
            "                        <a:gs pos=\"0\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:shade val=\"51000\" />" +
            "                                <a:satMod val=\"130000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"80000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:shade val=\"93000\" />" +
            "                                <a:satMod val=\"130000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"100000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:shade val=\"94000\" />" +
            "                                <a:satMod val=\"135000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                    </a:gsLst>" +
            "                    <a:lin ang=\"16200000\" scaled=\"0\" />" +
            "                </a:gradFill>" +
            "            </a:fillStyleLst>" +
            "            <a:lnStyleLst>" +
            "                <a:ln w=\"9525\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">" +
            "                    <a:solidFill>" +
            "                        <a:schemeClr val=\"phClr\">" +
            "                            <a:shade val=\"95000\" />" +
            "                            <a:satMod val=\"105000\" />" +
            "                        </a:schemeClr>" +
            "                    </a:solidFill>" +
            "                    <a:prstDash val=\"solid\" />" +
            "                </a:ln>" +
            "                <a:ln w=\"25400\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">" +
            "                    <a:solidFill>" +
            "                        <a:schemeClr val=\"phClr\" />" +
            "                    </a:solidFill>" +
            "                    <a:prstDash val=\"solid\" />" +
            "                </a:ln>" +
            "                <a:ln w=\"38100\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">" +
            "                    <a:solidFill>" +
            "                        <a:schemeClr val=\"phClr\" />" +
            "                    </a:solidFill>" +
            "                    <a:prstDash val=\"solid\" />" +
            "                </a:ln>" +
            "            </a:lnStyleLst>" +
            "            <a:effectStyleLst>" +
            "                <a:effectStyle>" +
            "                    <a:effectLst>" +
            "                        <a:outerShdw blurRad=\"40000\" dist=\"20000\" dir=\"5400000\"" +
            "                            rotWithShape=\"0\">" +
            "                            <a:srgbClr val=\"000000\">" +
            "                                <a:alpha val=\"38000\" />" +
            "                            </a:srgbClr>" +
            "                        </a:outerShdw>" +
            "                    </a:effectLst>" +
            "                </a:effectStyle>" +
            "                <a:effectStyle>" +
            "                    <a:effectLst>" +
            "                        <a:outerShdw blurRad=\"40000\" dist=\"23000\" dir=\"5400000\"" +
            "                            rotWithShape=\"0\">" +
            "                            <a:srgbClr val=\"000000\">" +
            "                                <a:alpha val=\"35000\" />" +
            "                            </a:srgbClr>" +
            "                        </a:outerShdw>" +
            "                    </a:effectLst>" +
            "                </a:effectStyle>" +
            "                <a:effectStyle>" +
            "                    <a:effectLst>" +
            "                        <a:outerShdw blurRad=\"40000\" dist=\"23000\" dir=\"5400000\"" +
            "                            rotWithShape=\"0\">" +
            "                            <a:srgbClr val=\"000000\">" +
            "                                <a:alpha val=\"35000\" />" +
            "                            </a:srgbClr>" +
            "                        </a:outerShdw>" +
            "                    </a:effectLst>" +
            "                    <a:scene3d>" +
            "                        <a:camera prst=\"orthographicFront\">" +
            "                            <a:rot lat=\"0\" lon=\"0\" rev=\"0\" />" +
            "                        </a:camera>" +
            "                        <a:lightRig rig=\"threePt\" dir=\"t\">" +
            "                            <a:rot lat=\"0\" lon=\"0\" rev=\"1200000\" />" +
            "                        </a:lightRig>" +
            "                    </a:scene3d>" +
            "                    <a:sp3d>" +
            "                        <a:bevelT w=\"63500\" h=\"25400\" />" +
            "                    </a:sp3d>" +
            "                </a:effectStyle>" +
            "            </a:effectStyleLst>" +
            "            <a:bgFillStyleLst>" +
            "                <a:solidFill>" +
            "                    <a:schemeClr val=\"phClr\" />" +
            "                </a:solidFill>" +
            "                <a:gradFill rotWithShape=\"1\">" +
            "                    <a:gsLst>" +
            "                        <a:gs pos=\"0\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"40000\" />" +
            "                                <a:satMod val=\"350000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"40000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"45000\" />" +
            "                                <a:shade val=\"99000\" />" +
            "                                <a:satMod val=\"350000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"100000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:shade val=\"20000\" />" +
            "                                <a:satMod val=\"255000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                    </a:gsLst>" +
            "                    <a:path path=\"circle\">" +
            "                        <a:fillToRect l=\"50000\" t=\"-80000\" r=\"50000\" b=\"180000\" />" +
            "                    </a:path>" +
            "                </a:gradFill>" +
            "                <a:gradFill rotWithShape=\"1\">" +
            "                    <a:gsLst>" +
            "                        <a:gs pos=\"0\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:tint val=\"80000\" />" +
            "                                <a:satMod val=\"300000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                        <a:gs pos=\"100000\">" +
            "                            <a:schemeClr val=\"phClr\">" +
            "                                <a:shade val=\"30000\" />" +
            "                                <a:satMod val=\"200000\" />" +
            "                            </a:schemeClr>" +
            "                        </a:gs>" +
            "                    </a:gsLst>" +
            "                    <a:path path=\"circle\">" +
            "                        <a:fillToRect l=\"50000\" t=\"50000\" r=\"50000\" b=\"50000\" />" +
            "                    </a:path>" +
            "                </a:gradFill>" +
            "            </a:bgFillStyleLst>" +
            "        </a:fmtScheme>" +
            "    </a:themeElements>" +
            "    <a:objectDefaults />" +
            "    <a:extraClrSchemeLst />" +
            "</a:theme>";

        final String defaultSchemeName = "Office";

        ThemePart themePart = null;
        try {
            themePart = new ThemePart(partName);
            java.util.HashMap<String, String> mappings = new java.util.HashMap<>();
            mappings.put("themeName", defaultSchemeName);
            mappings.put("dk1", "<a:sysClr val=\"windowText\" lastClr=\"000000\" />");
            mappings.put("lt1", "<a:sysClr val=\"window\" lastClr=\"FFFFFF\" />");
            mappings.put("dk2", "<a:srgbClr val=\"44546A\" />");
            mappings.put("lt2", "<a:srgbClr val=\"E7E6E6\" />");
            mappings.put("accent1", "<a:srgbClr val=\"5B9BD5\" />");
            mappings.put("accent2", "<a:srgbClr val=\"ED7D31\" />");
            mappings.put("accent3", "<a:srgbClr val=\"A5A5A5\" />");
            mappings.put("accent4", "<a:srgbClr val=\"FFC000\" />");
            mappings.put("accent5", "<a:srgbClr val=\"4472C4\" />");
            mappings.put("accent6", "<a:srgbClr val=\"70AD47\" />");
            mappings.put("hlink",   "<a:srgbClr val=\"0563C1\" />");
            mappings.put("folHlink","<a:srgbClr val=\"954F72\" />");
            mappings.put("majorFont", "Times New Roman");
            mappings.put("minorFont", "Arial");
            Theme theme = (Theme)org.docx4j.XmlUtils.unmarshallFromTemplate(themeStr, getPackage(), mappings);
            themePart.setJaxbElement(theme);
        }
        catch (JAXBException e) {
            e.printStackTrace();
        }
        return themePart;
    }

    public DocumentSettingsPart getDefaultSettingsPart(PartName partName) {

        final String documentSettingsStr =
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
            "<w:settings\n" +
            "   xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\"\n" +
            "   xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
            "   xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"\n" +
            "   xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\"\n" +
            "   xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\"\n" +
            "   xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\"\n" +
            "   xmlns:w14=\"http://schemas.microsoft.com/office/word/2010/wordml\"\n" +
            "   xmlns:w15=\"http://schemas.microsoft.com/office/word/2012/wordml\"\n" +
            "   xmlns:sl=\"http://schemas.openxmlformats.org/schemaLibrary/2006/main\"\n" +
            "   mc:Ignorable=\"w14 w15\">\n" +
            "   <w:zoom w:percent=\"100\" />\n" +
            "   <w:defaultTabStop w:val=\"720\" />\n" +
            "   <w:characterSpacingControl w:val=\"doNotCompress\" />\n" +
            "   <w:compat>\n" +
            "       <w:compatSetting w:name=\"compatibilityMode\"\n" +
            "           w:uri=\"http://schemas.microsoft.com/office/word\" w:val=\"15\" />\n" +
            "       <w:compatSetting w:name=\"overrideTableStyleFontSizeAndJustification\"\n" +
            "           w:uri=\"http://schemas.microsoft.com/office/word\" w:val=\"1\" />\n" +
            "       <w:compatSetting w:name=\"enableOpenTypeFeatures\"\n" +
            "           w:uri=\"http://schemas.microsoft.com/office/word\" w:val=\"1\" />\n" +
            "       <w:compatSetting w:name=\"doNotFlipMirrorIndents\"\n" +
            "           w:uri=\"http://schemas.microsoft.com/office/word\" w:val=\"1\" />\n" +
            "       <w:compatSetting w:name=\"differentiateMultirowTableHeaders\"\n" +
            "           w:uri=\"http://schemas.microsoft.com/office/word\" w:val=\"1\" />\n" +
            "   </w:compat>\n" +
            "   <w:rsids>\n" +
            "       <w:rsidRoot w:val=\"008463FB\" />\n" +
            "       <w:rsid w:val=\"002B1CD8\" />\n" +
            "       <w:rsid w:val=\"008463FB\" />\n" +
            "   </w:rsids>\n" +
            "   <m:mathPr>\n" +
            "       <m:mathFont m:val=\"Cambria Math\" />\n" +
            "       <m:brkBin m:val=\"before\" />\n" +
            "       <m:brkBinSub m:val=\"--\" />\n" +
            "       <m:smallFrac m:val=\"0\" />\n" +
            "       <m:dispDef />\n" +
            "       <m:lMargin m:val=\"0\" />\n" +
            "       <m:rMargin m:val=\"0\" />\n" +
            "       <m:defJc m:val=\"centerGroup\" />\n" +
            "       <m:wrapIndent m:val=\"1440\" />\n" +
            "       <m:intLim m:val=\"subSup\" />\n" +
            "       <m:naryLim m:val=\"undOvr\" />\n" +
            "   </m:mathPr>\n" +
            "   <w:themeFontLang w:val=\"en-US\" />\n" +
            "   <w:clrSchemeMapping w:bg1=\"light1\" w:t1=\"dark1\"\n" +
            "       w:bg2=\"light2\" w:t2=\"dark2\" w:accent1=\"accent1\" w:accent2=\"accent2\"\n" +
            "       w:accent3=\"accent3\" w:accent4=\"accent4\" w:accent5=\"accent5\" w:accent6=\"accent6\"\n" +
            "       w:hyperlink=\"hyperlink\" w:followedHyperlink=\"followedHyperlink\" />\n" +
            "   <w:shapeDefaults>\n" +
            "       <o:shapedefaults v:ext=\"edit\" spidmax=\"1026\" />\n" +
            "       <o:shapelayout v:ext=\"edit\">\n" +
            "           <o:idmap v:ext=\"edit\" data=\"1\" />\n" +
            "       </o:shapelayout>\n" +
            "   </w:shapeDefaults>\n" +
            "   <w:decimalSymbol w:val=\",\" />\n" +
            "   <w:listSeparator w:val=\";\" />\n" +
            "</w:settings>";

        DocumentSettingsPart documentSettingsPart = null;
        try {
            documentSettingsPart = new DocumentSettingsPart(partName);
            java.util.HashMap<String, String> mappings = new java.util.HashMap<>();

            final CTSettings documentSettings = (CTSettings)((JAXBElement<?>)org.docx4j.XmlUtils.unmarshallFromTemplate(documentSettingsStr, getPackage(), mappings)).getValue();
            documentSettingsPart.setJaxbElement(documentSettings);
        }
        catch (JAXBException e) {
            e.printStackTrace();
        }
        return documentSettingsPart;
    }

    public Map<String, CTShapetype> getVMLShapeTypes() {
        return vmlShapeTypes;
    }

    public void setVMLShapeTypes(Map<String, CTShapetype> shapeTypes) {
        this.vmlShapeTypes = shapeTypes;
    }

    private HashSet<String> usedLanguages = new HashSet<>();

    public Set<String> getUsedLanguages() {
        return usedLanguages;
    }

    private HashSet<String> usedLanguagesEA = new HashSet<>();

    public Set<String> getUsedLanguagesEA() {
        return usedLanguagesEA;
    }

    private HashSet<String> usedLanguagesBidi = new HashSet<>();

    public Set<String> getUsedLanguagesBidi() {
        return usedLanguagesBidi;
    }
}
