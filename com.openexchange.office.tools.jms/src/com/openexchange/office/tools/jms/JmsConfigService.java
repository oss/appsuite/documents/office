/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.jms;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.tools.annotation.RegisteredService;

@Service
@RegisteredService
public class JmsConfigService {

    public static final String KEY_JMS_USERNAME = "com.openexchange.dcs.client.username";
    public static final String JMS_USERNAME_DEFAULT = "";
    
    public static final String KEY_JMS_PASSWORD = "com.openexchange.dcs.client.password";
    public static final String JMS_PASSWORD_DEFAULT = ""; 
    
    public static final String KEY_JMS_SSL_ENABLED = "com.openexchange.dcs.client.ssl.enabled";
    public static final String KEY_JMS_SSL_VERIFY_HOSTNAME = "com.openexchange.dcs.client.ssl.verifyHostname";

    public static final String KEY_JMS_SSL_KEYSTORE_PATH = "com.openexchange.dcs.client.ssl.keystore.path";
    public static final String JMS_SSL_KEYSTORE_PATH_DEFAULT = "/opt/open-xchange/etc/security/documents-collaboration-client.ks";

    public static final String KEY_JMS_SSL_KEYSTORE_PASSWORD = "com.openexchange.dcs.client.ssl.keystore.password";
    public static final String JMS_SSL_KEYSTORE_PASSWORD_DEFAULT = "password";

    public static final String KEY_JMS_SSL_TRUSTSTORE_PATH = "com.openexchange.dcs.client.ssl.truststore.path";
    public static final String JMS_SSL_TRUSTSTORE_PATH_DEFAULT = "/opt/open-xchange/etc/security/documents-collaboration-client.ts";

    public static final String KEY_JMS_SSL_TRUSTSTORE_PASSWORD = "com.openexchange.dcs.client.ssl.truststore.password";
    public static final String JMS_SSL_TRUSTSTORE_PASSWORD_DEFAULT = "password";

    @Autowired
    private ConfigurationService configurationService;    
    
    //-------------------------------------------------------------------------
    /**
     * Provides the state of the verify host name feature to check
     * the host name against a stored certificate when using SSL
     * connections.
     * <b>REMARK: The current implementation does not support host names for
     * DCS instances, therefore setting verifyHostName to true won't make any
     * sense.</b>
     * TODO: change this hard-coded solution if we want to support host names, too.
     *
     * @return FALSE
     */
    public boolean isRT2SSLVerifyHostname() {
        return false;
        //return configurationService.getBoolProperty(KEY_JMS_SSL_VERIFY_HOSTNAME, false);
    }

    //-------------------------------------------------------------------------
    public boolean isRT2SSLEnabled() {
        return configurationService.getBoolProperty(KEY_JMS_SSL_ENABLED, false);
    }

    //-------------------------------------------------------------------------
    public String getRT2SSLKeystorePath() {
        final String sID = configurationService.getProperty(KEY_JMS_SSL_KEYSTORE_PATH);
        return StringUtils.isBlank(sID) ? JMS_SSL_KEYSTORE_PATH_DEFAULT : sID;
    }

    //-------------------------------------------------------------------------
    public String getRT2SSLKeystorePassword() {
        final String sID = configurationService.getProperty(KEY_JMS_SSL_KEYSTORE_PASSWORD);
        return StringUtils.isBlank(sID) ? JMS_SSL_KEYSTORE_PASSWORD_DEFAULT : sID;
    }

    //-------------------------------------------------------------------------
    public String getRT2SSLTrustStorePath() {
        final String sID = configurationService.getProperty(KEY_JMS_SSL_TRUSTSTORE_PATH);
        return StringUtils.isBlank(sID) ? JMS_SSL_TRUSTSTORE_PATH_DEFAULT : sID;
    }

    //-------------------------------------------------------------------------
    public String getRT2SSLTrustStorePassword() {
        final String sID = configurationService.getProperty(KEY_JMS_SSL_TRUSTSTORE_PASSWORD);
        return StringUtils.isBlank(sID) ? JMS_SSL_TRUSTSTORE_PASSWORD_DEFAULT : sID;
    }

    //-------------------------------------------------------------------------
    public String getJmsUsername() {
        final String sID = configurationService.getProperty(KEY_JMS_USERNAME);
        return StringUtils.isBlank(sID) ? JMS_USERNAME_DEFAULT : sID;
    }

    //-------------------------------------------------------------------------
    public String getJmsPassword() {
        final String sID = configurationService.getProperty(KEY_JMS_PASSWORD);
        return StringUtils.isBlank(sID) ? JMS_PASSWORD_DEFAULT : sID;
    }    
}
