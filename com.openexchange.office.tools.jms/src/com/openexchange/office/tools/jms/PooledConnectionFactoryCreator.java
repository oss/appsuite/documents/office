/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.jms;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.transport.TransportListener;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Throwables;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.dcs.registry.DCSRegistry;
import com.openexchange.office.dcs.registry.DCSRegistryException;
import com.openexchange.office.dcs.registry.DCSRegistryItem;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;

@Service
public class PooledConnectionFactoryCreator implements OsgiBundleContextAware, DisposableBean {

	private static final Logger log = LoggerFactory.getLogger(PooledConnectionFactoryCreator.class);

	@Autowired
	private JmsConfigService jmsConfigService;

    @Autowired
    private DCSRegistry dcsRegistry;

    private final ScheduledExecutorService scheduledExecService = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder("UrlInSyncWatchDogThread-%d").build());

    private LocalDateTime lastErrMsg = null;

	private AtomicReference<PooledConnectionFactory> pooledConnectionFactoryRef = new AtomicReference<>();

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
	    // Prevent a double initialization due to calling setApplicationContext() via
	    // injectDependencies && by a explicit call of setApplicationContext() in the
	    // OSGi / spring-boot convenience layer (OsgiBundlesContextAndActivator).
	    if (pooledConnectionFactoryRef.get() == null) {
	        if (pooledConnectionFactoryRef.compareAndSet(null, new PooledConnectionFactory())) {
	            log.debug("Creating PooledConnectionFactory for connection to JMS Server...");
	            pooledConnectionFactoryRef.get().setConnectionFactory(getJmsConnectionFactory());
	            bundleCtx.registerServiceToOsgi(new PooledConnectionFactoryProxy(pooledConnectionFactoryRef.get()), PooledConnectionFactoryProxy.class);
	            bundleCtx.registerServiceToOsgi(getJmsTemplateWithoutTtl(), JmsTemplateWithoutTtl.class);
	            bundleCtx.registerServiceToOsgi(getJmsTemplateWithTtl(), JmsTemplateWithTtl.class);
	        }
	    }
	}

    @Override
	public void destroy() throws Exception {
        scheduledExecService.shutdown();
        //-------------------------------------------------------------------------
        log.info("... deactivate connection factory");
        PooledConnectionFactory factory = pooledConnectionFactoryRef.get();
        if (factory != null) {
            try {
                factory.stop();
            } catch (Exception e) {
                // DOCS-4402 in case a shutdown is in progress it's possible that
                // we cannot invoke any asynchronous processes in the ActiveMQ client
                // anymore. Just catch possible exceptions and log them on DEBUG
                // level.
                log.debug("Exception caught trying to stop PooledConnectionFactory", e);
            }
        }
    }

    private JmsTemplateWithoutTtl getJmsTemplateWithoutTtl() {
    	JmsTemplateWithoutTtl res =  new JmsTemplateWithoutTtl(pooledConnectionFactoryRef.get());
    	res.setTimeToLive(0);
    	res.setExplicitQosEnabled(false);
    	return res;
    }

    private JmsTemplateWithTtl getJmsTemplateWithTtl() {
    	JmsTemplateWithTtl res = new JmsTemplateWithTtl(pooledConnectionFactoryRef.get());
    	res.setTimeToLive(60000);
    	res.setExplicitQosEnabled(true);    	
    	return res;
    }

    private EnhActiveMQSSLConnectionFactory getJmsConnectionFactory() {
        final EnhActiveMQSSLConnectionFactory jmsConnectionFactory = new EnhActiveMQSSLConnectionFactory();

        String jmsUsername = jmsConfigService.getJmsUsername();
        String jmsPassword = jmsConfigService.getJmsPassword();
        if (StringUtils.isNotBlank(jmsUsername)) {
            jmsConnectionFactory.setUserName(jmsUsername);
        }
        if (StringUtils.isNotBlank(jmsPassword)) {
            jmsConnectionFactory.setPassword(jmsPassword);
        }

        final String failOverBrokerUrl = getFailoverBrokerUrl();
        jmsConnectionFactory.setBrokerURL(failOverBrokerUrl);

        if (failOverBrokerUrl.contains("ssl:")) {
            try {
                // set keystore properties
                jmsConnectionFactory.setKeyStore(jmsConfigService.getRT2SSLKeystorePath());
                jmsConnectionFactory.setKeyStorePassword(jmsConfigService.getRT2SSLKeystorePassword());

                // set truststore properties
                jmsConnectionFactory.setTrustStore(jmsConfigService.getRT2SSLTrustStorePath());
                jmsConnectionFactory.setTrustStorePassword(jmsConfigService.getRT2SSLTrustStorePassword());
            } catch (Exception e) {
                log.error(Throwables.getRootCause(e).getMessage());
            }
        }

        jmsConnectionFactory.setTransportListener(new TransportListener() {

            private String lastErrorMsg = null;

            @Override
            public void transportResumed() {
                log.info("Resumed transport connection to dcs with broker url {}", jmsConnectionFactory.getBrokerURL());
            }

            @Override
            public void transportInterupted() {
                log.info("Interrupted transport connection to dcs with broker url {}", jmsConnectionFactory.getBrokerURL());
            }

            @Override
            public void onException(IOException ex) {
                if ((lastErrorMsg == null) || !lastErrorMsg.equals(ex.getMessage())) {
                    log.info("Caught IOException: {}", ex);
                    lastErrorMsg = ex.getMessage();
                }
                jmsConnectionFactory.setBrokerURL(getFailoverBrokerUrl());
            }

            @Override
            public void onCommand(Object cmd) {
                log.debug("onCommand: {}", cmd);
            }
        });
        scheduledExecService.scheduleWithFixedDelay(new UrlInSyncWatchDogThread(jmsConnectionFactory), 1, 1, TimeUnit.MINUTES);
        return jmsConnectionFactory;
    }

    private String getFailoverBrokerUrl() {
        Collection<DCSRegistryItem> dcsRegistryItems = new ArrayList<>();
        try {
             dcsRegistryItems = dcsRegistry.getDCSInstances();
        } catch (DCSRegistryException ex) {
            if (lastErrMsg == null) {
                lastErrMsg = LocalDateTime.now();
                log.error("Exception caught - cannot provide broker url, please check configuration & setup! DatabaseException: " + ex.getMessage());
            } else {
                if (ChronoUnit.MINUTES.between(LocalDateTime.now(), lastErrMsg) > 1) {
                    log.error("Exception caught - cannot provide broker url, please check configuration & setup!");
                    lastErrMsg = null;
                }
            }
        }

        final StringBuilder res = new StringBuilder(256).append("failover:(");

        Iterator<DCSRegistryItem> iter = dcsRegistryItems.iterator();
        while (iter.hasNext()) {
            final DCSRegistryItem item = iter.next();
            final boolean useSSL = item.isUseSSL();

            res.append(useSSL ? "ssl" : "tcp").append("://").
                append(item.getInterface()).
                append(":").
                append(item.getJMSPort());

            if (useSSL) {
                res.append("?socket.verifyHostName=" + (jmsConfigService.isRT2SSLVerifyHostname() ? "true" : "false"));
            }

            if (iter.hasNext()) {
                res.append(",");
            }
        }
        res.append(")?maxReconnectAttempts=3");

        // DOCS-3619: We have to disable the updateURIsSupported option if SSL connections are
        // used between client & broker with failover transport. There is a strange behavior of
        // ActiveMQ and the JMS client. It receives a ConnectionControl message from a broker and starts
        // a rebalance process. For this rebalancing the FailoverTransport class prioritize the connected
        // broker information from ConnectionControl message, where the correct protocol and ip-addresses
        // are provided, but which lacks the important query parameters. This leads to the fact that
        // the verifyHostName option get lost on this rebalance process and the client won't be able to
        // connect to a broker that uses a certificate that does not match to the domain/hostname.
        // Since ActiveMQ 5.4 the option updateURIsSupported has been implemented and by default is set
        // to true.
        if (jmsConfigService.isRT2SSLEnabled() && !jmsConfigService.isRT2SSLVerifyHostname()) {
            res.append("&updateURIsSupported=false");
        }
        ActiveMQConnectionFactory amqFactory = (ActiveMQConnectionFactory)(pooledConnectionFactoryRef.get().getConnectionFactory());
        if ((amqFactory == null) || !amqFactory.getBrokerURL().equals(res.toString())) {
            log.info("Broker Url for the connection to DCS is: {}", res);
        } else {
            log.debug("Broker Url for the connection to DCS is: {}", res);
        }
        return res.toString();
    }

    private class UrlInSyncWatchDogThread implements Runnable {

        private ActiveMQConnectionFactory activeMqConnectionFactory;

        public UrlInSyncWatchDogThread(ActiveMQConnectionFactory activeMqConnectionFactory) {
            this.activeMqConnectionFactory = activeMqConnectionFactory;
        }

        @SuppressWarnings("synthetic-access")
        @Override
        public void run() {
            // DOCS-4583 ensure that background processing is running although
            // unexpected (runtime) exceptions are thrown. Otherwise we lose the
            // ability to react on changing IP addresses which can trigger DOCS-4583.
            try {
                String currentUrl = activeMqConnectionFactory.getBrokerURL();
                String discoveredBasedUrl = getFailoverBrokerUrl();
                if (!currentUrl.equals(discoveredBasedUrl)) {
                    activeMqConnectionFactory.setBrokerURL(discoveredBasedUrl);
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);
                log.error("Unexpected exception caught in UrlInSyncWatchDogThread" ,e);
            }
        }
    }
}
