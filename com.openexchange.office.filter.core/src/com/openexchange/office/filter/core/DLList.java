/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import com.openexchange.office.filter.core.component.Child;

public class DLList<E> implements List<E> {

    private DLNode<E> first;
    private DLNode<E> last;

	public DLList() {
		first = null;
		last = null;
	}

	public E getFirst() {
		return first!=null ? first.getData() : null;
	}

	public E getLast() {
		return last!=null ? last.getData() : null;
	}

	@Override
	public boolean add(E e) {
		addNode(new DLNode<E>(e));
		return true;
	}

	@Override
	public void add(int index, E element) {
		addNode(index, new DLNode<E>(element));
	}

	public void addFirst(E e) {
		addFirstNode(new DLNode<E>(e));
	}

	@Override
	public boolean addAll(Collection<? extends E> arg0) {
        for(E e:arg0) {
            add(e);
        }
        return !arg0.isEmpty();
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends E> arg1) {
        for(E e:arg1) {
            add(arg0, e);
        }
        return !arg1.isEmpty();
	}

	@Override
	public void clear() {
		first = null;
		last = null;
	}

	@Override
	public boolean contains(Object o) {
		DLNode<E> n = first;
		while(n!=null) {
			if(n.getData()==o) {
				return true;
			}
			n = n.next;
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
        for(Object o:arg0) {
            DLNode<E> n = first;
            while(n!=null) {
                if(!(n.getData()==o)) {
                    return false;
                }
                n = n.next;
            }
        }
        return true;
	}

	@Override
	public E get(int index) {
		return getNode(index).getData();
	}

	@Override
	public int indexOf(Object o) {
		int i = 0;
		DLNode<E> n = first;
		while(n!=null) {
			if(n.getData()==o) {
				return i;
			}
			n = n.next;
			i++;
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return first==null;
	}

	private class DLLIterator implements Iterator<E> {

		final private DLList<E> list;
		private DLNode<E> nextNode;
		private DLNode<E> lastNode;

		DLLIterator(DLList<E> list) {
			this.list = list;
			nextNode = list.getFirstNode();
		}

		@Override
		public boolean hasNext() {
			return nextNode!=null;
		}

		@Override
		public E next() {
			lastNode = nextNode;
			nextNode = nextNode.next;
			return lastNode.getData();
		}

		@Override
		public void remove() {
			list.removeNode(lastNode);
		}
	}

	@Override
	public Iterator<E> iterator() {
		return new DLLIterator(this);
	}

	@Override
	public int lastIndexOf(Object o) {
		int i = size() - 1;
		DLNode<E> n = last;
		while(n!=null) {
			if(n.getData()==o) {
				return i;
			}
			n = n.prev;
			i--;
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
	    return new ListIterator<E>() {

	        int i = 0;
	        DLNode<E> node = getFirstNode();
	        DLNode<E> lastReturned = null;

	        @Override
            public void add(E e) {
	            i++;
	            addNode(node, new DLNode<E>(e), true);
            }

            @Override
            public boolean hasNext() {
                return node!=null;
            }

            @Override
            public boolean hasPrevious() {
                return node!=null && node.prev!=null;
            }

            @Override
            public E next() {
                i++;
                lastReturned = node;
                node = node.next;
                return lastReturned.getData();
            }

            @Override
            public int nextIndex() {
                return i;
            }

            @Override
            public E previous() {
                i--;
                node = node.prev;
                return node.getData();
            }

            @Override
            public int previousIndex() {
                return i-1;
            }

            @Override
            public void remove() {
                i--;
                removeNode(node);
            }

            @Override
            public void set(E e) {
                lastReturned.setData(e);
            }
	    };
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		DLNode<E> n = first;
		while(n!=null) {
			if(n.getData()==o) {
				removeNode(n);
				return true;
			}
			n = n.next;
		}
		return false;
	}

	@Override
	public E remove(int index) {
		return removeNode(getNode(index)).getData();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E set(int index, E element) {
		final DLNode<E> n = getNode(index);
		final E ret = n.getData();
		n.setData(element);
		return ret;
	}

	@Override
	public int size() {
	    int size = 0;
	    DLNode<E> node = first;
	    while(node!=null) {
            size++;
	        node = node.next;
	    }
		return size;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
	    final int s = size();
	    final Object[] a = new Object[s];
	    DLNode<E> node = first;
	    for(int i=0; i < s; i++) {
	        a[i] = node.getData();
	        node = node.next;
	    }
		return a;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	public DLNode<E> getFirstNode() {
		return first;
	}

	public DLNode<E> getLastNode() {
		return last;
	}

	public void addNode(DLNode<E> newNode) {
		if(first==null) {
			first = newNode;
			last = first;
			newNode.next = null;
			newNode.prev = null;
		}
		else {
			final DLNode<E> prev = last;
			last = newNode;
			prev.next = last;
			newNode.prev = prev;
			newNode.next = null;
		}
	}

	public void addNode(int index, DLNode<E> newNode) {
        if(index==0) {
            addFirstNode(newNode);
        }
        else {
            addNode(getNode(index-1), newNode, false);
        }
	}

	public void addFirstNode(DLNode<E> newNode) {
		if(first==null) {
			first = newNode;
			last = first;
			newNode.next = null;
			newNode.prev = null;
		}
		else {
			final DLNode<E> f = first;
			first = newNode;
			first.prev = null;
			first.next = f;
			f.prev = first;
		}
	}

	public void addNode(DLNode<E> refNode, DLNode<E> newNode, boolean before) {
		if(refNode==null) {
			addNode(newNode);
		}
		else if(before) {
			newNode.prev = refNode.prev;
			newNode.next = refNode;
			if(refNode.prev!=null) {
				refNode.prev.next = newNode;
			}
			else {
				first = newNode;
			}
			refNode.prev = newNode;
		}
		else {
			newNode.prev = refNode;
			newNode.next = refNode.next;
			if(refNode.next!=null) {
				refNode.next.prev = newNode;
			}
			else {
				last = newNode;
			}
			refNode.next = newNode;
		}
	}

    public DLNode<E> setNode(DLNode<E> oldNode, DLNode<E> newNode) {
        newNode.prev = oldNode.prev;
        newNode.next = oldNode.next;
        if(newNode.prev!=null) {
            newNode.prev.next = newNode;
        }
        else {
            first = newNode;
        }
        if(newNode.next!=null) {
            newNode.next.prev = newNode;
        }
        else {
            last = newNode;
        }
        oldNode.prev = null;
        oldNode.next = null;
        return oldNode;
    }

    public void setNodes(DLNode<E> oldNode, DLList<E> newNodes) {

        final Object newParent = oldNode.getData() instanceof Child ? ((Child)oldNode.getData()).getParent() : null;

        final DLNode<E> newFirst = newNodes.first;
        newFirst.prev = oldNode.prev;
        if(newFirst.prev!=null) {
            newFirst.prev.next = newFirst;
        }
        else {
            first = newFirst;
        }
        final DLNode<E> newLast = newNodes.last;
        newLast.next = oldNode.next;
        if(newLast.next!=null) {
            newLast.next.prev = newLast;
        }
        else {
            last = newLast;
        }
        newNodes.first = newNodes.last = oldNode.next = oldNode.prev = null;

        DLNode<E> node = newFirst;
        while(true) {
            Object data = node.getData();
            if(data instanceof Child) {
                ((Child)data).setParent(newParent);
            }
            if(node==newLast) {
                break;
            }
            node = node.next;
        }
    }

	public DLNode<E> removeNode(DLNode<E> e) {
		if(e.prev!=null) {
			e.prev.next = e.next;
		}
		else {
			first = e.next;
		}
		if(e.next!=null) {
			e.next.prev = e.prev;
		}
		else {
			last = e.prev;
		}
		return e;
	}

	public void removeNodes(DLNode<E> startNode, DLNode<E> endNode) {
		if(startNode == endNode || endNode == null) {
			removeNode(startNode);
			return;
		}

		// removing nodes
		final DLNode<E> prevNode = startNode.prev;
		final DLNode<E> nextNode = endNode.next;
		if(prevNode!=null) {
			prevNode.next = nextNode;
			if(nextNode!=null) {
				nextNode.prev = prevNode;
			}
			else {
				last = prevNode;
			}
		}
		else {
			first = nextNode;
			if(nextNode!=null) {
				nextNode.prev = null;
			}
			else {
				last = null;
			}
		}
	}

	public void removeNodes(DLNode<E> startNode, int count) {
	    if(count==0) {
	        return;
	    }
	    DLNode<E> endNode = startNode;
	    for(int i=1; i<count; i++) {
	        endNode = endNode.next;
	    }
	    removeNodes(startNode, endNode);
	}

	public DLNode<E> getNode(int index) {
		DLNode<E> n = first;
		while(index-->0) {
			n = n.next;
		}
		return n;
	}

    private class DLLNodeIterator implements Iterator<DLNode<E>> {

        final private DLList<E> list;
        private DLNode<E> nextNode;
        private DLNode<E> lastNode;

        DLLNodeIterator(DLList<E> list) {
            this.list = list;
            nextNode = list.getFirstNode();
        }

        @Override
        public boolean hasNext() {
            return nextNode!=null;
        }

        @Override
        public DLNode<E> next() {
            lastNode = nextNode;
            nextNode = nextNode.next;
            return lastNode;
        }

        @Override
        public void remove() {
            list.removeNode(lastNode);
        }
    }

    public Iterator<DLNode<E>> getNodeIterator() {
        return new DLLNodeIterator(this);
    }

    public void moveNodes(DLList<E> destList) {
        moveNodes(destList, null);
    }

    public void moveNodes(DLList<E> destList, Object newParent) {
        if(first!=null) {
            DLNode<E> node = first;
            while(true) {
                Object data = node.getData();
                if(data instanceof Child) {
                    ((Child)data).setParent(newParent);
                }
                if(node==last) {
                    break;
                }
                node = node.next;
            }

            if(destList.last!=null) {
                destList.last.next = first;
                first.prev = destList.last;
                destList.last = last;
            }
            else {
                destList.first = first;
                destList.last = last;
            }
            first = null;
            last = null;
        }
    }

	public void moveNodes(DLNode<E> sourceNode, DLList<E> destList) {
	    final DLNode<E> tmpLast = last;
	    last = sourceNode.prev;
	    if(sourceNode.prev!=null) {
	        sourceNode.prev.next = null;
	    }
	    else {
	        first = null;
	    }
	    if(destList.last!=null) {
	        destList.last.next = sourceNode;
	        sourceNode.prev = destList.last;
	    }
	    else {
	        destList.first = sourceNode;
	        sourceNode.prev = null;
	    }
	    destList.last = tmpLast;
	}

	public void moveNodes(DLNode<E> sourceNode, int count, DLList<E> destList, DLNode<E> destRefNode, boolean before) {
	    moveNodes(sourceNode, count, destList, destRefNode, before, null);
	}

	public void moveNodes(DLNode<E> sourceNode, int count, DLList<E> destList, DLNode<E> destRefNode, boolean before, Object newParent) {
		while(((count==-1)||(count-->=1))&&sourceNode!=null) {
			final DLNode<E> nextNode = sourceNode.next;
			final DLNode<E> nextRefNode = removeNode(sourceNode);
			destList.addNode(destRefNode, nextRefNode, before);
            if(newParent!=null) {
                final Object data = nextRefNode.getData();
                if(data instanceof Child) {
                    ((Child)data).setParent(newParent);
                }
            }
			sourceNode = nextNode;
			destRefNode = nextRefNode;
			before = false;
		}
	}

    public void moveNodes(DLNode<E> sourceNode, DLNode<E> sourceEndNode, DLList<E> destList, DLNode<E> destRefNode, boolean before) {
        moveNodes(sourceNode, sourceEndNode, destList, destRefNode, before, null);
    }

	public void moveNodes(DLNode<E> sourceNode, DLNode<E> sourceEndNode, DLList<E> destList, DLNode<E> destRefNode, boolean before, Object newParent) {
		do {
			final DLNode<E> nextNode = sourceNode.next;
			final DLNode<E> nextRefNode = removeNode(sourceNode);
			destList.addNode(destRefNode, nextRefNode, before);
            if(newParent!=null) {
                final Object data = nextRefNode.getData();
                if(data instanceof Child) {
                    ((Child)data).setParent(newParent);
                }
            }
			if(sourceNode==sourceEndNode) {
				break;
			}
			sourceNode = nextNode;
			destRefNode = nextRefNode;
			before = false;
		}
		while(sourceNode!=null);
	}
}
