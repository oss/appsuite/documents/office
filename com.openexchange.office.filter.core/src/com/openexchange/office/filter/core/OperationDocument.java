/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.office.tools.common.memory.MemoryObserver.MemoryListener;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.session.Session;

public abstract class OperationDocument implements AutoCloseable {

    final protected static Logger log = LoggerFactory.getLogger(OperationDocument.class);

    protected final IResourceManager      resourceManager;
    
    protected final Session               session;
    
    protected final DocumentProperties    documentProperties;
//    protected boolean                     useConfiguration = true;

    private MemoryObserver              memoryObserver = null;
    private MemoryListener              memoryListener = null;

    protected boolean                   createFinalDocument = false;
    
    @Autowired
    protected ConfigurationService configurationService;
    
    @Autowired
    protected ConfigurationHelper configurationHelper;

    protected OperationDocument(Session session, IResourceManager resourceManager, DocumentProperties documentProperties) {
        this.session = session;
        this.resourceManager = resourceManager;
        this.documentProperties = documentProperties!=null ? documentProperties : new DocumentProperties();

        registerMemoryListener();
    }

    public IResourceManager getResourceManager() {
        return resourceManager;
    }

    public DocumentProperties getDocumentProperties() {
        return documentProperties;
    }

    public String getUserLanguage() {
        return documentProperties.optString(DocumentProperties.PROP_USER_LANGUAGE, "");
    }

    public void registerShape2pngReplacementImage(String replacementUrl) {
        Map<String, String> replacementImages = (Map<String, String>) documentProperties.get(DocumentProperties.PROP_SHAPE2PNG_REPLACEMENT_IMAGES);
        if(replacementImages == null) {
            replacementImages = new HashMap<String, String>();
            documentProperties.put(DocumentProperties.PROP_SHAPE2PNG_REPLACEMENT_IMAGES, replacementImages);
        }
        final int pageIndex = replacementUrl.lastIndexOf(".p");
        if(pageIndex > -1) {
            final int hashIndex = replacementUrl.lastIndexOf(".h", pageIndex);
            if(hashIndex > -1) {
                replacementImages.put(replacementUrl.substring(hashIndex + 2, pageIndex), replacementUrl);
            }
        }
    }

    final public void setCreateFinalDocument(boolean createFinalDocument) {
        this.createFinalDocument = createFinalDocument;
    }

    final public void registerMemoryListener() {
        getMemoryObserver();
        if(memoryObserver!=null) {
            if(memoryListener==null) {
                memoryListener = new MemoryListener() {
                    @Override
                    public void memoryTresholdExceeded(long usedMemory, long maxMemory) {
                        notifyLowMemory();
                    }
                };
                memoryObserver.addListener(memoryListener);
            }
            if(memoryObserver.isUsageThresholdExceeded()) {
                notifyLowMemory();
            }
        }
    }

    private void removeMemoryListener() {
        if(memoryListener!=null && memoryObserver!=null) {
            memoryObserver.removeListener(memoryListener);
            memoryListener = null;
        }
        memoryObserver = null;
    }

    abstract protected void notifyLowMemory();

    private MemoryObserver getMemoryObserver () {
        if (memoryObserver == null) {
            memoryObserver = MemoryObserver.get();
        }
        return memoryObserver;
    }

    @Override
    public void close() {
        removeMemoryListener();
    }

    public String getOfficeConfigurationValue(String settingPath, String defaultValue) {
        return configurationHelper != null ? configurationHelper.getOfficeConfigurationValue(session, settingPath, defaultValue) : defaultValue;
    }

    public Integer getIntegerOfficeConfigurationValue(String settingPath, Integer defaultValue) {
        return configurationHelper != null ? configurationHelper.getIntegerOfficeConfigurationValue(session, settingPath, defaultValue) : defaultValue;
    }

    private Integer maxJsonSize = null;

    public int getMaxJsonSize() {
        if(maxJsonSize==null) {
        	maxJsonSize = configurationHelper != null ? configurationService.getIntProperty("com.openexchange.json.maxSize", 2500) : 2500;
            if(maxJsonSize==0) {
                maxJsonSize = Integer.MAX_VALUE;
            }
        }
        return maxJsonSize.intValue();
    }
}
