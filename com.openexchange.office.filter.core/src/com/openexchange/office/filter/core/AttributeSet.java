/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

public class AttributeSet {

    private final JSONObject attrs = new JSONObject();

    public AttributeSet setAttributes(JSONObject attrs) {
        mergeAttributes(this.attrs, attrs);
        return this;
    }

    public JSONObject getAttributes() {
        return attrs;
    }

    public JSONObject getOrCreate(final String key) {
        JSONObject result = attrs.optJSONObject(key);
        if (result == null) {
            result = new JSONObject();
            attrs.putSafe(key, result);
        }
        return result;
    }

    public static void mergeAttributes(JSONObject target, JSONObject attrs) {
        for (Entry<String, Object> entry : attrs.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof JSONObject) {
                if (target.optJSONObject(key)==null) {
                    target.putSafe(key, value);
                }
                else {
                    JSONObject as = target.optJSONObject(key);
                    if(as==null) {
                        as = new JSONObject();
                        target.putSafe(key, as);
                    }
                    mergeAttributes(as, (JSONObject) value);
                }
            }
            else {
                target.putSafe(key, value);
            }
        }
    }

    public static JSONObject getOrCreateJSONObject(final JSONObject o, final String key) {
        JSONObject v = o.optJSONObject(key);
        if(v == null) {
            v = new JSONObject();
            o.putSafe(key, v);
        }
        return v;
    }

    public static JSONArray getOrCreateJSONArray(final JSONObject o, final String key) {
        JSONArray v = o.optJSONArray(key);
        if(v == null) {
            v = new JSONArray();
            o.putSafe(key, v);
        }
        return v;
    }
}
