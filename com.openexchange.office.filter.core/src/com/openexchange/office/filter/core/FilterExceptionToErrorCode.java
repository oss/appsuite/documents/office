/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.event.Level;

import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.tools.common.error.ErrorCode;


/**
 * General mapping class to provide a suitable mapping between
 * FilterException and ErrorCode.
 *
 * @author Carsten Driesner
 * @since 7.6.0
 *
 */
public class FilterExceptionToErrorCode {

    /**
     * Maps a filter exception to a general ErrorCode object.
     *
     * @param e a filter exception
     * @param defErrorCode a default error code to be used if there is no
     *        mapping available.
     * @return an ErrorCode object setup with error information suitable for
     *         the provided filter exception.
     */
    static public ErrorCode map(final FilterException e, final ErrorCode defErrorCode) {
        return map(e, defErrorCode, false);
    }

    /**
     * Maps a filter exception to a general ErrorCode object.
     *
     * @param e a filter exception
     * @param defErrorCode a default error code to be used if there is no
     *        mapping available.
     * @param only exchange the default error code in case a specific error
     *        code could be mapped, otherwise use defErrorCode as result
     * @return an ErrorCode object setup with error information suitable for
     *         the provided filter exception.
     */
    static public ErrorCode map(final FilterException e, final ErrorCode defErrorCode, boolean forceDefErrorCode) {
        ErrorCode errorCode = defErrorCode;
        final String subErrorType = e.getSubType();

        // try to map a filter exception to our error code schema using possible
        // cause exception and subTypes
        switch (e.getErrorcode()) {
            case CRITICAL_ERROR:
                errorCode = (forceDefErrorCode) ? defErrorCode : ErrorCode.GENERAL_UNKNOWN_ERROR;
                if (FilterException.ST_MEDIATYPE_NOT_SUPPORTED.equals(subErrorType)) {
                    errorCode = ErrorCode.LOADODCUMENT_MEDIATYPE_NOT_SUPPORTED_ERROR;
                    errorCode.setValue((String)e.getValue());
                }
                break;
            case WRONG_PASSWORD: errorCode = ErrorCode.LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR; break;
            case UNSUPPORTED_ENCRYPTION_USED: errorCode = ErrorCode.LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR; break;
            case COMPLEXITY_TOO_HIGH:
                errorCode = ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR;
                if (FilterException.ST_MAX_SHEET_COUNT_EXCEEDED.equals(subErrorType)) {
                    errorCode = ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR;
                }
                break;
            case MEMORY_USAGE_TOO_HIGH: errorCode = ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR; break;
            case MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED: errorCode = ErrorCode.GENERAL_SYSTEM_BUSY_ERROR; break;
            case STRICT_OOXML_NOT_SUPPORTED: errorCode = ErrorCode.LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR; break;
            case FEATURE_NOT_SUPPORTED: {
                errorCode = ErrorCode.LOADDOCUMENT_FEATURE_NOT_SUPPORTED_ERROR;
                if (FilterException.ST_FRAME_ATTACHED_TO_FRAME_NOT_SUPPORTED.equals(subErrorType)) {
                    errorCode = ErrorCode.LOADDOCUMENT_FEATURE_NOT_SUPPORTED_FRAME_ATTACHED_TO_FRAME_ERROR;
                }
                break;
            }
            default: errorCode = defErrorCode; break;
        }

       return errorCode;
    }

    /**
     * Determines the LogLevel that should be used to output the
     * log message.
     *
     * @param e the filter exception caught and where the needed log-level
     *          should be determined
     * @return the log-level that should be used for logging this exception
     */
    static public Level determineLogLevel(final FilterException e) {
        Level logLevel;

        // filter exception should at most be logged on WARN level
        switch (e.getErrorcode()) {
            case CRITICAL_ERROR: logLevel = Level.WARN; break;
            case WRONG_PASSWORD: logLevel = Level.INFO; break;
            case UNSUPPORTED_ENCRYPTION_USED: logLevel = Level.INFO; break;
            case COMPLEXITY_TOO_HIGH: logLevel = Level.INFO; break;
            case MEMORY_USAGE_TOO_HIGH: logLevel = Level.WARN; break;
            case MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED: logLevel = Level.WARN; break;
            case STRICT_OOXML_NOT_SUPPORTED: logLevel = Level.INFO; break;
            case FEATURE_NOT_SUPPORTED: logLevel = Level.INFO; break;
            default: logLevel = Level.WARN; break;
        }

        return logLevel;
    }

    /**
     * Logs a filter exception message using the provided log-level.
     *
     * @param logger the logger to be used for the logging message.
     * @param level the log-level to be used for the logging message.
     * @param message the message to be logged.
     * @param e the filter exception
     */
    static public void log(final Logger logger, Level level, final String logText, final String context, FilterException e) {
        Validate.notNull(logger);

        // add filter exception type
        final StringBuffer out = new StringBuffer(logText);
        if(e.getErrorcode() == com.openexchange.office.filter.api.FilterException.ErrorCode.COMPLEXITY_TOO_HIGH) {
            e = null;   // we don't want a stack and we don't want the text "If you write an issue ..."
            out.append("Complexity of document is too hight.");
        } else {
            out.append(" Type: " + e.getErrorcode().toString());

            // add optional document context information with the appeal to attach a document to reproduce the issue
            if (StringUtils.isNotEmpty(context)) {
                out.append(". Document context: " + context + ". If you write an issue for this problem, please add the document to enable development to fix it.");
            }
        }

        final String message = out.toString();
        switch (level) {
        	case TRACE: logger.trace(message, e); break;
        	case DEBUG: logger.debug(message, e); break;
        	case WARN: logger.warn(message, e); break;
        	case ERROR: logger.error(message, e); break;
        	case INFO: logger.info(message, e); break;
        default: logger.warn(message, e); break;
        }
    }

    /**
     * Determine if a FilterException should be logged or not. There are a few
     * exceptional FilterExceptions which should be not logged as they don't
     * need the attention of a administrator.
     *
     * @param errorFromFilterException the error code mapped by a FilterException
     * @return TRUE if the exception should be logged and FALSE if not.
     */
    static public boolean shouldBeLogged(ErrorCode errorFromFilterException) {
        boolean result = true;
        switch (errorFromFilterException.getCode()) {
            case ErrorCode.CODE_LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR: { result = false; break; }
            default: break;
        }
        return result;
    }
}
