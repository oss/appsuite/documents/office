/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

import java.util.Map;
import com.google.common.collect.ImmutableMap;

// everything in this file has to be ascii encoded. a conversion and vice versa can be done with the tool native2ascii

final public class PresentationObjectTitles {

    static final public ImmutableMap<String, String> en = new ImmutableMap.Builder<String, String>()
        .put("title",    "Click to edit Master title style")
        .put("subTitle", "Click to edit Master subtitle style")
        .put("level1",   "Edit Master text styles")
        .put("level2",   "Second level")
        .put("level3",   "Third level")
        .put("level4",   "Fourth level")
        .put("level5",   "Fifth level")
        .build();

    static final ImmutableMap<String, String> ja = new ImmutableMap.Builder<String, String>()
        .put("title",    "\u30de\u30b9\u30bf\u30fc \u30bf\u30a4\u30c8\u30eb\u306e\u66f8\u5f0f\u8a2d\u5b9a")
        .put("subTitle", "\u30de\u30b9\u30bf\u30fc \u30b5\u30d6\u30bf\u30a4\u30c8\u30eb\u306e\u66f8\u5f0f\u8a2d\u5b9a")
        .put("level1",   "\u30de\u30b9\u30bf\u30fc \u30c6\u30ad\u30b9\u30c8\u306e\u66f8\u5f0f\u8a2d\u5b9a")
        .put("level2",   "\u7b2c 2 \u30ec\u30d9\u30eb")
        .put("level3",   "\u7b2c 3 \u30ec\u30d9\u30eb")
        .put("level4",   "\u7b2c 4 \u30ec\u30d9\u30eb")
        .put("level5",   "\u7b2c 5 \u30ec\u30d9\u30eb")
        .build();

    static final ImmutableMap<String, String> lv = new ImmutableMap.Builder<String, String>()
        .put("title",    "Redi\u0123\u0113t \u0161ablona virsraksta stilu")
        .put("subTitle", "Noklik\u0161\u0137iniet, lai redi\u0123\u0113tu \u0161ablona apak\u0161virsraksta stilu")
        .put("level1",   "Noklik\u0161\u0137iniet, lai redi\u0123\u0113tu \u0161ablona teksta stilus")
        .put("level2",   "Otrais l\u012bmenis")
        .put("level3",   "Tre\u0161ais l\u012bmenis")
        .put("level4",   "Ceturtais l\u012bmenis")
        .put("level5",   "Piektais l\u012bmenis")
        .build();

    static final ImmutableMap<String, String> nb = new ImmutableMap.Builder<String, String>()
        .put("title",    "Klikk for \u00e5 redigere tittelstil")
        .put("subTitle", "Klikk for \u00e5 redigere undertittelstil i malen")
        .put("level1",   "Klikk for \u00e5 redigere tekststiler i malen")
        .put("level2",   "Andre niv\u00e5")
        .put("level3",   "Tredje niv\u00e5")
        .put("level4",   "Fjerde niv\u00e5")
        .put("level5",   "Femte niv\u00e5")
        .build();

    static final ImmutableMap<String, String> nl = new ImmutableMap.Builder<String, String>()
        .put("title",    "Klik om de stijl te bewerken")
        .put("subTitle", "Klik om de ondertitelstijl van het model te bewerken")
        .put("level1",   "Klik om de modelstijlen te bewerken")
        .put("level2",   "Tweede niveau")
        .put("level3",   "Derde niveau")
        .put("level4",   "Vierde niveau")
        .put("level5",   "Vijfde niveau")
        .build();

    static final ImmutableMap<String, String> pl = new ImmutableMap.Builder<String, String>()
        .put("title",    "Kliknij, aby edytowa\u0107 styl")
        .put("subTitle", "Kliknij, aby edytowa\u0107 styl wzorca podtytu\u0142u")
        .put("level1",   "Kliknij, aby edytowa\u0107 style wzorca tekstu")
        .put("level2",   "Drugi poziom")
        .put("level3",   "Trzeci poziom")
        .put("level4",   "Czwarty poziom")
        .put("level5",   "Pi\u0105ty poziom")
        .build();

    static final ImmutableMap<String, String> pt = new ImmutableMap.Builder<String, String>()
        .put("title",    "Clique para editar o estilo")
        .put("subTitle", "Clique para editar o estilo do subt\u00edtulo do Modelo Global")
        .put("level1",   "Clique para editar os estilos")
        .put("level2",   "Segundo n\u00edvel")
        .put("level3",   "Terceiro n\u00edvel")
        .put("level4",   "Quarto n\u00edvel")
        .put("level5",   "Quinto n\u00edvel")
        .build();

    static final ImmutableMap<String, String> ptBR = new ImmutableMap.Builder<String, String>()
        .put("title",    "Clique para editar o t\u00edtulo mestre")
        .put("subTitle", "Clique para editar o estilo do subt\u00edtulo Mestre")
        .put("level1",   "Clique para editar os estilos do texto mestre")
        .put("level2",   "Segundo n\u00edvel")
        .put("level3",   "Terceiro n\u00edvel")
        .put("level4",   "Quarto n\u00edvel")
        .put("level5",   "Quinto n\u00edvel")
        .build();

    static final ImmutableMap<String, String> ro = new ImmutableMap.Builder<String, String>()
        .put("title",    "Clic pentru editare stil titlu")
        .put("subTitle", "Face\u021bi clic pentru a edita stilul de subtitlu coordonator")
        .put("level1",   "Face\u021bi clic pentru a edita stilurile de text Coordonator")
        .put("level2",   "Al doilea nivel")
        .put("level3",   "Al treilea nivel")
        .put("level4",   "Al patrulea nivel")
        .put("level5",   "Al cincilea nivel")
        .build();

    static final ImmutableMap<String, String> ru = new ImmutableMap.Builder<String, String>()
        .put("title",    "\u041e\u0431\u0440\u0430\u0437\u0435\u0446 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u0430")
        .put("subTitle", "\u041e\u0431\u0440\u0430\u0437\u0435\u0446 \u043f\u043e\u0434\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u0430")
        .put("level1",   "\u041e\u0431\u0440\u0430\u0437\u0435\u0446 \u0442\u0435\u043a\u0441\u0442\u0430")
        .put("level2",   "\u0412\u0442\u043e\u0440\u043e\u0439 \u0443\u0440\u043e\u0432\u0435\u043d\u044c")
        .put("level3",   "\u0422\u0440\u0435\u0442\u0438\u0439 \u0443\u0440\u043e\u0432\u0435\u043d\u044c")
        .put("level4",   "\u0427\u0435\u0442\u0432\u0435\u0440\u0442\u044b\u0439 \u0443\u0440\u043e\u0432\u0435\u043d\u044c")
        .put("level5",   "\u041f\u044f\u0442\u044b\u0439 \u0443\u0440\u043e\u0432\u0435\u043d\u044c")
        .build();

    static final ImmutableMap<String, String> sk = new ImmutableMap.Builder<String, String>()
        .put("title",    "Upravte \u0161t\u00fdly predlohy textu")
        .put("subTitle", "Kliknut\u00edm upravte \u0161t\u00fdl predlohy podnadpisov")
        .put("level1",   "Kliknite sem a upravte \u0161t\u00fdly predlohy textu.")
        .put("level2",   "Druh\u00e1 \u00farove\u0148")
        .put("level3",   "Tretia \u00farove\u0148")
        .put("level4",   "\u0160tvrt\u00e1 \u00farove\u0148")
        .put("level5",   "Piata \u00farove\u0148")
        .build();

    static final ImmutableMap<String, String> sv = new ImmutableMap.Builder<String, String>()
        .put("title",    "Klicka h\u00e4r f\u00f6r att \u00e4ndra format")
        .put("subTitle", "Klicka om du vill redigera mall f\u00f6r underrubrikformat")
        .put("level1",   "Klicka h\u00e4r f\u00f6r att \u00e4ndra format p\u00e5 bakgrundstexten")
        .put("level2",   "Niv\u00e5 tv\u00e5")
        .put("level3",   "Niv\u00e5 tre")
        .put("level4",   "Niv\u00e5 fyra")
        .put("level5",   "Niv\u00e5 fem")
        .build();

    static final ImmutableMap<String, String> zhCN = new ImmutableMap.Builder<String, String>()
        .put("title",    "\u5355\u51fb\u6b64\u5904\u7f16\u8f91\u6bcd\u7248\u6807\u9898\u6837\u5f0f")
        .put("subTitle", "\u5355\u51fb\u4ee5\u7f16\u8f91\u6bcd\u7248\u526f\u6807\u9898\u6837\u5f0f")
        .put("level1",   "\u5355\u51fb\u6b64\u5904\u7f16\u8f91\u6bcd\u7248\u6587\u672c\u6837\u5f0f")
        .put("level2",   "\u7b2c\u4e8c\u7ea7")
        .put("level3",   "\u7b2c\u4e09\u7ea7")
        .put("level4",   "\u7b2c\u56db\u7ea7")
        .put("level5",   "\u7b2c\u4e94\u7ea7")
        .build();

    static final ImmutableMap<String, String> zhTW = new ImmutableMap.Builder<String, String>()
        .put("title",    "\u6309\u4e00\u4e0b\u4ee5\u7de8\u8f2f\u6bcd\u7247\u6a19\u984c\u6a23\u5f0f")
        .put("subTitle", "\u6309\u4e00\u4e0b\u4ee5\u7de8\u8f2f\u6bcd\u7247\u526f\u6a19\u984c\u6a23\u5f0f")
        .put("level1",   "\u6309\u4e00\u4e0b\u4ee5\u7de8\u8f2f\u6bcd\u7247\u6587\u5b57\u6a23\u5f0f")
        .put("level2",   "\u7b2c\u4e8c\u5c64")
        .put("level3",   "\u7b2c\u4e09\u5c64")
        .put("level4",   "\u7b2c\u56db\u5c64")
        .put("level5",   "\u7b2c\u4e94\u5c64")
        .build();

    static final ImmutableMap<String, String> ca = new ImmutableMap.Builder<String, String>()
        .put("title",    "Feu clic aqu\u00ed per editar l'estil")
        .put("subTitle", "Feu clic aqu\u00ed per editar l'estil de subt\u00edtols del patr\u00f3")
        .put("level1",   "Feu clic aqu\u00ed per editar els estils de text")
        .put("level2",   "Segon nivell")
        .put("level3",   "Tercer nivell")
        .put("level4",   "Quart nivell")
        .put("level5",   "Cinqu\u00e8 nivell")
        .build();

    static final ImmutableMap<String, String> cs = new ImmutableMap.Builder<String, String>()
        .put("title",    "Kliknut\u00edm lze upravit styl.")
        .put("subTitle", "Kliknut\u00edm m\u016f\u017eete upravit styl p\u0159edlohy.")
        .put("level1",   "Klepnut\u00edm lze upravit styly p\u0159edlohy textu.")
        .put("level2",   "Druh\u00e1 \u00farove\u0148")
        .put("level3",   "T\u0159et\u00ed \u00farove\u0148")
        .put("level4",   "\u010ctvrt\u00e1 \u00farove\u0148")
        .put("level5",   "P\u00e1t\u00e1 \u00farove\u0148")
        .build();

    static final ImmutableMap<String, String> da = new ImmutableMap.Builder<String, String>()
        .put("title",    "Klik for at redigere i master")
        .put("subTitle", "Klik for at redigere undertiteltypografien i masteren")
        .put("level1",   "Klik for at redigere typografi i masteren")
        .put("level2",   "Andet niveau")
        .put("level3",   "Tredje niveau")
        .put("level4",   "Fjerde niveau")
        .put("level5",   "Femte niveau")
        .build();

    static final ImmutableMap<String, String> de = new ImmutableMap.Builder<String, String>()
        .put("title",    "Titelmastertitelformat durch Klicken bearbeiten")
        .put("subTitle", "Formatvorlage des Untertitelmasters durch Klicken bearbeiten")
        .put("level1",   "Mastertextformat bearbeiten")
        .put("level2",   "Zweite Ebene")
        .put("level3",   "Dritte Ebene")
        .put("level4",   "Vierte Ebene")
        .put("level5",   "F\u00fcnfte Ebene")
        .build();

    static final ImmutableMap<String, String> el = new ImmutableMap.Builder<String, String>()
        .put("title",    "\u039a\u03ac\u03bd\u03c4\u03b5 \u03ba\u03bb\u03b9\u03ba \u03b3\u03b9\u03b1 \u03bd\u03b1 \u03b5\u03c0\u03b5\u03be\u03b5\u03c1\u03b3\u03b1\u03c3\u03c4\u03b5\u03af\u03c4\u03b5 \u03c4\u03bf\u03bd \u03c4\u03af\u03c4\u03bb\u03bf \u03c5\u03c0\u03bf\u03b4\u03b5\u03af\u03b3\u03bc\u03b1\u03c4\u03bf\u03c2")
        .put("subTitle", "\u039a\u03ac\u03bd\u03c4\u03b5 \u03ba\u03bb\u03b9\u03ba \u03b3\u03b9\u03b1 \u03bd\u03b1 \u03b5\u03c0\u03b5\u03be\u03b5\u03c1\u03b3\u03b1\u03c3\u03c4\u03b5\u03af\u03c4\u03b5 \u03c4\u03bf\u03bd \u03c5\u03c0\u03cc\u03c4\u03b9\u03c4\u03bb\u03bf \u03c4\u03bf\u03c5 \u03c5\u03c0\u03bf\u03b4\u03b5\u03af\u03b3\u03bc\u03b1\u03c4\u03bf\u03c2")
        .put("level1",   "\u03a3\u03c4\u03c5\u03bb \u03ba\u03b5\u03b9\u03bc\u03ad\u03bd\u03bf\u03c5 \u03c5\u03c0\u03bf\u03b4\u03b5\u03af\u03b3\u03bc\u03b1\u03c4\u03bf\u03c2")
        .put("level2",   "\u0394\u03b5\u03cd\u03c4\u03b5\u03c1\u03bf \u03b5\u03c0\u03af\u03c0\u03b5\u03b4\u03bf")
        .put("level3",   "\u03a4\u03c1\u03af\u03c4\u03bf \u03b5\u03c0\u03af\u03c0\u03b5\u03b4\u03bf")
        .put("level4",   "\u03a4\u03ad\u03c4\u03b1\u03c1\u03c4\u03bf \u03b5\u03c0\u03af\u03c0\u03b5\u03b4\u03bf")
        .put("level5",   "\u03a0\u03ad\u03bc\u03c0\u03c4\u03bf \u03b5\u03c0\u03af\u03c0\u03b5\u03b4\u03bf")
        .build();

    static final ImmutableMap<String, String> es = new ImmutableMap.Builder<String, String>()
        .put("title",    "Haga clic para modificar el estilo de t\u00edtulo del patr\u00f3n")
        .put("subTitle", "Haga clic para editar el estilo de subt\u00edtulo del patr\u00f3n")
        .put("level1",   "Haga clic para modificar el estilo de texto del patr\u00f3n")
        .put("level2",   "Segundo nivel")
        .put("level3",   "Tercer nivel")
        .put("level4",   "Cuarto nivel")
        .put("level5",   "Quinto nivel")
        .build();

    static final ImmutableMap<String, String> fi = new ImmutableMap.Builder<String, String>()
        .put("title",    "Muokkaa perustyyl. napsautt.")
        .put("subTitle", "Muokkaa alaotsikon perustyyli\u00e4 napsautt.")
        .put("level1",   "Muokkaa tekstin perustyylej\u00e4 napsauttamalla")
        .put("level2",   "toinen taso")
        .put("level3",   "kolmas taso")
        .put("level4",   "nelj\u00e4s taso")
        .put("level5",   "viides taso")
        .build();

    static final ImmutableMap<String, String> fr = new ImmutableMap.Builder<String, String>()
        .put("title",    "Modifiez le style du titre")
        .put("subTitle", "Modifier le style des sous-titres du masque")
        .put("level1",   "Cliquez pour modifier les styles du texte du masque")
        .put("level2",   "Deuxi\u00e8me niveau")
        .put("level3",   "Troisi\u00e8me niveau")
        .put("level4",   "Quatri\u00e8me niveau")
        .put("level5",   "Cinqui\u00e8me niveau")
        .build();

    static final ImmutableMap<String, String> hu = new ImmutableMap.Builder<String, String>()
        .put("title",    "Mintac\u00edm szerkeszt\u00e9se")
        .put("subTitle", "Kattintson ide az alc\u00edm mint\u00e1j\u00e1nak szerkeszt\u00e9s\u00e9hez")
        .put("level1",   "Mintasz\u00f6veg szerkeszt\u00e9se")
        .put("level2",   "M\u00e1sodik szint")
        .put("level3",   "Harmadik szint")
        .put("level4",   "Negyedik szint")
        .put("level5",   "\u00d6t\u00f6dik szint")
        .build();

    static final ImmutableMap<String, String> it = new ImmutableMap.Builder<String, String>()
        .put("title",    "Fare clic per modificare lo stile del titolo")
        .put("subTitle", "Fai clic per modificare lo stile del sottotitolo dello schema")
        .put("level1",   "Fare clic per modificare stili del testo dello schema")
        .put("level2",   "Secondo livello")
        .put("level3",   "Terzo livello")
        .put("level4",   "Quarto livello")
        .put("level5",   "Quinto livello")
        .build();

    static final ImmutableMap<String, String> tr = new ImmutableMap.Builder<String, String>()
        .put("title",    "Ba\u015fl\u0131k eklemek i\u00e7in t\u0131klay\u0131n")
        .put("subTitle", "Alt ba\u015fl\u0131k eklemek i\u00e7in t\u0131klay\u0131n")
        .put("level1",   "As\u0131l metin stillerini d\u00fczenle")
        .put("level2",   "\u0130kinci seviye")
        .put("level3",   "\u00dc\u00e7\u00fcnc\u00fc seviye")
        .put("level4",   "D\u00f6rd\u00fcnc\u00fc seviye")
        .put("level5",   "Be\u015finci seviye")
        .build();

    static private Map<String, ImmutableMap<String, String>> languageToMap = null;

    private static Map<String, ImmutableMap<String, String>> getLanguageToMap() {
        if(languageToMap==null) {
            languageToMap = ImmutableMap.<String, ImmutableMap<String, String>> builder()
                .put("en",    en)
                .put("en-gb", en)
                .put("en-us", en)

                .put("ja",    ja)
                .put("ja-jp", ja)

                .put("lv",    lv)
                .put("lv-lv", lv)

                .put("nb",    nb)   /* Bokm\u00e5l, Norway (NO) */
                .put("no",    nb)
                .put("no-no", nb)

                .put("nl",    nl)
                .put("nl-nl", nl)
                .put("nl-br", nl)

                .put("pl",    pl)
                .put("pl-pl", pl)

                .put("pt",    pt)
                .put("pt-pt", pt)
                .put("pt-br", ptBR)

                .put("ro",    ro)
                .put("ro-ro", ro)

                .put("ru",    ru)
                .put("ru-ru", ru)

                .put("sk",    sk)
                .put("sk-sk", sk)

                .put("sv",    sv)
                .put("sv-se", sv)
                .put("sv-fi", sv)

                .put("zh",    zhCN)
                .put("zh-hk", zhCN)
                .put("zh-tw", zhTW)

                .put("ca",    ca)
                .put("ca-es", ca)

                .put("cs",    cs)
                .put("cs-cz", cs)

                .put("da",    da)
                .put("da-dk", da)

                .put("de",    de)
                .put("de-de", de)
                .put("de-at", de)
                .put("de-ch", de)
                .put("de-li", de)
                .put("de-lu", de)

                .put("el",    el)
                .put("el-GR", el)

                .put("es",    es)
                .put("es-es", es)

                .put("fi",    fi)
                .put("fi-fi", fi)

                .put("fr",    fr)
                .put("fr-fr", fr)
                .put("fr-be", fr)
                .put("fr-ca", fr)

                .put("hu",    hu)
                .put("hu-hu", hu)

                .put("it",    it)
                .put("it-it", it)

                .put("tr",    tr)
                .put("tr-tr", tr)
                .build();
        }
        return languageToMap;
    }

    public static ImmutableMap<String, String> getLanguageToMap(Map<String, ImmutableMap<String, String>> languages, String lang) {
        final StringBuffer langStringBuf = new StringBuffer(6);
        langStringBuf.append(lang.toLowerCase());
        if(langStringBuf.length()>2) {
            langStringBuf.setCharAt(2, '-');
        }
        ImmutableMap<String, String> languageMap = languages.get(langStringBuf.toString());
        if(languageMap==null&&lang.length()>2) {
            languageMap = languages.get(langStringBuf.substring(0, 2));
        }
        if(languageMap==null) {
            languageMap = languages.get("en");
        }
        return languageMap;
    }

    /*
     * translationKey can be: "title", "subTitle", "level1", "level2", "level3", "level4" or "level5"
     *
     */
    public static String getTranslation(String translationKey, String lang, String defaultValue) {
        if(translationKey==null) {
            return defaultValue;
        }
        return getLanguageToMap(getLanguageToMap(), lang).get(translationKey);
    }
}
