/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

public interface IChildNodeCombiner {

    // returns true if the next node could be combined, the next node will be removed then
    boolean tryCombineWithNextNode(INodeAccessor<Object> parent, DLNode<Object> nextNode);

    public static void optimize(INodeAccessor<Object> parent) {
        DLNode<Object> node = parent.getContent().getFirstNode();
        while((node = tryRemoveIgnorables(parent.getContent(), node)) != null) {
            while(node.getData() instanceof IChildNodeCombiner && ((IChildNodeCombiner)node.getData()).tryCombineWithNextNode(parent, tryRemoveIgnorables(parent.getContent(), node.getNext()))) {
                // if the node could be combined with the next node we try to combine again
            }
            node = node.getNext();
        }
    }

    // returns the first node that is not to be ignored or null if there is no further node
    public static DLNode<Object> tryRemoveIgnorables(DLList<Object> content, DLNode<Object> node) {
        while(node!=null && node.getData() instanceof IIgnore) {
            node = content.removeNode(node).next;
        }
        return node;
    }
}
