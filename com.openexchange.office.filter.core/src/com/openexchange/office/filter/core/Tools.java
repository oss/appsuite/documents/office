/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import com.openexchange.imagetransformation.ImageInformation;
import com.openexchange.java.BoolReference;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.tools.images.ImageTransformationUtility;

public class Tools {

    public static JSONObject writeFilterVersion(JSONObject attrs) throws JSONException {
        JSONObject documentAttrs = attrs.optJSONObject(OCKey.DOCUMENT.value());
        if(documentAttrs == null) {
            documentAttrs = new JSONObject();
            attrs.put(OCKey.DOCUMENT.value(), documentAttrs);
        }
        return documentAttrs.put(OCKey.FV.value(), currentVersion);
    }

    public static int currentVersion = 2;

    public static double[] rotatePoint(double ptX, double ptY, double refX, double refY, double rotation) {
        ptX -= refX;
        ptY -= refY;
        double x = ptX;
        double y = ptY;
        double a = (Math.PI * rotation) / 180.0;
        double s = Math.sin(a);
        double c = Math.cos(a);
        ptX = x * c - y * s;
        ptY = x * s + y * c;
        ptX += refX;
        ptY += refY;
        double[] pts = new double[2];
        pts[0] = ptX;
        pts[1] = ptY;
        return pts;
    }

    public static Pair<Integer, Integer> getPixelSizeFromImage(InputStream inputStream) {

        Pair<Integer, Integer> pixelSize = null;
        BufferedInputStream bufferedInputStream = null;

        try {
            if(inputStream!=null) {
                bufferedInputStream = new BufferedInputStream(inputStream);
                final ImageInformation imageInformation = ImageTransformationUtility.readImageInformation(bufferedInputStream, new BoolReference(false));
                if(imageInformation!=null) {
                    pixelSize = Pair.of(Integer.valueOf(imageInformation.width), Integer.valueOf(imageInformation.height));
                }
                else {
                    bufferedInputStream.reset();
                    final BufferedImage image = ImageIO.read(bufferedInputStream);
                    if(image!=null) {
                        pixelSize = Pair.of(Integer.valueOf(image.getWidth()), Integer.valueOf(image.getHeight()));
                    }
                }
            }
        }
        catch(IOException e) {
            //
        }
        IOUtils.closeQuietly(bufferedInputStream);
        IOUtils.closeQuietly(inputStream);
        return pixelSize;
    }

    public static Pair<Double, Double> getDPIFromImage(InputStream is) {

        Double xDPI = null;
        Double yDPI = null;

        try {

            final ImageInputStream imageInput = ImageIO.createImageInputStream(is);
            if(imageInput!=null) {
                final Iterator<ImageReader> it = ImageIO.getImageReaders(imageInput);
                if (it.hasNext()) {


                    final ImageReader reader = it.next();
                    reader.setInput(imageInput);
                    final IIOMetadata meta = reader.getImageMetadata(0);
                    Node n = meta.getAsTree("javax_imageio_1.0").getFirstChild();
                    while (n!=null) {
                        final String nodeName = n.getNodeName();
                        if (nodeName.equals("Dimension")) {
                            Node n2 = n.getFirstChild();
                            while (n2!=null) {
                                final String n2Name = n2.getNodeName();
                                if (n2Name.equals("HorizontalPixelSize")) {
                                    final NamedNodeMap nnm = n2.getAttributes();
                                    final Node n3 = nnm.item(0);
                                    xDPI = 25.4d / Double.parseDouble(n3.getNodeValue());
                                }
                                else if (n2.equals("VerticalPixelSize")) {
                                    final NamedNodeMap nnm = n2.getAttributes();
                                    final Node n3 = nnm.item(0);
                                    yDPI = 25.4d / Double.parseDouble(n3.getNodeValue());
                                }
                                n2 = n2.getNextSibling();
                            }
                        }
                        n = n.getNextSibling();
                    }
                }
                imageInput.close();
            }
        }
        catch(Exception e) {
            //
        };
        return xDPI == null ? null : Pair.of(xDPI, yDPI==null ? xDPI : yDPI);

    }

    /*
     * If available this method returns the original size of the given graphic in 1/100thmm
     */

    public static Pair<Double, Double> getOriginalSizeFromImage(InputStream is) {

        Double originalWidth = null;
        Double originalHeight = null;

        try {

            final ImageInputStream imageInput = ImageIO.createImageInputStream(is);
            if(imageInput!=null) {
                final Iterator<ImageReader> it = ImageIO.getImageReaders(imageInput);
                if (it.hasNext()) {

                    Integer xDPI = null;
                    Integer yDPI = null;

                    final ImageReader reader = it.next();
                    reader.setInput(imageInput);
                    final IIOMetadata meta = reader.getImageMetadata(0);
                    Node n = meta.getAsTree("javax_imageio_1.0").getFirstChild();
                    while (n!=null) {
                        final String nodeName = n.getNodeName();
                        if (nodeName.equals("Dimension")) {
                            Node n2 = n.getFirstChild();
                            while (n2!=null) {
                                final String n2Name = n2.getNodeName();
                                if (n2Name.equals("HorizontalPixelSize")) {
                                    final NamedNodeMap nnm = n2.getAttributes();
                                    final Node n3 = nnm.item(0);
                                    xDPI = Math.round(25.4f / Float.parseFloat(n3.getNodeValue()));
                                }
                                else if (n2.equals("VerticalPixelSize")) {
                                    final NamedNodeMap nnm = n2.getAttributes();
                                    final Node n3 = nnm.item(0);
                                    yDPI = Math.round(25.4f / Float.parseFloat(n3.getNodeValue()));
                                }
                                n2 = n2.getNextSibling();
                            }
                        }
                        n = n.getNextSibling();
                    }
                    final double dpi = (xDPI!=null&&yDPI!=null&&xDPI == yDPI) ? xDPI.doubleValue() : 96.0;

                    originalWidth = Double.valueOf(reader.getWidth(0) * 2540d / dpi);
                    originalHeight = Double.valueOf(reader.getHeight(0) * 2540d / dpi);
                }
                imageInput.close();
            }
        }
        catch(Exception e) {
            //
        };
        return originalWidth!=null&&originalHeight!=null ? Pair.of(originalWidth, originalHeight) : null;
    }

    static public void addFamilyAttribute(JSONObject attrs, OCKey familyName, OCKey attributeName, Object o) {
        JSONObject dest = attrs.optJSONObject(familyName.value());
        try {
            if(dest == null) {
                dest = new JSONObject();
                attrs.put(familyName.value(), dest);
            }
            dest.put(attributeName.value(), o);
        } catch (JSONException e) {
            //
        }
    }

    public static int junitTestParam = 0;
}
