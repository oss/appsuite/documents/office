/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.component;

import org.json.JSONArray;
import org.json.JSONException;
import com.openexchange.office.filter.core.SplitMode;

public class CUtil {

    public static int getRootPoints(JSONArray pos1, JSONArray pos2) throws JSONException {
        int i, iMax = Math.max(pos1.length(), pos2.length());
        for(i=0; i<iMax; i++) {
            if(pos1.getInt(i)!=pos2.getInt(i)) {
                break;
            }
        }
        return i;
    }

    public static boolean sameParentComponent(JSONArray pos1, JSONArray pos2) throws JSONException {
        final int l1 = pos1.length();
        final int l2 = pos2.length();
        if(l1!=l2) {
            return false;
        }
        for(int i=0; i<l1-1; i++) {
            if(pos1.getInt(i)!=pos2.getInt(i)) {
                return false;
            }
        }
        return true;
    }
    /*
     * compares two component positions
     *
     * returns a value < 0 if pos1 is less than pos2
     *                   0 if pos1 is == pos2
     *                 > 0 if pos1 is greater than pos2
     *
     */
    public static int compare(JSONArray pos1, JSONArray pos2) throws JSONException {
        final int iMax = Math.max(pos1.length(), pos2.length());
        for(int i=0; i<iMax; i++) {
            final int i1 = pos1.getInt(i);
            final int i2 = pos2.getInt(i);
            if(i1==i2) {
                continue;
            }
            return i1-i2;
        }
        return pos1.length() - pos2.length();
    }

    public static JSONArray clone(JSONArray source, int arrayLength) throws JSONException {
        final JSONArray c = new JSONArray(arrayLength);
        for(int i=0; i<arrayLength; i++) {
            c.put(i, source.getInt(i));
        }
        return c;
    }

    public static int getMaxComponentIndex(IComponent<?> c) {
        int cMax = 0;
        while(c!=null) {
            cMax = c.getNextComponentNumber()-1;
            c = c.getNextComponent();
        }
        return cMax;
    }

    public static void delete(IComponent<?> rootComponent, JSONArray start, JSONArray end) throws JSONException {

        if(end==null) {
            end = start;
        }
        final int rootPoints = getRootPoints(start, end);

        // 0 to c
        for(int i=end.length(); i > rootPoints + 1; i--) {
            JSONArray s = clone(end, i);
            JSONArray e = clone(end, i);
            s.put(i-1, 0);
            if(i!=end.length()) {
                int a = e.getInt(i - 1) - 1;
                if(a < 0) {
                    continue;
                }
                e.put(i - 1, a);
            }
            _delete(rootComponent, s, e);
        }

        //
        if(start.length()>rootPoints && end.length()>rootPoints) {
            int s0 = start.getInt(rootPoints);
            if(start.length()>rootPoints+1) {
                s0++;
            }
            int e0 = end.getInt(rootPoints);
            if(end.length()>rootPoints+1) {
                e0--;
            }
            int c = (e0-s0)+1;
            if(c > 0)  {
                JSONArray s = clone(start, rootPoints+1);
                JSONArray e = clone(start, rootPoints+1);
                s.put(rootPoints, s0);
                e.put(rootPoints, e0);
                _delete(rootComponent, s, e);
            }
        }
        else if((start.length()==rootPoints) && (end.length()==rootPoints)) {
            _delete(rootComponent, start, end);
        }

        // c to end
        for(int i=start.length(); i > rootPoints + 1; i--) {
            JSONArray s = clone(start, i);
            JSONArray e = clone(start, i);

            final IComponent<?> c1 = rootComponent.getComponent(start, i);
            int cMax = getMaxComponentIndex(c1);
            e.put(i-1, cMax);
            if(i!=start.length()) {
                int a = s.getInt(i - 1) + 1;
                if(a > cMax) {
                    continue;
                }
                s.put(i - 1, a);
            }
            _delete(rootComponent, s, e);
        }
    }

    private static void _delete(IComponent<?> rootComponent, JSONArray start, JSONArray end)
        throws JSONException {

        if(start==null||start.length()==0)
            return;

        int startComponent = start.getInt(start.length()-1);
        int endComponent = startComponent;
        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endComponent = end.getInt(end.length()-1);
        }
        final IComponent<?> component = rootComponent.getComponent(start, start.length());
        component.splitStart(startComponent, SplitMode.DELETE);
        component.delete((endComponent-startComponent)+1);
    }

    public static String componentToString(IComponent<?> c) {
        final StringBuffer stringBuffer = new StringBuffer();
        componentToString(c, stringBuffer);
        return stringBuffer.toString();
    }

    private static void componentToString(IComponent<?> c, StringBuffer stringBuffer) {
        stringBuffer.append(c.simpleName());
        IComponent<?> childComponent = c.getChildComponent(0);
        if(childComponent!=null) {
            stringBuffer.append('(');
            while(childComponent!=null) {
                componentToString(childComponent, stringBuffer);
                childComponent = childComponent.getNextComponent();
                if(childComponent!=null) {
                    stringBuffer.append(',');
                }
            }
            stringBuffer.append(')');
        }
    }
}
