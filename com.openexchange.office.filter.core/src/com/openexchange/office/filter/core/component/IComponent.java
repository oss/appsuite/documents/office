/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.component;

import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.OperationDocument;

public interface IComponent<OpDoc extends OperationDocument> extends IComponentContext<OpDoc> {

    public Object getObject();

    public int getComponentNumber();

    public void setComponentNumber(int componentNumber);

    public int getNextComponentNumber();

    public ComponentContext<OpDoc> getContextChild(Set<Class<?>> parentContextList);

    public IComponent<OpDoc> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception;

    public IComponent<OpDoc> insertChildComponent(ComponentContext<OpDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OpDoc> child, ComponentType type, JSONObject attrs) throws Exception;

    public void applyAttrsFromJSON(JSONObject attrs) throws Exception;

    public IComponent<OpDoc> getNextComponent();

    public IComponent<OpDoc> getComponent(int positionCount);

    public IComponent<OpDoc> getComponent(JSONArray position, int positionCount);

    public IComponent<OpDoc> getParentComponent();

    public IComponent<OpDoc> getChildComponent(int number);

    public void delete(int count);

    public abstract String simpleName();
}
