/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.component;

import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.OperationDocument;
import com.openexchange.office.filter.core.SplitMode;

public abstract class ComponentContext<OpDoc extends OperationDocument> implements IComponentContext<OpDoc> {

    final protected OpDoc operationDocument;
    private ComponentContext<OpDoc> parentContext;
    private DLNode<Object> node;

    public ComponentContext(OpDoc operationDocument, DLNode<Object> n) {
        this.operationDocument = operationDocument;
        parentContext = null;
        node = n;
    }

    public ComponentContext(ComponentContext<OpDoc> p, DLNode<Object> n) {
        operationDocument = p.operationDocument;
        parentContext = p;
        node = n;
    }

    public Object getObject() {
        return getNode().getData();
    }

    @Override
    public OpDoc getOperationDocument() {
        return operationDocument;
    }

    @Override
    public ComponentContext<OpDoc> getParentContext() {
        return parentContext;
    }

    public void setParentContext(ComponentContext<OpDoc> p) {
        parentContext = p;
    }

    @Override
    public DLNode<Object> getNode() {
        return node;
    }

    public void setNode(DLNode<Object> n) {
        node = n;
    }

    @Override
    public void splitStart(int n, SplitMode splitMode) {
        //
    }

    @Override
    public void splitEnd(int n, SplitMode splitMode) {
        //
    }

    @Override
    public abstract IComponent<OpDoc> getNextChildComponent(ComponentContext<OpDoc> previousChildContext, IComponent<OpDoc> previousChildComponent);

    public ComponentContext<OpDoc> getRootContext() {
        ComponentContext<OpDoc> rootContext;
        ComponentContext<OpDoc> parent = this;
        do {
            rootContext = parent;
            parent = parent.getParentContext();
        }
        while(parent!=null);

        return rootContext;
    }
}
