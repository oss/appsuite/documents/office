/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.component;

public enum ComponentType {
    PARAGRAPH,
    TABLE,
    TR,
    TC,
    TAB,
    BOOKMARK_START,
    BOOKMARK_END,
    COMMENT_REFERENCE,
    COMMENT_RANGE_START,
    COMMENT_RANGE_END,
    SIMPLE_FIELD,
    COMPLEX_FIELD_START,
    COMPLEX_FIELD_SEPARATE,
    COMPLEX_FIELD_END,
    HARDBREAK_DEFAULT,
    HARDBREAK_PAGE,
    HARDBREAK_COLUMN,
    AC_SHAPE,
    AC_GROUP,
    AC_CHART,
    AC_CONNECTOR,
    AC_IMAGE,
    AC_COMMENT,
    AC_FRAME,
    HARDBREAK,
    FIELD
}
