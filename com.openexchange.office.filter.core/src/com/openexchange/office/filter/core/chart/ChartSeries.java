/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.filter.core.chart;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.AttributeSet;

public class ChartSeries extends AttributeSet {

    private JSONArray dataPoints = new JSONArray();
    private boolean   hiddenMarker;

    public ChartSeries() {
        getSeriesJSON().putSafe(OCKey.DATA_POINTS.value(), dataPoints);
    }

    public void setSeriesAttributes(String type, String values, String title, int axisXIndex, int axisYIndex, Integer axisZIndex,  boolean chart3d) {

        JSONObject series = getSeriesJSON();
        series.putSafe(OCKey.TYPE.value(), type);
        series.putSafe(OCKey.VALUES.value(), values);
        series.putSafe(OCKey.TITLE.value(), title);
        series.putSafe(OCKey.AXIS_X_INDEX.value(), axisXIndex);
        series.putSafe(OCKey.AXIS_Y_INDEX.value(), axisYIndex);
        series.putSafe(OCKey.CHART3D.value(), chart3d);
        if (axisZIndex != null) {
            series.putSafe(OCKey.AXIS_Z_INDEX.value(), axisZIndex);
        }
    }

    public void createInsertOperation(List<Integer> start, JSONArray operationQueue, int seriesIndex) throws JSONException {
        JSONObject op = new JSONObject();

        op.put(OCKey.START.value(), start);
        op.putSafe(OCKey.SERIES.value(), seriesIndex);

        JSONObject attrs = getAttributes();
        if (hiddenMarker) {
            // workaround for Bug 51731
            try {
                JSONObject fillNone = new JSONObject(new JSONTokener("{" + OCKey.FILL.value() + ":{" + OCKey.TYPE.value() + ":'none'}," + OCKey.LINE.value() + ":{" + OCKey.TYPE.value() + ":'solid'}}"));
                for (int i = 0; i < dataPoints.length(); i++) {
                    JSONObject dp = dataPoints.optJSONObject(i);
                    if (dp == null) {
                        dp = fillNone;
                    } else {
                        mergeAttributes(dp, fillNone);
                    }
                    if (JSONObject.NULL.equals(dp)) {
                        dp = new JSONObject();
                    }
                    dataPoints.put(i, dp);
                }
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        op.putSafe(OCKey.ATTRS.value(), attrs);
        op.putSafe(OCKey.NAME.value(), OCValue.INSERT_CHART_SERIES.value());

        operationQueue.put(op);
    }
    
    public void addDataPointStyle(JSONObject dataPointStyle) {
        dataPoints.put(dataPointStyle);
    }

    public JSONObject getSeriesJSON() {
        JSONObject attrs = getAttributes();
        if (!attrs.has(OCKey.SERIES.value())) {
            attrs.putSafe(OCKey.SERIES.value(), new JSONObject());
        }
        return attrs.optJSONObject(OCKey.SERIES.value());
    }

    public String getType() {
        return getSeriesJSON().optString(OCKey.TYPE.value());
    }

    public boolean isLine() {
        return StringUtils.contains(getType(), "line");
    }

    public boolean isScatter() {
        return StringUtils.contains(getType(), "scatter");
    }

    public boolean isBubble() {
        return StringUtils.contains(getType(), "bubble");
    }

    public boolean isPie() {
        return StringUtils.contains(getType(), "pie");
    }

    public boolean isDonut() {
        return StringUtils.contains(getType(), "donut");
    }
    
    public boolean isBar() {
        return StringUtils.contains(getType(), "bar");
    }
    

    public void setHiddenMarker(boolean hiddenMarker) {
        this.hiddenMarker = hiddenMarker;
    }

    public String getValues() {
        return getSeriesJSON().optString(OCKey.VALUES.value());
    }

    public void setNames(String names) {
        getSeriesJSON().putSafe(OCKey.NAMES.value(), names);
    }

    public void setBubbles(String bubbles) {
        getSeriesJSON().putSafe(OCKey.BUBBLES.value(), bubbles);
    }

    public void setValues(String values) {
        getSeriesJSON().putSafe(OCKey.VALUES.value(), values);
    }
    
    public void addLabelPointPosition(String position) {
        final JSONObject dataLabel = AttributeSet.getOrCreateJSONObject(getSeriesJSON(), OCKey.DATA_LABEL.value());
        final JSONArray dataLabels = AttributeSet.getOrCreateJSONArray(dataLabel, OCKey.DATA_LABELS.value());
        final JSONObject positionObject = new JSONObject();
        positionObject.putSafe(OCKey.DATA_LABEL_POS.value(), position);
        dataLabels.put(positionObject);
    }
}
