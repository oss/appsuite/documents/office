/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.chart;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.AttributeSet;

public class ChartAxis extends AttributeSet {

    private final AttributeSet grid = new AttributeSet();
    private final AttributeSet title = new AttributeSet();

    private Integer axis;

    ChartAxis() {

    }

    public void setAxis(Integer axis) {
        this.axis = axis;
    }

    public void createInsertOperation(List<Integer> start, JSONArray operationQueue) throws JSONException {

        if (!getAttributes().isEmpty()) {
            String axPos = null;
            Integer crossAx = null;
            if (axis == 0) {
                axPos = "b";
                crossAx = 1;
            } else if (axis == 1) {
                axPos = "l";
                crossAx = 0;
            } else if (axis == 2) {
                axPos = "t";
                crossAx = 3;
            } else if (axis == 3){
                axPos = "r";
                crossAx = 2;
            } else if (axis == 10) {
                axPos = "b";
                crossAx = 1;
            } else if (axis == 11) {
                axPos = "t";
                crossAx = 3;
            }
            
            JSONObject op = new JSONObject(7);
            op.put(OCKey.START.value(), start);
            op.putSafe(OCKey.AXIS.value(), axis);
            op.putSafe(OCKey.ATTRS.value(), getAttributes());
            op.putSafe(OCKey.NAME.value(), OCValue.CHANGE_CHART_AXIS.value());
            op.putSafe(OCKey.CROSS_AX.value(), crossAx);
            op.putSafe(OCKey.AX_POS.value(), axPos);
            if (axis == 10 || axis == 11) {
                op.putSafe(OCKey.Z_AXIS.value(), true);
            }
            operationQueue.put(op);
        }

        if (!grid.getAttributes().isEmpty()) {
            JSONObject op = new JSONObject(4);
            op.put(OCKey.START.value(), start);
            op.putSafe(OCKey.AXIS.value(), axis);
            op.putSafe(OCKey.ATTRS.value(), grid.getAttributes());
            op.putSafe(OCKey.NAME.value(), OCValue.CHANGE_CHART_GRID.value());
            operationQueue.put(op);
        }

        if (!title.getAttributes().isEmpty()) {
            JSONObject op = new JSONObject(4);
            op.put(OCKey.START.value(), start);
            op.putSafe(OCKey.AXIS.value(), axis);
            op.putSafe(OCKey.ATTRS.value(), title.getAttributes());
            op.putSafe(OCKey.NAME.value(), OCValue.CHANGE_CHART_TITLE.value());
            operationQueue.put(op);
        }

    }

    public Integer getAxis() {
        return axis;
    }

    public AttributeSet getTitle() {
        return title;
    }

    public AttributeSet getGridLine() {
        return grid;
    }

}
