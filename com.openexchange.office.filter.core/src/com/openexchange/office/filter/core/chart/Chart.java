/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.chart;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.AttributeSet;

public class Chart extends AttributeSet {

    private final List<ChartSeries> series = new ArrayList<>();
    private final AttributeSet legend = new AttributeSet();
    private final Map<Integer, ChartAxis> axes = new HashMap<>();

    private final AttributeSet mainTitle = new AttributeSet();
    private boolean hasErrorBars = false;

    public Chart() {

    }

    public void createAttributes(JSONObject target) {
        JSONObject attrs = getAttributes();
        mergeAttributes(target, attrs);
    }

    public void createJSONOperations(List<Integer> position, JSONArray operationQueue) throws JSONException {

        if (hasErrorBars) {
            JSONObject chartOperation = operationQueue.getJSONObject(operationQueue.length() -1);
            JSONObject attrs;
            if (!chartOperation.has(OCKey.ATTRS.value())) {
                chartOperation.put(OCKey.ATTRS.value(), new JSONObject());
            }
            attrs =  chartOperation.getJSONObject(OCKey.ATTRS.value());

            if (!attrs.has(OCKey.CHART.value())) {
                attrs.put(OCKey.CHART.value(), new JSONObject());
            }
            
            attrs.getJSONObject(OCKey.CHART.value()).putSafe(OCKey.ERROR_BAR.value(), true);
        }
        
        int seriesIndex = 0;
        for (ChartSeries serie : series) {
            serie.createInsertOperation(position, operationQueue, seriesIndex);
            seriesIndex++;
        }

        if (!mainTitle.getAttributes().isEmpty()) {
            JSONObject op = new JSONObject(3);
            op.put(OCKey.START.value(), position);
            op.putSafe(OCKey.ATTRS.value(), mainTitle.getAttributes());
            op.putSafe(OCKey.NAME.value(), OCValue.CHANGE_CHART_TITLE.value());
            operationQueue.put(op);
        }

        if (!legend.getAttributes().isEmpty()) {
            JSONObject op = new JSONObject(3);
            op.put(OCKey.START.value(), position);
            op.putSafe(OCKey.ATTRS.value(), legend.getAttributes());
            op.putSafe(OCKey.NAME.value(), OCValue.CHANGE_CHART_LEGEND.value());
            operationQueue.put(op);
        }

        for (ChartAxis axis : axes.values()) {
            axis.createInsertOperation(position, operationQueue);
        }
    }

    public void setChartStyleId(int chartStyleId) {
        JSONObject chart = getAttributes().optJSONObject(OCKey.CHART.value());
        if (chart == null) {
            chart = new JSONObject();
            getAttributes().putSafe(OCKey.CHART.value(), chart);
        }
        chart.putSafe(OCKey.CHART_STYLE_ID.value(), chartStyleId);
    }

    public ChartSeries addDataSeries(ChartSeries serie) {
        series.add(serie);
        return serie;
    }

    public ChartSeries insertDataSeries(int seriesIndex, ChartSeries serie) {
        series.add(seriesIndex, serie);
        return serie;
    }

    public ChartSeries setDataSeriesAttributes(int seriesIndex, JSONObject attrs) {
        ChartSeries serie = series.get(seriesIndex);
        serie.setAttributes(attrs);
        return serie;
    }

    public ChartSeries deleteDataSeries(int seriesIndex) {
        return series.remove(seriesIndex);
    }

    public AttributeSet getLegend() {
        return legend;
    }

    public void setAxis(ChartAxis axis) {
        axes.put(axis.getAxis(), axis);
    }

    public ChartAxis getAxis(Integer axisId) {
        ChartAxis res = axes.get(axisId);
        if (res == null) {
            res = new ChartAxis();
            res.setAxis(axisId);
            setAxis(res);
        }
        return res;
    }
    
    public void removeAxes() {
        axes.clear();
        
    }
    
    public Collection<ChartAxis>getAxes () {
        return axes.values();
    }
    
    public boolean containsAxis(Integer axisId) {
        return axes.containsKey(axisId);
    }

    public AttributeSet getTitle(Integer axisId) {
        if (axisId == null) {
            return mainTitle;
        }
        return getAxis(axisId).getTitle();
    }

    public List<ChartSeries> getSeries() {
        return Collections.unmodifiableList(series);
    }

    public boolean isScatter() {
        if (series.isEmpty()) {
            return false;
        }
        return series.get(0).isScatter();
    }

    public boolean isBubble() {
        if (series.isEmpty()) {
            return false;
        }
        return series.get(0).isBubble();
    }

    public boolean isPieOrDonut() {
        if (series.isEmpty()) {
            return false;
        }
        return series.get(0).isPie() || series.get(0).isDonut();
    }
    
    public boolean isBar() {
        if (series.isEmpty()) {
            return false;
        }
        return series.get(0).isBar();
    }
    

    public void setHasErrorBar() {
        hasErrorBars = true;
    }
    
    public boolean hasErrorBars() {
        return hasErrorBars;
    }
}
