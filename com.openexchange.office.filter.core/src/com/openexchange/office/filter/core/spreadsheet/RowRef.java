/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import org.apache.commons.lang3.mutable.MutableInt;

public final class RowRef implements IColumnRowRef {

    private int row;

    public RowRef(int row) {
        this.row = row;
    }

    @Override
    public int hashCode() {
        return row;
    }

    @Override
    public String toString() {
        return Long.toString(row + 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RowRef) {
            return row == ((RowRef)obj).getValue();
        }
        return false;
    }

    @Override
    public int compareTo(IColumnRowRef arg0) {
        return getValue() - arg0.getValue();
    }

    @Override
    public int getValue() {
        return row;
    }

    @Override
    public void setValue(int row) {
        this.row = row;
    }

    @Override
    public void move(int offset) {
        this.row += offset;
    }

    @Override
    public RowRef deepClone() {
        return clone();
    }

    @Override
    public RowRef clone() {
        try {
            return (RowRef)super.clone();
        }
        catch (CloneNotSupportedException e) {
            return new RowRef(0);
        }
    }

    /*
     * The offset gives the position up from where the row should be parsed.
     *
     * After parsing the offset points to last parsed character + 1 or -1 if
     * no row could be parsed. If nothing could be parsed the row is initialized
     * with 0.
     *
     */
    public static RowRef parseRowRef(String row, MutableInt offset) {

        int y = 0;
        int numbers = 0;
        int numberStartIndex = offset.intValue();

        if(numberStartIndex>=0) {
            if(numberStartIndex<row.length()&&row.charAt(numberStartIndex) == '$') {
                numberStartIndex++;
            }
            while(numbers+numberStartIndex<row.length()) {
                char character = row.charAt(numberStartIndex+numbers);
                if((character<'0')||(character>'9')) {
                    break;
                }
                numbers++;
            }
            if(numbers<=0||numbers>=8) {
                offset.setValue(-1);
            }
            else {
                int i;
                for(i = 0; i < numbers; i++) {
                    y *= 10;
                    y += row.charAt(i+numberStartIndex)-'0';
                }
                offset.setValue(i+numberStartIndex);
                y--;
            }
        }
        return new RowRef(y);
    }
}
