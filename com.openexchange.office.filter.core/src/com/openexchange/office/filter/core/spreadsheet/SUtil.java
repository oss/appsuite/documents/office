/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import org.json.JSONArray;
import org.json.JSONException;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;

public class SUtil {

    public static void moveSheets(ISpreadsheet<? extends ISheet> spreadsheet, JSONArray sa)
        throws JSONException {

        final DLList<? extends ISheet> sheetList = spreadsheet.getContent();

        int sheetCount = 0;
        final String[] sheetNames = new String[sa.length()];
        DLNode<? extends ISheet> node = sheetList.getFirstNode();
        while(node!=null) {
            sheetNames[sheetCount++] = node.getData().getName();
            node = node.getNext();
        }
        if(sheetCount!=sa.length()) {
            throw new FilterException("sheet count does not match for moveSheets operation", ErrorCode.CRITICAL_ERROR);
        }
        final String[] destSheetNames = new String[sa.length()];
        for(int i=0; i<sa.length(); i++) {
            destSheetNames[i] = sheetNames[sa.getInt(i)];
        }
        for(int i=0; i<sa.length(); i++) {
            int s = getSheetIndex(spreadsheet, destSheetNames[i]);
            if(s!=i) {
                spreadsheet.moveSheet(s, i);
            }
        }
    }

    private static int getSheetIndex(INodeAccessor<? extends ISheet> spreadsheet, String sheetName) {
        DLNode<? extends ISheet> node = spreadsheet.getContent().getFirstNode();
        int i = 0;
        while(node!=null) {
            if(sheetName.equals(node.getData().getName())) {
                return i;
            }
            node = node.getNext();
            i++;
        }
        return -1;
    }
}
