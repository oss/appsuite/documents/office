/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import org.apache.commons.lang3.mutable.MutableInt;

public final class Interval implements Cloneable {

    IColumnRowRef min;
    IColumnRowRef max;

    public Interval(IColumnRowRef min, IColumnRowRef max) {
        this.min = min;
        this.max = max;
    }

    public IColumnRowRef getMin() {
        return min;
    }

    public IColumnRowRef getMax() {
        return max;
    }

    public Interval setValue(Interval interval) {
        this.min = interval.getMin();
        this.max = interval.getMax();
        return this;
    }

    public Interval setValue(int min, int max) {
        this.min.setValue(min);
        this.max.setValue(max);
        return this;
    }

    public Interval setMinValue(int min) {
        this.min.setValue(min);
        return this;
    }

    public Interval setMaxValue(int max) {
        this.max.setValue(max);
        return this;
    }

    public Interval move(int offset) {
        min.move(offset);
        max.move(offset);
        return this;
    }

    public boolean isRowInterval() {
        return min instanceof RowRef;
    }

    public boolean isColumnInterval() {
        return min instanceof ColumnRef;
    }

    /**
     * Returns the number of columns/rows covered by this index interval.
     *
     * @returns
     *  The number of columns/rows covered by this index interval.
     */
    public int size() {
        return (max.getValue() - min.getValue()) + 1;
    }

    /**
     * Returns whether this index interval covers a single index (first and
     * last index are equal).
     *
     * @returns
     *  Whether this index interval covers a single index.
     */
    public boolean single() {
        return min.getValue() == max.getValue();
    }

    /**
     * Returns whether this index interval contains the passed column/row
     * index.
     *
     * @param index
     *  The zero-based column/row index to be checked.
     *
     * @returns
     *  Whether this index interval contains the passed column/row index.
     */
    public boolean containsIndex(int index) {
        return (min.getValue() <= index) && (index <= max.getValue());
    }

    /**
     * Returns the index interval covered by this index interval, and the
     * passed index interval.
     *
     * @param interval
     *  The other index interval to be intersected with this index interval.
     *
     * @returns
     *  The index interval covered by this and the passed index interval, if
     *  existing; otherwise null.
     */
    public Interval intersect(Interval interval) {
        int first =  Math.max(min.getValue(), interval.getMin().getValue());
        int last = Math.min(max.getValue(), interval.getMax().getValue());
        return (first <= last) ? deepClone().setValue(first, last) : null;
    }

    /**
     * Returns whether this index interval overlaps with the passed index
     * interval by at least one column/row.
     *
     * @param interval
     *  The index interval to be checked.
     *
     * @returns
     *  Whether this index interval overlaps with the passed index interval.
     */
    public boolean overlaps(Interval interval) {
        return (getMin().getValue() <= interval.getMax().getValue()) && (interval.getMin().getValue() <= getMax().getValue());
    }

    /**
     * Returns whether this index interval completely contains the passed index
     * interval.
     *
     * @param interval
     *  The index interval to be checked.
     *
     * @returns
     *  Whether this index interval completely contains the passed index
     *  interval.
     */
    public boolean contains(Interval interval) {
        return (getMin().getValue() <= interval.getMin().getValue()) && (interval.getMax().getValue() <= getMax().getValue());
    }

    @Override
    public String toString() {
        final String mi = min.toString();
        if(min.equals(max)) {
            return mi;
        }
        final String ma = max.toString();
        final StringBuilder builder = new StringBuilder(mi.length() + ma.length() + 1);
        builder.append(mi);
        builder.append(':');
        builder.append(ma);
        return builder.toString();
    }

    public Interval deepClone() {
        final Interval clone = clone();
        clone.min = min.deepClone();
        clone.max = max.deepClone();
        return clone;
    }

    @Override
    public Interval clone() {
        try {
            return (Interval)super.clone();
        }
        catch (CloneNotSupportedException e) {
            return new Interval(new ColumnRef(0), new ColumnRef(0));
        }
    }

    public static Interval createInterval(boolean columns, int min, int max) {
        return new Interval(columns ? new ColumnRef(min) : new RowRef(min), columns ? new ColumnRef(max) : new RowRef(max));
    }

    public static Interval parseInterval(boolean columns, String interval) {
        final MutableInt offset = new MutableInt(0);
        final IColumnRowRef min = columns ? ColumnRef.parseColumnRef(interval, offset) : RowRef.parseRowRef(interval, offset);
        if(offset.intValue()==-1) {
            return null;
        }
        offset.setValue(interval.indexOf(':', offset.intValue()));
        if(offset.intValue()==-1) {
            return new Interval(min, min);
        }
        if(offset.intValue()+1>=interval.length()) {
            return null;
        }
        offset.increment();
        final IColumnRowRef  max = columns ? ColumnRef.parseColumnRef(interval, offset) : RowRef.parseRowRef(interval, offset);
        if(offset.intValue()==-1) {
            return null;
        }
        return new Interval(min, max);
    }
}
