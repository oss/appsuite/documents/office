/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.List;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;

public class FormatCode {

    final DLList<Entry> formatCodes = new DLList<Entry>();
    final String toString;
    DLNode<Entry> lastDecimalPointPlaceholderNode = null;

    int integerDigits = 0;
    int decimalDigits = 0;
    boolean grouping = false;

    // ------------------------------------------------------------

    boolean isFraction = false;

    public boolean isFraction() {
        return isFraction;
    }

    // ------------------------------------------------------------

    boolean isText = false;

    public boolean isText() {
        return isText;
    }

    // ------------------------------------------------------------

    public boolean isBoolean() { // BOOLEAN is not specified
        return toString.equals("BOOLEAN");
    }

    // ------------------------------------------------------------

    boolean isCurrency = false;

    public boolean isCurrency() {
        return isCurrency;
    }

    // ------------------------------------------------------------

    public boolean isPercentage() {
        return toString.contains("%");
    }

    // ------------------------------------------------------------

    boolean isScientific;

    public boolean isScientific() {
        return isScientific;
    }

    // ------------------------------------------------------------

    int mCount = 0;
    boolean isDateOrTime = false;

    boolean isDateOrTime() {
        return isDateOrTime;
    }

    // ------------------------------------------------------------

    boolean isDate = false;

    public boolean isDate() {
        if(isDate) {
            return true;
        }
        if(!isTime) {
            return isDateOrTime; // if its not date and not time it is possible that its a month only
        }
        return false;
    }

    // ------------------------------------------------------------

    boolean isTime = false;

    public boolean isTime() {
        return isTime;
    }

    public int getIntegerDigits() {
        return integerDigits;
    }

    public int getDecimalDigits() {
        return decimalDigits;
    }

    public boolean isGrouping() {
        return grouping;
    }

    public DLNode<Entry> getDecimalPointPlaceholderNode() {
        return lastDecimalPointPlaceholderNode;
    }

    public FormatCode(String code) {

        DateTimeEntry lastDateTimeEntry = null; // is needed to distiguish between "m"inute and "m"onth

        for(int i=0; i < code.length(); i++) {
            final char c = code.charAt(i);
            switch(c) {
                case '$' :
                case '-' :
                case '+' :
                case '(' :
                case ')' :
                case ':' :
                case '!' :
                case '^' :
                case '&' :
                case '\'':
                case '~' :
                case '{' :
                case '}' :
                case '<' :
                case '>' :
                case '=' :
                case ' ' :
                // PASSTHROUGH INTENDED
                {
                    appendText(String.valueOf(c));
                    break;
                }
                case ',' : {
                    if(formatCodes.getLast() instanceof DigitPlaceholder) {
                        appendFormatCode(c);
                        grouping = true;
                    }
                    else {
                        appendText(String.valueOf(c));
                    }
                    break;
                }
                case '/' : { // fraction if preceded and followed by a DigitPlaceholder otherwise it is simple text
                    if(i != 0 && i + 1 < code.length()) {
                        if(isDigitPlaceholder(code.charAt(i-1)) && isDigitPlaceholder(code.charAt(i + 1))) {
                            isFraction = true;
                            formatCodes.add(new FractionEntry());
                        }
                        else  {
                            appendText(String.valueOf(code.charAt(i)));
                        }
                    }
                    break;
                }
                case '?' :
                case '0' :
                case '#' :
                // PASSTHROUGH INTENDED
                {
                    if(lastDecimalPointPlaceholderNode!=null) {
                        decimalDigits++;
                    }
                    else if(c=='0'){
                        integerDigits++;
                    }
                    final Entry lastEntry = formatCodes.isEmpty() ? null : formatCodes.get(formatCodes.size() - 1);
                    if(lastEntry instanceof DigitPlaceholder) {
                        ((DigitPlaceholder)lastEntry).appendFormat(c);
                    }
                    else {
                        formatCodes.add(new DigitPlaceholder(String.valueOf(c)));
                    }
                    break;
                }
                case '"' : { // extract encoded text "..."
                    i++;
                    int l = 0;
                    while(i + l < code.length() && code.charAt(i + l)!='"') {
                        l++;
                    }
                    appendText(code.substring(i, i + l));
                    i += l;
                    break;
                }
                case '\\' : { // extract encoded character \.
                    i++;
                    if(i < code.length()) {
                        appendText(String.valueOf(code.charAt(i)));
                    }
                    break;
                }
                case '@' : {
                    isText = true;
                    formatCodes.add(new TextPlaceholder());
                    break;
                }
                case '.' : {
                    final DecimalPointPlaceholder decimalPointPlaceholder = new DecimalPointPlaceholder();
                    lastDecimalPointPlaceholderNode = new DLNode<Entry>(decimalPointPlaceholder);
                    formatCodes.addNode(lastDecimalPointPlaceholderNode);
                    break;
                }
                case '_' : {
                    i++;
                    if(i < code.length()) {
                        formatCodes.add(new UnderscoreEntry(code.charAt(i)));
                    }
                    break;
                }
                case 'A' : {
                    final String content = code.substring(i);
                    if(content.startsWith("AM/PM")) {
                        formatCodes.add(new AmPmPlaceholder("AM/PM"));
                        isTime = true;
                        i += 4;
                    }
                    else if(content.startsWith("A/P")) {
                        formatCodes.add(new AmPmPlaceholder("A/P"));
                        isTime = true;
                        i += 2;
                    }
                    break;
                }
                case 'e' :
                case 'E' : {
                    final String content = code.substring(i);
                    if(content.startsWith("E-") || content.startsWith("E+") || content.startsWith("e-") || content.startsWith("e+"))  {
                        formatCodes.add(new ScientificPlaceholder(code.substring(i, i + 2)));
                        isScientific = true;
                        i++;
                    }
                    else {
                        appendText(String.valueOf(c));
                    }
                    break;
                }
                case '[' : { // extract square bracket or time format
                    i++;
                    int l = 0;
                    while(i + l < code.length() && code.charAt(i + l)!=']') {
                        l++;
                    }
                    final SquareBracketEntry entry = new SquareBracketEntry(code.substring(i, i + l));
                    if(entry.isTimeSymbol()) {
                        isTime = true;
                    }
                    else if(entry.isCurrency()) {
                        isCurrency = true;
                    }
                    formatCodes.add(entry);
                    i += l;
                    break;
                }
                // date codes
                case 'y' :
                case 'Y' :
                case 'd' :
                case 'D' : {
                    int l = getSameCharCount(i, code);
                    isDate = true;
                    lastDateTimeEntry = new DateTimeEntry(Character.toUpperCase(c), l);
                    formatCodes.add(lastDateTimeEntry);
                    i += l - 1;
                    break;
                }
                // time codes
                case 'h' :
                case 'H' :
                case 's' :
                case 'S' : {
                    int l = getSameCharCount(i, code);
                    isTime = true;
                    if(c == 's' && lastDateTimeEntry!=null && lastDateTimeEntry.getDateTimeSymbol() == 'M') { // "M" preceding "s" is interpreted as minute
                        lastDateTimeEntry.setDateTimeSymbol('m');
                    }
                    lastDateTimeEntry = new DateTimeEntry(Character.toLowerCase(c), l);
                    formatCodes.add(lastDateTimeEntry);
                    i += l - 1;
                    break;
                }
                // date or time
                case 'm' :
                case 'M' : {
                    int l = getSameCharCount(i, code);
                    mCount += l;
                    isDateOrTime = true;
                    lastDateTimeEntry = new DateTimeEntry(lastDateTimeEntry!=null && lastDateTimeEntry.getDateTimeSymbol() == 'h' ? 'm' : 'M', l);  // "m" directly following "h" is interpreted as minute
                    formatCodes.add(lastDateTimeEntry);
                    i += l - 1;
                    break;
                }
                // special oo codes
                case 'G' :
                case 'W' : {
                    int l = getSameCharCount(i, code);
                    if(c == 'W' && l == 1) { // only double W is used as TimeEntry ... single W is still text
                        appendText(String.valueOf(c));
                    }
                    else {
                        isDate = true;
                        lastDateTimeEntry = new DateTimeEntry(Character.toUpperCase(c), l);
                        formatCodes.add(lastDateTimeEntry);
                        i += l - 1;
                    }
                    break;
                }
                default : {
                    appendFormatCode(c);
                }
            }
        }

        final StringBuilder toStringBuilder = new StringBuilder();
        for(Entry entry:formatCodes) {
            toStringBuilder.append(entry.toString());
        }
        toString = toStringBuilder.toString();
    }

    void appendText(String text) {
        if(formatCodes.getLast() instanceof TextEntry) {
            ((TextEntry)formatCodes.getLast()).appendText(text);
        }
        else {
            formatCodes.add(new TextEntry(text));
        }
    }

    public void appendFormatCode(char c) {
        final Entry lastEntry = formatCodes.isEmpty() ? null : formatCodes.get(formatCodes.size() - 1);
        if(lastEntry instanceof FormatEntry) {
            ((FormatEntry)lastEntry).appendFormat(c);
        }
        else {
            formatCodes.add(new FormatEntry(String.valueOf(c)));
        }
    }

    @Override
    public String toString() {
        return toString;
    }

    public DLList<Entry> getEntries() {
        return formatCodes;
    }

    public abstract static class Entry {

        @Override
        public abstract String toString();

    }

    public static class TextEntry extends Entry {

        String text;

        public TextEntry(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public void appendText(String t) {
            this.text = this.text + t;
        }
    }

    public static class TextPlaceholder extends Entry {

        @Override
        public String toString() {
            return String.valueOf('@');
        }
    }

    public static class DecimalPointPlaceholder extends Entry {

        @Override
        public String toString() {
            return String.valueOf('.');
        }
    }

    public static class DigitPlaceholder extends Entry {

        final StringBuilder placeholder;

        public DigitPlaceholder(String placeholder) {
            this.placeholder = new StringBuilder(placeholder);
        }

        public void appendFormat(char c) {
            placeholder.append(c);
        }

        @Override
        public String toString() {
            return placeholder.toString();
        }
    }

    public static class AmPmPlaceholder extends Entry {

        final String placeholder;

        public AmPmPlaceholder(String placeholder) {
            this.placeholder = placeholder;
        }

        @Override
        public String toString() {
            return placeholder.toString();
        }
    }

    public static class ScientificPlaceholder extends Entry {

        final String placeholder;

        public ScientificPlaceholder(String placeholder) {
            this.placeholder = placeholder;
        }

        @Override
        public String toString() {
            return placeholder.toString();
        }
    }

    public static class FormatEntry extends Entry {

        final StringBuilder format;

        public FormatEntry(String format) {
            this.format = new StringBuilder(format);
        }

        public void appendFormat(char c) {
            format.append(c);
        }

        @Override
        public String toString() {
            return format.toString();
        }
    }

    public static class DateTimeEntry extends Entry {

        char c;
        int count;
        boolean isElapsedTime = false;

        public DateTimeEntry(char c, int count) {
            this.c = c;
            this.count = count;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder(count);
            for(int i = 0; i < count; i++) {
                builder.append(c);
            }
            return builder.toString();
        }

        public char getDateTimeSymbol() {
            return c;
        }

        public void setDateTimeSymbol(char c) {
            this.c = c;
        }

        public boolean isElapsedTime() {
            return isElapsedTime;
        }

        public void setIsElapsedTime(boolean isElapsedTime) {
            this.isElapsedTime = isElapsedTime;
        }

        public int getDateTimeLenght() {
            return count;
        }
    }

    public static class SquareBracketEntry extends Entry {

        final String bracketEntry;

        public SquareBracketEntry(String bracketEntry) {
            this.bracketEntry = bracketEntry;
        }

        @Override
        public String toString() {
            return "[" + bracketEntry + "]";
        }

        public String getContent() {
            return bracketEntry;
        }

        public boolean isCurrency() {
            return bracketEntry.startsWith("$");
        }

        public boolean isTimeSymbol() {
            return bracketEntry.equals("h") || bracketEntry.equals("H") || bracketEntry.equals("mm") || bracketEntry.equals("MM") || bracketEntry.equals("ss") || bracketEntry.equals("SS");
        }
    }

    public static class UnderscoreEntry extends Entry {

        final char c;

        public UnderscoreEntry(char c) {
            this.c = c;
        }

        @Override
        public String toString() {
            // return "_" + c;  // _ does not work in lo
            return " ";
        }
    }

    public static class FractionEntry extends Entry {

        @Override
        public String toString() {
            return "/";
        }
    }

    static boolean isDigitPlaceholder(char c) {
        return c == '?' || c == '0' || c == '#';
    }

    static boolean isTimeSymbol(String c) {
        return c.equals("h") || c.equals("H") || c.equals("mm") || c.equals("MM") || c.equals("ss") || c.equals("SS");
    }

    static int getSameCharCount(int i, String t) {
        final char c = t.charAt(i);
        int l = 1;
        while(i + l < t.length()) {
            if(t.charAt(i + l) == c) {
                l++;
            }
            else {
                break;
            }
        }
        return l;
    }
}
