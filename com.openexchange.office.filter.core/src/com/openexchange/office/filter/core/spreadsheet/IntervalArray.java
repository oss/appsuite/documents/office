/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class IntervalArray extends ArrayList<Interval> implements Cloneable {

    public IntervalArray() {
        //
    }

    public IntervalArray(int capacity) {
        super(capacity);
    }

    public IntervalArray(List<Interval> intervals) {
        super(intervals);
    }

    public IntervalArray(Interval interval) {
        super(1);

        this.add(interval);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        forEach(interval -> {
            if(builder.length()!=0) {
                builder.append(' ');
            }
            builder.append(interval.toString());
        });
        return builder.toString();
    }

    public IntervalArray deepClone() {
        final IntervalArray clone = new IntervalArray(size());
        forEach(interval -> {
            clone.add(interval.deepClone());
        });
        return clone;
    }

    @Override
    public IntervalArray clone() {
        return (IntervalArray)super.clone();
    }

    public static IntervalArray createIntervals(boolean columns, String intervals) {
        if(intervals==null) {
            return new IntervalArray(0);
        }
        final String[] tokenArray = intervals.split(" ", -1);
        final IntervalArray list = new IntervalArray(tokenArray.length);
        for(String entry:tokenArray) {
            final Interval interval = Interval.parseInterval(columns, entry);
            if(interval!=null) {
                list.add(interval);
            }
        }
        return list;
    }

    public IntervalArray move(int offset) {
        forEach(interval -> {
            interval.move(offset);
        });
        return this;
    }

    /**
     * Merges the index intervals in the passed data source so that no index
     * interval in the result overlaps with or adjoins to another index
     * interval. The resulting index intervals will be sorted in ascending
     * order.
     *
     * @param intervals
     *  The index intervals to be merged. This function also accepts a single
     *  index interval as parameter.
     *
     * @returns
     *  A sorted array of merged index intervals exactly covering the index
     *  intervals in this array.
     */
    public IntervalArray merge() {
        sort();
        for(int i=0; i < size(); i++) {
            Interval interval1 = get(i);
            // extend current interval, as long as adjacent or overlapping with next interval
            if(i + 1 < size()) {
                Interval interval2 = get(i + 1);
                while(interval1.getMax().getValue() + 1 >= interval2.getMin().getValue()) {
                    interval1.getMax().setValue(Math.max(interval1.getMax().getValue(), interval2.getMax().getValue()));
                    remove(i + 1);
                    if(i + 1 >= size()) {
                        break;
                    }
                    interval2 = get(i + 1);
                }
            }
        }
        return this;
    }

    /**
     * Returns the index intervals covered by this array, and the passed array
     * of index intervals. More precisely, returns the existing intersection
     * intervals from all interval pairs of the cross product of the interval
     * arrays.
     *
     * @param intervals
     *  The other index intervals to be intersected with this index intervals.
     *  This method also accepts a single index interval as parameter.
     *
     * @returns
     *  The index intervals covered by this array, and the passed array of
     *  index intervals.
     */
    public IntervalArray intersect(IntervalArray intervals) {
        final IntervalArray resultIntervals = new IntervalArray();
        for(Interval interval1:this) {
            for(Interval interval2:intervals) {
                final Interval interval = interval1.intersect(interval2);
                if(interval!=null) {
                    resultIntervals.add(interval);
                }
            }
        }
        return resultIntervals;
    }

    /**
     * Returns the difference of the index intervals in this array, and the
     * passed index intervals (all index intervals contained in this array,
     * that are not contained in the passed array).
     *
     * @param intervals
     *  The index intervals to be removed from this array. This method also
     *  accepts a single index interval as parameter.
     *
     * @returns
     *  All index intervals that are contained in this array, but not in the
     *  passed array. May be an empty array, if the passed index intervals
     *  completely cover all index intervals in this array.
     */
    public IntervalArray difference(Interval interval) {
        return difference(new IntervalArray(interval));
    }

    public IntervalArray difference(IntervalArray intervals) {

        // the resulting intervals
        IntervalArray resultIntervals = deepClone();

        // reduce intervals in 'result' with each interval from the passed array
        for(Interval interval2:intervals) {
            // initialize the arrays for this iteration
            final IntervalArray source = resultIntervals;
            resultIntervals = new IntervalArray();

            // process each interval in the source interval list
            for(Interval interval1:source) {

                // check if the intervals cover each other at all
                if (!interval1.overlaps(interval2)) {
                    resultIntervals.add(interval1.clone());
                    break;
                }

                // do nothing if interval2 covers interval1 completely (delete interval1 from the result)
                if (interval2.contains(interval1)) {
                    break;
                }

                // if interval1 starts before interval2, extract the leading part of interval1
                if (interval1.getMin().getValue() < interval2.getMin().getValue()) {
                    resultIntervals.add(interval1.deepClone().setMaxValue(interval2.getMin().getValue() - 1));
                }

                // if interval1 ends after interval2, extract the trailing part of interval1
                if (interval1.getMax().getValue() > interval2.getMax().getValue()) {
                    resultIntervals.add(interval1.deepClone().setMinValue(interval2.getMax().getValue() + 1));
                }
            }
            // early exit the loop, if all source intervals have been deleted already
            if(resultIntervals.isEmpty()) {
                break;
            }
        }
        return resultIntervals;
    }

    public IntervalArray sort() {

        this.sort(new Comparator<Interval>() {

            @Override
            public int compare(Interval m1, Interval m2) {
                if(m1.getMin().getValue() < m2.getMin().getValue()) {
                    return -1;
                }
                else if(m1.getMin().getValue() > m2.getMin().getValue()) {
                    return 1;
                }
                else {
                    if(m1.getMax().getValue() < m2.getMax().getValue()) {
                        return -1;
                    }
                    else if(m1.getMax().getValue() > m2.getMax().getValue()) {
                        return 1;
                    }
                    return 0;
                }
            }
        });
        return this;
    }
}
