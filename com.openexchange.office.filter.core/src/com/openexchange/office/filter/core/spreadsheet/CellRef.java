/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class CellRef implements Cloneable {

    private int column;
    private int row;

    public CellRef(int column, int row) {
        this.column = column;
        this.row = row;
    }

    public CellRef(JSONArray ref) {
        try {
            this.column = ref.getInt(0);
            this.row = ref.getInt(1);
        }
        catch (JSONException e) {
            this.column = 0;
            this.row = 0;
        }
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Returns the column or row index of this cell address.
     *
     * @param columns
     *  Whether to return the column index (`true`), or the row index
     *  (`false`).
     *
     * @returns
     *  The column or row index of this cell address.
     */
    public int get(boolean columns) {
        return columns ? getColumn() : getRow();
    }

    public CellRef set(int value, boolean columns) {
        if(columns) {
            setColumn(value);
        }
        else {
            setRow(value);
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CellRef) {
            CellRef cellRef = (CellRef) o;
            return this.column == cellRef.column && this.row == cellRef.row;
        }
        return false;
    }

    public final JSONArray getJSONArray() {
        final JSONArray jsonArray = new JSONArray(2);
        jsonArray.put(column);
        jsonArray.put(row);
        return jsonArray;
    }

    @Override
    public CellRef clone() {
        try {
            return (CellRef)super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "CellRef [column=" + column + ", row=" + row + "]";
    }

    /*
     * odf uses "." whereas xlsx is using "!" as separator
     */
    public static CellRef createCellRefWithSheet(String cellRef, String sheetSeparator) {
        final int indexOf = cellRef.lastIndexOf(sheetSeparator);
        return createCellRef(indexOf!=-1 ? cellRef.substring(indexOf + 1) : cellRef);
    }
    /**
     * translates string CellRef like "B23" into a computable form : 1, 22
     *
     * returns null on error or missing cellRef
     *
     */
    public static CellRef createCellRef(String cellRef) {
    	return createCellRef(cellRef, null, null);
    }

    public static String getCellRef(CellRef cellRef) {
        return getCellRef(cellRef.getColumn(), cellRef.getRow());
    }

    public static String getCellRef(int column, long row) {
        StringBuilder buffer = new StringBuilder(8);
        addCellRef(buffer, column, row);
        return buffer.toString();
    }

    /**
     * translates string CellRef like "B23" into a computable form : 1, 22
     *
     * returns null on error or missing cellRef
     *
     */
    public static CellRef createCellRef(String cellRef, Integer defaultX, Integer defaultY) {

        if(cellRef==null||cellRef.isEmpty())
            return null;

        CellRef cellPosition = null;
        Integer x = null;
        Integer y = null;

        int characters = 0;
        int characterStartIndex = cellRef.charAt(0) == '$' ? 1 : 0;

        while((characters+characterStartIndex)<cellRef.length()) {
            char character = cellRef.charAt(characterStartIndex+characters);
            if((character<'A')||(character>'Z')) {
                break;
            }
            characters++;
        }
        if(characters==0&&defaultX!=null) {
        	x = defaultX;
        }
        else if(characters>0&&characters<6) {

            final int[] exMap = { 1, 26, 676, 17576, 456976 };

            int ex = characters - 1;
            x = -1;

            for(int i = 0; i < characters; i++) {
                x +=((cellRef.charAt(i+characterStartIndex)-'A')+1)*exMap[ex-i];
            }
        }

        if(x==null) {
        	return null;
        }

        int numbers = 0;
        int numberStartIndex = characterStartIndex += characters;

        if(numberStartIndex<cellRef.length()&&cellRef.charAt(numberStartIndex) == '$') {
            numberStartIndex++;
        }
        while(numbers+numberStartIndex<cellRef.length()) {
            char character = cellRef.charAt(numberStartIndex+numbers);
            if((character<'0')||(character>'9')) {
                break;
            }
            numbers++;
        }
        if(numbers==0&&defaultY!=null) {
        	y = defaultY;
        }
        else if(numbers>0&&numbers<=7) {
        	y = 0;
        	for(int i = 0; i < numbers; i++) {
                y *= 10;
                y += cellRef.charAt(i+numberStartIndex)-'0';
            }
        	y--;
        }
        if(y!=null) {
            cellPosition = new CellRef(x, y);
        }
        return cellPosition;
    }

    // ----------------------------------------------------------------------------------------------------

    public static boolean insertRows(CellRef cellRef, int start, int count) {
        if(cellRef!=null) {
            final int r = cellRef.getRow();
            final int to = r + count;
            if (r>=1048576) {
                return true;
            } else if (r>=start) {
                cellRef.setRow(to);
            }
        }
        return false;
    }

    public static void insertRows(List<? extends ICellRefAccess> list, int start, int count) {
        for(int i=0;i<list.size();i++) {
            if(insertRows(list.get(i).getCellRef(true), start, count)) {
                list.remove(i--);
            }
        }
    }

    public static boolean deleteRows(CellRef cellRef, int start, int count) {
        if(cellRef!=null) {
            final int end = start+count-1;
            final int r = cellRef.getRow();
            if (r>end) {
                cellRef.setRow(r - count);
            } else if (r>=start) {
                return true;
            }
        }
        return false;
    }

    public static void deleteRows(List<? extends ICellRefAccess> list, int start, int count) {
        for(int i=0;i<list.size();i++) {
            if(deleteRows(list.get(i).getCellRef(true), start, count)) {
                list.remove(i--);
            }
        }
    }

    public static boolean insertColumns(CellRef cellRef, int start, int count) {
        if(cellRef!=null) {
            final int c = cellRef.getColumn();
            final int to = c + count;
            if (c>=16383) {
                return true;
            } else if (c>=start) {
                cellRef.setColumn(to);
            }
        }
        return false;
    }

    public static void insertColumns(List<? extends ICellRefAccess> list, int start, int count) {
        for(int i=0;i<list.size();i++) {
            if(insertColumns(list.get(i).getCellRef(true), start, count)) {
                list.remove(i--);
            }
        }
    }

    public static boolean deleteColumns(CellRef cellRef, int start, int count) {
        if(cellRef!=null) {
            final int end = start+count-1;
            final int c = cellRef.getColumn();
            if (c>end) {
                cellRef.setColumn(c - count);
            } else if (c>=start) {
                return true;
            }
        }
        return false;
    }

    public static void deleteColumns(List<? extends ICellRefAccess> list, int start, int count) {
        for(int i=0;i<list.size();i++) {
            if(deleteColumns(list.get(i).getCellRef(true), start, count)) {
                list.remove(i--);
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------

    public static void addCellRefColumn(StringBuilder buffer, int column) {

        int pos = buffer.length();
        while(column>=0) {
            buffer.insert(pos, (char)('A' + (column % 26)));
            column /= 26;
            column--;
        }
    }

    public static void addCellRefRow(StringBuilder buffer, long row) {
        buffer.append(Long.toString(row+1));
    }

    public static void addCellRef(StringBuilder buffer, int column, long row) {
        addCellRefColumn(buffer, column);
        addCellRefRow(buffer, row);
    }

    // ----------------------------------------------------------------------------------------------------

    public static List<CellRef> createCellRefList(String s) {
        if(s==null) {
            return new ArrayList<CellRef>(0);
        }
        final String[] array = s.split(" ", -1);
        final List<CellRef> list = new ArrayList<CellRef>(array.length);
        for(String entry:array) {
            final CellRef c = CellRef.createCellRef(entry);
            if(c!=null) {
                list.add(c);
            }
        }
        return list;
    }

    /**
     * Returns the A1 notation of this cell address list as used in document
     * operations (space-separated) or null if empty.
     *
     * @returns
     *  The space-separated A1 notation of this cell address list as used in
     *  document operations.
     */
    public static String toString(List<CellRef> list) {
        if(list.isEmpty()) {
            return null;
        }
        final StringBuilder builder = new StringBuilder();
        for(CellRef cellRef:list) {
            if(builder.length()!=0) {
                builder.append(' ');
            }
            builder.append(CellRef.getCellRef(cellRef));
        }
        return builder.toString();
    }
}