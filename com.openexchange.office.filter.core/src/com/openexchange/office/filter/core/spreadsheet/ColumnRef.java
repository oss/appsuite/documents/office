/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import org.apache.commons.lang3.mutable.MutableInt;

public final class ColumnRef implements IColumnRowRef {

    private int column;

    public ColumnRef(int column) {
        this.column = column;
    }

    @Override
    public int hashCode() {
        return column;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(5);
        int i = column;
        while(i>=0) {
            builder.insert(0, (char)('A' + (i % 26)));
            i /= 26;
            i--;
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ColumnRef) {
            return column == ((ColumnRef)obj).getValue();
        }
        return false;
    }

    @Override
    public int compareTo(IColumnRowRef arg0) {
        return getValue() - arg0.getValue();
    }

    @Override
    public int getValue() {
        return column;
    }

    @Override
    public void setValue(int column) {
        this.column = column;
    }

    @Override
    public void move(int offset) {
        this.column += offset;
    }

    @Override
    public ColumnRef deepClone() {
        return clone();
    }

    @Override
    public ColumnRef clone() {
        try {
            return (ColumnRef)super.clone();
        }
        catch (CloneNotSupportedException e) {
            return new ColumnRef(0);
        }
    }

    /*
     * The offset gives the position up from where the row should be parsed.
     *
     * After parsing the offset points to last parsed character + 1 or -1 if
     * no column could be parsed. If nothing could be parsed the column is initialized
     * with 0.
     *
     */
    public static ColumnRef parseColumnRef(String interval, MutableInt offset) {

        int x = 0;
        int characters = 0;
        int characterStartIndex = offset.intValue();
        if(characterStartIndex>=0) {
            if(interval.charAt(characterStartIndex) == '$') {
                characterStartIndex++;
            }
            while((characters+characterStartIndex)<interval.length()) {
                char character = interval.charAt(characterStartIndex+characters);
                if((character<'A')||(character>'Z')) {
                    break;
                }
                characters++;
            }
            if(characters<=0||characters>=6) {
                offset.setValue(-1);
            }
            else {

                final int[] exMap = { 1, 26, 676, 17576, 456976 };

                int i, ex = characters - 1;
                x = -1;

                for(i = 0; i < characters; i++) {
                    x +=((interval.charAt(i+characterStartIndex)-'A')+1)*exMap[ex-i];
                }
                offset.setValue(i+characterStartIndex);
            }
        }
        return new ColumnRef(x);
    }
}
