/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.ArrayList;
import java.util.List;

public class CellRefRangeArray extends ArrayList<CellRefRange> implements Cloneable {

    public CellRefRangeArray() {
        //
    }

    public CellRefRangeArray(int capacity) {
        super(capacity);
    }

    public CellRefRangeArray(List<CellRefRange> ranges) {
        super(ranges);
    }

    public CellRefRangeArray(CellRefRange range) {
        super(1);

        this.add(range);
    }

    public CellRefRange first() {
        return isEmpty() ? null : get(0);
    }

    /**
     * Returns whether any cell range address in this list overlaps with any
     * cell range address in the passed data source.
     *
     * @param ranges
     *  The other cell range addresses to be checked. This method also accepts
     *  a single cell range address as parameter.
     *
     * @returns
     *  Whether any cell range address in this list overlaps with any cell
     *  range address in the passed data source.
     */
    public boolean overlaps(CellRefRangeArray rangeSource) {
        for(CellRefRange range:rangeSource) {
            if(overlaps(range)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the difference of the cell range addresses in this list, and
     * the passed cell range addresses (all cell range addresses contained in
     * this list, that are not contained in the passed list).
     *
     * @param ranges
     *  The cell range addresses to be removed from this list. This method also
     *  accepts a single cell range address as parameter.
     *
     * @returns
     *  All cell range addresses that are contained in this list, but not in
     *  the passed list. May be an empty array, if the passed list completely
     *  covers all cell range addresses in this list.
     */
    public CellRefRangeArray difference(CellRefRange range) {
        return difference(new CellRefRangeArray(range));
    }

    public CellRefRangeArray difference(CellRefRangeArray ranges) {

        if(ranges.isEmpty()) {
            return deepClone();
        }
        // the resulting ranges
        CellRefRangeArray resultRanges = new CellRefRangeArray(this);
        for(CellRefRange range2:ranges) {
            CellRefRangeArray sourceRanges = resultRanges;
            resultRanges = new CellRefRangeArray();

            for(CellRefRange range1:sourceRanges) {

                // check if the ranges cover each other at all
                if (!range1.overlaps(range2)) {
                    resultRanges.add(range1.clone());
                    continue;
                }

                // do nothing if range2 covers range1 completely (delete range1 from the result)
                if (range2.contains(range1)) {
                    continue;
                }

                // if range1 starts above range2, extract the upper part of range1
                if (range1.getStart().getRow() < range2.getStart().getRow()) {
                    resultRanges.add(new CellRefRange(range1.getStart().getColumn(), range1.getStart().getRow(), range1.getEnd().getColumn(), range2.getStart().getRow() - 1));
                }

                // if range1 starts left of range2, extract the left part of range1
                if (range1.getStart().getColumn() < range2.getStart().getColumn()) {
                    resultRanges.add(new CellRefRange(range1.getStart().getColumn(), Math.max(range1.getStart().getRow(), range2.getStart().getRow()), range2.getStart().getColumn() - 1, Math.min(range1.getEnd().getRow(), range2.getEnd().getRow())));
                }

                // if range1 ends right of range2, extract the right part of range1
                if (range1.getEnd().getColumn() > range2.getEnd().getColumn()) {
                    resultRanges.add(new CellRefRange(range2.getEnd().getColumn() + 1, Math.max(range1.getStart().getRow(), range2.getStart().getRow()), range1.getEnd().getColumn(), Math.min(range1.getEnd().getRow(), range2.getEnd().getRow())));
                }

                // if range1 ends below range2, extract the lower part of range1
                if (range1.getEnd().getRow() > range2.getEnd().getRow()) {
                    resultRanges.add(new CellRefRange(range1.getStart().getColumn(), range2.getEnd().getRow() + 1, range1.getEnd().getColumn(), range1.getEnd().getRow()));
                }
            }
            if(resultRanges.isEmpty()) {
                break;
            }
        }
        return resultRanges;
    }

    public boolean overlaps(CellRefRange rangeSource) {
        for(CellRefRange range:this) {
            if(range.overlaps(rangeSource)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        forEach(range -> {
            if(builder.length()!=0) {
                builder.append(' ');
            }
            builder.append(CellRefRange.getCellRefRange(range));
        });
        return builder.toString();
    }

    public CellRefRangeArray deepClone() {
        final CellRefRangeArray clone = new CellRefRangeArray(size());
        forEach(range -> {
            clone.add(range.clone());
        });
        return clone;
    }

    @Override
    public CellRefRangeArray clone() {
        return (CellRefRangeArray)super.clone();
    }

    public static CellRefRangeArray createRanges(String ranges) {
        if(ranges==null) {
            return new CellRefRangeArray(0);
        }
        final String[] tokenArray = ranges.split(" ", -1);
        final CellRefRangeArray list = new CellRefRangeArray(tokenArray.length);
        for(String entry:tokenArray) {
            final CellRefRange c = CellRefRange.createCellRefRange(entry);
            if(c!=null) {
                list.add(c);
            }
        }
        return list;
    }
}
