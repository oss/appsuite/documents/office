/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

public final class SmlUtils {

    public static String getAddressWithoutBraces(String in) {
        if(in==null||in.length()<2) {
            return in;
        }
        if(in.charAt(0)=='['&&in.charAt(in.length()-1)==']') {
            return in.substring(1, in.length()-1);
        }
        return in;
    }

    public static CellRefRange[] createCellRefRanges(String ranges) {
        if(ranges==null) {
            return new CellRefRange[0];
        }
        final String[] tokenArray = ranges.split(" ", -1);
        final CellRefRange[] cellRefRangeArray = new CellRefRange[tokenArray.length];
        for(int i = 0; i < tokenArray.length; i++) {
            cellRefRangeArray[i] = CellRefRange.createCellRefRange(tokenArray[i]);
        }
        return cellRefRangeArray;
    }

}
