/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.ArrayList;
import java.util.List;

public class CellRefArray extends ArrayList<CellRef> implements Cloneable {

    public CellRefArray() {
        //
    }

    public CellRefArray(int capacity) {
        super(capacity);
    }

    public CellRefArray(List<CellRef> ranges) {
        super(ranges);
    }

    public CellRefArray(CellRef range) {
        super(1);

        this.add(range);
    }

    public CellRef first() {
        return isEmpty() ? null : get(0);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        forEach(range -> {
            if(builder.length()!=0) {
                builder.append(' ');
            }
            builder.append(CellRef.getCellRef(range));
        });
        return builder.toString();
    }

    public CellRefArray deepClone() {
        final CellRefArray clone = new CellRefArray(size());
        forEach(range -> {
            clone.add(range.clone());
        });
        return clone;
    }

    @Override
    public CellRefArray clone() {
        return (CellRefArray)super.clone();
    }

    public static CellRefArray createCellRefArray(String cellRefs) {
        if(cellRefs==null) {
            return new CellRefArray(0);
        }
        final String[] tokenArray = cellRefs.split(" ", -1);
        final CellRefArray list = new CellRefArray(tokenArray.length);
        for(String entry:tokenArray) {
            final CellRef c = CellRef.createCellRef(entry);
            if(c!=null) {
                list.add(c);
            }
        }
        return list;
    }
}
