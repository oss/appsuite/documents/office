/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.core.spreadsheet;

import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class CellRefRange implements Cloneable {

    final CellRef start;
    final CellRef end;

    public CellRefRange() {
    	start = new CellRef(0, 0);
    	end = new CellRef(0, 0);
    }

    public CellRefRange(int c1, int r1, int c2, int r2) {
    	start = new CellRef(c1, r1);
    	end = new CellRef(c2, r2);
    }

    public CellRefRange(Interval column, Interval row) {
        this(column.getMin().getValue(), row.getMin().getValue(), column.getMax().getValue(), row.getMax().getValue());
    }

    public CellRefRange(CellRef start, CellRef end) {
        this.start = start;
        this.end = end;
    }

    public CellRef getStart() {
        return start;
    }

    public CellRef getEnd() {
        return end;
    }

    public int getColumns() {
        return (end.getColumn() - start.getColumn()) + 1;
    }

    public int getRows() {
        return (end.getRow() - start.getRow()) + 1;
    }

    @Override
    public boolean equals(Object cellRefRange) {
        if(cellRefRange==this) {
            return true;
        }
        if(!(cellRefRange instanceof CellRefRange)) {
            return false;
        }
        return start.equals(((CellRefRange)cellRefRange).getStart())&&end.equals(((CellRefRange)cellRefRange).getEnd());
    }

    /**
     * Returns whether the size (width AND height) of this range address is
     * equal to the size of the passed range address.
     *
     * @param range
     *  The range address to be checked.
     *
     * @returns
     *  Whether this range address has the same size as the passed range
     *  address.
     */
    public boolean equalSize(CellRefRange range) {
        return (getColumns() == range.getColumns()) && (getRows() == range.getRows());
    }

    /**
     * Returns whether this cell range address covers a single column (column
     * indexes in start address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single column.
     */
    public boolean singleCol() {
        return getStart().getColumn() == getEnd().getColumn();
    }

    /**
     * Returns whether this cell range address covers a single row (row indexes
     * in start address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single row.
     */
    public boolean singleRow() {
        return getStart().getRow() == getEnd().getRow();
    }

    /**
     * Returns whether this cell range address covers a single column or row.
     *
     * @param columns
     *  Whether to check this cell range address for a single column (`true`),
     *  or for a single row (`false`).
     *
     * @returns
     *  Whether this cell range address covers a single column or row.
     */
    public boolean singleLine(boolean columns) {
        return columns ? this.singleCol() : this.singleRow();
    }

    /**
     * Returns whether this cell range address covers a single cell (start
     * address and end address are equal).
     *
     * @returns
     *  Whether this cell range address covers a single cell.
     */
    public boolean single() {
        return getStart().equals(getEnd());
    }

    /**
    * Returns whether this cell range address overlaps with the passed cell
    * range address by at least one cell.
    *
    * @param range
    *  The other cell range address to be checked.
    *
    * @returns
    *  Whether this cell range address overlaps with the passed cell range
    *  address by at least one cell.
    */
    public boolean overlaps(CellRefRange range) {
        return (getStart().getColumn() <= range.getEnd().getColumn()) && (range.getStart().getColumn() <= getEnd().getColumn()) && (getStart().getRow() <= range.getEnd().getRow()) && (range.getStart().getRow() <= getEnd().getRow());
    }

    /**
     * Returns the column interval covered by this cell range address.
     *
     * @returns
     *  The column interval covered by this cell range address.
     */
    public Interval colInterval() {
        return Interval.createInterval(true, getStart().getColumn(), getEnd().getColumn());
    }

    /**
     * Returns the row interval covered by this cell range address.
     *
     * @returns
     *  The row interval covered by this cell range address.
     */
    public Interval rowInterval() {
        return Interval.createInterval(false, getStart().getRow(), getEnd().getRow());
    }

    /**
     * Returns the column or row interval covered by this cell range address.
     *
     * @param columns
     *  Whether to return the column interval (`true`), or the row interval
     *  (`false`) of this cell range address.
     *
     * @returns
     *  The column or row interval covered by this cell range address.
     */
    public Interval interval(boolean columns) {
        return columns ? colInterval() : rowInterval();
    }

    /**
     * Returns the address of the top row in this cell range address.
     *
     * @returns
     *  The address of the top row in this cell range address.
     */
    public CellRefRange headerRow() {
        return new CellRefRange(getStart().getColumn(), getStart().getRow(), getEnd().getColumn(), getStart().getRow());
    }

    /**
     * Returns the address of the bottom row in this cell range address.
     *
     * @returns
     *  The address of the bottom row in this cell range address.
     */
    public CellRefRange footerRow() {
        return new CellRefRange(getStart().getColumn(), getEnd().getRow(), getEnd().getColumn(), getEnd().getRow());
    }

    public boolean isInside(int column, int row) {
        return column>=Math.min(start.getColumn(), end.getColumn())&&column<=Math.max(start.getColumn(), end.getColumn())
            && row>=Math.min(start.getRow(), end.getRow())&&row<=Math.max(start.getRow(), end.getRow());
    }

    public boolean intersects(CellRefRange cellRefRange) {

        final int left  = Math.max( start.getColumn(), cellRefRange.getStart().getColumn() );
        final int right = Math.min( end.getColumn(), cellRefRange.getEnd().getColumn() );
        final int top   = Math.max( start.getRow(), cellRefRange.getStart().getRow() );
        final int bottom= Math.min( end.getRow(), cellRefRange.getEnd().getRow() );
        return (right<left||bottom<top) ? false : true;
    }

    public static  CellRefRange modifyCellRefRange(CellRefRange cellRefRange, int start, int count, boolean column, int rel, boolean append) {

        CellRefRange newCellRefRange = null;
        if(cellRefRange!=null) {
            if(rel>0) {
                if(column) {
                    newCellRefRange = CellRefRange.insertColumnRange(cellRefRange, start, count, append);
                }
                else {
                    newCellRefRange = CellRefRange.insertRowRange(cellRefRange, start, count, append);
                }
            }
            else {
                if(column) {
                    newCellRefRange = CellRefRange.deleteColumnRange(cellRefRange, start, count);
                }
                else {
                    newCellRefRange = CellRefRange.deleteRowRange(cellRefRange, start, count);
                }
            }
        }
        return newCellRefRange;
    }

    /**
     * Returns the intersecting cell range address between this cell range
     * address, and the passed cell range address.
     *
     * @param range
     *  The other cell range address.
     *
     * @returns
     *  The address of the cell range covered by both cell range addresses, if
     *  existing; otherwise null.
     */
    public CellRefRange intersect(CellRefRange range) {
        return overlaps(range) ? new CellRefRange(Math.max(getStart().getColumn(), range.getStart().getColumn()), Math.max(getStart().getRow(), range.getStart().getRow()),
                                                  Math.min(getEnd().getColumn(), range.getEnd().getColumn()), Math.min(getEnd().getRow(), range.getEnd().getRow())) : null;
    }

    public boolean contains(CellRefRange cellRefRange) {
    	return isInside(cellRefRange.getStart().getColumn(), cellRefRange.getStart().getRow())&&
    			isInside(cellRefRange.getEnd().getColumn(), cellRefRange.getEnd().getRow());
    }

    public final JSONObject getJSONObject() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put(OCKey.START.value(), start.getJSONArray());
        if (!start.equals(end)) { jsonObject.put(OCKey.END.value(), end.getJSONArray()); }
        return jsonObject;
    }

    public static CellRefRange insertRowRange(CellRefRange cellRefRange, int startRow, int count, boolean append) {
        final CellRef start = cellRefRange.getStart();
        final CellRef end = cellRefRange.getEnd();
        int r1 = start.getRow();
        int r2 = end.getRow();
        if(startRow<=start.getRow()) {
            r1+=count;
            r2+=count;
        }
        else if(startRow<=(append?end.getRow()+1:end.getRow())) {
            r2+=count;
        }
        if(r2>1048575) {
            r2=1048575;
            if(r1>1048575) {
                return null;
            }
        }
        return new CellRefRange(new CellRef(start.getColumn(), r1), new CellRef(end.getColumn(), r2));
    }

    public static CellRefRange deleteRowRange(CellRefRange cellRef, int startRow, int count) {
    	int endRow = (startRow + count)-1;
    	int row1 = cellRef.getStart().getRow();
        int row2 = cellRef.getEnd().getRow();
        if(startRow<=row2) {
            if(startRow<=row1&&endRow>=row2) {
                return null;                                // merged cell is completely removed
            }
            else if(endRow<row1) {
                row1 -= count;                              // merged cell is completely moved up by count
                row2 -= count;
            }
            else if(startRow>row1&&endRow<=row2) {
                row2 -= count;                              // merged cell is shortened by delete count
            }
            else if(startRow>row1) {
                row2 = startRow-1;                          // new end is to be set
            }
            else {
                row1 = startRow;                            // new start is to be set
                row2 -= count;
            }
        }
        return new CellRefRange(new CellRef(cellRef.getStart().getColumn(), row1), new CellRef(cellRef.getEnd().getColumn(), row2));
    }

    public static CellRefRange insertColumnRange(CellRefRange cellRefRange, int startColumn, int count, boolean append) {
        final CellRef start = cellRefRange.getStart();
        final CellRef end = cellRefRange.getEnd();
        int c1 = start.getColumn();
        int c2 = end.getColumn();
        if(startColumn<=start.getColumn()) {
            c1+=count;
            c2+=count;
        }
        else if(startColumn<=(append?end.getColumn()+1:end.getColumn())) {
            c2+=count;
        }
        if(c2>16383) {
            c2=16383;
            if(c1>16383) {
                return null;
            }
        }
        return new CellRefRange(new CellRef(c1, start.getRow()), new CellRef(c2, end.getRow()));
    }

    public static CellRefRange deleteColumnRange(CellRefRange cellRef, int startColumn, int count) {
    	int endColumn = (startColumn+count)-1;
        int column1 = cellRef.getStart().getColumn();
        int column2 = cellRef.getEnd().getColumn();
        if(startColumn<=column2) {
            if(startColumn<=column1&&endColumn>=column2) {
                return null;                                // merged cell is completely removed
            }
            else if(endColumn<column1) {
                column1 -= count;                           // merged cell is completely moved up by count
                column2 -= count;
            }
            else if(startColumn>column1&&endColumn<=column2) {
                column2 -= count;                           // merged cell is shortened by delete count
            }
            else if(startColumn>column1) {
                column2 = startColumn-1;                    // new end is to be set
            }
            else {
                column1 = startColumn;                      // new start is to be set
                column2 -= count;
            }
        }
        return new CellRefRange(new CellRef(column1, cellRef.getStart().getRow()), new CellRef(column2, cellRef.getEnd().getRow()));
    }

    @Override
    public CellRefRange clone() {
        return new CellRefRange(start.clone(), end.clone());
    }

    @Override
    public String toString() {
        return "CellRefRange [start=" + start + ", end=" + end + "]";
    }

    public static String getCellRefRange(CellRefRange cellRefRange) {
        return getCellRefRange(cellRefRange.getStart().getColumn(), cellRefRange.getStart().getRow(), cellRefRange.getEnd().getColumn(), cellRefRange.getEnd().getRow());
    }

    public static CellRefRange createCellRefRangeWithSheet(String cellRefRange, String sheetSeparator) {
        final int indexOf = cellRefRange.lastIndexOf(sheetSeparator);
        final String range = cellRefRange.substring(indexOf + 1);
        return createCellRefRange(range);
    }

    public static String getCellRefRange(int startColumn, long startRow, int endColumn, int endRow) {
        StringBuilder buffer = new StringBuilder(17);
        CellRef.addCellRef(buffer, startColumn, startRow);
        if(startColumn!=endColumn || startRow!=endRow) {
            buffer.append(':');
            CellRef.addCellRef(buffer, endColumn, endRow);
        }
        return buffer.toString();
    }
/*
    public static String getCellRefRange(JSONObject range) throws JSONException {
        final JSONArray start = range.getJSONArray(OCKey.START.value());
        final JSONArray end = range.optJSONArray(OCKey.END.value());
        final CellRef startRef = new CellRef(start.getInt(0), start.getInt(1));
        final CellRef endRef = (end==null) ? startRef : new CellRef(end.getInt(0), end.getInt(1));
        return SmlUtils.getCellRefRange(new CellRefRange(startRef, endRef));
    }
*/
    public static String getAbsoluteCellRefRange(int startColumn, long startRow, int endColumn, int endRow) {
        StringBuilder buffer = new StringBuilder(21);
        buffer.append('$');
        CellRef.addCellRefColumn(buffer, startColumn);
        buffer.append('$');
        CellRef.addCellRefRow(buffer, startRow);
        buffer.append(':');
        buffer.append('$');
        CellRef.addCellRefColumn(buffer, endColumn);
        buffer.append('$');
        CellRef.addCellRefRow(buffer, endRow);
        return buffer.toString();
    }

    public static String getAbsoluteCellRefRange(CellRefRange cellRefRange) {
        return CellRefRange.getAbsoluteCellRefRange(cellRefRange.getStart().getColumn(), cellRefRange.getStart().getRow(), cellRefRange.getEnd().getColumn(), cellRefRange.getEnd().getRow());
    }

    public Interval createInterval(boolean columns) {
        return columns ? Interval.createInterval(columns, getStart().getColumn(), getEnd().getColumn()) : Interval.createInterval(columns, getStart().getRow(), getEnd().getRow());
    }

    public static CellRefRange createFromInterval(Interval interval, int min, int max) {
        if(interval.isColumnInterval()) {
            return new CellRefRange(interval.getMin().getValue(), min, interval.getMax().getValue(), max);
        }
        return new CellRefRange(min, interval.getMin().getValue(), max, interval.getMax().getValue());
    }

    public static CellRefRange createCellRefRange(String range) {
        if(range==null)
            return null;
        final CellRef start = CellRef.createCellRef(range, 0, 0);
        if(start==null)
            return null;
        int indexOfSeparator = range.indexOf(':');
        if(indexOfSeparator==-1) {
            return new CellRefRange(start, new CellRef(start.getColumn(), start.getRow()));
        }
        if(indexOfSeparator<2||(indexOfSeparator+1)>=range.length())
            return null;
        final CellRef end = CellRef.createCellRef(range.substring(indexOfSeparator+1), 16383, 1048575);
        if(end==null)
            return null;
        return new CellRefRange(start, end);
    }

    // if ignoreFullRange is not null the range is not changed if the range length is as long as ignoreFullRange
    public static void insertRows(List<? extends ICellRefRangeAccess> list, int start, int count, boolean append, Integer ignoreFullRange) {
        for(int i=0;i<list.size();i++) {
            final ICellRefRangeAccess refRangeAccess = list.get(i);
            final CellRefRange cellRefRange = refRangeAccess.getCellRefRange(true);
            if(ignoreFullRange==null||cellRefRange.getRows()!=ignoreFullRange) {
                final CellRefRange newMergeCell = CellRefRange.insertRowRange(cellRefRange, start, count, append);
                if(newMergeCell==null) {
                    list.remove(i--);
                }
                else {
                    refRangeAccess.setCellRefRange(newMergeCell);
                }
            }
        }
    }

    // if ignoreFullRange is not null the range is not changed if the range length is as long as ignoreFullRange
    public static void deleteRows(List<? extends ICellRefRangeAccess> list, int start, int count, boolean removeSingleRange, Integer ignoreFullRange) {
        for(int i=0;i<list.size();i++) {
            final ICellRefRangeAccess refRangeAccess = list.get(i);
            final CellRefRange cellRefRange = refRangeAccess.getCellRefRange(true);
            if(ignoreFullRange==null||cellRefRange.getRows()!=ignoreFullRange) {
                final CellRefRange newMergeCell = CellRefRange.deleteRowRange(cellRefRange, start, count);
                if(newMergeCell==null) {
                    list.remove(i--);
                }
                else if(removeSingleRange&&newMergeCell.getStart().equals(newMergeCell.getEnd())) {
                    list.remove(i--);
                }
                else {
                    refRangeAccess.setCellRefRange(newMergeCell);
                }
            }
        }
    }

    // if ignoreFullRange is not null the range is not changed if the range length is as long as ignoreFullRange
    public static void insertColumns(List<? extends ICellRefRangeAccess> list, int start, int count, boolean append, Integer ignoreFullRange) {
        for(int i=0;i<list.size();i++) {
            final ICellRefRangeAccess refRangeAccess = list.get(i);
            final CellRefRange cellRefRange = refRangeAccess.getCellRefRange(true);
            if(ignoreFullRange==null||cellRefRange.getColumns()!=ignoreFullRange) {
                final CellRefRange newMergeCell = CellRefRange.insertColumnRange(cellRefRange, start, count, append);
                if(newMergeCell==null) {
                    list.remove(i--);
                }
                else {
                    refRangeAccess.setCellRefRange(newMergeCell);
                }
            }
        }
    }

    // if ignoreFullRange is not null the range is not changed if the range length is as long as ignoreFullRange
    public static void deleteColumns(List<? extends ICellRefRangeAccess> list, int start, int count, boolean removeSingleRange, Integer ignoreFullRange) {
        for(int i=0;i<list.size();i++) {
            final ICellRefRangeAccess refRangeAccess = list.get(i);
            final CellRefRange cellRefRange = refRangeAccess.getCellRefRange(true);
            if(ignoreFullRange==null||cellRefRange.getColumns()!=ignoreFullRange) {
                final CellRefRange newMergeCell = CellRefRange.deleteColumnRange(cellRefRange, start, count);
                if(newMergeCell==null) {
                    list.remove(i--);
                }
                else if(removeSingleRange&&newMergeCell.getStart().equals(newMergeCell.getEnd())) {
                    list.remove(i--);
                }
                else {
                    refRangeAccess.setCellRefRange(newMergeCell);
                }
            }
        }
    }
}
