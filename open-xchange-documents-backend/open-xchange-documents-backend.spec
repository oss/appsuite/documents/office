
Name:          open-xchange-documents-backend
BuildArch:     noarch
#!BuildIgnore: post-build-checks
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
BuildRequires: open-xchange-grizzly
BuildRequires: open-xchange-oauth
BuildRequires: open-xchange-documentconverter-api
BuildRequires: open-xchange-documents-templates
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       The Open-Xchange backend office extension
AutoReqProv:   no
Requires:      open-xchange-oauth >= @OXVERSION@
Requires:      open-xchange-documentconverter-api >= @OXVERSION@
Requires:      open-xchange-file-distribution >= @OXVERSION@
Requires:      open-xchange-sessionstorage-hazelcast >= @OXVERSION@
Requires:      open-xchange-documents-templates >= @OXVERSION@
Obsoletes:     open-xchange-documents-core < 7.4.2
Obsoletes:     open-xchange-calcengineserver < 7.4.2
Obsoletes:     calcengine < 1.7
Provides:      open-xchange-documents-backend = %{version}
Provides:      open-xchange-documents-core = %{version}
Provides:      open-xchange-calcengineserver = %{version}
Provides:      calcengine = 1.7
Conflicts:     open-xchange-realtime-core
Obsoletes:     open-xchange-realtime-core < 7.10.3
Conflicts:     open-xchange-realtime-json
Obsoletes:     open-xchange-realtime-json < 7.10.3

%description
This package contains the backend components for the office productivity

Authors:
--------
    Open-Xchange

%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
. /opt/open-xchange/lib/oxfunctions.sh
CONFFILES="settings/office.properties hazelcast/officeDocumentDirectory.properties"
for FILE in $CONFFILES; do
    ox_move_config_file /opt/open-xchange/etc/groupware /opt/open-xchange/etc $FILE
done
if [ ${1:-0} -eq 2 ]; then
    # only when updating

    # prevent bash from expanding, see bug 13316
    GLOBIGNORE='*'

    # SoftwareChange_Request-2052
    ox_add_property io.ox/office//module/useLocalStorage true /opt/open-xchange/etc/settings/office.properties

    # SoftwareChange_Request-2053
    ox_add_property io.ox/office//module/templatePath /opt/open-xchange/templates/documents /opt/open-xchange/etc/settings/office.properties

    # SoftwareChange_Request-2937
    ox_add_property io.ox/office//module/documentRestoreEnabled true /opt/open-xchange/etc/settings/office.properties

    # SoftwareChange_Request-2947
    VALUE=$(ox_read_property com.openexchange.hazelcast.configuration.map.name /opt/open-xchange/etc/hazelcast/officeDocumentDirectory.properties)
    if [ "$VALUE" == "officeDocumentDirectory-1" ]; then
        ox_set_property com.openexchange.hazelcast.configuration.map.name officeDocumentDirectory-2 /opt/open-xchange/etc/hazelcast/officeDocumentDirectory.properties
    fi

    # SoftwareChange_Request-2948
    VALUE=$(ox_read_property com.openexchange.hazelcast.configuration.map.maxIdleSeconds /opt/open-xchange/etc/hazelcast/officeDocumentDirectory.properties)
    if [ "900" = "$VALUE" ]; then
        ox_set_property com.openexchange.hazelcast.configuration.map.maxIdleSeconds 1800 /opt/open-xchange/etc/hazelcast/officeDocumentDirectory.properties
    fi

    # SoftwareChange_Request-3331
    ox_add_property io.ox/office//spreadsheet/maxFormulas 10000 /opt/open-xchange/etc/settings/office.properties

    # SoftwareChange_Request-3589
    ox_add_property com.openexchange.capability.guard-docs false /opt/open-xchange/etc/documents.properties

    # SoftwareChange_Request-3665
    ox_add_property com.openexchange.office.maxDocumentFileSize 100 /opt/open-xchange/etc/documents.properties

    SCR=SCR-574
    if ox_scr_todo ${SCR}
    then
      pfile=/opt/open-xchange/etc/documents.properties
      bl_key=com.openexchange.office.upload.blacklist
      bl_value=127.0.0.1-127.255.255.255,localhost
      wl_key=com.openexchange.office.upload.whitelist
      ox_add_property ${bl_key} ${bl_value} ${pfile}
      ox_add_property ${wl_key} "" ${pfile}
      ox_scr_done ${SCR}
    fi

    SCR=SCR-589
    if ox_scr_todo ${SCR}; then
        pfile=/opt/open-xchange/etc/documents-collaboration-client.properties
        if ! contains "com.openexchange.dcs.client.database.jdbc.additionalPropertyName" ${pfile}; then
            cat <<EOF | (cd /opt/open-xchange/etc && patch --forward --no-backup-if-mismatch --reject-file=- --fuzz=3 >/dev/null)
diff --git a/documents-collaboration-client.properties b/documents-collaboration-client.properties
index 47ab86059..30565ed3b 100644
--- a/documents-collaboration-client.properties
+++ b/documents-collaboration-client.properties
@@ -16,6 +16,19 @@
 # Default: n/a
 com.openexchange.dcs.client.database.password=

+# The JDBC database driver related com.openexchange.dcs.client.database.jdbc.*
+# properties are overrides for configuration values, already specified at other locations
+# within the current deployment.
+# The default JDBC deployment configuration values are read first and can be overridden
+# here in case a dedicated DCS database server is used by this DCS client.
+# The default DCS database server JDBC properties are taken from the following database
+# configuration files in that order: dbconnector.yaml, configdb.properties.
+# If a JDBC specific default value should be used, the 'jdbcDefault' string value needs to be applied:
+# com.openexchange.dcs.client.database.jdbc.jdbcStringProperty=jdbcDefault
+# If additional JDBC properties, that might be available but not listed here, need to be set by the
+# administrator, those entries can be freely added to this configuration using the following pattern:
+# com.openexchange.dcs.client.database.jdbc.additionalPropertyName=additionalPropertyValue
+
 # Determines, if the server hostname is to be verified in case a
 # secured connection to the documents-collaboration server is
 # requested.
EOF
        fi
        ox_add_property com.openexchange.dcs.client.database.jdbc.useSSL "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.requireSSL "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.verifyServerCertificate "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.enabledTLSProtocols "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.clientCertificateKeyStoreUrl "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.clientCertificateKeyStoreType "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.clientCertificateKeyStorePassword "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.trustCertificateKeyStoreUrl "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.trustCertificateKeyStoreType "" ${pfile}
        ox_add_property com.openexchange.dcs.client.database.jdbc.trustCertificateKeyStorePassword "" ${pfile}
        ox_scr_done ${SCR}
    fi

    SCR=SCR-590
    if ox_scr_todo ${SCR}; then
        pfile=/opt/open-xchange/etc/documents-collaboration-client.properties
        ox_add_property com.openexchange.dcs.client.username "" ${pfile}
        ox_add_property com.openexchange.dcs.client.password "" ${pfile}
        ox_scr_done ${SCR}
    fi
fi

PROTECT=( documents-collaboration-client.properties )
for FILE in "${PROTECT[@]}"
do
    ox_update_permissions "/opt/open-xchange/etc/$FILE" root:open-xchange 640
done

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/etc/
%config(noreplace) /opt/open-xchange/etc/settings/office.properties
%config(noreplace) /opt/open-xchange/etc/documents.properties
%config(noreplace) /opt/open-xchange/etc/documents-collaboration-client.properties
%config(noreplace) /opt/open-xchange/etc/security/documents-collaboration-client.ks
%config(noreplace) /opt/open-xchange/etc/security/documents-collaboration-client.ts
%config(noreplace) /opt/open-xchange/etc/hazelcast/officeDocumentDirectory.properties
%config(noreplace) /opt/open-xchange/etc/hazelcast/officeDocSaveStateDirectory.properties
%config(noreplace) /opt/open-xchange/etc/hazelcast/officeDocumentCleanupLock.properties
%config(noreplace) /opt/open-xchange/etc/hazelcast/officeDocumentResources.properties
%config(noreplace) /opt/open-xchange/etc/hazelcast/rt2DocOnNodeMap.properties
%config(noreplace) /opt/open-xchange/etc/hazelcast/rt2NodeHealthMap.properties
/opt/open-xchange/etc/security/documents.list
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*
%dir /opt/open-xchange/sbin/
/opt/open-xchange/sbin/*
/opt/open-xchange/templates/file-icon-default.png
/opt/open-xchange/templates/file-icon-presentation.png
/opt/open-xchange/templates/file-icon-spreadsheet.png
/opt/open-xchange/templates/file-icon-text.png
/opt/open-xchange/templates/notify.comment.mention.html.tmpl

%changelog
* Mon Feb 01 2021 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.10.5 release
* Fri Jan 15 2021 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.10.5 release
* Thu Dec 17 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview of 7.10.5 release
* Fri Nov 27 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.10.5 release
* Tue Oct 06 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.5 release
* Tue Jul 28 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.10.4 release
* Tue Jun 30 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview of 7.10.4 release
* Wed May 20 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.10.4 release
* Mon Feb 03 2020 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.4
* Thu Nov 28 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.10.3 release
* Wed Jun 19 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.3 release
* Fri May 10 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview for 7.10.1 release
* Mon Sep 10 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.1
* Mon Jun 25 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth preview for 7.10.0 release
* Thu Apr 19 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth preview for 7.10.0 release
* Tue Apr 03 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth preview for 7.10.0 release
* Tue Feb 20 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third preview for 7.10.0 release
* Fri Feb 02 2018 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview for 7.10.0 release
* Mon Dec 04 2017 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.10.0 release
* Tue May 16 2017 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Secpmd preview of 7.8.4 release
* Mon Apr 03 2017 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second release cadidate for 7.8.3 release
* Thu Nov 24 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First release cadidate for 7.8.3 release
* Tue Nov 15 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third preview of 7.8.3 release
* Sat Oct 29 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second preview of 7.8.3 release
* Fri Oct 14 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.8.2 release
* Tue Jun 14 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.2 release
* Sat May 07 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.8.2 release
* Wed Mar 30 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.8.1 release
* Thu Mar 24 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.1 release
* Tue Mar 15 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth preview of 7.8.1 release
* Fri Mar 04 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth preview of 7.8.1 release
* Sat Feb 20 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.8.1 release
* Tue Feb 02 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.8.1 release
* Tue Jan 26 2016 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.1 release
* Fri Oct 09 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.8.1 release
* Fri Oct 02 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.8.0 release
* Fri Sep 25 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.8.0 release
* Fri Sep 18 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.8.0 release
* Mon Sep 07 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.8.0 release
* Fri Aug 21 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.8.0 release
* Fri Aug 21 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.8.0 release
* Fri Jul 03 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Build for patch 2015-07-10
* Mon Mar 16 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.8.0 release
* Mon Mar 09 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Twelfth candidate for 7.6.2 release
* Fri Mar 06 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Eleventh candidate for 7.6.2 release
* Wed Mar 04 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Tenth candidate for 7.6.2 release
* Tue Mar 03 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Ninth candidate for 7.6.2 release
* Tue Feb 24 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Eighth candidate for 7.6.2 release
* Wed Feb 11 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Seventh candidate for 7.6.2 release
* Fri Jan 30 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.6.2 release
* Tue Jan 27 2015 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.6.2 release
* Fri Dec 12 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.6.2 release
* Fri Dec 05 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.6.2 release
* Fri Nov 21 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.6.2 release
* Tue Nov 04 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Ninth candidate for 7.6.1 release
* Fri Oct 31 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.6.2 release
* Thu Oct 30 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Eighth candidate for 7.6.1 release
* Mon Oct 27 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Seventh candidate for 7.6.1 release
* Tue Oct 21 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.6.1 release
* Thu Oct 16 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.6.2 release
* Tue Oct 14 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.6.1 release
* Fri Oct 10 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.6.1 release
* Thu Oct 02 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.6.1 release
* Tue Sep 16 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.6.1 release
* Wed Sep 10 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.6.1 release
* Wed Aug 20 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Build for patch 2014-08-25
* Tue Aug 19 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Build for patch 2014-08-25
* Mon Jul 21 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.6.1
* Tue Jul 15 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Build for patch 2014-07-21
* Tue Jul 08 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Build for patch 2014-07-17
* Tue Jun 24 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Seventh candidate for 7.6.0 release
* Fri Jun 20 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.6.0 release
* Fri Jun 13 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.6.0 release
* Fri May 30 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.6.0 release
* Fri May 16 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.6.0 release
* Mon May 05 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.6.0 release
* Fri Apr 11 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.6.0 release
* Thu Mar 27 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.4.2 Spreadsheet Final release
* Wed Mar 26 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.4.2 Spreadsheet Final release
* Wed Feb 26 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.4.2 spreadsheet release
* Wed Feb 12 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.6.0
* Fri Feb 07 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.4.2 release
* Thu Feb 06 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.4.2 release
* Tue Feb 04 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.4.2 release
* Thu Jan 23 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.4.2 release
* Fri Jan 10 2014 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.4.2 release
* Mon Dec 23 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.4.2 release
* Thu Dec 19 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.4.2
* Tue Dec 10 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
preparation for 7.0.1 - sprint #19
* Wed Nov 20 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.4.1 release
* Fri Nov 15 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.4.1 release
* Thu Nov 07 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.4.1 release
* Wed Oct 23 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.4.1 release
* Thu Oct 10 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First sprint increment for 7.4.1 release
* Tue Sep 24 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Eleventh candidate for 7.4.0 release
* Fri Sep 20 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.4.1 release
* Fri Sep 20 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Tenth candidate for 7.4.0 release
* Thu Sep 12 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Ninth candidate for 7.4.0 release
* Sun Sep 01 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Eighth candidate for 7.4.0 release
* Tue Aug 27 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Seventh candidate for 7.4.0 release
* Fri Aug 23 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Sixth candidate for 7.4.0 release
* Mon Aug 19 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fifth candidate for 7.4.0 release
* Tue Aug 13 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.4.0 release
* Tue Aug 06 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third release candidate for 7.4.0
* Fri Aug 02 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second release candidate for 7.4.0
* Wed Jul 17 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.4.0 release
* Tue Jul 16 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.4.0
* Mon Jul 01 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.2.2 release
* Fri Jun 28 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.2.2 release
* Wed Jun 26 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Release candidate for 7.2.2 release
* Fri Jun 21 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second feature freeze for 7.2.2 release
* Mon Jun 17 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Feature freeze for 7.2.2 release
* Mon Jun 03 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First sprint increment for 7.2.2 release
* Wed May 29 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.2.2 release
* Mon May 27 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.2.2
* Wed May 22 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.2.1 release
* Wed May 15 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.2.1 release
* Mon Apr 22 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.2.1 release
* Mon Apr 15 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.4.0
* Mon Apr 15 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.2.1
* Wed Apr 10 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Fourth candidate for 7.2.0 release
* Mon Apr 08 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Third candidate for 7.2.0 release
* Tue Apr 02 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
Second candidate for 7.2.0 release
* Tue Apr 02 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
First candidate for 7.2.0 release
* Fri Mar 15 2013 Martin Hollmichel <martin.hollmichel@open-xchange.com>
prepare for 7.2.0
* Wed Sep 19 2012 Martin Hollmichel <martin.hollmichel@open-xchange.com>
first build
