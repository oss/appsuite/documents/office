/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.directory;


public class DocSaveState {

    private final DocResourceID docResourceId;
    private boolean saveInProgress;

    public DocSaveState(final DocResourceID docResourceId) {
        this.docResourceId = docResourceId;
        this.saveInProgress = false;
    }

    public DocSaveState(final DocResourceID docResourceId, boolean saveInProgress) {
        this.docResourceId = docResourceId;
        this.saveInProgress = saveInProgress;
    }

    public String getResourceId() {
        return this.docResourceId.toString();
    }

    public boolean getSaveInProgress() {
        return this.saveInProgress;
    }

    public void setSaveInProgress(boolean state) {
        this.saveInProgress = state;
    }
}
