/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.directory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.openexchange.office.tools.rt.EncodeUtility;

/**
 * Identifies a
 * @author carsten-driesner
 *
 */
public class DocRestoreID {

	private static final DocRestoreID EMPTY_DOC_RESTORE_ID = new DocRestoreID("", 0l); 
	
    public final static String DOC_RESTORE_SEPARATOR = ":";
    private final String docResourceId;
    private final long createdTimeStamp;

    /**
     * Initializes a new {@link DocRestoreID}.
     * @param id
     */
    private DocRestoreID(final String docRestoreId) {
        final String[] parts = docRestoreId.split(DOC_RESTORE_SEPARATOR);
        Validate.isTrue(parts.length == 2, "Provided DocRestoreID has not the correct format", (Object[])parts);
        docResourceId = parts[0];
        createdTimeStamp = Long.parseLong(parts[1]);
    }

    /**
     * Initializes a new {@link DocRestoreID}.
     * @param id
     * @param createdTimeStamp
     */
    private DocRestoreID(final String id, long createdTimeStamp) {
        this.docResourceId = id;
        this.createdTimeStamp = createdTimeStamp;
    }

    /**
     * Creates a string representation of a DocResourceID.
     * @return a sting which represents this DocResourceID instance
     */
    @Override
    public String toString() {
        return toString(docResourceId, createdTimeStamp);
    }

    public String getResourceID() { return this.docResourceId; }

    public long getCreated() { return this.createdTimeStamp; }

    @Override
    public boolean equals(final Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof DocRestoreID)) return false;

        DocRestoreID otherRestoreID = (DocRestoreID)other;
        return ((StringUtils.equals(docResourceId, otherRestoreID.docResourceId)) && (otherRestoreID.createdTimeStamp == this.createdTimeStamp));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((docResourceId == null) ? 0 : docResourceId.hashCode());
        result = prime * result + Long.valueOf(createdTimeStamp).hashCode();
        return result;
    }

    /**
     * Creates a DocResourceID which consists of two values:
     *  - context id, resource as provided by real-time via GroupDispatcher getID().getResource()
     *
     * @param contextID the context id where the document exists
     * @param resourceId the resource id provided by real-time
     * @return a DocResourceID instance which is associated with the document
     *  described by the three arguments.
     */
    public static DocRestoreID create(int contextID, final String resourceId, long createdTimeStamp) {
        return new DocRestoreID(contextID + "@" + resourceId, createdTimeStamp);
    }

    public static DocRestoreID createWithEncode(int contextID, final String resourceId, long createdTimeStamp) {
        return new DocRestoreID(contextID + "@" + EncodeUtility.encodeResourcePart(resourceId), createdTimeStamp);
    }

    public static DocRestoreID create(final String docResourceID, long createdTimeStamp) {
        return new DocRestoreID(docResourceID, createdTimeStamp);
    }

    public static DocRestoreID create(final String docRestoreID) {
        return new DocRestoreID(docRestoreID);
    }

    public static String toString(final String id, long created) {
        final StringBuffer strBuf = new StringBuffer(id);
        strBuf.append(DOC_RESTORE_SEPARATOR);
        strBuf.append(created);
        return strBuf.toString();
    }
    
    public static DocRestoreID getEmptyDocRestoryID() {
    	return EMPTY_DOC_RESTORE_ID;
    }
}
