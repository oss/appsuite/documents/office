/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.directory;

import java.util.HashSet;
import java.util.Set;

public class DocumentResources implements Cloneable, IStreamIDCollection {

    private final DocRestoreID docRestoreId;
    private final String uniqueInstanceId;
    private Set<String> docResources;
    private long timeStamp;

    public DocumentResources(final DocRestoreID docRestoreId, final long timeStamp, final String uniqueInstanceId, final Set<String> docResources) {
        this.docRestoreId = docRestoreId;
        this.timeStamp = timeStamp;
        this.uniqueInstanceId = uniqueInstanceId;
        this.docResources = docResources;
    }

    public final DocRestoreID getDocRestoreId() {
        return this.docRestoreId;
    }

    public final Set<String> getResources() {
        return this.docResources;
    }

    public void setResources(final Set<String> docResources) {
        this.docResources = docResources;
    }

    @Override
    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUniqueInstanceId() {
        return this.uniqueInstanceId;
    }

    @Override
    public DocumentResources clone() {
        DocumentResources copy = new DocumentResources(this.docRestoreId, this.timeStamp, this.uniqueInstanceId, null);
        Set<String> setCopied = new HashSet<String>(docResources.size());
        for (String streamID : docResources) {
            setCopied.add(streamID);
        }
        copy.setResources(setCopied);
        return copy;
    }

    @Override
    public Set<String> getStreamIDs() {
        return docResources;
    }

    @Override
    public void touch(long timeStamp) {
        this.setTimeStamp(timeStamp);
    }

    @Override
    public DocRestoreID getUniqueCollectionID() {
        return this.docRestoreId;
    }

}
