/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.directory;

import com.openexchange.office.tools.rt.EncodeUtility;


public class DocResourceID {

    private final String docResourceId;

    /**
     * Initializes a new {@link DocResourceID}.
     * @param id
     */
    private DocResourceID(final String id) {
        this.docResourceId = id;
    }

    /**
     * Creates a string representation of a DocResourceID.
     * @return a sting which represents this DocResourceID instance
     */
    @Override
    public String toString() {
        return this.docResourceId;
    }

    @Override
    public boolean equals(final Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof DocResourceID)) return false;

        DocResourceID otherDocResourceID = (DocResourceID)other;
        return (otherDocResourceID.docResourceId.equals(docResourceId));
    }

    @Override
    public int hashCode() {
        return (null != docResourceId) ? docResourceId.hashCode() : 0;
    }

    /**
     * Creates a DocResourceID which consists of two values:
     *  - context id, resource as provided by real-time via GroupDispatcher getID().getResource()
     *
     * @param contextID the context id where the document exists
     * @param resourceId the resource id provided by real-time
     * @return a DocResourceID instance which is associated with the document
     *  described by the three arguments.
     */
    public static DocResourceID createDocResourceID(final String contextID, final String resourceId) {
        return new DocResourceID(contextID + "@" + resourceId);
    }

    public static DocResourceID createDocResourceAndEncodeResPart(final String contextID, final String resourceId) {
        return new DocResourceID(contextID + "@" + EncodeUtility.encodeResourcePart(resourceId));
    }

    public static DocResourceID createDocResourceID(final String docResourceID) {
        return new DocResourceID(docResourceID);
    }
}
