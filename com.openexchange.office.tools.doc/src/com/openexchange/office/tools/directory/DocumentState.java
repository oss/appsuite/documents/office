/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.directory;

import java.util.Date;


public class DocumentState {

    private final DocRestoreID docRestoreId;
    private String uniqueInstanceId;
    private boolean active;
    private long timeStamp;
    private String fileName;
    private String mimeType;
    private String backupManagedDocFileId;
    private String backupManagedOpsFileId;
    private int currentOperationStateNumber;
    private String currentVersion;
    private int baseOperationStateNumber;
    private String baseVersion;

    /**
     *
     * @param docResourceId
     */
    public DocumentState(final DocRestoreID docRestoreId, final String uniqueInstanceId) {
        this.docRestoreId = docRestoreId;
        this.uniqueInstanceId = uniqueInstanceId;
        this.active = false;
        this.timeStamp = new Date().getTime();
        this.fileName = null;
        this.mimeType = null;
        this.backupManagedDocFileId = null;
        this.backupManagedOpsFileId = null;
        currentVersion = null;
        baseVersion = null;
    }

    /**
     *
     * @param docResourceId
     * @param backupManagedFileId
     * @param operationStateNumber
     * @param version
     */
    public DocumentState(final DocRestoreID docResourceId, final String uniqueInstanceId, int operationStateNumber, final String version) {
    	this(docResourceId, uniqueInstanceId);

        this.baseOperationStateNumber = operationStateNumber;
        this.baseVersion = version;
        this.currentOperationStateNumber = operationStateNumber;
        this.currentVersion = version;
    }

    /**
     *
     * @param docResourceId
     * @param backupManagedFileId
     * @param operationStateNumber
     * @param version
     */
    public DocumentState(final DocRestoreID docResourceId,
                         final String uniqueInstanceId,
                         final boolean active,
                         final long timeStamp,
                         final String fileName,
                         final String mimeType,
                         final String backupDocId,
                         final String backupOpsId,
    		             int baseOSN,
    		             final String baseVersion,
    		             int currentOSN,
    		             final String currentVersion) {
    	this(docResourceId, uniqueInstanceId);

    	this.active = active;
    	this.timeStamp = timeStamp;
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.backupManagedDocFileId = backupDocId;
        this.backupManagedOpsFileId = backupOpsId;
        this.baseOperationStateNumber = baseOSN;
        this.baseVersion = baseVersion;
        this.currentOperationStateNumber = currentOSN;
        this.currentVersion = currentVersion;
    }

    public final DocRestoreID getRestoreId() { return this.docRestoreId; }

    public String getUniqueInstanceId() { return this.uniqueInstanceId; }

    public void setUniqueInstanceId(final String uniqueInstanceId) { this.uniqueInstanceId = uniqueInstanceId; }

    public boolean isActive() { return this.active; }

    public void setActive(boolean active) { this.active = active; }

    public long getTimeStamp() { return this.timeStamp; }

    public void setTimeStamp(long timeStamp) { this.timeStamp = timeStamp; }

    public String getFileName() { return this.fileName; }

    public String getMimeType() { return this.mimeType; }

    public String getManagedDocFileId() { return this.backupManagedDocFileId; }

    public void setManagedDocFileId(final String managedFileId) { this.backupManagedDocFileId = managedFileId; }

    public String getManagedOpsFileId() { return this.backupManagedOpsFileId; }

    public void setFileName(final String fileName) { this.fileName = fileName; }

    public void setMimeType(final String mimeType) { this.mimeType = mimeType; }

    public void setManagedOpsFileId(final String managedFileId) { this.backupManagedOpsFileId = managedFileId; }

    public int getCurrentOSN() { return this.currentOperationStateNumber; }

    public void setCurrentOSN(int osn) { this.currentOperationStateNumber = osn; }

    public String getCurrentVersion() { return this.currentVersion; }

    public void setCurrentVersion(final String version) { this.currentVersion = version; }

    public String getBaseVersion() { return this.baseVersion; }

    public void setBaseVersion(final String version) { this.baseVersion = version; }

    public int getBaseOSN() { return this.baseOperationStateNumber; }

    public void setBaseOSN(int osn) { this.baseOperationStateNumber = osn; }
}
