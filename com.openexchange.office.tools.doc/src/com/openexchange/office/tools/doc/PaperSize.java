/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Defines certain paper sizes and helper functions.
 *
 * @author Carsten Driesner
 * @since 7.8.3
 */
public enum PaperSize {

    A4,
    LETTER;

    /**
     * Static unmodifiable map language to default paper size.
     * Don't forget to update getPageSize() if you introduce
     * a new paper size entry!!
     */
    public static final Map<String, PaperSize> PAPERSIZE_LANGMAP;
    static {
        // supported locales with the default format
        final Map<String, PaperSize> tmp = new HashMap<String, PaperSize>();
        tmp.put("en-US", PaperSize.LETTER);
        tmp.put("en-GB", PaperSize.A4);
        tmp.put("de-DE", PaperSize.A4);
        tmp.put("fr-FR", PaperSize.A4);
        tmp.put("fr-CA", PaperSize.LETTER);
        tmp.put("es-ES", PaperSize.A4);
        tmp.put("es-MX", PaperSize.LETTER);
        tmp.put("nl-NL", PaperSize.A4);
        tmp.put("pl-PL", PaperSize.A4);
        tmp.put("ja-JP", PaperSize.A4);
        tmp.put("it-IT", PaperSize.A4);
        tmp.put("zh-CN", PaperSize.A4);
        tmp.put("zh-TW", PaperSize.A4);
        tmp.put("hu-HU", PaperSize.A4);
        tmp.put("sk-SK", PaperSize.A4);
        tmp.put("cs-CZ", PaperSize.A4);
        tmp.put("lv-LV", PaperSize.A4);
        tmp.put("ro-RO", PaperSize.A4);
        tmp.put("pt-BR", PaperSize.LETTER);
        tmp.put("sv-SE", PaperSize.A4);
        tmp.put("da-DK", PaperSize.A4);
        tmp.put("ru-RU", PaperSize.A4);

        PAPERSIZE_LANGMAP = Collections.unmodifiableMap(tmp);
    }

    /**
     * Retrieves the default page size for a specific language.
     *
     * @param sLanguageCode the language code (ISO)
     * @return the default page size in that country or PaperSize.A4 if language is not mapped
     */
    public static PageSize getPageSize(final String sLanguageCode) {
        PaperSize aPaperSize = PAPERSIZE_LANGMAP.get(sLanguageCode);
        return getPageSize((aPaperSize == null) ? PaperSize.A4 : aPaperSize);
    }

    /**
     * Retrieves the page size of paper size.
     *
     * @param ePaperSize a paper size
     * @return the page size in 1/100th mm
     */
    public static PageSize getPageSize(PaperSize ePaperSize) {
        PageSize aPageSize = null;

        switch (ePaperSize) {
            case A4: aPageSize = PageSize.A4; break;
            case LETTER: aPageSize = PageSize.LETTER; break;
            // fallback is A4
            default: aPageSize = PageSize.A4;
        }
        return aPageSize;
    }
};
