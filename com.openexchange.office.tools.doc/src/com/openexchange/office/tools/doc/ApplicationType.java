/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;


/**
 * Enumeration for OX Documents application types.
 *
 * @author Carsten Driesner
 */
public enum ApplicationType {

    APP_NONE,
	APP_TEXT,
    APP_SPREADSHEET,
    APP_PRESENTATION;

	public final static String APP_TEXT_STRING = "text";
	public final static String APP_SPREADSHEET_STRING = "spreadsheet";
	public final static String APP_PRESENTATION_STRING = "presentation";
	
    public int intValue() {
    	return this.ordinal();
    }

    public String toString() {
    	return enumToString(this);
    }

	/**
     * Provides the pre-defined text for a provided application type.
     *
     * @param type
     *  The application type which should be mapped to a string.
     *
     * @return
     *  The application type as a string or null if the type is not specific
     *  (e.g. APP_NONE).
     */
	static public String enumToString(ApplicationType type) {
    	if (type == APP_TEXT) {
    		return "text";
    	} else if (type == APP_SPREADSHEET) {
    		return APP_SPREADSHEET_STRING;
    	} else if (type == APP_PRESENTATION) {
    		return APP_PRESENTATION_STRING;
    	}
    	return null;
    }

    /**
     * Converts a string to a possible application type.
     *
     * @param typeString
     *  A string containing a possible application type.
     *
     * @return
     *  The application type if the string contains the pre-defined text
     *  for the possible application types. Returns APP_NONE if the
     *  string maps to no known application type.
     */
    static public ApplicationType stringToEnum(String typeString) {
    	ApplicationType type = APP_NONE;

    	if ((null != typeString) && (typeString.length() > 0)) {
    		if (APP_TEXT_STRING.equalsIgnoreCase(typeString)) {
    			return APP_TEXT;
    		} else if (APP_SPREADSHEET_STRING.equalsIgnoreCase(typeString)) {
    			return APP_SPREADSHEET;
    		} else if (APP_PRESENTATION_STRING.equalsIgnoreCase(typeString)) {
    			return APP_PRESENTATION;
    		}
    	}

    	return type;
    }

    /**
     * Maps a document type to application type.
     *
     * @param docType
     *  The document type which should be mapped to the application type.
     *
     * @return
     *  The application type mapped from the document type or APP_NONE if
     *  no mapping is available.
     */
    static public ApplicationType documentTypeToApplicationType(DocumentType docType) {
    	if (docType == DocumentType.TEXT)
    		return ApplicationType.APP_TEXT;
    	else if (docType == DocumentType.SPREADSHEET)
    		return ApplicationType.APP_SPREADSHEET;
    	else if (docType == DocumentType.PRESENTATION)
    		return ApplicationType.APP_PRESENTATION;
    	else
    		return ApplicationType.APP_NONE;
    }

    /**
     * Maps a document format to application type.
     *
     * @param docFormat
     *  The document format which should be mapped to the application type.
     *
     * @return
     *  The application type mapped from the document format or APP_NONE if
     *  no mapping is available.
     */
    static public ApplicationType documentFormatToApplicationType(DocumentFormat docFormat) {
        if (docFormat == DocumentFormat.DOCX)
            return ApplicationType.APP_TEXT;
        else if (docFormat == DocumentFormat.ODP)
            return ApplicationType.APP_PRESENTATION;
        else if (docFormat == DocumentFormat.ODS)
            return ApplicationType.APP_SPREADSHEET;
        else if (docFormat == DocumentFormat.ODT)
            return ApplicationType.APP_TEXT;
        else if (docFormat == DocumentFormat.PPTX)
            return ApplicationType.APP_PRESENTATION;
        else if (docFormat == DocumentFormat.XLSX)
            return ApplicationType.APP_SPREADSHEET;
        else
            return ApplicationType.APP_NONE;
    }
}
