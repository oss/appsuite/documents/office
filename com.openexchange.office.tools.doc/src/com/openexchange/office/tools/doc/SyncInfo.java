/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Contains data that is needed for synchronization purposes between
 * client and server.
 *
 * {@link SyncInfo}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public class SyncInfo {

    public static final int     SYNC_UNKNOWN_DOCOSN = -1;
    public static final String  SYNC_FIRST_VERSION  = "1";
    public static final String  SYNC_INFO = "syncInfo";

    private static final String SYNC_FOLDERID = "folderId";
    private static final String SYNC_FILEID = "fileId";
    private static final String SYNC_DOCOSN = "document-osn";
    private static final String SYNC_FILEVERSION = "fileVersion";
    private static final String SYNC_SUPPORTSVERSIONS = "supportsVersions";
    private static final String SYNC_FILENAME = "fileName";
    private static final String SYNC_RESTOREID = "restore_id";
    private static final String SYNC_RENAME_NEEDS_RELOAD = "renameNeedsReload";

    // members
    private int          documentOperationStateNumber = SYNC_UNKNOWN_DOCOSN;
    private String       version;
    private final String folderId;
    private final String fileId;
    private String       fileName;
    private boolean      supportsVersions = true;
    private String       restoreID;
    private boolean      renameNeedsReload = false;

    public SyncInfo(String folderId, String fileId) {
        this.folderId = folderId;
        this.fileId = fileId;
    }

    public SyncInfo(final JSONObject jsonObject) throws Exception {
        folderId = jsonObject.getString(SYNC_FOLDERID);
        fileId = jsonObject.getString(SYNC_FILEID);
        version = jsonObject.getString(SYNC_FILEVERSION);
        documentOperationStateNumber = jsonObject.getInt(SYNC_FILEVERSION);
        supportsVersions = jsonObject.getBoolean(SYNC_SUPPORTSVERSIONS);
        fileName = jsonObject.getString(SYNC_FILENAME);
        restoreID = jsonObject.getString(SYNC_RESTOREID);
        renameNeedsReload = jsonObject.getBoolean(SYNC_RENAME_NEEDS_RELOAD);
    }

    public boolean isValidSyncInfo() {
        return (documentOperationStateNumber != SYNC_UNKNOWN_DOCOSN);
    }

    public void updateSyncInfo(final String ver, int docOSN) {
        this.version = ver;
        documentOperationStateNumber = docOSN;
    }

    public void updateFileVersion(final String ver) {
        this.version = ver;
    }

    public void updateDocmentOSN(int docOSN) {
        documentOperationStateNumber = docOSN;
    }

    public void setSupportsVersions(boolean supportsVersions) {
        this.supportsVersions = supportsVersions;
    }

    public void setRenameNeedsReload(boolean renameNeedsReload) {
    	this.renameNeedsReload = renameNeedsReload;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public void setRestoreID(final String restoreID) {
        this.restoreID = restoreID;
    }

    public JSONObject toJSON()
    	throws JSONException
    {
        final JSONObject jsonObject = new JSONObject();

        jsonObject.put(SYNC_FOLDERID, folderId);
        jsonObject.put(SYNC_FILEID, fileId);
        jsonObject.put(SYNC_FILEVERSION, (version == null) ? JSONObject.NULL : version);
        jsonObject.put(SYNC_DOCOSN, documentOperationStateNumber);
        jsonObject.put(SYNC_SUPPORTSVERSIONS, supportsVersions);
        jsonObject.put(SYNC_FILENAME, (fileName == null) ? JSONObject.NULL : fileName);
        jsonObject.put(SYNC_RESTOREID, (restoreID == null) ? JSONObject.NULL : restoreID);
        jsonObject.put(SYNC_RENAME_NEEDS_RELOAD, renameNeedsReload);

        return jsonObject;
    }

}
