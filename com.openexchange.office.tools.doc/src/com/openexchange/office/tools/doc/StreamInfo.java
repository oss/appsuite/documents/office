/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.tx.TransactionAware;

/**
 * Provides data associated with the input stream retrieved from the document.
 *
 * {@link StreamInfo}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public class StreamInfo implements Closeable {
    private static final Logger LOG = LoggerFactory.getLogger(StreamInfo.class);

    public static final String NO_USERNAME = "";
    public static final int NO_USERID   = -1;

    private ErrorCode errorCode = ErrorCode.NO_ERROR;
    private InputStream documentStream = null;
    private File metaData = null;
    private boolean writeProtected = false;
    private String lockedByUser = NO_USERNAME;
    private int lockedByUserId = NO_USERID;
    private DocumentFormat documentFormat = DocumentFormat.NONE;
    private IDBasedFileAccess fileAccess = null;
    private boolean decrypted = false;

    public StreamInfo() {
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public InputStream getDocumentStream() {
        return documentStream;
    }

    public File getMetaData() {
        return metaData;
    }

    public boolean isWriteProtected() {
        return writeProtected;
    }

    public boolean isDecrypted() {
        return decrypted;
    }

    public String getLockedByUser() {
        return this.lockedByUser;
    }

    public int getLockedByUserId() {
        return this.lockedByUserId;
    }

    public DocumentFormat getDocumentFormat() {
        return this.documentFormat;
    }

    public void setErrorCode(final ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public void setWriteProtected(boolean writeProtected) {
        this.writeProtected = writeProtected;
    }

    public void setDecrypted(boolean decrypted) {
        this.decrypted = decrypted;
    }

    public void setMetaData(final File metaData) {
        this.metaData = metaData;
    }

    public void setDocumentStream(final InputStream documentStream) {
        this.documentStream = documentStream;
    }

    public void setLockedByUser(final String lockedByUser) {
        this.lockedByUser = lockedByUser;
    }

    public void setLockedByUserId(final int lockedByUserId) {
        this.lockedByUserId = lockedByUserId;
    }

    public void setDocumentFormat(final DocumentFormat documentFormat) {
        this.documentFormat = documentFormat;
    }

    public void setFileAccess(final IDBasedFileAccess fileAccess) {
        this.fileAccess = fileAccess;
    }

    @Override
    public void close() throws IOException
    {
        if (null != this.fileAccess) {
            try {
                ((TransactionAware)this.fileAccess).finish();
            } catch (OXException e) {
                LOG.warn("Exception caught while trying to close IDBasedFileAccess instance.", e);
                throw new IOException(e);
            }
        }
    }
}
