/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

/**
 * Provides the page size of a document, where the width and height
 * are defined in 1/100th mm.
 *
 * @author Carsten Driesner
 * @since 7.8.3
 */
public class PageSize {

    /**
     * Pre-defined page sizes based on paper sizes.
     */
    public final static PageSize A4 = new PageSize(21000, 29700);
    public final static PageSize LETTER = new PageSize(21590, 27940);

    private final int width;
    private final int height;

    /**
     * Initializes a new PageSize with width and height.
     *
     * @param width the width in 1/100th mm
     * @param height the height in 1/100th mm
     */
    public PageSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Provides the page width in 1/100th mm.
     *
     * @return the page width in 1/100th mm.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Provides the page height in 1/100th mm.
     *
     * @return the page height in 1/100th mm.
     */
    public int getHeight() {
        return height;
    }
}
