/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.file.storage.File;

/**
 * Stores and provides meta data for a certain document.
 *
 * {@link DocumentMetaData}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public class DocumentMetaData implements Cloneable {

    final public static String META_BINARY_CONVERTED = "binary_converted";

    private String lockedByUser = "";
    private Date lockedUntil = null;
    private long fileSize = 0;
    private String fileName = "";
    private final String folderId;
    private final String fileId;
    private final Date lastModified;
    private final Date created;
    private final String title;
    private final String mimeType;
    private final String fileVersion;
    private final int lockedByUserId;
    private boolean isBinaryConverted = false;
    private Boolean optWriteProtected = null;

    /**
     * Create document meta with initial values.
     *
     * @param mimeType
     */
    public DocumentMetaData(final String fileName, final String mimeType, final int fileSize) {
        this.mimeType = mimeType;
        this.fileName = fileName;
        this.title = "";
        this.fileVersion = null;
        this.lockedByUserId = 0;
        this.lastModified = new Date();
        this.created = new Date();
        this.folderId = null;
        this.fileId = null;
        this.fileSize = fileSize;
    }

    /**
     *
     * @param metaData
     * @param lockedByUser
     * @param lockedByUserId
     */
    public DocumentMetaData(final File metaData, final String lockedByUser, int lockedByUserId) {
        this.lockedByUser = lockedByUser;
        this.lockedUntil = metaData.getLockedUntil();
        this.lockedByUserId = lockedByUserId;
        this.fileSize = metaData.getFileSize();
        this.fileName = metaData.getFileName();
        this.folderId = metaData.getFolderId();
        this.fileId = metaData.getId();
        this.lastModified = metaData.getLastModified();
        this.created = metaData.getCreated();
        this.title = metaData.getTitle();
        this.mimeType = metaData.getFileMIMEType();
        this.fileVersion = metaData.getVersion();

        setBinaryConverted(metaData.getMeta());
    }

    /**
     *
     * @param streamInfo
     */
    public DocumentMetaData(final StreamInfo streamInfo) {
        this.lockedByUser = streamInfo.getLockedByUser();
        this.lockedUntil = streamInfo.getMetaData().getLockedUntil();
        this.lockedByUserId = streamInfo.getLockedByUserId();
        this.fileSize = streamInfo.getMetaData().getFileSize();
        this.fileName = streamInfo.getMetaData().getFileName();
        this.folderId = streamInfo.getMetaData().getFolderId();
        this.fileId = streamInfo.getMetaData().getId();
        this.lastModified = streamInfo.getMetaData().getLastModified();
        this.created = streamInfo.getMetaData().getCreated();
        this.title = streamInfo.getMetaData().getTitle();
        this.mimeType = streamInfo.getMetaData().getFileMIMEType();
        this.fileVersion = streamInfo.getMetaData().getVersion();

        setBinaryConverted(streamInfo.getMetaData().getMeta());
    }

    /**
     *
     * @param docMetaData
     */
    private DocumentMetaData(final DocumentMetaData docMetaData) {
        this.lockedByUser = docMetaData.lockedByUser;
        this.lockedUntil = docMetaData.lockedUntil;
        this.lockedByUserId = docMetaData.lockedByUserId;
        this.fileSize = docMetaData.fileSize;
        this.fileName = docMetaData.fileName;
        this.folderId = docMetaData.folderId;
        this.fileId = docMetaData.fileId;
        this.lastModified = docMetaData.lastModified;
        this.created = docMetaData.created;
        this.title = docMetaData.title;
        this.mimeType = docMetaData.mimeType;
        this.fileVersion = docMetaData.fileVersion;
        this.isBinaryConverted = docMetaData.isBinaryConverted;
    }

	/**
	 * Creates a JSONObject from a DocumentMetaData instance.
	 *
	 * @param metaData
	 *  A DocumentMetaData instance which should be used to fill the JSONObject.
	 *
	 * @param last_opened [optional]
	 *  Additional information about the time of the last opened action. Can be
	 *  omitted.
	 *
	 * @return
	 *  A JSONObject with the necessary file descriptor properties or null if
	 *  this is not possible.
	 */
	static public JSONObject createJSONObject(final DocumentMetaData metaData, java.util.Date last_opened) {
	    JSONObject fileDescriptor = null;
	
	    if (null != metaData) {
	        try {
	            JSONObject tmp = new JSONObject();
	            // mandatory fields
	            tmp.put(File.Field.FOLDER_ID.getName(), metaData.getFolderId());
	            tmp.put(File.Field.ID.getName(), metaData.getFileId());
	            tmp.put(File.Field.FILENAME.getName(), metaData.getFileName());
	
	            // optional fields, at least some storages may not support them
	            if (metaData.getLastModified() != null) {
	                tmp.put(File.Field.LAST_MODIFIED.getName(), (metaData.getLastModified().getTime()));
	            }
	            if (metaData.getCreated() != null) {
	                tmp.put(File.Field.CREATED.getName(), (metaData.getCreated().getTime()));
	            }
	            tmp.put(File.Field.TITLE.getName(), metaData.getTitle());
	            tmp.put(File.Field.FILE_MIMETYPE.getName(), metaData.getMimeType());
	            tmp.put(FileDescriptor.LAST_OPENED, (null != last_opened) ? (last_opened.getTime()) : (new java.util.Date().getTime()));
	            fileDescriptor = tmp;
	        } catch (JSONException e) {
	            // nothing to do - we provide null in case of an exception
	        }
	    }
	    return fileDescriptor;
	}

    /**
     * Creates a new error class instance which contains the same values
     * as this instance.
     */
    @Override
    public DocumentMetaData clone() {
        return new DocumentMetaData(this);
    }

    /**
     * Sets the write protected state for this DocumentMetaData instance.
     * ATTENTION: This state is only valid for this instance and should
     * never be stored globally. The write protected state of a file
     * depends on the client context.
     *
     * @param isWriteProtected
     */
    public void setWriteProtectedState(boolean isWriteProtected) {
        optWriteProtected = new Boolean(isWriteProtected);
    }

    /**
     * Reset write protected state.
     */
    public void resetWriteProtectedState() {
        optWriteProtected = null;
    }

    /**
     * Provides the write protected state. The value can be null, if
     * the state is unknown.
     * ATTENTION: This state is only valid for this instance and should
     * never be stored globally. The write protected state of a file
     * depends on the client context.
     *
     * @return
     */
    public Boolean getWriteProtectedState() {
        return optWriteProtected;
    }

    /**
     * @param meta
     */
    protected void setBinaryConverted(final Map<String, Object> meta) {
        // set flag, that this document is initially created from a binary conversion
        if (null != meta) {
            final Boolean metaBinaryConverted = (Boolean) meta.get(META_BINARY_CONVERTED);

            // set flag once and instantly remove information at Map
            this.isBinaryConverted = (null != metaBinaryConverted) && metaBinaryConverted.booleanValue();
            meta.remove(META_BINARY_CONVERTED);
        }
    }

    /**
     * Provides the locking user of the document, if present.
     * @return
     */
    public String getLockedByUser() {
        return lockedByUser;
    }

    /**
     * Provides the lock until state of the document, if active.
     * @return
     */
    public Date getLockedUntil() {
        return lockedUntil;
    }

    /**
     * Provides the file name of the document.
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Provides the file size of the document.
     * @return
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * Provides the folder id of the document.
     * @return
     */
    public String getFolderId() {
        return folderId;
    }

    /**
     * Provides the file id of the document.
     * @return
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * Provides the date of the last modification.
     *
     * @return
     */
    public Date getLastModified() {
        return lastModified;
    }

    /**
     * Provides the created data of the document.
     *
     * @return
     */
    public Date getCreated() {
        return created;
    }

    /**
     * Provides the title of the document.
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Provides the mime type.
     *
     * @return
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Provides the file version.
     *
     * @return
     */
    public String getVersion() {
        return fileVersion;
    }

    /**
     * @return
     */
    public boolean isBinaryConverted() {
        return isBinaryConverted;
    }

    /**
     * Provides the user id of the user who locked it.
     *
     * @return
     */
    public int getLockedByUserId() {
        return lockedByUserId;
    }
}
