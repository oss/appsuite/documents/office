/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.file.storage.File;


/**
 * A file descriptor references a file in the appsuite environment, where
 * the file id and folder id are the unique key.
 *
 * @author Carsten Driesner
 *
 */
public class FileDescriptor {

    public final static String LAST_OPENED = "last_opened";

    private String m_fileId;
    private String m_folderId;
    private java.util.Date m_lastModified;
    private java.util.Date m_created;
    private java.util.Date m_lastOpened;
    private String m_title;
    private String m_mimeType;
    private String m_fileName;

    /**
     * Initializes the file descriptor instance with file and
     * folder id. Use isValid() to verify that the instance references
     * a file in the appsuite environment.
     *
     * @param fileId
     *  The file id of the file descriptor.
     *
     * @param folderId
     *  The folder id of the file descriptor.
     */
    public FileDescriptor(final String fileId, final String folderId) {
        m_fileId = fileId;
        m_folderId = folderId;
    }

    /**
     * Initializes the file descriptor using a JSONObject with file descriptor
     * properties. Use isValid() to verify that the instance references a file
     * in the appsuite environment.
     *
     * @param fileDescriptor
     */
    public FileDescriptor(final JSONObject fileDescriptor) {
        parseFileDescriptor(fileDescriptor);
    }

    /**
     * Provides the file id.
     *
     * @return
     */
    public final String getFileId() {
        return m_fileId;
    }

    /**
     * Provides the folder id.
     *
     * @return
     */
    public final String getFolderId() {
        return m_folderId;
    }

    /**
     * Provides the last modified date.
     *
     * @return
     */
    public final java.util.Date getLastModified() {
        return m_lastModified;
    }

    /**
     * Provides the created date.
     *
     * @return
     */
    public final java.util.Date getCreated() {
        return m_created;
    }

    /**
     * Provides the last opened date.
     *
     * @return
     */
    public final java.util.Date getLastOpened() {
        return m_lastOpened;
    }

    /**
     * Provides the title.
     *
     * @return
     */
    public final String getTitle() {
        return m_title;
    }

    /**
     * Provides the mime type.
     *
     * @return
     */
    public final String getMimeType() {
        return m_mimeType;
    }

    /**
     * Provides the file name.
     * @return
     */
    public final String getFileName() {
        return m_fileName;
    }

    /**
     * Determines if this file descriptor instance is valid and
     * references a file. It doesn't check if the descriptor references
     * a physical file.
     *
     * @return
     *  TRUE if the file descriptor contains a file and folder id, otherwise
     *  FALSE.
     */
    public boolean isValid() {
        return (!("".equals(m_fileId))) && (!("".equals(m_folderId)));
    }

    /**
     * Creates a JSONObject from a File metaData instance.
     *
     * @param metaData
     *  A File meta data instance which should be used to fill the JSONObject.
     *
     * @param last_opened [optional]
     *  Additional information about the time of the last opened action. Can be
     *  omitted.
     *
     * @return
     *  A JSONObject with the necessary file descriptor properties or null if
     *  this is not possible.
     */
    static public JSONObject createJSONObject(final File metaData, java.util.Date last_opened) {
        JSONObject fileDescriptor = null;

        if (null != metaData) {
            try {
                JSONObject tmp = new JSONObject();
                // mandatory fields
                tmp.put(File.Field.FOLDER_ID.getName(), metaData.getFolderId());
                tmp.put(File.Field.ID.getName(), metaData.getId());
                tmp.put(File.Field.FILENAME.getName(), metaData.getFileName());
                // it's safe to provide the mandatory fields - even if later an
                // exception is thrown
                fileDescriptor = tmp;

                // optional fields, at least some storages may not support them
                if (metaData.getLastModified() != null) {
                    tmp.put(File.Field.LAST_MODIFIED.getName(), (metaData.getLastModified().getTime()));
                }
                if (metaData.getCreated() != null) {
                    tmp.put(File.Field.CREATED.getName(), (metaData.getCreated().getTime()));
                }
                tmp.put(File.Field.TITLE.getName(), metaData.getTitle());
                tmp.put(File.Field.FILE_MIMETYPE.getName(), metaData.getFileMIMEType());
                tmp.put(LAST_OPENED, (null != last_opened) ? (last_opened.getTime()) : (new java.util.Date().getTime()));
                fileDescriptor = tmp;
            } catch (JSONException e) {
                // nothing to do - we provide null in case of an exception
            }
        }
        return fileDescriptor;
    }

    /**
     * Sets new file descriptor data from a JSONObject.
     *
     * @param fileDescriptor
     * @return
     *  TRUE if the JSONObject contains the minimal file descriptor properties
     *  otherwise FALSE.
     */
    private boolean parseFileDescriptor(final JSONObject fileDescriptor) {
        boolean result = false;

        if (null != fileDescriptor) {
            // mandatory
            m_fileId = fileDescriptor.optString(File.Field.ID.getName());
            m_folderId = fileDescriptor.optString(File.Field.FOLDER_ID.getName());
            // optional
            m_title = fileDescriptor.optString(File.Field.TITLE.getName());
            m_mimeType = fileDescriptor.optString(File.Field.FILE_MIMETYPE.getName());
            m_fileName = fileDescriptor.optString(File.Field.FILENAME.getName());

            long time = fileDescriptor.optLong(File.Field.LAST_MODIFIED.getName());
            if (time != 0) {
                m_lastModified = new java.util.Date(time);
            }
            time = fileDescriptor.optLong(File.Field.CREATED.getName());
            if (time != 0) {
                m_created = new java.util.Date(time);
            }
            time = fileDescriptor.optLong(LAST_OPENED);
            if (time != 0) {
                m_lastOpened = new java.util.Date(time);
            }

            result = isValid();
        }

        return result;
    }
}
