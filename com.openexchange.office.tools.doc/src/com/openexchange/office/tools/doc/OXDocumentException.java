/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import com.openexchange.exception.OXException;
import com.openexchange.office.tools.common.error.ErrorCode;

/**
 * Encapsulates the reason of a failure to process a document
 * specific request.
 *
 * {@link OXDocumentException}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public class OXDocumentException extends OXException {

    private static final long serialVersionUID = 1L;

    final private ErrorCode errorCode;

    public OXDocumentException(String message, ErrorCode eCode) {
        super(eCode.getCode(), message);
        errorCode = eCode;
    }

    public OXDocumentException(String message, Throwable e, ErrorCode eCode) {
        super(eCode.getCode(), message, e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
    }

    public OXDocumentException(Throwable e, ErrorCode eCode) {
        super(e);
        setStackTrace(e.getStackTrace());
        errorCode = eCode;
    }

    public OXDocumentException(ErrorCode eCode) {
        super(eCode.getCode(), "");
        errorCode = eCode;
    }

    /**
     * Provides the error code which describes the reason of the
     * document processing failure.
     *
     * @return
     *  The error code which describes the reason of the failure.
     */
    public ErrorCode getErrorcode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return getLogMessage();
    }

    @Override
    public String toString() {
        return getLogMessage();
    }

    /**
     * Gets the composed log message for this OXDocumentException
     * instance.
     *
     * @return
     *  the composed log message
     */
    public String getLogMessage() {
        final StringBuilder sb = new StringBuilder(256);

        // make sure that our error code is also logged
        sb.append("ErrorCode='");
        sb.append(errorCode.toString());
        sb.append("' ");

        sb.append(super.getLogMessage());

        return sb.toString();
    }
}
