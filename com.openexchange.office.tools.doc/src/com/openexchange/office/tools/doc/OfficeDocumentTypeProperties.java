/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import com.openexchange.office.tools.doc.DocumentFormatHelper.OfficeDocumentType;

public class OfficeDocumentTypeProperties {

    //-------------------------------------------------------------------------
    private String mimeType;
    private String module;
    private String extension;
    private OfficeDocumentType docType;
    private DocumentFormat docFormat;

    //-------------------------------------------------------------------------
    private OfficeDocumentTypeProperties(String mimeType, String module, String extension, OfficeDocumentType docType, DocumentFormat docFormat) {
        this.mimeType = mimeType;
        this.module = module;
        this.extension = extension;
        this.docType = docType;
        this.docFormat = docFormat;
    }

    //-------------------------------------------------------------------------
    public String getMimeType() { return mimeType; }

    //-------------------------------------------------------------------------
    public String getModule() { return module; }

    //-------------------------------------------------------------------------
    public String getExtension() { return extension; }

    //-------------------------------------------------------------------------
    public OfficeDocumentType getOfficeDocumentType() { return docType; }

    //-------------------------------------------------------------------------
    public DocumentFormat getDocumentFormat() { return docFormat; }

    //-------------------------------------------------------------------------
    static public OfficeDocumentTypeProperties determineDefaultOfficeProperties(String docTypeName) {
        String mimeType;
        String module;
        String extensionType;
        DocumentFormat docFormat;
        OfficeDocumentType officeDocumentType;

        if (docTypeName.equals(ApplicationType.APP_SPREADSHEET_STRING)) {
            module             = ApplicationType.APP_SPREADSHEET_STRING;
            officeDocumentType = OfficeDocumentType.MS_DOCUMENT_SPREADSHEET;
            docFormat          = DocumentFormat.XLSX;
            extensionType      = "xlsx";
            mimeType           = MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET;
        } else if (docTypeName.equals(ApplicationType.APP_PRESENTATION_STRING)) {
            module             = ApplicationType.APP_PRESENTATION_STRING;
            officeDocumentType = OfficeDocumentType.MS_DOCUMENT_PRESENTATION;
            docFormat          = DocumentFormat.PPTX;
            extensionType      = "pptx";
            mimeType           = MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION;
        } else {
            module             = ApplicationType.APP_TEXT_STRING;
            officeDocumentType = OfficeDocumentType.MS_DOCUMENT_WORD;
            extensionType      = "docx";
            docFormat          = DocumentFormat.DOCX;
            mimeType           = MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING;
        }

        return new OfficeDocumentTypeProperties(mimeType, module, extensionType, officeDocumentType, docFormat);
    }

}
