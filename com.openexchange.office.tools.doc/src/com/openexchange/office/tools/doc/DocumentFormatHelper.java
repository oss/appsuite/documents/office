/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.util.HashMap;
import java.util.Map;


public class DocumentFormatHelper {


    public enum OfficeDocumentType {
        NONE,
        MS_DOCUMENT_WORD,
        MS_DOCUMENT_SPREADSHEET,
        MS_DOCUMENT_PRESENTATION,
        ODF_DOCUMENT_WRITER,
        ODF_DOCUMENT_CALC,
        ODF_DOCUMENT_IMPRESS
    }

    public enum OfficeTemplateDocumentType {
        NONE,
        MS_DOCUMENT_WORD_TEMPLATE,
        MS_DOCUMENT_SPREADSHEET_TEMPLATE,
        MS_DOCUMENT_PRESENTATION_TEMPLATE,
        ODF_DOCUMENT_WRITER_TEMPLATE,
        ODF_DOCUMENT_CALC_TEMPLATE,
        ODF_DOCUMENT_IMPRESS_TEMPLATE
    }

    public static final String PROP_MIME_TYPE         = "MimeType";
    public static final String PROP_INPUT_TYPE        = "InputType";
    public static final String PROP_FILTER_SHORT_NAME = "FilterShortName";
    public static final String PROP_DOCUMENT_TYPE     = "DocumentType";
    public static final String PROP_DOCUMENT_FORMAT   = "DocumentFormat";

    /**
     * Provides conversion format info for mime type and extension type,
     * where the mime type is preferred.
     *
     * @param mimeType
     *  The mine type where we want to get the conversion format info. Can be
     *  null.
     *
     * @param extensionType
     *  The extension type of the file where we want to get conversion info
     *  for.
     *
     * @return
     *  The conversion format info or null if there is no known conversion
     *  information available.
     */
    static public Map<String, String> getConversionFormatInfo(String mimeType, String extensionType) {
        HashMap<String, String> result = null;

        // get default conversion for mime type first
        if (mimeType != null) {
            if (mimeType.startsWith("application/msword") || mimeType.equals("application/rtf") || mimeType.equals("text/rtf")) {
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
            } else if (mimeType.startsWith("application/vnd.ms-excel")) {
                // xlsm files have a binary mime type set, although they
                // are xml files => do not convert xlsm files (see #31245)
                if ((null == extensionType) || !extensionType.equals("xlsm")) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                }
            } else if (mimeType.startsWith("application/vnd.ms-word")) {
                // dotm and docm files have a binary mime type set, although they are xml files => do not convert them
                if ((null == extensionType) || !(extensionType.equals("dotm")||extensionType.equals("docm"))) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER);
                }
            } else if (mimeType.startsWith("application/vnd.ms-powerpoint")) {
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
            }
        }

        // get default conversion for extension type, if no valid mime type was given before
        if ((null == result) && (null != extensionType)) {
            if (extensionType.equals("doc") || extensionType.equals("dot") || extensionType.equals("rtf")) {
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
            }
            else if (extensionType.equals("xls") || extensionType.equals("xlt") || extensionType.equals("xlsb")) {
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
            }
            else if (extensionType.equals("ppt") || extensionType.equals("pot") || extensionType.equals("pps")) {
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
            }
        }

        return result;
    }

    /**
     * Provides the default document format info for a certain document type.
     *
     * @param type
     *  The document template type where the default template format info
     *  should be provided.
     *
     * @return
     *  The default document format info for the provided type or null if there
     *  is no known format info available.
     */
    static public HashMap<String, String> getFormatData(OfficeDocumentType officeDocumentType) {
        HashMap<String, String> data = new HashMap<String, String>();

        switch (officeDocumentType) {
        case MS_DOCUMENT_WORD: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING);
            data.put(PROP_INPUT_TYPE, "docx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.TEXT.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.DOCX.toString());
            break;
        }

        case MS_DOCUMENT_SPREADSHEET: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET);
            data.put(PROP_INPUT_TYPE, "xlsx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.SPREADSHEET.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.XLSX.toString());
            break;
        }

        case MS_DOCUMENT_PRESENTATION: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION);
            data.put(PROP_INPUT_TYPE, "pptx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.PRESENTATION.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.PPTX.toString());
            break;
        }

        case ODF_DOCUMENT_WRITER: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT);
            data.put(PROP_INPUT_TYPE, "odt");
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.TEXT.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODT.toString());
            break;
        }

        case ODF_DOCUMENT_CALC: {
            data.put(PROP_INPUT_TYPE, "ods");
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET);
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.SPREADSHEET.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODS.toString());
            break;
        }

        case ODF_DOCUMENT_IMPRESS: {
            data.put(PROP_INPUT_TYPE, "odp");
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION);
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.PRESENTATION.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODP.toString());
            break;
        }

        default: {
            break;
        }
        }

        return data;
    }

    /**
     * Provides the default template format info for a certain document template type.
     *
     * @param templateType
     *  The document template type where the default template format info
     *  should be provided.
     *
     * @return
     *  The template document format info for the provided type or null if there
     *  is no known format info available.
     */
    static public Map<String, String> getTemplateFormatData(OfficeTemplateDocumentType templateType) {
        final Map<String, String> data = new HashMap<String, String>();

        switch (templateType) {
        case MS_DOCUMENT_WORD_TEMPLATE: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE);
            data.put(PROP_INPUT_TYPE, "dotx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.TEXT.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.DOCX.toString());
            break;
        }

        case MS_DOCUMENT_SPREADSHEET_TEMPLATE: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE);
            data.put(PROP_INPUT_TYPE, "xltx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.SPREADSHEET.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.XLSX.toString());
            break;
        }

        case MS_DOCUMENT_PRESENTATION_TEMPLATE: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE);
            data.put(PROP_INPUT_TYPE, "potx");
            data.put(PROP_FILTER_SHORT_NAME, "ooxml");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.PRESENTATION.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.PPTX.toString());
            break;
        }

        case ODF_DOCUMENT_WRITER_TEMPLATE: {
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE);
            data.put(PROP_INPUT_TYPE, "ott");
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.TEXT.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODT.toString());
            break;
        }

        case ODF_DOCUMENT_CALC_TEMPLATE: {
            data.put(PROP_INPUT_TYPE, "ots");
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE);
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.SPREADSHEET.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODS.toString());
            break;
        }

        case ODF_DOCUMENT_IMPRESS_TEMPLATE: {
            data.put(PROP_INPUT_TYPE, "otp");
            data.put(PROP_MIME_TYPE, MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE);
            data.put(PROP_FILTER_SHORT_NAME, "odf");
            data.put(PROP_DOCUMENT_TYPE, DocumentType.PRESENTATION.toString());
            data.put(PROP_DOCUMENT_FORMAT, DocumentFormat.ODP.toString());
            break;
        }

        default: {
            break;
        }
        }

        return data;
    }

    /**
     * Provides the document format info that results if a document is created
     * based on a certain template format.
     *
     * @param mimeType
     *  The mime type of a document template file.
     *
     * @param extensionType
     *  The extension type of a document template file.
     *
     * @return
     *  The document format info or null if there is no known mapping.
     */
    static public Map<String, String> getTemplateTargetFormatInfo(String mimeType, String extensionType) {
        Map<String, String> result = null;

        // get default template target format for mime type first
        if (null != mimeType) {
            if (null == result) {
                // Spreadsheet
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_CALC);
                }
            }

            if (null == result) {
                // Text
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER);
                }
            }

            if (null == result) {
                // Presentation
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_IMPRESS);
                }
            }
        }

        // get default template target format for extension type, if no valid mime type was given
        if ((null == result) && (null != extensionType)) {
            if ((extensionType.equalsIgnoreCase("xltx")) || (extensionType.equalsIgnoreCase("xltm"))) {
                // Spreadsheet
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
            } else if (extensionType.equalsIgnoreCase("ots")) {
                result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_CALC);
            } else if ((extensionType.equalsIgnoreCase("dotx")) || (extensionType.equalsIgnoreCase("dotm"))) {
                // Text
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
            } else if (extensionType.equalsIgnoreCase("ott")) {
                result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER);
            } else if ((extensionType.equalsIgnoreCase("potx")) || (extensionType.equalsIgnoreCase("potm"))) {
                // Presentation
                result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
            } else if (extensionType.equalsIgnoreCase("otp")) {
                result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_IMPRESS);
            }
        }

        return result;
    }

    /**
     * Provides the document format info using the mime type
     * or extension type. 
     *
     * @param mimeType
     *  The mime type of a document file. First used to find a match.
     *
     * @param extensionType
     *  The extension type of a document file. Is used if the mime type
     *  doesn't have a match.
     *
     * @return
     *  The document format info or null if there is no known mapping.
     */
    static public Map<String, String> getFormatInfoForMimeTypeOrExtension(String mimeType, String extensionType) {
        Map<String, String> result = null;

        if (null != mimeType) {
            if (null == result) {
                // Spreadsheet
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_CALC);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_CALC);
                }
            }

            if (null == result) {
                // Text
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER);
                }
            }

            if (null == result) {
                // Presentation
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_IMPRESS);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO)) {
                    result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE)) {
                    result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_IMPRESS);
                }
            }
        }

        if ((null == result) && (null != extensionType)) {
        	switch (extensionType) {
        	case "xlsx":
        	case "xlsm":
        	case "xltx":
        	case "xltm": result = getFormatData(OfficeDocumentType.MS_DOCUMENT_SPREADSHEET); break;
        	case "ods":
        	case "ots": result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_CALC); break;
        	case "docx":
        	case "docm":
        	case "dotx":
        	case "dotm": result = getFormatData(OfficeDocumentType.MS_DOCUMENT_WORD); break;
        	case "odt":
        	case "ott": result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_WRITER); break;
        	case "pptx":
        	case "pptm":
        	case "potx":
        	case "ppsm":
        	case "ppsx":
        	case "potm": result = getFormatData(OfficeDocumentType.MS_DOCUMENT_PRESENTATION); break;
        	case "odp":
        	case "otp": result = getFormatData(OfficeDocumentType.ODF_DOCUMENT_IMPRESS); break;
        	default: // unknown
            }
        }

        return result;
    }

    /**
     * Provides the mapping from a normal document format to a pre-defined
     * document template format. The mapping uses the mime type and the
     * extension type into account, preferring the mine type.
     *
     * @param mimeType
     *  The mime type of the document if known or null.
     *
     * @param extensionType
     *  The extension type of the document.
     *
     * @return
     *  The document template format info or null if there is no mapping.
     */
    static public Map<String, String> getTemplateFormatInfoForDocument(String mimeType, String extensionType) {
        Map<String, String> result = null;

        if (null != mimeType) {
            if (null == result) {
                // Spreadsheet
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_SPREADSHEET_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_SPREADSHEET_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_CALC_TEMPLATE);
                }
            }

            if (null == result) {
                // Text
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_WORD_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_WORD_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_WRITER_TEMPLATE);
                }
            }

            if (null == result) {
                // Presentation
                if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_PRESENTATION_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_MACRO)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_PRESENTATION_TEMPLATE);
                } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT)) {
                    result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_IMPRESS_TEMPLATE);
                }
            }
        }

        if ((null == result) && (null != extensionType)) {

            // get default template target format for extension type, if no valid mime type was given
            if ((extensionType.equalsIgnoreCase("xlsx")) || (extensionType.equalsIgnoreCase("xlsm"))) {
                // Spreadsheet
                result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_SPREADSHEET_TEMPLATE);
            } else if (extensionType.equalsIgnoreCase("ods")) {
                result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_CALC_TEMPLATE);
            } else if ((extensionType.equalsIgnoreCase("docx")) || (extensionType.equalsIgnoreCase("docm"))) {
                // Text
                result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_WORD_TEMPLATE);
            } else if (extensionType.equalsIgnoreCase("odt")) {
                result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_WRITER_TEMPLATE);
            } else if ((extensionType.equalsIgnoreCase("pptx")) || (extensionType.equalsIgnoreCase("pptm"))) {
                // Presentation
                result = getTemplateFormatData(OfficeTemplateDocumentType.MS_DOCUMENT_PRESENTATION_TEMPLATE);
            } else if (extensionType.equalsIgnoreCase("odp")) {
                result = getTemplateFormatData(OfficeTemplateDocumentType.ODF_DOCUMENT_IMPRESS_TEMPLATE);
            }
        }

        return result;
    }

    /**
     * Determines if the provided mime type or extension type specify a
     * supported document template format. Supported means that it can be
     * loaded without conversion needed.
     *
     * @param mimeType
     *  The mime type of the document if known or null.
     *
     * @param extensionType
     *  The extension type of the document.
     *
     * @return
     *  TRUE if the mime/extension type describe a supported document template
     *  format, FALSE if not.
     */
    static public boolean isSupportedTemplateFormat(String mimeType, String extensionType) {
        // get default template target format for mime type first
        if (null != mimeType) {
            // Spreadsheet
            if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE)) {
               return true;
            }

            // Text
            if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE)) {
                return true;
            }

            // Presentation
            if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO)) {
                return true;
            } else if (mimeType.equalsIgnoreCase(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE)) {
                return true;
            }
        }

        // get default template target format for extension type, if no valid mime type was given
        if (null != extensionType) {
            if ((extensionType.equalsIgnoreCase("xltx")) || (extensionType.equalsIgnoreCase("xltm")) || (extensionType.equalsIgnoreCase("ots"))) {
                // Spreadsheet
                return true;
            } else if ((extensionType.equalsIgnoreCase("dotx")) || (extensionType.equalsIgnoreCase("dotm")) || (extensionType.equalsIgnoreCase("ott"))) {
                // Text
                return true;
            } else if ((extensionType.equalsIgnoreCase("potx")) || (extensionType.equalsIgnoreCase("potm")) || (extensionType.equalsIgnoreCase("otp"))) {
                // Presentation
                return true;
            }
        }

        return false;
    }
}
