/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Helper class to provide certain document based mime types and
 * function to determine mime-types from file extensions.
 *
 * @author Carsten Driesner
 *
 */
public class MimeTypeHelper {
    private static AtomicReference<HashMap<String, DocumentType>> m_mapTemplateMimeTypeToDocumentType = new AtomicReference<HashMap<String, DocumentType>>();
    private static AtomicReference<HashMap<String, DocumentType>> m_mapMimeTypeToDocumentType = new AtomicReference<HashMap<String, DocumentType>>();

    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO = "application/vnd.ms-excel.sheet.macroEnabled.12";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO = "application/vnd.ms-excel.template.macroEnabled.12";

    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_MACRO = "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE = "application/vnd.openxmlformats-officedocument.presentationml.template";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO = "application/vnd.ms-powerpoint.presentation.macroEnabled.12";

    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_MACRO = "application/vnd.ms-word.document.macroEnabled.12";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
    public static final String MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE_MACRO = "application/vnd.ms-word.template.macroEnabled.12";

    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT = "application/vnd.oasis.opendocument.text";
    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE = "application/vnd.oasis.opendocument.text-template";

    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET = "application/vnd.oasis.opendocument.spreadsheet";
    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE = "application/vnd.oasis.opendocument.spreadsheet-template";

    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION = "application/vnd.oasis.opendocument.presentation";
    public static final String MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE = "application/vnd.oasis.opendocument.presentation-template";

    public static final String MIMETYPE_MSEXCEL = "application/vnd.ms-excel";
    public static final String MIMETYPE_MSPOWERPOINT = "application/vnd.ms-powerpoint";
    public static final String MIMETYPE_MSWORD = "application/msword";

    /**
     * Provides the document type for a provided template mime type.
     *
     * @param templateMimeType
     *  A template document mime type.
     *
     * @return
     *  The document type or null if the mime type couldn't be recognized as
     *  a supported document template type.
     */
    public static DocumentType getDocumentTypeFromTemplateMimeType(String templateMimeType) {
        return getStaticDocumentTypeTemplateMap().get(templateMimeType);
    }

    /**
     * Provides the document type for a provided template mime type.
     *
     * @param templateMimeType
     *  A template document mime type.
     *
     * @return
     *  The document type or null if the mime type couldn't be recognized as
     *  a supported document template type.
     */
    public static DocumentType getDocumentTypeFromMimeType(String templateMimeType) {
        return getStaticDocumentTypeMap().get(templateMimeType);
    }

    // ------------------------------------------------------------------------
    // Templates
    // ------------------------------------------------------------------------

    /**
     * Retrieves the initialized static template document type map to speed up the search.
     */
    private static HashMap<String, DocumentType> getStaticDocumentTypeTemplateMap() {
    	HashMap<String, DocumentType> result = m_mapTemplateMimeTypeToDocumentType.get();

        if (result == null) {
        	final HashMap<String, DocumentType> mapMimeTypeToDocumentType = new HashMap<String, DocumentType>();

            // Excel/Calc
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSEXCEL, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE, DocumentType.SPREADSHEET);

        	// Powerpoint/Impress
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSPOWERPOINT, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE, DocumentType.PRESENTATION);

        	// Word/Writer
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE_MACRO, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSWORD, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE, DocumentType.TEXT);

        	m_mapTemplateMimeTypeToDocumentType.compareAndSet(null, mapMimeTypeToDocumentType);
        	result = mapMimeTypeToDocumentType;
        }

        return result;
    }

    // ------------------------------------------------------------------------
    // Documents & Templates
    // ------------------------------------------------------------------------

    /**
     * Retrieves the initialized static document type map to speed up the search.
     */
    private static HashMap<String, DocumentType> getStaticDocumentTypeMap() {
    	HashMap<String, DocumentType> result = m_mapMimeTypeToDocumentType.get();

    	if (result == null) {
        	final HashMap<String, DocumentType> mapMimeTypeToDocumentType = new HashMap<String, DocumentType>();

            // Excel
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSEXCEL, DocumentType.SPREADSHEET);
        	// Calc
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET, DocumentType.SPREADSHEET);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE, DocumentType.SPREADSHEET);

            // Powerpoint
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_MACRO, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSPOWERPOINT, DocumentType.PRESENTATION);
        	// Impress
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION, DocumentType.PRESENTATION);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE, DocumentType.PRESENTATION);

            // Word
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_MACRO, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE_MACRO, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_MSWORD, DocumentType.TEXT);
        	// Writer
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT, DocumentType.TEXT);
        	mapMimeTypeToDocumentType.put(MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE, DocumentType.TEXT);

            m_mapMimeTypeToDocumentType.compareAndSet(null, mapMimeTypeToDocumentType);
            result = mapMimeTypeToDocumentType;
    	}

        return result;
    }
}
