/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.doc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class ExtensionHelper {

    private static AtomicReference<HashMap<String, String>> m_mapExtensionsToMimeType = new AtomicReference<HashMap<String, String>>();
    private static AtomicReference<HashMap<String, String>> m_mapTemplateExtensionsToMimeType = new AtomicReference<HashMap<String, String>>();
    private static AtomicReference<HashMap<String, DocumentType>> m_mapExtensionToDocumentType = new AtomicReference<HashMap<String, DocumentType>>();
    private static AtomicReference<Map<String, DocumentFormat>> m_mapExtensionToDocumentFormat = new AtomicReference<Map<String, DocumentFormat>>();

    // template extensions
    public static final String EXCEL_TEMPLATE_EXTENSIONS[] = { "xltx", "xltm", "xlt" };
    public static final String WORD_TEMPLATE_EXTENSIONS[] = { "dotx", "dotm", "dot" };
    public static final String POWERPOINT_TEMPLATE_EXTENSIONS[] = { "potx", "pptm", "pot" };
    public static final String ODF_CALC_TEMPLATE_EXTENSIONS[] = { "ots" };
    public static final String ODF_WRITER_TEMPLATE_EXTENSIONS[] = { "ott" };
    public static final String ODF_IMPRESS_TEMPLATE_EXTENSIONS[] = { "otp" };
    // document extensions
    public static final String EXCEL_DOC_EXTENSIONS[] = { "xlsx", "xlsm", "xls" };
    public static final String WORD_DOC_EXTENSIONS[] = { "docx", "docm", "doc" };
    public static final String POWERPOINT_DOC_EXTENSIONS[] = { "pptx", "pptm", "ppt" };
    public static final String ODF_CALC_DOC_EXTENSIONS[] = { "ods" };
    public static final String ODF_WRITER_DOC_EXTENSIONS[] = { "odt" };
    public static final String ODF_IMPRESS_DOC_EXTENSIONS[] = { "odp" };

    // document type extensions
    public static final String SPREADSHEET_EXTENSIONS[] = { "xlsx", "xlsm", "xltx", "xltm", "ods", "ots" };
    public static final String TEXT_EXTENSIONS[] = { "docx", "docm", "dotx", "dotm", "odt", "ott" };
    public static final String PRESENTATION_EXTENSIONS[] = { "pptx", "pptm", "potx", "potm", "odp", "otp" };

    /**
     * Provides the mime type of a known template file extension.
     *
     * @param templateExt
     *  A template document file extension where the mime type should be
     *  retrieved for.
     *
     * @return
     *  The mime type for the template document file extensions or null
     *  if the extension is unknown, null or not a template document extension.
     */
    public static String getMimeTypeFromTemplateExtension(String templateExt) {
        return getStaticMimeTypeTemplateMap().get(templateExt);
    }

    /**
     * Provides the mime type of a known template file extension.
     *
     * @param templateExt
     *  A template document file extension where the mime type should be
     *  retrieved for.
     *
     * @return
     *  The mime type for the template document file extensions or null
     *  if the extension is unknown, null or not a template document extension.
     */
    public static String getMimeTypeFromExtension(String extension) {
        return getStaticMimeTypeMap().get(extension);
    }

    /**
     * Provides the document type for an extension.
     *
     * @param templateMimeType
     *  A template document mime type.
     *
     * @return
     *  The document type or null if the mime type couldn't be recognized as
     *  a supported document template type.
     */
    public static DocumentType getDocumentTypeFromExtension(String extension) {
        return getStaticDocumentTypeMap().get(extension);
    }

    /**
     * Provides the document format for an extension.
     *
     * @param the extension of the document file
     *
     * @return
     *  The document format or null if the extension cannot be mapped to
     *  a document format.
     */
    public static DocumentFormat getDocumentFormatFromExtension(String extension) {
        return getStaticDocumentFormatMap().get(extension);
    }

    /**
     * Retrieves the initialized static mime type map to speed up the search.
     */
    private static HashMap<String, String> getStaticMimeTypeMap() {
        HashMap<String, String> result = m_mapExtensionsToMimeType.get();

        if (result == null) {
            final HashMap<String, String> mapExtensionsToMimeType = new HashMap<String, String>();

            // Excel
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE);
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSEXCEL);
            mapExtensionsToMimeType.put(EXCEL_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET);
            mapExtensionsToMimeType.put(EXCEL_DOC_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_MACRO);
            mapExtensionsToMimeType.put(EXCEL_DOC_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSEXCEL);

            // Powerpoint
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE);
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSPOWERPOINT);
            mapExtensionsToMimeType.put(POWERPOINT_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION);
            mapExtensionsToMimeType.put(POWERPOINT_DOC_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_MACRO);
            mapExtensionsToMimeType.put(POWERPOINT_DOC_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSPOWERPOINT);

            // Word
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE);
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSWORD);
            mapExtensionsToMimeType.put(WORD_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING);
            mapExtensionsToMimeType.put(WORD_DOC_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_MACRO);
            mapExtensionsToMimeType.put(WORD_DOC_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSWORD);

            // Calc
            mapExtensionsToMimeType.put(ODF_CALC_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE);
            mapExtensionsToMimeType.put(ODF_CALC_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET);
            // Impress
            mapExtensionsToMimeType.put(ODF_IMPRESS_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE);
            mapExtensionsToMimeType.put(ODF_IMPRESS_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION);
            // Writer
            mapExtensionsToMimeType.put(ODF_WRITER_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE);
            mapExtensionsToMimeType.put(ODF_WRITER_DOC_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT);

            m_mapExtensionsToMimeType.compareAndSet(null, mapExtensionsToMimeType);
            result = mapExtensionsToMimeType;
        }

        return result;
    }

    /**
     * Retrieves the initialized static template mime type map to speed up the search.
     */
    private static HashMap<String, String> getStaticMimeTypeTemplateMap() {
        HashMap<String, String> result = m_mapTemplateExtensionsToMimeType.get();

        if (result == null) {
            final HashMap<String, String> mapExtensionsToMimeType = new HashMap<String, String>();

            // Excel
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE);
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEET_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(EXCEL_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSEXCEL);

            // Powerpoint
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE);
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATION_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(POWERPOINT_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSPOWERPOINT);

            // Word
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE);
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[1], MimeTypeHelper.MIMETYPE_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSING_TEMPLATE_MACRO);
            mapExtensionsToMimeType.put(WORD_TEMPLATE_EXTENSIONS[2], MimeTypeHelper.MIMETYPE_MSWORD);

            // Calc
            mapExtensionsToMimeType.put(ODF_CALC_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE);
            // Impress
            mapExtensionsToMimeType.put(ODF_IMPRESS_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE);
            // Writer
            mapExtensionsToMimeType.put(ODF_WRITER_TEMPLATE_EXTENSIONS[0], MimeTypeHelper.MIMETYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE);

            m_mapTemplateExtensionsToMimeType.compareAndSet(null, mapExtensionsToMimeType);
            result = mapExtensionsToMimeType;
        }

        return result;
    }

    /**
     * Retrieves the initialized static document type map to speed up the search.
     */
    private static HashMap<String, DocumentType> getStaticDocumentTypeMap() {
        HashMap<String, DocumentType> result = m_mapExtensionToDocumentType.get();

        if (result == null) {
            final HashMap<String, DocumentType> mapExtensionToDocumentType = new HashMap<String, DocumentType>();

            // Excel
            mapExtensionToDocumentType.put(EXCEL_TEMPLATE_EXTENSIONS[0], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(EXCEL_TEMPLATE_EXTENSIONS[1], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(EXCEL_TEMPLATE_EXTENSIONS[2], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(EXCEL_DOC_EXTENSIONS[0], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(EXCEL_DOC_EXTENSIONS[1], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(EXCEL_DOC_EXTENSIONS[2], DocumentType.SPREADSHEET);

            // Powerpoint
            mapExtensionToDocumentType.put(POWERPOINT_TEMPLATE_EXTENSIONS[0], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(POWERPOINT_TEMPLATE_EXTENSIONS[1], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(POWERPOINT_TEMPLATE_EXTENSIONS[2], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(POWERPOINT_DOC_EXTENSIONS[0], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(POWERPOINT_DOC_EXTENSIONS[1], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(POWERPOINT_DOC_EXTENSIONS[2], DocumentType.PRESENTATION);

            // Word
            mapExtensionToDocumentType.put(WORD_TEMPLATE_EXTENSIONS[0], DocumentType.TEXT);
            mapExtensionToDocumentType.put(WORD_TEMPLATE_EXTENSIONS[1], DocumentType.TEXT);
            mapExtensionToDocumentType.put(WORD_TEMPLATE_EXTENSIONS[2], DocumentType.TEXT);
            mapExtensionToDocumentType.put(WORD_DOC_EXTENSIONS[0], DocumentType.TEXT);
            mapExtensionToDocumentType.put(WORD_DOC_EXTENSIONS[1], DocumentType.TEXT);
            mapExtensionToDocumentType.put(WORD_DOC_EXTENSIONS[2], DocumentType.TEXT);

            // Calc
            mapExtensionToDocumentType.put(ODF_CALC_TEMPLATE_EXTENSIONS[0], DocumentType.SPREADSHEET);
            mapExtensionToDocumentType.put(ODF_CALC_DOC_EXTENSIONS[0], DocumentType.SPREADSHEET);

            // Impress
            mapExtensionToDocumentType.put(ODF_IMPRESS_TEMPLATE_EXTENSIONS[0], DocumentType.PRESENTATION);
            mapExtensionToDocumentType.put(ODF_IMPRESS_DOC_EXTENSIONS[0], DocumentType.PRESENTATION);

            // Writer
            mapExtensionToDocumentType.put(ODF_WRITER_TEMPLATE_EXTENSIONS[0], DocumentType.TEXT);
            mapExtensionToDocumentType.put(ODF_WRITER_DOC_EXTENSIONS[0], DocumentType.TEXT);

            m_mapExtensionToDocumentType.compareAndSet(null, mapExtensionToDocumentType);
            result = mapExtensionToDocumentType;
        }

        return result;
    }

    /**
     * Retrieves the initialized static document type map to speed up the search.
     */
    private static Map<String, DocumentFormat> getStaticDocumentFormatMap() {
        Map<String, DocumentFormat> result = m_mapExtensionToDocumentFormat.get();

        if (result == null) {
            final Map<String, DocumentFormat> mapExtensionToDocumentFormat = new HashMap<>();

            // Excel
            mapExtensionToDocumentFormat.put(EXCEL_TEMPLATE_EXTENSIONS[0], DocumentFormat.XLSX);
            mapExtensionToDocumentFormat.put(EXCEL_TEMPLATE_EXTENSIONS[1], DocumentFormat.XLSX);
            mapExtensionToDocumentFormat.put(EXCEL_DOC_EXTENSIONS[0], DocumentFormat.XLSX);
            mapExtensionToDocumentFormat.put(EXCEL_DOC_EXTENSIONS[1], DocumentFormat.XLSX);

            // Powerpoint
            mapExtensionToDocumentFormat.put(POWERPOINT_TEMPLATE_EXTENSIONS[0], DocumentFormat.PPTX);
            mapExtensionToDocumentFormat.put(POWERPOINT_TEMPLATE_EXTENSIONS[1], DocumentFormat.PPTX);
            mapExtensionToDocumentFormat.put(POWERPOINT_DOC_EXTENSIONS[0], DocumentFormat.PPTX);
            mapExtensionToDocumentFormat.put(POWERPOINT_DOC_EXTENSIONS[1], DocumentFormat.PPTX);

            // Word
            mapExtensionToDocumentFormat.put(WORD_TEMPLATE_EXTENSIONS[0], DocumentFormat.DOCX);
            mapExtensionToDocumentFormat.put(WORD_TEMPLATE_EXTENSIONS[1], DocumentFormat.DOCX);
            mapExtensionToDocumentFormat.put(WORD_DOC_EXTENSIONS[0], DocumentFormat.DOCX);
            mapExtensionToDocumentFormat.put(WORD_DOC_EXTENSIONS[1], DocumentFormat.DOCX);

            // Calc
            mapExtensionToDocumentFormat.put(ODF_CALC_TEMPLATE_EXTENSIONS[0], DocumentFormat.ODS);
            mapExtensionToDocumentFormat.put(ODF_CALC_DOC_EXTENSIONS[0], DocumentFormat.ODS);

            // Impress
            mapExtensionToDocumentFormat.put(ODF_IMPRESS_TEMPLATE_EXTENSIONS[0], DocumentFormat.ODP);
            mapExtensionToDocumentFormat.put(ODF_IMPRESS_DOC_EXTENSIONS[0], DocumentFormat.ODP);

            // Writer
            mapExtensionToDocumentFormat.put(ODF_WRITER_TEMPLATE_EXTENSIONS[0], DocumentFormat.ODT);
            mapExtensionToDocumentFormat.put(ODF_WRITER_DOC_EXTENSIONS[0], DocumentFormat.ODT);

            m_mapExtensionToDocumentFormat.compareAndSet(null, mapExtensionToDocumentFormat);
            result = mapExtensionToDocumentFormat;
        }

        return result;
    }
}
