/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.rt;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link EncodeUtility} - A utility class for encoding/decoding.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.8.0
 */
public class EncodeUtility {

    /**
     * Initializes a new {@link EncodeUtility}.
     */
    private EncodeUtility() {
        super();
    }

    /**
     * Encodes given part for a resource identifier
     *
     * @param resourcePart The plain part of the resource identifier
     * @return The encoded part for a resource identifier
     */
    public static String encodeResourcePart(String resourcePart) {
        if (StringUtils.isEmpty(resourcePart)) {
            return resourcePart;
        }
        try {
            return URLEncoder.encode(resourcePart, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Cannot occur
            return resourcePart;
        }
    }

    /**
     * Decodes given part for a resource identifier
     *
     * @param encodedResourcePart The encoded part of the resource identifier
     * @return The decoded part for a resource identifier
     */
    public static String decodeResourcePart(String encodedResourcePart) {
        if (StringUtils.isEmpty(encodedResourcePart)) {
            return encodedResourcePart;
        }
        try {
            return URLDecoder.decode(encodedResourcePart, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Cannot occur
            return encodedResourcePart;
        }
    }

}
