/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.util;

import java.io.InputStream;
import java.util.List;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.session.Session;


public class FileHelperServiceMock implements FileHelperService {
    private FileDataStore fileDataStore;
    @SuppressWarnings("unused")
    private Session session;
    private boolean canRead = true;
    private boolean canWrite = true;

    public FileHelperServiceMock(FileDataStore fileDataStore, Session session) {
        this.fileDataStore = fileDataStore;
        this.session = session;
    }

    public void setCanReadResult(boolean result) {
        this.canRead = result;
    }

    public void setCanWriteResult(boolean result) {
        this.canWrite = result;
    }

    @Override
    public boolean canWriteToFile(String fileId) {
        return canWrite;
    }

    @Override
    public boolean canWriteToFile(File metaData, int userId) {
        return canWrite;
    }

    @Override
    public Set<Integer> canWriteToFile(String fileId, Context ctx, List<Integer> userIds) throws OXException {
        return null;
    }

    @Override
    public boolean canCreateFileAndWrite(String sFolderId) {
        return canWrite;
    }

    @Override
    public boolean canReadFile(String folderId, String fileId, int userId) {
        return canRead;
    }

    @Override
    public File getFileMetaData(String fileID, String version) throws OXException {
        FileData fileData = fileDataStore.getFileData(fileID, version);
        return (fileData != null) ? fileData.getFile() : null;
    }

    @Override
    public File getFileMetaData(String fileId) {
        return null;
    }

    @Override
    public ErrorCode createFileWithStream(String folder_id, String fileName, String mimeType, InputStream inputStream) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode createFileWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, String mimeType, InputStream inputStream) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode createFileAndWriteStream(File file, InputStream inputStream) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode createFileAndWriteStream(IDBasedFileAccess fileAccess, File file, InputStream inputStream) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode createFileCopyWithStream(String folder_id, String fileName, File sourceFile, InputStream inputStream, boolean copyMetaData) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode createFileCopyWithStream(IDBasedFileAccess fileAccess, String folder_id, String fileName, File sourceFile, InputStream inputStream, boolean copyMetaData) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode writeStreamToFile(File targetFile, InputStream inputStream, File copyMetaData) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public ErrorCode writeStreamToFile(IDBasedFileAccess fileAccess, File targetFile, InputStream inputStream, File copyMetaData) {
        return ErrorCode.NO_ERROR;
    }

    @Override
    public File getMetaDataFromFileName(String folder_id, String fileName) throws OXException {
        return null;
    }

    @Override
    public File getMetaDataFromFileName(String folder_id, String fileName, List<Field> fields) throws OXException {
        return null;
    }

    @Override
    public boolean fileNameExists(String folder_id, String fileName) throws OXException {
        return false;
    }

    @Override
    public ErrorCode deleteFile(String fileId, boolean hardDelete) {
        return ErrorCode.NO_ERROR;
    }

}
