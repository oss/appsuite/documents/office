package test.com.openexchange.office.document.util;

import java.util.HashMap;
import java.util.Map;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.composition.IDBasedAdministrativeFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.session.Session;


public class IDBasedFileAccessFactoryMock implements IDBasedFileAccessFactory, FileDataStore {

    private static final Map<Session, IDBasedFileAccess> createdFileAccess = new HashMap<>();
    private static final Map<String, FileData> storedMetaFiles = new HashMap<>();

    @Override
    public IDBasedFileAccess createAccess(Session session) {
        IDBasedFileAccess instance = createdFileAccess.get(session);
        if (instance == null) {
            instance = new IDBasedFileAccessMock(this, session);
            createdFileAccess.put(session, instance);
        }
        return instance;
    }

    @Override
    public IDBasedAdministrativeFileAccess createAccess(int contextId) {
        return null;
    }

    @Override
    public void resetFileData() {
        storedMetaFiles.clear();
    }

    @Override
    public FileData getFileData(String id, String version) {
        String key = getAccessKey(id, version);
        return storedMetaFiles.get(key);
    }

    @Override
    public void setFileData(String id, String version, File file) {
        String key = getAccessKey(id, version);
        FileData fileData = storedMetaFiles.get(key);
        if (fileData == null) {
            fileData = new FileData(file, null);
        }
        fileData.setFile(file);
    }

    @Override
    public void setFileData(String id, String version, File file, byte[] data) {
        String key = getAccessKey(id, version);
        FileData fileData = storedMetaFiles.get(key);
        if (fileData == null) {
            fileData = new FileData(file, data);
        } else {
            fileData.setFile(file);
            fileData.setData(data);
        }
    }

    @Override
    public void setFileData(String id, String version, FileData fileData) {
        String key = getAccessKey(id, version);
        storedMetaFiles.put(key, fileData);
    }

    private String getAccessKey(String id, String version)  {
        return id + "-" + version;
    }

}
