/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageObjectPermission;
import com.openexchange.file.storage.FolderPath;
import com.openexchange.file.storage.MediaStatus;
import com.openexchange.groupware.EntityInfo;
import com.openexchange.java.GeoLocation;

public class FileMock implements File {
    private Map<String, String> props = new HashMap<>();

    public FileMock(String fileId, String version) {
        props.put(File.Field.VERSION.toString(), version);
        props.put(File.Field.ID.toString(), fileId);
    }

    public void setProperty(String key, Object data) {
        String strData = null;

        if (data instanceof String) {
            strData = (String)data;
        } else if (data instanceof Date) {
            strData = DateFormat.getDateInstance().format((Date)data);
        } else if (data instanceof Integer) {
            strData = ((Integer)data).toString();
        } else if (data instanceof Long) {
            strData = ((Long)data).toString();
        } else {
            throw new RuntimeException("not supported data type");
        }
        props.put(key, strData);
    }

    @Override
    public String getProperty(String key) {
        return props.get(key);
    }

    @Override
    public Set<String> getPropertyNames() {
        return props.keySet();
    }

    @Override
    public Date getLastModified() {
        Date result = null;
        String date = props.get(File.Field.LAST_MODIFIED.toString());
        try {
            if (date == null) {
                date = LocalDateTime.now().toString();
            }
            result = DateFormat.getDateInstance().parse(date);
        } catch (ParseException e) {
            // nothing to do
        }
        return result;
    }

    @Override
    public void setLastModified(Date now) {
        String strData = DateFormat.getDateInstance().format(now);
        props.put(File.Field.LAST_MODIFIED.toString(), strData);
    }

    @Override
    public Date getCreated() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCreated(Date creationDate) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public int getModifiedBy() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setModifiedBy(int lastEditor) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getFolderId() {
        return props.get(File.Field.FOLDER_ID.toString());
    }

    @Override
    public void setFolderId(String folderId) {
        props.put(File.Field.FOLDER_ID.toString(), folderId);
    }

    @Override
    public String getTitle() {
        return props.get(File.Field.TITLE.toString());
    }

    @Override
    public void setTitle(String title) {
        props.put(File.Field.TITLE.toString(), title);
    }

    @Override
    public String getVersion() {
        return props.get(File.Field.VERSION.toString());
    }

    @Override
    public void setVersion(String version) {
        props.put(File.Field.VERSION.toString(), version);
    }

    @Override
    public String getContent() {
        return props.get(File.Field.CONTENT.toString());
    }

    @Override
    public long getFileSize() {
        long result = 0;
        String value = props.get(File.Field.FILE_SIZE.toString());
        if (StringUtils.isNotEmpty(value)) {
            result = Long.parseLong(value);
        }
        return result;
    }

    @Override
    public void setFileSize(long length) {
        props.put(File.Field.FILE_SIZE.toString(), String.valueOf(length));
    }

    @Override
    public String getFileMIMEType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFileMIMEType(String type) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getFileName() {
        return props.get(File.Field.FILENAME.toString());
    }

    @Override
    public void setFileName(String fileName) {
        props.put(File.Field.FILENAME.toString(), fileName);
    }

    @Override
    public String getId() {
        return props.get(File.Field.ID.toString());
    }

    @Override
    public void setId(String id) {
        props.put(File.Field.ID.toString(), id);
    }

    @Override
    public int getCreatedBy() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setCreatedBy(int cretor) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDescription(String description) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getURL() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setURL(String url) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public long getSequenceNumber() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getCategories() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCategories(String categories) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Date getLockedUntil() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setLockedUntil(Date lockedUntil) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getFileMD5Sum() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFileMD5Sum(String sum) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public int getColorLabel() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setColorLabel(int color) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isCurrentVersion() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setIsCurrentVersion(boolean bool) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getVersionComment() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setVersionComment(String string) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setNumberOfVersions(int numberOfVersions) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public int getNumberOfVersions() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Map<String, Object> getMeta() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMeta(Map<String, Object> properties) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public EntityInfo getCreatedFrom() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCreatedFrom(EntityInfo createdFrom) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public EntityInfo getModifiedFrom() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setModifiedFrom(EntityInfo modifiedFrom) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getUniqueId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setUniqueId(String uniqueId) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isAccurateSize() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setAccurateSize(boolean accurateSize) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public List<FileStorageObjectPermission> getObjectPermissions() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setObjectPermissions(List<FileStorageObjectPermission> objectPermissions) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isShareable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setShareable(boolean shareable) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setSequenceNumber(long sequenceNumber) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public FolderPath getOrigin() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOrigin(FolderPath origin) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Date getCaptureDate() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCaptureDate(Date captureDate) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public GeoLocation getGeoLocation() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setGeoLocation(GeoLocation geoLocation) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Long getWidth() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setWidth(long width) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Long getHeight() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setHeight(long height) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getCameraMake() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraMake(String cameraMake) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String getCameraModel() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraModel(String cameraModel) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Long getCameraIsoSpeed() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraIsoSpeed(long isoSpeed) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Double getCameraAperture() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraAperture(double aperture) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Double getCameraFocalLength() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraFocalLength(double focalLength) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Double getCameraExposureTime() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCameraExposureTime(double exposureTime) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Map<String, Object> getMediaMeta() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMediaMeta(Map<String, Object> mediaMeta) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public MediaStatus getMediaStatus() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMediaStatus(MediaStatus mediaStatus) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public File dup() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void copyInto(File other) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void copyFrom(File other) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void copyInto(File other, Field... fields) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void copyFrom(File other, Field... fields) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Set<Field> differences(File other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean equals(File other, Field criterium, Field... criteria) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean matches(String pattern, Field... fields) {
        // TODO Auto-generated method stub
        return false;
    }

}
