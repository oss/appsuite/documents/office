/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.util;

import java.util.Set;
import java.util.UUID;
import com.openexchange.session.Origin;
import com.openexchange.session.Session;


public class SessionMock implements Session {
    private String client = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0";
    private String localIp = "127.0.0.1";
    private String sessionId = UUID.randomUUID().toString();
    private int cid = 1;
    private int uid = 1;

    public SessionMock(int contextId, int userId) {
        cid = contextId;
        uid = userId;
    }

    @Override
    public int getContextId() {
        return cid;
    }

    @Override
    public String getLocalIp() {
        return localIp;
    }

    @Override
    public void setLocalIp(String ip) {
        localIp = ip;
    }

    @Override
    public String getLoginName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean containsParameter(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Object getParameter(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPassword() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getRandomToken() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSecret() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSessionID() {
        return sessionId;
    }

    @Override
    public int getUserId() {
        return uid;
    }

    @Override
    public String getUserlogin() {
        return null;
    }

    @Override
    public String getLogin() {
        return null;
    }

    @Override
    public void setParameter(String name, Object value) {
        // nothing to do
    }

    @Override
    public String getAuthId() {
        return null;
    }

    @Override
    public String getHash() {
        return null;
    }

    @Override
    public void setHash(String hash) {
        // nothing to do
    }

    @Override
    public String getClient() {
        return client;
    }

    @Override
    public void setClient(String client) {
       this.client = client;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public Set<String> getParameterNames() {
        return null;
    }

    @Override
    public Origin getOrigin() {
        return null;
    }

    @Override
    public boolean isStaySignedIn() {
        return false;
    }

}
