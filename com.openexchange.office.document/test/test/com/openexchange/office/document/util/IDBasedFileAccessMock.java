/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.NotImplementedException;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageCapability;
import com.openexchange.file.storage.FileStorageFileAccess.SortDirection;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.Range;
import com.openexchange.file.storage.composition.FileID;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.search.SearchTerm;
import com.openexchange.groupware.results.Delta;
import com.openexchange.groupware.results.TimedResult;
import com.openexchange.session.Session;
import com.openexchange.tools.iterator.SearchIterator;

public class IDBasedFileAccessMock implements IDBasedFileAccess {

    private FileDataStore fileDataStore;
    private Session session;
    private OXException exToThrowForGetFileMetadata = null;
    private OXException exToThrowForSaveFileMetadata = null;

    public IDBasedFileAccessMock(FileDataStore fileDataStore, Session session) {
        this.fileDataStore = fileDataStore;
        this.session = session;
    }

    public void setThrowExceptionForGetFileMetaData(OXException ex) {
        exToThrowForGetFileMetadata = ex;
    }

    public void setThrowExceptionForSaveFileMetaData(OXException ex) {
        exToThrowForSaveFileMetadata = ex;
    }

    @Override
    public void startTransaction() throws OXException {
        // nothing to do
    }

    @Override
    public void commit() throws OXException {
        // nothing to do
    }

    @Override
    public void rollback() throws OXException {
        // nothing to do
    }

    @Override
    public void finish() throws OXException {
        // nothing to do
    }

    @Override
    public void setTransactional(boolean transactional) {
        // nothing to do
    }

    @Override
    public void setRequestTransactional(boolean transactional) {
        // nothing to do
    }

    @Override
    public void setCommitsTransaction(boolean commits) {
        // nothing to do
    }

    @Override
    public List<OXException> getWarnings() {
        return null;
    }

    @Override
    public List<OXException> getAndFlushWarnings() {
        return null;
    }

    @Override
    public void addWarning(OXException warning) {
        // nothing to do
    }

    @Override
    public void removeWarning(OXException warning) {
        // nothing to do
    }

    @Override
    public boolean supports(String serviceID, String accountID, FileStorageCapability... capabilities) throws OXException {
        return true;
    }

    @Override
    public boolean exists(String id, String version) throws OXException {
        return true;
    }

    @Override
    public File getFileMetadata(String id, String version) throws OXException {
        if (exToThrowForGetFileMetadata != null)
            throw exToThrowForGetFileMetadata;

        FileData fileData = fileDataStore.getFileData(id, version);
        if (fileData == null) {
            File file = new DefaultFile();
            file.setId(id);
            file.setVersion(version);
            fileData = new FileData(file, null);
        }
        return new DefaultFile(fileData.getFile());
    }

    @Override
    public String saveFileMetadata(File document, long sequenceNumber) throws OXException {
        return saveFileMetadata(document, sequenceNumber, null);
    }

    @Override
    public String saveFileMetadata(File document, long sequenceNumber, List<Field> modifiedColumns) throws OXException {
        if (exToThrowForSaveFileMetadata != null)
            throw exToThrowForSaveFileMetadata;

        String id = document.getId();
        String version = document.getVersion();
        FileData fileData = fileDataStore.getFileData(id, version);

        final File fileDataFile = (fileData == null) ? new DefaultFile() : fileData.getFile();
        if (fileData == null) {
            fileDataFile.setId(id);
            fileDataFile.setVersion(version);
            fileData = new FileData(fileDataFile, null);
        }

        if (modifiedColumns == null) {
            fileData.setFile(document);
        } else {
            modifiedColumns.stream()
                           .forEach(f -> copyProperty(fileDataFile, f.toString(), document));
        }
        if (fileDataFile.getLastModified() == null) {
            fileDataFile.setLastModified(new Date());
        }
        fileDataStore.setFileData(id, version, fileData);
        return id;
    }

    @Override
    public String saveFileMetadata(File document, long sequenceNumber, List<Field> modifiedColumns, boolean ignoreWarnings, boolean tryAddVersion) throws OXException {
        String id = document.getId();
        String version = document.getVersion();
        FileData fileData = fileDataStore.getFileData(id, version);
        
        if (fileData != null) {
            String newVersion = fileData.getFile().getVersion();
            Long versionValue = Long.valueOf(newVersion);
            ++versionValue;
            fileData.getFile().setVersion(versionValue.toString());
        }
        return saveFileMetadata(document, sequenceNumber, modifiedColumns);
    }

    @Override
    public String copy(String sourceId, String version, String destFolderId, File update, InputStream newData, List<Field> modifiedFields) throws OXException {
        return null;
    }

    @Override
    public List<String> move(List<String> sourceIds, long sequenceNumber, String destFolderId, boolean adjustFilenamesAsNeeded) throws OXException {
        return null;
    }

    @Override
    public InputStream getDocument(String id, String version) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InputStream getDocument(String id, String version, long offset, long length) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InputStream optThumbnailStream(String id, String version) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Document getDocumentAndMetadata(String id, String version, String clientEtag) throws OXException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber) throws OXException {
        return saveDocument(document, data, sequenceNumber, null);
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<Field> modifiedColumns) throws OXException {
        String id = document.getId();
        String version = document.getVersion();

        byte[] readData;
        try {
            readData = IOUtils.toByteArray(data);
        } catch (IOException e) {
            throw new OXException();
        }

        FileData fileData = fileDataStore.getFileData(id, version);
        if (fileData == null) {
            fileData = new FileData(document, readData);
        } else {
            fileData.setData(readData);
            if (modifiedColumns != null) {
                saveFileMetadata(document, sequenceNumber, modifiedColumns);
            } else {
                fileData.setFile(document);
            }
        }
        return id;
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<Field> modifiedColumns, boolean ignoreVersion) throws OXException {
        throw new OXException(new NotImplementedException("not supported"));
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<Field> modifiedColumns, boolean ignoreVersion, boolean ignoreWarnings, boolean tryAddVersion) throws OXException {
        throw new OXException(new NotImplementedException("not supported"));
    }

    @Override
    public String saveDocument(File document, InputStream data, long sequenceNumber, List<Field> modifiedColumns, long offset) throws OXException {
        throw new OXException(new NotImplementedException("not supported"));
    }

    @Override
    public void removeDocument(String folderId, long sequenceNumber) throws OXException {
        // nothing to do
    }

    @Override
    public List<String> removeDocument(List<String> ids, long sequenceNumber) throws OXException {
        return null;
    }

    @Override
    public List<String> removeDocument(List<String> ids, long sequenceNumber, boolean hardDelete) throws OXException {
        return null;
    }

    @Override
    public String[] removeVersion(String id, String[] versions) throws OXException {
        return null;
    }

    @Override
    public void unlock(String id) throws OXException {
        // nothing to do
    }

    @Override
    public void lock(String id, long diff) throws OXException {
        // nothing to do
    }

    @Override
    public void touch(String id) throws OXException {
        // nothing to do
    }

    @Override
    public TimedResult<File> getDocuments(String folderId) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getDocuments(String folderId, List<Field> columns) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getDocuments(String folderId, List<Field> columns, Field sort, SortDirection order) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getDocuments(String folderId, List<Field> fields, Field sort, SortDirection order, Range range) throws OXException {
        return null;
    }

    @Override
    public SearchIterator<File> getUserSharedDocuments(List<Field> fields, Field sort, SortDirection order) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getVersions(String id) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getVersions(String id, List<Field> columns) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getVersions(String id, List<Field> columns, Field sort, SortDirection order) throws OXException {
        return null;
    }

    @Override
    public TimedResult<File> getDocuments(List<String> ids, List<Field> columns) throws OXException {
        return null;
    }

    @Override
    public Delta<File> getDelta(String folderId, long updateSince, List<Field> columns, boolean ignoreDeleted) throws OXException {
        return null;
    }

    @Override
    public Delta<File> getDelta(String folderId, long updateSince, List<Field> columns, Field sort, SortDirection order, boolean ignoreDeleted) throws OXException {
        return null;
    }

    @Override
    public SearchIterator<File> search(String query, List<Field> cols, String folderId, Field sort, SortDirection order, int start, int end) throws OXException {
        return null;
    }

    @Override
    public SearchIterator<File> search(String query, List<Field> cols, String folderId, boolean includeSubfolders, Field sort, SortDirection order, int start, int end) throws OXException {
        return null;
    }

    @Override
    public SearchIterator<File> search(List<String> folderIds, SearchTerm<?> searchTerm, List<Field> fields, Field sort, SortDirection order, int start, int end) throws OXException {
        return null;
    }

    @Override
    public SearchIterator<File> search(String folderId, boolean includeSubfolders, SearchTerm<?> searchTerm, List<Field> fields, Field sort, SortDirection order, int start, int end) throws OXException {
        return null;
    }

    @Override
    public Map<String, Long> getSequenceNumbers(List<String> folderIds) throws OXException {
        return null;
    }

    @Override
    public Map<String, String> getETags(List<String> folderIds) throws OXException {
        return null;
    }

    @Override
    public Map<FileID, FileStorageFolder[]> restore(List<String> fileIds, String defaultDestFolderId) throws OXException {
        return null;
    }

    @Override
    public String getBackwardLink(String folderId, String id, Map<String, String> additionals) throws OXException {
        return null;
    }

    private void copyProperty(File target, String name, File source) {
        if (File.Field.ID.toString().equals(name)) {                
            target.setId(source.getId());
        } else if (File.Field.FOLDER_ID.toString().equals(name)) {                
            target.setFolderId(source.getFolderId());
        } else if (File.Field.FILE_SIZE.toString().equals(name)) {
            target.setFileSize(source.getFileSize());
        } else if (File.Field.FILENAME.toString().equals(name)) {
            target.setFileName(source.getFileName());
        } else if (File.Field.LAST_MODIFIED.toString().equals(name)) {
            target.setLastModified(source.getLastModified());
        } else if (File.Field.MODIFIED_BY.toString().equals(name)) {
            target.setModifiedBy(source.getModifiedBy());
        } else if (File.Field.META.toString().equals(name)) {
            target.setMeta(source.getMeta());
        } 
    }

}
