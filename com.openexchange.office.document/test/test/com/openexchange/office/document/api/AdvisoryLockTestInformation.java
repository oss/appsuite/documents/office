/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.api;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import java.util.Collection;
import java.util.HashSet;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.openexchange.config.ConfigurationService;
import com.openexchange.file.storage.composition.IDBasedAdministrativeFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.File;
import com.openexchange.office.rt2.fields.RealtimeIDAccess;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import test.com.openexchange.office.document.util.FileDataStore;
import test.com.openexchange.office.document.util.FileHelperServiceMock;
import test.com.openexchange.office.document.util.IDBasedFileAccessFactoryMock;
import test.com.openexchange.office.document.util.StorageHelperServiceMock;


public class AdvisoryLockTestInformation extends UnittestInformation {
    private IDBasedFileAccessFactoryMock myIdBasedFileAccessFactoryMock;
    private IDBasedFileAccessFactory fileIdBasedAccessFactory;
    private FileHelperServiceFactory fileHelperServiceFactory;
    private StorageHelperServiceFactory storageHelperServiceFactory;

    public FileDataStore getFileDataStore() {
        return (FileDataStore)myIdBasedFileAccessFactoryMock;
    }

    public IDBasedFileAccessFactory getIDBasedFileAccessFactory() {
        return fileIdBasedAccessFactory;
    }

    public FileHelperServiceFactory getFileHelperServiceFactory() {
        return fileHelperServiceFactory;
    }

    public StorageHelperServiceFactory getStorageHelperServiceFactory() {
        return storageHelperServiceFactory; 
    }

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res = new HashSet<>();
        res.add(StorageHelperServiceFactory.class);
        res.add(FileHelperServiceFactory.class);
        res.add(IDBasedFileAccessFactory.class);
        res.add(RealtimeIDAccess.class);
        return res;
    }

    @Override
    protected void initMocks() {
        ServerSession session = mock(ServerSession.class);
        User mockUser = mock(User.class);
        when(session.getUser()).thenReturn(mockUser);

        ConfigurationService cfgService = this.testOsgiBundleContextAndUnitTestActivator.getMock(ConfigurationService.class);
        when(cfgService.getBoolProperty(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(true);
        when(cfgService.getProperty(Mockito.startsWith("com.openexchange.office.advisoryLock"), Mockito.anyString())).thenReturn("test-server");

        myIdBasedFileAccessFactoryMock = new IDBasedFileAccessFactoryMock();
        fileIdBasedAccessFactory = this.testOsgiBundleContextAndUnitTestActivator.getMock(IDBasedFileAccessFactory.class);
        when(fileIdBasedAccessFactory.createAccess(Mockito.isA(Session.class))).thenAnswer(new Answer<IDBasedFileAccess>() {
            @SuppressWarnings("synthetic-access")
            @Override
            public IDBasedFileAccess answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return myIdBasedFileAccessFactoryMock.createAccess((Session) args[0]);
            }
        });
        when(fileIdBasedAccessFactory.createAccess(Mockito.isA(Integer.class))).thenAnswer(new Answer<IDBasedAdministrativeFileAccess>() {
            @SuppressWarnings("synthetic-access")
            @Override
            public IDBasedAdministrativeFileAccess answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return myIdBasedFileAccessFactoryMock.createAccess((Integer) args[0]);
            }
        });

        storageHelperServiceFactory = this.testOsgiBundleContextAndUnitTestActivator.getMock(StorageHelperServiceFactory.class);
        when(storageHelperServiceFactory.create(Mockito.isA(Session.class), Mockito.anyString())).thenReturn(new StorageHelperServiceMock(true));

        fileHelperServiceFactory = this.testOsgiBundleContextAndUnitTestActivator.getMock(FileHelperServiceFactory.class);
        when(fileHelperServiceFactory.create(Mockito.isA(Session.class))).thenAnswer(new Answer<FileHelperService>() {
            @Override
            public FileHelperService answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return new FileHelperServiceMock(getFileDataStore(), (Session) args[0]);
            }
        });

        RealtimeIDAccess rtIDAccess = this.testOsgiBundleContextAndUnitTestActivator.getMock(RealtimeIDAccess.class);
        when(rtIDAccess.getRealtimeID(Mockito.isA(File.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                String result = "";
                Object[] args = invocation.getArguments();
                File file = (File)args[0];
                if (file != null) {
                    String id = file.getId();
                    String[] parts = id.split("/");
                    // id of the file-id is 2nd part - 1st is folder-id
                    result = parts[1];
                }
                return result;
            }
        });
    }

}
