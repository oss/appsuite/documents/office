/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.time.LocalDateTime;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.document.api.AdvisoryLockInfo;
import com.openexchange.office.tools.common.error.ErrorCode;

public class AdvisoryLockInfoTest {

    @Test
    public void testAdvisoryLockInfo() throws Exception {
        AdvisoryLockInfo advLockInfo = new AdvisoryLockInfo(ErrorCode.NO_ERROR);
        assertEquals(advLockInfo.getErrorCode(), ErrorCode.NO_ERROR);

        final String hostName = "test-server.test.org";
        final int cid = 1;
        final int uid = 1000;
        final String fileId = "10/1000";
        final String fileId2 = "20/2000";
        final String sinceDateTime = LocalDateTime.now().toString();
        advLockInfo = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime, fileId);
        assertEquals(advLockInfo.getErrorCode(), ErrorCode.NO_ERROR);
        assertEquals(advLockInfo.getContextId(), cid);
        assertEquals(advLockInfo.getUserId(), uid);
        assertEquals(advLockInfo.getSinceDateTime(), sinceDateTime);
        assertEquals(advLockInfo.getName(), hostName);
        assertEquals(advLockInfo.getOrigFileId(), fileId);
        assertFalse(advLockInfo.getAdvisoryLockWritten());

        JSONObject json = advLockInfo.asJSONObject();
        AdvisoryLockInfo advLockInfoFromJSON = new AdvisoryLockInfo(json);
        assertEquals(advLockInfo, advLockInfoFromJSON);
        assertFalse(advLockInfoFromJSON.getAdvisoryLockWritten());
        advLockInfoFromJSON.setAdvisoryLockWritten(true);
        assertTrue(advLockInfoFromJSON.getAdvisoryLockWritten());
        assertEquals(advLockInfoFromJSON.getErrorCode(), ErrorCode.NO_ERROR);
        advLockInfoFromJSON.setErrorCode(ErrorCode.ADVISORY_LOCK_SET_WARNING);
        assertEquals(advLockInfoFromJSON.getErrorCode(), ErrorCode.ADVISORY_LOCK_SET_WARNING);

        int hash1 = advLockInfo.hashCode();
        int hash2 = advLockInfoFromJSON.hashCode();
        assertEquals(hash1, hash2);

        final String sinceDateTime2 = LocalDateTime.now().toString();
        AdvisoryLockInfo advLockInfo10 = new AdvisoryLockInfo("abc", cid, uid, sinceDateTime, fileId);
        AdvisoryLockInfo advLockInfo11 = new AdvisoryLockInfo(null, cid, uid, sinceDateTime, fileId);
        AdvisoryLockInfo advLockInfo111 = new AdvisoryLockInfo(null, cid, uid, sinceDateTime, fileId);
        AdvisoryLockInfo advLockInfo2 = new AdvisoryLockInfo(hostName, 2, uid, sinceDateTime, fileId);
        AdvisoryLockInfo advLockInfo3 = new AdvisoryLockInfo(hostName, cid, 1001, sinceDateTime, fileId);
        AdvisoryLockInfo advLockInfo40 = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime2, fileId);
        AdvisoryLockInfo advLockInfo41 = new AdvisoryLockInfo(hostName, cid, uid, null, fileId);
        AdvisoryLockInfo advLockInfo411 = new AdvisoryLockInfo(hostName, cid, uid, null, fileId);
        AdvisoryLockInfo advLockInfo50 = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime2, fileId);
        AdvisoryLockInfo advLockInfo51 = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime2, null);
        AdvisoryLockInfo advLockInfo511 = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime2, null);
        AdvisoryLockInfo advLockInfo512 = new AdvisoryLockInfo(hostName, cid, uid, sinceDateTime2, fileId2);
        assertEquals(advLockInfo, advLockInfo);
        assertNotEquals(advLockInfo, null);
        assertNotEquals(advLockInfo, "test");
        assertNotEquals(advLockInfo, advLockInfo10);
        assertNotEquals(advLockInfo11, advLockInfo);
        assertNotEquals(advLockInfo, advLockInfo2);
        assertNotEquals(advLockInfo, advLockInfo3);
        assertNotEquals(advLockInfo, advLockInfo40);
        assertNotEquals(advLockInfo41, advLockInfo);
        assertEquals(advLockInfo11, advLockInfo111);
        assertEquals(advLockInfo41, advLockInfo411);
        assertNotEquals(advLockInfo, advLockInfo50);
        assertNotEquals(advLockInfo51, advLockInfo50);
        assertEquals(advLockInfo51, advLockInfo511);
        assertNotEquals(advLockInfo50, advLockInfo512);

        AdvisoryLockInfo advLockInfoWithNull1 = new AdvisoryLockInfo(null, cid, uid, null, fileId);
        int hashWithNull1 = advLockInfoWithNull1.hashCode();
        assertNotEquals(0, hashWithNull1);

        AdvisoryLockInfo advLockInfoWithNull2 = new AdvisoryLockInfo(null, cid, uid, sinceDateTime, null);
        int hashWithNull2 = advLockInfoWithNull2.hashCode();
        assertNotEquals(0, hashWithNull2);
}

}
