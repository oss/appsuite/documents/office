/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.document.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.document.AdvisoryLockInfoServiceImpl;
import com.openexchange.office.document.api.AdvisoryLockInfo;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import test.com.openexchange.office.document.util.DocumentTestOsgiBundleContextAndUnitTestActivator;
import test.com.openexchange.office.document.util.FileHelperServiceMock;
import test.com.openexchange.office.document.util.IDBasedFileAccessMock;
import test.com.openexchange.office.document.util.SessionMock;
import test.com.openexchange.office.document.util.StorageHelperServiceMock;

public class AdvisoryLockInfoServiceTest {

    private DocumentTestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private AdvisoryLockTestInformation unittestInformation;

    private static final String CFG_ADVISORY_LOCK_KEY = "com.openexchange.office.advisoryLock";

    @BeforeEach
    public void init() throws OXException {
        unittestInformation = new AdvisoryLockTestInformation();
        unitTestBundle = new DocumentTestOsgiBundleContextAndUnitTestActivator(unittestInformation);
        unitTestBundle.start();
    }

    @Test
    public void testAdvisoryLockInfoServiceSimple() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        ConfigurationService cfgService = unitTestBundle.getService(ConfigurationService.class);
        unittestInformation.getFileDataStore().resetFileData();

        assertTrue(advisoryLockInfoService.isAdvisoryLockEnabled());

        AdvisoryLockMode advLockMode = advisoryLockInfoService.getAdvisoryLockMode();
        assertEquals(advLockMode, AdvisoryLockMode.ADVISORY_LOCK_MODE_CUSTOM);

        String lockSystemName = advisoryLockInfoService.getAdvisoryLockSystemName();
        String cfgValue = cfgService.getProperty(CFG_ADVISORY_LOCK_KEY, "");
        assertEquals(lockSystemName, cfgValue);

        // configure origin mode
        when(cfgService.getProperty(Mockito.startsWith("com.openexchange.office.advisoryLock"), Mockito.anyString())).thenReturn("origin");
        advLockMode = advisoryLockInfoService.getAdvisoryLockMode();
        assertEquals(advLockMode, AdvisoryLockMode.ADVISORY_LOCK_MODE_ORIGIN);

        // configure disabled advisory lock mode
        when(cfgService.getProperty(Mockito.startsWith("com.openexchange.office.advisoryLock"), Mockito.anyString())).thenReturn("");
        advLockMode = advisoryLockInfoService.getAdvisoryLockMode();
        assertEquals(advLockMode, AdvisoryLockMode.ADVISORY_LOCK_MODE_NOT_ENABLED);

        // set custom mode again
        when(cfgService.getProperty(Mockito.startsWith("com.openexchange.office.advisoryLock"), Mockito.anyString())).thenReturn("test-server");
    }

    @Test
    public void testCheckAndUpdateAdvisoryLock() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertTrue(advLockInfo != null);
        assertTrue(advLockInfo.getAdvisoryLockWritten());
        assertEquals(name, advLockInfo.getName());
        assertEquals(session.getUserId(), advLockInfo.getUserId());
        assertEquals(session.getContextId(), advLockInfo.getContextId());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // do call checkAndUpdateAdvisoryLock() again - from other system -> should result in warning
        // and advisory lock itself has not been changed
        final String name2 = "test-system-2.local.org";
        advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name2, folderId, id);
        assertTrue(advLockInfo != null);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(name, advLockInfo.getName());
        assertEquals(session.getUserId(), advLockInfo.getUserId());
        assertEquals(session.getContextId(), advLockInfo.getContextId());
        assertEquals(ErrorCode.ADVISORY_LOCK_SET_WARNING.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());
    }

    @Test
    public void testCheckAndUpdateAdvisoryLockWithCopiedFile() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertTrue(advLockInfo != null);
        assertTrue(advLockInfo.getAdvisoryLockWritten());
        assertEquals(name, advLockInfo.getName());
        assertEquals(session.getUserId(), advLockInfo.getUserId());
        assertEquals(session.getContextId(), advLockInfo.getContextId());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // create a copy of the custom meta data and setting it to a new file
        final String idCopy = "1000/1001";
        final String folderIdCopy = "1000";
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        File metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        Map<String, Object> customMetaData = metaData.getMeta();
        DefaultFile copiedFileMetaData = new DefaultFile();
        copiedFileMetaData.setId(idCopy);
        copiedFileMetaData.setFolderId(folderIdCopy);
        copiedFileMetaData.setMeta(customMetaData);
        fileAccess.saveFileMetadata(copiedFileMetaData, FileStorageFileAccess.DISTANT_FUTURE);

        // check with the copied meta data file with a different system name -> no warning/error as copy is detected &
        // the advisory lock custom meta data is updated for the file
        final String name2 = "test-system-2.local.org";
        advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name2, folderIdCopy, idCopy);
        assertTrue(advLockInfo != null);
        assertTrue(advLockInfo.getAdvisoryLockWritten());
        assertEquals(name2, advLockInfo.getName());
        assertEquals(session.getUserId(), advLockInfo.getUserId());
        assertEquals(session.getContextId(), advLockInfo.getContextId());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());
    }

    @Test
    public void testCheckAndUpdateAdvisoryLockWithOldJSONVersionData() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        // create a the file meta data manually to have an older version string written
        final IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        final DefaultFile copiedFileMetaData = new DefaultFile();
        copiedFileMetaData.setId(id);
        copiedFileMetaData.setFolderId(folderId);
        copiedFileMetaData.setLastModified(new Date());

        final Instant now = Instant.now();
        AdvisoryLockInfo lockInfo = new AdvisoryLockInfo(name, cid, uid, now.toString(), "");
        JSONObject lockInfoAsJSON = lockInfo.asJSONObject();
        lockInfoAsJSON.remove("origFileId"); // copied from AdvisoryLockInfo
        Map<String, Object> customMetaData = new HashMap<>();
        customMetaData.put(AdvisoryLockInfo.ADVISORYLOCK_KEY, lockInfoAsJSON.toString());
        copiedFileMetaData.setMeta(customMetaData);
        fileAccess.saveFileMetadata(copiedFileMetaData, FileStorageFileAccess.DISTANT_FUTURE);

        // using a 2nd system name -> must raise warning - this is the old behavior
        final String name2 = "test-system-2.local.org";
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name2, folderId, id);
        assertTrue(advLockInfo != null);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(name, advLockInfo.getName());
        assertEquals(session.getUserId(), advLockInfo.getUserId());
        assertEquals(session.getContextId(), advLockInfo.getContextId());
        assertEquals(ErrorCode.ADVISORY_LOCK_SET_WARNING.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());
    }

    @Test
    public void testCheckAndUpdateAdvisoryLockWithBadCustomMetaData() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        // create file meta data with bad advisory lock meta data
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        Map<String, Object> customMetaData = new HashMap<>();
        customMetaData.put(AdvisoryLockInfo.ADVISORYLOCK_KEY, "abcd");
        DefaultFile copiedFileMetaData = new DefaultFile();
        copiedFileMetaData.setId(id);
        copiedFileMetaData.setFolderId(folderId);
        copiedFileMetaData.setMeta(customMetaData);
        fileAccess.saveFileMetadata(copiedFileMetaData, FileStorageFileAccess.DISTANT_FUTURE);

        // advisory lock implementation should detect non-json readable string and provide error
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(advLockInfo.getErrorCode().getCodeAsStringConstant(), ErrorCode.ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR.getCodeAsStringConstant());
    }

    @Test
    public void testResetAdvisoryLock() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid1 = 1;
        final int uid1 = 1;
        Session session1 = new SessionMock(cid1, uid1);

        final String nameSys1 = "test-system-1.local.org";
        AdvisoryLockInfo advLockInfo1 = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session1, nameSys1, folderId, id);
        assertTrue(advLockInfo1 != null);
        assertTrue(advLockInfo1.getAdvisoryLockWritten());

        // check file access data for initial advisory lock data
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session1);
        File metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        assertNotNull(metaData);
        Map<String, Object> customMetaData = metaData.getMeta();
        assertNotNull(customMetaData);
        Object advLockData = customMetaData.get(AdvisoryLockInfo.ADVISORYLOCK_KEY);
        assertNotNull(advLockData);
        assertTrue(advLockData instanceof String);
        JSONObject advLockJSON = new JSONObject((String)advLockData);
        AdvisoryLockInfo advLockInfoFromMeta = new AdvisoryLockInfo(advLockJSON);
        assertEquals(advLockInfo1, advLockInfoFromMeta);

        final int cid2 = 1;
        final int uid2 = 2;
        Session session2 = new SessionMock(cid2, uid2);

        final String nameSys2 = "test-system-2.local.org";
        boolean result = advisoryLockInfoService.resetAdvisoryLock(session2, nameSys2, folderId, id);
        assertTrue(result);

        // check file access data for overwritten advisory lock data
        fileAccess = fileAccessFactory.createAccess(session2);
        metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        assertNotNull(metaData);
        customMetaData = metaData.getMeta();
        assertNotNull(customMetaData);
        advLockData = customMetaData.get(AdvisoryLockInfo.ADVISORYLOCK_KEY);
        assertNotNull(advLockData);
        assertTrue(advLockData instanceof String);
        advLockJSON = new JSONObject((String)advLockData);
        AdvisoryLockInfo advLockInfo2 = new AdvisoryLockInfo(advLockJSON);
        assertEquals(nameSys2, advLockInfo2.getName());
        assertEquals(session2.getContextId(), advLockInfo2.getContextId());
        assertEquals(session2.getUserId(), advLockInfo2.getUserId());
        assertNotNull(advLockInfo2.getSinceDateTime());
    }

    @Test
    public void testClearAdvisoryLock() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);
        final String nameSys1 = "test-system-1.local.org";
        AdvisoryLockInfo advLockInfo1 = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, nameSys1, folderId, id);
        assertNotNull(advLockInfo1);
        assertTrue(advLockInfo1.getAdvisoryLockWritten());

        advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);

        // check with file access data for cleared advisory lock data
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        File metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        assertNotNull(metaData);
        Map<String, Object> customMetaData = metaData.getMeta();
        assertNotNull(customMetaData);
        Object advLockData = customMetaData.get(AdvisoryLockInfo.ADVISORYLOCK_KEY);
        assertNull(advLockData);
    }

    @Test
    public void testAdvisoryLockErrorCaseBadJSON() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        // prepare bad advisory lock string in custom meta data of file
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        File metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        assertNotNull(metaData);
        Map<String, Object> customMetaData = metaData.getMeta();
        assertNotNull(customMetaData);
        Object advLockData = customMetaData.get(AdvisoryLockInfo.ADVISORYLOCK_KEY);
        assertNull(advLockData);

        // prepare bad advisory lock string in custom meta data of file and store it
        customMetaData.put(AdvisoryLockInfo.ADVISORYLOCK_KEY, "{ \"test:\" \"test\"}");
        metaData.setMeta(customMetaData);
        fileAccess.saveFileMetadata(metaData, FileStorageFileAccess.DISTANT_FUTURE);

        final String nameSys = "test-system-1.local.org";
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, nameSys, folderId, id);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(advLockInfo.getErrorCode().getCodeAsStringConstant(), ErrorCode.ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR.getCodeAsStringConstant());
    }

    @Test
    public void testCheckAndUpdateAdvisoryLockWithOXException() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        // prepare bad IDBasedFileAcces instance
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        if (fileAccess instanceof IDBasedFileAccessMock) {
            IDBasedFileAccessMock mock = (IDBasedFileAccessMock)fileAccess;
            mock.setThrowExceptionForGetFileMetaData(new OXException());
        }

        final String nameSys = "test-system-1.local.org";
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, nameSys, folderId, id);
        assertNotNull(advLockInfo);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(advLockInfo.getErrorCode().getCodeAsStringConstant(), ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
    }

    @Test
    public void testResetAdvisoryLockWithOXException() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        // prepare bad IDBasedFileAcces instance
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        if (fileAccess instanceof IDBasedFileAccessMock) {
            IDBasedFileAccessMock mock = (IDBasedFileAccessMock)fileAccess;
            mock.setThrowExceptionForGetFileMetaData(new OXException());
        }

        final String nameSys = "test-system-1.local.org";
        boolean success = advisoryLockInfoService.resetAdvisoryLock(session, nameSys, folderId, id);
        assertFalse(success);
    }

    @Test
    public void testClearAdvisoryLockWithOXException() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        final String id = "1000/1000";
        final String folderId = "1000";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final String nameSys1 = "test-system-1.local.org";
        AdvisoryLockInfo advLockInfo1 = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, nameSys1, folderId, id);
        assertNotNull(advLockInfo1);
        assertTrue(advLockInfo1.getAdvisoryLockWritten());

        // prepare bad IDBasedFileAcces instance
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        IDBasedFileAccessMock fileAccessMock = null;
        if (fileAccess instanceof IDBasedFileAccessMock) {
            fileAccessMock = (IDBasedFileAccessMock)fileAccess;
            fileAccessMock.setThrowExceptionForGetFileMetaData(new OXException());
        }

        advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);

        // check with file access data that clearing didn't work due to exception
        assertNotNull(fileAccessMock);
        fileAccessMock.setThrowExceptionForGetFileMetaData(null);
        File metaData = fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
        assertNotNull(metaData);
        Map<String, Object> customMetaData = metaData.getMeta();
        assertNotNull(customMetaData);
        Object advLockData = customMetaData.get(AdvisoryLockInfo.ADVISORYLOCK_KEY);
        assertNotNull(advLockData);
    }

    @Test
    public void testAdvisoryLockMethodsWithNoMetaDataSupport() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        StorageHelperServiceFactory storageHelperServiceFactory = unittestInformation.getStorageHelperServiceFactory();

        // set mockito rule to provide no support for custom meta-data
        when(storageHelperServiceFactory.create(Mockito.isA(Session.class), Mockito.anyString())).thenReturn(new StorageHelperServiceMock(false));

        // advisory lock gets not written, but no error code is provided
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertNotNull(advLockInfo);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // reset does not work either
        boolean success = advisoryLockInfoService.resetAdvisoryLock(session, name, folderId, id);
        assertFalse(success);

        try {
            advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);
        } catch (Exception e) {
            fail("Exception caught while trying to clearAdvisoryLock()");
        }

        // reset mockito rule
        when(storageHelperServiceFactory.create(Mockito.isA(Session.class), Mockito.anyString())).thenReturn(new StorageHelperServiceMock(true));
    }

    @Test
    public void testAdvisoryLockMethodsWithNoIDBasedFileAccess() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        // ensure that advisory lock implementation cannot get valid IDBasedFileAccess instance
        IDBasedFileAccessFactory fileAccessFactory = unittestInformation.getIDBasedFileAccessFactory();
        when(fileAccessFactory.createAccess(Mockito.isA(Session.class))).thenReturn(null);

        // advisory lock gets not written, but no error code is provided
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertNotNull(advLockInfo);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // reset does not work either
        boolean success = advisoryLockInfoService.resetAdvisoryLock(session, name, folderId, id);
        assertFalse(success);

        try {
            advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);
        } catch (Exception e) {
            fail("Exception caught while trying to clearAdvisoryLock()");
        }

        // reset mockito rule to get IDBasedFileAccess instances again
        IDBasedFileAccessFactory fileAccessFactoryMock = (IDBasedFileAccessFactory)unittestInformation.getFileDataStore();
        when(fileAccessFactory.createAccess(Mockito.isA(Session.class))).thenAnswer(new Answer<IDBasedFileAccess>() {
            @Override
            public IDBasedFileAccess answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return fileAccessFactoryMock.createAccess((Session) args[0]);
            }
        });

    }

    @Test
    public void testAdvisoryLockMethodsWithNoWriteAccess() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        // ensure that advisory lock implementation cannot read/write/create files
        FileHelperServiceFactory fileHelperServiceFactory = unittestInformation.getFileHelperServiceFactory();
        when(fileHelperServiceFactory.create(Mockito.isA(Session.class))).thenAnswer(new Answer<FileHelperService>() {
            @SuppressWarnings("synthetic-access")
            @Override
            public FileHelperService answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              FileHelperServiceMock mock = new FileHelperServiceMock(unittestInformation.getFileDataStore(), (Session) args[0]);
              mock.setCanReadResult(false);
              mock.setCanWriteResult(false);
              return mock;
            }
        });

        // advisory lock gets not written, but no error code is provided
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertNotNull(advLockInfo);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(ErrorCode.NO_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // reset does not work either
        boolean success = advisoryLockInfoService.resetAdvisoryLock(session, name, folderId, id);
        assertFalse(success);

        try {
            advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);
        } catch (Exception e) {
            fail("Exception caught while trying to clearAdvisoryLock()");
        }

        // reset mockito rule for read/write/create file access
        fileHelperServiceFactory = unittestInformation.getFileHelperServiceFactory();
        when(fileHelperServiceFactory.create(Mockito.isA(Session.class))).thenAnswer(new Answer<FileHelperService>() {
            @SuppressWarnings("synthetic-access")
            @Override
            public FileHelperService answer(InvocationOnMock invocation) throws Throwable {
              Object[] args = invocation.getArguments();
              return new FileHelperServiceMock(unittestInformation.getFileDataStore(), (Session) args[0]);
            }
        });

    }

    @Test
    public void testAdvisoryLockMethodsWithOXExceptionOnSaveFileMetadata() throws Exception {
        AdvisoryLockInfoService advisoryLockInfoService = prepareAdvisoryLockInfoService();
        unittestInformation.getFileDataStore().resetFileData();

        final String id = "1000/1000";
        final String folderId = "1000";
        final String name = "test-system.local.org";

        final int cid = 1;
        final int uid = 1;
        Session session = new SessionMock(cid, uid);

        final IDBasedFileAccessFactory fileAccessFactory = unitTestBundle.getService(IDBasedFileAccessFactory.class);

        // prepare bad IDBasedFileAcces instance
        IDBasedFileAccess fileAccess = fileAccessFactory.createAccess(session);
        if (fileAccess instanceof IDBasedFileAccessMock) {
            IDBasedFileAccessMock mock = (IDBasedFileAccessMock)fileAccess;
            mock.setThrowExceptionForSaveFileMetaData(new OXException());
        }

        // advisory lock gets not written & error code is provided
        AdvisoryLockInfo advLockInfo = advisoryLockInfoService.checkAndUpdateAdvisoryLock(session, name, folderId, id);
        assertNotNull(advLockInfo);
        assertFalse(advLockInfo.getAdvisoryLockWritten());
        assertEquals(ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant(), advLockInfo.getErrorCode().getCodeAsStringConstant());

        // reset does not work either
        boolean success = advisoryLockInfoService.resetAdvisoryLock(session, name, folderId, id);
        assertFalse(success);

        try {
            advisoryLockInfoService.clearAdvisoryLock(session, folderId, id);
        } catch (Exception e) {
            fail("Exception caught while trying to clearAdvisoryLock()");
        }

        // reset bas file access instance
        if (fileAccess instanceof IDBasedFileAccessMock) {
            IDBasedFileAccessMock mock = (IDBasedFileAccessMock)fileAccess;
            mock.setThrowExceptionForSaveFileMetaData(null);
        }

    }

    AdvisoryLockInfoService prepareAdvisoryLockInfoService() {
        AdvisoryLockInfoService advisoryLockInfoService = new AdvisoryLockInfoServiceImpl();
        unitTestBundle.injectDependencies(advisoryLockInfoService);
        return advisoryLockInfoService;
    }

}
