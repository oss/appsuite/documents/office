/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.office.document.api.BackupFileService;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;

/**
 * Service to provide special functions for the bak-file handling. 
 *
 * @author Carsten Driesner
 * @since 7.10.4
 *
 */
@Service
@RegisteredService(registeredClass=BackupFileService.class)
public class BackupFileServiceImpl implements BackupFileService {
	private final static Logger log = LoggerFactory.getLogger(BackupFileServiceImpl.class);

	@Autowired
	private StorageHelperServiceFactory storageHelperServiceFactory;
	
	@Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;
	
	@Autowired
	private FileHelperServiceFactory fileHelperServiceFactory;
	
    /**
     * Checks whether the user can create or write a backup file in the
     * folder provided.
     *
     * @param session the session of the user that needs to write/create a bak-file
     * @param folderId the folder id where the bak-file should be written/created
     * @param metaData the document meta data of the original document file
     * @param encrypted specifies if the original document file is encrypted or not
     * @return TRUE if the user is able to create/write a bak, otherwise FALSE
     */

	@Override
	public boolean canCreateOrWriteBackupFile(final Session session, final String folderId, final DocumentMetaData metaData, boolean encrypted) {
		final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);

		boolean canCreateWriteBackupFile = false;

		if (!storageHelper.supportsFileVersions()) {
			// we must check further requirements to answer the question
			try {
				final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
				final int[] permissions = folderHelperService.getFolderPermissions(folderId);

				if (null != permissions) {
					final int write = permissions[FolderHelperService.FOLDER_PERMISSIONS_WRITE];
					final int folder = permissions[FolderHelperService.FOLDER_PERMISSIONS_FOLDER];

					if ((folder >= com.openexchange.file.storage.FileStoragePermission.CREATE_OBJECTS_IN_FOLDER)
							&& (write >= com.openexchange.file.storage.FileStoragePermission.WRITE_ALL_OBJECTS)) {
						// permissions are sufficient to create/write a bak file - no further tests
						// necessary
						canCreateWriteBackupFile = true;
					} else {
						// We have again dig deeper and now check a possible bak-file existence
						// First check file existence using the current file name
						final String fileName = metaData.getFileName();
						final String backupFileName = FileHelper.createFilenameWithPostfix(fileName,
								BackupFileService.BACKUP_FILE_POSTFIX, encrypted);
						// BE CAREFULL - search must be done using the final file name including a
						// possible .pgp extension in case of encryption!!
						final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
						final String searchBakFileName = (encrypted
								? backupFileName + "." + com.openexchange.office.tools.common.files.FileHelper.STR_EXT_PGP : backupFileName);
						final File bakMetaData = fileHelperService.getMetaDataFromFileName(folderId, searchBakFileName);

						if (null != bakMetaData) {
							// BACKUP file exists - check write permissions
							canCreateWriteBackupFile = fileHelperService.canWriteToFile(bakMetaData, session.getUserId());
						} else {
							// BACKUP file doesn't exists - check folder permissions
							canCreateWriteBackupFile = (folder >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER);
						}
					}
				}
			} catch (Exception e) {
				log.error("BackupFileServiceImpl: Exception caught while trying to determine backup file permissions", e);
			}
		} else {
			canCreateWriteBackupFile = true;
		}

		return canCreateWriteBackupFile;
	}

}
