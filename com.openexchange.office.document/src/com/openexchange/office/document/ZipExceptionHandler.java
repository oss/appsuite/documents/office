/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.util.zip.ZipException;

import org.apache.commons.lang3.StringUtils;

public class ZipExceptionHandler {
	private static final String ZIP_PREFIX_INVALID_ENTRY_CRC = "invalid entry CRC";

	private ZipExceptionHandler() {
		// nothing to do
	}

	/**
	 * Interpret a ZipException instance and decide if the exception is
	 * fatal for further processing or not.
	 *
	 * @param e an arbitrary ZipException
	 * @return TRUE if we should not further process the Zip stream and FALSE
	 * if we should ignore the exception.
	 */
	public static boolean isFatalZipError(ZipException e) {
		return isCRCError(e) ? false : true;
	}

	/**
	 * Determines if the ZipException describes a CRC error.
	 *
	 * @param e an arbitrary ZipException
	 * @return TRUE if the ZipException instance describes a CRC error,
	 * otherwise FALSE
	 */
	public static boolean isCRCError(ZipException e) {
		final String msg = e.getMessage();
		if (StringUtils.isNotEmpty(msg) && msg.startsWith(ZIP_PREFIX_INVALID_ENTRY_CRC, 0)) {
			return true;
		}

		return false;
	}
}
