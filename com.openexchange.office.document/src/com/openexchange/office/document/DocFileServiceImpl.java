/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.java.Strings;
import com.openexchange.office.document.api.DocFileService;
import com.openexchange.office.document.api.DocumentConstants;
import com.openexchange.office.document.api.LockingInfo;
import com.openexchange.office.document.api.RenameResult;
import com.openexchange.office.document.api.WriteInfo;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.StreamInfo;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tx.TransactionAwares;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link DocFileServiceImpl}
 *
 * The DocFileHelper class contains many utility methods to retrieve a document
 * stream from a file or write a stream to a file. To retrieve meta data from
 * a file, e.g. permissions, locking and determine mine type, extension, document
 * type/format.
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class DocFileServiceImpl implements DocFileService {

    private static final Logger LOG = LoggerFactory.getLogger(DocFileServiceImpl.class);
    private static final Field UPDATE_METADATA_FIELDS[] = { Field.VERSION };

    // - Members --------------------------------------------------------

    private AtomicBoolean docFormatAndTypeSet = new AtomicBoolean(false);
    private DocumentFormat documentFormat = DocumentFormat.NONE;
    private DocumentType documentType = DocumentType.NONE;

    @Autowired
    private UserService userService;

    @Autowired
    private IDBasedFileAccessFactory fileFactory;

    @Autowired
    private StorageHelperServiceFactory storageHelperServiceFactory;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    @Autowired(required = false)
    private CryptographicAwareIDBasedFileAccessFactory encryptionAwareFileAccessFactory;

    @Autowired(required = false)
    private CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory;

    /**
     * Retrieves the document format.
     *
     * @return
     *         The document format of the file used by the file helper
     *         instance.
     */
    public DocumentFormat getDocumentFormat() {
        return documentFormat;
    }

    /**
     * Retrieves the document type.
     *
     * @return
     *         The document format of the file used by the file helper
     *         instance.
     */
    public DocumentType getDocumentType() {
        return documentType;
    }

    /**
     * Access the responsible storage adapter to retrieve the meta
     * data directly from the storage. This method doesn't cache
     * the data and is therefore expensive!
     *
     * @param session
     *            The session to be used for a possible access to the real
     *            meta data.
     *
     * @return
     */
    public File getMetaDataFromStorage(Session session, final String fileId) throws OXException {
        return getMetaDataFromFile(session, fileId, FileStorageFileAccess.CURRENT_VERSION);
    }

    /**
     * Retrieves the document stream from the file.
     *
     * @param session
     *            The session of the user who requested to retrieve the document
     *            stream.
     *
     * @return
     *         The document stream or null if the document stream could not
     *         be retrieved.
     */
    public StreamInfo getDocumentStream(Session session, final String folderId, final String fileId, String encryptionInfo) {
        final StreamInfo streamInfo = getDocumentStream(session, folderId, fileId, FileStorageFileAccess.CURRENT_VERSION, encryptionInfo);

        if (null != streamInfo) {
            initDocFormatAndType(streamInfo);
        }

        return streamInfo;
    }

    /**
     * Retrieves the document stream requested by the AJAX request data.
     *
     * @param services
     *            The service lookup to be used by the method to create necessary
     *            services.
     *
     * @param session
     *            The session of the user who requested to retrieve the document
     *            stream.
     *
     * @param requestData
     *            The AJAX request data which must contain the id of the file, folder_id and
     *            the version to be used to retrieve the document stream. If the version
     *            is omitted the method uses the current version of the file.
     *
     * @return
     *         The document stream or null if the document stream could not
     *         be retrieved.
     */
    public StreamInfo getDocumentStream(Session session, AJAXRequestData requestData, final String encryptionInfo) {
        final String fileId = requestData.getParameter("id");
        final String folderId = requestData.getParameter("folder_id");
        String version = requestData.getParameter("version");

        if ((null == version) || (0 == version.length()) || (version.equals("0"))) {
            version = FileStorageFileAccess.CURRENT_VERSION;
        }

        // create temporary DocFileHelper instance and cal
        return getDocumentStream(session, folderId, fileId, version, encryptionInfo);
    }

    /**
     * Retrieve user information about the user who locked a file.
     *
     * @param session
     * @param file
     * @return
     *         The locking information with user id and user name or NULL if nobody has
     *         locked the file.
     */
    @Override
    public LockingInfo getLockingInfo(final Session session, final File file) {
        Validate.notNull(session);
        Validate.notNull(file);

        final LockingInfo result = new LockingInfo();

        final Date now = new Date();
        final Date lockedUntil = file.getLockedUntil();

        if ((null != lockedUntil) && (lockedUntil.after(now))) {

            // determine special lock properties only, if lock is still active
            result.setLockedByUserId(file.getModifiedBy());

            if (null != userService) {
                try {
                    final User user = userService.getUser(file.getModifiedBy(), userService.getContext(session.getContextId()));
                    result.setLockedByUser(user.getDisplayName());
                } catch (OXException e) {
                    // set user name to empty for unknown user name
                    result.setLockedByUser(StreamInfo.NO_USERNAME);
                    LOG.warn("Couldn't retrieve user name who locked the file", e);
                }
            }
        }

        return result;
    }

    /**
     * Retrieves the document stream and the meta data specified by the fileId,
     * folderId and version.
     *
     * @param services
     *            The service lookup to be used by the method to create necessary
     *            services.
     *
     * @param session
     *            The session of the user who requested to retrieve the document
     *            stream.
     *
     * @param folderId
     *            The folder id where the file resides.
     *
     * @param fileId
     *            The file id of the file to read the document stream from.
     *
     * @param version
     *            The version of the file to be used to read the document stream from.
     *            If the version is null the method uses the current version.
     *
     * @param writeProtected
     *            Provides the write protected state of the file from the perspective
     *            of the user.
     *
     * @param lockedUntil
     *            Provides how long the file has exclusively been locked.
     *
     * @param lockedByUser
     *            Provides who has locked the file exclusively.
     *
     * @param documentFormat
     *            Provides the document format of the file.
     *
     * @param documentSize
     *            Provides the size of the file.
     *
     * @return
     *         A StreamInfo object which contains the stream and meta data of the requested
     *         file. ATTENTION: The stream and/or meta data member of the StreamInfo object can be null
     *         and must be checked by the caller, including the errorCode member which provides
     *         more information about the cause of any problem.
     */
    public StreamInfo getDocumentStream(Session session, String folderId, String fileId, String version, final String encryptionInfo) {
        final StreamInfo streamInfo = new StreamInfo();
        final long time1 = System.currentTimeMillis();
        boolean decrypted = false;

        IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session) && !Strings.isEmpty(fileId) && !Strings.isEmpty(folderId)) ? fileFactory.createAccess(session) : null;
        final IDBasedFileAccess fileAccess4MetaData = fileAccess;

        if ((null != fileAccess) && (null != encryptionInfo) && (encryptionInfo.length() > 0)) {
            try {

                if ((encryptionAwareFileAccessFactory != null) && (encryptionAuthenticationFactory != null)) {
                    EnumSet<CryptographyMode> cryptMode = EnumSet.of(CryptographyMode.DECRYPT);
                    // String authentication = encryptionAuthenticationFactory.createAuthenticationFrom(request.optHttpServletRequest());
                    String authentication = encryptionInfo;
                    fileAccess = encryptionAwareFileAccessFactory.createAccess(fileAccess, cryptMode, session, authentication);
                    decrypted = true;
                } else {
                    // file is encrypted, but we can't decrypt...
                    fileAccess = null;
                }
            } catch (final OXException e) {
                LOG.error("Exception caught while trying to create encryption aware file access", e);
                fileAccess = null;
            }

        }

        if (null != fileAccess) {
            try {
                final String accessId = fileId;
                Document docData = null;
                InputStream inputStream = null;
                File metaData = null;

                final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);

                // first check the support for the efficient data/metadata retrieval
                if (storageHelper.supportsEfficientRetrieval()) {
                    docData = fileAccess.getDocumentAndMetadata(accessId, version);
                    if (null != docData) {
                        inputStream = docData.getData();
                        if (fileAccess4MetaData == fileAccess) { // otherwise get it via fileAccess4MetaData later, so we get the correct name and size for encrypted files
                            metaData = docData.getFile();
                        }
                    }
                }

                // Fallback, if that doesn't work
                if (null == inputStream) {
                    inputStream = fileAccess.getDocument(accessId, version);
                }

                // set the initial properties
                streamInfo.setFileAccess(fileAccess);
                streamInfo.setDocumentStream(inputStream);

                if (null != streamInfo.getDocumentStream()) {

                    // possible fallback, if we didn't receive the meta data
                    if (null == metaData) {
                        try {
                            metaData = fileAccess4MetaData.getFileMetadata(accessId, version);
                        } catch (OXException e) {
                            LOG.warn("Couldn't retrieve meta data of the file", e);
                        }
                    }

                    if (null != metaData) {
                        MDCHelper.safeMDCPut(MDCEntries.FILENAME, metaData.getFileName());
                        // determine write protected state using the write permissions
                        final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

                        streamInfo.setWriteProtected(!fileHelperService.canWriteToFile(metaData, session.getUserId()));
                        streamInfo.setMetaData(metaData);
                        streamInfo.setDocumentFormat(DocFileHelper.getDocumentFormat(metaData.getFileName()));

                        // determine file locking state
                        final LockingInfo lockInfo = getLockingInfo(session, metaData);
                        streamInfo.setLockedByUser(lockInfo.getLockedByUser());
                        streamInfo.setLockedByUserId(lockInfo.getLockedByUserId());

                        // set flag if document stream was obtained from an encrypted document
                        streamInfo.setDecrypted(decrypted);
                    } else {
                        LOG.warn("Couldn't retrieve meta data of the file");
                    }
                }
            } catch (OXException e) {
                LOG.error("Couldn't read document stream", e);
                streamInfo.setDocumentStream(null);
                streamInfo.setFileAccess(null);
                streamInfo.setDecrypted(false);
                streamInfo.setErrorCode(ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, true));
            } catch (Exception e) {
                LOG.error("Couldn't read document stream", e);
                streamInfo.setDocumentStream(null);
                streamInfo.setFileAccess(null);
                streamInfo.setDecrypted(false);
                // set general error as we cannot extract the exact cause
                streamInfo.setErrorCode(ErrorCode.LOADDOCUMENT_FAILED_ERROR);
            } finally {
                closeFileAccess(fileAccess);
                MDC.remove(MDCEntries.FILENAME);
           }
        }

        LOG.trace("RT connection: TIME to get document stream = " + (System.currentTimeMillis() - time1) + "ms");

        return streamInfo;
    }

    /**
     * Finishes the access to the IDBasedFileAccess instance.
     *
     * @param fileAccess a IDBasedFileAccess instance or null
     */
    private static void closeFileAccess(IDBasedFileAccess fileAccess) {
        try {
            if (null != fileAccess) {
                fileAccess.finish();
            }
        } catch (OXException e) {
            LOG.warn("Couldn't finish the file access", e);
        }
    }

    /**
     * Writes the provided document stream to the file associated with this
     * FileHelper instance.
     *
     * @param services
     *            The service lookup to be used by the method to create necessary
     *            services.
     *
     * @param session
     *            The session of the user who wants to write the document stream to the
     *            file.
     *
     * @param documentStream
     *            The document stream that should be stored into the file.
     *
     * @param fileId
     *            The file id of the file to read the document stream from.
     *
     * @param folderId
     *            The folder id where the file resides.
     *
     * @param metaData
     *            Optional meta data of the file to be written. Will be used to optimize the
     *            access to the storage, if present.
     *
     * @param storageHelper
     *            An instance which provides storage specific properties in the context of the
     *            session and folder used by this call.
     *
     * @param userExtension
     *            Specifies the extension to be used for the file.
     *
     * @param setUserExtension
     *            Specifies if the extension of the file should be set or
     *            removed. If TRUE the user extension will be set otherwise the user
     *            extension will be removed.
     *
     * @param revisionless
     *            Specifies if the stream should be written to the file using a new
     *            version or overwrite the latest version. TRUE means overwrite
     *            the latest version with the provided document stream. FALSE to
     *            create a new version.
     *
     * @return
     *         A WriteInfo instance which contains the information about the write document
     *         stream process. Information like error code, version and other properties
     *         can be retrieved.
     */
    public WriteInfo writeDocumentStream(final Session session, final InputStream documentStream, String fileId, String folderId, final File metaData, final StorageHelperService storageHelper, String userExtension, boolean setUserExtension, boolean revisionless, boolean encrypt) {
        final WriteInfo writeInfo = new WriteInfo(null, ErrorCode.NO_ERROR);
        IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final long time1 = System.currentTimeMillis();

        if ((null != fileAccess) && encrypt) {
            try {
                if ((encryptionAwareFileAccessFactory != null) && (encryptionAuthenticationFactory != null)) {
                    EnumSet<CryptographyMode> cryptMode = EnumSet.of(CryptographyMode.ENCRYPT);
                    fileAccess = encryptionAwareFileAccessFactory.createAccess(fileAccess, cryptMode, session);
                } else {
                    // file is encrypted, but we can't decrypt...
                    fileAccess = null;
                }
            } catch (final OXException e) {
                fileAccess = null;
            }
        }

        if ((null != documentStream) && (null != fileAccess) && !Strings.isEmpty(fileId)) {
            final String accessId = fileId;
            final String version = FileStorageFileAccess.CURRENT_VERSION;
            boolean rollback = false;

            try {
                final File docMetaData = (null != metaData) ? metaData : fileAccess.getFileMetadata(accessId, version);
                final DefaultFile newMetadata = new DefaultFile();
                final ArrayList<com.openexchange.file.storage.File.Field> modifiedColumns = new ArrayList<>();

                // make sure that the important parts of the meta data are filled
                newMetadata.setId(fileId);
                newMetadata.setFolderId(folderId);
                newMetadata.setVersionComment(DocumentConstants.OX_DOCUMENTS_CMNT);

                // check for possible custom meta data - advisory lock info
                final Map<String, Object> customMetaData = metaData.getMeta();
                if (customMetaData != null) {
                    newMetadata.setMeta(customMetaData);
                    modifiedColumns.add(com.openexchange.file.storage.File.Field.META);
                }

                // ensure that the filename has the given user extension, if set as call parameter
                if (null != userExtension) {
                    final String fileName = docMetaData.getFileName();
                    final boolean hasUserExtension = fileName.endsWith(userExtension);

                    if (!hasUserExtension && setUserExtension) {
                        // add user extension
                        newMetadata.setFileName(fileName + userExtension);
                        modifiedColumns.add(com.openexchange.file.storage.File.Field.FILENAME);
                    } else if (hasUserExtension && !setUserExtension) {
                        // remove user extension
                        newMetadata.setFileName(fileName.substring(0, fileName.length() - userExtension.length()));
                        modifiedColumns.add(com.openexchange.file.storage.File.Field.FILENAME);
                    }
                }

                fileAccess.startTransaction();
                rollback = true;
                LOG.trace("RT connection: [writeDocumentStream] saveDocument using meta data: " + newMetadata.toString());
                if (revisionless && storageHelper.supportsIgnorableVersion()) {
                    // use revision-less save, if the underlying storage supports it
                    fileAccess.saveDocument(newMetadata, documentStream, FileStorageFileAccess.DISTANT_FUTURE, modifiedColumns, true);
                } else {
                    // fall-back for storages where non-revision save is not supported
                    fileAccess.saveDocument(newMetadata, documentStream, FileStorageFileAccess.DISTANT_FUTURE, modifiedColumns);
                }
                fileAccess.commit();
                rollback = false;

                writeInfo.version = newMetadata.getVersion();
            } catch (final OXException e) {
                LOG.error("Couldn't write document stream, Exception caught: ", e);
                writeInfo.errorCode = ExceptionToErrorCode.map(e, ErrorCode.SAVEDOCUMENT_FAILED_ERROR, true);
            } catch (final Exception e) {
                LOG.error("Couldn't write document stream, Exception caught: ", e);
                writeInfo.errorCode = ErrorCode.SAVEDOCUMENT_FAILED_ERROR;
            } finally {
                // Roll-back (if needed) and finish
                if (rollback) {
                    TransactionAwares.rollbackSafe(fileAccess);
                }
                TransactionAwares.finishSafe(fileAccess);
            }
        }
        LOG.trace("RT connection: TIME to write document stream = " + (System.currentTimeMillis() - time1) + "ms");

        return writeInfo;
    }

    /**
     * Renames the document file in the info store using the provided
     * new file name. ATTENTION: This method doesn't check for the
     * storage capabilities. It just
     *
     * @param session
     *            The session of the user who requested to rename the document file.
     *
     * @param newFileName
     *            The new file name of the document file.
     *
     * @return
     *         The new file name of the document file. Can be null if the rename
     *         operation failed.
     */
    public RenameResult renameDocument(Session session, String newFileName, final String folderId, final String fileId) {
        RenameResult renameResult = new RenameResult();
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final String currentVersion = FileStorageFileAccess.CURRENT_VERSION;
        final long time1 = System.currentTimeMillis();

        ErrorCode result = ErrorCode.NO_ERROR;

        if ((null != fileAccess) && (!Strings.isEmpty(newFileName))) {
            try {
                final ArrayList<Field> modifiedColumns = new ArrayList<>();
                final DefaultFile metadata = new DefaultFile();

                metadata.setId(fileId);
                metadata.setFolderId(folderId);
                metadata.setFileName(newFileName);
                metadata.setVersion(currentVersion);

                modifiedColumns.add(Field.FILENAME);
                renameResult.setNewFileId(fileAccess.saveFileMetadata(metadata, FileStorageFileAccess.DISTANT_FUTURE, modifiedColumns));
                LOG.debug("RT connection: Renamed document file-id: " + renameResult.newFileId());
                fileAccess.commit();

                // retrieve the real filename from the metadata, it may have
                // been changed from the requested one by the file service;
                // add a short sleep in order to avoid asynchronous errors while
                // getting the updated metadata, which may currently happen with
                // cascaded DB backends
                // (see also https://bugs.open-xchange.com/show_bug.cgi?id=26080)
                try {
                    renameResult.setMetaData(fileAccess.getFileMetadata(renameResult.newFileId(), currentVersion));
                } catch (OXException e) {
                    LOG.debug("RT connection: Access meta data of renamed file failed!", e);
                    result = ErrorCode.RENAMEDOCUMENT_FAILED_ERROR;
                }
            } catch (Exception e) {
                result = ErrorCode.RENAMEDOCUMENT_FAILED_ERROR;
                if (e instanceof OXException) {
                    // map possible exceptions to our error codes
                    result = ExceptionToErrorCode.map((OXException) e, ErrorCode.RENAMEDOCUMENT_FAILED_ERROR, false);
                }
                LOG.error(result.getDescription(), e);
            }
        } else {
            // map this problem to validation error
            result = ErrorCode.RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR;
        }

        renameResult.setErrorCode(result);
        LOG.trace("RT connection: TIME to rename document file = " + (System.currentTimeMillis() - time1) + "ms");

        return renameResult;
    }

    /**
     * Sets the version to a specified value of a file
     *
     * @param session The session of the user who requested to rename the document file.
     * @param folderId The folder id where the file resides.
     * @param fileId The file id of the file to change the version.
     * @param setVersion The new version.
     * @return the set version
     * @throws OXException if something went wrong
     */
    @Override
    public String setVersionOfFile(final Session session, final String folderId, final String fileId, final String setVersion) throws OXException {
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;

        String version = null;

        // set active version to restored document version
        final DefaultFile metadata = new DefaultFile();

        metadata.setId(fileId);
        metadata.setFolderId(folderId);
        metadata.setVersion(setVersion);

        // and save the meta data
        fileAccess.saveFileMetadata(metadata, FileStorageFileAccess.DISTANT_FUTURE, Arrays.asList(UPDATE_METADATA_FIELDS));
        fileAccess.commit();
        version = setVersion;

        return version;
    }

    /**
     * Provides the latest version string of the file
     *
     * @param session
     *            The session to be used to access the file store.
     *
     * @return
     *         The version string or null.
     *
     * @throws OXEception
     *             Throws an exception if the meta data of the document file
     *             couldn't be retrieved.
     */
    public String getVersionFromFile(final Session session, final String fileId) throws OXException {
        return (String) getFieldValueFromFile(session, fileId, Field.VERSION);
    }

    /**
     * Provides the file name of the file
     *
     * @param session
     *            The session to be used to access the file store.
     *
     * @param fileId
     *            The id of the file which name should be retrieved.
     *
     * @return
     *         The file name or null.
     *
     * @throws OXEception
     *             Throws an exception if the meta data of the document file
     *             couldn't be retrieved.
     */
    public String getFilenameFromFile(final Session session, final String fileId) throws OXException {
        return (String) getFieldValueFromFile(session, fileId, Field.FILENAME);
    }

    /**
     * Retrieves the file meta data using the provided session.
     *
     * @param session
     *            The session to be used as the context to retrieve the file meta data.
     *
     * @param fileId
     *            The id of the file which name should be retrieved.
     *
     * @param version
     *            A version of the file where the meta data should be retrieved from.
     *            Can be null where the method uses the latest version.
     *
     * @return
     *         The file meta data or null if necessary services are not available.
     *
     * @throws OXException
     *             Throws an exception if the meta data of the document file couldn't
     *             be retrieved.
     */
    public File getMetaDataFromFile(Session session, String fileId, String version) throws OXException {
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final String fileVersion = (null == version) ? FileStorageFileAccess.CURRENT_VERSION : version;
        final long time1 = System.currentTimeMillis();
        File metaData = null;

        if (null != fileAccess) {
            final String accessId = fileId;
            metaData = fileAccess.getFileMetadata(accessId, fileVersion);

            initDocFormatAndType(metaData);
        }
        LOG.trace("RT connection: TIME to get meta data from file = " + (System.currentTimeMillis() - time1) + "ms");

        return metaData;
    }

    /**
     * Provides the specified field value of the file
     *
     * @param session
     *            The session to be used to access the file store.
     *
     * @return
     *         The field value or null.
     *
     * @throws OXEception
     *             Throws an exception if the meta data of the document file
     *             couldn't be retrieved.
     */
    protected Object getFieldValueFromFile(Session session, final String fileId, Field field) throws OXException {

        Object result = null;

        try {
            final String currentVersion = FileStorageFileAccess.CURRENT_VERSION;
            final File metaData = getMetaDataFromFile(session, fileId, currentVersion);

            if (null != metaData) {
                switch (field) {
                    case FILENAME: {
                        result = metaData.getFileName();
                        break;
                    }

                    case FILE_SIZE: {
                        result = Long.valueOf(metaData.getFileSize());
                        break;
                    }

                    case FILE_MIMETYPE: {
                        result = metaData.getFileMIMEType();
                        break;
                    }

                    case VERSION: {
                        result = metaData.getVersion();
                        break;
                    }

                    default: {
                        break;
                    }
                }
            }
        } catch (OXException e) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("RT connection: Couldn't retrieve field value from document meta data", e);
            }
            throw e;
        }

        return result;
    }

    private void initDocFormatAndType(File metaData) {
        if ((metaData != null) && docFormatAndTypeSet.compareAndSet(false, true)) {
            documentFormat = DocFileHelper.getDocumentFormat(metaData.getFileName());
            documentType = DocFileHelper.getDocumentTypeFromDocumentFormat(documentFormat);
        }
    }

    private void initDocFormatAndType(StreamInfo streamInfo) {
        if ((streamInfo != null) && docFormatAndTypeSet.compareAndSet(false, true)) {
            documentFormat = streamInfo.getDocumentFormat();
            documentType = DocFileHelper.getDocumentTypeFromDocumentFormat(documentFormat);
        }
    }

}
