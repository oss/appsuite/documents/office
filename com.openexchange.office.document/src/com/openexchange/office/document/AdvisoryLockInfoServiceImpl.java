/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.document.api.AdvisoryLockInfo;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.document.api.AdvisoryLockMode;
import com.openexchange.office.rt2.fields.RealtimeIDAccess;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tx.TransactionAwares;

@Service
@RegisteredService(registeredClass=AdvisoryLockInfoService.class)
public class AdvisoryLockInfoServiceImpl implements AdvisoryLockInfoService {

    private static final Logger LOG = LoggerFactory.getLogger(AdvisoryLockInfoServiceImpl.class);
    private static final String KEY_ADVISORY_LOCK = "com.openexchange.office.advisoryLock";
    private static final Field UPDATE_METADATA_META_FIELD[] = { Field.META, Field.LAST_MODIFIED };

    @Autowired
    private IDBasedFileAccessFactory fileFactory;

    @Autowired
    private StorageHelperServiceFactory storageHelperServiceFactory;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    @Autowired
    protected ConfigurationService configurationService;

    @Autowired
    private RealtimeIDAccess realtimeIDAccess;

    /**
     * Checks and possibly updates an advisory lock for a document file with current host name and user information.
     *
     * @param session the session of the client that wants to check a possible advisory lock
     * @param name the name of the system used by the client to access the document file.
     * @param folderId the folder id of the document file that should be checked.
     * @param fileId the file id of the document file that should be checked.
     * @param fistUser specifies if the check is done in the context of the first user or not
     * @throws Exception
     */
    @Override
    public AdvisoryLockInfo checkAndUpdateAdvisoryLock(Session session, String name, String folderId, String fileId) throws Exception {
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final String fileVersion = FileStorageFileAccess.CURRENT_VERSION;
        AdvisoryLockInfo result = new AdvisoryLockInfo(ErrorCode.NO_ERROR);

        if (null != fileAccess) {
            final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);
            if (!storageHelper.supportsExtendedMetaData())
                return result;

            // determine file rights before continuing with advisory lock info
            final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
            final int userId = session.getUserId();
            if (!fileHelperService.canReadFile(folderId, fileId, userId) || !fileHelperService.canWriteToFile(fileId))
                return result;

            try {
                final File metaData = fileAccess.getFileMetadata(fileId, fileVersion);
                final Map<String, Object> customMeta = metaData.getMeta();

                boolean reset = true;
                final Object advLockStored = (null != customMeta) ? customMeta.get(AdvisoryLockInfo.ADVISORYLOCK_KEY) : null; 
                if ((advLockStored != null) && (advLockStored instanceof String)) {
                    final String advLockStoredString = (String) advLockStored;
                    try {
                        reset = false;

                        final JSONObject advLockInfoData = new JSONObject(advLockStoredString);
                        result = new AdvisoryLockInfo(advLockInfoData);
                        if (!result.getName().equals(name)) {
                            // DOCS-3990 detect file copy and ignore advisory lock in this case
                            if (isSameFile(fileAccess, metaData, result)) {
                                final ErrorCode errorCode = ErrorCode.ADVISORY_LOCK_SET_WARNING;
                                errorCode.setValue(advLockInfoData.toString());
                                result.setErrorCode(errorCode);
                                LOG.debug("advisory lock {} found for file-id {} where host {} tries to open it - set ADVISORY_LOCK_SET_WARNING", advLockStoredString, fileId, name);
                            } else {
                                // ignore advisory lock - real-time id not identical - must have been created by copying
                                LOG.debug("advisory lock {} found for file-id {] that was created by copying. ignore advisory lock", advLockStoredString, fileId);
                                reset = true;
                            }
                        } else if (StringUtils.isEmpty(advLockStoredString)) {
                            reset = true;
                        }
                    } catch (JSONException e) {
                        LOG.info("JSONException caught trying to retrieve advisory lock info from meta data of file-id " + fileId, e);
                        result = new AdvisoryLockInfo(ErrorCode.ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR);
                    }
                } else if (advLockStored != null) {
                    LOG.warn("AdvisoryLock {} found with invalid data type for file-id {}!", advLockStored.toString(), fileId);
                }

                if (reset) {
                    final String advisoryLockData = createAdvisoryLockData(session, name, fileId);
                    final Map<String, Object> newMeta = (customMeta == null) ? new HashMap<>() : customMeta;
                    newMeta.put(AdvisoryLockInfo.ADVISORYLOCK_KEY, advisoryLockData);

                    writeNewCustomMetaData(fileAccess, folderId, fileId, metaData.getLastModified(), newMeta);
                    LOG.debug("update advisory lock for folder-id {}, file-id {} and AdvisoryLock data {}", folderId, fileId, advisoryLockData);

                    result = new AdvisoryLockInfo(new JSONObject(advisoryLockData));
                    result.setAdvisoryLockWritten(true);
                }
            } catch (OXException e) {
                LOG.warn("Exception caught trying to retrieve/update meta data from/for file-id {} for advisory lock", fileId, e);
                final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
                result = new AdvisoryLockInfo(errorCode);
            } catch (JSONException je) {
                LOG.warn("JSONException caught trying to retrieve/update meta data from/for file-id {} for advisory lock", fileId, je);
                result = new AdvisoryLockInfo(ErrorCode.ADVISORY_LOCK_COULDNT_PROCESS_ERROR);
            }
        } else {
            LOG.info("Couldn't retrieve IDBasedFileAccess for session {}", session);
        }

        return result;
    }

    /**
     * Determines if the original stored file id in the AdvisoryLockInfo
     * references the same file as the provided fileId using the extracted
     * realtime ID.
     *
     * @param advLockInfo the AdvisoryLockInfo created by the custom meta data
     * @param fileId the file id of the document file to be opened
     * @return <code>true</code> in case the file id provided in the AdvisoryLockInfo
     * references the same file as the provided file id using the extracted realtime id.
     * In case the AdvisoryLockInfo doesn't contain a original file id the method returns
     * <code>true</code> to be compatible with old implementations.
     */
    private boolean isSameFile(IDBasedFileAccess fileAccess, File metaFileToOpen, AdvisoryLockInfo advLockInfo) throws OXException {
        boolean result = true;
        String origFileId = advLockInfo.getOrigFileId();
        if (StringUtils.isNotEmpty(origFileId)) {
            final File advStoredFile = fileAccess.getFileMetadata(origFileId, FileStorageFileAccess.CURRENT_VERSION);
            String storedFileUid = realtimeIDAccess.getRealtimeID(advStoredFile);
            String openedFileUid = realtimeIDAccess.getRealtimeID(metaFileToOpen);
            if (StringUtils.isNotEmpty(storedFileUid) && StringUtils.isNotEmpty(openedFileUid)) {
                result = openedFileUid.equals(storedFileUid);
            }
        }
        return result;
    }

    /**
     * Resets an advisory lock for a document file with current name and user information.
     *
     * @param session the session of the client that wants to reset the advisory lock
     * @param name the name of the system used by the client to access the document file.
     * @param folderId the folder id of the document file that should reset the advisory lock information.
     * @param fileId the file id of the document file which that should reset the advisory lock information.
     * @return TRUE if the advisory lock was reset or FALSE if not.
     * @throws Exception
     */
    @Override
    public boolean resetAdvisoryLock(Session session, String name, String folderId, String fileId) throws Exception {
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final String fileVersion = FileStorageFileAccess.CURRENT_VERSION;

        if (null != fileAccess) {
            final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);
            if (!storageHelper.supportsExtendedMetaData())
                return false;

            // check write access before trying to reset advisory lock
            final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
            if (!fileHelperService.canWriteToFile(fileId))
                return false;

            try {
                final File metaData = fileAccess.getFileMetadata(fileId, fileVersion);
                final Map<String, Object> customMeta = metaData.getMeta();
                final Map<String, Object> newMeta = (customMeta == null) ? new HashMap<>() : customMeta;

                final String advisoryLockData = createAdvisoryLockData(session, name, fileId);
                newMeta.put(AdvisoryLockInfo.ADVISORYLOCK_KEY, advisoryLockData);
                writeNewCustomMetaData(fileAccess, folderId, fileId, metaData.getLastModified(), customMeta);

                LOG.debug("reset AdvisoryLock for folder-id {}, file-id {} with AdvisoryLock data {}", folderId, fileId, advisoryLockData);
                return true;
            } catch (OXException e) {
                LOG.warn("Exception caught trying to reset advisory lock meta data for file-id " + fileId, e);
            }
        } else {
            LOG.info("Coudln't retrieve IDBasedFileAccess for session {}", session);
        }

        return false;
    }

    /**
     * Clears an advisory lock previously stored for a document file.
     *
     * @param session the session of the client that wants to clear the advisory lock
     * @param folderId the folder id of the document file which should have a cleared advisory lock
     * @param fileId the file id of the document file which should have a cleared advisory lock
     * @throws Exception
     */
    @Override
    public void clearAdvisoryLock(Session session, String folderId, String fileId) throws Exception {
        final IDBasedFileAccess fileAccess = ((null != fileFactory) && (null != session)) ? fileFactory.createAccess(session) : null;
        final String fileVersion = FileStorageFileAccess.CURRENT_VERSION;

        if (null != fileAccess) {
            final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);
            if (!storageHelper.supportsExtendedMetaData())
                return;

            // check write access before trying to clear advisory lock
            final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
            if (!fileHelperService.canWriteToFile(fileId))
                return;

            try {
                final File metaData = fileAccess.getFileMetadata(fileId, fileVersion);
                final Map<String, Object> customMeta = metaData.getMeta();

                if ((null != customMeta) && (customMeta.remove(AdvisoryLockInfo.ADVISORYLOCK_KEY) != null)) {
                    writeNewCustomMetaData(fileAccess, folderId, fileId, metaData.getLastModified(), customMeta);
                    LOG.debug("clear advisory lock for folder-id {}, file-id {}", folderId, fileId); 
                }
            } catch (OXException e) {
                LOG.warn("Exception caught trying to reset advisory lock meta data for file-id " + fileId, e);
            }
        } else {
            LOG.info("Coudln't retrieve IDBasedFileAccess for session {}", session);
        }
    }

    /**
     * Determines if the advisory lock feature is enabled or not.
     *
     * @return TRUE if the advisory lock feature is enabled otherwise FALSE.
     */
    @Override
    public boolean isAdvisoryLockEnabled() {
        final String value = configurationService.getProperty(KEY_ADVISORY_LOCK, "");
        return (AdvisoryLockMode.getAdvisoryLockModeFromString(value) != AdvisoryLockMode.ADVISORY_LOCK_MODE_NOT_ENABLED);
    }

    /**
     * Provides the current advisory lock mode.
     *
     * @return the advisory lock mode. If the advisory lock feature is not enabled
     * ADVISORY_LOCK_MODE_NOT_ENABLED will be returned.
     */
    @Override
    public AdvisoryLockMode getAdvisoryLockMode() {
        final String value = configurationService.getProperty(KEY_ADVISORY_LOCK, "");
        return AdvisoryLockMode.getAdvisoryLockModeFromString(value);
    }

    /**
     * Provides the custom name of the system to be used for the advisory lock info.
     * This value is only valid if the advisory lock mode is set to
     * ADVISORY_LOCK_MODE_CUSTOM. In all other cases this value will be an empty string.
     *
     * @return the name of the system to be used for the advisory lock in case the
     * advisory lock mode is set to ADVISORY_LOCK_MODE_CUSTOM.
     */
    @Override
    public String getAdvisoryLockSystemName() {
        final String value = configurationService.getProperty(KEY_ADVISORY_LOCK, "");
        return value;
    }

    /**
     * Creates the advisory lock data string for the provided session and name.
     *
     * @param session the session of a client
     * @param name the name of the system used to access the document file
     * @return a string in json-format containing the advisory lock data
     * @throws JSONException throws JSONException if the json object could not be created
     */
    private String createAdvisoryLockData(Session session, String name, String fileId) throws JSONException {
        int userId = 0;
        int contextId = 0;

        try {
            userId = session.getUserId();
            contextId = session.getContextId();
        } catch (Exception e) {
            LOG.error("Exception caught trying to retrieve user data from session", e);
        }

        final Instant now = Instant.now();
        return new AdvisoryLockInfo(name, contextId, userId, now.toString(), fileId).asJSONObject().toString();
    }

    /**
     * Writes new custom meta data to file using the provided file access. 
     *
     * @param fileAccess the file access of the client that wants to write the new custom meta data
     * @param folderId the folder id of the file to update
     * @param fileId the file id of the file to update
     * @param customMeta the custom meta data to be written to the file
     * @throws Exception if a low-level error happened
     */
    private void writeNewCustomMetaData(IDBasedFileAccess fileAccess, String folderId, String fileId, Date lastModified, Map<String, Object> customMeta) throws Exception {
        final DefaultFile newMetaData = new DefaultFile();
        newMetaData.setMeta(customMeta);
        newMetaData.setId(fileId);
        newMetaData.setFolderId(folderId);
        newMetaData.setLastModified(lastModified);

        fileAccess.startTransaction();
        boolean rollback = true;
        try {
            fileAccess.saveFileMetadata(newMetaData, FileStorageFileAccess.DISTANT_FUTURE, Arrays.asList(UPDATE_METADATA_META_FIELD));
            fileAccess.commit();
            rollback = false;
        } catch (OXException e) {
            LOG.warn("OXException caught trying to save file custom meta data for file-id " + fileId, e);
            throw e;
        } finally {
            if (rollback) {
                TransactionAwares.rollbackSafe(fileAccess);
            }
            TransactionAwares.finishSafe(fileAccess);
        }
    }

}
