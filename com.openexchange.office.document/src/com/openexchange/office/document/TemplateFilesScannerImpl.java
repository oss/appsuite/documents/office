/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.groupware.results.TimedResult;
import com.openexchange.office.document.api.TemplateFilesScanner;
import com.openexchange.office.document.api.TemplateFilesScannerFactory.TemplateFilterType;
import com.openexchange.office.document.tools.LocalFileMappingManager;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.session.FakeSession;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.ExtensionHelper;
import com.openexchange.office.tools.doc.MimeTypeHelper;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.filename.FileNameTools;
import com.openexchange.tools.iterator.SearchIterator;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TemplateFilesScannerImpl implements TemplateFilesScanner {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateFilesScannerImpl.class);

    private static final boolean DISABLE_BINARY_MS_FORMATS = true;

    @Autowired
    private LocalFileMappingManager m_localFileMapper;

    @Autowired
    private IDBasedFileAccessFactory fileFactory;

    @Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;

    @Autowired
    private UserConfigurationFactory userConfigFactory;

    @Autowired
    private ConfigurationHelper configurationHelper;

    private ExtensionFilter m_extensionFilter = null;
    private TemplateFilterType m_filterType = TemplateFilterType.ONLY_TEMPLATES;

    /**
     * A filter class to filter out language specific folders in the global
     * template folder.
     *
     * {@link LanguageFolderFilter}
     *
     * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
     */
    private static class LanguageFolderFilter implements FilenameFilter {
        private String m_specificLangFolder = null;
        private String m_generalLangFolder = null;

        public LanguageFolderFilter(String specificLangFolder, String generalLangFolder) {
            m_specificLangFolder = specificLangFolder;
            if (!specificLangFolder.equalsIgnoreCase(generalLangFolder)) {
                m_generalLangFolder = generalLangFolder;
            }
        }

        /**
         * Filer method to filter out all folders except the language specific
         * folders.
         *
         * @param file
         *  The file instance to be checked.
         *
         * @param fileName
         *  The file name of the file instance to be checked.
         *
         * @return
         *  TRUE if the file should not be removed from the result set,
         *  otherwise FALSE.
         */
        @Override
        public boolean accept(java.io.File file, String fileName) {
            return (file.isDirectory() &&
                    (fileName.equalsIgnoreCase(m_specificLangFolder)) ||
                    (fileName.equalsIgnoreCase(m_generalLangFolder)));
        }
    }

    /**
     * A filter class to filter out non-templates file according to the file
     * extension.
     *
     * {@link ExtensionFilter}
     *
     * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
     */
    private static class ExtensionFilter implements FilenameFilter {
        private final HashMap<String, Boolean> m_acceptedExtensions = new HashMap<>();

        public ExtensionFilter(String[] acceptedExtensions, String[]...more) {
            if (null != acceptedExtensions) {
                for (String ext : acceptedExtensions) {
                    m_acceptedExtensions.put(ext, true);
                }
                for (String[] strArray : more) {
                    for (String ext : strArray) {
                        m_acceptedExtensions.put(ext, true);
                    }
                }
            }
        }

        @Override
        public boolean accept(java.io.File file, String fileName) {
            String extension = FileHelper.getExtension(fileName);

            return (file.isFile() && m_acceptedExtensions.containsKey(extension));
        }
    }

    public TemplateFilesScannerImpl(TemplateFilterType filterType) {
        m_filterType = filterType;

        if (filterType == TemplateFilterType.ONLY_TEMPLATES) {
            // select only supported document template extensions
            m_extensionFilter = new ExtensionFilter(
                ExtensionHelper.EXCEL_TEMPLATE_EXTENSIONS,
                ExtensionHelper.WORD_TEMPLATE_EXTENSIONS,
                ExtensionHelper.POWERPOINT_TEMPLATE_EXTENSIONS,
                ExtensionHelper.ODF_CALC_TEMPLATE_EXTENSIONS,
                ExtensionHelper.ODF_IMPRESS_TEMPLATE_EXTENSIONS,
                ExtensionHelper.ODF_WRITER_TEMPLATE_EXTENSIONS);
        } else {
            // use all supported document extensions
            m_extensionFilter = new ExtensionFilter(
                ExtensionHelper.SPREADSHEET_EXTENSIONS,
                ExtensionHelper.TEXT_EXTENSIONS,
                ExtensionHelper.PRESENTATION_EXTENSIONS,
                ExtensionHelper.ODF_CALC_TEMPLATE_EXTENSIONS,
                ExtensionHelper.ODF_IMPRESS_TEMPLATE_EXTENSIONS,
                ExtensionHelper.ODF_WRITER_TEMPLATE_EXTENSIONS);
        }
    }

    /**
     *
     * @return
     * @throws OXException
     */
    @Override
    public ErrorCode searchForTemplates(final ServerSession session, String type, final JSONArray templates) throws OXException {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        if ((null != session) && (null != type) && (type.length() > 0)) {
            DocumentType searchFilter = DocumentType.NONE;

            // We want to measure the time needed to find the templates
            // including to check for folders that were deleted (moved to
            // the trash folder).
            long time1 = System.currentTimeMillis();
            // Map type to DocumenntType to restrict search filter - other values means all
            if (ApplicationType.APP_TEXT_STRING.equalsIgnoreCase(type)) {
                searchFilter = DocumentType.TEXT;
            } else if (ApplicationType.APP_SPREADSHEET_STRING.equalsIgnoreCase(type)) {
                searchFilter = DocumentType.SPREADSHEET;
            } else if (ApplicationType.APP_PRESENTATION_STRING.equalsIgnoreCase(type)) {
                searchFilter = DocumentType.PRESENTATION;
            }

            final IDBasedFileAccess fileAccess = fileFactory.createAccess(session);
        	final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
            final User user = session.getUser();
            final String fallbackTemplatePath = "/opt/open-xchange/templates/documents";
            String userLangCode = "en"; // fallback if no language is set

            if (null != user) {
                userLangCode = UserHelper.mapUserLanguageToLangCode(user.getPreferredLanguage());
            }

            final Map<String, Boolean> templateFolders = new LinkedHashMap<>();
            final Map<String, Boolean> templateFoldersFromContextAdmin = new LinkedHashMap<>();

            // get User's template folder
            templateFolders.put(folderHelperService.getUserTemplatesFolderId(session), false);

            getUserData(session.getUserId(), session, "io.ox/office", "MyTemplateFolders", templateFolders, true);
            getUserData(session.getContext().getMailadmin(), session, "io.ox/office", "ContextTemplateFolders", templateFoldersFromContextAdmin, false);

            templateFolders.putAll(templateFoldersFromContextAdmin);

            if (null != templateFolders) {
                ErrorCode err = null;
                for (Entry<String, Boolean> folderDescriptor: templateFolders.entrySet()){

                    boolean ignoreNoneTemplates = folderDescriptor.getValue();
                    String folderId = folderDescriptor.getKey();

                    try{
	                    // prevent to check an invalid folder id
	                    if (StringUtils.isNotEmpty(folderId) && folderHelperService.exists(folderId)) {
		                    if (!folderHelperService.isFolderTrashOrInTrash(folderId)) {
		                        err = scanUserFolderForTemplates(fileAccess, templates, folderId, searchFilter, ignoreNoneTemplates);
		                        if (ErrorCode.NO_ERROR != err && ErrorCode.NO_ERROR == errorCode) {
		                            errorCode = err;
		                        }
		                    }
	                    }
                    } catch (OXException e) {
                        // moved try catch inside the loop, because context admin could set template folder from external (for ex.: Dropbox),
                        // where only he has access to
                        if (isErrorCode(e, FileStorageExceptionCodes.ACCOUNT_NOT_FOUND)) {
                            LOG.debug("Error user template paths cannot be checked to be inside the trash folder, probably user has no access to this folder in this context", e);
                        } else if (isErrorCode(e, FileStorageExceptionCodes.ADMIN_FILE_ACCESS_NOT_AVAILABLE)) {
                            LOG.debug("templatefolder: user has no read access for this globaltemplatefolder: " + folderId, e);
                        } else if (isErrorCode(e, FileStorageExceptionCodes.UNKNOWN_FILE_STORAGE_SERVICE)) {
                            LOG.debug("templatefolder: user can not read folder. file storage service is only open for user himself and cannot be used as globaltemplatefolder (" + folderId + ")", e);
                        } else if (isErrorCode(e, FolderExceptionErrorMessage.FOLDER_NOT_VISIBLE)) {
                            LOG.debug("templatefolder: Folder " + folderId + " not visible for user, he cannot use this folder for global templates", e);
                        } else {
                            LOG.warn("Error user template paths cannot be checked to be inside the trash folder", e);
                        }
                    }
                }
            }

            // next step: we try to scan the global template path and
            // collect the files there.
            final String globalPath = configurationHelper.getStringOfficeConfigurationValue(session, "//module/templatePath", fallbackTemplatePath);
            if ((null != globalPath) && (globalPath.length() > 0)) {
                // scan the global folder and collect templates according to the user language
                errorCode = scanGlobalTemplateFolder(globalPath, templates, searchFilter, userLangCode, m_extensionFilter);
            } else {
                LOG.warn("Error global template path is not set");
            }


            for (int i = 0; i < templates.length(); i++) {
                try {
	                JSONObject template = templates.getJSONObject(i);
	                String folderId = template.getString("folder_id");

	                if (StringUtils.equals(template.getString("source"),"global")) {
	                    template.put("editable", false);
	                } else {
	                    template.put("editable", folderHelperService.folderHasWriteAccess(folderId));
	                }
	                if (templateFoldersFromContextAdmin.containsKey(folderId)) {
	                    template.put("source", "admin");
	                }

	                template.put("com.openexchange.file.sanitizedFilename", FileNameTools.sanitizeFilename(template.getString("filename")));
                } catch (JSONException e) {
                	LOG.warn("JSONException caught trying to collect templates from global template path", e);
                }
            }

            // log time on info level
            long time2 = System.currentTimeMillis();
            LOG.debug("Scan template folders including delete folder detection: " + (time2 - time1) + "ms");
        }

        return errorCode;
    }

    /**
     *
     * with help of the UserConfigurationHelper, userdata is read.
     * whose userdata is decided by userId & contextId
     *
     * id & key defines the data which is put to target-set
     *
     * @param userId
     * @param contextId
     * @param id
     * @param key
     * @param target
     * @throws OXException
     */
    private void getUserData(int userId, ServerSession realSession, String id, String key, Map<String, Boolean> target, boolean fromUser) throws OXException{
        final Session session = new FakeSession(userId, realSession);
        final UserConfigurationHelper userConfHelper = userConfigFactory.create(session, id, Mode.WRITE_BACK);
        final FolderHelperService folderHelperService = folderHelperServiceFactory.create(realSession);
        final String userFolderId = folderHelperService.getUserFolderId(realSession);

        JSONArray array = userConfHelper.getJSONArray(key);
        if (null == array) {
            if (fromUser) {
                try{
                    //Migration
                    if (StringUtils.isEmpty(userFolderId)) {
                        LOG.warn("Couldn't retrieve valid user folder");
                        return;
                    }
                    array = userConfHelper.getJSONArray("UserTemplateFolders");
                    if (null == array){
                        array = new JSONArray();
                    }

                    Set<String> tmpList = new LinkedHashSet<>();
                    for (int i = 0; i < array.length(); i++){
                        tmpList.add(array.getString(i));
                    }
                    tmpList.add(userFolderId);
                    array = new JSONArray(tmpList);

                    userConfHelper.setValue(key, array);
                    userConfHelper.setValue("UserTemplateFolders", new JSONArray());
                    userConfHelper.flushCache();
                } catch (JSONException e){
                    throw new OXException(e);
                }
            } else {
                return;
            }
        }

        try{
            for (int i = 0; i < array.length(); i++){
                String folderId = array.getString(i);

                boolean ignoreNoneTemplates = false;
                if (StringUtils.equals(folderId, userFolderId)) {
                    ignoreNoneTemplates = true;
                }
                target.put(folderId, ignoreNoneTemplates);
            }
        }
        catch (JSONException e){
            throw new OXException(e);
        }
    }

    /**
     * Collects the files within the specified folder while checking that no
     * duplicate files are added.
     *
     * @param folder
     *  The folder which should be scanned.
     *
     * @param templates
     *  The JSONArray which will contain the found files.
     *
     * @param duplicates
     *  A hash map which ensures that no duplicate files are collected. The
     *  first occurrence will win and be part of the templates collection.
     *
     * @param extFilter
     *  A filter that filter out all files which don't comply to the
     *  extension filter provided. Can be null so no filtering is done.
     *
     * @return
     *  ErrorCode.NONE on success or an error code if something went wrong.
     */
    private ErrorCode collectTemplateFiles(java.io.File folder, JSONArray templates, HashMap<String, Boolean> duplicates, String type, ExtensionFilter extFilter) {
        java.io.File[] filesAndFolders = folder.listFiles();

        if (null != filesAndFolders) {
            for (java.io.File file : filesAndFolders) {
                if (file.isDirectory()) {
                    // scan sub-folders, too
                    collectTemplateFiles(file, templates, duplicates, type, extFilter);
                } else {
                    // scan the current folder for the files
                    String fileName = file.getName();
                    if (null != fileName) {
                        Boolean exists = duplicates.get(fileName);
                        if (null == exists) {
                            // new entry found, put into map and add it to the
                            // templates
                            duplicates.put(fileName, true);
                            try {
                                // we use the extension to determine the mime type of the file
                                boolean addFile = true;
                                String ext = FileHelper.getExtension(fileName);

                                // use optional file extension filter to filter
                                // out unwanted files.
                                if ((null == extFilter) || ((null != extFilter) && extFilter.accept(file, fileName))) {
                                    String fileMimeType = getMimeTypeFromExtension(ext);
                                    long lastModified = file.lastModified();

                                    if (addFile && DISABLE_BINARY_MS_FORMATS) {
                                        addFile = !isBinaryMSMimeType(fileMimeType);
                                    }

                                    if (addFile) {
                                        putFileData(templates, true, file.getCanonicalPath(), "template:local", file.getName(), file.getName(), "global", type, lastModified, fileMimeType);
                                    }
                                }
                            } catch (JSONException e) {
                                // just ignore
                            } catch (IOException e) {
                                return ErrorCode.GETTEMPLATELIST_GLOBALFOLDER_IO_ERROR;
                            }
                        }
                    }
                }
            }
        }

        return ErrorCode.NO_ERROR;
    }

    /**
     * Scans a set of folders for specific language folders and collects
     * all files within these folders.
     *
     * @param folders
     *  An array of Files that specifies what folders should be scanned for the
     *  language specific folders.
     *
     * @param templates
     *  A JSON array which will collect all template files.
     *
     * @param langCode
     *  The user language code in ISO format.
     *
     * @param extFilter
     *  A filter that filter out all files which don't comply to the
     *  extension filter provided. Can be null so no filtering is done.
     *
     * @return
     *  ErrorCode.NONE on success or an error code if something went wrong.
     */
    private ErrorCode scanForLanguageDocumentTemplates(java.io.File[] folders, JSONArray templates, String langCode, ExtensionFilter extFilter) {
        final String fallbackLangCode = "en";
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        // create the general language code if we have a specific one
        String generalLangCode = langCode;
        if ((null != langCode) && (langCode.indexOf("-") > 0)) {
            generalLangCode = langCode.substring(0, langCode.indexOf("-"));
        }

        // loop through all document folders and search for the concrete
        // language folders within these folders.
        for (java.io.File folder : folders) {
            String folderName = folder.getName();
            String type = null;

            if (ApplicationType.APP_TEXT_STRING.equalsIgnoreCase(folderName)) {
                type = DocumentType.TEXT.toString().toLowerCase();
            } else if (ApplicationType.APP_SPREADSHEET_STRING.equalsIgnoreCase(folderName)) {
                type = DocumentType.SPREADSHEET.toString().toLowerCase();
            } else if (ApplicationType.APP_PRESENTATION_STRING.equalsIgnoreCase(folderName)) {
                type = DocumentType.PRESENTATION.toString().toLowerCase();
            }

            if (null != type) {
                // now we sort the entries for size to search the specific
                // language folder before the general one.
                int numOfTemplates = templates.length();
                HashMap<String, Boolean> duplicates = new HashMap<>();
                java.io.File[] langFoldersFound = folder.listFiles(new LanguageFolderFilter(langCode, generalLangCode));
                if (langFoldersFound.length > 0) {

                    java.util.List<java.io.File> langFolders = Arrays.asList(langFoldersFound);
                    java.util.Collections.sort(langFolders, new java.util.Comparator<java.io.File>() {
                        @Override
                        public int compare(java.io.File f1, java.io.File f2) {
                            return (f1.getName().length() > f2.getName().length()) ? -1 : 1;
                        }
                    });

                    // iterate through the found language folders and collect
                    // the template file.
                    ErrorCode collectErrorCode = ErrorCode.NO_ERROR;
                    for (java.io.File langFolder : langFolders) {
                        collectErrorCode = collectTemplateFiles(langFolder, templates, duplicates, type, extFilter);
                        if (collectErrorCode.getCode() != ErrorCode.NO_ERROR.getCode()) {
                            errorCode = collectErrorCode;
                            break;
                        }
                    }
                }

                // if we didn't find any templates in the specific lang folders
                // and the fallback language code is not langCode or
                // generalLangCode, we try to use it as last possible step
                if ((templates.length() == numOfTemplates) &&
                    (!((fallbackLangCode.equalsIgnoreCase(langCode)) || (fallbackLangCode.equalsIgnoreCase(generalLangCode))))) {
                    langFoldersFound = folder.listFiles(new LanguageFolderFilter(fallbackLangCode, null));
                    if (langFoldersFound.length > 0) {
                        errorCode = collectTemplateFiles(langFoldersFound[0], templates, duplicates, type, extFilter);
                    }
                }

                // Finally, check if there are some common templates
                langFoldersFound = folder.listFiles(new LanguageFolderFilter("common", null));
                if (langFoldersFound.length > 0) {
                    errorCode = collectTemplateFiles(langFoldersFound[0], templates, duplicates, type, extFilter);
                }
            }
        }

        return errorCode;
    }

    /**
     * Scans the global template folder, trying to find the document specific
     * folders according to the provided searchFilter and the provided user
     * language code.
     *
     * @param path
     *  The path of the global templates folder containing the document type specific sub-folders.
     *  Multiple template root folders can be separated by semicolon.
     *
     * @param templates
     *  A JSONArray which will contain the found document templates within
     *  the templates folders.
     *
     * @param searchFilter
     *  A filter containing the type to select the document templates, a
     *  value of DocumentType.NONE means all document types.
     *
     * @param extFilter
     *  A filter that filter out all files which don't comply to the
     *  extension filter provided. Can be null so no filtering is done.
     *
     * @return
     *  ErrorCode.NONE on success or an error code if something went wrong.
     */
    private ErrorCode scanGlobalTemplateFolder(String path, JSONArray templates, DocumentType searchFilter, String userLangCode, ExtensionFilter extFilter) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        StringTokenizer defaultTokenizer = new StringTokenizer(path, ";", false);

        while (defaultTokenizer.hasMoreTokens())
        {
            final String foldername = defaultTokenizer.nextToken();

            try {
                java.io.File file = new java.io.File(foldername);
                if (file.exists() && file.isDirectory()) {
                    java.io.File[] files = file.listFiles();

                    if (DocumentType.NONE == searchFilter) {
                        // use all folders, filter out possible files
                        ArrayList<java.io.File> folders = new ArrayList<>();
                        for (java.io.File folder : files) {
                            if (folder.isDirectory()) {
                                folders.add(folder);
                            }
                        }

                        if (!folders.isEmpty()) {
                            scanForLanguageDocumentTemplates(files, templates, userLangCode, extFilter);
                        }
                    } else {
                        // use just one folder
                        String folderToFind = searchFilter.name();
                        for (java.io.File folder : files) {
                            if (folder.isDirectory() && (folderToFind.equalsIgnoreCase(folder.getName()))) {
                                java.io.File[] theFolder = new java.io.File[1];
                                theFolder[0] = folder;
                                scanForLanguageDocumentTemplates(theFolder, templates, userLangCode, extFilter);
                                break;
                            }
                        }
                    }
                }
            } catch (SecurityException e) {
                LOG.warn("SecurityException caught trying to scan global template folder - please check securiy settings", e);
                errorCode = ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR;
            }
        }

        return errorCode;
    }

    /**
     * Scans the provided folder for document template files according to the
     * search filter.
     *
     * @param fileAccess
     *  The file access to be used for the method.
     *
     * @param templates
     *  A JSON array that will collect the found document template files.
     *
     * @param folderId
     *  The folder to be searched.
     *
     * @param searchFilter
     *  The search filter which specifies what template documents should be
     *  collected.
     *
     * @return
     *  ErrorCode.NONE on success or an error code if something went wrong.
     */
    private ErrorCode scanUserFolderForTemplates(IDBasedFileAccess fileAccess, JSONArray templates, String folderId, DocumentType searchFilter, boolean ignoreNoneTemplates) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        if ((null != folderId) && (null != fileAccess)) {
            try {
                // retrieve all files from the user folder, currently we don't want to
                // search deeper in the folder!
                TimedResult<File> files = fileAccess.getDocuments(folderId);
                if (null != files) {
                    java.util.Date currentDate = new java.util.Date();
                    SearchIterator<File> iter = files.results();

                    while (iter.hasNext()) {
                        File file = iter.next();
                        String ext = FileHelper.getExtension(file.getFileName());
                        String fileMimeType = file.getFileMIMEType();
                        String knownMimeType = ExtensionHelper.getMimeTypeFromExtension(ext);
                        DocumentType fileDocType = DocumentType.NONE;
                        java.util.Date lastModifiedDate = file.getLastModified();
                        long lastModified = (null != lastModifiedDate) ? lastModifiedDate.getTime() : currentDate.getTime();

                        // First use the extension to find (template) documents, second the
                        // mime type of the file. Special handling for binary ms document
                        // (template) files as they share the same mime type as the normal
                        // documents. Dependent on the filter set at construction time only
                        // templates or all supported document files are found (special case
                        // that we want to ignore non-templates in special folders).
                        if (null != knownMimeType) {
                            fileDocType = getDocumentTypeFromMimeType(knownMimeType, ignoreNoneTemplates);
                        } else {
                            fileDocType = getDocumentTypeFromMimeType(fileMimeType, ignoreNoneTemplates);
                        }

                        if ((null != fileDocType) && (DocumentType.NONE != fileDocType) &&
                            ((DocumentType.NONE == searchFilter) || (searchFilter == fileDocType))) {
                            putFileData(templates, false, file.getId(), folderId, file.getFileName(), file.getTitle(), "user", fileDocType.toString().toLowerCase(), lastModified, file.getFileMIMEType());
                        }
                    }
                }
            } catch (OXException e) {
                errorCode = ErrorCode.GETTEMPLATELIST_UNKNOWN_ERROR;
            } catch (JSONException e) {
                errorCode = ErrorCode.GETTEMPLATELIST_UNKNOWN_ERROR;
            }
        } else {
            return ErrorCode.GETTEMPLATELIST_USERFOLDER_UNKNOWN_ERROR;
        }

        return errorCode;
    }

    /**
     * Put the file data into the provided JSONArray.
     *
     * @param array
     *  The JSONArray which should be used to add a new file data entry to.
     *
     * @param isLocalFile
     *  Must be set to TRUE if the file data specify a local file system
     *  document.
     *
     * @param fileId
     *  The file id of the document template. Must be a OX Drive specific id
     *  or a local file system path (depends on isLocalFile). Must not be null.
     *
     * @param folderId
     *  The folder id of the document template. Must not be null.
     *
     * @param fileName
     *  The file name of the document template. Must not be null.
     *
     * @param title
     *  The title of the document template. Should be set to fileName, if
     *  there is no title. Must not be null.
     *
     * @param source
     *  The source of the document template. Possible values must be "user",
     *  "global", which describe where th document template has been found.
     *  Must not be null.
     *
     * @param type
     *  The document type of the document template. Possible values are:
     *  "text", "spreadsheet" or "presentation". Must not be null.
     *
     * @param lastModified
     *  The last modified date in millseconds since
     *
     * @param mimeType
     *  The mime type of the document template. Can be set to null, if the
     *  mime type is unknown.
     *
     * @throws JSONException
     */
    private void putFileData(JSONArray array, boolean isLocalFile, String fileId, String folderId, String fileName, String title, String source, String type, long lastModified, String mimeType) throws JSONException {
        try {
            final JSONObject fileData = new JSONObject();
            fileData.put("folder_id", folderId);
            fileData.put("filename", fileName);
            fileData.put("title", title);
            fileData.put("source", source);
            fileData.put("type", type);
            fileData.put("last_modified", lastModified);
            if (null != mimeType) {
                fileData.put("mimetype", mimeType);
            }

            // Special treatment for local files - we generate a artificial ID
            // which is stored with the path in the LocalFileMappingManager.
            // This helps to protect this code from being used to access other
            // files due to hacked strings.
            if (isLocalFile) {
                String id = m_localFileMapper.getIDFromPath(fileId);
                if (null == id) {
                    id = LocalFileMappingManager.generateIDFromPath(fileId);
                    if (null != id) {
                        m_localFileMapper.associateIdAndPath(id, fileId);
                        // set the artificial ID
                        fileId = id;
                    }
                } else {
                    fileId = id;
                }
            }

            // put entry into the array
            if (null != fileId) {
                fileData.put("id", fileId);
                array.put(fileData);
            }
        } catch (JSONException e) {
            throw e;
        }
    }

    private static boolean isErrorCode(OXException e, FileStorageExceptionCodes code) {
        return e.getCode() == code.getNumber() && e.getPrefix().equals(FileStorageExceptionCodes.PREFIX);
    }

    private static boolean isErrorCode(OXException e, FolderExceptionErrorMessage code) {
        return e.getCode() == code.getNumber() && e.getPrefix().equals(FolderExceptionErrorMessage.prefix());
    }

    /**
     * Determine if the provided mime type belongs to a binary document
     * template format of MS Office.
     *
     * @param mimeType
     *  The mime type to be checked.
     *
     * @return
     *  TRUE if the mime type belongs to a binary document template format of
     *  MS Office.
     */
    private boolean isBinaryMSMimeType(String mimeType) {
        return ((MimeTypeHelper.MIMETYPE_MSEXCEL.equalsIgnoreCase(mimeType)) ||
                (MimeTypeHelper.MIMETYPE_MSPOWERPOINT.equalsIgnoreCase(mimeType)) ||
                (MimeTypeHelper.MIMETYPE_MSWORD.equalsIgnoreCase(mimeType)));
    }

    /**
     * Provides the document type dependent on the mime type and filter set.
     *
     * @param mimeType
     *  the mime type of the documents
     *
     * @param ignoreNoneTemplates
     *  force to ignore non-template documents
     *
     * @return the document type of the mime type. Can be DocumentType.NONE if
     *  the mime type is unknown or doesn't comply to the set document type filter.
     *  @see TemplateFilterType
     */
    private DocumentType getDocumentTypeFromMimeType(final String mimeType, boolean ignoreNoneTemplates) {
        DocumentType result = DocumentType.NONE;

        if ((m_filterType == TemplateFilterType.ALL_DOCUMENTS) && !ignoreNoneTemplates) {
            result = MimeTypeHelper.getDocumentTypeFromMimeType(mimeType);
        } else {
            result = MimeTypeHelper.getDocumentTypeFromTemplateMimeType(mimeType);
        }
        result = (DISABLE_BINARY_MS_FORMATS && isBinaryMSMimeType(mimeType)) ? DocumentType.NONE : result;

        return result;
    }

    /**
     * Provides the mime type of a file extension if it's a supported document
     * type and complies to the current filter settings.
     *
     * @param extension a file extension
     * @return the mime type of the file or null if unknown
     */
    private String getMimeTypeFromExtension(final String extension) {
        return (m_filterType == TemplateFilterType.ALL_DOCUMENTS) ?
                ExtensionHelper.getMimeTypeFromExtension(extension) : ExtensionHelper.getMimeTypeFromTemplateExtension(extension);
    }
}
