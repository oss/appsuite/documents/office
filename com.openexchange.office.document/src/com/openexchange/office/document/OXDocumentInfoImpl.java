/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.office.document.api.DocFileService;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.LockingInfo;
import com.openexchange.office.document.api.OXDocumentInfo;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;

public class OXDocumentInfoImpl implements OXDocumentInfo, InitializingBean {

    //-------------------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(OXDocumentImpl.class);

    //-------------------------------------------------------------------------
    protected static final int MAX_DOCUMENT_FILE_SIZE = 100; // MBytes

    //-------------------------------------------------------------------------
    private boolean newDoc = false;
    private File metaData = null;
    private ErrorCode lastErrorCode = ErrorCode.NO_ERROR;
    private StorageHelperService storageHelper= null;
    private DocumentMetaData documentMetaData = null;
    private Session session = null;
    private DocumentFormat documentFormat = null;
    private boolean isTemplateDoc = false;
    private boolean isRescueDoc = false;

    private final String fileId;
    private final String encryptionInfo;

    @Autowired
    private DocFileServiceFactory docFileServiceFactory;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link OXDocumentImpl}.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param folderId
     *            The folder id of the file to be accessed.
     * @param fileId
     *            The file id of the file to be accessed.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     * @param storageHelper
     *            A helper instance which provides data to the referenced document storage. The instance must be
     *            initialized with the same folderId, where the document itself is stored.
     */
    public OXDocumentInfoImpl(Session session, String fileId, boolean newDoc, StorageHelperService storageHelper) {
        this.session = session;
        this.newDoc = newDoc;
        this.storageHelper = storageHelper;

        this.fileId = fileId;
        this.encryptionInfo = "";

//        impl_Init(fileId, "");
    }

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link OXDocumentImpl}.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param userData
     *            The UserData instance of the user associated with the session.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     * @param storageHelper
     *            A helper instance which provides data to the referenced document storage. The instance must be
     *            initialized with the same folderId, where the document itself is stored.
     */
    public OXDocumentInfoImpl(Session session, UserData userData, DocFileServiceImpl fileHelper, boolean newDoc, StorageHelperService storageHelper) {
        this.session = session;
        this.newDoc = newDoc;
        this.storageHelper = storageHelper;

        this.fileId = userData.getFileId();
        this.encryptionInfo = userData.getEncryptionInfo();
//        impl_Init(userData.getFileId(), userData.getEncryptionInfo());
    }

    @Override
	public void afterPropertiesSet() throws Exception {
    	impl_Init(fileId, encryptionInfo);
	}

	//-------------------------------------------------------------------------
    /**
     * Initializes the OXDocument instance for source mode file.
     *
     * @param folderId
     * @param fileId
     * @param fileHelper
     * @param encryptionInfo
     */
    private void impl_Init(final String fileId, String encryptionInfo) {
    	final DocFileService fileHelper = docFileServiceFactory.createInstance();

        try {
            // See #40279 - it's possible to get the document stream, but not the meta data
            // In this case prevent that we have a document buffer - which will result in a
            // general error.
            metaData = fileHelper.getMetaDataFromFile(session, fileId, FileStorageFileAccess.CURRENT_VERSION);

            if (null != metaData) {
                long nMaxDocumentFileSize = MAX_DOCUMENT_FILE_SIZE * 1024L * 1024L;

                nMaxDocumentFileSize = configurationService.getIntProperty("com.openexchange.office.maxDocumentFileSize", MAX_DOCUMENT_FILE_SIZE) * 1024L * 1024L;

                final long nFileSize = metaData.getFileSize();

                if (nFileSize <= nMaxDocumentFileSize) {
                	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

                    final boolean bWriteProtected = !fileHelperService.canWriteToFile(metaData, session.getUserId());
                    final LockingInfo aLockingInfo = fileHelper.getLockingInfo(session, metaData);

                    documentFormat = fileHelper.getDocumentFormat();
                    documentMetaData = new DocumentMetaData(metaData, aLockingInfo.getLockedByUser(), aLockingInfo.getLockedByUserId());
                    documentMetaData.setWriteProtectedState(bWriteProtected);
                    isTemplateDoc = DocumentFormatHelper.isSupportedTemplateFormat(documentMetaData.getMimeType(), FileHelper.getExtension(documentMetaData.getFileName()));
                } else {
                	lastErrorCode = ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR;
                    log.debug("RT connection: Document exceeds limit for file size {}MB, doc size: {}MB!", nMaxDocumentFileSize, (nFileSize / (1024L * 1024L)));
                }
            } else {
                log.warn("RT connection: Unable to retrieve the document's meta data - must give up to open the file");
            }
        } catch (OXException e) {
            lastErrorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
            MDCHelper.safeMDCPut(MDCEntries.ERROR, lastErrorCode.getCodeAsStringConstant());
            log.error("RT connection: unable to retrieve meta data or permissions, exception caught", e);
            MDC.remove(MDCEntries.ERROR);
        } catch (Exception e) {
            lastErrorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
            MDCHelper.safeMDCPut(MDCEntries.ERROR, lastErrorCode.getCodeAsStringConstant());
            log.error("RT connection: general exception caught", e);
            MDC.remove(MDCEntries.ERROR);
        }
    }

    //-------------------------------------------------------------------------
	@Override
	public DocumentMetaData getDocumentMetaData() {
		return documentMetaData;
	}

    //-------------------------------------------------------------------------
	@Override
	public ErrorCode getLastError() {
        return lastErrorCode;
	}

    //-------------------------------------------------------------------------
	@Override
	public Session getSession() {
		return session;
	}

    //-------------------------------------------------------------------------
	@Override
	public String getFolderId() {
        return (metaData != null) ? metaData.getFolderId() : null;
	}

    //-------------------------------------------------------------------------
	@Override
	public String getFileId() {
        return (metaData != null) ? metaData.getId() : null;
	}

    //-------------------------------------------------------------------------
	@Override
	public String getVersion() {
        return (metaData != null) ? metaData.getVersion() : null;
	}

    //-------------------------------------------------------------------------
	@Override
	public File getMetaData() {
		return metaData;
	}

    //-------------------------------------------------------------------------
	@Override
	public StorageHelperService getStorageHelper() {
        return storageHelper;
	}

    //-------------------------------------------------------------------------
	@Override
	public DocumentFormat getDocumentFormat() {
        return documentFormat;
	}

    //-------------------------------------------------------------------------
	@Override
	public DocumentType getDocumentType() {
        DocumentType type;
        final DocumentFormat format = getDocumentFormat();
        switch (format) {
        case DOCX:
        case ODT : type = DocumentType.TEXT; break;

        case XLSX:
        case ODS : type = DocumentType.SPREADSHEET; break;

        case PPTX:
        case ODP : type = DocumentType.PRESENTATION; break;

        case NONE:
        default  : type = DocumentType.NONE; break;
        }

        return type;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isRescueDocument() {
        return isRescueDoc;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isTemplateDocument() {
		return isTemplateDoc;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isNewDocument() {
		return newDoc;
	}

    //-------------------------------------------------------------------------
	@Override
	public int getUniqueDocumentId() {
		return OXDocumentImpl.DEFAULT_DOCUMENT_ID;
	}

    //-------------------------------------------------------------------------
	@Override
	public byte[] getDocumentBuffer() {
		return null;
	}

}
