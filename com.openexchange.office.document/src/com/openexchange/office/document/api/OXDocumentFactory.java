/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import java.io.InputStream;

import com.openexchange.office.document.OXDocumentFactoryImpl;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.annotation.ConcreteService;
import com.openexchange.office.tools.common.debug.SaveDebugProperties;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;

@ConcreteService(clazz=OXDocumentFactoryImpl.class)
public interface OXDocumentFactory {

    //-------------------------------------------------------------------------
    /**
     * Creates a new OXDocumentInfo instance.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param fileId
     *            The file id of the file to be accessed.
     */
	public OXDocumentInfo createDocumentInfoAccess(Session session, String fileId, boolean newDoc);

    //-------------------------------------------------------------------------
    /**
     * Creates a new OXDocumentInfo instance.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param userData
     *            The UserData instance of the user associated with the session.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     */
    public OXDocumentInfo createDocumentInfoAccess(Session session, UserData userData, boolean newDoc);

    //-------------------------------------------------------------------------
    /**
     * Create a new OXDocument instance.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param folderId
     *            The folder id of the file to be accessed.
     * @param fileId
     *            The file id of the file to be accessed.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     */
    public OXDocument createDocumentAccess(Session session, String folderId, String fileId, boolean newDoc, final IResourceManager resourceManager, boolean logDocContext, final SaveDebugProperties dbgProps);

    //-------------------------------------------------------------------------
    /**
     * Create a new OXDocument instance.
     *
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param userData
     *            The UserData instance of the user associated with the session.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     * @param storageHelper
     *            A helper instance which provides data to the referenced document storage. The instance must be
     *            initialized with the same folderId, where the document itself is stored.
     */
	public OXDocument createDocumentAccess(Session session, UserData userData, boolean newDoc, IResourceManager resourceManager, boolean logDocContext,	SaveDebugProperties dbgProps);

    //-------------------------------------------------------------------------
    /**
     * Create a new OXDocument instance.
     *
     * @param session
     * @param services
     * @param docStream
     * @param fileName
     * @param mimeType
     * @param resourceManager
     * @param storageHelper
     */
    public OXDocument createDocumentAccess(Session session, InputStream docStream, String fileName, String mimeType, IResourceManager resourceManager, StorageHelperService storageHelper);
}
