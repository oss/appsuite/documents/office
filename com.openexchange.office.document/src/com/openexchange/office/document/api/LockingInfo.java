/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import com.openexchange.office.tools.doc.StreamInfo;

public class LockingInfo {
    public String lockingUserName;
    public int    lockingUserId;

    public LockingInfo() {
        this.lockingUserName = StreamInfo.NO_USERNAME;
        this.lockingUserId   = StreamInfo.NO_USERID;
    }

    public LockingInfo(final String lockingUserName, final int lockingUserId) {
        this.lockingUserName = lockingUserName;
        this.lockingUserId   = lockingUserId;
    }

    public String getLockedByUser() {
        return lockingUserName;
    }

    public int getLockedByUserId() {
        return lockingUserId;
    }

    public void setLockedByUser(final String lockedByUser) {
        lockingUserName = lockedByUser;
    }

    public void setLockedByUserId(final int lockedByUserId) {
        lockingUserId = lockedByUserId;
    }

}
