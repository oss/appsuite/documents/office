/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import com.openexchange.office.document.AdvisoryLockInfoServiceImpl;
import com.openexchange.office.tools.annotation.ConcreteService;
import com.openexchange.session.Session;

@ConcreteService(clazz=AdvisoryLockInfoServiceImpl.class)
public interface AdvisoryLockInfoService {

    /**
     * Checks and possibly updates an advisory lock for a document file with current host name and user information.
     *
     * @param session the session of the client that wants to check a possible advisory lock
     * @param name the name of the system used by the client to access the document file.
     * @param folderId the folder id of the document file that should be checked.
     * @param fileId the file id of the document file that should be checked.
     * @throws Exception
     */
    public AdvisoryLockInfo checkAndUpdateAdvisoryLock(Session session, String name, String folderId, String fileId) throws Exception;

    /**
     * Resets an advisory lock for a document file with current host name and user information.
     *
     * @param session the session of the client that wants to reset the advisory lock
     * @param name the name of the system used by the client to access the document file.
     * @param folderId the folder id of the document file that should reset the advisory lock information.
     * @param fileId the file id of the document file which that should reset the advisory lock information.
     * @return TRUE if the advisory lock was reset or FALSE if not.
     * @throws Exception
     */
    public boolean resetAdvisoryLock(Session session, String name, String folderId, String fileId) throws Exception;

    /**
     * Clears an advisory lock previously stored for a document file.
     *
     * @param session the session of the client that wants to clear the advisory lock
     * @param folderId the folder id of the document file which should have a cleared advisory lock
     * @param fileId the file id of the document file which should have a cleared advisory lock
     * @throws Exception
     */
    public void clearAdvisoryLock(Session session, String folderId, String fileId) throws Exception;

    /**
     * Determines if the advisory lock feature is enabled or not.
     *
     * @return TRUE if the advisory lock feature is enabled otherwise FALSE.
     */
    public boolean isAdvisoryLockEnabled();

    /**
     * Provides the current advisory lock mode.
     *
     * @return the advisory lock mode. If the advisory lock feature is not enabled
     * ADVISORY_LOCK_MODE_NOT_ENABLED will be returned.
     */
    public AdvisoryLockMode getAdvisoryLockMode();

    /**
     * Provides the custom name of the system to be used for the advisory lock info.
     * This value is only valid if the advisory lock mode is set to
     * ADVISORY_LOCK_MODE_CUSTOM. In all other cases this value will be an empty string.
     *
     * @return the name of the system to be used for the advisory lock in case the
     * advisory lock mode is set to ADVISORY_LOCK_MODE_CUSTOM.
     */
    public String getAdvisoryLockSystemName();
}
