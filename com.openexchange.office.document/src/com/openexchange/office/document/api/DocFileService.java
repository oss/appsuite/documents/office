/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import java.io.InputStream;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.StreamInfo;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;

public interface DocFileService {

	public static final String NO_ENCRYPTION = "";

    /**
     * Retrieves the document format.
     *
     * @return
     *         The document format of the file used by the file helper
     *         instance.
     */
    public DocumentFormat getDocumentFormat();

    /**
     * Retrieves the document type.
     *
     * @return
     *         The document format of the file used by the file helper
     *         instance.
     */
    public DocumentType getDocumentType();

    /**
     * Access the responsible storage adapter to retrieve the meta
     * data directly from the storage. This method doesn't cache
     * the data and is therefore expensive!
     *
     * @param session
     *            The session to be used for a possible access to the real
     *            meta data.
     *
     * @return
     */
    public File getMetaDataFromStorage(Session session, final String fileId) throws OXException;

    /**
     * 
     * @param session
     * @param requestData
     * @param encryptionInfo
     * @return
     */
    public StreamInfo getDocumentStream(Session session, AJAXRequestData requestData, final String encryptionInfo );

    /**
     * Retrieves the document stream from the file.
     *
     * @param session
     *            The session of the user who requested to retrieve the document
     *            stream.
     *
     * @return
     *         The document stream or null if the document stream could not
     *         be retrieved.
     */
    public StreamInfo getDocumentStream(Session session, String folderId, String fileId, String encryptionInfo);

    /**
     * 
     * @param session
     * @param folderId
     * @param fileId
     * @param version
     * @param encryptionInfo
     * @return
     */
    public StreamInfo getDocumentStream(Session session, String folderId, String fileId, String version, String encryptionInfo);

    /**
     * Retrieve user information about the user who locked a file.
     * @param session
     * @param file
     * @return
     *  The locking information with user id and user name or NULL if nobody has
     *  locked the file.
     */
    public LockingInfo getLockingInfo(final Session session, final File file);

    /**
     * Renames the document file in the info store using the provided
     * new file name. ATTENTION: This method doesn't check for the
     * storage capabilities. It just
     *
     * @param session
     *            The session of the user who requested to rename the document file.
     *
     * @param newFileName
     *            The new file name of the document file.
     *
     * @return
     *         The new file name of the document file. Can be null if the rename
     *         operation failed.
     */
    public RenameResult renameDocument(Session session, String newFileName, final String folderId, final String fileId);

    /**
     * Provides the latest version string of the file
     *
     * @param session
     *            The session to be used to access the file store.
     *
     * @return
     *         The version string or null.
     *
     * @throws OXEception
     *             Throws an exception if the meta data of the document file
     *             couldn't be retrieved.
     */
    public String getVersionFromFile(Session session, String fileId) throws OXException;

    /**
     * Provides the file name of the file
     *
     * @param session
     *            The session to be used to access the file store.
     *
     * @param fileId
     *            The id of the file which name should be retrieved.
     *
     * @return
     *         The file name or null.
     *
     * @throws OXEception
     *             Throws an exception if the meta data of the document file
     *             couldn't be retrieved.
     */
    public String getFilenameFromFile(Session session, String fileId) throws OXException;

    /**
     * Retrieves the file meta data using the provided session.
     *
     * @param session
     *            The session to be used as the context to retrieve the file meta data.
     *
     * @param fileId
     *            The id of the file which name should be retrieved.
     *
     * @param version
     *            A version of the file where the meta data should be retrieved from.
     *            Can be null where the method uses the latest version.
     *
     * @return
     *         The file meta data or null if necessary services are not available.
     *
     * @throws OXException
     *             Throws an exception if the meta data of the document file couldn't
     *             be retrieved.
     */
    public File getMetaDataFromFile(Session session, String fileId, String version) throws OXException;


    /**
     * Writes the provided document stream to the file associated with this
     * FileHelper instance.
     *
     * @param services
     *            The service lookup to be used by the method to create necessary
     *            services.
     *
     * @param session
     *            The session of the user who wants to write the document stream to the
     *            file.
     *
     * @param documentStream
     *            The document stream that should be stored into the file.
     *
     * @param fileId
     *            The file id of the file to read the document stream from.
     *
     * @param folderId
     *            The folder id where the file resides.
     *
     * @param metaData
     *            Optional meta data of the file to be written. Will be used to optimize the
     *            access to the storage, if present.
     *
     * @param storageHelper
     *            An instance which provides storage specific properties in the context of the
     *            session and folder used by this call.
     *
     * @param userExtension
     *            Specifies the extension to be used for the file.
     *
     * @param setUserExtension
     *            Specifies if the extension of the file should be set or
     *            removed. If TRUE the user extension will be set otherwise the user
     *            extension will be removed.
     *
     * @param revisionless
     *            Specifies if the stream should be written to the file using a new
     *            version or overwrite the latest version. TRUE means overwrite
     *            the latest version with the provided document stream. FALSE to
     *            create a new version.
     *
     * @return
     *         A WriteInfo instance which contains the information about the write document
     *         stream process. Information like error code, version and other properties
     *         can be retrieved.
     */
    public WriteInfo writeDocumentStream(final Session session, final InputStream documentStream, String fileId, String folderId, final File metaData, final StorageHelperService storageHelper, String userExtension, boolean setUserExtension, boolean revisionless, boolean encrypt);

    /**
     * Sets the version to a specified value of a file
     *
     * @param session    The session of the user who requested to rename the document file.
     * @param folderId   The folder id where the file resides.
     * @param fileId     The file id of the file to change the version.
     * @param setVersion The new version.
     * @return the set version
     * @throws OXException if something went wrong
     */
    public String setVersionOfFile(final Session session, final String folderId, final String fileId, final String setVersion) throws OXException;

}
