/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.session.Session;

public interface BackupFileService {
    public static final String BACKUP_FILE_POSTFIX = "-bak";

    /**
     * Checks whether the user can create or write a backup file in the
     * folder provided.
     *
     * @param session the session of the user that needs to write/create a bak-file
     * @param folderId the folder id where the bak-file should be written/created
     * @param metaData the document meta data of the original document file
     * @param encrypted specifies if the original document file is encrypted or not
     * @return TRUE if the user is able to create/write a bak, otherwise FALSE
     */
    public boolean canCreateOrWriteBackupFile(final Session session, final String folderId, final DocumentMetaData metaData, boolean encrypted);

}
