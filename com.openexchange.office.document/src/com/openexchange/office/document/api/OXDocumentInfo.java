/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import com.openexchange.file.storage.File;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;

public interface OXDocumentInfo {

    public DocumentMetaData getDocumentMetaData();

    public ErrorCode getLastError();

    public Session getSession();

    public String getFolderId();

    public String getFileId();

    public String getVersion();

    public File getMetaData();

    public StorageHelperService getStorageHelper();

    public DocumentFormat getDocumentFormat();

    public DocumentType getDocumentType();

    public boolean isRescueDocument();

    public boolean isTemplateDocument();

    public boolean isNewDocument();

    public int getUniqueDocumentId();

    public byte[] getDocumentBuffer();

}
