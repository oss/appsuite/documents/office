/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.tools.common.error.ErrorCode;

/**
 * Provides information about an optional advisory
 * lock set on a document file.
 *
 * @author Carsten Driesner {carsten.driesner@open-xchange.com}
 * @since 7.10.5
 *
 */
public class AdvisoryLockInfo {
    public static final String ADVISORYLOCK_KEY = "oxDocAdvisoryLock";

    private static final String PROP_HOSTNAME = "hostName";
    private static final String PROP_CONTEXT_ID = "cid";
    private static final String PROP_USER_ID = "uid";
    private static final String PROP_SINCE = "since";
    private static final String PROP_ORIG_FILEID = "origFileId";

    private final String name;
    private final int contextId;
    private final int userId;
    private final String sinceDateTime;
    private final String origFileId;
    private ErrorCode errorCode = ErrorCode.NO_ERROR;
    private boolean advisoryLockWritten = false;

    public AdvisoryLockInfo(ErrorCode errorCode) {
        name = "";
        contextId = -1;
        userId = -1;
        sinceDateTime = "";
        origFileId = "";
        this.errorCode = errorCode;
    }

	public AdvisoryLockInfo(String hostName, int contextId, int userId, String sinceDateTime, String origFileId) {
		this.name = hostName;
		this.contextId = contextId;
		this.userId = userId;
		this.sinceDateTime = sinceDateTime;
		this.origFileId = origFileId;
	}

	public AdvisoryLockInfo(JSONObject advisoryLockInfo) throws JSONException {
		name = advisoryLockInfo.getString(PROP_HOSTNAME);
		contextId = advisoryLockInfo.getInt(PROP_CONTEXT_ID);
		userId = advisoryLockInfo.getInt(PROP_USER_ID);
		sinceDateTime = advisoryLockInfo.getString(PROP_SINCE);
		origFileId = advisoryLockInfo.optString(PROP_ORIG_FILEID, "");
	}

	public JSONObject asJSONObject() throws JSONException {
		final JSONObject json = new JSONObject();
		json.put(PROP_HOSTNAME, this.name);
		json.put(PROP_CONTEXT_ID, this.contextId);
		json.put(PROP_USER_ID, this.userId);
		json.put(PROP_SINCE, this.sinceDateTime);
		json.put(PROP_ORIG_FILEID, this.origFileId);
		return json;
	}

	public String getName() {
		return this.name;
	}

    public int getContextId() {
        return this.contextId;
    }

    public int getUserId() {
        return this.userId;
    }

    public String getSinceDateTime() {
        return this.sinceDateTime;
    }

    public String getOrigFileId() {
        return this.origFileId;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public AdvisoryLockInfo setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    public boolean getAdvisoryLockWritten() {
        return this.advisoryLockWritten;
    }

    public void setAdvisoryLockWritten(boolean written) {
        this.advisoryLockWritten = written;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((sinceDateTime == null) ? 0 : sinceDateTime.hashCode());
        result = prime * result + contextId;
        result = prime * result + userId;
        result = prime * result + ((origFileId == null) ? 0 : origFileId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AdvisoryLockInfo other = (AdvisoryLockInfo) obj;
        if (contextId != other.contextId)
            return false;
        if (userId != other.userId)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (sinceDateTime == null) {
            if (other.sinceDateTime != null)
                return false;
        } else if (!sinceDateTime.equals(other.sinceDateTime))
            return false;
        if (origFileId == null) {
            if (other.origFileId != null)
                return false;
        } else if (!origFileId.equals(other.origFileId))
            return false;
        return true;
    }

}
