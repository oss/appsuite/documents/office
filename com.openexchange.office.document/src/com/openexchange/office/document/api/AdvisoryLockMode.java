/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import org.apache.commons.lang3.StringUtils;

public enum AdvisoryLockMode {

    ADVISORY_LOCK_MODE_ORIGIN,
    ADVISORY_LOCK_MODE_CUSTOM,
    ADVISORY_LOCK_MODE_NOT_ENABLED;

    public static AdvisoryLockMode getAdvisoryLockModeFromString(String mode) {
        final String ORIGIN = "origin";

        if (ORIGIN.equalsIgnoreCase(mode)) {
            return ADVISORY_LOCK_MODE_ORIGIN;
        } else if (StringUtils.isEmpty(mode)) {
            return ADVISORY_LOCK_MODE_NOT_ENABLED;
        }
        return ADVISORY_LOCK_MODE_CUSTOM;
    }
}
