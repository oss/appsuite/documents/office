/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.doc.OXDocumentException;

public interface OXDocument extends OXDocumentInfo {

	// the default document id
    public static final int DEFAULT_DOCUMENT_ID = -1;

    // specifies that no encryption info are available
    public static final String NO_ENCRYPTION_INFO = "";

    // factor to calc memory requirements for pgp document files
    public static final float PGP_FILE_SIZE_FACTOR = 1.75f;

    /**
     * Provides the active sheet index.
     *
     * @return The active sheet index or 0 if the index is unknown (in case of a non- spreadsheet document) or cannot be retrieved.
     */
    public int getActiveSheetIndex();

    /**
     * Provides the sheet count.
     *
     * @return The sheet count or 0 if the count is unknown (in case of a non- spreadsheet document) or cannot be retrieved.
     */
    public int getSheetCount();

    /**
     * Sets a new unique document id to be stored in the document on the next successful flushDocument.
     *
     * @param uniqueId The new unique document id to be stored.
     */
    public void setUniqueDocumentId(int uniqueId);

    /**
     * Provides possible rescue document operations which caused a filter exception.
     *
     * @return
     *         the JSONObject containing the rescue operations or null if there are no
     *         rescue operations.
     */
    public JSONArray getRescueOperations();

    /**
     * Checks that the last known version/osn of document content is consistent
     * with the loaded document.
     *
     * @param version the last known version of the document content
     * @param osn the last known osn of the document content
     * @return TRUE
     * @throws IllegalStateException if no document stream could be read. Therefore
     *             the method should only be called, if error code is NO_ERROR.
     */
    public boolean isConsistWithVersionAndOSN(final String version, int osn) throws Exception;

    /**
     * Retrieves the operations from the document and optional additional operations.
     *
     * @param session The session of the client requesting the document operations.
     * @param importer The importer instance to be used to read the document stream and provide the operations.
     * @param appendOperations Optional operations which should be appended to the document operations. Normally used if there are queued
     *            operations which are not part of the last document stream.
     * @param previewData Optional preview data object which contains data to request a special preview data from the filter including a
     *            possible callback.
     * @return The JSONObject containing all operations from the document, the saved document operations and the given operations.
     * @throws FilterException in case the filter implementation is not able to provide the operations.
     */
    public JSONObject getOperations(IImporter importer, final JSONObject appendOperations, boolean createFastLoadOperations) throws FilterException;

    /**
     * Provides the new document stream which uses the source document stream and the
     * operations to create a new document stream.
     *
     * @param exporter
     * @param resourceManager
     * @param actions
     * @param finalSave
     * @param saveAsTemplate
     * @return
     */
    public ResolvedStreamInfo getResolvedDocumentStream(final IExporter exporter, final IResourceManager resourceManager, final JSONArray operations, final String version, boolean finalSave, boolean saveAsTemplate, boolean dirty);

    /**
     * Saves the new document to the storage.
     *
     * @param exporter
     *            The exporter instance used to create the new document stream.
     * @param resourceManager
     *            The resource manager used by this document.
     * @param opsArray
     *            A JSONArray containing the pending operations to be stored in the new document.
     * @param revisionless
     *            Specifies if the new document should be saved as a new version or using the latest
     * @param finalSave
     *            Specifies if the new document stream should be handled as the final save. This can be used by the exporter to
     *            optimize the document, e.g. finally remove non-referenced resources.
     * @param encrypt
     *            Specifies if the document must be stored encrypted or not
     * @param dirty
     *            Specifies if calculated document content is valid or dirty (means must be calculated on load).
     * @return
     *         The result of the save operation stored in the SaveResult instance. The different properties (error code, version)
     *         can be retrieved from the instance.
     */
    public SaveResult save(final IExporter exporter, final IResourceManager resourceManager, final JSONArray opsArray, boolean revisionless, boolean finalSave, boolean encrypt, boolean dirty) throws OXDocumentException;

}
