/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.api;

import org.json.JSONObject;

import com.openexchange.file.storage.File;
import com.openexchange.office.tools.common.error.ErrorCode;

/**
 * Property class to transport result data for a rename request.
 *
 * @author Carsten Driesner
 *
 */
public class RenameResult {

    private ErrorCode errorCode;
    private File metaData;
    private JSONObject jsonResult;
    private String newFileId;
    private boolean reload;

    /**
     * Creates a success rename result object.
     */
    public RenameResult() {
        this.errorCode = ErrorCode.NO_ERROR;
        this.metaData = null;
        this.jsonResult = new JSONObject();
        this.newFileId = null;
        this.reload = false;
    }

    /**
     * Creates a rename result object with the provided
     * error code and meta data.
     *
     * @param errorCode the error code from the rename processing.
     * @param metaData the new mata data or null if not available.
     */
    public RenameResult(final ErrorCode errorCode, final File metaData, final String newFileId) {
        this.errorCode = errorCode;
        this.metaData = metaData;
        this.jsonResult = new JSONObject();
        this.newFileId = newFileId;
    }

    public ErrorCode errorCode() {
    	return errorCode;
    }

    public File metaData() {
    	return metaData;
    }

    public JSONObject jsonResult() {
    	return jsonResult;
    }

    public String newFileId() {
    	return newFileId;
    }

    public boolean reload() {
    	return reload;
    }

    public void setErrorCode(ErrorCode errorCode) {
    	this.errorCode = errorCode;
    }

    public void setMetaData(File metaData) {
    	this.metaData = metaData;
    }

    public void setJsonResult(JSONObject jsonResult) {
    	this.jsonResult = jsonResult;
    }

    public void setNewFileId(String newFileId) {
    	this.newFileId = newFileId;
    }

    public void setReload(boolean reload) {
    	this.reload = reload;
    }
}
