/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.stereotype.Service;

import com.openexchange.office.document.api.Document;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.tools.annotation.RegisteredService;

@Service
@RegisteredService(registeredClass=DocumentDisposer.class)
public class DocumentDisposerImpl implements DocumentDisposer  {

	private ConcurrentMap<String, WeakReference<Document>> aDocumentsMap = new ConcurrentHashMap<>();

    //-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see com.openexchange.office.document.DocumentDisposer#getDocument(java.lang.String)
	 */
	@Override
	public Document getDocument(String docUID) {
		WeakReference<Document> aWeakRef = aDocumentsMap.get(docUID);

		if (null != aWeakRef)
			return aWeakRef.get();

		return null;
	}

    //-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see com.openexchange.office.document.DocumentDisposer#addDocumentRef(com.openexchange.office.document.api.Document)
	 */
	@Override
	public void addDocumentRef(Document aDoc) {
		WeakReference<Document> aWeakRef = new WeakReference<>(aDoc);
		aDocumentsMap.putIfAbsent(aDoc.getDocUID().getValue(), aWeakRef);
	}

    //-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see com.openexchange.office.document.DocumentDisposer#removeDocumentRef(java.lang.String)
	 */
	@Override
	public void removeDocumentRef(String docUID) {
		aDocumentsMap.remove(docUID);
	}
}
