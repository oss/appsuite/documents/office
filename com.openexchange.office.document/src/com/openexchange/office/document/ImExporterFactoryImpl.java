/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.doc.DocumentFormat;

/**
 * 
 * @author 
 *
 */
@Service
@RegisteredService(registeredClass=ImExporterAccessFactory.class)
public class ImExporterFactoryImpl implements ImExporterAccessFactory, OsgiBundleContextAware {

    private final static Logger LOG = LoggerFactory.getLogger(ImExporterFactoryImpl.class);

    private OsgiBundleContextAndActivator bundleCtx;
    
    @Override
    public IImporter getImporterAccess(final DocumentFormat documentFormat) {
        IImporter importer = null;            
        
        switch (documentFormat) {
            case DOCX: {
                importer = bundleCtx.getService(com.openexchange.office.filter.ooxml.docx.Importer.class);
                break;
            }

            case PPTX: {
                importer = bundleCtx.getService(com.openexchange.office.filter.ooxml.pptx.Importer.class);
                break;
            }

            case XLSX: {
                importer = bundleCtx.getService(com.openexchange.office.filter.ooxml.xlsx.Importer.class);
                break;
            }

            case ODT: {
                importer = bundleCtx.getService(com.openexchange.office.filter.odf.odt.dom.Importer.class);
                break;
            }
            case ODP: {
                importer = bundleCtx.getService(com.openexchange.office.filter.odf.odp.dom.Importer.class);
                break;
            }
            case ODS: {
                importer = bundleCtx.getService(com.openexchange.office.filter.odf.ods.dom.Importer.class);
                break;
            }

            case NONE:
            case ODG:
            default: {
            	LOG.warn("ImExporterFactory: There is no import filter service available for the document format: " + documentFormat.name());
                break;
            }
        }
        return importer;
    }

    @Override
    public IExporter getExporterAccess(final DocumentFormat documentFormat) {
        IExporter exporter = null;

        switch (documentFormat) {
            case DOCX: {
                exporter = bundleCtx.getService(com.openexchange.office.filter.ooxml.docx.Exporter.class);
                break;
            }

            case PPTX: {
                exporter = bundleCtx.getService(com.openexchange.office.filter.ooxml.pptx.Exporter.class);
                break;
            }

            case XLSX: {
                exporter = bundleCtx.getService(com.openexchange.office.filter.ooxml.xlsx.Exporter.class);
                break;
            }

            case ODT:
            case ODP:
            case ODS: {
                exporter = bundleCtx.getService(com.openexchange.office.filter.odf.Exporter.class);
                break;
            }

            case NONE:
            case ODG:
            default: {
            	LOG.warn("ImExporterFactory: There is no export filter service available for the document format: " + documentFormat.name());
                break;
            }
        }
        return exporter;
    }

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
