/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import org.apache.commons.lang3.StringUtils;

import com.openexchange.office.tools.common.user.UserData;

public class DocContextLogger {

    public static String getContextString(final String user, final String folderId, final String fileId) {
        final StringBuffer buf = new StringBuffer(128);

        buf.append(StringUtils.isEmpty(user) ? "unknown": user);
        buf.append(", ");
        buf.append(StringUtils.isEmpty(folderId) ? "???" : folderId);
        buf.append(".");
        buf.append(StringUtils.isEmpty(fileId) ? "???" : fileId);
        return buf.toString();
    }

    public static String getContextStringWithDoc(boolean debugInfo, final String sUserId, final UserData userData, final OXDocumentImpl oxDoc) {
        final String idString = sUserId;
        final String folderId = ((null != userData) ? StringUtils.isEmpty(userData.getFolderId()) ? "???" : userData.getFolderId() : "???");
        final String fileId = ((null != userData) ? StringUtils.isEmpty(userData.getFileId()) ? "???" : userData.getFileId() : "???");
        final String fileName = oxDoc.getDocumentMetaData().getFileName();

        return getContextStringWithDoc(debugInfo, idString, folderId, fileId, fileName);
    }

    public static String getContextStringWithDoc(boolean debugInfo, final String sUserId, final String folder_id, final String file_id, final OXDocumentImpl oxDoc) {
        final String idString = sUserId;
        final String folderId = StringUtils.isEmpty(folder_id) ? "???" : folder_id;
        final String fileId = StringUtils.isEmpty(file_id) ? "???" : file_id;
        final String fileName = oxDoc.getDocumentMetaData().getFileName();

        return getContextStringWithDoc(debugInfo, idString, folderId, fileId, fileName);
    }

    public static String getContextStringWithDoc(boolean debugInfo, final String sUserId, final String folderId, final String fileId, final String fileName) {
        final StringBuffer buf = new StringBuffer(128);

        buf.append(StringUtils.isEmpty(sUserId) ? "unknown": sUserId);
        buf.append(", ");
        buf.append(StringUtils.isEmpty(folderId) ? "???" : folderId);
        buf.append(".");
        buf.append(StringUtils.isEmpty(fileId) ? "???" : fileId);
        if (debugInfo) {
            buf.append(", ");
            buf.append(StringUtils.isEmpty(fileName) ? "unknown doc name" : fileName);
        }
        return buf.toString();
    }

}
