/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.java.Strings;
import com.openexchange.office.document.api.DocFileService;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.DocumentConstants;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.ResolvedStreamInfo;
import com.openexchange.office.document.api.SaveResult;
import com.openexchange.office.document.api.WriteInfo;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.core.FilterExceptionToErrorCode;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.debug.SaveDebugProperties;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ErrorCode2BackupErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToBackupErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.office.tools.common.system.SystemInfoHelper;
import com.openexchange.office.tools.common.system.SystemInfoHelper.MemoryInfo;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.OXDocumentException;
import com.openexchange.office.tools.doc.StreamInfo;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import com.openexchange.user.UserService;


/**
 * {@link OXDocumentImpl}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class OXDocumentImpl implements OXDocument, InitializingBean {
    private static final Logger LOG = LoggerFactory.getLogger(OXDocumentImpl.class);

    //-------------------------------------------------------------------------
    public static final int MAX_DOCUMENT_FILE_SIZE = 100; // maximum file size in MiBytes for documents

    private static final Field UPDATE_METADATA_FIELDS[] = { Field.VERSION };
    private static final String PROP_OPERATIONS = "operations";

    //-------------------------------------------------------------------------
    private boolean m_newDoc = false;
    private boolean m_logDocContext = false;
    private File m_metaData = null;
    private ErrorCode m_lastErrorCode = ErrorCode.NO_ERROR;
    private byte[] m_documentBuffer = null;
    private StorageHelperService m_storageHelper = null;
    private DocumentMetaData m_documentMetaData = null;
    private JSONObject m_documentOperations = null;
    private IResourceManager m_resourceManager = null;
    private JSONObject m_uniqueDocumentId = null;
    private Session m_session = null;
    private DocumentFormat m_documentFormat = null;
    private boolean m_isTemplateDoc = false;
    private boolean m_isRescueDoc = false;
    private int m_activeSheetIndex = 0;
    private int m_sheetCount = 0;
    private SaveDebugProperties m_debugProperties = null;

    private String folderId;
    private String fileId;
    private String encryptionInfo;

    @Autowired
    private DocFileServiceFactory docFileServiceFactory;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private IResourceManagerFactory resourceManagerFactory;

    @Autowired
    private UserService userService;

    @Autowired
    private IDBasedFileAccessFactory idBasedFileAccessFactory;

    @Autowired(required=false)
    private CryptographicAwareIDBasedFileAccessFactory encryptionAwareFileAccessFactory;

    @Autowired(required=false)
    private CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    //-------------------------------------------------------------------------
    private static class DocumentStreamInfo {

        public final ErrorCode errorCode;
        public final InputStream docStream;

        public DocumentStreamInfo(final ErrorCode errorCode, final InputStream docStream, final JSONArray badOps) {
            this.errorCode = errorCode;
            this.docStream = docStream;
        }
    }

    //-------------------------------------------------------------------------
    private static class FilterInfo {

        public ErrorCode errorCode;
        public boolean retry;
        public int opsCanBeApplied;

        public FilterInfo(final ErrorCode errorCode, boolean retry, int opsCanBeApplied) {
            this.errorCode = errorCode;
            this.retry = retry;
            this.opsCanBeApplied = opsCanBeApplied;
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link OXDocumentImpl}.
     *
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param folderId
     *            The folder id of the file to be accessed.
     * @param fileId
     *            The file id of the file to be accessed.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     * @param storageHelper
     *            A helper instance which provides data to the referenced document storage. The instance must be
     *            initialized with the same folderId, where the document itself is stored.
     */
    public OXDocumentImpl(final Session session, final String folderId, final String fileId, boolean newDoc, final IResourceManager resourceManager, final StorageHelperService storageHelper, boolean logDocContext, final SaveDebugProperties dbgProps) {
        m_resourceManager = resourceManager;
        m_session = session;
        m_newDoc = newDoc;
        m_storageHelper = storageHelper;
        m_logDocContext = logDocContext;
        m_debugProperties = dbgProps;

        this.fileId = fileId;
        this.folderId = folderId;
        this.encryptionInfo = NO_ENCRYPTION_INFO;

//        impl_Init(folderId, fileId, NO_ENCRYPTION_INFO);
    }

    //-------------------------------------------------------------------------
    /**
     * Initializes a new {@link OXDocumentImpl}.
     *
     * @param services
     *            The service lookup instance to be used by this instance.
     * @param session
     *            The session of the client that requested a service which needs documents access.
     * @param userData
     *            The UserData instance of the user associated with the session.
     * @param fileHelper
     *            The FileHelper instance that is responsible for the file access regarding the document.
     * @param newDoc
     *            Specifies, if this instance references a new document or not.
     * @param resourceManager
     *            The ResourceManager instance which stores resources addressed by the document to be available via REST-API and
     *            other documents.
     * @param storageHelper
     *            A helper instance which provides data to the referenced document storage. The instance must be
     *            initialized with the same folderId, where the document itself is stored.
     */
    public OXDocumentImpl(final Session session, final UserData userData, boolean newDoc, final IResourceManager resourceManager, final StorageHelperService storageHelper, boolean logDocContext, final SaveDebugProperties dbgProps) {
        m_resourceManager = resourceManager;
        m_session = session;
        m_newDoc = newDoc;
        m_storageHelper = storageHelper;
        m_logDocContext = logDocContext;
        m_debugProperties = dbgProps;

        this.fileId = userData.getFileId();
        this.folderId = userData.getFolderId();
        this.encryptionInfo = userData.getEncryptionInfo();

//        impl_Init(userData.getFolderId(), userData.getFileId(), userData.getEncryptionInfo());
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param services
     * @param session
     * @param docStream
     * @param fileName
     * @param mimeType
     * @param resourceManager
     * @param storageHelper
     */
    public OXDocumentImpl(final Session session, final InputStream docStream, final String fileName, final String mimeType, final IResourceManager resourceManager, final StorageHelperService storageHelper) {
        m_resourceManager = resourceManager;
        m_session = session;
        m_newDoc = false;
        m_storageHelper = storageHelper;
        m_logDocContext = false;
        m_debugProperties = null;

        try {
            final byte[] documentBuffer = IOUtils.toByteArray(docStream);
            m_documentMetaData = new DocumentMetaData(fileName, mimeType, documentBuffer.length);
            m_isTemplateDoc = false;
            m_documentBuffer = documentBuffer;

//            impl_Init(documentBuffer, null);
        } catch (IOException e) {
            m_lastErrorCode = ErrorCode.LOADDOCUMENT_FAILED_ERROR;
            MDCHelper.safeMDCPut(MDCEntries.ERROR, m_lastErrorCode.getCodeAsStringConstant());
            LOG.error("OXDocumentImpl: unable to retrieve document stream, exception caught");
            MDC.remove(MDCEntries.ERROR);
        }
    }

    @Override
	public void afterPropertiesSet() throws Exception {
    	if (m_documentBuffer != null) {
    		impl_Init(m_documentBuffer, null);
    	} else {
    		impl_Init(folderId, fileId, encryptionInfo);
    	}
	}

	//-------------------------------------------------------------------------
    /**
     * Initializes the OXDocument instance for source mode file.
     *
     * @param folderId
     * @param fileId
     * @param fileHelper
     * @param encryptionInfo
     */
    private void impl_Init(final String folderId, final String fileId, String encryptionInfo) {

    	final DocFileService fileHelper = docFileServiceFactory.createInstance();
        final StreamInfo streamInfo = fileHelper.getDocumentStream(m_session, folderId, fileId, encryptionInfo);
        final InputStream documentStm = streamInfo.getDocumentStream();
        if (null != documentStm) {
            try {
                // See #40279 - it's possible to get the document stream, but not the meta data
                // In this case prevent that we have a document buffer - which will result in a
                // general error.
                m_metaData = streamInfo.getMetaData();
                if (null != m_metaData) {
                    // #49556 - We have to prevent to create any memory based stream for the
                    // filter if the file size exceeds a certain limit. Otherwise we can trigger
                    // OOM exceptions which must be prevented.
                    long nMaxDocumentFileSize = OXDocumentInfoImpl.MAX_DOCUMENT_FILE_SIZE * 1024L * 1024L;

                    nMaxDocumentFileSize = configurationService.getIntProperty("com.openexchange.office.maxDocumentFileSize", OXDocumentInfoImpl.MAX_DOCUMENT_FILE_SIZE) * 1024L * 1024L;

                    // Use a file size factor to calculate the memory needed
                    // to load the document. If the real size is unknown the
                    // function must copy buffers which needs much more memory.
                    long nFileSize = m_metaData.getFileSize();
                    if ((nFileSize > 0) && (nFileSize <= nMaxDocumentFileSize)) {
                        // We have to check the current heap space consumption although the document
                        // doesn't exceed the document file size limit.
                        final MemoryObserver aMemObserver = MemoryObserver.get();
                        final float fCurrentFileSizeFactor = StringUtils.isEmpty(encryptionInfo) ? 1.0f : PGP_FILE_SIZE_FACTOR;
                        final long estimatedMemoryNeeded = (Math.round(nFileSize * fCurrentFileSizeFactor));
                        if (aMemObserver.willHeapLimitBeReached(estimatedMemoryNeeded)) {
                            // Not safe to load document due to restricted free heap. Set error and
                            // provide it to the caller.
                            aMemObserver.incNumOfDocsNotLoadedDueToMemPressure();
                            streamInfo.setErrorCode(ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR);
                            final MemoryInfo memInfo = SystemInfoHelper.getMemoryInfo();
                            LOG.warn("RT connection: Heap memory to low to read document, max free heap {}, doc size {}, estimated free heap during loading {}", memInfo.freeHeapSize, nFileSize, estimatedMemoryNeeded);
                        } else {
                            // Safe to load to document as we have enough free heap.
                            // Create byte array with stream content using toByteArray(stream, length) to
                            // prevent a huge memory overhead of toByteArray(stream), see #49556
                            if (StringUtils.isEmpty(encryptionInfo)) {
                                // no encryption and we can use the file size to use the correct buffer size
                                m_documentBuffer = IOUtils.toByteArray(documentStm, nFileSize);
                            } else {
                                // unknown stream size as decryption is done transparently
                                m_documentBuffer = IOUtils.toByteArray(documentStm);
                            }
                            m_documentFormat = fileHelper.getDocumentFormat();
                            m_documentMetaData = new DocumentMetaData(streamInfo);
                            m_documentMetaData.setWriteProtectedState(streamInfo.isWriteProtected());
                            m_isTemplateDoc = DocumentFormatHelper.isSupportedTemplateFormat(m_documentMetaData.getMimeType(), FileHelper.getExtension(m_documentMetaData.getFileName()));
                        }
                    } else if (nFileSize > 0) {
                        // set correct error code to streamInfo
                        streamInfo.setErrorCode(ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR);
                        LOG.warn("RT connection: Complexitity of document is to high for file size {} MB, doc size: {} MB!", nMaxDocumentFileSize, (nFileSize / (1024L * 1024L)));
                    } else if (nFileSize < 0) {
                        // Ox Guard returns a negative size using it's IDBasedFileAccess implementation. It doesn't know the
                        // size of the decrypted stream which it provides transparently. We try to determine the encrypted size
                        // using the default IDBasedFileAccess implementation. If this fails we have to provide this general
                        // error.
                        // 49875 In case we fail to write content to the document the size is zero. Therefore
                        // we have to separate the error for size 0 and size < 0 here. Otherwise the error message
                        // on the client is misleading.
                        streamInfo.setErrorCode(ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR);
                        LOG.warn("RT connection: Document file size cannot be retrieved. To protect the system loading of the document was prevented!");
                    } else {
                        streamInfo.setErrorCode(ErrorCode.LOADDOCUMENT_EMPTY_DOCUMENT_FILE_ERROR);
                        LOG.warn("RT connection: Document file id " + fileId + " size is zero - there is no document content!");
                    }
                } else {
                    LOG.warn("RT connection: Unable to retrieve the document's meta data - must give up to open the file");
                }
            } catch (IOException e) {
                Throwable cause = e.getCause();
                if ((cause != null) && (OXException.class.isInstance(cause))) {
                    streamInfo.setErrorCode(ExceptionToErrorCode.map((OXException)cause, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false));
                }
                LOG.error("RT connection: unable to retrieve document stream, exception caught", e);
            } finally {
                IOHelper.closeQuietly(documentStm);
            }
        }

        impl_Init(m_documentBuffer, streamInfo);
    }

    //-------------------------------------------------------------------------
    /**
     * Second initializer which works on the document buffer.
     *
     * @param documentBuffer
     * @param streamInfo
     */
    private void impl_Init(final byte[] documentBuffer, final StreamInfo streamInfo) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        m_documentBuffer = documentBuffer;

        if (null != documentBuffer) {
            final ZipInputStream zipInputStm = new ZipInputStream(new ByteArrayInputStream(m_documentBuffer));
            ZipEntry entry = null;
            byte[] operationsBuffer = null;
            byte[] extendedDataBuffer = null;
            byte[] docBuffer = null;

            try {
                // create a dummy resource manager, if no one is given
                if (null == m_resourceManager) {
                    m_resourceManager = resourceManagerFactory.createInstance();
                }

                while ((entry = zipInputStm.getNextEntry()) != null) {
                    final String entryName = entry.getName();

                    if (entryName.equals(DocFileHelper.OX_DOCUMENT_EXTENDEDDATA)) {
                        try {
                            // read document identifier
                            extendedDataBuffer = IOUtils.toByteArray(zipInputStm);
                        } catch (IOException e) {
                            LOG.error("RT connection: unable to retrieve extended data stream, exception caught", e);
                        }
                    } else if (entryName.indexOf(DocFileHelper.OX_RESCUEDOCUMENT_DOCUMENTSTREAM_NAME) == 0) {
                        try {
                            // read original document
                            m_isRescueDoc = true;
                            docBuffer = IOUtils.toByteArray(zipInputStm);
                        } catch (IOException e) {
                            LOG.error("RT connection: unable to retrieve original document stream, exception caught", e);
                        }
                    } else if (entryName.indexOf(DocFileHelper.OX_RESCUEDOCUMENT_OPERATIONSSTREAM_NAME) == 0) {
                        try {
                            // read original document
                            final String op = IOUtils.toString(zipInputStm, Charset.forName("UTF-8"));
                            // non continuous osn numbers lead to a load error in the frontend, so we have to remove them as some
                            // operations are filtered by the calcengine
                            operationsBuffer = op.replaceAll("\\\"osn\\\":[0-9]+,?", "").getBytes();
                            m_isRescueDoc = true;
                        } catch (IOException e) {
                            LOG.error("RT connection: unable to retrieve operations stream, exception caught", e);
                        }
                    } else if (entryName.startsWith(DocFileHelper.OX_RESCUEDOCUMENT_RESOURCESTREAM_NAME_PREFIX)) {
                        // read resource and put into the resource manager, if not already contained
                        final String resourceName = entryName.substring(DocFileHelper.OX_RESCUEDOCUMENT_RESOURCESTREAM_NAME_PREFIX.length());

                        if (!m_resourceManager.containsResource(Resource.getUIDFromResourceName(resourceName))) {
                            byte[] resourceBuffer = null;

                            try {
                                resourceBuffer = IOUtils.toByteArray(zipInputStm);
                            } catch (IOException e) {
                                LOG.error("RT connection: unable to retrieve resource document stream, exception caught", e);
                            }

                            m_resourceManager.addResource(resourceBuffer);
                        }
                    }

                    // 61762: We ignore ZipException if not fatal to let the filter
                    // decide, if we can load the document or not
                    try {
                        zipInputStm.closeEntry();
                    } catch (ZipException e) {
                        if (ZipExceptionHandler.isFatalZipError(e)) {
                            throw e;
                        } else if (ZipExceptionHandler.isCRCError(e)) {
                            LOG.info("RT connection: Exception caught trying to close entry - CRC error in document detected - filter should be able to correct it automatically!", e);
                        } else {
                            errorCode = ErrorCode.LOADODCUMENT_DOCUMENT_DAMAGED_WARNING;
                            LOG.warn("RT connection: Exception caught trying to close entry - parts of the document can be corrupt!", e);
                        }
                    }
                }
            } catch (IOException e) {
                if (e instanceof java.util.zip.ZipException && e.getMessage().contains("only DEFLATED entries can have EXT descriptor")) {
                    errorCode = ErrorCode.LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR;
                } else {
                    errorCode = ErrorCode.GENERAL_FILE_CORRUPT_ERROR;
                    LOG.error("RT connection: IOException caught while reading the document stream", e);
                }
            } catch (Exception e) {
                errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                LOG.error("RT connection: Exception caught while reading the document stream", e);
            } finally {
                IOHelper.closeQuietly(zipInputStm);
                try {
                    if (null != streamInfo) {
                        streamInfo.close();
                    }
                } catch (IOException io) {
                    LOG.warn("Could not correctly close IDBasedFileAccess instance", io);
                }

                // set content of original document
                if (null != docBuffer) {
                    m_documentBuffer = docBuffer;
                }
                if (null != extendedDataBuffer) {
                    try {
                        m_uniqueDocumentId = new JSONObject(new String(extendedDataBuffer));
                    } catch (JSONException e) {
                        LOG.error("RT connection: unable to setup json object for unique document identifier", e);
                    }
                }

                // set content of already available operations
                if (null != operationsBuffer) {
                    m_documentOperations = parseOperationStringFromRescueDocument(operationsBuffer);
                    if (null == m_documentOperations) {
                        LOG.error("RT connection: unable to setup json object for document operations");
                    }
                }
            }
        } else {
            try {
                if (null != streamInfo) {
                    streamInfo.close();
                }
            } catch (IOException e) {
                LOG.warn("Could not correctly close IDBasedFileAccess instance", e);
            } finally {
                if (null == m_documentBuffer) {
                    m_lastErrorCode = ErrorCode.LOADDOCUMENT_FAILED_ERROR;
                }
            }
        }

        if (null == m_documentBuffer) {
            if (null != streamInfo) {
                // set last error code using the stream info if accessuible from the file helper getDocumentStream()
                m_lastErrorCode = streamInfo.getErrorCode();
            }
            if (m_lastErrorCode.isNoError()) {
                m_lastErrorCode = ErrorCode.LOADDOCUMENT_FAILED_ERROR;
            }
            m_lastErrorCode = ErrorCode.setErrorClass(m_lastErrorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
            if (isErrorDueToComplexityOrMemory(m_lastErrorCode)) {
            	LOG.warn("OXDocument Ctor: Could not read document content due to file size or current memory consumption level.");
            } else {
            	LOG.warn("OXDocument Ctor: could not retrieve a valid document buffer/meta data from given document file.");
            }
        } else {
            m_lastErrorCode = errorCode;
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Determines if the detected error is related to our document size limit or
     * that the current memory consumption prevents loading the document.
     *
     * @param errCode
     * @return TRUE if the error is related to document size/memory, otherwise it's
     * FALSE.
     */
    private boolean isErrorDueToComplexityOrMemory(final ErrorCode errCode) {
        final int code = errCode.getCode();

        boolean result = false;
        switch (code) {
        case ErrorCode.CODE_GENERAL_MEMORY_TOO_LOW_ERROR:
        case ErrorCode.CODE_GENERAL_SYSTEM_BUSY_ERROR:
        case ErrorCode.CODE_LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR: result = true; break;
        default: result = false; break;
        }

        return result;
    }

    //-------------------------------------------------------------------------
    /**
     * Checks that the last known version/osn of document content is consistent
     * with the loaded document.
     *
     * @param version the last known version of the document content
     * @param osn the last known osn of the document content
     * @return TRUE
     * @throws IllegalStateException if no document stream could be read. Therefore
     *             the method should only be called, if error code is NO_ERROR.
     */
    @Override
    public boolean isConsistWithVersionAndOSN(final String version, int osn) throws Exception {
        if (m_lastErrorCode.isNoError()) {
            final String docVersion = m_documentMetaData.getVersion();
            final int docOSN = getUniqueDocumentId();

            // a consistent document must have the same version and the osn is
            // always higher or equal than the document osn
            boolean versionOk = (version == null) ? (version == docVersion) : (version.equals(docVersion));
            boolean consistent = (versionOk && (docOSN <= osn));
            LOG.debug("RT connection: Consistency check between document file and last known version. Consistency result: {} with last known version: {} osn: {} and document version: {}, osn: {}", consistent, version, osn, docVersion, docOSN);

            return consistent;
        } else {
            throw new IllegalStateException("Cannot determine consistent state, if document could not be read successfully!");
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieves the operations from the document and optional additional operations.
     *
     * @param session The session of the client requesting the document operations.
     * @param importer The importer instance to be used to read the document stream and provide the operations.
     * @param appendOperations Optional operations which should be appended to the document operations. Normally used if there are queued
     *            operations which are not part of the last document stream.
     * @param previewData Optional preview data object which contains data to request a special preview data from the filter including a
     *            possible callback.
     * @return The JSONObject containing all operations from the document, the saved document operations and the given operations.
     * @throws FilterException in case the filter implementation is not able to provide the operations.
     */
    @Override
    public JSONObject getOperations(IImporter importer, final JSONObject appendOperations, boolean createFastLoadOperations) throws FilterException {
        JSONObject jsonResult = null;
        Session session = m_session;

        if ((null != session) && (null != m_documentBuffer) && (null != importer)) {
            final InputStream documentStm = new ByteArrayInputStream(m_documentBuffer);
            try {
                String userLangCode = getUserLangCode(session);
                long startTime = System.currentTimeMillis();

                DocumentProperties docProps = new DocumentProperties();
                docProps.put(DocumentProperties.PROP_USER_LANGUAGE, userLangCode);

                // request the document operations, preview data will be provided via callback
                jsonResult = importer.createOperations(session, documentStm, docProps, createFastLoadOperations);

                String uniqueDocumentId = docProps.optString(DocumentProperties.PROP_UNIQUE_DOCUMENT_IDENTIFIER, null);
                if (null != uniqueDocumentId) {
                    try {
                        this.setUniqueDocumentId(Integer.parseInt(uniqueDocumentId));
                    } catch (NumberFormatException e) {
                        // nothing to do as the unique document identifier
                        // is only an optional value
                    }
                }
                // set the optional active sheet index (in case this is not a
                // spreadsheet document).
                m_activeSheetIndex = docProps.optInteger(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX, 0);
                m_sheetCount = docProps.optInteger(DocumentProperties.PROP_SPREADSHEET_SHEET_COUNT, 0);

                LOG.debug("RT connection: TIME getOperations: {} ms", (System.currentTimeMillis() - startTime));
            } finally {
                IOHelper.closeQuietly(documentStm);
            }
        }

        // getting operations from the original document is mandatory
        if (null != jsonResult) {
            // append possible operations from the rescue document
            if (null != m_documentOperations) {
                OperationHelper.appendJSON(jsonResult, m_documentOperations);
            }

            // append given operations
            if (null != appendOperations) {
                OperationHelper.appendJSON(jsonResult, appendOperations);
            }
        }

        return jsonResult;
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @return
     */
    @Override
    public final DocumentMetaData getDocumentMetaData() {
        return m_documentMetaData;
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieves the last error code set by the latest method.
     *
     * @return The last error code set by the last method called.
     */
    @Override
    public ErrorCode getLastError() {
        return m_lastErrorCode;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the session used to load/save the document stream.
     *
     * @return
     *         the session of the client requested to load/save the document stream or
     *         null, if there was no session
     */
    @Override
    public Session getSession() {
        return m_session;
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieves the resource manager used by this document.
     *
     * @return the ResourceManager used to read the document
     */
    IResourceManager getResourceManager() {
        return m_resourceManager;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the unique document id stored in the document.
     *
     * @return Returns the unique document id stored in the document or -1 if no id is available.
     */
    @Override
    public int getUniqueDocumentId() {
        int id = DEFAULT_DOCUMENT_ID;

        if (null != m_uniqueDocumentId) {
            id = m_uniqueDocumentId.optInt("uniqueDocumentIdentifier", DEFAULT_DOCUMENT_ID);
        }

        return id;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the active sheet index.
     *
     * @return The active sheet index or 0 if the index is unknown (in case of a non- spreadsheet document) or cannot be retrieved.
     */
    public int getActiveSheetIndex() {
        return m_activeSheetIndex;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the sheet count.
     *
     * @return The sheet count or 0 if the count is unknown (in case of a non- spreadsheet document) or cannot be retrieved.
     */
    public int getSheetCount() {
        return m_sheetCount;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the folder id of the document file used by this document instance.
     *
     * @return The folder id or null if no document file is associated with this document instance.
     */
    @Override
    public String getFolderId() {
        return (m_metaData != null) ? m_metaData.getFolderId() : null;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the file id of the document file used by this document instance.
     *
     * @return The file id or null if no document file is associated with this document instance.
     */
    @Override
    public String getFileId() {
        return (m_metaData != null) ? m_metaData.getId() : null;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the version of the document at the time this instance was
     * created.
     *
     * @return
     *         The version of the document file, can be null.
     */
    @Override
    public String getVersion() {
        return (m_metaData != null) ? m_metaData.getVersion() : null;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the meta data of the document file at the time this
     * instance was created.
     *
     * @return
     *         The meta data of the document file.
     */
    @Override
    public File getMetaData() {
        return m_metaData;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the storage helper used by this instance.
     *
     * @return
     *         The storage helper instance of the document file.
     */
    @Override
    public StorageHelperService getStorageHelper() {
        return m_storageHelper;
    }

    //-------------------------------------------------------------------------
    /**
     * Sets a new unique document id to be stored in the document on the next successful flushDocument.
     *
     * @param uniqueId The new unique document id to be stored.
     */
    public void setUniqueDocumentId(int uniqueId) {
        if (null == m_uniqueDocumentId) {
            m_uniqueDocumentId = new JSONObject();
        }

        try {
            m_uniqueDocumentId.put("uniqueDocumentIdentifier", uniqueId);
        } catch (JSONException e) {
            // this is not a fatal error as we can work without this unique
            // id. just the speed up of the local cache won't work
            LOG.error("RT connection: Couldn't set unique document id to JSONObject", e);
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the document format of the document.
     *
     * @return The document format or null if the document instance was not initialized with a valid & existing file & folder id.
     */
    @Override
    public DocumentFormat getDocumentFormat() {
        return m_documentFormat;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the document type of the document.
     *
     * @return The document type or null if the document instance was not initialized with a valid & existing file & folder id.
     */
    @Override
    public DocumentType getDocumentType() {
        DocumentType type = DocumentType.NONE;
        final DocumentFormat format = getDocumentFormat();
        switch (format) {
        case DOCX:
        case ODT : type = DocumentType.TEXT; break;

        case XLSX:
        case ODS : type = DocumentType.SPREADSHEET; break;

        case PPTX:
        case ODP : type = DocumentType.PRESENTATION; break;

        case NONE:
        default  : type = DocumentType.NONE; break;
        }

        return type;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the document buffer containing the stream data as byte array.
     *
     * @return The docment stream data as byte array or null, if not available.
     */
    @Override
    public byte[] getDocumentBuffer() {
        return m_documentBuffer;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides possible rescue document operations which caused a filter exception.
     *
     * @return
     *         the JSONObject containing the rescue operations or null if there are no
     *         rescue operations.
     */
    @Override
    public JSONArray getRescueOperations() {
        JSONArray result = null;

        if (null != m_documentOperations) {
            result = m_documentOperations.optJSONArray(MessagePropertyKey.KEY_OPERATIONS);
        }

        return result;
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the rescue state of this document.
     *
     * @return
     *         TRUE if the document is a rescue document, otherwise FALSE.
     */
    @Override
    public boolean isRescueDocument() {
        return m_isRescueDoc;
    }

    //-------------------------------------------------------------------------
	@Override
	public boolean isTemplateDocument() {
		return m_isTemplateDoc;
	}

    //-------------------------------------------------------------------------
	@Override
	public boolean isNewDocument() {
		return m_newDoc;
	}

    //-------------------------------------------------------------------------
    /**
     * Initalizes a document properties object with properties required for saving
     * a document.
     *
     * @param session the session of the client requested to save the document
     * @param version the version to be used for saving the document
     * @param saveAsTemplate TRUE if the document should be saved as template, otherwise FALSE
     * @param dirty TRUE if the export filter should set the dirty flag to ensure recalculating formulars
     *            on loading.
     * @return a document properties instance with initialized properties
     * @throws Exception
     */
    private DocumentProperties setupDocumentPropertiesForSave(final Session session, final String version, boolean saveAsTemplate, boolean dirty) throws Exception {
        String userLangCode = "en-US";
        String userName = "";
        try {
        	final User user = userService.getUser(session.getUserId(),  session.getContextId());
            userLangCode = UserHelper.mapUserLanguageToLangCode(user.getPreferredLanguage());
            userName = user.getDisplayName();
        } catch (OXException ex) {
        	// use defaults
        }
        DocumentProperties docProps = new DocumentProperties();
        docProps.put(DocumentProperties.PROP_USER_LANGUAGE, userLangCode);
        docProps.put(DocumentProperties.PROP_LAST_MODIFIED_BY, userName);
        docProps.put(DocumentProperties.PROP_SAVE_TEMPLATE_DOCUMENT, saveAsTemplate);
        docProps.put(DocumentProperties.PROP_CALCULATE_ON_LOAD, dirty);
        docProps.put(DocumentProperties.PROP_SAVE_DOC_HISTORY, (m_debugProperties != null) ? m_debugProperties.saveDocHistory(false) : false);
        LOG.debug("RT connection: DocumentProperties used for filter = {}", docProps.toString());

        int documentOSN = this.getUniqueDocumentId();
        if (documentOSN != DEFAULT_DOCUMENT_ID) {
            docProps.put(DocumentProperties.PROP_UNIQUE_DOCUMENT_IDENTIFIER, String.valueOf(documentOSN));
        }
        if (null != version) {
            // only use a valid version string
            docProps.put(DocumentProperties.PROP_VERSION, version);
        }

        return docProps;
    }

    //-------------------------------------------------------------------------
    /**
     * Create a combined operations array with the document operations
     * object and pending operations array.
     *
     * @param docOpsObject a operations object with a property "operations" with the operations array
     * @param operations pending operations to be combined with the document operations
     * @return An array with the combined document and pending operations. Can be empty if none
     *         of the provided arguments contains operations.
     */
    private final JSONArray createCombinedOperations(final JSONObject docOpsObject, final JSONArray operations) {
        final JSONArray combinedOperations = new JSONArray();
        // get combined operations
        // append possible operations from the rescue document
        if ((null != docOpsObject) && (docOpsObject.has(PROP_OPERATIONS))) {
            try {
                final JSONArray docOperations = docOpsObject.getJSONArray(PROP_OPERATIONS);
                JSONHelper.appendArray(combinedOperations, docOperations);
            } catch (JSONException e) {
                LOG.error("RT connection: Couldn't append document operations to array.", e);
            }
        }

        // apply collected operations
        if ((null != operations) && !operations.isEmpty()) {
            try {
                JSONHelper.appendArray(combinedOperations, operations);
            } catch (JSONException e) {
                LOG.error("RT connection: Couldn't append pending operations to array.", e);
            }
        }

        return combinedOperations;
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param e
     * @param combinedOps
     * @return
     */
    private FilterInfo evalFilterExceptionForRetry(final FilterException e, int combinedOps) {
        ErrorCode errorCode;
        boolean retry = false;
        int opsCanBeApplied = 0;

        final String userId = getUserIdWithContextFromSession(m_session);
        final String docUserData = DocContextLogger.getContextStringWithDoc(m_logDocContext, userId, this.getFolderId(), this.getFileId(), this.m_documentMetaData.getFileName());
        final Level level = FilterExceptionToErrorCode.determineLogLevel(e);

        if (e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED) {
            FilterExceptionToErrorCode.log(LOG, level, "Document export filter run out of memory while exporting the document. ", docUserData, e);
            errorCode = ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR;
            // setErrorClass provides a clone with changed error class!
            errorCode = ErrorCode.setErrorClass(errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
        } else {
            FilterExceptionToErrorCode.log(LOG, level, "Document export filter threw a filter exception while exporting the document.", docUserData, e);

            // set retry count, if some (but not all) operations could be applied to the original document
            int opsAppliedSuccessful = e.getOperationCount();
            if ((opsAppliedSuccessful >= 0) && (opsAppliedSuccessful < combinedOps)) {
                retry = true;
                errorCode = ErrorCode.SAVEDOCUMENT_PARTIAL_CHANGES_SAVED_ERROR;
                opsCanBeApplied = opsAppliedSuccessful;
            } else {
                errorCode = ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR;
            }
            // setErrorClass provides a clone with changed error class!
            errorCode = ErrorCode.setErrorClass(errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
        }

        return new FilterInfo(errorCode, retry, opsCanBeApplied);
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param operations
     * @return
     */
    private final String getOperationsString(final JSONArray operations) {
        return operations.toString();
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param operations
     * @param goodOpsCount
     * @return
     */
    private final String getGoodOperationsString(final JSONArray operations, int goodOpsCount) {
        String opsString = null;

        if ((null != operations) && !operations.isEmpty() && goodOpsCount > 0) {
            try {
                final JSONArray goodOperations = JSONHelper.subArray(operations, 0, goodOpsCount);
                opsString = goodOperations.toString();
            } catch (JSONException e) {
                LOG.error("RT connection: Couldn't retrieve the good operations from the operations array.", e);
            }
        }

        return opsString;
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param exporter
     * @param resourceManager
     * @param messageChunkList
     * @param version
     * @param finalSave
     * @param saveAsTemplate
     * @return
     */
    private DocumentStreamInfo createDocumentStream(final IExporter exporter, final IResourceManager resourceManager, final JSONArray operations, final String version, boolean finalSave, boolean saveAsTemplate, boolean dirty) {
        final int MAX_TRY_COUNT = 2;
        final Session session = m_session;
        final boolean isRescueDoc = this.isRescueDocument();
        final boolean hasOperations = (operations != null) && (!operations.isEmpty());
        final JSONArray combinedOperations = isRescueDoc ? operations : createCombinedOperations(m_documentOperations, operations);

        int opsApplied = 0;
        JSONArray badOperations = null;
        ByteArrayInputStream currDocStream = (null != m_documentBuffer) ? new ByteArrayInputStream(m_documentBuffer) : null;
        InputStream resolvedDocumentStream = null;
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        // if there are any operations, try to export them with the document exporter first
        if ((null != currDocStream) && (!combinedOperations.isEmpty() || saveAsTemplate) && (null != exporter) && (null != resourceManager)) {
            InputStream combinedStm = null;
            boolean stop = false;
            int tryCount = hasOperations ? 0 : 1; // no retry, if we don't have operations

            do {
                try {
                    final DocumentProperties docProps = setupDocumentPropertiesForSave(session, version, saveAsTemplate, dirty);
                    final String mergeOperations = ((tryCount == 0) || isRescueDoc) ? getOperationsString(combinedOperations) : getGoodOperationsString(combinedOperations, opsApplied);

                    combinedStm = exporter.createDocument(session, currDocStream, mergeOperations, resourceManager, docProps, finalSave);
                    tryCount++;
                } catch (FilterException e) {
                    if (tryCount == 0) {
                        final int combinedOpsCount = combinedOperations.length();

                        FilterInfo filterInfo = this.evalFilterExceptionForRetry(e, combinedOpsCount);
                        if (filterInfo.retry) {
                            tryCount += 1;
                            opsApplied = filterInfo.opsCanBeApplied;
                            currDocStream.reset(); // reset stream to start save again
                        } else {
                            // no retry -> bail out
                            tryCount = MAX_TRY_COUNT;
                            stop = true;
                        }

                        errorCode = filterInfo.errorCode;
                    } else {
                        LOG.error("Document export filter threw an exception while exporting the document", e);
                        // exception on second try is always a fatal error
                        errorCode = ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR;
                        // setErrorClass provides a clone with changed error class!
                        errorCode = ErrorCode.setErrorClass(errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                        opsApplied = 0;
                        stop = true;
                    }
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                    LOG.error("Document export filter threw an exception while exporting the document", e);
                    errorCode = ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR;
                    // setErrorClass provides a clone with changed error class!
                    errorCode = ErrorCode.setErrorClass(errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                    stop = true;
                }
            } while ((combinedStm == null) && (tryCount < MAX_TRY_COUNT) && !stop);

            IOHelper.closeQuietly(resolvedDocumentStream);

            if (null != (resolvedDocumentStream = combinedStm)) {
                if (tryCount == 1) {
                    // set to no error, if first try was successful
                    errorCode = ErrorCode.NO_ERROR;
                } else {
                    badOperations = combinedOperations;
                }
            } else {
                badOperations = combinedOperations;
            }
        } else if ((null != currDocStream) && combinedOperations.isEmpty()) {
            // short-cut (no changes and provide doc stream), we just have to
            // provide the original document stream
            resolvedDocumentStream = currDocStream;
        } else if (!combinedOperations.isEmpty()) {
            errorCode = ErrorCode.SAVEDOCUMENT_FAILED_NOBACKUP_ERROR;

            if (null == exporter) {
                errorCode = ErrorCode.SAVEDOCUMENT_FAILED_NO_FILTER_ERROR;
            } else if (null == resourceManager) {
                errorCode = ErrorCode.SAVEDOCUMENT_FAILED_NO_RESOURCEMANAGER_ERROR;
            } else if (null == currDocStream) {
                errorCode = ErrorCode.SAVEDOCUMENT_FAILED_NO_DOCUMENTSTREAM_ERROR;
            }
        }

        return new DocumentStreamInfo(errorCode, resolvedDocumentStream, badOperations);
    }

    //-------------------------------------------------------------------------
    /**
     * Provides the new document stream which uses the source document stream and the
     * operations to create a new document stream.
     *
     * @param exporter
     * @param resourceManager
     * @param actions
     * @param finalSave
     * @param saveAsTemplate
     * @return
     */
    @Override
    public ResolvedStreamInfo getResolvedDocumentStream(final IExporter exporter, final IResourceManager resourceManager, final JSONArray operations, final String version, boolean finalSave, boolean saveAsTemplate, boolean dirty) {
        InputStream resolvedDocumentStream = null;
        InputStream debugDocumentStream = null;

        // set error code to no error
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        m_lastErrorCode = errorCode;

        // create document stream -
        long startTime = System.currentTimeMillis();
        DocumentStreamInfo docStreamInfo = createDocumentStream(exporter, resourceManager, operations, version, finalSave, saveAsTemplate, dirty);
        LOG.trace("RT connection: TIME createDocument: {} ms", System.currentTimeMillis() - startTime);

        errorCode = docStreamInfo.errorCode;
        resolvedDocumentStream = docStreamInfo.docStream;
        //JSONArray badOperations = docStreamInfo.badOps;

        /*
         * { // !!! DEBUG code to create OX rescue documents by setting the document name to the !!! // !!! name of the env var
         * 'OX_RESCUEDOCUMENT_NAME'; may be commented for final release!!! final String oxRescueDocumentName_debug =
         * System.getenv("OX_RESCUEDOCUMENT_NAME"); if ((null != oxRescueDocumentName_debug) && (null != m_fileHelper)) { try { final String
         * filename = m_fileHelper.getFilenameFromFile(session); if ((null != filename) && filename.startsWith(oxRescueDocumentName_debug))
         * { errorCode = ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR; } } catch (OXException e) {
         * LOG.error("Could not retrieve file name from file for saving OX rescue document"); } } }
         */
        m_lastErrorCode = errorCode;

        // if the there was an error before, we write a zipped OXDocument with
        // the original document and the operations content in separate streams
        if (errorCode != ErrorCode.NO_ERROR) {
            InputStream inputStm = null;
            final ByteArrayOutputStream byteArrayStm = new ByteArrayOutputStream(8192);
            final ZipOutputStream zipOutputStm = new ZipOutputStream(byteArrayStm);

            // set error code to worst case, if we don't have a complete/good document stream
            if (null == resolvedDocumentStream) {
                errorCode = ErrorCode.SAVEDOCUMENT_FAILED_NOBACKUP_ERROR;
            }

            // write original document as well as collected operations
            // and collected resources into different zip entry streams
            try {
                // write original document stream
                if (null != m_documentBuffer) {
                    inputStm = new ByteArrayInputStream(m_documentBuffer);

                    try {
                        zipOutputStm.putNextEntry(new ZipEntry(DocFileHelper.OX_RESCUEDOCUMENT_DOCUMENTSTREAM_NAME));
                        IOUtils.copy(inputStm, zipOutputStm);
                        zipOutputStm.flush();
                    } catch (IOException e) {
                        LOG.error("Exception while writing original document stream to backup document", e);
                    } finally {
                        zipOutputStm.closeEntry();
                        IOHelper.closeQuietly(inputStm);
                        inputStm = null;
                    }

                }

                // Write collected operations, that were added by editing users, into
                // separate zip entry stream. Make sure that we write the operations
                // even if badOperations is not filled by createDocumentStream. Use
                // the complete operations contained in message chunk list. See #40036
                //final JSONArray allOperations = ((null != badOperations) && !badOperations.isEmpty()) ? badOperations : createCombinedOperations(m_documentOperations, operations);
                if ((null != operations) && !operations.isEmpty()) {
                    inputStm = new ByteArrayInputStream(operations.toString().getBytes("UTF-8"));

                    try {
                        zipOutputStm.putNextEntry(new ZipEntry(DocFileHelper.OX_RESCUEDOCUMENT_OPERATIONSSTREAM_NAME));
                        IOUtils.copy(inputStm, zipOutputStm);
                        zipOutputStm.flush();
                    } catch (IOException e) {
                        LOG.error("Exception while writing operations stream to backup document", e);
                    } finally {
                        zipOutputStm.closeEntry();
                        IOHelper.closeQuietly(inputStm);
                        inputStm = null;
                    }

                }

                // write collected resources, that were added by
                // editing users, into separate zip entry streams
                if (null != resourceManager) {
                    final Map<String, byte[]> resourceBuffers = resourceManager.getByteArrayTable();

                    for (final String curKey : resourceBuffers.keySet()) {
                        final byte[] curResourceBuffer = resourceBuffers.get(curKey);

                        if (null != curResourceBuffer) {
                            final String curResourceName = DocFileHelper.OX_RESCUEDOCUMENT_RESOURCESTREAM_NAME_PREFIX + Resource.getResourceNameFromUID(curKey);

                            inputStm = new ByteArrayInputStream(curResourceBuffer);

                            try {
                                zipOutputStm.putNextEntry(new ZipEntry(curResourceName));
                                IOUtils.copy(inputStm, zipOutputStm);
                                zipOutputStm.flush();
                            } catch (IOException e) {
                                LOG.error("Exception while writing collected resources to backup document", e);
                            } finally {
                                zipOutputStm.closeEntry();
                                IOHelper.closeQuietly(inputStm);
                                inputStm = null;
                            }
                        }
                    }
                }

                zipOutputStm.flush();
            } catch (IOException e) {
                LOG.error("Exception while writing backup document", e);
            } finally {
                IOHelper.closeQuietly(zipOutputStm);
                final byte[] byteArray = byteArrayStm.toByteArray();

                if (byteArray.length > 0) {
                    // we have a debug stream -> set error code accordingly
                    debugDocumentStream = new ByteArrayInputStream(byteArray);
                    errorCode = ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR;
                }
            }
        }

        // don't overwrite the memory error code
        if (m_lastErrorCode.getCode() != ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR.getCode()) {
            // set the final error code and create the result object
            m_lastErrorCode = errorCode;
        }
        LOG.trace("RT connection: TIME getResolvedDocument: {} ms", System.currentTimeMillis() - startTime);

        return new ResolvedStreamInfo(resolvedDocumentStream, m_lastErrorCode, debugDocumentStream);
    }

    //-------------------------------------------------------------------------
    /**
     * Saves the new document to the storage.
     *
     * @param exporter
     *            The exporter instance used to create the new document stream.
     * @param resourceManager
     *            The resource manager used by this document.
     * @param opsArray
     *            A JSONArray containing the pending operations to be stored in the new document.
     * @param revisionless
     *            Specifies if the new document should be saved as a new version or using the latest
     * @param finalSave
     *            Specifies if the new document stream should be handled as the final save. This can be used by the exporter to
     *            optimize the document, e.g. finally remove non-referenced resources.
     * @paran encrypt
     *            Specifies if the document must be stored encrypted or not
     * @param dirty
     *            Specifies if calculated document content is valid or dirty (means must be calculated on load).
     * @return
     *         The result of the save operation stored in the SaveResult instance. The different properties (error code, version)
     *         can be retrieved from the instance.
     */
    public SaveResult save(final IExporter exporter, final IResourceManager resourceManager, final JSONArray opsArray, boolean revisionless, boolean finalSave, boolean encrypt, boolean dirty) throws OXDocumentException {
        final SaveResult saveResult = new SaveResult(null, ErrorCode.NO_ERROR);
        String lastVersion = m_metaData.getVersion();
        String fileVersion = null;
        Session session = m_session;
        ErrorCode backupError = ErrorCode.NO_ERROR;
        IDBasedFileAccess fileAccess = null;
        ResolvedStreamInfo resolvedStreamInfo = null;
        long time1 = System.currentTimeMillis();

        try {
            final boolean storageSupportsVersions = m_storageHelper.supportsFileVersions();
            fileAccess = idBasedFileAccessFactory.createAccess(session);
            IDBasedFileAccess encryptFileAccess = null;

            if (encrypt) {
                try {

                    if ((encryptionAwareFileAccessFactory != null) && (encryptionAuthenticationFactory != null)) {
                        EnumSet<CryptographyMode> cryptMode = EnumSet.of(CryptographyMode.ENCRYPT);
                        encryptFileAccess = encryptionAwareFileAccessFactory.createAccess(fileAccess, cryptMode, session );
                    }
                } catch (final OXException e) {
                    LOG.error("OXDocument caught exception trying to access Guard file access!", e);
                }
            }

            // No versions support && create a new version && not a new document => we
            // have to create a backup file with the current doc content
            if (!storageSupportsVersions && !revisionless && !m_newDoc) {
                InputStream origDocStream = (null != m_documentBuffer) ? new ByteArrayInputStream(m_documentBuffer) : null;
                if (null != origDocStream) {
                    try {
                        final String folder_id = getFolderId();
                        final File currentFile = m_metaData;
                        final String fileName = currentFile.getFileName();

                        File foundBakFile = null;
                        if (!Strings.isEmpty(fileName)) {
                            // create backup file name to be used for possible encryption IDBasedFileAccess
                            final String backupFileName = FileHelper.createFilenameWithPostfix(fileName, BackupFileServiceImpl.BACKUP_FILE_POSTFIX, encrypt);
                            // search must be done using the final file name including a possible .pgp extension in case of encryption
                            final String searchBakFileName = (encrypt ? backupFileName + "." + FileHelper.STR_EXT_PGP : backupFileName);

                            if (!StringUtils.isEmpty(backupFileName)) {
                                // check file existence using the current file name
                                IDBasedFileAccess backupFileAccess = fileAccess;
                                if (encrypt && (encryptFileAccess != null)) {
                                    backupFileAccess = encryptFileAccess;
                                }

                            	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

                                if (null != backupFileAccess) {
                                    // Get meta data via original file access
                                    // Fix for 52592: We need to search with the original file (including possible pgp-extension), but
                                    // we MUST USE the file name for a new file (without pgp) extension.
                                    foundBakFile = fileHelperService.getMetaDataFromFileName(folder_id, searchBakFileName);
                                    if (foundBakFile == null) {
                                        // create a new BAK and use the meta data from the original file
                                        backupError = fileHelperService.createFileCopyWithStream(backupFileAccess, currentFile.getFolderId(), backupFileName, currentFile, origDocStream, true);
                                    } else {
                                        // overwrite existing backup file
                                        final File oldBackFile = foundBakFile;
                                        backupError = fileHelperService.writeStreamToFile(backupFileAccess, oldBackFile, origDocStream, currentFile);
                                    }
                                    // map all general error codes to the specific backup error codes
                                    backupError = ErrorCode2BackupErrorCode.mapToBackupErrorCode(backupError, ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR);
                                    if (!backupError.isError()) {
                                        // in case of success, set the backup file written flag
                                        saveResult.backupFileWritten = true;
                                    }
                                } else {
                                    backupError = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                                }
                            } else {
                                backupError = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                            }
                        } else {
                            backupError = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                        }
                    } catch (OXException e) {
                        LOG.warn("Exception caught while trying to store backup file", e);
                        backupError = ExceptionToBackupErrorCode.map(e, ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR, false);
                    } catch (Throwable e) {
                        ExceptionUtils.handleThrowable(e);
                        LOG.warn("Exception caught while trying to store backup file", e);
                        backupError = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                    } finally {
                        // close stream for the backup file
                        IOHelper.closeQuietly(origDocStream);
                    }
                } else {
                    backupError = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                }

                // In case we detect anything wrong, including warnings, set the
                // the error class correctly to warning.
                if (backupError.getCode() != ErrorCode.CODE_NO_ERROR) {
                    // setErrorClass provides a clone with changed error class!
                    backupError = ErrorCode.setErrorClass(backupError, ErrorCode.ERRORCLASS_WARNING);
                }
            }

            // Normal save, retrieve stream with latest actions applied. This includes
            // creating a rescue file, if saving failed due to filter problems.
            resolvedStreamInfo = getResolvedDocumentStream(exporter, resourceManager, opsArray, FileStorageFileAccess.CURRENT_VERSION, finalSave, m_isTemplateDoc, dirty);

            // debug helper code - to simulate a slow save due to long running filter code
            if ((null != m_debugProperties) && (m_debugProperties.slowSave(false) && (m_debugProperties.slowSaveUser(false)))) {
                int delay = m_debugProperties.slowSaveTime(0);
                try {
                	LOG.warn("RT connection: Attention slow save debug feature enabled, now waiting for {} seconds", (delay / 1000));
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            if (null != resolvedStreamInfo) {
            	final DocFileService docFileService = docFileServiceFactory.createInstance();

                final ErrorCode errorOnDocumentStream = resolvedStreamInfo.errorCode;
                ErrorCode writeDocErrorCode = ErrorCode.NO_ERROR;

                // Check, if we have a good document stream which we always want to store. This
                // can be just a part of the operations, but we are sure that this document can
                // be opened afterwards.
                if (null != resolvedStreamInfo.resolvedStream) {
                    // write the good operations part of the document
                    m_metaData.setVersionComment(DocumentConstants.OX_DOCUMENTS_CMNT);
                    final WriteInfo writeInfo = docFileService.writeDocumentStream(session, resolvedStreamInfo.resolvedStream, getFileId(), getFolderId(), m_metaData, m_storageHelper, Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX, false, revisionless, encrypt);
                    writeDocErrorCode = writeInfo.errorCode;
                    fileVersion = writeInfo.version;
                    if (writeDocErrorCode.isError()) {
                        // in case of an error set fatal error class
                        // setErrorClass provides a clone with changed error class!
                        writeDocErrorCode = ErrorCode.setErrorClass(writeDocErrorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                    } else {
                        // the document with the good parts should be activated in
                        // case of no write error
                        lastVersion = fileVersion;
                        writeDocErrorCode = errorOnDocumentStream;
                    }
                }

                // Check, if we have a bad document stream which we also want to store.
                if (null != resolvedStreamInfo.debugStream) {
                    boolean setActiveVersionToPrevious = false;
                    boolean errorOnCreatingResolvedStream = (errorOnDocumentStream != ErrorCode.NO_ERROR);
                    ErrorCode rescueError = ErrorCode.NO_ERROR;

                    if (errorOnCreatingResolvedStream) {
                        // ATTENTION: There is an error creating the document stream from
                        // the operations. We have to assume that the document cannot be
                        // opened afterwards. Therefore we have to protect the last known
                        // good version to not be overwritten by "revisionless" save!
                        // Therefore we set "revisionless" to false here, if the current
                        // storage supports versions.
                        revisionless = false;
                        setActiveVersionToPrevious = storageSupportsVersions;
                    }

                	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

                    if (storageSupportsVersions || !errorOnCreatingResolvedStream) {
                        // just write bad document as rescue document
                        final WriteInfo writeInfo = docFileService.writeDocumentStream(session, resolvedStreamInfo.debugStream, getFileId(), getFolderId(), m_metaData, m_storageHelper, Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX, true, revisionless, encrypt);
                        rescueError = writeInfo.errorCode;
                        fileVersion = writeInfo.version;
                        if (rescueError.isError()) {
                            // in case of an error set fatal error class
                            // setErrorClass provides a clone with changed error class!
                            rescueError = ErrorCode.setErrorClass(writeDocErrorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                        }
                    } else {
                        // Versions are not supported and we encountered an error
                        // creating the new document stream. Make sure that we
                        // store the content to a file with the same name and
                        // the _ox extension. If it exists, overwrite it.
                        final String folder_id = getFolderId();
                        final File currentFile = m_metaData;

                        File foundFile = null;
                        String rescueFileName = currentFile.getFileName();
                        if (!Strings.isEmpty(rescueFileName)) {
                            // we cannot just add the rescue extension - determine if we save with encryption or not (remove possible pgp extension)
                            rescueFileName = FileHelper.getFileName(rescueFileName, encrypt) + Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX;
                            // search must be done using the final file name including a possible .pgp extension in case of encryption
                            final String searchRescueFileName = (encrypt ? rescueFileName + "." + FileHelper.STR_EXT_PGP : rescueFileName);

                            // check file existence using file name
                            foundFile = fileHelperService.getMetaDataFromFileName(folder_id, searchRescueFileName);
                        }

                        IDBasedFileAccess rescueFileAccess = (encrypt ? encryptFileAccess : fileAccess);
                        if (rescueFileAccess != null) {
                            if (foundFile == null) {
                                // write new rescue file
                                rescueError = fileHelperService.createFileWithStream(rescueFileAccess, currentFile.getFolderId(), rescueFileName, currentFile.getFileMIMEType(), resolvedStreamInfo.debugStream);
                            } else {
                                // overwrite existing rescue file
                                final File oldRescueFile = foundFile;
                                rescueError = fileHelperService.writeStreamToFile(rescueFileAccess, oldRescueFile, resolvedStreamInfo.debugStream, null);
                            }
                        } else {
                            // no valid file access available to write rescue file
                            writeDocErrorCode = ErrorCode.SAVEDOCUMENT_FAILED_NOBACKUP_ERROR;
                        }

                        if (writeDocErrorCode.isNoError()) {
                            // set error code dependent on the fact that we wrote/didn't write the rescue file
                            writeDocErrorCode = (errorOnDocumentStream.isError()) ? errorOnDocumentStream : (rescueError.isError()) ? ErrorCode.SAVEDOCUMENT_FAILED_NOBACKUP_ERROR : ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR;
                        }
                        // setErrorClass provides a clone with changed error class!
                        writeDocErrorCode = ErrorCode.setErrorClass(writeDocErrorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                    }

                    if (writeDocErrorCode != ErrorCode.NO_ERROR) {
                        // error evaluation
                        saveResult.errorCode = writeDocErrorCode;
                        if (setActiveVersionToPrevious) {
                            // setErrorClass provides a clone with changed error class!
                            saveResult.errorCode = ErrorCode.setErrorClass(saveResult.errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
                        }
                        m_lastErrorCode = saveResult.errorCode;
                    }

                    if (setActiveVersionToPrevious) {
                        // Set the active version to last known good version as
                        // we detected a filter problem storing the latest version
                        // correctly.

                        // First set the error information correctly
                        saveResult.errorCode = resolvedStreamInfo.errorCode;
                        // setErrorClass provides a clone with changed error class!
                        saveResult.errorCode = ErrorCode.setErrorClass(saveResult.errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);

                        final String folderId = getFolderId();
                        final String fileId = getFileId();

                        if ((null != fileAccess) && (null != folderId) && (null != fileId)) {
                            // set the last known good version
                            try {
                                final DefaultFile metadata = new DefaultFile();

                                metadata.setId(fileId);
                                metadata.setFolderId(folderId);
                                metadata.setVersion(lastVersion);

                                // and save the meta data
                                fileAccess.saveFileMetadata(metadata, FileStorageFileAccess.DISTANT_FUTURE, Arrays.asList(UPDATE_METADATA_FIELDS));
                                fileAccess.commit();
                                fileVersion = lastVersion;
                            } catch (OXException e) {
                                // Catch exception and log error - we don't want to hide the "real" error
                                // that we were not able to store the document correctly.
                                LOG.error("Failed to activate the last good version for the broken document.", e);
                            }
                        }
                    }
                } else if (writeDocErrorCode != ErrorCode.NO_ERROR) {
                    // set save result with current write doc error
                    saveResult.errorCode = writeDocErrorCode;
                    m_lastErrorCode = saveResult.errorCode;
                }
            } else {
                // unknown information about creating the document stream - create
                // a generic error and set the fatal error class
                saveResult.errorCode = (null != resolvedStreamInfo) ? resolvedStreamInfo.errorCode : ErrorCode.SAVEDOCUMENT_FAILED_ERROR;
                // setErrorClass provides a clone with changed error class!
                saveResult.errorCode = ErrorCode.setErrorClass(saveResult.errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
            }
        } catch (Exception e) {
            LOG.error("Failed to write document stream on OXDocument.save().", e);
            saveResult.errorCode = ErrorCode.SAVEDOCUMENT_FAILED_ERROR;
            // setErrorClass provides a clone with changed error class!
            saveResult.errorCode = ErrorCode.setErrorClass(saveResult.errorCode, ErrorCode.ERRORCLASS_FATAL_ERROR);
        } finally {
            // close the input streams for the good and bad part of the latest changes
            IOHelper.closeQuietly((resolvedStreamInfo != null) ? resolvedStreamInfo.resolvedStream : null);
            IOHelper.closeQuietly((resolvedStreamInfo != null) ? resolvedStreamInfo.debugStream : null);
            finishFileAccessSafely(fileAccess);
        }

        // in case of no save error, we check the backup error state for errors/warnings
        if (!saveResult.errorCode.isError() && (backupError.isError() || backupError.isWarning())) {
            saveResult.errorCode = backupError;
        }

        saveResult.version = fileVersion;
        LOG.trace("RT connection: TIME document save: {} ms", (System.currentTimeMillis() - time1));
        return saveResult;
    }

    //-------------------------------------------------------------------------
    /**
     *
     * @param fileAccess
     */
    private void finishFileAccessSafely(IDBasedFileAccess fileAccess) {
        if (null != fileAccess) {
            try {
                fileAccess.finish();
            } catch (OXException e) {
                LOG.error("Failed to finish file access.", e);
            }
        }
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieve the user language code.
     *
     * @param session
     *            The user session.
     *
     * @return
     *         The language code of the user.
     */
    private String getUserLangCode(Session session) {
        String result = null;

        if (session instanceof ServerSession) {
            final User user = ((ServerSession) session).getUser();

            if (null != user) {
                result = UserHelper.mapUserLanguageToLangCode(user.getPreferredLanguage());
            }
        }

        return result;
    }

    //-------------------------------------------------------------------------
    /**
     * Retrieve the user id and context as string from a session. The method
     * should only be used for logging/output purpose.
     *
     * @param session the session to be used to retrieve the user id
     *
     * @return the user id as string or "unknown" if the user id could
     *         not be retrieved.
     */
    private final String getUserIdWithContextFromSession(final Session session) {
        String userIdWithContext = "unknown";
        final StringBuffer tmp = new StringBuffer(32);

        if (null != session) {
            try {
                tmp.append(session.getUserId());
                tmp.append("@");
                tmp.append(session.getContextId());
                userIdWithContext = tmp.toString();
            } catch (Exception e) {
                // nothing to do - this method provides the user id as string
                // for logging purpose
            }
        }

        return userIdWithContext;
    }

    //-------------------------------------------------------------------------
    /**
     * Parses the operation string retrieved from a rescue document. The
     * parser function provides the operations within a JSONObject. The
     * function accepts strings with the direct JSONArray with operations or
     * a JSONObject with a JSONArray property.
     *
     * @param operationsBuffer a operation string created from the operations stream
     *            from a rescue document.
     *
     * @return a JSONObject which contains a "operations" property that stores
     *         a JSONArray with the operations from the rescue document. Can be
     *         null if no operations could be parsed.
     */
    private final JSONObject parseOperationStringFromRescueDocument(final byte[] operationsBuffer) {
        JSONObject result = null;

        // decode operation buffer to string
        final String operationsString = new String(operationsBuffer);

        try {
            // Try to parse the string with json object as root
            result = new JSONObject(operationsString);
        } catch (JSONException e) {
            // nothing to do - we use try and error
        }

        if (null == result) {
            // parsing the string with json object as root failed
            // now trying to parse as json array
            try {
                final JSONArray tmp = new JSONArray(operationsString);
                result = new JSONObject();
                result.put(PROP_OPERATIONS, tmp);
            } catch (JSONException e) {
                // nothing to do - we use try and error
            }
        }

        return result;
    }

}
