/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.api.OXDocumentInfo;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.debug.SaveDebugProperties;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;

@Service
@RegisteredService(registeredClass=OXDocumentFactory.class)
public class OXDocumentFactoryImpl implements OXDocumentFactory, OsgiBundleContextAware {

	@Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;
	
	@Autowired
	private StorageHelperServiceFactory storageHelperServiceFactory;
	
	private OsgiBundleContextAndActivator bundleCtx;
	
	@Override
	public OXDocumentInfo createDocumentInfoAccess(Session session, String fileId, boolean newDoc) {		
		final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
		final StorageHelperService storageHelperService = storageHelperServiceFactory.create(session, folderHelperService.getFolderId(fileId));
		
		OXDocumentInfo res = new OXDocumentInfoImpl(session, fileId, newDoc, storageHelperService);
		bundleCtx.prepareObject(res);
		return res;
	}

	@Override
	public OXDocumentInfo createDocumentInfoAccess(Session session, UserData userData, boolean newDoc) {
		final StorageHelperService storageHelperService = storageHelperServiceFactory.create(session, userData.getFolderId());
		OXDocumentInfo res = new OXDocumentInfoImpl(session, userData.getFileId(), newDoc, storageHelperService);
		bundleCtx.prepareObject(res);
		return res;
	}

	@Override
	public OXDocument createDocumentAccess(Session session, String folderId, String fileId, boolean newDoc, IResourceManager resourceManager, boolean logDocProperties, SaveDebugProperties dbgProps) {
		final StorageHelperService storageHelperService = storageHelperServiceFactory.create(session, folderId);
		OXDocument res = new OXDocumentImpl(session, folderId, fileId, logDocProperties, resourceManager, storageHelperService, logDocProperties, dbgProps);
		bundleCtx.prepareObject(res);
		return res;
	}

	@Override
	public OXDocument createDocumentAccess(Session session, UserData userData, boolean newDoc, IResourceManager resourceManager, boolean logDocContext,	SaveDebugProperties dbgProps) {
		final StorageHelperService storageHelperService = storageHelperServiceFactory.create(session, userData.getFolderId());
		OXDocument res = new OXDocumentImpl(session, userData, logDocContext, resourceManager, storageHelperService, logDocContext, dbgProps);
		bundleCtx.prepareObject(res);
		return res;
	}

	@Override
	public OXDocument createDocumentAccess(Session session, InputStream docStream, String fileName, String mimeType,
			IResourceManager resourceManager, StorageHelperService storageHelper) {
		OXDocument res = new OXDocumentImpl(session, docStream, fileName, mimeType, resourceManager, storageHelper);
		bundleCtx.prepareObject(res);
		return res;
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}

//final OXDocument aOXDocument = (bFastEmpty) ? null : oxDocumentFactory.createDocumentAccess(aSession, aUserData, bNewDoc, aResManager, false, null);