/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.tools;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.document.DocContextLogger;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.IPartImporter;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.common.user.UserData;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.session.Session;

/**
 * Wrapper class to provide a general access to the importer regardless of the
 * features that the importer implements.
 *
 * @author Carsten Driesner
 * @since 7.8.4
 * @see com.openexchange.office.IPartImporter
 */
public class ChunkableDocLoader {
    //-------------------------------------------------------------------------
	private final static String PROP_PART_COUNT        = "partCount";
	private final static String PROP_ACTIVE_PART_INDEX = "activePartIndex";

    //-------------------------------------------------------------------------
    private static final Logger LOG = LoggerFactory.getLogger(ChunkableDocLoader.class);

    //-------------------------------------------------------------------------
	private final OXDocument docLoader;

	//-------------------------------------------------------------------------
	private final IImporter importer;

	//-------------------------------------------------------------------------
	private final IPartImporter partImporter;

	//-------------------------------------------------------------------------
	private final DocumentProperties docProperties;

    //-------------------------------------------------------------------------
	private boolean createFastLoadOperations;

	//-------------------------------------------------------------------------
	private Map<String, Object> metaDataMap = null;

	/**
	 * Initializes a new chunkable document loader instance.
	 *
	 * @param docLoader a valid document loader instance
	 * @param importer a valid importer instance (supporting chunks or not)
	 * @param importerProps optional importer properties to be used by this loader instance
	 */
	public ChunkableDocLoader(final OXDocument docLoader, final IImporter importer, boolean createFastLoadOperations) {
		Validate.notNull(docLoader);
		Validate.notNull(importer);

		this.docLoader = docLoader;
		this.importer = importer;
		this.docProperties = new DocumentProperties();
		this.createFastLoadOperations = createFastLoadOperations;

		if (importer instanceof IPartImporter) {
			partImporter = (IPartImporter)importer;
		} else {
			partImporter = null;
		}
	}

	public void setCreateFastLoadOperations(boolean createFastLoadOperations) {
	    this.createFastLoadOperations = createFastLoadOperations;

	    final Object operationDocument = docProperties.get(DocumentProperties.PROP_DOCUMENT);
	    if(operationDocument instanceof OfficeOpenXMLOperationDocument) {
	        ((OfficeOpenXMLOperationDocument)operationDocument).setCreateFastLoadOperations(createFastLoadOperations);
	    }
	}

	/**
	 * Prepares to load the document stream and create the operations.
	 *
	 * @return TRUE if the preparation was successful, otherwise FALSE.
	 * @throws Exception
	 */
	public boolean prepareLoad() throws Exception {
		if (null != partImporter) {
			final Session session = docLoader.getSession();
			final InputStream inStream = new ByteArrayInputStream(docLoader.getDocumentBuffer());
			partImporter.initPartitioning(session, inStream, docProperties, createFastLoadOperations);
		} else {
			// nothing to do for old importer interface
		}

		return true;
	}

	/**
	 * Retrieves the file meta data of the document.
	 *
	 * @return
	 * @throws Exception
	 */
	public DocumentMetaData getFileMetaData() throws Exception {
		return docLoader.getDocumentMetaData();
	}

	/**
	 * Retrieves the low-level document loader instance used by
	 * this chunkable document loader.
	 *
	 * @return
	 * @throws Exception
	 */
	public OXDocument getDocumentLoader() throws Exception {
		return docLoader;
	}

	/**
	 * Retrieves the global operations of the document. Global operations are
	 * defined as operations which are connected to the whole document and not
	 * to parts (e.g. page size, number of pages, sheets, slides).
	 *
	 * @returns JSONObject containing a property "operations" which includes
	 *  the global operations.
	 *
	 * @throws Exception
	 */
	public JSONObject getGlobalOperations() throws Exception {
		JSONObject globalOps = new JSONObject();

		if (null != partImporter) {
			metaDataMap = partImporter.getMetaData(docProperties);
			Object value = metaDataMap.get(MessagePropertyKey.KEY_OPERATIONS);

			if (value instanceof JSONArray) {
				globalOps.put(MessagePropertyKey.KEY_OPERATIONS, value);
			}

			metaDataMap.put(PROP_ACTIVE_PART_INDEX, docProperties.get(DocumentProperties.PROP_SPREADHSEET_ACTIVE_SHEET_INDEX));
		} else {
			// empty global operations
			globalOps.put(MessagePropertyKey.KEY_OPERATIONS, new JSONArray());
		}

		return globalOps;
	}

	/**
	 * Provides the active part index of the document. The active part is defined
	 * as the page, sheet or slide which currently visible to the user. The index is
	 * zero based.
	 *
	 * @return
	 * @throws Exception
	 */
	public int getActivePartIndex() throws Exception {
		int index = 1;

		if (metaDataMap != null) {
			Object value = metaDataMap.get(PROP_ACTIVE_PART_INDEX);
			if (value instanceof Integer)
				index = (Integer)value;
		}

		return index;
	}


	/**
	 * Provides the number of parts of which this document consists. This
	 * can be one or greater than one.
	 *
	 * @return
	 * @throws Exception
	 */
	public int getPartsCount() throws Exception {
		int count = 1;

		if (metaDataMap != null) {
			Object value = metaDataMap.get(PROP_PART_COUNT);
			if (value instanceof Integer)
				count = (Integer)value;
		}

		return count;
	}

	/**
	 * Retrieves the active operations of the document. Active operations are
	 * defined as operations which are connected to the active part of the document
	 * (e.g. the part which is visible to the user: page, slide or sheet.
	 *
	 * @returns JSONObject containing a property "operations" which includes
	 *  the global operations.
	 *
	 * @throws Exception
	 */
	public JSONObject getActiveOperations() throws Exception {
		JSONObject opsObject = new JSONObject();

		if (null != partImporter) {
			final Map<String, Object> activePartProps = partImporter.getActivePart(docProperties);
			if (null != activePartProps) {
				Object value = activePartProps.get(MessagePropertyKey.KEY_OPERATIONS);
				if (value instanceof JSONArray) {
					final JSONArray activeOps = (JSONArray)value;
					opsObject.put(MessagePropertyKey.KEY_OPERATIONS, activeOps);
				}
			}
			// sheet count is only available after getActivePart is called!!
			metaDataMap.put(PROP_PART_COUNT, docProperties.get(DocumentProperties.PROP_SPREADSHEET_SHEET_COUNT));
		} else {
			final Session session = docLoader.getSession();
			final InputStream inStream = new ByteArrayInputStream(docLoader.getDocumentBuffer());

			opsObject = importer.createOperations(session, inStream, docProperties, createFastLoadOperations);
			if (null == opsObject) {
				throw new FilterException("No operations received from filter!", FilterException.ErrorCode.CRITICAL_ERROR);
			}
		}

		return opsObject;
	}

	/**
	 * Retrieves the remaining operations of the document. Remaining operations consists
	 * of all operations which are not part of the global or active part. This does not
	 * include the operations stored in a rescue document as they are bound to trigger
	 * errors.
	 *
	 * @returns JSONObject containing a property "operations" which includes
	 *  the remaining operations. The remaining operations count can be zero!
	 *
	 * @throws Exception
	 */
	public JSONObject getRemainingOperations() throws Exception {
		JSONObject remOpsObject = new JSONObject();
		JSONArray remOps = new JSONArray();

		if (null != partImporter) {
			Map<String, Object> nextPartProps = partImporter.getNextPart(docProperties);
			while (nextPartProps != null) {
				final Object value = nextPartProps.get(MessagePropertyKey.KEY_OPERATIONS);
				if (value instanceof JSONArray) {
					JSONHelper.appendArray(remOps, (JSONArray)value);
				}
				nextPartProps = partImporter.getNextPart(docProperties);
			}
		}

		remOpsObject.put(MessagePropertyKey.KEY_OPERATIONS, remOps);

		return remOpsObject;
	}

	/**
	 * Logs context information about the current user and document.
	 *
	 * @param debugInfo
	 * @param userData
	 * @param docLoader
	 * @return
	 */
	public static String getContextStringWithDoc(boolean debugInfo, final UserData userData, final ChunkableDocLoader docLoader) {
		String aDocContextString = "";

		try {
			if (userData != null)
			{
	            final String idString = StringUtils.isEmpty(userData.getUserId()) ? "unknown user id" : userData.getUserId();
	            final String folderId = StringUtils.isEmpty(userData.getFolderId()) ? "???" : userData.getFolderId();
	            final String fileId = StringUtils.isEmpty(userData.getFileId()) ? "???" : userData.getFileId();
	            final String fileName = docLoader.getFileMetaData().getFileName();

	            aDocContextString =  DocContextLogger.getContextStringWithDoc(debugInfo, idString, folderId, fileId, fileName);
			}
		} catch (Throwable e) {
        	ExceptionUtils.handleThrowable(e);
			// nothing to do - provide empty string for context
			LOG.error("RT connection: Couldn't create document context string", e);
		}

		return aDocContextString;
	}

    /**
     * Triggers creation of replacement graphic cache, should be called after calling getRemainingOperations.
     *
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public void prepareReplacementGraphics(ApplicationType applicationType, IDocumentConverter documentConverter, Session session) throws Exception {
        final Map<String, String> replacements = (Map<String, String>) docProperties.get(DocumentProperties.PROP_SHAPE2PNG_REPLACEMENT_IMAGES);

        if(documentConverter != null && replacements != null && replacements.size() > 1) {
            try(InputStream docInput = new ByteArrayInputStream(docLoader.getDocumentBuffer())) {
                final HashMap<String, Object> jobProperties = new HashMap<>(8);
                final HashMap<String, Object> resultProperties = new HashMap<>(8);
                jobProperties.put(Properties.PROP_INPUT_STREAM, docInput);

                jobProperties.put(Properties.PROP_SHAPE_REPLACEMENTS, new JSONObject(replacements).toString());

                String inputType = "docx";
                if(applicationType == ApplicationType.APP_SPREADSHEET) {
                    inputType = "xlsx";
                }
                else if(applicationType == ApplicationType.APP_PRESENTATION) {
                    inputType = "pptx";
                }
                jobProperties.put(Properties.PROP_INPUT_TYPE, inputType);
                jobProperties.put(Properties.PROP_MIME_TYPE, "image/png");
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
                jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
                jobProperties.put(Properties.PROP_NO_CACHE, Boolean.TRUE);
                jobProperties.put(Properties.PROP_PRIORITY, JobPriority.HIGH);

                documentConverter.convert("shape2png", jobProperties, resultProperties, session);
            }
        }
    }
}
