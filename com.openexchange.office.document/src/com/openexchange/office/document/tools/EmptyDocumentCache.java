/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.tools;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.tools.annotation.RegisteredService;

/**
 * Caches the language specific empty document data for faster access.
 *
 * @author Carsten Driesner
 * @author Stefan Eckert
 * @since 7.8.0
 */
@Service
@RegisteredService
public class EmptyDocumentCache {

    private final static Logger LOG = Logger.getLogger(EmptyDocumentCache.class.getName());
    private final HashMap<String, EmptyDocData> emptyDocCache;

    public EmptyDocumentCache() {
        emptyDocCache = new HashMap<>();
    }

    /**
     * Provides the cached document data for an empty document and a language.
     *
     * @param key the module name (e.g. text, spreadsheet or presentation). Must not be null or empty.
     * @param lang the language code of the client requesting the new document. Must not be null or empty.
     * @return stored document data (stream, operation and optional html string)
     */
    public synchronized EmptyDocData getDocData(String module, String lang) throws Exception {
        Validate.notEmpty(module);
        Validate.notEmpty(lang);

        return emptyDocCache.get(getAccessKey(module, lang));
    }

    /**
     * Stores the new document data of a specific language in the empty
     * document cache.
     *
     * @param key the module name. Must not be null.
     * @param lang the language code of the client. Must not be null.
     * @param aEmptyDocData the empty document data filled with stream, operations and optional html string.
     * @throws Exception
     */
    public synchronized void setDocData(String module, String lang, final EmptyDocData aEmptyDocData) throws Exception {
        Validate.notEmpty(module);
        Validate.notEmpty(lang);
        Validate.notNull(aEmptyDocData);

        final String sAccessKey = getAccessKey(module, lang);

        long current = System.currentTimeMillis();

        try {
            // Manipulates the first operation which contains the document attributes
            // where we can set language specific data
            final JSONArray  ops = aEmptyDocData.getOperations();
            final JSONObject attrs = ops.getJSONObject(0).getJSONObject(OCKey.ATTRS.value());

            JSONObject character = attrs.optJSONObject(OCKey.CHARACTER.value());
            if (null == character) {
                character = new JSONObject();
                attrs.put(OCKey.CHARACTER.value(), character);
            }
            character.put(OCKey.LANGUAGE.value(), lang);
        } catch (JSONException e) {
            throw new RuntimeException("error while trying to change language of operations", e);
        }

        emptyDocCache.put(sAccessKey, aEmptyDocData);

        long time = System.currentTimeMillis() - current;

        LOG.log(Level.FINE, "DocumentCache changing language via copy operations takes " + time + "ms!");
    }

    /**
     * Creates the specific access key using key and language.
     *
     * @param module the module string. Must not be null.
     * @param lang the language code. Must not be null.
     * @return the access key which is a combination of key and lang.
     */
    private String getAccessKey(String module, String lang) {
        return (module + '-' + lang);
    }
}
