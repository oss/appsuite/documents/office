/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.tools;

import org.json.JSONArray;

/**
 * Contains the necessary data to create an empty document via 'fastEmpty'.
 *
 * @author Carsten Driesner
 * @since 7.8.3
 */
public class EmptyDocData {
    private final String htmlDoc;
    private final JSONArray operations;
    private final byte[] documentData;

    /**
     * Initialzes a new EmptyDocData instance.
     *
     * @param htmlDoc the html document string created by backend using a generic html creator.
     * @param operations the operations created by the responsible document filter
     * @param documentData the document content
     */
    public EmptyDocData(final String htmlDoc, final JSONArray operations, final byte[] documentData) {
        this.htmlDoc = htmlDoc;
        this.operations = operations;
        this.documentData = documentData;
    }

    public String getHtmlDocString() {
        return htmlDoc;
    }

    public JSONArray getOperations() {
        return operations;
    }

    public byte[] getDocumentData() {
        return documentData;
    }
}
