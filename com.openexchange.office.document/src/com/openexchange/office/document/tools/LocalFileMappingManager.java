/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.tools;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.office.tools.annotation.RegisteredService;

/**
 * Manages the access to local files using a specific prefix for these kind of
 * paths. Every local file is associated with a unique ID which provides the
 * input stream for this file.
 *
 * @author Carsten Driesner
 * @since 7.6.2
 */

@Service
@RegisteredService
public class LocalFileMappingManager {
    private static final Logger LOG = LoggerFactory.getLogger(LocalFileMappingManager.class);

    private final HashMap<String, String> m_localFileMapper = new HashMap<String, String>();
    private final HashMap<String, String> m_pathMapper = new HashMap<String, String>();

    public static final String prefixID = "template:";

    /**
     *
     * Initializes a new {@link LocalFileMappingManager}.
     */
    public LocalFileMappingManager() {
    }

    /**
     * Determines if the local file mapper has a association to the provided
     * path.
     *
     * @param path
     *  The local file path that should be checked.
     *
     * @return
     *  The associated ID of the path or null if no ID is stored for the path.
     */
    public synchronized String getIDFromPath(String path) {
        return m_pathMapper.get(path);
    }

    /**
     * Provides a local file path which is associated with the provided id.
     *
     * @param id
     *  A id which describes a certain local file path (system-dependent).
     *
     * @return
     *  The local file path or null if there is no association is stored for
     *  the provided id.
     */
    public synchronized String getPathFromId(String id) {
        return m_localFileMapper.get(id);
    }

    /**
     * Set a new association between and id and a local file path.
     *
     * @param id
     *  A id which describes a certain local file path.
     *
     * @param path
     *  A local file system path (system-dependent).
     *
     * @return
     *  TRUE if the id was not part of the current map, otherwise FALSE.
     */
    public synchronized boolean associateIdAndPath(String id, String path) {
        String result = m_localFileMapper.put(id, path);
        m_pathMapper.put(path, id);
        return (null == result);
    }

    /**
     * Provides a local file path which is associated with the provided id.
     *
     *
     * @param id
     *  A id which describes a certain local file path.
     *
     * @param path
     *  A local file system path (system-dependent).
     *
     * @return
     *  TRUE if the id and path have been stored, otherwise FALSE.
     */
    public synchronized boolean associateIdAndPath(String id, String path, boolean noOverwrite) {
        String result = m_localFileMapper.get(id);
        if (!(noOverwrite && (null != result))) {
            m_localFileMapper.put(id, path);
            m_pathMapper.put(path, id);
        }
        return (null == result);
    }

    /**
     * Generates a ID from a provided path.
     *
     * @param path
     *  A local file system path;
     *
     * @return
     *  A ID generated from the path;
     */
    public static String generateIDFromPath(String path) {
        String result = null;

        try {
            byte[] message = path.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] theDigest = md.digest(message);
            StringBuilder sb = new StringBuilder();
            sb.append(prefixID);
            for (byte b : theDigest) {
                sb.append(String.format("%02X", b));
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            LOG.error("Exception while creating a id from a local file path", e);
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Exception while creating a id from a local file path", e);
        }
        return result;
    }

    /**
     * Provides an input stream from the file associated with the provided
     * id.
     *
     * @param id
     *  A id which describes a certain local file path.
     *
     * @return
     *  An input stream to access the file data or null if no association
     *  is known or the file is not accessible.
     */
    public InputStream getInputStreamFromID(String id) {
        InputStream stream = null;
        String path = getPathFromId(id);

        if ((null != path) && (path.length() > 0)) {
            stream = DocFileHelper.getDocumentStreamFromLocalFile(path);
        }
        return stream;
    }
}
