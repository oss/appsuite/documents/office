/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.document.tools;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentType;

public class DocFileHelper {

    // stream names used within the document stream
    static public final String OX_RESCUEDOCUMENT_DOCUMENTSTREAM_NAME = "oxoffice/documentStream";
    static public final String OX_RESCUEDOCUMENT_OPERATIONSSTREAM_NAME = "oxoffice/operationUpdates";
    static public final String OX_RESCUEDOCUMENT_RESOURCESTREAM_NAME_PREFIX = "oxoffice/resource/";
    static public final String OX_DOCUMENT_EXTENDEDDATA = "oxoffice/extendedData.json";

    // Mime types and extensions
    // ------------------------------------------------------
    static public final String IMAGE_JPEG = "image/jpeg";
    static public final String EXTENSION_JPG_1 = "jpg";
    static public final String EXTENSION_JPG_2 = "jpeg";
    static public final String IMAGE_EMF = "image/x-emf";
    static public final String IMAGE_EMF2 = "image/emf";
    static public final String EXTENSION_EMF = "emf";
    static public final String IMAGE_WMF = "image/x-wmf";
    static public final String EXTENSION_WMF = "wmf";
    static public final String IMAGE_GIF = "image/gif";
    static public final String EXTENSION_GIF = "gif";
    static public final String IMAGE_PICT = "image/pict";
    static public final String EXTENSION_PICT = "pct";
    static public final String IMAGE_PNG = "image/png";
    static public final String EXTENSION_PNG = "png";
    public static final String IMAGE_TIFF = "image/tiff";
    public static final String EXTENSION_TIFF = "tiff";
    public static final String EXTENSION_TIFF2 = "tif";
    public static final String IMAGE_EPS = "application/postscript"; // as reported by XmlGraphics
    public static final String EXTENSION_EPS = "eps";
    public static final String IMAGE_BMP = "image/bmp";
    public static final String EXTENSION_BMP = "bmp";
    public static final String EXTENSION_ODFTEXT = "odt";
    public static final String EXTENSION_ODT = "odt";
    public static final String DOC_ODF_TEXT = "application/vnd.oasis.opendocument.text";
    public static final String EXTENSION_ODS = "ods";
    public static final String DOC_ODF_SPREADSHEET = "application/vnd.oasis.opendocument.spreadsheet";
    public static final String EXTENSION_ODP = "odp";
    public static final String DOC_ODF_PRESENTATION = "application/vnd.oasis.opendocument.presentation";
    public static final String EXTENSION_ODG = "odg";
    public static final String DOC_ODF_GRAPHIC = "application/vnd.oasis.opendocument.graphics";
    public static final String EXTENSION_DOCX = "docx";
    public static final String DOC_DOCX_TEXT = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String EXTENSION_XLSX = "xlsx";
    public static final String DOC_DOCX_SPREADSHEET = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String EXTENSION_PPTX = "pptx";
    public static final String DOC_DOCX_PRESENTATION = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    public static final String EXTENSION_XML = "xml";
    public static final String TEXT_XML = "text/xml";
    public static final String EXTENSION_SVG = "svg";
    public static final String IMAGE_SVG = "image/svg+xml";
    public static final String OCTET_STREAM = "application/octet-stream";

    private static volatile Map<String, DocumentFormat> m_extensionMap = null;
    private static volatile Object m_Synchronizer = new Object();

	private DocFileHelper() {
		super();
	}

    /**
     * Retrieves the document stream from the provided path.
     *
     * @param path
     *            A full-qualified path to a file stored in local file system.
     *
     * @return
     *         The document data as an input stream or null if there was a problem
     *         accessing the file.
     */
    static public InputStream getDocumentStreamFromLocalFile(String path) {
        final java.io.File localFile = new java.io.File(path);
        InputStream inputStream = null;

        if (localFile.exists() && localFile.isFile()) {
            try {
                return new BufferedInputStream(new FileInputStream(localFile));
            } catch (FileNotFoundException e) {
                // nothing to do - we return null instead of the stream
            }
        }

        return inputStream;
    }

    /**
     * Tries to determine the mime type from a given file name.
     *
     * @param fileName
     *            The file name that should be used to retrieve the mime type from.
     *
     * @return
     *         The mime type of the fileName or the empty string if the
     *         mime type could not be determined.
     */
    static public String getMimeType(String fileName) {
        final String extension = FileHelper.getExtension(fileName);
        String mimeType = "";

        if (extension.length() > 0) {
            switch (getDocumentFormat(fileName)) {
                case DOCX: {
                    mimeType = DOC_DOCX_TEXT;
                    break;
                }

                case PPTX: {
                    mimeType = DOC_DOCX_PRESENTATION;
                    break;
                }

                case XLSX: {
                    mimeType = DOC_DOCX_SPREADSHEET;
                    break;
                }

                case ODT: {
                    mimeType = DOC_ODF_TEXT;
                    break;
                }

                case ODS: {
                    mimeType = DOC_ODF_SPREADSHEET;
                    break;
                }

                case NONE:
                case ODG:
                case ODP:
                default: {
                    if (extension.equals(EXTENSION_DOCX)) {
                        mimeType = DOC_DOCX_TEXT;
                    } else if (extension.equals(EXTENSION_XLSX)) {
                        mimeType = DOC_DOCX_SPREADSHEET;
                    } else if (extension.equals(EXTENSION_PPTX)) {
                        mimeType = DOC_DOCX_PRESENTATION;
                    } else if (extension.equals(EXTENSION_ODT)) {
                        mimeType = DOC_ODF_TEXT;
                    } else if (extension.equals(EXTENSION_ODS)) {
                        mimeType = DOC_ODF_SPREADSHEET;
                    } else if (extension.equals(EXTENSION_ODP)) {
                        mimeType = DOC_ODF_PRESENTATION;
                    } else if (extension.equals(EXTENSION_ODG)) {
                        mimeType = DOC_ODF_GRAPHIC;
                    } else if (extension.equals(EXTENSION_BMP)) {
                        mimeType = IMAGE_BMP;
                    } else if (extension.equals(EXTENSION_EMF)) {
                        mimeType = IMAGE_EMF;
                    } else if (extension.equals(EXTENSION_GIF)) {
                        mimeType = IMAGE_GIF;
                    } else if (extension.equals(EXTENSION_JPG_1)) {
                        mimeType = IMAGE_JPEG;
                    } else if (extension.equals(EXTENSION_JPG_2)) {
                        mimeType = IMAGE_JPEG;
                    } else if (extension.equals(EXTENSION_PICT)) {
                        mimeType = IMAGE_PICT;
                    } else if (extension.equals(EXTENSION_PNG)) {
                        mimeType = IMAGE_PNG;
                    } else if (extension.equals(EXTENSION_TIFF)) {
                        mimeType = IMAGE_TIFF;
                    } else if (extension.equals(EXTENSION_TIFF2)) {
                        mimeType = IMAGE_TIFF;
                    } else if (extension.equals(EXTENSION_WMF)) {
                        mimeType = IMAGE_WMF;
                    } else if (extension.equals(EXTENSION_XML)) {
                        mimeType = TEXT_XML;
                    } else if (extension.equals(EXTENSION_SVG)) {
                        mimeType = IMAGE_SVG;
                    }
                    break;
                }
            }
        }

        return ((mimeType.length() < 1) ? OCTET_STREAM : mimeType);
    }

    /**
     * Provides a global map containing associations between extension
     * and general document format.
     *
     * @return
     *         The global map containing associations between extension
     *         and general document format.
     */
    static public Map<String, DocumentFormat> getExtensionMap() {
        // Java DCL pattern with volatile member
        Map<String, DocumentFormat> serviceMap = m_extensionMap;

        if (null == serviceMap) {
            synchronized (m_Synchronizer) {
                if (null == (serviceMap = m_extensionMap)) {
                    serviceMap = m_extensionMap = new HashMap<>();

                    m_extensionMap.put("docx", DocumentFormat.DOCX);
                    m_extensionMap.put("docx_ox", DocumentFormat.DOCX);
                    m_extensionMap.put("docm", DocumentFormat.DOCX);
                    m_extensionMap.put("docm_ox", DocumentFormat.DOCX);
                    m_extensionMap.put("dotx", DocumentFormat.DOCX);
                    m_extensionMap.put("dotx_ox", DocumentFormat.DOCX);
                    m_extensionMap.put("dotm", DocumentFormat.DOCX);
                    m_extensionMap.put("dotm_ox", DocumentFormat.DOCX);
                    m_extensionMap.put("pptx", DocumentFormat.PPTX);
                    m_extensionMap.put("pptx_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("pptm", DocumentFormat.PPTX);
                    m_extensionMap.put("pptm_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("potx", DocumentFormat.PPTX);
                    m_extensionMap.put("potx_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("potm", DocumentFormat.PPTX);
                    m_extensionMap.put("potm_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("ppsx", DocumentFormat.PPTX);
                    m_extensionMap.put("ppsx_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("ppsm", DocumentFormat.PPTX);
                    m_extensionMap.put("ppsm_ox", DocumentFormat.PPTX);
                    m_extensionMap.put("xlsx", DocumentFormat.XLSX);
                    m_extensionMap.put("xlsx_ox", DocumentFormat.XLSX);
                    m_extensionMap.put("xltx", DocumentFormat.XLSX);
                    m_extensionMap.put("xltx_ox", DocumentFormat.XLSX);
                    m_extensionMap.put("xlsm", DocumentFormat.XLSX);
                    m_extensionMap.put("xlsm_ox", DocumentFormat.XLSX);
                    m_extensionMap.put("xltm", DocumentFormat.XLSX);
                    m_extensionMap.put("xltm_ox", DocumentFormat.XLSX);
                    m_extensionMap.put("odt", DocumentFormat.ODT);
                    m_extensionMap.put("odt_ox", DocumentFormat.ODT);
                    m_extensionMap.put("ott", DocumentFormat.ODT);
                    m_extensionMap.put("ott_ox", DocumentFormat.ODT);
                    m_extensionMap.put("ods", DocumentFormat.ODS);
                    m_extensionMap.put("ods_ox", DocumentFormat.ODS);
                    m_extensionMap.put("ots", DocumentFormat.ODS);
                    m_extensionMap.put("ots_ox", DocumentFormat.ODP);
                    m_extensionMap.put("odp", DocumentFormat.ODP);
                    m_extensionMap.put("odp_ox", DocumentFormat.ODP);
                    m_extensionMap.put("otp", DocumentFormat.ODP);
                    m_extensionMap.put("otp_ox", DocumentFormat.ODP);
                }
            }
        }

        return serviceMap;
    }

    /**
     * Tries to determine the general document type from a given file name.
     *
     * @param fileName
     *            The file name to be used to determine the document format.
     *
     * @return
     *         The document type or DocumentType.NONE if no known type could
     *         be found.
     */
    static public DocumentType getDocumentType(String fileName) {
        return getDocumentTypeFromDocumentFormat(getDocumentFormat(fileName));
    }

    /**
     * Tries to determine the general document format from a given file name.
     *
     * @param fileName
     *            The file name to be used to determine the document format.
     *
     * @return
     *         The document format or DocumentFormat.NONE if no known format could
     *         be found.
     */
    static public DocumentFormat getDocumentFormat(String fileName) {
        final String fileExtension = FileHelper.getExtension(fileName, true);
        final Map<String, DocumentFormat> extensionMap = getExtensionMap();
        DocumentFormat documentFormat = DocumentFormat.NONE;

        if ((null != fileExtension) && extensionMap.containsKey(fileExtension)) {
            documentFormat = extensionMap.get(fileExtension);
        }

        return documentFormat;
    }

    /**
     * Maps a document format to a document type.
     *
     * @param documentFormat
     *            The document format where the type should be determined.
     *
     * @return
     *         The document type or DocumentType.NONE if no known format has been
     *         provided.
     */
    static public DocumentType getDocumentTypeFromDocumentFormat(DocumentFormat documentFormat) {
        DocumentType documentType = DocumentType.NONE;

        if (null != documentFormat) {
            switch (documentFormat) {
                case DOCX:
                case ODT: {
                    documentType = DocumentType.TEXT;
                    break;
                }

                case XLSX:
                case ODS: {
                    documentType = DocumentType.SPREADSHEET;
                    break;
                }

                case PPTX:
                case ODP:
                case ODG: {
                    documentType = DocumentType.PRESENTATION;
                    break;
                }

                case NONE:
                default: {
                    break;
                }
            }
        }

        return documentType;
    }
}
