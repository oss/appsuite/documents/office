/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXDocRestAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;

import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2RestMultiNodeTests {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv ()
        throws Exception
    {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void test_RESTAPI_GetDocumentWithUsersOnTwoDifferentNodes()
        throws Exception
    {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final TestRunConfig aEnvNode02  = TestRunConfigHelper.defineEnv4Node02 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

        final TextDoc       aDocUser01  = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        final EditableDoc aDocUser02 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite02, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

        int nCountApplyOps = 0;

        aDocUser01.open     ();
        aDocUser02.open     ();

        aDocUser01.applyOps (); ++nCountApplyOps;
        aDocUser01.applyOps (); ++nCountApplyOps;

        try
        {
            final OXDocRestAPI aDoc2RESTAPI = new OXDocRestAPI();

            aDoc2RESTAPI.bind(aAppsuite02);
            final InputStream inStream = aDoc2RESTAPI.getDocument(aDocUser02.getFileId(), aDocUser02.getFolderId(), aDocUser02.getRT2ClientUID(), aDocUser02.getDocUID(), "Unnamed.docx", aDocUser02.getType());
            Assert.assertTrue("PDF document does not contain expected content", pdfStreamVerifier(inStream));
        }
        catch (final ServerResponseException e)
        {
            Assert.fail("Server sent not expected response: " + e.getMessage());
        }

        aDocUser01.close    ();
        aDocUser02.close    ();

        //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01, aDocUser02)));

        // verify the document content that must include the text two times
        boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser01, TextDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored or viewer could change it!", bVerified);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
    }

    //-------------------------------------------------------------------------
    private boolean pdfStreamVerifier(final InputStream aResultInStream)
        throws Exception
    {
        final String sPDFContent = IOUtils.toString(aResultInStream, "utf-8");
        final int indexPDF = sPDFContent.indexOf("PDF");
        final int indexPages = sPDFContent.indexOf("/Type/Pages");

        return (indexPDF >= 0) && (indexPages >= 0) && (indexPages > indexPDF);
    }

}
