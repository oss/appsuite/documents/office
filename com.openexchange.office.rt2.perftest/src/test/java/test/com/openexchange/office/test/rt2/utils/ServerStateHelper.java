/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Assert;

import com.hazelcast.core.DistributedObjectEvent;
import com.hazelcast.core.DistributedObjectListener;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;

public class ServerStateHelper implements DistributedObjectListener {

    //-------------------------------------------------------------------------
    private static final int MAX_TRIES_TO_CHECK_SERVER_STATE = 20;
    private static final int TIMEOUT_VALUE = 2000;

    private static final String REFCOUNT_CLIENT_PREFIX = "rt2cache.atomic.";
    private static final String REFCOUNT_CLIENT_POSTFIX = ".refcount.clients.a";
    private static final String STATE_CREATED = "created";
    private static final String STATE_REMOVED = "removed";
    private static final String HZ_ATOMICLONG_SERVICE_NAME = "hz:impl:atomicLongService";

    private static final MessageFormat cacheEntryNameMsgFmt = new MessageFormat("rt2cache.atomic.{0}.refcount.clients.a");

    private Map<String, String> atomicLongMap = new ConcurrentHashMap<>();
    private AtomicBoolean started = new AtomicBoolean(false);
    private HazelcastInstance hzInstance = null;
    private AtomicReference<String> hzListenerID = new AtomicReference<>(null);

    //-------------------------------------------------------------------------
    public ServerStateHelper() {

    }

    //-------------------------------------------------------------------------
    public boolean start() throws Exception {
        if (started.compareAndSet(false, true)) {
            hzInstance = HazelcastThinClientProxy.getHzInstance();

            hzListenerID.set(hzInstance.addDistributedObjectListener(this).toString());
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------
    public boolean stop() {
        if (started.compareAndSet(true, false)) {
            if (hzInstance != null) {
                hzInstance.removeDistributedObjectListener(UUID.fromString(hzListenerID.get()));
                return true;
            } else {
                throw new IllegalStateException("start was called but hzInstance was not initialized!");
            }
        }

        return false;
    }

    //-------------------------------------------------------------------------
    public boolean stopAndShutdown() throws Exception {
        if (started.compareAndSet(true, false)) {
            if (hzInstance != null) {
                hzInstance.removeDistributedObjectListener(UUID.fromString(hzListenerID.get()));
                HazelcastThinClientProxy.shutdown();
                return true;
            } else {
                throw new IllegalStateException("start was called but hzInstance was not initialized!");
            }
        }

        return false;
    }

    //-------------------------------------------------------------------------
    public static RT2JmxClient getConnectedJmxClient(final TestRunConfig aEnvNode) throws Exception {
        final RT2JmxClient aRT2JmxClient = new RT2JmxClient("", aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aRT2JmxClient.connect();
        return aRT2JmxClient;
    }

    //-------------------------------------------------------------------------
    public boolean checkServerState(final TestRunConfig aEnvNode, final Set<String> aDocUIDClosed) throws Exception {
        if (started.get() == false) {
            throw new IllegalStateException("instance must be started before checkServerState can be called!");
        }

        if (aDocUIDClosed.isEmpty()) {
            return true;
        }

        boolean bServerStateOk = false;
        final IMap<String, String> rt2DocOnNodeMap = hzInstance.getMap("rt2DocOnNodeMap-1");

        int tries = 0;
        try {
            boolean tryAgain = true;
            boolean docHzMapEntriesRemoved = true;
            boolean docAtomicLongRemoved = true;

            while (tryAgain) {
                ++tries;

                docHzMapEntriesRemoved = true;
                docAtomicLongRemoved = true;
                for (String docUID : aDocUIDClosed) {
                    docHzMapEntriesRemoved &= !rt2DocOnNodeMap.containsKey(docUID);
                    docAtomicLongRemoved &= wasAtomicLongRemoved(docUID);
                }

                tryAgain = (tries >= MAX_TRIES_TO_CHECK_SERVER_STATE) ? false : !(docHzMapEntriesRemoved && docAtomicLongRemoved);
                if (tryAgain) {
                    Thread.sleep(TIMEOUT_VALUE);
                }
            }

            if (!docHzMapEntriesRemoved)
                Assert.fail("Server based DocNodeMap contains entries of closed documents! DocUIDs = " + SetHelper.getCollectedStringEntries(aDocUIDClosed));

            if (!docAtomicLongRemoved)
                Assert.fail("Server based AtomicLong(s) used as ref-count found for closed documents! DocUIDs = " + SetHelper.getCollectedStringEntries(aDocUIDClosed));

            return true;
        } catch (Exception e) {
            Assert.fail("Exception caught while trying to check server state" + e.getMessage());
        }

        return bServerStateOk;
    }

    //-------------------------------------------------------------------------
    public static String generateNameFromDocUid(String docUid) {
        Object[] objArr = { docUid };
        return cacheEntryNameMsgFmt.format(objArr);
    }

    //-------------------------------------------------------------------------
    private static String createRefCountName(String docUid) {
        final StringBuilder tmp = new StringBuilder(REFCOUNT_CLIENT_PREFIX);
        tmp.append(docUid);
        tmp.append(REFCOUNT_CLIENT_POSTFIX);
        return tmp.toString();
    }

    //-------------------------------------------------------------------------
    private boolean wasAtomicLongRemoved(String docUid) {
        final String refCountName = createRefCountName(docUid);
        final String state = atomicLongMap.get(refCountName);
        if (state != null)
            return STATE_REMOVED.equals(state);

        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public void distributedObjectCreated(DistributedObjectEvent event) {
        final String serviceName = event.getServiceName();
        if (HZ_ATOMICLONG_SERVICE_NAME.equals(serviceName)) {
            final String name = event.getDistributedObject().getName();
            if (name.startsWith(REFCOUNT_CLIENT_PREFIX)) {
                atomicLongMap.put(name, STATE_CREATED);
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void distributedObjectDestroyed(DistributedObjectEvent event) {
        final String serviceName = event.getServiceName();
        if (HZ_ATOMICLONG_SERVICE_NAME.equals(serviceName)) {
            final String objName = (String) event.getObjectName();
            if (objName.startsWith(REFCOUNT_CLIENT_PREFIX)) {
                atomicLongMap.put(objName, STATE_REMOVED);
            }
        }
    }

}
