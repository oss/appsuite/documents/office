/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import com.openexchange.office.rt2.perftest.fwk.OXDocRestAPI;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;

public class RecentFileListVerifier {
    final int MAX_RECENT_ENTRIES = 30;
	
	//-------------------------------------------------------------------------
	private RecentFileListVerifier() {
		// nothing to do
	}

    //-------------------------------------------------------------------------
    public static boolean isEmptyRecentList(final OXDocRestAPI aDocRestAPI, final String sDocType) throws Exception {
        final JSONArray aRecentFileList = getRecentFileListFromMiddleware(aDocRestAPI, sDocType);
        return (aRecentFileList != null) && (aRecentFileList.length() == 0);
    }

    //-------------------------------------------------------------------------
    public static int getEntryCountOfRecentList(final OXDocRestAPI aDocRestAPI, final String sDocType) throws Exception {
        final JSONArray aRecentFileList = getRecentFileListFromMiddleware(aDocRestAPI, sDocType);

        if (aRecentFileList != null)
            return aRecentFileList.length();
        else
            return 0;
    }

    //-------------------------------------------------------------------------
    public static boolean checkTopEntryInRecentList(final OXDocRestAPI aDocRestAPI, final String sDocType, final JSONObject aFileDescriptor) throws Exception {
        final JSONArray aRecentFileList = getRecentFileListFromMiddleware(aDocRestAPI, sDocType);
        Assert.assertTrue((aRecentFileList != null) && (aRecentFileList.length() > 0));

        return isEqual(aFileDescriptor, aRecentFileList.getJSONObject(0));
    }

    //-------------------------------------------------------------------------
    public static boolean checkEntryNotInRecentList(final OXDocRestAPI aDocRestAPI, final String sDocType, final JSONObject aFileDescriptor) throws Exception {
        final JSONArray aRecentFileList = getRecentFileListFromMiddleware(aDocRestAPI, sDocType);
        Assert.assertTrue(aRecentFileList != null);

        if (aRecentFileList.length() > 0) {
            int i = 0;
            while (i < aRecentFileList.length()) {
                final JSONObject aRecentFileListEnry = aRecentFileList.getJSONObject(i++);
                if (isEqual(aFileDescriptor, aRecentFileListEnry)) {
                	return false;
                }
            }
        }

        return true;
    }

    //-------------------------------------------------------------------------
    public static JSONObject getFileDescriptor(String sFolderId, String sFileId) {
        final JSONObject aJSONResult = new JSONObject();
        aJSONResult.put("id", sFileId);
        aJSONResult.put("folder_id", sFolderId);
        return aJSONResult;
    }

    //-------------------------------------------------------------------------
    private static JSONArray getRecentFileListFromMiddleware(final OXDocRestAPI aDocRestAPI, final String sDocType) throws Exception {
        try {
            final JSONObject aResult = aDocRestAPI.getTemplateAndRecentFileList(sDocType);
            Assert.assertTrue("Server sent unexpected result for 'gettemplatesandrecentlist'", templatesAndRecentsVerifier(aResult));

            return aResult.getJSONArray("recents");
        } catch (final ServerResponseException e) {
            Assert.fail("Server sent not expected response: " + e.getMessage());

            throw e;
        }
    }

    //-------------------------------------------------------------------------
    private static boolean templatesAndRecentsVerifier(final JSONObject aTemplateAndRecentsResult) {
        return ((null != aTemplateAndRecentsResult) && (aTemplateAndRecentsResult.has("templates")) && (aTemplateAndRecentsResult.has("recents")));
    }

    //-------------------------------------------------------------------------
    private static boolean isEqual(final JSONObject aFileDescrToCheck, final JSONObject aStoredFileDescr) {
        boolean bResult = false;

        if ((aFileDescrToCheck != null) && (aStoredFileDescr != null)) {
            final Iterator<String> aKeysIter = aFileDescrToCheck.keys();
            while (aKeysIter.hasNext()) {
                final String sKey = aKeysIter.next();
                if (!isEqual(aFileDescrToCheck.get(sKey), aStoredFileDescr.opt(sKey))) {
                	return false;
                }
            }

            bResult = true;
        } else if ((aFileDescrToCheck == null) && (aStoredFileDescr == null))
            bResult = true;

        return bResult;
    }

    //-------------------------------------------------------------------------
    private static boolean isEqual(Object o1, Object o2) {
        if ((o1 != null) && (o2 != null))
            return o1.equals(o2);
        else if ((o1 == null) && (o2 == null))
            return true;

        return false;
    }
}
