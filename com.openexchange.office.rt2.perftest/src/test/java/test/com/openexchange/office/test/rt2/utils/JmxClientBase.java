/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import javax.management.MBeanInfo;

public class JmxClientBase
{
    //-------------------------------------------------------------------------
	private String           m_sClusterName;
	private JMXClient        m_aJmxClient;

    //-------------------------------------------------------------------------
	public JmxClientBase(String sClusterName, String sServerName, int nPort)
	{
		m_sClusterName = sClusterName;
		m_aJmxClient   = new JMXClient(sServerName, nPort);
	}

    //-------------------------------------------------------------------------
	public void connect()
	    throws Exception
	{
		if (null != m_aJmxClient)
			m_aJmxClient.connect();
		else
			throw new IllegalStateException("No connection to JMX server!");
	}

    //-------------------------------------------------------------------------
	public void disconnect()
	    throws Exception
	{
		if (null != m_aJmxClient)
			m_aJmxClient.disconnect();
		else
			throw new IllegalStateException("No connection to JMX server!");
	}

    //-------------------------------------------------------------------------
	public boolean isConnected()
	{
		return ((null != m_aJmxClient) && (m_aJmxClient.isConnected()));
	}

    //-------------------------------------------------------------------------
	public String getClusterName()
	{
		return m_sClusterName;
	}

    //-------------------------------------------------------------------------
	public Object getAttribute(String sObjectName, String sAttributeName)
		throws Exception
	{
		if (isConnected())
			return m_aJmxClient.getAttribute(sObjectName, sAttributeName);

		throw new IllegalStateException("No connection to JMX server!");
	}

    //-------------------------------------------------------------------------
	public Object invokeOperation(String sObjectName, String sOperationName, final Object[] argValues, final String[] argTypes)
	   throws Exception
	{
		if (isConnected())
			return m_aJmxClient.invokeOperation(sObjectName, sOperationName, argValues, argTypes);

		throw new IllegalStateException("No connection to JMX server!");
	}

    //-------------------------------------------------------------------------
	public MBeanInfo getMBeanInfo(String sObjectName) throws Exception {
	    if (isConnected()) {
	        return m_aJmxClient.getMBeanInfo(sObjectName);
	    }

        throw new IllegalStateException("No connection to JMX server!");
	}

}
