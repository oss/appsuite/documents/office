/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;

import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2CollabStressTest {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv ()
        throws Exception
    {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testStressDocCollaboration()
        throws Exception
    {
        final int MAX_RUNS = 30;
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final TestRunConfig aEnvNode02  = TestRunConfigHelper.defineEnv4Node02 ();
        final TestRunConfig aEnvNode03  = TestRunConfigHelper.defineEnv4Node03 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);
        final OXAppsuite    aAppsuite03 = TestSetupHelper.openAppsuiteConnection(aEnvNode03);

        for (int i = 0; i < MAX_RUNS; i++) {
            // aDocUser1 is the editor of the document
            final TextDoc aDocUser1  = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT);
            aDocUser1.createNew   ();
            aDocUser1.setAcceptsEditRightsRequest(true);

            // aDocUser2 is a viewer collaborate with aDocUser1
            final EditableDoc aDocUser2 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite02, Doc.DOCTYPE_TEXT, aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);

            // aDocUser3 is a viewer collaborate with aDocUser1
            final EditableDoc aDocUser3 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite03, Doc.DOCTYPE_TEXT, aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);

            int nCountApplyOps = 0;

            aDocUser1.open       ();
            aDocUser2.open       ();
            aDocUser3.open       ();

            aDocUser1.applyOps (); ++nCountApplyOps;
            aDocUser1.applyOps (); ++nCountApplyOps;

            aDocUser1.close      ();
            aDocUser2.close      ();
            aDocUser3.close      ();

            //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser1, aDocUser2)));

            // verify the document content that must include the text two times
            boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser1, TextDoc.STR_TEXT, nCountApplyOps);
            Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);

        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite03);
    }

}
