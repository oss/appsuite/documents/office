/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jetty.util.security.Credential.MD5;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.MessageProperties;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.NewDocumentData;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXConfigRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXDocRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXDocSpellcheckRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXFilesRestAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OperationConstants;
import com.openexchange.office.rt2.perftest.operations.Operations;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.perftest.operations.SupportedOperations;
import com.openexchange.office.tools.error.ErrorCode;

import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.GlobalTemplateListVerifier;
import test.com.openexchange.office.test.rt2.utils.HtmlChecker;
import test.com.openexchange.office.test.rt2.utils.ImageFormatException;
import test.com.openexchange.office.test.rt2.utils.ImageInfo;
import test.com.openexchange.office.test.rt2.utils.OperationsVerification;
import test.com.openexchange.office.test.rt2.utils.PresentationDocumentVerifier;
import test.com.openexchange.office.test.rt2.utils.RecentFileListVerifier;
import test.com.openexchange.office.test.rt2.utils.TemplateListVerifier;
import test.com.openexchange.office.test.rt2.utils.TemplateProperties;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.UserDataVerifier;

//=============================================================================
public class RT2RestTests {

    private static final String SPELL_RESULT = "spellResult";

	// -------------------------------------------------------------------------
	@BeforeClass
	public static void setUpEnv() throws Exception {
		StatusLine.setEnabled(false);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

		final TextDoc aDocUser = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT, false);
		aDocUser.createNew();

		aDocUser.open();

		int nCountApplyOps = 0;
		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;

		try {
			final OXDocRestAPI aDocRESTAPI = new OXDocRestAPI();

			aDocRESTAPI.bind(aAppsuite);
			final InputStream inStream = aDocRESTAPI.getDocument(aDocUser.getFileId(), aDocUser.getFolderId(),
					aDocUser.getRT2ClientUID(), aDocUser.getDocUID(), "Unnamed.docx", aDocUser.getType());
			Assert.assertTrue("PDF document does not contain expected content", pdfStreamVerifier(inStream));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDocUser.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDocUser, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_RestoreDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// ATTENTION: Text documents with OT don't support restore document!
		final PresentationDoc aDocUser01 = (PresentationDoc) TestSetupHelper.newDocInst(aAppsuite01,
				Doc.DOCTYPE_PRESENTATION, false);
		aDocUser01.createNew();

		int nCountApplyOps = 0;

		aDocUser01.open();

		aDocUser01.applyOps();
		++nCountApplyOps;
		aDocUser01.applyOps();
		++nCountApplyOps;

		aDocUser01.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser01, PresentationDoc.STR_TEXT, nCountApplyOps);

		try {
			final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();

			docConfigAPI.bind(aAppsuite01);
			final JSONObject officeSettings = docConfigAPI.list("io.ox/office");
			final boolean concurrentEditing = officeSettings.getJSONObject("text").optBoolean("concurrentEditing", true);
			if (!concurrentEditing) {
				final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
				aDoc1RESTAPI.bind(aAppsuite01);

				final JSONObject aResult = aDoc1RESTAPI.restoreDocument(aDocUser01.getRestoreID(), aDocUser01.getFileId(),
						aDocUser01.getFolderId(), "Document-Restored", new JSONArray());
				Assert.assertTrue("Restore document does not contain expected content", restoreVerifier(aResult));
			} else {
				// restoreDocument currently doesn't support concurrent editing
			}

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_RestoreDocumentWithLocalChanges() throws Exception {
		final int NUM_OF_LOCAL_OPS = 2;
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// ATTENTION: Text documents with OT don't support restore document!
		final PresentationDoc aDocUser01 = (PresentationDoc) TestSetupHelper.newDocInst(aAppsuite01,
				Doc.DOCTYPE_PRESENTATION, false);
		aDocUser01.createNew();

		int nCountApplyOps = 0;

		aDocUser01.open();

		aDocUser01.applyOps();
		++nCountApplyOps;
		aDocUser01.applyOps();
		++nCountApplyOps;

		aDocUser01.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser01, PresentationDoc.STR_TEXT, nCountApplyOps);

		try {
			final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();
			docConfigAPI.bind(aAppsuite01);

			final JSONObject officeSettings = docConfigAPI.list("io.ox/office");
			final boolean concurrentEditing = officeSettings.getJSONObject("text").optBoolean("concurrentEditing", true);
			if (!concurrentEditing) {
				final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
				final OXFilesRestAPI aFilesAPI = new OXFilesRestAPI();

				aDoc1RESTAPI.bind(aAppsuite01);
				aFilesAPI.bind(aAppsuite01);

				final JSONArray aDelFileResult = aFilesAPI.deleteFile(aDocUser01.getFolderId(), aDocUser01.getFileId());
				Assert.assertTrue("Result of delete REST call not as expected!", deleteResultVerifier(aDelFileResult));

				final JSONArray aLocalOperations = new JSONArray();
				if (NUM_OF_LOCAL_OPS > 0) {
					for (int i = 0; i < NUM_OF_LOCAL_OPS; i++) {
						aLocalOperations.put(aDocUser01.createInsertTextOperation(/* slide */0, /* drawing */0,
								/* paragraph */0, /* pos */0, PresentationDoc.STR_TEXT));
					}
				}

				final JSONObject aResult = aDoc1RESTAPI.restoreDocument(aDocUser01.getRestoreID(), aDocUser01.getFileId(),
						aDocUser01.getFolderId(), "Document-Restored", aLocalOperations);
				Assert.assertTrue("Restore document does not contain expected content", restoreVerifier(aResult));

				final String sRestoredFolderId = aResult.getString("restoredFolderId");
				final String sRestoredFileId = aResult.getString("restoredFileId");
				final JSONObject aLoadData = getLoadDataFromDocument(aAppsuite01, sRestoredFolderId, sRestoredFileId);
				final String sHtmlDoc = aLoadData.getString(MessageProperties.PROP_HTMLDOC);
				final JSONArray aOpsArray = aLoadData.getJSONArray(MessageProperties.PROP_OPERATIONS);
				final List<JSONObject> aOpsList = Operations.toOperationsList(aOpsArray);

				boolean bVerified = PresentationDocumentVerifier.checkPresentationDocumentContent(sHtmlDoc, aOpsList, PresentationDoc.STR_TEXT, nCountApplyOps + NUM_OF_LOCAL_OPS);
				Assert.assertTrue("Html string should contain the inserted text & local changes for this document. Document was not correctly restored! HtmlDoc: " + sHtmlDoc,	bVerified);
			} else {
				// restoreDocument currently doesn't support concurrent editing
			}
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetOperations() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		int nCountApplyOps = 0;

		aDocUser01.open();

		aDocUser01.applyOps();
		++nCountApplyOps;
		aDocUser01.applyOps();
		++nCountApplyOps;

		aDocUser01.close();

		try {
			final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();
			final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();

			docConfigAPI.bind(aAppsuite01);
			aDoc1RESTAPI.bind(aAppsuite01);
			final JSONObject officeConfigData = docConfigAPI.list("io.ox/office");
			final JSONObject aResult = aDoc1RESTAPI.getOperations(aDocUser01.getFolderId(), aDocUser01.getFileId());

			Assert.assertTrue("Document does not contain expected text", hasTextDocExpectedTextContent(officeConfigData, aResult, nCountApplyOps));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetOperationsWithAllDocumentTypes() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// TEXT
		final EditableDoc aDocUserText = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		int nCountApplyOps = createDocumentAndApplyDefaultOpsAndCloseIt(aDocUserText);

		final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();
		docConfigAPI.bind(aAppsuite01);
		final JSONObject officeConfigData = docConfigAPI.list("io.ox/office");

		try {

			final JSONObject aTextDocData = getLoadDataFromDocument(aAppsuite01, aDocUserText.getFolderId(), aDocUserText.getFileId());
			Assert.assertTrue("Document does not contain expected text", hasTextDocExpectedTextContent(officeConfigData, aTextDocData, nCountApplyOps));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// PRESENTATION
		EditableDoc aDocUserPresentation = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite01,
				Doc.DOCTYPE_PRESENTATION, false);
		nCountApplyOps = createDocumentAndApplyDefaultOpsAndCloseIt(aDocUserPresentation);

		try {
			final JSONObject aPresDocData = getLoadDataFromDocument(aAppsuite01, aDocUserPresentation.getFolderId(), aDocUserPresentation.getFileId());
			Assert.assertTrue("Document does not contain expected text", hasPresentationDocExpectedTextContent(officeConfigData, aPresDocData, nCountApplyOps));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// SPREADSHEET
		EditableDoc aDocUserSpreadsheet = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_SPREADSHEET, false);
		createDocumentAndApplyDefaultOpsAndCloseIt(aDocUserSpreadsheet);

		try {
			final JSONObject aDocUserSpreadsheetData = getLoadDataFromDocument(aAppsuite01,	aDocUserSpreadsheet.getFolderId(), aDocUserSpreadsheet.getFileId());
			final String sHtml = aDocUserSpreadsheetData.optString(MessageProperties.PROP_HTMLDOC);
			Assert.assertTrue("Unexpected getoperations answer for Spreadsheet document", StringUtils.isEmpty(sHtml));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_RecentFileListHandling() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		final OXFilesRestAPI filesRestAPI = new OXFilesRestAPI();

		aDoc1RESTAPI.bind(aAppsuite01);
		filesRestAPI.bind(aAppsuite01);

		RecentFileListVerifier.getEntryCountOfRecentList(aDoc1RESTAPI, aDocUser01.getType());

		aDocUser01.open();

		// opening the document should set it to the top of the recent list
		final JSONObject aExpectedTopEntry = new JSONObject();
		aExpectedTopEntry.put("id", aDocUser01.getFileId());
		aExpectedTopEntry.put("folder_id", aDocUser01.getFolderId());
		Assert.assertTrue("Top entry in recent file list not expected",
			RecentFileListVerifier.checkTopEntryInRecentList(aDoc1RESTAPI, aDocUser01.getType(), aExpectedTopEntry));

		aDocUser01.close();

		// closing the document should not change the recent list
		Assert.assertTrue("Top entry in recent file list not expected",
			RecentFileListVerifier.checkTopEntryInRecentList(aDoc1RESTAPI, aDocUser01.getType(), aExpectedTopEntry));

		// remove document file from drive and check that recent list is correctly updated
		filesRestAPI.removeFile(aDocUser01.getFolderId(), aDocUser01.getFileId());
		Assert.assertTrue("Entry of removed file in recent file list!",	RecentFileListVerifier.checkEntryNotInRecentList(aDoc1RESTAPI, aDocUser01.getType(), aExpectedTopEntry));

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetTemplateListSimple() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateList("text");

			Assert.assertTrue("Unexpected result for 'gettemplatelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'templates'", aResult.opt("templates"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aTemplatesList = aResult.getJSONArray("templates");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Template list contains invalid property values",
					TemplateListVerifier.verifyTemplateList(aTemplatesList));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDocUser01.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetTemplateAndRecentFileListSimple() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'templates'", aResult.opt("templates"));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aTemplatesList = aResult.getJSONArray("templates");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Template list contains invalid property values",
					TemplateListVerifier.verifyTemplateList(aTemplatesList));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDocUser01.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetTemplateListAndUseIt() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateList("text");

			Assert.assertTrue("Unexpected result for 'gettemplatelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'templates'", aResult.opt("templates"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aTemplatesList = aResult.getJSONArray("templates");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Template list contains invalid property values",
					TemplateListVerifier.verifyTemplateList(aTemplatesList));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDocUser01.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_CreateDocumentFromTemplate() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDoc01User = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT, false);
		aDoc01User.createNew();

		final OXDocRestAPI aDocRESTAPI = new OXDocRestAPI();
		aDocRESTAPI.bind(aAppsuite);

		aDoc01User.open();

		JSONArray aTemplatesList = null;

		try {
			final JSONObject aResult = aDocRESTAPI.getTemplateList("text");

			Assert.assertTrue("Unexpected result for 'gettemplatelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'templates'", aResult.opt("templates"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			aTemplatesList = aResult.getJSONArray("templates");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Template list contains invalid property values",
					TemplateListVerifier.verifyTemplateList(aTemplatesList));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		NewDocumentData aDocFromTemplateData = null;
		try {
			if ((null != aTemplatesList) && (aTemplatesList.length() > 0)) {
				final JSONObject aFirstTemplate = aTemplatesList.getJSONObject(0);
				final String sTemplateFileId = aFirstTemplate.getString(TemplateProperties.PROPERTY_FILE_ID);
				aDocFromTemplateData = aDocRESTAPI.createNewDocumentFromTemplate(sTemplateFileId,
						aDoc01User.getFolderId(), Doc.DOCTYPE_TEXT, "document_from_template");
			}
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDoc01User.close();

		Assert.assertNotNull(aDocFromTemplateData);

		// try to open the document created from the first template
		final TextDoc aTemplateDocUser = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT,
				aDoc01User.getFolderId(), aDocFromTemplateData.getFileId(),
				RT2.calcDriveDocID(aEnvNode01, aDocFromTemplateData.getFileId()), true);
		aTemplateDocUser.open();

		aTemplateDocUser.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_ClearRecentList() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			aDoc1RESTAPI.clearRecentList("text");
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list contains entries, but should be empty!", (aRecentsList.length() == 0));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		aDocUser01.open();

		aDocUser01.applyOps();
		aDocUser01.applyOps();

		aDocUser01.close();

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list must contain entries, but is empty!", (aRecentsList.length() > 0));

			// opening the document should set it to the top of the recent list
			final JSONObject aExpectedTopEntry = new JSONObject();
			aExpectedTopEntry.put("id", aDocUser01.getFileId());
			aExpectedTopEntry.put("folder_id", aDocUser01.getFolderId());
			Assert.assertTrue("Top entry in recent file list not expected", RecentFileListVerifier
					.checkTopEntryInRecentList(aDoc1RESTAPI, aDocUser01.getType(), aExpectedTopEntry));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			aDoc1RESTAPI.clearRecentList("text");
		} catch (final ServerResponseException e) {
			TestSetupHelper.closeAppsuiteConnections(aAppsuite01);

			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list contains entries, but should be empty!", (aRecentsList.length() == 0));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_DeleteFileFromRecentList() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			aDoc1RESTAPI.clearRecentList("text");
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list contains entries, but should be empty!", (aRecentsList.length() == 0));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		aDocUser01.open();

		aDocUser01.applyOps();
		aDocUser01.applyOps();

		aDocUser01.close();

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list must contain entries, but is empty!", (aRecentsList.length() > 0));

			// opening the document should set it to the top of the recent list
			final JSONObject aExpectedTopEntry = new JSONObject();
			aExpectedTopEntry.put("id", aDocUser01.getFileId());
			aExpectedTopEntry.put("folder_id", aDocUser01.getFolderId());
			Assert.assertTrue("Top entry in recent file list not expected", RecentFileListVerifier
					.checkTopEntryInRecentList(aDoc1RESTAPI, aDocUser01.getType(), aExpectedTopEntry));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			aDoc1RESTAPI.deleteFileFromRecentList("text", aDocUser01.getFileId());
		} catch (final ServerResponseException e) {
			TestSetupHelper.closeAppsuiteConnections(aAppsuite01);

			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateAndRecentFileList("text");

			Assert.assertTrue("Unexpected result for 'gettemplateandrecentfilelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'recents'", aResult.opt("recents"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aRecentsList = aResult.getJSONArray("recents");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Recents list contains entries, but should be empty!", (aRecentsList.length() == 0));

		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetContextTemplateFolders() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			final JSONArray aTemplateFolderList = aDoc1RESTAPI.getContextTemplateFolders();

			Assert.assertNotNull("Unexpected answer from middleware - no template folder list", aTemplateFolderList);
			Assert.assertTrue("Unexpected types/values in template list found",
					GlobalTemplateListVerifier.verifyTemplateList(aTemplateFolderList));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetUserInfo() throws Exception {
		final String TEST_USER_ID = "2";

		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			final JSONObject aUserInfoResult = aDoc1RESTAPI.getUserInfo(TEST_USER_ID);

			Assert.assertNotNull("Unexpected answer from middleware - no user info object found", aUserInfoResult);
			Assert.assertNotNull("Expected to find 'userData' property in server result",
					aUserInfoResult.optJSONObject("userData"));
			Assert.assertTrue("User data doesn't contain all mandatory properties",
					UserDataVerifier.checkUserDataInfo(aUserInfoResult.getJSONObject("userData")));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_logPerformanceData() throws Exception {
		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			final JSONObject aJSONData = new JSONObject();
			aJSONData.put("test1", true);
			aJSONData.put("test2", 1000);

			final String sPerformanceData = aJSONData.toString();
			aDoc1RESTAPI.logPerformanceData(sPerformanceData);
		} catch (final ServerResponseException e) {
			final HttpStatusCode aStatusCode = e.getStatusCode();
			if (aStatusCode.getStatusCode() == HttpStatusCode.GONE.code()) {
				// performance logging not enabled
			} else {
				Assert.fail("Server sent not expected response: " + e.getMessage());
			}
		}

		// -------------------------------------------------------------------------
		try {
			aDoc1RESTAPI.logPerformanceData(null);
			Assert.fail("Server sent no error response, although the call must fail");
		} catch (final ServerResponseException e) {
			// correct behavior - call must fail
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_logClientData() throws Exception {
		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		// -------------------------------------------------------------------------
		try {
			final String anonymize = String.valueOf(false);
			final String fileId = "100@100/20182";
			final String folderId = "100";
			final String fileName = "unnamed (1).docx";
			final String userId = UUID.randomUUID().toString();
			final String docUID = MD5.digest(fileId);
			final String errorCode = ErrorCode.LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR.getCodeAsStringConstant();
			final String logData = "";

			int statusCode = aDoc1RESTAPI.logClientData(anonymize, fileId, folderId, fileName, userId, docUID,
					errorCode, logData);
			Assert.assertTrue("Status code is not as expected", statusCode == HttpStatusCode.CREATED.code());
		} catch (final ServerResponseException e) {
			Assert.fail("Server response exception: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final String anonymize = String.valueOf(false);
			final String fileId = "100@100/20182";
			final String folderId = "100";
			final String fileName = "unnamed (1).docx";
			final String userId = UUID.randomUUID().toString();
			final String docUID = MD5.digest(fileId);
			final String errorCode = ErrorCode.LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR.getCodeAsStringConstant();
			final String logData = "This is a test string!";

			int statusCode = aDoc1RESTAPI.logClientData(anonymize, fileId, folderId, fileName, userId, docUID,
					errorCode, logData);
			Assert.assertTrue("Status code is not as expected", statusCode == HttpStatusCode.CREATED.code());
		} catch (final ServerResponseException e) {
			Assert.fail("Server response exception: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final String anonymize = String.valueOf(false);
			final String fileId = "com.hidrive:///root/documents/folder1/unnamed%20(1).docx";
			final String folderId = "com.hidrive:///root/documents/folder1";
			final String fileName = "unnamed (1).docx";
			final String userId = UUID.randomUUID().toString();
			final String docUID = MD5.digest(fileId);
			final String errorCode = ErrorCode.LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR.getCodeAsStringConstant();
			final String logData = "";

			int statusCode = aDoc1RESTAPI.logClientData(anonymize, fileId, folderId, fileName, userId, docUID,
					errorCode, logData);
			Assert.assertTrue("Status code is not as expected", statusCode == HttpStatusCode.CREATED.code());
		} catch (final ServerResponseException e) {
			Assert.fail("Server response exception: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		try {
			final String anonymize = String.valueOf(false);
			final String fileId = "com.hidrive:///root/documents/folder1/unnamed%20(1).docx";
			final String folderId = "com.hidrive:///root/documents/folder1";
			final String fileName = "unnamed (1).docx";
			final String userId = UUID.randomUUID().toString();
			final String docUID = MD5.digest(fileId);
			final String errorCode = ErrorCode.LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR.getCodeAsStringConstant();
			final String logData = "This is a test string!";

			int statusCode = aDoc1RESTAPI.logClientData(anonymize, fileId, folderId, fileName, userId, docUID,
					errorCode, logData);
			Assert.assertTrue("Status code is not as expected", statusCode == HttpStatusCode.CREATED.code());
		} catch (final ServerResponseException e) {
			Assert.fail("Server response exception: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_TemplatePreview() throws Exception {
		final int PREVIEW_WIDTH = 120;

		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		try {
			final JSONObject aResult = aDoc1RESTAPI.getTemplateList("text");

			Assert.assertTrue("Unexpected result for 'gettemplatelist'", (aResult != null));
			Assert.assertNotNull("Result contains no mandatory property 'templates'", aResult.opt("templates"));
			Assert.assertNotNull("Result contains no mandatory property 'error'", aResult.opt("error"));

			final JSONArray aTemplatesList = aResult.getJSONArray("templates");
			final JSONObject aErrorObject = aResult.getJSONObject("error");

			final ErrorCode aError = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
			Assert.assertTrue("Result error should be 'NO_ERROR'", aError.isNoError());

			Assert.assertTrue("Template list contains invalid property values",
					TemplateListVerifier.verifyTemplateList(aTemplatesList));

			for (int i = 0; i < aTemplatesList.length(); i++) {
				final JSONObject aTemplateListEntry = aTemplatesList.getJSONObject(i);
				final String sFileId = aTemplateListEntry.getString(TemplateProperties.PROPERTY_FILE_ID);
				final String sFolderId = aTemplateListEntry.getString(TemplateProperties.PROPERTY_FOLDER_ID);
				// ensure that we only test with global templates
				if (sFileId.startsWith("template:")) {
					final InputStream aImageStream = aDoc1RESTAPI.templatePreview(sFileId, sFolderId, PREVIEW_WIDTH);

					Assert.assertNotNull("Bad answer from server, image stream is null", aImageStream);

					final ImageInfo aImageInfo = ImageInfo.getImageInfo(aImageStream);
					Assert.assertTrue("Image width is not as expected",
							(aImageInfo.getDimension().getWidth() == PREVIEW_WIDTH));
				}
			}
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		} catch (final ImageFormatException e) {
			Assert.fail("Server sent broken image or used an unknown image format: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_SpellcheckParagraph() throws Exception {
		final String osName = System.getProperty("os.name");

		if (osName.indexOf("Linux") >= 0) {
			final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
			final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

			final OXDocSpellcheckRestAPI spellcheckAPI = aAppsuite.getSpellcheckRestAPI();
			final String appType = "text";
			final String locale = "en-US";

			// spellcheck a correct paragraph text
			final String goodParagraphText = "The quick brown fox jumps over the lazy dog.";
			JSONObject result = spellcheckAPI.spellcheckParagraph(goodParagraphText, appType, locale);

			Assert.assertNotNull(result);
			JSONArray spellResult = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellResult.length() == 0);

			// spellcheck an incorrect paragraph text
			final String wrongWord1 = "queck";
			final String wrongWord2 = "junps";
			final String wrongWord3 = "laty";
			final String wrongParagraphText = "The " + wrongWord1 + " brown fox " + wrongWord2 + " over the " + wrongWord3 + " dog.";
			result = spellcheckAPI.spellcheckParagraph(wrongParagraphText, appType, locale);

			// [{"start":4,"length":5,"replacements":["quick","quack"],"locale":"en_US","word":"queck"},
			//  {"start":20,"length":5,"replacements":["jumps","junks"],"locale":"en_US","word":"junps"},
			//  {"start":35,"length":4,"replacements":["lat","lay","laity","platy","late","lats","lacy","lady","lath","lazy","Katy","laty"],"locale":"en_US","word":"laty"}]
			Assert.assertNotNull(result);
			spellResult = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellResult.length() == 3);

			// check correction for error "queck"
			final JSONObject correction1 = spellResult.getJSONObject(0);
			Assert.assertEquals(correction1.getInt("start"), 4);
			Assert.assertEquals(correction1.getInt("length"), wrongWord1.length());
			Assert.assertEquals(correction1.getString("word"), wrongWord1);

			// check correction for error "junps"
			final JSONObject correction2 = spellResult.getJSONObject(1);
			Assert.assertEquals(correction2.getInt("start"), 20);
			Assert.assertEquals(correction2.getInt("length"), wrongWord2.length());
			Assert.assertEquals(correction2.getString("word"), wrongWord2);

			// check correction for error "laty"
			final JSONObject correction3 = spellResult.getJSONObject(2);
			Assert.assertEquals(correction3.getInt("start"), 35);
			Assert.assertEquals(correction3.getInt("length"), wrongWord3.length());
			Assert.assertEquals(correction3.getString("word"), wrongWord3);
		} else {
			// no support for other operating systems
		}
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_SpellcheckWord() throws Exception {
		final String osName = System.getProperty("os.name");

		if (osName.indexOf("Linux") >= 0) {
			final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
			final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

			final OXDocSpellcheckRestAPI spellcheckAPI = aAppsuite.getSpellcheckRestAPI();
			final String appType = "text";
			final String locale = "en-US";

			// spellcheck a correct/known word
			final String goodWord = "brown";
			JSONObject result = spellcheckAPI.spellcheckWord(goodWord, appType, locale);

			Assert.assertNotNull(result);
			JSONArray spellResult = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellResult.length() == 0);

			// spellcheck an incorrect/unknown word
			final String wrongWord = "junps";
			result = spellcheckAPI.spellcheckWord(wrongWord, appType, locale);

			Assert.assertNotNull(result);
			spellResult = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellResult.length() == 1);

			// check correction for error "junps"
			final JSONObject correction = spellResult.getJSONObject(0);
			Assert.assertEquals(correction.getInt("start"), 0);
			Assert.assertEquals(correction.getInt("length"), wrongWord.length());
			Assert.assertEquals(correction.getString("word"), wrongWord);
			Assert.assertNotNull(correction.getJSONArray("replacements"));
			Assert.assertTrue((correction.getJSONArray("replacements").length() > 0));
		} else {
			// no support for other operating systems
		}
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_SpellcheckWordReplacements() throws Exception {
		final String osName = System.getProperty("os.name");

		if (osName.indexOf("Linux") >= 0) {
			final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
			final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

			final OXDocSpellcheckRestAPI spellcheckAPI = aAppsuite.getSpellcheckRestAPI();
			final String appType = "text";
			final String locale = "en-US";

			// find replacements a correct/known word
			final String goodWord = "brown";
			JSONObject result = spellcheckAPI.spellcheckWordReplacements(goodWord, appType, locale);

			Assert.assertNotNull(result);
			JSONArray spellReplacements = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellReplacements.length() >= 0);

			// find replacements an incorrect/unknown word
			final String wrongWord = "junps";
			result = spellcheckAPI.spellcheckWordReplacements(wrongWord, appType, locale);

			Assert.assertNotNull(result);
			spellReplacements = result.getJSONArray(SPELL_RESULT);
			Assert.assertTrue(spellReplacements.length() >= 0);
		} else {
			// no support for other operating systems
		}
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_SpellcheckSupportedLocales() throws Exception {
		final String osName = System.getProperty("os.name");

		if (osName.indexOf("Linux") >= 0) {
			final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
			final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

			final OXDocSpellcheckRestAPI spellcheckAPI = aAppsuite.getSpellcheckRestAPI();
			JSONObject result = spellcheckAPI.spellcheckSupportedLocales();

			Assert.assertNotNull(result);
			JSONArray spellReplacements = result.getJSONArray("SupportedLocales");
			Assert.assertTrue(spellReplacements.length() > 0);
		} else {
			// no support for other operating systems
		}
	}

    // -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_GetDocumentState() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew();

        aDocUser01.open();

        aDocUser01.applyOps();
        aDocUser01.applyOps();

        aDocUser01.close();

        final String folderId = aDocUser01.getFolderId();
        final String fileId = aDocUser01.getFileId();

        try {
            final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();
            final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
            final OXFilesRestAPI filesRestAPI = new  OXFilesRestAPI();

            docConfigAPI.bind(aAppsuite01);
            aDoc1RESTAPI.bind(aAppsuite01);
            filesRestAPI.bind(aAppsuite01);
            final JSONObject aResult = aDoc1RESTAPI.getDocumentState(folderId, fileId);

            Assert.assertNotNull(aResult);
            JSONObject data = aResult.optJSONObject("data");
            Assert.assertNotNull(data);
            JSONObject syncInfo = data.optJSONObject("syncInfo");
            if (syncInfo != null) {
                Assert.assertTrue(syncInfo.getInt("document-osn") > 0);

            } else {
                Assert.fail("Answer from 'getdocumentstate' does not provide 'syncInfo' property. Error=" + data.toString());
            }
            filesRestAPI.deleteFile(folderId, fileId);
        } catch (final ServerResponseException e) {
            Assert.fail("Server sent not expected response: " + e.getMessage());
        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	private int createDocumentAndApplyDefaultOpsAndCloseIt(final EditableDoc aDoc) throws Exception {
		aDoc.createNew();

		int nCountApplyOps = 0;

		aDoc.open();

		aDoc.applyOps();
		++nCountApplyOps;
		aDoc.applyOps();
		++nCountApplyOps;

		aDoc.close();

		return nCountApplyOps;
	}

	// -------------------------------------------------------------------------
	private boolean pdfStreamVerifier(final InputStream aResultInStream) throws Exception {
		final String sPDFContent = IOUtils.toString(aResultInStream, "utf-8");
		final int indexPDF = sPDFContent.indexOf("PDF");
		final int indexPages = sPDFContent.indexOf("/Type/Pages");

		return (indexPDF >= 0) && (indexPages >= 0) && (indexPages > indexPDF);
	}

	// -------------------------------------------------------------------------
	private boolean restoreVerifier(final JSONObject aRestoreResult) throws Exception {
		return ((null != aRestoreResult) && (!aRestoreResult.keySet().isEmpty())
				&& (StringUtils.isNotEmpty(aRestoreResult.getString("restoredFolderId")))
				&& (StringUtils.isNotEmpty(aRestoreResult.getString("restoredFileId"))) && (isNoError(aRestoreResult)));
	}

	// -------------------------------------------------------------------------
	private boolean hasTextDocExpectedTextContent(JSONObject officeSettings, JSONObject getOpsResult, int exptectedCount) throws Exception {
        final boolean fastLoad = getOptJSONObject(officeSettings,"text", new JSONObject()).optBoolean("useFastLoad", true);

		if (fastLoad) {
			final String sHtml = getOpsResult.getString(MessageProperties.PROP_HTMLDOC);

			return htmlVerifier(sHtml, TextDoc.STR_TEXT, exptectedCount);
		}

		final JSONObject insertTextOp = new JSONObject();
		insertTextOp.put(OperationConstants.OPERATION_NAME, SupportedOperations.OPERATION_INSERTTEXT);
		insertTextOp.put(OperationConstants.OPERATION_TEXT, TextDoc.STR_TEXT);
		OperationsUtils.compressObject(insertTextOp);
		return operationsVerifier(getOpsResult.getJSONArray("operations"), insertTextOp, exptectedCount);
	}

	// -------------------------------------------------------------------------
	private boolean hasPresentationDocExpectedTextContent(JSONObject officeSettings, JSONObject getOpsResult, int exptectedCount) throws Exception {
		final boolean fastLoad = getOptJSONObject(officeSettings, "presentation", new JSONObject()).optBoolean("useFastLoad", true);

		if (fastLoad) {
			final String sHtml = getOpsResult.getString(MessageProperties.PROP_HTMLDOC);

			return htmlVerifier(sHtml, TextDoc.STR_TEXT, exptectedCount);
		}

		final JSONObject insertTextOp = new JSONObject();
		insertTextOp.put(OperationConstants.OPERATION_NAME, SupportedOperations.OPERATION_INSERTTEXT);
		insertTextOp.put(OperationConstants.OPERATION_TEXT, PresentationDoc.STR_TEXT + PresentationDoc.STR_TEXT);
		OperationsUtils.compressObject(insertTextOp);
		return operationsVerifier(getOpsResult.getJSONArray("operations"), insertTextOp, (exptectedCount / 2));
	}

	// -------------------------------------------------------------------------
	private boolean operationsVerifier(final JSONArray aOperations, final JSONObject aOpToFind, long nCount)
			throws Exception {
		final List<JSONObject> aOpsList = OperationsVerification.createOperationsList(aOperations);
		final long found = OperationsVerification.numOfMatchedOperations(aOpsList, aOpToFind);
		return (found == nCount);
	}

	// -------------------------------------------------------------------------
	private boolean htmlVerifier(final String sHtmlDocString, final String sStringToFind, int nCount) throws Exception {
		return (HtmlChecker.numOfTextInHtml(sHtmlDocString, sStringToFind) == nCount);
	}

	// -------------------------------------------------------------------------
	private boolean deleteResultVerifier(final JSONArray aDeleteResult) {
		return ((null != aDeleteResult) && (aDeleteResult.length() == 0));
	}

	// -------------------------------------------------------------------------
	private JSONObject getLoadDataFromDocument(final OXAppsuite aAppsuite, String sFolderId, String sFileId)
			throws Exception {
		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();

		aDoc1RESTAPI.bind(aAppsuite);
		return aDoc1RESTAPI.getOperations(sFolderId, sFileId);
	}

	// -------------------------------------------------------------------------
	private boolean isNoError(final JSONObject aResultObject) throws Exception {
		final JSONObject aErrorObject = aResultObject.getJSONObject("error");
		final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aErrorObject, ErrorCode.NO_ERROR);
		return aErrorCode.isNoError();
	}

    // -------------------------------------------------------------------------
	private JSONObject getOptJSONObject(JSONObject settings, String optPropName, JSONObject defaultValue) {
	    JSONObject result = null;
	    if (settings != null) {
	        result = settings.optJSONObject(optPropName);
	    }
        return (result == null) ? defaultValue : result;
	}
}
