/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

import org.apache.commons.lang.Validate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.protocol.RT2Protocol;

public class DocContentVerifierHelper
{
    //-------------------------------------------------------------------------
	private DocContentVerifierHelper() {}

    //-------------------------------------------------------------------------
	public static boolean isContentAsExpected(final EditableDoc aDocToVerify, long nTimeoutMS, final Predicate<DocContentAccessor> aVerifier) throws Exception {
		Validate.notNull(aDocToVerify);
		Validate.notNull(aVerifier);

        final AtomicReference<String> aHtmlDocData             = new AtomicReference<>();
        final CountDownLatch          aOpenDocResponseReceived = new CountDownLatch(1);

        aDocToVerify.addBroadcastObserver(b ->
        {
            if ((b.getType().equals(RT2Protocol.RESPONSE_OPEN_DOC)) ||
                (b.getType().equals(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK)))
            {
                final JSONObject aJSONBody = b.getBody();
                if (aJSONBody.has("htmlDoc"))
                    aHtmlDocData.compareAndSet(null, aJSONBody.getString("htmlDoc"));

                aOpenDocResponseReceived.countDown();
            }
        });

        // open & close document again to check that operations were stored
        aDocToVerify.open     ();
        boolean bReceived = aOpenDocResponseReceived.await(nTimeoutMS, TimeUnit.MILLISECONDS);
        aDocToVerify.close    ();

        Assert.assertTrue("User should received RESPONSE_OPEN_DOC/RESPONSE_OPEN_DOC_CHUNK before time-out. Something went wrong for docuid: " + aDocToVerify.getDocUID() + " and clientuid: " + aDocToVerify.getRT2ClientUID(), bReceived);

        // check that reloaded document contains the text inserted by the insertText operations (applyOps)
        return aVerifier.test(new DocContentAccessor(aHtmlDocData.get(), aDocToVerify.getRecordedOperations(), aDocToVerify.getType()));
	}

    //-------------------------------------------------------------------------
	public static boolean isContentAsExpected(final String sHtmlDoc, final List<JSONObject> aOps, String sDocType, final Predicate<DocContentAccessor> aVerifier) throws Exception {
        // check that reloaded document contains the text inserted by the insertText operations (applyOps)
        return aVerifier.test(new DocContentAccessor(sHtmlDoc, aOps, sDocType));
	}

    //-------------------------------------------------------------------------
	public static void verifyDocumentContent(OXAppsuite appsuite, Doc aDocToCheck, String text, int count) throws Exception {
		String folderId = aDocToCheck.getFolderId();
		String fileId = aDocToCheck.getFileId();
		String type = aDocToCheck.getType();
		String docUid = aDocToCheck.getDocUID();
		String driveDocId = aDocToCheck.getDriveDocId();

		final Doc aDocToVerify = TestSetupHelper.newDocInst(appsuite, type, folderId, fileId, driveDocId, true);

		boolean verified = false;
		switch (type) {
		case Doc.DOCTYPE_TEXT: verified = TextDocumentContentVerifier.checkTextDocumentContent((TextDoc)aDocToVerify, text, count); break;
		case Doc.DOCTYPE_PRESENTATION: verified = PresentationDocumentContentVerifier.checkTextDocumentContent((PresentationDoc)aDocToVerify, text, count) ; break;
		default: Assert.fail("Document type not supported!"); break;
		}
		Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored! DocUid: "
						+ docUid + ", file-id:" + aDocToCheck.getFileId() + ", clientUid: " + aDocToVerify.getRT2ClientUID(), verified);
	}

    //-------------------------------------------------------------------------
	public static void verifySpreadsheetDocumentContent(OXAppsuite appsuite, SpreadsheetDoc aDocToCheck) throws Exception {
		String folderId = aDocToCheck.getFolderId();
		String fileId = aDocToCheck.getFileId();
		String docUid = aDocToCheck.getDocUID();
		String driveDocId = aDocToCheck.getDriveDocId();

		final SpreadsheetDoc aDocToVerify = TestSetupHelper.newDocInst(appsuite, Doc.DOCTYPE_SPREADSHEET, folderId, fileId, driveDocId, true);
		final JSONArray appliedOps = aDocToCheck.getRecordedAppliedOperations();
		boolean verified = SpreadsheetDocumentVerifier.checkDocumentContent(aDocToVerify, appliedOps);

		Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored! DocUid: "
				+ docUid + ", file-id:" + aDocToCheck.getFileId() + ", clientUid: " + aDocToVerify.getRT2ClientUID(), verified);
	}

    //-------------------------------------------------------------------------
	public static void verifySpreadsheetDocumentContentAndStateForMultiSheet(OXAppsuite appsuite, SpreadsheetDoc aDocToCheck) throws Exception {
        String folderId = aDocToCheck.getFolderId();
        String fileId = aDocToCheck.getFileId();
        String docUid = aDocToCheck.getDocUID();
        String driveDocId = aDocToCheck.getDriveDocId();

        final SpreadsheetDoc aDocToVerify = TestSetupHelper.newDocInst(appsuite, Doc.DOCTYPE_SPREADSHEET, folderId, fileId, driveDocId, true);
        final JSONArray appliedOps = aDocToCheck.getRecordedAppliedOperations();
        boolean verified = SpreadsheetDocumentVerifier.checkDocumentContent(aDocToVerify, appliedOps);

        Assert.assertTrue("No operations with preview flag received - Two-stage load for Spreadsheet broken?",aDocToVerify.werePreviewOpsReceivedOnOpen());
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored! DocUid: "
                + docUid + ", file-id:" + aDocToCheck.getFileId() + ", clientUid: " + aDocToVerify.getRT2ClientUID(), verified);
	}
}
