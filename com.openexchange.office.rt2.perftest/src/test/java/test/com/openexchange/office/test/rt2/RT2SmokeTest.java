/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;

import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.DocHelper;
import test.com.openexchange.office.test.rt2.utils.RT2JmxClient;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2SmokeTest {

    private final int TIME_CYCLE_FOR_ACKS_MS = 30000;

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv() throws Exception {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSmokeTestSingleDocumentAndCleanBackend() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final RT2JmxClient aJmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);

        aJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aJmxClient.isConnected());

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
            serverStateStarted = serverState.start();

            // check clean-up of backend resources as good as possible
            Integer countDocProcessors = aJmxClient.getCountDocProcessors();
            Integer countDocProxies = aJmxClient.getCountDocProxies();

            // values for doc processor and doc proxy must be available
            Assert.assertNotNull(countDocProcessors);
            Assert.assertNotNull(countDocProxies);
            Assert.assertTrue(countDocProcessors >= 0);
            Assert.assertTrue(countDocProxies >= 0);

            Integer numOfMsgWaitingForAck = null;
            Set<String> aSetWithDocUIDs = null;
            int nCountApplyOps = 0;

            //-------------------------------------------------------------------------
            // Test text document
            final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
            aTextDoc.createNew();
            aTextDoc.open();
            aTextDoc.applyOps();
            ++nCountApplyOps;

            // check that we can find a value for msgs without ACK for our client/doc connection
            numOfMsgWaitingForAck = aJmxClient.getMessageCountWaitingForAckOfDoc(aTextDoc.getRT2ClientUID(), aTextDoc.getDocUID());
            Assert.assertTrue(numOfMsgWaitingForAck != null);

            // wait for the ACK timer to expire to send ACK messages and reset count to zero
            Thread.sleep(TIME_CYCLE_FOR_ACKS_MS + 1000);
            numOfMsgWaitingForAck = aJmxClient.getMessageCountWaitingForAckOfDoc(aTextDoc.getRT2ClientUID(), aTextDoc.getDocUID());
            Assert.assertTrue((numOfMsgWaitingForAck != null) && (numOfMsgWaitingForAck == 0));

            // check that count of doc processor & proxy is consistent
            countDocProcessors = aJmxClient.getCountDocProcessors();
            countDocProxies = aJmxClient.getCountDocProxies();

            // values for doc processor and doc proxy must be available
            Assert.assertNotNull(countDocProcessors);
            Assert.assertNotNull(countDocProxies);
            Assert.assertTrue(countDocProcessors > 0);
            Assert.assertTrue(countDocProxies > 0);

            aTextDoc.close();

            // verify the document content that must include the text one time
    		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);

            //-------------------------------------------------------------------------
            // Test spreadsheet document
            final SpreadsheetDoc aCalcDoc = (SpreadsheetDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_SPREADSHEET);

            aCalcDoc.enableAppliedOperationsRecording(true);

            aCalcDoc.createNew();
            aCalcDoc.open();
            aCalcDoc.applyOps();
            aCalcDoc.close();

            // verify the document content that must include the values inserted before
            DocContentVerifierHelper.verifySpreadsheetDocumentContent(aAppsuite, aCalcDoc);

            //-------------------------------------------------------------------------
            // Test presentation document
            final PresentationDoc aPresDoc = (PresentationDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_PRESENTATION);

            nCountApplyOps = 0;
            aPresDoc.createNew();
            aPresDoc.open();
            aPresDoc.applyOps();
            ++nCountApplyOps;
            aPresDoc.close();

            // verify the document content that must include the text one time
    		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aPresDoc, PresentationDoc.STR_TEXT, nCountApplyOps);

            //-------------------------------------------------------------------------
            // Verify clean-up of server state after document has been closed
            aSetWithDocUIDs = DocHelper.createClosedDocUIDs(aTextDoc, aCalcDoc, aPresDoc);
            Assert.assertTrue(serverState.checkServerState(aEnvNode, aSetWithDocUIDs));
        } finally {
            if (serverStateStarted) {
                serverState.stopAndShutdown();
            }
        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

}
