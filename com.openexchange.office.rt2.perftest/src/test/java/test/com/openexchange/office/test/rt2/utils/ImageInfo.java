/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;

import com.openexchange.office.rt2.perftest.operations.Dimension;

//=============================================================================
public class ImageInfo
{
    //-------------------------------------------------------------------------
	public static final String EXTENSION_JPG    = "jpg";
	public static final String EXTENSION_PNG    = "png";
	public static final String EXTENSION_TIFF   = "tif";
	public static final String EXTENSION_GIF    = "gif";
	public static final String EXTENSION_WMF    = "wmf";

    //-------------------------------------------------------------------------
	public static final String MIMETYPE_JPG     = "image/jpg";
	public static final String MIMETYPE_PNG     = "image/png";
	public static final String MIMETYPE_TIFF    = "image/tiff";
	public static final String MIMETYPE_GIF     = "image/gif";
	public static final String MIMETYPE_WMF     = "image/wmf";
	public static final String MIMETYPE_UNKNOWN = "application/octet-stream";

    //-------------------------------------------------------------------------
    private static final byte[] jpgFormatPrefix = new byte[]
    { 
        (byte) 0xff, (byte) 0xd8
    };

    //-------------------------------------------------------------------------
    private static final byte[] pngFormatPrefix = new byte[]
    { 
        (byte)0x89, (byte)0x50, (byte)0x4e, (byte)0x47, 
        (byte)0x0d, (byte)0x0a, (byte)0x1a, (byte)0x0a
    };

    //-------------------------------------------------------------------------
    private static final byte[] tiffFormatPrefix1 = new byte[]
    { 
        (byte) 'I', (byte) 'I', (byte)0x2a, (byte)0x00
    };

    //-------------------------------------------------------------------------
    private static final byte[] tiffFormatPrefix2 = new byte[]
    { 
        (byte) 'M', (byte) 'M', (byte)0x00, (byte)0x2a
    };

    //-------------------------------------------------------------------------
    private static final byte[] gifFormatHeader1 = new byte[] {
        (byte) 'G', (byte)'I', (byte)'F',
        (byte) '8', (byte)'7', (byte)'a'
    };

    //-------------------------------------------------------------------------
    private static final byte[] gifFormatHeader2 = new byte[] {
        (byte) 'G', (byte)'I', (byte)'F',
        (byte) '8', (byte)'9', (byte)'a'
    };

    //-------------------------------------------------------------------------
    private static final byte[] wmfFormatHeader = new byte[] {
        (byte)0xd7, (byte)0xcd, (byte)0xc6, (byte)0x9a
    };

    //-------------------------------------------------------------------------
	private Dimension aDimension;

    //-------------------------------------------------------------------------
	private String    aMimeType;

    //-------------------------------------------------------------------------
    public ImageInfo()
	{
		aDimension = new Dimension(0, 0);
		aMimeType  = "octet-stream";
	}

    //-------------------------------------------------------------------------
	public ImageInfo(Dimension aDimension, String aMimeType)
	{
		this.aDimension = aDimension;
		this.aMimeType  = aMimeType;
	}

    //-------------------------------------------------------------------------
	public Dimension getDimension()
	{
		return aDimension;
	}

    //-------------------------------------------------------------------------
	public String getMimeType()
	{
		return aMimeType;
	}

    //-------------------------------------------------------------------------
	public static String getImageFormat(final byte[] aImageData)
	{
		if (isJPGData(aImageData))
			return MIMETYPE_JPG;
		else if (isPNGData(aImageData))
			return MIMETYPE_PNG;
		else if (isTIFFData(aImageData))
			return MIMETYPE_TIFF;
		else if (isWMFData(aImageData))
		    return MIMETYPE_WMF;
		else
			return MIMETYPE_UNKNOWN;
	}

    //-------------------------------------------------------------------------
    public static boolean isJPGData(final byte[] aImageData)
    {
        return ArrayHelper.startsWith(aImageData, jpgFormatPrefix);
    }

    //-------------------------------------------------------------------------
    public static boolean isPNGData(final byte[] aImageData)
    {
        return ArrayHelper.startsWith(aImageData, pngFormatPrefix);
    }

    //-------------------------------------------------------------------------
    public static boolean isTIFFData(final byte[] aImageData)
    {
        return (ArrayHelper.startsWith(aImageData, tiffFormatPrefix1) ||
               (ArrayHelper.startsWith(aImageData, tiffFormatPrefix2)));
    }

    //-------------------------------------------------------------------------
    public static boolean isGIFData(final byte[] aImageData)
    {
        return (ArrayHelper.startsWith(aImageData, gifFormatHeader1) ||
               (ArrayHelper.startsWith(aImageData, gifFormatHeader2)));
    }

    public static boolean isWMFData(final byte[] imageData) {
        return ArrayHelper.startsWith(imageData, wmfFormatHeader);
    }

    //-------------------------------------------------------------------------
    public static ImageInfo getImageInfo(final InputStream aImageInputStream)
        throws IOException
    {
        final byte[] aImageData = IOUtils.toByteArray(aImageInputStream);
        return getImageInfo(aImageData);
    }

    //-------------------------------------------------------------------------
    public static ImageInfo getImageInfo(final byte[] aImageData)
        throws IOException, ImageFormatException
    {
        int    nWidth    = -1;
        int    nHeight   = -1;
        String sMimeType = getImageFormat(aImageData);

        final ByteArrayInputStream aInStream = new ByteArrayInputStream(aImageData);
        final BufferedImage aBufferedImage = ImageIO.read(aInStream);

        if (null != aBufferedImage)
        {
            nWidth  = aBufferedImage.getWidth();
            nHeight = aBufferedImage.getHeight();
            aInStream.close();
        }
        else
        {
            throw new ImageFormatException();
        }

        return new ImageInfo(new Dimension(nWidth, nHeight), sMimeType);
    }

}
