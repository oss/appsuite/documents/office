/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.websocket.CloseReason;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.AsyncResult;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXConfigRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXWSClient;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OperationConstants;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.tools.error.ErrorCode;

import test.com.openexchange.office.test.rt2.utils.JMXHelper;
import test.com.openexchange.office.test.rt2.utils.RT2MessageDeferredHandler;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.SessionJmxClient;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

//=============================================================================
public class RT2DocumentErrorTests extends RT2TestBase {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv () throws Exception {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testJoinAndModifyAsViewer() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final OXConfigRestAPI docConfigAPI = new OXConfigRestAPI();
		docConfigAPI.bind(aAppsuite01);

		// check concurrent editing mode to enable/disable test
		final JSONObject officeSettings = docConfigAPI.list("io.ox/office");
		final boolean concurrentEditing = officeSettings.getJSONObject("text").optBoolean("concurrentEditing", true);
		if (!concurrentEditing) {
	        final TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
	        aDocUser01.createNew();

	        // aDoc2 is read-only and cannot send applyOps - the office backend drops applyOps from viewer
	        // Important: set autocloseonerror to false as otherwise the auto-close just hangs!
	        // This is related to the fact that the auto-close implementation is called in the
	        // context of the single-thread RT2MessageLoop, which won't be able to notify us for
	        // new messages.
	        final EditableDoc aDocUser02 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite02, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

	        int nCountApplyOps = 0;

	        aDocUser01.open     ();
	        aDocUser02.open     ();

	        aDocUser01.applyOps (); ++nCountApplyOps;
	        aDocUser01.applyOps (); ++nCountApplyOps;

	        final AsyncResult<RT2Message> aApplyOpsResult = new AsyncResult<>();
	        final RT2MessageDeferredHandler aDeferredHandler = new RT2MessageDeferredHandler(aApplyOpsResult);

	        // this is wrong and should be answered by the backend with a hangup message
	        // therefore we don't count this apply operation!
	        final Deferred< RT2Message> aResponseDef = aDocUser02.applyOpsAsync ();
	        aResponseDef.done(aDeferredHandler);
	        aResponseDef.fail(aDeferredHandler);

	        // This is the most complicated case as aDoc02.applyOps will trigger an
	        // asynchronous error handler. So we have a race between close and this
	        // error notification. Should also be handled correctly - the checkServerState
	        // will check this.
	        boolean bResponse = aApplyOpsResult.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
	        Assert.assertTrue("Apply operations result not received in time", bResponse);

	        // the result of this wrong apply operations must be a generic error response
	        final RT2Message aResponse = aApplyOpsResult.get();
	        final ErrorCode  aErrorCode = RT2MessageGetSet.getError(aResponse);
	        Assert.assertEquals(RT2Protocol.RESPONSE_GENERIC_ERROR, aResponse.getType());
	        Assert.assertEquals(ErrorCode.HANGUP_NO_EDIT_RIGHTS_ERROR.getCodeAsStringConstant(), aErrorCode.getCodeAsStringConstant());

	        aDocUser01.close    ();
	        aDocUser02.close    ();

	        //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01, aDocUser02)));

	        // verify the document content that must include the text two times
	        boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser01, TextDoc.STR_TEXT, nCountApplyOps);
	        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored or viewer could change it!", bVerified);
		}

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
    }

    //-------------------------------------------------------------------------
    @Ignore
    @Test
    public void testDocumentBackgroundSaveError() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        int nCountApplyOps = 0;

        aDocUser01.open ();

        // register ourself for broadcast messages
        final AtomicReference<ErrorCode> aErrorCodeRef = new AtomicReference<>(ErrorCode.NO_ERROR);
        final CountDownLatch aCountdownLatch = new CountDownLatch(1);
        aDocUser01.addBroadcastObserver(b -> {
            if (b.getType().equals(RT2Protocol.BROADCAST_HANGUP))
            {
                final ErrorCode aErrorCode = RT2MessageGetSet.getError(b);
                aErrorCodeRef.set(aErrorCode);
                aCountdownLatch.countDown();
            }
        });

        for (int i = 0; i < RT2TestConstants.MIN_APPLYOPS_TO_TRIGGER_BACKGROUND_SAVE - 1; i++)
        {
            aDocUser01.applyOps (); ++nCountApplyOps;
            // sleep to behave as the js client (which collect operations
            // for about 1 second to reduce the amount of requests.
            Thread.sleep(250);
        }

        // apply a bad operation to force a filter error on backend side
        final JSONArray  aOpsArray = new JSONArray();
        aOpsArray.put(createBadOperation(aDocUser01));
        aDocUser01.applyOps(aOpsArray);

        // wait for the hangup broadcast which must be sent by the backend
        final boolean bBroadcastReceived = aCountdownLatch.await(RT2TestConstants.DEFAULT_BACKGROUNDSAVE_TIMEOUT, TimeUnit.MILLISECONDS);

        aDocUser01.close (true);

        Assert.assertTrue("Didn't receive hangup broadcast in time. No background save??", bBroadcastReceived);
        Assert.assertTrue("Broadcast did not send an error code. No error occurred?", aErrorCodeRef.get().isError());

        //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01)));

        // We have to create a new instance to verify the document content.
        // As the close() will run into a close response with an error the internal
        // state machine is not set-up correctly.
        for (int i = 0;i<=10; ++i) {
            System.out.println();
        }
        aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

        // Verify that the document content includes the text 49 times, although we
        // applied a bad operation. The backend automatically tries to store as much
        // operations as possible. Therefore we can check that all applyOps before the
        // bad operation have been stored successfully.
        final boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser01, TextDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored or viewer could change it!", bVerified);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testDocumentHandlesSessionInvalid() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        aDocUser01.open ();

        final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
        final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
        Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

        int nCountApplyOps = 0;

        aDocUser01.applyOps (); ++nCountApplyOps;
        aDocUser01.applyOps (); ++nCountApplyOps;

        // register ourself for broadcast messages
        final AtomicBoolean              aException      = new AtomicBoolean(false);
        final AtomicReference<ErrorCode> aErrorCodeRef   = new AtomicReference<>(ErrorCode.NO_ERROR);
        final CountDownLatch             aCountdownLatch = new CountDownLatch(1);
        aDocUser01.addErrorObserver(e -> {
            try
            {
                final ErrorCode aErrorCode = RT2MessageGetSet.getError(e);
                aErrorCodeRef.set(aErrorCode);
                aCountdownLatch.countDown();
            }
            catch (final Exception ex)
            {
                aException.set(true);
                aCountdownLatch.countDown();
            }
        });

        // remove user session
        final String sessionId = aDocUser01.getContext().accessSession().getSessionId();
        aSessionJmxClient.removeClientSession(sessionId);

        // force client to send a request to the backend which must fail now
        aDocUser01.applyOpsAsync();

        boolean bResponse = aCountdownLatch.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertTrue("Error not received in time", bResponse);
        Assert.assertFalse("Error handler should not throw exception", aException.get());

        final ErrorCode aErrorReceived = aErrorCodeRef.get();
        Assert.assertTrue("Error received must be a session invalid error!", (aErrorReceived.isError() && (aErrorReceived.getCode() == ErrorCode.CODE_GENERAL_SESSION_INVALID_ERROR)));

        aDocUser01.close ();

        Assert.assertTrue("The test should be able to apply operations before the invalid session notification happened", (nCountApplyOps > 0));
        // TODO: use the hazelcast instance to do this check - using jmx is not reliable
        //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01)));

        // Attention: Don't call close TestSetupHelper.closeAppsuiteConnections(aAppsuite01)
        // as we already lost our session. It will fail with http error 403.
        aAppsuite01.shutdownWSConnection();
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSessionInvalidWithNotificationWithUnmodifiedDocument() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        aDocUser01.open ();

        final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
        final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
        Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

        long beforeCountInvalidatedSessions = aSessionJmxClient.getCountInvalidatedSessionNotifications();

        // remove user session
        final String sessionId = aDocUser01.getContext().accessSession().getSessionId();
        aSessionJmxClient.removeClientSession(sessionId);

        Thread.sleep(3000);

        long afterCountInvalidatedSessions = aSessionJmxClient.getCountInvalidatedSessionNotifications();
        Assert.assertTrue(afterCountInvalidatedSessions > beforeCountInvalidatedSessions);

        final List<String> latestSessionsInvalidated = aSessionJmxClient.getLatestInvalidatedSessionsViaNotifications();
        Assert.assertNotNull(latestSessionsInvalidated);
        Assert.assertTrue(latestSessionsInvalidated.stream().filter(s -> (s.indexOf(sessionId) > 0)).count() == 1);

        // Check that the document instance was closed due to the session lost notification
        final Set<String> findSet = new HashSet<>();
        findSet.add(aDocUser01.getDocUID());
        final Set<String> found = aSessionJmxClient.findHazelcastDocMapEntries(findSet);
        Assert.assertTrue(found.size() == 0);

        // Attention: Don't call close TestSetupHelper.closeAppsuiteConnections(aAppsuite01)
        // as we already lost our session. It will fail with http error 403.
        aAppsuite01.shutdownWSConnection();
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSessionInvalidWithNotificationWithModifiedDocument() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        aDocUser01.open();

        // modify document
        aDocUser01.applyOps();

        final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
        final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
        Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

        long beforeCountInvalidatedSessions = aSessionJmxClient.getCountInvalidatedSessionNotifications();

        // remove user session
        final String sessionId = aDocUser01.getContext().accessSession().getSessionId();
        aSessionJmxClient.removeClientSession(sessionId);

        Thread.sleep(3000);

        long afterCountInvalidatedSessions = aSessionJmxClient.getCountInvalidatedSessionNotifications();
        Assert.assertTrue(afterCountInvalidatedSessions > beforeCountInvalidatedSessions);

        final List<String> latestSessionsInvalidated = aSessionJmxClient.getLatestInvalidatedSessionsViaNotifications();
        Assert.assertNotNull(latestSessionsInvalidated);
        Assert.assertTrue(latestSessionsInvalidated.stream().filter(s -> (s.indexOf(sessionId) > 0)).count() == 1);

        // Check that the document instance was not closed as it's modified and
        // we hope that a client tries to reconnect before the 30 minutes timeout
        final Set<String> findSet = new HashSet<>();
        findSet.add(aDocUser01.getDocUID());
        final Set<String> found = aSessionJmxClient.findHazelcastDocMapEntries(findSet);
        Assert.assertTrue(found.size() == 1);

        // Attention (1): Don't call close TestSetupHelper.closeAppsuiteConnections(aAppsuite01)
        // as we already lost our session. It will fail with http error 403.
        // Attention (2): This test will leave a living DocProcessor, DocProxy and Hazelcast
        // entries. This is correct and one of the purposes of this special test.
    }

    // -------------------------------------------------------------------------
    @Test
    public void testOfflineWithLostSession() throws Exception {
        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        setNackFrequenceOfServer(aEnvNode);
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

        aDoc.createNew();
        aDoc.open();

        aDoc.applyOps();
        CloseReason closeReason = new CloseReason(CloseReason.CloseCodes.CLOSED_ABNORMALLY, "Test Offline");
        aAppsuite.accessWSClient().terminateSession(closeReason);

        final SessionJmxClient sessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        sessionJmxClient.connect();

        OXWSClient wsClient = aAppsuite.accessWSClient();
        boolean offlineDetected = false;
        for (int i = 0; i <= 10; ++i) {
            if (offlineDetected) {
                break;
            }
            Thread.sleep(1000);
            offlineDetected = sessionJmxClient.getAllOfflineWSChannels().stream()
                    .anyMatch(id -> id.contains(wsClient.getConnectionId()))
                    && sessionJmxClient.getAllActiveWSChannels().stream()
                            .noneMatch(id -> id.contains(wsClient.getConnectionId()));
        }
        Assert.assertTrue(offlineDetected);

        // remove specific user session
        final String sessionID = aDoc.getContext().accessSession().getSessionId();
        sessionJmxClient.removeClientSession(sessionID);
    }

    //-------------------------------------------------------------------------
    @Ignore
    @Test
    public void testDocumentHandlesSaveErrorOnViewerClose() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
        aDocUser01.createNew   ();

        final EditableDoc aDocUser02 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite02, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

        aDocUser01.open ();
        aDocUser02.open ();

        final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
        final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
        Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

        int nCountApplyOps = 0;

        aDocUser01.applyOps (); ++nCountApplyOps;
        aDocUser01.applyOps (); ++nCountApplyOps;

        // register listener for broadcast messages to user 1
        final AtomicBoolean              aException1      = new AtomicBoolean(false);
        final AtomicBoolean              aHangup2         = new AtomicBoolean(false);
        final AtomicReference<ErrorCode> aErrorCodeRef1   = new AtomicReference<>(ErrorCode.NO_ERROR);
        final CountDownLatch             aCountdownLatch1 = new CountDownLatch(1);
        aDocUser01.addBroadcastObserver(b -> {
            if (b.getType().equals(RT2Protocol.BROADCAST_HANGUP))
            {
                final ErrorCode aErrorCode = RT2MessageGetSet.getError(b);
                aErrorCodeRef1.set(aErrorCode);
                aCountdownLatch1.countDown();
            }
        });

        aDocUser02.addBroadcastObserver(b -> {
            if (b.getType().equals(RT2Protocol.BROADCAST_HANGUP)) {
                aHangup2.set(true);
            }
        });

        // apply a bad operation to force a filter error on saving the document
        final JSONArray aOpsArray = new JSONArray();
        aOpsArray.put(createBadOperation(aDocUser01));
        aDocUser01.applyOps(aOpsArray);

        // trigger a save with the bad operation which must fail
        aDocUser02.close (true);

        boolean bResponse = aCountdownLatch1.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertTrue("Error via hangup broadcast not received in time", bResponse);
        Assert.assertFalse("Error handler should not throw exception", aException1.get());
        Assert.assertFalse("BROADCAST_HANGUP should not be sent to aDocUser2!", aHangup2.get());

        final ErrorCode aErrorReceived1 = aErrorCodeRef1.get();
        Assert.assertTrue("Error received must be a save failed due to file operation error invalid error!", isSameError(aErrorReceived1, ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR));

        aDocUser01.close (true);

        Assert.assertTrue("The test should be able to apply more thane zero operations", (nCountApplyOps > 0));
        //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01)));

        // We have to create a new instance to verify the document content.
        // As the close() will run into a close response with an error the internal
        // state machine is not set-up correctly.
        aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

        // Verify that the document content includes the text two times, although we
        // applied a bad operation. The backend automatically tries to store as much
        // operations as possible. Therefore we can check that both applyOps before the
        // bad operation have been stored successfully.
        final boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser01, TextDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored or viewer could change it!", bVerified);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
    }

    //-------------------------------------------------------------------------
    @Ignore
    public void testDocumentBackgroundSaveAndAfterwardsSessionInvalid() throws Exception {
        testDocumentBackgroundSaveError();
        testDocumentHandlesSessionInvalid();
    }

    //-------------------------------------------------------------------------
    private static JSONObject createBadOperation(final Doc aDoc) throws Exception {
        final JSONObject aBadOp = new JSONObject();

        aBadOp.put(OperationConstants.OPERATION_OSN, aDoc.getOSN());
        aBadOp.put(OperationConstants.OPERATION_OPL, 1);
        aBadOp.put(OperationConstants.OPERATION_NAME, OperationConstants.OPERATION_CREATEERROR);
        OperationsUtils.compressObject(aBadOp);

        return aBadOp;
    }

    //-------------------------------------------------------------------------
    private boolean isSameError(final ErrorCode aError1, final ErrorCode aError2) {
        return (aError1.getCode() == aError2.getCode());
    }

}
