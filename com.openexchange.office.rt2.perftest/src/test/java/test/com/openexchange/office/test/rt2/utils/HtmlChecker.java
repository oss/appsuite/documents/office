/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlChecker
{
	private static final Logger log = LoggerFactory.getLogger(HtmlChecker.class);
	
    //-------------------------------------------------------------------------
	private HtmlChecker() {}

    //-------------------------------------------------------------------------
	/**
	 * Count the occurrence of a specified string in the provided
	 * html string.</br>
	 * <b>ATTENTION:</b> This code does NOT use a html parser therefore it
	 * won't find text which is separated on several spans.
	 *
	 * @param sHtmlString the html string provided by fast-load which must
	 *  not be null
	 * @param sText the text to be counted which must not be null
	 * @return the number of occurrences of the provided text in the
	 *  html string.
	 */
	public static int numOfTextInHtml(String sHtmlString, String sText)
	{
        Validate.notNull(sHtmlString);
        Validate.notNull(sText);

        final String sTextToFind = StringEscapeUtils.escapeHtml4(sText);
        final int    nTextLength = sTextToFind.length();

        boolean bEnd       = false;
        int     nCurrIndex = 0;
        int     nCount     = 0;

        while (!bEnd)
        {
            nCurrIndex = sHtmlString.indexOf(sTextToFind, nCurrIndex);
            bEnd       = (nCurrIndex < 0);
            nCount     = (!bEnd) ? nCount + 1 : nCount;
            nCurrIndex = bEnd ? nCurrIndex : (nCurrIndex + nTextLength);
        }
        log.info("numOfTextInHtml, sText: " + sText + ", count: " + nCount);
        System.out.println("numOfTextInHtml, sText: " + sText + ", count: " + nCount);        
        return nCount;
	}

	public static boolean containsImageWithID(String sHtmlString, String sImageID)
	{
        Validate.notNull(sHtmlString);
        Validate.notNull(sImageID);

        final String sTextToFind = StringEscapeUtils.escapeHtml4(sImageID);
        final int    nTextLength = sTextToFind.length();

        boolean bEnd       = false;
        int     nCurrIndex = 0;
        int     nCount     = 0;

        while (!bEnd)
        {
            nCurrIndex = sHtmlString.indexOf(sTextToFind, nCurrIndex);
            bEnd       = (nCurrIndex < 0);
            nCount     = (!bEnd) ? nCount + 1 : nCount;
            nCurrIndex = bEnd ? nCurrIndex : (nCurrIndex + nTextLength);
        }

        return (nCount >= 1);
	}
}
