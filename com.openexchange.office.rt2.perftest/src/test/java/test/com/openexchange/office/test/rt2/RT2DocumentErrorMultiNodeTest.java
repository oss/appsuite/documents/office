/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OperationConstants;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.tools.error.ErrorCode;

import test.com.openexchange.office.test.rt2.utils.JMXHelper;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.SessionJmxClient;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2DocumentErrorMultiNodeTest {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv ()
        throws Exception
    {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testDocumentHandlesSaveErrorOnViewerCloseWithManyClientsOnDifferentNodes()
        throws Exception
    {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final TestRunConfig aEnvNode02  = TestRunConfigHelper.defineEnv4Node02 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
        final OXAppsuite    aAppsuite03 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();

            TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
            aDocUser01.createNew   ();

            final EditableDoc aDocUser02 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite02, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);
            final EditableDoc aDocUser03 = (EditableDoc)TestSetupHelper.newDocInst (aAppsuite03, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

            aDocUser01.open ();
            aDocUser02.open ();
            aDocUser03.open ();

            final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
            final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
            Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

            int nCountApplyOps = 0;

            aDocUser01.applyOps (); ++nCountApplyOps;
            aDocUser01.applyOps (); ++nCountApplyOps;

            // register listener for broadcast messages to user 1
            final AtomicBoolean              aException1      = new AtomicBoolean(false);
            final AtomicReference<ErrorCode> aErrorCodeRef1   = new AtomicReference<>(ErrorCode.NO_ERROR);
            final CountDownLatch             aCountdownLatch1 = new CountDownLatch(1);
            aDocUser01.addBroadcastObserver(b -> {
                if (b.getType().equals(RT2Protocol.BROADCAST_HANGUP))
                {
                    final ErrorCode aErrorCode = RT2MessageGetSet.getError(b);
                    aErrorCodeRef1.set(aErrorCode);
                    aCountdownLatch1.countDown();
                }
            });

            // register listener for broadcast messages to user 3
            final AtomicBoolean              aException3      = new AtomicBoolean(false);
            final AtomicReference<ErrorCode> aErrorCodeRef3   = new AtomicReference<>(ErrorCode.NO_ERROR);
            final CountDownLatch             aCountdownLatch3 = new CountDownLatch(1);
            aDocUser03.addBroadcastObserver(b -> {
                if (b.getType().equals(RT2Protocol.BROADCAST_HANGUP))
                {
                    final ErrorCode aErrorCode = RT2MessageGetSet.getError(b);
                    aErrorCodeRef3.set(aErrorCode);
                    aCountdownLatch3.countDown();
                }
            });

            // apply a bad operation to force a filter error on saving the document
            final JSONArray  aOpsArray = new JSONArray();
            aOpsArray.put(createBadOperation(aDocUser01));
            aDocUser01.applyOps(aOpsArray);

            // trigger a save with the bad operation which must fail
            aDocUser02.close (true);

            // wait for the broadcast to user 1
            boolean bResponse1 = aCountdownLatch1.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Error via hangup broadcast not received in time", bResponse1);
            Assert.assertFalse("Error handler should not throw exception", aException1.get());

            // wait for the broadcast to user 3
            boolean bResponse3 = aCountdownLatch3.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Error via hangup broadcast not received in time", bResponse3);
            Assert.assertFalse("Error handler should not throw exception", aException3.get());

            final ErrorCode aErrorReceived1 = aErrorCodeRef1.get();
            Assert.assertTrue("Error received must be a save failed due to file operation error invalid error! Received: " + aErrorReceived1.getCodeAsStringConstant(), isSameError(aErrorReceived1, ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR));

            // close the document for user 1 & 3
            aDocUser01.close (true);
            aDocUser03.close (true);

            Assert.assertTrue("The test should be able to apply more thane zero operations", (nCountApplyOps > 0));
            //Assert.assertTrue (ServerStateHelper.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01)));

            // We have to create a new instance to verify the document content.
            // As the close() will run into a close response with an error the internal
            // state machine is not set-up correctly.
            aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, aDocUser01.getFolderId(), aDocUser01.getFileId(), aDocUser01.getDriveDocId(), false);

            // Verify that the document content includes the text two times, although we
            // applied a bad operation. The backend automatically tries to store as much
            // operations as possible. Therefore we can check that both applyOps before the
            // bad operation have been stored successfully.
            final boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser01, TextDoc.STR_TEXT, nCountApplyOps);
            Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored or viewer could change it!", bVerified);
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
        TestSetupHelper.closeAppsuiteConnections(aAppsuite03);
    }

    //-------------------------------------------------------------------------
    private static JSONObject createBadOperation(final Doc aDoc) throws Exception {
        final JSONObject aBadOp = new JSONObject();

        aBadOp.put(OperationConstants.OPERATION_OSN, aDoc.getOSN());
        aBadOp.put(OperationConstants.OPERATION_OPL, 1);
        aBadOp.put(OperationConstants.OPERATION_NAME, OperationConstants.OPERATION_CREATEERROR);
        OperationsUtils.compressObject(aBadOp);

        return aBadOp;
    }

    //-------------------------------------------------------------------------
    private boolean isSameError(final ErrorCode aError1, final ErrorCode aError2) {
        return (aError1.getCode() == aError2.getCode());
    }

}
