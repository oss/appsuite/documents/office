/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import com.openexchange.office.rt2.perftest.fwk.AsyncResult;
import com.openexchange.office.rt2.perftest.fwk.Deferred.Func;
import com.openexchange.office.rt2.protocol.RT2Message;

public class RT2MessageDeferredHandler extends Func<RT2Message>
{
    //-------------------------------------------------------------------------
	private AsyncResult<RT2Message> aResult;

    //-------------------------------------------------------------------------
	public RT2MessageDeferredHandler(final AsyncResult<RT2Message> aResult)
	{
		this.aResult = aResult;
	}

    //-------------------------------------------------------------------------
    public AsyncResult<RT2Message> getAsyncResult()
    {
        return aResult;
    }

    //-------------------------------------------------------------------------
    @Override
    public void done (final RT2Message aMsg)
		throws Exception
	{
    	setIfResultIsEmpty(aMsg);
	}

    //-------------------------------------------------------------------------
    @Override
	public void fail (final RT2Message aMsg)
		throws Exception
	{
    	setIfResultIsEmpty(aMsg);
	}

    //-------------------------------------------------------------------------
    @Override
	public void always (final RT2Message aMsg)
        throws Exception
    {
    	setIfResultIsEmpty(aMsg);
    }

    //-------------------------------------------------------------------------
    private synchronized void setIfResultIsEmpty(final RT2Message aMsg)
        throws Exception
    {
        if (aResult.get() == null)
            aResult.set(aMsg);
    }
}
