/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.ClientStatusProperties;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.protocol.RT2Message;

import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;

public abstract class RT2TestBase {

    //-------------------------------------------------------------------------
	protected RT2TestBase() {
		super();
	}

    //-------------------------------------------------------------------------
    protected void setNackFrequenceOfServer(TestRunConfig ... envNodes) throws Exception {
    	for (TestRunConfig envNode : envNodes) {
    		ServerStateHelper.getConnectedJmxClient(envNode).setNackFrequenceOfServer(5);
    	}
    }

    //-------------------------------------------------------------------------
    protected void resetNackFrequenceOfServer(TestRunConfig ... envNodes) throws Exception {
    	for (TestRunConfig envNode : envNodes) {
    		ServerStateHelper.getConnectedJmxClient(envNode).setNackFrequenceOfServer(60);
    	}
    }

    //-------------------------------------------------------------------------
    protected boolean waitForEditRightsIndicator(AtomicInteger editRightsIndicator, long nTimeout) throws Exception {
        boolean bResult = false;

        synchronized (editRightsIndicator) {
        	editRightsIndicator.wait(nTimeout);
        }

        bResult = (editRightsIndicator.get() > 0);
        editRightsIndicator.set(0);
        return bResult;
    }

    //-------------------------------------------------------------------------
    protected boolean waitForDeferred(Deferred<RT2Message> aDeferred, long nTimeout) throws Exception {
        final long nEnd = System.currentTimeMillis() + nTimeout;

        boolean bTimeout = false;
        while (aDeferred.isPending() && !bTimeout) {
            Thread.sleep(nTimeout / 5);
            bTimeout = System.currentTimeMillis() >= nEnd;
        }

        return (!aDeferred.isPending());
    }

    //-------------------------------------------------------------------------
    protected Boolean getEditRightsRequestState(RT2Message aUpdateClientBroadcast, String sClientUID) throws Exception {
        final JSONObject body = aUpdateClientBroadcast.getBody();
        final JSONObject updateClientsData = body.getJSONObject("clients");
        final JSONArray changedClients = updateClientsData.optJSONArray(ClientStatusProperties.PROP_UPDATECLIENTS_CHANGED);

        if (null != changedClients) {
            JSONObject clientData = null;
            for (int i = 0; i < changedClients.length(); i++) {
                final JSONObject aTmp = changedClients.getJSONObject(i);
                if (sClientUID.equals(aTmp.getString(ClientStatusProperties.PROP_CLIENTUID))) {
                	clientData = aTmp;
                    break;
                }
            }

            if ((null != clientData) && (clientData.has(ClientStatusProperties.PROP_REQUESTS_EDITRIGHTS)))
                return clientData.getBoolean(ClientStatusProperties.PROP_REQUESTS_EDITRIGHTS);
        }

        return null;
    }
}
