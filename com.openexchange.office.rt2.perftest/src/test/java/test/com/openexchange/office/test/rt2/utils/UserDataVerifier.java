/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Assert;

//=============================================================================
public class UserDataVerifier
{
	public static final String USERDATA_DISPLAYNAME  = "displayName";
	public static final String USERDATA_GIVENNAME    = "givenName";
	public static final String USERDATA_SURNAME      = "surName";
	public static final String USERDATA_PREFLANGUAGE = "preferredLanguage";
	public static final String USERDATA_TIMEZONE     = "timeZone";
	public static final String USERDATA_LOCALE       = "locale";

    //-------------------------------------------------------------------------
    private static final PropertyInfo[] aKnownProps = new PropertyInfo[] {
        new PropertyInfo(USERDATA_DISPLAYNAME, String.class, false),
        new PropertyInfo(USERDATA_GIVENNAME, String.class, false),
        new PropertyInfo(USERDATA_SURNAME, String.class, false),
        new PropertyInfo(USERDATA_PREFLANGUAGE, String.class, false),
        new PropertyInfo(USERDATA_TIMEZONE, String.class, false),
        new PropertyInfo(USERDATA_LOCALE, JSONObject.class, false)
    };

    //-------------------------------------------------------------------------
    public static final Map<String, PropertyInfo> PROP_INFO_MAP = Collections.unmodifiableMap(MapHelper.toMap(Arrays.asList(aKnownProps)));

    //-------------------------------------------------------------------------
	private UserDataVerifier() {}

    //-------------------------------------------------------------------------
    public static boolean checkUserDataInfo(final JSONObject aUserInfo)
        throws Exception
    {
		if (aUserInfo == null)
		{
			Assert.fail("User data entry must not be null!");
			return false;
		}
		else
			return PROP_INFO_MAP.values()
					            .stream()
			                    .allMatch(v -> PropertyInfo.checkPropertyValue(v, aUserInfo.opt(v.key())));
	}
}
