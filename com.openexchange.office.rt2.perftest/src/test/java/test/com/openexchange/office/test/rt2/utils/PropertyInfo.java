/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.junit.Assert;

public class PropertyInfo implements Associable<String>
{
	private String   sPropertyName;
	private Class<?> aType;
	private boolean  bCanBeNullOrEmpty;

    //-------------------------------------------------------------------------
	public PropertyInfo(String sPropertyName, Class<?> aValueType, boolean bCanBeNullOrEmpty)
	{
		Validate.isTrue(StringUtils.isNotEmpty(sPropertyName));
		Validate.notNull(aValueType);

		this.sPropertyName     = sPropertyName;
		this.aType             = aValueType;
		this.bCanBeNullOrEmpty = bCanBeNullOrEmpty;
	}

    //-------------------------------------------------------------------------
	public Class<?> getType() { return aType; }
	public boolean canBeNullOrEmpty() { return this.bCanBeNullOrEmpty; }

    //-------------------------------------------------------------------------
	public <T> boolean isValid(T aPropertyValue)
	{
		if ((null == aPropertyValue) && bCanBeNullOrEmpty)
			return true;
		else
			return ((null != aPropertyValue) && (aType.isAssignableFrom(aPropertyValue.getClass())));
	}

    //-------------------------------------------------------------------------
	@Override
	public String key()
	{
		return this.sPropertyName;
	}

    //-------------------------------------------------------------------------
	public static boolean checkPropertyValue(final PropertyInfo aPropInfo, final Object v)
	{
		boolean isValid = aPropInfo.isValid(v);

		if (!isValid)
			Assert.assertTrue("Property " + aPropInfo.key() + " value type is invalid, expected type " + aPropInfo.getType().getCanonicalName(), isValid);

		return isValid;
	}
}
