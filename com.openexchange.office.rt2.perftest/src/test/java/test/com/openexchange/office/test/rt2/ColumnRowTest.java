/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import org.junit.Assert;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.operations.ColumnRow;

public class ColumnRowTest {
	private final String[] TEST_CELL_REF_STRINGS = {
		"A1",
		"Z99",
		"AA1001",
		"AZA12345"
	};

	private final ColumnRow[] TEST_CELL_REF_RESULTS = {
		new ColumnRow(1, 0),
		new ColumnRow(26, 98),
		new ColumnRow(27, 1000),
		new ColumnRow(1353, 12344)
	};

    @SuppressWarnings("unlikely-arg-type")
	@Test
    public void testColumnRowClass() throws Exception {
    	// check correct parsing & generic methods of ColumnRow class
    	for (int i = 0; i < TEST_CELL_REF_STRINGS.length; i++) {
    		final String cellRefAsString = TEST_CELL_REF_STRINGS[i];
        	final ColumnRow checkColRow = new ColumnRow(cellRefAsString);

        	final ColumnRow correctResultColRow = TEST_CELL_REF_RESULTS[i];
        	Assert.assertTrue(checkColRow.column() == correctResultColRow.column());
        	Assert.assertTrue(checkColRow.row() == correctResultColRow.row());

        	Assert.assertTrue(checkColRow.toString().equals(cellRefAsString));
        	Assert.assertTrue(checkColRow.equals(correctResultColRow));
        	Assert.assertTrue(checkColRow.hashCode() == correctResultColRow.hashCode());
        	Assert.assertTrue(ColumnRow.valueOf(cellRefAsString).equals(correctResultColRow));
        	Assert.assertTrue(ColumnRow.toString(correctResultColRow.column(), correctResultColRow.row()).contentEquals(cellRefAsString));
    	}

    	// check equals
    	final String cellRefAsString = "A1";
    	final String cellRefAsStringNotEqual1 = "A2";
    	final String cellRefAsStringNotEqual2 = "Z1";
    	final ColumnRow colRow = new ColumnRow(cellRefAsString);
    	Assert.assertFalse(colRow.equals(null));
    	Assert.assertFalse(colRow.equals(cellRefAsString));
    	Assert.assertTrue(colRow.equals(colRow));
    	Assert.assertFalse(colRow.equals(new ColumnRow(cellRefAsStringNotEqual1)));
    	Assert.assertFalse(colRow.equals(new ColumnRow(cellRefAsStringNotEqual2)));

    	// special case empty string
    	final ColumnRow colRowWithEmptyString = new ColumnRow("");
    	Assert.assertTrue(colRowWithEmptyString.equals(colRow));

    	// default ctor
    	final ColumnRow colRowDefaultCtor = new ColumnRow();
    	colRowDefaultCtor.equals(colRow);

    	// set methods
    	final ColumnRow colRowWithSetStart = new ColumnRow();
    	colRowWithSetStart.setColumn(26);
    	colRowWithSetStart.setRow(100);
    	final ColumnRow colRowWithSetResult = new ColumnRow("Z101");
    	Assert.assertTrue(colRowWithSetStart.equals(colRowWithSetResult));

    	// inc methods
    	final ColumnRow colRowInit = new ColumnRow();
    	final int initCol = colRowInit.column();
    	final int initRow = colRowInit.row();
    	colRowInit.incColumn();
    	Assert.assertTrue(colRowInit.column() == (initCol + 1));
    	Assert.assertTrue(colRowInit.row() == initRow);
    	colRowInit.incRow();
    	Assert.assertTrue(colRowInit.row() == (initRow + 1));
    	Assert.assertTrue(colRowInit.column() == (initCol + 1));

    	// check bad cell reference strings
    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("A0");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("123ABC");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("A");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("@1");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("[1");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("A$");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow("A>");

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	// bad column or row values in ctor
    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow(-1, -1);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow(-1, 0);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow(0, 0);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final ColumnRow badColRow = new ColumnRow(1, -1);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	// bad column or row values in static toString() method
    	try {
    		@SuppressWarnings("unused")
			final String cellRef = ColumnRow.toString(-1, 0);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final String cellRef = ColumnRow.toString(1, -1);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final String cellRef = ColumnRow.toString(0, 0);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
    		@SuppressWarnings("unused")
			final String cellRef = ColumnRow.toString(-1, -1);

    		Assert.fail("Bad cell reference string must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	// bad values for setColumn/setRow
    	try {
			final ColumnRow aColRow = new ColumnRow();
    		aColRow.setColumn(0);

    		Assert.fail("Bad setColumn must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
			final ColumnRow aColRow = new ColumnRow();
    		aColRow.setColumn(-1);

    		Assert.fail("Bad setColumn must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}

    	try {
			final ColumnRow aColRow = new ColumnRow();
    		aColRow.setRow(-1);

    		Assert.fail("Bad setRow must throw exception!");
    	} catch (IllegalArgumentException e) {
    		Assert.assertTrue(true);
    	} catch (Exception e) {
    		Assert.fail("Unexpected exception caught!");
    	}
    }

}
