/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONObject;


public class OperationsVerification
{
    //-------------------------------------------------------------------------
	private OperationsVerification()
	{
		// nothing to do
	}

    //-------------------------------------------------------------------------
	public static List<JSONObject> createOperationsList(final JSONArray aOperations)
	{
		Validate.notNull(aOperations);

		final List<JSONObject> aResult = new ArrayList<>();

		aOperations.forEach(o -> aResult.add((JSONObject)o));
		return aResult;
	}

    //-------------------------------------------------------------------------
	public static boolean containsOperation(final List<JSONObject> aOpsList, final JSONObject aOpsToFind)
	{
		Validate.notNull(aOpsList);
		Validate.notNull(aOpsToFind);

		return aOpsList.stream().anyMatch(o -> match(o, aOpsToFind));
	}

    //-------------------------------------------------------------------------
	public static long numOfMatchedOperations(final List<JSONObject> aOpsList, final JSONObject aOpsToFind)
	{
		Validate.notNull(aOpsList);
		Validate.notNull(aOpsToFind);

		return aOpsList.stream().filter(o -> match(o, aOpsToFind)).count();
	}

    //-------------------------------------------------------------------------
	public static JSONObject createOperationToFind(final String[] sNames, final Object[] aValues) throws Exception
	{
		final JSONObject aOpToFind = new JSONObject();

		for (int i = 0; i < sNames.length; i++)
			aOpToFind.put(sNames[i], aValues[i]);

		return aOpToFind;
	}

    //-------------------------------------------------------------------------
	private static boolean match(final JSONObject anyOp, final JSONObject findOp)
	{
		if ((null != anyOp) && (null != findOp))
		{
			final Set<String> anyOpProps  = anyOp.keySet();
			final Set<String> findOpProps = findOp.keySet();

			if (findOpProps.size() <= anyOpProps.size())
			{
				return findOpProps.stream()
								  .allMatch(s -> containsAndMatch(anyOp, s, findOp.get(s)));
			}
		}

		return false;
	}

    //-------------------------------------------------------------------------
	private static boolean containsAndMatch(final JSONObject aOp, final String sPropName, final Object value)
	{
		return isEqual(value, aOp.opt(sPropName));
	}

    //-------------------------------------------------------------------------
	private static boolean isEqual(final Object a, final Object b)
	{
		if ((null == a) && (null == b))
			return true;
		else if ((null == a) || (null == b))
			return false;
		else
			return a.equals(b);
	}
}
