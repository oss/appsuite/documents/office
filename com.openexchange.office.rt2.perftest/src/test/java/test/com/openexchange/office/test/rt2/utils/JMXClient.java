/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.List;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.lang.Validate;

public class JMXClient
{
    //-------------------------------------------------------------------------
	private final String JMX_SERVER_URL_PREFIX  = "service:jmx:rmi:///jndi/rmi://";
	private final String JMX_SERVER_URL_POSTFIX = "/server";

    //-------------------------------------------------------------------------
	private String                m_aServerName;
	private int                   m_nPort;
	private JMXConnector          m_aJMXConnector;
	private MBeanServerConnection m_aMBeanServerConnection;

    //-------------------------------------------------------------------------
	public JMXClient(String sServerName, int nPort)
	{
		Validate.notEmpty(sServerName);

		m_aServerName = sServerName;
		m_nPort       = nPort;
	}

    //-------------------------------------------------------------------------
	public MBeanServerConnection getMBeanConnection()
	{
		return m_aMBeanServerConnection;
	}

    //-------------------------------------------------------------------------
	public boolean isConnected()
	{
		return ((null != m_aJMXConnector) && (null != m_aMBeanServerConnection));
	}

    //-------------------------------------------------------------------------
	public void connect()
		throws Exception
	{
		if (null == m_aJMXConnector)
		{
			final String sUrl = createConnectUrlString(m_aServerName, m_nPort);
			JMXServiceURL url = new JMXServiceURL(sUrl);
	
			m_aJMXConnector = JMXConnectorFactory.connect(url, null);
			m_aMBeanServerConnection = m_aJMXConnector.getMBeanServerConnection();
		}
	}

    //-------------------------------------------------------------------------
	public void disconnect()
		throws Exception
	{
		if (null != m_aJMXConnector)
			m_aJMXConnector.close();
	}

    //-------------------------------------------------------------------------
	public Object getAttribute(String sObjectName, String sAttributeName)
	    throws Exception
	{
		Validate.notEmpty(sObjectName);
		Validate.notEmpty(sAttributeName);

		final ObjectName aObjName = new ObjectName(sObjectName);
		return m_aMBeanServerConnection.getAttribute(aObjName, sAttributeName);
	}

    //-------------------------------------------------------------------------
	@SuppressWarnings("unused")
	private List<MBeanAttributeInfo> getMBeanAttributesInfo(String sAttributeName)
	    throws Exception
	{
		final MBeanInfo            aMBeanInfo       = getMBeanInfo(sAttributeName);
		final MBeanAttributeInfo[] aMBeanAttributes = aMBeanInfo.getAttributes();
		return ArrayHelper.asList(aMBeanAttributes);
	}

    //-------------------------------------------------------------------------
	@SuppressWarnings("unused")
	private MBeanAttributeInfo getMBeanAttributeInfo(String sAttributeName)
	    throws Exception
	{
		final MBeanInfo aMBeanInfo = getMBeanInfo(sAttributeName);

		final MBeanAttributeInfo[] aAttributes = aMBeanInfo.getAttributes();
		if (!isEmptyArray(aAttributes))
		{
			for (final MBeanAttributeInfo aAttribute : aAttributes)
			{
				if (aAttribute.getName().equals(sAttributeName))
					return aAttribute;
			}
		}

		return null;
	}

    //-------------------------------------------------------------------------
	protected MBeanInfo getMBeanInfo(String sObjName)
	    throws Exception
	{
		if (isConnected())
		{
			ObjectName aObjName = new ObjectName(sObjName);
			return m_aMBeanServerConnection.getMBeanInfo(aObjName);
		}

		throw new IllegalStateException("No connection to JMX server!");
	}

    //-------------------------------------------------------------------------
	public Object invokeOperation(String sObjectName, String sOperationName, final Object[] argValues, final String[] argTypes)
	   throws Exception
	{
		if (isConnected())
		{
			final ObjectName objName = new ObjectName(sObjectName); 
			final Object ret = m_aMBeanServerConnection.invoke(objName, sOperationName, argValues, argTypes);

			return ret;
		}
		return null;
	}

    //-------------------------------------------------------------------------
	private String createConnectUrlString(String sServerName, int nPort)
	{
		final StringBuilder aTmp = new StringBuilder(JMX_SERVER_URL_PREFIX);
		aTmp.append(sServerName);
		aTmp.append(":");
		aTmp.append(nPort);
		aTmp.append(JMX_SERVER_URL_POSTFIX);
		return aTmp.toString();
	}

    //-------------------------------------------------------------------------
	boolean isEmptyArray(Object[] aArray)
	{
		return ((null != aArray) && (aArray.length > 0));
	}
}
