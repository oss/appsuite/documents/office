/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import javax.management.AttributeNotFoundException;
import javax.websocket.CloseReason;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.DocEvent;
import com.openexchange.office.rt2.perftest.doc.DocEventObserver;
import com.openexchange.office.rt2.perftest.doc.DocUser;
import com.openexchange.office.rt2.perftest.doc.DocUserRegistry;
import com.openexchange.office.rt2.perftest.doc.EDocumentState;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.ConfiguredConnectionStatus;
import com.openexchange.office.rt2.perftest.fwk.ConnectionProblems;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.perftest.fwk.JoinResultData;
import com.openexchange.office.rt2.perftest.fwk.MessageConstants;
import com.openexchange.office.rt2.perftest.fwk.NewDocumentData;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXFilesRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXRT2RestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXWSClient;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OperationsHelper;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.RT2JmxClient;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2CollabStandardTests extends RT2TestBase {

    //-------------------------------------------------------------------------
	private static final Logger LOG = Slf4JLogger.create(RT2CollabStandardTests.class);

	// -------------------------------------------------------------------------
	@BeforeClass
	public static void setUpEnv() throws Exception {
		StatusLine.setEnabled(false);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testSingleDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final Doc aDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();
		aDoc.open();
		aDoc.applyOps();
		aDoc.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testFastEmptyDocuments() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

		// TEXT
		final Doc aTextDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		NewDocumentData aNewDocData = aTextDoc.createNew();

		String sHtmlDoc = aNewDocData.getHtmlString();
		Assert.assertTrue("testFastEmptyDocuments [TEXT] - no html string", StringUtils.isNotEmpty(sHtmlDoc));

		Assert.assertTrue("testFastEmptyDocuments [TEXT] - no operations",
				(aNewDocData.getFastEmptyOperations().length() > 0));
		int nFastEmptyOSN = OperationsHelper.getFollowUpOSN(aNewDocData.getFastEmptyOperations());
		Assert.assertTrue("testFastEmptyDocuments [TEXT] - no/invalid OSN set", (nFastEmptyOSN > 0));

		aTextDoc.open();

		int nClientOSN = aTextDoc.getOSN() + 1;
		int nServerOSN = aTextDoc.getServerOSN();
		Assert.assertTrue(
				"testFastEmptyDocuments [TEXT] - server & fastEmpty OSN must be identical in case of no document modification. DocUid: "
						+ aTextDoc.getDocUID() + ", clientUid: " + aTextDoc.getRT2ClientUID(),
				(nFastEmptyOSN == nServerOSN));
		Assert.assertTrue(
				"testFastEmptyDocuments [TEXT] - client & fastEmpty OSN must be identical in case of no document modification. DocUid: "
						+ aTextDoc.getDocUID() + ", clientUid: " + aTextDoc.getRT2ClientUID(),
				(nFastEmptyOSN == nClientOSN));

		aTextDoc.close();

		// PRESENTATION
		final Doc aPresentationDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_PRESENTATION);

		aNewDocData = aPresentationDoc.createNew();

		sHtmlDoc = aNewDocData.getHtmlString();
		Assert.assertTrue(
				"testFastEmptyDocuments [PRESENTATION] - no html string. DocUid: " + aPresentationDoc.getDocUID()
						+ ", clientUid: " + aPresentationDoc.getRT2ClientUID(),
				StringUtils.isNotEmpty(aNewDocData.getHtmlString()));

		Assert.assertTrue(
				"testFastEmptyDocuments [PRESENTATION] - no operations. DocUid: " + aPresentationDoc.getDocUID()
						+ ", clientUid: " + aPresentationDoc.getRT2ClientUID(),
				(aNewDocData.getFastEmptyOperations().length() > 0));
		nFastEmptyOSN = OperationsHelper.getFollowUpOSN(aNewDocData.getFastEmptyOperations());
		Assert.assertTrue("testFastEmptyDocuments [PRESENTATION] - no/invalid OSN set. DocUid: "
				+ aPresentationDoc.getDocUID() + ", clientUid: " + aPresentationDoc.getRT2ClientUID(),
				(nFastEmptyOSN > 0));

		aPresentationDoc.open();

		nClientOSN = aPresentationDoc.getOSN() + 1;
		nServerOSN = aPresentationDoc.getServerOSN();
		Assert.assertTrue(
				"testFastEmptyDocuments [PRESENTATION] - server & fastEmpty OSN must be identical in case of no document modification. DocUid: "
						+ aPresentationDoc.getDocUID() + ", clientUid: " + aPresentationDoc.getRT2ClientUID(),
				(nFastEmptyOSN == nServerOSN));
		Assert.assertTrue(
				"testFastEmptyDocuments [PRESENTATION] - client & fastEmpty OSN must be identical in case of no document modification"
						+ aPresentationDoc.getDocUID() + ", clientUid: " + aPresentationDoc.getRT2ClientUID(),
				(nFastEmptyOSN == nClientOSN));

		aPresentationDoc.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testAll3DocumentTypes() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

		final Doc aTextDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
		final Doc aSpreadsheetDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_SPREADSHEET);
		final Doc aPresentationDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_PRESENTATION);

		aTextDoc.createNew();
		aTextDoc.open();
		aTextDoc.applyOps();
		aTextDoc.close();

		aSpreadsheetDoc.createNew();
		aSpreadsheetDoc.open();
		aSpreadsheetDoc.applyOps();
		aSpreadsheetDoc.close();

		aPresentationDoc.createNew();
		aPresentationDoc.open();
		aPresentationDoc.applyOps();
		aPresentationDoc.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testTextDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT, false);

		int nCountApplyOps = 0;

		aTextDoc.createNew();
		++nCountApplyOps;

		aTextDoc.open();
		aTextDoc.applyOps();
		aTextDoc.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testSpreadsheetDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final SpreadsheetDoc aSpreadsheetDoc = (SpreadsheetDoc) TestSetupHelper.newDocInst(aAppsuite,
				Doc.DOCTYPE_SPREADSHEET, false);

		aSpreadsheetDoc.enableAppliedOperationsRecording(true);

		aSpreadsheetDoc.createNew();
		aSpreadsheetDoc.open();
		aSpreadsheetDoc.applyOps();
		aSpreadsheetDoc.close();

		// verify the document content that must include the values inserted before
		DocContentVerifierHelper.verifySpreadsheetDocumentContent(aAppsuite, aSpreadsheetDoc);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testPresentationDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final PresentationDoc aPresDoc = (PresentationDoc) TestSetupHelper.newDocInst(aAppsuite,
				Doc.DOCTYPE_PRESENTATION, false);

		int nCountApplyOps = 0;

		aPresDoc.createNew();
		++nCountApplyOps;

		aPresDoc.open();
		aPresDoc.applyOps();
		aPresDoc.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aPresDoc, PresentationDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testSingleDocumentLonger() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT, false);

		aDoc.createNew();
		aDoc.open();

		int nCountApplyOps = 0;

		final long nStart = System.currentTimeMillis();
		final long nEnd = nStart + (2 * RT2.INTERVAL_4_FLUSH_ACK_IN_MS) + 500; // we want to ensure ack was triggered at
																				// least 2 times ;-)

		while (true) {
			// this sleep should just emulate the behavior of the js client
			// (which collects ops for about 1 second)
			Thread.sleep(1000);
			aDoc.applyOps();
			++nCountApplyOps;

			final long nNow = System.currentTimeMillis();
			if (nNow > nEnd)
				break;
		}

		aDoc.close();

		final RT2 aRT2 = aDoc.accessRT2();
		final int nLostMessages = aRT2.getLostMessageCount();
		final int nMessagesResend = aRT2.getResendCount();

		if (nLostMessages > 0) {
			Assert.fail("testSingleDocumentLonger [01] found " + nLostMessages + " lost messages for docUid: "
					+ aDoc.getDocUID() + ", clientUid: " + aDoc.getRT2ClientUID());
		}

		if (nMessagesResend > 0) {
			LOG.forLevel(ELogLevel.E_WARNING)
					.withMessage("testSingleDocumentLonger [02] there was " + nMessagesResend
							+ " resend messages via NACK. DocUid: " + aDoc.getDocUID() + ", clientUid: "
							+ aDoc.getRT2ClientUID())
					.log();
		}

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDoc, TextDoc.STR_TEXT, nCountApplyOps);
		
		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testOperationsRecording() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final Doc aDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();
		aDoc.open();
		aDoc.applyOps();
		aDoc.applyOps();
		aDoc.close();

		final StringBuilder sOps = new StringBuilder(256);
		final List<JSONObject> lOps = aDoc.getRecordedOperations();
		for (final JSONObject aOp : lOps)
			sOps.append(aOp.toString());

		Assert.assertTrue("testOperationsRecording [0] no operations recorded", sOps.length() > 0);
		System.out.println(sOps.toString());

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testFastReOpen() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final Doc aDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();
		aDoc.open();
		aDoc.close();

		for (int i = 0; i < 5; ++i) {
			System.err.println("... REOPEN [" + i + "]");
			aDoc.open();
			aDoc.close();
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testClientOpensTwoDocumentsAndChangeThemAtTheSameTime() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDoc01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		final TextDoc aDoc02 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);

		aDoc01.createNew();
		aDoc02.createNew();

		int nCountApplyOps1 = 0;
		int nCountApplyOps2 = 0;

		aDoc01.open();
		aDoc02.open();

		aDoc01.applyOps();
		++nCountApplyOps1;
		aDoc02.applyOps();
		++nCountApplyOps2;
		aDoc01.applyOps();
		++nCountApplyOps1;
		aDoc02.applyOps();
		++nCountApplyOps2;

		aDoc01.close();
		aDoc02.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDoc01, TextDoc.STR_TEXT, nCountApplyOps1);

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDoc02, TextDoc.STR_TEXT, nCountApplyOps2);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testJoinSameDocumentOnSameBackendNode() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// the test now has to respect that only ONE client can have edit rights
		final TextDoc aDoc01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDoc01.createNew();

		// aDoc2 is read-only and cannot send applyOps - the office backend drops
		// applyOps from viewer
		final EditableDoc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
				aDoc01.getFileId(), aDoc01.getDriveDocId(), true);

		int nCountApplyOps = 0;

		aDoc01.open();
		aDoc02.open();

		aDoc01.applyOps();
		++nCountApplyOps;
		aDoc01.applyOps();
		++nCountApplyOps;

		aDoc01.close();
		aDoc02.close();

		// Because of async close
		Thread.sleep(10000);

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDoc01, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testSingleDocumentOnConflictingWSChannel() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final OXWSClient aWSClient = aAppsuite.accessWSClient();
		final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		int nCountApplyOps = 0;

		aDoc.createNew();
		aDoc.open();
		aDoc.applyOps();
		++nCountApplyOps;

		Thread.sleep(500);
		aWSClient.simulateTwoConflictingSessions();
		aDoc.applyOps();
		++nCountApplyOps;
		aDoc.applyOps();
		++nCountApplyOps;
		Thread.sleep(500);

		aDoc.applyOps();
		++nCountApplyOps;
		aDoc.close();

		// verify the document content that must include the text one time
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDoc, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testOffline() throws Exception {
		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();
		aDoc.open();

		aDoc.applyOps();
		CloseReason closeReason = new CloseReason(CloseReason.CloseCodes.CLOSED_ABNORMALLY, "Test Offline");
		aAppsuite.accessWSClient().terminateSession(closeReason);

		OXWSClient wsClient = aAppsuite.accessWSClient();
		RT2JmxClient jmxClient = ServerStateHelper.getConnectedJmxClient(aEnvNode);
		boolean offlineDetected = false;
		for (int i = 0; i <= 10; ++i) {
			if (offlineDetected) {
				break;
			}
			Thread.sleep(1000);
			offlineDetected = jmxClient.getAllOfflineWSChannels().stream()
					.anyMatch(id -> id.contains(wsClient.getConnectionId()))
					&& jmxClient.getAllActiveWSChannels().stream()
							.noneMatch(id -> id.contains(wsClient.getConnectionId()));
		}
		Assert.assertTrue(offlineDetected);
	}

	// -------------------------------------------------------------------------
	@Ignore
	@Test
	public void testSwitchEditRightsSameBackend() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		aDocUser1.setAcceptsEditRightsRequest(true);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final EditableDoc aDocUser2 = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		final CountDownLatch aUser2HasEditMode = new CountDownLatch(1);
		aDocUser2.enableDocEvents(true);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser2.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					System.out.println("sUser1UUID:" + sUser1UUID + ",sUser2UUID:" + sUser2UUID + ", document: "
							+ aDocUser2.getEditorUID());
					if (sUser2UUID.equals(sNewEditorUID)) {
						System.out.println("New editor: " + sNewEditorUID);
						aUser2HasEditMode.countDown();
					}
				}
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// try to acquire edit rights from editor client
		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// user 2 waits for the approval of user 1
		final boolean bHasEditoMode = aUser2HasEditMode.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit mode didn't change in an expected time frame", bHasEditoMode);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text two times
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser1, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Ignore
	@Test
	public void testSwitchEditRightsDeclinedByEditor() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		// editor always declines edit requests
		aDocUser1.setAcceptsEditRightsRequest(false);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final EditableDoc aDocUser2 = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		aDocUser2.enableDocEvents(true);

		final AtomicBoolean aCheckForEditRightsReset = new AtomicBoolean(false);
		final AtomicBoolean aCheckForEditRightsRequested = new AtomicBoolean(false);
		final CountDownLatch aUser2NoEditRights = new CountDownLatch(1);
		final CountDownLatch aUser2RequestsEditRights = new CountDownLatch(1);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser2.addBroadcastObserver(b -> {
			if (b.getType().equals(RT2Protocol.BROADCAST_UPDATE_CLIENTS)) {
				if (aCheckForEditRightsReset.get() && (getEditRightsRequestState(b, sUser2UUID) == Boolean.FALSE))
					aUser2NoEditRights.countDown();
				if (aCheckForEditRightsRequested.get() && (getEditRightsRequestState(b, sUser2UUID) == Boolean.TRUE))
					aUser2RequestsEditRights.countDown();
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// try to acquire edit rights from editor client
		aCheckForEditRightsRequested.set(true);
		aCheckForEditRightsReset.set(true);

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait for update where our edit rights request is broadcasted
		final boolean bEditRightsRequested = aUser2RequestsEditRights.await(RT2TestConstants.DEFAULT_TIMEOUT,
				TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit rights state not broadcasted in an expected time frame", bEditRightsRequested);

		// wait for the denial of user 1 broadcasted
		final boolean bNoEditRightsReceived = aUser2NoEditRights.await(RT2TestConstants.DEFAULT_TIMEOUT,
				TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit rights state not reset in an expected time frame", bNoEditRightsReceived);

		// edit rights must not have changed
		Assert.assertEquals(sUser1UUID, aDocUser2.getEditorUID());

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text two times
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser1, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Ignore
	@Test
	public void testSwitchEditRightsForce() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		// editor doesn't handle edit rights requests at all (emulates a non-responding
		// client)
		aDocUser1.setHandleEditRightsRequests(false);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final EditableDoc aDocUser2 = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		aDocUser2.enableDocEvents(true);

		final AtomicBoolean aCheckForEditRightsRequested = new AtomicBoolean(false);
		final CountDownLatch aUser2HasEditRights = new CountDownLatch(1);
		final CountDownLatch aUser2RequestsEditRights = new CountDownLatch(1);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser2.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser2UUID.equals(sNewEditorUID))
						aUser2HasEditRights.countDown();
				}
			}
		});

		aDocUser2.addBroadcastObserver(b -> {
			if (b.getType().equals(RT2Protocol.BROADCAST_UPDATE_CLIENTS)) {
				if (aCheckForEditRightsRequested.get() && (getEditRightsRequestState(b, sUser2UUID) == Boolean.TRUE))
					aUser2RequestsEditRights.countDown();
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// try to acquire edit rights from editor client
		aCheckForEditRightsRequested.set(true);

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait for update where our edit rights request is broadcasted
		final boolean bEditRightsRequested = aUser2RequestsEditRights.await(RT2TestConstants.DEFAULT_TIMEOUT,
				TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit rights state not broadcasted in an expected time frame", bEditRightsRequested);
		Assert.assertEquals(sUser1UUID, aDocUser2.getEditorUID());

		// user 2 now tries to force to receive the edit rights
		final Deferred<RT2Message> aResult = aDocUser2.editRightsAsync(MessageConstants.ACTION_EDITRIGHTS_FORCE, null);

		boolean bDeferredHasResult = waitForDeferred(aResult, RT2TestConstants.DEFAULT_TIMEOUT);
		Assert.assertTrue("Edit rights force request not answered in time with response!", bDeferredHasResult);

		// wait for update where our edit rights request is broadcasted
		final boolean bEditRights = aUser2HasEditRights.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit rights state not broadcasted in an expected time frame", bEditRights);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text two times
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser1, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Ignore
	@Test
	public void testSwitchEditRightsSeveralTimes() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		aDocUser1.setHandleEditRightsRequests(true);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final EditableDoc aDocUser2 = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		aDocUser2.enableDocEvents(true);
		aDocUser2.setHandleEditRightsRequests(true);

		final AtomicInteger aUser1HasEditRights = new AtomicInteger(0);
		final AtomicInteger aUser2HasEditRights = new AtomicInteger(0);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser1.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser1UUID.equals(sNewEditorUID)) {
						aUser1HasEditRights.incrementAndGet();
						synchronized (aUser1HasEditRights) {
							aUser1HasEditRights.notifyAll();
						}
					}
				}
			}
		});

		aDocUser2.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser2UUID.equals(sNewEditorUID)) {
						aUser2HasEditRights.incrementAndGet();
						synchronized (aUser2HasEditRights) {
							aUser2HasEditRights.notifyAll();
						}
					}
				}
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		// wait until we know that user1 has the edit rights
		boolean bEditRightsReceived = waitForEditRightsIndicator(aUser1HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// The test switches the edit rights three times user1 -> user2 -> user1 ->
		// user2

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 2 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser2HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 2 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		// user 1 now tries to receive the edit rights from user 2
		aDocUser1.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 1 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser1HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 1 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 2 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser2HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 2 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text nCountApplyOps times
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser1, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testSingleDocumentWithBadConnection() throws Exception {
		int nLoops = 30;
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;
		RT2.SIMULATE_BAD_CONNECTION = true;

		// use a constant drop rate, drop every 10th message
		RT2.BAD_CONNECTION_CONF = new ConfiguredConnectionStatus(ConnectionProblems.CONSTANT_DROP_RATE, 10);

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();
		aDoc.open();

		int nCountApplyOps = 0;

		while (nLoops > 0) {
			aDoc.applyOpsAsync();
			++nCountApplyOps;
			Thread.sleep(10);

			aDoc.applyOpsAsync();
			++nCountApplyOps;
			Thread.sleep(50);

			aDoc.applyOpsAsync();
			++nCountApplyOps;
			Thread.sleep(100);

			nLoops--;
		}

		aDoc.close();

		// verify the document content that must include the text nCountApplyOps times
		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDoc, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);

		RT2.SIMULATE_BAD_CONNECTION = false;
	}

	// -------------------------------------------------------------------------
	@Test
	public void testForceQuitOnDocument() throws Exception {
		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode01);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser.createNew();

		int nCountApplyOps = 0;

		aDocUser.open();

		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;

		// Simulate a silentCloseLeave used by the web frontend
		// The webfrontend sends a close-reques and waits about
		// 8 seconds before it sends a leave with force quit.
		boolean responseReceived = aDocUser.sendClose();
		Assert.assertTrue(responseReceived);

		// simulate an force leave from a client
		aDocUser.simpleLeave(true);

		// we have to reset the new document data to ensure that
		// the next open won't be a fast-empty open (done by default
		// with close() not used in this test).
		aDocUser.clearNewDocumentData();

		DocContentVerifierHelper.verifyDocumentContent(aAppsuite01, aDocUser, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------#
	@Test
	public void testEmergencyLeaveViaRESTForClientWithSingleDocument() throws Exception {
		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final RT2JmxClient jmxClient = ServerStateHelper.getConnectedJmxClient(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

		final TextDoc aDocUser = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
		aDocUser.createNew();

		int nCountApplyOps = 0;

		aDocUser.open();

		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;
		aDocUser.applyOps();
		++nCountApplyOps;

		try {
			final OXRT2RestAPI aRT2RESTAPI = new OXRT2RestAPI();

			aRT2RESTAPI.bind(aAppsuite);

			// emergency leave is a fire & forget rest call, therefore don't check server
			// result
			aRT2RESTAPI.emergencyLeave(aDocUser.getRT2ClientUID(), aDocUser.getDocUID(), new JSONArray(), false);
		} catch (final ServerResponseException e) {
			Assert.fail("Send emergencyleave message failed: " + e.getMessage());
		}

		aDocUser.setClosedState();
		// we have to reset the new document data to ensure that
		// the next open won't be a fast-empty open (done by default
		// with close() not used in this test).
		aDocUser.clearNewDocumentData();

		// We need a sleep here as we did a fire&forget emergency leave and therefore
		// have no information about the server state.
		boolean docProcRemoved = waitForDocProcessorIsRemoved(jmxClient, aDocUser.getDocUID(), RT2TestConstants.DEFAULT_TIMEOUT);
		Assert.assertTrue(docProcRemoved);

		DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDocUser, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testEmergencyLeaveViaRESTWithTwoClientsSameNode() throws Exception {
		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite1 = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final OXAppsuite aAppsuite2 = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final RT2JmxClient jmxClient = ServerStateHelper.getConnectedJmxClient(aEnvNode);

		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite1, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();

		final TextDoc aDocUser2 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite2, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		// we need to wait a little bit to ensure that the clients updates were
		// received and processed by the clients
		Thread.sleep(1000);

		final DocUserRegistry aDoc01UserReg = aDocUser1.accessUserRegistry();
		final DocUserRegistry aDoc02UserReg = aDocUser2.accessUserRegistry();

		List<DocUser> aListDoc1 = aDoc01UserReg.asList();
		List<DocUser> aListDoc2 = aDoc02UserReg.asList();
		Assert.assertTrue(
				"All document user registries must have the same number of clients! listDoc1: " + aListDoc1.size()
						+ ", listDoc2: " + aListDoc2.size(),
				((aListDoc1.size() == 2) && (aListDoc1.size() == aListDoc2.size())));

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		try {
			final OXRT2RestAPI aRT2RESTAPI = new OXRT2RestAPI();

			aRT2RESTAPI.bind(aAppsuite1);

			// emergency leave is a fire & forget rest call, therefore don't check server
			// result
			aRT2RESTAPI.emergencyLeave(aDocUser1.getRT2ClientUID(), aDocUser1.getDocUID(), new JSONArray(), false);
		} catch (final ServerResponseException e) {
			Assert.fail("Send emergencyleave message failed: " + e.getMessage());
		}

		aDocUser1.setClosedState();
		// we have to reset the new document data to ensure that
		// the next open won't be a fast-empty open (done by default
		// with close() not used in this test).
		aDocUser1.clearNewDocumentData();

		// We need a sleep here as we did a fire&forget emergency leave and therefore
		// have no information about the server state.
		boolean clientRemoved = waitForClientRemovedFromDocProcessor(jmxClient, aDocUser1.getDocUID(), aDocUser1.getRT2ClientUID(), RT2TestConstants.DEFAULT_TIMEOUT);
		Assert.assertTrue(clientRemoved);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite1);

		// retrieve user list again and see that the first user really left the document
		aListDoc2 = aDoc02UserReg.asList();
		Assert.assertTrue(
				"Document user registry of user 2 must only have ONE user! Current size = " + aListDoc2.size(),
				(aListDoc2.size() == 1));

		aDocUser2.close();

		DocContentVerifierHelper.verifyDocumentContent(aAppsuite2, aDocUser2, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite2);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testEmergencyLeaveViaRESTConcurrently() throws Exception {
		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final RT2JmxClient jmxClient = ServerStateHelper.getConnectedJmxClient(aEnvNode);
		final OXAppsuite aAppsuite1 = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final OXAppsuite aAppsuite2 = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final OXAppsuite aAppsuite3 = TestSetupHelper.openAppsuiteConnection(aEnvNode);

		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite1, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();

		final TextDoc aDocUser2 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite2, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		final TextDoc aDocUser3 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite3, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();
		aDocUser3.open();

		// we need to wait a little bit to ensure that the clients updates were
		// received and processed by the clients
		Thread.sleep(1000);

		final DocUserRegistry aDoc01UserReg = aDocUser1.accessUserRegistry();
		final DocUserRegistry aDoc02UserReg = aDocUser2.accessUserRegistry();
		final DocUserRegistry aDoc03UserReg = aDocUser3.accessUserRegistry();

		List<DocUser> aListDoc1 = aDoc01UserReg.asList();
		List<DocUser> aListDoc2 = aDoc02UserReg.asList();
		List<DocUser> aListDoc3 = aDoc03UserReg.asList();
		Assert.assertTrue("All document user registries must have the same number of clients!", ((aListDoc1.size() == 3)
				&& (aListDoc1.size() == aListDoc2.size() && (aListDoc2.size() == aListDoc3.size()))));

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		final AtomicReference<String> aThread1AssertMsg = new AtomicReference<>(null);
		final AtomicReference<String> aThread2AssertMsg = new AtomicReference<>(null);

		final Thread t1 = new Thread() {
			@Override
			public void run() {
				try {
					final OXRT2RestAPI aRT2RESTAPI = new OXRT2RestAPI();

					aRT2RESTAPI.bind(aAppsuite1);

					Thread.sleep(1000);

					// emergency leave is a fire & forget rest call, therefore don't check server
					// result
					aRT2RESTAPI.emergencyLeave(aDocUser1.getRT2ClientUID(), aDocUser1.getDocUID(), new JSONArray(),
							false);
				} catch (final Exception e) {
					aThread2AssertMsg.set(e.getMessage());
					e.printStackTrace();
				}
			}
		};

		final Thread t2 = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
					aDocUser2.close();
					aDocUser3.close();
				} catch (Exception e) {
					aThread2AssertMsg.set(e.getMessage());
					e.printStackTrace();
				}
			}
		};

		t1.start();
		t2.start();

		t1.join(0);
		t2.join(0);

		if (aThread1AssertMsg.get() != null)
			Assert.fail(aThread1AssertMsg.get());

		if (aThread2AssertMsg.get() != null)
			Assert.fail(aThread2AssertMsg.get());

		// We need a sleep here as we did a fire&forget emergency leave and therefore
		// have no information about the server state.
		boolean docProcRemoved = waitForDocProcessorIsRemoved(jmxClient, aDocUser1.getDocUID(), RT2TestConstants.DEFAULT_TIMEOUT);
		Assert.assertTrue(docProcRemoved);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite1);

		DocContentVerifierHelper.verifyDocumentContent(aAppsuite2, aDocUser2, TextDoc.STR_TEXT, nCountApplyOps);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite2);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite3);
	}

    // -------------------------------------------------------------------------
    @Test
    public void testAdvisoryLockInfo() throws Exception {
        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        setNackFrequenceOfServer(aEnvNode);

        final RT2JmxClient aJmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aJmxClient.isConnected());

        try {
            if ("ADVISORY_LOCK_MODE_ORIGIN".equalsIgnoreCase(aJmxClient.getAdvisoryLockInfoMode())) {
                final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
                final OXAppsuite aAppsuiteAlternative = TestSetupHelper.openAppsuiteAlternativeConnection(aEnvNode);

                final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
                aDocUser1.createNew();
                aDocUser1.open();

                // advisory lock must not be set first user
                final JoinResultData joinResultData1 = aDocUser1.getJoinResultData();
                Assert.assertTrue(ErrorCode.NO_ERROR.getCode() == joinResultData1.getErrorCode().getCode());

                // connect with 2nd user and check the advisory lock again - this time we must see the warning
                final TextDoc aDocUser2 = (TextDoc) TestSetupHelper.newDocInst(aAppsuiteAlternative, Doc.DOCTYPE_TEXT,
                        aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
                aDocUser2.open();

                final JoinResultData joinResultData2 = aDocUser2.getJoinResultData();
                Assert.assertTrue(ErrorCode.ADVISORY_LOCK_SET_WARNING.getCode() == joinResultData2.getErrorCode().getCode());

                aDocUser1.close();
                aDocUser2.close();

                TestSetupHelper.closeAppsuiteConnections(aAppsuite);
                TestSetupHelper.closeAppsuiteConnections(aAppsuiteAlternative);
            }
        } catch (AttributeNotFoundException e) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("No advisory lock info feature available - ignoring test");
        }
    }

    // -------------------------------------------------------------------------
    @Test
    public void testSuccessfulGCIfDocOnNodeMapEntryIsMissing() throws Exception {
        final TestRunConfig envNode = TestRunConfigHelper.defineEnv4Node01();
        setNackFrequenceOfServer(envNode);

        OXAppsuite appsuite = TestSetupHelper.openAppsuiteConnection(envNode);
        final RT2JmxClient jmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, envNode.sServerName, envNode.nServerJMXPort);

        jmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", jmxClient.isConnected());

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;

        // store document data to re-open it after error processing
        String docFolderId;
        String docFileId;
        String driveDocId;

        try {
            serverStateStarted = serverState.start();

            final TextDoc docUser1 = (TextDoc) TestSetupHelper.newDocInst(appsuite, Doc.DOCTYPE_TEXT);

            // register ourself for broadcast messages
            final AtomicBoolean exceptionReceived = new AtomicBoolean(false);
            final AtomicReference<ErrorCode> errorCodeRef = new AtomicReference<>(ErrorCode.NO_ERROR);
            final CountDownLatch countdownLatch = new CountDownLatch(1);
            docUser1.addErrorObserver(e -> {
                try {
                    final ErrorCode errorCode = RT2MessageGetSet.getError(e);
                    errorCodeRef.set(errorCode);
                    countdownLatch.countDown();
                } catch (final Exception ex) {
                    exceptionReceived.set(true);
                    countdownLatch.countDown();
                }
            });

            docUser1.createNew();
            docUser1.open();

            docFolderId = docUser1.getFolderId();
            docFileId = docUser1.getFileId();
            driveDocId = docUser1.getDriveDocId();

            docUser1.applyOps();
            docUser1.applyOps();

            // ensure that doc map entry for doc exists
            Set<String> searchDocUids = new HashSet<>();
            searchDocUids.add(docUser1.getDocUID());
            Set<String> foundDocUids = jmxClient.findHazelcastDocMapEntries(searchDocUids);
            Assert.assertTrue(foundDocUids != null);
            Assert.assertTrue(foundDocUids.size() == 1);

            // remove the doc on node map entry to simulate the problems described by
            // DOCS-1731, DOCS-2518
            boolean removed = jmxClient.removeDocOnNodeMapEntry(docUser1.getDocUID());
            Assert.assertTrue(removed);

            // trigger an exception on the server-side
            docUser1.syncAsync();

            TestSetupHelper.closeAppsuiteConnections(appsuite);

            boolean errorResponseReceived = countdownLatch.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue(errorResponseReceived);
            Assert.assertTrue(errorCodeRef.get().getCodeAsStringConstant().equals(ErrorCode.GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR.getCodeAsStringConstant()));

            // try to re-open the same document again to see if we can open it successfully
            appsuite = TestSetupHelper.openAppsuiteConnection(envNode);
            final TextDoc docUser1Reopen = (TextDoc) TestSetupHelper.newDocInst(appsuite, Doc.DOCTYPE_TEXT, docFolderId, docFileId, driveDocId, true);

            docUser1Reopen.open();

            // check document's state - must be open
            Assert.assertTrue(EDocumentState.E_OPEN == docUser1Reopen.getDocState());

            TestSetupHelper.closeAppsuiteConnections(appsuite);
        } finally {
            if (serverStateStarted) {
                serverState.stopAndShutdown();
            }
        }
    }

    // -------------------------------------------------------------------------
    @Test
    public void testSaveDocDoesNotCreateNewVersion() throws Exception {
        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        setNackFrequenceOfServer(aEnvNode);

        final RT2JmxClient aJmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aJmxClient.isConnected());

        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
		final OXFilesRestAPI aFilesAPI = new OXFilesRestAPI();

		aFilesAPI.bind(aAppsuite);

        aDocUser1.createNew();
        aDocUser1.open();

		int nCountApplyOps = 0;

		aDocUser1.applyOps(); ++nCountApplyOps;
		aDocUser1.applyOps(); ++nCountApplyOps;

		aDocUser1.saveDoc();

		// 1st save creates a new version (as we created a new file)
		JSONArray versionsOfFile = aFilesAPI.getVersionsOfFile(aDocUser1.getFolderId(), aDocUser1.getFileId());
		Assert.assertTrue(versionsOfFile.length() == 2);

		aDocUser1.applyOps(); ++nCountApplyOps;
		aDocUser1.applyOps(); ++nCountApplyOps;
		aDocUser1.saveDoc();

		// 2nd save does NOT create a new version
		versionsOfFile = aFilesAPI.getVersionsOfFile(aDocUser1.getFolderId(), aDocUser1.getFileId());
		Assert.assertTrue(versionsOfFile.length() == 2);
		Assert.assertTrue(nCountApplyOps == 4);

		aDocUser1.close();

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

    // -------------------------------------------------------------------------
	@Test
	@Ignore
	public void testBadConnectionSeveralTimesWithDocVerification() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;
		RT2.SIMULATE_BAD_CONNECTION = true;

		// use a constant drop rate, drop every 10th message
		RT2.BAD_CONNECTION_CONF = new ConfiguredConnectionStatus(ConnectionProblems.CONSTANT_DROP_RATE, 10);

		final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
		setNackFrequenceOfServer(aEnvNode);
		final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
		final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

		aDoc.createNew();

		int nApplyLoops = 30;
		int nOpenCloseLoops = 10;
		int nCountApplyOps = 0;

		while (nOpenCloseLoops > 0) {
			aDoc.open();

			while (nApplyLoops > 0) {
				aDoc.applyOpsAsync(); ++nCountApplyOps;
				Thread.sleep(10);

				aDoc.applyOpsAsync(); ++nCountApplyOps;
				Thread.sleep(50);

				aDoc.applyOpsAsync(); ++nCountApplyOps;
				Thread.sleep(100);

				--nApplyLoops;
			}

			--nOpenCloseLoops;

			aDoc.close();

			// verify the document content that must include the text nCountApplyOps times
			DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aDoc, TextDoc.STR_TEXT, nCountApplyOps);
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite);

		RT2.SIMULATE_BAD_CONNECTION = false;
	}

    // -------------------------------------------------------------------------
    private boolean waitForClientRemovedFromDocProcessor(RT2JmxClient jmxClient, String docUid, String clientUid, long timeoutInMS) throws IllegalArgumentException {
    	boolean clientUidRemoved = false;

    	if (timeoutInMS < 0)
    		throw new IllegalArgumentException("Timeout value must be zero or a positive number!");

    	int run = 0;
    	long waitTime = timeoutInMS / 10;
    	long timeout = System.currentTimeMillis() + timeoutInMS;
    	long now = System.currentTimeMillis();
    	do {
    		try {
    			if (run > 0) { Thread.sleep(waitTime); }
        		final Set<String> clientUids = jmxClient.getClientsOfDoc(docUid);
        		if (clientUids != null) {
        			clientUidRemoved = clientUids.stream().filter(c -> clientUid.equals(c)).count() == 0;
        		}
            	now = System.currentTimeMillis();
            	++run;
    		} catch (Exception e) {
    			// bail out on exception
    			timeout = now;
    			System.out.println(e);
    		}
    	} while ((now < timeout) && !clientUidRemoved);

    	return clientUidRemoved;
    }

    // -------------------------------------------------------------------------
    private boolean waitForDocProcessorIsRemoved(RT2JmxClient jmxClient, String docUid, long timeoutInMS) throws IllegalArgumentException {
    	boolean docProcRemoved = false;

    	if (timeoutInMS < 0)
    		throw new IllegalArgumentException("Timeout value must be zero or a positive number!");

    	int run = 0;
    	long waitTime = timeoutInMS / 10;
    	long timeout = System.currentTimeMillis() + timeoutInMS;
    	long now = System.currentTimeMillis();
    	do {
    		try {
    			if (run > 0) { Thread.sleep(waitTime); }
        		docProcRemoved = !jmxClient.isActiveDocProcessorOnThisNode(docUid);
            	now = System.currentTimeMillis();
            	++run;
    		} catch (Exception e) {
    			// bail out on exception
    			timeout = now;
    			System.out.println(e);
    		}
    	} while ((now < timeout) && !docProcRemoved);

    	return docProcRemoved;
    }
}
