/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.ConfiguredConnectionStatus;
import com.openexchange.office.rt2.perftest.fwk.ConnectionProblems;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.PresentationDocumentContentVerifier;
import test.com.openexchange.office.test.rt2.utils.SpreadsheetDocumentVerifier;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2ReloadStressTests extends RT2TestBase {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv() throws Exception {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testFastReloadWithSameClientWithAllDocumentTypes() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

        int nCountApplyOps = 0;

        //-------------------------------------------------------------------------
        final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
        aTextDoc.createNew();
        aTextDoc.open();
        aTextDoc.applyOps();
        ++nCountApplyOps;

        aTextDoc.close();

        // Verify the document content that must include the text one time. This is done
        // using the same client to open the document again and check it -> DOCS-3009
        boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);

        //-------------------------------------------------------------------------
        final SpreadsheetDoc aCalcDoc = (SpreadsheetDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_SPREADSHEET);

        aCalcDoc.enableAppliedOperationsRecording(true);

        aCalcDoc.createNew();
        aCalcDoc.open();
        aCalcDoc.applyOps();
        aCalcDoc.close();

        // Verify the document content that must include the value one time. This is done
        // using the same client to open the document again and check it -> DOCS-3009
        bVerified = SpreadsheetDocumentVerifier.checkDocumentContent(aCalcDoc, aCalcDoc.getRecordedAppliedOperations());
        Assert.assertTrue("Operations recorded and received from filter don't create the same document. Document was not correctly stored!", bVerified);

        //-------------------------------------------------------------------------
        final PresentationDoc aPresDoc = (PresentationDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_PRESENTATION);

        nCountApplyOps = 0;
        aPresDoc.createNew();
        aPresDoc.open();
        aPresDoc.applyOps();
        ++nCountApplyOps;
        aPresDoc.close();

        // Verify the document content that must include the text one time. This is done
        // using the same client to open the document again and check it -> DOCS-3009
        bVerified = PresentationDocumentContentVerifier.checkTextDocumentContent(aPresDoc, PresentationDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

    // -------------------------------------------------------------------------
    @Test
    public void testSingleDocumentWithBadConnectionCheckWithReload() throws Exception {
        int nLoops = 30;
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;
        RT2.SIMULATE_BAD_CONNECTION = true;

        // use a constant drop rate, drop every 10th message
        RT2.BAD_CONNECTION_CONF = new ConfiguredConnectionStatus(ConnectionProblems.CONSTANT_DROP_RATE, 10);

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        setNackFrequenceOfServer(aEnvNode);
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final TextDoc aDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

        aDoc.createNew();
        aDoc.open();

        int nCountApplyOps = 0;

        while (nLoops > 0) {
            aDoc.applyOpsAsync();
            ++nCountApplyOps;
            Thread.sleep(10);

            aDoc.applyOpsAsync();
            ++nCountApplyOps;
            Thread.sleep(50);

            aDoc.applyOpsAsync();
            ++nCountApplyOps;
            Thread.sleep(100);

            nLoops--;
        }

        aDoc.close();

        // Verify the document content that must include the text one time. This is done
        // using the same client to open the document again and check it -> DOCS-3009
        boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDoc, TextDoc.STR_TEXT, nCountApplyOps);
        Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);

        RT2.SIMULATE_BAD_CONNECTION = false;
    }

    // -------------------------------------------------------------------------
    @Test
    public void testFastReloadWithSameClientManyTries() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

        int tries = 10;
        while (tries > 0) {
            int nCountApplyOps = 0;

            //-------------------------------------------------------------------------
            final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
            aTextDoc.createNew();
            aTextDoc.open();
            aTextDoc.applyOps();
            ++nCountApplyOps;

            aTextDoc.close();

            // Verify the document content that must include the text one time. This is done
            // using the same client to open the document again and check it -> DOCS-3009
            boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);
            Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);
            tries--;
        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

}
