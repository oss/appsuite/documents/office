/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.operations.OperationConstants;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.perftest.operations.SpreadsheetOperationsConstants;

public class SpreadsheetDocumentVerifier {

    //-------------------------------------------------------------------------
	private static final String CHANGE_CELLS_OP = "changeCells";

    //-------------------------------------------------------------------------
	private SpreadsheetDocumentVerifier() { /* nothing to do  */ }

    //-------------------------------------------------------------------------
    public static boolean checkDocumentContent(final SpreadsheetDoc aDoc, final JSONArray appliedOperations) throws Exception {
    	if (appliedOperations != null) {

            // open & close document again to receive the stored operations from the filter
    		aDoc.enableOperationsRecording(true);
    		aDoc.open     ();
            aDoc.close    ();
    		
    		final Map<String, Object> aDocModel = new HashMap<>();
    		final Map<String, Object> aAppliedOpsModel = new HashMap<>();
    		final List<JSONObject> aRecordedOps = aDoc.getRecordedOperations();

    		fillSpreadsheetModelFromOperations(aAppliedOpsModel, appliedOperations);
    		fillSpreadsheetModelFromOperations(aDocModel, aRecordedOps);

    		return aDocModel.equals(aAppliedOpsModel);
    	}

        return false;
    }

    //-------------------------------------------------------------------------
    private static void fillSpreadsheetModelFromOperations(final Map<String, Object> model, final List<JSONObject> operations) throws Exception {
		final Iterator<JSONObject> aOpsIter = operations.iterator();
		while (aOpsIter.hasNext()) {
			applyChangesIfChangeCellsOperation(model, aOpsIter.next());
		}
    }

    //-------------------------------------------------------------------------
    private static void fillSpreadsheetModelFromOperations(final Map<String, Object> model, final JSONArray operations) throws Exception {
    	for (int i = 0; i < operations.length(); i++) {
    		applyChangesIfChangeCellsOperation(model, operations.getJSONObject(i));
    	}
    }

    //-------------------------------------------------------------------------
    private static boolean applyChangesIfChangeCellsOperation(final Map<String, Object> model, final JSONObject operation) throws Exception {
    	OperationsUtils.decompressObject(operation);

		final String opName = operation.optString(OperationConstants.OPERATION_NAME);
		if (CHANGE_CELLS_OP.equals(opName)) {
			applyChangeCellsOperation(model, operation);
			return true;
		}

		return false;
    }

    //-------------------------------------------------------------------------
    private static void applyChangeCellsOperation(final Map<String, Object> aModel, final JSONObject aChangeCellOp) throws Exception {
    	final JSONObject content = aChangeCellOp.getJSONObject(OperationConstants.OPERATION_CONTENTS);

    	final Set<String> set = content.keySet();
    	set.stream().forEach(cell -> {
    		boolean removeCell = false;
    		Object value = null;

    		final Object valueObj = content.opt(cell);
    		if (valueObj instanceof JSONObject) {
    			// CellData
    			final JSONObject jsonObj = (JSONObject)valueObj;
    			value = jsonObj.opt(SpreadsheetOperationsConstants.CHANGE_CELL_VALUE);
    			removeCell = jsonObj.optBoolean(SpreadsheetOperationsConstants.CHANGE_CELL_UNDEFINE, false);
    		} else {
    			// CellScalar
    			value = valueObj;
    		}

    		if (removeCell) {
    			aModel.remove(cell);
    		} else {
        		aModel.put(cell, value);
    		}
    	});
    }

}
