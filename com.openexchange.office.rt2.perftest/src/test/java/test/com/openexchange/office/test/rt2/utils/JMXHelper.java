/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

//=============================================================================
public class JMXHelper
{
    //-------------------------------------------------------------------------
	private JMXHelper() {}

    //-------------------------------------------------------------------------
	public static <T> boolean isNotNullAndAssignable(Object aObject, final Class<T> aClass)
	{
		return (null != aObject) && (aClass.isAssignableFrom(aObject.getClass()));
	}

    //-------------------------------------------------------------------------
    public static boolean containsPositiveNonZeroEntry(final int[] intArray) throws Exception
    {
        boolean bNonZeroEntry = false;

        if (null == intArray)
            return bNonZeroEntry;

        for (int n : intArray)
        {
            bNonZeroEntry |= (n > 0);
            if (bNonZeroEntry)
                break;
        }

        return bNonZeroEntry;
    }
}
