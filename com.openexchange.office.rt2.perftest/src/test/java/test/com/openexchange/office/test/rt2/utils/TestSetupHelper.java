/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.DocumentFactory;
import com.openexchange.office.rt2.perftest.doc.TestDocProvider;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2Env;
import com.openexchange.office.rt2.perftest.fwk.RT2;

public class TestSetupHelper
{
    //-------------------------------------------------------------------------
	private TestSetupHelper() {}

    //-------------------------------------------------------------------------
	public static OXAppsuite openAppsuiteConnection(final TestRunConfig aConfig) throws Exception
	{
        final OXAppsuite aAppsuite = new OXAppsuite  (aConfig);

        aAppsuite.accessSession().login();
        return aAppsuite;
	}

    //-------------------------------------------------------------------------
    public static OXAppsuite openAppsuiteAlternativeConnection(final TestRunConfig aConfig) throws Exception
    {
        final OXAppsuite aAppsuite = new OXAppsuite  (aConfig, true);
        aAppsuite.accessSession().login();
        return aAppsuite;
    }

    //-------------------------------------------------------------------------
	public static void closeAppsuiteConnections(final OXAppsuite aAppsuite) throws Exception
	{
        aAppsuite.accessSession().logout();
        aAppsuite.shutdownWSConnection();
	}

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public static < T extends Doc > T  newDocInst (final OXAppsuite aAppsuite        ,
                                                   final String     sDocType         ,
                                                   final String     sFolderID        ,
                                                   final String     sFileID          ,
                                                   final String     sDriveDocID      ,
                                                   boolean          bAutoCloseOnError)
        throws Exception
    {
        final Doc aDoc = newDocInst (aAppsuite, sDocType, bAutoCloseOnError);
        aDoc.setFolderId   (sFolderID  );
        aDoc.setFileId     (sFileID    );
        aDoc.setDriveDocId (sDriveDocID);
        return (T) aDoc;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public static < T extends Doc > T newDocInstForSpecialTestFile (final OXAppsuite aAppsuite    ,
                                                                          String     sDocType     ,
                                                                    final String     sTestFileName)
        throws Exception
    {
        final Map< String, String > lFileData = TestDocProvider.getTestFileData(sTestFileName);

        if (StringUtils.isEmpty(sDocType))
            sDocType = lFileData.get (TestDocProvider.DOC_TYPE);

        final Doc aDoc = newDocInst (aAppsuite, sDocType);

        aDoc.setFolderId  (lFileData.get (TestDocProvider.FOLDER_ID   ));
        aDoc.setFileId    (lFileData.get (TestDocProvider.FILE_ID     ));
        aDoc.setDriveDocId(lFileData.get (TestDocProvider.DRIVE_DOC_ID));

        return (T) aDoc;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public static < T extends Doc > T newDocInst (final OXAppsuite aAppsuite,
                                                  final String     sDocType )
        throws Exception
    {
        final OXRT2Env aRT2     = aAppsuite.accessRT2Env ();
        final RT2      aDocImpl = aRT2     .newRT2Inst   ();
        final Doc      aDoc     = DocumentFactory.create(sDocType);

        aDoc.bind                     (aDocImpl);
        aDoc.setTimeouts              (10000, 10000, 10000, /*continue on timeout*/true, /* auto close on error */true);
        aDoc.enableOperationsRecording(true);
        aDoc.enableDocEvents          (true);

        return (T) aDoc;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public static < T extends Doc > T newDocInst (final OXAppsuite aAppsuite,
                                                  final String     sDocType ,
                                                  boolean          bAutoCloseOnError)
        throws Exception
    {
        final OXRT2Env aRT2     = aAppsuite.accessRT2Env ();
        final RT2      aDocImpl = aRT2     .newRT2Inst   ();
        final Doc      aDoc     = DocumentFactory.create(sDocType);

        aDoc.bind                     (aDocImpl);
        aDoc.setTimeouts              (10000, 10000, 10000, /*continue on timeout*/true, bAutoCloseOnError);
        aDoc.enableOperationsRecording(true);
        aDoc.enableDocEvents          (true);

        return (T) aDoc;
    }

}
