/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;

import test.com.openexchange.office.test.rt2.utils.JMXClient;
import test.com.openexchange.office.test.rt2.utils.RT2JmxClient;
import test.com.openexchange.office.test.rt2.utils.SessionJmxClient;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2JmxClientTest {

    //-------------------------------------------------------------------------
    private static final long RT2_GCTEST_TIMEOUT = 180000;

    //-------------------------------------------------------------------------
    private static final long RT2_GCTEST_FREQUENCY = 30000;

    //-------------------------------------------------------------------------
    @Test
    public void testJMXClient()
        throws Exception
    {
        final String RT2_OBJNAME_DOC_ON_NODE_MAP  = "com.openexchange.office.rt2:name=DocOnNodeMap";
        final String RT2_ATTR_MAP_DOC_ON_NODE_MAP = "DocNodeMapping";

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01 ();

        // check state of the backend node to verify that all
        // resources have been free'd
        final JMXClient aJmxClient = new JMXClient(aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        try
        {
            aJmxClient.connect();
            final Object aObject = aJmxClient.getAttribute(RT2_OBJNAME_DOC_ON_NODE_MAP, RT2_ATTR_MAP_DOC_ON_NODE_MAP);

            Assert.assertNotNull(aObject);
            Assert.assertTrue(aObject instanceof Map);
        }
        catch (Exception e)
        {
            Assert.fail("Exception caught while trying to check backend state: " + e.getMessage());
        }
        finally
        {
            aJmxClient.disconnect();
        }
    }

    //-------------------------------------------------------------------------
    @Test
    public void testRT2JmxClient()
        throws Exception
    {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode    = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite   = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final Doc           aDoc        = TestSetupHelper.newDocInst (aAppsuite, Doc.DOCTYPE_TEXT);

        aDoc.createNew();
        aDoc.open     ();
        aDoc.applyOps ();

        final RT2JmxClient aRT2JmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aRT2JmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aRT2JmxClient.isConnected());

        final Integer nMsgWaitingForAck = aRT2JmxClient.getMessageCountWaitingForAckOfDoc(aDoc.getRT2ClientUID(),  aDoc.getDocUID());
        Assert.assertNotNull(nMsgWaitingForAck);

        aDoc.close    ();
        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testSessionJmxClient()
        throws Exception
    {
        final TestRunConfig aEnvNode  = TestRunConfigHelper.defineEnv4Node01 ();

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final Long gcTimeout = aSessionJmxClient.getGCTimeoutAttributeFromServer();
        if (null == gcTimeout) {
            Assert.fail("Couldn't find garbage collector timeout value via JMX");
        }

        final Long gcFrequency = aSessionJmxClient.getGCFrequencyAttributeFromServer();
        if (null == gcFrequency) {
            Assert.fail("Couldn't find garbage collector frequency value via JMX");
        }

        aSessionJmxClient.setGCTiming(RT2_GCTEST_TIMEOUT, RT2_GCTEST_FREQUENCY);

        Long newGCTimeout = aSessionJmxClient.getGCTimeoutAttributeFromServer();
        if (null == newGCTimeout) {
            Assert.fail("Couldn't find new garbage collector timeout value via JMX");
        }

        final Long newGCFrequency = aSessionJmxClient.getGCFrequencyAttributeFromServer();
        if (null == newGCFrequency) {
            Assert.fail("Couldn't find new garbage collector frequency value via JMX");
        }

        // reset old value
        aSessionJmxClient.setGCTiming(gcTimeout, gcFrequency);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testBackendAttributesJmxClient()
        throws Exception
    {
        final TestRunConfig aEnvNode  = TestRunConfigHelper.defineEnv4Node01 ();

        final RT2JmxClient aJmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        aJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aJmxClient.isConnected());

        // Check rt3 backend properties and that they provide reasonable values
        final Integer countDocProcessors = aJmxClient.getCountDocProcessors();
        final Integer countDocProxies = aJmxClient.getCountDocProxies();

        final int valueCountDocProcessors = (countDocProcessors != null) ? ((int)countDocProcessors) : -1;
        final int valueCountDocProxies = (countDocProxies != null) ? ((int)countDocProxies) : -1;
        Assert.assertTrue(valueCountDocProcessors >= 0);
        Assert.assertTrue(valueCountDocProxies >= 0);
    }
}
