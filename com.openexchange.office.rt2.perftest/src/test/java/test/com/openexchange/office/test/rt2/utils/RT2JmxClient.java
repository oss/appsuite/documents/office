/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;

//=============================================================================
@SuppressWarnings("rawtypes")
public class RT2JmxClient extends JmxClientBase {

	// -------------------------------------------------------------------------
	private static final String STR_INSTANCE = "instance";
	private static final String STR_NAME = "name";
	private static final String STR_TYPE = "type";

	// -------------------------------------------------------------------------
	private static final String STR_HAZELCAST_STRUCTURE = "com.hazelcast";
	private static final String STR_HAZELCAST_IATOMICLONG = "IAtomicLong";
	private static final String STR_HAZELCAST_PREFIX = "_hzInstance_1_";
	private static final String STR_HAZELCAST_IATOMICLONG_VALUE = "currentValue";

	// -------------------------------------------------------------------------
	private static final String STR_RT2_ATOMIC_PREFIX = "rt2cache.atomic.";
	private static final String STR_RT2_ATOMIC_POSTFIX = ".refcount.clients.a";

	// -------------------------------------------------------------------------
	private static final String OBJNAME_DOC_ON_NODE_MAP = "com.openexchange.office.rt2:name=DocOnNodeMap";
	private static final String ATTR_MAP_DOC_ON_NODE_MAP = "DocNodeMapping";
	private static final Class<Map> DOC_ON_NODE_MAP_CLASS = Map.class;

	protected static final String OBJNAME_BACKEND_MONITORING = "com.openexchange.office.rt2:name=BackendMonitoring";
	private static final String OP_REMOVEDOCONNODEMAPENTRY = "removeDocOnNodeMapEntry";
	private static final String OP_ISACTIVEDOCPROCONNODE = "isActiveDocProcessorOnThisNode";
	private static final String OP_GETCLIENTSOFDOC = "getClientsOfDocUid";

	private static final String OBJNAME_WEBSOCKET_MONITORING = "com.openexchange.office.rt2:name=WebsocketMonitoring";
	private static final String OBJNAME_RT2_METRICS_CLIENTREGISTRY = "com.openexchange.office.rt2.metrics:name=ClientRegistry.size";

	private static final String OBJNAME_OFFICE_MONITORING = "com.openexchange.office:name=OfficeMonitoring";
	private static final String ATTR_ADVISORY_LOCK_INFO_MODE = "AdvisoryLockInfo_Mode";

	private static final String ACKDATASTRING_COUNT = "count: ";
	private static final String ACKDATASTRING_SEPARATOR = ",";

	// -------------------------------------------------------------------------
	// com.hazelcast:instance=_hzInstance_1_localhorstCluster,type=IAtomicLong,name=rt2cache.atomic.2c28820eafe8592c31782dec037eddbc.refcount.clients.a

	public RT2JmxClient(String sClusterName, String sServerName, int nPort) {
		super(sClusterName, sServerName, nPort);
	}

	// -------------------------------------------------------------------------
	public String getHazelcastDocMapEntry(String sDocUID) throws Exception {
		final Object aAttribute = this.getAttribute(OBJNAME_DOC_ON_NODE_MAP, ATTR_MAP_DOC_ON_NODE_MAP);
		if (JMXHelper.isNotNullAndAssignable(aAttribute, DOC_ON_NODE_MAP_CLASS)) {
			final Map aMap = (Map) aAttribute;
			final Set aSet = aMap.keySet();
			final Optional aKey = aSet.stream().findFirst();

			return (String) aKey.get();
		}

		throw new NoSuchElementException();
	}

	// -------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public Set<String> findHazelcastDocMapEntries(final Set<String> aDocUIDs) throws Exception {
		Validate.notNull(aDocUIDs);

		final Object aAttribute = getAttribute(OBJNAME_DOC_ON_NODE_MAP, ATTR_MAP_DOC_ON_NODE_MAP);
		if (JMXHelper.isNotNullAndAssignable(aAttribute, DOC_ON_NODE_MAP_CLASS)) {
			final Map aMap = (Map) aAttribute;
			final Set aSet = aMap.keySet();
			// we just collect String entries therefore this cast is safe
			return (Set<String>) aSet.stream().filter(s -> ((s instanceof String) && aDocUIDs.contains(s)))
					.collect(Collectors.toSet());
		}

		throw new NoSuchElementException();
	}

	// -------------------------------------------------------------------------
	public Long getHazelcastAtomicLongValue(String sDocUID) throws Exception {
		final String sObjName = getHazelcastAtomicLongObjectName(sDocUID, getClusterName());
		final Object aAttribute = getAttribute(sObjName, STR_HAZELCAST_IATOMICLONG_VALUE);

		if (JMXHelper.isNotNullAndAssignable(aAttribute, Long.class))
			return (Long) aAttribute;
		else
			return null;
	}

	// -------------------------------------------------------------------------
	public Set<String> findHazelcastAtomicLongValues(final Set<String> aDocUIDs) throws Exception {
		Validate.notNull(aDocUIDs);

		final String sClusterName = getClusterName();

		Set<String> names = aDocUIDs.stream().map(s -> RT2JmxClient.getHazelcastAtomicLongObjectName(s, sClusterName))
				.collect(Collectors.toSet());
		return names.stream().filter(s -> hasHazelcastAtomicLongValue(s)).collect(Collectors.toSet());
	}

	// -------------------------------------------------------------------------
	public Long getGCTimeoutAttributeFromServer() throws Exception {
		final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "GCTimeout");
		if ((null != value) && (value instanceof Long)) {
			return (Long) value;
		} else {
			return null;
		}
	}

	// -------------------------------------------------------------------------
	public Long getGCFrequencyAttributeFromServer() throws Exception {
		final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "GCFrequency");
		if ((null != value) && (value instanceof Long)) {
			return (Long) value;
		} else {
			return null;
		}
	}

	// -------------------------------------------------------------------------
	public void setGCTiming(long gcOfflineTimeout, long gcFrequency) throws Exception {
		final Long[] argValues = new Long[] { gcOfflineTimeout, gcFrequency };
		final String[] argTypes = new String[] { "java.lang.Long", "java.lang.Long" };
		invokeOperation(OBJNAME_BACKEND_MONITORING, "setGCTiming", argValues, argTypes);
	}

	// -------------------------------------------------------------------------
	public Integer getCountDocProcessors() throws Exception {
		final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "CountRT2DocProcessors");
		if ((null != value) && (value instanceof Integer)) {
			return (Integer) value;
		}
		return null;
	}

	// -------------------------------------------------------------------------
	public Integer getCountDocProxies() throws Exception {
		final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "CountRT2DocProxies");
		if ((null != value) && (value instanceof Integer)) {
			return (Integer) value;
		}
		return null;
	}

	// -------------------------------------------------------------------------
	public Integer getCountClientInfos() throws Exception {
		final Object value = getAttribute(OBJNAME_RT2_METRICS_CLIENTREGISTRY, "Value");
		if ((null != value) && (value instanceof Integer)) {
			return (Integer) value;
		}
		return null;
	}

	// -------------------------------------------------------------------------
	public void setNackFrequenceOfServer(int value) {
		Object[] argValues = { value };
		String[] argTypes = { Integer.class.getName() };
		try {
			invokeOperation(OBJNAME_BACKEND_MONITORING, "updateNackFrequenceOfServer", argValues, argTypes);
		} catch (Exception e) {
		}
	}

	// -------------------------------------------------------------------------
	public Integer getMessageCountWaitingForAckOfDoc(String clientUID, String docUID) throws Exception {
		final Object data = getAttribute(OBJNAME_BACKEND_MONITORING, "StatusMsgsWaitingForAckOfDoc");
		if ((null != data) && (data instanceof Map)) {
			@SuppressWarnings("unchecked")
			final Map<String, String> docClientAckMap = (Map<String, String>) data;
			final String key = docUID + "_" + clientUID;
			final String clientAckData = docClientAckMap.get(key);
			return getCountFromAckDataString(clientAckData);
		}
		return null;
	}

	// -------------------------------------------------------------------------
	public List<String> getAllActiveWSChannels() throws Exception {
		final Object data = getAttribute(OBJNAME_WEBSOCKET_MONITORING, "AllActiveWSChannels");
		@SuppressWarnings("unchecked")
		final List<String> result = (List<String>) data;
		return result;
	}

	// -------------------------------------------------------------------------
	public List<String> getAllOfflineWSChannels() throws Exception {
		final Object data = getAttribute(OBJNAME_WEBSOCKET_MONITORING, "AllOfflineWSChannels");
		@SuppressWarnings("unchecked")
		final List<String> result = (List<String>) data;
		return result;
	}

    // -------------------------------------------------------------------------
	public String getAdvisoryLockInfoMode() throws Exception {
	    final Object data = getAttribute(OBJNAME_OFFICE_MONITORING, ATTR_ADVISORY_LOCK_INFO_MODE);
        final String result = (String) data;
        return result;
	}

    // -------------------------------------------------------------------------
	public boolean removeDocOnNodeMapEntry(String docUid) throws Exception {
        final Object result = invokeOperation(OBJNAME_BACKEND_MONITORING,
            OP_REMOVEDOCONNODEMAPENTRY,
            new String[] { docUid },
            new String[] { String.class.getName()
            });
        return (Boolean)result;
	}

	// -------------------------------------------------------------------------
	public boolean isActiveDocProcessorOnThisNode(String docUid) throws Exception {
        final Object result = invokeOperation(OBJNAME_BACKEND_MONITORING,
        		OP_ISACTIVEDOCPROCONNODE,
                new String[] { docUid },
                new String[] { String.class.getName()
                });
            return (Boolean)result;
	}

	// -------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public Set<String> getClientsOfDoc(String docUid) throws Exception {
        final Object result = invokeOperation(OBJNAME_BACKEND_MONITORING,
        		OP_GETCLIENTSOFDOC,
                new String[] { docUid },
                new String[] { String.class.getName()
                });
            return (Set<String>)result;
	}

	// -------------------------------------------------------------------------
	private boolean hasHazelcastAtomicLongValue(String sObjectName) {
		boolean result = false;

		try {
			result = Objects.nonNull(getAttribute(sObjectName, STR_HAZELCAST_IATOMICLONG_VALUE));
		} catch (Exception e) {
			// unknown state - therefore return false
			return false;
		}

		return result;
	}

    //-------------------------------------------------------------------------
    public long getCountInvalidatedSessionNotifications() throws Exception {
		return (Long)getAttribute(OBJNAME_BACKEND_MONITORING, "CountSessionInvalidNotificationsReceived");
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
	public List<String> getLatestInvalidatedSessionsViaNotifications() throws Exception {
		final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "LastSessionInvalidNotificationsReceived");
		if ((value != null) && (value instanceof List<?>)) {
			return (List<String>)value;
		}
		return new ArrayList<String>();
    }

	// -------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public List<String> getRegisteredRT2LockObjects() throws Exception {
        final Object value = getAttribute(OBJNAME_BACKEND_MONITORING, "RegisteredRT2LockObjects");
        if ((value != null) && (value instanceof List<?>)) {
            return (List<String>)value;
        }
        return new ArrayList<String>();
    }

	// -------------------------------------------------------------------------
	private static String getHazelcastAtomicLongObjectName(String sDocUID, String sClusterName) {
		final StringBuilder sTmp = new StringBuilder(STR_HAZELCAST_STRUCTURE);
		sTmp.append(":");
		sTmp.append(STR_INSTANCE);
		sTmp.append("=");
		sTmp.append(STR_HAZELCAST_PREFIX);
		sTmp.append(sClusterName);
		sTmp.append(",");
		sTmp.append(STR_TYPE);
		sTmp.append("=");
		sTmp.append(STR_HAZELCAST_IATOMICLONG);
		sTmp.append(",");
		sTmp.append(STR_NAME);
		sTmp.append("=");
		sTmp.append(STR_RT2_ATOMIC_PREFIX);
		sTmp.append(sDocUID);
		sTmp.append(STR_RT2_ATOMIC_POSTFIX);

		return sTmp.toString();
	}

	// -------------------------------------------------------------------------
	private Integer getCountFromAckDataString(String ackDataString) {
		Integer result = null;

		if (StringUtils.isNotEmpty(ackDataString)) {
			final int numIndex = ackDataString.indexOf(ACKDATASTRING_COUNT);
			final int colonIndex = ackDataString.indexOf(ACKDATASTRING_SEPARATOR);
			if (numIndex >= 0) {
				final int len = colonIndex - numIndex - ACKDATASTRING_COUNT.length();
				final int begIdx = numIndex + ACKDATASTRING_COUNT.length();
				result = Integer.parseInt(ackDataString.substring(begIdx, begIdx + len));
			}
		}

		return result;
	}

}
