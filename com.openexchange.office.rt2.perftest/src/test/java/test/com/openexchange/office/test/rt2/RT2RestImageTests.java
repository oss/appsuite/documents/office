/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.NewDocumentData;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXDocRestAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;
import com.openexchange.office.rt2.perftest.operations.Dimension;
import com.openexchange.office.rt2.perftest.operations.Margin;
import com.openexchange.office.rt2.perftest.operations.Operations;

import test.com.openexchange.office.test.rt2.utils.ImageInfo;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2RestImageTests {

	private static final String TEST_JPG_BASE64 =
			"/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAMCAgMCAgMDAwME"
					+ "AwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBD"
					+ "AQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU"
					+ "FBQUFBQUFBT/wgARCAAQABADAREAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAj/xAAUAQEA"
					+ "AAAAAAAAAAAAAAAAAAAA/9oADAMBAAIQAxAAAAGVAAf/xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oA"
					+ "CAEBAAEFAh//xAAUEQEAAAAAAAAAAAAAAAAAAAAg/9oACAEDAQE/AR//xAAUEQEAAAAAAAAAAAAA"
					+ "AAAAAAAg/9oACAECAQE/AR//xAAUEAEAAAAAAAAAAAAAAAAAAAAg/9oACAEBAAY/Ah//xAAUEAEA"
					+ "AAAAAAAAAAAAAAAAAAAg/9oACAEBAAE/IR//2gAMAwEAAgADAAAAEJJP/8QAFBEBAAAAAAAAAAAA"
					+ "AAAAAAAAIP/aAAgBAwEBPxAf/8QAFBEBAAAAAAAAAAAAAAAAAAAAIP/aAAgBAgEBPxAf/8QAFBAB"
					+ "AAAAAAAAAAAAAAAAAAAAIP/aAAgBAQABPxAf/9k=";
	private static final Dimension aTestJPGDim = new Dimension(16, 16);

	private static final String TEST_PNG_BASE64 =
			"iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAIAAABvFaqvAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA"
					+ "B3RJTUUH4gYZDSsAS6GTpgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH"
					+ "AAAAQElEQVQ4y2NkwA3+NxxgIBowMVAJjBSD/v9HQaNhBAGM/xvwyI4myMFpEAteWQd0gXrkCB8N"
					+ "bKoGNmaOGolhBABYZw9oJkCn7gAAAABJRU5ErkJggg==";
	private static final Dimension aTestPNGDim = new Dimension(24, 24);

	private static final String TEST_GIF_BASE64 =
			"R0lGODlhQABAAKUsAAAAABYWFhwcHCoqKkBAQElJSU9PT1VVVV1dXV9fX2xsbHd3d35+foCAgIiI"
					+ "iJWVlZeXl5+fn6CgoKurq6ysrK2trbu7u7y8vL6+vsDAwMjIyNTU1Nzc3OPj4+Tk5Obm5ufn5+rq"
					+ "6uvr6+3t7fHx8fT09PX19fb29vj4+Pr6+vv7+/z8/P//////////////////////////////////"
					+ "/////////////////////////////////////////////yH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5"
					+ "BAEKAD8ALAAAAABAAEAAAAbIQJZwSCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrN"
					+ "brvf8Lh8Tq/b73gtSJIYHBgWJEIaACODAIiJHGgaAhQhKicbCg2HhiyEl2oeARdFKhiWomsQBStJ"
					+ "maNqBA9KqZiFaygAE66xsIkAlWazFEMpuSWwl6+rrUUZAMKvxWmlp0TJy7fNaJyh0crDqmqNFSIr"
					+ "KB0OACbb52wfEQgDBgsTH6PVefT19vf4+fr7/P3+/wADChxIsKDBgwgTKlxoJQgAOw==";
	private static final Dimension aTestGIFDim = new Dimension(64, 64);

	// same dimension as plain gif
	private static final String TEST_ANIM_GIF_BASE64 =
			"R0lGODlhQABAAPUAAAAAABYWFhwcHCoqKkBAQElJSU9PT1VVVV1dXV9fX2xsbHd3d35+foCAgIiI"
					+ "iJWVlZeXl5+fn6CgoKurq6ysrK2trbu7u7y8vL6+vsDAwMjIyNTU1Nzc3OPj4+Tk5Obm5ufn5+rq"
					+ "6uvr6+3t7fHx8fT09PX19fb29vj4+Pr6+vv7+/z8/P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hFD"
					+ "cmVhdGVkIHdpdGggR0lNUAAh+QQBFAAtACwAAAAAQABAAAAGyECWcEgsGo/IpHLJbDqf0Kh0Sq1a"
					+ "r9isdsvter/gsHhMLpvP6LR6zW673/C4fE6v2+94LUiSGBwYFiRCGgAjgwCIiRxoGgIUISonGwoN"
					+ "h4YshJdqHgEXRSoYlqJrEAUrSZmjagQPSqmYhWsoABOusbCJAJVmsxRDKbklsJevq61FGQDCr8Vp"
					+ "padEycu3zWicodHKw6pqjRUiKygdDgAm2+dsHxEIAwYLEx+j1Xn09fb3+Pn6+/z9/v8AAwocSLCg"
					+ "wYMIEypcaCUIACH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEUAC0ALAAAAABAAEAAAAZhQJZwSCwa"
					+ "j8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh8Tq/b7/h8+KQnqvpEK4CDhIWG"
					+ "h0d/hSmChIpNjYiSk5SVlpeYmZqbnJ2en6ChoqOkpaaTQQA7";

	private static final String TEST_WMF_BASE64 =
			"183GmgAAAAAAAGEB8gHsCQAAAABuXgEACQAAA6sCAAAQAMABAAAAAMABAAAmBg8AdgNXTUZDAQAA"
					+ "AAAAAQC+iAAAAAABAAAAVAMAAAAAAABUAwAAAQAAAGwAAAAAAAAAAAAAAAwAAAASAAAAAAAAAAAA"
					+ "AABgAQAA8QEAACBFTUYAAAEAVAMAACUAAAADAAAAAAAAAAAAAAAAAAAADQAAABMAAAADAAAABAAA"
					+ "AAAAAAAAAAAAAAAAAMoNAAB0EwAARgAAACwAAAAgAAAARU1GKwFAAQAcAAAAEAAAAAIQwNsBAAAA"
					+ "bAAAAHYAAABGAAAATAAAAEAAAABFTUYrIkAAAAwAAAAAAAAAHkAJAAwAAAAAAAAAJEAAAQwAAAAA"
					+ "AAAAIUAAAAwAAAAAAAAABEAAAAwAAAAAAAAAEQAAAAwAAAAIAAAACwAAABAAAABgAAAAYAAAAAkA"
					+ "AAAQAAAA7AkAAOwJAAAMAAAAEAAAAAAAAAAAAAAACgAAABAAAAAAAAAAAAAAABQAAAAMAAAADQAA"
					+ "ABIAAAAMAAAAAQAAACEAAAAIAAAAHgAAABgAAAAAAAAAAAAAAGEBAADyAQAAIQAAAAgAAAAhAAAA"
					+ "CAAAACcAAAAYAAAAAQAAAAAAAAByn88AAAAAACUAAAAMAAAAAQAAACYAAAAcAAAAAgAAAAUAAAAA"
					+ "AAAAAAAAAP///wAlAAAADAAAAAIAAAADAAAATAAAABoAAAAPAAAAJQAAABkAAAAGAAAAHwAAABkA"
					+ "AAAaAAAAGQAAABoAAAAPAAAAJQAAAA8AAAAlAAAAGQAAAB8AAAAZAAAAIgAAAAwAAAD/////FAAA"
					+ "AAwAAAANAAAAIQAAAAgAAAAlAAAADAAAAAcAAIAoAAAADAAAAAIAAAAmAAAAHAAAAAIAAAAAAAAA"
					+ "AAAAAAAAAAA0ZaQAJQAAAAwAAAACAAAABAAAAEwAAAAaAAAADwAAACUAAAAZAAAABgAAAB8AAAAZ"
					+ "AAAAGgAAABkAAAAaAAAADwAAACUAAAAPAAAAJQAAABkAAAAfAAAAGQAAACIAAAAMAAAA/////xQA"
					+ "AAAMAAAADQAAACIAAAAMAAAA/////xQAAAAMAAAADQAAACEAAAAIAAAAIgAAAAwAAAD/////FAAA"
					+ "AAwAAAANAAAAIgAAAAwAAAD/////FAAAAAwAAAANAAAARgAAABwAAAAQAAAARU1GKwJAAAAMAAAA"
					+ "AAAAAA4AAAAUAAAAAAAAABAAAAAUAAAABQAAAAsCAAAAAAUAAAAMAvIBYQEEAAAAAgEBAAQAAAAE"
					+ "AQ0ACAAAAPoCAAAAAAAAAAAAAAQAAAAtAQAABwAAAPwCAAD///8AAAAEAAAALQEBABwAAAD7AgAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAC0BAgAE"
					+ "AAAALgEYAAUAAAAJAv///wAEAAAABwEDAAMAAAAeAAcAAAAWBPMBYgEAAAAAAwAAAB4AAwAAAB4A"
					+ "CAAAAPoCBQAAAAAA////AAQAAAAtAQMABAAAAPABAAAHAAAA/AIAAHKfzwAAAAQAAAAtAQAABAAA"
					+ "APABAQARAAAAOAUBAAYAHwAZABoAGQAaAA8AJQAPACUAGQAfABkABAAAACcB//8DAAAAHgAIAAAA"
					+ "+gIAAAAAAAA0ZaQABAAAAC0BAQAEAAAA8AEDAAcAAAD8AgEA////AAAABAAAAC0BAwAEAAAA8AEA"
					+ "ABAAAAAlAwYAHwAZABoAGQAaAA8AJQAPACUAGQAfABkABAAAACcB//8EAAAAJwH//wMAAAAeAAQA"
					+ "AAAnAf//BAAAACcB//8DAAAAAAA=";

	private static final String IMAGE_DOCX_PREFIX = "word/media/xid";
	private static final Optional<Integer> empty = Optional.empty();

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_AddFileAndGetFile() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final byte[] testJPGData = Base64.decode(TEST_JPG_BASE64);
		final String aSHA256JPG = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testJPGData), true);

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		final NewDocumentData aNewDocData = aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		String sAddedFileName = null;
		String sAddedFileId = null;

		// JPG
		try {
			final JSONObject aResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_JPG,
					TEST_JPG_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), aSHA256JPG);
			sAddedFileName = aResult.optString("added_filename");
			sAddedFileId = aResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'addfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_JPG) > IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), empty, empty);
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			Assert.assertTrue("Not a jpg stream", ImageInfo.isJPGData(aResultImageData));
			Assert.assertTrue("Dimension of image not as expected",
					ImageInfo.getImageInfo(aResultImageData).getDimension().equals(aTestJPGDim));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// PNG
		final byte[] testPNGData = Base64.decode(TEST_PNG_BASE64);
		final String aSHA256PNG = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testPNGData), true);
		try {
			final JSONObject aResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_PNG,
					TEST_PNG_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), null);
			sAddedFileName = aResult.optString("added_filename");
			sAddedFileId = aResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'addfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_PNG) > IMAGE_DOCX_PREFIX.length());
			Assert.assertTrue("Unexpected SHA-256 value generated by REST-service 'addfile'",
					sAddedFileName.indexOf(aSHA256PNG) == IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), empty, empty);
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			Assert.assertTrue("Not a png stream", ImageInfo.isPNGData(aResultImageData));
			Assert.assertTrue("Dimension of image not as expected",
					ImageInfo.getImageInfo(aResultImageData).getDimension().equals(aTestPNGDim));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// GIF
		final byte[] testGIFData = Base64.decode(TEST_GIF_BASE64);
		final String aSHA256GIF = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testGIFData), true);
		try {
			final JSONObject aResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_GIF,
					TEST_GIF_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), null);
			sAddedFileName = aResult.optString("added_filename");
			sAddedFileId = aResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'addfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_GIF) > IMAGE_DOCX_PREFIX.length());
			Assert.assertTrue("Unexpected SHA-256 value generated by REST-service 'addfile'",
					sAddedFileName.indexOf(aSHA256GIF) == IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), empty, empty);
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			Assert.assertTrue("Dimension of image not as expected",
					ImageInfo.getImageInfo(aResultImageData).getDimension().equals(aTestGIFDim));
			// GIF can be transformed to JPG depending on the converter - fallback
			// by MW is JPG while IC does not transform by default anymore.
			Assert.assertTrue("Not a png or jpg stream", ImageInfo.isPNGData(aResultImageData) || ImageInfo.isJPGData(aResultImageData));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// animated GIF
		final byte[] testAnimGIFData = Base64.decode(TEST_ANIM_GIF_BASE64);
		final String aSHA256AnimGIF = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testAnimGIFData), true);
		try {
			final JSONObject aResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_GIF,
					TEST_ANIM_GIF_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), null);
			sAddedFileName = aResult.optString("added_filename");
			sAddedFileId = aResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'addfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_GIF) > IMAGE_DOCX_PREFIX.length());
			Assert.assertTrue("Unexpected SHA-256 value generated by REST-service 'addfile'",
					sAddedFileName.indexOf(aSHA256AnimGIF) == IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), empty, empty);
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			Assert.assertTrue("Not a gif stream", ImageInfo.isGIFData(aResultImageData));
			Assert.assertTrue("Dimension of image not as expected",
					ImageInfo.getImageInfo(aResultImageData).getDimension().equals(aTestGIFDim));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		aDocUser01.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_AddFileAndGetFile_ConvertWithDocFile() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		final NewDocumentData aNewDocData = aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		String sAddedFileName = null;
		String sAddedFileId = null;

		// WMF
		final byte[] testWMFData = Base64.decode(TEST_WMF_BASE64);
		final String aSHA256WMF = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testWMFData), true);

		Assert.assertTrue(ImageInfo.isWMFData(testWMFData));

		JSONObject addFileResult = null;
		try {
			addFileResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_WMF,
					TEST_WMF_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), null);
			sAddedFileName = addFileResult.optString("added_filename");
			sAddedFileId = addFileResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'addfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_WMF) > IMAGE_DOCX_PREFIX.length());
			Assert.assertTrue("Unexpected SHA-256 value generated by REST-service 'addfile'",
					sAddedFileName.indexOf(aSHA256WMF) == IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		final Dimension scaled = new Dimension(128, 128);
		final Margin margin = new Margin(317, 317, 317, 317);

		// add operations to save document with added wmf image
		if (null != sAddedFileName) {
			aDocUser01.addImageID(addFileResult);

			// create operation to insert a JPG drawing with the added image
			final JSONArray lOps = new JSONArray();
			final JSONObject aOp1 = Operations.createInsertParagraph(0, aDocUser01);
			lOps.put(aOp1);

			final JSONObject aOp2 = Operations.createInsertDrawingOperation(0, 0, "Test-WMF", scaled, margin, sAddedFileName, aDocUser01);
			lOps.put(aOp2);

			aDocUser01.applyOps(lOps);
		} else {
			Assert.fail("Impossible case - addfile JPG and/or PNG failed without being detected!");
		}

		aDocUser01.close();

		// ensure that we use a different client instance to prevent DOCS-3009 issue.
		final TextDoc aDocToVerify = TestSetupHelper.newDocInst(aAppsuite01, aDocUser01.getType(),
				aDocUser01.getFolderId(),aDocUser01.getFileId(), aDocUser01.getDriveDocId(), true);
		boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocToVerify, sAddedFileName);
		Assert.assertTrue("Html document doesn't contain the image previously added!", bVerified);

		// verify that we get the image data from the document via 'getfile'
		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), Optional.of(scaled.getWidth()), Optional.of(scaled.getHeight()));
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			// we accept the original or converted stream as the result depends on certain conditions
			final String convertedImageFormat = ImageInfo.getImageFormat(aResultImageData);
			Assert.assertTrue(isImageFormat(convertedImageFormat, ImageInfo.MIMETYPE_JPG, ImageInfo.MIMETYPE_PNG, ImageInfo.MIMETYPE_WMF));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_AddFileAndGetFile_CheckSecurity() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final byte[] testJPGData = Base64.decode(TEST_JPG_BASE64);
		final String aSHA256JPG = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testJPGData), true);

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode01User2 = TestRunConfigHelper.defineEnv4Node01User2();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite01U02 = TestSetupHelper.openAppsuiteConnection(aEnvNode01User2);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		final NewDocumentData aNewDocData = aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		final OXDocRestAPI aDoc1U2RESTAPI = new OXDocRestAPI();
		aDoc1U2RESTAPI.bind(aAppsuite01U02);

		aDocUser01.open();

		String sAddedFileName = null;
		String sAddedFileId = null;

		try {
			final JSONObject aResult = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_JPG,
					TEST_JPG_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), aSHA256JPG);
			sAddedFileName = aResult.optString("added_filename");
			sAddedFileId = aResult.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileName));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileId));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileName.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'adfile'",
					sAddedFileName.indexOf(ImageInfo.EXTENSION_JPG) > IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		try {
			final InputStream aImageIS = aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1",
					sAddedFileName, aNewDocData.getFileName(), empty, empty);
			final byte[] aResultImageData = IOUtils.toByteArray(aImageIS);

			Assert.assertTrue("Result stream is empty!", ((aResultImageData != null) && (aResultImageData.length > 0)));
			Assert.assertTrue("Not a jpg stream", ImageInfo.isJPGData(aResultImageData));
			Assert.assertTrue("Dimension of image not as expected",
					ImageInfo.getImageInfo(aResultImageData).getDimension().equals(aTestJPGDim));
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// check that we cannot read a non-existing image using a random SHA-256 value
		final byte[] randomData = { 1,2,3,4,5,6,7,8,9,10 };
		final String nonExistingFileName = IMAGE_DOCX_PREFIX + Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(randomData), true);
		try {
			aDoc1RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1", nonExistingFileName, aNewDocData.getFileName(), empty, empty);
			Assert.fail("Server has to return an error as image with name " + nonExistingFileName + " does not exists!");
		} catch (final ServerResponseException e) {
			Assert.assertTrue("Http status code 404 expected", e.getStatusCode() == HttpStatusCode.NOT_FOUND);
		}

		// check that we cannot get the image data of a document that we have no access to
		try {
			aDoc1U2RESTAPI.getFile(aDocUser01.getFileId(), aDocUser01.getFolderId(), "1", sAddedFileName, aNewDocData.getFileName(), empty, empty);
			Assert.fail("Server has to return an error as user 2 has no access rights to the file from user 1");
		} catch (final ServerResponseException e) {
			Assert.assertTrue("Http status code 403 expected", e.getStatusCode() == HttpStatusCode.FORBIDDEN);
		}

		aDocUser01.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	@Test
	public void test_RESTAPI_AddFileAndInsertImageIntoDocument() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final byte[] testJPGData = Base64.decode(TEST_JPG_BASE64);
		final String aSHA256JPG = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testJPGData), true);
		final Dimension useJPGDim = new Dimension(423, 423);
		final Margin aTestJPGMargin = new Margin(317, 317, 317, 317);

		final byte[] testPNGData = Base64.decode(TEST_PNG_BASE64);
		final String aSHA2562PNG = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(testPNGData), true);
		final Dimension usePNGDim = new Dimension(635, 635);
		final Margin aTestPNGMargin = new Margin(317, 317, 317, 317);

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

		final TextDoc aDocUser01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT, false);
		final NewDocumentData aNewDocData = aDocUser01.createNew();

		final OXDocRestAPI aDoc1RESTAPI = new OXDocRestAPI();
		aDoc1RESTAPI.bind(aAppsuite01);

		aDocUser01.open();

		String sAddedFileNameJPG = null;
		String sAddedFileIdJPG = null;
		JSONObject aAddImageResultJPG = null;

		// -------------------------------------------------------------------------
		try {
			aAddImageResultJPG = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_JPG,
					TEST_JPG_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), aSHA256JPG);
			sAddedFileNameJPG = aAddImageResultJPG.optString("added_filename");
			sAddedFileIdJPG = aAddImageResultJPG.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileNameJPG));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileIdJPG));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileNameJPG.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'adfile'",
					sAddedFileNameJPG.indexOf(ImageInfo.EXTENSION_JPG) > IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		String sAddedFileNamePNG = null;
		String sAddedFileIdPNG = null;
		JSONObject aAddImageResultPNG = null;

		// -------------------------------------------------------------------------
		try {
			aAddImageResultPNG = aDoc1RESTAPI.addFile(aNewDocData.getFileName(), ImageInfo.EXTENSION_PNG,
					TEST_PNG_BASE64, aDocUser01.getFileId(), aDocUser01.getFolderId(), aSHA2562PNG);
			sAddedFileNamePNG = aAddImageResultPNG.optString("added_filename");
			sAddedFileIdPNG = aAddImageResultPNG.optString("added_fileid");

			Assert.assertTrue("Unexpected result for added_filename", StringUtils.isNotEmpty(sAddedFileNamePNG));
			Assert.assertTrue("Unexpected result for added_fileid", StringUtils.isNotEmpty(sAddedFileIdPNG));
			Assert.assertTrue("Unexpected media string prefix from REST-service 'addfile'",
					sAddedFileNamePNG.indexOf(IMAGE_DOCX_PREFIX) == 0);
			Assert.assertTrue("Unexpected media string postfix from REST service 'adfile'",
					sAddedFileNamePNG.indexOf(ImageInfo.EXTENSION_PNG) > IMAGE_DOCX_PREFIX.length());
		} catch (final ServerResponseException e) {
			Assert.fail("Server sent not expected response: " + e.getMessage());
		}

		// -------------------------------------------------------------------------
		if ((null != aAddImageResultJPG) && (null != aAddImageResultPNG)) {
			aDocUser01.addImageID(aAddImageResultJPG);

			// create operation to insert a JPG drawing with the added image
			final JSONArray lOps = new JSONArray();
			final JSONObject aOp1 = Operations.createInsertParagraph(0, aDocUser01);
			lOps.put(aOp1);

			final String sImageUrlJPG = sAddedFileNameJPG;
			final JSONObject aOp2 = Operations.createInsertDrawingOperation(0, 0, "Test-JPG", useJPGDim, aTestJPGMargin, sImageUrlJPG, aDocUser01);
			lOps.put(aOp2);

			// -------------------------------------------------------------------------
			aDocUser01.addImageID(aAddImageResultPNG);

			// create operation to insert a PNG drawing with the added image
			final JSONObject aOp3 = Operations.createInsertParagraph(0, aDocUser01);
			lOps.put(aOp3);

			final String sImageUrlPNG = sAddedFileNamePNG;
			final JSONObject aOp4 = Operations.createInsertDrawingOperation(0, 0, "Test-PNG", usePNGDim,
					aTestPNGMargin, sImageUrlPNG, aDocUser01);
			lOps.put(aOp4);

			aDocUser01.applyOps(lOps);
		} else {
			Assert.fail("Impossible case - addfile JPG and/or PNG failed without being detected!");
		}

		aDocUser01.close();

		// ensure that we use a different client instance to prevent DOCS-3009 issue.
		final TextDoc aDocToVerify = TestSetupHelper.newDocInst(aAppsuite01, aDocUser01.getType(),
				aDocUser01.getFolderId(),aDocUser01.getFileId(), aDocUser01.getDriveDocId(), true);
		boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocToVerify, sAddedFileNameJPG,   sAddedFileNamePNG);
		Assert.assertTrue("Html document doesn't contain the image previously added!", bVerified);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
	}

	// -------------------------------------------------------------------------
	private boolean isImageFormat(String format, String ... expectedFormats) {
		return (((expectedFormats != null) ? (Arrays.asList(expectedFormats).stream().filter(e -> e.equals(format)).count()) : 0) > 0);
	}

}
