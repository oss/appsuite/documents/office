/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

import test.com.openexchange.office.test.rt2.utils.DocHelper;
import test.com.openexchange.office.test.rt2.utils.JMXHelper;
import test.com.openexchange.office.test.rt2.utils.RT2JmxClient;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.SessionJmxClient;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

@Ignore
public class RT2GCTest {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(RT2GCTest.class);

    //-------------------------------------------------------------------------
    private static final long RT2_GCTEST_TIMEOUT = 180000;

    //-------------------------------------------------------------------------
    private static final long RT2_GCTEST_FREQUENCY = 30000;

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv ()
        throws Exception
    {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGCWithScenarioBrokenConnectionForDocument()
        throws Exception
    {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig   aEnvNode      = TestRunConfigHelper.defineEnv4Node01 ();
        final RT2JmxClient    aRT2JmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);
        final Mutable<String> aDocUID       = new MutableObject<>();

        aRT2JmxClient.connect();

        createAndOpenDocumentAndShutdownConnectionWithoutCorrectCloseAndLeaveSequence(aDocUID);

        Assert.assertNotNull(aDocUID.getValue());

        // wait until the garbage collector freed all resources
        Thread.sleep(RT2_GCTEST_TIMEOUT + RT2_GCTEST_FREQUENCY + 3000);

        // now check the accessible internal states if garbage collecting was successful
        final Set<String> aSetWithDocUIDs = Collections.singleton(aDocUID.getValue());
        final Set<String> aHaveAtomicLongs = aRT2JmxClient.findHazelcastAtomicLongValues(aSetWithDocUIDs);
        Assert.assertTrue(aHaveAtomicLongs.size() == 0);

        // doc processor and proxy should be zero again
        final Integer countDocProcessors = aRT2JmxClient.getCountDocProcessors();
        final Integer countDocProxies = aRT2JmxClient.getCountDocProxies();
        Assert.assertEquals(0, (countDocProcessors != null) ? ((int)countDocProcessors) : -1);
        Assert.assertEquals(0, (countDocProxies != null) ? ((int)countDocProxies) : -1);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testGCWithScenarioDocumentHandlesSessionInvalidWithPassiveClient()
        throws Exception
    {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode01  = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        final SessionJmxClient aSessionJmxClient = new SessionJmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode01.sServerName, aEnvNode01.nServerJMXPort);
        aSessionJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aSessionJmxClient.isConnected());

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();

            final TextDoc aDocUser01 = (TextDoc)TestSetupHelper.newDocInst (aAppsuite01, Doc.DOCTYPE_TEXT, false);
            aDocUser01.createNew();

            aDocUser01.open();

            final String aDocUID = aDocUser01.getDocUID();
            final int[] aShortTermSesions = aSessionJmxClient.getSessionAttributeShortTerm();
            final boolean bNonZeroEntry   = JMXHelper.containsPositiveNonZeroEntry(aShortTermSesions);
            Assert.assertTrue("There must be an entry in the short-term session store", bNonZeroEntry);

            int nCountApplyOps = 0;

            aDocUser01.applyOps(); ++nCountApplyOps;
            aDocUser01.applyOps(); ++nCountApplyOps;

            // register ourself for broadcast messages
            final AtomicBoolean              aException      = new AtomicBoolean(false);
            final AtomicReference<ErrorCode> aErrorCodeRef   = new AtomicReference<>(ErrorCode.NO_ERROR);
            final CountDownLatch             aCountdownLatch = new CountDownLatch(1);
            aDocUser01.addErrorObserver(e -> {
                try
                {
                    final ErrorCode aErrorCode = RT2MessageGetSet.getError(e);
                    aErrorCodeRef.set(aErrorCode);
                    aCountdownLatch.countDown();
                }
                catch (final Exception ex)
                {
                    aException.set(true);
                    aCountdownLatch.countDown();
                }
            });

            // number of doc processor, proxy & client info should be one
            Integer countDocProcessors = aSessionJmxClient.getCountDocProcessors();
            Integer countDocProxies = aSessionJmxClient.getCountDocProxies();
            Assert.assertEquals(1, (countDocProcessors != null) ? ((int)countDocProcessors) : -1);
            Assert.assertEquals(1, (countDocProxies != null) ? ((int)countDocProxies) : -1);

            // remove user session
            final String sessionId = aDocUser01.getContext().accessSession().getSessionId();
            aSessionJmxClient.removeClientSession(sessionId);

            // force client to send a request to the backend which must fail now
            aDocUser01.applyOpsAsync();

            boolean bResponse = aCountdownLatch.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Error not received in time", bResponse);
            Assert.assertFalse("Error handler should not throw exception", aException.get());

            final ErrorCode aErrorReceived = aErrorCodeRef.get();
            Assert.assertTrue("Error received must be a session invalid error!", (aErrorReceived.isError() && (aErrorReceived.getCode() == ErrorCode.CODE_GENERAL_SESSION_INVALID_ERROR)));

            // wait until the garbage collector freed all resources
            Thread.sleep(RT2_GCTEST_TIMEOUT + RT2_GCTEST_FREQUENCY + 3000);

            // now check the accessible internal states if garbage collecting was successful
            final Set<String> aSetWithDocUIDs = Collections.singleton(aDocUID);
            final Set<String> aHaveAtomicLongs = aSessionJmxClient.findHazelcastAtomicLongValues(aSetWithDocUIDs);
            Assert.assertTrue(aHaveAtomicLongs.size() == 0);

            // number of doc processor, proxy & client info should be zero again
            countDocProcessors = aSessionJmxClient.getCountDocProcessors();
            countDocProxies = aSessionJmxClient.getCountDocProxies();
            Assert.assertEquals(0, (countDocProcessors != null) ? ((int)countDocProcessors) : -1);
            Assert.assertEquals(0, (countDocProxies != null) ? ((int)countDocProxies) : -1);

            aDocUser01.close ();

            Assert.assertTrue("The test should be able to apply operations before the invalid session notification happened", (nCountApplyOps > 0));
            Assert.assertTrue (serverState.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDocUser01)));
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

        // Attention: Don't call close TestSetupHelper.closeAppsuiteConnections(aAppsuite01)
        // as we already lost our session. It will fail with http error 403.
        aAppsuite01.shutdownWSConnection();
    }

    //-------------------------------------------------------------------------
    private void createAndOpenDocumentAndShutdownConnectionWithoutCorrectCloseAndLeaveSequence(Mutable<String> aDocUID) throws Exception {
        final TestRunConfig aEnvNode      = TestRunConfigHelper.defineEnv4Node01 ();
        final OXAppsuite    aAppsuite     = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final Doc           aDoc          = TestSetupHelper.newDocInst (aAppsuite, Doc.DOCTYPE_TEXT);
        final RT2JmxClient  aRT2JmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);

        aRT2JmxClient.connect();

        Integer countDocProcessors = aRT2JmxClient.getCountDocProcessors();
        Integer countDocProxies = aRT2JmxClient.getCountDocProxies();
        Assert.assertEquals("Pre-requiremnt violation: Make sure that no other active doc instance is running!", 0, (countDocProcessors != null) ? ((int)countDocProcessors) : -1);
        Assert.assertEquals("Pre-requiremnt violation: Make sure that no other active doc instance is running!", 0, (countDocProxies != null) ? ((int)countDocProxies) : -1);

        final Long gcTimeout = aRT2JmxClient.getGCTimeoutAttributeFromServer();
        if (null != gcTimeout) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("Garbage collector timeout " + gcTimeout + "ms");
        } else {
            Assert.fail("Couldn't find garbage collector timeout value via JMX");
        }

        final Long gcFrequency = aRT2JmxClient.getGCFrequencyAttributeFromServer();
        if (null != gcFrequency) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("Garbage collector frequency " + gcFrequency + "ms");
        } else {
            Assert.fail("Couldn't find garbage collector frequency value via JMX");
        }

        aRT2JmxClient.setGCTiming(RT2_GCTEST_TIMEOUT, RT2_GCTEST_FREQUENCY);

        Long newGCTimeout = aRT2JmxClient.getGCTimeoutAttributeFromServer();
        if (null != newGCTimeout) {
            Assert.assertTrue(newGCTimeout == RT2_GCTEST_TIMEOUT);
            LOG.forLevel(ELogLevel.E_INFO).withMessage("Garbage collector timeout " + gcTimeout + "ms");
        } else {
            Assert.fail("Couldn't find new garbage collector timeout value via JMX");
        }

        final Long newGCFrequency = aRT2JmxClient.getGCFrequencyAttributeFromServer();
        if (null != newGCFrequency) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("Garbage collector frequency " + gcFrequency + "ms");
        } else {
            Assert.fail("Couldn't find new garbage collector frequency value via JMX");
        }

        Assert.assertEquals(RT2_GCTEST_TIMEOUT, (long)newGCTimeout);
        Assert.assertEquals(RT2_GCTEST_FREQUENCY, (long)newGCFrequency);

        aDoc.createNew();
        aDoc.open     ();

        final String docUID = aDoc.getDocUID();

        // check that atomic value in hazelcast exists and has the correct value
        final Long refCount = aRT2JmxClient.getHazelcastAtomicLongValue(docUID);
        Assert.assertTrue((refCount != null) && (refCount == 1));

        aDoc.applyOps ();

        // simulate that the client closes the connection without any previous
        // close / leave messages
        aAppsuite.accessWSClient().shutdown();

        aDocUID.setValue(aDoc.getDocUID());
    }

}
