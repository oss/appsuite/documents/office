/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.DocEvent;
import com.openexchange.office.rt2.perftest.doc.DocEventObserver;
import com.openexchange.office.rt2.perftest.doc.DocUser;
import com.openexchange.office.rt2.perftest.doc.DocUserRegistry;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.doc.TestDocProvider;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.MessageConstants;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2RestAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.ServerResponseException;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OperationConstants;

import test.com.openexchange.office.test.rt2.utils.DocHelper;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

//=============================================================================
public class RT2CollabStandardMultiNodeTest extends RT2TestBase {

	// -------------------------------------------------------------------------
	// private static final Logger LOG =
	// Slf4JLogger.create(RT2CollabStandardMultiNodeTest.class);

	// -------------------------------------------------------------------------
	final String OP_INSERTTEXT_PROP_NAMES[] = { OperationConstants.OPERATION_NAME, OperationConstants.OPERATION_TEXT };
	final String OP_INSERTTEXT_VALUES[] = { TextDoc.OPERATION_INSERTTEXT, TextDoc.STR_TEXT };

	// -------------------------------------------------------------------------
	@BeforeClass
	public static void setUpEnv() throws Exception {
		StatusLine.setEnabled(false);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testTwoDocuments() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();

		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		final Doc aDoc01 = TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		final Doc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT);

		aDoc01.createNew();
		aDoc02.createNew();

		aDoc01.open();
		aDoc02.open();

		aDoc01.applyOps();
		aDoc02.applyOps();

		aDoc01.applyOps();
		aDoc02.applyOps();

		aDoc01.close();
		aDoc02.close();

		aDoc01.removeDocFile();
		aDoc02.removeDocFile();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testJoinSameDocumentOnDifferentBackendNodes() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();

			// the test now has to respect that only ONE client can have edit rights
			final TextDoc aDoc01 = TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
			aDoc01.createNew();
	
			// aDoc2 is read-only and cannot send applyOps - the office backend drops
			// applyOps from viewer
			final EditableDoc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
					aDoc01.getFileId(), aDoc01.getDriveDocId(), true);
	
			int nCountApplyOps = 0;
	
			aDoc01.open();
			aDoc02.open();
	
			aDoc01.applyOps();
			++nCountApplyOps;
			aDoc01.applyOps();
			++nCountApplyOps;
	
			aDoc01.close();
			aDoc02.close();
	
			Assert.assertTrue(serverState.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDoc01, aDoc02)));
	
			// verify the document content that must include the text one time
			boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDoc01, TextDoc.STR_TEXT,
					nCountApplyOps);
			Assert.assertTrue(
					"Html string should contain the inserted text for this document. Document was not correctly stored!",
					bVerified);
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testPresenter() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		final StringBuilder DBG = new StringBuilder(256);

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();
			final PresenterDoc aDoc01 = TestSetupHelper.newDocInstForSpecialTestFile(aAppsuite01, Doc.DOCTYPE_PRESENTER,
					TestDocProvider.TESTFILE_CALCENGINE_AS_A_SERVICE_PDF);
			final PresenterDoc aDoc02 = TestSetupHelper.newDocInstForSpecialTestFile(aAppsuite02, Doc.DOCTYPE_PRESENTER,
					TestDocProvider.TESTFILE_CALCENGINE_AS_A_SERVICE_PDF);
	
			aDoc01.open();
			aDoc02.open();
	
			DBG.append("doc 01 UID          : " + aDoc01.getDocUID() + "\n");
			DBG.append("doc 02 UID          : " + aDoc02.getDocUID() + "\n");
			DBG.append("doc 01 RT-ID        : " + aDoc01.getRT2ClientUID() + "\n");
			DBG.append("doc 02 RT-ID        : " + aDoc02.getRT2ClientUID() + "\n");
	
			aDoc01.startRemotePresentation();
			aDoc02.joinPresentation();
	
			final int nMaxSlidesOfTestDoc = 8;
			for (int nSlide = 1; nSlide <= nMaxSlidesOfTestDoc; ++nSlide) {
				aDoc01.changeSlide(nSlide);
				Thread.sleep(250);
			}
	
			DBG.append("doc 01 active slide : " + aDoc01.getActiveSlide() + "\n");
			DBG.append("doc 02 active slide : " + aDoc02.getActiveSlide() + "\n");
	
			System.err.println(DBG.toString());
	
			final List<JSONObject> lUpdateSlideOps = aDoc02.getRecordedOperations();
			for (final JSONObject aUpdateSlideOp : lUpdateSlideOps) {
				System.out.println(aUpdateSlideOp.toString(4));
			}
	
			final List<DocUser> lUsers = aDoc02.accessUserRegistry().asList();
			for (final DocUser aUser : lUsers) {
				System.out.println(aUser);
			}
	
			aDoc02.leavePresentation();
			aDoc01.endRemotePresentation();
	
			aDoc01.close();
			aDoc02.close();
	
			Assert.assertTrue(serverState.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDoc01, aDoc02)));
			Assert.assertEquals("testPresenter[01] active slides are different on both sides", aDoc01.getActiveSlide(),
					aDoc02.getActiveSlide());
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testCollaborationAndDocumentStatusUpdates() throws Exception {
		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		// the test now has to respect that only ONE client can have edit rights
		final TextDoc aDoc01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDoc01.enableDocEvents(true);
		aDoc01.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				System.err.println("doc 01 doc event : " + aEvent + "\n");
			}
		});

		aDoc01.createNew();

		// aDoc2 is read-only and cannot send applyOps - the office backend drops
		// applyOps from viewer
		final Doc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
				aDoc01.getFileId(), aDoc01.getDriveDocId(), true);
		aDoc02.enableDocEvents(true);
		aDoc02.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				System.err.println("doc 02 doc event : " + aEvent + "\n");
			}
		});

		int nCountApplyOps = 0;

		aDoc01.open();
		aDoc02.open();

		final String sEditorUIDDoc01 = aDoc01.getEditorUID();
		final String sEditorUIDDoc02 = aDoc02.getEditorUID();
		Assert.assertTrue("The editor UID for both document instances in collaboration must be the SAME!",
				((sEditorUIDDoc01 != null) && (sEditorUIDDoc01.equals(sEditorUIDDoc02))));

		aDoc01.applyOps();
		++nCountApplyOps;
		aDoc01.applyOps();
		++nCountApplyOps;

		// TODO: this sleep smells and should check for clients update message!
		Thread.sleep(2000);

		final int nDoc01ServerOSN = aDoc01.getServerOSN();
		final int nDoc02ServerOSN = aDoc02.getServerOSN();
		Assert.assertTrue("The server OSN for both document instances in collaboration must be the SAME!",
				(nDoc01ServerOSN == nDoc02ServerOSN));

		aDoc01.close();
		aDoc02.close();

		// verify the document content that must include the text one time
		boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDoc01, TextDoc.STR_TEXT,
				nCountApplyOps);
		Assert.assertTrue(
				"Html string should contain the inserted text for this document. Document was not correctly stored!",
				bVerified);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testCollaborationAndClientStatusUpdates() throws Exception {
		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);
		final OXAppsuite aAppsuite03 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		// the test now has to respect that only ONE client can have edit rights
		final Doc aDoc01 = TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDoc01.enableDocEvents(true);
		aDoc01.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				System.err.println("doc 01 doc event : " + aEvent + "\n");
			}
		});

		aDoc01.createNew();

		final Doc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
				aDoc01.getFileId(), aDoc01.getDriveDocId(), true);
		aDoc02.enableDocEvents(true);
		aDoc02.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				System.err.println("doc 02 doc event : " + aEvent + "\n");
			}
		});

		final Doc aDoc03 = TestSetupHelper.newDocInst(aAppsuite03, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
				aDoc01.getFileId(), aDoc01.getDriveDocId(), true);
		aDoc03.enableDocEvents(true);
		aDoc03.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				System.err.println("doc 03 doc event : " + aEvent + "\n");
			}
		});

		aDoc01.open();
		aDoc02.open();
		aDoc03.open();

		// we need to wait a little bit to ensure that the clients updates were
		// receivd and processed by the clients
		Thread.sleep(1000);

		final DocUserRegistry aDoc01UserReg = aDoc01.accessUserRegistry();
		final DocUserRegistry aDoc02UserReg = aDoc02.accessUserRegistry();
		final DocUserRegistry aDoc03UserReg = aDoc03.accessUserRegistry();

		List<DocUser> aListDoc01 = aDoc01UserReg.asList();
		List<DocUser> aListDoc02 = aDoc02UserReg.asList();
		List<DocUser> aListDoc03 = aDoc03UserReg.asList();
		Assert.assertTrue("All document user registries must have the same number of clients!",
				((aListDoc01.size() == 3) && (aListDoc01.size() == aListDoc02.size())
						&& (aListDoc02.size() == aListDoc03.size())));

		aDoc01.close();

		// we need to wait a little bit to ensure that the clients updates were
		// receivd and processed by the clients
		Thread.sleep(1000);

		aListDoc02 = aDoc02UserReg.asList();
		aListDoc03 = aDoc03UserReg.asList();
		Assert.assertTrue("Document user registries for doc 2 & 3 must have the same number of clients!",
				((aListDoc02.size() == 2) && (aListDoc02.size() == aListDoc03.size())));

		aDoc02.close();

		// we need to wait a little bit to ensure that the clients updates were
		// receivd and processed by the clients
		Thread.sleep(1000);

		aListDoc03 = aDoc03UserReg.asList();
		Assert.assertTrue("Document user registries for doc 3 must have only one client!", ((aListDoc03.size() == 1)));

		aDoc03.close();

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite03);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testClientsOpenCloseDocConcurrently() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

        ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();
			// the test now has to respect that only ONE client can have edit rights
			final TextDoc aDoc01 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
			aDoc01.createNew();
	
			// aDoc2 is read-only and cannot send applyOps - the office backend drops
			// applyOps from viewer
			final Doc aDoc02 = TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT, aDoc01.getFolderId(),
					aDoc01.getFileId(), aDoc01.getDriveDocId(), true);
	
			int nCountApplyOps = 0;
	
			// prepare the document - open, change and close it so it won't be removed after
			// close
			aDoc01.open();
			aDoc01.applyOps();
			++nCountApplyOps;
			aDoc01.close();
	
			final AtomicReference<String> aThread1AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread2AssertMsg = new AtomicReference<>(null);
	
			// start two clients on different threads connected to different backend-nodes
			// which
			// open/close the same document in high frequency - it should trigger situations
			// where
			// one client wants to destroy the doc resources, while another one wants to
			// create it.
			final Thread t1 = new Thread() {
				@Override
				public void run() {
					try {
						for (int i = 0; i < 10; i++) {
							aDoc01.open();
							Thread.sleep(1000);
							aDoc01.close();
							Thread.sleep(1000);
						}
					} catch (Exception e) {
						aThread1AssertMsg.set(e.getMessage());
						e.printStackTrace();
					}
				}
			};
	
			final Thread t2 = new Thread() {
				@Override
				public void run() {
					try {
						for (int i = 0; i < 10; i++) {
							Thread.sleep(1000);
							aDoc02.open();
							Thread.sleep(1000);
							aDoc02.close();
						}
					} catch (Exception e) {
						aThread2AssertMsg.set(e.getMessage());
						e.printStackTrace();
					}
				}
			};
	
			t1.start();
			t2.start();
	
			t1.join(0);
			t2.join(0);
	
			if (aThread1AssertMsg.get() != null)
				Assert.fail(aThread1AssertMsg.get());
	
			if (aThread2AssertMsg.get() != null)
				Assert.fail(aThread2AssertMsg.get());
	
			Assert.assertTrue(serverState.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(aDoc01, aDoc02)));
	
			// verify the document content that must include the text two times
			boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDoc01, TextDoc.STR_TEXT,
					nCountApplyOps);
			Assert.assertTrue(
					"Html string should contain the inserted text for this document. Document was not correctly stored!",
					bVerified);
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	@Ignore
	public void testSwitchEditRightsDifferentBackends() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		setNackFrequenceOfServer(aEnvNode01, aEnvNode02);
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		aDocUser1.setAcceptsEditRightsRequest(true);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final EditableDoc aDocUser2 = (EditableDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		final CountDownLatch aUser2HasEditMode = new CountDownLatch(1);
		aDocUser2.enableDocEvents(true);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser2.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser2UUID.equals(sNewEditorUID))
						aUser2HasEditMode.countDown();
				}
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// try to acquire edit rights from editor client
		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// user 2 waits for the approval of user 1
		final boolean bHasEditoMode = aUser2HasEditMode.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
		Assert.assertTrue("Edit mode didn't change in an expected time frame", bHasEditoMode);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text two times
		boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser1, TextDoc.STR_TEXT,
				nCountApplyOps);
		Assert.assertTrue(
				"Html string should contain the inserted text for this document. Document was not correctly stored!",
				bVerified);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
    @Ignore
	@Test
	public void testSwitchEditRightsSeveralTimesWithEditingOnDifferentBackends() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode02 = TestRunConfigHelper.defineEnv4Node02();
		final OXAppsuite aAppsuite01 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite aAppsuite02 = TestSetupHelper.openAppsuiteConnection(aEnvNode02);

		// aDocUser1 is the editor of the document
		final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite01, Doc.DOCTYPE_TEXT);
		aDocUser1.createNew();
		aDocUser1.setHandleEditRightsRequests(true);

		// aDocUser2 is a viewer collaborate with aDocUser1
		final TextDoc aDocUser2 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite02, Doc.DOCTYPE_TEXT,
				aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
		aDocUser2.enableDocEvents(true);
		aDocUser2.setHandleEditRightsRequests(true);

		final AtomicInteger aUser1HasEditRights = new AtomicInteger(0);
		final AtomicInteger aUser2HasEditRights = new AtomicInteger(0);

		final String sUser1UUID = aDocUser1.getRT2ClientUID();
		final String sUser2UUID = aDocUser2.getRT2ClientUID();

		aDocUser1.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser1UUID.equals(sNewEditorUID)) {
						aUser1HasEditRights.incrementAndGet();
						synchronized (aUser1HasEditRights) {
							aUser1HasEditRights.notifyAll();
						}
					}
				}
			}
		});

		aDocUser2.addObserver(new DocEventObserver() {

			@Override
			public void notify(final DocEvent aEvent) throws Exception {
				if (aEvent.isEvent(DocEvent.EVENT_EDITOR_CHANGED)) {
					final String sNewEditorUID = aEvent.getData();
					if (sUser2UUID.equals(sNewEditorUID)) {
						aUser2HasEditRights.incrementAndGet();
						synchronized (aUser2HasEditRights) {
							aUser2HasEditRights.notifyAll();
						}
					}
				}
			}
		});

		int nCountApplyOps = 0;

		aDocUser1.open();
		aDocUser2.open();

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		// wait until we know that user1 has the edit rights
		boolean bEditRightsReceived = waitForEditRightsIndicator(aUser1HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		// The test switches the edit rights three times user1 -> user2 -> user1 ->
		// user2

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 2 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser2HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 2 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser2.applyOps();
		++nCountApplyOps;
		aDocUser2.applyOps();
		++nCountApplyOps;

		// user 1 now tries to receive the edit rights from user 2
		aDocUser1.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 1 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser1HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 1 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser1UUID, aDocUser1.getEditorUID());

		aDocUser1.applyOps();
		++nCountApplyOps;
		aDocUser1.applyOps();
		++nCountApplyOps;

		aDocUser2.editRights(MessageConstants.ACTION_EDITRIGHTS_REQUESTS, null);

		// wait until user 2 receives edit rights
		bEditRightsReceived = waitForEditRightsIndicator(aUser2HasEditRights, RT2TestConstants.DEFAULT_TIMEOUT);

		Assert.assertTrue("Edit rights state not received by user 2 in an expected time frame", bEditRightsReceived);
		Assert.assertEquals(sUser2UUID, aDocUser2.getEditorUID());

		aDocUser2.applyOps();
		++nCountApplyOps;
		aDocUser2.applyOps();
		++nCountApplyOps;

		aDocUser1.close();
		aDocUser2.close();

		// verify the document content that must include the text from both editors
		boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser1, TextDoc.STR_TEXT,
				nCountApplyOps);
		Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);

		TestSetupHelper.closeAppsuiteConnections(aAppsuite01);
		TestSetupHelper.closeAppsuiteConnections(aAppsuite02);
	}

	// -------------------------------------------------------------------------
	@Test
	public void testEmergencyLeaveViaRESTWithTwoClientsDifferentNodes() throws Exception {
		final TestRunConfig aEnvNode1 = TestRunConfigHelper.defineEnv4Node01();
		final TestRunConfig aEnvNode2 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite aAppsuite1 = TestSetupHelper.openAppsuiteConnection(aEnvNode1);
		final OXAppsuite aAppsuite2 = TestSetupHelper.openAppsuiteConnection(aEnvNode2);

        ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();
			final TextDoc aDocUser1 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite1, Doc.DOCTYPE_TEXT);
			aDocUser1.createNew();
	
			final TextDoc aDocUser2 = (TextDoc) TestSetupHelper.newDocInst(aAppsuite2, Doc.DOCTYPE_TEXT,
					aDocUser1.getFolderId(), aDocUser1.getFileId(), aDocUser1.getDriveDocId(), true);
	
			int nCountApplyOps = 0;
	
			aDocUser1.open();
			aDocUser2.open();
	
			// we need to wait a little bit to ensure that the clients updates were
			// received and processed by the clients
			Thread.sleep(1000);
	
			final DocUserRegistry aDoc01UserReg = aDocUser1.accessUserRegistry();
			final DocUserRegistry aDoc02UserReg = aDocUser2.accessUserRegistry();
	
			List<DocUser> aListDoc1 = aDoc01UserReg.asList();
			List<DocUser> aListDoc2 = aDoc02UserReg.asList();
			Assert.assertTrue("All document user registries must have the same number of clients!",
					((aListDoc1.size() == 2) && (aListDoc1.size() == aListDoc2.size())));
	
			aDocUser1.applyOps();
			++nCountApplyOps;
			aDocUser1.applyOps();
			++nCountApplyOps;
			aDocUser1.applyOps();
			++nCountApplyOps;
			aDocUser1.applyOps();
			++nCountApplyOps;
	
			try {
				final OXRT2RestAPI aRT2RESTAPI = new OXRT2RestAPI();
	
				aRT2RESTAPI.bind(aAppsuite1);
	
				// emergency leave is a fire & forget rest call, therefore don't check server
				// result
				aRT2RESTAPI.emergencyLeave(aDocUser1.getRT2ClientUID(), aDocUser1.getDocUID(), new JSONArray(), false);
			} catch (final ServerResponseException e) {
				Assert.fail("Send emergencyleave message failed: " + e.getMessage());
			}
	
			aDocUser1.setClosedState();
			// we have to reset the new document data to ensure that
			// the next open won't be a fast-empty open (done by default
			// with close() not used in this test).
			aDocUser1.clearNewDocumentData();
	
			// We need a sleep here as we did a fire&forget emergency leave and therefore
			// have no information about the server state.
			Thread.sleep(3000);
	
			TestSetupHelper.closeAppsuiteConnections(aAppsuite1);
	
			// retrieve user list again and see that the first user really left the document
			aListDoc2 = aDoc02UserReg.asList();
			Assert.assertTrue(
					"Document user registry of user 2 must only have ONE user! Current size = " + aListDoc2.size(),
					(aListDoc2.size() == 1));
	
			aDocUser2.close();
	
			Assert.assertTrue(serverState.checkServerState(aEnvNode1, DocHelper.createClosedDocUIDs(aDocUser1)));
	
			boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aDocUser2, TextDoc.STR_TEXT, nCountApplyOps);
			Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!",	bVerified);
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

		TestSetupHelper.closeAppsuiteConnections(aAppsuite2);
	}

}
