/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.concurrent.atomic.AtomicReference;

import org.junit.Assert;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;

import test.com.openexchange.office.test.rt2.utils.ClientDocThread;
import test.com.openexchange.office.test.rt2.utils.DocHelper;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;
import test.com.openexchange.office.test.rt2.utils.TextDocumentContentVerifier;

public class RT2LockingTests {

	public class OpenCloseClientDocThread extends ClientDocThread {
		public OpenCloseClientDocThread(Doc doc, AtomicReference<String> assertMsg) {
			super(doc, assertMsg);
		}

		@Override
		public void run() {
			try {
				for (int i = 0; i < 10; i++) {
					doc.open();
					Thread.sleep(1000);
					doc.close();
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				threadAssertMsg.set(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testConcurrentOpenCloseWithServeralClients() throws Exception {
		RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

		final TestRunConfig aEnvNode01 = TestRunConfigHelper.defineEnv4Node01();
		final OXAppsuite client11 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite client12 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite client21 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite client22 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite client31 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);
		final OXAppsuite client32 = TestSetupHelper.openAppsuiteConnection(aEnvNode01);

        ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
        	serverStateStarted = serverState.start();
			// let's create the document file with the first client
			final TextDoc aClient11Doc = (TextDoc) TestSetupHelper.newDocInst(client11, Doc.DOCTYPE_TEXT);
			aClient11Doc.createNew();

			// the other clients join the document using the file-id from the first client
			final Doc aClient12Doc = TestSetupHelper.newDocInst(client12, Doc.DOCTYPE_TEXT, aClient11Doc.getFolderId(),
					aClient11Doc.getFileId(), aClient11Doc.getDriveDocId(), true);
			final Doc aClient21Doc = TestSetupHelper.newDocInst(client21, Doc.DOCTYPE_TEXT, aClient11Doc.getFolderId(),
					aClient11Doc.getFileId(), aClient11Doc.getDriveDocId(), true);
			final Doc aClient22Doc = TestSetupHelper.newDocInst(client22, Doc.DOCTYPE_TEXT, aClient11Doc.getFolderId(),
					aClient11Doc.getFileId(), aClient11Doc.getDriveDocId(), true);
			final Doc aClient31Doc = TestSetupHelper.newDocInst(client31, Doc.DOCTYPE_TEXT, aClient11Doc.getFolderId(),
					aClient11Doc.getFileId(), aClient11Doc.getDriveDocId(), true);
			final Doc aClient32Doc = TestSetupHelper.newDocInst(client32, Doc.DOCTYPE_TEXT, aClient11Doc.getFolderId(),
					aClient11Doc.getFileId(), aClient11Doc.getDriveDocId(), true);

			int nCountApplyOps = 0;

			// prepare the document - open, change and close it so it won't be removed after close
			aClient11Doc.open();
			aClient11Doc.applyOps();
			++nCountApplyOps;
			aClient11Doc.close();

			final AtomicReference<String> aThread11AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread12AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread21AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread22AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread31AssertMsg = new AtomicReference<>(null);
			final AtomicReference<String> aThread32AssertMsg = new AtomicReference<>(null);

			// start six clients on different threads connected to the same middleware node
			// which open/close the same document in high frequency - it should trigger situations
			// where more than one client wants to open or close the same document. This checks
			// that creation and destruction of the DocProcessor instance and all dependent
			// resources are done atomically.
			final Thread t11 = new OpenCloseClientDocThread(aClient11Doc, aThread11AssertMsg);
			final Thread t12 = new OpenCloseClientDocThread(aClient12Doc, aThread12AssertMsg);
			final Thread t21 = new OpenCloseClientDocThread(aClient21Doc, aThread21AssertMsg);
			final Thread t22 = new OpenCloseClientDocThread(aClient22Doc, aThread22AssertMsg);
			final Thread t31 = new OpenCloseClientDocThread(aClient31Doc, aThread31AssertMsg);
			final Thread t32 = new OpenCloseClientDocThread(aClient32Doc, aThread32AssertMsg);

			t11.start();
			t12.start();
			t21.start();
			t22.start();
			t31.start();
			t32.start();

			t11.join(10000);
			t12.join(10000);
			t21.join(10000);
			t22.join(10000);
			t31.join(10000);
			t32.join(10000);

			if (aThread11AssertMsg.get() != null) Assert.fail(aThread11AssertMsg.get());
			if (aThread12AssertMsg.get() != null) Assert.fail(aThread12AssertMsg.get());
			if (aThread21AssertMsg.get() != null) Assert.fail(aThread21AssertMsg.get());
			if (aThread22AssertMsg.get() != null) Assert.fail(aThread22AssertMsg.get());
			if (aThread31AssertMsg.get() != null) Assert.fail(aThread31AssertMsg.get());
			if (aThread32AssertMsg.get() != null) Assert.fail(aThread32AssertMsg.get());

			Assert.assertTrue(serverState.checkServerState(aEnvNode01, DocHelper.createClosedDocUIDs(
					aClient11Doc, aClient12Doc, aClient21Doc, aClient22Doc, aClient31Doc, aClient32Doc)));

			// verify the document content that must include the text one time
			boolean bVerified = TextDocumentContentVerifier.checkTextDocumentContent(aClient11Doc, TextDoc.STR_TEXT, nCountApplyOps);
			Assert.assertTrue("Html string should contain the inserted text for this document. Document was not correctly stored!", bVerified);
        } finally {
        	if (serverStateStarted)
        		serverState.stop();
        }

		TestSetupHelper.closeAppsuiteConnections(client11);
		TestSetupHelper.closeAppsuiteConnections(client12);
		TestSetupHelper.closeAppsuiteConnections(client21);
		TestSetupHelper.closeAppsuiteConnections(client22);
		TestSetupHelper.closeAppsuiteConnections(client31);
		TestSetupHelper.closeAppsuiteConnections(client32);
	}

}
