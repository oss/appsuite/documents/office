/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.NoSuchElementException;

//=============================================================================
public class SessionJmxClient extends RT2JmxClient {

    //-------------------------------------------------------------------------
    private static final String OBJNAME_SESSIOND_ON_NODE = "com.openexchange.sessiond:name=SessionD Toolkit";
    private static final String ATTR_NUMBER_OF_LONGTERM_SESSIONS = "NumberOfLongTermSessions";
    private static final String ATTR_NUMBER_OF_SHORTTERM_SESSIONS = "NumberOfShortTermSessions";
    private static final String OP_CLEARUSERSESSIONSGLOBALLY = "clearUserSessionsGlobally";
    private static final String OP_REMOVECLIENTSESSION = "removeClientSession";

    //-------------------------------------------------------------------------
    public SessionJmxClient(String sClusterName, String sServerName, int nPort) {
        super(sClusterName, sServerName, nPort);
    }

    //-------------------------------------------------------------------------
    public int[] getSessionAttributeLongTerm() throws Exception {
        return getSessionAttribute(ATTR_NUMBER_OF_LONGTERM_SESSIONS);
    }

    //-------------------------------------------------------------------------
    public int[] getSessionAttributeShortTerm() throws Exception {
        return getSessionAttribute(ATTR_NUMBER_OF_SHORTTERM_SESSIONS);
    }

    //-------------------------------------------------------------------------
    public void clearUserSessionsGlobally(Integer nUserId, Integer nContextId) throws Exception {
        invokeOperation(OBJNAME_SESSIOND_ON_NODE, OP_CLEARUSERSESSIONSGLOBALLY, 
           new Integer[] { nUserId, nContextId },
           new String[] { int.class.getName(), int.class.getName()});
    }

    //-------------------------------------------------------------------------
    public boolean removeClientSession(String sessionId) throws Exception {
        final Object result = invokeOperation(RT2JmxClient.OBJNAME_BACKEND_MONITORING, OP_REMOVECLIENTSESSION, 
            new String[] { sessionId },
            new String[] { String.class.getName() });
        return (Boolean)result;
    }

    //-------------------------------------------------------------------------
    private int[] getSessionAttribute(String sAttributeName) throws Exception {
        final Object aAttribute = this.getAttribute(OBJNAME_SESSIOND_ON_NODE, sAttributeName);
        if (JMXHelper.isNotNullAndAssignable(aAttribute, int[].class)) {
            final int[] aArray = (int[]) aAttribute;
            return aArray;
        }

        throw new NoSuchElementException();
    }
}
