/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.Set;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;

import test.com.openexchange.office.test.rt2.utils.DocHelper;
import test.com.openexchange.office.test.rt2.utils.RT2JmxClient;
import test.com.openexchange.office.test.rt2.utils.ServerStateHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2DocumentTests extends RT2TestBase {

    // -------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv() throws Exception {
        StatusLine.setEnabled(false);
    }

    // -------------------------------------------------------------------------
    @Test
    public void testTwoDocumentsChangedAndLogout() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final RT2JmxClient aJmxClient = new RT2JmxClient(TestConfig.HZ_CLUSTER_NAME, aEnvNode.sServerName, aEnvNode.nServerJMXPort);

        aJmxClient.connect();
        Assert.assertTrue("JMX client should be able to connect to the backend node", aJmxClient.isConnected());

        final ServerStateHelper serverState = new ServerStateHelper();
        boolean serverStateStarted = false;
        try {
            serverStateStarted = serverState.start();
            setNackFrequenceOfServer(aEnvNode);
            final Doc textDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);
            final Doc calcDoc = TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_SPREADSHEET);

            textDoc.createNew();
            textDoc.open();

            calcDoc.createNew();
            calcDoc.open();

            // just logout - don't close/leave the document via requests
            TestSetupHelper.closeAppsuiteConnections(aAppsuite);

            // -------------------------------------------------------------------------
            // Verify clean-up of server state after document has been closed
            Set<String> setWithDocUIDs = DocHelper.createClosedDocUIDs(textDoc, calcDoc);
            Assert.assertTrue(serverState.checkServerState(aEnvNode, setWithDocUIDs));
        } finally {
            if (serverStateStarted) {
                serverState.stopAndShutdown();
            }
        }

        aAppsuite.shutdownWSConnection();
    }

}
