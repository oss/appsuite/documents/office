/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import org.json.JSONArray;
import org.junit.Assert;

//=============================================================================
public class GlobalTemplateListVerifier
{
    //-------------------------------------------------------------------------
	private GlobalTemplateListVerifier() {}

    //-------------------------------------------------------------------------
	public static boolean verifyTemplateList(final JSONArray aGlobalTemplateList)
	{
		boolean bResult = false;
	
		if ((null != aGlobalTemplateList) && (aGlobalTemplateList.length() > 0))
		{
			int i = 0;
			boolean bValid = true;
	
			while (bValid && (i < aGlobalTemplateList.length()))
				bValid = checkEntry(aGlobalTemplateList.get(i++));
			bResult = bValid;
		} else if (aGlobalTemplateList.length() == 0) {
			bResult = true;
		}

		return bResult;
	}

    //-------------------------------------------------------------------------
    public static boolean checkEntry(final Object o)
    {
         if (o == null)
         {
             Assert.fail("Global template list entry must not be null!");
             return false;
         }
         else
             return (isString(o) && ((Long.parseLong((String)o)) >= 0));
    }

    //-------------------------------------------------------------------------
    private static boolean isString(Object o)
    {
    	return (o instanceof String);
    }
}
