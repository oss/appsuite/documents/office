package test.com.openexchange.office.test.rt2;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2MultiSheetLoadTests {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv() throws Exception {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testMultisheet() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);

        //-------------------------------------------------------------------------
        // Test Spreadsheet document with more sheets
        final SpreadsheetDoc aMultisheetCalcDoc = (SpreadsheetDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_SPREADSHEET);

        aMultisheetCalcDoc.enableAppliedOperationsRecording(true);

        aMultisheetCalcDoc.createNew();
        aMultisheetCalcDoc.open();
        aMultisheetCalcDoc.applyOps();
        // create a second sheet
        final JSONArray insertSheetOp = getAddSheetOperation(aMultisheetCalcDoc);
        aMultisheetCalcDoc.applyOps(insertSheetOp);
        aMultisheetCalcDoc.applyOps();
        aMultisheetCalcDoc.close();

        // verify the document content that must include the values inserted before
        DocContentVerifierHelper.verifySpreadsheetDocumentContentAndStateForMultiSheet(aAppsuite, aMultisheetCalcDoc);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

    //-------------------------------------------------------------------------
    private JSONArray getAddSheetOperation(SpreadsheetDoc spreadsheetDoc) throws Exception {
        JSONObject addSheetOp = spreadsheetDoc.getAddSheetOperation();
        JSONArray ops = new org.json.JSONArray();
        ops.put(addSheetOp);
        return ops;
    }

}
