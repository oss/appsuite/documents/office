/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.List;

import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;

public class PresentationDocumentVerifier {

    //-------------------------------------------------------------------------
    public static boolean checkPresentationDocumentContent(final PresentationDoc aDoc, final String sText, final int numOfOccurrence)
        throws Exception
    {
        // verify the document content that must include the text four times
        final boolean bVerified = DocContentVerifierHelper.isContentAsExpected(aDoc, RT2TestConstants.DEFAULT_TIMEOUT, d -> 
        {
            if (d.getHtmlDocString() != null)
                return (HtmlChecker.numOfTextInHtml(d.getHtmlDocString(), PresentationDoc.STR_TEXT) == numOfOccurrence);

            return false;
        });

        return bVerified;
    }

    //-------------------------------------------------------------------------
    public static boolean checkPresentationDocumentContent(final String sHtmlDoc, List<JSONObject> aOps, final String sText, final int numOfOccurrence)
        throws Exception
    {
        // verify the document content that must include the text four times
        final boolean bVerified = DocContentVerifierHelper.isContentAsExpected(sHtmlDoc, aOps, Doc.DOCTYPE_TEXT, d -> 
        {
            if (d.getHtmlDocString() != null)
                return (HtmlChecker.numOfTextInHtml(d.getHtmlDocString(), PresentationDoc.STR_TEXT) == numOfOccurrence);

            return false;
        });

        return bVerified;
    }

    //-------------------------------------------------------------------------
    public static boolean checkPresentationDocumentContent(final TextDoc aDoc, final String sImageData1, final String sImageData2)
        throws Exception
    {
        // verify the document content that must include the text four times
        final boolean bVerified = DocContentVerifierHelper.isContentAsExpected(aDoc, RT2TestConstants.DEFAULT_TIMEOUT, d -> 
        {
            if (d.getHtmlDocString() != null)
                return ((HtmlChecker.containsImageWithID(d.getHtmlDocString(), sImageData1)) &&
                        ((HtmlChecker.containsImageWithID(d.getHtmlDocString(), sImageData2))));

            return false;
        });

        return bVerified;
    }

}
