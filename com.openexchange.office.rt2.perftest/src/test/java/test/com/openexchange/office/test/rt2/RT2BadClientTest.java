/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.json.JSONArray;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2RestAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.tools.error.ErrorCode;

import test.com.openexchange.office.test.rt2.utils.DocContentVerifierHelper;
import test.com.openexchange.office.test.rt2.utils.RT2TestConstants;
import test.com.openexchange.office.test.rt2.utils.StringHelper;
import test.com.openexchange.office.test.rt2.utils.TestRunConfigHelper;
import test.com.openexchange.office.test.rt2.utils.TestSetupHelper;

public class RT2BadClientTest {

    //-------------------------------------------------------------------------
    @BeforeClass
    public static void setUpEnv() throws Exception {
        StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testClientSendCloseAndLeaveWithoutWaitingForResponse() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

        int nCountApplyOps = 0;
        boolean leaveBeforeClose = false;
        try {
            aTextDoc.createNew();
            aTextDoc.open();
            aTextDoc.applyOps(); ++nCountApplyOps;

            // we now send irregularly close and leave without waiting for the
            // response - the server should be gracefully (we set the auto_close
            // header for a graceful leave process)
            final CountDownLatch closeAndLeaveReceived = new CountDownLatch(2);
            final AtomicLong closeErrorCode = new AtomicLong(ErrorCode.CODE_NO_ERROR);
            final Deferred<RT2Message> aCloseDeferred = aTextDoc.closeAsync();

            // close
            aCloseDeferred.done(new Deferred.Func<RT2Message>() {
                @Override
                public void done(final RT2Message aResponse) throws Exception {
                    closeAndLeaveReceived.countDown();
                    Integer code = aResponse.getHeader(RT2Protocol.HEADER_ERROR_CODE);
                    closeErrorCode.set(code);
                }
            });

            // leave
            final AtomicLong leaveErrorCode = new AtomicLong(ErrorCode.CODE_NO_ERROR);
            final Deferred<RT2Message> aLeaveDeferred = aTextDoc.leaveAsync(true);

            aLeaveDeferred.done(new Deferred.Func<RT2Message>() {

                @Override
                public void done(final RT2Message aResponse) throws Exception {
                    closeAndLeaveReceived.countDown();
                    Integer code = aResponse.getHeader(RT2Protocol.HEADER_ERROR_CODE);
                    leaveErrorCode.set(code);
                }
            });

            boolean received = closeAndLeaveReceived.await(RT2TestConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);

            final String clientUid = aTextDoc.getRT2ClientUID();
            final String docUid = aTextDoc.getDocUID();
            if (!aLeaveDeferred.isPending() && aCloseDeferred.isPending()) {
            	// Special case: as we don't wait for the close response before we send
            	// the leave request it's possible that the leave-request is processed
            	// before the close (leave has no seq-nr). We have to treat this as "success"
            	// if the leave is correctly answered. Just check the leave response error
            	// code for possible problems.
                if (leaveErrorCode.get() != ErrorCode.CODE_NO_ERROR) {
                    Assert.fail(StringHelper.replacePlaceholdersWithArgs("Leave overtook close and got error code, with com.openexchange.rt2.client.uid{}, com.openexchange.rt2.document.uid {} and error code {}", clientUid, docUid, Long.toString(leaveErrorCode.get())));
                } else {
                    leaveBeforeClose = true;
                }
            } else {
                if (!received || aCloseDeferred.isPending()) {
                    Assert.fail(StringHelper.replacePlaceholdersWithArgs("Close request has not been answered in time by server with com.openexchange.rt2.client.uid {}, com.openexchange.rt2.document.uid {} and error code {}", clientUid, docUid, Long.toString(leaveErrorCode.get())));
                }
                if (closeErrorCode.get() != ErrorCode.CODE_NO_ERROR) {
                    Assert.fail(StringHelper.replacePlaceholdersWithArgs("Close response with error code != NO_ERROR received with com.openexchange.rt2.client.uid {}, com.openexchange.rt2.document.uid {} and error code {}", clientUid, docUid, Long.toString(leaveErrorCode.get())));
                }
                if (aLeaveDeferred.isPending()) {
                    Assert.fail(StringHelper.replacePlaceholdersWithArgs("Leave request has not been answered in time by server! com.openexchange.rt2.client.uid {}, com.openexchange.rt2.document.uid {} and error code {}", clientUid, docUid, Long.toString(leaveErrorCode.get())));
                }
                if (closeErrorCode.get() != ErrorCode.CODE_NO_ERROR) {
                    Assert.fail(StringHelper.replacePlaceholdersWithArgs("Leave response with error code received, with com.openexchange.rt2.client.uid {}, com.openexchange.rt2.document.uid {} and error code {}", clientUid, docUid, Long.toString(leaveErrorCode.get())));
                }
            }
        } catch (Exception e) {
            Assert.fail("Exception detected " + e.getMessage());
        }

        // see DOCS-3198 and remove this if-statement as soon as this issue is fixed
        if (!leaveBeforeClose) {
            // verify the document content that must include the text one time
            DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);
        }

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

    @Test
    public void testClientSendsEmergencyLeaveAfterCloseLeave() throws Exception {
        RT2.HANDLE_PROTOCOL_ERRORS_GRACEFULLY = true;

        final TestRunConfig aEnvNode = TestRunConfigHelper.defineEnv4Node01();
        final OXAppsuite aAppsuite = TestSetupHelper.openAppsuiteConnection(aEnvNode);
        final TextDoc aTextDoc = (TextDoc) TestSetupHelper.newDocInst(aAppsuite, Doc.DOCTYPE_TEXT);

        int nCountApplyOps = 0;
        try {
            aTextDoc.createNew();
            aTextDoc.open();
            aTextDoc.applyOps(); ++nCountApplyOps;

            aTextDoc.close();

			final OXRT2RestAPI aRT2RESTAPI = new OXRT2RestAPI();

			aRT2RESTAPI.bind(aAppsuite);

			// emergency leave is normally a fire & forget request, but we want
			// to check for the expected error response
			ErrorCode errorCode = aRT2RESTAPI.emergencyLeave(aTextDoc.getRT2ClientUID(), aTextDoc.getDocUID(), new JSONArray(), false);
			Assert.assertTrue("Response does not contain excepted error code " + errorCode.toString(), ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR.equals(errorCode));
        } catch (Exception e) {
            Assert.fail("Exception detected " + e.getMessage());
        }

        // verify the document content that must include the text one time
        DocContentVerifierHelper.verifyDocumentContent(aAppsuite, aTextDoc, TextDoc.STR_TEXT, nCountApplyOps);

        TestSetupHelper.closeAppsuiteConnections(aAppsuite);
    }

}
