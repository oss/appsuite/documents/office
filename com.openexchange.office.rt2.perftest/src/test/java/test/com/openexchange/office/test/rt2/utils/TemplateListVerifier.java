/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

public class TemplateListVerifier
{
    //-------------------------------------------------------------------------
    private static final PropertyInfo[] aKnownProps = new PropertyInfo[] {
        new PropertyInfo(TemplateProperties.PROPERTY_FILE_ID, String.class, false),
        new PropertyInfo(TemplateProperties.PROPERTY_FOLDER_ID, String.class, false),
        new PropertyInfo(TemplateProperties.PROPERTY_FILENAME, String.class, false),
        new PropertyInfo(TemplateProperties.PROPERTY_TITLE, String.class, true),
        new PropertyInfo(TemplateProperties.PROPERTY_SOURCE, String.class, false),
        new PropertyInfo(TemplateProperties.PROPERTY_LAST_MODIFIED, Long.class, false)
    };

    //-------------------------------------------------------------------------
    public static final Map<String, PropertyInfo> PROP_INFO_MAP = Collections.unmodifiableMap(MapHelper.toMap(Arrays.asList(aKnownProps)));

    //-------------------------------------------------------------------------
	private TemplateListVerifier()
	{
		// helper class with only static functions
	}

    //-------------------------------------------------------------------------
	public static boolean verifyTemplateList(final JSONArray aTemplateList)
	{
		boolean bResult = false;

		if ((null != aTemplateList) && (aTemplateList.length() > 0))
		{
			int i = 0;
			boolean bValid = true;

			while (bValid && (i < aTemplateList.length()))
				bValid = checkTemplateListEntry(aTemplateList.getJSONObject(i++));
			bResult = bValid;
		}

		return bResult;
	}

    //-------------------------------------------------------------------------
	public static boolean checkTemplateListEntry(final JSONObject o)
	{
		if (o == null)
		{
			Assert.fail("Template list entry must not be null!");
			return false;
		}
		else
			return PROP_INFO_MAP.values()
					            .stream()
			                    .allMatch(v -> PropertyInfo.checkPropertyValue(v, o.opt(v.key())));
	}

}
