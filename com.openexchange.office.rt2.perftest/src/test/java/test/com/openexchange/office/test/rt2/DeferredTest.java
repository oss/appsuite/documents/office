/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2;

import org.junit.Assert;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.fwk.Deferred;

public class DeferredTest
{
	
	public static class DeferredForTest implements Deferred.Base< String >
	{
        private boolean bDoneCalled   = false;
        private boolean bFailCalled   = false;
        private boolean bAlwaysCalled = false;
        private String  sValue        = null;

        public void reset()
        {
            bDoneCalled   = false;
            bFailCalled   = false;
            bAlwaysCalled = false;
            sValue = null;
        }

        public String getValue()
        {
            return sValue;
        }

        public boolean wasDoneCalled()
        {
            return bDoneCalled;
        }

        public boolean wasFailCalled()
        {
            return bFailCalled;
        }

        public boolean wasAlwaysCalled()
        {
            return bAlwaysCalled;
        }

        @Override
        public void done(final String aData)
            throws Exception
        {
            bDoneCalled = true;
            sValue = aData;
        }

        @Override
        public void fail(final String aData)
            throws Exception
        {
            bFailCalled = true;
            sValue = aData;
        }

        @Override
        public void always(final String aData)
            throws Exception
        {
            bAlwaysCalled = true;
            sValue = aData;
        }
    };

    @Test
    public void test()
    	throws Exception
    {
        final DeferredForTest    aDeferredTest = new DeferredForTest();
              Deferred< String > aDef          = new Deferred< String > ();

        aDef.done(aDeferredTest)
            .fail(aDeferredTest)
            .always(aDeferredTest);

        Assert.assertTrue("Non resolved/rejected deferred must be pending", aDef.isPending());
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());

        aDef.resolve("Hello RESOLVE ");
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Done had to be called", aDeferredTest.wasDoneCalled());
        Assert.assertTrue("Always had to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Resolved/rejected deferred must not be pending", aDef.isPending());

        aDef.reject ("Hello REJECT"  );
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Fail had to be called", aDeferredTest.wasFailCalled());
        Assert.assertTrue("Always had to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Resolved/rejected deferred must not be pending", aDef.isPending());

        aDef.reject ("Hello REJECT 2");
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Fail had to be called", aDeferredTest.wasFailCalled());
        Assert.assertTrue("Always had to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Resolved/rejected deferred must not be pending", aDef.isPending());

        aDeferredTest.reset();
        aDef.resolve("Resolved");
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Done had to be called", aDeferredTest.wasDoneCalled());
        Assert.assertTrue("Always had to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Fail must not be called", aDeferredTest.wasFailCalled());
        Assert.assertEquals("Resolved", aDeferredTest.getValue());
        Assert.assertFalse("Resolved/rejected deferred must not be pending", aDef.isPending());

        aDeferredTest.reset();
        aDef.reject("Rejected");
        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Fail had to be called", aDeferredTest.wasFailCalled());
        Assert.assertTrue("Always had to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Done must not be called", aDeferredTest.wasDoneCalled());
        Assert.assertEquals("Rejected", aDeferredTest.getValue());
        Assert.assertFalse("Resolved/rejected deferred must not be pending", aDef.isPending());

        aDef.close();
        Assert.assertTrue("Deferred must be closed", aDef.isClosed());

        aDeferredTest.reset();
        aDef = new Deferred< String > (500);
        aDef.done(aDeferredTest)
            .fail(aDeferredTest)
            .always(aDeferredTest);

        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertFalse("Fail must not to be called", aDeferredTest.wasFailCalled());
        Assert.assertFalse("Always must not to be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Done must not be called", aDeferredTest.wasDoneCalled());
        Assert.assertTrue("Non resolved/rejected deferred must be pending", aDef.isPending());

        Thread.sleep(1000);

        Assert.assertFalse("Deferred must not be closed", aDef.isClosed());
        Assert.assertTrue("Fail must to be called", aDeferredTest.wasFailCalled());
        Assert.assertTrue("Always must be called", aDeferredTest.wasAlwaysCalled());
        Assert.assertFalse("Done must no be called", aDeferredTest.wasDoneCalled());
        Assert.assertFalse("Auto-rejected deferred must not be pending", aDef.isPending());
    }

}
