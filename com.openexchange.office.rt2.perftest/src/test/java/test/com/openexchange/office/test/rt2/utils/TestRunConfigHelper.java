/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;

public class TestRunConfigHelper {
	// -------------------------------------------------------------------------
	// private static final Logger LOG =
	// Slf4JLogger.create(TestRunConfigHelper.class);

	// -------------------------------------------------------------------------
	private TestRunConfigHelper() {
	}

	// -------------------------------------------------------------------------
	public static TestRunConfig defineEnv4Node01() throws Exception {
		final TestRunConfig aEnv = new TestRunConfig();

		aEnv.sTestId = "Node-01";

		aEnv.aUser = new UserDescriptor();
		if (Boolean.valueOf(System.getenv("IN_ECLIPSE"))) {
			aEnv.aUser.sLogin = "demo";
			aEnv.aUser.sPassword = "secret";

			aEnv.sAlternativeServerName = "127.0.0.1";
			aEnv.sAlternativeServerURL = "http://" + aEnv.sAlternativeServerName + ":8011";
			aEnv.sServerName = "localhost";
			aEnv.nServerJMXPort = 9913;
			aEnv.sServerURL = "http://" + aEnv.sServerName + ":8011";
			aEnv.sWSURL = "ws://" + aEnv.sServerName + ":8011";
			aEnv.hazelcastGroupName = "localhorstCluster-4.1";
			aEnv.hazelcastGroupPassword = "secret";
			aEnv.hazelcastPort = "5705"; // new port for Hazelcast 4
		} else {
			aEnv.aUser = new UserDescriptor();
			aEnv.aUser.sLogin = "test3@9";
			aEnv.aUser.sPassword = "secret";

			aEnv.sAlternativeServerName = "10.20.28.178"; // please use ip-address of aServerName
			aEnv.sServerName = "ci3.docs.open-xchange.com";
			aEnv.nServerJMXPort = 9999;
            aEnv.sAlternativeServerURL = "http://" + aEnv.sAlternativeServerName + ":8009";
			aEnv.sServerURL = "http://ci3.docs.open-xchange.com:8009";
			aEnv.sWSURL = "ws://ci3.docs.open-xchange.com:8009";
			aEnv.hazelcastPort = "5705"; // new port for Hazelcast 4
		}

		aEnv.sAPIRelPath = "ajax";

		aEnv.rGlobalConfig = defineGlobalEnv();

		return aEnv;
	}

    // -------------------------------------------------------------------------
    public static TestRunConfig defineEnv4Node01User2() throws Exception {
        final TestRunConfig aEnv = new TestRunConfig();

        aEnv.sTestId = "Node-01";

        aEnv.aUser = new UserDescriptor();
        if (Boolean.valueOf(System.getenv("IN_ECLIPSE"))) {
            aEnv.aUser.sLogin = "demo2";
            aEnv.aUser.sPassword = "secret";

            aEnv.sAlternativeServerName = "127.0.0.1";
            aEnv.sAlternativeServerURL = "http://" + aEnv.sAlternativeServerName + ":8011";
            aEnv.sServerName = "localhost";
            aEnv.nServerJMXPort = 9913;
            aEnv.sServerURL = "http://" + aEnv.sServerName + ":8011";
            aEnv.sWSURL = "ws://" + aEnv.sServerName + ":8011";
            aEnv.hazelcastGroupName = "localhorstCluster-4.1";
            aEnv.hazelcastGroupPassword = "secret";
            aEnv.hazelcastPort = "5705"; // new port for Hazelcast 4
        } else {
            aEnv.aUser = new UserDescriptor();
            aEnv.aUser.sLogin = "test2@9";
            aEnv.aUser.sPassword = "secret";

            aEnv.sAlternativeServerName = "10.20.28.178"; // please use ip-address of aServerName
            aEnv.sServerName = "ci3.docs.open-xchange.com";
            aEnv.nServerJMXPort = 9999;
            aEnv.sAlternativeServerURL = "http://" + aEnv.sAlternativeServerName + ":8009";
            aEnv.sServerURL = "http://ci3.docs.open-xchange.com:8009";
            aEnv.sWSURL = "ws://ci3.docs.open-xchange.com:8009";
            aEnv.hazelcastPort = "5705"; // new port for Hazelcast 4
        }

        aEnv.sAPIRelPath = "ajax";

        aEnv.rGlobalConfig = defineGlobalEnv();

        return aEnv;
    }

	// -------------------------------------------------------------------------
	// configuration for RT2 cluster installation on Open Stack
	/*
	 * private TestRunConfig defineEnv4Node01 () throws Exception { final
	 * TestRunConfig aEnv = new TestRunConfig ();
	 * 
	 * aEnv.sTestId = "Node-01";
	 * 
	 * aEnv.aUser = new UserDescriptor (); aEnv.aUser.sLogin =
	 * "user-ctx4-9@context-4"; aEnv.aUser.sPassword = "secret";
	 * 
	 * aEnv.sServerName = "10.20.28.152"; aEnv.nServerJMXPort = 9999;
	 * aEnv.sServerURL = "http://10.20.28.152:8009"; aEnv.sWSURL =
	 * "ws://10.20.28.152:8009"; aEnv.sAPIRelPath = "ajax";
	 * 
	 * aEnv.rGlobalConfig = defineGlobalEnv ();
	 * 
	 * return aEnv; }
	 */
	// -------------------------------------------------------------------------
	public static TestRunConfig defineEnv4Node02() throws Exception {
		final TestRunConfig aEnv = new TestRunConfig();

		aEnv.sTestId = "Node-02";

		aEnv.aUser = new UserDescriptor();
		aEnv.aUser.sLogin = "demo";
		aEnv.aUser.sPassword = "secret";

		aEnv.sServerName = "localhost";

		aEnv.nServerJMXPort = 9914;
		aEnv.sServerURL = "http://" + aEnv.sServerName + ":8012";
		aEnv.sWSURL = "ws://" + aEnv.sServerName + ":8012";
		aEnv.sAPIRelPath = "ajax";

		aEnv.rGlobalConfig = defineGlobalEnv();

		return aEnv;
	}

	// -------------------------------------------------------------------------
	public static TestRunConfig defineEnv4Node03() throws Exception {
		final TestRunConfig aEnv = new TestRunConfig();

		aEnv.sTestId = "Node-03";

		aEnv.aUser = new UserDescriptor();
		aEnv.aUser.sLogin = "demo";
		aEnv.aUser.sPassword = "secret";

		aEnv.sServerName = "localhost";

		aEnv.nServerJMXPort = 9915;
		aEnv.sServerURL = "http://" + aEnv.sServerName + ":8013";
		aEnv.sWSURL = "ws://" + aEnv.sServerName + ":8013";
		aEnv.sAPIRelPath = "ajax";

		aEnv.rGlobalConfig = defineGlobalEnv();

		return aEnv;
	}

	// -------------------------------------------------------------------------
	public static TestConfig defineGlobalEnv() throws Exception {
		final TestConfig aEnv = TestConfig.get();
		return aEnv;
	}
}
