/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.test.rt2.utils;

public class StringHelper {

	public static final String STR_PLACEHOLDER = "{}";

    //-------------------------------------------------------------------------
	private StringHelper() {}

    //-------------------------------------------------------------------------
    public static String replacePlaceholdersWithArgs(String msg, String...values)  {
    	if (msg == null)
    		throw new IllegalArgumentException("Placeholder string must not be null!");

    	final StringBuffer tmp = new StringBuffer(msg);
    	int valIndex = 0;
    	int index = 0;
    	while ((index = tmp.indexOf(STR_PLACEHOLDER, index)) > 0) {
    		String tmpValue = (values.length > valIndex) ? values[valIndex] : "null";
    		String value = (tmpValue == null) ? "null" : tmpValue;
    		tmp.replace(index, index + STR_PLACEHOLDER.length() , value);
    		index += value.length();
    		++valIndex;
    	}

    	return tmp.toString();
    }

}
