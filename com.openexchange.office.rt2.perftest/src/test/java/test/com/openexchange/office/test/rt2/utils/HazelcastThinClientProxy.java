/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package test.com.openexchange.office.test.rt2.utils;

import org.apache.commons.lang3.StringUtils;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;

public class HazelcastThinClientProxy {

    private static Object sync = new Object();
    private static HazelcastInstance hzInstance = null;

    private static void initHzInstance() throws Exception {
        synchronized (sync) {
            if (hzInstance == null) {
                final ClientConfig clientConfig = new ClientConfig();
                if (StringUtils.isNotEmpty(TestRunConfigHelper.defineEnv4Node01().hazelcastGroupName)) {
                	clientConfig.setClusterName(TestRunConfigHelper.defineEnv4Node01().hazelcastGroupName);
                }

                String member;
                if (TestRunConfigHelper.defineEnv4Node01().sServerName.equals("localhost")) {
                    member = "127.0.0.1";
                } else {
                    member = TestRunConfigHelper.defineEnv4Node01().sServerName;
                }
                member += ":" + TestRunConfigHelper.defineEnv4Node01().hazelcastPort;

                final ClientNetworkConfig clientNetConfig = new ClientNetworkConfig();
                clientNetConfig.addAddress(member);
                clientConfig.setNetworkConfig(clientNetConfig);

                hzInstance = HazelcastClient.newHazelcastClient(clientConfig);
                System.out.println(hzInstance.getCluster().getMembers());
            }
        }
    }

    public static HazelcastInstance getHzInstance() throws Exception {
        synchronized (sync) {
            if (hzInstance == null) {
                initHzInstance();
            }
            return hzInstance;
        }
    }

    public static void shutdown() throws Exception {
        HazelcastInstance hzInstanceToShutdown = null;

        synchronized (sync) {
            hzInstanceToShutdown = hzInstance;
            hzInstance = null;
        }

        if (hzInstanceToShutdown != null) {
            hzInstanceToShutdown.shutdown();
        }
    }

    private HazelcastThinClientProxy() {}
}
