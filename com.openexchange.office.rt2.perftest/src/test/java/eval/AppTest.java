/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package eval;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.Ignore;
import org.junit.Test;

import com.openexchange.office.rt2.perftest.App;
import com.openexchange.office.rt2.perftest.cmdline.CmdLine;
import com.openexchange.office.rt2.perftest.impl.EOperation;

@Ignore
public class AppTest
{
    //-------------------------------------------------------------------------
    @Test
    public void testApp()
    	throws Exception
    {
    	final String[] lArgs = new String[4];
    	
    	lArgs[0] = "-"+CmdLine.OPT_SHORT_OPERATION;

//		lArgs[1] = EOperation.STR_PREPARE_TESTENV;
//		lArgs[1] = EOperation.STR_DELETE_TESTENV;
    	lArgs[1] = EOperation.STR_RUN_TESTS;

    	lArgs[2] = "-"+CmdLine.OPT_SHORT_CONFIG_PATH;
    	lArgs[3] = "/tmp";
    	
    	final long nStart = System.currentTimeMillis();

    	App.main(lArgs);

    	final long nEnd   = System.currentTimeMillis();
    	final long nTime  = (nEnd - nStart);
    		      
    	System.out.println("### time needed "+DurationFormatUtils.formatDuration(nTime, "HH:mm:ss-S", true)+"");
    	System.exit(0);
    }
}
