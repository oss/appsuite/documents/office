/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package eval;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.Ignore;
import org.junit.Test;
import com.openexchange.office.rt2.perftest.App;
import com.openexchange.office.rt2.perftest.cmdline.CmdLine;
import com.openexchange.office.rt2.perftest.impl.EOperation;

@Ignore("System.exit(0) in einem Unittest???")
public class PresenterClient
{
    //-------------------------------------------------------------------------
    public static final String ENCODING              = "utf-8";
    public static final String CONFIG_DIR            ="/tmp/presenter/client";
    public static final String FILE_TEST_CONFIG      = CONFIG_DIR+"/config.properties";
    public static final String FILE_PRESENTER_CONFIG = CONFIG_DIR+"/presentertest.properties";

    //-------------------------------------------------------------------------
    @Test
    public void run()
    	throws Exception
    {
        FileUtils.forceMkdir(new File (CONFIG_DIR));
        impl_createTestConfig ();

    	final String[] lArgs = new String[4];

    	lArgs[0] = "-"+CmdLine.OPT_SHORT_OPERATION;
    	lArgs[1] = EOperation.STR_RUN_TESTS;
    	lArgs[2] = "-"+CmdLine.OPT_SHORT_CONFIG_PATH;
    	lArgs[3] = CONFIG_DIR;
    	
    	final long nStart = System.currentTimeMillis();

    	App.main(lArgs);

    	final long nEnd   = System.currentTimeMillis();
    	final long nTime  = (nEnd - nStart);
    		      
    	System.out.println("### time needed "+DurationFormatUtils.formatDuration(nTime, "HH:mm:ss-S", true)+"");
    	System.exit(0);
    }
    
    //-------------------------------------------------------------------------
    private void impl_createTestConfig ()
        throws Exception
    {
        final String sTestConfig        = ""
                                        + "context.count=10\n"
                                        + "user.count=100\n"
                                        
                                        + "testcase.count=1\n"
                                        
                                        + "testcase.1.impl=com.openexchange.office.rt2.perftest.testcases.PresenterTestClient\n"
                                        + "testcase.1.count=100\n"
                                        + "testcase.1.user-range=100-500\n"
                                        
                                        + "test.runner.strategy=delay-one-by-one\n"
                                        + "test.runner.delay=1000\n"
                                        
                                        + "dbg.server.host=null\n"
                                        + "monitor.server.host=localhost\n"
                                        + "monitor.server.port=56789\n"
                                        
                                        + "appsuite.url=http://localhost/appsuite\n"
                                        + "websocket.url=ws://localhost/appsuite\n"
                                        
                                        + "session.timeout=10000\n"
                                        + "rest.timeout=120000\n"
                                        + "websocket.timeout.send.async=120000\n"
                                        + "websocket.buffsize.out=5242880\n"
                                        + "websocket.buffsize.in=5242880\n"

                                        + "use.uuid.filenames=false\n"
                                        ;

        final String sPresenterConfig   = ""
                                        + "test.presenter.client.count=100\n"

                                        + "test.presenter.slide.count=8\n"
                                        + "test.presenter.slide.changes=25\n"
                                        
                                        + "test.presenter.join-presentation.time-frame=10000\n"

                                        + "test.presenter.folder-id=251\n"
                                        + "test.presenter.file-id=251/605\n"
                                        + "test.presenter.drive-doc-id=1001@251/605\n"

                                        + "test.presenter.timeout.continue=true\n"
                                        + "test.presenter.timeout.close=true\n"
                                        + "test.presenter.autoclose.on.error=true\n"

                                        + "test.presenter.rc.host=localhost\n"
                                        + "test.presenter.rc.port=61617\n"
                                        ;

        FileUtils.writeStringToFile(new File(FILE_TEST_CONFIG     ), sTestConfig     , ENCODING);
        FileUtils.writeStringToFile(new File(FILE_PRESENTER_CONFIG), sPresenterConfig, ENCODING);
    }
}
