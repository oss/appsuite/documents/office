/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package eval;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXSession;

public class Testi
{
    //-------------------------------------------------------------------------
    // 224/4173
    public static void main(String[] lArgs)
    {
    	try
    	{
    		final TestRunConfig aCfg = new TestRunConfig ();
    		aCfg.aUser = new UserDescriptor ();
    		aCfg.aUser.sName = "demo";
    		aCfg.aUser.sPassword = "secret"; // has to be defined before test can run !
    		
    		aCfg.sServerURL = "http://localhost/appsuite";
    		aCfg.sWSURL = "ws://localhost:8009";
    		aCfg.sAPIRelPath = "api";

    		final OXAppsuite appsuite = new OXAppsuite (aCfg);
    		final OXSession session = appsuite.accessSession();

  		    session.login(aCfg.aUser);

    		session.logout();
    	}
    	catch(Throwable ex)
    	{
    		System.err.println (ex.getMessage());
    		ex.printStackTrace (System.err     );
    	}
    }
}
