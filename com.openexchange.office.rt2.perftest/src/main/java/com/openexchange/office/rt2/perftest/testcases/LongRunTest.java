/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2Env;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
/** test if RT2 backend works for a longer time period ...
 *  and check if e.g. session will timeout ...
 *  or memory consumption will increase ...
 */
public class LongRunTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(LongRunTest.class);

    //-------------------------------------------------------------------------
    public LongRunTest ()
        throws Exception
    {
    	super();
    }

    //-------------------------------------------------------------------------
    @Override
    public int calculateTestIterations ()
        throws Exception
    {
    	return Integer.MAX_VALUE;
    }

    //-------------------------------------------------------------------------
    @Override
    protected void runTest()
    	throws Exception
    {
//		LogUtils.enableDebugLogging();

    	final TestRunConfig         aCfg                          = getRunConfig ();
        final UserDescriptor        aUser                         = aCfg.aUser;
        final StatusLine            aStatusLine                   = getStatusLine ();

        final int                   nIterations                   = Integer.MAX_VALUE;
        final int                   nDelayInMS                    = 2000;
        final boolean               bValidateSessionsOnServerSide = true; // TODO read from config

        final OXAppsuite            aOX                           = new OXAppsuite   (aCfg);
        final OXSession             aSession                      = aOX.accessSession();
        final OXRT2Env              aRT2Env                       = aOX.accessRT2Env ();

              String                sLastSessionID                = null;
              String                sLastPublicSessionCookie      = null;
              String                sLastSessionCookie            = null;
              String                sLastSecretCookie             = null;

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("login ..."	 )
    		.log 		();
    	aSession.login (aUser);

    	sLastSessionID           = aSession.getSessionId          ();
    	sLastPublicSessionCookie = aSession.getPublicSessionCookie();
    	sLastSessionCookie       = aSession.getSessionCookie      ();
    	sLastSecretCookie        = aSession.getSecretCookie       ();

    	LOG	.forLevel	(ELogLevel.E_WARNING)
    		.withMessage("session info ..." )
    		.setVar		("session-id"           , sLastSessionID          )
    		.setVar		("public-session-cookie", sLastPublicSessionCookie)
    		.setVar		("session-cookie"       , sLastSessionCookie      )
    		.setVar		("secret-cookie"        , sLastSecretCookie       )
    		.log 		();

    	for (int i=0; i<nIterations; ++i)
        {
            final String  sOXRoute      = aSession.getLoadBalanceRoute();
            final String  sActSessionID = aSession.getSessionId       ();
            final boolean bIsAutoLogin  = aSession.isAutoLogin        ();

            if (StringUtils.equals(sLastSessionID, sActSessionID))
            {
                aStatusLine.printMessage("route="+sOXRoute+" : auto-login="+bIsAutoLogin+" : validation="+bValidateSessionsOnServerSide+" : session="+sActSessionID);
            }
            else
            {
                aStatusLine.printMessage("route="+sOXRoute+" : SESSION CHANGE ? : last session="+sLastSessionID+" : act session="+sActSessionID);
                aStatusLine.countError();
            }

            final RT2 aDocImpl = aRT2Env.newRT2Inst ();
            final Doc aDoc     = new TextDoc ();

            aDocImpl.enableSessionValidationOnServerSide(bValidateSessionsOnServerSide);

            aDoc.bind (aDocImpl   );
        	aDoc.bind (aStatusLine);

            enableMessageExceptionHandling(aDocImpl);

        	LOG	.forLevel	(ELogLevel.E_INFO	      )
        		.withMessage("create new document ...")
        		.log 		();
            aDoc.createNew();

            LOG	.forLevel	(ELogLevel.E_INFO	)
        		.withMessage("load document ...")
        		.log 		();
            aDoc.open();

            LOG	.forLevel	(ELogLevel.E_INFO	 )
        		.withMessage("close document ...")
        		.log 		();
            aDoc.close();

            Thread.sleep (nDelayInMS);

            aStatusLine.countTestIteration();
        }

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("logout ..."	 )
    		.log 		();
        aSession.logout ();
    }
}

