/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;

import com.openexchange.office.rt2.perftest.impl.ETestRunnerStrategy;
import com.openexchange.office.rt2.perftest.impl.EThreadPoolType;
import com.openxchange.office_communication.tools.config.IComplexConfiguration;

//=============================================================================
public class TestConfig
{
    //-------------------------------------------------------------------------
    public static final String CONFIG_PACKAGE = "/config";

    //-------------------------------------------------------------------------
//	public static final String DEFAULT_DB_SCHEMA = "oxcfgdb_5";
//	public static final String DEFAULT_PASSWORD  = "xxx";

    //-------------------------------------------------------------------------
    public static final String OXADMINMASTER_NAME     = "oxadminmaster";
    public static final String OXADMINMASTER_PASSWORD = "secret";

    //-------------------------------------------------------------------------
    public static final String CONFIG_PREFIX_PREDEFINED_USER = "user.";

    //-------------------------------------------------------------------------
    public static final int BASEID_CONTEXT = 1000;
    public static final int BASEID_USER    = 1000;

    public static final String HZ_CLUSTER_NAME = "localhorstCluster";

    //-------------------------------------------------------------------------
    private TestConfig ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    public static synchronized TestConfig get ()
        throws Exception
    {
    	if (m_gSingleton == null)
    		m_gSingleton = new TestConfig ();
    	return m_gSingleton;
    }
    
    //-------------------------------------------------------------------------
    public static void defineOutsideReportPath (final String sReportPath)
        throws Exception
    {
        TestConfig.get().setReportDir(sReportPath);
    }
    
    //-------------------------------------------------------------------------
    public Map< Integer, UserDescriptor > getUser ()
    	throws Exception
    {
    	if (m_lUsers != null)
    		return m_lUsers;

    	final Map< Integer, UserDescriptor > lUsers                 = new HashMap<> ();
    	final String                         sDefaultPassword       = getOXUserDefaultPassword ();
    	final int                            nContextCount          = getContextCount ();
    	final int                            nUserCount             = getUserCount    ();
              int                            nUserNrOverAllContexts = 0;

        if (nUserCount > 0)
        {
        	// auto-generated users, contexts, passwords etc.
	    	for (int nContextNr=1; nContextNr<=nContextCount; ++nContextNr)
	    	{
	    		for (int nUserNrInContext=1; nUserNrInContext<=nUserCount; ++nUserNrInContext)
	    		{
	    			++nUserNrOverAllContexts;

	    			final UserDescriptor aUser = new UserDescriptor ();
	    			aUser.nNr        = nUserNrOverAllContexts;
	    			aUser.nContextNr = nContextNr;
	    			aUser.sName      = nameUser (nContextNr, nUserNrInContext);
	    			aUser.sPassword  = sDefaultPassword;
	    			aUser.sLogin     = aUser.sName + "@" + TestConfig.nameContext(nContextNr);

	    			lUsers.put (nUserNrOverAllContexts, aUser);
	    		}
	    	}
        }
        else
        {
            // pre-defined users in configuration
            final IComplexConfiguration           iCfg         = mem_Config ();

            int     nUserIndex = 1;
            boolean bUserConfigFound = true;

            while (bUserConfigFound)
            {
                final String                      sUserNr      = Integer.toString(nUserIndex);
                final Set< Map< String, String >> lUserConfigs = iCfg.gets("user", sUserNr);

                if (lUserConfigs.isEmpty())
                    bUserConfigFound = false;
                else
                {
                    final Iterator<Map<String, String>> aUserPropertiesIter = lUserConfigs.iterator();
                    while (aUserPropertiesIter.hasNext())
                    {
                        createAndaddUserDescriptor(lUsers, nUserIndex, aUserPropertiesIter.next());
                    }

                    ++nUserIndex;
                }
            }
        }

    	m_lUsers = lUsers;
    	return m_lUsers;
    }

    //-------------------------------------------------------------------------
    private void createAndaddUserDescriptor(final Map< Integer, UserDescriptor > aUserMap, int nNr, final Map<String, String> aUserProperties)
        throws Exception
    {
        final UserDescriptor aUserDescriptor = new UserDescriptor(aUserProperties);

        aUserDescriptor.nNr = nNr;

        // default handling of some properties
        if (aUserDescriptor.nContextNr == null)
            aUserDescriptor.nContextNr = getOXUserDefaultContext ();
        if (StringUtils.isEmpty(aUserDescriptor.sPassword))
            aUserDescriptor.sPassword = getOXUserDefaultPassword();
        if (StringUtils.isEmpty(aUserDescriptor.sLogin))
            aUserDescriptor.sLogin = aUserDescriptor.sName;

        // check validity of mandatory properties
        Validate.notNull(aUserDescriptor.nNr);
        Validate.notNull(aUserDescriptor.nContextNr);
        Validate.isTrue(StringUtils.isNotEmpty(aUserDescriptor.sLogin));
        Validate.isTrue(StringUtils.isNotEmpty(aUserDescriptor.sPassword));

        aUserMap.put(nNr, aUserDescriptor);
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("deprecation")
    public List< TestCaseDescriptor > getTestCases ()
    	throws Exception
    {
    	final List< TestCaseDescriptor > lTests     =  new ArrayList<> ();
    	final IComplexConfiguration      iCfg       = mem_Config ();
    	final int                        nTestCount = iCfg.get("testcase.count", int.class);

    	for (int nTest=1; nTest<=nTestCount; ++nTest)
    	{
    		final String                      sTestNr      = Integer.toString(nTest);
    		final Set< Map< String, String >> lTestConfigs = iCfg.gets("testcase", sTestNr);
    		
    		if (lTestConfigs.isEmpty())
    			continue;
    		
    		final Map< String, String > aTestConfig = lTestConfigs.iterator().next();
    		
    		final TestCaseDescriptor aTest = new TestCaseDescriptor ();
    		aTest.nNr = nTest;
    		aTest.takeFromConfig (aTestConfig);

    		lTests.add (aTest);
    	}
    	
    	return lTests;
    }

    //-------------------------------------------------------------------------
    public ThreadPoolDescriptor getThreadPoolConfig ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg     = mem_Config ();
    	final ThreadPoolDescriptor  aPoolCfg = new ThreadPoolDescriptor ();
    	
    	aPoolCfg.eType = EThreadPoolType.fromString(iCfg.get("test.runner.thread-pool.type", String.class));
    	aPoolCfg.nSize = ConfigUtils.readInt       (iCfg.get("test.runner.thread-pool.size", String.class), 0);
    	
    	return aPoolCfg;
    }

    //-------------------------------------------------------------------------
    public ETestRunnerStrategy getTestRunnerStrategy ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("test.runner.strategy", String.class);
    	final ETestRunnerStrategy   eValue = ETestRunnerStrategy.fromString(sValue);
    	return eValue;
    }

    //-------------------------------------------------------------------------
    public boolean isBreakOnError ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Boolean               bValue = ConfigUtils.readBoolean(iCfg.get("test.break-on-error", String.class), false);
    	return bValue;
    }

    //-------------------------------------------------------------------------
    public Integer getContextCount ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("context.count", String.class), 1);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getUserCount ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("user.count", String.class), 1);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getSessionTimeoutInUNKNOWN ()
    	throws Exception
    {
        final int                   DEFAULT_UI_CLIENT = 10000;
    	final IComplexConfiguration iCfg              = mem_Config ();
    	final Integer               nValue            = ConfigUtils.readInt (iCfg.get("session.timeout", String.class), DEFAULT_UI_CLIENT);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTestRunnerDelayInMS ()
    	throws Exception
    {
        final int                   DEFAULT_1_SEC = 1000;
    	final IComplexConfiguration iCfg          = mem_Config ();
    	final Integer               nValue        = ConfigUtils.readInt (iCfg.get("test.runner.delay", String.class), DEFAULT_1_SEC);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public String getImapServer ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("imap.server", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getSmtpServer ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("smtp.server", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getAppsuiteUrl ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("appsuite.url", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getWSDLRootUrl ()
    	throws Exception
    {
    	final String sAppsuiteURL = getAppsuiteUrl();
    	final String sWSDLRoot    = StringUtils.replace(sAppsuiteURL, "/appsuite", "");
    	return sWSDLRoot;
    }

    //-------------------------------------------------------------------------
    public String getAppsuiteUrlRelpath (final String sEntryPoint)
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sKey   = "appsuite.url.relpath."+sEntryPoint;
    	final String                sValue = iCfg.get(sKey, String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getWebsocketUrl ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("websocket.url", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public Integer getRestAPITimeoutInMS ()
        throws Exception
    {
        final int                   DEFAULT_2_MIN = 120000;
        final IComplexConfiguration iCfg          = mem_Config ();
        final Integer               nValue        = ConfigUtils.readInt(iCfg.get("rest.timeout", String.class), DEFAULT_2_MIN);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getWebsocketAsynSendTimeoutInMS ()
        throws Exception
    {
        final int                   DEFAULT_2_MIN = 120000;
        final IComplexConfiguration iCfg          = mem_Config ();
        final Integer               nValue        = ConfigUtils.readInt(iCfg.get("websocket.timeout.send.async", String.class), DEFAULT_2_MIN);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getWebsocketOutBufferSizeInBytes ()
        throws Exception
    {
        final int                   DEFAULT_5_MB = 5 * 1024 * 1024;
        final IComplexConfiguration iCfg         = mem_Config ();
        final Integer               nValue       = ConfigUtils.readInt(iCfg.get("websocket.buffsize.out", String.class), DEFAULT_5_MB);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getWebsocketInBufferSizeInBytes ()
        throws Exception
    {
        final int                   DEFAULT_5_MB = 5 * 1024 * 1024;
        final IComplexConfiguration iCfg         = mem_Config ();
        final Integer               nValue       = ConfigUtils.readInt(iCfg.get("websocket.buffsize.in", String.class), DEFAULT_5_MB);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public String getOXDBSchemaName ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("oxdb.schema.name", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getOXUserDefaultPassword ()
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final String                sValue = iCfg.get("user.default.password", String.class);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public Integer getOXUserDefaultContext ()
    	throws Exception
    {
    	final int                   DEFAULT_CONTEXT = 1;
    	final IComplexConfiguration iCfg            = mem_Config ();
    	final Integer               nValue          = ConfigUtils.readInt(iCfg.get("user.default.context", String.class), 1);;
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public void setReportDir (final String sReportDir)
        throws Exception
    {
        m_sReportDirOverride = sReportDir;
    }

    //-------------------------------------------------------------------------
    public String getReportDir ()
    	throws Exception
    {
        if ( ! StringUtils.isEmpty (m_sReportDirOverride))
            return m_sReportDirOverride;
        
    	final String                sTempDir = FileUtils.getTempDirectoryPath();
    	final IComplexConfiguration iCfg     = mem_Config ();
    	final String                sValue   = iCfg.get("report.dir", String.class, sTempDir);
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public String getMonitorServerHost ()
        throws Exception
    {
        final IComplexConfiguration iCfg   = mem_Config ();
        final String                sValue = iCfg.get("monitor.server.host", String.class);
        return sValue;
    }

    //-------------------------------------------------------------------------
    public Integer getMonitorServerPort ()
        throws Exception
    {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Integer               nValue = ConfigUtils.readInt (iCfg.get("monitor.server.port", String.class), null);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public boolean useUUIDFileNames ()
        throws Exception
    {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("use.uuid.filenames", String.class), false);
        return bValue;
    }

    //-------------------------------------------------------------------------
    public <T> T getDirectValue (final String     sKey    ,
    							 final Class< T > aType   ,
    							 final T          aDefault)
    	throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final T                     aValue = (T) iCfg.get(sKey, aType, aDefault);
    	return aValue;
    }

    //-------------------------------------------------------------------------
    public static int calculateContextId (final int nContextNr)
    	throws Exception
    {
    	return BASEID_CONTEXT + nContextNr;
    }

    //-------------------------------------------------------------------------
    public static int calculateUserId (final int nUserNr)
    	throws Exception
    {
    	return BASEID_USER + nUserNr;
    }

    //-------------------------------------------------------------------------
    public static String nameContext (final int nContextNr)
    	throws Exception
    {
    	return "context-"+nContextNr;
    }

    //-------------------------------------------------------------------------
    public static String nameContextAdmin (final int nContextNr)
    	throws Exception
    {
    	return "context-admin-"+nContextNr;
    }

    //-------------------------------------------------------------------------
    public static String nameUser (final int nContextNr,
    							   final int nUserNr   )
    	throws Exception
    {
    	return "user-ctx"+nContextNr+"-"+nUserNr;
    }

    //-------------------------------------------------------------------------
    private final IComplexConfiguration mem_Config ()
        throws Exception
    {
    	if (m_iConfig == null)
    		m_iConfig = ConfigAccess.accessConfig(CONFIG_PACKAGE);
    	return m_iConfig;
    }

    //-------------------------------------------------------------------------
    private static TestConfig m_gSingleton = null;
    
    //-------------------------------------------------------------------------
    private IComplexConfiguration m_iConfig = null;

    //-------------------------------------------------------------------------
    private Map< Integer, UserDescriptor > m_lUsers = null;

    //-------------------------------------------------------------------------
    private String m_sReportDirOverride = null;
}
