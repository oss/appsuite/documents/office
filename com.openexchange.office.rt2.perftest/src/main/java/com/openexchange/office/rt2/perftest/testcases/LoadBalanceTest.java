/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.config.items.LoadBalanceTestConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2Env;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class LoadBalanceTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(LoadBalanceTest.class);

    //-------------------------------------------------------------------------
    public LoadBalanceTest ()
        throws Exception
    {
    	super();
    }

    //-------------------------------------------------------------------------
    static
    {
    	StatusLine.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    @Override
    public int calculateTestIterations ()
        throws Exception
    {
        final LoadBalanceTestConfig aTestCfg    = new LoadBalanceTestConfig();
        final int                   nIterations = aTestCfg.getRepetitions  ();
    	return nIterations;
    }

    //-------------------------------------------------------------------------
    @Override
    protected void runTest()
    	throws Exception
    {
//		LogUtils.enableDebugLogging();

    	final TestRunConfig         aCfg        = getRunConfig ();
        final UserDescriptor        aUser       = aCfg.aUser;
        final StatusLine            aStatusLine = getStatusLine ();

        final LoadBalanceTestConfig aTestCfg    = new LoadBalanceTestConfig();
        final int                   nIterations = aTestCfg.getRepetitions  ();
        final int                   nDelayInMS  = aTestCfg.getDelayInMS    ();

        final OXAppsuite            aOX         = new OXAppsuite   (aCfg);
        final OXSession             aSession    = aOX.accessSession();
        final OXRT2Env              aRT2Env     = aOX.accessRT2Env ();

              String                sLastSessionID           = null;
              String                sLastSessionCookie       = null;
              String                sLastPublicSessionCookie = null;
              String                sLastSecretCookie        = null;

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("login ..."	 )
    		.log 		();
    	aSession.login (aUser);

    	sLastSessionID           = aSession.getSessionId          ();
    	sLastSessionCookie       = aSession.getSessionCookie      ();
    	sLastPublicSessionCookie = aSession.getPublicSessionCookie();
    	sLastSecretCookie        = aSession.getSecretCookie       ();

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("session info ..." )
    		.setVar		("session-id"           , sLastSessionID          )
    		.setVar		("session-cookie"       , sLastSessionCookie      )
    		.setVar		("public-session-cookie", sLastPublicSessionCookie)
    		.setVar		("secret-cookie"        , sLastSecretCookie       )
    		.log 		();

    	for (int i=0; i<nIterations; ++i)
        {
            final RT2 aDocImpl = aRT2Env.newRT2Inst ();
            final Doc aDoc     = new TextDoc ();

            aDoc.bind (aDocImpl   );
        	aDoc.bind (aStatusLine);

            enableMessageExceptionHandling(aDocImpl);

        	LOG	.forLevel	(ELogLevel.E_INFO	      )
        		.withMessage("create new document ...")
        		.log 		();
            aDoc.createNew();

            LOG	.forLevel	(ELogLevel.E_INFO	)
        		.withMessage("load document ...")
        		.log 		();
            aDoc.open();

            final String sHTTPRoute = aSession.getLoadBalanceRoute();
            final String sWSRoute   = aDocImpl.getNodeID          ();
            if (StringUtils.equals(sHTTPRoute, sWSRoute))
                LOG	.forLevel	(ELogLevel.E_INFO)
    	    		.withMessage("["+i+"/"+nIterations+"] route-check : ok")
    	    		.setVar		("http-route", sHTTPRoute)
    	    		.setVar		("ws-route"  , sWSRoute  )
    	    		.log 		();
            else
                LOG	.forLevel	(ELogLevel.E_ERROR)
    	    		.withMessage("["+i+"/"+nIterations+"] route-check : DIFFERENT !")
    	    		.setVar		("http-route", sHTTPRoute)
    	    		.setVar		("ws-route"  , sWSRoute  )
    	    		.log 		();

            LOG	.forLevel	(ELogLevel.E_INFO	 )
        		.withMessage("close document ...")
        		.log 		();
            aDoc.close    ();

            Thread.sleep (nDelayInMS);

            aSession.refresh();
            final String sNewSessionID           = aSession.getSessionId          ();
            final String sNewSessionCookie       = aSession.getSessionCookie      ();
            final String sNewPublicSessionCookie = aSession.getPublicSessionCookie();
            final String sNewSecretCookie        = aSession.getSecretCookie       ();

            if ( ! StringUtils.equals(sLastSessionID, sNewSessionID))
            {
                LOG	.forLevel	(ELogLevel.E_ERROR)
    	    		.withMessage("["+i+"/"+nIterations+"] session changed after refresh")
    	    		.setVar		("old-session-id", sLastSessionID)
    	    		.setVar		("new-session-id", sNewSessionID )
    	    		.log 		();
            }
            if ( ! StringUtils.equals(sLastSessionCookie, sNewSessionCookie))
            {
                LOG	.forLevel	(ELogLevel.E_WARNING)
    	    		.withMessage("["+i+"/"+nIterations+"] session cookie changed after refresh")
    	    		.setVar		("old-session-cookie", sLastSessionCookie)
    	    		.setVar		("new-session-cookie", sNewSessionCookie )
    	    		.log 		();
            }
            if ( ! StringUtils.equals(sLastPublicSessionCookie, sNewPublicSessionCookie))
            {
                LOG	.forLevel	(ELogLevel.E_WARNING)
    	    		.withMessage("["+i+"/"+nIterations+"] public session cookie changed after refresh")
    	    		.setVar		("old-public-session-cookie", sLastPublicSessionCookie)
    	    		.setVar		("new-public-session-cookie", sNewPublicSessionCookie )
    	    		.log 		();
            }
            if ( ! StringUtils.equals(sLastSecretCookie, sNewSecretCookie))
            {
                LOG	.forLevel	(ELogLevel.E_WARNING)
    	    		.withMessage("["+i+"/"+nIterations+"] secret cookie changed after refresh")
    	    		.setVar		("old-secret-cookie", sLastSecretCookie)
    	    		.setVar		("new-secret-cookie", sNewSecretCookie )
    	    		.log 		();
            }
            sLastSessionID     = sNewSessionID    ;
            sLastSessionCookie = sNewSessionCookie;
            sLastSecretCookie  = sNewSecretCookie ;
        }

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("logout ..."	 )
    		.log 		();
        aSession.logout ();
    }
}

