/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class RT2ScheduledThreadPoolExecutor {

    //-------------------------------------------------------------------------
    private static final int MAX_THREADS = 2;

    //-------------------------------------------------------------------------
    private static RT2ScheduledThreadPoolExecutor singleton = null;

    //-------------------------------------------------------------------------
    private ScheduledThreadPoolExecutor threadPoolExecutor = null;

	//-------------------------------------------------------------------------
	private RT2ScheduledThreadPoolExecutor () {
		threadPoolExecutor = new ScheduledThreadPoolExecutor(MAX_THREADS, new ThreadFactoryBuilder().setNameFormat("RT2ACK-%d").build());
	}

    //-------------------------------------------------------------------------
    public static synchronized RT2ScheduledThreadPoolExecutor get () {
        if (singleton == null) {
        	singleton = new RT2ScheduledThreadPoolExecutor ();
        }
        return singleton;
    }

    //-------------------------------------------------------------------------
    public ScheduledThreadPoolExecutor getThreadPool() {
    	return get().threadPoolExecutor;
    }

    //-------------------------------------------------------------------------
    public static void shutdown() {
    	final RT2ScheduledThreadPoolExecutor self = get();
    	final ScheduledThreadPoolExecutor pool = self.threadPoolExecutor;

    	if (!pool.isTerminating() && !pool.isTerminated()) {
    		pool.shutdown();

    		boolean waitSuccessful = false;
    		try {
    			waitSuccessful = pool.awaitTermination(1000, TimeUnit.MILLISECONDS);
    		} catch (InterruptedException e) {
    			Thread.currentThread().interrupt();
    		} finally {
    			if (!waitSuccessful) {
        			pool.shutdownNow();
    			}
    		}
    	}
    }

}
