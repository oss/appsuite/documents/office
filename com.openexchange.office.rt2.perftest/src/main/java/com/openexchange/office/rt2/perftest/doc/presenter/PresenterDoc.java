/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc.presenter;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.DocEvent;
import com.openexchange.office.rt2.perftest.doc.DocUserRegistry;
import com.openexchange.office.rt2.perftest.doc.MessageAndErrorNotifierDoc;
import com.openexchange.office.rt2.perftest.fwk.AsyncResult;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.tools.common.concurrent.SimpleCondition;
import net.as_development.asdk.tools.common.pattern.observation.Observer;

//=============================================================================
public class PresenterDoc extends    MessageAndErrorNotifierDoc
                          implements IPresenterDoc
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(PresenterDoc.class);

    //-------------------------------------------------------------------------
    public enum EPresentationState
    {
        E_NOT_RUNNING,
        E_RUNNING    ,
        E_PAUSED     ;
    }

    //-------------------------------------------------------------------------
    private AtomicReference< EPresentationState > m_ePresentationState = new AtomicReference<>(EPresentationState.E_NOT_RUNNING);

    //-------------------------------------------------------------------------
    private AtomicInteger                         m_nActiveSlide       = new AtomicInteger (0);

    //-------------------------------------------------------------------------
    private AtomicInteger                         m_nActiveUsers       = new AtomicInteger (0);

    //-------------------------------------------------------------------------
    public PresenterDoc ()
        throws Exception
    {
    	super(Doc.DOCTYPE_PRESENTER);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void startRemotePresentation ()
        throws Exception
    {
        if (m_ePresentationState.get() != EPresentationState.E_NOT_RUNNING)
            return;

        final JSONObject             aBody     = impl_createSlideInfoOperation(0);
        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("start_presentation_request", aBody.toString(), aResult);
        wait4Async ("start_presentation", aDeferred, aResult);

        m_ePresentationState.set(EPresentationState.E_RUNNING);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void pauseRemotePresentation ()
        throws Exception
    {
        if (m_ePresentationState.get() != EPresentationState.E_RUNNING)
            return;

        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("pause_presentation_request", null, aResult);
        wait4Async ("pause_presentation", aDeferred, aResult);

        m_ePresentationState.set(EPresentationState.E_PAUSED);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void continueRemotePresentation ()
        throws Exception
    {
        if (m_ePresentationState.get() != EPresentationState.E_PAUSED)
            return;

        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("continue_presentation_request", null, aResult);
        wait4Async ("continue_presentation", aDeferred, aResult);
        
        m_ePresentationState.set(EPresentationState.E_RUNNING);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void endRemotePresentation ()
        throws Exception
    {
        if (m_ePresentationState.get() != EPresentationState.E_RUNNING)
            return;

        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("end_presentation_request", null, aResult);
        wait4Async ("end_presentation", aDeferred, aResult);
        
        m_ePresentationState.set(EPresentationState.E_NOT_RUNNING);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void joinPresentation ()
        throws Exception
    {
        // joining a remote running presentation is not bound to our own internal state machine
        // dont check m_ePresentationState here !

        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("join_presentation_request", null, aResult);
        wait4Async ("join_presentation", aDeferred, aResult);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void leavePresentation ()
        throws Exception
    {
        // leaving a remote running presentation is not bound to our own internal state machine
        // dont check m_ePresentationState here !

        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("leave_presentation_request", null, aResult);
        wait4Async ("leave_presentation", aDeferred, aResult);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void changeSlide (final int nSlide)
        throws Exception
    {
        if (m_ePresentationState.get() != EPresentationState.E_RUNNING)
            return;

        final JSONObject             aBody     = impl_createSlideInfoOperation(nSlide);
        final AsyncResult< Boolean > aResult   = new AsyncResult<> ();
        final Deferred< RT2Message > aDeferred = deferredAppAction ("update_slide_request", aBody.toString(), aResult);
        wait4Async ("update_slide", aDeferred, aResult);

        m_nActiveSlide.set(nSlide);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void trackClientsJoinedPresentation (final int             nClientCountAfterJoin,
                                                                      final SimpleCondition aSync                )
        throws Exception
    {
        final DocUserRegistry aUserRegistry = accessUserRegistry();
        final int             nClients      = aUserRegistry.getCount();

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("wait until all clients joined ...")
            .setVar     ("client-count-after-join", nClientCountAfterJoin)
            .setVar     ("client-count-now"       , nClients)
            .log        ();

        // a) all users already joined ?
        if (nClients >= nClientCountAfterJoin)
        {
            LOG .forLevel   (ELogLevel.E_DEBUG)
                .withMessage("... (a) count of registered doc user = " + nClients + " >= expected clients : " + nClientCountAfterJoin + " -> trigger sync")
                .log        ();
            aSync.set ();
            return;
        }

        // b) wait for joining user
        addObserver(new Observer< DocEvent > ()
        {
            @Override
            public void notify(final DocEvent aEvent)
                throws Exception
            {
                if ( ! aEvent.isEvent(DocEvent.EVENT_ACTIVE_USER_LIST_CHANGED))
                    return;

                final DocUserRegistry aUserRegistryNow = aEvent.getData();
                final int             nClientsNow      = aUserRegistryNow.getCount();

                m_nActiveUsers.set(nClientsNow);

                if (nClientsNow >= nClientCountAfterJoin)
                {
                    LOG .forLevel   (ELogLevel.E_DEBUG)
                        .withMessage("... (b) count of registered doc user = " + nClientsNow + " >= expected clients : " + nClientCountAfterJoin + " -> trigger sync")
                        .log        ();
                    aSync.set ();
                }
            }
        });
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void waitUntilClientsLeftPresentation (final int             nClientCountAfterLeave,
                                                                        final SimpleCondition aSync                 )
        throws Exception
    {
        final DocUserRegistry aUserRegistry = accessUserRegistry();
        final int             nClients      = aUserRegistry.getCount();

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("wait until all clients left ...")
            .setVar     ("client-count-after-left", nClientCountAfterLeave)
            .setVar     ("client-count-now"       , nClients)
            .log        ();

        // a) all users already gone ?
        if (nClients <= nClientCountAfterLeave)
        {
            LOG .forLevel   (ELogLevel.E_DEBUG)
                .withMessage("... (a) count of registered doc user = " + nClients + " <= expected clients : " + nClientCountAfterLeave + " -> trigger sync")
                .log        ();
            aSync.set ();
            return;
        }

        // b) wait for leaving user
        addObserver(new Observer< DocEvent > ()
        {
            @Override
            public void notify(final DocEvent aEvent)
                throws Exception
            {
                if ( ! aEvent.isEvent(DocEvent.EVENT_ACTIVE_USER_LIST_CHANGED))
                    return;

                final DocUserRegistry aUserRegistryNow = aEvent.getData();
                final int             nClientsNow      = aUserRegistryNow.getCount();

                LOG .forLevel   (ELogLevel.E_DEBUG)
                    .withMessage("... got update" )
                    .setVar     ("client-count", nClients)
                    .log        ();

                m_nActiveUsers.set(nClientsNow);

               if (nClientsNow <= nClientCountAfterLeave)
                {
                    LOG .forLevel   (ELogLevel.E_DEBUG)
                        .withMessage("... (b) count of registered doc user = " + nClientsNow + " <= expected clients : " + nClientCountAfterLeave + " -> trigger sync")
                        .log        ();
                    aSync.set ();
                }
            }
        });
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONArray getNextApplyOps ()
        throws Exception
    {
        throw new UnsupportedOperationException ("Presenter is special and do not support applyOps() !");
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONObject getCurrentSelection()
        throws Exception
    {
        throw new UnsupportedOperationException ("Presenter is special and do not support getCurrentSelection() !");
    }

    //-------------------------------------------------------------------------
    public /* no sycnhronized */ int getActiveSlide ()
        throws Exception
    {
        return m_nActiveSlide.get ();
    }

    //-------------------------------------------------------------------------
    public /* no sycnhronized */ int getActiveUsers ()
        throws Exception
    {
        return m_nActiveUsers.get ();
    }

    //-------------------------------------------------------------------------
    @Override
    protected /* no synchronized */ void onDocResponse (final RT2Message aResponse)
        throws Exception
    {
        if (aResponse.isType (RT2Protocol.BROADCAST_UPDATE))
        {
            final JSONObject aBody        = aResponse.getBody();
            final JSONObject aDocStatus   = aBody.getJSONObject("docStatus");
            final int        nActiveSlide = aDocStatus.getInt("activeSlide");

            // Be careful - a broadcast update can be sent for different things this does
            // NOT mean that an slide change has been notified! This must be checked by
            // the client code.
            onSlideChanged (nActiveSlide);
        }
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void onSlideChanged (final int nActiveSlide)
        throws Exception
    {
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... got slide change : " + nActiveSlide)
            .setVar     ("user", getRT2ClientUID())
            .log        ();

        // Only notify EVENT_ACTIVE_SLIDE_CHANGED event in case the active slide
        // has been changed by the presenter
        final int nOldActiveSlide = m_nActiveSlide.get();
        if (nActiveSlide != nOldActiveSlide) {
            m_nActiveSlide.set (nActiveSlide);
            fire(PresenterDocEvent.create(PresenterDocEvent.EVENT_ACTIVE_SLIDE_CHANGED, nActiveSlide));
        }
    }

    //-------------------------------------------------------------------------
    private JSONObject impl_createSlideInfoOperation(final int nSlide)
        throws Exception
    {
        final JSONObject aSlideInfoOp = new JSONObject();
        final JSONObject aActiveSlide = new JSONObject();
        aActiveSlide.put("activeSlide", nSlide      );
        aSlideInfoOp.put("slideInfo"  , aActiveSlide);

    	return aSlideInfoOp;
    }

}
