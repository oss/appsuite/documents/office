/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import org.apache.commons.lang.StringUtils;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.distributed_cache.DistributedCache;
import net.as_development.asdk.distributed_cache.DistributedCacheConfig;
import net.as_development.asdk.distributed_cache.impl.ERunMode;

//=============================================================================
public class MsgStats
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(MsgStats.class);

    //-------------------------------------------------------------------------
    public static final String CFGKEY_DBG_SERVER_HOST = "dbg.server.host";
    public static final String CFGKEY_DBG_SERVER_PORT = "dbg.server.port";
    
    //-------------------------------------------------------------------------
    public static final String LOC_WS_CLIENT_IN  = "ws-client-in" ;
    public static final String LOC_WS_CLIENT_OUT = "ws-client-out";
    public static final String LOC_WS_SERVER_IN  = "ws-server-in" ;
    public static final String LOC_WS_SERVER_OUT = "ws-server-out";

    //-------------------------------------------------------------------------
    private MsgStats ()
    {}

    //-------------------------------------------------------------------------
    public static synchronized MsgStats get ()
    	throws Exception
    {
    	if (m_gSingleton == null)
    		m_gSingleton = new MsgStats ();
    	return m_gSingleton;
    }

    //-------------------------------------------------------------------------
    public synchronized void statMsg (final RT2Message aMsg     ,
    								  final String     sLocation)
    {
    	try
    	{
    		if (impl_isDisabled ())
    			return;
    		
    		final String           sMsgId = RT2MessageGetSet.getMessageID(aMsg);
    		final String           sKey   = "msg."+sMsgId+"."+sLocation;
    		final String           sValue = Long.toString(System.currentTimeMillis());
    		final DistributedCache aCache = mem_Cache ();
    		aCache.set(sKey, sValue);
    	}
    	catch (Throwable ex)
    	{
    		LOG	.forLevel	(ELogLevel.E_ERROR)
    			.withError	(ex)
    			.log 		();
    	}
    }
    
    //-------------------------------------------------------------------------
    private synchronized boolean impl_isDisabled ()
        throws Exception
    {
    	if (m_bIsDisabled != null)
    		return m_bIsDisabled;
    	
    	final TestConfig aCfg          = TestConfig.get ();
    	final String     sDBGServer    = aCfg.getDirectValue (CFGKEY_DBG_SERVER_HOST, String.class, null);
    					 m_bIsDisabled = (
    							 			StringUtils.isEmpty         (sDBGServer            ) ||
    							 			StringUtils.equalsIgnoreCase(sDBGServer, "disabled") ||
    							 			StringUtils.equalsIgnoreCase(sDBGServer, "null"    )
    							 		 );
    	return m_bIsDisabled;
    }
    
    //-------------------------------------------------------------------------
    private synchronized DistributedCache mem_Cache ()
        throws Exception
    {
    	if (m_aCache == null)
    	{
    		final TestConfig             aCfg           = TestConfig.get ();
    		final String                 sDBGServerHost = aCfg.getDirectValue (CFGKEY_DBG_SERVER_HOST, String.class, null);
    		final String                 sDBGServerPort = aCfg.getDirectValue (CFGKEY_DBG_SERVER_PORT, String.class, null);
    		final DistributedCache       aCache         = new DistributedCache ();
    		final DistributedCacheConfig aCacheCfg      = aCache.configure();
    		
    		aCacheCfg.setAddress(sDBGServerHost                  );
    		aCacheCfg.setPort   (Integer.parseInt(sDBGServerPort));
    		aCacheCfg.setRunMode(ERunMode.E_CLIENT               );

    		aCache.connect  ();
    		m_aCache = aCache;
    	}
    	return m_aCache;
    }

    //-------------------------------------------------------------------------
    private static MsgStats m_gSingleton = null;

    //-------------------------------------------------------------------------
    private DistributedCache m_aCache = null;

    //-------------------------------------------------------------------------
    private Boolean m_bIsDisabled = null;
}
