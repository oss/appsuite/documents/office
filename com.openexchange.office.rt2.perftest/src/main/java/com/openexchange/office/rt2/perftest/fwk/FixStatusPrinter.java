/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;

//=============================================================================
public class FixStatusPrinter
{
    //-------------------------------------------------------------------------
    public FixStatusPrinter ()
    	throws Exception
    {}

    //-------------------------------------------------------------------------
    public void setLineCount (final int nCount)
    	throws Exception
    {
    	m_nLineCount = nCount;
    }
    
    //-------------------------------------------------------------------------
    public void print (final String... lLines)
    	throws Exception
    {
    	final int nLines = lLines.length;
    	Validate.isTrue(nLines == m_nLineCount, "Given set of status lines do not match configured line count ! Please initialize printer by calling setLineCount() right.");
    	
    	if ( ! m_bIsFirst)
    		impl_resetLine ();
    	else
    		m_bIsFirst = false;

    	for (final String sLine : lLines)
    	{
    		final String sNormLine = StringUtils.removeEnd(sLine, "\n");
    		System.out.println (sNormLine);
    	}
    }

    //-------------------------------------------------------------------------
    private void impl_resetLine ()
    	throws Exception
    {
    	for (int i=0; i<m_nLineCount; ++i)
    		System.out.print ("\33[1A\33[2K");
    }

    //-------------------------------------------------------------------------
    private int m_nLineCount = 1;

    //-------------------------------------------------------------------------
    private boolean m_bIsFirst = true;
}
