/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.rt2.perftest.config.items.LoadTestConfig;
import com.openexchange.office.rt2.perftest.doc.utils.JSONHelper;
import com.openexchange.office.rt2.perftest.fwk.AsyncResult;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.perftest.fwk.IEventHandler;
import com.openexchange.office.rt2.perftest.fwk.JoinResultData;
import com.openexchange.office.rt2.perftest.fwk.NewDocumentData;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorClient;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorConst;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.operations.OSNGenerator;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import org.junit.Assert;
import net.as_development.asdk.tools.common.CollectionUtils;
import net.as_development.asdk.tools.common.pattern.observation.ObservableBase;

//=============================================================================
public abstract class Doc extends ObservableBase<DocEvent> implements IDoc, OSNGenerator {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(Doc.class);

    public static final String ERROR_NO_ERROR = "NO_ERROR";

    //-------------------------------------------------------------------------
    public static final String DOCTYPE_TEXT         = "text";
    public static final String DOCTYPE_SPREADSHEET  = "spreadsheet";
    public static final String DOCTYPE_PRESENTATION = "presentation";
    public static final String DOCTYPE_PRESENTER    = "presenter";

    //-------------------------------------------------------------------------
    private final Object m_aSync = new Object();
    private String m_sDocType = null;
    private Mutable<EDocumentState> m_eState = null;
    private RT2 m_aDocImpl = null;
    private long m_nTimeoutMS4Open = LoadTestConfig.DEFAULT_TIMEOUT_4_OPEN;
    private long m_nTimeoutMS4Apply = LoadTestConfig.DEFAULT_TIMEOUT_4_APPLY;
    private long m_nTimeoutMS4Close = LoadTestConfig.DEFAULT_TIMEOUT_4_CLOSE;
    private int m_nPreviewVersion = 1;
    private boolean m_bAutoCloseOnError = LoadTestConfig.DEFAULT_AUTO_CLOSE_ON_ERROR;
    private boolean m_bContinueOnTimeout = LoadTestConfig.DEFAULT_CONTINUE_ON_TIMEOUT;
    private AtomicBoolean m_bProvideDocEvents = new AtomicBoolean(false);
    private AtomicBoolean m_bRecordOperations = new AtomicBoolean(false);
    private AtomicBoolean m_bRecordAppliedOperations = new AtomicBoolean(false);
    private AtomicInteger m_aClientOSN = new AtomicInteger(0);
    private AtomicInteger m_aServerOSN = new AtomicInteger(0);
    private AtomicInteger m_lastAppliedOSN = new AtomicInteger(0);
    private String m_sRestoreID = null;
    private List<JSONObject> m_lRecordedOperations = null;
    private boolean m_bPreviewOpsOnOpen = false;
    private JSONArray m_aRecordedAppliedOperations = null;
    private StatusLine m_aStatusLine = null;
    private IEventHandler m_iEventHandler = null;
    private NewDocumentData m_aNewDocumentData = null;
    private JoinResultData m_joinResultData = null;
    private List<JSONObject> m_aActiveUsersList = null;
    private DocUserRegistry m_aUserRegistry = null;
    private String m_sInstanceUID = UUID.randomUUID().toString();
    private AtomicReference<String> m_aEditorClientUID = new AtomicReference<>("");

    //-------------------------------------------------------------------------
    public Doc(final String sType) throws Exception {
        m_sDocType = sType;
    }

    //-------------------------------------------------------------------------
    public String getType() {
        return m_sDocType;
    }

    //-------------------------------------------------------------------------
    public String getInstanceUID() {
        return m_sInstanceUID;
    }

    //-------------------------------------------------------------------------
    public synchronized void bind(final StatusLine aStatusLine) throws Exception {
        m_aStatusLine = aStatusLine;
    }

    //-------------------------------------------------------------------------
    public synchronized void bind(final RT2 aDocImpl) throws Exception {
        m_aDocImpl = aDocImpl;
        m_aDocImpl.setDocType(m_sDocType);
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Binding event handler").log();
        m_aDocImpl.setEventHandler(mem_EventHandler());
    }

    //-------------------------------------------------------------------------
    public synchronized OXAppsuite getContext() {
        return m_aDocImpl.getContext();
    }

    //-------------------------------------------------------------------------
    public synchronized void setTimeouts(final long nMS4Open, final long nMS4Apply, final long nMS4Close, final boolean bContinueOnTimeout, final boolean bAutoCloseOnError) throws Exception {
        m_nTimeoutMS4Open = nMS4Open;
        m_nTimeoutMS4Apply = nMS4Apply;
        m_nTimeoutMS4Close = nMS4Close;
        m_bContinueOnTimeout = bContinueOnTimeout;
        m_bAutoCloseOnError = bAutoCloseOnError;
    }

    //-------------------------------------------------------------------------
    public void enableOperationsRecording(final boolean bEnabled) throws Exception {
        m_bRecordOperations.set(bEnabled);
    }

    //-------------------------------------------------------------------------
    public void enableAppliedOperationsRecording(final boolean bEnabled) {
        m_bRecordAppliedOperations.set(bEnabled);

        synchronized (this) {
            if (bEnabled) {
                m_aRecordedAppliedOperations = new JSONArray();
            }
        }
    }

    //-------------------------------------------------------------------------
    public void enableDocEvents(final boolean bEnabled) throws Exception {
        m_bProvideDocEvents.set(bEnabled);
    }

    //-------------------------------------------------------------------------
    public boolean isOperationsRecordingEnabled() throws Exception {
        return m_bRecordOperations.get();
    }

    //-------------------------------------------------------------------------
    public boolean isAppliedOperationsRecordingEnabled() {
        return m_bRecordAppliedOperations.get();
    }

    //-------------------------------------------------------------------------
    public boolean areDocEventsEnabled() throws Exception {
        return m_bProvideDocEvents.get();
    }

    //-------------------------------------------------------------------------
    public synchronized RT2 accessRT2() throws Exception {
        return mem_DocImpl();
    }

    //-------------------------------------------------------------------------
    public synchronized EDocumentState getDocState() {
        return m_eState.getValue();
    }

    //-------------------------------------------------------------------------
    public boolean werePreviewOpsReceivedOnOpen() {
        return m_bPreviewOpsOnOpen;
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ NewDocumentData createNew() throws Exception {
        m_aNewDocumentData = mem_DocImpl().createFileNew();
        return m_aNewDocumentData;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void clearNewDocumentData() throws Exception {
        m_aNewDocumentData = null;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getEditorUID() throws Exception {
        return m_aEditorClientUID.get();
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void removeDocFile() throws Exception {
        mem_DocImpl().removeFile();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void setFolderId(final String sFolderID) throws Exception {
        mem_DocImpl().setFolderId(sFolderID);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getFolderId() throws Exception {
        return mem_DocImpl().getFolderId();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void setFileId(final String sFileID) throws Exception {
        mem_DocImpl().setFileId(sFileID);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getFileId() throws Exception {
        return mem_DocImpl().getFileId();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void setDriveDocId(final String sDriveDocID) throws Exception {
        mem_DocImpl().setDriveDocId(sDriveDocID);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getDriveDocId() throws Exception {
        return mem_DocImpl().getDriveDocId();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getRT2ClientUID() throws Exception {
        return mem_DocImpl().getRT2ClientUID();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getDocUID() throws Exception {
        return mem_DocImpl().getDocUID();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getNodeID() throws Exception {
        return mem_DocImpl().getNodeID();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Integer getOSN() throws Exception {
        return getActOSN();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ int getServerOSN() throws Exception {
        return m_aServerOSN.get();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ int getLastAppliedOSN() throws Exception {
        return m_lastAppliedOSN.get();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ List<JSONObject> getRecordedOperations() throws Exception {
        final List<JSONObject> lOps = new ArrayList<>();
        CollectionUtils.copy(mem_RecordedOperations(), lOps);
        return lOps;
    }

    //-------------------------------------------------------------------------
    public synchronized JSONArray getRecordedAppliedOperations() throws Exception {
        return JSONHelper.shallowCopy(m_aRecordedAppliedOperations);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ DocUserRegistry accessUserRegistry() throws Exception {
        return mem_UserRegistry();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ NewDocumentData getNewDocumentData() throws Exception {
        return m_aNewDocumentData;
    }

    //-------------------------------------------------------------------------
    public JoinResultData getJoinResultData() {
        return this.m_joinResultData;
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void open() throws Exception {
        open(null);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ int applyOps() throws Exception {
        final JSONArray lOps = getNextApplyOps();
        return applyOps(lOps);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ int applyOps(final JSONArray lOps) throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Apply;
        }

        final int nCurrOSN = getActOSN();
        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        final Deferred<RT2Message> aDeferred = applyOpsAsync(aDocImpl, lOps, nCurrOSN, aResult);

        wait4Async("apply ops", aDeferred, aResult, nTimeout);

        final Boolean asyncResult = aResult.get();

        if ((null == asyncResult) || !asyncResult.booleanValue()) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return nCurrOSN;
        }

        return nCurrOSN;
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ Deferred<RT2Message> applyOpsAsync() throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        final JSONArray lOps = getNextApplyOps();
        final int nCurrOSN = getActOSN();
        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        return applyOpsAsync(aDocImpl, lOps, nCurrOSN, aResult);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void editRights(final String command, String value) throws Exception {
        long nTimeout = 0;
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Apply;
        }

        final int nCurrOSN = getActOSN();
        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        final Deferred<RT2Message> aDeferred = editRightsAsync(aDocImpl, command, nCurrOSN, value, aResult);

        wait4Async("editrights " + command, aDeferred, aResult, nTimeout);

        if (aResult.get() == false) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> editRightsAsync(String command, String value) throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        final int nCurrOSN = getActOSN();
        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        return editRightsAsync(aDocImpl, command, nCurrOSN, value, aResult);
    }

    //-------------------------------------------------------------------------
    @Override
    public void updateUserData() throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Apply;
        }

        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        final Deferred<RT2Message> aDeferred = updateUserDataAsync(aDocImpl, aResult);
        wait4Async("updateuserdata", aDeferred, aResult, nTimeout);

        if (!aResult.get()) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public void addImageID(JSONObject aAddedImageData) throws Exception {
        long nTimeout = 0;
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Apply;
        }

        final AsyncResult<Boolean> aResult = new AsyncResult<>();
        final Deferred<RT2Message> aDeferred = addImageIdAsync(aDocImpl, aAddedImageData, aResult);

        wait4Async("addimageid ", aDeferred, aResult, nTimeout);

        if (aResult.get() == false) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }
    }

    //-------------------------------------------------------------------------
    public boolean sendClose() throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Close;
        }

        final AsyncResult<Boolean> aCloseResult = new AsyncResult<>(Boolean.FALSE);
        final Deferred<RT2Message> aCloseDeferred = closeAsync(aDocImpl, aCloseResult);
        wait4Async("close", aCloseDeferred, aCloseResult, nTimeout);

        return aCloseResult.get();
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void close() throws Exception {
        close(false);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void close(boolean bForceQuit) throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Close;
        }

        final AsyncResult<Boolean> aCloseResult = new AsyncResult<>(Boolean.FALSE);
        final Deferred<RT2Message> aCloseDeferred = closeAsync(aDocImpl, aCloseResult);
        wait4Async("close", aCloseDeferred, aCloseResult, nTimeout);

        if (!aCloseResult.get()) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            boolean bAutoClosed = impl_doAutoCloseOnErrorIfConfigured();
            if (!bAutoClosed && bForceQuit)
                impl_doLeave(bForceQuit);
        } else {
            impl_doLeave(bForceQuit);
        }

        aDocImpl.free();
        mem_StatusLine().countDocClose();
    }

    //-------------------------------------------------------------------------
    public void saveDoc() throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Close;
        }

        final AsyncResult<Boolean> saveResult = new AsyncResult<>(Boolean.FALSE);
        final Deferred<RT2Message> aSaveDocDeferred = saveDocAsync(aDocImpl, saveResult);
        wait4Async("saveDoc", aSaveDocDeferred, saveResult, nTimeout);

        if (!saveResult.get()) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_doLeave(boolean bForceQuit) throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Close;
        }

        final AsyncResult<Boolean> aLeaveResult = new AsyncResult<>();
        final Deferred<RT2Message> aLeaveDeferred = leaveAsync(aDocImpl, bForceQuit, aLeaveResult);
        wait4Async("leave", aLeaveDeferred, aLeaveResult, nTimeout);

        if (!aLeaveResult.get()) {
            // make no sense to trigger leave again after leave request failed ... ;-)
        }

        if (!impl_switchDocState(EDocumentState.E_LEFT, EDocumentState.E_CLOSED)) {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("INCONSISTENT STATE").setVar("instance", getInstanceUID()).setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_LEFT).setVar("new-stated", EDocumentState.E_CLOSED).log();

            if (bForceQuit) {
                // make sure that we switch to our final state in case of a
                // force quit - we know that we have trouble
                impl_forceSwitchDocState(EDocumentState.E_CLOSED);
            }
        }
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void simpleLeave(boolean bForceQuit) throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Close;
        }

        final AsyncResult<Boolean> aLeaveResult = new AsyncResult<>();
        final Deferred<RT2Message> aLeaveDeferred = leaveAsync(aDocImpl, bForceQuit, aLeaveResult);
        wait4Async("leave", aLeaveDeferred, aLeaveResult, nTimeout);

        // it makes no sense to trigger leave again or send other requests

        if (!impl_switchDocState(EDocumentState.E_LEFT, EDocumentState.E_CLOSED)) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("INCONSISTENT STATE").setVar("instance", getInstanceUID()).setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_LEFT).setVar("new-stated", EDocumentState.E_CLOSED).log();
        }

        aDocImpl.free();
        mem_StatusLine().countDocClose();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void setClosedState() throws Exception {
        impl_forceSwitchDocState(EDocumentState.E_CLOSED);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void createPreview() throws Exception {
        RT2 aDocImpl = null;
        int nPreviewVersion = 0;
        synchronized (this) {
            m_nPreviewVersion += 1;
            nPreviewVersion = m_nPreviewVersion;
            aDocImpl = m_aDocImpl;
        }

        aDocImpl.createPreviewNew(nPreviewVersion);
    }

    //-------------------------------------------------------------------------
    @Override
    public Deferred<RT2Message> syncAsync() throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        return aDocImpl.sync(this.getOSN());
    }

    //-------------------------------------------------------------------------
    @Override
    public Deferred<RT2Message> syncAsync(int nOpsAppliedOSN) throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        return aDocImpl.sync(nOpsAppliedOSN);
    }

    //-------------------------------------------------------------------------
    public Deferred<RT2Message> closeAsync() throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        final AsyncResult<Boolean> aCloseResult = new AsyncResult<>(Boolean.FALSE);
        return impl_deferredClose(aDocImpl, aCloseResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> leaveAsync(final boolean bForceQuit) throws Exception {
        RT2 aDocImpl = null;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
        }

        final AsyncResult<Boolean> aResult = new AsyncResult<>(Boolean.FALSE);
        return impl_deferredLeave(aDocImpl, bForceQuit, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> joinAsync(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredJoin(aDoc, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> openAsync(final RT2 aDoc, final JSONObject aOpenData, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredOpen(aDoc, aOpenData, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> applyOpsAsync(final RT2 aDoc, final JSONArray lOps, final int nCurrOSN, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredApplyOps(aDoc, lOps, nCurrOSN, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> updateUserDataAsync(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        final JSONObject aUserData = getCurrentSelection();
        return impl_deferredUpdateUserData(aDoc, aUserData, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> editRightsAsync(final RT2 aDoc, final String sCommand, int nCurrentOSN, final String sValue, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredEditRights(aDoc, sCommand, nCurrentOSN, sValue, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> addImageIdAsync(final RT2 aDoc, final JSONObject aAddedImageData, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredAddImageID(aDoc, aAddedImageData, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> closeAsync(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredClose(aDoc, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> leaveAsync(final RT2 aDoc, final boolean bForceQuit, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredLeave(aDoc, bForceQuit, aResult);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> syncAsyn(final RT2 aDoc, int nCurrentOSN, final AsyncResult<Boolean> result) throws Exception {
        return aDoc.sync(nCurrentOSN);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> saveDocAsync(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        return impl_deferredSaveDoc(aDoc, aResult);
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void open(final JSONObject aOpenData) throws Exception {
        RT2 aDocImpl = null;
        long nTimeout = 0;

        synchronized (this) {
            aDocImpl = m_aDocImpl;
            nTimeout = m_nTimeoutMS4Open;
        }

        final AsyncResult<Boolean> aJoinResult = new AsyncResult<>();
        final Deferred<RT2Message> aJoinDeferred = joinAsync(aDocImpl, aJoinResult);
        wait4Async("join", aJoinDeferred, aJoinResult, nTimeout);

        if ((aJoinResult.get() == null) || (!aJoinResult.get())) {
            // dont count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }

        final AsyncResult<Boolean> aOpenResult = new AsyncResult<>();
        final Deferred<RT2Message> aOpenDeferred = openAsync(aDocImpl, aOpenData, aOpenResult);
        wait4Async("open", aOpenDeferred, aOpenResult, nTimeout);

        if ((aOpenResult.get() == null) || !aOpenResult.get()) {
            // don't count error on status line : it's done within helper method impl_join4Async () already !
            impl_doAutoCloseOnErrorIfConfigured();
            return;
        }

        mem_StatusLine().countDocOpen();
    }

    //-------------------------------------------------------------------------
    protected abstract /* no synchronized */ JSONArray getNextApplyOps() throws Exception;

    //-------------------------------------------------------------------------
    protected abstract /* no synchronized */ JSONObject getCurrentSelection() throws Exception;

    //-------------------------------------------------------------------------
    public /* no synchronized */ void checkLBRouting(final OXSession aSession) throws Exception {
        final String sHTTPRoute = aSession.getLoadBalanceRoute();
        String sWSRoute = null;

        synchronized (this) {
            sWSRoute = m_aDocImpl.getNodeID();
        }

        if (StringUtils.equals(sHTTPRoute, sWSRoute)) {
            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("load-balance-route-check").setVar("instance", getInstanceUID()).setVar("http-route", sHTTPRoute).setVar("ws-route", sWSRoute).log();
        } else {
            if (!StringUtils.isEmpty(sWSRoute)) {
                LOG.forLevel(ELogLevel.E_WARNING).withMessage("load-balance-route-check : routes are different").setVar("instance", getInstanceUID()).setVar("http-route", sHTTPRoute).setVar("ws-route", sWSRoute).log();
            }
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void fire(final DocEvent aEvent) throws Exception {
        if (m_bProvideDocEvents.get() == false)
            return;

        super.fire(aEvent);
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ JSONArray createOperationsArray() throws Exception {
        return new JSONArray();
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void updateOSN(final RT2Message aResponse) throws Exception {
        Integer nOSN = null;
        final JSONObject aBody = aResponse.getBody();

        if (aBody.has(MessageProperties.PROP_SERVEROSN))
            nOSN = aBody.getInt(MessageProperties.PROP_SERVEROSN);
        else {
            final JSONObject aDocStatus = aBody.optJSONObject(MessageProperties.PROP_DOCSTATUS);
            if (aDocStatus != null)
                nOSN = aDocStatus.optInt(MessageProperties.PROP_SERVEROSN);
        }

        if (nOSN == null) {
            // as long s we dont know if this is an error we shouldn't print out that message ...
            //            LOG .forLevel   (ELogLevel.E_WARNING)
            //                .withMessage("Miss OSN within response !")
            //                .log        ();
            return;
        }

        int nOldOSN = this.getServerOSN();
        boolean bOSNChanged = (nOldOSN != nOSN);

        m_lastAppliedOSN.set(nOSN);
        m_aServerOSN.set(nOSN);

        if (aResponse.isType(RT2Protocol.RESPONSE_OPEN_DOC)) {
            // The server defines the starting OSN with the response open doc.
            // The server uses the next expected OSN, therefore use here - 1
            m_aClientOSN.set(nOSN - 1);
        }

        if (bOSNChanged)
            fire(DocEvent.create(DocEvent.EVENT_OSN_CHANGED, nOSN));
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void updateEditor(final RT2Message aResponse) throws Exception {
        final JSONObject aBody = aResponse.getBody();
        final JSONObject aDocStatus = aBody.optJSONObject(MessageProperties.PROP_DOCSTATUS);

        if (aDocStatus != null) {
            LOG.forLevel(ELogLevel.E_TRACE).withMessage("updateEditor").log();
            final String sEditorUID = aDocStatus.optString(MessageProperties.PROP_EDITUSERID);
            if (null != sEditorUID) {
                LOG.forLevel(ELogLevel.E_TRACE).withMessage("updateEditor, editoruid: " + sEditorUID).log();
                if (!sEditorUID.equals(m_aEditorClientUID.get())) {
                    m_aEditorClientUID.set(sEditorUID);
                    fire(DocEvent.create(DocEvent.EVENT_EDITOR_CHANGED, sEditorUID));
                }
            } else {
                LOG.forLevel(ELogLevel.E_ERROR).withMessage("document status found, but without editor id").setVar("instance", getInstanceUID()).setVar("broadcast", aBody).log();
            }
        } else {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("document status not found").setVar("instance", getInstanceUID()).setVar("broadcast", aBody).log();
        }
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void updateRestoreID(final RT2Message aResponse) throws Exception {
        final JSONObject aBody = aResponse.getBody();
        final JSONObject aSyncInfo = aBody.optJSONObject(MessageProperties.PROP_SYNCINFO);

        if (aSyncInfo != null) {
            final String sRestoreID = aSyncInfo.getString(MessageProperties.PROP_RESTOREID);
            if (StringUtils.isNotEmpty(sRestoreID))
                m_sRestoreID = sRestoreID;
        }
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getRestoreID() throws Exception {
        return m_sRestoreID;
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ int getActOSN() throws Exception {
        final int nOSN = m_aClientOSN.get();
        return nOSN;
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void startOpsChunk() {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ int getNextOSN() {
        return m_aServerOSN.get();
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void endOpsChunk() {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    private synchronized boolean impl_switchDocState(final EDocumentState eExpectedState, final EDocumentState aNewState) throws Exception {
        final Mutable<EDocumentState> rState = mem_State();
        final EDocumentState eActState = rState.getValue();

        if (eActState.equals(EDocumentState.E_IN_ERROR))
            return true; // !? not sure if it's a good idea ... but we try to e.g. close the doc on remote side in case of an error ...

        if (eActState != eExpectedState)
            return false;

        rState.setValue(aNewState);
        return true;
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_forceSwitchDocState(final EDocumentState aNewState) throws Exception {
        final Mutable<EDocumentState> rState = mem_State();
        final EDocumentState eActState = rState.getValue();

        LOG.forLevel(ELogLevel.E_INFO).withMessage("force document state").setVar("instance", getInstanceUID()).setVar("oldState", eActState).setVar("newState", aNewState).log();

        rState.setValue(aNewState);
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ Deferred<RT2Message> deferredAppAction(final String sAppAction, final String sBody, final AsyncResult<Boolean> aResult) throws Exception {
        final Map<String, Object> lHeader = new HashMap<>();
        lHeader.put(RT2Protocol.HEADER_APP_ACTION, sAppAction);

        return deferredRequest(RT2Protocol.REQUEST_APP_ACTION, lHeader, sBody, aResult);
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ Deferred<RT2Message> deferredRequest(final String sRequest, final Map<String, Object> lHeader, final String sBody, final AsyncResult<Boolean> aResult) throws Exception {
        final Deferred<RT2Message> aDeferred = mem_DocImpl().request(sRequest, lHeader, sBody);

        LOG.forLevel(ELogLevel.E_INFO).withMessage(sRequest + " : START").setVar("instance", getInstanceUID()).log();

        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage(sRequest + " : DONE").setVar("instance", getInstanceUID()).setVar("response", aResponse).log();
                aResult.set(true);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage(sRequest + " : FAIL").setVar("instance", getInstanceUID()).setVar("response", aResponse).log();
                    aResult.set(false);
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredJoin(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("JOIN : START").setVar("instance", getInstanceUID()).log();

        impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_JOIN_OUT_CLIENT, null);

        if (!impl_switchDocState(EDocumentState.E_CLOSED, EDocumentState.E_IN_JOIN)) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("JOIN : INCONSISTENT").setVar("instance", getInstanceUID()).setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_CLOSED).setVar("new-stated", EDocumentState.E_IN_JOIN).log();
            throw new RuntimeException("inconsistent state detected for com.openexchange.rt2.client.uid " + this.getRT2ClientUID());
        }

        final Deferred<RT2Message> aDeferred = aDoc.join();

        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                final String sMsgType = aResponse.getType();
                if (!StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_JOIN)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("JOIN : UNEXPECTED").setVar("instance", getInstanceUID()).setVar("response", aResponse).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_JOIN_IN_CLIENT, "deferred:unexpected-response");
                    return;
                }

                if (!impl_switchDocState(EDocumentState.E_IN_JOIN, EDocumentState.E_JOINED)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("JOIN : INCONSISTENT").setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_JOIN_IN_CLIENT, "deferred:inconsistent-state");
                    return;
                }

                m_joinResultData = new JoinResultData(aResponse.getBody());

                LOG.forLevel(ELogLevel.E_INFO).withMessage("JOIN : DONE").setVar("instance", getInstanceUID()).setVar("response", aResponse).log();
                // checking for error codes is done within RT2.onMessage() method already ...
                // and fail() handler of this deferred is called then !
                aResult.set(true);
                impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_JOIN_IN_CLIENT, null);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("JOIN : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_JOIN_IN_CLIENT, "deferred:fail");
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredOpen(final RT2 aDoc, final JSONObject aOpenData, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("OPEN : START").setVar("instance", getInstanceUID()).log();

        impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_OPEN_OUT_CLIENT, null);

        if (!impl_switchDocState(EDocumentState.E_JOINED, EDocumentState.E_IN_OPEN)) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("OPEN : INCONSISTENT").setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_JOINED).setVar("new-stated", EDocumentState.E_IN_OPEN).setVar("instance", getInstanceUID()).log();
            throw new RuntimeException("inconsistent state detected for com.openexchange.rt2.client.uid " + this.getRT2ClientUID());
        }

        final Deferred<RT2Message> aDeferred = aDoc.open(aOpenData);

        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {

                final String sMsgType = aResponse.getType();
                if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_OPEN_DOC_CHUNK)) {
                    LOG.forLevel(ELogLevel.E_INFO).withMessage("OPEN : IN PROGRESS").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                    updateRestoreID(aResponse);
                } else if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_OPEN_DOC)) {
                    LOG.forLevel(ELogLevel.E_INFO).withMessage("OPEN : DONE").setVar("instance", getInstanceUID()).setVar("response", aResponse).log();

                    if (!impl_switchDocState(EDocumentState.E_IN_OPEN, EDocumentState.E_OPEN)) {
                        LOG.forLevel(ELogLevel.E_ERROR).withMessage("OPEN : INCONSISTENT").setVar("instance", getInstanceUID()).log();

                        aDeferred.close();
                        aResult.set(false);
                        impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_OPEN_IN_CLIENT, "deferred:inconsistent-state");
                        return;
                    }

                    // the final open response provides an initial state for both
                    // document and clients state
                    updateOSN(aResponse);
                    updateEditor(aResponse);
                    updateActiveUserList(aResponse);

                    aDeferred.close();
                    aResult.set(true);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_OPEN_IN_CLIENT, null);
                } else {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("OPEN : UNEXPECTED").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                    aDeferred.close();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_OPEN_IN_CLIENT, "deferred:unexpected");
                }
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("OPEN : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                    aDeferred.close();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_OPEN_IN_CLIENT, "deferred:fail");
                }
            });
        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredClose(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("CLOSE : START").setVar("instance", getInstanceUID()).log();

        impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_CLOSE_OUT_CLIENT, null);

        if (!impl_switchDocState(EDocumentState.E_OPEN, EDocumentState.E_IN_CLOSE)) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("CLOSE : INCONSISTENT").setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_OPEN).setVar("new-stated", EDocumentState.E_IN_CLOSE).setVar("instance", getInstanceUID()).log();
            throw new RuntimeException("inconsistent state detected for com.openexchange.rt2.client.uid " + this.getRT2ClientUID());
        }

        final Deferred<RT2Message> aDeferred = aDoc.close();

        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                final String sMsgType = aResponse.getType();
                if (!StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_CLOSE_DOC)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("CLOSE : UNEXPECTED").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_CLOSE_IN_CLIENT, "deferred:unexpected-state");
                    return;
                }

                if (!impl_switchDocState(EDocumentState.E_IN_CLOSE, EDocumentState.E_CLOSED)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("OPEN : INCONSISTENT").setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_CLOSE_IN_CLIENT, "deferred:inconsistent-state");
                    return;
                }

                LOG.forLevel(ELogLevel.E_INFO).withMessage("CLOSE : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                // checking for error codes is done within RT2.onMessage() method already ...
                // and fail() handler of this deferred is called then !
                aResult.set(true);
                impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_CLOSE_IN_CLIENT, null);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("CLOSE : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_CLOSE_IN_CLIENT, "deferred:fail");
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredLeave(final RT2 aDoc, final boolean bForceQuit, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("LEAVE : START").setVar("instance", getInstanceUID()).log();

        impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_LEAVE_OUT_CLIENT, null);

        // Don't check switchDocState in case we use a force quit. ForceQuit is
        // used by clients in emergency cases where it needs to release server-side
        // resources (due to error handling, etc.). ForceQuit SHOULD NEVER be used
        // under normal circumstances.
        if (!impl_switchDocState(EDocumentState.E_CLOSED, EDocumentState.E_IN_LEAVE) && !bForceQuit) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("LEAVE : INCONSISTENT").setVar("current-stated", mem_State().getValue()).setVar("expected-stated", EDocumentState.E_CLOSED).setVar("new-stated", EDocumentState.E_IN_LEAVE).setVar("instance", getInstanceUID()).log();
            throw new RuntimeException("inconsistent state detected for com.openexchange.rt2.client.uid " + this.getRT2ClientUID());
        }

        final Deferred<RT2Message> aDeferred = aDoc.leave(bForceQuit);

        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                final String sMsgType = aResponse.getType();
                if (!StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_LEAVE)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("LEAVE : UNEXPECTED").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_LEAVE_OUT_CLIENT, "deferred:unexpected-state");
                    return;
                }

                if (!impl_switchDocState(EDocumentState.E_IN_LEAVE, EDocumentState.E_LEFT)) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("LEAVE : INCONSISTENT").setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_LEAVE_OUT_CLIENT, "deferred:inconsistent-state");
                    return;
                }

                LOG.forLevel(ELogLevel.E_INFO).withMessage("LEAVE : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                // checking for error codes is done within RT2.onMessage() method already ...
                // and fail() handler of this deferred is called then !
                aResult.set(true);
                impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_LEAVE_OUT_CLIENT, null);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("LEAVE : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                    impl_monitorDocTask(aDoc, RT2MonitorConst.KEY_DOC_LEAVE_OUT_CLIENT, "deferred:fail");
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredApplyOps(final RT2 aDoc, final JSONArray lOps, final int nCurrOSN, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("APPLY : START").setVar("instance", getInstanceUID()).log();

        final Deferred<RT2Message> aDeferred = aDoc.applyOps(lOps);
        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage("APPLY : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                final String sMsgType = aResponse.getType();
                if ((StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_APPLY_OPS)) || (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_SAVE_DOC))) {
                    synchronized (this) {
                        if (m_aRecordedAppliedOperations != null) {
                            JSONHelper.appendArray(m_aRecordedAppliedOperations, lOps);
                        }
                    }

                    updateOSN(aResponse);
                    aResult.set(true);
                } else
                    aResult.set(false);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("APPLY : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredUpdateUserData(final RT2 aDoc, final JSONObject aUserData, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("UPDATEUSERDATA: START").setVar("instance", getInstanceUID()).log();

        final Deferred<RT2Message> aDeferred = aDoc.updateUserData(aUserData);
        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage("UPDATEUSERDATA : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                final String sMsgType = aResponse.getType();
                if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_APP_ACTION))
                    aResult.set(true);
                else
                    aResult.set(false);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("UPDATEUSERDATA : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredEditRights(final RT2 aDoc, final String sCommand, final int nCurrentOSN, final String sValue, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("EDITRIGHTS: START").setVar("instance", getInstanceUID()).setVar("command", sCommand).setVar("osn", nCurrentOSN).setVar("value", sValue).log();

        final Deferred<RT2Message> aDeferred = aDoc.editRights(sCommand, nCurrentOSN, sValue);
        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage("EDITRIGHTS : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                final String sMsgType = aResponse.getType();
                if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_EDITRIGHTS))
                    aResult.set(true);
                else
                    aResult.set(false);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("EDITRIGHTS : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredAddImageID(final RT2 aDoc, final JSONObject aAddedImageData, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("ADD_IMAGE_ID: START").setVar("instance", getInstanceUID()).log();

        final Deferred<RT2Message> aDeferred = aDoc.addImageId(aAddedImageData);
        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage("ADD_IMAGE_ID : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                final String sMsgType = aResponse.getType();
                if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_APP_ACTION))
                    aResult.set(true);
                else
                    aResult.set(false);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("ADD_IMAGE_ID : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_deferredSaveDoc(final RT2 aDoc, final AsyncResult<Boolean> aResult) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("SAVEDOC: START").setVar("instance", getInstanceUID()).log();

        final Deferred<RT2Message> aDeferred = aDoc.saveDoc();
        aDeferred.done(new Deferred.Func<RT2Message>() {

            @Override
            public void done(final RT2Message aResponse) throws Exception {
                LOG.forLevel(ELogLevel.E_INFO).withMessage("SAVEDOC : DONE").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();

                final String sMsgType = aResponse.getType();
                if (StringUtils.equals(sMsgType, RT2Protocol.RESPONSE_SAVE_DOC))
                    aResult.set(true);
                else
                    aResult.set(false);
            }
        })

            .fail(new Deferred.Func<RT2Message>() {

                @Override
                public void fail(final RT2Message aResponse) throws Exception {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("SAVEDOC : FAIL").setVar("response", aResponse).setVar("instance", getInstanceUID()).log();
                    aResult.set(false);
                }
            })

            .always(new Deferred.Func<RT2Message>() {

                @Override
                public void always(final RT2Message aResponse) throws Exception {
                    aDeferred.close();
                }
            });

        return aDeferred;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void wait4Async(final String sTask, final Deferred<RT2Message> aDeferred, final AsyncResult<Boolean> aResult) throws Exception {
        long nTimeoutInMS = 0;
        synchronized (this) {
            nTimeoutInMS = m_nTimeoutMS4Apply;
        }

        wait4Async(sTask, aDeferred, aResult, nTimeoutInMS);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void wait4Async(final String sTask, final Deferred<RT2Message> aDeferred, final AsyncResult<Boolean> aResult, final long nTimeoutMS) throws Exception {
        RT2 aDocImpl = null;
        StatusLine aStatusLine = mem_StatusLine();
        boolean bContinueOnTimeout = false;
        String sDocType = null;
        synchronized (this) {
            aDocImpl = m_aDocImpl;
            bContinueOnTimeout = m_bContinueOnTimeout;
            sDocType = m_sDocType;
        }

        final int MAX_TIMEOUTS = 3;
        final RT2Message aRequest = aDeferred.getMetaDataValue("request");
        final String sMsgId = RT2MessageGetSet.getMessageID(aRequest);
        boolean bOK = false;
        int nTimeouts = 0;

        while (!bOK) {
            bOK = aResult.await(nTimeoutMS, TimeUnit.MILLISECONDS);
            if (!bOK) {
                aStatusLine.countTimeout(nTimeouts);
                nTimeouts++;

                LOG.forLevel(ELogLevel.E_ERROR).withMessage("async task [" + sTask + "] failed with timeout [timeout=" + nTimeoutMS + " tried=" + nTimeouts + "] ...").setVar("instance", getInstanceUID()).setVar("timeout", nTimeoutMS).setVar("msg-id", sMsgId).setVar("doc-type", sDocType).setVar("folder-id", aDocImpl.getFolderId()).setVar("file-id", aDocImpl.getFileId()).setVar("drive-id", aDocImpl.getDriveDocId()).setVar("rt2-com.openexchange.rt2.client.uid", aDocImpl.getRT2ClientUID()).log();

                if (!bContinueOnTimeout)
                    Assert.fail("async task [" + sTask + "] failed with timeout ...");

                if (nTimeouts > MAX_TIMEOUTS) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("async task [" + sTask + "] failed with timeout - giving up [timeout=" + nTimeoutMS + " tried=" + nTimeouts + "]").setVar("instance", getInstanceUID()).setVar("timeout", nTimeoutMS).setVar("msg-id", sMsgId).setVar("doc-type", sDocType).setVar("folder-id", aDocImpl.getFolderId()).setVar("file-id", aDocImpl.getFileId()).setVar("drive-id", aDocImpl.getDriveDocId()).setVar("rt2-com.openexchange.rt2.client.uid", aDocImpl.getRT2ClientUID()).log();
                    break;
                }
            }
        }

        final Boolean bResult = aResult.get();
        if (bResult == null || !bResult.booleanValue()) {
            aStatusLine.countError();

            LOG.forLevel(ELogLevel.E_ERROR).withMessage("async task [" + sTask + "] failed with error ...").setVar("instance", getInstanceUID()).setVar("result", bResult).setVar("msg-id", sMsgId).setVar("doc-type", sDocType).setVar("folder-id", aDocImpl.getFolderId()).setVar("file-id", aDocImpl.getFileId()).setVar("drive-id", aDocImpl.getDriveDocId()).setVar("rt2-com.openexchange.rt2.client.uid", aDocImpl.getRT2ClientUID()).log();

            if (!bContinueOnTimeout)
                Assert.fail("async task [" + sTask + "] failed with error ...");
        } else {
            if (nTimeouts > 0) {
                LOG.forLevel(ELogLevel.E_WARNING).withMessage("async task [" + sTask + "] was successfully after [" + nTimeouts + "] timeouts !").setVar("instance", getInstanceUID()).setVar("result", bResult).setVar("msg-id", sMsgId).setVar("doc-type", sDocType).setVar("folder-id", aDocImpl.getFolderId()).setVar("file-id", aDocImpl.getFileId()).setVar("drive-id", aDocImpl.getDriveDocId()).setVar("rt2-com.openexchange.rt2.client.uid", aDocImpl.getRT2ClientUID()).log();
            }
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_monitorDocTask(final RT2 aDoc, final String sTask, final String sError) throws Exception {
        if (StringUtils.isEmpty(sError)) {
            RT2MonitorClient.get().record(RT2MonitorConst.SCOPE_DOC, sTask, null, RT2MonitorConst.DATAKEY_DOC_UID, aDoc.getDocUID(), RT2MonitorConst.DATAKEY_CLIENT_UID, aDoc.getRT2ClientUID());
        } else {
            final Exception ex = new Exception(sError);

            RT2MonitorClient.get().record(ex, RT2MonitorConst.KEY_EX_CLIENT_DOC, null, RT2MonitorConst.DATAKEY_DOC_UID, aDoc.getDocUID(), RT2MonitorConst.DATAKEY_CLIENT_UID, aDoc.getRT2ClientUID());
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ boolean impl_doAutoCloseOnErrorIfConfigured() throws Exception {
        EDocumentState eState = null;
        synchronized (this) {
            if (!m_bAutoCloseOnError)
                return false;

            final Mutable<EDocumentState> rState = mem_State();
            eState = rState.getValue();

            if (eState == EDocumentState.E_IN_ERROR)
                return false;

            rState.setValue(EDocumentState.E_IN_ERROR);
        }

        LOG.forLevel(ELogLevel.E_INFO).withMessage("trigger auto-close of document ...").setVar("instance", getInstanceUID()).setVar("folder-id", getFolderId()).setVar("file-id", getFileId()).setVar("drive-id", getDriveDocId()).log();

        final RT2 aDocImpl = mem_DocImpl();
        final long nTimeout = 10000;

        // See DOCS-1192: ERROR The unique ID of the client is not known!
        // Don't send more requests in case we haven't joined the document,
        // otherwise we trigger exceptions/error logs on the backend  as
        // we have no state on the server-side. Just close/free the connection
        // and resources!
        if (eState != EDocumentState.E_IN_JOIN) {

            if ((eState != EDocumentState.E_IN_LEAVE) && (eState != EDocumentState.E_LEFT)) {
                final AsyncResult<Boolean> aCloseResult = new AsyncResult<>();
                final Deferred<RT2Message> aCloseDeferred = closeAsync(aDocImpl, aCloseResult);
                wait4Async("(auto) close", aCloseDeferred, aCloseResult, nTimeout);
            }

            final AsyncResult<Boolean> aLeaveResult = new AsyncResult<>();
            final Deferred<RT2Message> aLeaveDeferred = leaveAsync(aDocImpl, true, aLeaveResult);
            wait4Async("(auto) leave", aLeaveDeferred, aLeaveResult, nTimeout);
        }

        aDocImpl.free();
        mem_StatusLine().countDocClose();
        return true;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_onDocResponse(final RT2Message aResponse) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("impl_onDocResponse ...").setVar("instance", getInstanceUID()).setVar("type", aResponse.getType()).setVar("response", aResponse).log();

        impl_updateDocumentStatus(aResponse);
        impl_recordOperations(aResponse);
        impl_checkUpdateOSN(aResponse);
        impl_updateActiveUserList(aResponse);

        onDocResponse(aResponse);
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_onError(final RT2Message aErrorResponse) throws Exception {
        onError(aErrorResponse);
        impl_doAutoCloseOnErrorIfConfigured();
    }

    //-------------------------------------------------------------------------
    private void /* no synchronized */ impl_updateDocumentStatus(final RT2Message aResponse) throws Exception {
        if (RT2Protocol.BROADCAST_UPDATE.equals(aResponse.getType())) {
            LOG.forLevel(ELogLevel.E_TRACE).withMessage("broadcast update body: " + aResponse.getBodyString()).log();
            updateEditor(aResponse);
        } else if (RT2Protocol.RESPONSE_OPEN_DOC_CHUNK.equals(aResponse.getType())) {
            if (RT2MessageGetSet.isPreview(aResponse)) {
                m_bPreviewOpsOnOpen = true;
            }
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_checkUpdateOSN(final RT2Message aResponse) throws Exception {
        // update OSN for viewer when we receive a broadcast update, too
        if (RT2Protocol.BROADCAST_UPDATE.equals(aResponse.getType()))
            updateOSN(aResponse);
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_recordOperations(final RT2Message aResponse) throws Exception {
        if (!m_bRecordOperations.get())
            return;

        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("record operations ...").setVar("instance", getInstanceUID()).log();

        final String sBody = aResponse.getBodyString();
        if (StringUtils.isEmpty(sBody))
            return;

        final JSONObject aBody = new JSONObject(sBody);
        final JSONArray lOps = aBody.optJSONArray("operations");
        final JSONObject aDocState = aBody.optJSONObject("docStatus");
        final List<JSONObject> aRecorder = mem_RecordedOperations();

        if (lOps != null) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("... found operations array").setVar("instance", getInstanceUID()).log();

            final int c = lOps.length();
            for (int i = 0; i < c; ++i) {
                final JSONObject aOp = lOps.getJSONObject(i);

                OperationsUtils.decompressObject(aOp);
                aRecorder.add(aOp);
            }
        }

        if (aDocState != null) {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("... found doc state").setVar("instance", getInstanceUID()).log();

            aRecorder.add(aDocState);
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_updateActiveUserList(final RT2Message aResponse) throws Exception {
        if (aResponse.isType(RT2Protocol.BROADCAST_UPDATE_CLIENTS))
            updateActiveUserList(aResponse);
    }

    //-------------------------------------------------------------------------
    protected void updateActiveUserList(final RT2Message aResponse) throws Exception {
        try {
            final JSONObject aBody = aResponse.getBody();
            final JSONObject aClientsUpdate = aBody.getJSONObject("clients");

            if ((null == aClientsUpdate) || (aClientsUpdate.keySet().isEmpty())) {
                LOG.forLevel(ELogLevel.E_ERROR).withMessage("update clients broadcast without clients data...").setVar("instance", getInstanceUID()).log();
                return;
            }

            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("update list of active users ...").log();

            updateActiveUsersListWithClientsUpdateData(aClientsUpdate);

            final List<JSONObject> lActiveUsers = mem_ActiveUsersList();
            final List<DocUser> lAllKnownUser = new ArrayList<>();
            final DocUserRegistry aUserRegistry = mem_UserRegistry();

            synchronized (m_aSync) {
                final int c = lActiveUsers.size();
                final String sThisUserId = getRT2ClientUID();

                for (int i = 0; i < c; ++i) {
                    final JSONObject aUserJSON = lActiveUsers.get(i);
                    final DocUser aUser = DocUser.create(aUserJSON);
                    final boolean bItsMe = StringUtils.equals(aUser.getUserId(), sThisUserId);

                    if (bItsMe) {
                        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("... register myself as new active user [" + aUser.getId() + "]").setVar("instance", getInstanceUID()).setVar("user", aUser).log();
                    } else {
                        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("... register new active user [" + aUser.getId() + "]").setVar("instance", getInstanceUID()).setVar("user", aUser).log();
                    }

                    lAllKnownUser.add(aUser);
                }

                LOG.forLevel(ELogLevel.E_INFO).withMessage("Current user list created").setVar("instance", getInstanceUID()).setVar("size", lAllKnownUser.size()).log();
            }

            aUserRegistry.setAllKnownUser(lAllKnownUser);

            fire(DocEvent.create(DocEvent.EVENT_ACTIVE_USER_LIST_CHANGED, aUserRegistry));
        } catch (Throwable ex) {
            LOG.forLevel(ELogLevel.E_ERROR).withError(ex).setVar("instance", getInstanceUID()).log();
            // do nothing else !
            // errors within recorded data will be analyzed later (by outside code!)mem_ActiveUsersList
        }
    }

    //-------------------------------------------------------------------------
    private void updateActiveUsersListWithClientsUpdateData(final JSONObject aUpdateClientsData) throws Exception {
        final List<JSONObject> lActiveUsers = mem_ActiveUsersList();
        final JSONArray aFullClients = aUpdateClientsData.optJSONArray("list");

        synchronized (m_aSync) {
            if (null != aFullClients) {
                // this is a full list update containing all known clients
                lActiveUsers.clear();
                for (int i = 0; i < aFullClients.length(); i++)
                    lActiveUsers.add(aFullClients.getJSONObject(i));
            } else {
                // check for the possible update clients notification buckets
                final JSONArray aAddedClients = aUpdateClientsData.optJSONArray("joined");
                final JSONArray aRemovedClients = aUpdateClientsData.optJSONArray("left");
                final JSONArray aChangedClients = aUpdateClientsData.optJSONArray("changed");

                if ((null != aAddedClients) || (null != aRemovedClients) || (null != aChangedClients)) {
                    final Map<String, JSONObject> aClientsMap = new HashMap<>();
                    for (final JSONObject o : lActiveUsers)
                        aClientsMap.put(o.getString("clientUID"), o);

                    if (null != aAddedClients) {
                        for (int i = 0; i < aAddedClients.length(); i++) {
                            final JSONObject aClientData = aAddedClients.getJSONObject(i);
                            aClientsMap.put(aClientData.getString("clientUID"), aClientData);
                        }
                    }

                    if (null != aRemovedClients) {
                        for (int i = 0; i < aRemovedClients.length(); i++)
                            aClientsMap.remove(aRemovedClients.getString(i));
                    }

                    if (null != aChangedClients) {
                        for (int i = 0; i < aChangedClients.length(); i++) {
                            final JSONObject aClientData = aChangedClients.getJSONObject(i);
                            aClientsMap.put(aClientData.getString("clientUID"), aClientData);
                        }
                    }

                    lActiveUsers.clear();
                    for (final JSONObject o : aClientsMap.values())
                        lActiveUsers.add(o);
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void onDocResponse(final RT2Message aResponse) throws Exception {
        // nothing to do ... derived class do it's own stuff there ... or nothing ...
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void onError(final RT2Message aErrorResponse) throws Exception {
        // nothing to do ... derived class do it's own stuff there ... or nothing ...
    }

    //-------------------------------------------------------------------------
    private synchronized RT2 mem_DocImpl() throws Exception {
        return m_aDocImpl;
    }

    //-------------------------------------------------------------------------
    private synchronized Mutable<EDocumentState> mem_State() throws Exception {
        if (m_eState == null)
            m_eState = new MutableObject<>(EDocumentState.E_CLOSED);
        return m_eState;
    }

    //-------------------------------------------------------------------------
    private synchronized List<JSONObject> mem_RecordedOperations() throws Exception {
        if (m_lRecordedOperations == null)
            m_lRecordedOperations = new Vector<>();
        return m_lRecordedOperations;
    }

    //-------------------------------------------------------------------------
    private synchronized StatusLine mem_StatusLine() throws Exception {
        if (m_aStatusLine == null)
            m_aStatusLine = StatusLine.get();
        return m_aStatusLine;
    }

    //-------------------------------------------------------------------------
    protected synchronized List<JSONObject> getCopiedActiveUserList() {
        if (m_aActiveUsersList == null)
            m_aActiveUsersList = new Vector<>();
        return new Vector<>(m_aActiveUsersList);
    }

    //-------------------------------------------------------------------------
    protected synchronized List<JSONObject> mem_ActiveUsersList() throws Exception {
        if (m_aActiveUsersList == null)
            m_aActiveUsersList = new Vector<>();
        return m_aActiveUsersList;
    }

    //-------------------------------------------------------------------------
    private synchronized IEventHandler mem_EventHandler() throws Exception {
        if (m_iEventHandler != null)
            return m_iEventHandler;

        m_iEventHandler = new IEventHandler() {

            @Override
            public void onEvent(final Object aSource, final String sEvent, final Object aData) throws Exception {
                RT2Message msg = (RT2Message) aData;
                LOG.forLevel(ELogLevel.E_TRACE).withMessage("Received event! Type: " + msg.getType() + ", msg: " + msg).log();
                if (StringUtils.equals(sEvent, IEventHandler.EVENT_DOC_RESPONSE))
                    impl_onDocResponse(msg);
                else if (StringUtils.equals(sEvent, IEventHandler.EVENT_ERROR))
                    impl_onError(msg);
                else
                    throw new UnsupportedOperationException("No support for event '" + sEvent + "'. Please implement !");
            }
        };

        return m_iEventHandler;
    }

    //-------------------------------------------------------------------------
    private synchronized DocUserRegistry mem_UserRegistry() throws Exception {
        if (m_aUserRegistry == null)
            m_aUserRegistry = new DocUserRegistry();
        return m_aUserRegistry;
    }

}
