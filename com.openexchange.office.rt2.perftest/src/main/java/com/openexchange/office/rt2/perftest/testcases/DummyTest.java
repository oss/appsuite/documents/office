/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class DummyTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(DummyTest.class);
    
    //-------------------------------------------------------------------------
    public DummyTest ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    @Override
    public void runTest ()
    	throws Exception
    {
    	final TestRunConfig aCfg = getRunConfig ();
    	final Date          aNow = new Date ();
    	final String        sNow = DateFormatUtils.format(aNow, "dd.MM.yyyy-HH:mm:ss:S");
    	
    	LOG	.forLevel   (ELogLevel.E_INFO)
    		.withMessage("Dummy-Test ["+aCfg.sTestId+"] started at '"+sNow+"' ...")
    		.setVar     ("user", aCfg.aUser)
    		.log        ();
    }
}
