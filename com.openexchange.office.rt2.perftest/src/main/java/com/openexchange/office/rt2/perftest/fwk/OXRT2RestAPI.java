/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class OXRT2RestAPI {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXRT2RestAPI.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_RT2   = "/rt2";
    public static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    private OXAppsuite m_aContext = null;

    //-------------------------------------------------------------------------
    public OXRT2RestAPI ()
        throws Exception {
    }

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aContext)
        throws Exception {
        m_aContext = aContext;
    }

    //-------------------------------------------------------------------------
    private String getBaseUri ()
        throws Exception {
        return m_aContext.accessConfig().sAPIRelPath;
    }

    //-------------------------------------------------------------------------
    public ErrorCode emergencyLeave(String clientUID, String docUID, final JSONArray operations, boolean recalcNecessary)
        throws Exception {
        final OXConnection    aConnection = m_aContext.accessConnection();
        final OXSession       aSession    = m_aContext.accessSession   ();
        final String          sRestAPIUri = getBaseUri () + REST_API_RT2;
        final String          sSessionId  = aSession.getSessionId();
        final String          sChannelId  = UUID.randomUUID().toString();

        final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aEmptyNameValuePair);

        final JSONObject leaveDataJSON = new JSONObject();
        leaveDataJSON.put("operations", operations);
        leaveDataJSON.put("recalcFlag", recalcNecessary);

        final NameValuePair[] aBodyNameValuePairs = new NameValuePair[6];
        aBodyNameValuePairs[0] = new BasicNameValuePair("action"     , "emergencyleave"        );
        aBodyNameValuePairs[1] = new BasicNameValuePair("session"    , sSessionId              );
        aBodyNameValuePairs[2] = new BasicNameValuePair("client_uid" , clientUID               );
        aBodyNameValuePairs[3] = new BasicNameValuePair("doc_uid"    , docUID                  );
        aBodyNameValuePairs[4] = new BasicNameValuePair("channel_uid", sChannelId              );
        aBodyNameValuePairs[5] = new BasicNameValuePair("leaveData"  , leaveDataJSON.toString());

        final URIBuilder aUriBuilder = new URIBuilder ();
        for (final NameValuePair aParam : aBodyNameValuePairs)
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        final String sEncBodyString = aUriBuilder.build().toString().substring(1);

        LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + sEncBodyString).log();

        final UnboundHttpResponse response = aConnection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
        return getErrorCodeFromResponse(response);
    }

    //-------------------------------------------------------------------------
    private ErrorCode getErrorCodeFromResponse(UnboundHttpResponse response) throws Exception {
    	ErrorCode result = ErrorCode.NO_ERROR;

        final String body = response.getBody();
        if (StringUtils.isNotEmpty(body)) {
        	try {
                final org.json.JSONObject resultAsJSON = new org.json.JSONObject(body);
                final org.json.JSONObject data = resultAsJSON.getJSONObject("data");
                final org.json.JSONObject errorAsJSON = data.getJSONObject("error");
                result = ErrorCode.createFromJSONObject(errorAsJSON, ErrorCode.NO_ERROR);
        	} catch (JSONException e) {
        		// do nothing and interpret it as no error - html response
        	}
        }

        return result;
    }

}
