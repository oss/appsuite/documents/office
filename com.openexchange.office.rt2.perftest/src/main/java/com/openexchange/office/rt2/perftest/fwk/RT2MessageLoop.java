/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

/**
 * {@link RT2MessageLoop}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.3
 */
public class RT2MessageLoop {

    /**
     * Initializes a new {@link RT2MessageLoop}.
     * @param iHandler
     * @throws Exception
     */
    public RT2MessageLoop(final IMessageHandler messageHandler) {
        super();

        m_messageHandler = messageHandler;

        m_loopResultFuture = m_loopExecutor.submit(() -> {
            while (m_isRunning.get()) {
                try {
                    final String curMsg = m_queue.take();

                    if ((null != curMsg) && (null != m_messageHandler)) {
                        m_messageHandler.onMessage(this, curMsg);
                    }
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    LOG.forLevel(ELogLevel.E_TRACE).withMessage("RT2MessageLoop received interrupt").log();
                } catch (Throwable e) {
                    final RT2MessageExceptionHandler exceptionHandler = m_exceptionHandler;

                    // let a possibly set exception handler handle the
                    // exception or just log the exception message
                    if (null != exceptionHandler) {
                        exceptionHandler.handleException(this, e);
                    } else {
                        LOG.forLevel(ELogLevel.E_ERROR).withError(e).log();
                    }
                }
            }

            return null;
        });

    }

    /**
     * @param exceptionHandler
     */
    void setMessageExceptionHandler(final RT2MessageExceptionHandler exceptionHandler) {
        m_exceptionHandler = exceptionHandler;
    }

    /**
     * @throws Exception
     */
    public void stop() {
        if (m_isRunning.compareAndSet(true, false)) {
            m_loopResultFuture.cancel(true);
        }
    }

    /**
     * @param sMsg
     * @throws Exception
     */
    public void enqueue(final String sMsg) {
        m_queue.offer(sMsg);
    }

    // - Members ---------------------------------------------------------------

    final private static Logger LOG = Slf4JLogger.create(RT2MessageLoop.class);

    final AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final private BlockingQueue<String> m_queue = new LinkedBlockingQueue<>();

    final private ExecutorService m_loopExecutor = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("RT2MessageLoop-%d").build());

    final private IMessageHandler m_messageHandler;

    final private Future<Void> m_loopResultFuture;

    volatile private RT2MessageExceptionHandler m_exceptionHandler = null;
}
