/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

//=============================================================================
public class TestDocProvider
{
    //-------------------------------------------------------------------------
    public static final String FOLDER_ID_PUBLIC_SHARE = "304";
//    public static final String FOLDER_ID_PUBLIC_SHARE = "294";
//    public static final String FOLDER_ID_PUBLIC_SHARE = "1083";

    //-------------------------------------------------------------------------
    public static final String TESTFILE_CALCENGINE_AS_A_SERVICE_PDF = "calcengine_as_a_service.pdf";
    public static final String TESTFILE_COLLAB_RW_DOCX              = "collab-rw.docx";

    //-------------------------------------------------------------------------
    public static final String FOLDER_ID     = "folder-id"    ;
    public static final String FILE_ID       = "file-id"      ;
    public static final String DRIVE_DOC_ID  = "drive-doc-id" ;
    public static final String DOC_TYPE      = "doc-type"     ;

    //-------------------------------------------------------------------------
    public static Map< String, String > getTestFileData (final String sTestFileName)
        throws Exception
    {
        final Map< String, String > lData = new HashMap< String, String > ();

        if (StringUtils.equals(sTestFileName, TestDocProvider.TESTFILE_CALCENGINE_AS_A_SERVICE_PDF))
        {
            lData.put(FOLDER_ID   , "286"                );
            lData.put(FILE_ID     , "2861/605"            );
            lData.put(DRIVE_DOC_ID, "1001@286/605"       );
            lData.put(DOC_TYPE    , Doc.DOCTYPE_PRESENTER);
        }
        else
        if (StringUtils.equals(sTestFileName, TestDocProvider.TESTFILE_COLLAB_RW_DOCX))
        {
            lData.put(FOLDER_ID   , "1083"          );
            lData.put(FILE_ID     , "1083/63"       );
            lData.put(DRIVE_DOC_ID, "1001@1083/63"  );
            lData.put(DOC_TYPE    , Doc.DOCTYPE_TEXT);
        }
        else
            throw new UnsupportedOperationException ("Test file '"+sTestFileName+"' is unknown.");

        return lData;
    }
}