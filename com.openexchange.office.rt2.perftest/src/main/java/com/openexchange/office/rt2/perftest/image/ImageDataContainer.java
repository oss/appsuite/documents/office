/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.validation.constraints.NotNull;

import org.springframework.util.StreamUtils;

import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public class ImageDataContainer {
	private static final String IMAGETYPE_JPEG = "data:image/jpeg;base64,";
	private static final String IMAGETYPE_PNG = "data:image/png;base64,";

    private static final Logger LOG = Slf4JLogger.create(ImageDataContainer.class);

	//-------------------------------------------------------------------------
	private static class ImageDataFormats {
		public byte[] binaryImageData;
		public String base64ImageData;

		public ImageDataFormats(byte[] bytes, String base64) {
			binaryImageData = bytes;
			base64ImageData = base64;
		}
	};

	private final Map<String, ImageDataFormats> imageDataEntryFormats = new HashMap<>();
	private final Map<String, ImageDataEntry> imageDataEntries = new HashMap<>();

	//-------------------------------------------------------------------------
	public ImageDataContainer(final ImageDataEntry[] entries) {
		if (entries != null) {
			for (ImageDataEntry entry : entries) {
				final byte[] imageData = readImageDataFromCfgEntry(entry);
				if (imageData != null) {

					ImageDataFormats imageDataFormatEntry = new ImageDataFormats(imageData, convertToBase64(imageData, entry.getFormat()));
					imageDataEntryFormats.put(entry.getPath(), imageDataFormatEntry);
					imageDataEntries.put(entry.getPath(), entry);
				}
			}
		}
	}

	//-------------------------------------------------------------------------
	public int entries() {
		return imageDataEntryFormats.size();
	}

	//-------------------------------------------------------------------------
	public Set<String> entryIds() {
		return imageDataEntryFormats.keySet();
	}

	//-------------------------------------------------------------------------
	public String getImageAsBase64EncodedString(String id) {
		final ImageDataFormats entry = imageDataEntryFormats.get(id);
		if (entry != null)
			return entry.base64ImageData;
		return null;
	}

	//-------------------------------------------------------------------------
	public byte[] getImageAsBytes(String id) {
		final ImageDataFormats entry = imageDataEntryFormats.get(id);
		if (entry != null)
			return entry.binaryImageData;
		return null;
	}

	//-------------------------------------------------------------------------
	public ImageDataEntry getImageDataEntry(String id) {
		return imageDataEntries.get(id);
	}

	//-------------------------------------------------------------------------
	private byte[] readImageDataFromCfgEntry(@NotNull ImageDataEntry entry) {
		if (entry.getType() == ImageDataType.IMAGE_DATA_TYPE_RESOURCE_PATH) {
			try (InputStream inputStream = getClass().getResourceAsStream(entry.getPath())) {
				if (null != inputStream) {
					return StreamUtils.copyToByteArray(inputStream);
				}
				LOG.forLevel(ELogLevel.E_ERROR).withMessage("Cannot get stream from image resource").setVar("path", entry.getPath()).log();
			} catch (IOException e) {
				LOG.forLevel(ELogLevel.E_ERROR).withMessage("Error reading image resource").setVar("path", entry.getPath()).log();
			}
		} else if (entry.getType() == ImageDataType.IMAGE_DATA_TYPE_PATH) {
	        try (InputStream inputStream =  new FileInputStream(entry.getPath())) {
                return StreamUtils.copyToByteArray(inputStream);
	        } catch(FileNotFoundException ex) {
	        	LOG.forLevel(ELogLevel.E_ERROR).withMessage("Configured image file not found").setVar("path", entry.getPath()).log();
	        } catch(IOException ex) {
	        	LOG.forLevel(ELogLevel.E_ERROR).withMessage("Error reading image file").setVar("path", entry.getPath()).log();
	        }
		}

		return null;
	}

	//-------------------------------------------------------------------------
	private String convertToBase64(byte[] data, String imageFormat) {
		final byte[] encoded = Base64.getEncoder().encode(data);

		String formatPrefix;
		switch (imageFormat) {
		case ImageFormat.JPEG: formatPrefix = IMAGETYPE_JPEG; break;
		case ImageFormat.PNG: formatPrefix = IMAGETYPE_PNG; break;
		default: return null;
		}

		final StringBuilder strBuf = new StringBuilder(20);
		strBuf.append(formatPrefix);
		strBuf.append(new String(encoded, Charset.forName("US-ASCII")));
		return strBuf.toString();
	}
}

