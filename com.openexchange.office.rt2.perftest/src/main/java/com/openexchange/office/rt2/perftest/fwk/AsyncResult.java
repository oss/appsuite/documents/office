/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;




//=============================================================================
public class AsyncResult< T >
{
    //-------------------------------------------------------------------------
    public AsyncResult ()
    {}
    
    //-------------------------------------------------------------------------
    public AsyncResult (final T aInitValue)
    {
        m_aResult = aInitValue;
    }

    //-------------------------------------------------------------------------
    public void set (final T aResult)
    	throws Exception
    {
    	m_aResult = aResult;
    	mem_Wait ().countDown();
    }
    
    //-------------------------------------------------------------------------
    public T get ()
    	throws Exception
    {
    	return m_aResult;
    }

    //-------------------------------------------------------------------------
    public void reset ()
        throws Exception
    {
    	m_aResult = null;
    	m_aWait   = null;
    }

    //-------------------------------------------------------------------------
    public boolean await(final long     nTime,
    				     final TimeUnit aUnit)
    	throws Exception
    {
    	final CountDownLatch aWait   = mem_Wait ();
    	final boolean        bResult = aWait.await(nTime, aUnit);
    	return bResult;
    }

    //-------------------------------------------------------------------------
    private CountDownLatch mem_Wait ()
        throws Exception
    {
    	if (m_aWait == null)
    		m_aWait = new CountDownLatch (1);
    	return m_aWait;
    }

    //-------------------------------------------------------------------------
    private T m_aResult = null;

    //-------------------------------------------------------------------------
    private CountDownLatch m_aWait = null;
}
