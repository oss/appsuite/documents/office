/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class MessagePerformanceStats
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(MessagePerformanceStats.class);

    //-------------------------------------------------------------------------
    public static final long TIME_MAX   = 120000;
    public static final long TIME_SLICE =    500;
    
//    //-------------------------------------------------------------------------
//    public static final String HEADER_CLIENT_OUT             = "dbg_perf_client_out";
//    public static final String HEADER_CLIENT_IN              = "dbg_perf_client_in" ;
//    
//    //-------------------------------------------------------------------------
//    public static final String HEADER_SERVER_IN              = "dbg_perf_server_in"              ;
//    public static final String HEADER_SERVER_OUT             = "dbg_perf_server_out"             ;
//    public static final String HEADER_SERVER_LOAD_START      = "dbg_perf_server_load_start"      ;
//    public static final String HEADER_SERVER_LOAD_END        = "dbg_perf_server_load_end"        ;
//    public static final String HEADER_SERVER_APPLYOPS_START  = "dbg_perf_server_applyops_start"  ;
//    public static final String HEADER_SERVER_APPLYOPS_END    = "dbg_perf_server_applyops_end"    ;
//    public static final String HEADER_SERVER_SAVE_START      = "dbg_perf_server_save_start"      ;
//    public static final String HEADER_SERVER_SAVE_END        = "dbg_perf_server_save_end"        ;
//    public static final String HEADER_SERVER_RECENTLIST_START= "dbg_perf_server_recentlist_start";
//    public static final String HEADER_SERVER_RECENTLIST_END  = "dbg_perf_server_recentlist_end"  ;
//    public static final String HEADER_SERVER_SEQUENCER_START = "dbg_perf_server_sequencer_start" ;
//    public static final String HEADER_SERVER_SEQUENCER_END   = "dbg_perf_server_sequencer_end"   ;
//    public static final String HEADER_SERVER_ROUTE_DOC_IN    = "dbg_perf_server_route_doc_in"    ;
//    public static final String HEADER_SERVER_ROUTE_DOC_OUT   = "dbg_perf_server_route_doc_out"   ;

    //-------------------------------------------------------------------------
    private MessagePerformanceStats ()
    {}

    //-------------------------------------------------------------------------
    public static synchronized MessagePerformanceStats get ()
    	throws Exception
    {
    	if (m_gSingleton == null)
    		m_gSingleton = new MessagePerformanceStats ();
    	return m_gSingleton;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void analyzeMessage (final RT2Message aMessage)
    	throws Exception
    {
    	try
    	{
    		final String sMsgType            = aMessage.getType       ();
    		final String sMsgID              = RT2MessageGetSet.getMessageID  (aMessage);
    
    		final Long   nClientOut          = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_OUT             );
    		final Long   nClientIn           = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_CLIENT_IN              );
    		
    		final Long   nServerIn           = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_IN              );
    		final Long   nServerOut          = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_OUT             );
    		
    		final Long   nDocRouteIn         = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_ROUTE_DOC_IN    );
    		final Long   nDocRouteOut        = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_ROUTE_DOC_OUT   );
    		
    		final Long   nSequencerStart     = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SEQUENCER_START );
    		final Long   nSequencerEnd       = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SEQUENCER_END   );
    		
    		final Long   nDocLoadStart       = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_LOAD_START      );
    		final Long   nDocLoadEnd         = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_LOAD_END        );
    		
    		final Long   nDocApplyOpsStart   = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_APPLYOPS_START  );
    		final Long   nDocApplyOpsEnd     = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_APPLYOPS_END    );
    		
    		final Long   nDocSaveStart       = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SAVE_START      );
    		final Long   nDocSaveEnd         = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_SAVE_END        );
    
    		final Long   nDocRecentListStart = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_RECENTLIST_START);
    		final Long   nDocRecentListEnd   = aMessage.getHeader     (RT2Protocol.HEADER_DEBUG_PERFORMANCE_SERVER_RECENTLIST_END  );
    
    		final Long   nTime4All           = impl_calcTimeBetween (nClientOut         , nClientIn        );
    		
    		final Long   nTime4Server        = impl_calcTimeBetween (nServerIn          , nServerOut       );
    		final Long   nTime4DocRoute      = impl_calcTimeBetween (nDocRouteIn        , nDocRouteOut     );
    		final Long   nTime4Sequencer     = impl_calcTimeBetween (nSequencerStart    , nSequencerEnd    );
    
    		final Long   nTime4Load          = impl_calcTimeBetween (nDocLoadStart      , nDocLoadEnd      );
    		final Long   nTime4ApplyOps      = impl_calcTimeBetween (nDocApplyOpsStart  , nDocApplyOpsEnd  );
    		final Long   nTime4Save          = impl_calcTimeBetween (nDocSaveStart      , nDocSaveEnd      );
    		final Long   nTime4RecentList    = impl_calcTimeBetween (nDocRecentListStart, nDocRecentListEnd);
    		
    		impl_addTime (m_lStats4All          , nTime4All       );
    		impl_addTime (m_lStats4Server       , nTime4Server    );
    		impl_addTime (m_lStats4Sequencer    , nTime4Sequencer );
    		impl_addTime (m_lStats4DocRoute     , nTime4DocRoute  );
    		impl_addTime (m_lStats4DocLoad      , nTime4Load      );
    		impl_addTime (m_lStats4DocApplyOps  , nTime4ApplyOps  );
    		impl_addTime (m_lStats4DocSave      , nTime4Save      );
    		impl_addTime (m_lStats4DocRecentList, nTime4RecentList);

    		LOG	.forLevel	(ELogLevel.E_DEBUG)
    			.withMessage("analyzed message performance data ...")
    			.setVar     ("msg-id"               , sMsgID          )
    			
    			.setVar     ("client-out"           , nClientOut      )
    			.setVar     ("client-in"            , nClientIn       )
    			.setVar     ("server-out"           , nServerOut      )
    			.setVar     ("server-in"            , nServerIn       )

    			.setVar     ("time-4-all"           , nTime4All       )
    			.setVar     ("time-4-server"        , nTime4Server    )
    			.setVar     ("time-4-sequencer"     , nTime4Sequencer )
    			.setVar     ("time-4-doc-route"     , nTime4DocRoute  )
    			.setVar     ("time-4-doc-load"      , nTime4Load      )
    			.setVar     ("time-4-doc-applyops"  , nTime4ApplyOps  )
    			.setVar     ("time-4-doc-save"      , nTime4Save      )
    			.setVar     ("time-4-doc-recentlist", nTime4RecentList)
    			.log		();
    	}
    	catch (final Throwable ex)
    	{
    		LOG	.forLevel	(ELogLevel.E_ERROR)
    			.withError	(ex)
    			.log		();
    	}
    }

    //-------------------------------------------------------------------------
    public void dumpCSV (final String sFile)
        throws Exception
    {
    	final File aFile = new File (sFile);
    	final File aDir  = aFile.getParentFile();

    	if ( ! aDir.exists())
    		aDir.mkdirs();
    	
    	if ( ! aDir.exists())
    		throw new IOException ("Could not create dir '"+aDir+"'.");
    	
    	final StringBuilder sCSV = new StringBuilder (256);
    	sCSV.append ("Categories,All,Server,Sequencer,DocRoute,Load,Apply,Save,RecentList\n");
    	
    	long nSlice = 0;
    	for (nSlice = TIME_SLICE; nSlice <= TIME_MAX; nSlice += TIME_SLICE)
    		impl_dumpSlice2CSV (sCSV, nSlice);

    	FileUtils.write(aFile, sCSV.toString(), "utf-8");
    }
    
    //-------------------------------------------------------------------------
    private void impl_dumpSlice2CSV (final StringBuilder sCSV  ,
    								 final long         nSlice)
    	throws Exception
    {
    	final long nUISlice = nSlice / 100;

    	sCSV.append (nUISlice);
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4All          , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4Server       , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4Sequencer    , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4DocRoute     , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4DocLoad      , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4DocApplyOps  , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4DocSave      , nSlice));
    	sCSV.append (","   );
    	sCSV.append (impl_getCount (m_lStats4DocRecentList, nSlice));
    	sCSV.append ("\n"  );
    }
    
    //-------------------------------------------------------------------------
    private static void impl_addTime (final Map< Long, MutableInt > lStats,
    								  final Long                    nTime )
        throws Exception
    {
    	if (nTime == null)
    		return;
    	
    	final Long       nSlice      = impl_getTimeSlice (nTime);
    	      MutableInt nSliceValue = lStats.get(nSlice);

    	if (nSliceValue == null)
    	{				
    		nSliceValue = new MutableInt (0);
    		lStats.put(nSlice, nSliceValue);
    	}
    	
    	nSliceValue.increment();
    }
    
    //-------------------------------------------------------------------------
    private static int impl_getCount (final Map< Long, MutableInt > lStats,
    								  final long                    nSlice)
        throws Exception
    {
    	final MutableInt nSliceValue = lStats.get(nSlice);
    	if (nSliceValue == null)
    		return 0;
    	
    	final int nCount = nSliceValue.intValue();
    	return nCount;
    }

    //-------------------------------------------------------------------------
    private static long impl_getTimeSlice (final long nTime)
    	throws Exception
    {
    	long nSlice = 0;
    	for (nSlice = TIME_SLICE; nSlice <= TIME_MAX; nSlice += TIME_SLICE)
    	{
    		if (nTime < nSlice)
    			return nSlice;
    	}
    	return nSlice;
    }
    
    //-------------------------------------------------------------------------
    private static Long impl_calcTimeBetween (final Long nStart,
    										  final Long nEnd  )
    	throws Exception
    {
    	if (
    		(nStart == null) ||
    		(nEnd   == null)
    	   )
    		return null;
    	
    	final Long nTime = nEnd - nStart;
    	return nTime;
    }
    
    //-------------------------------------------------------------------------
    private static MessagePerformanceStats m_gSingleton = null;

    //-------------------------------------------------------------------------
    private Map< Long, MutableInt > m_lStats4All           = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4Server        = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4Sequencer     = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4DocRoute      = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4DocLoad       = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4DocSave       = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4DocApplyOps   = new HashMap< Long, MutableInt > ();
    private Map< Long, MutableInt > m_lStats4DocRecentList = new HashMap< Long, MutableInt > ();
}
