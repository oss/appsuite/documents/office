/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

/*
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXConnection;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.OXWSClient;
import com.openexchange.office.rt2.perftest.fwk.OXWSMessageSink;
*/
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;

//=============================================================================
public class ComplexTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    public ComplexTest ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    @Override
    public void runTest ()
    	throws Exception
    {
//		final TestRunConfig   aCfg        = getRunConfig ();
//		final UserDescriptor  aUser       = aCfg.aUser;
//		
//		//System.out.println ("run ["+aCfg.sTestId+"] ...");
//		
//		final OXAppsuite      aAppsuite   = new OXAppsuite (aCfg);
//		final OXConnection    aConnection = aAppsuite.accessConnection ();
//		final OXSession       aSession    = aAppsuite.accessSession    ();
//		final OXWSClient      aWSClient   = aAppsuite.accessWSClient   ();
//		final OXWSMessageSink aMsgSink    = aWSClient.accessMessageSink();
//
//		aSession.login(aUser);
//		final String sSession = aSession.getSessionId();
//		
//		final JSONObject lHeader = new JSONObject ();
//		lHeader.put("msg_id"    , UUID.randomUUID().toString());
//		lHeader.put("session_id", sSession                    );
//		lHeader.put("doc_id"    , "1@224/4173"                );
//		lHeader.put("folder_id" , "224"                       );
//		lHeader.put("file_id"   , "224/4173"                  );
//		
//		final JSONObject aJSON = new JSONObject ();
//		aJSON.put("type"  , "join" );
//		aJSON.put("header", lHeader);
//		
//		final String         sJSON = aJSON.toString();
//		final CountDownLatch aJoin = new CountDownLatch (3);
//
//		aMsgSink .reset(aJoin);
//		aWSClient.send (sJSON);
//		final Boolean bOK = aJoin.await(5000 , TimeUnit.MILLISECONDS);
//		
//		Validate.isTrue(bOK, "Timeout on waiting for right count of response messages.");
//		//System.out.println("response queue : "+aMsgSink.getAll());
//		
//		aSession.logout ();
    }
}
