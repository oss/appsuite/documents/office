/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

/**
 * {@link RT2Channel}
 */
public class RT2Channel implements IMessageHandler {

    final private static int PING_MAX_STR_LEN = 100;

    /**
     * Initializes a new {@link RT2Channel}.
     * @throws Exception
     */
    private RT2Channel(final OXAppsuite aContext) throws Exception {
        super();

        // access client and configure it
        m_wsClient = aContext.accessWSClient();
        m_wsClient.setConnectionId(m_channelId);
        m_wsClient.setMessageHandler(this);

        // create output message loop
        m_outputMessageLoop = new RT2MessageLoop((source, msg) -> {
            m_wsClient.send(msg).get();
        });

        // create input message loop
        m_inputMessageLoop = new RT2MessageLoop((source, msg) -> {
            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("got server response ...").setVar("message", msg).log();
            if (StringUtils.isEmpty(msg)) {
                LOG.forLevel(ELogLevel.E_WARNING).withMessage("... got server response with no data ?!").log();
                return;
            }

            final RT2Message aResponse = RT2MessageFactory.fromJSONString(msg);
            String sClientUID = null;

            LOG.forLevel(ELogLevel.E_TRACE).withMessage("Received message of type: " + aResponse.getType()).log();

            // tricky : under normal circumstances responses came from server and
            // contain client uid of the original request.
            // But special responses (as e.g. offline/online) are generated
            // on client side directly. They do not contain any client uid !
            // They have to be broadcast to all known clients  ... see below.
            if (aResponse.hasHeader(RT2Protocol.HEADER_CLIENT_UID)) {
                sClientUID = RT2MessageGetSet.getClientUID(aResponse);
            }

            // a) direct response for one specific client
            if (!StringUtils.isEmpty(sClientUID)) {
                impl_notifyClient(sClientUID, aResponse);
                return;
            }

            // b) (internal) 'broadcast' to all clients !
            //    e.g. offline mode
            synchronized (m_clientRegistry) {
                final Iterator<String> rRegistry = m_clientRegistry.keySet().iterator();

                while (rRegistry.hasNext()) {
                    sClientUID = rRegistry.next();
                    RT2MessageGetSet.setClientUID(aResponse, sClientUID);
                    impl_notifyClient(sClientUID, aResponse);
                }
            }
        });
    }

    public static RT2Channel createChannel(final OXAppsuite context) throws Exception {
    	return new RT2Channel(context);
    }

    // - IMessageHandler -------------------------------------------------------

    /**
     * called from WS for incoming response messages
     */
    @Override
    public void onMessage(final Object aSource, final String sResponse) throws Exception {
        // Low-level handling of our explicit ping message as some browsers
        // don't stick to the websocket rfc definition.
        // ATTENTION: We use internal knowledge about the size of the ping
        // message to minimize the overhead using fromJSONString() which
        // is expensive for longer messages. On a higher-level this is always
        // done, doing this twice would be too much overhead. This assumption
        // can break as soon as we decide to create larger messages.
        if (StringUtils.isNotEmpty(sResponse) && (sResponse.length() < PING_MAX_STR_LEN)) {
            if (RT2MessageFactory.fromJSONString(sResponse).getType() == RT2Protocol.PING) {
                // we directly put the pong answer to the out queue - there is
                // no reason to bother higher-levels with this low-level part
                // of the rt2 protocol
                m_outputMessageLoop.enqueue(RT2MessageFactory.toJSONString(RT2MessageFactory.newMessage(RT2Protocol.PONG)));

                return;
            }
        }

        m_inputMessageLoop.enqueue(sResponse);
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param exceptionHandler
     */
    void setMessageExceptionHandler(final RT2MessageExceptionHandler exceptionHandler) {
        m_outputMessageLoop.setMessageExceptionHandler(exceptionHandler);
        m_inputMessageLoop.setMessageExceptionHandler(exceptionHandler);
    }

    /**
     * @return
     * @throws Exception
     */
    public String getChannelId() throws Exception {
        return m_channelId;
    }

    /**
     * @param sClientUID
     * @param aRequest
     * @throws Exception
     */
    public void sendRequest(final String sClientUID, final RT2Message aRequest) throws Exception {
        m_outputMessageLoop.enqueue(RT2MessageFactory.toJSONString(aRequest));
    }

    /**
     * @param sClientUID
     * @param iMsgHandler
     * @throws Exception
     */
    public void registerClient(final String sClientUID, final IRT2MessageHandler iMsgHandler) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("register RT2 client '" + sClientUID + "' ...").setVar("channel-id", m_channelId).log();

        // m_clientRegistry is a SynchronizedMap
        m_clientRegistry.put(sClientUID, iMsgHandler);
    }

    /**
     * @param sClientUID
     * @throws Exception
     */
    public void unregisterClient(final String sClientUID) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("unregister RT2 client '" + sClientUID + "' ...").setVar("channel-id", m_channelId).log();

        // m_clientRegistry is a SynchronizedMap
        m_clientRegistry.remove(sClientUID);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param sClientUID
     * @param aResponse
     * @throws Exception
     */
    private void impl_notifyClient(final String sClientUID, final RT2Message aResponse) throws Exception {
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("notify ...").setVar("com.openexchange.rt2.client.uid", sClientUID).setVar("response", aResponse).log();

        final IRT2MessageHandler iClient = m_clientRegistry.get(sClientUID);
        final String sMsgType = aResponse.getType();

        //  MsgStats.get().statMsg(aResponse, MsgStats.LOC_WS_CLIENT_IN);

        // Don't log messages which are connected to the low-level ACK/NACK system which
        // could be received after the we already removed a client from the shared ws-connection!
        if ((iClient == null) && (!impl_isAckNackMessage(sMsgType))) {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("... callback for '" + sClientUID + "' is gone. Drop response message.").setVar("Msg-Type", sMsgType).log();
            return;
        }

        if (null != iClient) {
            iClient.onMessage(this, aResponse);
        } else {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("... callback for '" + sClientUID + "' is not possible due to missing/null client.").setVar("Msg-Type", sMsgType).log();
        }
    }

    /**
     * @param msgType
     * @return
     */
    private boolean impl_isAckNackMessage(String msgType) {
        return (RT2Protocol.ACK_SIMPLE.equals(msgType) || RT2Protocol.NACK_SIMPLE.equals(msgType));
    }

    // - Members ---------------------------------------------------------------

    final private static Logger LOG = Slf4JLogger.create(RT2Channel.class);

    final private String m_channelId = UUID.randomUUID().toString();

    final private Map<String, IRT2MessageHandler> m_clientRegistry = Collections.synchronizedMap(new HashMap<>());

    final private OXWSClient m_wsClient;

    final private RT2MessageLoop m_outputMessageLoop;

    final private RT2MessageLoop m_inputMessageLoop;
}
