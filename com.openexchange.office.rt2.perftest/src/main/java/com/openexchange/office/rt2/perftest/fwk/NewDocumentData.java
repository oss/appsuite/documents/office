/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import org.json.JSONArray;

//-------------------------------------------------------------------------
public class NewDocumentData
{
	private String    m_sFileName;
	private String    m_sFileId;
	private String    m_sHtmlDoc;
	private JSONArray m_aOperations;

    //-------------------------------------------------------------------------
	public NewDocumentData(String sFileId)
	{
		m_sFileId = sFileId;
	}

    //-------------------------------------------------------------------------
	public NewDocumentData(String sFileName, String sFileId, String sHtmlDoc, JSONArray aOperations)
	{
		m_sFileName   = sFileName;
		m_sFileId     = sFileId;
		m_sHtmlDoc    = sHtmlDoc;
		m_aOperations = aOperations;
	}

	public String    getFileName() { return m_sFileName; }
	public String    getFileId() { return m_sFileId; }
	public String    getHtmlString() { return m_sHtmlDoc; }
	public JSONArray getFastEmptyOperations() { return m_aOperations; }
}
