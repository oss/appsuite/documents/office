/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public abstract class TestCaseBase implements Callable<Throwable> {

    /**
     * Initializes a new {@link TestCaseBase}.
     *
     * @throws Exception
     */
    public TestCaseBase() throws Exception {
        // nothing to do
    }

    // - Callable --------------------------------------------------------------

    /**
    *
    */
    @Override
    public Throwable call() {
        m_testRunNumber = GLOBAL_TESTRUN_COUNTER.incrementAndGet();

        try {
            final Thread aThisThread = Thread.currentThread();
            aThisThread.setName("TestCaseThread " + m_runConfig.sTestId);

            m_statusLine.countTestStart();

            setUpTest();
            runTest();
            tearDownTest();

            m_statusLine.countTestEnd();
        } catch (Throwable ex) {
            final String configString = m_runConfig.toString();
            final Exception excp = new Exception("exception in env : " + configString, ex);

            setExceptionError(excp);
            m_statusLine.countTestFailed();

            LOG.forLevel(ELogLevel.E_ERROR).withError(excp).log();

            return excp;
        }

        return getLastError();
    }

    // - Object -----------------------------------------------------------------

    /**
    *
    */
    @Override
    public String toString() {
        final StringBuilder sString = new StringBuilder(256);

        sString.append(super.toString());
        sString.append(" {");
        sString.append("config=" + m_runConfig);
        sString.append("}");

        return sString.toString();
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param aRunConfig
     * @throws Exception
     */
    public void bind(final TestRunConfig aRunConfig) throws Exception {
        m_runConfig = aRunConfig;
    }

    /**
     * @param aStatusLine
     * @throws Exception
     */
    public void bind(final StatusLine aStatusLine) throws Exception {
        m_statusLine = aStatusLine;
    }

    /**
     * @return
     * @throws Exception
     */
    public int calculateTestIterations() throws Exception {
        return 1;
    }

    /**
     * @param rt2
     */
    public void enableMessageExceptionHandling(final RT2 rt2) throws Exception {
        if (null != rt2) {
            rt2.setMessageExceptionHandler((source, exception) -> {
                final TestCaseMonitor testCaseMonitor = TestCaseMonitor.get();

                setExceptionError(exception);

                if (null != testCaseMonitor) {
                    testCaseMonitor.setTestCaseError(this);
                } else {
                    m_statusLine.countTestFailed();
                    LOG.forLevel(ELogLevel.E_ERROR).withError(exception).log();
                }
            });
        }
    }

    /**
     * @return
     */
    public synchronized Throwable getLastError() {
        return (null != m_exceptionError) ? m_exceptionError : m_runError;
    }

    /**
     * @param testRunError
     */
    protected synchronized void setExceptionError(Throwable exceptionError) {
        m_exceptionError = exceptionError;
    }

    /**
     * @param runError
     */
    protected synchronized void setRunError(Throwable runError) {
        m_runError = runError;
    }

    /**
     * @throws Exception
     */
    protected void setUpTest() throws Exception {
        // can be overwritten if needed
    }

    /**
     * @throws Exception
     */
    protected abstract void runTest() throws Exception;

    /**
     * @throws Exception
     */
    protected void tearDownTest() throws Exception {
        // can be overwritten if needed
    }

    /**
     * @return
     * @throws Exception
     */
    protected TestConfig getGlobalConfig() {
        return m_runConfig.rGlobalConfig;
    }

    /**
     * @return
     * @throws Exception
     */
    protected TestRunConfig getRunConfig() {
        return m_runConfig;
    }

    /**
     * @return
     * @throws Exception
     */
    protected StatusLine getStatusLine() {
        return m_statusLine;
    }

    /**
     * @return
     */
    protected long getTestRunNumber() {
        return m_testRunNumber;
    }

    // - Members ---------------------------------------------------------------

    final protected static AtomicLong GLOBAL_TESTRUN_COUNTER = new AtomicLong(0);

    final private static Logger LOG = Slf4JLogger.create(TestCaseBase.class);

    volatile private long m_testRunNumber;

    private TestRunConfig m_runConfig = null;

    private StatusLine m_statusLine = null;

    private Throwable m_exceptionError = null;

    private Throwable m_runError = null;
}
