/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

//=============================================================================
public class DocUser
{
    //-------------------------------------------------------------------------
    public static final String KEY_ID      = "id"       ;
    public static final String KEY_USER_ID = "clientUID";

    //-------------------------------------------------------------------------
    public DocUser ()
    {}
    
    //-------------------------------------------------------------------------
    public static DocUser create (final JSONObject aJSON)
        throws Exception
    {
        final DocUser aUser = new DocUser ();
        aUser.setJSON (aJSON);
        return aUser;
    }

    //-------------------------------------------------------------------------
    public synchronized void setJSON (final JSONObject aJSON)
        throws Exception
    {
        m_aJSON = aJSON;
    }
    
    //-------------------------------------------------------------------------
    public synchronized JSONObject getJSON ()
        throws Exception
    {
        if (m_aJSON == null)
            m_aJSON = new JSONObject ();
        return m_aJSON;
    }

    //-------------------------------------------------------------------------
    public synchronized String getUniqueKey ()
        throws Exception
    {
        return StringUtils.join (Integer.toString(getId ()), "", getUserId ());
    }
    
    //-------------------------------------------------------------------------
    public synchronized int getId ()
        throws Exception
    {
        return getJSON ().getInt(KEY_ID);
    }

    //-------------------------------------------------------------------------
    public synchronized String getUserId ()
        throws Exception
    {
        return getJSON ().getString(KEY_USER_ID);
    }
    
    //-------------------------------------------------------------------------
    @Override
    public String toString ()
    {
        try
        {
            final StringBuilder sString = new StringBuilder (256);
            sString.append (super.toString ()        );
            sString.append (" : id="    +getId     ());
            sString.append (", user-id="+getUserId ());
            return sString.toString ();
        }
        catch (Throwable ex)
        {
            return super.toString ();
        }
    }

    //-------------------------------------------------------------------------
    private JSONObject m_aJSON = null;
}