/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config.items;

import com.openexchange.office.rt2.perftest.config.ConfigAccess;
import com.openexchange.office.rt2.perftest.config.ConfigUtils;
import com.openxchange.office_communication.tools.config.IComplexConfiguration;

//=============================================================================
public class LoadBalanceTestConfig
{
    //-------------------------------------------------------------------------
    public static final String CONFIG_PACKAGE = "/loadbalance";

    //-------------------------------------------------------------------------
    public static final int DEFAULT_REPITIONS =    1;
    public static final int DEFAULT_DELAY     = 1000;

    //-------------------------------------------------------------------------
    public LoadBalanceTestConfig ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    public Integer getRepetitions ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.loadbalance.repetitions", String.class), DEFAULT_REPITIONS);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getDelayInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.loadbalance.delay", String.class), DEFAULT_DELAY);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    private final IComplexConfiguration mem_Config ()
        throws Exception
    {
    	if (m_iConfig == null)
    		m_iConfig = ConfigAccess.accessConfig(CONFIG_PACKAGE);
    	return m_iConfig;
    }

    //-------------------------------------------------------------------------
    private IComplexConfiguration m_iConfig = null;
}
