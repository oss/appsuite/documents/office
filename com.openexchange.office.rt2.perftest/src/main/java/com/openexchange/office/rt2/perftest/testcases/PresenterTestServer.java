/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.lang3.time.DurationFormatUtils;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.config.items.PresenterTestConfig;
import com.openexchange.office.rt2.perftest.doc.presenter.IPresenterDoc;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDoc;
import com.openexchange.office.rt2.perftest.fwk.ISessionAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorClient;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorConst;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.tools.common.concurrent.SimpleCondition;

//=============================================================================

public class PresenterTestServer extends PresenterTestBase {
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(PresenterTestServer.class);

    //-------------------------------------------------------------------------
    public PresenterTestServer () throws Exception {
    	super();
    }

    //-------------------------------------------------------------------------
    @Override
    public int calculateTestIterations () throws Exception {
        final PresenterTestConfig aTestCfg    = mem_PresenterConfig ();
        final int                 nIterations = aTestCfg.getSlideChanges();
        return nIterations;
    }

    //-------------------------------------------------------------------------
    @Override
    public void runTest () throws Exception {
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("start presenter (server) ...")
            .log        ();

        final Mutable< PresenterDoc   > aDocRef     = new MutableObject< PresenterDoc   >();
        final Mutable< IPresenterDoc  > iDocRef     = new MutableObject< IPresenterDoc  >();
        final Mutable< ISessionAPI    > iSessionRef = new MutableObject< ISessionAPI    >();
        final Mutable< UserDescriptor > aUserRef    = new MutableObject< UserDescriptor >();

        getTestDocAndEnv (aDocRef, iDocRef, iSessionRef, aUserRef);

        final PresenterDoc   aDoc     = aDocRef    .getValue ();
        final IPresenterDoc  iDoc     = iDocRef    .getValue ();
        final ISessionAPI    iSession = iSessionRef.getValue ();
        final UserDescriptor aUser    = aUserRef   .getValue ();

        iSession.login (aUser);

        final SimpleCondition aJoinSync = impl_addObserverAndProvideSyncForJoiningClients(aDoc);

        iDoc.open ();

        iDoc.startRemotePresentation ();

        impl_waitForJoiningClients (aDoc, aJoinSync);

        impl_changeSlides (aDoc);

        iDoc.leavePresentation();

        impl_waitForLeavingClients (aDoc);

        iDoc.endRemotePresentation ();
        iDoc.close ();

        iSession.logout ();
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ SimpleCondition impl_addObserverAndProvideSyncForJoiningClients(final PresenterDoc aDoc)  throws Exception {
        final SimpleCondition     aJoinSync                 = new SimpleCondition ();
        final PresenterTestConfig aTestCfg                  = mem_PresenterConfig ();
        final int                 nConfiguredClients        = aTestCfg.getClientCount();
        final int                 nExpectedDocUserAfterJoin = nConfiguredClients + 1; // our own 'user' is part of that list too !

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("add observer for ["+nConfiguredClients+"] joining clients ...")
            .log        ();

        aDoc.trackClientsJoinedPresentation(nExpectedDocUserAfterJoin, aJoinSync);
        return aJoinSync;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_waitForJoiningClients (final PresenterDoc aDoc, final SimpleCondition aJoinSync) throws Exception {
        final long nStart = System.currentTimeMillis();
        while (true)
        {
            final boolean bOK = aJoinSync.await(10, TimeUnit.SECONDS);
            if (bOK)
                break;

            final long   nNow  = System.currentTimeMillis();
            final long   nTime = (nNow - nStart);
            final String sTime = DurationFormatUtils.formatDuration(nTime, "HH:mm:ss.SSS");

            LOG .forLevel   (ELogLevel.E_INFO)
                .withMessage("... ["+sTime+"] wait further for joining clients")
                .log        ();
        }

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... all expected clients joined the presentation")
            .log        ();
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_waitForLeavingClients (final PresenterDoc aDoc) throws Exception {
        final PresenterTestConfig aTestCfg                   = mem_PresenterConfig ();
        final int                 nConfiguredClients         = aTestCfg.getClientCount();
        final int                 nExpectedDocUserAfterLeave = 1; // our own 'user' is part of that list too !
        final SimpleCondition     aLeaveSync                 = new SimpleCondition ();

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("wait for ["+nConfiguredClients+"] leaving clients ...")
            .log        ();

        aDoc.waitUntilClientsLeftPresentation(nExpectedDocUserAfterLeave, aLeaveSync);

        final long nStart = System.currentTimeMillis();
        while (true)
        {
            final boolean bOK = aLeaveSync.await(10, TimeUnit.SECONDS);
            if (bOK)
                break;

            final long   nNow  = System.currentTimeMillis();
            final long   nTime = (nNow - nStart);
            final String sTime = DurationFormatUtils.formatDuration(nTime, "HH:mm:ss.SSS");

            LOG .forLevel   (ELogLevel.E_INFO)
                .withMessage("... ["+sTime+"] wait further for leaving clients")
                .log        ();
        }
    
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... all clients left the presentation")
            .log        ();
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_changeSlides (final PresenterDoc aDoc) throws Exception {
        final RT2MonitorClient    aMonitor           = RT2MonitorClient.get         ();
        final String              sClientUID         = aDoc.getRT2ClientUID         ();
        final PresenterTestConfig aTestCfg           = mem_PresenterConfig          ();
        final int                 nSlideCount        = aTestCfg.getSlideCount       ();
        final int                 nSlideChanges      = aTestCfg.getSlideChanges     ();
        final long                nSlideChangesDelay = aTestCfg.getSlideChangesDelay();
        final StatusLine          aStatusLine        = getStatusLine                ();

        if (nSlideCount < 1) {
            LOG .forLevel   (ELogLevel.E_ERROR)
                .withMessage("Configuration issue ? slide-count is set to " + nSlideCount)
                .log        ();
            return;
        }

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("start presentation with changing slides : " + nSlideChanges +" changes of " + nSlideCount + " slides ...")
            .log        ();

        int nSlide = 0;
        for (int nChange=0; nChange<nSlideChanges; ++nChange) {
            nSlide++;

            if (nSlide > nSlideCount)
                nSlide = 1;

            LOG .forLevel   (ELogLevel.E_INFO)
                .withMessage("... change slide : "+nSlide+" of "+nSlideCount+" [iteration "+nChange+" of "+nSlideChanges+"]")
                .log        ();

            aStatusLine.countTestIteration();

            aMonitor.record(
                RT2MonitorConst.SCOPE_PRESENTER,
                RT2MonitorConst.KEY_PRESENTER_SERVER_SLIDE_CHANGED,
                null,
                RT2MonitorConst.DATAKEY_CLIENT_UID  , sClientUID,
                RT2MonitorConst.DATAKEY_ACTIVE_SLIDE, nSlide
            );

            aDoc.changeSlide(nSlide);

            Thread.sleep (nSlideChangesDelay);
        }

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("OK")
            .log        ();
    }

}
