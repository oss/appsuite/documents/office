/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import org.apache.commons.lang3.time.DurationFormatUtils;

//=============================================================================
public class StatusLine
{
    //-------------------------------------------------------------------------
    public static final int LINE_COUNT = 6;

    //-------------------------------------------------------------------------
    private StatusLine ()
    {
        m_nStartTime = System.currentTimeMillis();
    }

    //-------------------------------------------------------------------------
    public static synchronized void setEnabled (final boolean bEnabled)
    /* no throws Exception */
    {
        m_bEnabled = bEnabled;
    }

    //-------------------------------------------------------------------------
    public static synchronized StatusLine get ()
   {
        if (m_gSingleton == null)
            m_gSingleton = new StatusLine ();
        return m_gSingleton;
    }

    //-------------------------------------------------------------------------
    public synchronized void setTestCount (final int nCount)
    {
        m_nTestCount = nCount;
    }

    //-------------------------------------------------------------------------
    public synchronized void setMaxTestIterations (final int nIterations)
    {
        m_nMaxTestIterations = nIterations;
    }

    //-------------------------------------------------------------------------
    public synchronized void countTestIteration ()
    {
        m_nActTestIteration ++;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countTestIteration (int add)
    {
        m_nActTestIteration += add;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized int getTestIteration ()
    {
        return m_nActTestIteration;
    }

    //-------------------------------------------------------------------------
    public synchronized void countTestStart ()
        /* no throws Exception */
    {
        m_nTestsRunning ++;

        if (m_nTestsRunning > m_nTestsRunningPeak)
            m_nTestsRunningPeak = m_nTestsRunning;

        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countTestEnd ()
    /* no throws Exception */
    {
        m_nTestsOK      ++;
        m_nTestsRunning --;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countTestFailed ()
    /* no throws Exception */
    {
        if (m_nFirstErrorAt < 1)
            m_nFirstErrorAt = m_nTestsRunning;

        m_nTestsRunning --;
        m_nTestsFailed  ++;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countTimeout (final int nLevel)
    /* no throws Exception */
    {
        m_nTimeouts[nLevel]++;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countError ()
    /* no throws Exception */
    {
        m_nTestErrors++;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countDocOpen ()
    /* no throws Exception */
    {
        m_nDocsOpen     ++;
        if (m_nDocsOpen > m_nDocsOpenPeak)
            m_nDocsOpenPeak = m_nDocsOpen;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void countDocClose ()
    /* no throws Exception */
    {
        m_nDocsOpen     --;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void setTestCaseErrorCount(final int testCaseErrorCount)
    {
        m_testCaseErrorCount = testCaseErrorCount;
        impl_printState (true);
    }

    //-------------------------------------------------------------------------
    public synchronized void printMessage (final String sMessage)
    /* no throws Exception */
    {
        m_sMessage = sMessage;
        impl_printState (false);
    }

    //-------------------------------------------------------------------------
    public synchronized void printErrorMessage (final String sErrorMessage)
    /* no throws Exception */
    {
        m_sMessage = sErrorMessage;
        impl_printState (true);
    }

    //-------------------------------------------------------------------------
    public synchronized void update ()
    /* no throws Exception */
    {
        impl_printState (true);
    }

    //-------------------------------------------------------------------------
    private void impl_printState (boolean force)
    /* no throws Exception */
    {
        if ( ! m_bEnabled)
            return;

        final long nNow      = System.currentTimeMillis();
        final long nTimeDiff = nNow - m_nLastRedrawTimestamp;

        if ((nTimeDiff < 1000) && !force)
            return;

        m_nLastRedrawTimestamp = nNow;

        try {
            final long nRunTime = nNow - m_nStartTime;
            final String sRunTime = DurationFormatUtils.formatDuration(nRunTime, "HH:mm:ss");
            final StringBuilder sTestLine = new StringBuilder(256);
            final StringBuilder sDocLine = new StringBuilder(256);
            final StringBuilder sIterationsLine = new StringBuilder(256);
            final StringBuilder sMonitorLine = new StringBuilder(256);
            final StringBuilder sTimeLine = new StringBuilder(256);
            final StringBuilder sMsgLine = new StringBuilder(256);

            sTestLine.append("----- : Tests       ");
            sTestLine.append(" : Configured : ");
            sTestLine.append(m_nTestCount);
            sTestLine.append(" : Running : ");
            sTestLine.append(m_nTestsRunning);
            sTestLine.append(" : OK : ");
            sTestLine.append(m_nTestsOK);
            sTestLine.append(" : Failed : ");
            sTestLine.append(m_nTestsFailed);
            sTestLine.append(" : Errors : ");
            sTestLine.append(m_nTestErrors);
            sTestLine.append(" : Peak (running) : ");
            sTestLine.append(m_nTestsRunningPeak);
            sTestLine.append(" : First Error at : ");
            sTestLine.append(m_nFirstErrorAt);

            sDocLine.append("----- : Docs        ");
            sDocLine.append(" : Open : ");
            sDocLine.append(m_nDocsOpen);
            sDocLine.append(" : Peak : ");
            sDocLine.append(m_nDocsOpenPeak);

            sIterationsLine.append("----- : Iterations  ");
            sIterationsLine.append(" : ");
            sIterationsLine.append(m_nActTestIteration);
            sIterationsLine.append(" / ");
            sIterationsLine.append(m_nMaxTestIterations);
            sIterationsLine.append(" : Timeouts :");

            for (int nTimeout : m_nTimeouts)
                sIterationsLine.append(" " + nTimeout);

            sMonitorLine.append("----- : Monitor     ");
            sMonitorLine.append(" : Errors : ");
            sMonitorLine.append(m_testCaseErrorCount);

            sTimeLine.append("----- : Time        ");
            sTimeLine.append(" : ");
            sTimeLine.append(sRunTime);

            sMsgLine.append("----- : Message     ");
            sMsgLine.append(" : ");
            sMsgLine.append(m_sMessage);

            mem_StateLine().print(sTestLine.toString(), sDocLine.toString(), sIterationsLine.toString(), sMonitorLine.toString(), sTimeLine.toString(), sMsgLine.toString());
        } catch (Throwable ex) {
            ex.printStackTrace(System.err);
        }
    }

    //-------------------------------------------------------------------------
    private synchronized FixStatusPrinter mem_StateLine ()
        throws Exception
    {
        if (m_aStateLine == null)
        {
            m_aStateLine = new FixStatusPrinter ();
            m_aStateLine.setLineCount(LINE_COUNT);
        }
        return m_aStateLine;
    }

    //-------------------------------------------------------------------------
    private static StatusLine m_gSingleton = null;

    //-------------------------------------------------------------------------
    private static boolean m_bEnabled = true;

    //-------------------------------------------------------------------------
    private int m_nTestCount = 0;

    //-------------------------------------------------------------------------
    private int m_nTestsRunning = 0;

    //-------------------------------------------------------------------------
    private int m_nTestsOK = 0;

    //-------------------------------------------------------------------------
    private int m_nTestsFailed = 0;

    //-------------------------------------------------------------------------
    private int m_nTestErrors = 0;

    //-------------------------------------------------------------------------
    private int m_nTestsRunningPeak = 0;

    //-------------------------------------------------------------------------
    private int[] m_nTimeouts = new int[] {0, 0, 0, 0, 0};

    //-------------------------------------------------------------------------
    private int m_nMaxTestIterations = 0;

    //-------------------------------------------------------------------------
    private int m_nActTestIteration = 0;

    //-------------------------------------------------------------------------
    private int m_nFirstErrorAt = 0;

    //-------------------------------------------------------------------------
    private int m_nDocsOpen = 0;

    //-------------------------------------------------------------------------
    private int m_nDocsOpenPeak = 0;

    //-------------------------------------------------------------------------
    private int m_testCaseErrorCount = 0;

    //-------------------------------------------------------------------------
    private long m_nStartTime = 0;

    //-------------------------------------------------------------------------
    private long m_nLastRedrawTimestamp = System.currentTimeMillis();

    //-------------------------------------------------------------------------
    private String m_sMessage = "";

    //-------------------------------------------------------------------------

    private FixStatusPrinter m_aStateLine = null;
}
