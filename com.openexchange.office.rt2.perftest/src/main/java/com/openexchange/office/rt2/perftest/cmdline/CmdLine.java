/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.cmdline;

import net.as_development.asdk.tools.commandline.CommandLineBase;

import com.openexchange.office.rt2.perftest.impl.EOperation;
import com.openexchange.office.rt2.protocol.RT2Protocol;

//=============================================================================
public class CmdLine extends CommandLineBase
{
    //-------------------------------------------------------------------------
    public static final String OPT_SHORT_CONFIG_PATH = "cp";
    public static final String OPT_SHORT_OPERATION   = "op";
    public static final String OPT_SHORT_REPORT_PATH = "rp";

    public static final String OPT_LONG_CONFIG_PATH  = "config-path";
    public static final String OPT_LONG_OPERATION    = "operation"  ;
    public static final String OPT_LONG_REPORT_PATH  = "report-path";

    //-------------------------------------------------------------------------
    public CmdLine()
    	throws Exception
    {
    	super("office-performance-test (v" + RT2Protocol.PROTOCOL_VERSION+")");

    	addOption (OPT_SHORT_CONFIG_PATH                ,
    			   OPT_LONG_CONFIG_PATH                 ,
    			   CommandLineBase.HAS_VALUE            ,
    			   CommandLineBase.REQUIRED             ,
    			   "the path where config files exists");

        addOption (OPT_SHORT_REPORT_PATH                ,
                   OPT_LONG_REPORT_PATH                 ,
                   CommandLineBase.HAS_VALUE            ,
                   CommandLineBase.NOT_REQUIRED         ,
                   "the path where report files will be generated");

        addOption (OPT_SHORT_OPERATION                  ,
    			   OPT_LONG_OPERATION                   ,
    			   CommandLineBase.HAS_VALUE            ,
    			   CommandLineBase.REQUIRED             ,
    			   "define which operation should run\n"+
    			   "operations : {"+EOperation.STR_PREPARE_TESTENV+", "+EOperation.STR_RUN_TESTS+"}");
    }

    //-------------------------------------------------------------------------
    public String getConfigPath ()
        throws Exception
    {
    	return getOptionValue(OPT_SHORT_CONFIG_PATH);
    }

    //-------------------------------------------------------------------------
    public String getReportPath ()
        throws Exception
    {
        return getOptionValue(OPT_SHORT_REPORT_PATH);
    }

    //-------------------------------------------------------------------------
    public EOperation getOperation ()
        throws Exception
    {
    	final String     sOp = getOptionValue(OPT_SHORT_OPERATION);
    	final EOperation eOp = EOperation.fromString (sOp);
    	return eOp;
    }
}
