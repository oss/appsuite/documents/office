/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.config.items.PresenterTestConfig;
import com.openexchange.office.rt2.perftest.doc.DocEvent;
import com.openexchange.office.rt2.perftest.doc.presenter.IPresenterDoc;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDoc;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDocEvent;
import com.openexchange.office.rt2.perftest.fwk.ISessionAPI;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorClient;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorConst;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

import junit.framework.Assert;
import net.as_development.asdk.tools.common.concurrent.SimpleCondition;
import net.as_development.asdk.tools.common.pattern.observation.Observer;

//=============================================================================
public class PresenterTestClient extends PresenterTestBase
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(PresenterTestClient.class);

    private static final AtomicLong START_ORDER = new AtomicLong(0);

    //-------------------------------------------------------------------------
    private PresenterDoc m_aDoc = null;

    //-------------------------------------------------------------------------
    private AtomicInteger m_slideChangesReceived = new AtomicInteger(0);;

    //-------------------------------------------------------------------------
    private AtomicLong m_lastSlideChangedReceived = new AtomicLong(0);;

    //-------------------------------------------------------------------------
    private SimpleCondition m_aShutdownSync = null;

    //-------------------------------------------------------------------------
    private SimpleCondition m_aSlideChangesReached = new SimpleCondition ();

    //-------------------------------------------------------------------------
    private long m_runNumber = 0;

    //-------------------------------------------------------------------------
    public PresenterTestClient ()
        throws Exception
    {
        super();
    }

    //-------------------------------------------------------------------------
    @Override
    public int calculateTestIterations ()
        throws Exception
    {
        final PresenterTestConfig aTestCfg    = mem_PresenterConfig ();
        final int                 nIterations = aTestCfg.getSlideChanges();
        return nIterations;
    }

    //-------------------------------------------------------------------------
    @Override
    public void runTest ()
        throws Exception {
        final TestRunConfig aRunConfig = getRunConfig();

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("start presenter (client : "+aRunConfig.sTestId+") ...")
            .log        ();

        m_runNumber = START_ORDER.incrementAndGet();

        final Mutable< PresenterDoc   > aDocRef     = new MutableObject< PresenterDoc   >();
        final Mutable< IPresenterDoc  > iDocRef     = new MutableObject< IPresenterDoc  >();
        final Mutable< ISessionAPI    > iSessionRef = new MutableObject< ISessionAPI    >();
        final Mutable< UserDescriptor > aUserRef    = new MutableObject< UserDescriptor >();

        getTestDocAndEnv (aDocRef, iDocRef, iSessionRef, aUserRef);

        final PresenterDoc   aDoc     = aDocRef    .getValue ();
        final ISessionAPI    iSession = iSessionRef.getValue ();
        final UserDescriptor aUser    = aUserRef   .getValue ();

        final int     nSlideChanges        = mem_PresenterConfig().getSlideChanges();
        final long    nTimeoutApply        = mem_PresenterConfig().getTimeout4ApplyInMS();
        final boolean bContinueOnTimeout   = mem_PresenterConfig().getContinueOnTimeout();
        final int     nExpectedClientCount = mem_PresenterConfig().getClientCount();

        m_aDoc = aDoc;

        impl_monitorSlideChanges(nSlideChanges);

        iSession.login(aUser);

        impl_joinPresentation();

        m_lastSlideChangedReceived.set(System.currentTimeMillis());

        int slideChangesReceived = m_slideChangesReceived.get();
        while (!m_aSlideChangesReached.await(1000, TimeUnit.MILLISECONDS)) {
            int currentSlideChangesReceived = m_slideChangesReceived.get();

            final long now = System.currentTimeMillis();
            final boolean expectedActiveUsers = (aDoc.getActiveUsers() == nExpectedClientCount);
            final boolean slideChangedReceived = (currentSlideChangesReceived > slideChangesReceived);
            final long timeSpanSinceLastSlideChangeEvent = Math.max(0, (now - m_lastSlideChangedReceived.get()));

            if (expectedActiveUsers && !slideChangedReceived && (timeSpanSinceLastSlideChangeEvent >  nTimeoutApply)) {
                LOG.forLevel(ELogLevel.E_ERROR)
                   .withMessage("waiting for slideChanged broadcast timed out ...")
                   .setVar     ("instance"      , aDoc.getInstanceUID())
                   .setVar     ("timeout"       , nTimeoutApply        )
                   .setVar     ("doc-type"      , aDoc.getType()       )
                   .setVar     ("folder-id"     , aDoc.getFolderId   ())
                   .setVar     ("file-id"       , aDoc.getFileId     ())
                   .setVar     ("drive-id"      , aDoc.getDriveDocId ())
                   .setVar     ("rt2-com.openexchange.rt2.client.uid", aDoc.getRT2ClientUID  ())
                   .log        ();

                if ( ! bContinueOnTimeout)
                    Assert.fail("waiting for slideChanged broadcast timed out ...");
            }
            slideChangesReceived = m_slideChangesReceived.get();
        }

        impl_leavePresentation();

        mem_ShutdownSync().await();

        iSession.logout();
    }

    //-------------------------------------------------------------------------
    private void impl_monitorSlideChanges (int nExpectedSlideChanges) throws Exception {
        final RT2MonitorClient aMonitor    = RT2MonitorClient.get();
        final String           sClientUID  = m_aDoc.getRT2ClientUID();
        final StatusLine       aStatusLine = getStatusLine ();

        m_aDoc.addObserver(new Observer< DocEvent >() {
            @Override
            public /* no synchronized */ void notify(final DocEvent aEvent) throws Exception {
                if (aEvent.isEvent(PresenterDocEvent.EVENT_ACTIVE_SLIDE_CHANGED)) {
                    final Integer nActiveSlide = aEvent.getData();

                    m_slideChangesReceived.incrementAndGet();
                    m_lastSlideChangedReceived.set(System.currentTimeMillis());

                    aMonitor.record(
                        RT2MonitorConst.SCOPE_PRESENTER,
                        RT2MonitorConst.KEY_PRESENTER_CLIENT_SLIDE_CHANGED,
                        null,
                        RT2MonitorConst.DATAKEY_CLIENT_UID  , sClientUID,
                        RT2MonitorConst.DATAKEY_ACTIVE_SLIDE, nActiveSlide
                    );

                    aStatusLine.countTestIteration();

                    if (m_slideChangesReceived.get() == nExpectedSlideChanges) {
                        m_aSlideChangesReached.set();
                    }
                }
            }
        });
    }

    //-------------------------------------------------------------------------
    private void impl_joinPresentation () throws Exception {
        final long nNextJoin = impl_nextJoinTime ();

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("presenter client : join presentation in "+nNextJoin+" ms ...")
            .log        ();

        Thread.sleep (nNextJoin);

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("presenter client : join presentation now ...")
            .log        ();

        m_aDoc.open             ();
        m_aDoc.joinPresentation ();
    }

    //-------------------------------------------------------------------------
    private void impl_leavePresentation ()
        throws Exception
    {
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("presenter client : leave presentation now ...")
            .log        ();

        final long cfgDelay = mem_PresenterConfig().getLeavePresentationDelayInMS();
        final long delay = this.m_runNumber * cfgDelay;

        Thread.sleep(delay);

        m_aDoc.leavePresentation ();
        m_aDoc.close             ();

        // inform 'main thread' presentation is over and we can logout user and close this test ;-)
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("presenter client : trigger shutdown ...")
            .log        ();

        mem_ShutdownSync ().set();
    }

    //-------------------------------------------------------------------------
    private long impl_nextJoinTime ()
        throws Exception
    {
        final PresenterTestConfig aTestCfg       = mem_PresenterConfig ();
        final long                nTimeFrameInMS = aTestCfg.getJoinPresentationTimeFrameInMS();
        final long                nNextJoin      = ThreadLocalRandom.current().nextLong(0, nTimeFrameInMS);
        return nNextJoin;
    }

    //-------------------------------------------------------------------------
    private synchronized SimpleCondition mem_ShutdownSync ()
        throws Exception
    {
        if (m_aShutdownSync == null)
            m_aShutdownSync = new SimpleCondition ();
        return m_aShutdownSync;
    }

}
