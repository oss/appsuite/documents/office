/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.lang.ref.WeakReference;
import org.apache.commons.lang3.StringUtils;

//=============================================================================
public class DocEvent
{
    //-------------------------------------------------------------------------
    public static final String EVENT_OSN_CHANGED              = "osn-changed"             ; // data=[int]=osn
    public static final String EVENT_ACTIVE_USER_LIST_CHANGED = "active-user-list-changed"; // data=[DocUserRegistry]=reference to document bound doc user registry
    public static final String EVENT_EDITOR_CHANGED           = "editor-changed"          ; // data=[String]=com.openexchange.rt2.client.uid of editor

    //-------------------------------------------------------------------------
    public static < T > DocEvent create (final String sEvent,
                                         final T      aData )
        throws Exception
    {
        final DocEvent aEvent = new DocEvent ();
        aEvent.m_sEvent = sEvent;
        aEvent.m_rData  = new WeakReference<>(aData);
        return aEvent;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ boolean isEvent (final String sEvent)
        throws Exception
    {
        return StringUtils.equals(m_sEvent, sEvent);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ String getEvent ()
        throws Exception
    {
        return m_sEvent;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public /* no synchronized */< T > T getData ()
        throws Exception
    {
        return (T) m_rData.get ();
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ String toString ()
    {
        final StringBuilder sString = new StringBuilder (256);
        sString.append (super.toString ()             );
        sString.append (" : '" + m_sEvent       + "' ");
        sString.append (" : "  + m_rData.get () + " " );
        return sString.toString ();
    }

    //-------------------------------------------------------------------------
    private String m_sEvent = null;

    //-------------------------------------------------------------------------
    private WeakReference< Object > m_rData = null;
}
