/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
// http://oxpedia.org/wiki/index.php?title=HTTP_API
// http://oxpedia.org/wiki/index.php?title=OXSessionLifecycle
public class OXConnection {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXConnection.class);

    //-------------------------------------------------------------------------
    public static final String DEFAULT_ENCODING = "utf-8";

    //-------------------------------------------------------------------------
    public static final String CONTENT_TYPE_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_TEXT_JAVASCRIPT = "text/javascript";

    //-------------------------------------------------------------------------
    private OXAppsuite m_aAppsuite = null;
    private TestRunConfig m_aConfig = null;
    private HttpClient m_aClient = null;
    private HttpClientContext m_aContext = null;
    private CookieStore m_aCookieStore = null;
    private boolean m_useAlternativeHost = false;

    //-------------------------------------------------------------------------
    public OXConnection() throws Exception {}

    //-------------------------------------------------------------------------
    public OXConnection(boolean useAlternativeHost) throws Exception {
        m_useAlternativeHost = useAlternativeHost;
    }

    //-------------------------------------------------------------------------
    public boolean usesAlternativeHost() {
        return m_useAlternativeHost;
    }

    //-------------------------------------------------------------------------
    public synchronized void bind(final OXAppsuite aAppsuite) throws Exception {
        m_aAppsuite = aAppsuite;
        m_aConfig = aAppsuite.accessConfig();
    }

    //-------------------------------------------------------------------------
    protected synchronized UnboundHttpResponse doGet(final URI aUri) throws Exception {
        HttpGet aRequest = null;
        try {
            LOG.forLevel(ELogLevel.E_INFO).withMessage("[" + m_aAppsuite.getId() + "] GET ['" + aUri + "']").log();

            aRequest = new HttpGet(aUri);
            final HttpClient aClient = mem_Client();

            LOG.forLevel(ELogLevel.E_INFO).withMessage("Request ['" + aUri + "'] : " + aRequest).setVar("cookies", getCookiesString()).log();

            final HttpResponse aResponse = aClient.execute(aRequest);

            final int nStatus = aResponse.getStatusLine().getStatusCode();
            if (nStatus != HttpStatus.SC_OK) {
                final StringBuilder sMsg = new StringBuilder(256);

                sMsg.append("Request failed : ");
                sMsg.append(aResponse.getStatusLine());

                throw new ServerResponseException(HttpStatusCode.createFromInt(nStatus), sMsg.toString());
            }

            LOG.forLevel(ELogLevel.E_INFO).withMessage("... Response ['" + aUri + "'] : " + aResponse.getStatusLine()).log();

            final UnboundHttpResponse aResponseClone = UnboundHttpResponse.create(aResponse);
            return aResponseClone;
        } finally {
            if (aRequest != null)
                aRequest.releaseConnection();
        }
    }

    //-------------------------------------------------------------------------
    protected synchronized UnboundHttpResponse doPost(final URI aUri, final String sContentType, final String sBody) throws Exception {
        HttpPost aRequest = null;
        try {
            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("[" + m_aAppsuite.getId() + "] POST ['" + aUri + "']").setVar("content-type", sContentType).setVar("body", sBody).log();

            aRequest = new HttpPost(aUri);

            if (!StringUtils.isEmpty(sContentType))
                aRequest.setHeader(HttpHeaders.CONTENT_TYPE, sContentType);

            if (!StringUtils.isEmpty(sBody)) {
                final HttpEntity aBody = new ByteArrayEntity(sBody.getBytes(DEFAULT_ENCODING));
                aRequest.setEntity(aBody);
            }

            final HttpClient aClient = mem_Client();
            final HttpResponse aResponse = aClient.execute(aRequest);

            final int nStatus = aResponse.getStatusLine().getStatusCode();
            if (!isSuccessfulHttpStatus(nStatus)) {
                final String sMsg = "Request failed with : " + aResponse.getStatusLine();

                LOG.forLevel(ELogLevel.E_ERROR).withMessage(sMsg).log();

                throw new ServerResponseException(HttpStatusCode.createFromInt(nStatus), sMsg.toString());
            }

            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("... Response ['" + aUri + "'] : " + aResponse.getStatusLine()).log();

            final UnboundHttpResponse aResponseClone = UnboundHttpResponse.create(aResponse);
            return aResponseClone;
        } finally {
            if (aRequest != null)
                aRequest.releaseConnection();
        }
    }

    //-------------------------------------------------------------------------
    protected synchronized UnboundHttpResponse doPut(final URI aUri, final String sContentType, final String sBody) throws Exception {
        HttpPut aRequest = null;
        try {
            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("[" + m_aAppsuite.getId() + "] PUT ['" + aUri + "']").setVar("content-type", sContentType).setVar("body", sBody).log();

            aRequest = new HttpPut(aUri);

            if (!StringUtils.isEmpty(sContentType))
                aRequest.setHeader(HttpHeaders.CONTENT_TYPE, sContentType);

            if (!StringUtils.isEmpty(sBody)) {
                final HttpEntity aBody = new ByteArrayEntity(sBody.getBytes(DEFAULT_ENCODING));
                aRequest.setEntity(aBody);
            }

            final HttpClient aClient = mem_Client();
            final HttpResponse aResponse = aClient.execute(aRequest);

            final int nStatus = aResponse.getStatusLine().getStatusCode();
            if (nStatus != HttpStatus.SC_OK) {
                final String sMsg = "Request failed with : " + aResponse.getStatusLine();

                LOG.forLevel(ELogLevel.E_ERROR).withMessage(sMsg).log();

                throw new ServerResponseException(HttpStatusCode.createFromInt(nStatus), sMsg.toString());
            }

            LOG.forLevel(ELogLevel.E_DEBUG).withMessage("... Response ['" + aUri + "'] : " + aResponse.getStatusLine()).log();

            final UnboundHttpResponse aResponseClone = UnboundHttpResponse.create(aResponse);
            return aResponseClone;
        } finally {
            if (aRequest != null)
                aRequest.releaseConnection();
        }
    }

    //-------------------------------------------------------------------------
    protected synchronized void dumpCookies() throws Exception {
        //System.out.println(getCookies ());
    }

    //-------------------------------------------------------------------------
    protected synchronized Map<String, Cookie> getCookies() throws Exception {
        final Map<String, Cookie> lCookies = new HashMap<String, Cookie>();
        final CookieStore aCookieStore = mem_CookieStore();
        final List<Cookie> aCookieList = aCookieStore.getCookies();

        for (final Cookie aCookie : aCookieList) {
            final String sName = aCookie.getName();
            lCookies.put(sName, aCookie);
        }

        return lCookies;
    }

    //-------------------------------------------------------------------------
    protected synchronized String getCookiesString() throws Exception {
        final StringBuilder sCookies = new StringBuilder(256);
        final CookieStore aCookieStore = mem_CookieStore();
        final List<Cookie> lCookies = aCookieStore.getCookies();

        for (final Cookie aCookie : lCookies) {
            sCookies.append(aCookie);
            sCookies.append("\n");
        }

        return sCookies.toString();
    }

    //-------------------------------------------------------------------------
    protected synchronized URI buildRequestUri(final String sRestApi, final NameValuePair... lParams) throws Exception {
        final String sBaseUri = m_useAlternativeHost ? m_aConfig.sAlternativeServerURL : m_aConfig.sServerURL;
        final String sRestUri = StringUtils.join(new String[] { sBaseUri, sRestApi }, "/");
        final URI aFullUri = buildUri(sRestUri, lParams);
        return aFullUri;
    }

    //-------------------------------------------------------------------------
    protected static URI buildUri(final String sBaseUri, final NameValuePair... lParams) throws Exception {
        final URI aRestUri = new URI(sBaseUri);
        final URIBuilder aUriBuilder = new URIBuilder(aRestUri);

        for (final NameValuePair aParam : lParams)
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());

        final URI aFullUri = aUriBuilder.build();
        return aFullUri;
    }

    //-------------------------------------------------------------------------
    protected static String encodeUriParams(final NameValuePair... lParams) throws Exception {
        final URI aRestUri = new URI("");
        final URIBuilder aUriBuilder = new URIBuilder(aRestUri);

        for (final NameValuePair aParam : lParams)
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());

        final URI aFullUri = aUriBuilder.build();
        final String sFullUri = aFullUri.toString();
        final String sEncodedParams = StringUtils.removeStart(sFullUri, "?");
        return sEncodedParams;
    }

    private boolean isSuccessfulHttpStatus(int httpStatus) {
        switch (httpStatus) {
            case HttpStatus.SC_OK:
            case HttpStatus.SC_CREATED:
            case HttpStatus.SC_ACCEPTED:
            case HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION:
            case HttpStatus.SC_NO_CONTENT:
            case HttpStatus.SC_RESET_CONTENT:
            case HttpStatus.SC_PARTIAL_CONTENT:
            case HttpStatus.SC_MULTI_STATUS:
                return true;
            default:
                return false;
        }
    }

    //-------------------------------------------------------------------------
    protected synchronized HttpClient mem_Client() throws Exception {
        if (m_aClient == null) {
            final TestConfig aConfig = m_aConfig.rGlobalConfig;
            final int nTimeOutMS = aConfig.getRestAPITimeoutInMS();

            final ConnectionConfig aConnectionConfig = ConnectionConfig.DEFAULT;
            final CookieStore aCookieStore = mem_CookieStore();
            final SocketConfig aSocketConfig = SocketConfig.custom().setSoTimeout(nTimeOutMS).setTcpNoDelay(true).setSoKeepAlive(true).setSoReuseAddress(true).build();
            final RequestConfig aRequestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.DEFAULT).setRedirectsEnabled(true).setConnectTimeout(nTimeOutMS).setSocketTimeout(nTimeOutMS).setConnectionRequestTimeout(nTimeOutMS).build();
            final HttpClient aClient = HttpClients.custom().setDefaultConnectionConfig(aConnectionConfig).setDefaultRequestConfig(aRequestConfig).setDefaultSocketConfig(aSocketConfig).setDefaultCookieStore(aCookieStore).setUserAgent(Constants.USER_AGENT).build();
            m_aClient = aClient;
        }
        return m_aClient;
    }

    //-------------------------------------------------------------------------
    protected HttpClientContext mem_Context() throws Exception {
        if (m_aContext == null) {
            final HttpClientContext aContext = HttpClientContext.create();
            final CookieStore aCookieStore = mem_CookieStore();
            aContext.setCookieStore(aCookieStore);
            m_aContext = aContext;
        }
        return m_aContext;
    }

    //-------------------------------------------------------------------------
    protected CookieStore mem_CookieStore() throws Exception {
        if (m_aCookieStore == null)
            m_aCookieStore = new InterceptedCookieStore();
        return m_aCookieStore;
    }

}
