/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import net.as_development.asdk.api.monitoring.IMonitor;
import net.as_development.asdk.monitoring.MonitorClientEnv;

//=============================================================================
public class RT2MonitorClient
{
	//-------------------------------------------------------------------------
	private RT2MonitorClient ()
	{}

    //-------------------------------------------------------------------------
    public static synchronized RT2MonitorClient get ()
        throws Exception
    {
        if (m_gSingleton == null)
            m_gSingleton = new RT2MonitorClient ();
        return m_gSingleton;
    }

    //-------------------------------------------------------------------------
    public synchronized void setServerHost (final String sHost)
        throws Exception
    {
        mem_ClientEnv().configure().setServerHost(sHost);
    }

    //-------------------------------------------------------------------------
    public synchronized void setServerPort (final int nPort)
        throws Exception
    {
        mem_ClientEnv().configure().setServerPort(nPort);
    }

    //-------------------------------------------------------------------------
	public /* no synchronized */ void start ()
	    throws Exception
	{
	    mem_ClientEnv ().start();
	    m_bRunning.set(true);
	}

    //-------------------------------------------------------------------------
    public /* no synchronized */ void stop ()
        throws Exception
    {
        mem_ClientEnv ().stop();
        m_bRunning.set(false);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void record (final String    sScope  ,
                                              final String    sKey    ,
                                              final String    sMessage,
                                              final Object... lData   )
        /* no throws Exception */
    {
        if (m_bRunning.get() == false)
            return;

        try
        {
            final IMonitor iMonitor = impl_getMonitor4Scope (sScope);
            iMonitor.record(sKey, sMessage, lData);
        }
        catch (final Throwable exIgnore)
        {
            // ignore any monitor exception as it's an optional debug feature
            // and has not to 'disturb' the real product ;-)
        }
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void record (final Throwable aEx     ,
                                              final String    sKey    ,
                                              final String    sMessage,
                                              final Object... lData   )
        /* no throws Exception */
    {
        if (m_bRunning.get() == false)
            return;

        try
        {
            final String   sExMessage      = aEx.getMessage();
            final String   sExStack        = ExceptionUtils.getStackTrace(aEx);
            final IMonitor iMonitor        = impl_getMonitor4Scope (RT2MonitorConst.SCOPE_EXCEPTION);
            final Object[] lAdditionalData = new Object[]
                                             {
                                                 RT2MonitorConst.DATAKEY_EX_MESSAGE, sExMessage,
                                                 RT2MonitorConst.DATAKEY_EX_STACK  , sExStack
                                             };
            final Object[] lFullData       = ArrayUtils.addAll (lAdditionalData, lData);

            iMonitor.record(sKey, sMessage, lFullData);
        }
        catch (final Throwable exIgnore)
        {
            // ignore any monitor exception as it's an optional debug feature
            // and has not to 'disturb' the real product ;-)
        }
    }

    //-------------------------------------------------------------------------
    private synchronized IMonitor impl_getMonitor4Scope (final String sScope)
        throws Exception
    {
        final Map< String, IMonitor > aRegistry = mem_MonitorRegistry ();
              IMonitor                iMonitor  = aRegistry.get(sScope);

        if (iMonitor == null)
        {
            iMonitor = MonitorClientEnv.newMonitor(sScope);
            aRegistry.put(sScope, iMonitor);
        }

        return iMonitor;
    }

    //-------------------------------------------------impl_getMonitor4Scope------------------------
    private synchronized MonitorClientEnv mem_ClientEnv ()
        throws Exception
    {
        if (m_aClientEnv == null)
        {
            final MonitorClientEnv aClientEnv = MonitorClientEnv.get ();
            aClientEnv .configure    (                             )
                       .setServerHost(RT2Const.DEFAULT_MONITOR_HOST)
                       .setServerPort(RT2Const.DEFAULT_MONITOR_PORT);
            m_aClientEnv = aClientEnv;
        }
        return m_aClientEnv;
    }

    //-------------------------------------------------------------------------
    private synchronized Map< String, IMonitor > mem_MonitorRegistry ()
        throws Exception
    {
        if (m_aMonitorRegistry == null)
            m_aMonitorRegistry = new HashMap< > ();
        return m_aMonitorRegistry;
    }

    //-------------------------------------------------------------------------
    private static RT2MonitorClient m_gSingleton = null;

    //-------------------------------------------------------------------------
	private MonitorClientEnv m_aClientEnv = null;

    //-------------------------------------------------------------------------
	private AtomicBoolean m_bRunning = new AtomicBoolean (false);

	//-------------------------------------------------------------------------
	private Map< String, IMonitor > m_aMonitorRegistry = null;
}
