/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.error;

import java.util.Arrays;
import java.util.Optional;

public enum HttpStatusCode {

    // ---------------------------------------------------------------
    ACCEPTED(202),
    BAD_REQUEST(400),
    CONFLICT(409),
    CREATED(201),
    FORBIDDEN(403),
    GONE(410),
    INTERNAL_SERVER_ERROR(500),
    MOVED_PERMANENTLY(301),
    NO_CONTENT(204),
    NOT_ACCEPTABLE(406),
    NOT_FOUND(404),
    NOT_MODIFIED(304),
    OK(200),
    PRECONDITION_FAILED(412),
    SEE_OTHER(303),
    SERVICE_UNAVAILABLE(503),
    TEMPORARY_REDIRECT(307),
    UNAUTHORIZED(401),
    UNSUPPORTED_MEDIA_TYPE(415);

    // ---------------------------------------------------------------
    private int nStatusCode;

    // ---------------------------------------------------------------
    private HttpStatusCode(int nStatusCode) {
        this.nStatusCode = nStatusCode;
    }

    // ---------------------------------------------------------------
    public int getStatusCode() { return nStatusCode; }

    // ---------------------------------------------------------------
    public int code() { return nStatusCode; }

    // ---------------------------------------------------------------
    public static HttpStatusCode createFromInt(int nStatusCode) {
        final Optional<HttpStatusCode> aOpt = 
            Arrays.asList(HttpStatusCode.values()).stream()
                                                  .filter(v -> equals(v.getStatusCode(), nStatusCode))
                                                  .findAny();

        return (aOpt.isPresent()) ? aOpt.get() : null;
    }

    // ---------------------------------------------------------------
    private static boolean equals(int a, int b) {
        return a == b;
    }
}
