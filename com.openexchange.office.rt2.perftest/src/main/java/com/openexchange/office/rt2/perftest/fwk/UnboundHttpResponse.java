/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

//=============================================================================
public class UnboundHttpResponse
{
    //-------------------------------------------------------------------------
    public static final String DEFAULT_ENCODING = "utf-8";

    //-------------------------------------------------------------------------
    private UnboundHttpResponse ()
    {}

    //-------------------------------------------------------------------------
    public static UnboundHttpResponse create (final HttpResponse aBoundResponse)
    	throws Exception
    {
    	final UnboundHttpResponse aUnboundResponse = new UnboundHttpResponse ();
    	
    	aUnboundResponse.impl_setHeader (aBoundResponse);
    	aUnboundResponse.impl_setBodyIS (aBoundResponse);
    	aUnboundResponse.impl_setStatus (aBoundResponse);
    	
    	return aUnboundResponse;
    }

    //-------------------------------------------------------------------------
    public Map< String, String > getHeader ()
    	throws Exception
    {
    	return mem_Header ();
    }

    //-------------------------------------------------------------------------
    public String getBody ()
    	throws Exception
    {
    	if (null == m_sBody)
    		m_sBody = IOUtils.toString(m_aBodyIS, DEFAULT_ENCODING);
    	return m_sBody;
    }

    //-------------------------------------------------------------------------
    public InputStream getBodyIS ()
        throws Exception
    {
        return m_aBodyIS;
    }

    //-------------------------------------------------------------------------
    public StatusLine getStatusLine ()
    	throws Exception
    {
    	return m_aStatus;
    }

    //-------------------------------------------------------------------------
    private void impl_setHeader (final HttpResponse aSource)
    	throws Exception
    {
    	final Header[]              lFrom = aSource.getAllHeaders();
    	final Map< String, String > lTo   = mem_Header ();

    	for (final Header aFrom : lFrom)
    		lTo.put(aFrom.getName(), aFrom.getValue());
    }

    //-------------------------------------------------------------------------
    private void impl_setBodyIS (final HttpResponse aSource)
        throws Exception
    {
    	final HttpEntity  aBody    = aSource.getEntity  ();
    	final InputStream aContent = aBody  .getContent ();
    	m_aBodyIS = new ByteArrayInputStream(IOUtils.toByteArray(aContent));
    }

    //-------------------------------------------------------------------------
    private void impl_setStatus (final HttpResponse aSource)
    	throws Exception
    {
    	m_aStatus = aSource.getStatusLine();
    }

    //-------------------------------------------------------------------------
    private Map< String, String > mem_Header ()
    	throws Exception
    {
    	if (m_lHeader == null)
    		m_lHeader = new HashMap< String, String > ();
    	return m_lHeader;
    }
    
    //-------------------------------------------------------------------------
    private Map< String, String > m_lHeader = null;

    //-------------------------------------------------------------------------
    private String m_sBody = null;

    //-------------------------------------------------------------------------
    private StatusLine m_aStatus = null;

    //-------------------------------------------------------------------------
    private ByteArrayInputStream m_aBodyIS = null;
}
