/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableInt;
import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.jamonapi.MonitorFactory;
import com.jamonapi.proxy.MonProxyFactory;
import com.openexchange.office.rt2.perftest.config.TestCaseDescriptor;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.ThreadPoolDescriptor;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.fwk.RT2MonitorClient;
import com.openexchange.office.rt2.perftest.fwk.RT2ScheduledThreadPoolExecutor;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.impl.TestCaseMonitor.TestCaseErrorHandler;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

/**
 * {@link TestCaseRunner}
 */
public class TestCaseRunner implements TestCaseErrorHandler {

    /**
     * Initializes a new {@link TestCaseRunner}.
     *
     * @throws Exception
     */
    public TestCaseRunner() throws Exception {
        super();
    }

    // - TestCaseErrorHandler --------------------------------------------------

    @Override
    public void testCasesFailed(Map<TestCaseBase, Throwable> testCaseErrors) throws Exception {
        // log error(s)
        final StringBuilder testCaseErrorsMessageBuilder =  new StringBuilder(1024);
        int errorNumber = 0;

        // create message of all TestCase errors so far
        for (final Throwable curTestCaseError : testCaseErrors.values()) {
            if (testCaseErrorsMessageBuilder.length() > 0) {
                testCaseErrorsMessageBuilder.append('\n').append("=====").append('\n');
            }

            testCaseErrorsMessageBuilder.
                append("TestCase error #").append(++errorNumber).append(": \n").
                append(Throwables.getRootCause(curTestCaseError));
        }

        final StatusLine statusLine = StatusLine.get();
        final String monitorErrorMessage = testCaseErrorsMessageBuilder.toString();

        statusLine.setTestCaseErrorCount(errorNumber);
        statusLine.printErrorMessage(monitorErrorMessage);

        // log TestCase errors
        LOG.forLevel(ELogLevel.E_ERROR).withMessage(monitorErrorMessage).log();

        // shutdown TestCaseMonitor since it is not needed anymore
        m_testCaseMonitor.shutdown();

        // cancel all submitted test cases
        LOG.forLevel(ELogLevel.E_INFO).withMessage("Test case failed handler called => start canceling all test cases.").log();

        synchronized (m_testCaseResultFutureMap) {
            m_testCaseResultFutureMap.values().forEach((Future<Throwable> curTestCaseResultFuture) -> {
                try {
                    curTestCaseResultFuture.cancel(true);
                } catch (@SuppressWarnings("unused") Throwable e) {
                    // ok
                }
            });
        }

        LOG.forLevel(ELogLevel.E_INFO).withMessage("Test case failed handler called => canceled all test cases.").log();
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param aConfig
     * @throws Exception
     */
    public void bind(final TestConfig aConfig) throws Exception {
        m_config = aConfig;
    }

    /**
     * @throws Exception
     */
    public void run() throws Exception {
        try {
            // TODO use config to enable/disable monitoring
            MonProxyFactory.enable(true);

            impl_startMonitor();
            impl_registerShutdownHook();

            final Collection<TestCaseDescriptor> testCaseDescriptions = m_config.getTestCases();
            final Collection<TestCaseBase> testCaseCollection = new ArrayList<>();
            final MutableInt testNumber = new MutableInt(1);
            final MutableInt testIterations = new MutableInt(0);

            for (final TestCaseDescriptor curTestCaseDescription : testCaseDescriptions) {
                impl_createTestsForTestCase(curTestCaseDescription, testCaseCollection, testNumber, testIterations);
            }

            Validate.isTrue(!testCaseCollection.isEmpty(), "No tests defined ? Please configure some!");

            final int testCaseCount = testCaseCollection.size();
            final ExecutorService aExec = impl_createThreadPool(testCaseCount);

            m_testCaseMonitor = new TestCaseMonitor(testCaseCollection, this);

            m_testCaseErrorOcurred.set(false);
            m_testCaseResultFutureMap.clear();

            m_statusLine.setTestCount(testCaseCount);
            m_statusLine.setMaxTestIterations(testIterations.intValue());

            impl_runWithStrategy(aExec, testCaseCollection);
        } catch (final Throwable ex) {
            LOG.forLevel(ELogLevel.E_ERROR).withError(ex).log();
        }

        Thread.sleep(100);

        if (null != m_testCaseMonitor) {
            m_testCaseMonitor.shutdown();
        }

        m_statusLine.update();

        RT2ScheduledThreadPoolExecutor.shutdown();

        impl_deregisterShutdownHook();

        generateReports();

        impl_stopMonitor();
    }

    // - Implementation --------------------------------------------------------

    /**
     * @throws Exception
     */
    private void impl_startMonitor() throws Exception {
        final String sHost = m_config.getMonitorServerHost();
        final Integer nPort = m_config.getMonitorServerPort();

        if (StringUtils.isEmpty(sHost))
            return;

        final RT2MonitorClient aMonitor = RT2MonitorClient.get();
        aMonitor.setServerHost(sHost);
        aMonitor.setServerPort(nPort);
        aMonitor.start();
    }

    /**
     * @throws Exception
     */
    private void impl_stopMonitor() throws Exception {
        RT2MonitorClient.get().stop();
    }

    /**
     * @param nTestCount
     * @return
     * @throws Exception
     */
    private ExecutorService impl_createThreadPool(final int nTestCount) throws Exception {
        final ThreadPoolDescriptor aCfg = m_config.getThreadPoolConfig();
        final ETestRunnerStrategy eRunnerStrategy = m_config.getTestRunnerStrategy();
        ExecutorService aPool = null;

        if (aCfg.eType == EThreadPoolType.E_UNLIMITED) {
            if (eRunnerStrategy == ETestRunnerStrategy.E_ALL_AT_ONCE)
                aPool = Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat("TestThread--%d").build());
            else if (eRunnerStrategy == ETestRunnerStrategy.E_DELAY_ONE_BY_ONE)
                aPool = Executors.newScheduledThreadPool(nTestCount, new ThreadFactoryBuilder().setNameFormat("TestThread--%d").build());
            else
                throw new UnsupportedOperationException("No support for '" + eRunnerStrategy + "' implemented yet.");
        } else if (aCfg.eType == EThreadPoolType.E_FIX) {
            Validate.isTrue(aCfg.nSize > 0, "Thread pool size needs to be > 0 for thread pool type FIX.");

            if (eRunnerStrategy == ETestRunnerStrategy.E_ALL_AT_ONCE)
                aPool = Executors.newFixedThreadPool(aCfg.nSize, new ThreadFactoryBuilder().setNameFormat("TestThread--%d").build());
            else if (eRunnerStrategy == ETestRunnerStrategy.E_DELAY_ONE_BY_ONE)
                aPool = Executors.newScheduledThreadPool(aCfg.nSize, new ThreadFactoryBuilder().setNameFormat("TestThread--%d").build());
            else
                throw new UnsupportedOperationException("No support for '" + eRunnerStrategy + "' implemented yet.");
        } else
            throw new UnsupportedOperationException("No support for '" + aCfg.eType + "' implemented yet.");

        return aPool;
    }

    /**
     * @throws Exception
     */
    private void impl_registerShutdownHook() throws Exception {
        if (m_shutdownHook != null)
            return;

        final Thread aShutdownHook = new Thread() {

            @Override
            public void run() {
                try {
                    LOG.forLevel(ELogLevel.E_INFO).withMessage("shutdown hook triggered ...").log();

                    generateReports();
                } catch (final Throwable ex) {
                    LOG.forLevel(ELogLevel.E_ERROR).withError(ex).log();
                }
            }
        };

        LOG.forLevel(ELogLevel.E_INFO).withMessage("register shutdown hook ...").log();

        Runtime.getRuntime().addShutdownHook(aShutdownHook);
        m_shutdownHook = aShutdownHook;

        LOG.forLevel(ELogLevel.E_INFO).withMessage("... shutdown hook registered").log();
    }

    /**
     * @throws Exception
     */
    private void impl_deregisterShutdownHook() throws Exception {
        final Thread aShutdownHook = m_shutdownHook;

        m_shutdownHook = null;

        if (aShutdownHook != null)
            Runtime.getRuntime().removeShutdownHook(aShutdownHook);
    }

    /**
     * @param aExec
     * @param testCases
     * @throws Exception
     */
    private void impl_runWithStrategy(final ExecutorService aExec, final Collection<TestCaseBase> testCases) throws Exception {
        final ETestRunnerStrategy eStrategy = m_config.getTestRunnerStrategy();

        if (eStrategy == ETestRunnerStrategy.E_ALL_AT_ONCE) {
            impl_runAllAtOnce(aExec, testCases);
        } else if (eStrategy == ETestRunnerStrategy.E_DELAY_ONE_BY_ONE) {
            impl_runDelayedOneByOne(aExec, testCases);
        } else {
            throw new UnsupportedOperationException("No support for '" + eStrategy + "' implemented yet.");
        }
    }

    /**
     * @param executorService
     * @param testCases
     * @throws Exception
     */
    private void impl_runAllAtOnce(final ExecutorService executorService, final Collection<TestCaseBase> testCases) throws Exception {
        testCases.forEach((TestCaseBase curTestCase) -> {
            final Future<Throwable> curTestCaseResultFuture = executorService.submit(curTestCase);

            if (null != curTestCaseResultFuture) {
                m_testCaseResultFutureMap.put(curTestCase, curTestCaseResultFuture);
            }
        });

        impl_joinForFinishingTests();
    }

    /**
     * @param aExec
     * @param testCases
     * @throws Exception
     */
    private void impl_runDelayedOneByOne(final ExecutorService aExec, final Collection<TestCaseBase> testCases) throws Exception {
        final ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService) aExec;
        final Integer delayMillis = m_config.getTestRunnerDelayInMS();
        final boolean breakOnError = m_config.isBreakOnError();
        final TestCaseMonitor usedTestCaseMonitor = m_testCaseMonitor;
        int startTimeMillis = delayMillis;

        for (final TestCaseBase curTestCase : testCases) {
            if ((null != usedTestCaseMonitor) && usedTestCaseMonitor.hasTestCaseError()) {
                // leave if first failed test has been found
                break;
            }

            m_testCaseResultFutureMap.put(
                curTestCase,
                scheduledExecutorService.schedule(curTestCase, startTimeMillis, TimeUnit.MILLISECONDS));

            startTimeMillis += delayMillis;
        }

        impl_joinForFinishingTests();
    }

    /**
     * @param testCases
     * @throws Exception
     */
    private void impl_joinForFinishingTests() throws Exception {
        for (final Future<Throwable> curTestCaseResult : m_testCaseResultFutureMap.values()) {
            final Throwable aError = (null != curTestCaseResult) ?
                curTestCaseResult.get() :
                    null;

            if (aError == null) {
                continue;
            }

            LOG.forLevel(ELogLevel.E_ERROR).withError(aError).log();
        }
    }

    /**
     * @param testCase
     * @param testCollection
     * @param testNumber
     * @param testIterations
     * @throws Exception
     */
    private void impl_createTestsForTestCase(final TestCaseDescriptor testCase, final Collection<TestCaseBase> testCollection, final MutableInt testNumber, final MutableInt testIterations) throws Exception {
        final StatusLine statusLineConfig = StatusLine.get();
        final Class<?> aTestCaseClass = Class.forName(testCase.sImplClass);
        final Map<Integer, UserDescriptor> lTestUser = m_config.getUser();
        int testCaseCount = testCase.nTestCount;

        Validate.isTrue(testCaseCount > 0, "No test cases defined ?");
        Validate.isTrue(!lTestUser.isEmpty(), "No test user defined ?");

        LOG.forLevel(ELogLevel.E_INFO).withMessage("create " + testCaseCount + " tests ...").log();

        for (int i = 0; i < testCaseCount; ++i) {
            final TestRunConfig aConfig = new TestRunConfig();
            final UserDescriptor aUser = impl_getUserForTestCase(testCase, lTestUser);

            aConfig.sImplClass = testCase.sImplClass;
            aConfig.aUser = aUser;
            aConfig.sServerURL = m_config.getAppsuiteUrl();
            aConfig.sWSURL = m_config.getWebsocketUrl();
            aConfig.sAPIRelPath = m_config.getAppsuiteUrlRelpath("api");
            aConfig.sTestId = "test-" + testNumber.getValue();
            aConfig.rGlobalConfig = m_config;

            testNumber.increment();

            final TestCaseBase curTest = (TestCaseBase) aTestCaseClass.newInstance();

            curTest.bind(aConfig);
            curTest.bind(statusLineConfig);

            testCollection.add(curTest);
            testIterations.add(curTest.calculateTestIterations());
        }

        LOG.forLevel(ELogLevel.E_INFO).withMessage("... done.").log();
    }

    /**
     * @param testCase
     * @param userPool
     * @return
     * @throws Exception
     */
    private UserDescriptor impl_getUserForTestCase(final TestCaseDescriptor testCase, final Map<Integer, UserDescriptor> userPool) throws Exception {
        final int nMaxUser = testCase.lUser.size() - 1;
        int userPos = 0;

        if (testCase.nLastUserFromList == null)
            userPos = 0;
        else
            userPos = testCase.nLastUserFromList;

        if (userPos >= nMaxUser)
            userPos = 0;
        else
            userPos += 1;

        testCase.nLastUserFromList = userPos;

        final Integer userNr = testCase.lUser.get(userPos);
        final UserDescriptor aUser = userPool.get(userNr);
        return aUser;
    }

    /**
     * @throws Exception
     */
    protected void generateReports() throws Exception {
        if (m_reportsGenerated)
            return;

        final TestConfig aConfig = m_config;
        final String sReportDir = aConfig.getReportDir();
        final File aJamonReport = new File(sReportDir, "jamon_report.html");
        final File aMsgStatsCSV = new File(sReportDir, "msg-stats.csv");

        impl_generateJamonReport(aJamonReport);
        impl_generateMsgStatsCSV(aMsgStatsCSV);

        m_reportsGenerated = true;
    }

    /**
     * @param aReportFile
     * @throws Exception
     */
    private void impl_generateJamonReport(final File aReportFile) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("generate JAMon report at '" + aReportFile + "' ...").log();

        final String sReport = MonitorFactory.getRootMonitor().getReport();
        FileUtils.deleteQuietly(aReportFile);
        FileUtils.write(aReportFile, sReport);
    }

    /**
     * @param aCSVFile
     * @throws Exception
     */
    private void impl_generateMsgStatsCSV(final File aCSVFile) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("generate Msg Performance Stats CSV at '" + aCSVFile + "' ...").log();

        FileUtils.deleteQuietly(aCSVFile);
        MessagePerformanceStats.get().dumpCSV(aCSVFile.getAbsolutePath());
    }

    // - Static members --------------------------------------------------------

    final protected static Logger LOG = Slf4JLogger.create(TestCaseRunner.class);

    // - Members ---------------------------------------------------------------

    final private StatusLine m_statusLine = StatusLine.get();

    final private Map<TestCaseBase, Future<Throwable>> m_testCaseResultFutureMap = Collections.synchronizedMap(new LinkedHashMap<>());

    final private AtomicBoolean m_testCaseErrorOcurred = new AtomicBoolean(false);

    volatile private TestCaseMonitor m_testCaseMonitor = null;

    private TestConfig m_config = null;

    private Thread m_shutdownHook = null;

    private boolean m_reportsGenerated = false;
}
