/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.operations;

public class OperationConstants {
    //-------------------------------------------------------------------------
	private OperationConstants() {}

    //-------------------------------------------------------------------------
	public static final String OPERATION_NAME     = "name";
	public static final String OPERATION_TEXT     = "text";
	public static final String OPERATION_START    = "start";
	public static final String OPERATION_TYPE     = "type";
	public static final String OPERATION_ATTRS    = "attrs";
	public static final String OPERATION_OPL      = "opl";
	public static final String OPERATION_OSN      = "osn";
	public static final String OPERATION_CONTENTS = "contents";

	public static final String OPERATION_CREATEERROR = "createError";

}
