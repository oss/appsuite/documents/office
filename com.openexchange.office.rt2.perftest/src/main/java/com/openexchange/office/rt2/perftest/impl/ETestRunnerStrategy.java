/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import org.apache.commons.lang3.StringUtils;

//=============================================================================
public enum ETestRunnerStrategy
{
    //-------------------------------------------------------------------------
    E_ALL_AT_ONCE,
    E_DELAY_ONE_BY_ONE;

    //-------------------------------------------------------------------------
    public static final String STR_ALL_AT_ONCE      = "all-at-once";
    public static final String STR_DELAY_ONE_BY_ONE = "delay-one-by-one";
    
    //-------------------------------------------------------------------------
    public static ETestRunnerStrategy fromString (final String sValue)
        throws Exception
    {
    	if (StringUtils.equalsIgnoreCase(sValue, STR_ALL_AT_ONCE))
    		return E_ALL_AT_ONCE;
    	else
    	if (StringUtils.equalsIgnoreCase(sValue, STR_DELAY_ONE_BY_ONE))
    		return E_DELAY_ONE_BY_ONE;
    	else
    		throw new UnsupportedOperationException ("No support for '"+sValue+"' implemented yet.");
    }

    //-------------------------------------------------------------------------
    public static String toString (final ETestRunnerStrategy eValue)
        throws Exception
    {
    	if (eValue == E_ALL_AT_ONCE)
    		return STR_ALL_AT_ONCE;
    	else
    	if (eValue == E_DELAY_ONE_BY_ONE)
    		return STR_DELAY_ONE_BY_ONE;
    	else
    		throw new UnsupportedOperationException ("No support for '"+eValue+"' implemented yet.");
    }
}
