/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.openexchange.office.rt2.protocol.RT2Message;

//=============================================================================
public abstract class MessageAndErrorNotifierDoc extends Doc
{
    //-------------------------------------------------------------------------
    private Queue<IMessageObserver> m_aMsgObserverQueue = new ConcurrentLinkedQueue<>();
    
    //-------------------------------------------------------------------------
    private Queue<IErrorObserver> m_aErrorObserverQueue = new ConcurrentLinkedQueue<>();

    //-------------------------------------------------------------------------
    public MessageAndErrorNotifierDoc(String sType) throws Exception {
        super(sType);
    }

    //-------------------------------------------------------------------------
    public void addBroadcastObserver(final IMessageObserver aObserver)
    {
        if (null != aObserver)
        	m_aMsgObserverQueue.add(aObserver);
    }

    //-------------------------------------------------------------------------
	public void removedBroadcastObserver(final IMessageObserver aObserver)
    {
        if (null != aObserver)
        	m_aMsgObserverQueue.remove(aObserver);
    }

    //-------------------------------------------------------------------------
    public void addErrorObserver(final IErrorObserver aObserver)
    {
        if (null != aObserver)
        	m_aErrorObserverQueue.add(aObserver);
    }

    //-------------------------------------------------------------------------
	public void removedErrorObserver(final IErrorObserver aObserver)
    {
        if (null != aObserver)
        	m_aErrorObserverQueue.remove(aObserver);
    }

    //-------------------------------------------------------------------------
	@Override
    protected /* no synchronized */ void onDocResponse (final RT2Message aResponse)
        throws Exception
    {
        final IMessageObserver[] aObserverToNotify = getMessageObservers();

        if (null != aObserverToNotify)
        {
            for (final IMessageObserver aObserver : aObserverToNotify)
            {
                try
                {
                    if (null != aObserver)
                        aObserver.receivedMessage(aResponse);
                }
                catch (Exception e)
                {
                    // nothing to do
                }
           }
        }
    }

    //-------------------------------------------------------------------------
	@Override
    protected /* no synchronized */ void onError (final RT2Message aResponse)
        throws Exception
    {
        final IErrorObserver[] aObserverToNotify = getErrorObservers();

        if (null != aObserverToNotify)
        {
            for (final IErrorObserver aObserver : aObserverToNotify)
            {
                try
                {
                    if (null != aObserver)
                        aObserver.errorDetected(aResponse);
                }
                catch (Exception e)
                {
                    // nothing to do
                }
           }
        }
    }

    //-------------------------------------------------------------------------
	private IMessageObserver[] getMessageObservers()
	{
        IMessageObserver[] aObserversToNotify = null;

        synchronized (m_aMsgObserverQueue)
        {
            if (!m_aMsgObserverQueue.isEmpty())
            {
                aObserversToNotify = new IMessageObserver[m_aMsgObserverQueue.size()];
                m_aMsgObserverQueue.toArray(aObserversToNotify);
            }
        }

        return aObserversToNotify;
	}

    //-------------------------------------------------------------------------
	private IErrorObserver[] getErrorObservers()
	{
		IErrorObserver[] aObserversToNotify = null;

        synchronized (m_aErrorObserverQueue)
        {
            if (!m_aErrorObserverQueue.isEmpty())
            {
                aObserversToNotify = new IErrorObserver[m_aErrorObserverQueue.size()];
                m_aErrorObserverQueue.toArray(aObserversToNotify);
            }
        }

        return aObserversToNotify;
	}
}
