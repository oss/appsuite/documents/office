/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.operations;

public class ColumnRow {
	private int column;
	private int row;

	public ColumnRow() {
		column = 1;
		row = 0;
	}

	public ColumnRow(int col, int row) {
		if ((col <= 0) || (row < 0))
			throw new IllegalArgumentException("Column or row must not be negative!");

		this.column = col;
		this.row = row;
	}

	public ColumnRow(String pos) {
		final ColumnRow colRow = valueOf(pos);
		column = colRow.column();
		row = colRow.row();
	}

	public ColumnRow(ColumnRow colRow) {
		this(colRow.column(), colRow.row());
	}

	public int column() {
		return column;
	}

	public int row() {
		return row;
	}

	public void setColumn(int col) {
		if (col <= 0)
			throw new IllegalArgumentException("Column must not be zero or negative!");

		column = col;
	}

	public void setRow(int row) {
		if (row < 0)
			throw new IllegalArgumentException("Row must not be negative!");

		this.row = row;
	}

	public void incColumn() {
		column += 1;
	}

	public void incRow() {
		row += 1;
	}

	@Override
	public String toString() {
		return ColumnRow.toString(column, row);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		else if (other == this)
			return true;
		else if (other instanceof ColumnRow) {
			final ColumnRow colRow = (ColumnRow)other; 
			return ((column == colRow.column()) && (row == colRow.row()));
		}

		return false;
	}

	@Override
	public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + column;
        result = prime * result + row;
        return result;
	}

    //-------------------------------------------------------------------------
    /**
     * Converts a column, row tuple to a text based
     * representation.
     *
     * @param nCol
     * @param nRow
     * @return
     */
    public static String toString(int nCol, int nRow) {
		if ((nCol <= 0) || (nRow < 0))
			throw new IllegalArgumentException("Column or row must not be negative!");

    	return formatAlphabetic(nCol) + String.valueOf(nRow + 1);
    }

    //-------------------------------------------------------------------------
    /**
     * Converts a string based cell reference to a column, row
     * representation.
     *
     * @param stringBasedRef
     * @return ColumnRow instance
     * @throws IllegalArgumentException
     */
    public static ColumnRow valueOf(String stringBasedRef) throws IllegalArgumentException {
	    // accept empty strings
	    if (stringBasedRef.length() == 0) { return new ColumnRow(1, 0); }

	    stringBasedRef = stringBasedRef.toUpperCase();

        int column = parseColumnPart(stringBasedRef);
        int row = parseRowPart(stringBasedRef);

        return new ColumnRow(column, row);
    }

    //-------------------------------------------------------------------------
    /**
     * Converts the passed number to an alphabetic number string. The
     * numbers `1` to `26` will be converted to single letters `A` to `Z`
     * respectively. The numbers `27` to `52` will be converted to multi-letter
     * numbers using specific conversion methods.
     *
     * @param value
     *  The number to be converted. Will be truncated to an integer.
     *
     * @returns
     *  The alphabetic number representation of the passed number
     * @throws IllegalArgumentException
     *  In case the number is zero or negative.
     */
    private static String formatAlphabetic(int value) throws IllegalArgumentException {

        if (value <= 0) { throw new IllegalArgumentException("Column value must not be zero or negative!"); }

        String result = "";
        for (value -= 1; value >= 0; value = (value / 26) - 1) {
            result = String.valueOf((char)(65 + value % 26)) + result;
        }

        return result;
    }

    //-------------------------------------------------------------------------
    /**
     * 
     * @param stringBasedRef
     * @return
     * @throws IllegalArgumentException
     */
    private static int parseColumnPart(String stringBasedRef) throws IllegalArgumentException {

    	boolean first = false;
    	int column = 0;

    	for (int i = 0; i < stringBasedRef.length(); i++) {
    		final char c = stringBasedRef.charAt(i);
    		if (isAlphabetic(c)) {
    			first = true;
            	column = column * 26 + stringBasedRef.charAt(i) - 64;
    		} else if (!first) {
    			throw new IllegalArgumentException("Cell reference must use alphabetic characters for column defintion: " + stringBasedRef);
    		} else {
    			break;
    		}
    	}

    	return column;
    }

    //-------------------------------------------------------------------------
    private static int parseRowPart(String stringBasedRef) throws IllegalArgumentException {
        
    	int numIndex = -1;
    	for (int i = 0; i < stringBasedRef.length(); i++) {
    		final char c = stringBasedRef.charAt(i);
    		if (isNumeric(c)) {
    			numIndex = i;
    			break;
    		}
    	}

    	if (numIndex < 0)
    		throw new IllegalArgumentException("Cell reference must contain a row part!");

    	int row;
        try {
            row = Integer.valueOf(stringBasedRef.substring(numIndex)) - 1;
            if (row < 0) {
            	throw new IllegalArgumentException("Cell reference must use row value > 0: " + stringBasedRef); 
            }
        } catch (NumberFormatException e) {
        	throw new IllegalArgumentException("Illegal column/row string " + stringBasedRef);
        }

        return row;
    }

    //-------------------------------------------------------------------------
    private static boolean isAlphabetic(char c) {
    	return ((c >= 'A') && (c <= 'Z'));
    }

    //-------------------------------------------------------------------------
    private static boolean isNumeric(char c) {
    	return ((c >= '0') && (c <= '9'));
    }
}
