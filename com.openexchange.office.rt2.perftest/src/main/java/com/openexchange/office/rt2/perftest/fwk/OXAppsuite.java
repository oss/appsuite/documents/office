/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;

//=============================================================================
/**
 */
public class OXAppsuite {

	// -------------------------------------------------------------------------
	private TestRunConfig m_aConfig = null;
	private OXConnection m_aConnection = null;
	private OXSession m_aSession = null;
	private OXCookies m_aCookies = null;
	private OXWSClient m_aWSClient = null;
	private OXFilesRestAPI m_filesRestAPI = null;
	private OXFoldersRestAPI m_foldersRestAPI = null;
	private OXDocSpellcheckRestAPI spellcheckAPI = null;
	private OXRT2Env m_aRT2 = null;
	private boolean m_useAlternativeHost = false;

	// -------------------------------------------------------------------------
	public OXAppsuite(final TestRunConfig aConfig) throws Exception {
		m_aConfig = aConfig;
	}

    // -------------------------------------------------------------------------
    public OXAppsuite(final TestRunConfig aConfig, boolean useAlternativeHost) throws Exception {
        m_aConfig = aConfig;
        m_useAlternativeHost = useAlternativeHost;
    }

    // -------------------------------------------------------------------------
    public synchronized boolean getUseAlternativeHost() {
        return m_useAlternativeHost;
    }

	// -------------------------------------------------------------------------
	public synchronized String getId() throws Exception {
		return accessConfig().sTestId;
	}

	// -------------------------------------------------------------------------
	public synchronized TestRunConfig accessConfig() throws Exception {
		return m_aConfig;
	}

	// -------------------------------------------------------------------------
	public synchronized OXSession accessSession() throws Exception {
		return mem_Session();
	}

	// -------------------------------------------------------------------------
	public synchronized OXCookies accessCookies() throws Exception {
		return mem_Cookies();
	}

	// -------------------------------------------------------------------------
	public synchronized OXConnection accessConnection() throws Exception {
		return mem_Connection();
	}

	// -------------------------------------------------------------------------
	public synchronized OXWSClient accessWSClient() throws Exception {
		return mem_WSClient();
	}

	// -------------------------------------------------------------------------
	public synchronized OXRT2Env accessRT2Env() throws Exception {
		return mem_RT2Env();
	}

	// -------------------------------------------------------------------------
	public void shutdownWSConnection() throws Exception {
		final OXWSClient wsClient = accessWSClient();

		if (null != wsClient) {
			wsClient.shutdown();
			synchronized (this) {
				m_aWSClient = null;
			}
			OXRT2Env env = accessRT2Env();
			if (null != env) {
				env.resetRT2Channel();
			}
		}
	}

	// -------------------------------------------------------------------------
	private OXConnection mem_Connection() throws Exception {
		if (m_aConnection == null) {
			final OXConnection aConnection = new OXConnection(m_useAlternativeHost);
			aConnection.bind(this);
			m_aConnection = aConnection;
		}
		return m_aConnection;
	}

	// -------------------------------------------------------------------------
	private OXSession mem_Session() throws Exception {
		if (m_aSession == null) {
			final OXSession aSession = new OXSession();
			aSession.bind(this);
			m_aSession = aSession;
		}
		return m_aSession;
	}

	// -------------------------------------------------------------------------
	private OXCookies mem_Cookies() throws Exception {
		if (m_aCookies == null) {
			final OXCookies aCookies = new OXCookies();
			aCookies.bind(this);
			m_aCookies = aCookies;
		}
		return m_aCookies;
	}

	// -------------------------------------------------------------------------
	private OXWSClient mem_WSClient() throws Exception {
		if (m_aWSClient == null) {
			final OXWSClient aClient = new OXWSClient();
			aClient.bind(this);
			m_aWSClient = aClient;
		}
		return m_aWSClient;
	}

	// -------------------------------------------------------------------------
	public synchronized OXFilesRestAPI getFilesRestAPI() throws Exception {
		if (m_filesRestAPI == null) {
			final OXFilesRestAPI files = new OXFilesRestAPI();
			files.bind(this);
			m_filesRestAPI = files;
		}
		return m_filesRestAPI;
	}

	// -------------------------------------------------------------------------
	public synchronized OXFoldersRestAPI getFoldersRestAPI() throws Exception {
		if (m_foldersRestAPI == null) {
			final OXFoldersRestAPI folders = new OXFoldersRestAPI();
			folders.bind(this);
			m_foldersRestAPI = folders;
		}
		return m_foldersRestAPI;
	}

	// -------------------------------------------------------------------------
	public synchronized OXDocSpellcheckRestAPI getSpellcheckRestAPI() throws Exception {
		if (spellcheckAPI == null) {
			final OXDocSpellcheckRestAPI folders = new OXDocSpellcheckRestAPI();
			folders.bind(this);
			spellcheckAPI = folders;
		}
		return spellcheckAPI;
	}

	// -------------------------------------------------------------------------
	private OXRT2Env mem_RT2Env() throws Exception {
		if (m_aRT2 == null) {
			final OXRT2Env aRT2Env = new OXRT2Env();
			aRT2Env.bind(this);
			m_aRT2 = aRT2Env;
		}
		return m_aRT2;
	}

}
