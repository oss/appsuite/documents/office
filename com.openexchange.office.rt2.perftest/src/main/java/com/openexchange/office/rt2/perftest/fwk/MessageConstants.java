/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

public class MessageConstants
{
    //-------------------------------------------------------------------------
    public static final String STR_OPERATIONS             = "operations"     ;
    public static final String STR_ACTION                 = "action"         ;
    public static final String STR_BODY                   = "body"           ;
    public static final String STR_OSN                    = "serverOSN"      ;
    public static final String STR_ERROR                  = "error"          ;
    public static final String STR_FASTEMPTY              = "fastEmpty"      ;
    public static final String STR_FASTEMPTY_OSN          = "fastEmptyOSN"   ;
    public static final String STR_VALUE                  = "value"          ;
    public static final String STR_FORCEEDITRESULT        = "forceEditResult";

    //-------------------------------------------------------------------------
    public static final String STR_SELECTIONS             = "selections"     ;
    public static final String STR_TYPE                   = "type"           ;
    public static final String STR_START                  = "start"          ;
    public static final String STR_END                    = "end"            ;

    //-------------------------------------------------------------------------
	public static final String ACTION_EDITRIGHTS_REQUESTS = "request";
	public static final String ACTION_EDITRIGHTS_APPROVED = "approved";
	public static final String ACTION_EDITRIGHTS_DECLINED = "declined";
	public static final String ACTION_EDITRIGHTS_FORCE    = "force";
	public static final String ACTION_EDITRIGHTS_ALL      = "@ALL";

    //-------------------------------------------------------------------------
	private MessageConstants() {}
}
