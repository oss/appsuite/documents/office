/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config.items;

import com.openexchange.office.rt2.perftest.config.ConfigAccess;
import com.openexchange.office.rt2.perftest.config.ConfigUtils;
import com.openxchange.office_communication.tools.config.IComplexConfiguration;

public class DriveLoadTestConfig {
    //-------------------------------------------------------------------------
    public static final String CONFIG_PACKAGE = "/loadtest";

    //-------------------------------------------------------------------------
    public static final int     DEFAULT_DELAY_CREATE         =  1000;
    public static final int     DEFAULT_TIMEOUT_4_CREATE     = 30000;
    public static final int     DEFAULT_TIMEOUT_4_CHECKFILE  = 30000;
    public static final boolean DEFAULT_CONTINUE_ON_TIMEOUT  = false;

    //-------------------------------------------------------------------------
    public DriveLoadTestConfig ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    public Integer getTimeout4Create()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.driveload.timeout.create", String.class), DEFAULT_TIMEOUT_4_CREATE);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4CheckFile ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.driveload.timeout.checkfile", String.class), DEFAULT_TIMEOUT_4_CHECKFILE);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getContinueOnTimeout ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("test.driveload.timeout.continue", String.class), DEFAULT_CONTINUE_ON_TIMEOUT);
    	return bValue;
    }

    //-------------------------------------------------------------------------
    public Integer getDelayCreateInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.driveload.delay.create", String.class), DEFAULT_DELAY_CREATE);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    private final IComplexConfiguration mem_Config ()
        throws Exception
    {
    	if (m_iConfig == null)
    		m_iConfig = ConfigAccess.accessConfig(CONFIG_PACKAGE);
    	return m_iConfig;
    }

    //-------------------------------------------------------------------------
    private IComplexConfiguration m_iConfig = null;

}
