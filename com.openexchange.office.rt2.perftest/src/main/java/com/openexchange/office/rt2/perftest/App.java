/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest;

import java.io.File;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.rt2.perftest.cmdline.CmdLine;
import com.openexchange.office.rt2.perftest.config.ConfigAccess;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.impl.EOperation;
import com.openexchange.office.rt2.perftest.impl.TestCaseRunner;

//=============================================================================
public class App 
{
    //-------------------------------------------------------------------------
    public static final int RET_OK              = 0;
    public static final int RET_GENERAL_ERROR   = 1;
    public static final int RET_INVALID_CMDLINE = 2;
    
    //-------------------------------------------------------------------------
    public static void main(final String[] lArgs)
    	throws Exception
    {
    	try
    	{
        	final CmdLine aCmdLine = new CmdLine ();
        	aCmdLine.parse(lArgs);
        	
        	if (aCmdLine.needsHelp())
        	{
        		aCmdLine.printHelp();
        		return;
        	}

        	impl_prepareLoggingEnv                 (aCmdLine);
        	impl_provideConfigurationPathToRuntime (aCmdLine);
        	impl_provideReportPathToRuntime        (aCmdLine);
        	
        	final EOperation eOp = aCmdLine.getOperation();
        	if (eOp == EOperation.E_RUN_TESTS)
        		impl_runTests (aCmdLine);
        	else
        		throw new UnsupportedOperationException ("No support for operation '"+eOp+"' implemented yet.");
            
            //System.out.println ("ok.");
    	}
    	catch (final Throwable ex)
    	{
    		System.err.println (ex.getMessage());
    		ex.printStackTrace (System.err     );
    		
        	System.exit(1);
    	}

    	System.exit(0);
    }

    //-------------------------------------------------------------------------
    private static void impl_prepareLoggingEnv (final CmdLine aCmdLine)
        throws Exception
    {
    	System.setProperty("APPLICATION_ID", "office-performance-test"   );
    	System.setProperty("INSTANCE_ID"   , UUID.randomUUID().toString());
    }

    //-------------------------------------------------------------------------
    private static void impl_provideConfigurationPathToRuntime (final CmdLine aCmdLine)
        throws Exception
    {
    	final String sConfigPath = aCmdLine.getConfigPath();
    	if (StringUtils.isEmpty (sConfigPath))
    	    return;
    	
    	final File aConfigPath = new File (sConfigPath);
    	if ( ! aConfigPath.isDirectory())
    	{
    		System.err.println ("Config path '"+aConfigPath+"' is not a valid path.");
    		System.exit(1);
    	}
    	
    	ConfigAccess.defineOutsideConfigPath(aConfigPath.getAbsolutePath());
    }

    //-------------------------------------------------------------------------
    private static void impl_provideReportPathToRuntime (final CmdLine aCmdLine)
        throws Exception
    {
        final String sReportPath = aCmdLine.getReportPath();
        if (StringUtils.isEmpty (sReportPath))
            return;
        
        final File aReportPath = new File (sReportPath);
        if ( ! aReportPath.isDirectory())
        {
            System.err.println ("Report path '"+aReportPath+"' is not a valid path.");
            System.exit(1);
        }

        TestConfig.defineOutsideReportPath(aReportPath.getAbsolutePath());
    }

    //-------------------------------------------------------------------------
    private static void impl_runTests (final CmdLine aCmdLine)
    	throws Exception
    {
    	final TestConfig     aCfg    = TestConfig.get ();
    	final TestCaseRunner aRunner = new TestCaseRunner ();

    	aRunner.bind(aCfg);
        aRunner.run ();
    }
}
