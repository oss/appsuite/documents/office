/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.openexchange.office.tools.error.ErrorCode;
import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class OXDocRestAPI
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXDocRestAPI.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_DOCUMENTFILTER = "/oxodocumentfilter";

    //-------------------------------------------------------------------------
    public static final String REST_API_DOCUMENTCONVERTER = "/oxodocumentconverter";

    //-------------------------------------------------------------------------
    public static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    private static final int JSON_PRETTY_INDENT = 4;

    //-------------------------------------------------------------------------
    public OXDocRestAPI ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aContext)
        throws Exception
    {
        m_aContext = aContext;
    }

    //-------------------------------------------------------------------------
    public NewDocumentData createNewDocument(final String sFolderId   ,
                                             final String sDocType    ,
                                             final String sDocFileName)
        throws Exception
    {
    	final String        sRestAPIUri  = mem_BaseURI () + REST_API_DOCUMENTFILTER;
    	final OXConnection  aConnection  = m_aContext.accessConnection();
    	final OXSession     aSession     = m_aContext.accessSession   ();
    	final String        sSessionId   = aSession.getSessionId();
    	final String        sNow         = String.valueOf(new Date().getTime());
    	final String        sNewFileName = StringUtils.isEmpty(sDocFileName) ? "unnamed" : sDocFileName;

    	final NameValuePair[] aParams  = new NameValuePair[9];
    	aParams[0] = new BasicNameValuePair("action"           , "createdefaultdocument");
    	aParams[1] = new BasicNameValuePair("target_folder_id" , sFolderId              );
    	aParams[2] = new BasicNameValuePair("target_filename"  , sNewFileName           );
    	aParams[3] = new BasicNameValuePair("document_type"    , sDocType               );
    	aParams[4] = new BasicNameValuePair("preserve_filename", "false"                );
    	aParams[5] = new BasicNameValuePair("convert"          , "false"                );
    	aParams[6] = new BasicNameValuePair("app"              , sDocType               );
    	aParams[7] = new BasicNameValuePair("session"          , sSessionId             );
    	aParams[8] = new BasicNameValuePair("uid"              , sNow                   );

    	final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("GET : "+aURI)
    		.log 		();

    	final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
    	final StatusLine          aResult   = aResponse.getStatusLine();

    	if (aResult.getStatusCode() != HttpStatusCode.OK.code())
    		throw new RuntimeException("bad server response : " + aResult);

    	final String     sBody  = aResponse.getBody ();
    	final JSONObject aBody  = new JSONObject(sBody);
    	final JSONObject aData  = aBody.optJSONObject(REST_JSON_ROOT);

    	if (aData == null)
    		throw new RuntimeException("Malformed answer from server - no data part found!\nresponse:\n"+sBody);

    	final JSONObject aFile         = aData.optJSONObject("file" );
    	final JSONObject aError        = aData.optJSONObject("error");
    	final String     sHtmlDoc      = aData.optString    ("htmlDoc");
    	final JSONArray  aFastEmptyOps = aData.optJSONArray ("operations");

    	if (aError == null)
    		throw new RuntimeException("Malformed answer from server - no error code found!\nresponse:\n"+aError);

    	if ( ! aError.getString("error").equals("NO_ERROR"))
    		throw new RuntimeException("Got error from server : "+aError);

    	return new NewDocumentData(aFile.getString("filename"), aFile.getString("id"), sHtmlDoc, aFastEmptyOps);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?
    //   action=createfromtemplate&document_type=text&
    //   target_filename=unnamed&target_folder_id=96&
    //   preserve_filename=false&convert=true&file_id=template%3AEC99071EF5C48001F65C128112CE95C6&uid=1529573682950.4036&
    //   app=text&session=b257a7aa1b0748019da00fb156cdba7d
    public NewDocumentData createNewDocumentFromTemplate(final String sTemplateFileId,
                                                         final String sFolderId      ,
                                                         final String sDocType       ,
                                                         final String sDocFileName)
        throws Exception
    {
    	final String        sRestAPIUri  = mem_BaseURI () + REST_API_DOCUMENTFILTER;
    	final OXConnection  aConnection  = m_aContext.accessConnection();
    	final OXSession     aSession     = m_aContext.accessSession   ();
    	final String        sSessionId   = aSession.getSessionId();
    	final String        sNow         = String.valueOf(new Date().getTime());
    	final String        sNewFileName = StringUtils.isEmpty(sDocFileName) ? "unnamed" : sDocFileName;

    	final NameValuePair[] aParams  = new NameValuePair[10];
    	aParams[0] = new BasicNameValuePair("action"           , "createfromtemplate" );
    	aParams[1] = new BasicNameValuePair("file_id"          , sTemplateFileId      );
    	aParams[2] = new BasicNameValuePair("target_folder_id" , sFolderId            );
    	aParams[3] = new BasicNameValuePair("target_filename"  , sNewFileName         );
    	aParams[4] = new BasicNameValuePair("document_type"    , sDocType             );
    	aParams[5] = new BasicNameValuePair("preserve_filename", "false"              );
    	aParams[6] = new BasicNameValuePair("convert"          , "false"              );
    	aParams[7] = new BasicNameValuePair("app"              , sDocType             );
    	aParams[8] = new BasicNameValuePair("session"          , sSessionId           );
    	aParams[9] = new BasicNameValuePair("uid"              , sNow                 );

    	final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("GET : "+aURI)
    		.log 		();

    	final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
    	final StatusLine          aResult   = aResponse.getStatusLine();

    	if (aResult.getStatusCode() != HttpStatusCode.OK.code())
    		throw new RuntimeException("bad server response : " + aResult);

    	final String     sBody  = aResponse.getBody ();
    	final JSONObject aBody  = new JSONObject(sBody);
    	final JSONObject aData  = aBody.optJSONObject(REST_JSON_ROOT);

    	if (aData == null)
    		throw new RuntimeException("Malformed answer from server - no data part found!\nresponse:\n"+sBody);

    	final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aData, ErrorCode.NO_ERROR);
    	if (aErrorCode.isError()) {
    		throw new RuntimeException("Got error from server : " + aErrorCode.toString());
    	}

    	final JSONObject aFile  = aData.optJSONObject("file" );
    	return new NewDocumentData(aFile.getString("filename"), aFile.getString("id"), null, null);
    }

    //-------------------------------------------------------------------------
    public JSONObject checkFile(final String sFileId)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
    	final String        sSessionId  = aSession.getSessionId();
    	final String        sNow        = String.valueOf(new Date().getTime());

    	final NameValuePair[] aParams  = new NameValuePair[4];
    	aParams[0] = new BasicNameValuePair("action"           , "checkfile" );
    	aParams[1] = new BasicNameValuePair("session"          , sSessionId  );
    	aParams[2] = new BasicNameValuePair("file_id"          , sFileId     );
    	aParams[3] = new BasicNameValuePair("uid"              , sNow        );

    	final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

    	LOG	.forLevel	(ELogLevel.E_INFO)
    		.withMessage("GET : "+aURI)
    		.log 		();

    	final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
    	final StatusLine          aResult   = aResponse.getStatusLine();

    	if (aResult.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR, "Bad server response : " + aResult);

    	final String     sBody  = aResponse.getBody ();
    	if (StringUtils.isEmpty(sBody))
    		throw new ServerResponseException(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR, "Answer from server is empty!");

    	final JSONObject aBody  = new JSONObject(sBody);
    	final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
    	final JSONObject aError = aData.optJSONObject("error");

    	if (aError == null)
    		throw new ServerResponseException(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

    	if ( ! aError.getString("error").equals("NO_ERROR"))
    		throw new ServerResponseException(ErrorCode.createFromJSONObject(aError, ErrorCode.GENERAL_UNKNOWN_ERROR), "Got error from server : " + aError);

    	return aData;
    }

    //-------------------------------------------------------------------------
    // http://ci3.docs.open-xchange.com/appsuite/api/oxodocumentconverter?session=9f83d6bba07f49b5acd1f4c4fb412c76&uid=1587465074432.6417&action=getdocument&documentformat=pdf&priority=instant&filename=unnamed%20(10).docx&id=34%2F517002&folder_id=34&mimetype=application%2Foctet-stream&revtag=revtag-2655&nocache=2656&version=2&async=true
    // http://localhost/appsuite/api/oxodocumentfilter?uid=1525349443873.7573&id=94%2F475&folder_id=94&filename=unnamed.pptx&version=2&source=drive&com.openexchange.realtime.resourceID=94%2F475&action=getdocument&documentformat=pdf&mimetype=application%2Foctet-stream&nocache=5373&doc_uid=10398e9dfe35a6bba69f17a9376cd0db&client_uid=8851e4bb-ee49-150e-2547-27cd5a45c3cf&app=presentation
    public InputStream getDocument(final String sFileId,
                                   final String sFolderId,
                                   final String sClientUID,
                                   final String sDocUID,
                                   final String sFileName,
                                   final String sAppType)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTCONVERTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[12];
        aParams[0] = new BasicNameValuePair("action"           , "getdocument" );
        aParams[1] = new BasicNameValuePair("session"          , sSessionId    );
        aParams[2] = new BasicNameValuePair("id"               , sFileId       );
        aParams[3] = new BasicNameValuePair("folder_id"        , sFolderId     );
        aParams[4] = new BasicNameValuePair("client_uid"       , sClientUID    );
        aParams[5] = new BasicNameValuePair("doc_uid"          , sDocUID       );
        aParams[6] = new BasicNameValuePair("filename"         , sFileName     );
        aParams[7] = new BasicNameValuePair("source"           , "drive"       );
        aParams[8] = new BasicNameValuePair("documentformat"   , "pdf"         );
        aParams[9] = new BasicNameValuePair("source"           , "drive"       );
        aParams[10] = new BasicNameValuePair("mimetype"        , "application%2Foctet-stream");
        aParams[11] = new BasicNameValuePair("app"             , sAppType      );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final InputStream aSeekableInStream = new ByteArrayInputStream(IOUtils.toByteArray(aResponse.getBodyIS()));
        aSeekableInStream.mark(0);
        final String sBody  = IOUtils.toString(aSeekableInStream, "utf-8");
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");
        aSeekableInStream.reset();

        if (sBody.indexOf("PDF") < 0)
        {
            final JSONObject aBody = new JSONObject(sBody);
            final JSONObject aError = aBody.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            throw new ServerResponseException(ErrorCode.GENERAL_ARGUMENTS_ERROR, "Malformed answer from server - no PDF data found & error code set to NO_ERROR!");
        }

        return aSeekableInStream;
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter (POST)
    public JSONObject restoreDocument(final String sRestoreID,
                                      final String sFileId,
                                      final String sTargetFolderId,
                                      final String sTargetFileName,
                                      final JSONArray aOperations)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aEmptyNameValuePair);

        final NameValuePair[] aBodyNameValuePairs = new NameValuePair[7];
        aBodyNameValuePairs[0] = new BasicNameValuePair("action"          , "saveaswithbackup"    );
        aBodyNameValuePairs[1] = new BasicNameValuePair("session"         , sSessionId            );
        aBodyNameValuePairs[2] = new BasicNameValuePair("file_id"         , sFileId               );
        aBodyNameValuePairs[3] = new BasicNameValuePair("target_folder_id", sTargetFolderId       );
        aBodyNameValuePairs[4] = new BasicNameValuePair("target_filename" , sTargetFileName       );
        aBodyNameValuePairs[5] = new BasicNameValuePair("restore_id"      , sRestoreID            );
        aBodyNameValuePairs[6] = new BasicNameValuePair("operations"      , aOperations.toString());

        final URIBuilder aUriBuilder = new URIBuilder ();
        for (final NameValuePair aParam : aBodyNameValuePairs)
        	aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        final String sEncBodyString = aUriBuilder.build().toString().substring(1);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("POST : " + aURI + "\n and body " + sEncBodyString)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody  = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
        final JSONObject aError = aData.optJSONObject("error");

        if (aError == null)
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

        final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
        if ( ! aError.getString("error").equals("NO_ERROR"))
            throw new ServerResponseException(aErrorCode, "Got error from server : " + aError);

        return aData;
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=getoperations&id=94%2F475&folder_id=94 (GET)
    public JSONObject getOperations(final String sFolderId,
                                    final String sFileId)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[4];
        aParams[0] = new BasicNameValuePair("action"           , "getoperations" );
        aParams[1] = new BasicNameValuePair("session"          , sSessionId      );
        aParams[2] = new BasicNameValuePair("id"               , sFileId         );
        aParams[3] = new BasicNameValuePair("folder_id"        , sFolderId       );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.keys().hasNext())
        {
            final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aData.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            return aData;
        }

        throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed JSON answer from server - no properties found!\nresponse:\n"+aBody);
    }

    //-------------------------------------------------------------------------
    public JSONObject getTemplateAndRecentFileList(final String sAppType)
        throws Exception
    {
        final OXConnection aConnection = m_aContext.accessConnection();
        final OXSession    aSession    = m_aContext.accessSession   ();
        final String       sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String       sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[3];
        aParams[0] = new BasicNameValuePair("action"  , "gettemplateandrecentlist" );
        aParams[1] = new BasicNameValuePair("session" , sSessionId                 );
        aParams[2] = new BasicNameValuePair("type"    , sAppType                   );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.keys().hasNext())
        {
            final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aData.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            return aData;
        }

        throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed JSON answer from server - no properties found!\nresponse:\n"+aBody);
    }

    //-------------------------------------------------------------------------
    public JSONObject addFile(final String sFileName, final String sExt, final String sFileData, final String sFileId, final String sFolderId, final String sHash)
        throws Exception
    {
        final String        IMAGEDATA_PREFIX  = "data:image/";
        final String        IMAGEDATA_POSTFIX = ";base64,";

        final OXConnection  aConnection  = m_aContext.accessConnection();
        final OXSession     aSession     = m_aContext.accessSession   ();
        final String        sRestAPIUri  = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId   = aSession.getSessionId();
        final StringBuilder aDataBuilder = new StringBuilder(IMAGEDATA_PREFIX);

        final JSONObject aRequestData = new JSONObject();
        aRequestData.put("add_ext", sExt);
        aDataBuilder.append(sExt);
        aDataBuilder.append(IMAGEDATA_POSTFIX);
        aDataBuilder.append(sFileData);
        aRequestData.put("add_filedata", aDataBuilder.toString() );
        aRequestData.put("add_hash", sHash);

        final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aEmptyNameValuePair);

        final NameValuePair[] aBodyNameValuePairs = new NameValuePair[6];
        aBodyNameValuePairs[0] = new BasicNameValuePair("action"      , "addfile"               );
        aBodyNameValuePairs[1] = new BasicNameValuePair("session"     , sSessionId              );
        aBodyNameValuePairs[2] = new BasicNameValuePair("filename"    , sFileName               );
        aBodyNameValuePairs[3] = new BasicNameValuePair("requestdata" , aRequestData.toString() );
        aBodyNameValuePairs[4] = new BasicNameValuePair("id"          , sFileId                 );
        aBodyNameValuePairs[5] = new BasicNameValuePair("folder_id"   , sFolderId               );

        final URIBuilder aUriBuilder = new URIBuilder ();
        for (final NameValuePair aParam : aBodyNameValuePairs)
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        final String sEncBodyString = aUriBuilder.build().toString().substring(1);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("POST : " + aURI + "\n and body " + sEncBodyString)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody  = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody = new JSONObject(sBody);

        return aBody.getJSONObject(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?uid=1529393411034.9874&id=96%2F835&folder_id=96&filename=unnamed%20(1).docx&version=3&source=drive&com.openexchange.realtime.resourceID=96%2F835&action=getfile&get_filename=word%2Fmedia%2Fuid21e58c9e276.jpg
    public InputStream getFile(
            final String sFileId,
            final String sFolderId,
            final String sVersion,
            final String sGetFileName,
            final String sFileName,
            Optional<Integer> width,
            Optional<Integer> height)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        URI reqURI = null;
        NameValuePair[] params = null;

        if (width.isPresent() && height.isPresent()) {
            params = new NameValuePair[11];
            params[9] = new BasicNameValuePair("width", width.get().toString());
            params[10] = new BasicNameValuePair("height", height.get().toString());
        } else {
            params = new NameValuePair[9];
        }
        params[0] = new BasicNameValuePair("action"       , "getfile"    );
        params[1] = new BasicNameValuePair("session"      , sSessionId   );
        params[2] = new BasicNameValuePair("id"           , sFileId      );
        params[3] = new BasicNameValuePair("folder_id"    , sFolderId    );
        params[4] = new BasicNameValuePair("filename"     , sFileName    );
        params[5] = new BasicNameValuePair("source"       , "drive"      );
        params[6] = new BasicNameValuePair("version"      , sVersion     );
        params[7] = new BasicNameValuePair("source"       , "drive"      );
        params[8] = new BasicNameValuePair("get_filename" , sGetFileName );

        reqURI = aConnection.buildRequestUri(sRestAPIUri, params);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+reqURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(reqURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        return new ByteArrayInputStream(IOUtils.toByteArray(aResponse.getBodyIS()));
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=gettemplatetlist&type=text&session=94d5a2f032934f9e9f19671920ef6142
    public JSONObject getTemplateList(final String sAppType)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[3];
        aParams[0] = new BasicNameValuePair("action"       , "gettemplatelist" );
        aParams[1] = new BasicNameValuePair("session"      , sSessionId        );
        aParams[2] = new BasicNameValuePair("type"         , sAppType          );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.keys().hasNext())
        {
            final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aData.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            return aData;
        }

        throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed JSON answer from server - no properties found!\nresponse:\n"+aBody);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=clearrecentlist&type=text&session=94d5a2f032934f9e9f19671920ef6142
    public void clearRecentList(final String sAppType)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[3];
        aParams[0] = new BasicNameValuePair("action"       , "clearrecentlist" );
        aParams[1] = new BasicNameValuePair("session"      , sSessionId        );
        aParams[2] = new BasicNameValuePair("type"         , sAppType          );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.length() > 0)
        {
            final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aData.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);
        }
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=deletefilefromrecentlist&type=text&remove_file_id=286%2F928&session=ed99f47bff474c97843d4b6ce418fa2e
    public void deleteFileFromRecentList(final String sAppType, final String sRemoveFileId)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[4];
        aParams[0] = new BasicNameValuePair("action"        , "clearrecentlist" );
        aParams[1] = new BasicNameValuePair("session"       , sSessionId        );
        aParams[2] = new BasicNameValuePair("type"          , sAppType          );
        aParams[3] = new BasicNameValuePair("remove_file_id", sRemoveFileId     );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.length() > 0)
        {
            final JSONObject aData  = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aData.optJSONObject("error");

            if (aError == null)
                throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed answer from server - no error code found!\nresponse:\n"+aError);

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);
        }
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=getcontexttemplatefolders&session=f8d2f8a2694046f9b98b6f31fdbb9619
    public JSONArray getContextTemplateFolders()
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[2];
        aParams[0] = new BasicNameValuePair("action"       , "getcontexttemplatefolders" );
        aParams[1] = new BasicNameValuePair("session"      , sSessionId                  );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        JSONArray aFoldersResult = null;

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.keys().hasNext())
        {
            // the result can be an JSONObject if no context folders are set or
            // a JSONArray if context template folders are available
            final Object dataRoot = aBody.get(REST_JSON_ROOT);
            if (dataRoot instanceof org.json.JSONObject) {
               aFoldersResult = new JSONArray();
            } else if (dataRoot instanceof org.json.JSONArray) {
               aFoldersResult = aBody.getJSONArray(REST_JSON_ROOT);
            }
            final JSONObject aError = aBody.optJSONObject("error");

            if (null == aFoldersResult)
                throw new ServerResponseException(ErrorCode.GENERAL_ARGUMENTS_ERROR, "Server sent malformed answer");

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            return aFoldersResult;
        }

        throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed JSON answer from server - no properties found!\nresponse:\n"+aBody);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/oxodocumentfilter?action=getuserinfo&session=f8d2f8a2694046f9b98b6f31fdbb9619&userId=1
    public JSONObject getUserInfo(final String sUserId)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[3];
        aParams[0] = new BasicNameValuePair("action"       , "getuserinfo" );
        aParams[1] = new BasicNameValuePair("session"      , sSessionId    );
        aParams[2] = new BasicNameValuePair("userId"       , sUserId       );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);

        final String sBody = aResponse.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        JSONObject aData;

        final JSONObject aBody  = new JSONObject(sBody);
        if (aBody.keys().hasNext())
        {
            aData = aBody.getJSONObject(REST_JSON_ROOT);
            final JSONObject aError = aBody.optJSONObject("error");

            if (null == aData)
                throw new ServerResponseException(ErrorCode.GENERAL_ARGUMENTS_ERROR, "Server sent malformed answer");

            final ErrorCode aErrorCode = ErrorCode.createFromJSONObject(aError, ErrorCode.NO_ERROR);
            if (aErrorCode.isError())
                throw new ServerResponseException(aErrorCode, "Got error from server : "+aError);

            return aData;
        }

        throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Malformed JSON answer from server - no properties found!\nresponse:\n"+aBody);
    }

    //-------------------------------------------------------------------------
    public void logPerformanceData(final String sPerformanceData)
        throws Exception
    {
        final OXConnection    aConnection = m_aContext.accessConnection();
        final OXSession       aSession    = m_aContext.accessSession   ();
        final String          sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String          sSessionId  = aSession.getSessionId();

        final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aEmptyNameValuePair);

        final String          sData = (null == sPerformanceData) ? JSONObject.NULL.toString() : sPerformanceData;
        final NameValuePair[] aBodyNameValuePairs = new NameValuePair[3];
        aBodyNameValuePairs[0] = new BasicNameValuePair("action"          , "logperformancedata" );
        aBodyNameValuePairs[1] = new BasicNameValuePair("session"         , sSessionId           );
        aBodyNameValuePairs[2] = new BasicNameValuePair("performanceData" , sData                );

        final URIBuilder aUriBuilder = new URIBuilder ();
        for (final NameValuePair aParam : aBodyNameValuePairs)
        	aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        final String sEncBodyString = aUriBuilder.build().toString().substring(1);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("POST : " + aURI + "\n and body " + sEncBodyString)
            .log        ();

        final UnboundHttpResponse aResponse = aConnection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
        final StatusLine          aResult   = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Server reports an error : " + aResult);
    }

    //-------------------------------------------------------------------------
    public int logClientData(String anonymous, String fileId, String folderId, String fileName, String userId, String docUid, String errorCode, String logData) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final OXSession session    = m_aContext.accessSession   ();
        final String restAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String sessionId  = session.getSessionId();

        final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
        final URI aURI = connection.buildRequestUri(restAPIUri, aEmptyNameValuePair);

        final String logDataToSend = (null == logData) ? JSONObject.NULL.toString() : logData;
        final NameValuePair[] aBodyNameValuePairs = new NameValuePair[10];
        aBodyNameValuePairs[0] = new BasicNameValuePair("action", "logclientdata");
        aBodyNameValuePairs[1] = new BasicNameValuePair("session", sessionId);
        aBodyNameValuePairs[2] = new BasicNameValuePair("logData", logDataToSend);
        aBodyNameValuePairs[3] = new BasicNameValuePair("logAnonymous", anonymous);
        aBodyNameValuePairs[4] = new BasicNameValuePair("fileId", fileId);
        aBodyNameValuePairs[5] = new BasicNameValuePair("folderId", folderId);
        aBodyNameValuePairs[6] = new BasicNameValuePair("fileName", fileName);
        aBodyNameValuePairs[7] = new BasicNameValuePair("userId", userId);
        aBodyNameValuePairs[8] = new BasicNameValuePair("docUid", docUid);
        aBodyNameValuePairs[9] = new BasicNameValuePair("errorCode", logDataToSend);

        final URIBuilder aUriBuilder = new URIBuilder ();
        for (final NameValuePair aParam : aBodyNameValuePairs)
        	aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        final String sEncBodyString = aUriBuilder.build().toString().substring(1);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("POST : " + aURI + "\n and body " + sEncBodyString)
            .log        ();

        final UnboundHttpResponse aResponse = connection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
        final StatusLine          aResult   = aResponse.getStatusLine();

        return aResult.getStatusCode();
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/oxodocumentfilter?action=templatepreview&id=template:EC99071EF5C48001F65C128112CE95C6&folder=template:local&format=preview_image&delivery=view&width=120&scaleType=contain&dummy=1515407073000
    public InputStream templatePreview(final String sFileId, final String sFolderId, final int nWidth)
        throws Exception
    {
        final OXConnection  aConnection = m_aContext.accessConnection();
        final OXSession     aSession    = m_aContext.accessSession   ();
        final String        sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String        sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[8];
        aParams[0] = new BasicNameValuePair("action"       , "templatepreview"            );
        aParams[1] = new BasicNameValuePair("session"      , sSessionId                   );
        aParams[2] = new BasicNameValuePair("id"           , sFileId                      );
        aParams[3] = new BasicNameValuePair("folder"       , sFolderId                    );
        aParams[4] = new BasicNameValuePair("delivery"     , "view"                       );
        aParams[5] = new BasicNameValuePair("width"        , ((Integer)nWidth).toString() );
        aParams[6] = new BasicNameValuePair("scaleType"    , "contain"                    );
        aParams[7] = new BasicNameValuePair("format"       , "preview_image"              );

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG .forLevel	(ELogLevel.E_INFO)
            .withMessage("GET : "+aURI)
            .log        ();

        final UnboundHttpResponse aResponse    = aConnection.doGet(aURI);
        final StatusLine          aResult      = aResponse.getStatusLine();
        final Map<String, String> aHeaders     = aResponse.getHeader();
        final String              sContentType = aHeaders.get("Content-Type");

        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);
        else if (sContentType.indexOf("image/") == 0)
        {
            // content type is an image, therefore return an input stream
            return new ByteArrayInputStream(IOUtils.toByteArray(aResponse.getBodyIS()));
        }
        else
        {
            // this is not an image, but a json based answer - must be an error and therefore
            // we have to decode and provide it as a human readable string
            final String     sBody      = aResponse.getBody();
            final JSONObject aJSONBody  = new JSONObject(sBody);
            final ErrorCode  aErrorCode = ErrorCode.createFromJSONObject(aJSONBody, ErrorCode.GENERAL_UNKNOWN_ERROR);
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), aErrorCode, "Error ocurred on server : " + aJSONBody.toString(JSON_PRETTY_INDENT));
        }
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/oxodocumentfilter?action=getdocumentstate&id=10/200&folder=10&format=preview_image&delivery=view&width=120&scaleType=contain&dummy=1515407073000
    public JSONObject getDocumentState(String folderId, String sFileId) throws Exception {
        final OXConnection aConnection = m_aContext.accessConnection();
        final OXSession    aSession    = m_aContext.accessSession   ();
        final String       sRestAPIUri = mem_BaseURI () + REST_API_DOCUMENTFILTER;
        final String       sSessionId  = aSession.getSessionId();

        final NameValuePair[] aParams  = new NameValuePair[4];
        aParams[0] = new BasicNameValuePair("action", "getdocumentstate");
        aParams[1] = new BasicNameValuePair("session", sSessionId);
        aParams[2] = new BasicNameValuePair("id", sFileId);
        aParams[3] = new BasicNameValuePair("folder_id", folderId);

        final URI aURI = aConnection.buildRequestUri(sRestAPIUri, aParams);

        LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+aURI).log();

        final UnboundHttpResponse aResponse = aConnection.doGet(aURI);
        final StatusLine aResult = aResponse.getStatusLine();

        if (aResult.getStatusCode() != HttpStatusCode.OK.code()) {
            throw new ServerResponseException(HttpStatusCode.createFromInt(aResult.getStatusCode()), "Bad server response : " + aResult);
        } else {
            final String sBody = aResponse.getBody();
            final JSONObject aJSONBody = new JSONObject(sBody);
            return aJSONBody;
        }
    }

    //-------------------------------------------------------------------------
    private String mem_BaseURI ()
        throws Exception
    {
        return m_aContext.accessConfig().sAPIRelPath;
    }

    //-------------------------------------------------------------------------
    private OXAppsuite m_aContext = null;
}
