/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc.presentation;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.FastEmptyDoc;
import com.openexchange.office.rt2.perftest.operations.Operations;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;

//=============================================================================
public class PresentationDoc extends FastEmptyDoc
{
    //-------------------------------------------------------------------------
	public static final String STR_TEXT = "Das gelbe Taxi von Xaver und Zacharias fährt jetzt quer durch Konstanz in Bayern.";
	public static final int    STR_LEN  = STR_TEXT.length();

    //-------------------------------------------------------------------------
    public PresentationDoc ()
        throws Exception
    {
    	super(Doc.DOCTYPE_PRESENTATION);
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONArray getNextApplyOps ()
        throws Exception
    {
    	final JSONArray lOps = createOperationsArray();
        Operations.addOperation    (lOps, createInsertTextOperation(/*slide*/0, /*drawing*/0, /*paragraph*/0, /*pos*/0, STR_TEXT));

        return lOps;
    }

    //-------------------------------------------------------------------------
	@Override
	protected JSONObject getCurrentSelection() throws Exception {
		return null;
	}

    //-------------------------------------------------------------------------
    public JSONObject createInsertTextOperation(final int    nSlide    ,
                                                final int    nDrawing  ,
                                                final int    nParagraph,
                                                final int    nPos      ,
    											final String sText     )
        throws Exception
    {
    	final JSONObject aOperation = new JSONObject();
    	final JSONArray  aPos       = new JSONArray ();
    	final int        nOSN       = getNextOSN();

    	aPos.put(nSlide    );
    	aPos.put(nDrawing  );
        aPos.put(nParagraph);
        aPos.put(nPos      );

    	aOperation.put("name" , "insertText");
    	aOperation.put("text" , sText       );
    	aOperation.put("start", aPos        );
    	aOperation.put("osn"  , nOSN        );
    	aOperation.put("opl"  , 1           );

    	OperationsUtils.compressObject(aOperation);
    	return aOperation;
    }

}
