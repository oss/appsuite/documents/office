/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

/**
 * {@link TestCaseMonitor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class TestCaseMonitor {

        // TestCaseMonitor timer name
        final private static String TESTCASE_MONITOR_TIMER_NAME = "TestCase Monitor timer";

        // the number of errors that are collected before an error notification is sent
        final private static int NOTIFY_ERROR_COUNT = 1;

        // TestCaseMonitor default timer period is 1s
        final private static long TESTCASE_MONITOR_PERIOD_MILLIS =  1000L;

        /**
         * {@link TestCaseErrorHandler}
         *
         * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
         * @since v7.10.3
         */
        public interface TestCaseErrorHandler {
            void testCasesFailed(Map<TestCaseBase, Throwable> testCaseErrors) throws Exception;
        }

        /**
         * Initializes a new {@link JobMonitor}.
         * @param jobProcessor
         * @param queueCountLimitHigh
         * @param queueCountLimitLow
         * @param queueTimeoutSeconds
         */
        public TestCaseMonitor(final Collection<TestCaseBase> testCases, final TestCaseErrorHandler testCaseErrorHandler) {
            super();

            TESTCASE_MONITOR_INSTANCE = this;

            m_testCases = testCases;
            m_testCaseErrorHandler = testCaseErrorHandler;

            implStartPeriodicTimer(TESTCASE_MONITOR_PERIOD_MILLIS);

            LOG.forLevel(ELogLevel.E_INFO).withMessage("Started Test case error monitor").log();
        }

        /**
         * @return
         */
        public static TestCaseMonitor get() {
            return TESTCASE_MONITOR_INSTANCE;
        }

        // - Public API ------------------------------------------------------------

        /**
         *
         */
        public void shutdown() {
            if (m_isRunning.compareAndSet(true, false)) {
                m_periodicTimer.cancel();

                LOG.forLevel(ELogLevel.E_INFO).withMessage("Test case error monitor shutdown finished").log();
            }
        }

        /**
         * @return
         */
        public boolean isRunning() {
            return m_isRunning.get();
        }

        /**
         * @param testCase
         */
        public void setTestCaseError(final TestCaseBase testCase) {
            if (m_isRunning.get()) {
                final Throwable testCaseError = (null != testCase) ?
                    testCase.getLastError() :
                        null;

                if (null != testCaseError) {
                    m_testCaseErrorMap.put(testCase, testCaseError);
                    implNotifyTestCaseErrorHandler();
                }
            }
        }

        /**
         * @return
         */
        public boolean hasTestCaseError() {
            return (m_testCaseErrorMap.size() > 0);
        }

        /**
         * @return
         */
        public Map<TestCaseBase, Throwable> getTestCaseErrorMap() {
            return implCreateTestCaseErrorMapCopy();
        }

        // - Implementation --------------------------------------------------------

        /**
         * @return
         */
        private Map<TestCaseBase, Throwable> implCreateTestCaseErrorMapCopy() {
            synchronized (m_testCaseErrorMap) {
                final Map<TestCaseBase, Throwable> ret = new LinkedHashMap<>(m_testCaseErrorMap.size());

                ret.putAll(m_testCaseErrorMap);

                return ret;
            }
        }

        /**
         * implStartTimer
         *
         * @param timeoutdMillis
         */
        private void implStartPeriodicTimer(final long periodMillis) {
            m_periodicTimer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    if (m_isRunning.get()) {
                        implCheckRunningTestCases();
                    }
                }
            }, periodMillis, periodMillis);
        }

        /**
         * Check all running tests for errors and notify TestCaseErrorHandler, if set
         */
        private void implCheckRunningTestCases() {
            for (final TestCaseBase curTestCase : m_testCases) {
                final Throwable curError = curTestCase.getLastError();

                if (null != curError) {
                    m_testCaseErrorMap.put(curTestCase, curError);

                    if (m_testCaseErrorMap.size() >= NOTIFY_ERROR_COUNT) {
                        // leave collecting loop if we reached the fixed
                        // numbers of errors to collect before notification
                        break;
                    }
                }
            }

            implNotifyTestCaseErrorHandler();
        }

        /**
         * notify test case error handler if set and if at
         * least one test case with an error has been found
         */
        private void implNotifyTestCaseErrorHandler() {
            synchronized (m_testCaseErrorMap) {
                if ((null != m_testCaseErrorHandler) && !m_notified && (m_testCaseErrorMap.size() > 0)) {
                    try {
                        m_testCaseErrorHandler.testCasesFailed(implCreateTestCaseErrorMapCopy());
                        m_testCaseErrorMap.clear();
                        m_notified = true;
                    } catch (Exception e) {
                        LOG.forLevel(ELogLevel.E_ERROR).withError(e).log();
                    }
                }
            }
        }

        // - Static Members --------------------------------------------------------

        final private static Logger LOG = Slf4JLogger.create(TestCaseMonitor.class);

        volatile private static TestCaseMonitor TESTCASE_MONITOR_INSTANCE = null;

        // - Members ---------------------------------------------------------------

        final protected AtomicBoolean m_isRunning = new AtomicBoolean(true);

        final private Timer m_periodicTimer = new Timer(TESTCASE_MONITOR_TIMER_NAME, true);

        final protected Map<TestCaseBase, Throwable> m_testCaseErrorMap = Collections.synchronizedMap(new LinkedHashMap<>());

        final private Collection<TestCaseBase> m_testCases;

        final private TestCaseErrorHandler m_testCaseErrorHandler;

        private boolean m_notified = false;
    }
