/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc.spreadsheet;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.EditableDoc;
import com.openexchange.office.rt2.perftest.operations.ColumnRow;
import com.openexchange.office.rt2.perftest.operations.Operations;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;

//=============================================================================
public class SpreadsheetDoc extends EditableDoc {
    //-------------------------------------------------------------------------
    public static final int START_CELL_VALUE = 100000;
    public static final int LAST_COLUMN      = 20;
    public static final int LAST_ROW         = 50;
    public static final int INIT_COLUMN      = 1;
    public static final int INIT_ROW         = 0;

    //-------------------------------------------------------------------------
    private int m_nCellRow = INIT_ROW;
    private int m_nCellColumn = INIT_COLUMN;
    private int m_nCellValue = START_CELL_VALUE;
    private int m_nActiveSheet = 0;
    private int m_nNewSheetId = 1;


    //-------------------------------------------------------------------------
    public SpreadsheetDoc () throws Exception {
    	super(Doc.DOCTYPE_SPREADSHEET);
    }

    //-------------------------------------------------------------------------
    public JSONObject getAddSheetOperation() throws Exception {
        final JSONObject aOperation = new JSONObject();
        final int nOSN = getNextOSN();
        aOperation.put("name", "insertSheet");
        aOperation.put("sheet", m_nNewSheetId);
        aOperation.put("sheetName", "Sheet" + (m_nNewSheetId + 1));
        aOperation.put("osn", nOSN);
        aOperation.put("opl", 1);
        m_nNewSheetId++;

        OperationsUtils.compressObject(aOperation);
        return aOperation;
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONArray getNextApplyOps () throws Exception {
    	int nRow    = m_nCellRow   ;
    	int nColumn = m_nCellColumn;
    	int nValue  = m_nCellValue ;

    	nValue++;

    	final JSONArray lOps = createOperationsArray();
    	Operations.addOperation(lOps, impl_createChangeCellsOperation(nColumn, nRow, nValue));

    	if (nColumn < LAST_COLUMN) {
    		nColumn++;
    	} else if (nRow > LAST_ROW) {
    		nColumn = INIT_COLUMN;
    		nRow    = INIT_ROW;
    	} else {
    		nColumn = INIT_COLUMN;
    		nRow++;
    	}

    	m_nCellRow    = nRow   ;
    	m_nCellColumn = nColumn;
    	m_nCellValue  = nValue ;

        return lOps;
    }

    //-------------------------------------------------------------------------
	@Override
	protected JSONObject getCurrentSelection() throws Exception {
		return null;
	}

    //-------------------------------------------------------------------------
    private JSONObject impl_createChangeCellsOperation(
    		final int nColumn, final int nRow, final int nValue ) throws Exception {
    	final JSONObject aOperation = new JSONObject();
    	final JSONObject aContents = new JSONObject();
    	final int nOSN = getNextOSN();
    	final String sPos = ColumnRow.toString(nColumn, nRow);

    	aContents.put(sPos, nValue);

    	aOperation.put("name", "changeCells");
    	aOperation.put("sheet", m_nActiveSheet);
    	aOperation.put("contents", aContents);
    	aOperation.put("osn", nOSN);
    	aOperation.put("opl", 1);

    	OperationsUtils.compressObject(aOperation);
    	return aOperation;
    }

}
