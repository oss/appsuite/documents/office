/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

import java.util.HashMap;
import java.util.Map;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.jms.core.IJMSServer;
import net.as_development.asdk.jms.core.JMSEnv;
import net.as_development.asdk.jms.core.JMSServer;

//=============================================================================
public class InstanceConfigure
{
    //-------------------------------------------------------------------------
    public InstanceConfigure ()
    {}

    //-------------------------------------------------------------------------
    public < T > void set (final String sKey  ,
                           final T      aValue)
        throws Exception
    {
        mem_Config ().put (sKey, aValue);
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public < T > T get (final String sKey)
        throws Exception
    {
        final T aValue = (T) mem_Config ().get (sKey);
        return aValue;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public < T > T get (final String sKey    ,
                        final T      aDefault)
        throws Exception
    {
        T aValue = (T) get (sKey);
        if (aValue == null)
            aValue = aDefault;
        return aValue;
    }

    //-------------------------------------------------------------------------
    private Map< String, Object > mem_Config ()
        throws Exception
    {
        if (m_aConfig == null)
            m_aConfig = new HashMap< String, Object >();
        return m_aConfig;
    }

    //-------------------------------------------------------------------------
    private Map< String, Object > m_aConfig = null;
}
