/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import org.apache.commons.lang3.mutable.Mutable;
import com.jamonapi.proxy.MonProxyFactory;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.config.items.PresenterTestConfig;
import com.openexchange.office.rt2.perftest.doc.presenter.IPresenterDoc;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDoc;
import com.openexchange.office.rt2.perftest.fwk.ISessionAPI;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2Env;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================

public abstract class PresenterTestBase extends TestCaseBase {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(PresenterTestBase.class);

    //-------------------------------------------------------------------------
    private PresenterTestConfig m_aPresenterConfig = null;

    //-------------------------------------------------------------------------
    public PresenterTestBase () throws Exception {
    	super();
    }

    //-------------------------------------------------------------------------
    protected void getTestDocAndEnv (final Mutable< PresenterDoc   > aDocRef    ,
                                     final Mutable< IPresenterDoc  > iDocRef    ,
                                     final Mutable< ISessionAPI    > iSessionRef,
                                     final Mutable< UserDescriptor > aUserRef   ) throws Exception {

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Create a provide presenter instances for the test...").log();

        final TestRunConfig       aRunCfg      = getRunConfig();
        final PresenterTestConfig aTestCfg     = mem_PresenterConfig ();

        final StatusLine          aStatusLine  = getStatusLine ();
        final UserDescriptor      aUser        = aRunCfg.aUser;

        final OXAppsuite          aOX          = new OXAppsuite      (aRunCfg);
        final OXSession           aSession     = aOX.accessSession   ();
        final OXRT2Env            aRT2Env      = aOX.accessRT2Env    ();
        final RT2                 aDocImpl     = aRT2Env.newRT2Inst  ();
        final PresenterDoc        aDoc         = new PresenterDoc    ();

        final ISessionAPI         iSession     = (ISessionAPI  ) MonProxyFactory.monitor(aSession);
        final IPresenterDoc       iDoc         = (IPresenterDoc) MonProxyFactory.monitor(aDoc    );

        aDoc.bind           (aDocImpl                         );
        aDoc.bind           (aStatusLine                      );

        aDoc.setTimeouts    (aTestCfg.getTimeout4OpenInMS   () ,
                             60000                             ,
                             aTestCfg.getTimeout4CloseInMS  () ,
                             aTestCfg.getContinueOnTimeout  () ,
                             aTestCfg.getAutoCloseDocOnError());

        aDoc.setFolderId    (aTestCfg.getTestFolderID       ());
        aDoc.setFileId      (aTestCfg.getTestFileID         ());
        aDoc.setDriveDocId  (aTestCfg.getTestDriveDocID     ());
        aDoc.enableDocEvents(true                             );

        enableMessageExceptionHandling(aDocImpl);

        aDocRef.setValue     (aDoc    );
        iDocRef.setValue     (iDoc    );
        iSessionRef.setValue (iSession);
        aUserRef.setValue    (aUser   );
    }

    //-------------------------------------------------------------------------
    protected PresenterTestConfig mem_PresenterConfig () throws Exception {
        if (m_aPresenterConfig == null)
            m_aPresenterConfig = new PresenterTestConfig ();
        return m_aPresenterConfig;
    }

}
