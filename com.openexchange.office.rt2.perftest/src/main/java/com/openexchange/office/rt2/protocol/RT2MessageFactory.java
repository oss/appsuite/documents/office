/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.openexchange.office.rt2.perftest.fwk.JSONObject;

//=============================================================================
public class RT2MessageFactory
{
    //-------------------------------------------------------------------------
    private RT2MessageFactory ()
    {}

    //-------------------------------------------------------------------------
    public static RT2Message newMessage (final String sType)
        throws Exception
    {
        Validate.notEmpty (sType, "Invalid argument 'type'.");

        final RT2Message aMessage = new RT2Message ();

        impl_defineMessageFlags  (aMessage, sType);
        impl_defineDefaultHeader (aMessage, sType);

        aMessage.setType (sType);

        return aMessage;
    }

    //-------------------------------------------------------------------------
    public static RT2Message cloneMessage (final RT2Message aOriginal ,
                                           final String     sCloneType)
        throws Exception
    {
        final RT2Message aClone = RT2MessageFactory.newMessage(sCloneType);

        RT2Message.copyAllHeader(aOriginal, aClone);
        RT2Message.copyBody     (aOriginal, aClone);

        aClone.setType (sCloneType);

        // msg flags are bound to message type.
        // Dont copy it - generate it new for new type always !
        impl_defineMessageFlags  (aClone, sCloneType);

        return aClone;
    }

    //-------------------------------------------------------------------------
    public static RT2Message createResponseFromMessage (final RT2Message aMessage     ,
                                                        final String     sResponseType)
        throws Exception
    {
        final RT2Message aResponse = RT2MessageFactory.cloneMessage(aMessage, sResponseType);
        // message id of response has to be the same !
        // but body needs to be fresh and new !
        aResponse.setBody(null);
        return aResponse;
    }

    //-------------------------------------------------------------------------
    public static RT2Message createRequestFromMessage (final RT2Message aMessage    ,
                                                       final String     sRequestType)
        throws Exception
    {
        final RT2Message aRequest = RT2MessageFactory.cloneMessage(aMessage, sRequestType);
        // new request needs new message id !
        aRequest.newMessageID();
        // but body needs to be fresh and new !
        aRequest.setBody(null);
        return aRequest;
    }

    //-------------------------------------------------------------------------
    private static void impl_defineMessageFlags (final RT2Message aMessage,
                                                 final String     sType   )
        throws Exception
    {
        final Integer nFlags4Type = RT2Protocol.get().getFlags4Type(sType);
        aMessage.setFlags(nFlags4Type);
    }

    //-------------------------------------------------------------------------
    private static void impl_defineDefaultHeader (final RT2Message aMessage,
                                                  final String     sType   )
        throws Exception
    {
        RT2MessageGetSet.newMessageID(aMessage);
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSONString (final String sJSON)
        throws Exception
    {
        final RT2Message aRTMsg = RT2MessageFactory.fromJSONString(sJSON, /*decode*/true);
        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSONString (final String  sJSON  ,
                                             final boolean bDecode)
        throws Exception
    {
        final JSONObject aJSON = new JSONObject ();
        aJSON.parseJSONString(sJSON);

        final RT2Message aRTMsg = RT2MessageFactory.fromJSON(aJSON, bDecode);
        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    public static RT2Message fromJSON (final JSONObject aJSON  ,
                                       final boolean    bDecode)
        throws Exception
    {
        final String      KEY_TYPE   = bDecode ? "t" : "type"  ;
        final String      KEY_HEADER = bDecode ? "h" : "header";
        final String      KEY_BODY   = bDecode ? "b" : "body"  ;

        final RT2Protocol aProtocol  = RT2Protocol.get ();
    
        String sType = (String)aJSON.getString(KEY_TYPE);
        if (bDecode)
            sType = aProtocol.decode (sType);
    
        final RT2Message aRTMsg = newMessage(sType);

        if (aJSON.hasAndNotNull(KEY_HEADER))
        {
            final Map< String, Object >              lHeader = RT2MessageFactory.impl_JSONObject2Map(aJSON.getJSONObject(KEY_HEADER));
            final Iterator< Entry< String, Object >> rHeader = lHeader.entrySet().iterator();

            while (rHeader.hasNext())
            {
                final Entry< String, Object > aHeader     = rHeader.next();
                      String                  sHeader     = aHeader.getKey();
                final Object                  aValue      = aHeader.getValue();

                if (bDecode)
                    sHeader = aProtocol.decode(sHeader);

                aRTMsg.setHeader(sHeader, aValue);
            }
        }

        final JSONObject aBody = aJSON.optJSONObject(KEY_BODY);
        aRTMsg.setBody(aBody);

        final StringBuilder sValidation = new StringBuilder (256);
        if ( ! aRTMsg.isValid(sValidation))
            throw new Exception ("Msg not valid : "+sValidation.toString ());

        return aRTMsg;
    }

    //-------------------------------------------------------------------------
    public static String toJSONString (final RT2Message aRTMsg)
        throws Exception
    {
        final String sJSON = RT2MessageFactory.toJSONString(aRTMsg, /*encode*/true);
        return sJSON;
    }

    //-------------------------------------------------------------------------
    public static JSONObject toJSON (final RT2Message aRTMsg)
        throws Exception
    {
        final JSONObject aJSON = RT2MessageFactory.toJSON(aRTMsg, /*encode*/true);
        return aJSON;
    }

    //-------------------------------------------------------------------------
    public static String toJSONString (final RT2Message aRTMsg ,
                                       final boolean    bEncode)
    	throws Exception
    {
        final JSONObject aJSON = RT2MessageFactory.toJSON (aRTMsg, bEncode);
        final String     sJSON = aJSON.toString ();
        return sJSON;
    }

    //-------------------------------------------------------------------------
    public static JSONObject toJSON (final RT2Message aRTMsg ,
                                     final boolean    bEncode)
        throws Exception
    {
        final String      KEY_TYPE   = bEncode ? "t" : "type"  ;
        final String      KEY_HEADER = bEncode ? "h" : "header";
        final String      KEY_BODY   = bEncode ? "b" : "body"  ;

        final JSONObject  aJSON      = new JSONObject ();
        final RT2Protocol aProtocol  = RT2Protocol.get ();

        String sType = aRTMsg.getType ();
        if (bEncode)
            sType = aProtocol.encode (sType);
        aJSON.put(KEY_TYPE, sType);

        final JSONObject    aJSONHeader = new JSONObject ();
        final Set< String > lHeader     = aRTMsg.listHeader();
        for (String sHeader : lHeader)
        {
            // filter out 'internal' header so they do not reach the client
            if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_JMS))
                continue;

            if (StringUtils.startsWith(sHeader, RT2Protocol.HEADER_PREFIX_INTERNAL))
                continue;

            // note: value needs to be retrieved from internal map,
            // before we might encode the header name ...
            // otherwise we will get "null" as value always ...
            // because the might encoded header is not known ;-)
            final Object aValue = aRTMsg.getHeader(sHeader);

            if (bEncode)
                sHeader = aProtocol.encode(sHeader);

            aJSONHeader.put (sHeader, aValue);
        }

        aJSON.put(KEY_HEADER, aJSONHeader);
        aJSON.put(KEY_BODY  , aRTMsg.getBody());

        return aJSON;
    }

    //-------------------------------------------------------------------------
    private static Map< String, Object > impl_JSONObject2Map (final JSONObject aJSON)
        throws Exception
    {
        final Map< String, Object > aMap = new HashMap< String, Object > ();
        final Set< Entry< String, Object >> lEntries = aJSON.entrySet();
        if (lEntries == null)
            return aMap;

        final Iterator< Entry< String, Object >> rEntries = lEntries.iterator();
        while (rEntries.hasNext())
        {
            final Entry< String, Object > rEntry = rEntries.next();
            final String                  sEntry = rEntry.getKey();
            final Object                  aValue = rEntry.getValue();
            aMap.put (sEntry, aValue);
        }

        return aMap;
    }

//    //-------------------------------------------------------------------------
//    public static void main (final String[] args)
//        throws Exception
//    {
//        final RT2Message m = new RT2Message ();
//        System.err.println (m.hasFlags ());
//        m.setFlags(18);
//
//        for (int i=0; i<32; ++i)
//            System.err.println (i+" : "+m.hasFlags (i));
//    }
}
