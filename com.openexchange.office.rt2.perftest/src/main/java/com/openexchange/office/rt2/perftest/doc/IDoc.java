/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.rt2.perftest.fwk.Deferred;
import com.openexchange.office.rt2.perftest.fwk.NewDocumentData;
import com.openexchange.office.rt2.protocol.RT2Message;

//-------------------------------------------------------------------------
public interface IDoc {

    public NewDocumentData createNew() throws Exception;

    public void removeDocFile() throws Exception;

    public void open() throws Exception;

    public int applyOps() throws Exception;

    public int applyOps(final JSONArray lOps) throws Exception;

    public Deferred<RT2Message> applyOpsAsync() throws Exception;

    public void editRights(String command, String value) throws Exception;

    public void updateUserData() throws Exception;

    public void addImageID(final JSONObject sImageId) throws Exception;

    public void close(boolean bForceQuit) throws Exception;

    public void close() throws Exception;

    public Deferred<RT2Message> closeAsync() throws Exception;

    public Deferred<RT2Message> leaveAsync(final boolean bForceQuit) throws Exception;

    public void createPreview() throws Exception;

    public Deferred<RT2Message> syncAsync() throws Exception;

    public Deferred<RT2Message> syncAsync(int opsAppliedOSN) throws Exception;

    public void saveDoc() throws Exception;
}
