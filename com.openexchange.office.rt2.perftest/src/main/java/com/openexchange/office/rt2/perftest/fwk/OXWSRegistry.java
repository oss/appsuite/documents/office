/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.HashMap;
import java.util.Map;

//=============================================================================
public class OXWSRegistry
{
    //-------------------------------------------------------------------------
    private OXWSRegistry ()
    {}

    //-------------------------------------------------------------------------
    public synchronized static OXWSRegistry get ()
        throws Exception
    {
        if (m_gSingleton == null)
            m_gSingleton = new OXWSRegistry ();
        return m_gSingleton;
    }

    //-------------------------------------------------------------------------
    public synchronized OXWSClient getClient (final String sClientId)
        throws Exception
    {
        final Map< String, OXWSClient > aClientMap = mem_ClientMap ();
              OXWSClient                aClient    = aClientMap.get (sClientId);

        if (aClient == null)
        {
            aClient = new OXWSClient ();
            aClientMap.put (sClientId, aClient);
        }
              
        return aClient;
    }
    
    //-------------------------------------------------------------------------
    private Map< String, OXWSClient > mem_ClientMap ()
        throws Exception
    {
        if (m_aClientMap == null)
            m_aClientMap = new HashMap< String, OXWSClient > ();
        return m_aClientMap;
    }

    //-------------------------------------------------------------------------
    private static OXWSRegistry m_gSingleton = null;

    //-------------------------------------------------------------------------
    private Map< String, OXWSClient > m_aClientMap = null;
}
