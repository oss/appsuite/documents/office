/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public class OXFilesRestAPI {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXFilesRestAPI.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_FILES = "/files";
    public static final String REST_API_INFOSTORE = "/infostore";

    //-------------------------------------------------------------------------
    private static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    private static final String columnsToFetchForFiles = "1,2,3,5,20,23,108,700,702,703,704,705,707,711,7040";

    //-------------------------------------------------------------------------
    private OXAppsuite m_aContext = null;

    //-------------------------------------------------------------------------
    public OXFilesRestAPI () {
    	super();
    }

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aContext)
        throws Exception {
        m_aContext = aContext;
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/files?action=document&folder=249&id=249%2F16111&version=2&34.1.1454590261792&delivery=view&scaleType=cover&width=200&height=150&format=thumbnail_image&content_type=image/jpeg&retry=1
    public void createPreviewNew(String sFolderId, String sFileId, int nVersion ) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final String baseUri = getBaseURI () + REST_API_FILES;
        final String now = String.valueOf(new Date().getTime());
        
        Validate.isTrue(!StringUtils.isEmpty(sFolderId), "Miss 'folder-id'.");
        Validate.isTrue(!StringUtils.isEmpty(sFileId  ), "Miss 'file-id'."  );
    
        final NameValuePair[] aParams = new NameValuePair[14];
        aParams[0] = new BasicNameValuePair("action", "document");
        aParams[1] = new BasicNameValuePair("folder", sFolderId);
        aParams[2] = new BasicNameValuePair("id", sFileId);
        aParams[3] = new BasicNameValuePair("version", Integer.toString(nVersion));
        aParams[4] = new BasicNameValuePair("timezone", "utc");
        aParams[5] = new BasicNameValuePair("delivery", "view");
        aParams[6] = new BasicNameValuePair("scaleType", "cover");
        aParams[7] = new BasicNameValuePair("width", "160");
        aParams[8] = new BasicNameValuePair("height", "160");
        aParams[9] = new BasicNameValuePair("height", "160");
        aParams[10] = new BasicNameValuePair("format", "thumbnail_image");
        aParams[11] = new BasicNameValuePair("content_type", "image/jpeg");
        aParams[12] = new BasicNameValuePair("retry", "1");
        aParams[13] = new BasicNameValuePair("uid", now);
    
        final URI aURI = connection.buildRequestUri(baseUri, aParams);
    
        LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+aURI).log();
    
        final UnboundHttpResponse aResponse = connection.doGet(aURI);
        final StatusLine          aResult    = aResponse.getStatusLine();
    
        if (aResult.getStatusCode() != HttpStatusCode.OK.code())
            throw new RuntimeException("bad server response : " + aResult);
        
        LOG.forLevel(ELogLevel.E_INFO).withMessage("Response : "+aResponse).log();
    }    

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/files?action=delete&hardDelete=false&session=58be72c3f4ca46e7a01bf5daf5bc3af4&timestamp=2116800000000 (PUT)
    public JSONArray deleteFile(final String sFolderId, final String sFileId) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final OXSession session = m_aContext.accessSession   ();
        final String restAPIUri = getBaseURI () + REST_API_FILES;
        final String sessionId = session.getSessionId();
        final String delTimeStamp = "2116800000000";

        final NameValuePair[] aParams = new NameValuePair[4];
        aParams[0] = new BasicNameValuePair("action", "delete");
        aParams[1] = new BasicNameValuePair("session", sessionId);
        aParams[2] = new BasicNameValuePair("hardDelete", "false");
        aParams[3] = new BasicNameValuePair("timestamp", delTimeStamp);
        final URI aURI = connection.buildRequestUri(restAPIUri, aParams);

        final JSONArray aBody = new JSONArray();
        final JSONObject aDelFileParams = new JSONObject();
        aDelFileParams.put("folder", sFolderId);
        aDelFileParams.put("id", sFileId);
        aBody.put(0, aDelFileParams);

        LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + aBody).log();

        final UnboundHttpResponse response = connection.doPut(aURI, OXConnection.CONTENT_TYPE_JSON, aBody.toString());
        final StatusLine result = response.getStatusLine();

        if (result.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()), "Bad server response : " + result);

        final String sBody = response.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aResponseBody  = new JSONObject(sBody);
        final String sError = aResponseBody.optString("error");
        if (StringUtils.isNotEmpty(sError))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, sError);

        return aResponseBody.getJSONArray(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/files?action=all&folder=417&columns=1%2C2%2C3%2C5%2C20%2C23%2C108%2C700%2C702%2C703%2C704%2C705%2C707%2C711%2C7040&sort=702
    // &order=asc&pregenerate_previews=true&timezone=utc&limit=0%2C210&session=38aef09db9db4ac2b0d3cbf93ce4474a
    public JSONArray getFiles(String folderId) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final OXSession session = m_aContext.accessSession   ();
        final String restAPIUri = getBaseURI () + REST_API_FILES;
        final String sessionId = session.getSessionId();

        Validate.isTrue(!StringUtils.isEmpty(folderId), "Miss 'folder-id'.");
    
        final NameValuePair[] aParams = new NameValuePair[9];
        aParams[0] = new BasicNameValuePair("action", "all");
        aParams[1] = new BasicNameValuePair("folder", folderId);
        aParams[2] = new BasicNameValuePair("columns", columnsToFetchForFiles);
        aParams[3] = new BasicNameValuePair("sort", Integer.toString(702));
        aParams[4] = new BasicNameValuePair("order", "asc");
        aParams[5] = new BasicNameValuePair("pregenerate_previews", "false");
        aParams[6] = new BasicNameValuePair("timezone", "utc");
        aParams[7] = new BasicNameValuePair("limit", "0, 210");
        aParams[8] = new BasicNameValuePair("session", sessionId);

    	final URI aURI = connection.buildRequestUri(restAPIUri, aParams);

    	LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+aURI).log();

    	final UnboundHttpResponse response = connection.doGet(aURI);
    	final StatusLine result = response.getStatusLine();

    	if (result.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Bad server response : " + result);

        final String body = response.getBody();
        if (StringUtils.isEmpty(body))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject jsonBody = new JSONObject(body);
        final String sError = jsonBody.optString("error");
        if (StringUtils.isNotEmpty(sError))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, sError);

        return jsonBody.getJSONArray(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    public JSONArray removeFile(final String sFolderId, final String sFileId  ) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final OXSession session = m_aContext.accessSession   ();
        final String restAPIUri = getBaseURI () + REST_API_INFOSTORE;
        final String sessionId = session.getSessionId();
        final String now = String.valueOf(new Date().getTime());

        final NameValuePair[] aParams  = new NameValuePair[4];
        aParams[0] = new BasicNameValuePair("action", "delete");
        aParams[1] = new BasicNameValuePair("hardDelete", "true");
        aParams[2] = new BasicNameValuePair("timestamp", now);
        aParams[3] = new BasicNameValuePair("session", sessionId);

        final String sRequestBody = "[{\"id\":\""+sFileId  +"\",\"folder\":\""+sFolderId+"\"}]";
        
        final URI aURI = connection.buildRequestUri(restAPIUri, aParams);
        
        LOG.forLevel(ELogLevel.E_INFO).withMessage("PUT : "+aURI).log();
        
        final UnboundHttpResponse response = connection.doPut(aURI, OXConnection.CONTENT_TYPE_JSON, sRequestBody);
        final StatusLine result = response.getStatusLine();

        if (result.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "bad server response : " + result);

        final String body = response.getBody ();
        final JSONObject jsonBody = new JSONObject(body);
        final String categories = jsonBody.optString("categories");
        
        if (StringUtils.equalsIgnoreCase(categories, "ERROR"))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Got error from server :\n" + jsonBody.toString());

        return jsonBody.getJSONArray(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/files/unnamed.docx?action=document&folder=296&id=296%2F1521&version=2&user=29&context=1&sequence=1569932946237&29.1.1569932946237&delivery=download&callback=antivirus
    public InputStream getDocumentFile(String folderId, String fileId, String version, String user, String context, String sequence) throws Exception {
    	final String restAPIUri = getBaseURI () + REST_API_INFOSTORE;
    	final OXConnection connection = m_aContext.accessConnection();
    	final OXSession session = m_aContext.accessSession   ();
    	final String sessionId = session.getSessionId();
    	final String now = String.valueOf(new Date().getTime());

    	final NameValuePair[] aParams  = new NameValuePair[7];
    	aParams[0] = new BasicNameValuePair("action", "document" );
    	aParams[1] = new BasicNameValuePair("file_id", fileId);
    	aParams[2] = new BasicNameValuePair("folder_id", folderId            );
    	aParams[3] = new BasicNameValuePair("user", user);
    	aParams[4] = new BasicNameValuePair("context", context);
    	aParams[5] = new BasicNameValuePair("session" , sessionId);
    	aParams[6] = new BasicNameValuePair("sequence", now);

    	final URI aURI = connection.buildRequestUri(restAPIUri, aParams);

    	LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+aURI).log();

    	final UnboundHttpResponse response = connection.doGet(aURI);
    	final StatusLine result = response.getStatusLine();

    	if (result.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Bad server response : " + result);

        final InputStream aSeekableInStream = new ByteArrayInputStream(IOUtils.toByteArray(response.getBodyIS()));
        aSeekableInStream.mark(0);
        final String sBody = IOUtils.toString(aSeekableInStream, "utf-8");
        
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        aSeekableInStream.reset();

        return aSeekableInStream;
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/files?action=versions&columns=1%2C2%2C3%2C4%2C5%2C20%2C23%2C51%2C52%2C100%2C102%2C108%2C109%2C700%2C701%2C702%2C703%2C704%2C705%2C706%2C707%2C708%2C709%2C710%2C711%2C7040&
    //folder=316&id=316%2F7070&timezone=utc&session=acfa80788ed14f59ae98a7150362951d
    public JSONArray getVersionsOfFile(String folderId, String fileId) throws Exception {
    	final String restAPIUri = getBaseURI () + REST_API_INFOSTORE;
    	final OXConnection connection = m_aContext.accessConnection();
    	final OXSession session = m_aContext.accessSession   ();
    	final String sessionId = session.getSessionId();

    	final NameValuePair[] aParams  = new NameValuePair[6];
    	aParams[0] = new BasicNameValuePair("action", "versions" );
    	aParams[1] = new BasicNameValuePair("id", fileId);
    	aParams[2] = new BasicNameValuePair("folder_id", folderId);
    	aParams[3] = new BasicNameValuePair("columns", columnsToFetchForFiles);
        aParams[4] = new BasicNameValuePair("timezone", "utc");
    	aParams[5] = new BasicNameValuePair("session" , sessionId);

    	final URI aURI = connection.buildRequestUri(restAPIUri, aParams);

    	LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+aURI).log();

    	final UnboundHttpResponse response = connection.doGet(aURI);
    	final StatusLine result = response.getStatusLine();

    	if (result.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Bad server response : " + result);

        final String body = response.getBody ();
        final JSONObject jsonBody = new JSONObject(body);
        final String categories = jsonBody.optString("categories");
        
        if (StringUtils.equalsIgnoreCase(categories, "ERROR"))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Got error from server :\n" + jsonBody.toString());

        return jsonBody.getJSONArray(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    private String getBaseURI () throws Exception {
        return m_aContext.accessConfig().sAPIRelPath;
    }

}
