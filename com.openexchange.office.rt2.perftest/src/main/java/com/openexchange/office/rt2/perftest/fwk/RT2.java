/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.doc.MessageProperties;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openexchange.office.rt2.protocol.internal.EDocState;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.tools.common.NumberUtils;

//=============================================================================
public class RT2 implements IRT2MessageHandler {

    //-------------------------------------------------------------------------
    public static final String STR_OPERATIONS    = "operations";
    public static final String STR_ACTION        = "action";
    public static final String STR_BODY          = "body";
    public static final String STR_OSN           = "serverOSN";
    public static final String STR_ERROR         = "error";
    public static final String STR_FASTEMPTY     = "fastEmpty";
    public static final String STR_FASTEMPTY_OSN = "fastEmptyOSN";
    public static final String STR_VALUE         = "value";

    public static final String STR_SELECTIONS = "selections";
    public static final String STR_TYPE       = "type";
    public static final String STR_START      = "start";
    public static final String STR_END        = "end";

    public static final String ACTION_EDITRIGHTS_REQUESTS = "request";
    public static final String ACTION_EDITRIGHTS_APPROVED = "approved";
    public static final String ACTION_EDITRIGHTS_DECLINED = "declined";
    public static final String ACTION_EDITRIGHTS_FORCE    = "force";
    public static final String ACTION_EDITRIGHTS_ALL      = "@ALL";

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(RT2.class);

    //-------------------------------------------------------------------------
    public static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    public static final long TIMEOUT_4_MISSING_ACKS_IN_MS = 60000;
    public static final long INTERVAL_4_FLUSH_ACK_IN_MS   = 30000;

    //-------------------------------------------------------------------------
    public static boolean                    HANDLE_PROTOCOL_ERRORS_GRACEFULLY = false;
    public static boolean                    SIMULATE_BAD_CONNECTION           = false;
    public static ConfiguredConnectionStatus BAD_CONNECTION_CONF               = new ConfiguredConnectionStatus();

    //-------------------------------------------------------------------------
    public RT2() throws Exception {}

    //-------------------------------------------------------------------------
    public static String calcDriveDocID(final TestRunConfig aConfig, final String sFileId) {
        final StringBuilder sId = new StringBuilder(32);

        sId.append(aConfig.aUser.nContextNr);
        sId.append("@");
        sId.append(sFileId);

        return sId.toString();
    }

    //-------------------------------------------------------------------------
    public synchronized void resetDocData() {
        m_sDocType = null;
        m_sFolderId = null;
        m_sFileId = null;
        m_sDriveDocId = null;
        m_sDocUID = null;
        m_nSeqNrOut.set(0);
        m_nSeqNrIn.set(0);
    }

    //-------------------------------------------------------------------------
    public synchronized OXAppsuite getContext() {
        return m_aContext;
    }

    //-------------------------------------------------------------------------
    protected synchronized void bind(final OXAppsuite aContext) throws Exception {
        m_aContext = aContext;
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void free() throws Exception {
        impl_dispose();
    }

    //-------------------------------------------------------------------------
    public synchronized void enableSessionValidationOnServerSide(final boolean bEnabled) throws Exception {
        m_bEnableSessionValidationOnServerSide = bEnabled;
    }

    //-------------------------------------------------------------------------
    public synchronized void setEventHandler(final IEventHandler iHandler) throws Exception {
        m_iEventHandler = iHandler;
    }

    //-------------------------------------------------------------------------
    public synchronized void setMessageExceptionHandler(final RT2MessageExceptionHandler msgExceptionHandler) throws Exception {
        m_msgExceptionHandler = msgExceptionHandler;
    }

    //-------------------------------------------------------------------------
    public synchronized void setDocType(final String sDocType) throws Exception {
        m_sDocType = sDocType;
    }

    //-------------------------------------------------------------------------
    public synchronized void setFolderId(final String sFolderId) throws Exception {
        m_sFolderId = sFolderId;
    }

    //-------------------------------------------------------------------------
    public synchronized void setFileId(final String sFileId) throws Exception {
        m_sFileId = sFileId;
    }

    //-------------------------------------------------------------------------
    public synchronized void setDriveDocId(final String sDriveDocId) throws Exception {
        m_sDriveDocId = sDriveDocId;
    }

    //-------------------------------------------------------------------------
    public synchronized String getFolderId() throws Exception {
        return mem_FolderId();
    }

    //-------------------------------------------------------------------------
    public synchronized String getFileId() throws Exception {
        return m_sFileId;
    }

    //-------------------------------------------------------------------------
    public synchronized String getDriveDocId() throws Exception {
        return mem_DriveDocId();
    }

    //-------------------------------------------------------------------------
    public synchronized String getRT2ClientUID() throws Exception {
        return m_clientUID;
    }

    //-------------------------------------------------------------------------
    public synchronized String getDocUID() throws Exception {
        return mem_DocUID();
    }

    //-------------------------------------------------------------------------
    public synchronized String getNodeID() throws Exception {
        if (StringUtils.isEmpty(m_sNodeID))
            m_sNodeID = "";
        return m_sNodeID;
    }

    //-------------------------------------------------------------------------
    public synchronized int getLostMessageCount() throws Exception {
        return m_nCountLostMessages;
    }

    //-------------------------------------------------------------------------
    public synchronized int getResendCount() throws Exception {
        return m_nCountResends.get();
    }

    //-------------------------------------------------------------------------
    public synchronized NewDocumentData createFileNew() throws Exception {
        final OXDocRestAPI aAPI = mem_DocRestAPI();
        final String sFolderId = mem_FolderId();
        final String sDocType = m_sDocType;
        final boolean bUseUUIDFileNames = impl_hasToUseUUIDFileNames();
        final String sDocFileName = bUseUUIDFileNames ? UUID.randomUUID().toString() : "unnamed";

        final NewDocumentData aNewDocData = aAPI.createNewDocument(sFolderId, sDocType, sDocFileName);
        if (aNewDocData != null)
            this.m_sFileId = aNewDocData.getFileId();
        else {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("No suitable answer from createNewDocument!").log();
        }

        return aNewDocData;
    }

    //-------------------------------------------------------------------------
    public synchronized void removeFile() throws Exception {
        final OXFilesRestAPI aAPI = getFilesRESTAPI();
        final String sFolderId = mem_FolderId();
        final String sFileId = mem_FileId();

        aAPI.removeFile(sFolderId, sFileId);
    }

    //-------------------------------------------------------------------------
    //http://localhost/appsuite/api/files?action=document&folder=249&id=249%2F16111&version=2&34.1.1454590261792&delivery=view&scaleType=cover&width=200&height=150&format=thumbnail_image&content_type=image/jpeg&retry=1
    public synchronized void createPreviewNew(final int nVersion) throws Exception {
        final OXFilesRestAPI aAPI = getFilesRESTAPI();
        final String sFolderId = mem_FolderId();
        final String sFileId = mem_FileId();

        aAPI.createPreviewNew(sFolderId, sFileId, nVersion);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> request(final String sRequest, final Map<String, Object> lHeader, final String sBody) throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(sRequest, lHeader, sBody);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> join() throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_JOIN, null, null);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> leave(boolean bForceQuit) throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_LEAVE, null, null);

        if (bForceQuit)
            RT2MessageGetSet.setAutoClose(aRequest, bForceQuit);

        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> open(final JSONObject aOpenData) throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_OPEN_DOC, null, null);

        // DOCS-4649: we need to set the mandatory old_client_uid - as we don't reload
        // we can always set this header to an empty string.
        RT2MessageGetSet.setOldClientUID(aRequest, "");
        if ((null != aOpenData) && aOpenData.has(MessageConstants.STR_FASTEMPTY)) {
            // set optional fast empty data for the backend
            RT2MessageGetSet.setFastEmpty(aRequest, aOpenData.getBoolean(MessageConstants.STR_FASTEMPTY));
            RT2MessageGetSet.setFastEmptyOSN(aRequest, aOpenData.getInt(MessageConstants.STR_FASTEMPTY_OSN));
        }

        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> close() throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_CLOSE_DOC, null, null);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> save() throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_SAVE_DOC, null, null);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> applyOps(final JSONArray lOperations) throws Exception {
        final JSONObject aOperationsBody = new JSONObject();
        aOperationsBody.put(MessageProperties.PROP_OPERATIONS, lOperations);

        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_APPLY_OPS, null, aOperationsBody.toString());
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> updateUserData(final JSONObject aUpdatedUserDataBody) throws Exception {
        final Map<String, Object> lHeader = new HashMap<>();
        lHeader.put(RT2Protocol.HEADER_APP_ACTION, "updateUserData");
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_APP_ACTION, lHeader, aUpdatedUserDataBody.toString());
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> editRights(String sCommand, int nCurrentOSN, String sValue) throws Exception {
        final JSONObject aEditRightsBody = new JSONObject();
        aEditRightsBody.put(MessageProperties.PROP_ACTION, sCommand);
        aEditRightsBody.put(MessageProperties.PROP_OSN, nCurrentOSN);
        // set optional value depending on the command
        if (null != sValue)
            aEditRightsBody.put(MessageProperties.PROP_VALUE, sValue);
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_EDITRIGHTS, null, aEditRightsBody.toString());
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> addImageId(final JSONObject aAddedImageData) throws Exception {
        final Map<String, Object> lHeader = new HashMap<>();
        lHeader.put(RT2Protocol.HEADER_APP_ACTION, "add_image_id_request");
        final JSONObject aBody = new JSONObject();
        aBody.put("imageInfo", aAddedImageData);
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_APP_ACTION, lHeader, aBody.toString());
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> sync(int clientOSN) throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_SYNC, null, null);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred<RT2Message> saveDoc() throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(RT2Protocol.REQUEST_SAVE_DOC, null, null);
        return impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void sendRequest(final String sRequest, final Map<String, Object> lHeader, final String sBody) throws Exception {
        final RT2Message aRequest = impl_createDefaultRequest(sRequest, null, sBody);
        aRequest.mergeHeader(lHeader);

        impl_sendRequest(aRequest);
    }

    //-------------------------------------------------------------------------
    @Override
    public /* no synchronized */ void onMessage(final Object aSource, final RT2Message aResponse) throws Exception {
        final String sMsgType = aResponse.getType();
        final boolean bHasSeqNr = aResponse.isSequenceMessage();
        boolean bBreak = false;

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("handle msg ...").setVar("response", aResponse).log();

        if (SIMULATE_BAD_CONNECTION && bHasSeqNr && !impl_isImportantMsg(sMsgType)) {
            // We just count every message and return in case
            // we reached the LOST_MESSAGE_RATE count.
            m_nMsgReceived.incrementAndGet();
            if (mustDropMessage(m_nMsgReceived, aResponse))
                return;
        }

        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.ACK_SIMPLE)) {
            // handle ACK response and return - no need for further notification
            impl_handleAck(aResponse);
            return;
        }

        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.BROADCAST_UPDATE_CLIENTS)) {
            LOG.forLevel(ELogLevel.E_TRACE).withMessage("BROADCAST_UPDATE_CLIENTS received: " + aResponse.getBodyString()).log();
        }

        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.NACK_SIMPLE)) {
            // handle NACK request and return - no need for further notification
            impl_handleNack(aResponse);
            return;
        }

        //        if (PERFORMANCE_MEASUREMENT_ENABLED === true)
        //            bBreak = impl_handlePerformanceMeasureData (aResponse);
        //        if (bBreak)
        //            return;

        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_SWITCH_DOCSTATE))
            bBreak = impl_handleSwitchDocState(aResponse);
        if (bBreak)
            return;

        // some errors have to be handled here directly
        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_GENERIC_ERROR))
            bBreak = impl_handleGenericError(aResponse);
        if (bBreak)
            return;

        if (StringUtils.equalsIgnoreCase(sMsgType, RT2Protocol.RESPONSE_LEAVE)) {
            final RT2Channel aChannel = mem_ComChannel();
            final String sClientUID = m_clientUID;
            aChannel.unregisterClient(sClientUID);
        }

        //      MessagePerformanceStats.get().analyzeMessage(aResponse);

        // TODO find better place to handle that
        m_sNodeID = aResponse.getHeader(RT2Protocol.HEADER_DEBUG_STATISTIC_NODEID);

        impl_processResponse(aResponse);
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_handleAck(final RT2Message aAck) throws Exception {
        final String sMsgId = aAck.getMessageID();

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("handle ACK ...").setVar("message", aAck).setVar("message-id", sMsgId).log();

        final JSONObject aBody = aAck.getBody();
        final JSONArray lAckList = aBody.optJSONArray(RT2Protocol.BODYPART_ACKS);

        if (lAckList == null) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage(".. no ACK data in body !").setVar("message-id", sMsgId).log();
            return;
        }

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("... remove pending messages according to ACKs").setVar("message-id", sMsgId).setVar("ack-list", lAckList).log();

        final Map<Integer, AckCheck> lAckChecks = mem_AckChecks();
        final long nNow = System.currentTimeMillis();

        synchronized (this) {
            for (int i = 0; i < lAckList.length(); i++) {
                final int nSeqNr = lAckList.getInt(i);
                final AckCheck aAckCheck = lAckChecks.remove(nSeqNr);

                if (aAckCheck == null) {
                    LOG.forLevel(ELogLevel.E_WARNING).withMessage("... there is no AckCheck item available for SEQ-NR = " + nSeqNr).setVar("message-id", sMsgId).log();
                    continue;
                }

                final long nTime = (nNow - aAckCheck.nSentAt);
                if (nTime > TIMEOUT_4_MISSING_ACKS_IN_MS) {
                    LOG.forLevel(ELogLevel.E_WARNING).withMessage("... ACK needed much time to be received for SEQ-NR = " + nSeqNr).setVar("message-id", sMsgId).setVar("ack-time", nTime).log();
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_handleNack(final RT2Message aNack) throws Exception {
        final String sMsgId = aNack.getMessageID();

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("handle NACK ...").setVar("message", aNack).setVar("message-id", sMsgId).log();

        final JSONObject aBody = aNack.getBody();
        final JSONArray lNackList = aBody.optJSONArray(RT2Protocol.BODYPART_NACKS);

        if (lNackList == null) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage(".. no NACK data in body !").setVar("message-id", sMsgId).log();
            return;
        }

        LOG.forLevel(ELogLevel.E_INFO).withMessage("... resend requested messages according to NACKs").setVar("message-id", sMsgId).setVar("nack-list", lNackList).log();

        final Map<Integer, AckCheck> lAckChecks = mem_AckChecks();
        final List<RT2Message> lMessages4Resend = new ArrayList<>();

        synchronized (this) {
            for (int i = 0; i < lNackList.length(); i++) {
                final int nSeqNr = lNackList.getInt(i);
                final AckCheck aAckCheck = lAckChecks.get(nSeqNr);
                final RT2Message aMessage4Resend = aAckCheck != null ? aAckCheck.aMessage : null;

                if (aMessage4Resend == null) {
                    LOG.forLevel(ELogLevel.E_ERROR).withMessage("... no message found for SEQ-NR = " + nSeqNr).setVar("message-id", sMsgId).log();
                    triggerEvent(RT2Const.EVENT_ERROR, null); // TODO add string param
                    return;
                }

                LOG.forLevel(ELogLevel.E_INFO).withMessage("... resend requested message [" + nSeqNr + " : " + sMsgId + "] according to NACK").log();

                lMessages4Resend.add(aMessage4Resend);
            }
        }

        for (final RT2Message aMessage4Resend : lMessages4Resend) {
            // send requested msg as stored - don't update seq-nr!
            impl_sendRequest(aMessage4Resend, true);

            m_nCountResends.incrementAndGet();
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ boolean impl_handleSwitchDocState(final RT2Message aResponse) throws Exception {
        final EDocState eDocState = RT2MessageGetSet.getDocState(aResponse);
        final boolean bGoOffline = eDocState.equals(EDocState.E_OFFLINE);
        final boolean bGoOnline = eDocState.equals(EDocState.E_ONLINE);

        LOG.forLevel(ELogLevel.E_INFO).withMessage("doc state switch to [" + eDocState + "] ...").log();

        // switch internal state and notify our listener
        if (bGoOffline) {
            triggerEvent(RT2Const.EVENT_OFFLINE, aResponse);
            m_bOnline.set(false);
        } else if (bGoOnline) {
            triggerEvent(RT2Const.EVENT_ONLINE, aResponse);
            m_bOnline.set(true);
        }

        return /* break further handling */ true;
    }

    //----------------------------------------------------------------------
    private synchronized boolean impl_handleGenericError(final RT2Message aResponse) throws Exception {
        final ErrorCode aError = RT2MessageGetSet.getError(aResponse);

        if (aError.equals(ErrorCode.GENERAL_SESSION_INVALID_ERROR)) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("Session invalid. Force reconnect.").log();

            // lost session is more or less a 'final' error
            // No further request will be accepted by the server
            // close connection and stop message processing

            // this is called first in rt2.js
            impl_stopFlushAcks();
            impl_resetACKQueue();
            impl_resetDocResponseQueue();
            impl_closeConnection();

            // this is not called within rt2.js ...
            // it's called 'later' within derived classes (as e.g. rt2deferredwrapper.js)
            // so we have to 'simulate' that and call these methods after impl_closeConnection() !
            impl_rejectAllDeferredsOnErrorMessage(aResponse);
        } else {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("Got server error : " + aError).log();

            // don't close connection for 'normal' errors.
            // client wants to trigger 'auto-close' might be ...
            // which isn't possible if we close all before ...

            // called within rt2deferredwrapper.js
            impl_rejectAllDeferredsOnErrorMessage(aResponse);
        }

        triggerEvent(RT2Const.EVENT_ERROR, aResponse);

        return /* break further handling */ true;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_processResponse(final RT2Message aResponse) throws Exception {
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("process response ...").setVar("response", aResponse).log();

        final boolean bHasSeqNr = aResponse.isSequenceMessage();
        final Integer nActSeqNr = bHasSeqNr ? RT2MessageGetSet.getSeqNumber(aResponse) : null;
        final Integer nExpectedSeqNr = m_nSeqNrIn.get() + 1;
        final boolean bOutOfOrder = !NumberUtils.equals(nActSeqNr, nExpectedSeqNr);

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("... check seq-nr[" + nActSeqNr + "] against last-seq-nr[" + (m_nSeqNrIn.get()) + "]").log();

        // a) response messages without seq-nr needs to be forwarded directly
        //    it's prio is higher then 'normal' messages ;-)

        if (!bHasSeqNr) {
            LOG.forLevel(ELogLevel.E_TRACE).withMessage("... prio response without seq-nr : forward message direct").log();
            impl_notifyResponse(aResponse);
            return;
        }

        // b) we already got N response messages.
        //    Now there is a message with N-x as seq-nr ... but those message was already handled !?
        //    This is a duplicate message which can be ignored.

        if (nActSeqNr < nExpectedSeqNr) {
            // this is just an info - it this happens massively there could be a problem
            // but it can happen sometimes  - this can be handled by our protocol without
            // a problem
            LOG.forLevel(ELogLevel.E_INFO).withMessage("... unexpected response with seq-nr[" + nActSeqNr + "] where expected seq-nr[" + nExpectedSeqNr + "] : will be ignored").log();
            return;
        }

        // c1) buffer response message

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("... queue response with seq-nr[" + nActSeqNr + "]").log();
        mem_DocResponseQueue().put(nActSeqNr, aResponse);

        // c2) store received seq number for lazy flush ack
        mem_ReceivedSeqNumbers().add(nActSeqNr);

        // d) look for responses within buffer, which are ready to be send
        //    Can be more than one if a message closes a gap in the queue.

        final List<RT2Message> lReadyResponses = impl_getReadyDocResponsesOrdered();
        for (final RT2Message aReadyResponse : lReadyResponses) {
            Validate.notNull(aReadyResponse, "Ready response is null ?! Whats happen ?");
            final Integer nReadySeqNr = RT2MessageGetSet.getSeqNumber(aReadyResponse);

            LOG.forLevel(ELogLevel.E_TRACE).withMessage("... found bufferred response with seq-nr[" + nReadySeqNr + "] : forward it").log();

            impl_notifyResponse(aReadyResponse);
        }

        // d) a message was out-of-order or a message closed a gap
        //    => check if we need to send a NACK message for further possible gaps

        if ((bOutOfOrder) || (!lReadyResponses.isEmpty())) {
            final List<Integer> lMissingSeqNrs = impl_getMissingSeqNrs();
            if (!lMissingSeqNrs.isEmpty()) {
                LOG.forLevel(ELogLevel.E_TRACE).withMessage("... requesting the following missing SEQ-NRS : " + lMissingSeqNrs).log();

                final JSONObject jBody = new JSONObject();
                jBody.put(RT2Protocol.BODYPART_NACKS, new JSONArray(lMissingSeqNrs));

                final RT2Message aNackRequest = impl_createNackRequest(jBody.toString());
                impl_sendRequest(aNackRequest);
            }
        }

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("response processed.").setVar("response", aResponse).log();
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_notifyResponse(final RT2Message aResponse) throws Exception {
        final String sMsgId = RT2MessageGetSet.getMessageID(aResponse);
        final boolean bIsError = RT2MessageGetSet.isError(aResponse);

        Deferred<RT2Message> aDeferred = null;
        synchronized (this) {
            aDeferred = mem_Deferreds().get(sMsgId);
        }

        if (aDeferred != null) {
            if (bIsError) {
                LOG.forLevel(ELogLevel.E_ERROR).withMessage("response with errors : reject deferred").log();
                aDeferred.reject(aResponse);
            } else {
                LOG.forLevel(ELogLevel.E_TRACE).withMessage("response ok : resolve deferred").log();
                aDeferred.resolve(aResponse);
            }

            if (aDeferred.isClosed()) {
                synchronized (this) {
                    mem_Deferreds().remove(sMsgId);
                }
            }
        }

        triggerEvent(IEventHandler.EVENT_DOC_RESPONSE, aResponse);
    }

    //----------------------------------------------------------------------
    private synchronized List<RT2Message> impl_getReadyDocResponsesOrdered() throws Exception {
        final List<RT2Message> lReadyResponses = new ArrayList<>();
        final Map<Integer, RT2Message> aResponseQueue = mem_DocResponseQueue();
        final Integer nSeqNrStart = m_nSeqNrIn.get();
        Integer nSeqNrTry = nSeqNrStart;
        Integer nSeqNrFinal = nSeqNrStart;

        while (true) {
            nSeqNrTry++;

            if (!aResponseQueue.containsKey(nSeqNrTry))
                break;

            final RT2Message aResponse = aResponseQueue.remove(nSeqNrTry);

            nSeqNrFinal = nSeqNrTry;
            lReadyResponses.add(aResponse);
        }

        m_nSeqNrIn.set(nSeqNrFinal);
        return lReadyResponses;
    }

    //----------------------------------------------------------------------
    private synchronized List<Integer> impl_getMissingSeqNrs() throws Exception {
        final List<Integer> lMissingSeqNrs = new ArrayList<>();
        final Map<Integer, RT2Message> aResponseQueue = mem_DocResponseQueue();
        int nSeqNrNext = m_nSeqNrIn.get();

        if (aResponseQueue.isEmpty())
            return lMissingSeqNrs;

        while (true) {
            nSeqNrNext++;

            if (aResponseQueue.containsKey(nSeqNrNext))
                break;

            lMissingSeqNrs.add(nSeqNrNext);
        }

        return lMissingSeqNrs;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_sendRequest(final RT2Message aRequest) throws Exception {
        return impl_sendRequest(aRequest, false);
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ Deferred<RT2Message> impl_sendRequest(final RT2Message aRequest, boolean bResend) throws Exception {
        final RT2Channel aChannel = mem_ComChannel();
        final String sClientUID = m_clientUID;
        final String sMsgId = RT2MessageGetSet.getMessageID(aRequest);
        final Map<String, Deferred<RT2Message>> lDeferreds = mem_Deferreds();
        final Deferred<RT2Message> aDeferred = new Deferred<>();

        aDeferred.setMetaData("request", aRequest);
        lDeferreds.put(sMsgId, aDeferred);

        //      MsgStats.get ().statMsg(aRequest, MsgStats.LOC_WS_CLIENT_OUT);

        if (!bResend)
            impl_defineAndCacheSeqNr4RequestIfNeeded(aRequest);

        RT2Message.cleanHeaderBeforeSendToServer(aRequest);

        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("send request ...").setVar("request", aRequest).log();

        final boolean bHasSeqNr = RT2MessageGetSet.hasSeqNumber(aRequest);
        final String sMsgType = RT2MessageGetSet.getType(aRequest);
        if (SIMULATE_BAD_CONNECTION && bHasSeqNr && !impl_isImportantMsg(sMsgType)) {
            // We just count every message to be sent and return in case
            // we reached the LOST_MESSAGE_RATE count.
            m_nMsgSent.incrementAndGet();
            if (mustDropMessage(m_nMsgSent, aRequest))
                return aDeferred;
        }

        aChannel.sendRequest(sClientUID, aRequest);

        return aDeferred;
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ void impl_defineAndCacheSeqNr4RequestIfNeeded(final RT2Message aRequest) throws Exception {
        if (!aRequest.isSequenceMessage())
            return;

        if (RT2MessageGetSet.hasSeqNumber(aRequest))
            throw new Exception("SEQ-NR handling broken ? Creating new SEQ-NR found an already existing one !");

        final int nSeqNr = m_nSeqNrOut.incrementAndGet();
        RT2MessageGetSet.setSeqNumber(aRequest, nSeqNr);

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("waiting for SEQ-NR: " + nSeqNr).log();

        final Map<Integer, AckCheck> lAckChecks = mem_AckChecks();
        final AckCheck aAckCheck = new AckCheck();

        aAckCheck.aMessage = aRequest;
        aAckCheck.nSentAt = System.currentTimeMillis();

        lAckChecks.put(nSeqNr, aAckCheck);
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ RT2Message impl_createDefaultRequest(final String sType, final Map<String, Object> lHeader, final String sBody) throws Exception {
        final RT2Message aRequest = RT2MessageFactory.newMessage(sType);
        aRequest.setBodyString(sBody);

        if (lHeader != null)
            aRequest.mergeHeader(lHeader);

        RT2MessageGetSet.setClientUID(aRequest, m_clientUID);
        RT2MessageGetSet.setDocUID(aRequest, mem_DocUID());

        if (aRequest.isSessionMessage())
            RT2MessageGetSet.setSessionID(aRequest, impl_getCurrentSessionId());

        // server side handling needs those values for these two requests only ....
        // where we can safe some bytes if we hide them on all other requests.
        // those values are cached on server side after open ...
        if ((StringUtils.equals(sType, RT2Protocol.REQUEST_JOIN)) || (StringUtils.equals(sType, RT2Protocol.REQUEST_OPEN_DOC))) {
            RT2MessageGetSet.setDriveDocID(aRequest, mem_DriveDocId());
            RT2MessageGetSet.setFolderID(aRequest, mem_FolderId());
            RT2MessageGetSet.setFileID(aRequest, mem_FileId());
            RT2MessageGetSet.setDocType(aRequest, mem_DocType());
        }

        synchronized (this) {
            if (!m_bEnableSessionValidationOnServerSide)
                aRequest.setHeader(RT2Protocol.HEADER_DEBUG_DISABLE_SESSION_VALIDATION, true);
        }

        return aRequest;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ RT2Message impl_createSimpleRequest(final String sType, final String sBody) throws Exception {
        final RT2Message aRequest = RT2MessageFactory.newMessage(sType);
        aRequest.setBodyString(sBody);

        RT2MessageGetSet.setClientUID(aRequest, m_clientUID);
        RT2MessageGetSet.setDocUID(aRequest, mem_DocUID());

        if (aRequest.isSessionMessage())
            RT2MessageGetSet.setSessionID(aRequest, impl_getCurrentSessionId());

        synchronized (this) {
            if (!m_bEnableSessionValidationOnServerSide)
                aRequest.setHeader(RT2Protocol.HEADER_DEBUG_DISABLE_SESSION_VALIDATION, true);
        }

        return aRequest;
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ RT2Message impl_createFlushAck(final String sBody) throws Exception {
        final RT2Message aRequest = impl_createSimpleRequest(RT2Protocol.ACK_SIMPLE, sBody);
        return aRequest;
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ RT2Message impl_createNackRequest(final String sBody) throws Exception {
        final RT2Message aRequest = impl_createSimpleRequest(RT2Protocol.NACK_SIMPLE, sBody);
        return aRequest;
    }

    //-------------------------------------------------------------------------
    protected /* no synchronized */ void triggerEvent(final String sEvent, final RT2Message aResponse) throws Exception {
        IEventHandler iHandler = null;
        synchronized (this) {
            iHandler = m_iEventHandler;
        }

        if (iHandler == null)
            return;

        try {
            iHandler.onEvent(this, sEvent, aResponse);
        } catch (Throwable exIgnore) {
            LOG.forLevel(ELogLevel.E_ERROR).withError(exIgnore).log();
        }
    }

    //-------------------------------------------------------------------------
    private void impl_sendFlushAck() throws Exception {
        if (!m_bOnline.get())
            return;

        final List<Integer> lReceivedSeqNrs = impl_getAndResetRecievedSeqNumbers();
        if (lReceivedSeqNrs.isEmpty())
            return;

        final JSONArray aSeqNrArray = new JSONArray(lReceivedSeqNrs);
        final com.openexchange.office.rt2.perftest.fwk.JSONObject aJSONBody = new com.openexchange.office.rt2.perftest.fwk.JSONObject();
        aJSONBody.put(RT2Protocol.BODYPART_ACKS, aSeqNrArray);

        final RT2Message aFlushAck = impl_createFlushAck(aJSONBody.toString());
        impl_sendRequest(aFlushAck);
    }

    //-------------------------------------------------------------------------
    private synchronized List<Integer> impl_getAndResetRecievedSeqNumbers() throws Exception {
        final List<Integer> lOrg = mem_ReceivedSeqNumbers();
        final List<Integer> lClone = new ArrayList<>();
        lClone.addAll(lOrg);
        lOrg.clear();
        return lClone;
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_startFlushAcks() throws Exception {
        if (m_aFlushAckFuture != null)
            return;

        m_aFlushAckRunnable = (new StoppableRunnable() {

            @Override
            public void run() {
                try {
                    // make sure that we don't call sendFlushAck if we were
                    // advised to stop
                    if (!isStopped()) {
                        impl_sendFlushAck();
                    }
                } catch (InterruptedException e) {
                    // nothing to do - possible if we stop the task
                    Thread.currentThread().interrupt();
                } catch (Throwable ex) {
                    LOG.forLevel(ELogLevel.E_ERROR).withError(ex).log();
                }
            }
        });

        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("start flush-acks ...").log();

        m_aFlushAckFuture = RT2ScheduledThreadPoolExecutor.get().getThreadPool().scheduleAtFixedRate(m_aFlushAckRunnable, INTERVAL_4_FLUSH_ACK_IN_MS, INTERVAL_4_FLUSH_ACK_IN_MS, TimeUnit.MILLISECONDS);
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ void impl_stopFlushAcks() throws Exception {
        ScheduledFuture<?> aFlushAckFuture = null;
        StoppableRunnable stoppableRunnable = null;

        synchronized (this) {
            aFlushAckFuture = m_aFlushAckFuture;
            m_aFlushAckFuture = null;
            stoppableRunnable = m_aFlushAckRunnable;
            m_aFlushAckRunnable = null;
        }

        if ((aFlushAckFuture == null) || (stoppableRunnable == null))
            return;

        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("stop flush-acks task ...").log();

        stoppableRunnable.stop();
        aFlushAckFuture.cancel(true);

        if (!aFlushAckFuture.isDone()) {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("... terminating flush ACK task not successful").log();
        }
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ void impl_resetACKQueue() throws Exception {
        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("reset Ack buffer ...").log();
        mem_AckChecks().clear();
    }

    //----------------------------------------------------------------------
    private synchronized void impl_resetDocResponseQueue() throws Exception {
        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("reset Doc-Response queue ...").log();
        mem_DocResponseQueue().clear();
    }

    //----------------------------------------------------------------------
    private synchronized void impl_resetSeqNr() throws Exception {
        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("reset SEQ-NR generator ...").log();
        m_nSeqNrIn.set(0);
        m_nSeqNrOut.set(0);
    }

    //-------------------------------------------------------------------------
    private static String impl_calcDocUID(final String sDriveDocId, final String sFolderId, final String sFileId) throws Exception {
        // current product implementation uses drive-doc-ID only !
        Validate.notEmpty(sDriveDocId, "Invalid argument 'driveDocId'.");

        final String HEXDIGITS = "0123456789abcdef";
        final String sJoinedID = sDriveDocId; // DO NOT ADD FOLDER AND FILE ID TO THE HASH ! Drive is strange - will be strange - for ever and ever ... !!!
        final byte[] lRaw = sJoinedID.getBytes("utf-8");
        final byte[] aRawHash = DigestUtils.md5(lRaw);
        final StringBuilder sHexHash = new StringBuilder(256);

        for (byte nByte : aRawHash) {
            final int nHex = (nByte & 0xff);
            final char nHigh = HEXDIGITS.charAt(nHex >> 0x4);
            final char nLow = HEXDIGITS.charAt(nHex & 0xf);
            sHexHash.append(nHigh);
            sHexHash.append(nLow);
        }

        return sHexHash.toString();
    }

    //----------------------------------------------------------------------
    private synchronized String impl_getCurrentSessionId() throws Exception {
        final OXSession aSession = m_aContext.accessSession();
        final String sSessionId = aSession.getSessionId();
        Validate.notEmpty(sSessionId, "No session found - but requested.");
        return sSessionId;
    }

    //----------------------------------------------------------------------
    private /* no synchronized */ void impl_closeAllDeferreds() throws Exception {
        LOG.forLevel(ELogLevel.E_DEBUG).withMessage("close all deferreds ...").log();

        final Map<String, Deferred<RT2Message>> aDeferredMap = new HashMap<>();
        synchronized (this) {
            aDeferredMap.putAll(mem_Deferreds());
        }

        final Iterator<Entry<String, Deferred<RT2Message>>> rDeferredMap = aDeferredMap.entrySet().iterator();
        while (rDeferredMap.hasNext()) {
            try {
                final Entry<String, Deferred<RT2Message>> aEntry = rDeferredMap.next();
                final Deferred<RT2Message> aDeferred = aEntry.getValue();

                aDeferred.close();
            } catch (final Throwable exIgnore) {
                LOG.forLevel(ELogLevel.E_ERROR).withError(exIgnore).log();
                // dont break loop if one deferred makes trouble !
            }
        }
    }

    //----------------------------------------------------------------------
    private synchronized void impl_rejectAllDeferredsOnErrorMessage(final RT2Message aResponse) throws Exception {
        LOG.forLevel(ELogLevel.E_ERROR).withMessage("reject all deferreds on error ...").log();

        final String sMessageID = RT2MessageGetSet.getMessageID(aResponse);
        final Iterator<Entry<String, Deferred<RT2Message>>> aDeferredMap = mem_Deferreds().entrySet().iterator();
        while (aDeferredMap.hasNext()) {
            try {
                final Entry<String, Deferred<RT2Message>> aEntry = aDeferredMap.next();
                final String sDeferredMessageId = aEntry.getKey();
                final Deferred<RT2Message> aDeferred = aEntry.getValue();

                if (StringUtils.equals(sDeferredMessageId, sMessageID))
                    aDeferred.reject(aResponse);
                else
                    aDeferred.reject(null);
            } catch (final Throwable exIgnore) {
                LOG.forLevel(ELogLevel.E_ERROR).withError(exIgnore).log();
                // dont break loop if one deferred makes trouble !
            }
        }
    }

    //-------------------------------------------------------------------------
    private static boolean impl_isImportantMsg(final String sMsgType) throws Exception {
        boolean result = false;

        Validate.notEmpty(sMsgType, "Invalid argument 'sMsgType'.");

        switch (sMsgType) {
            case RT2Protocol.REQUEST_OPEN_DOC:
            case RT2Protocol.RESPONSE_OPEN_DOC:
            case RT2Protocol.RESPONSE_LEAVE:
            case RT2Protocol.RESPONSE_CLOSE_DOC:
            case RT2Protocol.REQUEST_LEAVE:
            case RT2Protocol.REQUEST_CLOSE_DOC:
                result = true;
                break;
            default:
                result = false;
                break;
        }

        return result;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_dispose() throws Exception {
        impl_stopFlushAcks();
        impl_closeConnection();
        impl_closeAllDeferreds();
        impl_resetACKQueue();
        impl_resetDocResponseQueue();
        impl_resetSeqNr();
    }

    //-------------------------------------------------------------------------
    private boolean mustDropMessage(AtomicInteger aValue, final RT2Message aMsg) throws Exception {
        boolean bDrop = false;

        ConnectionProblems aConfiguredConnProblem = BAD_CONNECTION_CONF.getConfigureConnProblem();
        if (aConfiguredConnProblem == ConnectionProblems.NO_PROBLEMS) {
            // NOTHING TO DO
        } else if (aConfiguredConnProblem == ConnectionProblems.CONSTANT_DROP_RATE) {
            if (aValue.compareAndSet(BAD_CONNECTION_CONF.getOptionalDropRate(), 0))
                bDrop = true;
        } else if (aConfiguredConnProblem == ConnectionProblems.AVERAGE_DROP_RATE) {
            int nRandom = m_aRandomizer.nextInt(100);
            if (nRandom < BAD_CONNECTION_CONF.getOptionalDropRate())
                bDrop = true;
        } else {
            // not supported yet
            throw new NotImplementedException();
        }

        if (bDrop) {
            LOG.forLevel(ELogLevel.E_WARNING).withMessage("SIMULATE_BAD_CONNECTION - drop the following message: " + RT2MessageGetSet.getSeqNumber(aMsg) + ", msg-type: " + aMsg.getType()).log();
        }

        return bDrop;
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_closeConnection() throws Exception {
        if (m_aComChannel == null)
            return;

        m_bOnline.set(false);
        impl_stopFlushAcks();

        // connection is not opened exclusive for us ...
        // it's shared across all RT2 instances ...
        // Dont close it - dont stop it (btw: there is no API regarding these).
        // But deregister this client from that channel.

        m_aComChannel.unregisterClient(m_clientUID);
        m_aComChannel = null;
    }

    //-------------------------------------------------------------------------
    private synchronized RT2Channel mem_ComChannel ()
        throws Exception
    {
        if (m_aComChannel == null)
        {
        	final RT2Channel channel = this.m_aContext.accessRT2Env().getRT2Channel();
            final String sClientUID = m_clientUID;
            final RT2MessageExceptionHandler usedMsgExceptionHandler = m_msgExceptionHandler;

            channel.registerClient(sClientUID, this);
            channel.setMessageExceptionHandler(usedMsgExceptionHandler);

            m_aComChannel = channel;

            impl_startFlushAcks ();
            m_bOnline.set(true);
        }
        return m_aComChannel;
    }

    //-------------------------------------------------------------------------
    private synchronized String mem_FolderId() throws Exception {
        if (m_sFolderId == null) {
            final OXSession aSession = m_aContext.accessSession();
            m_sFolderId = Integer.toString(aSession.getHomeFolderId());
        }
        return m_sFolderId;
    }

    //-------------------------------------------------------------------------
    private synchronized String mem_FileId() throws Exception {
        Validate.isTrue(StringUtils.isNotEmpty(m_sFileId), "Miss 'file-id'.");
        return m_sFileId;
    }

    //-------------------------------------------------------------------------
    private synchronized String mem_DriveDocId() throws Exception {
        if (m_sDriveDocId == null) {

            final TestRunConfig aCfg = m_aContext.accessConfig();
            final StringBuilder sId = new StringBuilder(32);

            // fallback if context is configured
            Integer contextUser = null;
            if (aCfg.aUser.nContextNr == null) {
                final OXSession aSession = m_aContext.accessSession();
                contextUser = aSession.getContextId();
            }

            sId.append(contextUser);
            sId.append("@");
            sId.append(mem_FileId());

            m_sDriveDocId = sId.toString();
        }

        return m_sDriveDocId;
    }

    //-------------------------------------------------------------------------
    private synchronized String mem_DocType() throws Exception {
        Validate.isTrue(StringUtils.isNotEmpty(m_sDocType), "Miss 'doc-type'.");
        return m_sDocType;
    }

    //-------------------------------------------------------------------------
    private synchronized String mem_DocUID() throws Exception {
        if (m_sDocUID == null)
            m_sDocUID = impl_calcDocUID(mem_DriveDocId(), mem_FolderId(), mem_FileId());
        return m_sDocUID;
    }

    //-------------------------------------------------------------------------
    private synchronized Map<Integer, RT2Message> mem_DocResponseQueue() throws Exception {
        if (m_aResponseReorderQueue == null)
            m_aResponseReorderQueue = new HashMap<>();
        return m_aResponseReorderQueue;
    }

    //-------------------------------------------------------------------------
    private synchronized Map<Integer, AckCheck> mem_AckChecks() throws Exception {
        if (m_lAckChecks == null)
            m_lAckChecks = new HashMap<>();
        return m_lAckChecks;
    }

    //-------------------------------------------------------------------------
    private synchronized List<Integer> mem_ReceivedSeqNumbers() {
        if (m_lRecievedSeqNumbers == null)
            m_lRecievedSeqNumbers = new ArrayList<>();
        return m_lRecievedSeqNumbers;
    }

    //-------------------------------------------------------------------------
    private synchronized OXDocRestAPI mem_DocRestAPI() throws Exception {
        if (m_aDocRestAPI == null) {
            m_aDocRestAPI = new OXDocRestAPI();
            m_aDocRestAPI.bind(m_aContext);
        }
        return m_aDocRestAPI;
    }

    //-------------------------------------------------------------------------
    private synchronized OXFilesRestAPI getFilesRESTAPI() throws Exception {
    	if (m_aFilesRestAPI == null) {
    		m_aFilesRestAPI = new OXFilesRestAPI();
    		m_aFilesRestAPI.bind(m_aContext);
    	}
    	return m_aFilesRestAPI;
    }

    //-------------------------------------------------------------------------
    private synchronized Map<String, Deferred<RT2Message>> mem_Deferreds() throws Exception {
        if (m_lDeferreds == null)
            m_lDeferreds = new HashMap<>();
        return m_lDeferreds;
    }

    //-------------------------------------------------------------------------
    private synchronized Boolean impl_hasToUseUUIDFileNames() throws Exception {
        if (this.m_bUseUUIDFileNames == null) {
            final TestConfig aConfig = TestConfig.get();
            m_bUseUUIDFileNames = aConfig.useUUIDFileNames();
        }
        return m_bUseUUIDFileNames;
    }

    //-------------------------------------------------------------------------
    private static class AckCheck {

        public RT2Message aMessage = null;
        public Long       nSentAt  = null;
    }

    final private String m_clientUID = UUID.randomUUID().toString();

    //-------------------------------------------------------------------------
    private OXAppsuite m_aContext = null;

    //-------------------------------------------------------------------------
    private boolean m_bEnableSessionValidationOnServerSide = true;

    //-------------------------------------------------------------------------
    private Boolean m_bUseUUIDFileNames = null;

    //-------------------------------------------------------------------------
    private String m_sDocType = null;

    //-------------------------------------------------------------------------
    private String m_sFolderId = null;

    //-------------------------------------------------------------------------
    private String m_sFileId = null;

    //-------------------------------------------------------------------------
    private String m_sDriveDocId = null;

    //-------------------------------------------------------------------------
    private String m_sDocUID = null;

    //-------------------------------------------------------------------------
    private String m_sNodeID = null;

    //-------------------------------------------------------------------------
    private AtomicBoolean m_bOnline = new AtomicBoolean(false);

    //-------------------------------------------------------------------------
    private AtomicInteger m_nSeqNrOut = new AtomicInteger(0);

    //-------------------------------------------------------------------------
    private AtomicInteger m_nSeqNrIn = new AtomicInteger(0);

    //-------------------------------------------------------------------------
    private AtomicInteger m_nMsgReceived = new AtomicInteger(0);

    //-------------------------------------------------------------------------
    private AtomicInteger m_nMsgSent = new AtomicInteger(0);

    //-------------------------------------------------------------------------
    private Map<Integer, RT2Message> m_aResponseReorderQueue = null;

    //-------------------------------------------------------------------------
    private RT2Channel m_aComChannel = null;

    //-------------------------------------------------------------------------
    private OXDocRestAPI m_aDocRestAPI = null;

    //-------------------------------------------------------------------------
    private OXFilesRestAPI m_aFilesRestAPI = null;

    //-------------------------------------------------------------------------
    private Map<Integer, AckCheck> m_lAckChecks = null;

    //-------------------------------------------------------------------------
    private List<Integer> m_lRecievedSeqNumbers = null;

    //-------------------------------------------------------------------------
    private int m_nCountLostMessages = 0;

    //-------------------------------------------------------------------------
    private AtomicInteger m_nCountResends = new AtomicInteger(0);

    //-------------------------------------------------------------------------
    private ScheduledFuture<?> m_aFlushAckFuture = null;

    //-------------------------------------------------------------------------
    private StoppableRunnable m_aFlushAckRunnable = null;

    //-------------------------------------------------------------------------
    private Map<String, Deferred<RT2Message>> m_lDeferreds = null;

    //-------------------------------------------------------------------------
    private IEventHandler m_iEventHandler = null;

    //-------------------------------------------------------------------------
    private Random m_aRandomizer = new Random(42);

    //-------------------------------------------------------------------------
    volatile private RT2MessageExceptionHandler m_msgExceptionHandler = null;
}
