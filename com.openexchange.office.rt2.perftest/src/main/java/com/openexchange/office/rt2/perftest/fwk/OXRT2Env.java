/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class OXRT2Env
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXRT2Env.class);
    
    //-------------------------------------------------------------------------
    private OXAppsuite context = null;

    //-------------------------------------------------------------------------
    private RT2Channel channel = null;

    //-------------------------------------------------------------------------
    public OXRT2Env ()
    	throws Exception
    {}

    //-------------------------------------------------------------------------
    protected synchronized void bind (final OXAppsuite aContext)
        throws Exception
    {
    	context = aContext;
    }

    //-------------------------------------------------------------------------
    public synchronized RT2 newRT2Inst ()
    	throws Exception
    {
    	final RT2 aRT2Inst = new RT2 ();
    	aRT2Inst.bind (context);

    	LOG.forLevel(ELogLevel.E_TRACE).withMessage("new RT2 instance created").log();

    	return aRT2Inst;
    }

    //-------------------------------------------------------------------------
    public synchronized RT2Channel getRT2Channel() throws Exception {
    	if (channel == null) {
    		channel = RT2Channel.createChannel(context);
    	}
    	return channel;
    }

    //-------------------------------------------------------------------------
    public synchronized OXAppsuite getContext() {
        return context;
    }

    //-------------------------------------------------------------------------
    public synchronized void resetRT2Channel() {
        channel = null;
    }

}
