/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config.items;

import com.openexchange.office.rt2.perftest.config.ConfigAccess;
import com.openexchange.office.rt2.perftest.config.ConfigUtils;
import com.openxchange.office_communication.tools.config.IComplexConfiguration;

//=============================================================================

public class PresenterTestConfig {
    //-------------------------------------------------------------------------
    public static final String CONFIG_PACKAGE = "/presentertest";

    //-------------------------------------------------------------------------
    public static final int     DEFAULT_CLIENT_COUNT                      =     0;
    public static final int     DEFAULT_SLIDE_COUNT                       =     0;
    public static final int     DEFAULT_SLIDE_CHANGES                     =    10;
    public static final long    DEFAULT_SLIDE_CHANGES_DELAY               =  1000;
    public static final int     DEFAULT_TIMEOUT_4_OPEN                    = 30000;
    public static final int     DEFAULT_TIMEOUT_4_CLOSE                   = 30000;
    public static final int     DEFAULT_TIMEOUT_4_APPLY                   = 30000;
    public static final boolean DEFAULT_CONTINUE_ON_TIMEOUT               =  true;
    public static final boolean DEFAULT_AUTO_CLOSE_ON_ERROR               =  true;
    public static final long    DEFAULT_JOIN_PRESENTATION_TIMEFRAME_IN_MS = 60000;
    public static final long    DEFAULT_LEAVE_PRESENTATION_DELAY_IN_MS    =  1000;

    //-------------------------------------------------------------------------
    private IComplexConfiguration m_iConfig = null;

    //-------------------------------------------------------------------------
    public PresenterTestConfig () throws Exception {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    public Integer getClientCount () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.client.count", String.class), DEFAULT_CLIENT_COUNT);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getSlideCount () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.slide.count", String.class), DEFAULT_SLIDE_COUNT);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getSlideChanges () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.slide.changes", String.class), DEFAULT_SLIDE_CHANGES);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public Long getSlideChangesDelay () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Long                  nValue = ConfigUtils.readLong (iCfg.get("test.presenter.slide.changes.delay", String.class), DEFAULT_SLIDE_CHANGES_DELAY);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4OpenInMS () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.timeout.open", String.class), DEFAULT_TIMEOUT_4_OPEN);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4CloseInMS () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.timeout.close", String.class), DEFAULT_TIMEOUT_4_CLOSE);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4ApplyInMS () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.presenter.timeout.apply", String.class), DEFAULT_TIMEOUT_4_APPLY);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getContinueOnTimeout () throws Exception {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("test.presenter.timeout.continue", String.class), DEFAULT_CONTINUE_ON_TIMEOUT);
    	return bValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getAutoCloseDocOnError () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("test.presenter.autoclose.on.error", String.class), DEFAULT_AUTO_CLOSE_ON_ERROR);
        return bValue;
    }

    //-------------------------------------------------------------------------
    public Long getJoinPresentationTimeFrameInMS () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Long                  nValue = ConfigUtils.readLong (iCfg.get("test.presenter.join-presentation.time-frame", String.class), DEFAULT_JOIN_PRESENTATION_TIMEFRAME_IN_MS);
        return nValue;
    }

    public Long getLeavePresentationDelayInMS () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Long                  nValue = ConfigUtils.readLong (iCfg.get("test.presenter.leave-presentation.delay", String.class), DEFAULT_LEAVE_PRESENTATION_DELAY_IN_MS);
        return nValue;
    }

    //-------------------------------------------------------------------------
    public String getTestFolderID () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final String                sValue = iCfg.get("test.presenter.folder-id", String.class);
        return sValue;
    }

    //-------------------------------------------------------------------------
    public String getTestFileID () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final String                sValue = iCfg.get("test.presenter.file-id", String.class);
        return sValue;
    }

    //-------------------------------------------------------------------------
    public String getTestDriveDocID () throws Exception {
        final IComplexConfiguration iCfg   = mem_Config ();
        final String                sValue = iCfg.get("test.presenter.drive-doc-id", String.class);
        return sValue;
    }

    //-------------------------------------------------------------------------
    private final IComplexConfiguration mem_Config () throws Exception {
    	if (m_iConfig == null)
    		m_iConfig = ConfigAccess.accessConfig(CONFIG_PACKAGE);
    	return m_iConfig;
    }

}
