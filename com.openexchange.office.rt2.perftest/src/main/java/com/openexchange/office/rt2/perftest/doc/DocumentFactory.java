/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import com.openexchange.office.rt2.perftest.doc.presentation.PresentationDoc;
import com.openexchange.office.rt2.perftest.doc.presenter.PresenterDoc;
import com.openexchange.office.rt2.perftest.doc.spreadsheet.SpreadsheetDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;

//=============================================================================
public class DocumentFactory
{
    //-------------------------------------------------------------------------
	private DocumentFactory() {}

    //-------------------------------------------------------------------------
	public static Doc create(final String sDocType) throws Exception
	{
		Doc aDoc;

		switch (sDocType)
		{
		case Doc.DOCTYPE_TEXT: aDoc = new TextDoc(); break;
		case Doc.DOCTYPE_SPREADSHEET: aDoc = new SpreadsheetDoc (); break;
		case Doc.DOCTYPE_PRESENTATION: aDoc = new PresentationDoc (); break;
		case Doc.DOCTYPE_PRESENTER: aDoc = new PresenterDoc (); break;
		default: throw new UnsupportedOperationException ("No support for doc type '"+sDocType+"' implemented yet.");
		}

        return aDoc;
	}

}
