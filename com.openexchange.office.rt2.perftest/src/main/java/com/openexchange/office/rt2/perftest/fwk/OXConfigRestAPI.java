/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public class OXConfigRestAPI {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXConfigRestAPI.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_JSLOB = "/jslob";

    //-------------------------------------------------------------------------
    public static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    private OXAppsuite m_aContext = null;

    //-------------------------------------------------------------------------
    public OXConfigRestAPI () throws Exception {
    }

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aContext) throws Exception {
        m_aContext = aContext;
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/jslob?action=list&session=f3e3b3ac386d4443ab394c1197ae250e (PUT)
    public org.json.JSONObject list(final String configTreeData) throws Exception {
        final OXConnection connection = m_aContext.accessConnection();
        final OXSession session = m_aContext.accessSession   ();
        final String restAPIUri = getBaseURI () + REST_API_JSLOB;
        final String sessionId = session.getSessionId();

        final NameValuePair[] aParams = new NameValuePair[2];
        aParams[0] = new BasicNameValuePair("action", "list");
        aParams[1] = new BasicNameValuePair("session", sessionId);
        final URI aURI = connection.buildRequestUri(restAPIUri, aParams);

        final JSONArray aBody = new JSONArray();
        aBody.put(0, configTreeData);

        LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + aBody).log();

        final UnboundHttpResponse response = connection.doPut(aURI, OXConnection.CONTENT_TYPE_JSON, aBody.toString());
        final StatusLine result = response.getStatusLine();

        if (result.getStatusCode() != HttpStatusCode.OK.code())
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()), "Bad server response : " + result);

        final String sBody = response.getBody();
        if (StringUtils.isEmpty(sBody))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject aResponseBody  = new JSONObject(sBody);
        final String sError = aResponseBody.optString("error");
        if (StringUtils.isNotEmpty(sError))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, sError);

        final JSONArray configDatas = aResponseBody.getJSONArray(REST_JSON_ROOT);
        if (configDatas == null)
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is not correctly stuctured!");

        return configDatas.getJSONObject(0).getJSONObject("tree");
    }

    //-------------------------------------------------------------------------
    private String getBaseURI ()throws Exception {
        return m_aContext.accessConfig().sAPIRelPath;
    }

}
