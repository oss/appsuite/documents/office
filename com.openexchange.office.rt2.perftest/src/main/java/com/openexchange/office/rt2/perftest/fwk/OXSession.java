/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
/**
 * http://oxpedia.org/wiki/index.php?title=HTTP_API
 * http://oxpedia.org/wiki/index.php?title=OXSessionLifecycle
 */
public class OXSession implements ISessionAPI {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXSession.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_LOGIN = "/login?";
    public static final String REST_API_JSLOB = "/jslob";
    public static final String REST_API_MANIFESTS = "/apps/manifests";

    public static final String ACTION_LOGIN = "login";
    public static final String ACTION_STORE = "store";
    public static final String ACTION_LOGOUT = "logout";
    public static final String ACTION_REFRESH = "refreshSecret";
    public static final String ACTION_LIST = "list";
    public static final String ACTION_CONFIG = "config";

    public static final String PARAM_ACTION = "action";
    public static final String PARAM_USER_NAME = "name";
    public static final String PARAM_USER_PASSWORD = "password";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_CONTEXT_ID = "context_id";
    public static final String PARAM_SESSION = "session";
    public static final String PARAM_RAMPUP = "rampup";
    public static final String PARAM_RAMPUPFOR = "rampupFor";
    public static final String PARAM_TIMEOUT = "timeout";
    public static final String PARAM_CLIENT = "client";
    public static final String PARAM_DATA = "data";
    public static final String PARAM_LOCALE = "locale";
    public static final String PARAM_VERSION = "version";

    public static final String COOKIE_JSESSIONID = "JSESSIONID";

    private TestRunConfig m_aConfig = null;
    private OXConnection m_aConnection = null;
    private UserDescriptor m_aUser = null;
    private Integer m_nContextId = null;
    private Integer m_nUserId = null;
    private Integer m_nHomeFolderId = null;
    private String m_sSessionId = null;
    private String m_sPublicSessionCookie = null;
    private String m_sSessionCookie = null;
    private String m_sSecretCookie = null;
    private boolean m_bAutoLogin = false;
    private long m_nLastAccessTime = 0;
    private boolean m_useAlternativeHost = false;

    //-------------------------------------------------------------------------
    public OXSession() throws Exception {}

    //-------------------------------------------------------------------------
    public OXSession(boolean useAlternativeHost) throws Exception {
        m_useAlternativeHost = useAlternativeHost;
    }

    //-------------------------------------------------------------------------
    protected synchronized void bind(final OXAppsuite aAppsuite) throws Exception {
        m_aConfig = aAppsuite.accessConfig();
        m_aConnection = aAppsuite.accessConnection();
    }

    //-------------------------------------------------------------------------
    public boolean usesAlternativeHost() {
        return m_useAlternativeHost;
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized void login() throws Exception {
        login(m_aConfig.aUser);
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized void login(final UserDescriptor aUser) throws Exception {
        LOG.forLevel(ELogLevel.E_INFO).withMessage("login user '" + aUser + "' ...").log();

        final String sSessionID = impl_login(aUser);

        if (m_bAutoLogin)
            impl_makeSessionPersistent(aUser, sSessionID);
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized void logout() throws Exception {
        if (!impl_isSessionValid())
            throw new RuntimeException("There is no valid session for logout. Please login first.");

        LOG.forLevel(ELogLevel.E_INFO).withMessage("logout user '" + m_aUser + "' ...").log();

        final URI aRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_LOGIN, new BasicNameValuePair(PARAM_ACTION, ACTION_LOGOUT), new BasicNameValuePair(PARAM_SESSION, m_sSessionId));
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("GET : " + aRequest).log();

        final UnboundHttpResponse aResponse = m_aConnection.doGet(aRequest);

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Response : " + aResponse).log();

        m_aUser = null;
        m_nUserId = null;
        m_sSessionId = null;
        m_nHomeFolderId = null;
    }

    //-------------------------------------------------------------------------
    public synchronized void refresh() throws Exception {
        if (!impl_isSessionValid())
            throw new RuntimeException("There is no valid session for refresh. Please login first.");

        LOG.forLevel(ELogLevel.E_INFO).withMessage("refresh session for user '" + m_aUser + "' ...").log();

        final URI aRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_LOGIN, new BasicNameValuePair(PARAM_ACTION, ACTION_REFRESH), new BasicNameValuePair(PARAM_SESSION, m_sSessionId));
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("GET : " + aRequest).log();

        final UnboundHttpResponse aResponse = m_aConnection.doGet(aRequest);

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Response : " + aResponse).log();

        if (aResponse.getStatusLine().getStatusCode() != 200)
            throw new Error("Session refresh failed for user '" + m_aUser.sLogin + "'.");

        impl_updateSessionPartsFromCookies();
        m_nLastAccessTime = System.currentTimeMillis();
    }

    //-------------------------------------------------------------------------
    public synchronized void refreshIfNeeded() throws Exception {
        final long nActiveMS = impl_getActiveTimeInMS();
        if (nActiveMS > 60000)
            refresh();
    }

    //-------------------------------------------------------------------------
    public synchronized boolean isAutoLogin() throws Exception {
        return m_bAutoLogin;
    }

    //-------------------------------------------------------------------------
    public synchronized String getLoadBalanceRoute() throws Exception {
        final Map<String, Cookie> lCookies = m_aConnection.getCookies();
        final Cookie aJSESSIONID = lCookies.get("JSESSIONID");

        if (aJSESSIONID == null)
            return null;

        final String sJESSIONID = aJSESSIONID.getValue();
        final String sRoute = StringUtils.substringAfterLast(sJESSIONID, ".");
        return sRoute;
    }

    //-------------------------------------------------------------------------
    public synchronized void simulateBrokenSession() throws Exception {
        m_sSessionId = UUID.randomUUID().toString();
    }

    //-------------------------------------------------------------------------
    public synchronized String getSessionId() throws Exception {
        return m_sSessionId;
    }

    //-------------------------------------------------------------------------
    public synchronized String getPublicSessionCookie() throws Exception {
        return m_sPublicSessionCookie;
    }

    //-------------------------------------------------------------------------
    public synchronized String getSessionCookie() throws Exception {
        return m_sSessionCookie;
    }

    //-------------------------------------------------------------------------
    public synchronized String getSecretCookie() throws Exception {
        return m_sSecretCookie;
    }

    //-------------------------------------------------------------------------
    public synchronized int getUserId() throws Exception {
        return m_nUserId;
    }

    //-------------------------------------------------------------------------
    public synchronized int getContextId() {
        return m_nContextId;
    }

    //-------------------------------------------------------------------------
    public synchronized int getHomeFolderId() throws Exception {
        return m_nHomeFolderId;
    }

    //-------------------------------------------------------------------------
    private String impl_login(final UserDescriptor aUser) throws Exception {
        if (impl_isSessionValid())
            throw new RuntimeException("There is already a valid session. Please logout first before start new login ...");

        final TestConfig aConfig = TestConfig.get();
        final int nSessionTimeout = aConfig.getSessionTimeoutInUNKNOWN();

        int tries = 0;
        boolean tryAgain = true;

        // on some systems we need a second try for login
        UnboundHttpResponse aResponse = null;
        String sResponseBody;
        JSONObject aResponseBody = null;
        do {
            ++tries;
            aResponse = sendLoginRequest(aUser, nSessionTimeout);

            if (aResponse.getStatusLine().getStatusCode() != 200)
                throw new Error("Login not possible for user '" + aUser.sLogin + "'.");

            LOG.forLevel(ELogLevel.E_TRACE).withMessage("Response : " + aResponse).log();

            sResponseBody = aResponse.getBody();
            aResponseBody = new JSONObject(sResponseBody);

            if (aResponseBody.has("error")) {
                if ((tries >= 2) || ((aResponseBody.has("code")) && (aResponseBody.getString("code").equalsIgnoreCase("LGI-0006")))) {
                    // {"error":"The user name or password is incorrect.","error_params":["context-1"],"categories":"USER_INPUT","category":1,"code":"LGI-0006","error_id":"-1284417881-62","error_desc":"Missing context mapping for context \"context-1\". Login failed.","error_stack":["Missing context mapping for context \"context-1\". Login failed."]}
                    throw new Error("login user '" + aUser.sLogin + "' failed : " + aResponseBody.get("error"));
                }
            } else {
                // success
                tryAgain = false;
            }
        } while (tryAgain);

        final String sSession = aResponseBody.getString(PARAM_SESSION);
        final int nUserId = aResponseBody.getInt(PARAM_USER_ID);
        final int nContextId = aResponseBody.optInt(PARAM_CONTEXT_ID);

        Integer nHomeFolderId = null;

        final JSONObject aRampUpData = aResponseBody.optJSONObject(PARAM_RAMPUP);
        if ((null != aRampUpData) && (aRampUpData.length() > 0)) {
            nHomeFolderId = getHomeFolderId(aResponseBody.getJSONObject(PARAM_RAMPUP).getJSONObject("jslobs").getJSONObject("io.ox/core"));
        } else {
            LOG.forLevel(ELogLevel.E_TRACE).withMessage("Fallback due to empty ramup data, PUT : " + aResponse).log();

            final UnboundHttpResponse aJSlobResponse = sendJSlobRequest(sSession, "io.ox/core");
            final String sJSlobResponseBody = aJSlobResponse.getBody();
            final JSONObject aJSlobResponseBody = new JSONObject(sJSlobResponseBody);

            nHomeFolderId = getHomeFolderId(aJSlobResponseBody.getJSONArray(PARAM_DATA).getJSONObject(0));

            final UnboundHttpResponse aManifestResponse = sendAppsManifestRequest(ACTION_CONFIG);
            final String sManifestResponseBody = aManifestResponse.getBody();
            final JSONObject aManifestResponseBody = new JSONObject(sManifestResponseBody);

            aResponseBody = aManifestResponseBody;
        }

        if (nHomeFolderId == null) {
            LOG.forLevel(ELogLevel.E_ERROR).withMessage("Login/Jslob response did not contain (rampup) data - could not retrieve home folder ID.").log();
        }

        impl_updateSessionPartsFromCookies();

        m_aUser = aUser;
        m_nUserId = nUserId;
        m_nContextId = nContextId;
        m_sSessionId = sSession;
        m_nHomeFolderId = nHomeFolderId;
        m_nLastAccessTime = System.currentTimeMillis();
        m_bAutoLogin = impl_isAutoLogin(aResponseBody);

        return m_sSessionId;
    }

    //-------------------------------------------------------------------------
    private UnboundHttpResponse sendLoginRequest(final UserDescriptor aUser, int nSessionTimeout) throws Exception {
        final URI aRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_LOGIN);

        final String sRequestBody = OXConnection.encodeUriParams(
            new BasicNameValuePair(PARAM_ACTION, ACTION_LOGIN),
            new BasicNameValuePair(PARAM_CLIENT, "open-xchange-appsuite"), // no chance to change that ! fix value to simulate appsuite frontend client and retrieve e.g. rampup configuration  ...
            new BasicNameValuePair(PARAM_RAMPUP, "true"),
            new BasicNameValuePair(PARAM_RAMPUPFOR, "open-xchange-appsuite"), // no chance to change that ! fix value to simulate appsuite frontend client and retrieve e.g. rampup configuration  ...
            new BasicNameValuePair(PARAM_TIMEOUT, Integer.toString(nSessionTimeout)),
            new BasicNameValuePair(PARAM_USER_NAME, aUser.sLogin),
            new BasicNameValuePair(PARAM_USER_PASSWORD, aUser.sPassword),
            new BasicNameValuePair(PARAM_LOCALE, "en-US"),
            new BasicNameValuePair(PARAM_VERSION, "7.10.5"));
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("POST : " + aRequest).log();

        return m_aConnection.doPost(aRequest, OXConnection.CONTENT_TYPE_X_WWW_FORM_URLENCODED, sRequestBody);
    }

    //-------------------------------------------------------------------------
    private UnboundHttpResponse sendJSlobRequest(String sSession, String... strings) throws Exception {
        final JSONArray aJSONBody = new JSONArray();
        final URI aJSlobRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_JSLOB, new BasicNameValuePair(PARAM_ACTION, ACTION_LIST), new BasicNameValuePair(PARAM_SESSION, sSession));

        for (String s : strings)
            aJSONBody.put(s);

        return m_aConnection.doPut(aJSlobRequest, OXConnection.CONTENT_TYPE_TEXT_JAVASCRIPT, aJSONBody.toString());
    }

    //-------------------------------------------------------------------------
    private UnboundHttpResponse sendAppsManifestRequest(String sAction) throws Exception {
        final URI aJSlobRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_MANIFESTS, new BasicNameValuePair(PARAM_ACTION, sAction));
        return m_aConnection.doGet(aJSlobRequest);
    }

    //-------------------------------------------------------------------------
    private Integer getHomeFolderId(final JSONObject aResponseBody) {
        return aResponseBody.getJSONObject("tree").getJSONObject("folder").optInt("infostore");
    }

    //-------------------------------------------------------------------------
    private void impl_makeSessionPersistent(final UserDescriptor aUser, final String sSessionId) throws Exception {
        final TestConfig aConfig = TestConfig.get();
        final int nSessionTimeout = aConfig.getSessionTimeoutInUNKNOWN();

        final URI aRequest = m_aConnection.buildRequestUri(m_aConfig.sAPIRelPath + REST_API_LOGIN);

        final String sRequestBody = OXConnection.encodeUriParams(new BasicNameValuePair(PARAM_ACTION, ACTION_STORE), new BasicNameValuePair(PARAM_TIMEOUT, Integer.toString(nSessionTimeout)), new BasicNameValuePair(PARAM_SESSION, sSessionId));
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("POST : " + aRequest).log();

        final UnboundHttpResponse aResponse = m_aConnection.doPost(aRequest, OXConnection.CONTENT_TYPE_X_WWW_FORM_URLENCODED, sRequestBody);
        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Response : " + aResponse).log();

        if (aResponse.getStatusLine().getStatusCode() != 200)
            throw new Error("Making session persistent not possible for user '" + aUser.sLogin + "'.");

        final String sResponseBody = aResponse.getBody();
        final JSONObject aResponseBody = new JSONObject(sResponseBody);

        // {"error":"The user name or password is incorrect.","error_params":["context-1"],"categories":"USER_INPUT","category":1,"code":"LGI-0006","error_id":"-1284417881-62","error_desc":"Missing context mapping for context \"context-1\". Login failed.","error_stack":["Missing context mapping for context \"context-1\". Login failed."]}
        if (aResponseBody.has("error"))
            throw new Error("login user '" + aUser.sLogin + "' failed : " + aResponseBody.get("error"));

        impl_updateSessionPartsFromCookies();
    }

    //-------------------------------------------------------------------------
    private synchronized boolean impl_isSessionValid() throws Exception {
        final boolean bIsValid = !StringUtils.isEmpty(m_sSessionId);
        return bIsValid;
    }

    //-------------------------------------------------------------------------
    private synchronized long impl_getActiveTimeInMS() throws Exception {
        final long nNow = System.currentTimeMillis();
        final long nTime = nNow - m_nLastAccessTime;
        return nTime;
    }

    //-------------------------------------------------------------------------
    private void impl_updateSessionPartsFromCookies() throws Exception {
        final Iterator<Entry<String, Cookie>> lCookies = m_aConnection.getCookies().entrySet().iterator();
        while (lCookies.hasNext()) {
            final Entry<String, Cookie> aCookie = lCookies.next();
            final String sCookie = aCookie.getKey();

            if (StringUtils.startsWith(sCookie, "open-xchange-public-session-")) {
                final Cookie aValue = aCookie.getValue();
                final String sSession = aValue.getValue();
                m_sPublicSessionCookie = sSession;
            } else if (StringUtils.startsWith(sCookie, "open-xchange-session-")) {
                final Cookie aValue = aCookie.getValue();
                final String sSession = aValue.getValue();
                m_sSessionCookie = sSession;
            } else if (StringUtils.startsWith(sCookie, "open-xchange-secret-")) {
                final Cookie aValue = aCookie.getValue();
                final String sSecret = aValue.getValue();
                m_sSecretCookie = sSecret;
            }
        }
    }

    //-------------------------------------------------------------------------
    private boolean impl_isAutoLogin(final JSONObject aResponse) throws Exception {
        final JSONObject aRampupObject = aResponse.optJSONObject(PARAM_RAMPUP);

        JSONArray lCaps = null;

        if (null != aRampupObject) {
            lCaps = aResponse.getJSONObject(PARAM_RAMPUP).getJSONObject("serverConfig").getJSONArray("capabilities");
        } else {
            lCaps = aResponse.getJSONObject(PARAM_DATA).getJSONArray("capabilities");
        }

        final int c = lCaps.length();
        int i = 0;
        for (i = 0; i < c; ++i) {
            final JSONObject aCap = lCaps.getJSONObject(i);
            final String sCap = aCap.getString("id");

            if (StringUtils.equalsIgnoreCase(sCap, "autologin"))
                return true;
        }

        return false;
    }

}
