/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import com.jamonapi.proxy.MonProxyFactory;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.config.items.LoadTestConfig;
import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.IDoc;
import com.openexchange.office.rt2.perftest.doc.text.TextDoc;
import com.openexchange.office.rt2.perftest.fwk.ISessionAPI;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXRT2Env;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.fwk.RT2;
import com.openexchange.office.rt2.perftest.fwk.StatusLine;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;

//=============================================================================
public class TextDocumentLoadTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    public TextDocumentLoadTest ()
        throws Exception
    {
    	super();
    }

    //-------------------------------------------------------------------------
    @Override
    public int calculateTestIterations ()
        throws Exception
    {
        final LoadTestConfig aTestCfg    = new LoadTestConfig();
        final int            nDocs       = aTestCfg.getRepetitions4Docs   ();
        final int            nApplys     = aTestCfg.getRepetitions4Applys ();
        final int            nIterations = nDocs * nApplys;
    	return nIterations;
    }

    //-------------------------------------------------------------------------
    @Override
    public void runTest ()
        throws Exception
    {
        final TestRunConfig  aCfg               = getRunConfig ();
        final StatusLine     aStatusLine        = getStatusLine ();
        final LoadTestConfig aTestCfg           = new LoadTestConfig();
        final UserDescriptor aUser              = aCfg.aUser;

        final OXAppsuite     aOX                = new OXAppsuite      (aCfg);
        final OXSession      aSession           = aOX.accessSession   ();
        final OXRT2Env       aRT2Env            = aOX.accessRT2Env    ();
        final ISessionAPI    iSession           = (ISessionAPI) MonProxyFactory.monitor(aSession);

        final int            nTimeoutMS4Open    = aTestCfg.getTimeout4OpenInMS   ();
        final int            nTimeoutMS4Apply   = aTestCfg.getTimeout4ApplyInMS  ();
        final int            nTimeoutMS4Close   = aTestCfg.getTimeout4CloseInMS  ();
        final boolean        bContinueOnTimeout = aTestCfg.getContinueOnTimeout  ();
        final boolean        bAutoCloseOnError  = aTestCfg.getAutoCloseDocOnError();
        final int            nDocs              = aTestCfg.getRepetitions4Docs   ();
        final int            nApplys            = aTestCfg.getRepetitions4Applys ();
        final int            nDelayMS4Apply     = aTestCfg.getDelayApplyInMS     ();
        final boolean        bCloseConnection   = aTestCfg.getRepetitionsCloseAfterEveryDoc();

        iSession.login (aUser);

        RT2 aDocImpl = aRT2Env.newRT2Inst ();

        enableMessageExceptionHandling(aDocImpl);

        int nDoc = nDocs;
        while (nDoc-- > 0)
        {
            final Doc  aDoc     = new TextDoc ();
        	final IDoc iDoc     = (IDoc) MonProxyFactory.monitor(aDoc);

            aDocImpl.resetDocData();

        	aDoc.bind       (aDocImpl          );
        	aDoc.bind       (aStatusLine       );
        	aDoc.setTimeouts(nTimeoutMS4Open   ,
        					 nTimeoutMS4Apply  ,
        					 nTimeoutMS4Close  ,
        					 bContinueOnTimeout,
        					 bAutoCloseOnError );

            iDoc.createNew();

            iDoc.open();

            aDoc.checkLBRouting(aSession);

            int nApply = nApplys;
            while (nApply-- > 0)
            {
            	iDoc.applyOps();

                // as test iterations was calculated by counting all operations of all docs ...
                // we have to count iterations within applyOps() (of course)
                aStatusLine.countTestIteration();

                Thread.sleep(nDelayMS4Apply);
            }

            iDoc.close();

            // TODO preview hinder us from logout !!!!
            //iDoc.createPreviewNew();

            if (bCloseConnection) {
                aOX.shutdownWSConnection();
                // create a new ws client to enable the test to work on it
                aDocImpl = aRT2Env.newRT2Inst ();
            }
        }

        iSession.logout ();
    }
}
