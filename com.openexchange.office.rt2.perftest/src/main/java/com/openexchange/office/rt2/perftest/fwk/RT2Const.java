/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

//=============================================================================
public class RT2Const
{
    //-------------------------------------------------------------------------
    // rt2 events regarding the communication
    public static final String EVENT_DOC_RESPONSE       = "doc_response";
    public static final String EVENT_OFFLINE            = "offline"     ;
    public static final String EVENT_ONLINE             = "online"      ;
    public static final String EVENT_ERROR              = "error"       ;
    public static final String EVENT_RECEIVE            = "receive"     ;
    public static final String EVENT_DEFINITION_LIST    = RT2Const.EVENT_OFFLINE + " " + RT2Const.EVENT_ONLINE  + " " + RT2Const.EVENT_ERROR;

    //-------------------------------------------------------------------------
    // not part of JS client ... but needed by RT2Monitor env
    public static final String DEFAULT_MONITOR_HOST = "localhost";
    public static final int    DEFAULT_MONITOR_PORT = 9876;
}
