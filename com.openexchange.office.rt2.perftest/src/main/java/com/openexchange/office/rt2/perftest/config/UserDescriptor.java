/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

import java.util.Map;

//=============================================================================
public class UserDescriptor
{
    //-------------------------------------------------------------------------
	public static final String PROPERTY_NR         = "nr";
	public static final String PROPERTY_CONTEXT_NR = "context";
	public static final String PROPERTY_NAME       = "name";
	public static final String PROPERTY_LOGIN      = "login";
	public static final String PROPERTY_PASSWORD   = "password";

    //-------------------------------------------------------------------------
    public UserDescriptor ()
    {}

    //-------------------------------------------------------------------------
    public UserDescriptor (Map<String, String> aPropertyMap)
    {
        if (aPropertyMap.containsKey(PROPERTY_NR))
            nNr = Integer.decode(aPropertyMap.get(PROPERTY_NR));
        if (aPropertyMap.containsKey(PROPERTY_CONTEXT_NR))
            nContextNr = Integer.decode(aPropertyMap.get(PROPERTY_CONTEXT_NR));
        if (aPropertyMap.containsKey(PROPERTY_NAME))
            sName = aPropertyMap.get(PROPERTY_NAME);
        if (aPropertyMap.containsKey(PROPERTY_LOGIN))
            sLogin = aPropertyMap.get(PROPERTY_LOGIN);
        if (aPropertyMap.containsKey(PROPERTY_PASSWORD))
            sLogin = aPropertyMap.get(PROPERTY_PASSWORD);
    }

    //-------------------------------------------------------------------------
    public Integer nNr = null;

    //-------------------------------------------------------------------------
    public Integer nContextNr = null;

    //-------------------------------------------------------------------------
    public String sName = null;

    //-------------------------------------------------------------------------
    public String sLogin = null;

    //-------------------------------------------------------------------------
    public String sPassword = null;

    //-------------------------------------------------------------------------
    public String toString()
    {
        final StringBuilder sString = new StringBuilder (256);
        sString.append (super.toString()        );
        sString.append (" {"                    );
        sString.append ("nr="        +nNr       );
        sString.append (", context=" +nContextNr);
        sString.append (", name="    +sName     );
        sString.append (", login="   +sLogin    );
        sString.append (", password="+sPassword );
        sString.append ("}"                     );
        return sString.toString ();
    }

    //-------------------------------------------------------------------------
    public static Object getPropertyFromMap(Map<String, String> aPropertyMap, String sPropertyName)
    {
        Object aValue = null;

        final String sPropValue = aPropertyMap.get(sPropertyName);

        if (null != sPropValue)
        {
            aValue = sPropValue;

            if ((PROPERTY_NR.equals(sPropertyName)) ||
                (PROPERTY_CONTEXT_NR.equals(sPropertyName)))
            {
                aValue = Integer.decode(sPropValue);
            }
        }

        return aValue;
    }

}
