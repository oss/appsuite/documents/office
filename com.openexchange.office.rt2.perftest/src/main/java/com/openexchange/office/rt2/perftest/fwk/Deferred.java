/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

//=============================================================================
public class Deferred< T >
{
    //-------------------------------------------------------------------------
    public static interface Base< T >
    {
    	public void done (final T aData)
    		throws Exception;

    	public void fail (final T aData)
    		throws Exception;

    	public void always (final T aData)
            throws Exception;
    }
    
    //-------------------------------------------------------------------------
    public abstract static class Func< T > implements Base< T >
    {
        @Override
        @SuppressWarnings("unused")
        public void done (final T aData)
    		throws Exception
    	{}

        @Override
        @SuppressWarnings("unused")
    	public void fail (final T aData)
    		throws Exception
    	{}

        @Override
        @SuppressWarnings("unused")
    	public void always (final T aData)
            throws Exception
        {}
    }

    //-------------------------------------------------------------------------
    public Deferred ()
        throws Exception
    {}
    
    //-------------------------------------------------------------------------
    public Deferred (final int nTimeout)
        throws Exception
    {
        impl_autoRejectAfterTimeout (nTimeout);
    }

    //-------------------------------------------------------------------------
    public synchronized < V > void setMetaData(final String sKey  ,
    									       final V      aValue)
    	throws Exception
    {
    	mem_MetaData ().put(sKey, aValue);
    }

    //-------------------------------------------------------------------------
    public synchronized void setMetaData(final Map< String, Object > lMetaData)
    	throws Exception
    {
    	mem_MetaData ().putAll(lMetaData);
    }

    //-------------------------------------------------------------------------
    public synchronized Map< String, Object > getMetaData()
    	throws Exception
    {
    	return mem_MetaData ();
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public synchronized < V > V getMetaDataValue(final String sKey)
    	throws Exception
    {
    	return (V) mem_MetaData ().get(sKey);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void close ()
        throws Exception
    {
        m_bIsClosed.set(true);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ boolean isClosed ()
        throws Exception
    {
        return m_bIsClosed.get();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ boolean isPending ()
        throws Exception
    {
        return m_bPending.get();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void resolve (final T aData)
    	throws Exception
    {
    	m_bPending.compareAndSet(true, false);

		final Base< T > aDone = m_aDone.get();
		final Base< T > aAlways = m_aAlways.get();

		if (aDone != null)
		    aDone.done(aData);
		if (aAlways != null)
		    aAlways.always(aData);
    }
    
    //-------------------------------------------------------------------------
    public /* no synchronized */ void reject (final T aData)
    	throws Exception
    {
    	m_bPending.compareAndSet(true, false);
        final Base< T > aFail = m_aFail.get();
		final Base< T > aAlways = m_aAlways.get();

    	if (aFail != null)
    		aFail.fail(aData);
		if (aAlways != null)
		    aAlways.always(aData);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Deferred< T > done(final Base< T > aFunc)
    	throws Exception
    {
    	m_aDone.set(aFunc);
    	return this;
    }

    //-------------------------------------------------------------------------
    public synchronized Deferred< T > fail(final Base< T > aFunc)
    	throws Exception
    {
    	m_aFail.set(aFunc);
    	return this;
    }

    //-------------------------------------------------------------------------
    public synchronized Deferred< T > always(final Base< T > aFunc)
        throws Exception
    {
        m_aAlways.set(aFunc);
        return this;
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_autoRejectAfterTimeout (final int nTimeoutInMS)
        throws Exception
    {
        final ScheduledExecutorService aScheduler = mem_TimeoutScheduler ();
        aScheduler.schedule(new Callable< Void > ()
        {
            @Override
            public Void call()
                throws Exception
            {
                try
                {
                    reject (null);
                }
                catch (Throwable ex)
                {
                    // error in error : ignore it
                }
                
                return null;
            }
        }, nTimeoutInMS, TimeUnit.MILLISECONDS);
    }
    
    //-------------------------------------------------------------------------
    private synchronized Map< String, Object > mem_MetaData ()
        throws Exception
    {
    	if (m_lMetaData == null)
    		m_lMetaData = new HashMap< String, Object > ();
    	return m_lMetaData;
    }

    //-------------------------------------------------------------------------
    private static synchronized ScheduledExecutorService mem_TimeoutScheduler ()
        throws Exception
    {
        if (m_gTimeoutScheduler == null)
            m_gTimeoutScheduler = Executors.newScheduledThreadPool(10, new ThreadFactoryBuilder().setNameFormat("TimeoutScheduler-%d").build());
        return m_gTimeoutScheduler;
    }

    //-------------------------------------------------------------------------
    private AtomicReference< Base< T > > m_aDone = new AtomicReference<>(null);

    //-------------------------------------------------------------------------
    private AtomicReference< Base< T > > m_aFail = new AtomicReference<>(null);

    //-------------------------------------------------------------------------
    private AtomicReference< Base< T > > m_aAlways = new AtomicReference<>(null);

    //-------------------------------------------------------------------------
    private Map< String, Object > m_lMetaData = null;

    //-------------------------------------------------------------------------
    private AtomicBoolean m_bIsClosed = new AtomicBoolean (false);

    //-------------------------------------------------------------------------
    private AtomicBoolean m_bPending  = new AtomicBoolean (true);

    //-------------------------------------------------------------------------
    private static ScheduledExecutorService m_gTimeoutScheduler = null;
}
