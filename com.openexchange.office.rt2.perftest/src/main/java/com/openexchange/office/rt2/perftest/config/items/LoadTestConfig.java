/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config.items;

import org.apache.commons.lang3.BooleanUtils;

import com.openexchange.office.rt2.perftest.config.ConfigAccess;
import com.openexchange.office.rt2.perftest.config.ConfigUtils;
import com.openxchange.office_communication.tools.config.IComplexConfiguration;

//=============================================================================
public class LoadTestConfig
{
    //-------------------------------------------------------------------------
    public static final String CONFIG_PACKAGE = "/loadtest";

    //-------------------------------------------------------------------------
    public static final int     DEFAULT_REPITIONS_4_DOCS      =     1;
    public static final int     DEFAULT_REPITIONS_4_APPLYS    =     1;
    public static final int     DEFAULT_INSERTIMAGE_NTH_OP    =     0;
    public static final int     DEFAULT_DELAY_APPLY           =  1000;
    public static final int     DEFAULT_TIMEOUT_4_OPEN        = 30000;
    public static final int     DEFAULT_TIMEOUT_4_APPLY       = 10000;
    public static final int     DEFAULT_TIMEOUT_4_CLOSE       = 30000;
    public static final boolean DEFAULT_CONTINUE_ON_TIMEOUT   = false;
    public static final boolean DEFAULT_AUTO_CLOSE_ON_ERROR   = false;
    public static final boolean DEFAULT_CLOSE_AFTER_EVERY_DOC = false;

    //-------------------------------------------------------------------------
    public LoadTestConfig ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    public Integer getRepetitions4Docs ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.repetitions.docs", String.class), DEFAULT_REPITIONS_4_DOCS);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getRepetitions4Applys ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.repetitions.applys", String.class), DEFAULT_REPITIONS_4_APPLYS);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getInsertImageNthOp ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.use.insertimage.nth.op", String.class), DEFAULT_INSERTIMAGE_NTH_OP);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getRepetitionsCloseAfterEveryDoc ()
        throws Exception
    {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Boolean               nValue = ConfigUtils.readBoolean (iCfg.get("test.load.repetitions.closeaftereverydoc", String.class), DEFAULT_CLOSE_AFTER_EVERY_DOC);
        return BooleanUtils.toBoolean(nValue);
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4OpenInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.timeout.open", String.class), DEFAULT_TIMEOUT_4_OPEN);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4ApplyInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.timeout.apply", String.class), DEFAULT_TIMEOUT_4_APPLY);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getTimeout4CloseInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.timeout.close", String.class), DEFAULT_TIMEOUT_4_CLOSE);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Integer getDelayApplyInMS ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Integer               nValue = ConfigUtils.readInt (iCfg.get("test.load.delay.apply", String.class), DEFAULT_DELAY_APPLY);
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getContinueOnTimeout ()
        throws Exception
    {
    	final IComplexConfiguration iCfg   = mem_Config ();
    	final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("test.load.timeout.continue", String.class), DEFAULT_CONTINUE_ON_TIMEOUT);
    	return bValue;
    }

    //-------------------------------------------------------------------------
    public Boolean getAutoCloseDocOnError ()
        throws Exception
    {
        final IComplexConfiguration iCfg   = mem_Config ();
        final Boolean               bValue = ConfigUtils.readBoolean (iCfg.get("test.load.autoclose.on.error", String.class), DEFAULT_AUTO_CLOSE_ON_ERROR);
        return bValue;
    }

    //-------------------------------------------------------------------------
    private final IComplexConfiguration mem_Config ()
        throws Exception
    {
    	if (m_iConfig == null)
    		m_iConfig = ConfigAccess.accessConfig(CONFIG_PACKAGE);
    	return m_iConfig;
    }

    //-------------------------------------------------------------------------
    private IComplexConfiguration m_iConfig = null;
}
