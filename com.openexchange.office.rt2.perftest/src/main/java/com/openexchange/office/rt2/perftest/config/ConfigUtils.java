/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

//=============================================================================
public class ConfigUtils
{
    //-------------------------------------------------------------------------
    private ConfigUtils ()
    {}

    //-------------------------------------------------------------------------
    public static Integer readInt (final Map< String, String > aConfig ,
    							   final String                sKey    ,
    							   final Integer               aDefault)
        throws Exception
    {
    	Integer nValue = null;
    	try
    	{
    		nValue = Integer.parseInt(aConfig.get(sKey));
    	}
    	catch (Throwable ex)
    	{
    		nValue = aDefault;
    	}
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public static Integer readInt (final String  sValue  ,
    							   final Integer aDefault)
        throws Exception
    {
    	Integer nValue = null;
    	try
    	{
    		nValue = Integer.parseInt(sValue);
    	}
    	catch (Throwable ex)
    	{
    		nValue = aDefault;
    	}
    	return nValue;
    }

    //-------------------------------------------------------------------------
    public static Long readLong (final String  sValue  ,
                                 final Long    aDefault)
        throws Exception
    {
        Long nValue = null;
        try
        {
            nValue = Long.parseLong(sValue);
        }
        catch (Throwable ex)
        {
            nValue = aDefault;
        }
        return nValue;
    }

    //-------------------------------------------------------------------------
    public static Boolean readBoolean (final String  sValue  ,
    							       final Boolean aDefault)
        throws Exception
    {
    	Boolean bValue = null;
    	try
    	{
    		bValue = Boolean.parseBoolean(sValue);
    	}
    	catch (Throwable ex)
    	{
    		bValue = aDefault;
    	}
    	return bValue;
    }

    //-------------------------------------------------------------------------
    public static String readString (final String sValue, 
    		                         final String sDefault)
        throws Exception
    {
        String result;
        if (sValue == null)
            result = sDefault;
        else
            result = sValue;
        return result;
    }

    //-------------------------------------------------------------------------
    public static String readString (final Map< String, String > aConfig ,
    							      final String                sKey    ,
    							      final String                aDefault)
        throws Exception
    {
    	String sValue = null;
    	try
    	{
    		sValue = aConfig.get(sKey);
    	}
    	catch (Throwable ex)
    	{
    		sValue = aDefault;
    	}
    	return sValue;
    }

    //-------------------------------------------------------------------------
    public static List< Integer>  readIntList (final Map< String, String > aConfig  ,
    								           final String                sKey     ,
    								           final Integer...            lDefaults)
        throws Exception
    {
    	final String          sValue = aConfig.get (sKey);
    	final List< Integer > lList  = readIntList (sValue, lDefaults);
    	return lList;
    }

    //-------------------------------------------------------------------------
    public static List< Integer>  readIntList (final String     sConfigValue,
    								           final Integer... lDefaults   )
        throws Exception
    {
    	final List< Integer > lList = new ArrayList< Integer > ();
    	try
    	{
    		final String[] lListItems = StringUtils.splitPreserveAllTokens(sConfigValue, ",");
    		
    		for (final String sListItem : lListItems)
    		{
    			if (StringUtils.isEmpty(sListItem))
    				continue;

    			final Integer nValue = Integer.parseInt(sListItem);
    			lList.add (nValue);
    		}
    	}
    	catch (Throwable ex)
    	{
    		lList.clear ();

    		if (lDefaults != null)
    		{
    			for (final Integer nDefault : lDefaults)
    				lList.add (nDefault);
    		}
    	}
    	return lList;
    }
    //-------------------------------------------------------------------------
    public static Pair< Integer, Integer >  readIntRange (final Map< String, String > aConfig,
    								                      final String                sKey   )
        throws Exception
    {
    	final String   sValue  = aConfig.get (sKey);
    	final String[] lValues = StringUtils.split(sValue, "-");

    	if (lValues == null)
    		return null;

    	if (lValues.length != 2)
    		throw new IllegalArgumentException ("Illegal format of configuration item '"+sKey+"' with value '"+sValue+"'.");

    	final String                   sStart = lValues[0];
    	final String                   sEnd   = lValues[1];
    	final Integer                  nStart = Integer.parseInt(sStart);
    	final Integer                  nEnd   = Integer.parseInt(sEnd  );
    	final Pair< Integer, Integer > aRange = new ImmutablePair< Integer, Integer > (nStart, nEnd);

    	return aRange;
    }
}
