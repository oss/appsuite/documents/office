/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import org.apache.commons.lang3.ArrayUtils;

//=============================================================================
public class RT2MessageFlags
{
    //-------------------------------------------------------------------------
    private RT2MessageFlags ()
    {}

    //-------------------------------------------------------------------------
    public static final int    FLAG_NONE                  =  0;
    public static final int    FLAG_ALL                   = Integer.MAX_VALUE;
    public static final int    FLAG_SIMPLE_MSG            =  1;
    public static final int    FLAG_SESSION_BASED         =  2;
    public static final int    FLAG_SEQUENCE_NR_BASED     =  4;
    public static final int    FLAG_CHUNK_BASED           =  8;
    public static final int    FLAG_DOC_BASED             = 16;

    //-------------------------------------------------------------------------
    public static final String FLAG_STR_NONE              = "NONE"            ;
    public static final String FLAG_STR_ALL                = "ALL"            ;
    public static final String FLAG_STR_SIMPLE_MSG        = "SIMPLE_MSG"      ;
    public static final String FLAG_STR_SESSION_BASED     = "SESSION_BASED"   ;
    public static final String FLAG_STR_SEQUENCE_NR_BASED = "SEQUENCE_NR_BASE";
    public static final String FLAG_STR_CHUNK_BASED       = "CHUNK_BASED"     ;
    public static final String FLAG_STR_DOC_BASED         = "DOC_BASED"       ;

    //-------------------------------------------------------------------------
    public static boolean isFlag (final int nFlags,
                                  final int nFlag )
        throws Exception
    {
        if ((nFlags & nFlag) == nFlag)
            return true;

        return false;
    }
    
    //-------------------------------------------------------------------------
    public static boolean areFlagsNone (final int... lFlags)
        throws Exception
    {
        for (final int nFlag : lFlags)
        {
            if (nFlag != FLAG_NONE)
                return false;
        }

        return true;
    }

    //-------------------------------------------------------------------------
    public static String mapFlagToString (final int nFlag)
        throws Exception
    {
        if (nFlag == FLAG_NONE)
            return FLAG_STR_NONE;
        else
        if (nFlag == FLAG_ALL)
            return FLAG_STR_ALL;
        else
        if (nFlag == FLAG_SIMPLE_MSG)
            return FLAG_STR_SIMPLE_MSG;
        else
        if (nFlag == FLAG_SESSION_BASED)
            return FLAG_STR_SESSION_BASED;
        else
        if (nFlag == FLAG_SEQUENCE_NR_BASED)
            return FLAG_STR_SEQUENCE_NR_BASED;
        else
        if (nFlag == FLAG_CHUNK_BASED)
            return FLAG_STR_CHUNK_BASED;
        else
        if (nFlag == FLAG_DOC_BASED)
            return FLAG_STR_DOC_BASED;
        else
            throw new UnsupportedOperationException ("no support for flag '"+nFlag+"' implemented yet.");
    }
    
    //-------------------------------------------------------------------------
    public static String mapFlagFieldToString (final int nFlagField)
        throws Exception
    {
        final int[]  lFlags  = splitFlagFieldToFlags (nFlagField);
        final String sString = mapFlagsToString      (lFlags    );
        return sString;
    }

    //-------------------------------------------------------------------------
    public static int[] splitFlagFieldToFlags (final int nFlagField)
        throws Exception
    {
        int   nIndex = 0;
        int[] nFlags = new int[32]; // we support 32 flag max ,-)
        
        if (nFlagField == FLAG_NONE)
            nFlags[nIndex++] = FLAG_NONE;
        else
        if (nFlagField == FLAG_ALL)
            nFlags[nIndex++] = FLAG_ALL;
        else
        {
            if (isFlag(nFlagField, FLAG_SIMPLE_MSG))
                nFlags[nIndex++] = FLAG_SIMPLE_MSG;
    
            if (isFlag(nFlagField, FLAG_SESSION_BASED))
                nFlags[nIndex++] = FLAG_SESSION_BASED;
    
            if (isFlag(nFlagField, FLAG_SEQUENCE_NR_BASED))
                nFlags[nIndex++] = FLAG_SEQUENCE_NR_BASED;
    
            if (isFlag(nFlagField, FLAG_CHUNK_BASED))
                nFlags[nIndex++] = FLAG_CHUNK_BASED;
    
            if (isFlag(nFlagField, FLAG_DOC_BASED))
                nFlags[nIndex++] = FLAG_DOC_BASED;
        }

        if (nIndex < 1)
            nFlags = new int[0];
        else
            nFlags = ArrayUtils.subarray(nFlags, 0, nIndex);

        return nFlags;
    }
    
    //-------------------------------------------------------------------------
    public static String mapFlagsToString (final int... lFlags)
        throws Exception
    {
        final StringBuilder sFlags = new StringBuilder (256);
        
        sFlags.append ("[");
        
        if (lFlags != null)
        {
            boolean bAddSeparator = false;

            for (final int nFlag : lFlags)
            {
                if (bAddSeparator)
                    sFlags.append (", ");
                else
                    bAddSeparator = true;
                sFlags.append (mapFlagToString(nFlag));
            }
        }
        
        sFlags.append ("]");
        
        return sFlags.toString ();
    }

//    //-------------------------------------------------------------------------
//    public static void main (String[] args)
//    throws Exception
//    {
//        System.err.println (Integer.toBinaryString(FLAG_ALL));
//        System.err.println (mapFlagFieldToString(FLAG_ALL));
//        System.err.println (mapFlagsToString(FLAG_ALL, FLAG_NONE));
//    }
}
