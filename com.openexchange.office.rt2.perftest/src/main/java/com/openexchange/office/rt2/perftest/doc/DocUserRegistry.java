/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//=============================================================================
public class DocUserRegistry
{
    //-------------------------------------------------------------------------
    public DocUserRegistry ()
        throws Exception
    {}
    
    //-------------------------------------------------------------------------
    public synchronized boolean hasUser (final DocUser aUser)
        throws Exception
    {
        final String                 sKey      = aUser.getUniqueKey();
        final Map< String, DocUser > aRegistry = mem_UserRegistry  ();
        final boolean                bHas      = aRegistry.containsKey(sKey);
        return bHas;
    }

    //-------------------------------------------------------------------------
    public synchronized void setAllKnownUser (final List< DocUser > lUser)
        throws Exception
    {
        mem_UserRegistry ().clear ();
        
        for (final DocUser aUser : lUser)
            registerUser (aUser);
    }
    
    //-------------------------------------------------------------------------
    public synchronized void registerUser (final DocUser aUser)
        throws Exception
    {
        final String                 sKey      = aUser.getUniqueKey();
        final Map< String, DocUser > aRegistry = mem_UserRegistry  ();
        aRegistry.put (sKey, aUser);
    }
    
//    //-------------------------------------------------------------------------
//    public synchronized void deregisterUser (final DocUser aUser)
//        throws Exception
//    {
//        final String                 sKey      = aUser.getUniqueKey();
//        final Map< String, DocUser > aRegistry = mem_UserRegistry  ();
//        aRegistry.remove(sKey);
//    }

    //-------------------------------------------------------------------------
    public synchronized int getCount ()
        throws Exception
    {
        final Map< String, DocUser > aRegistry = mem_UserRegistry ();
        final int                    nCount    = aRegistry.size ();
        return nCount;
    }

    //-------------------------------------------------------------------------
    public synchronized List< DocUser > asList ()
        throws Exception
    {
        final Map< String, DocUser > aRegistry = mem_UserRegistry ();
        final List< DocUser >        aList     = new ArrayList< DocUser > (aRegistry.values());
        return aList;
    }

    //-------------------------------------------------------------------------
    private synchronized Map< String, DocUser > mem_UserRegistry ()
        throws Exception
    {
        if (m_aUserRegistry == null)
            m_aUserRegistry = new HashMap< String, DocUser >();
        return m_aUserRegistry;
    }

    //-------------------------------------------------------------------------
    private Map< String, DocUser > m_aUserRegistry = null;
}