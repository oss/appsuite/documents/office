/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import com.openexchange.office.rt2.perftest.config.InstanceConfigure;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;
import net.as_development.asdk.jms.core.IJMSServer;
import net.as_development.asdk.jms.core.JMSEnv;
import net.as_development.asdk.jms.core.JMSServer;
import net.as_development.asdk.jms.core.simple.ISubscriber;
import net.as_development.asdk.jms.core.simple.SimpleMessaging;

//=============================================================================
public class RemoteControl
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(RemoteControl.class);

    //-------------------------------------------------------------------------
    public static final String CFG_SERVER_MODE        = "server-mode";
    public static final String CFG_HOST               = "host";
    public static final String CFG_PORT               = "port";

    //-------------------------------------------------------------------------
    public static final String DEFAULT_SERVER_HOST    = "localhost";
    public static final int    DEFAULT_SERVER_PORT    = 98765      ;

    //-------------------------------------------------------------------------
    public static final String CHANNEL_CMD_BROADCAST    = "topic://office-perftest-cmd-broadcast"   ;
    public static final String CHANNEL_PRESENTER_MASTER = "queue://office-perftest-presenter-master";
    
    //-------------------------------------------------------------------------
    public RemoteControl ()
    {}

    //-------------------------------------------------------------------------
    public /* no synchronized*/ RemoteControl configure (final String sKey  ,
                                                         final Object aValue)
        throws Exception
    {
        mem_Config ().set (sKey, aValue);
        return this;
    }
    
    //-------------------------------------------------------------------------
    public /* no synchronized */ void start ()
        throws Exception
    {
        final Boolean bIsServer = mem_Config ().get (CFG_SERVER_MODE, Boolean.FALSE);
        if (bIsServer)
            mem_Server ().start();
    }
    
    //-------------------------------------------------------------------------
    public /* no synchronized */ void stop ()
        throws Exception
    {
        final Boolean bIsServer = mem_Config ().get (CFG_SERVER_MODE, Boolean.FALSE);
        if (bIsServer)
            mem_Server ().stop();

        mem_Client ().close();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void sendCmd (final String sCmd)
        throws Exception
    {
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("send cmd : "+sCmd)
            .log        ();
        mem_Client ().publish(CHANNEL_CMD_BROADCAST, sCmd);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void receiveCmd (final ISubscriber iCallback)
        throws Exception
    {
        mem_Client ().subscribe(CHANNEL_CMD_BROADCAST, iCallback);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void subscribe (final String      sURI     ,
                                                 final ISubscriber iCallback)
        throws Exception
    {
        mem_Client ().subscribe(sURI, iCallback);
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ void publish (final String sURI    ,
                                               final String sMessage)
        throws Exception
    {
        mem_Client ().publish(sURI, sMessage);
    }

    //-------------------------------------------------------------------------
    private synchronized JMSEnv mem_JMSEnv ()
        throws Exception
    {
        if (m_aJMSEnv != null)
            return m_aJMSEnv;

        final InstanceConfigure aConfig = mem_Config ();
        final JMSEnv            aEnv    = new JMSEnv ();

        aEnv.setServerIP  (aConfig.get (CFG_HOST, DEFAULT_SERVER_HOST));
        aEnv.setServerPort(aConfig.get (CFG_PORT, DEFAULT_SERVER_PORT));

        m_aJMSEnv = aEnv;
        return m_aJMSEnv;
    }

    //-------------------------------------------------------------------------
    private synchronized IJMSServer mem_Server ()
        throws Exception
    {
        if (m_iServer != null)
            return m_iServer;
        
        final IJMSServer iServer = JMSServer.newServer();
        iServer.setEnv(mem_JMSEnv ());
        
        m_iServer = iServer;
        return m_iServer;
    }

    //-------------------------------------------------------------------------
    private synchronized SimpleMessaging mem_Client ()
        throws Exception
    {
        if (m_aClient != null)
            return m_aClient;
        
        final SimpleMessaging iClient = new SimpleMessaging ();
        iClient.setEnv(mem_JMSEnv ());
        
        m_aClient = iClient;
        return m_aClient;
    }

    //-------------------------------------------------------------------------
    private synchronized InstanceConfigure mem_Config ()
        throws Exception
    {
        if (m_aConfig == null)
            m_aConfig = new InstanceConfigure();
        return m_aConfig;
    }

    //-------------------------------------------------------------------------
    private JMSEnv m_aJMSEnv = null;

    //-------------------------------------------------------------------------
    private IJMSServer m_iServer = null;

    //-------------------------------------------------------------------------
    private SimpleMessaging m_aClient = null;
    
    //-------------------------------------------------------------------------
    private InstanceConfigure m_aConfig = null;
}
