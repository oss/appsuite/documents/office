/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

public class OXCookies
{
    //-------------------------------------------------------------------------
    public OXCookies ()
    {}

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aAppsuite)
    	throws Exception
    {
    	m_rHttpCookieStore = new WeakReference< CookieStore >(aAppsuite.accessConnection().mem_CookieStore());
    }
    
    //-------------------------------------------------------------------------
    public synchronized Map< String, String > getCookies ()
        throws Exception
    {
    	final Map< String, String > lCookies     = new HashMap< String, String > ();
    	final CookieStore           aCookieStore = m_rHttpCookieStore.get ();
    	final List< Cookie >        lIntCookies  = aCookieStore.getCookies();

    	for (final Cookie aCookie : lIntCookies)
    	{
    		final String sCookie = aCookie.getName ();
    		final String sValue  = aCookie.getValue();
    		
    		lCookies.put(sCookie, sValue);
    	}
    	return lCookies;
    }

    //-------------------------------------------------------------------------
    protected synchronized Cookie getCookie (final String sMatch)
        throws Exception
    {
    	final CookieStore    aCookieStore = m_rHttpCookieStore.get ();
    	final List< Cookie > lCookies     = aCookieStore.getCookies();

    	for (final Cookie aCookie : lCookies)
    	{
    		final String sCookie = aCookie.getName();
    		if (sCookie.matches(sMatch))
    			return aCookie;
    	}
    	return null;
    }
    
    //-------------------------------------------------------------------------
    private WeakReference< CookieStore > m_rHttpCookieStore = null;
}
