/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A set of functions to support the work with JSONObjects and JSONArrays.
 *
 * @author Carsten Driesner
 */
public class JSONHelper {

    /**
     * Checks if a JSONArray reference is null or empty.
     *
     * @param array a JSONArray reference
     * @return TRUE if the reference is null or the JSONArray is empty
     */
    static public boolean isNullOrEmpty(final JSONArray array) {
        return ((null == array) || (array.length() == 0));
    }

    /**
     * Checks if a JSONArray reference is not null and not empty.
     *
     * @param array a JSONArray reference
     * @return TRUE if the reference is not null and the JSONArray is not empty
     */
    static public boolean isNotNullAndEmpty(final JSONArray array) {
        return ((null != array) && (array.length() > 0));
    }

    /**
     * Checks if a JSONObject reference is null or empty.
     *
     * @param array a JSONObject reference
     * @return TRUE if the reference is null or the JSONObject contains no properties.
     */
    static public boolean isNullOrEmpty(final JSONObject jsonObj) {
        return ((null == jsonObj) || (jsonObj.length() == 0));
    }

    /**
     * Checks if a JSONObject reference is not null and contains properties.
     *
     * @param array a JSONObject reference
     * @return TRUE if the reference is not null and the JSONObject contains properties.
     */
    static public boolean isNotNullAndEmpty(final JSONObject jsonObj) {
        return ((null != jsonObj) && (jsonObj.length() > 0));
    }

    /**
     * Creates a shallow copy of the JSONArray, means that the function provides a new JSONArray with references to the entries within the
     * source array.
     *
     * @param array The source array which should be shallow cloned.
     * @return A new JSONArray containing the references to the old entries.
     * @throws Exception
     */
    static public final JSONArray shallowCopy(final JSONArray array) throws JSONException {
        JSONArray copy = null;

        if (null != array) {
            copy = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                copy.put(array.get(i));
            }
        }

        return copy;
    }

    /**
     * Appends a JSONArray to an existing JSONArray.
     *
     * @param target The target JSONArray.
     * @param source The source JSONArray.
     * @return The target array contains at the end all entries of the source if both arguments are set, otherwise null will be returned.
     * @throws Exception
     */
    static public final JSONArray appendArray(final JSONArray target, final JSONArray source) throws JSONException {
        if ((target != null) && (source != null)) {
            for (int i = 0; i < source.length(); i++) {
                target.put(source.get(i));
            }
        }

        return target;
    }

    /**
     * Removing an item from a JSONArray at a specified position. This is necessary, because the 'remove()' function is not always available
     * to JSONArrays. This function creates a local JSONArray, that is filled with the values of the JSONArray, that is a parameter of it. A
     * second parameter is required to specify, which element shall be removed. Its value must be between 0 and the length of the JSONArray
     * - 1. The locally created JSONArray is returned from this function.
     *
     * @param arr The array, from which one element shall be removed
     * @param pos The position of the item, that shall be removed from the array. This value must be between 0 and arr.length - 1
     * @return If a problem occurred, the JSONArray will not be modified and returned. Otherwise a reduced version of the JSONArray is
     *         returned, that does not contain the specified item.
     */
    static public final JSONArray removeItemFromJSONArray(final JSONArray arr, int pos) {

        if ((arr == null) || (arr.length() - 1 < pos)) {
            return arr;
        }

        final JSONArray newArr = new JSONArray();

        for (int i = 0; i < arr.length(); i++) {

            if (i != pos) {
                try {
                    newArr.put(arr.get(i));
                } catch (JSONException e) {
                    return arr;
                }
            }
        }

        return newArr;
    }

    /**
     * Creates a new array which contains the entries from the source beginning with the index startIndex until the end of source.
     *
     * @param source The source JSONArray where entries should be
     * @param startIndex
     * @return
     * @throws Exception
     */
    static public final JSONArray subArray(final JSONArray source, int startIndex) throws JSONException {
        JSONArray subArray = null;

        if (null != source) {
            int endIndex = source.length();

            subArray = new JSONArray();
            for (int i = startIndex; i < endIndex; i++) {
                subArray.put(source.get(i));
            }
        }

        return subArray;
    }

    /**
     * Creates a new array which contains a subset of entries specified by startIndex and endIndex.
     *
     * @param source
     * @param startIndex
     * @param endIndex
     * @return
     * @throws Exception
     */
    static public final JSONArray subArray(final JSONArray source, int startIndex, int endIndex) throws JSONException {
        JSONArray subArray = null;

        if (null != source) {
            if (endIndex == -1) {
                endIndex = source.length();
            }

            subArray = new JSONArray();
            for (int i = startIndex; i < endIndex; i++) {
                subArray.put(source.get(i));
            }
        }

        return subArray;
    }

    /**
     * Creates a new array which contains a subset of entries specified by startIndex and length.
     *
     * @param source
     * @param startIndex
     * @param length
     * @return
     */
    static public final JSONArray subArrayLength(final JSONArray source, int startIndex, int length) throws JSONException {
        JSONArray subArray = null;

        if ((null != source) && (startIndex >= 0) && (length > 0)) {
            try {
                int endIndex = Math.min(Math.max(0, (startIndex + length)), Math.max(0, source.length() - 1));
                subArray = new JSONArray();

                for (int i = startIndex; i < endIndex; i++) {
                    subArray.put(source.get(i));
                }
            } catch (JSONException e) {
                // nothing to do
            }
        }

        return subArray;
     }
}
