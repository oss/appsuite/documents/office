/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc.text;

import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.openexchange.office.rt2.perftest.doc.Doc;
import com.openexchange.office.rt2.perftest.doc.FastEmptyDoc;
import com.openexchange.office.rt2.perftest.fwk.MessageConstants;
import com.openexchange.office.rt2.perftest.operations.Operations;
import com.openexchange.office.rt2.perftest.operations.OperationsUtils;
import com.openexchange.office.rt2.perftest.operations.SelectionUpdater;

//=============================================================================
public class TextDoc extends FastEmptyDoc implements SelectionUpdater
{
    //-------------------------------------------------------------------------
	public static final String STR_TEXT = "Das gelbe Taxi von Xaver und Zacharias fährt jetzt quer durch Konstanz in Bayern.";
	public static final int    STR_LEN  = STR_TEXT.length();

    //-------------------------------------------------------------------------
	public static final String OPERATION_INSERTTEXT = "insertText";

    //-------------------------------------------------------------------------
	private AtomicInteger    aXPos = new AtomicInteger(0);
	private static final int nYPos = 0;

    //-------------------------------------------------------------------------
    public TextDoc ()
        throws Exception
    {
    	super(Doc.DOCTYPE_TEXT);
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONArray getNextApplyOps ()
        throws Exception
    {
        final JSONArray lOps = this.createOperationsArray();
        this.startOpsChunk();
        Operations.addOperation    (lOps, Operations.createInsertParagraph    (0, this));
        Operations.addOperation    (lOps, Operations.createInsertTextOperation(0, 0, STR_TEXT, this, this));
        this.endOpsChunk();

        return lOps;
    }

    //-------------------------------------------------------------------------
    @Override
    protected JSONObject getCurrentSelection ()
        throws Exception
    {
    	final JSONObject aSelJSONObject = new JSONObject();
    	final JSONArray  lSelections    = new JSONArray();
    	final JSONObject aTextSelection = new JSONObject();
    	final JSONArray  lStartPos      = new JSONArray();
    	final JSONArray  lEndPos        = new JSONArray();
    	final int        nXPos          = aXPos.get();

    	aTextSelection.put(MessageConstants.STR_TYPE, "text");

    	lStartPos.put(nXPos);
    	lStartPos.put(nYPos);
    	aTextSelection.put(MessageConstants.STR_START, lStartPos);

    	lEndPos.put(nXPos);
    	lEndPos.put(nYPos);
    	aTextSelection.put(MessageConstants.STR_END, lEndPos);

    	lSelections.put(aTextSelection);
    	aSelJSONObject.put(MessageConstants.STR_SELECTIONS, lSelections);

    	OperationsUtils.compressObject(aSelJSONObject);
    	return aSelJSONObject;
    }

    //-------------------------------------------------------------------------
    @Override
    public void updateCurrentSelection()
    {
    	if (aXPos.compareAndSet((STR_LEN - 1), 0))
    	{
    		// nothing to do
    	}
    	else
    		aXPos.incrementAndGet();
    }

}
