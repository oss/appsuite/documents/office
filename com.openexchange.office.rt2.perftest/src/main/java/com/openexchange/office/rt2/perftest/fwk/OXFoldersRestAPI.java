/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public class OXFoldersRestAPI {

    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXFoldersRestAPI.class);

    //-------------------------------------------------------------------------
    public static final String REST_API_FOLDERS = "/folders";

    //-------------------------------------------------------------------------
    private static final String REST_JSON_ROOT = "data";

    //-------------------------------------------------------------------------
    private static final String columnsToFetchForFolders = "1,2,3,4,5,6,20,23,300,301,302,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,3010,3020,3030,3050,3201,3203,3204,3205,3220";

    //-------------------------------------------------------------------------
    private OXAppsuite context = null;

    //-------------------------------------------------------------------------
    public OXFoldersRestAPI () {
    	super();
    }

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite context)
        throws Exception {
        this.context = context;
    }

    //-------------------------------------------------------------------------
    public JSONObject getFolder(String folderId) throws Exception {
        final OXConnection connection = context.accessConnection();
        final OXSession session = context.accessSession();
        final String restAPIUri = getBaseURI () + REST_API_FOLDERS;

        final NameValuePair[] params = new NameValuePair[3];
        params[0] = new BasicNameValuePair("action", "get");
        params[1] = new BasicNameValuePair("id", folderId);
        params[2] = new BasicNameValuePair("session", session.getSessionId());

    	final URI request = connection.buildRequestUri(restAPIUri, params);
        final UnboundHttpResponse response = connection.doGet(request);

    	final StatusLine result = response.getStatusLine();
    	if (result.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Bad server response : " + result);

        final String body = response.getBody();
        final JSONObject jsonBody = new JSONObject(body);
        final String sError = jsonBody.optString("error");
        if (StringUtils.isNotEmpty(sError))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, sError);

        return jsonBody.getJSONObject(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    // http://localhost/appsuite/api/folders?action=list&all=0&altNames=true&parent=294&timezone=UTC&tree=1&session=38aef09db9db4ac2b0d3cbf93ce4474a
    //&columns=1%2C2%2C3%2C4%2C5%2C6%2C20%2C23%2C300%2C301%2C302%2C304%2C305%2C306%2C307%2C308%2C309%2C310%2C311%2C312%2C313%2C314%2C315%2C316%2C317
    //%2C318%2C319%2C3010%2C3020%2C3030%2C3050%2C3201%2C3203%2C3204%2C3205%2C3220
    public JSONArray getFolders(String parentFolderId) throws Exception {
        final OXConnection connection = context.accessConnection();
        final OXSession session = context.accessSession();
        final String restAPIUri = getBaseURI () + REST_API_FOLDERS;
        final String sessionId = session.getSessionId();

        Validate.isTrue(!StringUtils.isEmpty(parentFolderId), "Miss 'parentFolderId'.");
    
        final NameValuePair[] params = new NameValuePair[8];
        params[0] = new BasicNameValuePair("action", "list");
        params[1] = new BasicNameValuePair("all", "0");
        params[2] = new BasicNameValuePair("altNames", Boolean.toString(true));
        params[3] = new BasicNameValuePair("parent", parentFolderId);
        params[4] = new BasicNameValuePair("timezone", "UTC");
        params[5] = new BasicNameValuePair("tree", "1");
        params[6] = new BasicNameValuePair("session", sessionId);
        params[7] = new BasicNameValuePair("columns", columnsToFetchForFolders);

    	final URI uri = connection.buildRequestUri(restAPIUri, params);

    	LOG.forLevel(ELogLevel.E_INFO).withMessage("GET : "+uri).log();

    	final UnboundHttpResponse response = connection.doGet(uri);
    	final StatusLine result = response.getStatusLine();

    	if (result.getStatusCode() != HttpStatusCode.OK.code())
    		throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Bad server response : " + result);

        final String body = response.getBody();
        if (StringUtils.isEmpty(body))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");

        final JSONObject jsonBody = new JSONObject(body);
        final String sError = jsonBody.optString("error");
        if (StringUtils.isNotEmpty(sError))
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, sError);

        return jsonBody.getJSONArray(REST_JSON_ROOT);
    }

    //-------------------------------------------------------------------------
    private String getBaseURI () throws Exception {
        return context.accessConfig().sAPIRelPath;
    }

}
