/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

//=============================================================================
public class TestRunConfig
{
    //-------------------------------------------------------------------------
    public TestRunConfig ()
    {}
    
    //-------------------------------------------------------------------------
    public String sTestId = null;

    //-------------------------------------------------------------------------
    public String sImplClass = null;

    //-------------------------------------------------------------------------
    public String sServerURL = null;

    //-------------------------------------------------------------------------
    public String sServerName = null;

    //-------------------------------------------------------------------------
    public String sAlternativeServerName = null;

    //-------------------------------------------------------------------------
    public String sAlternativeServerURL = null;

    //-------------------------------------------------------------------------
    public int nServerJMXPort = 0;

    //-------------------------------------------------------------------------
    public String sAPIRelPath = null;

    //-------------------------------------------------------------------------
    public String sWSURL = null;
    
    //-------------------------------------------------------------------------
    public String hazelcastPort = "5701";

    //-------------------------------------------------------------------------
    public String hazelcastGroupName = null;

    //-------------------------------------------------------------------------
    public String hazelcastGroupPassword = null;

    //-------------------------------------------------------------------------
    public UserDescriptor aUser = null;

    //-------------------------------------------------------------------------
    public TestConfig rGlobalConfig = null;
    
    //-------------------------------------------------------------------------
    @Override
    public String toString()
    {
    	final StringBuilder sString = new StringBuilder (256);
    	
    	sString.append (super.toString ()           );
    	sString.append (" {test-id="    +sTestId    );
    	sString.append (", impl-class=" +sImplClass );
    	sString.append (", server-url=" +sServerURL );
    	sString.append (", api-relpath="+sAPIRelPath);
    	sString.append (", ws-url="     +sWSURL     );
    	sString.append (", hzPort="     +hazelcastPort);
    	sString.append (", user="       +aUser      );
    	sString.append ("}"                         );
    	
    	return sString.toString ();
    }
}
