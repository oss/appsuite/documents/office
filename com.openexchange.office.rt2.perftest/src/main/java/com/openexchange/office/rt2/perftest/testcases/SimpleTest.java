/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.testcases;

import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.perftest.config.UserDescriptor;
import com.openexchange.office.rt2.perftest.fwk.OXAppsuite;
import com.openexchange.office.rt2.perftest.fwk.OXFoldersRestAPI;
import com.openexchange.office.rt2.perftest.fwk.OXSession;
import com.openexchange.office.rt2.perftest.impl.TestCaseBase;

//=============================================================================
public class SimpleTest extends TestCaseBase
{
    //-------------------------------------------------------------------------
    public SimpleTest ()
        throws Exception
    {}

    //-------------------------------------------------------------------------
    @Override
    public void runTest ()
    	throws Exception
    {
    	final TestRunConfig cfg = getRunConfig ();
    	final UserDescriptor user = cfg.aUser;

    	final OXAppsuite oxInstance = new OXAppsuite(cfg);
    	final OXSession session = oxInstance.accessSession ();
    	final OXFoldersRestAPI foldersAPI = oxInstance.getFoldersRestAPI();

    	session.login(user);
    	foldersAPI.getFolder(String.valueOf(session.getHomeFolderId()));
    	session.logout();
    }
}
