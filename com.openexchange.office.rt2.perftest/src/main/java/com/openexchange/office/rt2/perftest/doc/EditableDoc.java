/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.json.JSONObject;

import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2Protocol;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public abstract class EditableDoc extends MessageAndErrorNotifierDoc
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(EditableDoc.class);

    //-------------------------------------------------------------------------
	private AtomicBoolean docAcceptedEditRequests = new AtomicBoolean(true);

    //-------------------------------------------------------------------------
	private AtomicBoolean handleEditRightsRequests = new AtomicBoolean(true);

    //-------------------------------------------------------------------------
	public EditableDoc(String sType) throws Exception {
		super(sType);
	}

    //-------------------------------------------------------------------------
	public boolean acceptsEditRightsRequest()
	{
        return docAcceptedEditRequests.get();
	}

    //-------------------------------------------------------------------------
	public void setAcceptsEditRightsRequest(boolean bAcceptsEditRequests)
	{
        docAcceptedEditRequests.set(bAcceptsEditRequests);
	}

    //-------------------------------------------------------------------------
	public boolean isEditor() throws Exception
	{
        return getRT2ClientUID().equals(this.getEditorUID());
	}

    //-------------------------------------------------------------------------
	public boolean handlesEditRightsRequests()
	{
        return handleEditRightsRequests.get();
	}

    //-------------------------------------------------------------------------
	public void setHandleEditRightsRequests(boolean bHandleIt)
	{
        handleEditRightsRequests.set(bHandleIt);
	}

    //-------------------------------------------------------------------------
	@Override
    protected /* no synchronized */ void onDocResponse (final RT2Message aResponse)
        throws Exception
    {
        final String sType = aResponse.getType();
        if (RT2Protocol.BROADCAST_UPDATE_CLIENTS.equals(sType) && isEditor() && handlesEditRightsRequests())
            handleUpdateClientsAndPossibleEditRightsRequests(aResponse);

        super.onDocResponse(aResponse);
    }

    //-------------------------------------------------------------------------
    private void handleUpdateClientsAndPossibleEditRightsRequests(final RT2Message aResponse)
        throws Exception
    {
        final List< JSONObject > lActiveUsers = getCopiedActiveUserList();

        LOG.forLevel(ELogLevel.E_TRACE).withMessage("handleUpdateClientsAndPossibleEditRightsRequests: " + this.getEditorUID() + ", count active-users: " + lActiveUsers.size() ).log();

        final Set< String > aClientsRequestingEditRights =
            lActiveUsers.stream()
                        .filter(u -> u.optBoolean(ClientStatusProperties.PROP_REQUESTS_EDITRIGHTS))
                        .map(u -> u.optString(ClientStatusProperties.PROP_CLIENTUID))
                        .collect(Collectors.toSet());

        if (acceptsEditRightsRequest())
        {
            if (!aClientsRequestingEditRights.isEmpty())
            {
                final String sClientUID = aClientsRequestingEditRights.iterator().next();
                LOG.forLevel(ELogLevel.E_TRACE).withMessage("document: " + this.getEditorUID() + ", requested: " + sClientUID).log();

                editRightsAsync(EditRightsProperties.ACTION_EDITRIGHTS_APPROVED, sClientUID);
            }
        }
        else
        {
            editRightsAsync(EditRightsProperties.ACTION_EDITRIGHTS_DECLINED, EditRightsProperties.ACTION_EDITRIGHTS_ALL);
        }
    }
}
