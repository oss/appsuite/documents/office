/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.image;

import com.openexchange.office.rt2.perftest.operations.Dimension;

public class ImageDimension {
	public static final double pixelTo100ThMMFactor = 26.45625;

	private ImageDimension() {}

	public static Dimension getDimensionFor100thMM(final Dimension dim) {
		final int newWidth = (int)Math.ceil(dim.getWidth() * pixelTo100ThMMFactor);
		final int newHeight = (int)Math.ceil(dim.getHeight() * pixelTo100ThMMFactor);

		return new Dimension(newWidth, newHeight);
	}
}
