/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.net.URI;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import com.openexchange.office.rt2.error.HttpStatusCode;
import com.openexchange.office.tools.error.ErrorCode;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

public class OXDocSpellcheckRestAPI {

	// -------------------------------------------------------------------------
	private static final Logger LOG = Slf4JLogger.create(OXDocSpellcheckRestAPI.class);

	// -------------------------------------------------------------------------
	public static final String REST_API_SPELLCHECK = "/spellchecker";

	// -------------------------------------------------------------------------
	public static final String REST_JSON_ROOT = "data";

	// -------------------------------------------------------------------------
	private OXAppsuite m_aContext = null;

	// -------------------------------------------------------------------------
	public OXDocSpellcheckRestAPI() {
		super();
	}

	// -------------------------------------------------------------------------
	public synchronized void bind(final OXAppsuite aContext) throws Exception {
		m_aContext = aContext;
	}

	/*
	 * Request URL: http://localhost/appsuite/api/spellchecker?session=
	 * 70a85301b6aa4a67911a5a7a0008634d action: spellparagraph paragraph: [{
	 * "word":"Text Body in Times 12 pt, line spacing in 1.15 lines, space after paragraph is 12 pt, Text Body in Times 12 pt, Text Body in Times 12 pt, line spacing in "
	 * ,"locale":"en-US"},{"word":"","locale":"en-US"},{
	 * "word":"1.15 lines, space after paragraph is 12 pt, Text Body in Times 12 pt, Text Body in Times 12 pt, line spacing in 1.15 lines, space after paragraph is 12 pt, Text Body in Times 12 pt,"
	 * ,"locale":"en-US"}] uid: 1578489425413.1323 app: text
	 */
	// -------------------------------------------------------------------------
	public JSONObject spellcheckParagraph(String paragraphText, String appType, String locale) throws Exception {
		final OXConnection connection = m_aContext.accessConnection();
		final OXSession session = m_aContext.accessSession();
		final String restAPIUri = mem_BaseURI() + REST_API_SPELLCHECK;
		final String sessionId = session.getSessionId();

		final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
		final URI aURI = connection.buildRequestUri(restAPIUri, aEmptyNameValuePair);

		// create data structure for the paragraph
		final JSONArray paragraphs = new JSONArray();
		final JSONObject paragraph = new JSONObject();
		paragraph.put("word", paragraphText);
		paragraph.put("locale", locale);
		paragraphs.put(paragraph);

		final NameValuePair[] aBodyNameValuePairs = new NameValuePair[5];
		aBodyNameValuePairs[0] = new BasicNameValuePair("action", "spellparagraph");
		aBodyNameValuePairs[1] = new BasicNameValuePair("session", sessionId);
		aBodyNameValuePairs[2] = new BasicNameValuePair("paragraph", paragraphs.toString());
		aBodyNameValuePairs[3] = new BasicNameValuePair("uid", Long.toString(System.currentTimeMillis()));
		aBodyNameValuePairs[4] = new BasicNameValuePair("app", appType);

		final URIBuilder aUriBuilder = new URIBuilder();
		for (final NameValuePair aParam : aBodyNameValuePairs) {
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        }
		final String sEncBodyString = aUriBuilder.build().toString().substring(1);

		LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + sEncBodyString).log();

		final UnboundHttpResponse response = connection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
		final StatusLine result = response.getStatusLine();

		if (result.getStatusCode() != HttpStatusCode.OK.code()) {
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()),
					"Bad server response : " + result);
        }

		final String body = response.getBody();
		if (StringUtils.isEmpty(body)) {
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");
        }

		final JSONObject aBody = new JSONObject(body);

		return aBody.getJSONObject(REST_JSON_ROOT);
	}

	// -------------------------------------------------------------------------
	public JSONObject spellcheckWord(String word, String appType, String locale) throws Exception {
		final OXConnection connection = m_aContext.accessConnection();
		final OXSession session = m_aContext.accessSession();
		final String restAPIUri = mem_BaseURI() + REST_API_SPELLCHECK;
		final String sessionId = session.getSessionId();

		final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
		final URI aURI = connection.buildRequestUri(restAPIUri, aEmptyNameValuePair);

		final NameValuePair[] aBodyNameValuePairs = new NameValuePair[7];
		aBodyNameValuePairs[0] = new BasicNameValuePair("action", "spellword");
		aBodyNameValuePairs[1] = new BasicNameValuePair("session", sessionId);
		aBodyNameValuePairs[2] = new BasicNameValuePair("word", word);
		aBodyNameValuePairs[3] = new BasicNameValuePair("uid", Long.toString(System.currentTimeMillis()));
		aBodyNameValuePairs[4] = new BasicNameValuePair("app", appType);
		aBodyNameValuePairs[5] = new BasicNameValuePair("locale", locale);
        aBodyNameValuePairs[6] = new BasicNameValuePair("offset", "0");

		final URIBuilder aUriBuilder = new URIBuilder();
		for (final NameValuePair aParam : aBodyNameValuePairs) {
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        }
		final String sEncBodyString = aUriBuilder.build().toString().substring(1);

		LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + sEncBodyString).log();

		final UnboundHttpResponse response = connection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
		final StatusLine result = response.getStatusLine();

		if (result.getStatusCode() != HttpStatusCode.OK.code()) {
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()),
					"Bad server response : " + result);
        }

		final String body = response.getBody();
		if (StringUtils.isEmpty(body)) {
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");
        }

		final JSONObject aBody = new JSONObject(body);

		return aBody.getJSONObject(REST_JSON_ROOT);
	}

	// -------------------------------------------------------------------------
	public JSONObject spellcheckWordReplacements(String word, String appType, String locale) throws Exception {
		final OXConnection connection = m_aContext.accessConnection();
		final OXSession session = m_aContext.accessSession();
		final String restAPIUri = mem_BaseURI() + REST_API_SPELLCHECK;
		final String sessionId = session.getSessionId();

		final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
		final URI aURI = connection.buildRequestUri(restAPIUri, aEmptyNameValuePair);

		final NameValuePair[] aBodyNameValuePairs = new NameValuePair[6];
		aBodyNameValuePairs[0] = new BasicNameValuePair("action", "spellwordreplacements");
		aBodyNameValuePairs[1] = new BasicNameValuePair("session", sessionId);
		aBodyNameValuePairs[2] = new BasicNameValuePair("word", word);
		aBodyNameValuePairs[3] = new BasicNameValuePair("uid", Long.toString(System.currentTimeMillis()));
		aBodyNameValuePairs[4] = new BasicNameValuePair("app", appType);
		aBodyNameValuePairs[5] = new BasicNameValuePair("locale", locale);

		final URIBuilder aUriBuilder = new URIBuilder();
		for (final NameValuePair aParam : aBodyNameValuePairs) {
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        }
		final String sEncBodyString = aUriBuilder.build().toString().substring(1);

		LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + sEncBodyString).log();

		final UnboundHttpResponse response = connection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
		final StatusLine result = response.getStatusLine();

		if (result.getStatusCode() != HttpStatusCode.OK.code()) {
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()),
					"Bad server response : " + result);
        }

		final String body = response.getBody();
		if (StringUtils.isEmpty(body)) {
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");
        }

		final JSONObject aBody = new JSONObject(body);

		return aBody.getJSONObject(REST_JSON_ROOT);
	}

	// -------------------------------------------------------------------------
	public JSONObject spellcheckSupportedLocales() throws Exception {
		final OXConnection connection = m_aContext.accessConnection();
		final OXSession session = m_aContext.accessSession();
		final String restAPIUri = mem_BaseURI() + REST_API_SPELLCHECK;
		final String sessionId = session.getSessionId();

		final NameValuePair[] aEmptyNameValuePair = new NameValuePair[0];
		final URI aURI = connection.buildRequestUri(restAPIUri, aEmptyNameValuePair);

		final NameValuePair[] aBodyNameValuePairs = new NameValuePair[3];
		aBodyNameValuePairs[0] = new BasicNameValuePair("action", "supportedlocales");
		aBodyNameValuePairs[1] = new BasicNameValuePair("session", sessionId);
		aBodyNameValuePairs[2] = new BasicNameValuePair("uid", Long.toString(System.currentTimeMillis()));

		final URIBuilder aUriBuilder = new URIBuilder();
		for (final NameValuePair aParam : aBodyNameValuePairs) {
            aUriBuilder.addParameter(aParam.getName(), aParam.getValue());
        }
		final String sEncBodyString = aUriBuilder.build().toString().substring(1);

		LOG.forLevel(ELogLevel.E_INFO).withMessage("POST : " + aURI + "\n and body " + sEncBodyString).log();

		final UnboundHttpResponse response = connection.doPost(aURI, "application/x-www-form-urlencoded", sEncBodyString);
		final StatusLine result = response.getStatusLine();

		if (result.getStatusCode() != HttpStatusCode.OK.code()) {
            throw new ServerResponseException(HttpStatusCode.createFromInt(result.getStatusCode()),
					"Bad server response : " + result);
        }

		final String body = response.getBody();
		if (StringUtils.isEmpty(body)) {
            throw new ServerResponseException(ErrorCode.GENERAL_UNKNOWN_ERROR, "Answer from server is empty!");
        }

		final JSONObject aBody = new JSONObject(body);

		return aBody.getJSONObject(REST_JSON_ROOT);
	}

	// -------------------------------------------------------------------------
	private String mem_BaseURI() throws Exception {
		return m_aContext.accessConfig().sAPIRelPath;
	}

}
