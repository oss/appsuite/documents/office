/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.operations;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

public class OperationsUtils
{
    //-------------------------------------------------------------------------
	private static class StringPair extends Pair<String, String>
	{
	    //-------------------------------------------------------------------------
		private static final long serialVersionUID = 1L;
		private String left;
		private String right;
		private Integer hash;

	    //-------------------------------------------------------------------------
		public StringPair(String left, String right) {
			this.left = left;
			this.right = right;
		}

	    //-------------------------------------------------------------------------
		@Override
		public String setValue(String value) {
			String tmp = right;
			right = value;
			return tmp;
		}

	    //-------------------------------------------------------------------------
		@Override
		public String getLeft() {
			return left;
		}

	    //-------------------------------------------------------------------------
		@Override
		public String getRight() {
			return right;
		}

	    //-------------------------------------------------------------------------
		@Override
		public boolean equals(final Object aObject) {
			if (null == aObject)
				return false;

			if (aObject instanceof StringPair) {
				final StringPair aPair = (StringPair)aObject;
				return (StringUtils.equals(this.left, aPair.left) &&
						StringUtils.equals(this.right, aPair.right));
			}

			return false;
		}

	    //-------------------------------------------------------------------------
		@Override
		public int hashCode() {
			if (hash == null)
				hash = calcHash(calcHash(0, left), right);
			return hash;
		}

	    //-------------------------------------------------------------------------
		private int calcHash(int hash, String str) {
	        int h = hash;
	        if (h == 0 && str.length() > 0) {
	            for (int i = 0; i < str.length(); i++) {
	                h = 31 * h + str.charAt(i);
	            }
	        }
	        return h;
		}
	}

    //-------------------------------------------------------------------------
	private OperationsUtils() {}

    //-------------------------------------------------------------------------
	private static final Map<String, String> KEY_COMPRESS_MAP;

    //-------------------------------------------------------------------------
	private static final Map<String, StringPair> VALUE_COMPRESS_MAP;

    //-------------------------------------------------------------------------
	private static final Map<String, String> KEY_DECOMPRESS_MAP;

    //-------------------------------------------------------------------------
	private static final Map<String, StringPair> VALUE_DECOMPRESS_MAP;

    //-------------------------------------------------------------------------
	static
	{
		final Map<String, String> aTmp = new HashMap<>();

		// top level keys
		aTmp.put("name"     , "n");
		aTmp.put("start"    , "o");
		aTmp.put("end"      , "q");
		aTmp.put("text"     , "w");
		aTmp.put("attrs"    , "a");
		aTmp.put("styleId"  , "st");
		aTmp.put("styleName", "sn");
		aTmp.put("target"   , "ta");
		aTmp.put("type"     , "ty");

		// families
		aTmp.put("changes"     , "cg");
		aTmp.put("character"   , "ch");
		aTmp.put("drawing"     , "dr");
		aTmp.put("paragraph"   , "pa");
		aTmp.put("presentation", "pr");
		aTmp.put("shape"       , "sh");
		aTmp.put("table"       , "tb");

		// character attributes
		aTmp.put("fontNameAsian",    "fna");
		aTmp.put("fontNameComplex",  "fnc");
		aTmp.put("fontNameEastAsia", "fnea");
		aTmp.put("fontNameSymbol",   "fns");
		aTmp.put("minFrameHeight",   "mfh");

		KEY_COMPRESS_MAP = Collections.unmodifiableMap(aTmp);
	}

    //-------------------------------------------------------------------------
	static
	{
		final Map<String, StringPair> aTmp = new HashMap<>();

		// operation names
		aTmp.put("changeAutoStyle", new StringPair("cas", "n"));
		aTmp.put("changeCells", new StringPair("cc", "n"));
		aTmp.put("changeCFRule", new StringPair("ccf", "n"));
		aTmp.put("changeColumns", new StringPair("ccol", "n"));
		aTmp.put("changeComment", new StringPair("cco", "n"));
		aTmp.put("changeLayout", new StringPair("cla", "n"));
		aTmp.put("changeMaster", new StringPair("cma", "n"));
		aTmp.put("changeName", new StringPair("cn", "n"));
		aTmp.put("changeRows", new StringPair("cro", "n"));
		aTmp.put("changeStyleSheet", new StringPair("css", "n"));
		aTmp.put("changeTable", new StringPair("ct", "n"));
		aTmp.put("changeTableColumn", new StringPair("ctc", "n"));
		aTmp.put("changeDVRule", new StringPair("cva", "n"));
		aTmp.put("copySheet", new StringPair("cs", "n"));
		aTmp.put("createError", new StringPair("ce", "n"));
		aTmp.put("delete", new StringPair("d", "n"));
		aTmp.put("deleteAutoStyle", new StringPair("das", "n"));
		aTmp.put("deleteCFRule", new StringPair("dcf", "n"));
		aTmp.put("deleteChartDataSeries", new StringPair("dcd", "n"));
		aTmp.put("deleteColumns", new StringPair("dcol", "n"));
		aTmp.put("deleteComment", new StringPair("dco", "n"));
		aTmp.put("deleteDrawing", new StringPair("dd", "n"));
		aTmp.put("deleteHeaderFooter", new StringPair("dhf", "n"));
		aTmp.put("deleteHyperlink", new StringPair("dhy", "n"));
		aTmp.put("deleteName", new StringPair("dn", "n"));
		aTmp.put("deleteNumberFormat", new StringPair("dnf", "n"));
		aTmp.put("deleteRows", new StringPair("dro", "n"));
		aTmp.put("deleteSheet", new StringPair("dsh", "n"));
		aTmp.put("deleteStyleSheet", new StringPair("dss", "n"));
		aTmp.put("deleteTable", new StringPair("dta", "n"));
		aTmp.put("deleteDVRule", new StringPair("dva", "n"));
		aTmp.put("group", new StringPair("g", "n"));
		aTmp.put("insertAutoStyle", new StringPair("ias", "n"));
		aTmp.put("insertBookmark", new StringPair("ibm", "n"));
		aTmp.put("insertCells", new StringPair("ic", "n"));
		aTmp.put("insertCFRule", new StringPair("icr", "n"));
		aTmp.put("insertChartDataSeries", new StringPair("icd", "n"));
		aTmp.put("insertColumn", new StringPair("ico", "n"));
		aTmp.put("insertColumns", new StringPair("icol", "n"));
		aTmp.put("insertComment", new StringPair("icom", "n"));
		aTmp.put("insertComplexField", new StringPair("icf", "n"));
		aTmp.put("insertDrawing", new StringPair("id", "n"));
		aTmp.put("insertField", new StringPair("if", "n"));
		aTmp.put("insertFontDescription", new StringPair("ifd", "n"));
		aTmp.put("insertHardBreak", new StringPair("ihb", "n"));
		aTmp.put("insertHeaderFooter", new StringPair("ihf", "n"));
		aTmp.put("insertHyperlink", new StringPair("ihl", "n"));
		aTmp.put("insertLayoutSlide", new StringPair("ils", "n"));
		aTmp.put("insertListStyle", new StringPair("ili", "n"));
		aTmp.put("insertMasterSlide", new StringPair("ims", "n"));
		aTmp.put("insertName", new StringPair("in", "n"));
		aTmp.put("insertNumberFormat", new StringPair("inf", "n"));
		aTmp.put("insertParagraph", new StringPair("ip", "n"));
		aTmp.put("insertRange", new StringPair("ira", "n"));
		aTmp.put("insertRows", new StringPair("iro", "n"));
		aTmp.put("insertSheet", new StringPair("ish", "n"));
		aTmp.put("insertSlide", new StringPair("isl", "n"));
		aTmp.put("insertStyleSheet", new StringPair("iss", "n"));
		aTmp.put("insertTab", new StringPair("itb", "n"));
		aTmp.put("insertTable", new StringPair("ita", "n"));
		aTmp.put("insertText", new StringPair("it", "n"));
		aTmp.put("insertTheme", new StringPair("ith", "n"));
		aTmp.put("insertDVRule", new StringPair("iva", "n"));
		aTmp.put("mergeCells", new StringPair("mc", "n"));
		aTmp.put("mergeParagraph", new StringPair("mp", "n"));
		aTmp.put("mergeTable", new StringPair("mt", "n"));
		aTmp.put("move", new StringPair("mo", "n"));
		aTmp.put("moveDrawing", new StringPair("md", "n"));
		aTmp.put("moveLayoutSlide", new StringPair("mls", "n"));
		aTmp.put("moveSheet", new StringPair("msh", "n"));
		aTmp.put("moveSlide", new StringPair("msl", "n"));
		aTmp.put("noOp", new StringPair("no", "n"));
		aTmp.put("setAttributes", new StringPair("sa", "n"));
		aTmp.put("setChartAxisAttributes", new StringPair("sca", "n"));
		aTmp.put("setChartDataSeriesAttributes", new StringPair("scd", "n"));
		aTmp.put("setChartGridlineAttributes", new StringPair("scg", "n"));
		aTmp.put("setChartLegendAttributes", new StringPair("scl", "n"));
		aTmp.put("setChartTitleAttributes", new StringPair("sct", "n"));
		aTmp.put("setDocumentAttributes", new StringPair("sda", "n"));
		aTmp.put("setDrawingAttributes", new StringPair("sdr", "n"));
		aTmp.put("setSheetAttributes", new StringPair("ssa", "n"));
		aTmp.put("setSheetName", new StringPair("ssn", "n"));
		aTmp.put("splitParagraph", new StringPair("sp", "n"));
		aTmp.put("splitTable", new StringPair("st", "n"));
		aTmp.put("ungroup", new StringPair("ug", "n"));
		aTmp.put("unknownValue", new StringPair("uk", "n"));
		aTmp.put("updateComplexField", new StringPair("ucf", "n"));
		aTmp.put("updateField", new StringPair("uf", "n"));

		VALUE_COMPRESS_MAP = Collections.unmodifiableMap(aTmp);
	}

    //-------------------------------------------------------------------------
	static
	{
		final Map<String, String> aTmp = new HashMap<>();

		KEY_COMPRESS_MAP.entrySet().stream()
		                           .forEach(e -> putInvertedStringEntry(aTmp, e));

		KEY_DECOMPRESS_MAP = Collections.unmodifiableMap(aTmp);
	}

    //-------------------------------------------------------------------------
	static
	{
		final Map<String, StringPair> aTmp = new HashMap<>();

		VALUE_COMPRESS_MAP.entrySet().stream()
		                             .forEach(e -> putInvertedStringPairEntry(aTmp, e));

		VALUE_DECOMPRESS_MAP = Collections.unmodifiableMap(aTmp);
	}

    //-------------------------------------------------------------------------
	public static void compressObject(final JSONObject aJSONObject) throws Exception
	{
		final String[] aKeys = aJSONObject.keySet().toArray(new String[0]);
		for (String k : aKeys)
		{
			if (KEY_COMPRESS_MAP.containsKey(k))
				replaceWithCompressed(aJSONObject, k, KEY_COMPRESS_MAP.get(k));
			else if (aJSONObject.optJSONObject(k) != null)
				compressObject(aJSONObject.getJSONObject(k));
		}
	}

    //-------------------------------------------------------------------------
	public static void decompressObject(final JSONObject aJSONObject) throws Exception
	{
		final String[] aKeys = aJSONObject.keySet().toArray(new String[0]);
		for (String k : aKeys)
		{
			if (KEY_DECOMPRESS_MAP.containsKey(k))
				replaceWithDecompressed(aJSONObject, k, KEY_DECOMPRESS_MAP.get(k));
			else if (aJSONObject.optJSONObject(k) != null)
				decompressObject(aJSONObject.getJSONObject(k));
		}
	}

    //-------------------------------------------------------------------------
	private static void replaceWithCompressed(final JSONObject aJSON, String sOldKey, String sNewKey) throws Exception
	{
		Object v = aJSON.get(sOldKey);
		if (VALUE_COMPRESS_MAP.containsKey(v))
		{
			final StringPair aPair = VALUE_COMPRESS_MAP.get(v);
			if (aPair.getRight().equals(sNewKey))
				v = aPair.getLeft();
		}

		aJSON.remove(sOldKey);

		if (v instanceof JSONObject)
			compressObject((JSONObject)v);

		aJSON.put(sNewKey, v);
	}

    //-------------------------------------------------------------------------
	private static void replaceWithDecompressed(final JSONObject aJSON, String sOldKey, String sNewKey) throws Exception
	{
		Object v = aJSON.get(sOldKey);
		if (VALUE_DECOMPRESS_MAP.containsKey(v))
		{
			final StringPair aPair = VALUE_DECOMPRESS_MAP.get(v);
			if (aPair.getRight().equals(sOldKey))
				v = aPair.getLeft();
		}

		aJSON.remove(sOldKey);

		if (v instanceof JSONObject)
			decompressObject((JSONObject)v);

		aJSON.put(sNewKey, v);
	}

    //-------------------------------------------------------------------------
	private static void putInvertedStringEntry(final Map<String, String> aMap, final Entry<String, String> aEntry)
	{
		aMap.put(aEntry.getValue(), aEntry.getKey());
	}

    //-------------------------------------------------------------------------
	private static void putInvertedStringPairEntry(final Map<String, StringPair> aMap, final Entry<String, StringPair> aEntry)
	{
		aMap.put(aEntry.getValue().getKey(), new StringPair(aEntry.getKey(), aEntry.getValue().getValue()));
	}
}
