/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;

//=============================================================================
public class TestCaseDescriptor
{
    //-------------------------------------------------------------------------
    public TestCaseDescriptor ()
    {}
    
    //-------------------------------------------------------------------------
    public Integer nNr = null;
    
    //-------------------------------------------------------------------------
    /// [mandatory] the class name of the test case
    public String sImplClass = null;

    //-------------------------------------------------------------------------
    /// [mandatory] define how many instances of this test has to run
    public Integer nTestCount = null;
    
//	//-------------------------------------------------------------------------
//	/// [optional] if this test require a dedicated user - it's the number of a configured user
//	public Integer nUser = null;

    //-------------------------------------------------------------------------
    /// [optional] a list of user to be spawned over all running tests of this type
    public List< Integer > lUser = null;

    //-------------------------------------------------------------------------
    /// internal state - define the last selected user from lUser list ...
    public Integer nLastUserFromList = null;
    
    //-------------------------------------------------------------------------
    public void takeFromConfig (final Map< String, String > aConfig) throws Exception {
    	sImplClass = aConfig.get("impl");
    	nTestCount = ConfigUtils.readInt (aConfig, "count", null);
    	lUser      = new ArrayList< Integer >();

    	final Integer nSingleUser = ConfigUtils.readInt (aConfig, "user", null);
    	if (nSingleUser != null) {
    		lUser.add (nSingleUser);
    		return;
    	}

    	List< Integer > aUserList = null;
    	final String value = ConfigUtils.readString(aConfig, "user-list", null);
    	if (value != null) {
    		if (value.indexOf(',') >= 0) {
    			aUserList = ConfigUtils.readIntList(aConfig, "user-list");
    		} else {
        		aUserList = createIntListFromValue(ConfigUtils.readInt(value, 1));
    		}
    	}

    	if ((aUserList != null) && !aUserList.isEmpty()) {
    		lUser.addAll (aUserList);
    		return;
    	}
    
    	final Pair< Integer, Integer > aUserRange = ConfigUtils.readIntRange(aConfig, "user-range");
    	if (aUserRange != null) {
    		final int nStart = aUserRange.getLeft ();
    		final int nEnd   = aUserRange.getRight();
    		
    		for (int i=nStart; i<=nEnd; ++i)
    			lUser.add (i);
    		return;
    	}

    	Validate.isTrue (false, "Test case ["+nNr+"] has no valid user definition.");
    }

    //-------------------------------------------------------------------------
    public String toString()
    {
    	final StringBuilder sString = new StringBuilder (256);
    	sString.append (super.toString()          );
    	sString.append (" {"                      );
    	sString.append ("nr="          +nNr       );
    	sString.append (", impl-class="+sImplClass);
    	sString.append (", test-count="+nTestCount);
    	sString.append (", users="     +lUser     );
    	sString.append ("}"                       );
    	return sString.toString ();
    }

    //-------------------------------------------------------------------------
    private List<Integer> createIntListFromValue(int value) {
    	List<Integer> aList = new ArrayList<>();

    	if (value > 0) {
    		for (int i = 1; i <= value; i++) {
    			aList.add(i);
    		}
    	}

    	return aList;
    }
}
