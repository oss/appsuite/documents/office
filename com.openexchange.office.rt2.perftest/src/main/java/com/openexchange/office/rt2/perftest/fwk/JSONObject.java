/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

//=============================================================================
public class JSONObject extends org.json.JSONObject
{
    //-------------------------------------------------------------------------
    public JSONObject ()
    {
        super ();
    }

    //-------------------------------------------------------------------------
    public JSONObject (final String sString)
    {
        super (sString);
    }

    //-------------------------------------------------------------------------
    public JSONObject (final org.json.JSONObject aObject)
    {
        super (aObject.toString());
    }
    
    //-------------------------------------------------------------------------
    public JSONObject parseJSONString (final String sString)
    {
        final JSONObject         aParser = new JSONObject (sString);
        final Iterator< String > rKeys   = aParser.keys();
        while (rKeys.hasNext())
        {
            final String sKey   = rKeys.next();
            final Object aValue = aParser.get(sKey);
            put (sKey, aValue);
        }
        return this;
    }
    
    //-------------------------------------------------------------------------
    public boolean hasAndNotNull (final String sKey)
    {
        if ( ! super.has(sKey))
            return false;
            
        final Object aValue = super.opt (sKey);
        if (aValue == null)
            return false;
        
        return true;
    }

    //-------------------------------------------------------------------------
    @Override
    public JSONObject getJSONObject (final String sKey)
    {
        final org.json.JSONObject aObject = super.getJSONObject(sKey);
        if (aObject == null)
            return null;
        final JSONObject aMappedObject = new JSONObject (aObject);
        return aMappedObject;
    }

    //-------------------------------------------------------------------------
    @Override
    public JSONObject optJSONObject (final String sKey)
    {
        final org.json.JSONObject aObject = super.optJSONObject(sKey);
        if (aObject == null)
            return null;
        final JSONObject aMappedObject = new JSONObject (aObject);
        return aMappedObject;
    }
    
    //-------------------------------------------------------------------------
    public Set< Entry< String, Object > > entrySet ()
    {
        final Map< String, Object > aSet  = new HashMap< String, Object > ();
        final Iterator< String >    rKeys = super.keys();

        while (rKeys.hasNext())
        {
            final String sKey   = rKeys.next();
            final Object aValue = super.get(sKey);
            aSet.put (sKey, aValue);
        }
        
        return aSet.entrySet();
    }
}
