/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import com.openexchange.office.tools.error.ErrorCode;
import com.openexchange.office.rt2.error.HttpStatusCode;

//=============================================================================
public class ServerResponseException extends RuntimeException {

    //-------------------------------------------------------------------------
	private static final long serialVersionUID = 1L;

    //-------------------------------------------------------------------------
	private final HttpStatusCode aStatusCode;

    //-------------------------------------------------------------------------
	private final ErrorCode aErrorCode;

    //-------------------------------------------------------------------------
	public ServerResponseException(HttpStatusCode nStatusCode) {
		this.aStatusCode = nStatusCode;
		this.aErrorCode = ErrorCode.NO_ERROR;
	}

    //-------------------------------------------------------------------------
	public ServerResponseException(HttpStatusCode aStatusCode, final String sMsg) {
		super(sMsg);

		this.aErrorCode = ErrorCode.NO_ERROR;
		this.aStatusCode = aStatusCode;
	}

    //-------------------------------------------------------------------------
	public ServerResponseException(ErrorCode aErrorCode, final String sMsg) {
		super(sMsg);

		this.aStatusCode = HttpStatusCode.OK;
		this.aErrorCode = aErrorCode;
	}

    //-------------------------------------------------------------------------
	public ServerResponseException(HttpStatusCode nStatusCode, ErrorCode aErrorCode, final String sMsg) {
		super(sMsg);

		this.aStatusCode = nStatusCode;
		this.aErrorCode = aErrorCode;
	}

    //-------------------------------------------------------------------------
	public HttpStatusCode getStatusCode() {
		return this.aStatusCode;
	}

    //-------------------------------------------------------------------------
	public ErrorCode getErrorCode() {
		return this.aErrorCode;
	}

}
