/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.fwk;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.HandshakeResponse;
import javax.websocket.MessageHandler;
import javax.websocket.RemoteEndpoint.Async;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.jetty.websocket.jsr356.JettyClientContainerProvider;

//import org.glassfish.tyrus.client.ClientManager;
import com.openexchange.office.rt2.perftest.config.TestConfig;
import com.openexchange.office.rt2.perftest.config.TestRunConfig;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openxchange.office_communication.tools.logging.ELogLevel;
import com.openxchange.office_communication.tools.logging.v2.Logger;
import com.openxchange.office_communication.tools.logging.v2.Slf4JLogger;

//=============================================================================
public class OXWSClient extends    Endpoint
    				    //implements MessageHandler.Whole< String >
{
    //-------------------------------------------------------------------------
    private static final Logger LOG = Slf4JLogger.create(OXWSClient.class);

    //-------------------------------------------------------------------------
    public static final String WS_URL_ROOT = "/rt2/v1/default";

    //-------------------------------------------------------------------------
    public static final String WS_HEADER_COOKIE = "Cookie";

    private static int countConnects = 0;
    
    //-------------------------------------------------------------------------
    public OXWSClient ()
    {}

    //-------------------------------------------------------------------------
    public synchronized void bind (final OXAppsuite aAppsuite)
        throws Exception
    {
        m_aAppsuite        = aAppsuite;
    	m_aConfig          = aAppsuite.accessConfig();
        m_rHttpCookieStore = new WeakReference< OXCookies >(aAppsuite.accessCookies());
        m_eState           = EState.E_UNKNOWN;
    }
    
    //-------------------------------------------------------------------------
    public synchronized void terminateSession(CloseReason closeReason) throws Exception {
    	Session session = mem_WSSession(false);
    	session.close(closeReason);
    }
    
    //-------------------------------------------------------------------------
    public synchronized void setMessageHandler (final IMessageHandler iHandler)
        throws Exception
    {
    	m_iMsgHandler = iHandler;
    }

    //-------------------------------------------------------------------------
    public synchronized void simulateTwoConflictingSessions ()
        throws Exception
    {
        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("simulate conflicting WS sessions ...")
            .log        ();

        if (m_aWSSessionOld != null)
            throw new Exception ("Simulating two conflicting WS sessions is already active. Dont call it twice.");

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... cache old session/handler")
            .log        ();
        m_aWSSessionOld  = m_aWSSession;
        m_aWSSession     = null;
        m_aMsgHandlerOld = m_aMsgHandler;
        m_aMsgHandler    = null;

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... create new session/handler")
            .log        ();
        mem_WSSession (/*force new*/ true); // creates new handler implicit

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... check for real new handler")
            .setVar     ("old-handler", ObjectUtils.identityToString(m_aMsgHandlerOld))
            .setVar     ("new-handler", ObjectUtils.identityToString(m_aMsgHandler   ))
            .log        ();
        if (ObjectUtils.equals (m_aMsgHandler, m_aMsgHandlerOld))
            throw new RuntimeException ("Sorry : simulation failed. Both (old and new) handler are the same.");

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... check for real new session")
            .setVar     ("old-session", ObjectUtils.identityToString(m_aWSSessionOld))
            .setVar     ("new-session", ObjectUtils.identityToString(m_aWSSession   ))
            .log        ();
        if (ObjectUtils.equals (m_aWSSession, m_aWSSessionOld))
            throw new RuntimeException ("Sorry : simulation failed. Both (old and new) session are the same.");

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... release old sender")
            .log        ();
        m_aSend = null;

        LOG .forLevel   (ELogLevel.E_INFO)
            .withMessage("... simulation is active now")
            .log        ();
    }

    //-------------------------------------------------------------------------
    public synchronized void prepareClose ()
    	throws Exception
    {
    	LOG	.forLevel   (ELogLevel.E_INFO)
    		.withMessage("WS : prepare close ..." )
    		.setVar		("this"         , m_sSelf        )
    		.setVar		("connection-id", m_sConnectionId)
    		.log        ();
    	m_eState = EState.E_IN_CLOSE;
    }

    //-------------------------------------------------------------------------
    public synchronized void free ()
        throws Exception
    {
    	LOG	.forLevel   (ELogLevel.E_INFO)
    		.withMessage("WS : free ..." )
            .setVar     ("this"         , m_sSelf        )
    		.setVar		("connection-id", m_sConnectionId)
    		.log        ();

    	m_eState = EState.E_IN_CLOSE;

    	final Session aSession = m_aWSSession;

    	m_aSend        = null;
    	m_aWSSession     = null;
    	m_aClientManager = null;

    	if (aSession != null)
    		aSession.close ();

    	m_eState = EState.E_CLOSED;
    }

    //-------------------------------------------------------------------------
    public synchronized void shutdown () throws Exception {
    	free();

    	m_eState = EState.E_SHUTDOWN;
    }

    //-------------------------------------------------------------------------
    public synchronized void setConnectionId (final String sConnectionId)
        throws Exception
    {
    	m_sConnectionId = sConnectionId;
    }

    //-------------------------------------------------------------------------    
    public synchronized String getConnectionId ()
    {
       	return m_sConnectionId;
    }    
    
    //-------------------------------------------------------------------------
    @Override
    public synchronized void onOpen(final Session        aSession,
    				   				final EndpointConfig aConfig )
    {
        try
        {
    	LOG	.forLevel   (ELogLevel.E_INFO)
    		.withMessage("["+impl_getId()+"] WS : open ..." )
            .setVar     ("this"         , m_sSelf        )
    		.setVar		("connection-id", m_sConnectionId)
    		.log        ();
        }
        catch (Throwable ex)
        {
            throw new RuntimeException (ex);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized void onClose(final Session     aSession,
    								 final CloseReason aReason )
    {
        try
        {
        	if (m_eState == EState.E_IN_CLOSE)
        	{
        		LOG	.forLevel   (ELogLevel.E_WARNING)
        			.withMessage("["+impl_getId()+"] WS : closed ..."  )
                    .setVar     ("this"         , m_sSelf        )
        			.setVar		("connection-id", m_sConnectionId)
        			.setVar		("code"         , aReason != null ? aReason.getCloseCode   () : "null")
        			.setVar		("reason"       , aReason != null ? aReason.getReasonPhrase() : "null")
        			.log        ();
        	}
        	else
        	if (m_eState == EState.E_UNKNOWN)
        	{
        		// This case needs to be ignored.
        		// Seems that Tyrus create new instance of this client to call onClose() on it only ...
        		// Makes no sense - but it's true.
        	}
        	else
        	{
        		final Exception aStacktrace = new Exception ();
        		LOG	.forLevel   (ELogLevel.E_ERROR)
        			.withMessage("["+impl_getId()+"] WS : closed ... BUT UNEXPECTED ! (triggered from somewhere else)")
        			.withError  (aStacktrace)
                    .setVar     ("this"         , m_sSelf        )
        			.setVar		("connection-id", m_sConnectionId)
        			.setVar		("code"         , aReason != null ? aReason.getCloseCode   () : "null")
        			.setVar		("reason"       , aReason != null ? aReason.getReasonPhrase() : "null")
        			.log        ();
        	}
        }
        catch (Throwable ex)
        {
            throw new RuntimeException (ex);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public synchronized void onError(final Session   aSession,
    								 final Throwable aError  )
    {
    	LOG	.forLevel	(ELogLevel.E_ERROR)
    		.withError	(aError)
            .setVar     ("this"         , m_sSelf        )
    		.setVar		("connection-id", m_sConnectionId)
    		.log        ();
    }

    //-------------------------------------------------------------------------
    public /* no synchronized */ Future< Void > send(final String sJSON)
        throws Exception
    {
        LOG .forLevel   (ELogLevel.E_TRACE)
            .withMessage("["+impl_getId()+"] WS : send ...")
            .setVar     ("this"         , m_sSelf        )
            .setVar     ("connection-id", m_sConnectionId)
            .setVar     ("request"      , sJSON          )
            .log        ();

        if (m_eState != EState.E_SHUTDOWN) {
            final Async          aSend = mem_Send ();
            final Future< Void > aSync = aSend.sendText(sJSON);
            return aSync;
        } else {
        	return null;
        }
    }

    //-------------------------------------------------------------------------
    private /* no synchronized */ void impl_onMessage(final MessageHandler aSource ,
                                                      final String         sMessage)
        throws Exception
    {
		LOG	.forLevel   (ELogLevel.E_TRACE)
			.withMessage("["+impl_getId()+"] WS : received ...")
            .setVar     ("this"         , m_sSelf        )
			.setVar		("connection-id", m_sConnectionId)
    		.setVar     ("response"     , sMessage       )
			.log        ();

		impl_rejectMessageFromOldSession (aSource, sMessage);

		IMessageHandler iHandler = null;
		synchronized (this)
		{
			iHandler = m_iMsgHandler;
		}
		// Makes no sense to hide error in case no handler is set !
		iHandler.onMessage(this, sMessage);
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_rejectMessageFromOldSession (final MessageHandler aSource ,
                                                                final String         sMessage)
        throws Exception
    {
        if (ObjectUtils.equals(m_aMsgHandlerOld, aSource))
            throw new Exception ("Got message from old session :\n"+sMessage);
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_setHttpUserAgentOnWSRequest (final Map< String, List< String > > lWSHeaders)
        throws Exception
    {
    	final List< String > lUserAgents = new ArrayList< String > ();
    	lUserAgents.add(Constants.USER_AGENT);
    	lWSHeaders.put("user-agent", lUserAgents);
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_setHttpCookiesOnWSRequest (final Map< String, List< String > > lWSHeaders)
        throws Exception
    {
    	final OXCookies             rHttpCookieStore = m_rHttpCookieStore.get ();
    	final Map< String, String > lHttpCookies     = rHttpCookieStore.getCookies();
              List< String >        lWSCookies       = lWSHeaders.get(WS_HEADER_COOKIE);

        if (lWSCookies == null)
        	lWSCookies = new ArrayList< String >();

        LOG	.forLevel	(ELogLevel.E_TRACE               )
    		.withMessage("set cookies on WS request ..." )
            .setVar     ("this"         , m_sSelf        )
    		.setVar		("connection-id", m_sConnectionId)
    		.log 		();

        final Iterator< Entry< String, String > > rCookies = lHttpCookies.entrySet().iterator();
        while (rCookies.hasNext())
        {
        	final Entry< String, String > rCookie = rCookies.next();
        	final String                  sCookie = rCookie.getKey() + "=" + rCookie.getValue();

        	LOG	.forLevel	(ELogLevel.E_TRACE                  )
        		.withMessage("... set next cookie on WS request")
                .setVar     ("this"         , m_sSelf           )
        		.setVar		("connection-id", m_sConnectionId   )
        		.setVar		("cookie"       , sCookie           )
        		.log 		();

        	lWSCookies.add (sCookie);
        }

        lWSHeaders.put(WS_HEADER_COOKIE, lWSCookies);
    }

    //-------------------------------------------------------------------------
    private synchronized void impl_setHttpOriginOnWSRequest(final Map< String, List< String > > lWSHeaders) throws Exception {
        final List< String > origins = new ArrayList< String > ();
        if (m_aAppsuite.getUseAlternativeHost()) {
            origins.add(m_aAppsuite.accessConfig().sAlternativeServerURL);
        } else {
            origins.add(m_aAppsuite.accessConfig().sServerURL);
        }
        lWSHeaders.put("origin", origins);
    }

    //-------------------------------------------------------------------------
    private synchronized WebSocketContainer mem_ClientManager ()
    	throws Exception
    {
    	if (m_aClientManager == null)
    	{
    		JettyClientContainerProvider.useSingleton(true);
    		JettyClientContainerProvider.useServerContainer(true);
    	    final TestConfig            aConfig       = this.m_aConfig.rGlobalConfig;
    		final WebSocketContainer    aManager      = ContainerProvider.getWebSocketContainer();
    		aManager.setAsyncSendTimeout(aConfig.getWebsocketAsynSendTimeoutInMS ());
    		aManager.setDefaultMaxTextMessageBufferSize(aConfig.getWebsocketOutBufferSizeInBytes());
    		m_aClientManager = aManager;
    	}
    	return m_aClientManager;
    }

    //-------------------------------------------------------------------------
    private synchronized String impl_getId ()
        throws Exception
    {
        if (m_aAppsuite != null)
            return m_aAppsuite.getId();
        return "unknown";
    }

    //-------------------------------------------------------------------------
    private synchronized ClientEndpointConfig mem_WSConfig ()
        throws Exception
    {
    	if (m_aWSConfig == null)
    	{
    		final ClientEndpointConfig aConfig = ClientEndpointConfig.Builder.create().configurator(new ClientEndpointConfig.Configurator()    		
    		{
    	        @Override
    	        public void beforeRequest(final Map< String, List< String > > lHeaders)
    	        {
    	            super.beforeRequest(lHeaders);
    	            try
    	            {
    	            	impl_setHttpUserAgentOnWSRequest (lHeaders);
    		            impl_setHttpCookiesOnWSRequest   (lHeaders);
    		            impl_setHttpOriginOnWSRequest    (lHeaders);
    		        	//System.err.println ("REQUEST : "+impl_WSHeader2String (lHeaders));
    	            }
    	            catch (Throwable ex)
    	            {
    	            	throw new RuntimeException (ex);
    	            }
    	        }

    	        @Override
    	        public void afterResponse(final HandshakeResponse aResponse)
    	        {
    	        	//System.err.println ("RESPONSE : "+impl_WSHeader2String (aResponse.getHeaders()));
    	        }
    	    }).build();

    		m_aWSConfig = aConfig;
    	}
    	return m_aWSConfig;
    }

    //-------------------------------------------------------------------------
    private synchronized Session mem_WSSession (final boolean bForceNew)
        throws Exception
    {
        if (bForceNew)
            m_aWSSession = null;

    	if (m_aWSSession == null)
    	{
    		Validate.notEmpty(m_sConnectionId, "Miss 'connectionId'.");

    		final ClientEndpointConfig aWSConfig      = mem_WSConfig      ();
    		final WebSocketContainer aClientManager =  mem_ClientManager ();
    		final String               sWSURL         = m_aConfig.sWSURL  + WS_URL_ROOT + "/" + m_sConnectionId;

    		LOG	.forLevel	(ELogLevel.E_INFO                         )
    			.withMessage("["+impl_getId()+"] WS : connect to server '"+sWSURL+"' ...")
                .setVar     ("this"         , m_sSelf        )
    			.setVar		("connection-id", m_sConnectionId)
    			.log 		();

    		try {
        		LOG	.forLevel	(ELogLevel.E_INFO                         )
    			.withMessage("["+impl_getId()+"] WS : connect to server '"+sWSURL+"' ...")
                .setVar     ("this"         , m_sSelf        )
    			.setVar		("connection-id", m_sConnectionId)
    			.log 		();
    			final Session aWSSession = aClientManager.connectToServer(OXWSClient.class, aWSConfig, URI.create(sWSURL));
    			m_aWSSession = aWSSession;
    		} catch (Exception ex) {
        		LOG	.forLevel	(ELogLevel.E_ERROR                         )
    			.withMessage("["+impl_getId()+"] WS : connect to server '"+sWSURL+"' failed...")
                .setVar     ("this"         , m_sSelf        )
    			.setVar		("connection-id", m_sConnectionId)
    			.log 		();    		
    		}
    	}
    	return m_aWSSession;
    }

    //-------------------------------------------------------------------------
    private synchronized MessageHandler.Whole< String > mem_MessageHandler ()
        throws Exception
    {
        if (m_aMsgHandler == null)
        {
            m_aMsgHandler = new MessageHandler.Whole< String >()
            {
                @Override
                public void onMessage (final String sMessage)
                {
                    try {
                        final RT2Message msg = RT2MessageFactory.fromJSONString(sMessage);
                        LOG.forLevel(ELogLevel.E_TRACE).withMessage("Received message type: " + msg.getType() + ", msg: " + sMessage).log();
                    } catch (Exception ex) {
                        LOG.forLevel(ELogLevel.E_ERROR).withMessage("Error - Exception caught: " + ex.getMessage());
                    }


                    try
                    {
                        LOG .forLevel   (ELogLevel.E_TRACE)
                            .withMessage("got response ...")
                            .setVar     ("response"     , sMessage       )
                            .setVar     ("this"         , m_sSelf        )
                            .setVar     ("connection-id", m_sConnectionId)
                            .log        ();
                        impl_onMessage (m_aMsgHandler, sMessage);
                    }
                    catch (Throwable ex)
                    {
                        LOG .forLevel   (ELogLevel.E_ERROR)
                            .withError  (ex)
                            .setVar     ("this"         , m_sSelf        )
                            .setVar     ("connection-id", m_sConnectionId)
                            .log        ();
                    }
                }
            };
        }
        return m_aMsgHandler;
    }

    //-------------------------------------------------------------------------
    private synchronized Async mem_Send ()
        throws Exception
    {
    	if (m_aSend == null)
    	{
    		final Session        aWSSession  = mem_WSSession (/*force new*/ false);
    		final MessageHandler aMsgHandler = mem_MessageHandler ();
    		aWSSession.addMessageHandler(aMsgHandler);
//    		aWSSession.setMaxIdleTimeout(-1         ); // no automatic idle timeout - please

    		final Async aSend = aWSSession.getAsyncRemote();
//    		aSend.setSendTimeout(-1); // no automatic timeout - please
    		m_aSend = aSend;
    	}
    	return m_aSend;
    }

    //-------------------------------------------------------------------------
    private static enum EState
    {
    	E_UNKNOWN,
    	E_RUNNING,
    	E_IN_CLOSE,
    	E_CLOSED,
    	E_SHUTDOWN;
    }

    //-------------------------------------------------------------------------
    private OXAppsuite m_aAppsuite = null;

    //-------------------------------------------------------------------------
    private String m_sSelf = ObjectUtils.identityToString(this);

    //-------------------------------------------------------------------------
    private String m_sConnectionId = null;

    //-------------------------------------------------------------------------
    private TestRunConfig m_aConfig = null;

    //-------------------------------------------------------------------------
//    private ClientManager m_aClientManager = null;
    private WebSocketContainer m_aClientManager = null;

    //-------------------------------------------------------------------------
    private ClientEndpointConfig m_aWSConfig = null;

    //-------------------------------------------------------------------------
    private Session m_aWSSession = null;

    //-------------------------------------------------------------------------
    private Session m_aWSSessionOld = null;

    //-------------------------------------------------------------------------
    private MessageHandler.Whole< String > m_aMsgHandler = null;

    //-------------------------------------------------------------------------
    private MessageHandler.Whole< String > m_aMsgHandlerOld = null;

    //-------------------------------------------------------------------------
    private Async m_aSend = null;

    //-------------------------------------------------------------------------
    private WeakReference< OXCookies > m_rHttpCookieStore = null;

    //-------------------------------------------------------------------------
    private IMessageHandler m_iMsgHandler = null;

    //-------------------------------------------------------------------------
    private EState m_eState = EState.E_UNKNOWN;
}
