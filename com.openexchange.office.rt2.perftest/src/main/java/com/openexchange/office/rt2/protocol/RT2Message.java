/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.openexchange.office.rt2.perftest.fwk.JSONObject;

//=============================================================================
public class RT2Message implements Comparable<RT2Message>
{
    //-------------------------------------------------------------------------
    // force using of RT2MessageFactory methods for creating new message instances
    protected RT2Message ()
    {}

    //-------------------------------------------------------------------------
    public static void cleanHeaderBeforeSendToClient (final RT2Message aMessage)
        throws Exception
    {
        cleanInternalHeader (aMessage);

        aMessage.impl_setHeader("breadcrumbId"                 , null); // camel specific header
        aMessage.impl_setHeader(RT2Protocol.HEADER_SESSION_ID  , null);
        aMessage.impl_setHeader(RT2Protocol.HEADER_DOC_STATE   , null);
        aMessage.impl_setHeader(RT2Protocol.HEADER_DRIVE_DOC_ID, null);
        aMessage.impl_setHeader(RT2Protocol.HEADER_FOLDER_ID   , null);
        aMessage.impl_setHeader(RT2Protocol.HEADER_FILE_ID     , null);
    }

    //-------------------------------------------------------------------------
    public static void cleanHeaderBeforeSendToServer (final RT2Message aMessage)
        throws Exception
    {
        cleanInternalHeader (aMessage);
    }

    //-------------------------------------------------------------------------
    public static void cleanInternalHeader (final RT2Message aMessage)
        throws Exception
    {
        aMessage.impl_setHeader(RT2Protocol.HEADER_INTERNAL_RECIPIENTS, null);
    }

    //-------------------------------------------------------------------------
    public static void copyHeaderWithprefix (final RT2Message aFrom  ,
    								         final RT2Message aTo    ,
    								         final String     sPrefix)
    	throws Exception
    {
    	final Set< String > lHeader = aFrom.listHeader();
    	for (final String sHeader : lHeader)
    	{
    		if ( ! StringUtils.startsWith(sHeader, sPrefix))
    			continue;

    		final Object aValue = aFrom.getHeader(sHeader);
    		aTo.setHeader(sHeader, aValue);
    	}
    }

    //-------------------------------------------------------------------------
    public static void copyAllHeader (final RT2Message aFrom,
                                      final RT2Message aTo  )
        throws Exception
    {
        final Set< String > lHeader = aFrom.listHeader();
        for (final String sHeader : lHeader)
        {
            final Object aValue = aFrom.getHeader(sHeader);
            aTo.setHeader(sHeader, aValue);
        }
    }

    //-------------------------------------------------------------------------
    public static void copySomeHeader (final RT2Message aFrom  ,
    							       final RT2Message aTo    ,
    							       final String...  lHeader)
    	throws Exception
    {
        for (final String sHeader : lHeader)
        {
        	final Object aValue = aFrom.getHeader (sHeader);
        	aTo.setHeader (sHeader, aValue);
        }
    }

    //-------------------------------------------------------------------------
    public static void copyBody (final RT2Message aFrom,
                                 final RT2Message aTo  )
        throws Exception
    {
        final JSONObject aBody = aFrom.getBody();
        aTo.setBody (aBody);
    }

    //-------------------------------------------------------------------------
    public boolean isType (final String sType)
        throws Exception
    {
        final boolean bIsType = StringUtils.equalsIgnoreCase(getType(), sType);
        return bIsType;
    }

    //-------------------------------------------------------------------------
    public String getType ()
        throws Exception
    {
    	return m_sType;
    }

    //-------------------------------------------------------------------------
    public void setType (final String sType)
        throws Exception
    {
        m_sType = sType;
    }

    //-------------------------------------------------------------------------
    public String getMessageID ()
        throws Exception
    {
        final String sMsgId = impl_getHeader(RT2Protocol.HEADER_MSG_ID, null);
        return sMsgId;
    }

    //-------------------------------------------------------------------------
    public void setMessageID (final String sMsgId)
        throws Exception
    {
        impl_setHeader(RT2Protocol.HEADER_MSG_ID, sMsgId);
    }

    //-------------------------------------------------------------------------
    public void newMessageID ()
        throws Exception
    {
        setMessageID (UUID.randomUUID().toString());
    }

    //-------------------------------------------------------------------------
    public void setFlags (final int... lFlags)
        throws Exception
    {
        int nFlags = 0;
        for (int nFlag : lFlags)
            nFlags |= nFlag;
        m_nFlags = nFlags;
    }

    //-------------------------------------------------------------------------
    public boolean hasFlags (final int... lFlags)
        throws Exception
    {
        Validate.notNull(lFlags, "Invalid argument 'flags'.");

        final int nFlags = m_nFlags;

        for (int nFlag : lFlags)
        {
            final boolean bIsFlag = RT2MessageFlags.isFlag(nFlags, nFlag);
            if ( ! bIsFlag)
                return false;
        }

        return true;
    }

    //-------------------------------------------------------------------------
    public Set< String > listHeader ()
        throws Exception
    {
    	final Set< String > lHeader = new HashSet<> ();
   		lHeader.addAll (mem_Header ().keySet());

    	return lHeader;
    }

    //-------------------------------------------------------------------------
    public boolean hasHeader (final String sHeader)
        throws Exception
    {
    	final Set< String > lHeader = listHeader ();
    	final boolean       bHas    = lHeader.contains(sHeader);
    	return bHas;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    public < T > T getHeader (final String sHeader)
        throws Exception
    {
        final int nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        final T   aValue = (T) getHeader2 (sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, nFlags);
    	return aValue;
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("cast")
    public < T > T getHeader2 (final String sHeader   ,
                               final int    nMissCheck,
                               final int... lFlags    )
        throws Exception
    {
        if ( ! hasFlags(lFlags))
            throw new Exception ("Message ["+getType()+"] dont support requested flags ["+RT2MessageFlags.mapFlagsToString(lFlags)+"] on getting header ["+sHeader+"].");

        final T aValue = impl_getHeader(sHeader, null);
        if (aValue != null)
            return aValue;

        if ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY)
            throw new Exception ("Message ["+getType()+"] miss mandatory header ["+sHeader+"]. message={"+this+"}");

        if (
            ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) &&
            ( ! RT2MessageFlags.areFlagsNone(lFlags)                      )
           )
        {
            throw new Exception ("Message ["+getType()+"] miss header ["+sHeader+"] forced by flags. message={"+this+"}");
        }

        return (T) null;
    }

    //-------------------------------------------------------------------------
    public < T > void setHeader (final String sHeader,
                                 final T      aValue )
        throws Exception
    {
        final int nFlags = RT2Protocol.get().getFlag4Header(sHeader);
        setHeader2 (sHeader, RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY, aValue, nFlags);
    }

    //-------------------------------------------------------------------------
    public < T > void setHeader2 (final String sHeader   ,
                                  final int    nMissCheck,
                                  final T      aValue    ,
                                  final int... lFlags    )
        throws Exception
    {
        if ( ! hasFlags(lFlags))
            throw new Exception ("Message ["+getType()+"] dont support requested flags ["+RT2MessageFlags.mapFlagsToString(lFlags)+"] on setting header ["+sHeader+"].");

        if (aValue != null)
        {
            impl_setHeader(sHeader, aValue);
            return;
        }

        if ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_ALWAYS_MANDATORY)
            throw new Exception ("Message ["+getType()+"] miss value for setting mandatory header ["+sHeader+"].");

        if (
            ((nMissCheck & RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) == RT2MessageHeaderCheck.MISSCHECK_FLAG_MANDATORY) &&
            ( ! RT2MessageFlags.areFlagsNone(lFlags)                            )
           )
        {
            throw new Exception ("Message ["+getType()+"] miss value for setting header ["+sHeader+"] forced by flags.");
        }
    }

    //----------------------------------------------------------------------
    public void mergeHeader (final Map< String, Object > lOutsideHeader)
        throws Exception
    {
        final Iterator< String > rHeader = lOutsideHeader.keySet ().iterator ();
        while (rHeader.hasNext ())
        {
            final String sHeader = rHeader       .next ();
            final Object aValue  = lOutsideHeader.get  (sHeader);
            setHeader (sHeader, aValue);
        }
    };

    //-------------------------------------------------------------------------
    public JSONObject getBody ()
        throws Exception
    {
    	return impl_getBodyJSON();
    }

    //-------------------------------------------------------------------------
    public String getBodyString ()
        throws Exception
    {
        return impl_getBodyString();
    }

    //-------------------------------------------------------------------------
    public void setBody (final JSONObject aBody)
        throws Exception
    {
    	impl_setBodyJSON (aBody);
    }

    //-------------------------------------------------------------------------
    public void setBodyString (final String sBody)
        throws Exception
    {
        final String     sNormBody = StringUtils.defaultString(sBody, "{}");
        final JSONObject aBody     = new JSONObject (sNormBody);
        impl_setBodyJSON (aBody);
    }

    //-------------------------------------------------------------------------
    public boolean isValid (final StringBuilder sReason)
    	throws Exception
    {
    	final String sType = getType ();
    	if (StringUtils.isEmpty (sType))
    	{
    		sReason.append ("'type' not defined");
    		return false;
    	}

    	final String sMsgId = RT2MessageGetSet.getMessageID (this);
    	if (StringUtils.isEmpty (sMsgId))
    	{
    		sReason.append ("'msg_id' not defined");
    		return false;
    	}

    	return true;
    }

    //-------------------------------------------------------------------------
    public boolean isSimpleMessage ()
        throws Exception
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SIMPLE_MSG));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isSessionMessage ()
        throws Exception
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SESSION_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isSequenceMessage ()
        throws Exception
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_SEQUENCE_NR_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isDocMessage ()
        throws Exception
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_DOC_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isChunkMessage ()
        throws Exception
    {
        final boolean bIs = (hasFlags (RT2MessageFlags.FLAG_CHUNK_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    public boolean isFinalMessage ()
        throws Exception
    {
        final boolean bIs = ( ! hasFlags (RT2MessageFlags.FLAG_CHUNK_BASED));
        return bIs;
    }

    //-------------------------------------------------------------------------
    @Override
    public String toString ()
    {
        try
        {
            final StringBuilder sString = new StringBuilder (256);
            sString.append (super.toString ()                                    );
            sString.append (" : "                                                );
            sString.append (RT2MessageFactory.toJSONString(this, /*encode*/false));
            return sString.toString ();
        }
        catch (Throwable ex)
        {
            return super.toString ();
        }
    }

	//-------------------------------------------------------------------------
	@Override
	public int compareTo(RT2Message o)
	{
		try
		{
			final Integer nSeq1Obj = RT2MessageGetSet.getSeqNumber(this);
			final Integer nSeq2Obj = RT2MessageGetSet.getSeqNumber(o);
			final int nSeq1 = (nSeq1Obj == null) ? 0 : nSeq1Obj;
			final int nSeq2 = (nSeq2Obj == null) ? 0 : nSeq2Obj;
			return (nSeq1 == nSeq2) ? 0 : ((nSeq1 < nSeq2) ? -1 : 1);
		}
		catch (Exception e)
		{
			throw new RuntimeException("RT2: Exception caught while trying to compare sequence numbers!", e);
		}
	}

    //-------------------------------------------------------------------------
    private < T > void impl_setHeader (final String sHeader,
                                       final T      aValue )
        throws Exception
    {
		final Map< String, Object > lHeader = mem_Header ();
		if (aValue == null)
			lHeader.remove(sHeader);
		else
			lHeader.put(sHeader, aValue);
    }

    //-------------------------------------------------------------------------
    @SuppressWarnings("unchecked")
    private < T > T impl_getHeader (final String sHeader ,
                                    final T      aDefault)
        throws Exception
    {
        T aValue = (T) mem_Header ().get(sHeader);

        if (aValue == null)
            aValue = aDefault;

        return aValue;
    }

    //-------------------------------------------------------------------------
    private void impl_setBodyJSON (final JSONObject aBody)
    	throws Exception
    {
    	m_aBodyJSON = aBody;
    }

    //-------------------------------------------------------------------------
    private JSONObject impl_getBodyJSON ()
    	throws Exception
    {
    	return mem_BodyJSON();
    }

    //-------------------------------------------------------------------------
    private String impl_getBodyString ()
    	throws Exception
    {
    	return mem_BodyJSON().toString();
    }

    //-------------------------------------------------------------------------
    private Map< String, Object > mem_Header ()
        throws Exception
    {
    	if (m_lHeader == null)
    		m_lHeader = new HashMap<> ();
    	return m_lHeader;
    }

    //-------------------------------------------------------------------------
    private JSONObject mem_BodyJSON ()
        throws Exception
    {
        if (m_aBodyJSON == null)
            m_aBodyJSON = new JSONObject();

        return m_aBodyJSON;
    }

    //-------------------------------------------------------------------------
    private String m_sType = null;

    //-------------------------------------------------------------------------
    private int m_nFlags = 0;

    //-------------------------------------------------------------------------
    private Map< String, Object > m_lHeader = null;

    //-------------------------------------------------------------------------
    private JSONObject m_aBodyJSON = null;
}
