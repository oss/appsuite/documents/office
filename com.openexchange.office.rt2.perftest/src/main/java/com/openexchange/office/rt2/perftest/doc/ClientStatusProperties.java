/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.doc;

public class ClientStatusProperties
{
    //-------------------------------------------------------------------------
	private ClientStatusProperties() {}

    //-------------------------------------------------------------------------
    public static final String EMPTY_USERID                = "";
    public static final String EMPTY_USERNAME              = "";
    public static final int    NO_USERID                   = -1;

    //-------------------------------------------------------------------------
    public static final String PROP_UPDATECLIENTS_LEFT     = "left";
    public static final String PROP_UPDATECLIENTS_JOINED   = "joined";
    public static final String PROP_UPDATECLIENTS_CHANGED  = "changed";
    public static final String PROP_UPDATECLIENTS_LIST     = "list";

    //-------------------------------------------------------------------------
    public static final String PROP_CLIENTUID              = "clientUID";
    public static final String PROP_ACTIVE                 = "active";
    public static final String PROP_DURATION_OF_INACTIVITY = "durationOfInactivity";
    public static final String PROP_ID                     = "id";
    public static final String PROP_USERDATA               = "userData";
    public static final String PROP_USER_DISPLAY_NAME      = "userDisplayName";
    public static final String PROP_GUEST                  = "guest";
    public static final String PROP_REQUESTS_EDITRIGHTS    = "requestsEditRights";
}
