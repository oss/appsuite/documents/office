/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.protocol.internal;

import org.json.JSONException;
import com.openexchange.office.rt2.perftest.fwk.JSONObject;

/**
 * Contains data that is needed for synchronization purposes between
 * client and server.
 *
 * {@link SyncInfo}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.6.2
 */
public class SyncInfo {

    public static final int     SYNC_UNKNOWN_DOCOSN = -1;
    public static final String  SYNC_FIRST_VERSION  = "1";
    public static final String  SYNC_INFO = "syncInfo";

    private static final String SYNC_FOLDERID = "folderId";
    private static final String SYNC_FILEID = "fileId";
    private static final String SYNC_DOCOSN = "document-osn";
    private static final String SYNC_FILEVERSION = "fileVersion";
    private static final String SYNC_SUPPORTSVERSIONS = "supportsVersions";
    private static final String SYNC_FILENAME = "fileName";
    private static final String SYNC_RESTOREID = "restore_id";

    // members
    private int          m_documentOperationStateNumber = SYNC_UNKNOWN_DOCOSN;
    private String       m_version;
    private final String m_folderId;
    private final String m_fileId;
    private String       m_fileName;
    private boolean      m_supportsVersions = true;
    private String       m_restoreID;

    public SyncInfo(final String folderId, final String fileId) {
        m_folderId         = folderId;
        m_fileId           = fileId;
        m_supportsVersions = true;
    }

    public SyncInfo(final JSONObject jsonObject) throws Exception {
        m_folderId                     = jsonObject.getString(SYNC_FOLDERID);
        m_fileId                       = jsonObject.getString(SYNC_FILEID);
        m_version                      = jsonObject.getString(SYNC_FILEVERSION);
        m_documentOperationStateNumber = jsonObject.getInt(SYNC_FILEVERSION);
        m_supportsVersions             = jsonObject.getBoolean(SYNC_SUPPORTSVERSIONS);
        m_fileName                     = jsonObject.getString(SYNC_FILENAME);
        m_restoreID                    = jsonObject.getString(SYNC_RESTOREID);
    }

    public boolean isValidSyncInfo() {
        return (m_documentOperationStateNumber != SYNC_UNKNOWN_DOCOSN);
    }

    public void updateSyncInfo(final String version, int docOSN) {
        m_version = version;
        m_documentOperationStateNumber = docOSN;
    }

    public void updateFileVersion(final String version) {
        m_version = version;
    }

    public void updateDocmentOSN(int docOSN) {
        m_documentOperationStateNumber = docOSN;
    }

    public void setSupportsVersions(boolean supportsVersions) {
        m_supportsVersions = supportsVersions;
    }

    public void setFileName(final String fileName) {
        m_fileName = fileName;
    }

    public void setRestoreID(final String restoreID) {
        m_restoreID = restoreID;
    }

    public JSONObject toJSON()
    	throws JSONException
    {
        final JSONObject jsonObject = new JSONObject();

        jsonObject.put(SYNC_FOLDERID, m_folderId);
        jsonObject.put(SYNC_FILEID, m_fileId);
        jsonObject.put(SYNC_FILEVERSION, (m_version == null) ? JSONObject.NULL : m_version);
        jsonObject.put(SYNC_DOCOSN, m_documentOperationStateNumber);
        jsonObject.put(SYNC_SUPPORTSVERSIONS, m_supportsVersions);
        jsonObject.put(SYNC_FILENAME, (m_fileName == null) ? JSONObject.NULL : m_fileName);
        jsonObject.put(SYNC_RESTOREID, (m_restoreID == null) ? JSONObject.NULL : m_restoreID);

        return jsonObject;
    }

}
