/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.operations;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Operations
{
    //-------------------------------------------------------------------------
	private Operations()
	{
		// nothing to do
	}

    //-------------------------------------------------------------------------
	public static JSONArray createOperationsArray()
	{
        return new JSONArray();
	}

    //-------------------------------------------------------------------------
	public static List<JSONObject> toOperationsList(final JSONArray aOperations)
	{
		List<JSONObject> aOpsList = new ArrayList<>();
		for (int i = 0; i < aOperations.length(); i++)
			aOpsList.add(aOperations.getJSONObject(i));
		return aOpsList;
	}

    //-------------------------------------------------------------------------
    public static void addOperation(final JSONArray aOperations,
                                    final JSONObject aOperation)
    {
        aOperations.put(aOperation);
    }

    //-------------------------------------------------------------------------
    public static JSONObject createInsertTextOperation(final int    nPos      ,
    												   final int    nParagraph,
    												   final String sText     ,
    												   OSNGenerator aOSNGenerator,
    												   SelectionUpdater aSelUpdater)
        throws Exception
    {
        final JSONObject aOperation = new JSONObject();
        final JSONArray  aPos       = new JSONArray ();
        final int        nOSN       = aOSNGenerator.getNextOSN();

        aPos.put(nPos      );
        aPos.put(nParagraph);

        aOperation.put(OperationConstants.OPERATION_NAME , SupportedOperations.OPERATION_INSERTTEXT);
        aOperation.put(OperationConstants.OPERATION_TEXT , sText       );
        aOperation.put(OperationConstants.OPERATION_START, aPos        );
        aOperation.put(OperationConstants.OPERATION_OSN  , nOSN        );
        aOperation.put(OperationConstants.OPERATION_OPL  , 1           );

        if (null != aSelUpdater)
           aSelUpdater.updateCurrentSelection();

        OperationsUtils.compressObject(aOperation);
        return aOperation;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createInsertParagraph(final int nParagraph, final OSNGenerator aOSNGenerator)
        throws Exception
    {
        final JSONObject aOperation = new JSONObject();
        final JSONArray  aPos       = new JSONArray ();
        final int        nOSN       = aOSNGenerator.getNextOSN();

        aPos.put(nParagraph);

        aOperation.put(OperationConstants.OPERATION_NAME , SupportedOperations.OPERATION_INSERTPARAGRAPH);
        aOperation.put(OperationConstants.OPERATION_START, aPos             );
        aOperation.put(OperationConstants.OPERATION_OSN  , nOSN             );
        aOperation.put(OperationConstants.OPERATION_OPL  , 1                );

        OperationsUtils.compressObject(aOperation);
        return aOperation;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createInsertDrawingOperation(int nPos, int nParagraph, String sName, Dimension aDim, Margin aMargin, String sImageUrl, final OSNGenerator aOSNGenerator)
        throws Exception
    {
        final JSONObject aOperation = new JSONObject();
        final JSONArray  aPos       = new JSONArray ();
        final JSONObject aAttrs     = new JSONObject();
        final int        nOSN       = aOSNGenerator.getNextOSN();

        aPos.put(nPos      );
        aPos.put(nParagraph);

        final JSONObject aDrawingAttrs = new JSONObject();
        aDrawingAttrs.put("name", sName);
        aDrawingAttrs.put("width", aDim.getWidth());
        aDrawingAttrs.put("height", aDim.getHeight());
        aDrawingAttrs.put("marginTop", aMargin.top());
        aDrawingAttrs.put("marginBottom", aMargin.bottom());
        aDrawingAttrs.put("marginLeft", aMargin.left());
        aDrawingAttrs.put("marginRight", aMargin.right());
        aAttrs.put(Attributes.ATTR_DRAWING, aDrawingAttrs);

        final JSONObject aImageAttrs = new JSONObject();
        aImageAttrs.put("imageUrl", sImageUrl);
        aAttrs.put(Attributes.ATTR_IMAGE, aImageAttrs);

        final JSONObject aLineAttrs = new JSONObject();
        aLineAttrs.put("type", "none");
        aAttrs.put(Attributes.ATTR_LINE, aLineAttrs);

        aOperation.put(OperationConstants.OPERATION_NAME , SupportedOperations.OPERATION_INSERTDRAWING);
        aOperation.put(OperationConstants.OPERATION_TYPE , "image"     );
        aOperation.put(OperationConstants.OPERATION_START, aPos        );
        aOperation.put(OperationConstants.OPERATION_ATTRS, aAttrs      );
        aOperation.put(OperationConstants.OPERATION_OSN  , nOSN        );
        aOperation.put(OperationConstants.OPERATION_OPL  , 1           );

        OperationsUtils.compressObject(aOperation);
        return aOperation;
    }

    //-------------------------------------------------------------------------
    public static JSONObject createInsertDrawingOperationWithBase64String(int nPos, int nParagraph, String sName, Dimension aDim, Margin aMargin, String base64ImageData, final OSNGenerator aOSNGenerator)
        throws Exception
    {
    	// insertDrawing {start:[0,0],type:"image",attrs:{drawing:{width:16510,height:12383,marginTop:317,marginLeft:317,marginBottom:317,marginRight:317},image:{imageData:"data:image/jpeg;base64,"},line:{type:"none"}}}
        final JSONObject aOperation = new JSONObject();
        final JSONArray  aPos       = new JSONArray ();
        final JSONObject aAttrs     = new JSONObject();
        final int        nOSN       = aOSNGenerator.getNextOSN();

        aPos.put(nPos      );
        aPos.put(nParagraph);

        final JSONObject aDrawingAttrs = new JSONObject();
        aDrawingAttrs.put("name", sName);
        aDrawingAttrs.put("width", aDim.getWidth());
        aDrawingAttrs.put("height", aDim.getHeight());
        aDrawingAttrs.put("marginTop", aMargin.top());
        aDrawingAttrs.put("marginBottom", aMargin.bottom());
        aDrawingAttrs.put("marginLeft", aMargin.left());
        aDrawingAttrs.put("marginRight", aMargin.right());
        aAttrs.put(Attributes.ATTR_DRAWING, aDrawingAttrs);

        final JSONObject aImageAttrs = new JSONObject();
        aImageAttrs.put("imageData", base64ImageData);
        aAttrs.put(Attributes.ATTR_IMAGE, aImageAttrs);

        final JSONObject aLineAttrs = new JSONObject();
        aLineAttrs.put("type", "none");
        aAttrs.put(Attributes.ATTR_LINE, aLineAttrs);

        aOperation.put(OperationConstants.OPERATION_NAME , SupportedOperations.OPERATION_INSERTDRAWING);
        aOperation.put(OperationConstants.OPERATION_TYPE , "image"     );
        aOperation.put(OperationConstants.OPERATION_START, aPos        );
        aOperation.put(OperationConstants.OPERATION_ATTRS, aAttrs      );
        aOperation.put(OperationConstants.OPERATION_OSN  , nOSN        );
        aOperation.put(OperationConstants.OPERATION_OPL  , 1           );

        OperationsUtils.compressObject(aOperation);
        return aOperation;
    }
}
