/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.perftest.operations;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;

//-------------------------------------------------------------------------
public class OperationsHelper
{
    //-------------------------------------------------------------------------
    public static final String KEY_OSN            = "osn";

    //-------------------------------------------------------------------------
    public static final String KEY_OPL            = "opl";

    //-------------------------------------------------------------------------
    public static int getFollowUpOSN(final JSONArray operationsArray) 
    	throws Exception
    {
    	Validate.notNull(operationsArray);

        int result = -1;
        int operationCount = operationsArray.length();
        if (operationCount > 0) {
            int osn = operationsArray.getJSONObject(operationCount-1).getInt(KEY_OSN);
            int opl = operationsArray.getJSONObject(operationCount-1).getInt(KEY_OPL);
            result = osn + opl;
        }

        return result;
    }

    //-------------------------------------------------------------------------
    private OperationsHelper() {}
}
