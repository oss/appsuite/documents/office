/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.error;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import com.openexchange.office.rt2.perftest.fwk.JSONObject;


/**
 * An enumeration to provide specific error information from the server to
 * the client side. The enumeration provides method to convert to JSON object
 * or to be created from a JSON object.
 *
 * {@link ErrorCode}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class ErrorCode implements Cloneable {
    // definitions of the error codes (int)
    public static final int CODE_NO_ERROR                                   = 0;

    //-------------------------------------------------------------------------
    public static final int CODE_GENERAL_UNKNOWN_ERROR                      = 1;
    public static final int CODE_GENERAL_ARGUMENTS_ERROR                    = 10;
    public static final int CODE_GENERAL_FILE_NOT_FOUND_ERROR               = 11;
    public static final int CODE_GENERAL_QUOTA_REACHED_ERROR                = 12;
    public static final int CODE_GENERAL_PERMISSION_CREATE_MISSING_ERROR    = 13;
    public static final int CODE_GENERAL_PERMISSION_READ_MISSING_ERROR      = 14;
    public static final int CODE_GENERAL_PERMISSION_WRITE_MISSING_ERROR     = 15;
    public static final int CODE_GENERAL_MEMORY_TOO_LOW_ERROR               = 16;
    public static final int CODE_GENERAL_SYSTEM_BUSY_ERROR                  = 17;
    public static final int CODE_GENERAL_FILE_LOCKED_ERROR                  = 18;
    public static final int CODE_GENERAL_FILE_CORRUPT_ERROR                 = 19;
    public static final int CODE_GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR = 20;
    public static final int CODE_GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR   = 21;
    public static final int CODE_GENERAL_FOLDER_NOT_VISIBLE_ERROR           = 22;
    public static final int CODE_GENERAL_FOLDER_NOT_FOUND_ERROR             = 23;
    public static final int CODE_GENERAL_FOLDER_INVALID_ID_ERROR            = 24;
    public static final int CODE_GENERAL_SESSION_INVALID_ERROR              = 25;
    public static final int CODE_GENERAL_CLIENT_UID_UNKNOWN_ERROR           = 26;
    public static final int CODE_GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR    = 27;
    public static final int CODE_GENERAL_MISSING_SERVICE_ERROR              = 28;
    public static final int CODE_GENERAL_SERVICE_DISABLED_ERROR             = 29;
    public static final int CODE_GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR     = 30;
    public static final int CODE_GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR     = 31;
    public static final int CODE_GENERAL_PROTOCOL_ERROR                     = 32;
    public static final int CODE_GENERAL_MALICIOUS_ACK_HANDLING_ERROR       = 33;
    public static final int CODE_GENERAL_SERVICE_DOWN_ERROR                 = 34;
    public static final int CODE_GENERAL_RESOURCE_NOT_FOUND_ERROR           = 35;
    public static final int CODE_GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE   = 36;
    public static final int CODE_GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR     = 37;

    //-------------------------------------------------------------------------
    public static final int CODE_LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR     = 403;

    //-------------------------------------------------------------------------
    public static final String CONSTANT_NO_ERROR             = "NO_ERROR";
    public static final String CONSTANT_NO_ERROR_DESCRIPTION = "No error";

    // must be adapted
    private static final int ERRORCLASS_COUNT        = 4;
    public static final int ERRORCLASS_NO_ERROR      = 0;
    public static final int ERRORCLASS_WARNING       = 1;
    public static final int ERRORCLASS_ERROR         = 2;
    public static final int ERRORCLASS_FATAL_ERROR   = 3;

    // definitions of pre-defined the ErrorCode enums
    public static final ErrorCode NO_ERROR = new ErrorCode(CODE_NO_ERROR, CONSTANT_NO_ERROR, CONSTANT_NO_ERROR_DESCRIPTION, 0);
    public static final ErrorCode GENERAL_UNKNOWN_ERROR = new ErrorCode(CODE_GENERAL_UNKNOWN_ERROR, "GENERAL_UNKNOWN_ERROR", "An unknown error has occurred.", 2);
    public static final ErrorCode GENERAL_ARGUMENTS_ERROR = new ErrorCode(CODE_GENERAL_ARGUMENTS_ERROR, "GENERAL_ARGUMENTS_ERROR", "The request have been called with missing or wrong arguments", 2);
    public static final ErrorCode GENERAL_FILE_NOT_FOUND_ERROR = new ErrorCode(CODE_GENERAL_FILE_NOT_FOUND_ERROR, "GENERAL_FILE_NOT_FOUND_ERROR", "The document file couldn't be found", 2);
    public static final ErrorCode GENERAL_QUOTA_REACHED_ERROR = new ErrorCode(CODE_GENERAL_QUOTA_REACHED_ERROR, "GENERAL_QUOTA_REACHED_ERROR", "The quota for the user has been reached. Storing additonal data is not possible.", 2);
    public static final ErrorCode GENERAL_PERMISSION_CREATE_MISSING_ERROR = new ErrorCode(CODE_GENERAL_PERMISSION_CREATE_MISSING_ERROR, "GENERAL_PERMISSION_CREATE_MISSING_ERROR", "The permission to create a file is not available.", 2);
    public static final ErrorCode GENERAL_PERMISSION_READ_MISSING_ERROR = new ErrorCode(CODE_GENERAL_PERMISSION_READ_MISSING_ERROR, "GENERAL_PERMISSION_READ_MISSING_ERROR", "The permission to read a file is not available.", 2);
    public static final ErrorCode GENERAL_PERMISSION_WRITE_MISSING_ERROR = new ErrorCode(CODE_GENERAL_PERMISSION_WRITE_MISSING_ERROR, "GENERAL_PERMISSION_WRITE_MISSING_ERROR", "The permission to write a file is not available.", 2);
    public static final ErrorCode GENERAL_MEMORY_TOO_LOW_ERROR = new ErrorCode(CODE_GENERAL_MEMORY_TOO_LOW_ERROR, "GENERAL_MEMORY_TOO_LOW_ERROR", "The memory of the system is not sufficient to process the request.", 2);
    public static final ErrorCode GENERAL_SYSTEM_BUSY_ERROR = new ErrorCode(CODE_GENERAL_SYSTEM_BUSY_ERROR, "GENERAL_SYSTEM_BUSY_ERROR", "The system is too busy to process the current request.", 2);
    public static final ErrorCode GENERAL_FILE_LOCKED_ERROR = new ErrorCode(CODE_GENERAL_FILE_LOCKED_ERROR, "GENERAL_FILE_LOCKED_ERROR", "The document is locked and cannot be modified.", 2);
    public static final ErrorCode GENERAL_FILE_CORRUPT_ERROR = new ErrorCode(CODE_GENERAL_FILE_CORRUPT_ERROR, "GENERAL_FILE_CORRUPT_ERROR", "The document is corrupt and cannot read.", 2);
    public static final ErrorCode GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR = new ErrorCode(CODE_GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR, "GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR", "A server component is not working correctly, therefore the request couldn't be processed.", 2);
    public static final ErrorCode GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR = new ErrorCode(CODE_GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR, "GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR", "The document is stored by a background save process. Editing is not allowed.", 2);
    public static final ErrorCode GENERAL_FOLDER_NOT_VISIBLE_ERROR = new ErrorCode(CODE_GENERAL_FOLDER_NOT_VISIBLE_ERROR, "GENERAL_FOLDER_NOT_VISIBLE_ERROR", "The folder is not visible due to missing permissions", 2);
    public static final ErrorCode GENERAL_FOLDER_NOT_FOUND_ERROR = new ErrorCode(CODE_GENERAL_FOLDER_NOT_FOUND_ERROR, "GENERAL_FOLDER_NOT_FOUND_ERROR", "The folder cannot be found - may be removed folder", 2);
    public static final ErrorCode GENERAL_FOLDER_INVALID_ID_ERROR = new ErrorCode(CODE_GENERAL_FOLDER_INVALID_ID_ERROR, "GENERAL_FOLDER_INVALID_ID_ERROR", "The folder id is invalid!", 2);
    public static final ErrorCode GENERAL_SESSION_INVALID_ERROR = new ErrorCode(CODE_GENERAL_SESSION_INVALID_ERROR, "GENERAL_SESSION_INVALID_ERROR", "The session is invalid!", 2);    
    public static final ErrorCode GENERAL_CLIENT_UID_UNKNOWN_ERROR = new ErrorCode(CODE_GENERAL_CLIENT_UID_UNKNOWN_ERROR, "GENERAL_CLIENT_UID_UNKNOWN_ERROR", "The unique ID of the client is not known!", 2);
    public static final ErrorCode GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR = new ErrorCode(CODE_GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR, "GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR", "", 2);
    public static final ErrorCode GENERAL_MISSING_SERVICE_ERROR = new ErrorCode(CODE_GENERAL_MISSING_SERVICE_ERROR, "GENERAL_MISSING_SERVICE_ERROR", "", 2);
    public static final ErrorCode GENERAL_SERVICE_DISABLED_ERROR = new ErrorCode(CODE_GENERAL_SERVICE_DISABLED_ERROR, "GENERAL_SERVICE_DISABLED_ERROR", "", 2);
    public static final ErrorCode GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR = new ErrorCode(CODE_GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR, "GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR", "", 2);
    public static final ErrorCode GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR = new ErrorCode(CODE_GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR, "GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR", "", 2);
    public static final ErrorCode GENERAL_PROTOCOL_ERROR = new ErrorCode(CODE_GENERAL_PROTOCOL_ERROR, "GENERAL_PROTOCOL_ERROR", "", 2);
    public static final ErrorCode GENERAL_MALICIOUS_ACK_HANDLING_ERROR = new ErrorCode(CODE_GENERAL_MALICIOUS_ACK_HANDLING_ERROR, "GENERAL_MALICIOUS_ACK_HANDLING_ERROR", "", 2);
    public static final ErrorCode GENERAL_SERVICE_DOWN_ERROR = new ErrorCode(CODE_GENERAL_SERVICE_DOWN_ERROR, "GENERAL_SERVICE_DOWN_ERROR", "", 2);
    public static final ErrorCode GENERAL_RESOURCE_NOT_FOUND_ERROR = new ErrorCode(CODE_GENERAL_RESOURCE_NOT_FOUND_ERROR, "GENERAL_RESOURCE_NOT_FOUND_ERROR", "", 2);
    public static final ErrorCode GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE = new ErrorCode(CODE_GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE, "GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE", "", 2);
    public static final ErrorCode GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR = new ErrorCode(CODE_GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR, "GENERAL_NODE_IN_MAINTENANCE_MODE_ERROR", "", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode CREATEDOCUMENT_CONVERSION_FAILED_ERROR = new ErrorCode(200, "CREATEDOCUMENT_CONVERSION_FAILED_ERROR", "The document couldn't be created because of a conversion error!", 2);
    public static final ErrorCode CREATEDOCUMENT_CANNOT_READ_TEMPLATEFILE_ERROR = new ErrorCode(201, "CREATEDOCUMENT_CANNOT_READ_TEMPLATEFILE_ERROR", "The document couldn't be created because the template file couldn't be read!", 2);
    public static final ErrorCode CREATEDOCUMENT_CANNOT_READ_DEFAULTTEMPLATEFILE_ERROR = new ErrorCode(202, "CREATEDOCUMENT_CANNOT_READ_DEFAULTTEMPLATEFILE_ERROR", "The document coulnd't be created because the default template file couldn't be read!", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode RENAMEDOCUMENT_FAILED_ERROR = new ErrorCode(300, "RENAMEDOCUMENT_FAILED_ERROR", "Renaming the document failed!", 2);
    public static final ErrorCode RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR = new ErrorCode(301, "RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR", "The document couldn't be renamed as the validation of the new file name failed. File name contains invalid characters!", 2);
    public static final ErrorCode RENAMEDOCUMENT_NOT_SUPPORTED_ERROR = new ErrorCode(302, "RENAMEDOCUMENT_NOT_SUPPORTED_ERROR", "The document cannot be renamed, because the storage doesn't support this function.", 2);
    public static final ErrorCode RENAMEDOCUMENT_SAVE_IN_PROGRESS_WARNING = new ErrorCode(303, "RENAMEDOCUMENT_SAVE_IN_PROGRESS_WARNING", "The document must be saved before it can be renamed. Wait for completion of the asynchronous rename request.", 1);
    public static final ErrorCode RENAMEDOCUMENT_NOT_POSSIBLE_IN_PROGRESS_ERROR = new ErrorCode(304, "RENAMEDOCUMENT_NOT_POSSIBLE_IN_PROGRESS_ERROR", "Rename currently not possible, a rename request is in progress.", 2);
    public static final ErrorCode RENAMEDOCUMENT_FAILED_DUPLICATE_FILE_NAME = new ErrorCode(305, "RENAMEDOCUMENT_FAILED_DUPLICATE_FILE_NAME", "Renaming the document failed because filename already exists!", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode LOADDOCUMENT_FAILED_ERROR = new ErrorCode(400, "LOADDOCUMENT_FAILED_ERROR", "Loading the document failed!", 2);
    public static final ErrorCode LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR = new ErrorCode(401, "LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR", "Loading the document failed because operations couldn't be generated!", 2);
    public static final ErrorCode LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR = new ErrorCode(402, "LOADDOCUMENT_CANNOT_READ_PASSWORD_PROTECTED_ERROR", "Loading the document failed because it's password protected!", 2);
    public static final ErrorCode LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR = new ErrorCode(CODE_LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR, "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR", "Loading the document failed because the complexity of the content is too high!", 2);
    public static final ErrorCode LOADDOCUMENT_CALCENGINE_NOT_WORKING_ERROR = new ErrorCode(405, "LOADDOCUMENT_CALCENGINE_NOT_WORKING_ERROR", "Loading the document failed because the server-side engine used for calculating formulas is not available.", 2);
    public static final ErrorCode LOADDOCUMENT_TIMEOUT_RETRIEVING_STREAM_ERROR = new ErrorCode(406, "LOADDOCUMENT_TIMEOUT_RETRIEVING_STREAM_ERROR", "Loading the document failed, because the document stream couldn't retrieve in time.", 2);
    public static final ErrorCode LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR = new ErrorCode(407, "LOADDOCUMENT_COMPLEXITY_TOO_HIGH_SHEET_COUNT_ERROR", "Loading the document failed, because the number of sheets is too high!", 2);
    public static final ErrorCode LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR = new ErrorCode(408, "LOADDOCUMENT_STRICT_OOXML_NOT_SUPPORTED_ERROR", "Loading the document failed, because strict mode for OOXML is not supported!", 2);
    public static final ErrorCode LOADDOCUMENT_FEATURE_NOT_SUPPORTED_ERROR = new ErrorCode(409, "LOADDOCUMENT_FEATURE_NOT_SUPPORTED_ERROR", "Loading the document failed, the document contains a not supported feature!", 2);
    public static final ErrorCode LOADDOCUMENT_FEATURE_NOT_SUPPORTED_FRAME_ATTACHED_TO_FRAME_ERROR = new ErrorCode(410, "LOADDOCUMENT_FEATURE_NOT_SUPPORTED_FRAME_ATTACHED_TO_FRAME_ERROR", "Loading the document failed, the document contains frame attached to frame which is not supported!", 2);
    public static final ErrorCode LOADDOCUMENT_FILE_HAS_CHANGED_ERROR = new ErrorCode(411, "LOADDOCUMENT_FILE_HAS_CHANGED_ERROR", "Loading the document failed, because the document content has changed! Please try again later.", 2);
    public static final ErrorCode LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR = new ErrorCode(412, "LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR", "Loading the document is not possible, because no import filter could be found for the document format.", 3);
    public static final ErrorCode LOADDOCUMENT_PRESENTATION_IS_BEING_PRESENTED_ERROR = new ErrorCode(413, "LOADDOCUMENT_PRESENTATION_IS_BEING_PRESENTED_ERROR", "Loading the document is not possible, because it's currently presented and therefore cannot be edited.", 2);
    public static final ErrorCode LOADDOCUMENT_ENCRYPTIONINFO_NOT_AVAILABLE_ERROR = new ErrorCode(414, "LOADDOCUMENT_ENCRYPTIONINFO_NOT_AVAILABLE_ERROR", "", 2);
    public static final ErrorCode LOADDOCUMENT_GUARD_NOT_AVAILABLE_ERROR = new ErrorCode(415, "LOADDOCUMENT_GUARD_NOT_AVAILABLE_ERROR", "", 2);
    public static final ErrorCode LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR = new ErrorCode(416, "LOADDOCUMENT_ABORTED_BY_CLIENT_ERROR", "", 2);
    public static final ErrorCode LOADODCUMENT_MEDIATYPE_NOT_SUPPORTED_ERROR = new ErrorCode(417, "LOADODCUMENT_MEDIATYPE_NOT_SUPPORTED_ERROR", "Loading the document is not possible, because the document type is not supported.", 3);
    public static final ErrorCode LOADODCUMENT_DOCUMENT_DAMAGED_WARNING = new ErrorCode(418, "LOADODCUMENT_DOCUMENT_DAMAGED_WARNING", "", 1);
    //-------------------------------------------------------------------------
    public static final ErrorCode HANGUP_INVALID_OPERATIONS_SENT_ERROR = new ErrorCode(500, "HANGUP_INVALID_OPERATIONS_SENT_ERROR", "Client sent invalid operations and therefore lost edit rights!", 2);
    public static final ErrorCode HANGUP_NO_EDIT_RIGHTS_ERROR = new ErrorCode(501, "HANGUP_NO_EDIT_RIGHTS_ERROR", "Client sent operations without having edit rights. Operations are ignored and client must switch to read-only mode!", 2);
    public static final ErrorCode HANGUP_INVALID_OSN_DETECTED_ERROR = new ErrorCode(502, "HANGUP_INVALID_OSN_DETECTED_ERROR", "Client has invalid OSN after losing edit rights. Must switch to read-only mode.", 2);
    public static final ErrorCode HANGUP_CALCENGINE_NOT_RESPONDING_ERROR = new ErrorCode(503, "HANGUP_CALCENGINE_NOT_RESPONDING_ERROR", "Calcengine is not responding. Clients must be set to read-only to resync again.", 2);
    public static final ErrorCode HANGUP_OT_UNRESOVABLE_OP_CONFLICT_ERROR = new ErrorCode(504, "HANGUP_OT_UNRESOVABLE_OP_CONFLICT_ERROR", "", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode SAVEDOCUMENT_FAILED_ERROR = new ErrorCode(601, "SAVEDOCUMENT_FAILED_ERROR", "Saving document failed", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_NOBACKUP_ERROR = new ErrorCode(602, "SAVEDOCUMENT_FAILED_NOBACKUP_ERROR", "Saving document failed - no backup file could be written.", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_OPERATIONS_ERROR = new ErrorCode(603, "SAVEDOCUMENT_FAILED_OPERATIONS_ERROR", "Saving document failed - the filter was not able to apply the operations to the document.", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_NO_FILTER_ERROR = new ErrorCode(605, "SAVEDOCUMENT_FAILED_NO_FILTER_ERROR", "Saving document failed - document export filter not available.", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_NO_RESOURCEMANAGER_ERROR = new ErrorCode(606, "SAVEDOCUMENT_FAILED_NO_RESOURCEMANAGER_ERROR", "Saving document failed - no resource manager available for filter.", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_NO_DOCUMENTSTREAM_ERROR = new ErrorCode(607, "SAVEDOCUMENT_FAILED_NO_DOCUMENTSTREAM_ERROR", "Saving document failed - no document stream available.", 2);
    public static final ErrorCode SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR = new ErrorCode(608, "SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR", "Saving document failed - The document is inconsistent and is saved as _ox file!", 2);
    public static final ErrorCode SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR = new ErrorCode(609, "SAVEDOCUMENT_SAVE_IN_PROGRESS_ERROR", "Saving document not possible, because a save request is still in progress", 2);
    public static final ErrorCode SAVEDOCUMENT_PARTIAL_CHANGES_SAVED_ERROR = new ErrorCode(610, "SAVEDOCUMENT_PARTIAL_CHANGES_SAVED_ERROR", "Saving document was not completely successful, only a subset of changes could be saved", 2);
    public static final ErrorCode SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR = new ErrorCode(611, "SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR", "Saving document failed, because the document content has changed!", 2);
    public static final ErrorCode SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR = new ErrorCode(650, "SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR", "Saving document succeeded, but couldn't create backup file!", 1);
    public static final ErrorCode SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR = new ErrorCode(651, "SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR", "Saving document succeeded, but couldn't create backup file due to missing permissions!", 1);
    public static final ErrorCode SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR = new ErrorCode(652, "SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR", "Saving document succeeded, but couldn't read/write backup file due to missing permissions!", 1);
    public static final ErrorCode SAVEDOCUMENT_BACKUPFILE_QUOTA_REACHED_ERROR = new ErrorCode(653, "SAVEDOCUMENT_BACKUPFILE_QUOTA_REACHED_ERROR", "Saving document succeeded, but couldn't create backup file due to exceeded quota!", 1);
    public static final ErrorCode SAVEDOCUMENT_BACKUPFILE_IS_LOCKED_ERROR = new ErrorCode(654, "SAVEDOCUMENT_BACKUPFILE_IS_LOCKED_ERROR", "Saving document succeeded, but couldn't create backup file due to a lock protection!", 1);
    //-------------------------------------------------------------------------
    public static final ErrorCode COPYDOCUMENT_FAILED_ERROR = new ErrorCode(700, "COPYDOCUMENT_FAILED_ERROR", "Copying the document failed due to unknown reasons.", 2);
    public static final ErrorCode COPYDOCUMENT_USERFOLDER_UNKOWN_ERROR = new ErrorCode(701, "COPYDOCUMENT_USERFOLDER_UNKOWN_ERROR", "Copying the document failed, because the user's folder is unknown.", 2);
    public static final ErrorCode COPYDOCUMENT_FILENAME_UNKNOWN_ERROR = new ErrorCode(702, "COPYDOCUMENT_FILENAME_UNKNOWN_ERROR", "Copying the document failed, because the file name is unknown", 2);
    public static final ErrorCode COPYDOCUMENT_TEMPLATE_FORMAT_UNKOWN_ERROR = new ErrorCode(703, "COPYDOCUMENT_TEMPLATE_FORMAT_UNKOWN_ERROR", "Copying the document failed, because the template format couldn't be determined.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode GETTEMPLATELIST_UNKNOWN_ERROR = new ErrorCode(800, "GETTEMPLATELIST_UNKNOWN_ERROR", "Retrieving the template list failed.", 2);
    public static final ErrorCode GETTEMPLATELIST_USERFOLDER_UNKNOWN_ERROR = new ErrorCode(801, "GETTEMPLATELIST_USERFOLDER_UNKNOWN_ERROR", "The user folder is unknown, therefore the user template list cannot be retrieved.", 2);
    public static final ErrorCode GETTEMPLATELIST_GLOBALFOLDER_IO_ERROR = new ErrorCode(802, "GETTEMPLATELIST_GLOBALFOLDER_IO_ERROR", "There was an IO exception accessing the local file system.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode LOCALFILE_FILE_NOT_FOUND_ERROR = new ErrorCode(900, "LOCALFILE_FILE_NOT_FOUND_ERROR", "The local file cannot be found.", 2);
    public static final ErrorCode LOCALFILE_CANNOT_READ_FILE_ERROR = new ErrorCode(901, "LOCALFILE_CANNOT_READ_FILE_ERROR", "The local file cannot be read.", 2);
    public static final ErrorCode LOCALFILE_INVALID_ID_ERROR = new ErrorCode(902, "LOCALFILE_INVALID_ID_ERROR", "The file id provided is invalid.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode GETUSERINFO_UNKNOWN_ERROR = new ErrorCode(1000, "GETUSERINFO_UNKNOWN_ERROR", "Retrieving the user info failed.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode BACKUPFILE_WONT_WRITE_WARNING = new ErrorCode(1100, "BACKUPFILE_WONT_WRITE_WARNING", "A backup file won't be writte due to insufficient permissions.", 1);
    //-------------------------------------------------------------------------
    public static final ErrorCode SWITCHEDITRIGHTS_IN_PROGRESS_WARNING = new ErrorCode(1201, "SWITCHEDITRIGHTS_IN_PROGRESS_WARNING", "Switching edit rights not possible. A switch is currently in progress.", 1);
    public static final ErrorCode SWITCHEDITRIGHTS_NOT_POSSIBLE_SAVE_IN_PROGRESS_WARNING = new ErrorCode(1202, "SWITCHEDITRIGHTS_NOT_POSSIBLE_SAVE_IN_PROGRESS_WARNING", "Switching edit rights not possible. A save request is currently in progress.", 1);
    public static final ErrorCode SWITCHEDITRIGHTS_NEEDS_MORE_TIME_WARNING = new ErrorCode(1203, "SWITCHEDITRIGHTS_NEEDS_MORE_TIME_WARNING", "Switching edit rights needs more time", 1);
    public static final ErrorCode SWITCHEDITRIGHTS_NOT_POSSIBLE_NOT_EDITOR = new ErrorCode(1204, "SWITCHEDITRIGHTS_NOT_POSSIBLE_NOT_EDITOR", "Request cannot be processed as client has no edit rights.", 1);
    //-------------------------------------------------------------------------
    public static final ErrorCode BACKUPDOCUMENT_SERVICE_NOT_AVAILABLE = new ErrorCode(1300, "BACKUPDOCUMENT_SERVICE_NOT_AVAILABLE", "The document cannot be saved, because the backup service is not available.", 2);
    public static final ErrorCode BACKUPDOCUMENT_BASEDOCSTREAM_NOT_FOUND = new ErrorCode(1301, "BACKUPDOCUMENT_BASEDOCSTREAM_NOT_FOUND", "The document cannot be saved, because the backup document stream cannot be found.", 2);
    public static final ErrorCode BACKUPDOCUMENT_CLIENTACTIONS_MALFORMED = new ErrorCode(1302, "BACKUPDOCUMENT_CLIENTACTIONS_MALFORMED", "The actions provided by the client are malformed and cannot be correctly interpreted as JSONArray", 2);
    public static final ErrorCode BACKUPDOCUMENT_SYNC_NOT_POSSIBLE = new ErrorCode(1303, "BACKUPDOCUMENT_SYNC_NOT_POSSIBLE", "The client operations cannot be synced with the document actions. Create backup is not possible!", 2);
    public static final ErrorCode BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN = new ErrorCode(1304, "BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN", "The document could only be written by restore document.", 2);
    public static final ErrorCode BACKUPDOCUMENT_RESTORE_DOCUMENT_PARTIAL_WRITTEN = new ErrorCode(1305, "BACKUPDOCUMENT_RESTORE_DOCUMENT_PARTIAL_WRITTEN", "The document could only be partially restored.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode PRESENTER_MAX_PARTICIPANTS_FOR_PRESENTATION_REACHED_ERROR = new ErrorCode(1400, "PRESENTER_MAX_PARTICIPANTS_FOR_PRESENTATION_REACHED_ERROR", "The maximum number of participants for this presentation has been reached. Joining is not possible.", 2);
    public static final ErrorCode PRESENTER_PRESENTATION_IS_BEING_EDITED_ERROR = new ErrorCode(1401, "PRESENTER_PRESENTATION_IS_BEING_EDITED_ERROR", "The remote presentation cannot be started, because the document is currently being edited.", 2);
    public static final ErrorCode PRESENTER_REMOTE_PRESENTATION_ALREADY_RUNNING_ERROR = new ErrorCode(1402, "PRESENTER_REMOTE_PRESENTATION_ALREADY_RUNNING_ERROR", "The remote presentation cannot be started, because a remote presentation is currently being held.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode TOO_MANY_CONNECTIONS_ERROR = new ErrorCode(1500, "TOO_MANY_CONNECTIONS_ERROR", "Maxiumum count of connections reached.", 2);
    //-------------------------------------------------------------------------
    public static final ErrorCode ADVISORY_LOCK_SET_WARNING = new ErrorCode(2000, "ADVISORY_LOCK_SET_WARNING", "", 1);
    public static final ErrorCode ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR = new ErrorCode(2001, "ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR", "", 2);
    public static final ErrorCode ADVISORY_LOCK_COULDNT_PROCESS_ERROR = new ErrorCode(2002, "ADVISORY_LOCK_ENTRY_NOT_READABLE_ERROR", "", 2);

    // property names for JSON access
    public static final String JSON_ERRORCLASS       = "errorClass";
    public static final String JSON_ERRORCODE        = "errorCode";
    public static final String JSON_ERROR            = "error";
    public static final String JSON_ERRORDESCRIPTION = "errorDescription";
    public static final String JSON_ERRORVALUE       = "errorValue";

    private final int m_code;
    private final String m_codeAsStringConstant;
    private String m_description;
    private String m_value;

    // can be changed by client code to change severity dependent on the context
    private int m_errorClass;

    /**
     * Creates a ErrorCode instance with the code, a unique constant string for the
     * code and a description.
     *
     * @param code
     *  The error code.
     *
     * @param codeAsString
     *  The code as a unique constant string.
     *
     * @param description
     *  The description of the error.
     */
    public ErrorCode(int code, final String codeAsString, final String description, int errorClass) {
        this.m_code = code;
        this.m_codeAsStringConstant = codeAsString;
        this.m_description = description;
        this.m_errorClass = errorClass;
        this.m_value = null;
    }

    /**
     * Determines if this instance represents an error or not.
     *
     * @return
     *  TRUE if the instance represents an error and FALSE if not.
     */
    public boolean isError() {
        return (m_errorClass >= ERRORCLASS_ERROR);
    }

    /**
     * Determines if this instance represents no error, means
     * it could be a warning or no error at all.
     *
     * @return
     *  TRUE if this instance represents no error or a warning, otherwise
     *  FALSE is returned.
     */
    public boolean isNoError() {
        return (m_errorClass <= ERRORCLASS_WARNING);
    }

    /**
     * Determines if this instance represents a warning or not.
     *
     * @return
     *  TRUE if the instance represents a warning and FALSE if not.
     */
    public boolean isWarning() {
        return (m_errorClass == ERRORCLASS_WARNING);
    }

    /**
     * Provides a description of the error.
     *
     * @return
     *  The description.
     */
    public String getDescription() {
       return m_description;
    }

    /**
     * Overrides a default description.
     *
     * @param newDescription
     *  The new description string to be used.
     */
    public void setDescription(String newDescription) {
        m_description = newDescription;
    }

    /**
     * Provides a unique number which describes the error case.
     *
     * @return
     *  The error code.
     */
    public int getCode() {
       return m_code;
    }

    /**
     * Provides a unique constant string for the error case.
     * @return
     */
    public String getCodeAsStringConstant() {
        return m_codeAsStringConstant;
    }

    /**
     * Provides the error class of the error.
     *
     * @return
     *  The error class of the error, which describes how severe the
     *  error condition must be treated by the code.
     */
    public int getErrorClass() {
        return m_errorClass;
    }

    /**
     * Provides an optional value to the error code. This value must
     * be interpreted dependent on the specific case. For example the
     * value can be the illegal character which prevents a rename
     * operation for a file.
     *
     * @return
     *  The optional value for this error. Can be null if the value is
     *  not set.
     */
    public String getValue() {
        return m_value;
    }

    /**
     * Provides an optional value to the error code. This value must
     * be interpreted dependent on the specific case. For example the
     * value can be the illegal character which prevents a rename
     * operation for a file.
     *
     * @return
     *  The optional value for this error. Can be null if the value is
     *  not set.
     */
    public void setValue(final String value) {
        m_value = value;
    }

    /**
     * Creates a new error class instance which contains the same values
     * as this instance.
     */
    @Override
    public ErrorCode clone() {
        return new ErrorCode(this.m_code, this.m_codeAsStringConstant, this.m_description, this.m_errorClass);
    }

    @Override
    public int hashCode() {
        return m_code;
    }

    /**
     * @return true if both objects (this and the given one) are equals.
     * We use the error code only to check these ...
     */
    @Override
    public boolean equals (final Object otherObject) {
        if (otherObject == null)
            return false;

        if (getClass() != otherObject.getClass ())
            return false;

        final ErrorCode check = (ErrorCode) otherObject;
        return (check.m_code == m_code);
    }

    /**
     * Sets the error class of the error. Sometimes an error has a
     * different impact and therefore the error class must be adapted.
     * Use this method with care as it can have a deep impact on the
     * error processing.
     *
     * @param errorClass
     */
    public static ErrorCode setErrorClass(final ErrorCode errorCode, int errorClass) {
        ErrorCode clone = errorCode.clone();
        if ((errorClass >= 0) && (errorClass < ERRORCLASS_COUNT)) {
            clone.m_errorClass = errorClass;
        }
        return clone;
    }

    /**
     * Provides a string representation of the error code instance.
     *
     * @return
     *  The string representation of the error code instance.
     */
    @Override
    public String toString() {
      return m_code + ": " + m_codeAsStringConstant;
    }

    /**
     * Provides a JSONObject which contains the state of the error
     * code instance.
     *
     * @return
     *  A JSONObject which contains the state of the error code
     *  instance or an empty object in case of an error.
     */
    public JSONObject getAsJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(JSON_ERRORCLASS, this.getErrorClass());
            jsonObject.put(JSON_ERRORCODE, this.getCode());
            jsonObject.put(JSON_ERROR, this.getCodeAsStringConstant());
            jsonObject.put(JSON_ERRORDESCRIPTION, this.getDescription());
            if (m_value != null) {
                jsonObject.put(JSON_ERRORVALUE, this.getValue());
            }
        } catch (final JSONException e) {
            //
        }
        return jsonObject;
    }

    /**
     * Creates a JSONObject that can be directly used as a result object
     * for server answers.
     *
     * @return
     *  A JSONObject which can be used as a server answer or an empty
     *  object in case of an error.
     */
    public JSONObject getAsJSONResultObject() {
        JSONObject resultObject = new JSONObject();
        JSONObject errorObject = this.getAsJSON();
        try {
            resultObject.put(JSON_ERROR, errorObject);
        } catch (final JSONException e) {
            //
        }
        return resultObject;
    }

    /**
     * Extracts the error code from a JSON object that is the full representation
     * of a ErrorCode instance (created by getAsJSON()).
     *
     * @param jsonObject
     *  The JSON object where the error code shall be extracted.
     *
     * @param defaultValue
     *  The default value to be used if the error code cannot be extracted.
     *
     * @return
     *  The error code or the default value if the code couldn't be extracted
     *  from the JSONObject.
     */
    public static int getErrorCodeFromJSON(JSONObject jsonObject, int defaultValue) {
        int result = defaultValue;
        if (null != jsonObject) {
            result = jsonObject.optInt(JSON_ERRORCODE, defaultValue);
        }
        return result;
    }

    /**
     * Extracts the error constant string from a JSON object that contains the
     * full representation of a ErrorCode instance (created by getAsJSON()).
     *
     * @param jsonObject
     *  The JSON object where the error constant string shall be extracted.
     *
     * @param defaultValue
     *  The default value to be used if the error code cannot be extracted.
     *
     * @return
     *  The error constant string or the default value if the code couldn't
     *  be extracted from the JSONObject.
     */
    public static String getErrorConstantFromJSON(JSONObject jsonObject, String defaultValue) {
        String result = defaultValue;
        if (null != jsonObject) {
            result = jsonObject.optString(JSON_ERROR, defaultValue);
        }
        return result;
    }

    /**
     * Extracts the error description from a JSON object that contains the
     * full representation of a ErrorCode instance (created by getAsJSON()).
     *
     * @param jsonObject
     *  The JSON object where the error description string shall be extracted.
     *
     * @param defaultValue
     *  The default value to be used if the error description cannot be extracted.
     *
     * @return
     *  The error description or the default value if the string couldn't be
     *  extracted from the JSONObject.
     */
    public static String getErrorDescriptionFromJSON(JSONObject jsonObject, String defaultValue) {
        String result = defaultValue;
        if (null != jsonObject) {
            result = jsonObject.optString(JSON_ERRORDESCRIPTION, defaultValue);
        }
        return result;
    }

    /**
     * 
     * @param jsonErrorObject
     * @param aDefaultErrorCode
     * @return
     */
    public static ErrorCode createFromJSONObject(org.json.JSONObject jsonErrorObject, ErrorCode aDefaultErrorCode) {
        if (null == jsonErrorObject)
            return aDefaultErrorCode;

        final Integer nErrorClass = jsonErrorObject.optInt(JSON_ERRORCLASS);
        final Integer nErrorCode = jsonErrorObject.optInt(JSON_ERRORCODE);
        final String sCodeAsStrConst = jsonErrorObject.optString(JSON_ERROR);

        return ((null != nErrorClass) && (null != nErrorCode) && (StringUtils.isNotEmpty(sCodeAsStrConst))) ?
            new ErrorCode(nErrorCode, sCodeAsStrConst, "", nErrorClass) : aDefaultErrorCode;
    }

 }
