#!/bin/bash

ROOT=${WORKSPACE_HOME}/${WORKSPACE}/src/office/com.openexchange.office.rt2.perftest

set JAVA_OPTS="-Duser.language=en -Duser.country=US -Dfile.encoding=utf-8"

mvn -f ${ROOT}/pom.xml -B clean compile -P generate-soap-client
