/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.office.document.api.Document;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.documents.access.DocRequester;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.OXDocumentException;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.jms.OfficeJmsDestination;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.json.ObjectMapperWrapper;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@RegisteredService(registeredClass=DocRequester.class)
public class DocRequesterImpl implements DocRequester {

    // ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(DocRequesterImpl.class);

	@Autowired
	private DocumentRequestProcessor documentRequestProcessor;

	@Autowired
	private DocumentDisposer documentDisposer;

	@Autowired
	private JmsTemplateWithoutTtl jmsTemplate;

	@Autowired
	private ObjectMapperWrapper objectMapperWrapper;

    @Autowired
    private CorrelationIdDisposer correlationIdDisposer;

    @Autowired
    private ClusterService clusterService;

	//-------------------------------------------------------------------------
	@Override
    public CompletableFuture<InputStream> sendSyncRequest(RT2DocUidType docUid, RT2CliendUidType clientUid) throws OXException {

		DocRequestData docRequestData = new DocRequestData(docUid.getValue(), clientUid.getValue(), clusterService.getLocalMemberUuid());

        log.debug("RT2: sendSyncRequest {}", docRequestData.toString());

        ImmutablePair<String, CompletableFuture<InputStream>> p = correlationIdDisposer.createCorrelationId();
        String correlationID = p.getKey();
        CompletableFuture<InputStream> result = p.getValue();
        final Document aDoc = getLocalDocumentFor(docUid.getValue());
        if (null == aDoc) {
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUid.getValue());
            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUid.getValue());
        	try {
        		jmsTemplate.convertAndSend(OfficeJmsDestination.requestDocTopic, objectMapperWrapper.getObjectMapper().writeValueAsString(docRequestData), (msg) -> {
        			msg.setJMSCorrelationID(correlationID);
        			return msg;
        		});
	        } catch (JmsException | JsonProcessingException jmsEx) {
	            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_SYSTEM_BUSY_ERROR.getCodeAsStringConstant());
	        	log.error("sendSyncRequest-Exception", jmsEx);
                MDC.remove(MDCEntries.ERROR);
	        	throw new OXDocumentException("", ErrorCode.GENERAL_SYSTEM_BUSY_ERROR);
	        } finally {
	            MDC.remove(MDCEntries.DOC_UID);
	            MDC.remove(MDCEntries.CLIENT_UID);
	        }
        } else {
            ManagedFile managedFile = documentRequestProcessor.processDocRequest(docUid, clientUid);
            InputStream isSteam = managedFile.getInputStream();
            result.complete(isSteam);
        }
        return result;
    }

    //-------------------------------------------------------------------------
	private Document getLocalDocumentFor(String docUID) throws OXDocumentException {
        return documentDisposer.getDocument(docUID);
	}
}
