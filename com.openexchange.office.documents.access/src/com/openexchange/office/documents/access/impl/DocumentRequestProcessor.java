/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.office.document.api.Document;
import com.openexchange.office.document.api.DocumentDisposer;
import com.openexchange.office.document.api.FlushableDocument;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.LogHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.OXDocumentException;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class DocumentRequestProcessor {

    private static final Logger log = LoggerFactory.getLogger(DocumentRequestProcessor.class);

    @Autowired
    private DocumentDisposer documentDisposer;

    //-------------------------------------------------------------------------
	public ManagedFile processDocRequest(RT2DocUidType docUid, RT2CliendUidType clientUid) throws OXException {
		ManagedFile result = null;

		MDCHelper.safeMDCPut(MDCEntries.DOC_UID, LogHelper.getLoggableString(docUid.getValue()));
        MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, LogHelper.getLoggableString(clientUid.getValue()));

        try {
            result = handleFlushToManagedFile(docUid, clientUid);
        } catch (DocumentNotFoundException e) {
        	throw e;
        } catch (OXException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
            log.error(e.getMessage(), e);
            MDC.remove(MDCEntries.ERROR);
            throw new OXDocumentException("Exception caught while trying to process request", ErrorCode.GENERAL_UNKNOWN_ERROR);
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
        }

        return result;
	}

    //-------------------------------------------------------------------------
	private ManagedFile handleFlushToManagedFile(RT2DocUidType docUid, RT2CliendUidType clientUid) throws OXException, JSONException {
		ManagedFile result = null;

        final Document aDoc = documentDisposer.getDocument(docUid.getValue());
        final FlushableDocument aFlushableDoc = (aDoc instanceof FlushableDocument) ? (FlushableDocument)aDoc : null;

        if (null != aFlushableDoc) {
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, LogHelper.getLoggableString(docUid.getValue()));
            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, LogHelper.getLoggableString(clientUid.getValue()));

            try {
                final ManagedFile aManagedFile = aFlushableDoc.flushToManagedFile(clientUid);

                if (null != aManagedFile)
                    result = aManagedFile;
            }
            catch (Exception e) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
                log.error(e.getMessage(), e);
                MDC.remove(MDCEntries.ERROR);
                throw new OXDocumentException("Exception caught while trying to process flushToManagedFile", ErrorCode.GENERAL_UNKNOWN_ERROR);
            } finally {
                MDC.remove(MDCEntries.DOC_UID);
                MDC.remove(MDCEntries.CLIENT_UID);
            }
        } else {
        	throw new DocumentNotFoundException();
        }

        return result;
	}
}
