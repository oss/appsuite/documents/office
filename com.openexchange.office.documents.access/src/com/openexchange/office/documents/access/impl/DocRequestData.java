/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DocRequestData {
	private String docUid;
	private String clientUid;
	private String nodeUid;

	public DocRequestData() {
	}

	public DocRequestData(String docUid, String clientUid, String nodeUid) {
		this.docUid = docUid;
		this.clientUid = clientUid;
		this.nodeUid = nodeUid;
	}

	public String getDocUid() {
		return docUid;
	}

	public String getClientUid() {
		return clientUid;
	}

	public String getNodeUid() {
		return nodeUid;
	}

	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this); 
	}
}
