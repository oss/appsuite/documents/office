/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.tools.annotation.Async;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

@Service
@Async(initialDelay=1, period=3, timeUnit=TimeUnit.MINUTES)
@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=CorrelationIdDisposer")
public class CorrelationIdDisposer implements Runnable {

	private ConcurrentHashMap<String, CompletableFuture<InputStream>> correlationIds = new ConcurrentHashMap<>();

	@Override
	public void run() {
		Set<String> toRemove = new HashSet<>();
		correlationIds.forEach(3, (k, v) -> {
			if (v.isDone() && (v.getNumberOfDependents() == 0)) {
				toRemove.add(k);
			}
		});
		toRemove.forEach(s -> correlationIds.remove(s));
	}

	public ImmutablePair<String, CompletableFuture<InputStream>> createCorrelationId() {
		
		CompletableFuture<InputStream> completableFuture = new CompletableFuture<>();
		
		String correlationId = UUID.randomUUID().toString();
        correlationIds.put(correlationId, completableFuture);
        return ImmutablePair.of(correlationId, completableFuture);
	}
	
	public CompletableFuture<InputStream> getCompletableFutureForCorrelationId(String correlationId) {
		CompletableFuture<InputStream> completableFuture = correlationIds.get(correlationId);
		if (completableFuture != null) {
			return completableFuture;
		}
		return null;
	}
	
	public void removeCorrelationId(String correlationId) {
		correlationIds.remove(correlationId);
	}
	
	@JMXBeanAttribute
	public int getCountWaitingCompletableFutures() {
		return correlationIds.size();
	}
}
