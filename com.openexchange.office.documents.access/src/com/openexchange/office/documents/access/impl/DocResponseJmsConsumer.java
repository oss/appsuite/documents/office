/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.google.common.io.ByteSource;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.OfficeJmsDestination;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.cluster.ClusterService;

@Service
public class DocResponseJmsConsumer implements MessageListener, JmsMessageListener, DisposableBean {

	private static final Logger log = LoggerFactory.getLogger(DocResponseJmsConsumer.class);
	
    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;	
	
    @Autowired
    private CorrelationIdDisposer correlationIdDisposer;
    
    @Autowired
    private ClusterService clusterService;
    
	private DefaultMessageListenerContainer msgListenerCont;
	
    @Override
	public void destroy() throws Exception {
    	if (msgListenerCont != null) {
    		msgListenerCont.destroy();
    	}
	}

	@Override
	public void startReceiveMessages() {
    	if (msgListenerCont == null) {
            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);            
            msgListenerCont.setConnectionFactory(pooledConnectionFactoryProxy.getPooledConnectionFactory());
            msgListenerCont.setConcurrentConsumers(1);
            msgListenerCont.setDestination(OfficeJmsDestination.getResponseDocQueue(clusterService.getLocalMemberUuid()));
            msgListenerCont.setMaxConcurrentConsumers(1);
            msgListenerCont.setPubSubDomain(true);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("DocRequesterImpl-%d").build()));
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.start();
    	}
	}

	@Override
	public void onMessage(Message message) {
		CompletableFuture<InputStream> completableFuture = null;
		try {
			String correlationId = message.getJMSCorrelationID();
			completableFuture = correlationIdDisposer.getCompletableFutureForCorrelationId(correlationId);
			if (completableFuture != null) {
				correlationIdDisposer.removeCorrelationId(correlationId);
				BytesMessage bytesMessage = (BytesMessage) message;
				byte[] data = new byte[(int) bytesMessage.getBodyLength()];
				bytesMessage.readBytes(data);
				InputStream inputStream = ByteSource.wrap(data).openStream();
				completableFuture.complete(inputStream);
			}
		} catch (JMSException | IOException ex) {
			log.error(ex.getMessage(), ex);
			if (completableFuture != null) {
				completableFuture.completeExceptionally(ex);
			}
		}
	}	
}
