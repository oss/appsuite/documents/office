/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.documents.access.impl;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;
import org.springframework.util.backoff.ExponentialBackOff;

import com.google.common.io.ByteStreams;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.office.tools.jms.JmsTemplateWithoutTtl;
import com.openexchange.office.tools.jms.OfficeJmsDestination;
import com.openexchange.office.tools.jms.PooledConnectionFactoryProxy;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.json.ObjectMapperWrapper;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
public class DocRequestJmsConsumer implements MessageListener, JmsMessageListener, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(DocRequestJmsConsumer.class);

    @Autowired
    private PooledConnectionFactoryProxy pooledConnectionFactoryProxy;

    @Autowired
    private ObjectMapperWrapper objectMapperWrapper;

    @Autowired
    private DocumentRequestProcessor documentRequestProcessor;

	@Autowired
	private JmsTemplateWithoutTtl jmsTemplate;

	@Autowired
	private ClusterService clusterService;

	//------------------------------------------------------------------------
    private DefaultMessageListenerContainer msgListenerCont;

	//------------------------------------------------------------------------
	@Override
	public void startReceiveMessages() {
    	if (msgListenerCont == null) {
            msgListenerCont = new DefaultMessageListenerContainer();
            ExponentialBackOff exponentialBackOff = new ExponentialBackOff();
            exponentialBackOff.setMaxInterval(60000);
            msgListenerCont.setBackOff(exponentialBackOff);
            msgListenerCont.setConnectionFactory(pooledConnectionFactoryProxy.getPooledConnectionFactory());
            msgListenerCont.setConcurrentConsumers(1);
            msgListenerCont.setDestination(OfficeJmsDestination.requestDocTopic);
            msgListenerCont.setMaxConcurrentConsumers(1);
            msgListenerCont.setPubSubDomain(true);
            msgListenerCont.setAutoStartup(true);
            msgListenerCont.setupMessageListener(this);
            msgListenerCont.setTaskExecutor(new SimpleAsyncTaskExecutor(new ThreadFactoryBuilder("DocRequestJmsConsumer-%d").build()));
            msgListenerCont.afterPropertiesSet();
            msgListenerCont.start();
    	}
	}

	//------------------------------------------------------------------------
	@Override
	public void destroy() throws Exception {
        if (msgListenerCont != null) {
            msgListenerCont.destroy();
        }
	}

	//------------------------------------------------------------------------
    @Override
    public void onMessage(Message jmsMsg) {
        try {
        	TextMessage txtJmsMsg = (TextMessage) jmsMsg;
        	DocRequestData docRequestData = objectMapperWrapper.getObjectMapper().readValue(txtJmsMsg.getText(), DocRequestData.class);
        	MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docRequestData.getDocUid());
        	MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, docRequestData.getClientUid());
        	MDCHelper.safeMDCPut(MDCEntries.BACKEND_PART, "docRequest");
        	try {
	        	ManagedFile managedFile = documentRequestProcessor.processDocRequest(new RT2DocUidType(docRequestData.getDocUid()), new RT2CliendUidType(docRequestData.getClientUid()));
	        	ActiveMQQueue queue = OfficeJmsDestination.getResponseDocQueue(docRequestData.getNodeUid());
	        	jmsTemplate.send(queue, new MessageCreator() {
	        		@SuppressWarnings("synthetic-access")
                    @Override
                    public Message createMessage(Session session) throws JMSException {
	        			BytesMessage bytesMessage = session.createBytesMessage();
	        			bytesMessage.setJMSCorrelationID(jmsMsg.getJMSCorrelationID());
	        			try {
	        				bytesMessage.writeBytes(ByteStreams.toByteArray(managedFile.getInputStream()));
	        			} catch (Exception e) {
	        				log.error(e.getMessage(), e);
	        			}
	        			return bytesMessage;
	        	    }
	        	});
        	} catch (DocumentNotFoundException ex) {
        		log.debug("Document with com.openexchange.rt2.document.uid {} not found on node with com.openexchange.rt2.backend.uid {}", docRequestData.getDocUid(), clusterService.getLocalMemberUuid());
        	}
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
        } finally {
        	MDC.remove(MDCEntries.DOC_UID);
			MDC.remove(MDCEntries.CLIENT_UID);
			MDC.remove(MDCEntries.BACKEND_PART);
        }
    }
}
