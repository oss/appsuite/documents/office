/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

public class StreamReaderPerfTest {

	private OsgiBundleContextAndActivator osgiBundleContextAndActivator = Mockito.mock(OsgiBundleContextAndActivator.class);

    private com.openexchange.office.filter.ooxml.docx.Importer docxImporter;
    private com.openexchange.office.filter.ooxml.pptx.Importer pptxImporter;
    private com.openexchange.office.filter.ooxml.xlsx.Importer xlsxImporter;

    @BeforeEach
    public void init() {
    	docxImporter = new com.openexchange.office.filter.ooxml.docx.Importer();
    	docxImporter.setApplicationContext(osgiBundleContextAndActivator);
    	pptxImporter = new com.openexchange.office.filter.ooxml.pptx.Importer();
    	pptxImporter.setApplicationContext(osgiBundleContextAndActivator);
    	xlsxImporter = new com.openexchange.office.filter.ooxml.xlsx.Importer();
    	xlsxImporter.setApplicationContext(osgiBundleContextAndActivator);


    }

//    @Test
	public void test() {
        try {
            final String spaces = "                                                                      ";
            final ByteArrayInputStream isDocx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/ooxml/test/simpleFieldTest.docx")));
            perfTest(0, 1, "docx", isDocx);
            final ByteArrayInputStream isXlsx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/ooxml/test/formula_import_xml_12.xlsx")));
            perfTest(0, 1, "xlsx", isXlsx);
            final ByteArrayInputStream isPptx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/ooxml/test/clipboardTable.pptx")));
            perfTest(0, 1, "pptx", isPptx);

            int times = 500;

            int t0 = perfTest(1, times, "docx", isDocx);
            int t1 = perfTest(0, times, "docx", isDocx);
            int t2 = perfTest(1, times, "xlsx", isXlsx);
            int t3 = perfTest(0, times, "xlsx", isXlsx);
            int t4 = perfTest(1, times, "pptx", isPptx);
            int t5 = perfTest(0, times, "pptx", isPptx);
            System.out.println("----------------------------------------------------------------------");
            StringBuffer buf = new StringBuffer(spaces);
            buf.replace(0, 9, Integer.valueOf(times).toString() + "times");
            buf.replace(10, 30, "documentBuilder (ms)");
            buf.replace(31, 40, "XMLStreamReader (ms)");
            System.out.println(buf.toString());
            buf = new StringBuffer(spaces);
            buf.replace(0, 9, "docx");
            buf.replace(10, 30, Integer.toString(t0));
            buf.replace(31, 40, Integer.toString(t1));
            System.out.println(buf.toString());
            buf = new StringBuffer(spaces);
            buf.replace(0, 9, "xlsx");
            buf.replace(10, 30, Integer.toString(t2));
            buf.replace(31, 40, Integer.toString(t3));
            System.out.println(buf.toString());
            buf = new StringBuffer(spaces);
            buf.replace(0, 9, "pptx");
            buf.replace(10, 30, Integer.toString(t4));
            buf.replace(31, 40, Integer.toString(t5));
            System.out.println(buf.toString());
            System.out.println("----------------------------------------------------------------------");
            System.out.flush();
        }
        catch(Exception e) {
        	e.printStackTrace();
            fail(e.getMessage());
        }
	}

	private int perfTest(int mode, int repeat, String type, ByteArrayInputStream is) throws FilterException, IOException {

	    long cur = System.currentTimeMillis();
	    Tools.junitTestParam = mode;

	    for(int i=0; i<repeat; i++) {
	        is.reset();
        	if(type.equals("docx")) {
               final JSONObject operations = docxImporter.createOperations(null, is, new DocumentProperties(), false);
        	}
        	else if(type.equals("pptx")) {
                final JSONObject operations = pptxImporter.createOperations(null, is, new DocumentProperties(), false);
        	}
            else if(type.equals("xlsx")) {
                final JSONObject operations = xlsxImporter.createOperations(null, is, new DocumentProperties(), false);
            }
    	}
	    return (int)(System.currentTimeMillis()-cur);
	}
}
