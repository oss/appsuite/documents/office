/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.docx4j.wml.CTSimpleField;
import org.docx4j.wml.P;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.ParagraphComponent;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class SimleFieldConversionTest {

    final Session session = null;
    final IResourceManager resourceManager = null;
    final DocumentProperties documentProperties = new DocumentProperties();

    @Test
    public void test_simpleField_conversion() {

        final String sourceFile = "test/com/openexchange/office/filter/ooxml/test/simpleFieldTest.docx";

        try {

            // loading the source test document containing 2 simple fields
            final DocxOperationDocument operationDocument = new DocxOperationDocument(session, resourceManager, documentProperties);
            operationDocument.loadDocument(new FileInputStream(new File(sourceFile)), false);

            if(countSimpleFields(operationDocument)!=2) {
            	fail("simple Field count is wrong");
            }

            // now we are applying bold attributes to the second paragraph -> this field can't be saved as simple field anymore
            final String applyOperations = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[1,0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CHARACTER.value() + "\":{\"bold\":true}},\"" + OCKey.END.value() + "\":[1,23],\"osn\":30,\"opl\":1}]";
            operationDocument.applyOperations(applyOperations);
            final InputStream savedDocument = operationDocument.save();

            // now we are loading the previously exported document and count again...
            final DocxOperationDocument exportedDocument = new DocxOperationDocument(session, resourceManager, documentProperties);
            exportedDocument.loadDocument(savedDocument, false);

            // the second was changed into a complex field by applying bold attributes, though a count of one is correct
            if(countSimpleFields(exportedDocument)!=1) {
            	fail("simle Field count is wrong after exporting");
            }
        }
        catch(Exception e) {
            fail(e.getMessage());
        }
    }

    private static int countSimpleFields(DocxOperationDocument operationDocument) {
    	int count = 0;

    	final OfficeOpenXMLComponent rootComponent = operationDocument.getRootComponent();
    	IComponent<OfficeOpenXMLOperationDocument> childComponent = rootComponent.getChildComponent(0);
    	while (childComponent!=null) {
    		if(childComponent instanceof ParagraphComponent) {

    			final P p = (P)childComponent.getObject();

    			// getRawContent returns content of paragraph without field conversion
    			final DLList<Object> DLList = p.getRawContent();
    			if(DLList!=null) {
    				for(Object o:DLList) {
    					if(o instanceof CTSimpleField) {
    						count++;
    					}
    				}
    			}
    		}
    		childComponent = childComponent.getNextComponent();
    	}
		return count;
    }
}
