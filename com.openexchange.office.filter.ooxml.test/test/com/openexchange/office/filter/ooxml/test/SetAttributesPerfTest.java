/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.htmldoc.TextHtmlDocumentBuilder;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class SetAttributesPerfTest {

    //@Test
    public void test() throws Exception {

        perfTest(true, true, 1); // ensure that static initialization of the filter is finished

        int times = 100;

        final Triple<Integer, Integer, Integer> normalT0 = perfTest(true, false, times);
        final Triple<Integer, Integer, Integer> normalT1 = perfTest(false, false, times);

        final Triple<Integer, Integer, Integer> withFastloadT0 = perfTest(true, true, times);
        final Triple<Integer, Integer, Integer> withFastloadT1 = perfTest(false, true, times);

        final String spaces = "                                                                                                                             ";
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------");
        StringBuffer buf = new StringBuffer(spaces);
        buf.replace(0, 9, Integer.valueOf(times).toString() + " times");
        buf.replace(10, 39, "old (operations, time, size)");
        buf.replace(40, 69, "new without setAttributes (operations, time, size)");
        System.out.println(buf.toString());

        buf = new StringBuffer(spaces);
        buf.replace(0, 9, "normal");
        buf.replace(10, 19, normalT0.getLeft().toString());
        buf.replace(20, 29, normalT0.getMiddle().toString());
        buf.replace(30, 39, normalT0.getRight().toString());
        buf.replace(40, 49, normalT1.getLeft().toString());
        buf.replace(50, 59, normalT1.getMiddle().toString());
        buf.replace(60, 69, normalT1.getRight().toString());
        System.out.println(buf.toString());

        buf = new StringBuffer(spaces);
        buf.replace(0, 9, "fastload");
        buf.replace(10, 19, withFastloadT0.getLeft().toString());
        buf.replace(20, 29, withFastloadT0.getMiddle().toString());
        buf.replace(30, 39, withFastloadT0.getRight().toString());
        buf.replace(40, 49, withFastloadT1.getLeft().toString());
        buf.replace(50, 59, withFastloadT1.getMiddle().toString());
        buf.replace(60, 69, withFastloadT1.getRight().toString());
        System.out.println(buf.toString());
        System.out.println("----------------------------------------------------------------------");
        System.out.flush();
    }

    private static Triple<Integer, Integer, Integer> perfTest(boolean createSetAttributesOperation, boolean useFastload, int repeat) throws Exception {

        final Session session = null;
        final IResourceManager resourceManager = null;
        final DocumentProperties documentProperties = new DocumentProperties();

        final ByteArrayInputStream isDocx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/ooxml/test/test.docx")));
        int operations = 0;
        int size = 0;
        long cur = System.currentTimeMillis();

        for(int i=0; i < repeat; i++) {
            isDocx.reset();
            final DocxOperationDocument operationDocument = new DocxOperationDocument(session, resourceManager, documentProperties);
            operationDocument.setCreateFastLoadOperations(createSetAttributesOperation);
            operationDocument.loadDocument(isDocx, false);
            final JSONObject ops = operationDocument.getOperations();
            operations = ops.getJSONArray("operations").length();
            if(useFastload) {
                size = TextHtmlDocumentBuilder.buildHtmlDocument(ops, "id", "folderId", "fileVersion", "fileName", "drive", null).length();
            }
            else {
                size = ops.toString().length();
            }
            operationDocument.close();
        }
        return Triple.of(Integer.valueOf(operations), Integer.valueOf((int)(System.currentTimeMillis()-cur)), Integer.valueOf(size));
    }
}
