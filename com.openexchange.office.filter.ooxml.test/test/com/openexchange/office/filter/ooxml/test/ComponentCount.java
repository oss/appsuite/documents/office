/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.MultiComponent;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.docx.DocxOperationDocument;
import com.openexchange.office.filter.ooxml.docx.components.HyperlinkContext;
import com.openexchange.office.filter.ooxml.docx.components.RunDelContext;
import com.openexchange.office.filter.ooxml.docx.components.RunInsContext;
import com.openexchange.office.filter.ooxml.docx.components.TextComponent;
import com.openexchange.office.filter.ooxml.drawingml.DMLGeometry;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class ComponentCount {

    final Session session = null;
    final IResourceManager resourceManager = null;
    final DocumentProperties documentProperties = new DocumentProperties();

    private class ComponentInfo {

        private class ComponentData {

            final Integer[]   parentPosition;
            final IComponent<OfficeOpenXMLOperationDocument>   component;

            ComponentData(IComponent<OfficeOpenXMLOperationDocument> c, Integer[] p) {
                parentPosition = p;
                component = c;
            }
        };
        final private List<ComponentData> components = new ArrayList<ComponentData>();

        public ComponentInfo(IComponent<OfficeOpenXMLOperationDocument> rootComponent) {
            getComponentInfo(rootComponent.getChildComponent(0), new Integer[0]);
		}

		public void getComponentHash(OutputStream outputStream) {
            final PrintWriter printWriter = outputStream!=null ? new PrintWriter(outputStream) : null;;
            for(ComponentData componentData:components) {
                final IComponent<OfficeOpenXMLOperationDocument> component = componentData.component;
                writeComponentInfo(printWriter, component, componentData.parentPosition, false);
                if(component instanceof MultiComponent) {
                    final List<OfficeOpenXMLComponent> multiComponentList = ((MultiComponent)component).getComponentList();
                    for(OfficeOpenXMLComponent mc:multiComponentList) {
                        writeComponentInfo(printWriter, mc, componentData.parentPosition, true);
                    }
                }
            }
            printWriter.flush();
            IOUtils.closeQuietly(printWriter);
        }

        private void writeComponentInfo(PrintWriter printWriter, IComponent<OfficeOpenXMLOperationDocument> component, Integer[] parentPosition, boolean multiComponentChild) {
            final String newline = System.getProperty("line.separator");

            final int componentLength = component instanceof MultiComponent ? 1 : component.getNextComponentNumber()-component.getComponentNumber();
            for(int i=0; i<componentLength; i++) {
                final StringBuffer v = new StringBuffer();
                for(int j = 0; j < parentPosition.length; j++) {
                    v.append(parentPosition[j].toString() + " ");
                }
                v.append(Integer.toString(component.getComponentNumber()+i) + " ");
                if(multiComponentChild) {
                    v.append("mc-child ");
                }
                v.append(component.getClass().getSimpleName().toString() + " ");
                if(component instanceof TextComponent) {
                    v.append("(" + ((org.docx4j.wml.IText)(component.getObject())).getValue().charAt(i) + ") ");
                }
                if(component instanceof IShapeType) {
                    v.append(((IShapeType)component).getType().toString() + " ");
                }
                ComponentContext<OfficeOpenXMLOperationDocument> parentContext = component.getParentContext();
                while(parentContext!=null&&!(parentContext instanceof OfficeOpenXMLComponent)) {
                    if(parentContext instanceof RunInsContext) {
                        v.append("inserted ");
                    }
                    else if(parentContext instanceof RunDelContext) {
                        v.append("deleted ");
                    }
                    else if(parentContext instanceof HyperlinkContext) {
                        v.append("hyperlink ");
                    }
                    parentContext = parentContext.getParentContext();
                }
                if(printWriter!=null) {
                    printWriter.write(v.toString() + newline);
                }
            }
        }

        private void getComponentInfo(IComponent<OfficeOpenXMLOperationDocument> component, Integer[] parent) {
            while(component!=null) {
                final IComponent<OfficeOpenXMLOperationDocument> childComponent = component.getChildComponent(0);
                components.add(new ComponentData(component, parent));
                if(childComponent!=null) {
                    final Integer[] newParent = new Integer[parent.length+1];
                    System.arraycopy(parent, 0, newParent, 0, parent.length);
                    newParent[newParent.length-1] = component.getComponentNumber();
                    getComponentInfo(childComponent, newParent);
                }
                component = component.getNextComponent();
            }
        }
    }

    @Test
    public void test_component_parsing() throws Exception {

        final boolean createSourceHashFile = false;

        final String sourceFile = "test/com/openexchange/office/filter/ooxml/test/test.docx";
        final String sourceHash = "test/com/openexchange/office/filter/ooxml/test/test.hash";
        final String destTempHash = "/tmp/test.hash";

        try {
            final DocxOperationDocument operationDocument = new DocxOperationDocument(session, resourceManager, documentProperties);
            operationDocument.loadDocument(new FileInputStream(new File(sourceFile)), false);

            final ComponentInfo componentInfo = new ComponentInfo(operationDocument.getRootComponent());

            OutputStream outputStream;
            if(createSourceHashFile) {
                outputStream = new FileOutputStream(new File(sourceHash));
            }
            else {
                outputStream = new FileOutputStream(new File(destTempHash));
            }

            try {
				final JSONObject presets = DMLGeometry.createJSPresetGeometries(true);
				Writer writer = new FileWriter("/tmp/preset.js");
				presets.write(writer);
				writer.close();
        	} catch (IOException e) {
        	    //
			} catch (JSONException e1) {
			    //
			}

            componentInfo.getComponentHash(outputStream);
            outputStream.flush();
            IOUtils.closeQuietly(outputStream);
            if(!createSourceHashFile) {
                ByteArrayInputStream in1 = null;
                ByteArrayInputStream in2 = null;
                try {
                    in1 = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(sourceHash)));
                    in2 = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(destTempHash)));
                    if(in1.available()!=in2.available()) {
                        fail("docx, test_components, compare files have different length");
                    }
                    else {
                        while(in1.available()>0) {
                            int v = in1.read();
                            if(v!=in2.read()) {
                                fail("docx, test_components, comparision error");
                                break;
                            }
                        }
                    }
                } catch (IOException e) {
                    fail("docx, test_components, compare failed");
                }
                finally {
                    IOUtils.closeQuietly(in1);
                    IOUtils.closeQuietly(in2);
                }
            }
        }
        catch(FilterException e) {
            fail(e.getMessage());
        }
        catch(FileNotFoundException e) {
            fail("file not found");
        }
    }
}
