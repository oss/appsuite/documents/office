/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.InputStream;
import java.util.List;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.pptx4j.pml.Presentation.SldMasterIdLst;
import org.pptx4j.pml.Presentation.SldMasterIdLst.SldMasterId;
import org.pptx4j.pml.SldLayout;
import org.pptx4j.pml.SlideLayoutIdList;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.Importer;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.templatemgr.ResourceProvider;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

public class PptxLayoutName {

	private OsgiBundleContextAndActivator osgiBundleContextAndActivator = Mockito.mock(OsgiBundleContextAndActivator.class);

	@Test
	public void test() {
        try {
        	assertTrue("Two Content".equals(getLayoutName(getDefaultDocument("en-US"), "twoObj")), "en-US layoutName does not match");
        	assertTrue("Nur Titel".equals(getLayoutName(getDefaultDocument("de"), "titleOnly")), "de layoutName does not match");
        	assertTrue("タイトル付きの図".equals(getLayoutName(getDefaultDocument("ja"), "picTx")), "ja layoutName does not match");
        }
        catch(FilterException e) {
        	e.printStackTrace();
            fail(e.getMessage());
        }
	}

	private InputStream getDefaultDocument(String language) {

		final Importer pptxImporter = new Importer();
		pptxImporter.setApplicationContext(osgiBundleContextAndActivator);

    	final DocumentProperties documentProperties = new DocumentProperties();
    	documentProperties.put(DocumentProperties.PROP_USER_LANGUAGE, language);

    	final ResourceProvider resourceProvider = new ResourceProvider();
    	final InputStream templateDocument = resourceProvider.getResource(resourceProvider.getEntry("presentation", "template", "Default"));


    	return pptxImporter.getDefaultDocument(templateDocument, documentProperties);
	}

	private String getLayoutName(InputStream in, String layoutType)
	    throws FilterException {

	    try(PptxOperationDocument operationDocument = new PptxOperationDocument(null, null, new DocumentProperties())) {
    	    operationDocument.loadDocument(in, false);

    		// set master & layout names
    		final MainPresentationPart mainPresentationPart = operationDocument.getPackage().getMainPresentationPart();
    		final SldMasterIdLst slideMasterIds = mainPresentationPart.getJaxbElement().getSldMasterIdLst();
    		if(slideMasterIds!=null) {
    			final List<SldMasterId> slideMasterIdList = slideMasterIds.getSldMasterId();
    			final SldMasterId slideMasterId = slideMasterIdList.get(0);
    			final SlideMasterPart slideMasterPart = (SlideMasterPart)mainPresentationPart.getRelationshipsPart().getPart(slideMasterId.getRid());

    			// retrieving layouts
    			final SlideLayoutIdList slideLayoutIds = slideMasterPart.getJaxbElement().getSldLayoutIdLst(false);
    			if(slideLayoutIds!=null) {
    				final RelationshipsPart masterRelationships = slideMasterPart.getRelationshipsPart();
    				final List<SldLayoutId> slideLayoutIdList = slideLayoutIds.getSldLayoutId();
    				for(SldLayoutId layoutId:slideLayoutIdList) {
    					final SldLayout slideLayout = ((SlideLayoutPart)masterRelationships.getPart(masterRelationships.getRelationshipByID(layoutId.getRid()))).getJaxbElement();
    					if(layoutType.equals(slideLayout.getType().value())) {
    						return slideLayout.getCSld().getName();
    					}
    				}
    			}
    		}
	    }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
        return "";
    }
}
