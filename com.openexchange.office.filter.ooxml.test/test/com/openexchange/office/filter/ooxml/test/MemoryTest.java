/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.FilterException;

public class MemoryTest {

    final static com.openexchange.office.filter.ooxml.docx.Importer docxImporter = new com.openexchange.office.filter.ooxml.docx.Importer();
    final static com.openexchange.office.filter.ooxml.pptx.Importer pptxImporter = new com.openexchange.office.filter.ooxml.pptx.Importer();
    final static com.openexchange.office.filter.ooxml.xlsx.Importer xlsxImporter = new com.openexchange.office.filter.ooxml.xlsx.Importer();

    @Test
    @Disabled
	public void test() {
        try {
            final String spaces = "                                                                      ";
            final ByteArrayInputStream isPptx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/ooxml/test/clipboardTable.pptx")));
            perfTest(1, "pptx", isPptx);

            int times = 10;

            int t1 = perfTest(times, "pptx", isPptx);
            System.out.println("----------------------------------------------------------------------");
            StringBuffer buf = new StringBuffer(spaces);
            buf = new StringBuffer(spaces);
            buf.replace(0, 9, "pptx");
            buf.replace(10, 30, Integer.toString(t1));
            System.out.println(buf.toString());
            System.out.println("----------------------------------------------------------------------");
            System.out.flush();
        }
        catch(Exception e) {
        	e.printStackTrace();
            fail(e.getMessage());
        }
	}

	private int perfTest(int repeat, String type, ByteArrayInputStream is) throws FilterException, IOException {

//	    long cur = System.currentTimeMillis();
//
//	    for(int i=0; i<repeat; i++) {
//	        is.reset();
//        	if(type.equals("docx")) {
//               final JSONObject operations = docxImporter.createOperations(null, is, new DocumentProperties());
//        	}
//        	else if(type.equals("pptx")) {
//                final JSONObject operations = pptxImporter.createOperations(null, is, new DocumentProperties());
//        	}
//            else if(type.equals("xlsx")) {
//                final JSONObject operations = xlsxImporter.createOperations(null, is, new DocumentProperties());
//            }
//    	}
//	    return (int)(System.currentTimeMillis()-cur);
		return 0;
	}
}
