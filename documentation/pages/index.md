---
title: Documents documenation
classes: no-counting
---

# General Information

OX Documents are browser based, cloud ready, text, spreadsheet and presentation products that can work with Microsoft Office and OpenOffice documents in a lossless way. And you can also collaborate with other people to edit shared documents on various devices. 

For compatibility with the legacy MS binary format files (.DOC, .XLS, .PPT) and for printing functionality in OX Text the document converter components (incl. readerengine) are required. 