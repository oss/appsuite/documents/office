/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.monitoring;

import com.openexchange.office.tools.doc.DocumentType;



/**
 * {@link OperationEvent}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class OperationsEvent extends DocumentEvent {

    public OperationsEvent(DocumentType documentType, OperationsType operationType) {
        super(documentType, EventType.OPERATIONS);

        m_operationType = operationType;
        m_actionCount = 1;
    }

    /**
     * Initializes a new {@link OperationsEvent}.
     * @param documentType
     * @param operationType
     * @param operationCount
     */
    public OperationsEvent(DocumentType documentType, OperationsType operationType, long actionCount) {
        super(documentType, EventType.OPERATIONS);

        m_operationType = operationType;
        m_actionCount = actionCount;
    }

    /**
     * @return The set OperationType
     */
    public OperationsType getOperationType() {
        return m_operationType;
    }

    /**
     * @return The count of operations of the specified type
     */
    public long getActionCount() {
        return m_actionCount;
    }

    // - Members ----------------------------------------------------------------------------

    private OperationsType m_operationType = OperationsType.NONE;

    private long m_actionCount = 0;
}
