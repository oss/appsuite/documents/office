/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.monitoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.office.document.api.AdvisoryLockInfoService;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.doc.DocumentType;

/**
 * {@link Statistics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@Service
@RegisteredService
public class Statistics implements InitializingBean, DisposableBean {

    // set period of reset task to 5min, which equals to the munin standard period
    private final long RESET_TIMER_PERIOD_MILLISECONDS = 5 * 60 * 1000;

    // set ahead time to periodically retrieve results this time
    // span before the next external and periodic retrieval
    private final long RESET_TIMER_AHEAD_TIME = 30 * 1000;

    @Autowired
    protected ConfigurationService configurationService;

    @Autowired
    protected AdvisoryLockInfoService advisoryLockInfoService;

    /**
     * Logger
     */
    protected static final Logger LOG = LoggerFactory.getLogger(Statistics.class);

	@Override
	public void afterPropertiesSet() throws Exception {
        for (DocumentType documentType : DocumentType.values()) {
            // correctly initialize all maps and sub maps
            m_documentsCreated.put(documentType, new AtomicLong());
            m_documentsErrors.put(documentType, new AtomicLong());

            // OpenType
            final EnumMap<OpenType, AtomicLong> openMap = new EnumMap<>(OpenType.class);
            m_documentsOpened.put(documentType, openMap);

            for (OpenType curOpenType : OpenType.values()) {
                openMap.put(curOpenType, new AtomicLong());
            }

            // OperationsType
            final EnumMap<OperationsType, AtomicLong> operationsMap = new EnumMap<>(OperationsType.class);
            m_documentsOperations.put(documentType, operationsMap);

            for (OperationsType curOperationsType : OperationsType.values()) {
                operationsMap.put(curOperationsType, new AtomicLong());
            }

            // SaveType
            final EnumMap<SaveType, AtomicLong> saveMap = new EnumMap<>(SaveType.class);
            m_documentsSaved.put(documentType, saveMap);

            for (SaveType curSaveType : SaveType.values()) {
                saveMap.put(curSaveType, new AtomicLong());
            }

            // CloseType
            final EnumMap<CloseType, AtomicLong> closeMap = new EnumMap<>(CloseType.class);
            m_documentsClosed.put(documentType, closeMap);

            for (CloseType curCloseType : CloseType.values()) {
                closeMap.put(curCloseType, new AtomicLong());
            }

            // initialize periodic members

            // documentsSizes
            m_documentsSizes.put(documentType, Collections.synchronizedList(new ArrayList<Long>()));
            m_documentsSizesResult.put(documentType, new Long(0));

            // otTransformationCounts
            m_otTransformationCounts.put(documentType, new AtomicLong());
            m_otUnresolvableTransformationCounts.put(documentType, new AtomicLong());
            m_otUnresolvableTransformationReloadCounts.put(documentType, new AtomicLong());
        }

        // Restore Document
        for (RestoreDocEventType curRestoreDocEventType : RestoreDocEventType.values()) {
            m_restoreDocEvents.put(curRestoreDocEventType, new AtomicLong());
        }

        // Background Save
        for (BackgroundSaveEventType cur : BackgroundSaveEventType.values()) {
            m_fssDocEvents.put(cur, new AtomicLong());
        }
    }

    /**
     * @param event
     */
    public synchronized void handleDocumentEvent(DocumentEvent event) {
    	implHandleDocumentEvent(event);
    }

    public synchronized void handleRestoreDocEvent(RestoreDocEvent event) {
    	implHandleRestoreDocEvent(event);
    }

    public synchronized void handleBackgroundSaveDocEvent(BackgroundSaveEvent event) {
    	implHandleBackgroundSaveDocEvent(event);
    }

    public synchronized void handlePresenterEvent(PresenterEvent event) {
    	implHandlePresenterEvent(event);
    }

    public synchronized void handleTransformation(OTEvent event) {
    	implHandleTransformation(event);
    }

    @Override
	public void destroy() throws Exception {
        m_resetTimer.cancel();
        m_resetTimer.purge();
        if (null != m_resetTask) {
            m_resetTask.cancel();
        }
	}

    /**
     * @return
     */
    public synchronized long getDocumentsCreated(com.openexchange.office.tools.doc.DocumentType documentType) {
        long ret = 0;

        if (null == documentType) {
            for (DocumentType curDocumentType : DocumentType.values()) {
                ret += m_documentsCreated.get(curDocumentType).get();
            }
        } else {
            ret = m_documentsCreated.get(documentType).get();
        }

        return ret;
    }

    /**
     * @return
     */
    public synchronized String getAdvisoryLockInfoMode() {
        return advisoryLockInfoService.getAdvisoryLockMode().toString();
    }

    /**
     * @return
     */
    public synchronized long getDocumentsOpened(DocumentType documentType, OpenType openType) {
        return getDocumentsCountValue(documentType, m_documentsOpened, openType);
    }

    /**
     * @return
     */
    public synchronized long getDocumentsOperations(DocumentType documentType, OperationsType operationType) {
        return getDocumentsCountValue(documentType, m_documentsOperations, operationType);
    }

    /**
     * @return
     */
    public synchronized long getDocumentsSaved(DocumentType documentType, SaveType saveType) {
        return getDocumentsCountValue(documentType, m_documentsSaved, saveType);
    }

    /**
     * @return
     */
    public synchronized long getDocumentsClosed(DocumentType documentType, CloseType closeType) {
        return getDocumentsCountValue(documentType, m_documentsClosed, closeType);
    }

    /**
     * @return
     */
    public synchronized long getDocumentsSizeMedian(DocumentType documentType) {
        initPeriodicResetTimer();

        return ((null == documentType) ? m_documentsSizesResultTotal.get() : m_documentsSizesResult.get(documentType));
    }

    public synchronized long getDocumentsStarted_RemotePresenterTotal() {
        return m_documentsStartedRemotePresenterTotal.get();
    }

    public synchronized long getDocumentsOpenedPresenterTotal() {
        return m_documentsOpenedPresenterTotal.get();
    }

    public synchronized long getDocumentsClosedPresenterTotal() {
        return m_documentsClosedPresenterTotal.get();
    }

    public synchronized long getDocumentsClosedPresenterTimeout() {
        return m_documentsClosedPresenterTimeout.get();
    }

    public synchronized long getDocumentsOperationsPresenterIncoming() {
        return m_documentsOperationsPresenterIncoming.get();
    }

    public synchronized long getDocumentsOperationsPresenterDistributed() {
        return m_documentsOperationsPresenterDistributed.get();
    }

    public synchronized long getRestoreDocCreated() {
        return m_restoreDocEvents.get(RestoreDocEventType.NEW_DOC).get();
    }

    public long getRestoreDocRemoved() {
        return m_restoreDocEvents.get(RestoreDocEventType.REMOVED_DOC).get();
    }

    public long getRestoreDocCurrent() {
        return m_restoreDocCurrent.get();
    }

    public long getRestoreDocRestoredSuccess() {
        return m_restoreDocEvents.get(RestoreDocEventType.RESTORED_SUCCESS).get();
    }

    public long getRestoreDocRestoredFailure() {
        return m_restoreDocEvents.get(RestoreDocEventType.RESTORED_ERROR).get();
    }

    public long getRestoreDocManagedFilesRemovedLastCycle() {
        return m_restoreDocEvents.get(RestoreDocEventType.MANAGED_FILES_LAST_CYCLE_REMOVED).get();
    }

    public long getRestoreDocManagedFilesInRemoveQueue() {
        return m_restoreDocEvents.get(RestoreDocEventType.MANAGED_FILES_TO_BE_REMOVED).get();
    }

    public long getRestoreDocManagedFilesRuntimeLastCycle() {
        return m_restoreDocEvents.get(RestoreDocEventType.MANAGED_FILES_LAST_CYCLE_TIME).get();
    }

    public long getRestoreDocManagedFilesCurrent() {
        return m_restoreDocManagedFilesCurrent.get();
    }

    public long getBackgroundSaveLastCycleDocCount() {
        return m_fssDocEvents.get(BackgroundSaveEventType.FSS_LAST_CYCLE_DOC_COUNT).get();
    }

    public long getBackgroundSaveLastCycleTime() {
        return m_fssDocEvents.get(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME).get();
    }

    public long getBackgroundSaveAverageTimePerDoc() {
        return m_fssDocEvents.get(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME_PER_DOC).get();
    }

    public long getBackgroundSaveMaxTimeForSave() {
        return m_fssMaxTimeForSave.get();
    }

    public long getBackgroundSaveMinTimeFasterSave() {
        return m_fssMinTimeForFasterSave.get();
    }

    public long getRestoreDocOpsMsgStored() {
        return m_restoreDocOpsMsgStored.get();
    }

    public long getRestoreDocOpsMsgStoredTime() {
        return m_restoreDocOpsMsgStoredTime.get();
    }

    public void setBackgroundSaveMaxTimeForSave(long maxTime) {
        m_fssMaxTimeForSave.set(maxTime);
    }

    public long getBackgroundSaveMinNumOfMsgForFasterSaveText() {
        return m_fssMsgCountForFasterSaveText.get();
    }

    public void setBackgroundSaveMinNumOfMsgForFasterSaveText(long count) {
        m_fssMsgCountForFasterSaveText.set(count);
    }

    public long getBackgroundSaveMinNumOfMsgForFasterSaveSpreadsheet() {
        return m_fssMsgCountForFasterSaveSpreadsheet.get();
    }

    public void setBackgroundSaveMinNumOfMsgForFasterSaveSpreadsheet(long count) {
        m_fssMsgCountForFasterSaveSpreadsheet.set(count);
    }

    public long getBackgroundSaveMinNumOfMsgForFasterSavePresentation() {
        return m_fssMsgCountForFasterSavePresentation.get();
    }

    public void setBackgroundSaveMinNumOfMsgForFasterSavePresentation(long count) {
        m_fssMsgCountForFasterSavePresentation.set(count);
    }

    public void setBackgroundSaveMinTimeFasterSave(long minTime) {
        m_fssMinTimeForFasterSave.set(minTime);
    }

    public void setRestoreDocOpsMsgStored(long count) {
        m_restoreDocOpsMsgStored.set(count);
    }

    public void setRestoreDocMsgStoredTime(long time) {
        m_restoreDocOpsMsgStoredTime.set(time);
    }

    // - OT -------------------------------------------------------------------

    public long getTransformationCount(DocumentType documentType) {
        if (null == documentType) {
            final AtomicLong accumulatedLong = new AtomicLong();

            m_otTransformationCounts.keySet().stream().forEach((curDocumentType)-> {
                accumulatedLong.addAndGet(m_otTransformationCounts.get(curDocumentType).get());
            });

            return accumulatedLong.get();
        }

        return m_otTransformationCounts.get(documentType).get();
    }

	public long getUnresolvableTransformations(DocumentType documentType) {
        if (null == documentType) {
            final AtomicLong accumulatedLong = new AtomicLong();

            m_otUnresolvableTransformationCounts.keySet().stream().forEach((curDocumentType)-> {
                accumulatedLong.addAndGet(m_otUnresolvableTransformationCounts.get(curDocumentType).get());
            });

            return accumulatedLong.get();
        }

        return m_otUnresolvableTransformationCounts.get(documentType).get();
	}

    public long getUnresolvableTransformationReloads(DocumentType documentType) {
        if (null == documentType) {
            final AtomicLong accumulatedLong = new AtomicLong();

            m_otUnresolvableTransformationReloadCounts.keySet().stream().forEach((curDocumentType) -> {
                accumulatedLong.addAndGet(m_otUnresolvableTransformationReloadCounts.get(curDocumentType).get());
            });

            return accumulatedLong.get();
        }

        return m_otUnresolvableTransformationReloadCounts.get(documentType).get();
    }

    // - Implementation --------------------------------------------------------

    private void implHandleRestoreDocEvent(RestoreDocEvent event) {
        if (null != event) {
            final RestoreDocEventType type = event.getEventType();

            m_eventCount.incrementAndGet();

            switch (type) {
                case NEW_DOC: {
                    m_restoreDocEvents.get(type).incrementAndGet();
                    m_restoreDocCurrent.incrementAndGet();
                    break;
                }

                case REMOVED_DOC: {
                    m_restoreDocEvents.get(type).incrementAndGet();
                    m_restoreDocCurrent.decrementAndGet();
                    break;
                }

                case RESTORED_SUCCESS:
                case RESTORED_ERROR: {
                    m_restoreDocEvents.get(type).incrementAndGet();
                    break;
                }

                case MANAGED_FILES_CURRENT_INC:
                    m_restoreDocManagedFilesCurrent.incrementAndGet();
                    break;

                case MANAGED_FILES_CURRENT_DEC:
                    m_restoreDocManagedFilesCurrent.decrementAndGet();
                    break;

                case MANAGED_FILES_LAST_CYCLE_TIME:
                    m_restoreDocEvents.get(type).set((Long)event.getValue());
                    break;

                case MANAGED_FILES_LAST_CYCLE_REMOVED:
                case MANAGED_FILES_TO_BE_REMOVED:
                    m_restoreDocEvents.get(type).set((Integer)event.getValue());
                    break;

                case NONE:
                default:
                    break;
            }
        }
    }

    /**
     * @param event
     */
    private void implHandleDocumentEvent(DocumentEvent event) {
        if (null != event) {
            final DocumentType documentType = event.getDocumentType();

            m_eventCount.incrementAndGet();

            if (null != documentType) {
                switch (event.getEventType()) {
                case CREATE: {
                    m_documentsCreated.get(documentType).incrementAndGet();
                    break;
                }

                case OPEN: {
                    if (event instanceof OpenEvent) {
                        final OpenEvent openEvent = (OpenEvent) event;

                        m_documentsOpened.get(documentType).get(openEvent.getOpenType()).incrementAndGet();
                        m_documentsSizes.get(documentType).add(openEvent.getDocumentSize());
                    }

                    break;
                }

                case OPERATIONS: {
                    if (event instanceof OperationsEvent) {
                        final OperationsEvent operationsEvent = (OperationsEvent) event;

                        m_documentsOperations.get(documentType).get(operationsEvent.getOperationType()).addAndGet(
                            operationsEvent.getActionCount());
                    }
                    break;
                }

                case SAVE: {
                    if (event instanceof SaveEvent) {
                        final SaveEvent saveEvent = (SaveEvent) event;

                        m_documentsSaved.get(documentType).get(saveEvent.getSaveType()).incrementAndGet();
                    }
                    break;
                }

                case CLOSE: {
                    if (event instanceof CloseEvent) {
                        final CloseEvent closeEvent = (CloseEvent) event;

                        m_documentsClosed.get(documentType).get(closeEvent.getCloseType()).incrementAndGet();
                    }
                    break;
                }

                case ERROR: {
                    m_documentsErrors.get(documentType).incrementAndGet();
                    break;
                }

                case NONE:
                default: {
                    break;
                }
                }
            }
        }
    }

    private void implHandleBackgroundSaveDocEvent(BackgroundSaveEvent event) {
        if (null != event) {
            final BackgroundSaveEventType eventType = event.getEventType();

            m_eventCount.incrementAndGet();

            switch (eventType) {
            case FSS_LAST_CYCLE_DOC_COUNT:
            case FSS_LAST_CYCLE_TIME: m_fssDocEvents.get(eventType).set(event.getValue()); break;
            case FSS_LAST_CYCLE_TIME_PER_DOC: {
                final long newValue = event.getValue();
                m_fssDocEvents.get(eventType).getAndAccumulate(newValue, (a , b) -> {
                    return (a == 0) ? b : (b == 0) ? a : (a + b) / 2;
                });
                break;
            }
            case FSS_LAST_CYCLE_TIME_PER_DOC_RESET: {
                m_fssDocEvents.get(BackgroundSaveEventType.FSS_LAST_CYCLE_TIME_PER_DOC).set(0);
                break;
            }
            default: break;
            }
        }
    }

    private void implHandlePresenterEvent(PresenterEvent event) {
        if (null != event) {
            final PresenterEventType eventType = event.getEventType();

            m_eventCount.incrementAndGet();

            switch (eventType) {
            case OPEN: m_documentsOpenedPresenterTotal.incrementAndGet(); break;
            case CLOSE: m_documentsClosedPresenterTotal.incrementAndGet(); break;
            case REMOTE_PRESENTER_START: m_documentsStartedRemotePresenterTotal.incrementAndGet(); break;
            case REMOTE_PRESENTER_END: m_documentsStartedRemotePresenterTotal.decrementAndGet(); break;
            case OPERATIONS_INCOMING: m_documentsOperationsPresenterIncoming.incrementAndGet(); break;
            case OPERATIONS_DISTRIBUTED: {
                final int nCount = event.getParticipantsCount();
                m_documentsOperationsPresenterDistributed.addAndGet(nCount);
                break;
            }
            case CLOSE_TIMEOUT: m_documentsClosedPresenterTimeout.incrementAndGet(); break;
            default: break;
            }
        }
    }

    private void implHandleTransformation(OTEvent event) {
    	if (null != event) {
    		final DocumentType documentType = event.getDocumentType();
    		if (documentType != DocumentType.NONE) {
    			final OTEventType eventType = event.getEventType();
    			if (eventType == OTEventType.TRANSFORMATION) {
                    m_otTransformationCounts.get(documentType).incrementAndGet();
    			} else if (eventType == OTEventType.UNRESOLVABLE_TRANSFORMATION) {
    				m_otUnresolvableTransformationCounts.get(documentType).incrementAndGet();
                } else if (eventType == OTEventType.UNRESOLVABLE_TRANSFORMATION_RELOAD) {
                    m_otUnresolvableTransformationReloadCounts.get(documentType).incrementAndGet();
                }
    		}
    	}
    }

    /**
    *
    */
    protected void initPeriodicResetTimer() {
        if (null == m_resetTask) {
            // the first values are retrieved => call reset period to retrieve latest results
            resetMeasurementPeriod();

            // the periodically called reset task
            m_resetTask = new TimerTask() {
                /*
                 * (non-Javadoc)
                 * @see com.openexchange.documentconverter.impl.Statistics.ResetTask#run()
                 */
                @Override
                public void run() {
                    resetMeasurementPeriod();
                }
            };

            // start the timer to periodically retrieve further result sets
            // a short amount of time before the next results will be requested
            // with a fixed period from external tools, e.g. 300s with munin
            m_resetTimer.scheduleAtFixedRate(
                m_resetTask,
                RESET_TIMER_PERIOD_MILLISECONDS - RESET_TIMER_AHEAD_TIME,
                RESET_TIMER_PERIOD_MILLISECONDS);
        }
    }

    /**
    *
    */
    protected synchronized void resetMeasurementPeriod() {
        m_documentsSizesResultTotal.set(getDocumentsMedianSizeValue(null));

        for (DocumentType documentType : DocumentType.values()) {
            // save documents sizes results and reset working Map
            m_documentsSizesResult.put(documentType, new Long(getDocumentsMedianSizeValue(documentType)));
            m_documentsSizes.get(documentType).clear();
        }
    }

    /**
     * @param documentType
     * @param documentsMap
     * @param opType
     * @return
     */
    static protected long getDocumentsCountValue(DocumentType documentType, Map<DocumentType, Map<? extends OpType, AtomicLong>> documentsMap, OpType opType) {
        long ret = 0;

        if ((null != documentType) && (null != documentsMap)) {
            final Map<? extends OpType, AtomicLong> opTypeMap = documentsMap.get(documentType);

            if (null == opType) {
                final Iterator<? extends OpType> keyIter = opTypeMap.keySet().iterator();

                while (keyIter.hasNext()) {
                    ret += opTypeMap.get(keyIter.next()).get();
                }
            } else {
                ret = opTypeMap.get(opType).get();
            }
        }

        return ret;
    }

    /*
     * @param sizesMap
     * @param documentType
     * @return
     */
    protected long getDocumentsMedianSizeValue(DocumentType documentType) {
        List<Long> workList = null;

        if (null == documentType) {
            workList = new ArrayList<>();

            for (DocumentType curDocumentType : DocumentType.values()) {
                final List<Long> curList = m_documentsSizes.get(curDocumentType);

                synchronized (curList) {
                    workList.addAll(curList);
                }
            }
        } else {
            workList = m_documentsSizes.get(documentType);
        }

        return getMedianValue(workList);
    }

    /**
     * @param workList
     * @return
     */
    protected static long getMedianValue(List<Long> workList) {
        double ret = 0.0;

        if (null != workList) {
            synchronized (workList) {
                final long[] longArray = new long[workList.size()];

                if (longArray.length > 0) {
                    final int medianIndex = longArray.length >> 1;
                    int i = 0;

                    for (Long curEntry : workList) {
                        longArray[i++] = curEntry;
                    }

                    Arrays.sort(longArray);

                    // calculate average of left and right values wrt. the median position for even size lists
                    ret = (0 == (longArray.length % 2)) ? ((longArray[medianIndex - 1] + longArray[medianIndex]) * 0.5) : longArray[medianIndex];
                }
            }
        }

        return Math.round(ret);
    }

    /**
     * @param workList
     * @return
     */
    protected static long getPeakValue(List<AtomicLong> workList) {
        long ret = 0;

        if (null != workList) {
            synchronized (workList) {
                for (AtomicLong curEntry : workList) {
                    final long curValue = curEntry.get();

                    if (curValue > ret) {
                        ret = curValue;
                    }
                }
            }
        }

        return ret;
    }


    // - Members ---------------------------------------------------------------

    private final Timer m_resetTimer = new Timer("Office statistics", true);

    private TimerTask m_resetTask = null;

    private final AtomicLong m_eventCount = new AtomicLong();

    private final Map<DocumentType, AtomicLong> m_documentsCreated = new EnumMap<>(DocumentType.class);

    private final Map<DocumentType, AtomicLong> m_documentsErrors = new EnumMap<>(DocumentType.class);

    private final Map<DocumentType, Map<? extends OpType, AtomicLong>> m_documentsOpened = new EnumMap<>(
        DocumentType.class);

    private final Map<DocumentType, Map<? extends OpType, AtomicLong>> m_documentsSaved = new EnumMap<>(
        DocumentType.class);

    private final Map<DocumentType, Map<? extends OpType, AtomicLong>> m_documentsClosed = new EnumMap<>(
        DocumentType.class);

    private final Map<DocumentType, Map<? extends OpType, AtomicLong>> m_documentsOperations = new EnumMap<>(
        DocumentType.class);

    private final Map<DocumentType, List<Long>> m_documentsSizes = new EnumMap<>(DocumentType.class);

    private final Map<DocumentType, Long> m_documentsSizesResult = new EnumMap<>(DocumentType.class);

    private final AtomicLong m_documentsSizesResultTotal = new AtomicLong();

    private final AtomicLong m_documentsStartedRemotePresenterTotal = new AtomicLong();

    private final AtomicLong m_documentsOpenedPresenterTotal = new AtomicLong();

    private final AtomicLong m_documentsClosedPresenterTotal = new AtomicLong();

    private final AtomicLong m_documentsClosedPresenterTimeout = new AtomicLong();

    private final AtomicLong m_documentsOperationsPresenterIncoming = new AtomicLong();

    private final AtomicLong m_documentsOperationsPresenterDistributed = new AtomicLong();

    private final Map<RestoreDocEventType, AtomicLong> m_restoreDocEvents = new EnumMap<>(RestoreDocEventType.class);

    private final AtomicLong m_restoreDocCurrent = new AtomicLong();

    private final AtomicLong m_restoreDocManagedFilesCurrent = new AtomicLong(0);

    private final Map<BackgroundSaveEventType, AtomicLong> m_fssDocEvents = new EnumMap<>(BackgroundSaveEventType.class);

    private final AtomicLong m_restoreDocOpsMsgStored = new AtomicLong(0);

    private final AtomicLong m_restoreDocOpsMsgStoredTime = new AtomicLong(0);

    private final AtomicLong m_fssMaxTimeForSave = new AtomicLong(0);

    private final AtomicLong m_fssMsgCountForFasterSaveText = new AtomicLong(0);

    private final AtomicLong m_fssMsgCountForFasterSaveSpreadsheet = new AtomicLong(0);

    private final AtomicLong m_fssMsgCountForFasterSavePresentation = new AtomicLong(0);

    private final AtomicLong m_fssMinTimeForFasterSave = new AtomicLong(0);

    private final Map<DocumentType, AtomicLong> m_otTransformationCounts = new EnumMap<>(DocumentType.class);

    private final Map<DocumentType, AtomicLong> m_otUnresolvableTransformationCounts = new EnumMap<>(DocumentType.class);

    private final Map<DocumentType, AtomicLong> m_otUnresolvableTransformationReloadCounts = new EnumMap<>(DocumentType.class);
}
