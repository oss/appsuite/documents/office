/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;

public class DataSourceAccessDrive extends DataSourceAccessBase implements DataSourceAccess {
	private static final Logger LOG = LoggerFactory.getLogger(DataSourceAccessDrive.class);

	@Autowired
	private FileHelperServiceFactory fileHelperServiceFactory;

	@Autowired
	private IDBasedFileAccessFactory idBasedFileAccessFactory;

	@Autowired
	private StorageHelperServiceFactory storageHelperServiceFactory;

	@Autowired(required=false)
	private CryptographicAwareIDBasedFileAccessFactory cryptographicAwareIDBasedFileAccessFactory;

	@Autowired(required=false)
	private CryptographicServiceAuthenticationFactory cryptographicServiceAuthenticationFactory;

	public DataSourceAccessDrive(ServerSession session) {
		super(session);
	}

	@Override
	public boolean canRead(String folderId, String id, String versionOrAttachmentId) throws DataSourceException {
		boolean canRead = false;

		final FileHelperService fileHelper = fileHelperServiceFactory.create(session);
        try {
    		canRead = fileHelper.canReadFile(folderId, id, session.getUserId());
        } catch (Exception e) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
        	LOG.error("DataSourceAccessDrive exception caught trying to determine read access for user session", e);
            MDC.remove(MDCEntries.ERROR);
        	throw new DataSourceException(ErrorCode.GENERAL_UNKNOWN_ERROR, e);
        }
        return canRead;
	}

	@Override
	public MetaData getMetaData(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
        final String encryptionInfo = determineEncryptionInfo(authCode);

		try {
			final IDBasedFileAccess fileAccess = getFileAccess(session, folderId, id, encryptionInfo);
			final String version = determineVersionDependentOnStorage(folderId, versionOrAttachmentId);
	        final File fileMetaData = fileAccess.getFileMetadata(id, version);
	    	return mapFileMetaDataToMetaData(fileMetaData);
		} catch (OXException e) {
			final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
			throw new DataSourceException(errorCode, e);
		}
	}

	@Override
	public InputStream getContentStream(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
        final String encryptionInfo = determineEncryptionInfo(authCode);

        try {
            final IDBasedFileAccess fileAccess = getFileAccess(session, folderId, id, encryptionInfo);
			final String version = determineVersionDependentOnStorage(folderId, versionOrAttachmentId);
            final File metaData = fileAccess.getFileMetadata(id, version);
            final long fileSize = metaData.getFileSize();

            checkFilSizeAndMemoryConsumption(fileSize, encryptionInfo);

            InputStream inStream = null;
            try {
	        	inStream = fileAccess.getDocument(id, version);
	        	final byte[] data = IOUtils.toByteArray(inStream);
	        	return new ByteArrayInputStream(data);
	    	} catch (IOException e) {
	    		throw new DataSourceException(ErrorCode.LOADDOCUMENT_FAILED_ERROR, e);
	    	} finally {
	    		IOHelper.closeQuietly(inStream);
	    	}
		} catch (OXException e) {
			final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
			throw new DataSourceException(errorCode, e);
		}
	}

    @Override
    public String getDataSource() {
        return DataSource.DRIVE;
    }

	private MetaData mapFileMetaDataToMetaData(File metaData) {
		return new MetaData(metaData.getFolderId(), metaData.getId(), metaData.getVersion(),
		                    metaData.getFileSize(), metaData.getFileName(), metaData.getFileMD5Sum(),
		                    metaData.getFileMIMEType());
	}

    private IDBasedFileAccess getFileAccess(ServerSession serverSession, String folderId, String fileId, String encryptionInfo) throws DataSourceException, OXException {
        if (null == idBasedFileAccessFactory)
            throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

        if (null == serverSession)
            throw new DataSourceException(ErrorCode.GENERAL_SESSION_INVALID_ERROR);

        if (StringUtils.isEmpty(fileId) || StringUtils.isEmpty(folderId))
            throw new DataSourceException(ErrorCode.GENERAL_ARGUMENTS_ERROR);

        IDBasedFileAccess fileAccess = idBasedFileAccessFactory.createAccess(serverSession);
        if ((null != encryptionInfo) && (encryptionInfo.length() > 0)) {
            final CryptographicAwareIDBasedFileAccessFactory encryptionAwareFileAccessFactory = cryptographicAwareIDBasedFileAccessFactory;
            final CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory = cryptographicServiceAuthenticationFactory;

            if ((encryptionAwareFileAccessFactory != null) && (encryptionAuthenticationFactory != null)) {
                final EnumSet<CryptographyMode> cryptMode = EnumSet.of(CryptographyMode.DECRYPT);
                final String authentication = encryptionInfo;
                fileAccess = encryptionAwareFileAccessFactory.createAccess(fileAccess, cryptMode, session, authentication );
            }
        }

        if (fileAccess == null)
            throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

        return fileAccess;
    }

    private String determineVersionDependentOnStorage(String folderId, String versionToRetrieve) {
		final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, folderId);

        String version = StringUtils.isEmpty(versionToRetrieve) ? FileStorageFileAccess.CURRENT_VERSION : versionToRetrieve;
		return storageHelper.supportsFileVersions() ? version : FileStorageFileAccess.CURRENT_VERSION;
    }

}
