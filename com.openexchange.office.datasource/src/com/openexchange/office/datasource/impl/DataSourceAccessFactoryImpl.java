/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import org.springframework.stereotype.Service;

import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceAccessFactory;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.tools.session.ServerSession;

@Service
@RegisteredService(registeredClass=DataSourceAccessFactory.class)
public class DataSourceAccessFactoryImpl implements DataSourceAccessFactory, OsgiBundleContextAware {

	private OsgiBundleContextAndActivator bundleCtx;

	@Override
	public DataSourceAccess createDataSourceAccess(ServerSession session, String dataSource) throws DataSourceException {
		DataSourceAccess result = null;

		switch (dataSource) {
		case DataSource.DRIVE_GUARD:
		case DataSource.DRIVE: result = new DataSourceAccessDrive(session); break;
		case DataSource.MAIL: result = new DataSourceAccessMail(session); break;
		case DataSource.COMPOSE: result = new DataSourceAccessCompose(session); break;
		case DataSource.TASKS:
		case DataSource.CALENDAR:
		case DataSource.CONTACTS: result = new DataSourceAccessCalendarTaskContact(session, dataSource); break;
		default: throw new DataSourceException(ErrorCode.GENERAL_ARGUMENTS_ERROR);
		}
		bundleCtx.prepareObject(result);
		return result;
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
