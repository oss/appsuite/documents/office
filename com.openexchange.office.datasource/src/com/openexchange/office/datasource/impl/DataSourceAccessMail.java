/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import com.openexchange.exception.OXException;
import com.openexchange.mail.MailServletInterface;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.service.encryption.EncryptionInfo;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.tools.session.ServerSession;

public class DataSourceAccessMail extends DataSourceAccessBase implements DataSourceAccess {
	private static final Logger LOG = LoggerFactory.getLogger(DataSourceAccessMail.class);

	public DataSourceAccessMail(ServerSession session) {
		super(session);
	}

	@Override
	public boolean canRead(String folderId, String id, String versionOrAttachmentId) throws DataSourceException {
		MailServletInterface mailAccess = null;

		try {
			mailAccess = MailServletInterface.getInstance(session);

            if (mailAccess == null)
                throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

    		return (mailAccess.getMessage(folderId, id) != null);
		} catch (OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("DataSourceAccessMail exception caught trying to determine read access for user session", e);
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
        	throw new DataSourceException(errorCode, e);
        } finally {
			if (mailAccess != null) {
			    try {
			    	mailAccess.close(true);
			    } catch (OXException e) {
			    	LOG.warn("DataSourceAccessMail exception caught trying to close MailServletInterface instance", e);
			    }
			}
		}
	}

	@Override
	public MetaData getMetaData(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
		MailServletInterface mailAccess = null;

		try {
		    mailAccess = initMailServletInterface(authCode);

    		final MailPart part = mailAccess.getMessageAttachment(folderId, id, versionOrAttachmentId, false);
    		if (part != null) {
    			final ContentType contentType = part.getContentType();

    			final long fileSize = part.getSize();
            	final String fileName = part.getFileName();
            	String mimeType = contentType.getBaseType();

            	// try to refine mime type using extension if we see an unknown mime type
            	final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(contentType.getBaseType(), FileHelper.getExtension(fileName, true));
            	if (docFormatInfo != null) {
            		mimeType = docFormatInfo.get(DocumentFormatHelper.PROP_MIME_TYPE);
            	}

            	final String hash = createHashFrom(fileSize, fileName, mimeType);
            	return new MetaData(folderId, id, versionOrAttachmentId, fileSize, fileName, hash, mimeType);
    		}

    		throw new DataSourceException(ErrorCode.GENERAL_RESOURCE_NOT_FOUND_ERROR);
        } catch (OXException e) {
			final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
			throw new DataSourceException(errorCode, e);
		} finally {
			if (mailAccess != null)
			    mailAccess.close();
		}
	}

	@Override
	public InputStream getContentStream(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
		MailServletInterface mailAccess = null;

		try {
			mailAccess = initMailServletInterface(authCode);

    		final MailPart part = mailAccess.getMessageAttachment(folderId, id, versionOrAttachmentId, false);
    		if (part != null) {
    	        final String encryptionInfo = determineEncryptionInfo(authCode);
    			final long fileSize = part.getSize();

    	    	checkFilSizeAndMemoryConsumption(fileSize, encryptionInfo);

            	final ContentType contentType = part.getContentType();
            	final String fileName = part.getFileName();

            	final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(contentType.getBaseType(), FileHelper.getExtension(fileName, true));
            	if (docFormatInfo != null) {
                	try {
            			final InputStream inStream = part.getInputStream();
                    	final byte[] data = IOUtils.toByteArray(inStream);
        	        	return new ByteArrayInputStream(data);
                	} catch (IOException e) {
                		throw new DataSourceException(ErrorCode.LOADDOCUMENT_FAILED_ERROR, e);
                    }
            	}

            	throw new DataSourceException(ErrorCode.LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR);
    		}

    		throw new DataSourceException(ErrorCode.LOADDOCUMENT_TIMEOUT_RETRIEVING_STREAM_ERROR);
        } catch (final OXException e) {
        	final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
			throw new DataSourceException(errorCode, e);
        } finally {
            if (mailAccess != null)
                mailAccess.close();
		}
	}

	protected MailServletInterface initMailServletInterface(Optional<String> authCode) throws OXException, DataSourceException {
		MailServletInterface mailAccess;
		if (authCode.isPresent()) {
		    final EncryptionInfo encryptionInfo = encryptionInfoService.createEncryptionInfo(session, authCode.get());
		    mailAccess = MailServletInterface.getInstanceWithDecryptionSupport(session, encryptionInfo.isPresent() ? encryptionInfo.getEncryptionValue() : null);
		} else {
		    mailAccess = MailServletInterface.getInstance(session);
		}

		if (mailAccess == null)
		    throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);
		return mailAccess;
	}

    @Override
    public String getDataSource() {
        return DataSource.MAIL;
    }

}
