/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import com.openexchange.exception.OXException;
import com.openexchange.mail.compose.Attachment;
import com.openexchange.mail.compose.AttachmentResult;
import com.openexchange.mail.compose.CompositionSpaceId;
import com.openexchange.mail.compose.CompositionSpaceService;
import com.openexchange.mail.compose.CompositionSpaceServiceFactoryRegistry;
import com.openexchange.mail.compose.CompositionSpaces;
import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.tools.session.ServerSession;


public class DataSourceAccessCompose extends DataSourceAccessBase {
    private static final Logger LOG = LoggerFactory.getLogger(DataSourceAccessCompose.class);

    @Autowired(required=false)
    private CompositionSpaceServiceFactoryRegistry compositionSpaceServiceFactorRegistry;

    public DataSourceAccessCompose(ServerSession session) {
        super(session);
    }

    @Override
    public boolean canRead(String folderId, String id, String versionOrAttachmentId) throws DataSourceException {
        if (compositionSpaceServiceFactorRegistry == null)
            throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

        try {
            final CompositionSpaceId compositionSpaceId = CompositionSpaceId.valueOf(id);
            final CompositionSpaceService compositionSpaceService = compositionSpaceServiceFactorRegistry.getFactoryFor(compositionSpaceId.getServiceId(), session).createServiceFor(session);
            final UUID attachmentUuid = CompositionSpaces.parseAttachmentId(versionOrAttachmentId);
            return (compositionSpaceService.getAttachment(compositionSpaceId.getId(), attachmentUuid) != null);
        } catch (OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCompose: Exception caught trying to get attachment from CompositionSpace", e);
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
            throw new DataSourceException(errorCode);
        }
    }

    @Override
    public MetaData getMetaData(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
        if (compositionSpaceServiceFactorRegistry == null)
            throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

        try {
            final CompositionSpaceId compositionSpaceId = CompositionSpaceId.valueOf(id);
            final CompositionSpaceService compositionSpaceService = compositionSpaceServiceFactorRegistry.getFactoryFor(compositionSpaceId.getServiceId(), session).createServiceFor(session);
            final UUID attachmentUuid = CompositionSpaces.parseAttachmentId(versionOrAttachmentId);
            final AttachmentResult attachmentResult = compositionSpaceService.getAttachment(compositionSpaceId.getId(), attachmentUuid);
            final Attachment attachment = (null != attachmentResult) ? attachmentResult.getFirstAttachment() : null;

            if (null == attachment) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCompose: no attachment available from CompositionSpace");
                MDC.remove(MDCEntries.ERROR);
                throw new DataSourceException(ErrorCode.GENERAL_UNKNOWN_ERROR);
            }

            final long fileSize = attachment.getSize();
            final String fileName = attachment.getName();
            String mimeType = attachment.getMimeType();

            // try to refine mime type using extension if we see an unknown mime type
            final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(mimeType, FileHelper.getExtension(fileName, true));
            if (docFormatInfo != null) {
                mimeType = docFormatInfo.get(DocumentFormatHelper.PROP_MIME_TYPE);
            }

            final String hash = createHashFrom(fileSize, fileName, mimeType);
            return new MetaData(folderId, id, versionOrAttachmentId, fileSize, fileName, hash, mimeType);
        } catch (OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCompose: Exception caught trying to get meta data from attachment from CompositionSpace", e);
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
            throw new DataSourceException(errorCode);
        }
    }

    @Override
    public InputStream getContentStream(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {
        if (compositionSpaceServiceFactorRegistry == null)
            throw new DataSourceException(ErrorCode.GENERAL_SERVICE_DOWN_ERROR);

        try {
            final CompositionSpaceId compositionSpaceId = CompositionSpaceId.valueOf(id);
            final CompositionSpaceService compositionSpaceService = compositionSpaceServiceFactorRegistry.getFactoryFor(compositionSpaceId.getServiceId(), session).createServiceFor(session);
            final UUID attachmentUuid = CompositionSpaces.parseAttachmentId(versionOrAttachmentId);
            final AttachmentResult attachmentResult = compositionSpaceService.getAttachment(compositionSpaceId.getId(), attachmentUuid);
            final Attachment attachment = (null != attachmentResult) ? attachmentResult.getFirstAttachment() : null;

            if (null == attachment) {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCompose: no attachment available from CompositionSpace");
                MDC.remove(MDCEntries.ERROR);
                throw new DataSourceException(ErrorCode.GENERAL_UNKNOWN_ERROR);
            }

            final long fileSize = attachment.getSize();

            checkFilSizeAndMemoryConsumption(fileSize, null);

            final byte[] data = IOUtils.toByteArray(attachment.getData());
            return new ByteArrayInputStream(data);
        } catch (OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCompose: Exception caught trying to get meta data from attachment from CompositionSpace", e);
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
            throw new DataSourceException(errorCode);
        } catch (IOException e) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
            LOG.error("DataSourceAccessCompose: Exception caught trying to get meta data from attachment from CompositionSpace", e);
            MDC.remove(MDCEntries.ERROR);
            throw new DataSourceException(ErrorCode.GENERAL_UNKNOWN_ERROR);
        }
    }

    @Override
    public String getDataSource() {
        return DataSource.COMPOSE;
    }

}
