/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.Types;
import com.openexchange.groupware.attach.AttachmentBase;
import com.openexchange.groupware.attach.AttachmentMetadata;
import com.openexchange.groupware.attach.Attachments;
import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.tools.session.ServerSession;

public class DataSourceAccessCalendarTaskContact extends DataSourceAccessBase implements DataSourceAccess {
    private static final Logger LOG = LoggerFactory.getLogger(DataSourceAccessCalendarTaskContact.class);

	final String dataSource;
	final int moduleId;

	public DataSourceAccessCalendarTaskContact(ServerSession session, String dataSource) throws DataSourceException {
		super(session);
		this.dataSource = dataSource;

		moduleId = mapDataSourceStringToModuleId(dataSource);
	}

	@Override
	public boolean canRead(String folder, String id, String versionOrAttachmentId) throws DataSourceException {
        try {
        	final int folderId = Integer.parseInt(folder);
        	final int attachedId = Integer.parseInt(versionOrAttachmentId);
        	final int objectId = Integer.parseInt(id);

        	final AttachmentBase attachmentAccess = Attachments.getInstance();
        	return (attachmentAccess.getAttachment(session, folderId, objectId, moduleId, attachedId, session.getContext(), session.getUser(), session.getUserConfiguration()) != null);
	    } catch (final OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
	        try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
	            LOG.error("DataSourceAccessCalendarTaskContact: Exception caught trying to retrieve meta data from task/calendar/contacts attachment.", e);
	        } finally {
                MDC.remove(MDCEntries.ERROR);
	        }
	        throw new DataSourceException(errorCode, e);
	    }
	}

	@Override
	public MetaData getMetaData(String folder, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {

        try {
        	final int folderId = Integer.parseInt(folder);
        	final int attachedId = Integer.parseInt(versionOrAttachmentId);
        	final int objectId = Integer.parseInt(id);

        	final AttachmentBase attachmentAccess = Attachments.getInstance();
        	final AttachmentMetadata metaData = attachmentAccess.getAttachment(session, folderId, objectId, moduleId, attachedId, session.getContext(), session.getUser(), session.getUserConfiguration());
        	if (metaData != null) {
        		final long fileSize = metaData.getFilesize();
        		final String fileName = metaData.getFilename();
        		String mimeType = metaData.getFileMIMEType();

            	// try to refine mime type using extension if we see an unknown mime type
            	final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(mimeType, FileHelper.getExtension(fileName, true));
            	if (docFormatInfo != null) {
            		mimeType = docFormatInfo.get(DocumentFormatHelper.PROP_MIME_TYPE);
            	}

            	final String hash = createHashFrom(fileSize, fileName, mimeType);
        		return new MetaData(folder, id, versionOrAttachmentId, fileSize, fileName, hash, mimeType);
        	}

        	throw new DataSourceException(ErrorCode.GENERAL_RESOURCE_NOT_FOUND_ERROR);
        } catch (final OXException e) {
            LOG.error("DataSourceAccessCalendarTaskContact: Exception caught trying to retrieve meta data from task/calendar/contacts attachment.", e);
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
            throw new DataSourceException(errorCode, e);
        }
	}

	@Override
	public InputStream getContentStream(String folder, String id, String versionOrAttachmentId, Optional<String> authCode) throws DataSourceException {

        try {
        	final int folderId = Integer.parseInt(folder);
        	final int objectId = Integer.parseInt(id);
        	final int attachedId = Integer.parseInt(versionOrAttachmentId);
        	final AttachmentBase attachmentAccess = Attachments.getInstance();
        	final AttachmentMetadata metaData = attachmentAccess.getAttachment(session, folderId, objectId, moduleId, attachedId, session.getContext(), session.getUser(), session.getUserConfiguration());
        	if (metaData != null) {
    	        final String encryptionInfo = determineEncryptionInfo(authCode);
        		final long fileSize = metaData.getFilesize();

        		checkFilSizeAndMemoryConsumption(fileSize, encryptionInfo);

        		final InputStream inStream = attachmentAccess.getAttachedFile(session, folderId, objectId, moduleId, attachedId, session.getContext(), session.getUser(), session.getUserConfiguration());
            	final byte[] data = IOUtils.toByteArray(inStream);
            	return new ByteArrayInputStream(data);
        	}

        	throw new DataSourceException(ErrorCode.GENERAL_RESOURCE_NOT_FOUND_ERROR);
        } catch (IOException e) {
            MDCHelper.safeMDCPut(MDCEntries.ERROR, ErrorCode.GENERAL_UNKNOWN_ERROR.getCodeAsStringConstant());
            LOG.error("DataSourceAccessCalendarTaskContact: IOException caught trying to retrieve input stream from task/calendar/contacts attachment.", e);
            MDC.remove(MDCEntries.ERROR);
            throw new DataSourceException(ErrorCode.GENERAL_UNKNOWN_ERROR, e);
        } catch (final OXException e) {
            final ErrorCode errorCode = ExceptionToErrorCode.map(e, ErrorCode.LOADDOCUMENT_FAILED_ERROR, false);
            try {
                MDCHelper.safeMDCPut(MDCEntries.ERROR, errorCode.getCodeAsStringConstant());
                LOG.error("DataSourceAccessCalendarTaskContact: Exception caught trying to retrieve input stream from task/calendar/contacts attachment.", e);
            } finally {
                MDC.remove(MDCEntries.ERROR);
            }
            throw new DataSourceException(errorCode, e);
        }
	}

    @Override
    public String getDataSource() {
        return dataSource;
    }

	private int mapDataSourceStringToModuleId(String source) throws DataSourceException {
		int module = -1;

		switch (source) {
		case DataSource.CALENDAR: module = Types.APPOINTMENT; break;
		case DataSource.CONTACTS: module = Types.CONTACT; break;
		case DataSource.TASKS: module = Types.TASK; break;
		default: throw new DataSourceException(ErrorCode.GENERAL_ARGUMENTS_ERROR);
		}

		return module;
	}

}
