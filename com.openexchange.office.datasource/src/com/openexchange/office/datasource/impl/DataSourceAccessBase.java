/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.impl;

import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.DataSourceResourceAccess;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.office.tools.common.system.SystemInfoHelper;
import com.openexchange.office.tools.service.encryption.EncryptionInfo;
import com.openexchange.office.tools.service.encryption.EncryptionInfoService;
import com.openexchange.tools.session.ServerSession;

public abstract class DataSourceAccessBase implements DataSourceAccess {

    private static final int DEFAULT_MAX_DOCUMENT_FILE_SIZE = 100; // 100 MB

    @Autowired
    protected ConfigurationService configurationService;
    
    @Autowired
    protected EncryptionInfoService encryptionInfoService;
    
	protected ServerSession session;

	DataSourceAccessBase(ServerSession session) {
		this.session = session;
	}

	@Override
	public DataSourceResourceAccess createResourceAccess(String folderId, String id, String versionOrAttachmentId, Optional<String> authCode) {
		return new DataSourceResourceAccessImpl(this, folderId, id, versionOrAttachmentId, authCode);
	}

	@Override
	public int getContextId() {
		return session.getContextId();
	}

	protected String determineEncryptionInfo(Optional<String> authCode) {
		EncryptionInfo encryptionInfo = null;

		if ((authCode != null) && (authCode.isPresent())) {
            encryptionInfo = encryptionInfoService.createEncryptionInfo(session, authCode.get());
    		if (encryptionInfo.isPresent()) {
    			return encryptionInfo.getEncryptionValue();
    		}
		}
		return null;
	}

	protected boolean checkFilSizeAndMemoryConsumption(long fileSize, String encryptionInfo) throws DataSourceException {
        if (!isSupportedFileSize(fileSize))
            throw new DataSourceException(ErrorCode.LOADDOCUMENT_COMPLEXITY_TOO_HIGH_ERROR);

        if (!enoughFreeHeapToLoadStreamData(fileSize, encryptionInfo))
        	throw new DataSourceException(ErrorCode.GENERAL_SYSTEM_BUSY_ERROR);

        return true;
	}

	protected boolean isSupportedFileSize(long fileSize) {
        long nMaxDocumentFileSize = DEFAULT_MAX_DOCUMENT_FILE_SIZE * 1024L * 1024L;

        if (null != configurationService) {
            nMaxDocumentFileSize = configurationService.getIntProperty("com.openexchange.office.maxDocumentFileSize", DEFAULT_MAX_DOCUMENT_FILE_SIZE) * 1024L * 1024L;
        }

        return (fileSize <= nMaxDocumentFileSize);
	}

    protected boolean enoughFreeHeapToLoadStreamData(long nFileSize, String encryptionInfo) {
        final MemoryObserver aMemObserver = MemoryObserver.get();

        final float fCurrentFileSizeFactor = StringUtils.isEmpty(encryptionInfo) ? 1.0f : OXDocument.PGP_FILE_SIZE_FACTOR;
        final long memNeededForStream = Math.round(nFileSize * fCurrentFileSizeFactor);
        return !aMemObserver.willHeapLimitBeReached(memNeededForStream);
    }

    protected String createHashFrom(long fileSize, String fileName, String mimeType) {
    	StringBuilder tmp = new StringBuilder(fileName);
    	tmp.append(fileSize);
    	tmp.append(mimeType);
    	return DigestUtils.md5Hex(tmp.toString());
    }
}
