/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.datasource.access;

public class MetaData {
    private final String folderId; // used for toString building
    private final String id; // used for toString building
    private final String versionOrAttachment;  // used for toString building
	private final long fileSize; // used for toString building
	private final String fileName; // used for toString building
	private final String hash; // used for toString building
	private final String source; // used for toString building
	private String mimeType;  // NOT used for toString building

	public MetaData(String folderId, String id, String versionOrAttachment, long fileSize, String fileName, String hash, String source) {
	    this.folderId = folderId;
	    this.id = id;
	    this.versionOrAttachment = versionOrAttachment;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.hash = hash;
		this.source = source;
		this.mimeType = "";
	}

	/**
	 * The toString method uses every member except <code>mimeType</code>,
	 * which is not an essential part of the class and might be used otherwise
	 */
	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder(256);
        builder.append("MetaData [folderId=");
        builder.append(folderId);
        builder.append(", id=");
        builder.append(id);
        builder.append(", versionOrAttachment=");
        builder.append(versionOrAttachment);
        builder.append(", fileSize=");
        builder.append(fileSize);
        builder.append(", fileName=");
        builder.append(fileName);
        builder.append(", hash=");
        builder.append(hash);
        builder.append(", source=");
        builder.append(source);
        builder.append("]");
        return builder.toString();
    }

    public MetaData(String folderId, String id, String versionOrAttachment, long fileSize, String fileName, String hash, String source, String mimeType) {
	    this(folderId, id, versionOrAttachment, fileSize, fileName, hash, source);
		this.mimeType = mimeType;
	}

    public String getFolderId() {
        return folderId;
    }

    public String getId() {
        return id;
    }

    public String getVersionOrAttachment() {
        return versionOrAttachment;
    }

	public long getFileSize() {
		return fileSize;
	}

	public String getFileName() {
		return fileName;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getHash() {
		return hash;
	}

	public String getSource() {
	    return source;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

}
