/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.templatemgr.api;

/**
 * {@link IResourceEntry}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IResourceEntry {

    /**
     * @return
     */
    public String getModule();

    /**
     * @param module
     */
    public void setModule(String module);

    /**
     * @return
     */
    public String getGroup();
    
    /**
     * @param group
     */
    public void setGroup(String group);

    /**
     * @return
     */
    public String getLocation();
    
    /**
     * @param location
     */
    public void setLocation(String location);

    /**
     * @return
     */
    public String getTitle();
    
    /**
     * @param title
     */
    public void setTitle(String title);

    /**
     * @return
     */
    public String getDescription();
    
    /**
     * @param description
     */
    public void setDescription(String description);
}
