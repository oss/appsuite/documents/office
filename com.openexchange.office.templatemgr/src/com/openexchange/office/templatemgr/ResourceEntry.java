/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.templatemgr;

import com.openexchange.office.templatemgr.api.IResourceEntry;

/**
 * {@link ResourceEntry}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class ResourceEntry implements IResourceEntry {
    
    private String m_module = null;
    private String m_group = null;
    private String m_location = null;
    private String m_title = null;
    private String m_description = null;
    
    /**
     * Initializes a new {@link ResourceEntry}.
     * @param module
     * @param group
     * @param location
     */
    public ResourceEntry(String module, String group, String location) {
        init(module, group, location, null, null);
    }

    /**
     * Initializes a new {@link ResourceEntry}.
     * @param module
     * @param group
     * @param location
     * @param title
     */
    public ResourceEntry(String module, String group, String location, String title) {
        init(module, group, location, title, null);
    }

    /**
     * Initializes a new {@link ResourceEntry}.
     * @param module
     * @param group
     * @param location
     * @param title
     * @param description
     */
    public ResourceEntry(String module, String group, String location, String title, String description) {
        init(module, group, location, title, description);
    }
    
    /**
     * @param _module
     * @param _group
     * @param _location
     * @param _title
     * @param _description
     */
    protected void init(String _module, String _group, String _location, String _title, String _description) {
        m_module = _module;
        m_group = _group;
        m_location = _location;
        m_title = _title;
        m_description = _description;
    }

    @Override
    public String getModule() {
        return m_module;
    }

    
    @Override
    public void setModule(String module) {
        m_module = module;
    }

    
    @Override
    public String getGroup() {
        return m_group;
    }

    
    @Override
    public void setGroup(String group) {
        m_group = group;
    }

    @Override
    public String getLocation() {
        return m_location;
    }

    
    @Override
    public void setLocation(String location) {
        m_location = location;
    }

    @Override
    public String getTitle() {
        return m_title;
    }

    @Override
    public void setTitle(String title) {
        m_title = title;
    }

    @Override
    public String getDescription() {
        return m_description;
    }

    @Override
    public void setDescription(String description) {
        m_description = description;
    }
}
