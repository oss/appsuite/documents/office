/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.templatemgr;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.openexchange.office.templatemgr.api.IResourceEntry;
import com.openexchange.office.templatemgr.api.IResourceProvider;
import com.openexchange.office.tools.annotation.RegisteredService;

/**
 * {@link ResourceProvider}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@Service
@RegisteredService(registeredClass=IResourceProvider.class)
public class ResourceProvider implements IResourceProvider {

    private volatile boolean          initialized = false;
    private ArrayList<IResourceEntry> entries     = new ArrayList<IResourceEntry>();

    /**
     * @return true in case the entry list has been initialized
     */
    protected boolean initEntries() {
        boolean isInit = initialized;

        if (!isInit) {
            synchronized(this) {
                isInit = initialized;
                if (!isInit) {
                    fillEntries();
                    isInit = initialized = true;
                }
            }
        }

        return isInit;
    }

    /**
     * Fill the entries list - must be called thread-safe as the resource provider
     * is used by several Grizzly instances in parallel.
     * See #44066
     */
    protected void fillEntries() {
        InputStream inputStm = null;

        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            if (null != factory) {
                final DocumentBuilder builder = factory.newDocumentBuilder();
                inputStm = this.getClass().getClassLoader().getResourceAsStream("com/openexchange/office/templatemgr/res/content.xml");
                final Document doc = ((null != builder) && (null != inputStm)) ? builder.parse(inputStm) : null;

                if (null != doc) {
                    doc.getDocumentElement().normalize();

                    final NodeList modules = doc.getElementsByTagName("module");

                    for (int i = 0; i < modules.getLength(); ++i) {
                        final Node module = modules.item(i);

                        if (Node.ELEMENT_NODE == module.getNodeType()) {
                            final String moduleName = module.getAttributes().getNamedItem("name").getNodeValue();
                            final NodeList groups = ((Element) module).getElementsByTagName("group");

                            for (int j = 0; j < groups.getLength(); ++j) {
                                final Node group = groups.item(j);

                                if (Node.ELEMENT_NODE == group.getNodeType()) {
                                    final String groupName = group.getAttributes().getNamedItem("name").getNodeValue();
                                    final NodeList resources = ((Element) group).getElementsByTagName("resource");

                                    for (int k = 0; k < resources.getLength(); ++k) {
                                        final Node resource = resources.item(k);

                                        if (Node.ELEMENT_NODE == resource.getNodeType()) {
                                            final NamedNodeMap attribs = resource.getAttributes();
                                            final String location = attribs.getNamedItem("location").getNodeValue();
                                            final String name = attribs.getNamedItem("name").getNodeValue();
                                            final String description = resource.getTextContent();

                                            entries.add(new ResourceEntry(moduleName, groupName, location, name, description));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            //
        } finally {
            IOUtils.closeQuietly(inputStm);
        }
    }

    @Override
    public List<IResourceEntry> getEntries() {

        initEntries();

        return entries;
    }

    @Override
    public List<IResourceEntry> getEntries(String module) {
        return getEntries(module, null);
    }

    @Override
    public List<IResourceEntry> getEntries(String module, String group) {
        final ArrayList<IResourceEntry> retList = new ArrayList<IResourceEntry>();

        initEntries();

        for (final Iterator<IResourceEntry> iter = entries.iterator(); iter.hasNext();) {
            final IResourceEntry entry = iter.next();

            if (((null == module) || entry.getModule().equals(module)) && ((null == group) || entry.getGroup().equals(group))) {
                retList.add(entry);
            }
        }

        return retList;
    }

    @Override
    public IResourceEntry getEntry(String module, String group, String title) {
        IResourceEntry retEntry = null;

        if ((null != module) && (null != group) && (null != title)) {
            initEntries();

            for (final Iterator<IResourceEntry> iter = entries.iterator(); (null == retEntry) && iter.hasNext();) {
                final IResourceEntry entry = iter.next();

                if (entry.getModule().equals(module) && entry.getGroup().equals(group) && entry.getTitle().equals(title)) {
                    retEntry = entry;
                }
            }
        }

        return retEntry;
    }

    @Override
    public InputStream getResource(IResourceEntry entry) {
        BufferedInputStream retStm = null;

        if (null != entry) {
            final IResourceEntry theEntry = getEntry(entry.getModule(), entry.getGroup(), entry.getTitle());

            if ((null != theEntry) && (null != theEntry.getLocation())) {
                final InputStream inputStm = this.getClass().getClassLoader().getResourceAsStream(theEntry.getLocation());

                if (null != inputStm) {
                    retStm = new BufferedInputStream(inputStm);
                }
            }
        }

        return retStm;
    }

    @Override
    public String getLocation() {
       return this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
    }
}
