
Name:          open-xchange-documents-monitoring
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Open-Xchange Documents Munin scripts
Requires:      open-xchange-core >= @OXVERSION@
Requires:      open-xchange-documents-backend >= @OXVERSION@
Requires:      munin-node, perl-JSON, perl-libwww-perl

%description
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains Open-Xchange Documents plugins for the Munin node.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. To see a real example of Munin in action, you can follow a link
from <http://munin.projects.linpro.no/> to a live installation.

Authors:
--------
    Open-Xchange


%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
TMPFILE=`mktemp /tmp/munin-node.configure.XXXXXXXXXX`
munin-node-configure --libdir /usr/share/munin/plugins/ --shell > $TMPFILE || :
if [ -f $TMPFILE ] ; then
  sh < $TMPFILE
  rm -f $TMPFILE
fi
/etc/init.d/munin-node restart || :

exit 0

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /usr/share/munin
/usr/share/munin/plugins/

%changelog
* Mon Feb 01 2021 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.5 release
* Fri Jan 15 2021 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.5 release
* Thu Dec 17 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.5 release
* Fri Nov 27 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.5 release
* Tue Oct 06 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.5 release
* Tue Jul 28 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.4 release
* Tue Jun 30 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.4 release
* Wed May 20 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.4 release
* Mon Feb 03 2020 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.4
* Thu Nov 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.3 release
* Wed Jun 19 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.3 release
* Fri May 10 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.1 release
* Mon Sep 10 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.1
* Mon Jun 25 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth preview for 7.10.0 release
* Thu Apr 19 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview for 7.10.0 release
* Tue Apr 03 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview for 7.10.0 release
* Tue Feb 20 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview for 7.10.0 release
* Fri Feb 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.0 release
* Mon Dec 04 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.0 release
* Tue May 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
Secpmd preview of 7.8.4 release
* Mon Apr 03 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second release cadidate for 7.8.3 release
* Thu Nov 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First release cadidate for 7.8.3 release
* Tue Nov 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview of 7.8.3 release
* Sat Oct 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.8.3 release
* Fri Oct 14 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.2 release
* Tue Jun 14 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Sat May 07 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.2 release
* Wed Mar 30 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.1 release
* Thu Mar 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.1 release
* Tue Mar 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.8.1 release
* Fri Mar 04 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview of 7.8.1 release
* Sat Feb 20 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.1 release
* Tue Feb 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.1 release
* Tue Jan 26 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.1 release
* Fri Oct 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.1 release
* Fri Oct 02 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.8.0 release
* Fri Sep 25 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.8.0 release
* Fri Sep 18 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.8.0 release
* Mon Sep 07 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.0 release
* Fri Aug 21 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.0 release
* Fri Aug 21 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.0 release
* Fri Jul 03 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2015-07-10
* Mon Mar 16 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.0 release
* Mon Mar 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Twelfth candidate for 7.6.2 release
* Fri Mar 06 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eleventh candidate for 7.6.2 release
* Wed Mar 04 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Tenth candidate for 7.6.2 release
* Tue Mar 03 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Ninth candidate for 7.6.2 release
* Tue Feb 24 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.6.2 release
* Wed Feb 11 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.2 release
* Fri Jan 30 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.2 release
* Tue Jan 27 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.2 release
* Fri Dec 12 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.2 release
* Fri Dec 05 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.2 release
* Fri Nov 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.2 release
* Tue Nov 04 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Ninth candidate for 7.6.1 release
* Fri Oct 31 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.2 release
* Thu Oct 30 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.X.0 release
* Mon Oct 27 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.1 release
* Tue Oct 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.1 release
* Thu Oct 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.2 release
* Tue Oct 14 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.1 release
* Fri Oct 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.1 release
* Thu Oct 02 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.1 release
* Tue Sep 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.1 release
* Wed Sep 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.1 release
* Wed Aug 20 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Tue Aug 19 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Mon Jul 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.1
* Tue Jul 15 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-07-21
* Tue Jul 08 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-07-17
* Tue Jun 24 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.0 release
* Fri Jun 20 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.0 release
* Fri Jun 13 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.0 release
* Fri May 30 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.0 release
* Fri May 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.0 release
* Mon May 05 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.0 release
* Fri Apr 11 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.0 release
* Wed Mar 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 Spreadsheet Final release
* Wed Feb 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 spreadsheet release
* Wed Feb 12 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.0
* Fri Feb 07 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.4.2 release
* Thu Feb 06 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.2 release
* Tue Feb 04 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.2 release
* Thu Jan 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.4.2 release
* Fri Jan 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.4.2 release
* Mon Dec 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 release
* Thu Dec 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.2
* Tue Nov 26 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.1 release
