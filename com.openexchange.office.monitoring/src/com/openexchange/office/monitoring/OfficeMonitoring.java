/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;
import com.openexchange.metrics.micrometer.Micrometer;
import com.openexchange.office.monitoring.api.IMonitoring;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.monitoring.CloseType;
import com.openexchange.office.tools.monitoring.OpenType;
import com.openexchange.office.tools.monitoring.OperationsType;
import com.openexchange.office.tools.monitoring.SaveType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link OfficeMonitoring}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=OfficeMonitoring")
@Service
public class OfficeMonitoring implements IMonitoring, InitializingBean {

    public static final String DOMAIN = "com.openexchange.office";

    private static final String NO_UNIT = null;
    private static final String UNIT_MS = "ms";
    private static final String GROUP = "appsuite.documents.";
    private static final String GROUP_CREATED = GROUP + "created.count";
    private static final String GROUP_MEDIAN_SIZE = GROUP + "median.size";
    private static final String GROUP_OPENED = GROUP + "opened.count";
    private static final String GROUP_CLOSED = GROUP + "closed.count";
    private static final String GROUP_SAVED = GROUP + "saved.count";
    private static final String GROUP_OPS = GROUP + "operations.count";
    private static final String GROUP_BKGNDSAVE = GROUP + "background.save";
    private static final String GROUP_OPS_TRANS = GROUP + "operations.transformed.count";
    private static final String GROUP_OPS_UNRES = GROUP + "operations.unresolvable.count";
    private static final String GROUP_OPS_UNRES_RELOAD = GROUP + "operations.unresolvable.reload.count";
    private static final Tags TAGS_TOTAL = Tags.of("type", "total");
    private static final Tags TAGS_TEXT = Tags.of("type", "text");
    private static final Tags TAGS_CALC = Tags.of("type", "spreadsheet");
    private static final Tags TAGS_PRES = Tags.of("type", "presentation");
    private static final Tags TAGS_FORMAT_TOTAL = Tags.of("format", "total");
    private static final Tags TAGS_FORMAT_OOXML = Tags.of("format", "OOXML");
    private static final Tags TAGS_FORMAT_BIN = Tags.of("format", "binary");
    private static final Tags TAGS_FORMAT_ODF = Tags.of("format", "ODF");
    private static final Tags TAGS_TYPE_TIMEOUT = Tags.of("type", "timeout");
    private static final Tags TAGS_INCOMING = Tags.of("type", "incoming");
    private static final Tags TAGS_DISTRIBUTED = Tags.of("type", "distributed");
    private static final String TEXT = ".text";
    private static final String CALC = ".spreadsheet";
    private static final String PRES = ".presentation";
    private static final String PRESENTER = ".presenter";

    @Autowired
    private Statistics m_statistics;

    public OfficeMonitoring() {
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CREATED + ".total", TAGS_TOTAL, "The total number of created documents.", NO_UNIT, this, (m) -> getDocumentsCreated_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CREATED, TAGS_TEXT, "The number of created documents.", NO_UNIT, this, (m) -> getDocumentsCreated_Text());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CREATED, TAGS_CALC, null, NO_UNIT, this, (m) -> getDocumentsCreated_Spreadsheet());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CREATED, TAGS_PRES, null, NO_UNIT, this, (m) -> getDocumentsCreated_Presentation());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_MEDIAN_SIZE + ".total", TAGS_TOTAL, "The totoal median size of opened documents.", NO_UNIT, this, (m) -> getDocumentsSizeMedian_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_MEDIAN_SIZE, TAGS_TEXT, "The median size of opened documents.", NO_UNIT, this, (m) -> getDocumentsSizeMedian_Text());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_MEDIAN_SIZE, TAGS_CALC, null, NO_UNIT, this, (m) -> getDocumentsSizeMedian_Spreadsheet());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_MEDIAN_SIZE, TAGS_PRES, null,  NO_UNIT, this, (m) -> getDocumentsSizeMedian_Presentation());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + TEXT + ".total", TAGS_FORMAT_TOTAL, "The total number of opened text documents.", NO_UNIT, this, (m) -> getDocumentsOpened_Text_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + TEXT, TAGS_FORMAT_OOXML, "The number of opened text documents.", NO_UNIT, this, (m) -> getDocumentsOpened_Text_OOXML());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + TEXT, TAGS_FORMAT_BIN, null, NO_UNIT, this, (m) -> getDocumentsOpened_Text_Binary());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + TEXT, TAGS_FORMAT_ODF, null, NO_UNIT, this, (m) -> getDocumentsOpened_Text_ODF());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + CALC + ".total", TAGS_FORMAT_TOTAL, "The total number of opened spreadsheet documents.", NO_UNIT, this, (m) -> getDocumentsOpened_Spreadsheet_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + CALC, TAGS_FORMAT_OOXML, "The number of opened spreadsheet documents.",  NO_UNIT, this, (m) -> getDocumentsOpened_Spreadsheet_OOXML());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + CALC, TAGS_FORMAT_BIN, null,  NO_UNIT, this, (m) -> getDocumentsOpened_Spreadsheet_Binary());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + CALC, TAGS_FORMAT_ODF, null,  NO_UNIT, this, (m) -> getDocumentsOpened_Spreadsheet_ODF());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRES + ".total", TAGS_FORMAT_TOTAL, "The total number of opened presentation documents.",  NO_UNIT, this, (m) -> getDocumentsOpened_Presentation_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRES, TAGS_FORMAT_OOXML, "The number of opened presentation documents.",  NO_UNIT, this, (m) -> getDocumentsOpened_Presentation_OOXML());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRES, TAGS_FORMAT_BIN, null,  NO_UNIT, this, (m) -> getDocumentsOpened_Presentation_Binary());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRES, TAGS_FORMAT_ODF, null,  NO_UNIT, this, (m) -> getDocumentsOpened_Presentation_ODF());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRES, TAGS_FORMAT_ODF, null,  NO_UNIT, this, (m) -> getDocumentsOpened_Presentation_ODF());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + TEXT, TAGS_TOTAL, "The number of closed text documents.", NO_UNIT, this, (m) -> getDocumentsClosed_Text_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + TEXT, TAGS_TYPE_TIMEOUT, null, NO_UNIT, this, (m) -> getDocumentsClosed_Text_Timeout());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + CALC, TAGS_TOTAL, "The number of closed spreadsheet documents.", NO_UNIT, this, (m) -> getDocumentsClosed_Spreadsheet_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + CALC, TAGS_TYPE_TIMEOUT, null, NO_UNIT, this, (m) -> getDocumentsClosed_Spreadsheet_Timeout());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + PRES, TAGS_TOTAL, "The number of closed presentation documents.", NO_UNIT, this, (m) -> getDocumentsClosed_Presentation_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + PRES, TAGS_TYPE_TIMEOUT, null, NO_UNIT, this, (m) -> getDocumentsClosed_Presentation_Timeout());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + TEXT + ".total", TAGS_TOTAL, "The total number of saved text documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Text_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + TEXT, Tags.of("type", "close"), "The number of saved text documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Text_Close());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + TEXT, Tags.of("type", "100Ops"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Text_100ops());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + TEXT, Tags.of("type", "15mins"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Text_15mins());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + TEXT, Tags.of("type", "flush"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Text_Flush());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + CALC + ".total", TAGS_TOTAL, "The total number of saved spreadsheet documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Spreadsheet_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + CALC, Tags.of("type", "close"), "The number of saved spreadsheet documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Spreadsheet_Close());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + CALC, Tags.of("type", "100Ops"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Spreadsheet_100ops());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + CALC, Tags.of("type", "15mins"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Spreadsheet_15mins());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + CALC, Tags.of("type", "flush"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Spreadsheet_Flush());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + PRES + ".total", TAGS_TOTAL, "The total number of saved presentation documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Presentation_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + PRES, Tags.of("type", "close"), "The number of saved presentation documents.", NO_UNIT, this, (m) -> getDocumentsSaved_Presentation_Close());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + PRES, Tags.of("type", "100Ops"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Presentation_100ops());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + PRES, Tags.of("type", "15mins"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Presentation_15mins());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_SAVED + PRES, Tags.of("type", "flush"), null, NO_UNIT, this, (m) -> getDocumentsSaved_Presentation_Flush());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + TEXT, TAGS_INCOMING, "The number of operations received for text documents.", NO_UNIT, this, (m) -> getDocumentsOperations_Text_Incoming());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + TEXT, TAGS_DISTRIBUTED, null, NO_UNIT, this, (m) -> getDocumentsOperations_Text_Distributed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + CALC, TAGS_INCOMING, "The number of operations received for spreadsheet documents.", NO_UNIT, this, (m) -> getDocumentsOperations_Spreadsheet_Incoming());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + CALC, TAGS_DISTRIBUTED, null, NO_UNIT, this, (m) -> getDocumentsOperations_Spreadsheet_Distributed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + PRES, TAGS_INCOMING, "The number of operations received for presentation documents.", NO_UNIT, this, (m) -> getDocumentsOperations_Presentation_Incoming());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + PRES, TAGS_DISTRIBUTED, null, NO_UNIT, this, (m) -> getDocumentsOperations_Presentation_Distributed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + PRESENTER, TAGS_INCOMING, "The number of operations received for presenter.", NO_UNIT, this, (m) -> getDocumentsOperations_Presenter_Incoming());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS + PRESENTER, TAGS_DISTRIBUTED, null, NO_UNIT, this, (m) -> getDocumentsOperations_Presenter_Distributed());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + "count.presentations.started", Tags.of("type", "remote"), null, NO_UNIT, this, (m) -> getDocumentsStarted_RemotePresenter_Total());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPENED + PRESENTER + ".total", TAGS_TOTAL, "The total median size of opened presenterdocuments.", NO_UNIT, this, (m) -> getDocumentsOpened_Presenter_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + PRESENTER + ".total", TAGS_TOTAL, "The total median size of closed presenter documents.", NO_UNIT, this, (m) -> getDocumentsClosed_Presenter_Total());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_CLOSED + PRESENTER, TAGS_TYPE_TIMEOUT, "The median size of closed documents due to timeout.", NO_UNIT, this, (m) -> getDocumentsClosed_Presenter_Timeout());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_BKGNDSAVE + ".count", Tags.of("type", "lastCycle"), "The number of saved document in the last background save cycle", NO_UNIT, this, (m) -> getBackgroundSave_SavedLastCycle());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_BKGNDSAVE + ".time", Tags.of("type", "lastCycle"), "The time needed for the last background save cycle.", UNIT_MS, this, (m) -> getBackgroundSave_TimeNeededLastCycle());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_BKGNDSAVE + ".average.time", Tags.of("type", "lastCycle"), "The average time for saving a document in the background save process.", UNIT_MS, this, (m) -> getBackgroundSave_AverageTimePerDoc());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_TRANS + ".total", TAGS_TOTAL, "The total number of transformed operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Total_ServerTransformed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_TRANS, TAGS_TEXT, "The number of transformed operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Text_ServerTransformed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_TRANS, TAGS_CALC, null, NO_UNIT, this, (m) -> getDocumentsOperations_Spreadsheet_ServerTransformed());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_TRANS, TAGS_PRES, null,  NO_UNIT, this, (m) -> getDocumentsOperations_Presentation_ServerTransformed());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES + ".total", TAGS_TOTAL, "The total number of unresolvable operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Total_UnresolvableTransformations());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES, TAGS_TEXT, "The number of unresolvable operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Text_UnresolvableTransformations());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES, TAGS_CALC, null, NO_UNIT, this, (m) -> getDocumentsOperations_Spreadsheet_UnresolvableTransformations());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES, TAGS_PRES, null,  NO_UNIT, this, (m) -> getDocumentsOperations_Presentation_UnresolvableTransformations());

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES_RELOAD + ".total", TAGS_TOTAL, "The total number of reloads due operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Total_UnresolvableTransformationReloads());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES_RELOAD, TAGS_TEXT, "The number of reloads due operations.", NO_UNIT, this, (m) -> getDocumentsOperations_Text_UnresolvableTransformationReloads());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES_RELOAD, TAGS_CALC, null, NO_UNIT, this, (m) -> getDocumentsOperations_Spreadsheet_UnresolvableTransformationReloads());
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP_OPS_UNRES_RELOAD, TAGS_PRES, null,  NO_UNIT, this, (m) -> getDocumentsOperations_Presentation_UnresolvableTransformationReloads());
    }

    // - IMonitoringCommon

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsCreated_Total()
     */
    @JMXBeanAttribute(name="AdvisoryLockInfo_Mode")
    @Override
    public String getAdvisoryLockInfo_Mode() {
        return m_statistics.getAdvisoryLockInfoMode();
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsCreated_Total()
     */
    @JMXBeanAttribute(name="DocumentsCreated_Total")
    @Override
    public long getDocumentsCreated_Total() {
        return m_statistics.getDocumentsCreated(null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsCreated_Text()
     */
    @JMXBeanAttribute(name="DocumentsCreated_Text")
    @Override
    public long getDocumentsCreated_Text() {
        return m_statistics.getDocumentsCreated(DocumentType.TEXT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsCreated_Spreadsheet()
     */
    @JMXBeanAttribute(name="DocumentsCreated_Spreadsheet")
    @Override
    public long getDocumentsCreated_Spreadsheet() {
        return m_statistics.getDocumentsCreated(DocumentType.SPREADSHEET);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsCreated_Presentation()
     */
    @JMXBeanAttribute(name="DocumentsCreated_Presentation")
    @Override
    public long getDocumentsCreated_Presentation() {
        return m_statistics.getDocumentsCreated(DocumentType.PRESENTATION);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IMonitoring#getMedianDocumentsSize_Total()
     */
    @JMXBeanAttribute(name="DocumentsSizeMedian_Total")
    @Override
    public long getDocumentsSizeMedian_Total() {
        return m_statistics.getDocumentsSizeMedian(null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getMedianDocumentsSize_Text_KB()
     */
    @JMXBeanAttribute(name="DocumentsSizeMedian_Text")
    @Override
    public long getDocumentsSizeMedian_Text() {
        return m_statistics.getDocumentsSizeMedian(DocumentType.TEXT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getMedianDocumentsSize_Spreadsheet()
     */
    @JMXBeanAttribute(name="DocumentsSizeMedian_Spreadsheet")
    @Override
    public long getDocumentsSizeMedian_Spreadsheet() {
        return m_statistics.getDocumentsSizeMedian(DocumentType.SPREADSHEET);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getMedianDocumentsSize_Presentation()
     */
    @JMXBeanAttribute(name="DocumentsSizeMedian_Presentation")
    @Override
    public long getDocumentsSizeMedian_Presentation() {
        return m_statistics.getDocumentsSizeMedian(DocumentType.PRESENTATION);
    }

    @JMXBeanAttribute(name="RestoreDocsCreated_Total")
    @Override
    public long getRestoreDocsCreated_Total() {
        return m_statistics.getRestoreDocCreated();
    }

    @JMXBeanAttribute(name="RestoreDocsRemoved_Total")
    @Override
    public long getRestoreDocsRemoved_Total() {
        return m_statistics.getRestoreDocRemoved();
    }

    @JMXBeanAttribute(name="RestoreDocs_Current")
    @Override
    public long getRestoreDocs_Current() {
        return m_statistics.getRestoreDocCurrent();
    }

    @JMXBeanAttribute(name="RestoreDocsRestored_Success")
    @Override
    public long getRestoreDocsRestored_Success() {
        return m_statistics.getRestoreDocRestoredSuccess();
    }

    @JMXBeanAttribute(name="RestoreDocsRestored_Failure")
    @Override
    public long getRestoreDocsRestored_Failure() {
        return m_statistics.getRestoreDocRestoredFailure();
    }

    // - IMonitoringText

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsOpened_Text_Total()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Text_Total")
    @Override
    public long getDocumentsOpened_Text_Total() {
        return m_statistics.getDocumentsOpened(DocumentType.TEXT, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsOpened_Text_Msx()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Text_OOXML")
    @Override
    public long getDocumentsOpened_Text_OOXML() {
        return m_statistics.getDocumentsOpened(DocumentType.TEXT, OpenType.MSX);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsOpened_Text_Binary()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Text_Binary")
    @Override
    public long getDocumentsOpened_Text_Binary() {
        return m_statistics.getDocumentsOpened(DocumentType.TEXT, OpenType.BINARY);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsOpened_Text_Odf()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Text_ODF")
    @Override
    public long getDocumentsOpened_Text_ODF() {
        return m_statistics.getDocumentsOpened(DocumentType.TEXT, OpenType.ODF);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsClosed_Text_Total()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Text_Total")
    @Override
    public long getDocumentsClosed_Text_Total() {
        return m_statistics.getDocumentsClosed(DocumentType.TEXT, CloseType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsClosed_Text_Timeout()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Text_Timeout")
    @Override
    public long getDocumentsClosed_Text_Timeout() {
        return m_statistics.getDocumentsClosed(DocumentType.TEXT, CloseType.TIMEOUT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsSaved_Text_Total()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Text_Total")
    @Override
    public long getDocumentsSaved_Text_Total() {
        return m_statistics.getDocumentsSaved(DocumentType.TEXT, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsSaved_Text_Close()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Text_Close")
    @Override
    public long getDocumentsSaved_Text_Close() {
        return m_statistics.getDocumentsSaved(DocumentType.TEXT, SaveType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsSaved_Text_100ops()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Text_100ops")
    @Override
    public long getDocumentsSaved_Text_100ops() {
        return m_statistics.getDocumentsSaved(DocumentType.TEXT, SaveType.OPS_100);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsSaved_Text_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Text_15mins")
    @Override
    public long getDocumentsSaved_Text_15mins() {
        return m_statistics.getDocumentsSaved(DocumentType.TEXT, SaveType.OPS_15MINS);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentsSaved_Text_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Text_Flush")
    @Override
    public long getDocumentsSaved_Text_Flush() {
        return m_statistics.getDocumentsSaved(DocumentType.TEXT, SaveType.FLUSH);
    }
    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentOperations_Text_Incoming()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Text_Incoming")
    @Override
    public long getDocumentsOperations_Text_Incoming() {
        return m_statistics.getDocumentsOperations(DocumentType.TEXT, OperationsType.INCOMING);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Text_Outgoing_Distributeted#getDocumentOperations_Text_Outgoing_Distributeted()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Text_Distributed")
    @Override
    public long getDocumentsOperations_Text_Distributed() {
        return m_statistics.getDocumentsOperations(DocumentType.TEXT, OperationsType.DISTRIBUTED);
    }

    // - IMonitoringSpreadsheet

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsOpened_Spreadsheet_Total()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Spreadsheet_Total")
    @Override
    public long getDocumentsOpened_Spreadsheet_Total() {
        return m_statistics.getDocumentsOpened(DocumentType.SPREADSHEET, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsOpened_Spreadsheet_Msx()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Spreadsheet_OOXML")
    @Override
    public long getDocumentsOpened_Spreadsheet_OOXML() {
        return m_statistics.getDocumentsOpened(DocumentType.SPREADSHEET, OpenType.MSX);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsOpened_Spreadsheet_Binary()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Spreadsheet_Binary")
    @Override
    public long getDocumentsOpened_Spreadsheet_Binary() {
        return m_statistics.getDocumentsOpened(DocumentType.SPREADSHEET, OpenType.BINARY);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsOpened_Spreadsheet_Odf()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Spreadsheet_ODF")
    @Override
    public long getDocumentsOpened_Spreadsheet_ODF() {
        return m_statistics.getDocumentsOpened(DocumentType.SPREADSHEET, OpenType.ODF);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsClosed_Spreadsheet_Total()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Spreadsheet_Total")
    @Override
    public long getDocumentsClosed_Spreadsheet_Total() {
        return m_statistics.getDocumentsClosed(DocumentType.SPREADSHEET, CloseType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsClosed_Spreadsheet_Timeout()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Spreadsheet_Timeout")
    @Override
    public long getDocumentsClosed_Spreadsheet_Timeout() {
        return m_statistics.getDocumentsClosed(DocumentType.SPREADSHEET, CloseType.TIMEOUT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsSaved_Spreadsheet_Total()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Spreadsheet_Total")
    @Override
    public long getDocumentsSaved_Spreadsheet_Total() {
        return m_statistics.getDocumentsSaved(DocumentType.SPREADSHEET, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsSaved_Spreadsheet_Close()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Spreadsheet_Close")
    @Override
    public long getDocumentsSaved_Spreadsheet_Close() {
        return m_statistics.getDocumentsSaved(DocumentType.SPREADSHEET, SaveType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsSaved_Spreadsheet_100ops()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Spreadsheet_100ops")
    @Override
    public long getDocumentsSaved_Spreadsheet_100ops() {
        return m_statistics.getDocumentsSaved(DocumentType.SPREADSHEET, SaveType.OPS_100);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsSaved_Spreadsheet_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Spreadsheet_15mins")
    @Override
    public long getDocumentsSaved_Spreadsheet_15mins() {
        return m_statistics.getDocumentsSaved(DocumentType.SPREADSHEET, SaveType.OPS_15MINS);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentsSaved_Spreadsheet_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Spreadsheet_Flush")
    @Override
    public long getDocumentsSaved_Spreadsheet_Flush() {
        return m_statistics.getDocumentsSaved(DocumentType.SPREADSHEET, SaveType.FLUSH);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentOperations_Spreadsheet_Incoming()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Spreadsheet_Incoming")
    @Override
    public long getDocumentsOperations_Spreadsheet_Incoming() {
        return m_statistics.getDocumentsOperations(DocumentType.SPREADSHEET, OperationsType.INCOMING);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Spreadsheet_Outgoing_Distributeted#getDocumentOperations_Spreadsheet_Outgoing_Distributeted()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Spreadsheet_Distributed")
    @Override
    public long getDocumentsOperations_Spreadsheet_Distributed() {
        return m_statistics.getDocumentsOperations(DocumentType.SPREADSHEET, OperationsType.DISTRIBUTED);
    }

    // - IMonitoring Presentation ----------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsOpened_Presentation_Total()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Presentation_Total")
    @Override
    public long getDocumentsOpened_Presentation_Total() {
        return m_statistics.getDocumentsOpened(DocumentType.PRESENTATION, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsOpened_Presentation_Msx()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Presentation_OOXML")
    @Override
    public long getDocumentsOpened_Presentation_OOXML() {
        return m_statistics.getDocumentsOpened(DocumentType.PRESENTATION, OpenType.MSX);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsOpened_Presentation_Binary()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Presentation_Binary")
    @Override
    public long getDocumentsOpened_Presentation_Binary() {
        return m_statistics.getDocumentsOpened(DocumentType.PRESENTATION, OpenType.BINARY);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsOpened_Presentation_Odf()
     */
    @JMXBeanAttribute(name="DocumentsOpened_Presentation_ODF")
    @Override
    public long getDocumentsOpened_Presentation_ODF() {
        return m_statistics.getDocumentsOpened(DocumentType.PRESENTATION, OpenType.ODF);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsClosed_Presentation_Total()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Presentation_Total")
    @Override
    public long getDocumentsClosed_Presentation_Total() {
        return m_statistics.getDocumentsClosed(DocumentType.PRESENTATION, CloseType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsClosed_Presentation_Timeout()
     */
    @JMXBeanAttribute(name="DocumentsClosed_Presentation_Timeout")
    @Override
    public long getDocumentsClosed_Presentation_Timeout() {
        return m_statistics.getDocumentsClosed(DocumentType.PRESENTATION, CloseType.TIMEOUT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.IInformation#getDocumentsSaved_Presentation_Total()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Presentation_Total")
    @Override
    public long getDocumentsSaved_Presentation_Total() {
        return m_statistics.getDocumentsSaved(DocumentType.PRESENTATION, null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsSaved_Presentation_Close()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Presentation_Close")
    @Override
    public long getDocumentsSaved_Presentation_Close() {
        return m_statistics.getDocumentsSaved(DocumentType.PRESENTATION, SaveType.CLOSE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsSaved_Presentation_100ops()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Presentation_100ops")
    @Override
    public long getDocumentsSaved_Presentation_100ops() {
        return m_statistics.getDocumentsSaved(DocumentType.PRESENTATION, SaveType.OPS_100);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsSaved_Presentation_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Presentation_15mins")
    @Override
    public long getDocumentsSaved_Presentation_15mins() {
        return m_statistics.getDocumentsSaved(DocumentType.PRESENTATION, SaveType.OPS_15MINS);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentsSaved_Presentation_15min()
     */
    @JMXBeanAttribute(name="DocumentsSaved_Presentation_Flush")
    @Override
    public long getDocumentsSaved_Presentation_Flush() {
        return m_statistics.getDocumentsSaved(DocumentType.PRESENTATION, SaveType.FLUSH);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentOperations_Presentation_Incoming()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Presentation_Incoming")
    @Override
    public long getDocumentsOperations_Presentation_Incoming() {
        return m_statistics.getDocumentsOperations(DocumentType.PRESENTATION, OperationsType.INCOMING);
    }

    /* (non-Javadoc)
     * @see com.openexchange.office.getDocumentsOperations_Presentation_Outgoing_Distributeted#getDocumentOperations_Presentation_Outgoing_Distributeted()
     */
    @JMXBeanAttribute(name="DocumentsOperations_Presentation_Distributed")
    @Override
    public long getDocumentsOperations_Presentation_Distributed() {
        return m_statistics.getDocumentsOperations(DocumentType.PRESENTATION, OperationsType.DISTRIBUTED);
    }

    @JMXBeanAttribute(name="DocumentsStarted_RemotePresenter_Total")
	@Override
	public long getDocumentsStarted_RemotePresenter_Total() {
		return m_statistics.getDocumentsStarted_RemotePresenterTotal();
	}

    @JMXBeanAttribute(name="DocumentsOpened_Presenter_Total")
	@Override
	public long getDocumentsOpened_Presenter_Total() {
		return m_statistics.getDocumentsOpenedPresenterTotal();
	}

    @JMXBeanAttribute(name="DocumentsClosed_Presenter_Total")
	@Override
	public long getDocumentsClosed_Presenter_Total() {
		return m_statistics.getDocumentsClosedPresenterTotal();
	}

    @JMXBeanAttribute(name="DocumentsClosed_Presenter_Timeout")
	@Override
	public long getDocumentsClosed_Presenter_Timeout() {
		return m_statistics.getDocumentsClosedPresenterTimeout();
	}

    @JMXBeanAttribute(name="DocumentsOperations_Presenter_Incoming")
	@Override
	public long getDocumentsOperations_Presenter_Incoming() {
		return m_statistics.getDocumentsOperationsPresenterIncoming();
	}

    @JMXBeanAttribute(name="DocumentsOperations_Presenter_Distributed")
	@Override
	public long getDocumentsOperations_Presenter_Distributed() {
		return m_statistics.getDocumentsOperationsPresenterDistributed();
	}

    @JMXBeanAttribute(name="RestoreDocsOpsMsgs_StoredLastCycle")
	@Override
	public long getRestoreDocsOpsMsgs_StoredLastCycle() {
		return m_statistics.getRestoreDocOpsMsgStored();
	}

    @JMXBeanAttribute(name="RestoreDocsOpsMsgs_StoreTimeNeededLastCycle")
	@Override
	public long getRestoreDocsOpsMsgs_StoreTimeNeededLastCycle() {
		return m_statistics.getRestoreDocOpsMsgStoredTime();
	}

    @JMXBeanAttribute(name="BackgroundSave_SavedLastCycle")
	@Override
	public long getBackgroundSave_SavedLastCycle() {
		return m_statistics.getBackgroundSaveLastCycleDocCount();
	}

    @JMXBeanAttribute(name="BackgroundSave_TimeNeededLastCycle")
	@Override
	public long getBackgroundSave_TimeNeededLastCycle() {
		return m_statistics.getBackgroundSaveLastCycleTime();
	}

    @JMXBeanAttribute(name="BackgroundSave_AverageTimePerDoc")
	@Override
	public long getBackgroundSave_AverageTimePerDoc() {
		return m_statistics.getBackgroundSaveAverageTimePerDoc();
	}

    @JMXBeanAttribute(name="BackgroundSave_Setting_MaxTimeForSave")
	@Override
	public long getBackgroundSave_Setting_MaxTimeForSave() {
		return m_statistics.getBackgroundSaveMaxTimeForSave();
	}

    @JMXBeanAttribute(name="BackgroundSave_Setting_MinTimeForFasterSave")
	@Override
	public long getBackgroundSave_Setting_MinTimeForFasterSave() {
		return m_statistics.getBackgroundSaveMinTimeFasterSave();
	}

    @JMXBeanAttribute(name="BackgroundSave_Setting_MinNumOfMessagesForSave_Text")
	@Override
	public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Text() {
		return m_statistics.getBackgroundSaveMinNumOfMsgForFasterSaveText();
	}

    @JMXBeanAttribute(name="BackgroundSave_Setting_MinNumOfMessagesForSave_Spreadsheet")
	@Override
	public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Spreadsheet() {
		return m_statistics.getBackgroundSaveMinNumOfMsgForFasterSaveSpreadsheet();
	}

    @JMXBeanAttribute(name="BackgroundSave_Setting_MinNumOfMessagesForSave_Presentation")
	@Override
	public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Presentation() {
		return m_statistics.getBackgroundSaveMinNumOfMsgForFasterSavePresentation();
	}

    @JMXBeanAttribute(name="RestoreDocsManagedFiles_RemovedLastCycle")
	@Override
	public long getRestoreDocsManagedFiles_RemovedLastCycle() {
		return m_statistics.getRestoreDocManagedFilesRemovedLastCycle();
	}

    @JMXBeanAttribute(name="RestoreDocsManagedFiles_InRemoveQueue")
	@Override
	public long getRestoreDocsManagedFiles_InRemoveQueue() {
		return m_statistics.getRestoreDocManagedFilesInRemoveQueue();
	}

    @JMXBeanAttribute(name="RestoreDocsManagedFiles_RuntimeLastCycle")
	@Override
	public long getRestoreDocsManagedFiles_RuntimeLastCycle() {
		return m_statistics.getRestoreDocManagedFilesRuntimeLastCycle();
	}

    @JMXBeanAttribute(name="RestoreDocsManagedFiles_Current")
	@Override
	public long getRestoreDocsManagedFiles_Current() {
		return m_statistics.getRestoreDocManagedFilesCurrent();
	}

	// - IMonitoringOT ---------------------------------------------------------

    @JMXBeanAttribute(name="DocumentsOperations_Total_ServerTransformed")
    @Override
    public long getDocumentsOperations_Total_ServerTransformed() {
        return m_statistics.getTransformationCount(null);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Text_ServerTransformed")
    @Override
    public long getDocumentsOperations_Text_ServerTransformed() {
        return m_statistics.getTransformationCount(DocumentType.TEXT);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Spreadsheet_ServerTransformed")
    @Override
    public long getDocumentsOperations_Spreadsheet_ServerTransformed() {
        return m_statistics.getTransformationCount(DocumentType.SPREADSHEET);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Presentation_ServerTransformed")
    @Override
    public long getDocumentsOperations_Presentation_ServerTransformed() {
        return m_statistics.getTransformationCount(DocumentType.PRESENTATION);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Total_UnresolvableTransformations")
	@Override
	public long getDocumentsOperations_Total_UnresolvableTransformations() {
		return m_statistics.getUnresolvableTransformations(null);
	}

    @JMXBeanAttribute(name="DocumentsOperations_Text_UnresolvableTransformations")
	@Override
	public long getDocumentsOperations_Text_UnresolvableTransformations() {
		return m_statistics.getUnresolvableTransformations(DocumentType.TEXT);
	}

    @JMXBeanAttribute(name="DocumentsOperations_Spreadsheet_UnresolvableTransformations")
	@Override
	public long getDocumentsOperations_Spreadsheet_UnresolvableTransformations() {
		return m_statistics.getUnresolvableTransformations(DocumentType.SPREADSHEET);
	}

    @JMXBeanAttribute(name="DocumentsOperations_Presentation_UnresolvableTransformations")
	@Override
	public long getDocumentsOperations_Presentation_UnresolvableTransformations() {
		return m_statistics.getUnresolvableTransformations(DocumentType.PRESENTATION);
	}

    @JMXBeanAttribute(name="DocumentsOperations_Total_UnresolvableTransformationReloads")
    @Override
    public long getDocumentsOperations_Total_UnresolvableTransformationReloads() {
        return m_statistics.getUnresolvableTransformationReloads(null);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Text_UnresolvableTransformationReloads")
    @Override
    public long getDocumentsOperations_Text_UnresolvableTransformationReloads() {
        return m_statistics.getUnresolvableTransformationReloads(DocumentType.TEXT);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Spreadsheet_UnresolvableTransformationReloads")
    @Override
    public long getDocumentsOperations_Spreadsheet_UnresolvableTransformationReloads() {
        return m_statistics.getUnresolvableTransformationReloads(DocumentType.SPREADSHEET);
    }

    @JMXBeanAttribute(name="DocumentsOperations_Presentation_UnresolvableTransformationReloads")
    @Override
    public long getDocumentsOperations_Presentation_UnresolvableTransformationReloads() {
        return m_statistics.getUnresolvableTransformationReloads(DocumentType.PRESENTATION);
    }

}

