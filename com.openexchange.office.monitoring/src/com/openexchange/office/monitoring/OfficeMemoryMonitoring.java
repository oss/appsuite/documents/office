/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.TimeZone;

import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.monitoring.api.IMonitoringMemory;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=OfficeMemoryMonitoring")
@Service
public class OfficeMemoryMonitoring implements IMonitoringMemory {
    public static final String DOMAIN = "com.openexchange.office";

    @JMXBeanAttribute(name="Observed_MemPool_Name")
	@Override
	public String getObserved_MemPool_Name() {
        return MemoryObserver.get().getObservedMemPoolName();
	}

    @JMXBeanAttribute(name="Observed_MemPool_MaxSize")
	@Override
	public long getObserved_MemPool_MaxSize() {
		return MemoryObserver.get().getObservedMemPoolMaxSize();
	}

    @JMXBeanAttribute(name="Observed_MemPool_ExceedThreshold")
	@Override
	public long getObserved_MemPool_ExceedThreshold() {
		return MemoryObserver.get().getMemPoolThreshold();
	}

    @JMXBeanAttribute(name="Observed_MemPool_PeakUsage")
	@Override
	public long getObserved_MemPool_PeakUsage() {
		return MemoryObserver.get().getObservedMemPoolPeakUsage();
	}

    @JMXBeanAttribute(name="Observed_MemPool_ExceedThresholdNotification_Count")
	@Override
	public long getObserved_MemPool_ExceedThresholdNotification_Count() {
		return MemoryObserver.get().getObservedMemPoolThresholdCount();
	}

    @JMXBeanAttribute(name="Heap_EstimatedFreeSize")
	@Override
	public long getHeap_EstimatedFreeSize() {
		return MemoryObserver.get().getCalcFreeHeapSize();
	}
    
    @JMXBeanAttribute(name="Heap_MaxSize")
	@Override
	public long getHeap_MaxSize() {
		return Runtime.getRuntime().maxMemory();
	}
    
    @JMXBeanAttribute(name="Heap_ExceedThreshold")
	@Override
	public long getHeap_ExceedThreshold() {
		return MemoryObserver.get().getMaxHeapExceedThreshold();
	}

    @JMXBeanAttribute(name="Heap_ExceedThresholdNotification_Count")
	@Override
    public long getHeap_ExceedThresholdNotification_Count() {
    	return MemoryObserver.get().getCountHeapThresholdNotifications();
    }

    @JMXBeanAttribute(name="Documents_NotLoadedDueTo_MemoryPressure_Count")
	@Override
    public long getDocuments_NotLoadedDueTo_MemoryPressure_Count() {
    	return MemoryObserver.get().getNumOfDocsNotLoadedDueToMemPressure();
    }

    @JMXBeanAttribute(name="GC_Triggered_Count")
	@Override
	public long getGC_Triggered_Count() {
		return MemoryObserver.get().getNumOfGCTriggered();
	}

    @JMXBeanAttribute(name="GC_Last_Triggered")
	@Override
	public String getGC_Last_Triggered() {
		final LocalDateTime gcLastRun = MemoryObserver.get().getGCLastTriggered();
		final ZonedDateTime zoneDateTime = gcLastRun.atZone(TimeZone.getTimeZone("UTC").toZoneId());
		return zoneDateTime.toString();
	}

    @JMXBeanAttribute(name="Documents_Requests_Deferred_DueTo_MemoryPressure")
    @Override
    public long getDocuments_Requests_Deferred_DueTo_MemoryPressure() {
        return MemoryObserver.get().getCountOfDocReqDeferredDueToMemPressure();
    }

    @JMXBeanAttribute(name="Documents_Sum_Of_Time_Requests_Waiting_For_GC_DueTo_MemoryPressure")
    @Override
    public long getDocuments_Sum_Of_Time_Requests_Waiting_For_GC_DueTo_MemoryPressure() {
        return MemoryObserver.get().getTimeWaitForGCDueToMemPressure();
    }
}
