/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring.api;

/**
 * {@link IMonitoringPresentation}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.3
 */
public interface IMonitoringPresentation {

    public long getDocumentsOpened_Presentation_Total();

    public long getDocumentsOpened_Presentation_OOXML();

    public long getDocumentsOpened_Presentation_Binary();

    public long getDocumentsOpened_Presentation_ODF();

    public long getDocumentsClosed_Presentation_Total();

    public long getDocumentsClosed_Presentation_Timeout();

    public long getDocumentsSaved_Presentation_Total();

    public long getDocumentsSaved_Presentation_Close();

    public long getDocumentsSaved_Presentation_100ops();

    public long getDocumentsSaved_Presentation_15mins();

    public long getDocumentsSaved_Presentation_Flush();

    public long getDocumentsOperations_Presentation_Incoming();

    public long getDocumentsOperations_Presentation_Distributed();

}
