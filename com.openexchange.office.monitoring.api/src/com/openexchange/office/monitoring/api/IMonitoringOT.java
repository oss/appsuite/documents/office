/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring.api;

/**
 * {@link IMonitoringOT}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.3

 */
public interface IMonitoringOT {

    long getDocumentsOperations_Total_ServerTransformed();

    long getDocumentsOperations_Text_ServerTransformed();

    long getDocumentsOperations_Spreadsheet_ServerTransformed();

    long getDocumentsOperations_Presentation_ServerTransformed();

    long getDocumentsOperations_Total_UnresolvableTransformations();

    long getDocumentsOperations_Text_UnresolvableTransformations();

    long getDocumentsOperations_Spreadsheet_UnresolvableTransformations();

    long getDocumentsOperations_Presentation_UnresolvableTransformations();

    long getDocumentsOperations_Total_UnresolvableTransformationReloads();

    long getDocumentsOperations_Text_UnresolvableTransformationReloads();

    long getDocumentsOperations_Spreadsheet_UnresolvableTransformationReloads();

    long getDocumentsOperations_Presentation_UnresolvableTransformationReloads();
}
