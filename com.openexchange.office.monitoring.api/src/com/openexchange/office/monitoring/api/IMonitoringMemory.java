/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring.api;

public interface IMonitoringMemory {

	public String getObserved_MemPool_Name();

	public long getObserved_MemPool_MaxSize();

	public long getObserved_MemPool_PeakUsage();
	
	public long getObserved_MemPool_ExceedThreshold();

	public long getObserved_MemPool_ExceedThresholdNotification_Count();

	public long getDocuments_NotLoadedDueTo_MemoryPressure_Count();

	public long getHeap_EstimatedFreeSize();

	public long getHeap_MaxSize();

	public long getHeap_ExceedThreshold();

	public long getHeap_ExceedThresholdNotification_Count();

	public long getGC_Triggered_Count();

	public String getGC_Last_Triggered();

	public long getDocuments_Requests_Deferred_DueTo_MemoryPressure();

	public long getDocuments_Sum_Of_Time_Requests_Waiting_For_GC_DueTo_MemoryPressure();
}

