/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.monitoring.api;



/**
 * {@link IInformation}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IMonitoringCommon {
    public String getAdvisoryLockInfo_Mode();

    public long getDocumentsCreated_Total();

    public long getDocumentsCreated_Text();

    public long getDocumentsCreated_Spreadsheet();

    public long getDocumentsCreated_Presentation();

    public long getDocumentsSizeMedian_Total();

    public long getDocumentsSizeMedian_Text();

    public long getDocumentsSizeMedian_Spreadsheet();

    public long getDocumentsSizeMedian_Presentation();

    public long getRestoreDocsCreated_Total();

    public long getRestoreDocsRemoved_Total();

    public long getRestoreDocs_Current();

    public long getRestoreDocsRestored_Success();

    public long getRestoreDocsRestored_Failure();

    public long getRestoreDocsManagedFiles_Current();

    public long getRestoreDocsManagedFiles_RemovedLastCycle();

    public long getRestoreDocsManagedFiles_InRemoveQueue();

    public long getRestoreDocsManagedFiles_RuntimeLastCycle();

    public long getRestoreDocsOpsMsgs_StoredLastCycle();

    public long getRestoreDocsOpsMsgs_StoreTimeNeededLastCycle();

    public long getBackgroundSave_SavedLastCycle();

    public long getBackgroundSave_TimeNeededLastCycle();

    public long getBackgroundSave_AverageTimePerDoc();

    public long getBackgroundSave_Setting_MaxTimeForSave();

    public long getBackgroundSave_Setting_MinTimeForFasterSave();

    public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Text();

    public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Spreadsheet();

    public long getBackgroundSave_Setting_MinNumOfMessagesForSave_Presentation();
}
