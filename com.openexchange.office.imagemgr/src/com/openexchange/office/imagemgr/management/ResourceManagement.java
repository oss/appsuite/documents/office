/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.imagemgr.impl.ResourceManagerImpl;
import com.openexchange.office.imagemgr.impl.ResourceManagers;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office:name=OfficeResourceManagerMonitoring")
public class ResourceManagement {

    @Autowired
    private ResourceManagers resourceManagers;

    @JMXBeanAttribute(name="ResourceManager")
    public Set<String> getRegisteredResourceManagers() {
        final Set<ResourceManagerImpl> regResMgrs = resourceManagers.getRegisteredResourceManagers();
        return regResMgrs.stream().map(r -> r.toString()).collect(Collectors.toSet());
    }

    @JMXBeanAttribute(name="ImagesMappedByResourceManager")
    public Map<String, Set<String> > getImageResourcesMapped() {
        final Map<String, Set<String>> result = new HashMap<>();
        final Set<ResourceManagerImpl> regResMgrs = resourceManagers.getRegisteredResourceManagers();
        regResMgrs.stream().forEach(r -> {
            result.put(r.getId(), r.getMappedResourcesInfo());
        });
        return result;
    }

}
