/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr;

import com.openexchange.office.tools.doc.DocumentFormat;


public class ResourceHelper {

    public static String getResourceName(final DocumentFormat docFormat, long uid, final String newExt) throws Exception {
        String resourceName = null;

        switch (docFormat) {
        case DOCX: {
            resourceName = "word/media/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }
        case PPTX: {
            resourceName = "ppt/media/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }
        case XLSX: {
            resourceName = "xl/media/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }
        case ODT: {
            resourceName = "Pictures/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }
        case ODS: {
            resourceName = "Pictures/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }
        case ODP: {
            resourceName = "Pictures/uid" + Long.toHexString(uid) + "." + newExt;
            break;
        }

        case NONE:
        case ODG:
        default: {
            break;
        }
        }

        return resourceName;
    }
}
