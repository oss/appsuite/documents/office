/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.office.tools.common.IOHelper;

/**
 * {@link Resource}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class Resource {

    public static final String RESOURCE_UID_PREFIX = "xid";
    private static final Logger LOG = LoggerFactory.getLogger(Resource.class);
    private static final String idPrefix = "office.resource0x";

    private ManagedFile m_managedFile = null;
    private String m_uid = null;

    public Resource(ManagedFile managedFile, String uid) {
    	this.m_managedFile = managedFile;
    	this.m_uid = uid;
    }
    
    @Override
    public String toString() {
        final StringBuilder tmp = new StringBuilder("Resource: ");
        tmp.append("ManagedId: ").append(getManagedId());
        if (m_managedFile != null) {
            tmp.append(", managed file: ").append(m_managedFile.getFile());
            tmp.append(", last access: ").append(convertToLocalDateTime(m_managedFile.getLastAccess()));
        }
        return tmp.toString();
    }

    /**
     * @return The content of this resource as a byte array
     */
    public byte[] getBuffer() {
        byte[] ret = null;

        if (null != m_managedFile) {
            try {
                final InputStream inputStream = m_managedFile.getInputStream();

                if (null != inputStream) {
                    try {
                        ret = IOUtils.toByteArray(inputStream);
                    } catch (IOException e) {
                        LOG.error(e.getMessage(), e);
                    } finally {
                        IOHelper.closeQuietly(inputStream);
                    }
                }
            } catch (OXException e) {
                LOG.error(e.getMessage());
            }
        }

        return ret;
    }

    /**
     * @return The UID of the contained data
     */
    public String getUID() {
        return m_uid;
    }

    /**
     * @return The id of the distributed managed file or null if the id is not valid
     */
    public String getManagedId() {
        return (null != m_uid) ? getManagedIdFromUID(getUID()) : null;
    }

    /**
     * @return The resource identifier of this resource as a string according to the following schema:
     * Resource.RESOURCE_UID_PREFIX + HexValueString
     */
    public String getResourceName() {
        return getResourceNameFromUID(m_uid);
    }

    /**
     *
     */
    public void touch() {
        if (null != m_managedFile) {
            m_managedFile.touch();
        }
    }

    /**
     *
     */
    public void delete() {
        if (null != m_managedFile) {
            m_managedFile.delete();
        }
    }

    /**
     * @param data the data where the SHA-256 hash should be calculated from
     * @return the SHA-256 hash of the provided data or null if not possible
     */
    public static String getSHA256(byte[] data) {
        String result = null;
        try {
            result = Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(data), true);
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Not possible to determine SHA-256 hash from data", e);
        }
        return result;
    }

    /**
     * @param hash of the managed file (is the SHA256 hash of the resource data)
     * @return
     */
    public static String getManagedIdFromUID(String hash) {
        return (idPrefix + hash);
    }

    /**
     * @param managedFileId the managed file id
     * @return
     */
    public static String getUIDFromManagedId(String managedFileId) {
        String uid = null;

        if (StringUtils.isNotEmpty(managedFileId) && managedFileId.startsWith(idPrefix)) {
            uid = managedFileId.substring(idPrefix.length());
        }

        return uid;
    }

    /**
     * @param uid The hash of a resource
     * @return The resource identifier for the given uid as a string according to the
     * following schema: RESOURCE_UID_PREFIX + HexValueString, or null in case of an invalid uid
     */
    public static String getResourceNameFromUID(String hash) {
        return ((null != hash) ? (RESOURCE_UID_PREFIX + hash) : null);
    }

    /**
     * @param resourceName A resource name with the following schema: Resource.RESOURCE_UID_PREFIX + HexValueString
     * @return The hash of a resource name, or 0 in case of an invalid resource name
     */
    public static String getUIDFromResourceName(String resourceName) {
        return ((null != resourceName) ? resourceName.substring(3) : null);
    }

    /**
     * @param sha256
     * @param resdata
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public static String getUIDFromData(String sha256, byte[] resdata) {
        return sha256;
    }

    /**
     * 
     * @param millisInEpoch
     * @return
     */
    private String convertToLocalDateTime(long millisInEpoch) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millisInEpoch), ZoneOffset.UTC).toString();
    }

}
