/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr;

import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.imagemgr.IResourceManager;

public class DocumentImageHelper {

	// return the proper imageUrl for imageData, the imageData is inserted into the resourceManager,
	// the filter needs to take care to insert the resource into the document package then
	// 
    // a imageFolder sting is expected to have following format:: "word/media/"
    public static String getImageUrlFromImageData(IResourceManager resourceManager, String imageData, String imageFolder) throws NoSuchAlgorithmException {
        String imageUrl = null;
        if(imageData != null) {
            String uid = resourceManager.addBase64(imageData);
            if(StringUtils.isNotEmpty(uid)) {
                imageUrl = imageFolder + Resource.RESOURCE_UID_PREFIX + uid;
                final String basePattern = "base64,";
                final String dataPattern = "data:image/";

                int pos = imageData.indexOf(basePattern);
                if (imageData.startsWith(dataPattern)) {
                    imageUrl += '.' + imageData.substring(dataPattern.length(), pos - 1);
                }
            }
        }
        return imageUrl;
    }
}
