/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr.impl;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.exception.OXException;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.imagemgr.ResourceException;
import com.openexchange.office.imagemgr.ResourceFactory;
import com.openexchange.tools.encoding.Base64;


/**
 * Implementation class that maintains resources added to its instance.
 * Resources represent binary data that are stored by managed files. The
 * implementation is responsible to create/remove manages files.
 * {@link IResourceManager}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class ResourceManagerImpl implements IResourceManager {

    private static final Logger log = LoggerFactory.getLogger(ResourceManagerImpl.class);
    private final Map<String, Resource> resources = new ConcurrentHashMap<>();
    private final String uuid = UUID.randomUUID().toString();
    private final String id;
    private final AtomicBoolean disposed = new AtomicBoolean(false);

    private ResourceLocker resourceLocker;

    @Autowired
    private ResourceFactory resourceFactory;
    
    /**
     * Initializes a new ResourceManagerImpl instance with a
     * preset identifier.
     */
    public ResourceManagerImpl(ResourceLocker resourceLocker) {
    	this.resourceLocker = resourceLocker;
    	this.id = "default";
	}

    /**
     * Initializes a new ResourceManagerImpl instance with an
     * arbitrary identifier.
     *
     * @param id an arbitrary identifier 
     */
    ResourceManagerImpl(ResourceLocker resourceLocker, String id) {
       	this.resourceLocker = resourceLocker;
        this.id = id;
    }

    /**
     * Provides an internal uuid to identify this instance.
     *
     * @return the uuid of this instance
     */
    @Override
    public String getUUID() {
    	return this.uuid;
    }

    public String getId() {
    	return this.id;
    }

	/**
     * Touches the contained resources so their referenced managed file instances would not be
     * garbage collected by the underlying ManagedFileManagement service.
     */
    void touch() {
        resources.forEach((k, v) -> {
            try {
                v.touch();
            } catch (Exception e) {
                log.error("ResourceManagerImpl: Exception caught trying to touch resource {} with managedId {}", v.getResourceName(), v.getManagedId());
            }
        });
    }

    /**
     * Provides a string view of the ResourceManagerImpl instance.
     */
    public String toString() {
        final StringBuilder tmp = new StringBuilder();
        tmp.append("uuid=").append(uuid);
        tmp.append(", id=").append(id);
        tmp.append(", locked=");
        if (resourceLocker != null) {
            tmp.append(resourceLocker.isLocked(this));
        } else {
            tmp.append("unknown");
        }
        tmp.append(", entries=").append(resources.size());

        return tmp.toString();
    }

    /**
     * Locks or unlocks all resources maintained by this ResourceManagerImpl
     * instance. This means that all resources are touched by a ResourceLocker
     * instance.
     *
     * @param lockResources
     */
    @Override
    public void lockResources(boolean lock) {
    	resourceLocker.lockResourceManager(this, lock);
    }

    /**
     * Adds a resource as base64 string describing the resource content.
     *
     * @param base64data a base64 
     * @return
     * @throws NoSuchAlgorithmException 
     */
    @Override
    public String addBase64(String base64Data) {
        String uid = null;

        final String basePattern = "base64,";
        int pos = base64Data.indexOf(basePattern);
        if ((pos != -1) && ((pos += basePattern.length()) < (base64Data.length() - 1))) {
            final byte[] byteData = Base64.decode(base64Data.substring(pos));
            if (byteData != null && byteData.length > 0) {
                uid = addResource(byteData);
            }
        }
        return uid;
    }

    /**
     * Adds a resource as a byte array containing the resource content.
     *
     * @param resdata
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public String addResource(byte[] resourceData) {
        return addResource(resourceFactory.create(resourceData));
    }

    /**
     * Adds a resource as byte array containing the resource content and
     * the calculated SHA256 hash.
     *
     * @param sha256 SHA256 hash of the resdata byte array
     * @param resdata
     * @return the unique key: sha256 of the resource data
     */
    public String addResource(String sha256, byte[] resourceData) {
        return addResource(resourceFactory.create(sha256, resourceData));
    }

    /**
     * Adds a Resource.
     *
     * @param resource
     * @return the unique key: sha256 of the resource data
     */
    public String addResource(Resource resource) {
        final String uid = (null != resource) ? resource.getUID() : null;

        if (null != uid) {
        	resources.putIfAbsent(uid, resource);
        }

        return ((null != uid) ? uid : null);
    }

    /**
     * Determines if this ResourcerManagerImpl contains a 
     * resource with the provided uid.
     *
     * @param uid a uid of the resource to be checked
     * @return true, if the resource for the given uid is contained within this instance
     */
    public boolean containsResource(String uid) {
       	return resources.containsKey(uid);
    }

    /**
     * Retrieves a resource with the given uid.
     *
     * @param uid
     * @return the resource for the given uid or null if the resource couldn't be found
     */
    public Resource getResource(String uid) {
      	return resources.get(uid);
    }

    /**
     * Retrieves the resource buffer from the given uid.
     *
     * @return the buffer with the content of the resource identified
     *  by the give uid or null if no content is available or the
     *  resource is unknown
     */
    public byte[] getResourceBuffer(String uid) {
		final Resource resource = getResource(uid);

		if (resource!=null) {
			return resource.getBuffer();
		}
		return null;
	}

    /**
     * Create a managed resource with the given managed resource id.
     * 
     * @param id the managed resource id
     * @return the resource for the given ManagedFile id or null if the resource couldn't be found
     */
    public Resource createManagedResource(String managedResourceId) {
        try {
			return resourceFactory.create(managedResourceId);
		} catch (OXException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
    }

    /**
     * Provides a hash table with all maintained resources data of this
     * ResourceManagerImpl instance.
     *
     * @return a hash table containing all maintained mappings from uid
     *  and resource content.
     */
    public Map<String, byte[]> getByteArrayTable() throws ResourceException {
        final Map<String, byte[]> ret = new Hashtable<>();
        final Map<String, String> notFound = new HashMap<>();

         resources.forEach((k, v) -> {
            if (v != null) {
                final byte[] buffer = v.getBuffer();
                if (buffer != null) {
                    ret.put(k, buffer);
                } else {
                    notFound.put(k, v.toString());
                }
            }
        });


        if (!notFound.isEmpty()) {
            log.warn("ResourceManagerImpl: Found entries without valid buffer");

            final StringBuilder tmp = new StringBuilder();
            if (log.isWarnEnabled()) {
                tmp.append("ResourceManagerImpl:");
                notFound.entrySet().stream().forEach(e -> {
                    tmp.append(" Entry: ").append(e.getKey());
                    tmp.append(" without valid buffer for ").append(e.getValue());
                    tmp.append(" found!");
                });
                log.warn(tmp.toString());
            }

            throw new ResourceException(tmp.toString());
        }

        return ret;
    }

    /**
     * Remove the resource with the given uid
     *
     * @param a resource uid to be removed
     */
    public void removeResource(String uid) {
        resources.remove(uid);
    }

    /**
     * Provides a Set of Strings which Resource info this
     * ResourceManagerImpl instance maintains.
     *
     * @return
     *  a Set containing the Resources this instance uses.
     */
    public Set<String> getMappedResourcesInfo() {
        Set<String> result = new HashSet<>();

        result = resources.values().stream()
                                   .map(v -> v.toString())
                                   .collect(Collectors.toSet());

        return result;
    }

	@Override
	public void dispose() {
		disposed.compareAndSet(false, true);
	}

	@Override
	public boolean isDisposed() {
		return disposed.get();
	}

}
