/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;

@Service
@RegisteredService(registeredClass=IResourceManagerFactory.class)
public class ResourceManagerFactory implements IResourceManagerFactory, OsgiBundleContextAware {

	@Autowired
	private ResourceLocker resourceLocker;

	@Autowired
	private ResourceManagers resourceManagers; 

	private OsgiBundleContextAndActivator bundleCtx;
	
	@Override
	public IResourceManager createInstance() {
		final ResourceManagerImpl resMgr = new ResourceManagerImpl(resourceLocker);
		resourceManagers.register(resMgr);
		bundleCtx.prepareObject(resMgr);
		return resMgr;
	}

	@Override
	public IResourceManager createInstance(String id) {
		final ResourceManagerImpl resMgr = new ResourceManagerImpl(resourceLocker, id);
		resourceManagers.register(resMgr);
		bundleCtx.prepareObject(resMgr);
		return resMgr;
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}	
}
