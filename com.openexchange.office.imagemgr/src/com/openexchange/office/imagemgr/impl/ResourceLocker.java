/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.tools.annotation.RegisteredService;


/**
 * {@link ResourceLocker}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@Service
@RegisteredService
public class ResourceLocker implements InitializingBean, DisposableBean {

    private static Logger LOG = LoggerFactory.getLogger(ResourceLocker.class);
    private static final long TOUCH_RESOURCE_TIMER_PERIOD = 60000;

    @Override
	public void afterPropertiesSet() throws Exception {
        // create the task to touch the held resources at the specified interval
        m_timerTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    final Set<ResourceManagerImpl> resourceManagers = getRegisteredResourceManagers();

                    synchronized (resourceManagers) {
                        // touch each collected resource
                        for (final Iterator<ResourceManagerImpl> resourceManagerIter = resourceManagers.iterator(); resourceManagerIter.hasNext();) {
                            resourceManagerIter.next().touch();
                        }
                    }
                } catch (Throwable t) {
                    ExceptionUtils.handleThrowable(t);
                    LOG.warn("Throwable caught in ResourceLocker service", t);
                }
            }
        };

        // start the periodic timer with the specified timeout
        m_timer.schedule(m_timerTask, TOUCH_RESOURCE_TIMER_PERIOD, TOUCH_RESOURCE_TIMER_PERIOD);
	}

    /**
     * @return
     */
    public Set<ResourceManagerImpl> getRegisteredResourceManagers() {
        return m_resourceManagers;
    }

    /**
     * @param resourceManager
     */
    public void lockResourceManager(ResourceManagerImpl resourceManager, boolean lock) {
        if (null != resourceManager) {
            synchronized (m_resourceManagers) {
                if (lock && !m_resourceManagers.contains(resourceManager)) {
                    m_resourceManagers.add(resourceManager);
                } else if (!lock && m_resourceManagers.contains(resourceManager)) {
                    m_resourceManagers.remove(resourceManager);
                }
            }
        }
    }

    public boolean isLocked(ResourceManagerImpl resourceManager) {
        if (null != resourceManager) {
            synchronized (m_resourceManagers) {
                return m_resourceManagers.contains(resourceManager);
            }
        }
        return false;
    }

    @Override
	public void destroy() throws Exception {
        LOG.info("ResourceLocker destroy called");

        if (null != m_timerTask)
            m_timerTask.cancel();

        m_timer.cancel();
        m_timer.purge();
	}

    // - Members -----------------------------------------------------------

    private final Timer m_timer = new Timer("com.openexchange.office.tools.ResourceLocker");

    private TimerTask m_timerTask = null;

    private final Set<ResourceManagerImpl> m_resourceManagers = new HashSet<>();
}
