/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr.impl;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.openexchange.office.tools.annotation.Async;
import com.openexchange.osgi.ExceptionUtils;

@Service
@Async(initialDelay=ResourceManagers.CHECK_UNAVAIL_REFS_FREQUENCY, period=ResourceManagers.CHECK_UNAVAIL_REFS_FREQUENCY, timeUnit=TimeUnit.MILLISECONDS)
public class ResourceManagers implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(ResourceManagers.class);

	protected static final long CHECK_UNAVAIL_REFS_FREQUENCY = 30000;

	private Map<String, WeakReference<ResourceManagerImpl>> createdResMgrs = new ConcurrentHashMap<>();

	public void register(ResourceManagerImpl resourceManager) {
		final WeakReference<ResourceManagerImpl> weakRef = new WeakReference<>(resourceManager);
		createdResMgrs.put(resourceManager.getUUID(), weakRef);
	}

	public Set<ResourceManagerImpl> getRegisteredResourceManagers() {
		return createdResMgrs.keySet().stream().map(k -> {
			final WeakReference<ResourceManagerImpl> weakRef = createdResMgrs.get(k);
			return weakRef.get();
		}).filter(r -> r != null).collect(Collectors.toSet());
	}

	@Override
	public void run() {
		try {
			createdResMgrs.entrySet().removeIf(e -> {
				return isNotValidInstance(e.getValue());
			});
		} catch (Throwable t) {
			ExceptionUtils.handleThrowable(t);
			LOG.warn("Exception caught trying to clean-up registered ResourceManagers", t);
		}
	}

	private boolean isNotValidInstance(WeakReference<ResourceManagerImpl> weakRef) {
		final ResourceManagerImpl instance = (weakRef != null) ? weakRef.get() : null;
		return (instance == null) ? true : instance.isDisposed();
	}
}
