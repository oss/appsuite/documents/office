/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.imagemgr;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.IOHelper;

@Service
@RegisteredService
public class ResourceFactory {
	
	private static final Logger log = LoggerFactory.getLogger(ResourceFactory.class);
	
	@Autowired
	private ManagedFileManagement fileMgr;
	
	public Resource create(byte[] buffer) {
		String sha256 = Resource.getSHA256(buffer);
		return create(sha256, buffer);
	}
	
	public Resource create(String sha256, byte[] buffer) {
		String uid = null;
		ManagedFile managedFile = null;
	    if (null != buffer) {
	    	uid = Resource.getUIDFromData(sha256, buffer);

	    	 // first try to find out, if we already have the managed file
	    	try {
	    		final String managedId = Resource.getManagedIdFromUID(uid);
	    		if (fileMgr.contains(managedId))
	    			managedFile = fileMgr.getByID(managedId);
	    	} catch (OXException e) {
              // nothing to do - exception means managed file is not known
	    	}

	    	if (null == managedFile) {
	    		// create a new managed file
	    		try {
	    			final InputStream inputStream = new ByteArrayInputStream(buffer);
	    			managedFile = fileMgr.createManagedFile(Resource.getManagedIdFromUID(uid), inputStream, true);
	    			IOHelper.closeQuietly(inputStream);
	    		} catch (OXException e) {
	    			log.error(e.getMessage(), e);
	    		}
	    	}
	    }
	    return new Resource(managedFile, uid);
	}
	
	public Resource create(String id) throws OXException {
		String uid = Resource.getUIDFromManagedId(id);
		if ((StringUtils.isNotEmpty(uid) && (fileMgr.contains(id)))) {
			ManagedFile managedFile = fileMgr.getByID(id);
			return new Resource(managedFile, uid);
        }
		return null;
    }
}	   
