/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Date;
import com.openexchange.file.storage.File;

public class RecentFileListTestHelper {

    public static File createAndSetFileMock(String fileId, String folderId, String fileName, String title, Date lastModified) {
        var file = mock(File.class);
        when(file.getId()).thenReturn(fileId);
        when(file.getFolderId()).thenReturn(folderId);
        when(file.getFileName()).thenReturn(fileName);
        when(file.getTitle()).thenReturn(title);
        when(file.getLastModified()).thenReturn(lastModified);
        return file;
    }
    
}
