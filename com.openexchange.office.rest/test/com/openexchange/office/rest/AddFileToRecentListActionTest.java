/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.tools.session.ServerSession;


public class AddFileToRecentListActionTest {

    @RegisterExtension
    public InUnitTestRule inUnitTestRule = new InUnitTestRule();

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private IDBasedFileAccessFactory idBaseFileAccessFactory;
    private RecentFileListManagerFactory recentFileListManagerFactory;
    private ParamValidatorService paramValidatorService;

    private AddFileToRecentListAction sut;

    @BeforeEach
    public void init() throws OXException {
        var unittestInformation = new RestUnittestInformation();
        unitTestBundle = new RestTestOsgiBundleContextAndUnitTestActivator(new RestBundleContextIdentificator(), unittestInformation);
        unitTestBundle.start();

        idBaseFileAccessFactory = unitTestBundle.getMock(IDBasedFileAccessFactory.class);
        recentFileListManagerFactory = unitTestBundle.getMock(RecentFileListManagerFactory.class);
        paramValidatorService = unitTestBundle.getMock(ParamValidatorService.class);

        sut = new AddFileToRecentListAction();
        unitTestBundle.injectDependencies(sut);
    }

    @Test
    public void performAddFileToRecentListAction() throws OXException {
        var fileId = "99/999";
        var folderId = "99";
        var fileName = "test.xlsx";
        var title = "title";
        var lastModified = new Date();
        var sessionId = UUID.randomUUID().toString();
        var type = "spreadsheet";

        var ajaxRequestData = mock(AJAXRequestData.class);
        var serverSession = mock(ServerSession.class);
        var idBasedFileAccess = mock(IDBasedFileAccess.class);
        var recentFileListManager = mock(RecentFileListManager.class);
        var file = RecentFileListTestHelper.createAndSetFileMock(fileId, folderId, fileName, title, lastModified);

        when(paramValidatorService.areAllParamsNonEmpty(any(AJAXRequestData.class), any(String[].class))).thenReturn(true);
        when(ajaxRequestData.getParameter(eq("type"))).thenReturn(type);
        when(ajaxRequestData.getParameter(eq("id"))).thenReturn(fileId);
        when(ajaxRequestData.getParameter(eq("session"))).thenReturn(sessionId);
        when(serverSession.getSessionID()).thenReturn(sessionId);
        when(idBaseFileAccessFactory.createAccess(serverSession)).thenReturn(idBasedFileAccess);
        when(recentFileListManagerFactory.create(serverSession)).thenReturn(recentFileListManager);
        when(idBasedFileAccess.getFileMetadata(eq(fileId), eq(FileStorageFileAccess.CURRENT_VERSION))).thenReturn(file);

        var ajaxRequestResult = sut.perform(ajaxRequestData, serverSession);

        assertNotNull(ajaxRequestResult);
    }

}
