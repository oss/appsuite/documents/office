package com.openexchange.office.rest;

import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.file.storage.TypeAware;


public interface TypeAwareFileStorageFolder extends FileStorageFolder, TypeAware {
    // nothing to do - just for mocking
}
