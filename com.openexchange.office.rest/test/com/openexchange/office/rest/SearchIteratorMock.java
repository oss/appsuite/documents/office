package com.openexchange.office.rest;

import java.util.ArrayList;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.tools.iterator.SearchIterator;


public class SearchIteratorMock<T> implements SearchIterator<T> {

    private List<T> elements = new ArrayList<T>();
    private int index = 0;

    public SearchIteratorMock(T element) {
        elements.add(element);
    }

    public SearchIteratorMock(T element1, T element2) {
        elements.add(element1);
        elements.add(element2);
    }

    @Override
    public boolean hasNext() throws OXException {
        return (index < elements.size());
    }

    @Override
    public T next() throws OXException {
        return elements.get(index++);
    }

    @Override
    public void close() {
        // nothing to do
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean hasWarnings() {
        return false;
    }

    @Override
    public void addWarning(OXException warning) {
        // nothing to do
    }

    @Override
    public OXException[] getWarnings() {
        return null;
    }

}
