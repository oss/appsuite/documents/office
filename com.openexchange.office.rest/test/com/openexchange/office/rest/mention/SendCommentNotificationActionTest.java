/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.file.storage.File;
import com.openexchange.user.User;

public class SendCommentNotificationActionTest {

	private SendCommentNotificationAction objectToTest = new SendCommentNotificationAction();

	private static final String TEST_EMAIL_1 = "testUser1@open-xchange.com";
	private static final int TEST_EMAIL_1_LENGTH = TEST_EMAIL_1.length();

	private static final String TEST_EMAIL_2 = "testUser2@openexchange.com";
	private static final int TEST_EMAIL_2_LENGTH = TEST_EMAIL_2.length();

	private static final MessageFormat MAIL_TO_MSG_FMT = new MessageFormat("<a href=\"mailto:{0}\">{1}</a>");

	@Test
	public void testMarkRecipientsInCommentOnlyRecipientWithText() {
		User user = Mockito.mock(User.class);
		Mockito.when(user.getMail()).thenReturn(TEST_EMAIL_1);

		List<CommentNotificationRecipientPositionInComment> positions = Arrays.asList(new CommentNotificationRecipientPositionInComment(user, 0, TEST_EMAIL_1_LENGTH));

		CommentNotificationRecipient commentNotificationRecipient = new CommentNotificationRecipient(user, positions);
		CommentNotificationRequestData requestDataObject = new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient), TEST_EMAIL_1, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args = {TEST_EMAIL_1, TEST_EMAIL_1};
		assertEquals(MAIL_TO_MSG_FMT.format(args), act);
	}

	@Test
	public void testMarkRecipientsInCommentOnlyRecipientWithTextWithDifferentEmailAddress() {
		User user = Mockito.mock(User.class);
		Mockito.when(user.getMail()).thenReturn(TEST_EMAIL_2);

		List<CommentNotificationRecipientPositionInComment> positions = Arrays.asList(new CommentNotificationRecipientPositionInComment(user, 0, TEST_EMAIL_1_LENGTH));

		CommentNotificationRecipient commentNotificationRecipient = new CommentNotificationRecipient(user, positions);
		CommentNotificationRequestData requestDataObject = new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient), TEST_EMAIL_1, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args = {TEST_EMAIL_2, TEST_EMAIL_1};
		assertEquals(MAIL_TO_MSG_FMT.format(args), act);
	}

	@Test
	public void testMarkRecipientsInCommentOneRecipientAndTextBefore() {
		User user = Mockito.mock(User.class);
		Mockito.when(user.getMail()).thenReturn(TEST_EMAIL_1);

		List<CommentNotificationRecipientPositionInComment> positions = Arrays.asList(new CommentNotificationRecipientPositionInComment(user, 19, TEST_EMAIL_1_LENGTH));

		CommentNotificationRecipient commentNotificationRecipient = new CommentNotificationRecipient(user, positions);
		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient), "Sehr geehrter Herr " + TEST_EMAIL_1, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args = {TEST_EMAIL_1, TEST_EMAIL_1};
		assertEquals("Sehr geehrter Herr " + MAIL_TO_MSG_FMT.format(args), act);
	}

	@Test
	public void testMarkRecipientsInCommentOneRecipientAndTextAfter() {
		User user = Mockito.mock(User.class);
		Mockito.when(user.getMail()).thenReturn(TEST_EMAIL_1);

		List<CommentNotificationRecipientPositionInComment> positions = Arrays.asList(new CommentNotificationRecipientPositionInComment(user, 0, TEST_EMAIL_1_LENGTH));

		CommentNotificationRecipient commentNotificationRecipient = new CommentNotificationRecipient(user, positions);
		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient), TEST_EMAIL_1 + " Mit freundlichen Gr��en", "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args = {TEST_EMAIL_1, TEST_EMAIL_1};
		assertEquals(MAIL_TO_MSG_FMT.format(args) + " Mit freundlichen Gr��en", act);
	}

	@Test
	public void testMarkRecipientsInCommentOneRecipientAndTextBeforeAndAfter() {
		User user = Mockito.mock(User.class);
		Mockito.when(user.getMail()).thenReturn(TEST_EMAIL_1);

		List<CommentNotificationRecipientPositionInComment> positions = Arrays.asList(new CommentNotificationRecipientPositionInComment(user, 19, TEST_EMAIL_1_LENGTH));

		CommentNotificationRecipient commentNotificationRecipient = new CommentNotificationRecipient(user, positions);
		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient),
						"Sehr geehrter Herr " + TEST_EMAIL_1 + " Mit freundlichen Gr��en", "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args = {TEST_EMAIL_1, TEST_EMAIL_1};
		assertEquals("Sehr geehrter Herr " + MAIL_TO_MSG_FMT.format(args) + " Mit freundlichen Gr��en", act);
	}

	@Test
	public void testMarkRecipientsInCommentTwoRecipientsWithOnlyTextBetweenThem() {
		User user1 = Mockito.mock(User.class);
		Mockito.when(user1.getMail()).thenReturn(TEST_EMAIL_1);
		User user2 = Mockito.mock(User.class);
		Mockito.when(user2.getMail()).thenReturn(TEST_EMAIL_2);

		CommentNotificationRecipientPositionInComment position1 = new CommentNotificationRecipientPositionInComment(user1, 0, TEST_EMAIL_1_LENGTH);
		CommentNotificationRecipientPositionInComment position2 = new CommentNotificationRecipientPositionInComment(user2, TEST_EMAIL_1_LENGTH + 5, TEST_EMAIL_2_LENGTH);

		CommentNotificationRecipient commentNotificationRecipient1 = new CommentNotificationRecipient(user1, Arrays.asList(position1));
		CommentNotificationRecipient commentNotificationRecipient2 = new CommentNotificationRecipient(user2, Arrays.asList(position2));

		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient1, commentNotificationRecipient2),
						TEST_EMAIL_1 + " and " + TEST_EMAIL_2, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args1 = {TEST_EMAIL_1, TEST_EMAIL_1};
		Object [] args2 = {TEST_EMAIL_2, TEST_EMAIL_2};
		assertEquals(MAIL_TO_MSG_FMT.format(args1) + " and " + MAIL_TO_MSG_FMT.format(args2), act);
	}

	@Test
	public void testMarkRecipientsInCommentTwoRecipientsWithoutAnyOtherText() {
		User user1 = Mockito.mock(User.class);
		Mockito.when(user1.getMail()).thenReturn(TEST_EMAIL_1);
		User user2 = Mockito.mock(User.class);
		Mockito.when(user2.getMail()).thenReturn(TEST_EMAIL_2);

		CommentNotificationRecipientPositionInComment position1 = new CommentNotificationRecipientPositionInComment(user1, 0, TEST_EMAIL_1_LENGTH);
		CommentNotificationRecipientPositionInComment position2 = new CommentNotificationRecipientPositionInComment(user2, TEST_EMAIL_1_LENGTH, TEST_EMAIL_2_LENGTH);

		CommentNotificationRecipient commentNotificationRecipient1 = new CommentNotificationRecipient(user1, Arrays.asList(position1));
		CommentNotificationRecipient commentNotificationRecipient2 = new CommentNotificationRecipient(user2, Arrays.asList(position2));

		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient1, commentNotificationRecipient2),
						TEST_EMAIL_1 + TEST_EMAIL_2, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args1 = {TEST_EMAIL_1, TEST_EMAIL_1};
		Object [] args2 = {TEST_EMAIL_2, TEST_EMAIL_2};
		assertEquals(MAIL_TO_MSG_FMT.format(args1) + MAIL_TO_MSG_FMT.format(args2), act);
	}

	@Test
	public void testMarkRecipientsInCommentTwoRecipientsTextBetweenAndBefore() {
		User user1 = Mockito.mock(User.class);
		Mockito.when(user1.getMail()).thenReturn(TEST_EMAIL_1);
		User user2 = Mockito.mock(User.class);
		Mockito.when(user2.getMail()).thenReturn(TEST_EMAIL_2);

		CommentNotificationRecipientPositionInComment position1 = new CommentNotificationRecipientPositionInComment(user1, 19, TEST_EMAIL_1_LENGTH);
		CommentNotificationRecipientPositionInComment position2 = new CommentNotificationRecipientPositionInComment(user2, TEST_EMAIL_1_LENGTH + 19 + 5, TEST_EMAIL_2_LENGTH);

		CommentNotificationRecipient commentNotificationRecipient1 = new CommentNotificationRecipient(user1, Arrays.asList(position1));
		CommentNotificationRecipient commentNotificationRecipient2 = new CommentNotificationRecipient(user2, Arrays.asList(position2));

		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient1, commentNotificationRecipient2),
						"Sehr geehrter Herr " + TEST_EMAIL_1 + " and " + TEST_EMAIL_2, "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args1 = {TEST_EMAIL_1, TEST_EMAIL_1};
		Object [] args2 = {TEST_EMAIL_2, TEST_EMAIL_2};
		assertEquals("Sehr geehrter Herr " + MAIL_TO_MSG_FMT.format(args1) + " and " + MAIL_TO_MSG_FMT.format(args2), act);
	}

	@Test
	public void testMarkRecipientsInCommentTwoRecipientsTextBetweenAndAfter() {
		User user1 = Mockito.mock(User.class);
		Mockito.when(user1.getMail()).thenReturn(TEST_EMAIL_1);
		User user2 = Mockito.mock(User.class);
		Mockito.when(user2.getMail()).thenReturn(TEST_EMAIL_2);

		CommentNotificationRecipientPositionInComment position1 = new CommentNotificationRecipientPositionInComment(user1, 0, TEST_EMAIL_1_LENGTH);
		CommentNotificationRecipientPositionInComment position2 = new CommentNotificationRecipientPositionInComment(user2, TEST_EMAIL_1_LENGTH + 5, TEST_EMAIL_2_LENGTH);

		CommentNotificationRecipient commentNotificationRecipient1 = new CommentNotificationRecipient(user1, Arrays.asList(position1));
		CommentNotificationRecipient commentNotificationRecipient2 = new CommentNotificationRecipient(user2, Arrays.asList(position2));

		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient1, commentNotificationRecipient2),
						TEST_EMAIL_1 + " and " + TEST_EMAIL_2 + " Mit freundlichen Gr��en", "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args1 = {TEST_EMAIL_1, TEST_EMAIL_1};
		Object [] args2 = {TEST_EMAIL_2, TEST_EMAIL_2};
		assertEquals(MAIL_TO_MSG_FMT.format(args1) + " and " + MAIL_TO_MSG_FMT.format(args2) + " Mit freundlichen Gr��en", act);
	}

	@Test
	public void testMarkRecipientsInCommentTwoRecpientTextBeforeAndAfterAndBetween() {
		User user1 = Mockito.mock(User.class);
		Mockito.when(user1.getMail()).thenReturn(TEST_EMAIL_1);
		User user2 = Mockito.mock(User.class);
		Mockito.when(user2.getMail()).thenReturn(TEST_EMAIL_2);

		CommentNotificationRecipientPositionInComment position1 = new CommentNotificationRecipientPositionInComment(user1, 19, TEST_EMAIL_1_LENGTH);
		CommentNotificationRecipientPositionInComment position2 = new CommentNotificationRecipientPositionInComment(user2, TEST_EMAIL_1_LENGTH + 19 + 5, TEST_EMAIL_2_LENGTH);

		CommentNotificationRecipient commentNotificationRecipient1 = new CommentNotificationRecipient(user1, Arrays.asList(position1));
		CommentNotificationRecipient commentNotificationRecipient2 = new CommentNotificationRecipient(user2, Arrays.asList(position2));

		CommentNotificationRequestData requestDataObject =
				new CommentNotificationRequestData(Arrays.asList(commentNotificationRecipient1, commentNotificationRecipient2),
						"Sehr geehrter Herr " + TEST_EMAIL_1 + " and " + TEST_EMAIL_2 + " Mit freundlichen Gr��en", "4711", Mockito.mock(File.class));
		String act = objectToTest.markRecipientsInComment(requestDataObject);
		Object [] args1 = {TEST_EMAIL_1, TEST_EMAIL_1};
		Object [] args2 = {TEST_EMAIL_2, TEST_EMAIL_2};
		assertEquals("Sehr geehrter Herr " + MAIL_TO_MSG_FMT.format(args1) + " and " + MAIL_TO_MSG_FMT.format(args2) + " Mit freundlichen Gr��en", act);
	}
}
