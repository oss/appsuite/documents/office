/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageFolderType;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.groupware.results.TimedResult;
import com.openexchange.office.document.api.TemplateFilesScanner;
import com.openexchange.office.document.api.TemplateFilesScannerFactory;
import com.openexchange.office.document.api.TemplateFilesScannerFactory.TemplateFilterType;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.RecentFileListHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.test.TestOsgiBundleContextAndUnitTestActivator;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;


public class GetTemplateAndRecentListActionTest {

    @RegisterExtension
    public InUnitTestRule inUnitTestRule = new InUnitTestRule();

    private TestOsgiBundleContextAndUnitTestActivator unitTestBundle;

    private TemplateFilesScannerFactory templateFilesScannerFactory;
    private IDBasedFileAccessFactory fileFactory;
    private IDBasedFolderAccessFactory folderFactory;
    private RecentFileListManagerFactory recentFileListManagerFactory;
    private FolderHelperServiceFactory folderHelperServiceFactory;

    private TemplateFilesScanner templateFilesScanner;

    private String fileId1 = "99/999";
    private String folderId1 = "99";
    private String fileName1 = "test.xlsx";
    private String title1 = "Title 1";
    private Date   lastModified1 = new Date();
    private String fileId2 = "99/888";
    private String folderId2 = "99";
    private String fileName2 = "deleted.xlsx";
    private String title2 = "Title 2";
    private Date   lastModified2 = new Date();
    private String trashFolderId = "3";

    private GetTemplateAndRecentListAction sut;

    @BeforeEach
    public void init() throws Exception {
        var unittestInformation = new RestUnittestInformation();
        unitTestBundle = new RestTestOsgiBundleContextAndUnitTestActivator(new RestBundleContextIdentificator(), unittestInformation);
        unitTestBundle.start();

        fileFactory = unitTestBundle.getMock(IDBasedFileAccessFactory.class);
        folderFactory = unitTestBundle.getMock(IDBasedFolderAccessFactory.class);
        recentFileListManagerFactory = unitTestBundle.getMock(RecentFileListManagerFactory.class);
        templateFilesScannerFactory = unitTestBundle.getMock(TemplateFilesScannerFactory.class);
        folderHelperServiceFactory = unitTestBundle.getMock(FolderHelperServiceFactory.class);

        templateFilesScanner = mock(TemplateFilesScanner.class);
        when(templateFilesScannerFactory.create(eq(TemplateFilterType.ALL_DOCUMENTS))).thenReturn(templateFilesScanner);

        sut = new GetTemplateAndRecentListAction();
        unitTestBundle.injectDependencies(sut);
        sut.afterPropertiesSet();
    }

    @Test
    public void performRecentListFilledNoEntriesOk() throws OXException, JSONException {
        var ajaxRequestData = mock(AJAXRequestData.class);
        var serverSession = mock(ServerSession.class);
        var sessionId = UUID.randomUUID().toString();

        var type = "spreadsheet";
        var appType = ApplicationType.APP_SPREADSHEET;
        var errorFromTemplateScanner = ErrorCode.NO_ERROR;

        var jsonRecentFileList = new ArrayList<JSONObject>();
        var recentFileListManager = mock(RecentFileListManager.class);

        when(ajaxRequestData.getParameter(eq("type"))).thenReturn(type);
        when(ajaxRequestData.getParameter(eq("session"))).thenReturn(sessionId);
        when(templateFilesScanner.searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class))).thenReturn(errorFromTemplateScanner);
        when(recentFileListManagerFactory.create(eq(serverSession))).thenReturn(recentFileListManager);
        when(recentFileListManager.readRecentFileList(eq(appType))).thenReturn(jsonRecentFileList);

        var ajaxResult = sut.perform(ajaxRequestData, serverSession);

        assertNotNull(ajaxResult);
        var json = ajaxResult.getResultObject();
        assertNotNull(json);
        if (json instanceof JSONObject jsonObj) {
            var templates = jsonObj.getJSONArray("templates");
            var recents = jsonObj.getJSONArray("recents");
            assertEquals(0, templates.length());
            assertEquals(0, recents.length());
        } else {
            fail("Unexpected type for result object!");
        }

        verify(templateFilesScanner, times(1)).searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class));
    }

    @SuppressWarnings({ "cast", "unchecked" })
    @Test
    public void performRecentListFilledWithOneEntryOk() throws OXException, JSONException {
        var ajaxRequestData = mock(AJAXRequestData.class);
        var serverSession = mock(ServerSession.class);
        var sessionId = UUID.randomUUID().toString();

        var type = "spreadsheet";
        var appType = ApplicationType.APP_SPREADSHEET;
        var errorFromTemplateScanner = ErrorCode.NO_ERROR;
        var file1 = RecentFileListTestHelper.createAndSetFileMock(fileId1, folderId1, fileName1, title1, lastModified1);

        var jsonRecentFileList = new ArrayList<JSONObject>();
        var recentFileListManager = mock(RecentFileListManager.class);
        jsonRecentFileList.add(RecentFileListHelper.getFileMetaDataAsJSON(file1));

        var fileAccess = mock(IDBasedFileAccess.class);
        var folderAccess = mock(IDBasedFolderAccess.class);
        var folderHelperService = mock(FolderHelperService.class);
        var metaDataForFiles = mock(TimedResult.class);
        var filesIter = new SearchIteratorMock<>(file1);

        when(ajaxRequestData.getParameter(eq("type"))).thenReturn(type);
        when(ajaxRequestData.getParameter(eq("session"))).thenReturn(sessionId);
        when(templateFilesScanner.searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class))).thenReturn(errorFromTemplateScanner);
        when(recentFileListManagerFactory.create(eq(serverSession))).thenReturn(recentFileListManager);
        when(recentFileListManager.readRecentFileList(eq(appType))).thenReturn(jsonRecentFileList);
        when(fileFactory.createAccess(eq(serverSession))).thenReturn(fileAccess);
        when(folderFactory.createAccess(eq(serverSession))).thenReturn(folderAccess);
        when(folderHelperServiceFactory.create(eq(serverSession))).thenReturn(folderHelperService);
        when(fileAccess.getDocuments(any(List.class), any(List.class))).thenReturn(metaDataForFiles);
        when(metaDataForFiles.results()).thenReturn(filesIter);

        var ajaxResult = sut.perform(ajaxRequestData, serverSession);

        assertNotNull(ajaxResult);
        var json = ajaxResult.getResultObject();
        assertNotNull(json);
        if (json instanceof JSONObject jsonObj) {
            var templates = jsonObj.getJSONArray("templates");
            var recents = jsonObj.getJSONArray("recents");
            assertEquals(0, templates.length());
            assertEquals(1, recents.length());
        } else {
            fail("Unexpected type for result object!");
        }

        verify(templateFilesScanner, times(1)).searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class));
        verify(fileAccess, never()).getFileMetadata(eq(fileId1), eq(FileStorageFileAccess.CURRENT_VERSION));
    }

    @SuppressWarnings({ "cast", "unchecked" })
    @Test
    public void performRecentListFilledWithTwoEntriesAndOneNotExistingOk() throws OXException, JSONException {
        var ajaxRequestData = mock(AJAXRequestData.class);
        var serverSession = mock(ServerSession.class);
        var sessionId = UUID.randomUUID().toString();

        var type = "spreadsheet";
        var appType = ApplicationType.APP_SPREADSHEET;
        var errorFromTemplateScanner = ErrorCode.NO_ERROR;
        var file1 = RecentFileListTestHelper.createAndSetFileMock(fileId1, folderId1, fileName1, title1, lastModified1);
        var file2 = RecentFileListTestHelper.createAndSetFileMock(fileId2, folderId2, fileName2, title2, lastModified2);

        var jsonRecentFileList = new ArrayList<JSONObject>();
        var recentFileListManager = mock(RecentFileListManager.class);
        jsonRecentFileList.add(RecentFileListHelper.getFileMetaDataAsJSON(file1));
        jsonRecentFileList.add(RecentFileListHelper.getFileMetaDataAsJSON(file2));

        var fileAccess = mock(IDBasedFileAccess.class);
        var folderAccess = mock(IDBasedFolderAccess.class);
        var folderHelperService = mock(FolderHelperService.class);
        var metaDataForFiles = mock(TimedResult.class);
        var filesIter = new SearchIteratorMock<>(file1);

        when(ajaxRequestData.getParameter(eq("type"))).thenReturn(type);
        when(ajaxRequestData.getParameter(eq("session"))).thenReturn(sessionId);
        when(templateFilesScanner.searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class))).thenReturn(errorFromTemplateScanner);
        when(recentFileListManagerFactory.create(eq(serverSession))).thenReturn(recentFileListManager);
        when(recentFileListManager.readRecentFileList(eq(appType))).thenReturn(jsonRecentFileList);
        when(fileFactory.createAccess(eq(serverSession))).thenReturn(fileAccess);
        when(folderFactory.createAccess(eq(serverSession))).thenReturn(folderAccess);
        when(folderHelperServiceFactory.create(eq(serverSession))).thenReturn(folderHelperService);
        when(fileAccess.getFileMetadata(eq(fileId1), eq(FileStorageFileAccess.CURRENT_VERSION))).thenReturn(file1);
        when(fileAccess.getFileMetadata(eq(fileId2), eq(FileStorageFileAccess.CURRENT_VERSION))).thenThrow(new OXException());

        when(fileAccess.getDocuments(any(List.class), any(List.class))).thenReturn(metaDataForFiles);
        when(metaDataForFiles.results()).thenReturn(filesIter);

        var ajaxResult = sut.perform(ajaxRequestData, serverSession);

        assertNotNull(ajaxResult);
        var json = ajaxResult.getResultObject();
        assertNotNull(json);
        if (json instanceof JSONObject jsonObj) {
            var templates = jsonObj.getJSONArray("templates");
            var recents = jsonObj.getJSONArray("recents");
            assertEquals(0, templates.length());
            assertEquals(1, recents.length());
        } else {
            fail("Unexpected type for result object!");
        }

        verify(templateFilesScanner, times(1)).searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class));
        verify(fileAccess, never()).getFileMetadata(eq(fileId1), eq(FileStorageFileAccess.CURRENT_VERSION));
    }

    @SuppressWarnings({ "cast", "unchecked" })
    @Test
    public void performRecentListFilledWithTwoEntriesAndOneResidesInTrashOk() throws OXException, JSONException {
        var ajaxRequestData = mock(AJAXRequestData.class);
        var serverSession = mock(ServerSession.class);
        var sessionId = UUID.randomUUID().toString();

        var type = "spreadsheet";
        var appType = ApplicationType.APP_SPREADSHEET;
        var errorFromTemplateScanner = ErrorCode.NO_ERROR;
        var file1 = RecentFileListTestHelper.createAndSetFileMock(fileId1, folderId1, fileName1, title1, lastModified1);
        var file2 = RecentFileListTestHelper.createAndSetFileMock(fileId2, folderId2, fileName2, title2, lastModified2);
        var file2InTrash = RecentFileListTestHelper.createAndSetFileMock(fileId2, trashFolderId, fileName2, title2, lastModified2);

        var jsonRecentFileList = new ArrayList<JSONObject>();
        var recentFileListManager = mock(RecentFileListManager.class);
        jsonRecentFileList.add(RecentFileListHelper.getFileMetaDataAsJSON(file1));
        jsonRecentFileList.add(RecentFileListHelper.getFileMetaDataAsJSON(file2));

        var fileAccess = mock(IDBasedFileAccess.class);
        var folderAccess = mock(IDBasedFolderAccess.class);
        var folderHelperService = mock(FolderHelperService.class);
        var metaDataForFiles = mock(TimedResult.class);
        var filesIter = new SearchIteratorMock<>(file1, file2InTrash);
        var trashFileStorageFolder = mock(TypeAwareFileStorageFolder.class);

        when(ajaxRequestData.getParameter(eq("type"))).thenReturn(type);
        when(ajaxRequestData.getParameter(eq("session"))).thenReturn(sessionId);
        when(templateFilesScanner.searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class))).thenReturn(errorFromTemplateScanner);
        when(recentFileListManagerFactory.create(eq(serverSession))).thenReturn(recentFileListManager);
        when(recentFileListManager.readRecentFileList(eq(appType))).thenReturn(jsonRecentFileList);
        when(fileFactory.createAccess(eq(serverSession))).thenReturn(fileAccess);
        when(folderFactory.createAccess(eq(serverSession))).thenReturn(folderAccess);
        when(folderHelperServiceFactory.create(eq(serverSession))).thenReturn(folderHelperService);
        when(folderHelperService.isFolderTrashOrInTrash(eq(trashFolderId))).thenReturn(true);
        when(folderAccess.getFolder(eq(trashFolderId))).thenReturn(trashFileStorageFolder);
        when(trashFileStorageFolder.getType()).thenReturn(FileStorageFolderType.TRASH_FOLDER);
        when(fileAccess.getFileMetadata(eq(fileId1), eq(FileStorageFileAccess.CURRENT_VERSION))).thenReturn(file1);
        when(fileAccess.getFileMetadata(eq(fileId2), eq(FileStorageFileAccess.CURRENT_VERSION))).thenReturn(file2);

        when(fileAccess.getDocuments(any(List.class), any(List.class))).thenReturn(metaDataForFiles);
        when(metaDataForFiles.results()).thenReturn(filesIter);

        var ajaxResult = sut.perform(ajaxRequestData, serverSession);

        assertNotNull(ajaxResult);
        var json = ajaxResult.getResultObject();
        assertNotNull(json);
        if (json instanceof JSONObject jsonObj) {
            var templates = jsonObj.getJSONArray("templates");
            var recents = jsonObj.getJSONArray("recents");
            assertEquals(0, templates.length());
            assertEquals(1, recents.length());
        } else {
            fail("Unexpected type for result object!");
        }

        verify(templateFilesScanner, times(1)).searchForTemplates(eq(serverSession), eq(type), any(JSONArray.class));
        verify(fileAccess, never()).getFileMetadata(eq(fileId1), eq(FileStorageFileAccess.CURRENT_VERSION));
    }

}
