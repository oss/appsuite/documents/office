/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.util.Collection;
import java.util.HashSet;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.office.document.api.TemplateFilesScannerFactory;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.tools.common.http.HttpModelConverterRegistry;
import com.openexchange.office.tools.common.osgi.context.test.UnittestInformation;
import com.openexchange.office.tools.service.conversion.OfficeConverter;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.validation.OfficeValidator;

public class RestUnittestInformation extends UnittestInformation {

    @Override
    protected Collection<Class<?>> getClassesToMock() {
        Collection<Class<?>> res =  new HashSet<>();
        res.add(IDBasedFileAccessFactory.class);
        res.add(IDBasedFolderAccessFactory.class);
        res.add(ParamValidatorService.class);
        res.add(RecentFileListManagerFactory.class);
        res.add(OfficeConverter.class);
        res.add(OfficeValidator.class);
        res.add(HttpModelConverterRegistry.class);
        res.add(TemplateFilesScannerFactory.class);
        res.add(FolderHelperServiceFactory.class);
        
        return res;
    }

}
