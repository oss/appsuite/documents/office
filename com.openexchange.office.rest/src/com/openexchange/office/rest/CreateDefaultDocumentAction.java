/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.tools.EmptyDocData;
import com.openexchange.office.document.tools.EmptyDocumentCache;
import com.openexchange.office.fields.DynamicFieldsService;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.core.FilterExceptionToErrorCode;
import com.openexchange.office.htmldoc.DocMetaData;
import com.openexchange.office.htmldoc.GenericHtmlDocumentBuilder;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.rest.operations.OperationTools;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.SessionIdCheck;
import com.openexchange.office.templatemgr.api.IResourceProvider;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.MissingParameter;
import com.openexchange.office.tools.common.TimeStampUtils;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.FileDescriptor;
import com.openexchange.office.tools.doc.OfficeDocumentTypeProperties;
import com.openexchange.office.tools.doc.PaperSize;
import com.openexchange.office.tools.monitoring.CreateEvent;
import com.openexchange.office.tools.monitoring.DocumentEvent;
import com.openexchange.office.tools.monitoring.ErrorType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

/**
 * {@link CreateDefaultDocumentAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */

/*
 * MH/KA comment this Action to make it compile
 * @Action(method = RequestMethod.GET, name = "createdefaultdocument", description =
 * "Create a default (empty) OOXML document based on given document type.", parameters = {
 * @Parameter(name = "target_folder_id", description = "Folder ID of the requested infoitem."),
 * @Parameter(name = "session", description = "A session ID previously obtained from the login module."),
 * @Parameter(name = "uid", description = "The unique id of the client application."),
 * @Parameter(name = "document_type", optional=true, description =
 * "Specifies the type of default document to be created. Possible values are: 'text' (default), 'spreadsheet' and 'presentation' ."),
 * responseDescription =
 * @Parameter(name = "as_template", optional = true, description = "Specifies that a template document should stay a template format, even if conversion is needed.") , },
 * "Response with timestamp: A JSON object containing the attributes of the created filestore item: 'id', 'folder_id', 'version' and 'filename'."
 * )
 */

@RestController
public class CreateDefaultDocumentAction extends DocumentRESTAction {

	private static final Logger LOG = LoggerFactory.getLogger(CreateDefaultDocumentAction.class);
	private static final String ENCRYPT_ACTION = "encrypt";
	private static final long MAX_TIMESPAN_FOR_SLOW_WRITE_MS = 1000;

	@Autowired
    private IResourceProvider resourceProvider;

	@Autowired
    private EmptyDocumentCache defaultDocCache;

	@Autowired
    private ImExporterAccessFactory imExporterFactory;

	@Autowired
	private FileHelperServiceFactory fileHelperServiceFactory;

	@Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;

	@Autowired
	private StorageHelperServiceFactory storageHelperServiceFactory;

	@Autowired
	private OXDocumentFactory oxDocumentFactory;

	@Autowired
	private DynamicFieldsService dynamicFieldsService;

	@Autowired
	private IDBasedFileAccessFactory idBasedFileAccessFactory;

	@Autowired(required=false)
	private IDocumentConverter documentConverter;

	@Autowired
	private GenericHtmlDocumentBuilder genericHtmlDocumentBuilder;

    @Autowired
    private Statistics statistics;

    private class TemplateResult {
        public final JSONObject jsonResult;
        public final DocumentType eventDocumentType;

        public TemplateResult(JSONObject jsonResult, DocumentType eventDocumentType) {
            this.jsonResult = jsonResult;
            this.eventDocumentType = eventDocumentType;
        }
    }

    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) {
        AJAXRequestResult requestResult = null;
        DocumentEvent documentEvent = null;
        DocumentType eventDocumentType = DocumentType.NONE;

        if ((null != request) && (null != session)) {
            // #60801: we need to check for an illegal CSRF attempt by checking the
            // correct sessionId; set errorcode to 400 in case of an invalid request
            if (SessionIdCheck.isNotValid(request.getParameter("session"), session)) {
                LOG.warn("CreateDefaultDocumentAction detected invalid session id - request rejected!");
                return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
            }

            JSONObject jsonResult = null;
            ErrorCode errorCode = ErrorCode.NO_ERROR;

            try {
                final String fileId = request.getParameter(RESTParameters.PARAMETER_FILE_ID);

                if (null == fileId) {
                    // create a new empty default document - this will be handled
                    // differently to speed-up the process
                    jsonResult = impl_createDefaultDocument(session, request);
                    eventDocumentType = getDocumentType(request);
                } else {
                    // create a new document based on a template document
                    TemplateResult templateResult = impl_createDocumentOnTemplate(session, request);
                    eventDocumentType = templateResult.eventDocumentType;
                    jsonResult = templateResult.jsonResult;
                }
            } catch (OXException e) {
                errorCode = ExceptionToErrorCode.map(e, errorCode, false);
                LOG.error(errorCode.getDescription(), e);
                jsonResult = errorCode.getAsJSONResultObject();
            } catch (FilterException e) {
                errorCode = FilterExceptionToErrorCode.map(e, errorCode);
                LOG.error(errorCode.getDescription(), e);
                jsonResult = errorCode.getAsJSONResultObject();
            } catch (Exception e) {
                 errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                 LOG.error("General exception caught while trying to process request", e);
            } finally {
                // create the final result and document event
                documentEvent = errorCode.isError() ? new DocumentEvent(eventDocumentType, ErrorType.CREATE) :
                    new CreateEvent(eventDocumentType);
                requestResult = new AJAXRequestResult(jsonResult);
            }
        }

        // update statistics with a possibly created event
        statistics.handleDocumentEvent(documentEvent);

        return requestResult;
    }

    /**
     * Determines the document type dependent on the request data from an AJAX
     * request.
     *
     * @param request
     *  The AJAX request data to be checked for the document type.
     *
     * @return
     *  The document type, can be DocmentType.NONE if the type couldn't be
     *  extracted.
     */
    DocumentType getDocumentType(AJAXRequestData request) {
        DocumentType result = DocumentType.NONE;

        if (null != request) {
            final String docTypeName = request.getParameter(RESTParameters.PARAMETER_DOCUMENT_TYPE).toLowerCase();

            if (docTypeName.equals(ApplicationType.APP_SPREADSHEET_STRING)) {
                result = DocumentType.SPREADSHEET;
            } else if (docTypeName.equals(ApplicationType.APP_PRESENTATION_STRING)) {
                result = DocumentType.PRESENTATION;
            } else {
                result = DocumentType.TEXT;
            }
        }
        return result;
    }


    /**
     * Handles the case that a new, empty document should be created, where we
     * can use several assumptions to speed-up the document generation.
     *
     * @param session
     *  The session of the client which requested to create a new empty document.
     *
     * @param request
     *  The AJAX request fill with the data what file should be used a template
     *  and where the file should be created.
     *
     * @return
     *  The JSON result for the request specific for creating a new empty document.
     *
     * @throws OXException
     * @throws FilterException
     */
    private JSONObject impl_createDefaultDocument(ServerSession session, AJAXRequestData request) throws Exception {
        JSONObject jsonResult = null;

        // check mandatory args and bail out with argument error, if required arg is missing
        final String missedReqParameter = MissingParameter.hasMissing(request,
                RESTParameters.PARAMETER_TARGET_FOLDER_ID,
                RESTParameters.PARAMETER_DOCUMENT_TYPE,
                RESTParameters.PARAMETER_TARGET_FILENAME);

        if (StringUtils.isNotEmpty(missedReqParameter)) {
            jsonResult = ErrorCode.GENERAL_ARGUMENTS_ERROR.getAsJSONResultObject();
            return jsonResult;
        }

        InputStream inputStream = null;
        InputStream resourceInputStream = null;

        final String docTypeName = request.getParameter(RESTParameters.PARAMETER_DOCUMENT_TYPE).toLowerCase();
        final String initialSheetname = request.getParameter(RESTParameters.PARAMETER_INITIAL_SHEETNAME);
        final String initialFormats = request.getParameter(RESTParameters.PARAMETER_INITIAL_FORMATS);
        final String initialLanguage = request.getParameter(RESTParameters.PARAMETER_INITIAL_LANGUAGE);

        // will be needed for speed-up access if creating a new empty document
        String htmlDoc = null;
        JSONObject documentOperations = null;

        // determine requested document type and corresponding extensionType
        final OfficeDocumentTypeProperties aDocTypeProps = OfficeDocumentTypeProperties.determineDefaultOfficeProperties(docTypeName);
        final String module = aDocTypeProps.getModule();
        final String extensionType = aDocTypeProps.getExtension();
        final String mimeType = aDocTypeProps.getMimeType();

        try {
            // look into the soft cache for stored empty default document data
            final User user = session.getUser();
            final String aUserLangCode = ((initialLanguage != null) && (initialLanguage.length() > 0)) ? initialLanguage : UserHelper.mapUserLanguageToLangCode(user.getPreferredLanguage());
            final IImporter importer = imExporterFactory.getImporterAccess(aDocTypeProps.getDocumentFormat());
            final EmptyDocData cacheData = defaultDocCache.getDocData(module, aUserLangCode);

            if (null != cacheData) {
                // cache hit - provide the fastLoad empty document data
                jsonResult = new JSONObject();
                jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, ErrorCode.NO_ERROR.getAsJSON());
                jsonResult.put(MessagePropertyKey.KEY_HTMLDOCUMENT, cacheData.getHtmlDocString());
                jsonResult.put(MessagePropertyKey.KEY_OPERATIONS, cacheData.getOperations());

                // create a input stream buffered by the byte array stored in the cache
                inputStream = new ByteArrayInputStream(cacheData.getDocumentData());
                LOG.debug("RT connection: cache hit for document type: {} data send from cache.", module);
            } else {
                LOG.debug("RT connection: cache miss for document type: {} data must be retrieved from file.", module);
                // no cache hit - load the default document
                // try to get default document from resource first
                if (null != resourceProvider) {
                    // !!! resource leak warning has been checked:
                    // if resourceInputStream is returned unchanged as the result
                    // InputStream, it will be closed by the caller; otherwise it will
                    // be closed instantly after retrieving the real result InputStream from the
                    // resourceInputStream after calling importer.getDefaultDocument(...) !!!
                    resourceInputStream = resourceProvider.getResource(resourceProvider.getEntry(module, "template", "Default"));
                }

                if (null != importer) {
                    final DocumentProperties docProps = new DocumentProperties();

                    docProps.put(DocumentProperties.PROP_CREATED_BY_DEFAULT_TEMPLATE, true);
                    // set user specific properties to create the correct document content (page size, document language, etc.)
                    docProps.put(DocumentProperties.PROP_CREATOR, user.getDisplayName());
                    docProps.put(DocumentProperties.PROP_USER_LANGUAGE, aUserLangCode);
                    if (module.equalsIgnoreCase(ApplicationType.APP_TEXT_STRING)) {
                        // Set page size for text only. Presentation uses a language independent screen page layout.
                        docProps.put(DocumentProperties.PROP_PAGESIZE_WIDTH, PaperSize.getPageSize(aUserLangCode).getWidth());
                        docProps.put(DocumentProperties.PROP_PAGESIZE_HEIGHT, PaperSize.getPageSize(aUserLangCode).getHeight());
                    }
                    if (initialSheetname != null) {
                        docProps.put(DocumentProperties.PROP_INITIAL_SHEETNAME, initialSheetname);
                    }
                    if (initialFormats != null) {
                        docProps.put(DocumentProperties.PROP_INITIAL_FORMATS, initialFormats);
                    }
                    inputStream = importer.getDefaultDocument(resourceInputStream, docProps);
                }

                if (resourceInputStream != inputStream) {
                    // Close resource input stream if default document stream
                    // was created by the import getDefaultDocument function. Don't
                    // do that if inputStm refers to resourceInputStream
                    IOHelper.closeQuietly(resourceInputStream);
                }
            }

            final File file = getNewFile(request, USE_DEFAULT_TARGET_FOLDER_ID, null, mimeType, extensionType, null, false);

            if (null == jsonResult) {
                // initialize json result object
                jsonResult = new JSONObject();
            }

            inputStream = impl_handleDynamicFields(inputStream, extensionType, request, jsonResult, session);

            final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
            final IDBasedFileAccess writeFileAccess = getFileAccess(session, request);

            long startTimeStampWriteFile = 0;
            long endTimeStampWriteFile = 0;

            ErrorCode errorCode = ErrorCode.NO_ERROR;
            if ((module.equalsIgnoreCase(ApplicationType.APP_TEXT_STRING) || module.equalsIgnoreCase(ApplicationType.APP_PRESENTATION_STRING)) &&
                (null == cacheData) && (null != inputStream)) {
                LOG.trace("createDefaultDocument for {} with cache miss", module);
                // just continue with the slower way - no cache hit
                final StorageHelperService aStorageHelper = storageHelperServiceFactory.create(session, file.getFolderId());
                final OXDocument oxDoc = oxDocumentFactory.createDocumentAccess(session, inputStream, file.getFileName(), mimeType, null, aStorageHelper);
                final boolean isFastLoad = genericHtmlDocumentBuilder.isFastLoadActive(module, session);

                LOG.trace("createDefaultDocument for {} before getOperations (cache miss)", module);
                documentOperations = oxDoc.getOperations(importer, null, isFastLoad);
                LOG.trace("createDefaultDocument for {} after getOperations (cache miss)", module);

                // create new buffered input stream to write it to the storage
                inputStream = new ByteArrayInputStream(oxDoc.getDocumentBuffer());

                // now we have to create the new document file using the input stream
                LOG.trace("createDefaultDocument for {} before writing stream to file (cache miss)", module);

                startTimeStampWriteFile = System.currentTimeMillis();
                errorCode = fileHelperService.createFileAndWriteStream(writeFileAccess, file, inputStream);
                endTimeStampWriteFile = System.currentTimeMillis();

                LOG.trace("createDefaultDocument for {} after writing stream to file (cache miss)", module);

                if (!errorCode.isError()) {
                    if (isFastLoad) {
                        // OperationReducer.reduceOperationCount(documentOperations);
                        htmlDoc = genericHtmlDocumentBuilder.buildHtmlDocument(module, documentOperations, new DocMetaData(file), session);
                        OperationTools.setOperationStateNumberToOperations(OXDocument.DEFAULT_DOCUMENT_ID, documentOperations);
                        defaultDocCache.setDocData(module,
                                                    aUserLangCode,
                                                    new EmptyDocData(htmlDoc, documentOperations.getJSONArray(MessagePropertyKey.KEY_OPERATIONS), oxDoc.getDocumentBuffer()));
                        jsonResult.put(MessagePropertyKey.KEY_HTMLDOCUMENT, htmlDoc);
                    } else {
                        OperationTools.setOperationStateNumberToOperations(OXDocument.DEFAULT_DOCUMENT_ID, documentOperations);
                    }
                    jsonResult.put(MessagePropertyKey.KEY_OPERATIONS, documentOperations.getJSONArray(MessagePropertyKey.KEY_OPERATIONS));
                }
            } else {
                LOG.trace("createDefaultDocument for {} (cache hit)", module);

                // now we have to create the new document file using the input stream
                startTimeStampWriteFile = System.currentTimeMillis();
                errorCode = fileHelperService.createFileAndWriteStream(writeFileAccess, file, inputStream);
                endTimeStampWriteFile = System.currentTimeMillis();

                LOG.trace("createDefaultDocument for {} after writing stream to file (cache hit)", module);

                if (!errorCode.isError() && (null != cacheData)) {
                    // set result data in case of a cache hit and the new file could be created
                    if (null != cacheData.getHtmlDocString()) {
                        jsonResult.put(MessagePropertyKey.KEY_HTMLDOCUMENT, cacheData.getHtmlDocString());
                    }
                    jsonResult.put(MessagePropertyKey.KEY_OPERATIONS, cacheData.getOperations());
                }
            }

            final long timeForWriteFile = TimeStampUtils.timeDiff(startTimeStampWriteFile, endTimeStampWriteFile);
            if (timeForWriteFile > MAX_TIMESPAN_FOR_SLOW_WRITE_MS) {
            	LOG.info("createDefaultDocument needed more than {}s to access filestore file ({}ms)", (MAX_TIMESPAN_FOR_SLOW_WRITE_MS / 1000), timeForWriteFile);
            }

            jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());
            if (!errorCode.isError()) {
                // add the file descriptor to the json object
                jsonResult.put(MessagePropertyKey.KEY_FILEDESCRIPTOR, FileDescriptor.createJSONObject(file, null));
            }
        } catch (final JSONException e) {
            LOG.error("Exception caught while trying to create default document", e);
            // if we encountered a JSON exception we have to provide a general
            // error to the client by just return a null result
            jsonResult = null;
        } finally {
            IOHelper.closeQuietly(inputStream);
        }

        return jsonResult;
    }

    /**
     * Handles optional dynamic fields which should be inserted.
     *
     * @param inputStream the document content stream
     * @param extensionType the extension type of the document
     * @param request the original request by the client
     * @param jsonResult the JSONObject instance that contains the current
     *                   result for the client
     * @param session the session of the requesting client
     * @return an updated document content stream or the original one if
     *         dynamic fields could not be applied
     */
    private InputStream impl_handleDynamicFields(final InputStream inputStream, String extensionType, final AJAXRequestData request, final JSONObject jsonResult, final ServerSession session) {
        InputStream result = inputStream;

        try {
            result = dynamicFieldsService.insertOptionalFields(inputStream, extensionType, request, jsonResult, session);
        } catch (Exception e) {
            LOG.debug("Handling optional fields is broken " + ErrorCode.GENERAL_ARGUMENTS_ERROR.getDescription(), e);

            try {
                jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA,  ErrorCode.GENERAL_ARGUMENTS_ERROR.getAsJSONResultObject().get(MessagePropertyKey.KEY_ERROR_DATA));
            } catch (final Exception je) {
                LOG.debug("Couldn't create JSON object while creating new document from template", je);
            }
        }

        return result;
    }

    /**
     * Handles the case that a new, empty document should be created, where we
     * can use several assumptions to speed-up the document generation.
     *
     * @param session
     *  The session of the client which requested to create a new empty document
     *  based on a arbitrary template file.
     *
     * @param request
     *  The AJAX request fill with the data what file should be used a template
     *  and where the file should be created.
     *
     * @return
     *  A template result containing the JSONObject to be sent to the client and
     *  specific information for event logging.
     *
     * @throws OXException
     * @throws FilterException
     */
    private TemplateResult impl_createDocumentOnTemplate(ServerSession session, AJAXRequestData request) throws Exception {
        TemplateResult result = null;
        DocumentType eventDocumentType = DocumentType.NONE;

        // check mandatory args and bail out with argument error, if required arg is missing
        final String missedReqParameter = MissingParameter.hasMissing(request,
            RESTParameters.PARAMETER_FILE_ID,
            RESTParameters.PARAMETER_TARGET_FOLDER_ID);

        if (StringUtils.isNotEmpty(missedReqParameter)) {
            result = new TemplateResult(ErrorCode.GENERAL_ARGUMENTS_ERROR.getAsJSONResultObject(), eventDocumentType);
            return result;
        }

        final String fileId = request.getParameter(RESTParameters.PARAMETER_FILE_ID);
        final String versionStr = request.getParameter(RESTParameters.PARAMETER_VERSION);
        final boolean asTemplate = Boolean.parseBoolean(request.getParameter(RESTParameters.PARAMETER_AS_TEMPLATE));
        String targetFolderId = request.getParameter(RESTParameters.PARAMETER_TARGET_FOLDER_ID);
        final String templateAccessId = fileId;
        String version = (null != versionStr) ? versionStr : "";
        InputStream inputStm = null;

        // determine requested document type and corresponding extensionType
        Map<String, String> conversionFormat = null;
        String extensionType;
        String mimeType;

        // Create document based on template file. It could be that we have to
        // convert the template document to the related ODF document format.
        if (version.length() == 0) {
            version = FileStorageFileAccess.CURRENT_VERSION;
        }

        final String reqCryptoAction = getCryptoAction(request);
        final boolean encryptTargetFile = determineEncryptTargetValue(reqCryptoAction);

        IDBasedFileAccess templateFileAccess;
        if (Boolean.parseBoolean(request.getParameter(RESTParameters.PARAMETER_ENCRYPTED_FILE))) {
            templateFileAccess = getFileAccess(session, request, "Decrypt");
        } else {
            templateFileAccess = idBasedFileAccessFactory.createAccess(session);
        }

        File templateFile = templateFileAccess.getFileMetadata(templateAccessId, version);
        final String templateFileName = templateFile.getFileName();

        extensionType = FileHelper.getExtension(templateFileName, true);
        mimeType = templateFile.getFileMIMEType();
        conversionFormat = DocumentFormatHelper.getConversionFormatInfo(mimeType, extensionType);
        boolean binaryConverted = false;
        if (conversionFormat != null) {
            if (null != documentConverter) {
                final AJAXRequestData loadRequest = new AJAXRequestData();
                loadRequest.putParameter(RESTParameters.PARAMETER_ID, fileId);
                loadRequest.putParameter(RESTParameters.PARAMETER_FOLDER_ID, templateFile.getFolderId());
                loadRequest.putParameter(RESTParameters.PARAMETER_VERSION, version);
                loadRequest.putParameter(RESTParameters.PARAMETER_FILENAME_DOCCONV, templateFileName);

                final InputStream documentInputStm = templateFileAccess.getDocument(templateAccessId, version);

                if (null != documentInputStm) {
                    HashMap<String, Object> jobProperties = new HashMap<>(4);
                    HashMap<String, Object> resultProperties = new HashMap<>(8);
                    final String filterShortName = conversionFormat.get(Properties.PROP_FILTER_SHORT_NAME);
                    final String convertedExtensionType = conversionFormat.get(Properties.PROP_INPUT_TYPE);

                    jobProperties.put(Properties.PROP_INPUT_STREAM, documentInputStm);
                    jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, filterShortName);

                    /* TODO (KA): set InputUrl property, if known later
                    if (null != inputURL) {
                        jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                    }
                    */

                    if (null != templateFileName) {
                        jobProperties.put(Properties.PROP_INFO_FILENAME, templateFileName);
                    }

                    // this is a user request in every case
                    jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);

                    inputStm = documentConverter.convert(filterShortName, jobProperties, resultProperties, session);
                    IOHelper.closeQuietly(documentInputStm);

                    if (null != inputStm) {
                    	binaryConverted = true;
                    }

                    // set new mime type and extensionType
                    mimeType = conversionFormat.get(Properties.PROP_MIME_TYPE);
                    extensionType = conversionFormat.get(Properties.PROP_INPUT_TYPE);

                    if (asTemplate) {
                        // target format should be a template => retrieve mime & extension type
                        final Map<String, String> templateFormatInfo = DocumentFormatHelper.getTemplateFormatInfoForDocument(mimeType, convertedExtensionType);
                        if (null != templateFormatInfo) {
                            mimeType = templateFormatInfo.get(Properties.PROP_MIME_TYPE);
                            extensionType = templateFormatInfo.get(Properties.PROP_INPUT_TYPE);
                        }
                    }
                }
            }

            eventDocumentType = DocumentType.valueOf(conversionFormat.get("DocumentType"));
        } else {
            inputStm = templateFileAccess.getDocument(templateAccessId, version);
        }

        final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
        boolean canWriteToFolder = folderHelperService.folderHasWriteAccess(targetFolderId);
        boolean canCreateFilesInFolder = folderHelperService.folderHasCreateAccess(targetFolderId);

        if (!canWriteToFolder || !canCreateFilesInFolder) {
            // #43749: Checking the write/create permissions is not dependent on the argument
            // preserveFileName! Therefore remove the check for preserveFileName.
            //
            // We cannot create files/write to the folder which contains our source template document (Edit as New).
            // Use a fall-back and create the document in the user's documents folder instead.
            // If this doesn't work we give up and provide an error.
            targetFolderId = folderHelperService.getUserDocumentsFolderId(session);
        }

        // ATTENTION: This method MUST be called after every detail where and how the new file must
        // be created are clarified. In case we don't have write permissions we have to use the users
        // default folder.
        // See #52305 & #52136 which are regressions
        final File file = getNewFile(request, targetFolderId, null, mimeType, extensionType, templateFile, binaryConverted);

        ErrorCode errorCode = ErrorCode.NO_ERROR;
        JSONObject jsonResult = new JSONObject();

        try {
            if (null != inputStm) {
            	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

            	IDBasedFileAccess writeFileAccess = null;
                if (encryptTargetFile) {
                    writeFileAccess = getFileAccess(session, request, "Encrypt");
                } else {
                    writeFileAccess = idBasedFileAccessFactory.createAccess(session);
                }

                errorCode = fileHelperService.createFileAndWriteStream(writeFileAccess, file, inputStm);

                if (!errorCode.isError()) {
                    // set the file descriptor
                    jsonResult.put(MessagePropertyKey.KEY_FILEDESCRIPTOR, FileDescriptor.createJSONObject(file, null));
                }
            } else {
                errorCode = ErrorCode.CREATEDOCUMENT_CANNOT_READ_DEFAULTTEMPLATEFILE_ERROR;
                // error case: we don't have a input stream
                if (null != conversionFormat) {
                    errorCode = ErrorCode.CREATEDOCUMENT_CONVERSION_FAILED_ERROR;
                } else if (null != fileId) {
                    errorCode = ErrorCode.CREATEDOCUMENT_CANNOT_READ_TEMPLATEFILE_ERROR;
                }
                LOG.warn(errorCode.getDescription());
            }
            jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());
            result = new TemplateResult(jsonResult, eventDocumentType);
        } catch (JSONException e) {
            LOG.error("Exception caught while createDocumentOnTemplate was processed", e);
            // if we encountered a JSON exception we have to provide a general
            // error to the client by just return a null result
            jsonResult = null;
        } finally {
        	IOHelper.closeQuietly(inputStm);
        }

        return result;
    }

    private boolean determineEncryptTargetValue(String reqCryptoAction) {
        return ENCRYPT_ACTION.equalsIgnoreCase(reqCryptoAction);
    }

}
