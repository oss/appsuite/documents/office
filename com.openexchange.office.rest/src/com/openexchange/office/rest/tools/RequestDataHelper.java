/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.tools;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.ajax.requesthandler.AJAXRequestData;

/**
 * A helper class to retrieve the request data from the AJAXRequest.
 *
 * {@link RequestDataHelper}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.8.0
 */
public class RequestDataHelper {
	private static final Logger LOG = LoggerFactory.getLogger(RequestDataHelper.class);
	public static final String KEY_REQUESTDATA = "requestdata";

	private JSONObject requestData;

	/**
	 * Initializes a new {@link RequestDataHelper}.
	 * 
	 * @param data
	 *            The AJAX request data received by the backend.
	 */
	public RequestDataHelper(final AJAXRequestData data) {
		this.requestData = null;

		if (null != data) {
			try {
				requestData = new JSONObject(data.getParameter(KEY_REQUESTDATA));
			} catch (JSONException e) {
				LOG.error("JSONException caught trying to retrieve request data from AJAXRequestData instance!", e);
			}
		}
	}

	/**
	 * Retrieves the request data as JSON object.
	 *
	 * @return The request data as JSON object or null.
	 */
	public final JSONObject getJSON() {
		return requestData;
	}

	/**
	 * Retrieves a parameter from the request data.
	 *
	 * @param paramName
	 *            The name of the parameter.
	 * @return The value of the parameter or null.
	 */
	public final String getParameter(final String paramName) {
		String value = null;

		if (null != requestData) {
			value = requestData.optString(paramName, null);
		}
		return value;
	}

}
