/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.office.tools.service.validation.annotation.NotNull;

public class RecentFileListHelper {
    public static final String UNKNOWN = "unknown";

    private static final Logger LOG = LoggerFactory.getLogger(RecentFileListHelper.class);
    private static List<Field> m_fields;

    static {
        List<Field> fields = new ArrayList<>(5);
        // initialize fields array with good to know fields
        fields.add(Field.ID);
        fields.add(Field.FOLDER_ID);
        fields.add(Field.FILENAME);
        fields.add(Field.LAST_MODIFIED);
        fields.add(Field.TITLE);
        m_fields = fields;
    }

    private RecentFileListHelper() {
        // nothing to do
    }

    /**
     * Retrieves the meta data of provided file ids.
     *
     * @param fileAccess
     *  A file access instance to be used to retrieve the meta data.
     *
     * @param ids
     *  A list of file ids for which meta data should be retrieved.
     *
     * @return
     *  The map with key id and value JSONObject which includes the
     *  meta data of the file ids provided. If there is no mapping for
     *  a provided key it must be assumed that the file is not available
     *  anymore (deleted or access rights have been modified).
     *
     * @throws OXException
     * @throws JSONException
     */
    public static Map<String, JSONObject> getMetaDataForFiles(@NotNull IDBasedFileAccess fileAccess, @NotNull List<String> ids) throws OXException, JSONException {
        HashMap<String, JSONObject> newMetaDataMap = null;
        // now check the meta data of the files using file access
        var fileResult = fileAccess.getDocuments(ids, m_fields);
        if (null != fileResult) {

            newMetaDataMap = new HashMap<String, JSONObject>();
            var iter = fileResult.results();

            // create hash map for faster access to store new meta data
            while (iter.hasNext()) {
                var file = iter.next();

                var id = file.getId();
                if (null != id) {
                    newMetaDataMap.put(id, getFileMetaDataAsJSON(file));
                }
            }
        }

        return newMetaDataMap;
    }

    /**
     * Checks the list of file ids for their meta data using a slower access
     * variant.
     *
     * @param fileAccess
     *  The file access instance to be used to retrieve meta data.
     *
     * @param ids
     *  The list of file ids where the meta data should be retrieved.
     *
     * @return
     *  A mapping of id to File which provides access to the file meta data,
     *  can be null if no meta data can be retrieved.
     */
    public static Map<String, JSONObject> getMetaDataForFileList(@NotNull IDBasedFileAccess fileAccess, @NotNull List<String> ids) {
        HashMap<String, JSONObject> newMetaDataMap = null;
        // now check the meta data of the files using file access
        // use the slower single access to filter out problematic files
        var idIter = ids.listIterator();

        var id = UNKNOWN;
        while (idIter.hasNext()) {
            try {
                id = idIter.next();
                var fileMetaData = getFileMetaData(fileAccess, id, FileStorageFileAccess.CURRENT_VERSION);
                if (newMetaDataMap == null) {
                    newMetaDataMap = new HashMap<String, JSONObject>();
                }
                newMetaDataMap.put(id, getFileMetaDataAsJSON(fileMetaData));
            } catch (Exception e) {
                LOG.debug("Non fatal exception caught trying to get file meta data for id " + id, e);
            }
        }
        return newMetaDataMap;
    }

    /**
     * Retrieves the file meta data for the provided file using the current version
     *
     * @param fileAccess the file access retrieved with a session
     * @param id the file id
     * @return the file object containing the meta data
     * @throws OXException in case the file meta data cannot be retrieved
     */
    public static File getFileMetaData(@NotNull IDBasedFileAccess fileAccess, @NotNull String id) throws OXException {
        return fileAccess.getFileMetadata(id, FileStorageFileAccess.CURRENT_VERSION);
    }

    /**
     * Retrieve the file meta data for the provided file-id/version 
     *
     * @param fileAccess the file access retrieved with a session
     * @param id the file id
     * @param version a version string
     * @return the file object containing the meta data
     * @throws OXException in case the file meta data cannot be retrieved
     */
    public static File getFileMetaData(@NotNull IDBasedFileAccess fileAccess, @NotNull String id, @NotNull String version) throws OXException {
        return fileAccess.getFileMetadata(id, version);
    }

    /**
     * Put the file meta data as properties into a JSONObject.
     *
     * @param file
     *  The file whose properties should be set into the JSONObject.
     *
     * @return
     *  A JSONObject which contains the file meta data as properties.
     *
     * @throws JSONException
     */
    public static JSONObject getFileMetaDataAsJSON(@NotNull final File file) throws JSONException {
        JSONObject newFileMetaData = new JSONObject();

        newFileMetaData.put("id", file.getId());
        newFileMetaData.put("folder_id", file.getFolderId());
        newFileMetaData.put("filename", file.getFileName());
        newFileMetaData.put("title", file.getTitle());
        newFileMetaData.put("last_modified", file.getLastModified());

        return newFileMetaData;
    }
}
