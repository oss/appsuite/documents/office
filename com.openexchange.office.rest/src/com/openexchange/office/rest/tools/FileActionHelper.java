/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.tools;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.imagetransformation.ImageMetadata;
import com.openexchange.imagetransformation.ImageMetadataOptions;
import com.openexchange.imagetransformation.ImageMetadataService;
import com.openexchange.net.HostList;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.tools.net.URITools;

@Service
public class FileActionHelper implements InitializingBean {

    // ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(FileActionHelper.class);

    private static final String HOST_DENYLIST_DEFAULT = "127.0.0.1-127.255.255.255,localhost";

    private static final int MAX_IMAGE_SIZE_IN_MB = 20;

    private static final int MAX_URL_LENGTH = 255;

    private static final Set<String> SUPPORTED_IMAGE_TYPES;

    static {
        final Set<String> supportedImageTypes = new HashSet<>();
        supportedImageTypes.add("png");
        supportedImageTypes.add("jpg");
        supportedImageTypes.add("jpeg");
        supportedImageTypes.add("gif");
        supportedImageTypes.add("tif");
        supportedImageTypes.add("tiff");
        supportedImageTypes.add("bmp");
        SUPPORTED_IMAGE_TYPES = Collections.unmodifiableSet(supportedImageTypes);
    }

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private static ImageMetadataService imageMetadataService;

    private static long maxImageSizeSupported = MAX_IMAGE_SIZE_IN_MB * 1024L * 1024L;
    private static int maxUrlLength;
    private static String denyImageUrlHostlistAsStr;
    private static String allowImageUrlHostlistAsStr;
    private static HostList denyImageUrlHostlist;
    private static HostList allowImageUrlHostlist;

    @Override
    public void afterPropertiesSet() throws Exception {
        maxUrlLength = configurationService.getIntProperty("com.openexchange.office.maxUrlLength", MAX_URL_LENGTH);
        maxImageSizeSupported = configurationService.getIntProperty("com.openexchange.office.maxImageSize", MAX_IMAGE_SIZE_IN_MB) * 1024L * 1024L;
        denyImageUrlHostlistAsStr = configurationService.getProperty("com.openexchange.office.upload.blacklist", HOST_DENYLIST_DEFAULT);
        allowImageUrlHostlistAsStr = configurationService.getProperty("com.openexchange.office.upload.whitelist", "");
        denyImageUrlHostlist = HostList.valueOf(denyImageUrlHostlistAsStr);
        allowImageUrlHostlist = HostList.valueOf(allowImageUrlHostlistAsStr);
    }

    // ---------------------------------------------------------------

    public long getMaxImageSizeSupported() {
        return maxImageSizeSupported;
    }

    // ---------------------------------------------------------------

    public String imageTypeDetection(InputStream imageInputStm) {
        String imageFormatName = null;
        if (null != imageInputStm) {
            try {
                final ImageMetadata imageMetadata = imageMetadataService.getMetadataFor(imageInputStm, null, null, ImageMetadataOptions.builder().withDimension().withFormatName().build());
                imageFormatName = imageMetadata.getFormatName();
            } catch (IOException e) {
                log.trace(e.getMessage());
            }
        }
        return imageFormatName;
    }

    // ---------------------------------------------------------------

    public byte[] getImageDataFromUrl(String fileURL) throws RESTException {
        byte[] result = null;
        try {
            HttpURLConnection httpConn;
            try {
                httpConn = (HttpURLConnection) URITools.getTerminalConnection(fileURL, Optional.of(validator), Optional.empty());
            } catch (Exception e) {
                throw new RESTException(ErrorCode.GENERAL_MISSING_SERVICE_ERROR, e);
            }
            try {
                int responseCode = httpConn.getResponseCode();

                // always check HTTP response code first
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    final int contentLength = httpConn.getContentLength(); // contentLenght is -1 if the length is not yet available
                    if (contentLength > maxImageSizeSupported) {
                        throwMaxImageSizeExceeding(maxImageSizeSupported);
                    }
                    final boolean notEnoughMemory = MemoryObserver.get().willHeapLimitBeReached(contentLength == -1 ? maxImageSizeSupported : contentLength); // assuming maxImageSizeSupported if contentLength is not available
                    if (notEnoughMemory) {
                        throw new RESTException("Memory threshold will be exceeded due to reading image data in memory", ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR);
                    }

                    // opens input stream from the HTTP connection
                    try (var inputStream = httpConn.getInputStream()) {
                        if(contentLength == -1) {
                            result = IOUtils.toByteArray(new BoundedInputStream(inputStream, maxImageSizeSupported + 1));
                            if(result.length > maxImageSizeSupported) {
                                throwMaxImageSizeExceeding(maxImageSizeSupported);
                            }
                        }
                        else {
                            result = IOUtils.toByteArray(inputStream, contentLength);
                        }
                    }

                    final String imageType = imageTypeDetection(new ByteArrayInputStream(result));
                    if (StringUtils.isEmpty(imageType) || !SUPPORTED_IMAGE_TYPES.contains(imageType)) {
                        throw new RESTException(ErrorCode.GENERAL_IMAGE_TYPE_NOT_SUPPORTED_ERROR);
                    }
                } else {
                    throw new RESTException("Error: " + responseCode + ", " + httpConn.getResponseMessage(), ErrorCode.GENERAL_ARGUMENTS_ERROR);
                }
            }
            finally {
                httpConn.disconnect();
            }
        } catch (RESTException e) {
            log.debug("RESTException caught while trying to get image data from provided url", e);
            throw e;
        } catch (Exception e) {
            log.error("Error while processing the retrieved information: " + e.getMessage(), e);
            throw new RESTException(ErrorCode.GENERAL_UNKNOWN_ERROR);
        }
        return result;
    }

    /**
     * Validates the given URL according to the allow list protocols and deny list hosts.
     *
     * @param url The URL to validate
     * @return An optional OXException
     */
    private static Function<URL, Optional<OXException>> validator = (url) -> {

        String urlasStr = url.toString();
        if (urlasStr.length() > maxUrlLength) {
            return Optional.of(new OXException(new RESTException("The length " + denyImageUrlHostlistAsStr.length() + " is to long!", ErrorCode.GENERAL_ARGUMENTS_ERROR)));
        }

        final UrlValidator defaultValidator = new UrlValidator();
        if (!defaultValidator.isValid(urlasStr)) {
            return Optional.of(new OXException(new RESTException(ErrorCode.GENERAL_ARGUMENTS_ERROR)));
        }
        String host = url.getHost();
        if (StringUtils.isBlank(allowImageUrlHostlistAsStr)) {
            if (denyImageUrlHostlist.contains(host)) {
                return Optional.of(new OXException(new RESTException("Host " + host + " is on deny list!", ErrorCode.GENERAL_ARGUMENTS_ERROR)));
            }
        } else {
            if (!allowImageUrlHostlist.contains(host)) {
                if (denyImageUrlHostlist.contains(host)) {
                    return Optional.of(new OXException(new RESTException("Host " + host + " is not on allow list and is on deny list!", ErrorCode.GENERAL_ARGUMENTS_ERROR)));
                }
            }
        }
        return Optional.empty();
    };

    public static void throwMaxImageSizeExceeding(long maxSize) throws RESTException {
        final ErrorCode errorCode = ErrorCode.GENERAL_IMAGE_SIZE_EXCEEDS_LIMIT_ERROR;
        errorCode.setValue(Long.valueOf(maxSize / (1024L * 1024L)).toString());
        throw new RESTException("Image size exceeds limit " + maxSize, errorCode);
    }
}
