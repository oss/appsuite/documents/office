
package com.openexchange.office.rest.tools;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.UserizedFile;
import com.openexchange.folderstorage.Permission;
import com.openexchange.folderstorage.Permissions;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.server.services.ServerServiceRegistry;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.ShareTargetPath;
import com.openexchange.share.groupware.ModuleSupport;
import com.openexchange.share.groupware.TargetPermission;
import com.openexchange.share.groupware.TargetProxy;
import com.openexchange.tools.session.ServerSession;

@Service
public class FileWritePermissionHelper {

    private static final String SHARED_FOLDER_ID = String.valueOf(FolderObject.SYSTEM_USER_INFOSTORE_FOLDER_ID);

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    @Autowired
    private FolderHelperServiceFactory folderHelperServiceFactory;

    public Map<Integer, Entry<String, String>> getUserWithWriteAccessFile(@NonNull String folderId, @NonNull String fileId, @NonNull List<Integer> userIds, @NonNull ServerSession session) throws OXException {

        FileHelperService fileHelper = fileHelperServiceFactory.create(session);
        FolderHelperService folderHelper = folderHelperServiceFactory.create(session);

        List<Integer> userIdList = new ArrayList<Integer>(userIds);

        Map<Integer, Entry<String, String>> usersFile = new HashMap<>();

        Entry<String, String> userFile = new AbstractMap.SimpleEntry<String, String>(folderId, fileId);

        if (folderId.equals(SHARED_FOLDER_ID)) {

            // Test if the user can write to to share file
            for (Integer userId : fileHelper.canWriteToFile(fileId, session.getContext(), userIdList)) {
                usersFile.put(userId, userFile);
                userIdList.remove(userId);
            }

            if (!userIdList.isEmpty()) {
                // Test if the user can write to the original File
                File file = fileHelper.getFileMetaData(fileId);
                if (UserizedFile.class.isInstance(file)) {
                    UserizedFile userizedFile = (UserizedFile) file;
                    String originalFileId = userizedFile.getOriginalId();
                    String originalFolderId = userizedFile.getOriginalFolderId();

                    userFile = new AbstractMap.SimpleEntry<String, String>(originalFolderId, originalFileId);

                    for (Integer userId : canWriteToOriginFile(userIdList, userizedFile, session)) {
                        usersFile.put(userId, userFile);
                    }
                }
            }

        } else {
            // Test if the user can write to the original folder
            for (Integer userId : folderHelper.canWriteToFolder(folderId, session.getContext(), userIdList)) {
                usersFile.put(userId, userFile);
                userIdList.remove(userId);
            }

            if (!userIdList.isEmpty()) {
                // Test if the use can write to the shared file
                String sharedFileId = fileId.replace(folderId, SHARED_FOLDER_ID);
                Entry<String, String> sharedFile = new AbstractMap.SimpleEntry<String, String>(SHARED_FOLDER_ID, sharedFileId);

                for (Integer userId : fileHelper.canWriteToFile(sharedFileId, session.getContext(), userIdList)) {
                    usersFile.put(userId, sharedFile);
                    userIdList.remove(userId);
                }
            }

        }

        return usersFile;
    }

    private Set<Integer> canWriteToOriginFile(List<Integer> userIds, UserizedFile userizedFile, ServerSession session) throws OXException {
        Set<Integer> userWithWriteAccess = new HashSet<>();

        String originalFileId = userizedFile.getOriginalId();
        String originalFolderId = userizedFile.getOriginalFolderId();

        ModuleSupport moduleSupport = ServerServiceRegistry.getInstance().getService(ModuleSupport.class);

        for (Integer userId : userIds) {

            // Test if the file is visible for the user
            // If you get the permissions and the file is not visible for the user the write permission is always true
            // because if the document is not visible for the user the code takes the session user as a fallback but there
            // is no documentation about it.
            boolean visible = moduleSupport.isVisible(FolderObject.INFOSTORE, originalFolderId, originalFileId, session.getContextId(), userId);

            if (visible) {
                // File Rights > Folder Rights > Owner + Folder Owner Rights
                boolean hasWritePermissions = false;
                boolean owner = userizedFile.getCreatedBy() == userId;

                // get original file permissions
                ShareTargetPath targetPath = moduleSupport.getPath(new ShareTarget(FolderObject.INFOSTORE, originalFolderId, originalFileId), session.getContextId(), userId);
                TargetProxy targetProxy = moduleSupport.resolveTarget(targetPath, session.getContextId(), userId);
                List<TargetPermission> permissions = targetProxy.getPermissions();

                // check file write permission
                for (TargetPermission targetPermission : permissions) {
                    if (!targetPermission.isGroup() && targetPermission.getEntity() == userId && canWrite(targetPermission.getBits(), owner)) {
                        hasWritePermissions = true;
                        userWithWriteAccess.add(userId);
                        break;
                    }
                }

                if (!hasWritePermissions) {
                    // get original folder permissions
                    ShareTargetPath folderTargetPath = moduleSupport.getPath(new ShareTarget(FolderObject.INFOSTORE, originalFolderId), session.getContextId(), userId);
                    TargetProxy folderTargetProxy = moduleSupport.resolveTarget(folderTargetPath, session.getContextId(), userId);
                    List<TargetPermission> folderPermissions = folderTargetProxy.getPermissions();

                    // check folder write permission
                    for (TargetPermission targetPermission : folderPermissions) {
                        if (!targetPermission.isGroup() && targetPermission.getEntity() == userId && canWrite(targetPermission.getBits(), owner)) {
                            userWithWriteAccess.add(userId);
                            break;
                        }
                    }
                }

            }
        }

        return userWithWriteAccess;
    }

    private boolean canWrite(int permissionBits, boolean owner) {
        int writePermission = Permissions.parsePermissionBits(permissionBits)[2];
        return writePermission == Permission.WRITE_ALL_OBJECTS || writePermission == Permission.MAX_PERMISSION || (owner && writePermission == Permission.WRITE_OWN_OBJECTS);
    }
}
