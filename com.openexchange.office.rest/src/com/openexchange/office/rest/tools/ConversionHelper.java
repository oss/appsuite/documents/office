/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.tools;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

@Service
public class ConversionHelper {

    // ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(ConversionHelper.class);

    @Autowired(required=false)
    private IDocumentConverter documentConverter;

    @Autowired(required=false)
    private DocFileServiceFactory docFileServiceFactory;

    // ---------------------------------------------------------------
    public static class ConversionData
    {
        private InputStream convertedInputStream;
        private String      convertedMimeType;
        private String      convertedExtension;
        private ErrorCode   conversionError;

        public ConversionData(InputStream inputStream, String mimeType, String extension, ErrorCode errorCode) {
            convertedInputStream = inputStream;
            convertedMimeType    = mimeType;
            convertedExtension   = extension;
            conversionError      = errorCode;
        }

        public InputStream getInputStream() { return convertedInputStream; }
        public String      getMimeType() { return convertedMimeType; }
        public String      getExtension() { return convertedExtension; }
        public ErrorCode   getErrorCode( ) { return conversionError; }
    }

    // ---------------------------------------------------------------
    public ConversionData convertDocument(@NotNull ServerSession session, @NotNull String fileId, @NotNull String folderId, @NotNull String version, @NotNull String fileName, @NotNull Map<String, String> conversionFormat, String encryptionInfo) throws Exception {
        // conversion already sets the correct target document format
        var errorCode = ErrorCode.NO_ERROR;

        if (null != documentConverter) {
            // OX Drive file must be loaded using Files API
            var loadRequest = new AJAXRequestData();
            loadRequest.putParameter("id", fileId);
            loadRequest.putParameter("folder_id", folderId);
            loadRequest.putParameter("version", version);
            loadRequest.putParameter("fileName", fileName);

            var streamInfo = docFileServiceFactory.createInstance().getDocumentStream(session, loadRequest, encryptionInfo);
            errorCode = streamInfo.getErrorCode();
            try (var documentInputStm = streamInfo.getDocumentStream()) {
                if (null != documentInputStm) {
                    return convertDocument(session, documentInputStm, fileName, conversionFormat, streamInfo.isDecrypted());
                }
            } finally {
                streamInfo.close();
            }
        }

        return new ConversionData(null, null, null, errorCode);
    }

    // ---------------------------------------------------------------
    public ConversionData convertDocument(
        final ServerSession session,
        final InputStream documentInputStm,
        final String fileName,
        final Map<String, String> conversionFormat,
        final boolean decrypted) {

        ErrorCode errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;

        if (null == documentInputStm)
            return new ConversionData(null, null, null, errorCode);

        if (null != documentConverter) {
            try {
                final HashMap<String, Object> jobProperties = new HashMap<>(8);
                final HashMap<String, Object> resultProperties = new HashMap<>(8);
                final String filterShortName = conversionFormat.get(Properties.PROP_FILTER_SHORT_NAME);

                jobProperties.put(Properties.PROP_INPUT_STREAM, documentInputStm);
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, filterShortName);
                jobProperties.put(Properties.PROP_LOCALE, getSessionLanguage(session));

                // this is a user request in every case
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);

                /* TODO (KA): set InputUrl property, if known later
                if (null != inputURL) {
                    jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                }
                */

                if (null != fileName) {
                    jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
                }

                if (decrypted) {
                    // set 'NO_CACHE=true' property for decrypted documents since we don't
                    // want to cache decrypted document conversion results for security reasons
                    jobProperties.put(Properties.PROP_NO_CACHE, Boolean.TRUE);
                }

                final InputStream inputStm = documentConverter.convert(filterShortName, jobProperties, resultProperties, session);

                if (null != inputStm) {
                    // set new mime type and extensionType
                    final String mimeType = conversionFormat.get(Properties.PROP_MIME_TYPE);
                    final String extensionType = conversionFormat.get(Properties.PROP_INPUT_TYPE);

                    return new ConversionData(inputStm, mimeType, extensionType, ErrorCode.NO_ERROR);
                }

                errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
            } catch (final Exception e) {
                errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                if (e instanceof OXException)
                    errorCode = ExceptionToErrorCode.map((OXException)e, errorCode, false);
            }
        } else {
            log.warn("RT connection: cannot retrieve document converter service!");
            errorCode = ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR;
        }

        return new ConversionData(null, null, null, errorCode);
    }

    // ---------------------------------------------------------------
    public static String getSessionLanguage(ServerSession session) {
        final User sessionUser = session.getUser();
        String prefLanguage = null;

        // user language
        if (null != sessionUser) {
            prefLanguage = sessionUser.getPreferredLanguage();
        }

        return StringUtils.isBlank(prefLanguage) ? "en_US" : prefLanguage;
    }

}
