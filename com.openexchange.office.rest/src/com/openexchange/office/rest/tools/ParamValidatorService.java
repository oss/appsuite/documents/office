/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.tools;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;

@Service
public class ParamValidatorService {

    protected static final Logger log = LoggerFactory.getLogger(ParamValidatorService.class);

    @Autowired
    private FileHelperServiceFactory fileHelperServiceRegistry;

    // ---------------------------------------------------------------
    public boolean areAllParamsNonEmpty(final AJAXRequestData aRequest, String[] paramNames) {

        return (aRequest != null) &&
            Arrays.asList(paramNames)
                  .stream()
                  .map(p -> aRequest.getParameter(p))
                  .filter(StringUtils::isEmpty)
                  .collect(Collectors.toSet())
                  .isEmpty();
    }

    // ---------------------------------------------------------------
    public Set<String> paramsMissing(final AJAXRequestData aRequest, String[] paramNames) {
        if (aRequest != null) {
            return Arrays.asList(paramNames)
                         .stream()
                         .map(p -> { return StringUtils.isEmpty(aRequest.getParameter(p)) ? p : null; })
                         .filter(StringUtils::isEmpty)
                         .collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }

    // ---------------------------------------------------------------
    public boolean canReadFile(ServerSession session, String folderId, String fileId) {
        boolean canRead = false;
        try {

            // check document access rights to ensure that the current user has access rights to retrieve document content
            final FileHelperService fileHelperService = fileHelperServiceRegistry.create(session);
            canRead = fileHelperService.canReadFile(folderId, fileId, session.getUserId());
        } catch (Exception e) {
            log.error("AjaxRequest GetFileAction exception caught while trying to check access rights", e);
        }

        return canRead;
    }

    // ---------------------------------------------------------------
    public static <T> T getParameterOrDefault(final AJAXRequestData request, String paramName, Class<T> clazz, T defaultValue) throws OXException {
    	T value = request.getParameter(paramName, clazz, true);
    	if (value == null) {
    		value = defaultValue;
    	}

    	return value;
    }

    // ---------------------------------------------------------------
    public static AJAXRequestResult getResultFor(int nHttpError) {
        final AJAXRequestResult result = new AJAXRequestResult();
        result.setHttpStatusCode(nHttpError);
        return result;
    }
}
