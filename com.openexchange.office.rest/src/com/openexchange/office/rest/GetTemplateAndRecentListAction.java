/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccess;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.office.document.api.TemplateFilesScanner;
import com.openexchange.office.document.api.TemplateFilesScannerFactory;
import com.openexchange.office.document.api.TemplateFilesScannerFactory.TemplateFilterType;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.RecentFileListHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.files.FileDescriptorHelper;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.filename.FileNameTools;
import com.openexchange.tools.session.ServerSession;

@SuppressWarnings("rawtypes")
@RestController
public class GetTemplateAndRecentListAction extends DocumentRESTAction implements InitializingBean {
    private static final Logger LOG = LoggerFactory.getLogger(GetTemplateAndRecentListAction.class);

    @Autowired
    private TemplateFilesScannerFactory m_TemplateFilesScannerFactory;
    
    @Autowired
    private IDBasedFileAccessFactory m_fileFactory;
    
    @Autowired
    private IDBasedFolderAccessFactory m_folderFactory;
    
    @Autowired
    private RecentFileListManagerFactory recentFileListManagerFactory;
    
    @Autowired
    private FolderHelperServiceFactory folderHelperServiceFactory;
    
    private TemplateFilesScanner templateFilesScanner;
    private final String[] m_props = { "filename", "last_modified", "title" };

	@Override
	public void afterPropertiesSet() throws Exception {
        templateFilesScanner = m_TemplateFilesScannerFactory.create(TemplateFilterType.ALL_DOCUMENTS);
	}

	@Override
	public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) {
        final JSONObject jsonObj = new JSONObject();
        final String type = requestData.getParameter("type").toLowerCase();
    	final ApplicationType appType = ApplicationType.stringToEnum(type);
        final JSONArray templates = new JSONArray();
        AJAXRequestResult requestResult = null;

        if ((null != session) && (appType != ApplicationType.APP_NONE)) {
        	try {
        	    ErrorCode errorCode = templateFilesScanner.searchForTemplates(session, type, templates);
                jsonObj.put("error", errorCode.getAsJSON());
                jsonObj.put("templates", templates);

                final RecentFileListManager recentFileListManager = recentFileListManagerFactory.create(session);
                final List<JSONObject> appRecentFiles = recentFileListManager.readRecentFileList(appType);
                if ((null != appRecentFiles) && (!appRecentFiles.isEmpty())) {
                    // now we have to check the recents list to be up to date
                    final IDBasedFileAccess fileAccess = ((null != m_fileFactory)) ? m_fileFactory.createAccess(session) : null;
                    final IDBasedFolderAccess folderAccess = ((null != m_folderFactory)) ? m_folderFactory.createAccess(session) : null;
                    if ((null != fileAccess) && (null != folderAccess)) {
                        final List<String> ids = new ArrayList<String>(appRecentFiles.size());

                        for (JSONObject fileObj : appRecentFiles) {

                            final String id = fileObj.optString("id", null);
                            fileObj.put("com.openexchange.file.sanitizedFilename", FileNameTools.sanitizeFilename(fileObj.getString("filename")));

                            if (null != id) {
                                ids.add(id);
                            }
                        }

                        Map<String, JSONObject> newMetaDataMap = null;
                        try {
                            newMetaDataMap = RecentFileListHelper.getMetaDataForFiles(fileAccess, ids);
                        } catch (Exception e) {
                            // Fallback, if we cannot retrieve the meta data in a
                            // fast way. Use the slow more time consuming way to
                            // files one by one.
                            newMetaDataMap = RecentFileListHelper.getMetaDataForFileList(fileAccess, ids);
                        }
                        if (null != newMetaDataMap) {
                            final Map<String, Boolean> removedIds = new HashMap<String, Boolean>();

                            boolean recentListChanged = checkRecentListWithMetaData(session, appRecentFiles, newMetaDataMap, removedIds);
                            if (recentListChanged && (removedIds.size() > 0)) {
                                // remove recent entries which were moved to trash or deleted
                                ListIterator<JSONObject> recentsIter = appRecentFiles.listIterator();
                                while (recentsIter.hasNext()) {
                                    String id = recentsIter.next().optString("id", null);
                                    if ((null != id) && (removedIds.containsKey(id))) {
                                        recentsIter.remove();
                                    }
                                }
                            }

                            // write-back adapted recent list
                            if (recentListChanged) {
                                recentFileListManager.writeRecentFileList(appType, appRecentFiles);
                                recentFileListManager.flush();
                            }
                        }
                    }
                	jsonObj.put("recents", new JSONArray(appRecentFiles));
                } else {
                	jsonObj.put("recents", new JSONArray());
                }
        	} catch (OXException e) {
                LOG.error("Exception while updating recent file list", e);
        	} catch (JSONException e) {
        		LOG.error("Exception while creating JSONObject answering AJAXRequest 'gettemplateandrecentlist'", e);
        	}

            requestResult = new AJAXRequestResult(jsonObj);
        }

        return requestResult;
	}

    /**
     * Checks the recent file list for removed, moved or updated entries using
     * the updated meta data.
     *
     * @param folderAccess
     *  A folder access instance to be used to retrieve the meta data.
     *
     * @param appRecentFiles
     *  A list of recent file entries.
     *
     * @param newMetaDataMap
     *  A map of new meta data associated by the file id.
     *
     * @param removedIds
     *  A map of ids which must be removed from the recent file list.
     *
     * @return
     *  TRUE if the recent file list has changed and must be written, otherwise
     *  FALSE and no write-back must be done.
     *
     * @throws JSONException
     */
	private boolean checkRecentListWithMetaData(Session session, List<JSONObject> appRecentFiles, Map<String, JSONObject> newMetaDataMap, Map<String, Boolean> removedIds) throws OXException, JSONException {
		final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);

        // Use new meta data to update recent files - there are some situations
	    // where we have to dig deeper to find out what happened. We want to
	    // support to still open files that have been moved to other folders -
	    // except the trash folder!!

        // Iterate through the recent file list and check
        // every file.
        boolean recentListChanged = false;
        long time1 = System.currentTimeMillis();

        for (int i = 0; i < appRecentFiles.size(); i++) {
            final JSONObject fileObj = appRecentFiles.get(i);
            final String id = fileObj.optString("id", null);
            final String folderId = fileObj.optString("folder_id", null);

            // Bug #67737
            // Ensure that an exception won't stop processing other entries of
            // the recent list
            try {
                if ((null != id) && (null != folderId)) {
                    JSONObject newFileMetaData = newMetaDataMap.get(id);
                    if (null != newFileMetaData) {
                        String newFolderId = newFileMetaData.optString("folder_id");
                        if (folderId.equals(newFolderId)) {
                            // check if the parent folder was moved to the trash
                            if (folderHelperService.isFolderTrashOrInTrash(newFolderId)) {
                                removedIds.put(id, true);
                                recentListChanged = true;
                            } else if (FileDescriptorHelper.containsChanges(fileObj, newFileMetaData, m_props)) {
                                // no move - just take over the new meta data
                                fileObj.put("filename", newFileMetaData.optString("filename", ""));
                                fileObj.put("title", newFileMetaData.optString("title", ""));
                                Date lastModified = (Date)newFileMetaData.opt("last_modified");
                                if (null != lastModified) {
                                    fileObj.put("last_modified", lastModified.getTime());
                                }
                                recentListChanged = true;
                            }
                        } else {
                            // We have a move - check if the new folder is the trash folder
                            if (folderHelperService.isFolderTrashOrInTrash(newFolderId)) {
                                // file has been moved to the trash => remove from recent files
                                removedIds.put(id, true);
                                recentListChanged = true;
                            } else {
                                // move to another folder => take over changes
                                fileObj.put("folder_id", newFolderId);
                                fileObj.put("filename", newFileMetaData.optString("filename", ""));
                                fileObj.put("title", newFileMetaData.optString("title", ""));
                                Date lastModified = (Date)newFileMetaData.opt("last_modified");
                                if (null != lastModified) {
                                    fileObj.put("last_modified", lastModified.getTime());
                                }
                                recentListChanged = true;
                            }
                        }
                    } else {
                        // id not known anymore => remove from recent list
                        removedIds.put(id, true);
                        recentListChanged = true;
                    }
                } else if (id != null) {
                    // invalid properties => remove from recent list
                    removedIds.put(id, true);
                    recentListChanged = true;
                }
            } catch (OXException e) {
            	final String folder_id = StringUtils.isEmpty(folderId) ? RecentFileListHelper.UNKNOWN : folderId;
            	final String file_id = StringUtils.isEmpty(id) ? RecentFileListHelper.UNKNOWN : id;
                LOG.debug("Non fatal exception caught trying to determine state of folder " + folder_id + ", file " + file_id, e);
                if (id != null) {
                    removedIds.put(id, true);
                    recentListChanged = true;
                }
            }
        }

        // time runtime to update the recent file list (moves/renames/deletes)
        long time2 = System.currentTimeMillis();
        LOG.debug("Check recent file list for moved/deleted entries lasts " + (time2 - time1) + "ms");

        return recentListChanged;
	}

}
