/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.util.Locale;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import com.openexchange.user.UserService;


/**
 * {@link GetUserInfoAction}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
@RestController
public class GetUserInfoAction extends DocumentRESTAction {

    private static final Logger log = LoggerFactory.getLogger(GetUserInfoAction.class);

    @Autowired
    private UserService userService;
    
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        final JSONObject jsonObj = new JSONObject();
        AJAXRequestResult requestResult = null;

        if ((null != requestData) && (null != session)) {
            ErrorCode errorCode = ErrorCode.NO_ERROR;

            try {
                final String userIdParam = requestData.getParameter("userId");
                if (null != userIdParam) {
                    int userId = Integer.parseInt(userIdParam);
                    User userData = userService.getUser(userId, session.getContext());
                    JSONObject userDataAsJSON = GetUserInfoAction.getUserDataAsJSON(userData);
                    jsonObj.put("userData", userDataAsJSON);
                } else {
                    errorCode = ErrorCode.NO_ERROR;
                }
            } catch (Exception e) {
                log.error("Exception while creating retrieving user data", e);
                errorCode = ErrorCode.GETUSERINFO_UNKNOWN_ERROR;
            } finally {
                try {
                    jsonObj.put("error", errorCode.getAsJSON());
                } catch (Exception e) {
                    log.error("Exception while creating json object with error data", e);
                }
            }
        }

        requestResult = new AJAXRequestResult(jsonObj);

        return requestResult;
    }

    private static final JSONObject getUserDataAsJSON(User userData) throws Exception {
        final JSONObject jsonObj = new JSONObject();

        if (null != userData) {
            jsonObj.put("displayName", userData.getDisplayName());
            jsonObj.put("givenName", userData.getGivenName());
            jsonObj.put("surName", userData.getSurname());
            jsonObj.put("preferredLanguage", userData.getPreferredLanguage());
            jsonObj.put("timeZone", userData.getTimeZone());

            Locale locale = userData.getLocale();
            if (null != locale) {
                final JSONObject localeData = new JSONObject();
                localeData.put("language", locale.getLanguage());
                localeData.put("country", locale.getCountry());
                jsonObj.put("locale", localeData);
            }
        }

        return jsonObj;
    }
}
