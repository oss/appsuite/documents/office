/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.openexchange.ajax.container.ByteArrayFileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographyMode;
import com.openexchange.imagetransformation.ImageInformation;
import com.openexchange.java.BoolReference;
import com.openexchange.office.document.api.DocumentConstants;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.service.http.AjaxActionServiceWithConcreteModel;
import com.openexchange.server.ServiceExceptionCode;
import com.openexchange.tools.images.ImageTransformationUtility;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link DocumentFilterAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link DocumentRESTAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:malte.timmermann@open-xchange.com">Malte Timmermann</a>
 */
public abstract class DocumentRESTAction<TO, DO> extends AjaxActionServiceWithConcreteModel<TO, DO> {

	private static final Logger LOG = LoggerFactory.getLogger(DocumentRESTAction.class);

	public static final String USE_DEFAULT_TARGET_FOLDER_ID = null;

	public static final String HTTP_CLIENT_USER_AGENT = "Open-Xchange Documents";
	public static final String HTTP_CLIENT_ID_DOCUMENTS = "documents";

	private static final String m_errorKey = "hasErrors";
    private static final String m_eTagBase = "http://www.open-xchange.com/filestore/";

    @Autowired(required=false)
    protected CryptographicServiceAuthenticationFactory cryptographicServiceAuthenticationFactory;

    @Autowired(required=false)
    protected CryptographicAwareIDBasedFileAccessFactory cryptographicAwareIDBasedFileAccessFactory;

    @Autowired
    protected IDBasedFileAccessFactory idBasedFileAccessFactory;

    @Autowired
    protected ParamValidatorService paramValidatorService;

    /**
     * @param request
     * @param data
     * @param format
     * @param mimeType
     * @param fileName (may be null, if fileName should be retrieved from request)
     * @return AJAXRequestResult
     * @throws OXException
     */
    static protected AJAXRequestResult createFileRequestResult(@NonNull AJAXRequestData request, @NonNull byte[] data,
            @NonNull String format, @NonNull String mimeType, String fileName) {
        AJAXRequestResult requestResult = null;
        final String fileId = request.getParameter(RESTParameters.PARAMETER_ID);
        final String fileVersion = request.getParameter(RESTParameters.PARAMETER_VERSION);
        String resultFileName = (null != fileName) ? fileName : request.getParameter("filename");

        if (null == resultFileName) {
            resultFileName = "file";
        }

        if (format.equals("json")) {

            JSONObject jsonObject = new JSONObject();
            HashMap<String, String> map = new HashMap<>();
            StringBuilder dataUrlBuilder = new StringBuilder("data:");

            dataUrlBuilder.append(mimeType).append(";base64,").append(Base64.encodeBase64String(data));

            map.put("mimeType", mimeType);
            map.put("fileName", resultFileName);
            map.put("dataUrl", dataUrlBuilder.toString());
            dataUrlBuilder = null;

            try {
                jsonObject.put("data", JSONObject.jsonObjectFor(map));
            } catch (JSONException e) {
                LOG.error("Exception detected while trying to create json result for request", e);
            }

            request.setFormat("json");
            requestResult = new AJAXRequestResult(jsonObject, "json");
        } else {

            if (data.length > 2 && ((data[0] & 0x00ff) == 0x00ff) && ((data[1] & 0x00ff) == 0x00d8)) { // quick jpeg check
                try {
                    final ImageInformation information = ImageTransformationUtility.readImageInformation(new BufferedInputStream(new ByteArrayInputStream(data)), new BoolReference(false));
                    if (information!=null && information.orientation > 1) {
                        // DOCS-2109 and DOCS-2992:
                        // we don't want to let the image response renderer perform
                        // an auto rotation or transformation at all in case the Exif data
                        // of the image indicates this, as all transformations (e.g. Exif
                        // rotation) should be done via explicit rotation attributes for
                        // the document image element
                        if (mimeType.startsWith("image/")) {
                            request.putParameter("transformationNeeded", "false");
                        }
                    }
                }
                catch(IOException e) {
                    LOG.error("Exception detected while trying to detect exif orientation of a jpeg image", e);
                }
            }

            final ByteArrayFileHolder fileHolder = new ByteArrayFileHolder(data);

            fileHolder.close();
            fileHolder.setName(resultFileName);
            fileHolder.setContentType(mimeType);

            requestResult = new AJAXRequestResult(fileHolder, "file");

            setETag(getETag(fileId, fileVersion), 0, requestResult);
        }
        return requestResult;
    }

    protected File getNewFile(AJAXRequestData request, String newTargetFolderId, String filename, String mimeType, String extensionType, File templateFile, boolean binaryConverted) {

        String targetFolderId = StringUtils.isEmpty(newTargetFolderId) ? request.getParameter("target_folder_id") : newTargetFolderId;
        String initialFileName = StringUtils.isEmpty(filename) ? request.getParameter("target_filename") : filename;

        boolean preserveFileName = Boolean.parseBoolean(request.getParameter("preserve_filename"));

        File file = new DefaultFile();

        // set the required file properties
        String createFileName = ((null == initialFileName) || (initialFileName.length() < 1)) ? "unnamed" : initialFileName;
        file.setId(FileStorageFileAccess.NEW);
        file.setFolderId(targetFolderId);
        file.setVersionComment(DocumentConstants.OX_DOCUMENTS_CMNT);

        if (!preserveFileName || templateFile == null) {
            createFileName = ((null == initialFileName) || (initialFileName.length() < 1)) ? "unnamed" : initialFileName;
        } else {
            createFileName = FileHelper.getBaseName(templateFile.getFileName());
        }

        setFileName(file, createFileName, mimeType, extensionType, request);

        if (binaryConverted) {
            Map<String, Object> meta = file.getMeta();

            if (null == meta) {
                file.setMeta(meta = new HashMap<>());
            }

            meta.put(DocumentMetaData.META_BINARY_CONVERTED, Boolean.TRUE);
        }

        return file;
    }

    protected String getCryptoAction(AJAXRequestData request) {
        String cryptoAction = request != null ? request.getParameter("cryptoAction") : "";
        return cryptoAction == null ? "" : cryptoAction;
    }

    protected IDBasedFileAccess getFileAccess(ServerSession session, AJAXRequestData request, String cryptoAction) throws OXException {

        if(!cryptoAction.isEmpty()) {

            //Parsing authentication from the request. Might be null since not all crypto-actions require authentication.
            String authentication = null;
            CryptographicServiceAuthenticationFactory encryptionAuthenticationFactory = cryptographicServiceAuthenticationFactory;
            if(encryptionAuthenticationFactory != null && request != null && request.optHttpServletRequest() != null) {
                authentication = encryptionAuthenticationFactory.createAuthenticationFrom(request.optHttpServletRequest());
            }

            //Creating file access with crypto functionalities
            CryptographicAwareIDBasedFileAccessFactory encryptionAwareFileAccessFactory = cryptographicAwareIDBasedFileAccessFactory;
            if (encryptionAwareFileAccessFactory != null) {
                EnumSet<CryptographyMode> cryptMode = CryptographyMode.createSet(cryptoAction);
                if (cryptMode.size() > 0) {
                    return encryptionAwareFileAccessFactory.createAccess(idBasedFileAccessFactory.createAccess(session), cryptMode, session, authentication);
                }

                throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("cryptoAction",cryptoAction);
            }
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(CryptographicAwareIDBasedFileAccessFactory.class.getSimpleName());
        }

        return idBasedFileAccessFactory.createAccess(session);
    }

    protected IDBasedFileAccess getFileAccess(ServerSession session, AJAXRequestData request) throws OXException {

        String cryptoAction = getCryptoAction(request);
        return getFileAccess(session, request, cryptoAction);
    }

    protected void setFileName(File file, String fileName, String mimeType, String extensionType, AJAXRequestData request) {
        if ("Encrypt".equals(request.getParameter("cryptoAction"))) {
            file.setFileName(fileName + "." + extensionType + ".pgp");
            file.setFileMIMEType("application/pgp-encrypted");

        } else {
            file.setFileName(fileName + "." + extensionType);
            file.setFileMIMEType(mimeType);
        }
    }


    /** return JSON which contain information about current user session.
     *
     *  @note    Retrieving session information is implemented very gracefully.
     *          It's because the session API seems not well designed and show
     *          different behavior on different installations.
     *          Especially on custom SSO (single sign on) installations the
     *          source of context information is not (real) clear.
     *
     *     @param    serverSession [IN]
     *             the server session object where we should retrieve all informations from.
     *
     *  @return a JSON object which contain all session data we need for RT.
     */
    protected JSONObject getJSONObjectFromServerSession(final ServerSession session)
    {
        JSONObject jsonSession = new JSONObject();

        try
        {
            final JSONObject jsonSessionDetails
                = new JSONObject()
                       .put("protocol" , "ox"           )
                       .put("component", JSONObject.NULL)
                       .put("resource" , session.getSessionID());

            jsonSession = new JSONObject().put("session", jsonSessionDetails);
        }
        catch (final Throwable e)
        {
            ExceptionUtils.handleThrowable(e);
            LOG.error(e.getMessage(), e);
            throw new RuntimeException ("Not able to retrieve needed information from session.", e);
        }

        return jsonSession;
    }

    /**
     * @param resultData
     * @return
     */
    static final AJAXRequestResult getAjaxRequestResult(JSONObject resultData) {
        JSONObject ret = resultData;

        if ((null == ret) || ret.isEmpty()) {
            ret = new JSONObject();

            try {
                ret.put(m_errorKey, true);
            } catch (JSONException e) {
                //
            }
        }

        return new AJAXRequestResult(ret);
    }

    /**
     * @param fileId
     * @param fileVersion
     * @return
     */
    static protected String getETag(String fileId, String fileVersion) {
        return (new StringBuilder(m_eTagBase)).append(fileId).append('/').append(fileVersion).append(".pdf").toString();
    }

    /**
     * @param eTag
     * @param expires
     * @param result
     * @throws OXException
     */
    static protected void setETag(String eTag, long expires, AJAXRequestResult result) {
        result.setExpires(expires);

        if (eTag != null) {
            result.setHeader("ETag", eTag);
        }
    }

    /**
     * @param request
     * @param testETag
     * @return
     * @throws OXException
     */
    static protected boolean checkETag(String fileId, String fileVersion, String testETag) {
        return getETag(fileId, fileVersion).equals(testETag);
    }

}
