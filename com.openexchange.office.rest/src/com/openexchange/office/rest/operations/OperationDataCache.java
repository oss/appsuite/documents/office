/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.timer.TimerService;

@Service
public class OperationDataCache implements Runnable, InitializingBean {

    // ---------------------------------------------------------------
	private static final Logger log = LoggerFactory.getLogger(OperationDataCache.class);
    private static final int  MAX_OPERATIONSCACHE_ENTRIES = 32;
    private static final long GC_FREQUENCY = 60000;

    //-------------------------------------------------------------------------
    private final static long MAX_IDLE_TIMESPAN = 300000;

    //-------------------------------------------------------------------------
    private static class OperationDataHolder {
        public String      key;
        public String      hash;
        public long        lastAccessTime;
        public ManagedFile htmlDocManagedFile;
        public ManagedFile opsDocManagedFile;
        public ManagedFile previewOpsManagedFile;
        public MetaData    metaData;

        // ---------------------------------------------------------------
        public OperationDataHolder(String key, String hash, MetaData metaData, ManagedFile htmlDocManagedFile, ManagedFile previewOpsManagedFile, ManagedFile opsDocManagedFile) {
            this.key = key;
            this.hash = hash;
            this.metaData = metaData;
            this.htmlDocManagedFile = htmlDocManagedFile;
            this.opsDocManagedFile = opsDocManagedFile;
            this.previewOpsManagedFile = previewOpsManagedFile;
            this.lastAccessTime = System.currentTimeMillis();
        }

        // ---------------------------------------------------------------
        public OperationDataHolder(final OperationDataHolder aHolder) {
            key = aHolder.key;
            hash = aHolder.hash;
            metaData = aHolder.metaData;
            htmlDocManagedFile = aHolder.htmlDocManagedFile;
            opsDocManagedFile = aHolder.opsDocManagedFile;
            previewOpsManagedFile = aHolder.previewOpsManagedFile;
            lastAccessTime = aHolder.lastAccessTime;
        }

        // ---------------------------------------------------------------
        public void touch() {
            lastAccessTime = System.currentTimeMillis();

            if (null != opsDocManagedFile)
                opsDocManagedFile.touch();
            if (null != previewOpsManagedFile)
                previewOpsManagedFile.touch();
            if (null != htmlDocManagedFile)
                htmlDocManagedFile.touch();
        }
    }

    // ---------------------------------------------------------------
    private final Map<String, OperationDataHolder> cacheOpsData = new HashMap<>();

    @Autowired
    private ManagedFileManagement managedFileService;

    @Autowired
    private TimerService timerService; 
    
    private int maxEntries;

    // ---------------------------------------------------------------
    public OperationDataCache() {
        this.maxEntries = MAX_OPERATIONSCACHE_ENTRIES;
    }

    // ---------------------------------------------------------------    
    @Override
	public void afterPropertiesSet() throws Exception {
    	timerService.scheduleAtFixedRate(this, GC_FREQUENCY, GC_FREQUENCY);		
	}

    // ---------------------------------------------------------------
    public boolean isEmpty() {
        synchronized (cacheOpsData) {
            return cacheOpsData.isEmpty();
        }
    }

    // ---------------------------------------------------------------
    public static String createKey(String folderId, String fileId, String versionOrAttachmentId, int context) {
    	return getKeyFromProps(folderId, fileId, versionOrAttachmentId, context);
    }

    // ---------------------------------------------------------------
    public boolean hasEntryAndTouch(String key) {
        OperationDataHolder cacheEntry = null;
        synchronized (cacheOpsData) {
            cacheEntry = cacheOpsData.get(key);

            if (null != cacheEntry)
                cacheEntry.touch();
        }

        return (null != cacheEntry);
    }

    // ---------------------------------------------------------------
    public OperationData get(String key) {
        OperationDataHolder aHolder = null;
        synchronized (cacheOpsData) {
            final OperationDataHolder cacheEntry = cacheOpsData.get(key);

            if (null != cacheEntry) {
                cacheEntry.touch();
                aHolder = new OperationDataHolder(cacheEntry);
            }
        }

        if (null != aHolder) {
            try {
                return this.createOperationData(aHolder);
            } catch (final Exception e) {
                removeEntryIfHashIdentical(key, aHolder.hash);

                log.debug("OperationDataCache caught exception trying to convert managed file data to cache entry - managed file expired?", e);
            }
        }

        return null;
    }

    // ---------------------------------------------------------------
    public void put(String key, final OperationData aOpsData) {
        OperationDataHolder holder = null;

        try {
            holder = this.createOperationDataHolder(aOpsData);
        } catch (Exception e) {
            log.warn("OperationDataCache caught exception trying to create entry with managed files", e);
        }

        OperationDataHolder oldEntry = null;

        if (null != holder) {
            synchronized (cacheOpsData) {
                if (cacheOpsData.size() >= maxEntries)
                    removeOldestEntry();
                oldEntry = cacheOpsData.put(key, holder);
            }
        }

        if (null != oldEntry) {
            destroyHolder(oldEntry);
        }
    }

    // ---------------------------------------------------------------
    public static String getKeyFromProps(String folderId, String fileId, String version, int context) {
        final StringBuilder tmp = new StringBuilder(folderId);
        tmp.append(fileId).append("-").append(version).append("-").append(context);
        return tmp.toString();
    }

    // ---------------------------------------------------------------
    @Override
    public void run() {
        removeExpiredEntries();
    }

    // ---------------------------------------------------------------
    private void removeEntryIfHashIdentical(String key, String hash) {
        synchronized (cacheOpsData) {
            final OperationDataHolder aHolder = cacheOpsData.get(key);
            // check for possible null values
            if ((aHolder != null) && StringUtils.equals(aHolder.hash, hash)) {
                removeEntry(key);
            }
        }
    }

    // ---------------------------------------------------------------
    private void removeOldestEntry() {
        long   oldestAccessTime = System.currentTimeMillis();
        String oldestEntryKey   = null;

        synchronized (cacheOpsData) {
            final Collection<OperationDataHolder> entries = cacheOpsData.values();
            for (final OperationDataHolder data : entries) {
                if (data.lastAccessTime < oldestAccessTime) {
                    oldestAccessTime = data.lastAccessTime;
                    oldestEntryKey   = data.key;
                }
            }

            if (StringUtils.isNotEmpty(oldestEntryKey)) {
                removeEntry(oldestEntryKey);
            }
        }
    }

    // ---------------------------------------------------------------
    private void removeExpiredEntries() {
        long currentTime = System.currentTimeMillis();

        synchronized (cacheOpsData) {
            final Collection<OperationDataHolder> entries = cacheOpsData.values();

            if (!entries.isEmpty()) {
                final Set<String> expiredEntries = entries.stream()
                                                          .filter(o -> expiredEntry(o, currentTime))
                                                          .map(o -> o.key)
                                                          .collect(Collectors.toSet());
                removeEntries(expiredEntries);
            }
        }
    }

    // ---------------------------------------------------------------
    private boolean expiredEntry(final OperationDataHolder data, long currentTime) {
        return (Math.abs(currentTime - data.lastAccessTime) > MAX_IDLE_TIMESPAN);
    }

    // ---------------------------------------------------------------
    private void removeEntries(final Set<String> keys) {
        log.debug("OperationDataCache removes expired entries " + keys.toString());

        for (final String key : keys) {
            removeEntry(key);
        }
    }

    // ---------------------------------------------------------------
    private void removeEntry(final String key) {
        final OperationDataHolder aHolder = cacheOpsData.remove(key);

        if (null != aHolder)
            destroyHolder(aHolder);
    }

    // ---------------------------------------------------------------
    private void destroyHolder(final OperationDataHolder dataHolder) {
        try {
            if (null != dataHolder.opsDocManagedFile)
                dataHolder.opsDocManagedFile.delete();
            if (null != dataHolder.htmlDocManagedFile)
                dataHolder.htmlDocManagedFile.delete();
            if (null != dataHolder.previewOpsManagedFile)
                dataHolder.previewOpsManagedFile.delete();
        } catch (Exception e) {
            // nothing to do - managed files are deleted automatically on timeout
        }
    }

    // ---------------------------------------------------------------
    private OperationData createOperationData(final OperationDataHolder dataHolder) throws Exception {
        JSONArray  jsonOpsArray    = null;
        JSONObject jsonPreviewData = null;
        String     htmlString      = null;

        try (final InputStream opsDataStream = dataHolder.opsDocManagedFile.getInputStream()) {
            byte[] aJsonBytes = IOUtils.toByteArray(opsDataStream);
            final String jsonArrayOpsString = new String(aJsonBytes, "UTF-8");
            aJsonBytes = null;
            jsonOpsArray = new JSONArray(jsonArrayOpsString);
        } finally {
            // nothing to do
        }

        try (final InputStream previewDataStream = dataHolder.previewOpsManagedFile.getInputStream()) {
            byte[] aJsonBytes = IOUtils.toByteArray(previewDataStream);
            final String jsonPreviewDataString = new String(aJsonBytes, "UTF-8");
            aJsonBytes = null;
            jsonPreviewData = new JSONObject(jsonPreviewDataString);
        } finally {
            // nothing to do
        }

        if (null != dataHolder.htmlDocManagedFile) {
            try (final InputStream htmlStream = dataHolder.htmlDocManagedFile.getInputStream()) {
                byte[] aJsonBytes = IOUtils.toByteArray(htmlStream);
                htmlString = new String(aJsonBytes, "UTF-8");
            } finally {
                // nothing to do
            }
        }

        return new OperationData(dataHolder.key, dataHolder.hash, dataHolder.metaData, htmlString, jsonPreviewData, jsonOpsArray);
    }

    // ---------------------------------------------------------------
    private OperationDataHolder createOperationDataHolder(final OperationData aOpsData) throws Exception {
        byte[] aOpsDocData = aOpsData.getOperations().toString().getBytes(Charset.forName("UTF-8"));
        final ManagedFile aOpsDataManagedFile = managedFileService.createManagedFile(aOpsDocData);
        aOpsDocData = null;

        ManagedFile aPreviewOpsManagedFile = null;
        if (null != aOpsData.getPreviewData()) {
            final byte[] aPreviewDocBytes = aOpsData.getPreviewData().toString().getBytes(Charset.forName("UTF-8"));
            aPreviewOpsManagedFile = managedFileService.createManagedFile(aPreviewDocBytes);
        }

        ManagedFile aHtmlDocManagedFile = null;
        if (null != aOpsData.getHtmlDoc()) {
            final byte[] aHtmlDocBytes = aOpsData.getHtmlDoc().getBytes(Charset.forName("UTF-8"));
            aHtmlDocManagedFile = managedFileService.createManagedFile(aHtmlDocBytes);
        }

        return new OperationDataHolder(aOpsData.getKey(), aOpsData.getHash(), aOpsData.getMetaData(), aHtmlDocManagedFile, aPreviewOpsManagedFile, aOpsDataManagedFile);
    }

}
