/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import org.apache.commons.lang3.Validate;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.message.OperationHelper;

public class OperationTools {
    /**
    *
    * @param docOSN
    */
   public static void setOperationStateNumberToOperations(final int docOSN, final JSONObject aOpsObject) throws JSONException {
       Validate.notNull(aOpsObject);

       // Ihe initial operation state number is determined by the stored osn or
       // must be the number of operations retrieved from the filter.
       int nUseOSN = (docOSN > 0) ? docOSN : aOpsObject.getJSONArray(OperationHelper.KEY_OPERATIONS).length();
       final JSONObject finalOperation = OperationHelper.getLastOperation(aOpsObject);
       if (finalOperation != null) {
           finalOperation.put(OperationHelper.KEY_OSN, nUseOSN - 1);
           finalOperation.put(OperationHelper.KEY_OPL, 1);
       }
   }

}
