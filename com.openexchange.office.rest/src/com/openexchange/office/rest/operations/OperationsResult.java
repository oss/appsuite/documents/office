/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.tools.common.error.ErrorCode;

public class OperationsResult implements OperationDataAccess {

    // ---------------------------------------------------------------
    private String htmlDoc;

    // ---------------------------------------------------------------
    private JSONObject previewData;

    // ---------------------------------------------------------------
    private JSONArray operations;

    // ---------------------------------------------------------------
    private ErrorCode errorCode;

    // ---------------------------------------------------------------
    private MetaData metaData;

    // ---------------------------------------------------------------
    private String hash;

    // ---------------------------------------------------------------
    public OperationsResult(ErrorCode errorCode, MetaData metaData, String htmlDoc, JSONObject previewData, JSONArray operations, String hash) {
        this.errorCode = errorCode;
        this.metaData = metaData;
        this.htmlDoc = htmlDoc;
        this.operations = operations;
        this.previewData = previewData;
        this.hash = hash;
    }

    // ---------------------------------------------------------------
    public OperationsResult(final ErrorCode errorCode) {
        Validate.isTrue(errorCode.isError());

        this.errorCode = errorCode;
        this.htmlDoc = null;
        this.operations = null;
        this.previewData = null;
        this.hash = null;
    }

    // ---------------------------------------------------------------
    public MetaData getMetaData() {
        return metaData;
    }

   // ---------------------------------------------------------------
    public ErrorCode getErrorCode() {
        return errorCode;
    }

    // ---------------------------------------------------------------
    @Override
    public String getHtmlDoc() {
        return htmlDoc;
    }

    // ---------------------------------------------------------------
    @Override
    public JSONObject getPreviewData() {
        return previewData;
    }

    // ---------------------------------------------------------------
    @Override
    public JSONArray getOperations() {
        return operations;
    }

    // ---------------------------------------------------------------
    @Override
    public String getHash() {
        return hash;
    }

}
