/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentFormatHelper;

public class DocumentContentAccess {
	private final byte[] contentData;
	private final String fileName;
	private final String mimeType;
	private final int size;
	private String hash = null;
	private String key;
	private DocumentFormat docFormat = DocumentFormat.NONE;

	public DocumentContentAccess(byte[] contentData, String fileName, String mimeType) {
		this.contentData = contentData;
		this.fileName = fileName;
		this.mimeType = mimeType;
		size = contentData.length;
	}

	public DocumentContentAccess(byte[] contentData, String fileName, String mimeType, String hash) {
		this.contentData = contentData;
		this.fileName = fileName;
		this.mimeType = mimeType;
		this.hash = hash;
		size = contentData.length;
		docFormat = mapMimeTypeToDocumentFormat(mimeType);
	}

	public ByteArrayInputStream getStream() {
		return new ByteArrayInputStream(contentData);
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public String getHash() throws NoSuchAlgorithmException {
		if (hash == null) {
			hash = MessageDigest.getInstance("SHA-256").digest(contentData).toString();
		}
		return hash;
	}

    public String getFileName() {
    	return fileName;
    }

    public String getMimeType() {
    	return mimeType;
    }

    public int getSize() {
    	return size;
    }

    public DocumentFormat getDocFormat() {
    	return docFormat;
    }

    private DocumentFormat mapMimeTypeToDocumentFormat(String mimeType) {
    	DocumentFormat result = DocumentFormat.NONE;

    	final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(mimeType, "");
    	if (docFormatInfo != null) {
    		docFormat = DocumentFormat.valueOf(docFormatInfo.get("DocumentType"));
    	}

    	return result;
    }

}
