/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import com.openexchange.office.datasource.access.DataSourceResourceAccess;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.rest.job.Job;
import com.openexchange.tools.session.ServerSession;

public class GetOperationsJob extends Job {

    private final ServerSession session;
    private final DataSourceResourceAccess docAccess;
    private final DocumentOperationService documentOperationService;
    private final OperationDataCache operationDataCache;

    // ---------------------------------------------------------------
    public GetOperationsJob(ServerSession session, DataSourceResourceAccess docAccess, DocumentOperationService documentOperationService, OperationDataCache operationDataCache) {
        this.session = session;
        this.docAccess = docAccess;
        this.documentOperationService = documentOperationService;
        this.operationDataCache = operationDataCache;
    }

    // ---------------------------------------------------------------
    @Override
    public boolean execute() throws Exception {
    	final MetaData metaData = docAccess.getMetaData();
        final String hash = metaData.getHash();
        final OperationsResult aResult = documentOperationService.getOperationsFromDataSourceAccess(docAccess, session);

        if (aResult.getErrorCode().isNoError()) {
            final String aEntryKey = OperationDataCache.createKey(docAccess.getFolder(), docAccess.getId(), docAccess.getVersionOrAttachmentId(), docAccess.getContextId());
            final OperationData aData = new OperationData(aEntryKey, hash, aResult.getMetaData(), aResult.getHtmlDoc(), aResult.getPreviewData(), aResult.getOperations());

            operationDataCache.put(aEntryKey, aData);
        }
        return aResult.getErrorCode().isNoError();
    }
}
