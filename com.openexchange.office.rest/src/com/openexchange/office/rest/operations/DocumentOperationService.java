/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.operations;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.java.Streams;
import com.openexchange.office.datasource.access.DataSourceResourceAccess;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.tools.ChunkableDocLoader;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.core.FilterExceptionToErrorCode;
import com.openexchange.office.htmldoc.DocMetaData;
import com.openexchange.office.htmldoc.GenericHtmlDocumentBuilder;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.rest.tools.ConversionHelper;
import com.openexchange.office.rest.tools.ConversionHelper.ConversionData;
import com.openexchange.office.rest.tools.RESTException;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.office.tools.common.system.SystemInfoHelper;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.ExtensionHelper;
import com.openexchange.tools.session.ServerSession;

@Service
public class DocumentOperationService {

	private static final Logger log = LoggerFactory.getLogger(DocumentOperationService.class);

    @Autowired
    private OXDocumentFactory oxDocFactory;

    @Autowired
    private ImExporterAccessFactory imExporterFactory;

    @Autowired
    private IResourceManagerFactory resourceManagerFactory;

    @Autowired
    private ConversionHelper conversionHelper;

    @Autowired
    private GenericHtmlDocumentBuilder genericHtmlDocumentBuilder;

    @Autowired(required = false)
    private IDocumentConverter documentConverter;

    // ---------------------------------------------------------------
    public OperationsResult getOperationsFromDataSourceAccess(final DataSourceResourceAccess docAccess, final ServerSession session) throws Exception {
    	final MetaData metaData = docAccess.getMetaData();
        final String fileName = metaData.getFileName();
        final String mimeType = metaData.getMimeType();

        InputStream documentStream = null;
        String extension = FileHelper.getExtension(fileName, true);
        DocumentFormat docFormat;
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        final Map<String,String> conversionFormat = DocumentFormatHelper.getConversionFormatInfo(mimeType, extension);
        if (conversionFormat == null) {
        	final Map<String, String> docFormatInfo = DocumentFormatHelper.getFormatInfoForMimeTypeOrExtension(mimeType, extension);
        	if (docFormatInfo == null)
        		throw new RESTException(ErrorCode.LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR);

    		docFormat = DocumentFormat.valueOf(docFormatInfo.get("DocumentFormat"));
    		documentStream = docAccess.getContentStream();
        } else {
        	InputStream documentInputStm = null;
        	ConversionData conversionData = null;
        	try {
        		documentInputStm = docAccess.getContentStream();
        		conversionData = conversionHelper.convertDocument(session, documentInputStm, fileName, conversionFormat, docAccess.getAuthCode().isPresent());
        	} finally {
                Streams.close(documentInputStm);
            }

            errorCode = conversionData.getErrorCode();
            documentStream = conversionData.getInputStream();
            extension = conversionData.getExtension();
            docFormat = ExtensionHelper.getDocumentFormatFromExtension(extension);
            metaData.setMimeType(conversionFormat.get(DocumentFormatHelper.PROP_MIME_TYPE));
        }

        try {
            if (errorCode.isNoError()) {
                return loadDocumentFromStream(session, documentStream, docFormat, metaData);
            }
        } finally {
            Streams.close(documentStream);
        }

        errorCode = errorCode.isNoError() ? ErrorCode.GENERAL_UNKNOWN_ERROR : errorCode;
        return new OperationsResult(errorCode);
    }

    // ---------------------------------------------------------------
    public OperationsResult loadDocumentFromStream(final ServerSession session, final InputStream documentStream, DocumentFormat docFormat, final MetaData metaData ) throws Exception {
        final ApplicationType appType = ApplicationType.documentFormatToApplicationType(docFormat);

        switch (appType) {
            case APP_PRESENTATION:
            case APP_TEXT:
            case APP_SPREADSHEET:  return loadDocumentFromStream(session, documentStream, appType, docFormat, metaData);
            default:               throw new IllegalArgumentException("Unknown application type provided to ");
        }
    }

    // ---------------------------------------------------------------
    public static boolean isMemConsumptionForDocumentOk(long nFileSize, String encryptionInfo) {
        final MemoryObserver aMemObserver = MemoryObserver.get();

        final float fCurrentFileSizeFactor = StringUtils.isEmpty(encryptionInfo) ? 1.0f : OXDocument.PGP_FILE_SIZE_FACTOR;
        final long additonalMemNeeded = Math.round(nFileSize * fCurrentFileSizeFactor);
        return aMemObserver.willHeapLimitBeReached(additonalMemNeeded);
    }

    // ---------------------------------------------------------------
    private OperationsResult loadDocumentFromStream(ServerSession session, InputStream docStream, ApplicationType appType, DocumentFormat docFormat, MetaData metaData) throws Exception {
        final IResourceManager resManager = resourceManagerFactory.createInstance();
        final OXDocument oxDocument = oxDocFactory.createDocumentAccess(session, docStream, metaData.getFileName(), metaData.getMimeType(), resManager, null);

        String htmlDoc = null;
        JSONObject operations = new JSONObject();

        //final String module = ApplicationType.enumToString(appType);

        // Initialize the error code retrieved by our OXDocument instance. The error code is
        // set by the ctor of the instance which tries to retrieve the document stream.
        ErrorCode errorCode = oxDocument.getLastError();

        // check error code after loading the document
        if (errorCode.isNoError()) {
            try {
                final IImporter importer = imExporterFactory.getImporterAccess(docFormat);
                if (importer != null) {
                    final String module = ApplicationType.enumToString(appType);
                    final boolean useFastLoad = genericHtmlDocumentBuilder.isFastLoadActive(module, session) && GenericHtmlDocumentBuilder.isFastLoadSupported(module);
                    final ChunkableDocLoader chunkDocLoader = new ChunkableDocLoader(oxDocument, importer, useFastLoad);
                    chunkDocLoader.prepareLoad();

                    final int partsCount = chunkDocLoader.getPartsCount();

                    JSONObject previewData = null;
                    if ((appType == ApplicationType.APP_SPREADSHEET) && (partsCount > 1)) {
                        // we need to send the operations in two parts - possible rescue
                        // operations will be sent via the second part
                        previewData = chunkDocLoader.getGlobalOperations();
                        previewData.put(MessagePropertyKey.KEY_ACTIVESHEET, chunkDocLoader.getActivePartIndex());
                        previewData.put(MessagePropertyKey.KEY_PREVIEW, true);
                        OperationHelper.appendJSON(previewData, chunkDocLoader.getActiveOperations());
                    } else {

                        OperationHelper.appendJSON(operations, chunkDocLoader.getGlobalOperations());
                        OperationHelper.appendJSON(operations, chunkDocLoader.getActiveOperations());

                        if (useFastLoad) {
                            final DocMetaData docMetaData = convertMetaData(metaData);
                            htmlDoc = genericHtmlDocumentBuilder.buildHtmlDocument(module, operations, docMetaData, session);
                            if (htmlDoc == null) { // an error happened, try to load the document again without fastload
                                chunkDocLoader.setCreateFastLoadOperations(false);
                                chunkDocLoader.prepareLoad();
                                operations = new JSONObject();
                                OperationHelper.appendJSON(operations, chunkDocLoader.getGlobalOperations());
                                OperationHelper.appendJSON(operations, chunkDocLoader.getActiveOperations());
                            }
                        }
                    }

                    OperationHelper.appendJSON(operations, chunkDocLoader.getRemainingOperations());
                    OperationTools.setOperationStateNumberToOperations(oxDocument.getUniqueDocumentId(), operations);
                    chunkDocLoader.prepareReplacementGraphics(appType, documentConverter, session);

                    return new OperationsResult(errorCode, metaData, htmlDoc, previewData, getOperationsArray(operations), metaData.getHash());
                }

                errorCode = ErrorCode.LOADDOCUMENT_NO_FILTER_FOR_DOCUMENT_ERROR;
            } catch (final FilterException e) {
                errorCode = handleFilterException(e);
            } catch (final Exception e) {
                errorCode = ErrorCode.LOADDOCUMENT_FAILED_ERROR;
                log.warn("GetOperationsActions: Exception caught, e");
            } finally {
                IOHelper.closeQuietly(docStream);
            }
        }

        return new OperationsResult(errorCode);
    }


    // ---------------------------------------------------------------
    private ErrorCode handleFilterException(FilterException e) {
        ErrorCode errorCode = ErrorCode.LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR;

        final Level level = FilterExceptionToErrorCode.determineLogLevel(e);
        FilterExceptionToErrorCode.log(log, level, "GetOperationsActions caught filter exception.", "GetOperationsAction", e);

        if ((e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_TOO_HIGH) || (e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED)) {
            // try to figure out what happened with the memory usage of the filter as
            // we want to provide two different error codes.
            errorCode = ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR;
            if (e.getErrorcode() == FilterException.ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED) {
                final String freeMemString = ((Long) (SystemInfoHelper.freeMemory() / SystemInfoHelper.MBYTES)).toString();
                log.info("Document could not be loaded, because current available heap space is not sufficient. Current free heap space: {} MB", freeMemString);
            }
        } else {
            // all other exceptions are mapped by general aspects
            errorCode = FilterExceptionToErrorCode.map(e, errorCode);
        }

        return errorCode;
    }

    // ---------------------------------------------------------------
    private static JSONArray getOperationsArray(final JSONObject opsObject) {
        return ((null == opsObject) || (null == opsObject.optJSONArray("operations"))) ? null : opsObject.optJSONArray("operations");
    }

    // ---------------------------------------------------------------
    private static DocMetaData convertMetaData(MetaData metaData) {
        return new DocMetaData(metaData.getFolderId(), metaData.getId(), metaData.getVersionOrAttachment(),
                               metaData.getFileSize(), metaData.getFileName(), metaData.getSource(), metaData.getMimeType());
    }
}
