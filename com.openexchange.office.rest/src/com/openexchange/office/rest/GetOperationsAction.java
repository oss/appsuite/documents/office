/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.log.LogProperties;
import com.openexchange.office.datasource.access.DataSource;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceAccessFactory;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.DataSourceResourceAccess;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.rest.job.AsyncJobWorker;
import com.openexchange.office.rest.operations.DocumentContentAccess;
import com.openexchange.office.rest.operations.DocumentOperationService;
import com.openexchange.office.rest.operations.GetOperationsJob;
import com.openexchange.office.rest.operations.OperationData;
import com.openexchange.office.rest.operations.OperationDataAccess;
import com.openexchange.office.rest.operations.OperationDataCache;
import com.openexchange.office.rest.operations.OperationsResult;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.RESTException;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.user.AuthorizationCache;
import com.openexchange.office.tools.doc.SyncInfo;
import com.openexchange.office.tools.service.encryption.EncryptionInfo;
import com.openexchange.office.tools.service.encryption.EncryptionInfoService;
import com.openexchange.tools.session.ServerSession;

@RestController
public class GetOperationsAction extends DocumentRESTAction {

    // ---------------------------------------------------------------
    private static final Logger LOG = LoggerFactory.getLogger(GetOperationsAction.class);
    private static final String[] MANDATORY_PARAMS = { ParameterDefinitions.PARAM_FILE_ID, ParameterDefinitions.PARAM_FOLDER_ID };

    @Autowired
    private DataSourceAccessFactory dataSourceAccessFactory;

    @Autowired
    private DocumentOperationService documentOperationService;

    @Autowired
    private EncryptionInfoService encryptionInfoService;

    @Autowired
    private OperationDataCache operationDataCache;

    // ---------------------------------------------------------------
    private AsyncJobWorker<GetOperationsJob> aGetOpsAsyncWorker = new AsyncJobWorker<>(1);

    // ---------------------------------------------------------------
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult aResult = null;

        if (!paramValidatorService.areAllParamsNonEmpty(requestData, MANDATORY_PARAMS))
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        final String folderId = requestData.getParameter(ParameterDefinitions.PARAM_FOLDER_ID);
        final String id = requestData.getParameter(ParameterDefinitions.PARAM_FILE_ID);
        final String versionOrAttachmentId = requestData.getParameter(ParameterDefinitions.PARAM_VERSION);
        final String authCodeStr = requestData.getParameter(ParameterDefinitions.PARAM_AUTH_CODE);
        final String dataSource = ParamValidatorService.getParameterOrDefault(requestData, ParameterDefinitions.PARAM_SOURCE, String.class, DataSource.DRIVE);

        JSONObject jsonResult = new JSONObject();
        ErrorCode errorCode  = ErrorCode.NO_ERROR;

        DataSourceAccess dataSourceAccess = null;
        try {
        	dataSourceAccess = dataSourceAccessFactory.createDataSourceAccess(session, dataSource);
        } catch (DataSourceException e) {
            LOG.error("GetOperationsAction: DataSourceException caught trying to retrieve document meta data for provided parameters", e);

            errorCode = e.getErrorCode();
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);
        }

        try {
            if (!dataSourceAccess.canRead(folderId, id, versionOrAttachmentId))
                return ParamValidatorService.getResultFor(HttpStatusCode.FORBIDDEN.getStatusCode());
        } catch (DataSourceException e) {
            LOG.error("GetOperationsAction: DataSourceException caught trying to retrieve document meta data for provided parameters", e);

            errorCode = e.getErrorCode();
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);
        }

        final Optional<String> authCode = Optional.ofNullable(authCodeStr);
        String subAction = requestData.getParameter(ParameterDefinitions.PARAM_SUB_ACTION);
        subAction = (null == subAction) ? ParameterDefinitions.PARAM_SUBACTION_OPEN : subAction;

        MetaData metaData;
        try {
            metaData = dataSourceAccess.getMetaData(folderId, id, versionOrAttachmentId, authCode);
        } catch (DataSourceException e) {
            LOG.error("GetOperationsAction: DataSourceException caught trying to retrieve document meta data for provided parameters", e);

            errorCode = e.getErrorCode();
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);
        } catch (Exception e) {
            LOG.error("GetOperationsAction: OXException caught trying to retrieve document content for provided parameters", e);
            errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            return new AJAXRequestResult(jsonResult);
        }

        final boolean encrypted = FileHelper.isEncryptedExtension(FileHelper.getExtension(metaData.getFileName()));

        try {
            if (subAction.equalsIgnoreCase(ParameterDefinitions.PARAM_SUBACTION_OPEN)) {
                OperationData aCachedData = operationDataCache.get(OperationDataCache.createKey(folderId, id, versionOrAttachmentId, session.getContextId()));

                // check up-to-date state of the cached data - if not set to null and ensure to load latest document data
                if (null != aCachedData)
                    aCachedData = metaData.getHash().equals(aCachedData.getHash()) ? aCachedData : null;

                if (null == aCachedData) {
                	final DataSourceResourceAccess docAccess = dataSourceAccess.createResourceAccess(folderId, id, versionOrAttachmentId, authCode);
                    // cache miss or cache entry dirty
                    final OperationsResult aDocOpsResult = documentOperationService.getOperationsFromDataSourceAccess(docAccess, session);

                    errorCode = aDocOpsResult.getErrorCode();
                    if (errorCode.isNoError()) {
                        jsonResult = getJsonResult(aDocOpsResult, folderId, id);

                        final String key = OperationDataCache.createKey(folderId, id, versionOrAttachmentId, session.getContextId());
                        final OperationData data = new OperationData(key,
                                                                     aDocOpsResult.getHash(),
                                                                     aDocOpsResult.getMetaData(),
                                                                     aDocOpsResult.getHtmlDoc(),
                                                                     aDocOpsResult.getPreviewData(),
                                                                     aDocOpsResult.getOperations());
                        operationDataCache.put(key, data);
                    }

                    try {
                        MDCHelper.safeMDCPut(LogProperties.Name.SESSION_CONTEXT_ID.getName(), Integer.toString(session.getContextId()));
                        LOG.info("GetOperationsAction: Document {} in context {} was not found in cache", id, session.getContextId());
                    } finally {
                        MDC.remove(LogProperties.Name.SESSION_CONTEXT_ID.getName());
                    }
                } else {
                    // cache hit
                    jsonResult = getJsonResult(aCachedData, folderId, id);

                    try {
                        MDCHelper.safeMDCPut(LogProperties.Name.SESSION_CONTEXT_ID.getName(), Integer.toString(session.getContextId()));
                        LOG.info("GetOperationsAction: Document {} in context {} found in cache", id, session.getContextId());
                    } finally {
                        MDC.remove(LogProperties.Name.SESSION_CONTEXT_ID.getName());
                    }
                }

                jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());

                if (StringUtils.isNotEmpty(authCodeStr)) {
                    final EncryptionInfo encryptionInfo = encryptionInfoService.createEncryptionInfo(session, authCodeStr);
                    if (encryptionInfo.isPresent()) {
                        AuthorizationCache.addKey(String.valueOf(session.getContextId()), String.valueOf(session.getUserId()), id, authCodeStr);
                    }
                }

                aResult = new AJAXRequestResult(jsonResult);
            } else if (subAction.equalsIgnoreCase(ParameterDefinitions.PARAM_SUBACTION_CLOSE)) {
                AuthorizationCache.removeKey(String.valueOf(session.getContextId()), String.valueOf(session.getUserId()), id);
                aResult = ParamValidatorService.getResultFor(HttpStatusCode.OK.getStatusCode());
            } else if (subAction.equalsIgnoreCase(ParameterDefinitions.PARAM_SUBACTION_PREFETCH)) {
                if (!encrypted) {
                    // no pre-fetch for encrypted documents
                    if (!operationDataCache.hasEntryAndTouch(OperationDataCache.createKey(folderId, id, versionOrAttachmentId, session.getContextId())))
                        aGetOpsAsyncWorker.addJob(new GetOperationsJob(session, dataSourceAccess.createResourceAccess(folderId, id, versionOrAttachmentId, authCode), documentOperationService, operationDataCache));
                }
                aResult = ParamValidatorService.getResultFor(HttpStatusCode.OK.getStatusCode());
            } else {
                return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
            }
        } catch (final OXException e) {
            errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            aResult = new AJAXRequestResult(jsonResult);

            LOG.error("GetOperationsAction: OXException caught trying to generate operations for provided document",e );
        } catch (final DataSourceException e) {
            errorCode = e.getErrorCode();
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            aResult = new AJAXRequestResult(jsonResult);

            LOG.error("GetOperationsAction: Exception caught trying to generate operations for provided document", e);
        } catch (Exception e) {
            errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
            try {jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON()); } catch (final JSONException je) { /* ignore it */ }
            aResult = new AJAXRequestResult(jsonResult);

            LOG.error("GetOperationsAction: Exception caught trying to generate operations for provided document",e );
        }

        return aResult;
    }

    // ---------------------------------------------------------------
    private static JSONObject getJsonResult(final OperationDataAccess aOpsData, String folderId, String id) throws JSONException {
        final JSONObject jsonResult = new JSONObject();

        jsonResult.put(MessagePropertyKey.KEY_OPERATIONS, aOpsData.getOperations());
        if (null != aOpsData.getPreviewData())
            jsonResult.put(MessagePropertyKey.KEY_PREVIEW, aOpsData.getPreviewData());
        if (null != aOpsData.getHtmlDoc())
            jsonResult.put(MessagePropertyKey.KEY_HTMLDOCUMENT, aOpsData.getHtmlDoc());

        // Add sync info to result to be compatible with normal join response
        // as we don't support local storage load always provide version 1 and
        // document-osn = -1 (set by default).
        final SyncInfo aSyncInfo = new SyncInfo(folderId, id);
        aSyncInfo.updateFileVersion(SyncInfo.SYNC_FIRST_VERSION);
        aSyncInfo.setFileName(aOpsData.getMetaData().getFileName());
        jsonResult.put(SyncInfo.SYNC_INFO, aSyncInfo.toJSON());
        return jsonResult;
    }

    // ---------------------------------------------------------------
    public void shutdown() {
        aGetOpsAsyncWorker.shutdown();
    }

    // ---------------------------------------------------------------
    public static String getHashForDocument(final DocumentContentAccess docAccess) throws RESTException {
    	try {
            return docAccess.getHash();
    	} catch (NoSuchAlgorithmException e) {
    		throw new RESTException(ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR, e);
    	}
    }
}
