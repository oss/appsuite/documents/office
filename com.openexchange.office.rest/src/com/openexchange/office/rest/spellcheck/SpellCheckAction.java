/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.spellcheck;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.base.Throwables;
import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.annotation.Nullable;
import com.openexchange.office.spellcheck.api.ISpellCheck;

/**
 * {@link SpellCheckAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
public abstract class SpellCheckAction implements AJAXActionService  {

    /**
     * LOG
     */
    final protected static Logger LOG = LoggerFactory.getLogger(SpellCheckAction.class);

	/**
	 * @param jsonResult
	 * @return
	 */
	protected AJAXRequestResult handleJSONResult(@Nullable JSONObject jsonResult) {
        final AJAXRequestResult requestResult = new AJAXRequestResult();

        requestResult.setResultObject((null != jsonResult) ? jsonResult: new JSONObject());

        return requestResult;
	}

	/**
	 * @param e
	 */
	protected void handleException(final Throwable e) {
	    LOG.error("Exception caught within SpellCheck request handler", Throwables.getRootCause(e));
	}

	// - Members ---------------------------------------------------------------

	@Autowired
	protected ISpellCheck m_spellCheck;
}
