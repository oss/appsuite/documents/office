/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.spellcheck;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link SpellWordAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@RestController
public class SpellWordAction extends SpellCheckAction {

    final private static String KEY_WORD = "word";
    final private static String KEY_TEXT = "text";
    final private static String KEY_LOCALE = "locale";
    final private static String KEY_OFFSET = "offset";
    final private static String KEY_NOREPLACEMENTS = "noReplacements";

    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        final String word = request.getParameter(KEY_WORD);
        final String locale = request.getParameter(KEY_LOCALE);
        final String offsetStr = request.getParameter(KEY_OFFSET);
        final String noReplacementsStr = request.getParameter(KEY_NOREPLACEMENTS);
        final int offset = (null != offsetStr) ? Integer.parseInt(offsetStr) : 0;
        final boolean noReplacements = (null != noReplacementsStr) && "true".equalsIgnoreCase(noReplacementsStr);
        JSONObject spellResult = null;

        if (StringUtils.isNotEmpty(word) && (null != locale)) {
            try {
                spellResult = m_spellCheck.checkSpelling(
                    new JSONObject().put(KEY_TEXT, word).put(KEY_LOCALE, locale).put(KEY_OFFSET, offset).put(KEY_NOREPLACEMENTS, noReplacements));
            } catch (Exception e) {
                handleException(e);
            }
        }

        return handleJSONResult(spellResult);
    }
}
