/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.spellcheck;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link SpellWordReplacementsAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@RestController
public class SpellWordReplacementsAction extends SpellCheckAction {

    final private static String KEY_WORD = "word";
    final private static String KEY_LOCALE = "locale";

    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        final String word = request.getParameter(KEY_WORD);
        final String locale = request.getParameter(KEY_LOCALE);
        JSONObject spellResult = new JSONObject();

        if (StringUtils.isNotEmpty(word) && (null != locale)) {
            try {
                spellResult = m_spellCheck.suggestReplacements(
                    new JSONObject().put(KEY_WORD, word).put(KEY_LOCALE, locale));
            } catch (Exception e) {
                handleException(e);
            }
        }

        return handleJSONResult(spellResult);
    }
}
