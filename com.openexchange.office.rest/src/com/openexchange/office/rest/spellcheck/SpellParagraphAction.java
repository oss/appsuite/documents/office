/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.spellcheck;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link SpellParagraphAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@RestController
public class SpellParagraphAction extends SpellCheckAction {

    final private static String KEY_PARAGAPH = "paragraph";
    final private static String KEY_NOREPLACEMENTS = "noReplacements";

    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        JSONObject spellResult = null;
        final String noReplacementsStr = request.getParameter(KEY_NOREPLACEMENTS);
        final boolean noReplacements = (null != noReplacementsStr) && "true".equalsIgnoreCase(noReplacementsStr);

        try {
            spellResult = m_spellCheck.checkParagraphSpelling(
                new JSONObject().put(KEY_PARAGAPH, new JSONArray(request.getParameter(KEY_PARAGAPH))).put(KEY_NOREPLACEMENTS, noReplacements));
        } catch (Exception e) {
            handleException(e);
        }

        return handleJSONResult(spellResult);
    }
}
