/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.osgi;

import java.util.HashSet;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.office.rest.GetFileAction;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

//=============================================================================
public class OfficeRestBundleContextAndActivator extends OsgiBundleContextAndActivator {

	public OfficeRestBundleContextAndActivator() {
		super(new OfficeRestBundleContextIdentificator());
	}

	@Override
	protected Set<Class<?>> getAdditionalNeededServices() {
		Set<Class<?>> res = new HashSet<>();
		res.add(com.openexchange.net.ssl.SSLSocketFactoryProvider.class);
		res.add(com.openexchange.net.ssl.config.SSLConfigurationService.class);
		return res;
	}

    @Override
    public void shutdown() throws OXException {
        final GetFileAction getFileActionService = getService(GetFileAction.class);

        if (null != getFileActionService) {
            getFileActionService.shutdown();
        }
	}
}
