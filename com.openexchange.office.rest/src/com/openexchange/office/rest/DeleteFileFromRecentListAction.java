/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.recentfilelist.RecentFileListManager;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.SessionIdCheck;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;

@RestController
public class DeleteFileFromRecentListAction extends DocumentRESTAction {
	private static final Logger LOG = LoggerFactory.getLogger(DeleteFileFromRecentListAction.class);
    private static final String[] MANDATORY_PARAMS = { ParameterDefinitions.PARAM_TYPE, ParameterDefinitions.PARAM_REMOVE_FILE_ID };

    @Autowired
    private FolderHelperServiceFactory folderHelperServiceFactory;
    
    @Autowired
    private RecentFileListManagerFactory recentFileListManagerFactory;
    
    
    
    @Override
	public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        if (!paramValidatorService.areAllParamsNonEmpty(requestData, MANDATORY_PARAMS))
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        final String type = requestData.getParameter(ParameterDefinitions.PARAM_TYPE).toLowerCase();
    	final ApplicationType appType = ApplicationType.stringToEnum(type);
        AJAXRequestResult requestResult = null;

        if ((null != session) && (appType != ApplicationType.APP_NONE)) {
            // #60801: we need to check for an illegal CSRF attempt by checking the
            // correct sessionId; set errorcode to 400 in case of an invalid request
            if (SessionIdCheck.isNotValid(requestData.getParameter("session"), session)) {
                LOG.warn("DeleteFileFromRecentListAction detected invalid session id - request rejected!");
                return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
            }

            try {
            	final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
                final String removeFileId = requestData.getParameter(ParameterDefinitions.PARAM_REMOVE_FILE_ID);
            	final JSONObject remove = new JSONObject();
            	remove.put("folder_id", folderHelperService.getFolderId(removeFileId));
            	remove.put("id", removeFileId);

                final RecentFileListManager recentFileListManager = recentFileListManagerFactory.create(session);
                final List<JSONObject> appRecentFiles = recentFileListManager.readRecentFileList(appType);
                if ((null != appRecentFiles) && (!appRecentFiles.isEmpty())) {
                    boolean changed = recentFileListManager.removeFileFromList(appRecentFiles, remove);
                    if (changed) {
                        recentFileListManager.writeRecentFileList(appType, appRecentFiles);
                        recentFileListManager.flush();
                    }
                }
            } catch (JSONException e) {
                LOG.error("Exception while creating JSONObject answering AJAXRequest in 'DeleteFileFromRecentListAction'", e);
            }
            requestResult = new AJAXRequestResult();
        }

        return requestResult;
	}

}
