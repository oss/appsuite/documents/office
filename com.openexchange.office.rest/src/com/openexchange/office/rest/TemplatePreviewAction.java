/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import static com.openexchange.java.Strings.isEmpty;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.container.FileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.DispatcherNotes;
import com.openexchange.conversion.DataProperties;
import com.openexchange.conversion.SimpleData;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.office.document.tools.LocalFileMappingManager;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.ExtensionHelper;
import com.openexchange.preview.PreviewDocument;
import com.openexchange.preview.PreviewExceptionCodes;
import com.openexchange.preview.PreviewOutput;
import com.openexchange.preview.PreviewService;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;


/**
 * {@link TemplatePreviewAction}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */

@DispatcherNotes(defaultFormat = "file", allowPublicSession = true)
@RestController
public class TemplatePreviewAction extends DocumentRESTAction {

	private static final Logger LOG = LoggerFactory.getLogger(TemplatePreviewAction.class);
    private static final long TEMPLATEPREVIEW_CACHE_EXPIRES = 10 * 60 * 60 * 1000; // 10 hours

    @Autowired
    private LocalFileMappingManager m_localFileMapper;
    
    @Autowired(required=false)
    private PreviewService previewService; 

    /* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult requestResult = null;
        final String id = requestData.getParameter("id");
        final String folder = requestData.getParameter("folder");

        if ((null == session) || (null == id) || (null == folder))
            throw AjaxExceptionCodes.MISSING_PARAMETER.create("Session, id or folder parameters are missing or have invalid values.");

        // we have to check if we received a ID referencing a local file or a file stored in OX Drive.
        if (!id.startsWith(LocalFileMappingManager.prefixID))
            throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("id", "Template preview should only be called for global template files.");

        // Local template file
        var fullFilePath = m_localFileMapper.getPathFromId(id);

        if (null == fullFilePath)
            throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("id", "Unkown template file id");

        if (null == previewService)
            throw AjaxExceptionCodes.UNEXPECTED_ERROR.create("PreviewService is not available.");

        try (var stream = m_localFileMapper.getInputStreamFromID(id)) {

            if (null == stream)
                throw AjaxExceptionCodes.UNEXPECTED_ERROR.create("Template file could not be read.");

            final String mimeType = requestData.getParameter("mimeType");
            final String fileName = FileHelper.getFileName(fullFilePath);
            final DataProperties dataProperties = new DataProperties(7);

            try {
                String delivery = requestData.getParameter("delivery");
                String scaleType = requestData.getParameter("scaleType");
                String previewType = requestData.getParameter("previewType");
                String contentType = getContentType(fileName, mimeType);

                LOG.debug("TemplatePreview: fullFilePath={}, fileName= {}, contentType={}", fullFilePath, ((null != fileName) ? fileName : "null"), ((null != contentType) ? contentType : "null"));
                dataProperties.put(DataProperties.PROPERTY_CONTENT_TYPE, contentType);
                dataProperties.put(DataProperties.PROPERTY_NAME, fileName);
                dataProperties.put("PreviewType", (previewType == null) ? "Thumbnail" : previewType);
                dataProperties.put("PreviewWidth", requestData.getParameter("width"));
                dataProperties.put("PreviewHeight", requestData.getParameter("height"));
                dataProperties.put("PreviewDelivery", (delivery == null) ? "view" : delivery);
                dataProperties.put("PreviewScaleType", (scaleType == null) ? "contain" : scaleType);
                PreviewDocument previewDocument = previewService.getPreviewFor(new SimpleData<>(stream, dataProperties), PreviewOutput.IMAGE, session, 1);

                try (var thumbnail = previewDocument.getThumbnail()) {
                    if (null == thumbnail) {
                        // No thumbnail available
                        throw PreviewExceptionCodes.THUMBNAIL_NOT_AVAILABLE.create("Preview image has not been generated in time");
                    }

                    // Prepare response
                    if(previewDocument.getClass().getName().equals("com.openexchange.documentpreview.OfficePreviewDocument")) {
                        requestData.putParameter("transformationNeeded", "false");
                    }

                    var bytes = Streams.stream2bytes(thumbnail);
                    var responseFileHolder = new FileHolder(Streams.newByteArrayInputStream(bytes), bytes.length, "image/jpeg", "image.jpg");
                    requestResult = new AJAXRequestResult();
                    requestResult.setResultObject(responseFileHolder, "preview_image");
                    requestResult.setFormat("preview_image");
                    requestResult.setExpires(TEMPLATEPREVIEW_CACHE_EXPIRES);
                    requestResult.setHeader("ETag", id);
                }
            } catch (IOException e) {
                throw AjaxExceptionCodes.IO_ERROR.create(e, e.getMessage());
            } catch (RuntimeException e) {
                throw AjaxExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
            }
        } catch (IOException e) {
            throw AjaxExceptionCodes.IO_ERROR.create("Template file could not be read.");
        }

        return requestResult;
    }

    /**
     * Provide the content type (mime type) based on the provided contentType
     * or fileName.
     *
     * @param fileName
     *  The file name of the template file including the extension.
     *
     * @param contentType
     *  A mime type of the template file or null if unknown.
     *
     * @return
     *  The content type (mime type) of the template file based on the provided
     *  content type of file name. Can be null if the content type was not
     *  recognized.
     */
    private static String getContentType(final String fileName, final String contentType) {
        final String fallbackContentType = "application/octet-stream";

        String mimeType = contentType;
        if (isEmpty(contentType)) {
            // Determine Content-Type by file name
            String ext = FileHelper.getExtension(fileName);
            mimeType = ExtensionHelper.getMimeTypeFromTemplateExtension(ext);
        }
        return (null == mimeType) ? fallbackContentType : mimeType;
    }
}
