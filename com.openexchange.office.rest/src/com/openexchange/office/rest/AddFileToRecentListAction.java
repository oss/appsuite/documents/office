/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.recentfilelist.RecentFileListManagerFactory;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.RecentFileListHelper;
import com.openexchange.office.rest.tools.SessionIdCheck;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.tools.session.ServerSession;

@SuppressWarnings("rawtypes")
@RestController
public class AddFileToRecentListAction extends DocumentRESTAction {

    private static final Logger LOG = LoggerFactory.getLogger(AddFileToRecentListAction.class);
    private static final String[] MANDATORY_PARAMS = { ParameterDefinitions.PARAM_TYPE, ParameterDefinitions.PARAM_FILE_ID };

    @Autowired
    private IDBasedFileAccessFactory fileFactory;
    
    @Autowired
    private RecentFileListManagerFactory recentFileListManagerFactory;
    
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        if (!paramValidatorService.areAllParamsNonEmpty(requestData, MANDATORY_PARAMS))
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        var type = requestData.getParameter(ParameterDefinitions.PARAM_TYPE).toLowerCase();
        var appType = ApplicationType.stringToEnum(type);
        AJAXRequestResult requestResult = null;

        if ((null != session) && (appType != ApplicationType.APP_NONE)) {
            if (SessionIdCheck.isNotValid(requestData.getParameter("session"), session)) {
                LOG.warn("AddFileToRecentListAction detected invalid session id - request rejected!");
                return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
            }

            var fileAccess = ((null != fileFactory)) ? fileFactory.createAccess(session) : null;
            if (fileAccess != null) {
                try {
                    var recentFileListManager = recentFileListManagerFactory.create(session);
                    var addFileId = requestData.getParameter(ParameterDefinitions.PARAM_FILE_ID);
                    var fileToAdd = RecentFileListHelper.getFileMetaData(fileAccess, addFileId);
                    var addFileMetaData = RecentFileListHelper.getFileMetaDataAsJSON(fileToAdd);
                    var appRecentFiles = recentFileListManager.readRecentFileList(appType);

                    recentFileListManager.addCurrentFile(appRecentFiles, addFileMetaData, fileToAdd.getLastModified());
                    recentFileListManager.writeRecentFileList(appType, appRecentFiles);
                    recentFileListManager.flush();
                } catch (JSONException e) {
                    LOG.error("Exception while creating JSONObject answering AJAXRequest in 'AddFileToRecentListAction'", e);
                }
            }
            requestResult = new AJAXRequestResult();
        }

        return requestResult;
    }
}
