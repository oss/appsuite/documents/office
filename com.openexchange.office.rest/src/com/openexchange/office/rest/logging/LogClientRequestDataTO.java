/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.logging;

import com.openexchange.office.tools.service.validation.annotation.NotNull;

public class LogClientRequestDataTO {

	private boolean logAnonymous;
	
	private String logData;
	
	private String folderId;
	
	private String fileId;
	
	@NotNull
	private String docUid;
		
	private String fileName;
	
	private String errorCode;
	
	@NotNull
	private String userId;

	public LogClientRequestDataTO(boolean logAnonymous, String logData, String folderId, String fileId, String docUid,
								  String fileName, String errorCode, String userId) {
		this.logAnonymous = logAnonymous;
		this.logData = logData;
		this.folderId = folderId;
		this.fileId = fileId;
		this.docUid = docUid;
		this.fileName = fileName;
		this.errorCode = errorCode;
		this.userId = userId;
	}
	
	public LogClientRequestDataTO() {		
	}

	public boolean isLogAnonymous() {
		return logAnonymous;
	}

	public void setLogAnonymous(boolean logAnonymous) {
		this.logAnonymous = logAnonymous;
	}

	public String getLogData() {
		return logData;
	}

	public void setLogData(String logData) {
		this.logData = logData;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getDocUid() {
		return docUid;
	}

	public void setDocUid(String docUid) {
		this.docUid = docUid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
