/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.json.JSONObject;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Service;

import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.rest.AsyncFileWriter;

@Service
public class LogPerformanceDataWriter extends AsyncFileWriter<JSONObject> implements DisposableBean {
    public static final long MAX_PERFORMANCE_LOG_FILESIZE  = 32 * 1024 * 1024; // 32 MB

    @SuppressWarnings("deprecation")
    private static final Log LOG = com.openexchange.log.Log.loggerFor(LogPerformanceDataWriter.class);
    private static final String LOG_PERFORMANCE_THREAD_NAME = "com.openexchange.office.rest.AsyncPerformanceLogger";
    private static final int  MAX_NUM_OF_RING_LOGFILES      = 2; // must be at least 1 (2 or more for real ring-buffer)
    private static final char NUM_SEPARATOR_CHAR            = '.';

    private int currentFileNumber = 0;
    private String currentFilePath;

    public LogPerformanceDataWriter() {
        super(LOG_PERFORMANCE_THREAD_NAME);

    }

    @Override
	public void destroy() throws Exception {
    	this.shutdown();
	}

	@Override
    public void init(String filePath) throws Exception {
        super.init(filePath);

        if (StringUtils.isNotEmpty(filePath)) {
            currentFileNumber = -1;
            currentFilePath   = getNextFilePath();
        }
    }

    /**
     * Writes the performance data to the file referenced by the
     * provided logging file path.
     *
     * @param loggingFilePath a file path to the logging file MUST NOT be null or empty
     * @param performanceData the performance data to be written to the log file
     */
    @Override
    protected void writeData(JSONObject performanceData) {
        String app = performanceData.optString("application", "");

        // Saving performance data in log file
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(getCurrentRingFile(), true))) {
            if (app.equals("viewer")) {
                OperationHelper.logViewerPerformanceData(performanceData, bufferedWriter);
            } else if (app.equals("text") || app.equals("presentation") || app.equals("spreadsheet")) {
                OperationHelper.logEditorPerformanceData(performanceData, bufferedWriter);
            }
        } catch (Exception e) {
            LOG.debug("LogPerformanceData writing to log file failed due to exception", e);
        }

    }

    @Override
    protected void logErrorMessage(final String sMsg, final Exception e) {

        if (null != e)
            LOG.error(sMsg, e);
        else
            LOG.error(sMsg);
    }

    private File getCurrentRingFile() throws Exception {
        File aFile = new File(currentFilePath);

        if (aFile.exists() && aFile.length() > MAX_PERFORMANCE_LOG_FILESIZE) {
            currentFilePath = getNextFilePath();
            aFile = new File(currentFilePath);
            truncateOrCreateFile(aFile);
        }

        return aFile;
    }

    private String getNextFilePath() throws Exception {
        currentFileNumber = ((currentFileNumber + 1) % MAX_NUM_OF_RING_LOGFILES);

        String filePath = getFilePath();
        if (currentFileNumber == 0)
            return filePath;

        final StringBuilder aTmp = new StringBuilder(getFilePath());
        aTmp.append(NUM_SEPARATOR_CHAR);
        aTmp.append(currentFileNumber);
        return aTmp.toString();
    }

    private void truncateOrCreateFile(final File aFile) throws Exception {
        if (aFile.exists()) {
            try (final FileOutputStream oStream = new FileOutputStream(aFile)) {
                oStream.getChannel().truncate(0);
                oStream.close();
            }
        } else {
            aFile.createNewFile();
        }
    }

}
