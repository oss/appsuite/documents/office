/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.logging;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.rest.DocumentRESTAction;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.tools.session.ServerSession;

@RestController
public class LogPerformanceDataAction extends DocumentRESTAction {
    private static final Logger LOG = LoggerFactory.getLogger(LogPerformanceDataAction.class);

    private static AtomicReference<Boolean> logPerformanceDataEnabled = new AtomicReference<>(null);
    private static AtomicReference<String> logPerformanceDataPath = new AtomicReference<>(null);

    @Autowired
    private LogPerformanceDataWriter aPerformanceDataWriter;
    
    @Autowired
    private ConfigurationHelper configurationHelper;

    private AtomicBoolean aPerformanceDataWriterInit = new AtomicBoolean(false);

    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        if ((null == session) || (session.isAnonymous())) {
            return getAJAXRequestResultWithStatusCode(401);
        }

        final String performanceData = requestData.getParameter("performanceData");
        if (StringUtils.isEmpty(performanceData)) {
            return getAJAXRequestResultWithStatusCode(400);
        }

        Boolean enabled = logPerformanceDataEnabled.get();
        String  loggingFilePath = logPerformanceDataPath.get();

        if (null == enabled) {
            try {
                enabled = configurationHelper.getBooleanOfficeConfigurationValue(session, "//module/logPerformanceData", false);
                if (enabled) {
                    loggingFilePath = configurationHelper.getStringOfficeConfigurationValue(session, "//module/performanceDataLogPath", null);

                    if (StringUtils.isNotEmpty(loggingFilePath)) {
                        logPerformanceDataPath.compareAndSet(null, loggingFilePath);
                        logPerformanceDataEnabled.compareAndSet(null, enabled);
                    }
                }
            } catch (Exception e) {
                // this is an optional feature therefore we just log the problem
                LOG.warn("Exception caught while tyring to determine performance logging properties", e);

                logPerformanceDataEnabled.compareAndSet(null, false);
                logPerformanceDataPath.compareAndSet(null, "");
            }
        }

        if (BooleanUtils.isTrue(enabled) && StringUtils.isNotEmpty(loggingFilePath)) {
            try {
                if (aPerformanceDataWriterInit.compareAndSet(false, true)) {
                    aPerformanceDataWriter.init(loggingFilePath);
                }

                // don't accept data which is too long
                if (performanceData.length() >= (LogPerformanceDataWriter.MAX_PERFORMANCE_LOG_FILESIZE / 10)) {
                    return getAJAXRequestResultWithStatusCode(400);
                }

                final JSONObject aJSONPerformanceData = new JSONObject(performanceData);
                writePerformanceData(aJSONPerformanceData);
                return getAJAXRequestResultWithStatusCode(200);
            }
            catch (final JSONException e) {
                return getAJAXRequestResultWithStatusCode(400);
            }
            catch (final Exception e) {
                LOG.warn("Exception caught while performance logging", e);
                return getAJAXRequestResultWithStatusCode(500);
            }
        }

        return getAJAXRequestResultWithStatusCode(410);
    }

    private void writePerformanceData(final JSONObject jsonPerformanceData) {
        aPerformanceDataWriter.add(jsonPerformanceData);
    }

    /**
     * Provide a AJAXRequestResult with a set status code.
     *
     * @param statusCode the status code to be set at the resulting AJAXRequestResult instance
     * @return the AJAXRequestResult with the provided status code set
     */
    private AJAXRequestResult getAJAXRequestResultWithStatusCode(int statusCode) {
        final AJAXRequestResult requestResult = new AJAXRequestResult();
        requestResult.setHttpStatusCode(statusCode);
        return requestResult;
    }

}
