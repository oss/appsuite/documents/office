/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.logging;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFileStorageFolder;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFolder;
import com.openexchange.log.LogProperties;
import com.openexchange.office.rest.DocumentRESTAction;
import com.openexchange.office.tools.common.EncodeUtility;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.conversion.ConversionException;
import com.openexchange.office.tools.service.conversion.ObjectMapperService;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.http.ParamaterBasedRequestData;
import com.openexchange.office.tools.service.http.SimpleAjaxRequestData;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

@RestController
public class LogClientDataAction extends DocumentRESTAction<LogClientRequestDataTO, LogClientRequestData> {

	private static final Logger LOG = LoggerFactory.getLogger(LogClientDataAction.class);
	public static final String CRASH_DATA_FOLDER_NAME = "crashdata";

	@Autowired
	private FileHelperServiceFactory fileHelperServiceFactory;

	@Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;

	@Autowired
	private ObjectMapperService objectMapperService;

	@Autowired
	private SpecialLogService specialLogService;

	@Override
	protected ParamaterBasedRequestData extractConreteRequestedata(AJAXRequestData requestData) throws ConversionException {
        final boolean logAnonymous = Boolean.valueOf(requestData.getParameter("logAnonymous"));

        final String logData = requestData.getParameter("logData");

        final String folderId = requestData.getParameter("folderId");
        final String fileId = requestData.getParameter("fileId");

        final String docUid = requestData.getParameter("docUid");
        final String fileName = requestData.getParameter("fileName");

        final String errorCode = requestData.getParameter("errorCode");
        final String userId = requestData.getParameter("userId");

		LogClientRequestDataTO logClientRequestDataTO = new LogClientRequestDataTO(logAnonymous, logData, folderId, fileId, docUid, fileName, errorCode, userId);
		try {
			return new SimpleAjaxRequestData(objectMapperService.getObjectMapper().writeValueAsString(logClientRequestDataTO));
		} catch (JsonProcessingException e) {
			throw new ConversionException(LogClientRequestDataTO.class, String.class, e);
		}
	}

	@Override
	protected AJAXRequestResult perform(LogClientRequestData requestModel, AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult res = getAJAXRequestResultWithStatusCode(201);
    	if (StringUtils.isNotBlank(requestModel.getLogData())) {

	        String crashDataFolderId = getCrashdataFolder(session);

	        InputStream inputStream = null;
	        final String mimeType = "text/plain";
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");

	        // we should never use the fileId/folderId directly to create a file name - it must be
	        // encoded, because we don't know the format and there are several characters which are
	        // forbidden in a filename
	        String crashDataFilename = EncodeUtility.encodeForFileName(requestModel.getFileName()) + EncodeUtility.encodeForFileName(requestModel.getFileId()) + formatter.format(LocalDateTime.now());
	        final File file = getNewFile(requestData, crashDataFolderId, crashDataFilename, mimeType, "txt", null, false);
	        try {
	        	inputStream = IOUtils.toInputStream(requestModel.getLogData(), Charset.defaultCharset());

            	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);

	        	ErrorCode errorCodeTmp = fileHelperService.createFileAndWriteStream(file, inputStream);
	        	if (errorCodeTmp.isError()) {
	        		throw AjaxExceptionCodes.UNEXPECTED_ERROR.create(errorCodeTmp.getDescription());
	        	}
	        } finally {
	        	IOHelper.closeQuietly(inputStream);
			}
        } else {
        	try {
                MDCHelper.safeMDCPut(MDCEntries.DOC_UID, requestModel.getDocUid());
                MDCHelper.safeMDCPut(LogProperties.Name.SESSION_CONTEXT_ID.getName(), Integer.toString(session.getContextId()));
                MDCHelper.safeMDCPut(LogProperties.Name.SESSION_USER_ID.getName(), requestModel.getUserId());
                MDCHelper.safeMDCPut(MDCEntries.ERROR, requestModel.getErrorCode());
		    	if (requestModel.isLogAnonymous()) {
		    		LOG.error("DocumentError written for com.openexchange.session.userId {} for file with com.openexchange.rt2.document.uid {} ({})", requestModel.getUserId(), requestModel.getDocUid(), requestModel.getErrorCode());
		       	} else {
		       		LOG.error("DocumentError written for com.openexchange.session.userId {} for file with com.openexchange.rt2.document.uid {} and folder-id {}, file-id={} ({})",
		       					requestModel.getUserId(), requestModel.getDocUid(), requestModel.getFolderId(), requestModel.getFileId(), requestModel.getErrorCode());
		        }
		    	specialLogService.logForDocUid(requestModel.getDocUid());
        	} finally {
                MDC.remove(MDCEntries.DOC_UID);
                MDC.remove(LogProperties.Name.SESSION_CONTEXT_ID.getName());
                MDC.remove(LogProperties.Name.SESSION_USER_ID.getName());
                MDC.remove(MDCEntries.ERROR);
			}
        }
        return res;
	}

	private String getCrashdataFolder(ServerSession session) throws OXException {
		final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
		final String userFolderId = folderHelperService.getUserFolderId(session);
		final FileStorageFolder [] subFolders = folderHelperService.getSubfolders(userFolderId, true);

		for (FileStorageFolder subFolder : subFolders) {
			if (subFolder.getName().equals(CRASH_DATA_FOLDER_NAME)) {
				return subFolder.getId();
			}
		}

		// Not found!
		DefaultFileStorageFolder fileStorageFolder = new DefaultFileStorageFolder();
		fileStorageFolder.setName(CRASH_DATA_FOLDER_NAME);
		fileStorageFolder.setParentId(userFolderId);
		folderHelperService.createFolder(fileStorageFolder);
		return getCrashdataFolder(session);
	}

    /**
     * Provide a AJAXRequestResult with a set status code.
     *
     * @param statusCode the status code to be set at the resulting AJAXRequestResult instance
     * @return the AJAXRequestResult with the provided status code set
     * @throws OXException
     */
    private AJAXRequestResult getAJAXRequestResultWithStatusCode(int statusCode) throws OXException {

        final AJAXRequestResult requestResult = new AJAXRequestResult();
        requestResult.setHttpStatusCode(statusCode);
        return requestResult;
    }
}
