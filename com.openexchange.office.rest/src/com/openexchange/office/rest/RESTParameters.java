/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

public class RESTParameters {

	public final static String PARAMETER_ID                = "id";
	public final static String PARAMETER_FILE_ID           = "file_id";
	public final static String PARAMETER_PART              = "part";
	public final static String PARAMETER_DOCUMENT_TYPE     = "document_type";
	public final static String PARAMETER_TARGET_FOLDER_ID  = "target_folder_id";
	public final static String PARAMETER_TARGET_FILENAME   = "target_filename";
	public final static String PARAMETER_INITIAL_SHEETNAME = "initial_sheetname";
    public final static String PARAMETER_INITIAL_FORMATS   = "initial_formats";
    public final static String PARAMETER_INITIAL_LANGUAGE  = "initial_language";
	public final static String PARAMETER_VERSION           = "version";
	public final static String PARAMETER_AS_TEMPLATE       = "as_template";
	public final static String PARAMETER_ENCRYPTED_FILE    = "encrypted_file";
	public final static String PARAMETER_FOLDER_ID         = "folder_id";
	public final static String PARAMETER_FILENAME          = "filename";

	public final static String PARAMETER_FILENAME_DOCCONV  = "fileName";

	private RESTParameters() {}
}
