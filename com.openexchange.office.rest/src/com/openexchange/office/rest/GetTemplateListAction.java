/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.TemplateFilesScanner;
import com.openexchange.office.document.api.TemplateFilesScannerFactory;
import com.openexchange.office.document.api.TemplateFilesScannerFactory.TemplateFilterType;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GetTemplateListAction}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
@RestController
public class GetTemplateListAction extends DocumentRESTAction implements InitializingBean {
	private static final Logger LOG = LoggerFactory.getLogger(GetTemplateListAction.class);

    private final static String[] MANDATORY_PARAMS = { ParameterDefinitions.PARAM_TYPE };
    
    private TemplateFilesScanner templateFilesScanner;
    
    @Autowired
    private TemplateFilesScannerFactory templateFilesScannerFactory;

    @Override
	public void afterPropertiesSet() throws Exception {
    	templateFilesScanner = templateFilesScannerFactory.create(TemplateFilterType.ALL_DOCUMENTS);
	}

	/* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        if (!paramValidatorService.areAllParamsNonEmpty(requestData, MANDATORY_PARAMS))
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        final JSONObject jsonObj = new JSONObject();
        final String type = requestData.getParameter(ParameterDefinitions.PARAM_TYPE).toLowerCase();
        final JSONArray templates = new JSONArray();
        AJAXRequestResult requestResult = null;

        if (null != session) {
        	ErrorCode errorCode = templateFilesScanner.searchForTemplates(session, type, templates);
        	try {
                jsonObj.put("error", errorCode.getAsJSON());
                jsonObj.put("templates", templates);
        	} catch (JSONException e) {
        		LOG.error("Exception while creating JSONObject answering AJAXRequest gettemplatelist", e);
        	}
        }

        requestResult = new AJAXRequestResult(jsonObj);

        return requestResult;
    }
}
