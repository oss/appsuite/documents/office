/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.getdocument;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.container.ByteArrayFileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.DispatcherNotes;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.office.documents.access.DocRequester;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.rest.DocumentRESTAction;
import com.openexchange.office.rest.ParameterDefinitions;
import com.openexchange.office.rest.tools.ConversionHelper;
import com.openexchange.office.rest.tools.ConversionHelper.ConversionData;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.tools.session.ServerSession;

@DispatcherNotes(defaultFormat = "file", allowPublicSession = true)
@RestController
public class GetDocumentAction extends DocumentRESTAction {

	private static final long MAX_TIMEOUT_FOR_REQUEST = 30;
	
    // ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(GetDocumentAction.class);

    // ---------------------------------------------------------------
    private static final String   PDF_MIMETYPE           = "application/pdf";
    private static final String   PDF_EXTENSION          = "pdf";
    private static final String[] aMandatoryParams       = { ParameterDefinitions.PARAM_DOCUID,
                                                             ParameterDefinitions.PARAM_FILE_ID,
                                                             ParameterDefinitions.PARAM_FOLDER_ID,
                                                             ParameterDefinitions.PARAM_FILENAME,
                                                             ParameterDefinitions.PARAM_CLIENTUID };

    @Autowired
    private DocRequester docRequester;

    @Autowired
    private ConversionHelper conversionHelper;
    
    // ---------------------------------------------------------------
	@Override
	public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult ajaxResult = null;

        if (!paramValidatorService.areAllParamsNonEmpty(requestData, aMandatoryParams))
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        final String folderId = requestData.getParameter(ParameterDefinitions.PARAM_FOLDER_ID);
        final String fileId   = requestData.getParameter(ParameterDefinitions.PARAM_FILE_ID);

        if (!paramValidatorService.canReadFile(session, folderId, fileId))
            return ParamValidatorService.getResultFor(HttpStatusCode.FORBIDDEN.getStatusCode());

        final String clientUID    = requestData.getParameter(ParameterDefinitions.PARAM_CLIENTUID);
        final String docUID       = requestData.getParameter(ParameterDefinitions.PARAM_DOCUID);
        final String fileName     = requestData.getParameter(ParameterDefinitions.PARAM_FILENAME);
        final String authCode     = requestData.getParameter(ParameterDefinitions.PARAM_AUTH_CODE);
        final boolean encrypted   = StringUtils.isNoneEmpty(authCode);

        ErrorCode  errorCode      = ErrorCode.LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR;

        CompletableFuture<InputStream> completableFuture;
        try {
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUID);
            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUID);
            MDCHelper.safeMDCPut(MDCEntries.FILENAME, fileName);

        	completableFuture = docRequester.sendSyncRequest(new RT2DocUidType(docUID), new RT2CliendUidType(clientUID));
        	InputStream inputStream = completableFuture.get(MAX_TIMEOUT_FOR_REQUEST, TimeUnit.SECONDS);
        	
            ajaxResult = convertManagedFile(session, inputStream, fileName, encrypted);

            // in case of a json result make sure that no conversion error occurs
            // and set format of the request
            if ("json".equals(ajaxResult.getFormat()))
                requestData.setFormat("json");
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
        } finally {
            MDC.remove(MDCEntries.DOC_UID);
            MDC.remove(MDCEntries.CLIENT_UID);
            MDC.remove(MDCEntries.FILENAME);
        }

        if (null == ajaxResult) {
            ajaxResult = createErrorResult(errorCode);
            requestData.setFormat("json");
        }

        return ajaxResult;
	}

    // ---------------------------------------------------------------
	private AJAXRequestResult convertManagedFile(final ServerSession session, final InputStream origDocumentStm, String fileName, boolean encrypted) throws Exception {
        AJAXRequestResult ajaxResult = null;

        final Map<String, String> conversionFormat = new HashMap<>(2);
        conversionFormat.put(Properties.PROP_MIME_TYPE, PDF_MIMETYPE);
        conversionFormat.put(Properties.PROP_INPUT_TYPE, PDF_EXTENSION);
        conversionFormat.put(Properties.PROP_FILTER_SHORT_NAME, PDF_EXTENSION);

        final ConversionData aConversionData = conversionHelper.convertDocument(session, origDocumentStm, fileName, conversionFormat, encrypted);
        final InputStream outDocumentStm = aConversionData.getInputStream();

        ErrorCode errorCode = aConversionData.getErrorCode();

        if (null != outDocumentStm) {
            try {
                final byte[] dataBuffer = IOUtils.toByteArray(outDocumentStm);

                if (null != dataBuffer) {
                    try (final ByteArrayFileHolder fileHolder = new ByteArrayFileHolder(dataBuffer)) {
                        final String resourceName = FileHelper.getFileNameWithReplacedExtension(fileName, PDF_EXTENSION, encrypted);

                        fileHolder.setName(resourceName);
                        fileHolder.setContentType(PDF_MIMETYPE);
                        ajaxResult = new AJAXRequestResult(fileHolder, "file");
                    } catch (Exception e) {
                    	log.error(e.getMessage(), e);
                        errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                    }
                }
            } finally {
                Streams.close(outDocumentStm);
            }
        }

        if (null == ajaxResult) {
            ajaxResult = createErrorResult(errorCode);
        }

        return ajaxResult;
	}

    // ---------------------------------------------------------------
	private AJAXRequestResult createErrorResult(ErrorCode errorCode) {
        final JSONObject jsonResult = new JSONObject();

        try {
            jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());
        } catch (final JSONException je) {
            /* ignore it */
        }

        return new AJAXRequestResult(jsonResult, "json");
	}
}
