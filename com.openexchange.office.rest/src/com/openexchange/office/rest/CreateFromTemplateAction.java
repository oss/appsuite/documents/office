/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.groupware.infostore.InfostoreExceptionCodes;
import com.openexchange.office.document.api.DocFileService;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.document.tools.LocalFileMappingManager;
import com.openexchange.office.fields.DynamicFieldsService;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.SessionIdCheck;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.user.UserHelper;
import com.openexchange.office.tools.doc.DocumentFormatHelper;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.office.tools.doc.ExtensionHelper;
import com.openexchange.office.tools.doc.FileDescriptor;
import com.openexchange.office.tools.doc.StreamInfo;
import com.openexchange.office.tools.monitoring.CreateEvent;
import com.openexchange.office.tools.monitoring.DocumentEvent;
import com.openexchange.office.tools.monitoring.ErrorType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.tx.TransactionAwares;
import com.openexchange.user.User;

@RestController
public class CreateFromTemplateAction extends DocumentRESTAction {

    private static final Logger LOG = LoggerFactory.getLogger(CreateFromTemplateAction.class);
    private static final String UNNAMED = "unnamed";

    @Autowired
    private LocalFileMappingManager m_localFileMapper;

    @Autowired
    private FolderHelperServiceFactory folderHelperServiceFactory;

    @Autowired(required=false)
    private IDocumentConverter documentConverter;

    @Autowired
    private DocFileServiceFactory docFileServiceFactory;

    @Autowired
    private ImExporterAccessFactory imExporterAccessFactory;

    @Autowired
    private Statistics statistics;

    @Autowired
    private DynamicFieldsService dynamicFieldsService;

    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        AJAXRequestResult requestResult = null;
        DocumentEvent documentEvent = null;

        if ((null != request) && (null != session)) {
            // #60801: we need to check for an illegal CSRF attempt by checking the
            // correct sessionId; set errorcode to 400 in case of an invalid request
            if (SessionIdCheck.isNotValid(request.getParameter("session"), session)) {
                LOG.warn("CreateFromTemplateAction detected invalid session id - request rejected!");
                return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
            }

            final IDBasedFileAccess fileAccess = getFileAccess(session, request);

            DocumentType eventDocumentType = DocumentType.NONE;
            InputStream inputStm = null;
            boolean rollback = false;

            try {
                final String fileId = request.getParameter("file_id");
            	final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);


                String targetFolderId = request.getParameter("target_folder_id");
                if (StringUtils.isNotBlank(targetFolderId)) {
                    // Fix for 64188: We have to check for write & create access which are
                    // two different access rights
                    boolean canWriteToFolder = folderHelperService.folderHasWriteAccess(targetFolderId);
                    boolean canCreateFilesInFolder = folderHelperService.folderHasCreateAccess(targetFolderId);
                    if (!canWriteToFolder || !canCreateFilesInFolder) {
                        targetFolderId = folderHelperService.getUserDocumentsFolderId(session);
                    }
                }

                if (StringUtils.isBlank(targetFolderId)) {
                    targetFolderId = folderHelperService.getUserDocumentsFolderId(session);
                }

                if ((null != fileId) && (null != fileAccess) && (null != targetFolderId)) {
                    // Create document based on template file. It could be that we have to
                    // convert the template document to the related ODF document format.
                    boolean bLocalFile = fileId.startsWith("template:");
                    final String version = FileStorageFileAccess.CURRENT_VERSION;
                    String fullFilePath = null;
                    String extensionType = null;
                    String mimeType = null;
                    File templateFile = null;
                    String templateFileName = null;

                    if (bLocalFile) {
                        // a local file must be treated differently
                        fullFilePath = m_localFileMapper.getPathFromId(fileId);
                        if (null == fullFilePath) {
                            throw new OXException();
                        }

                        String fileName = FileHelper.getFileName(fullFilePath);
                        extensionType = FileHelper.getExtension(fileName, true);
                        mimeType = ExtensionHelper.getMimeTypeFromTemplateExtension(extensionType);
                    } else {
                        templateFile = fileAccess.getFileMetadata(fileId, version);
                        templateFileName = templateFile.getFileName();
                        extensionType = FileHelper.getExtension(templateFileName, true);
                        mimeType = templateFile.getFileMIMEType();
                    }

                    // we can continue if we have a path or not local file
                    if ((bLocalFile && (null != fullFilePath)) || !bLocalFile) {
                        Map<String, String> conversionFormat = DocumentFormatHelper.getConversionFormatInfo(mimeType, extensionType);

                        if (conversionFormat != null) {
                            // conversion already sets the correct target document format
                            ErrorCode errorCode = ErrorCode.NO_ERROR;

                            if (null != documentConverter) {
                                StreamInfo streamInfo = null;
                                InputStream documentInputStm = null;

                                if (bLocalFile) {
                                    // local file can directly read from the local file system
                                    documentInputStm = DocFileHelper.getDocumentStreamFromLocalFile(fullFilePath);
                                } else {
                                    // OX Drive file must be loaded using Files API
                                    AJAXRequestData loadRequest = new AJAXRequestData();
                                    loadRequest.putParameter("id", fileId);
                                    loadRequest.putParameter("folder_id", targetFolderId);
                                    loadRequest.putParameter("version", version);
                                    loadRequest.putParameter("fileName", templateFileName);

                                    streamInfo = docFileServiceFactory.createInstance().getDocumentStream(session, loadRequest, DocFileService.NO_ENCRYPTION);
                                    documentInputStm = streamInfo.getDocumentStream();
                                    errorCode = streamInfo.getErrorCode();
                                }

                                if (null != documentInputStm) {
                                    HashMap<String, Object> jobProperties = new HashMap<>(4);
                                    HashMap<String, Object> resultProperties = new HashMap<>(8);
                                    final String filterShortName = conversionFormat.get(Properties.PROP_FILTER_SHORT_NAME);

                                    jobProperties.put(Properties.PROP_INPUT_STREAM, documentInputStm);
                                    jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, filterShortName);

                                    /* TODO (KA): set InputUrl property, if known later
                                    if (null != inputURL) {
                                        jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
                                    }
                                    */

                                    if (null != templateFileName) {
                                        jobProperties.put(Properties.PROP_INFO_FILENAME, templateFileName);
                                    }

                                    // this is a user request in every case
                                    jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);

                                    if ((null != streamInfo) && streamInfo.isDecrypted()) {
                                        // set 'NO_CACHE=true' conversion property for decrypted documents since we don't
                                        // want to cache decrypted document conversion results for security reasons
                                        jobProperties.put(Properties.PROP_NO_CACHE, Boolean.TRUE);
                                    }

                                    inputStm = documentConverter.convert(filterShortName, jobProperties, resultProperties, session);
                                    IOHelper.closeQuietly(documentInputStm);

                                    if (null != streamInfo) {
                                        streamInfo.close();
                                    }

                                    // set new mime type and extensionType
                                    mimeType = conversionFormat.get(Properties.PROP_MIME_TYPE);
                                    extensionType = conversionFormat.get(Properties.PROP_INPUT_TYPE);
                                } else {
                                    // no document stream -> provide error information
                                    documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                                    statistics.handleDocumentEvent(documentEvent);

                                    if (null != streamInfo) {
                                        streamInfo.close();
                                    }

                                    return new AJAXRequestResult(createErrorObject(errorCode));
                                }
                            }

                            eventDocumentType = DocumentType.valueOf(conversionFormat.get("DocumentType"));
                        } else {
                            // we have to set the correct target document format (created from the template)
                            final Map<String, String> targetFormat = DocumentFormatHelper.getTemplateTargetFormatInfo(mimeType, extensionType);
                            if (bLocalFile) {
                                inputStm = DocFileHelper.getDocumentStreamFromLocalFile(fullFilePath);
                            } else {
                                inputStm = fileAccess.getDocument(fileId, version);
                            }

                            // set new mime type and extensionType for the target document file
                            if (null != targetFormat) {
                                mimeType = targetFormat.get(Properties.PROP_MIME_TYPE);
                                extensionType = targetFormat.get(Properties.PROP_INPUT_TYPE);
                            }
                        }

                        final File file = getNewFile(request, targetFolderId, null, mimeType, extensionType, templateFile, false);

                        if (null != inputStm) {
                            final IImporter importer = imExporterAccessFactory.getImporterAccess(DocFileHelper.getDocumentFormat("." + extensionType));
                            if(importer!=null) {
                                try {
                                    final User user = session.getUser();

                                    final String initialLanguage = request.getParameter(RESTParameters.PARAMETER_INITIAL_LANGUAGE);
                                    final String aUserLangCode = ((initialLanguage != null) && (initialLanguage.length() > 0)) ? initialLanguage : UserHelper.mapUserLanguageToLangCode(user.getPreferredLanguage());
                                    final DocumentProperties documentProperties = new DocumentProperties();
                                    documentProperties.put(DocumentProperties.PROP_SAVE_TEMPLATE_DOCUMENT, false);
                                    documentProperties.put(DocumentProperties.PROP_USER_LANGUAGE, aUserLangCode);
                                    final InputStream documentStm = importer.getDefaultDocument(inputStm, documentProperties);
                                    if (documentStm != inputStm) {
                                        // Close resource input stream if default document stream
                                        // was created by the import getDefaultDocument function. Don't
                                        // do that if inputStm refers to resourceInputStream
                                        IOHelper.closeQuietly(inputStm);
                                    }
                                    inputStm = documentStm;
                                }
                                catch(FilterException e) {
                                    IOHelper.closeQuietly(inputStm);
                                    inputStm = null;
                                }
                            }
                        }

                        if (null != inputStm) {
                            final JSONObject result = new JSONObject(2);

                            try {
                                inputStm = dynamicFieldsService.insertOptionalFields(inputStm, extensionType, request, result, session);
                            } catch (Exception e) {
                                ErrorCode errorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
                                LOG.debug("Handling optional fields is broken " + errorCode.getDescription(), e);
                                try {
                                    requestResult = new AJAXRequestResult(createErrorObject(errorCode));
                                    documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                                } catch (final JSONException je) {
                                    LOG.debug("Couldn't create JSON object while creating new document from template", e);
                                }
                            }

                            fileAccess.startTransaction();
                            rollback = true;
                            try {
                                LOG.trace("RT connection: [CreateFromTemplate] saveDocument using meta data: " + ((null != file) ? file.toString() : "null"));
                                fileAccess.saveDocument(file, inputStm, FileStorageFileAccess.DISTANT_FUTURE, new ArrayList<Field>());
                            } catch (OXException e) {
                                //if 'only' validation of the name fails, we try it again with 'unnamend'
                                if ((e.getCode() == InfostoreExceptionCodes.VALIDATION_FAILED_CHARACTERS.getNumber()) || (e.getCode() == InfostoreExceptionCodes.VALIDATION_FAILED_SLASH.getNumber())){
                                    setFileName(file, UNNAMED, mimeType, extensionType, request);
                                    LOG.trace("RT connection: [CreateFromTemplate] saveDocument using meta data: " + ((null != file) ? file.toString() : "null"));
                                    fileAccess.saveDocument(file, inputStm, FileStorageFileAccess.DISTANT_FUTURE, new ArrayList<Field>());
                                } else {
                                    throw e;
                                }
                            }
                            fileAccess.commit();
                            rollback = false;

                            // set the file descriptor as a separate object
                            result.put(MessagePropertyKey.KEY_FILEDESCRIPTOR, FileDescriptor.createJSONObject(file, null));
                            // we need to provide if the file has been converted or not
                            result.put("converted", (conversionFormat != null));

                            requestResult = new AJAXRequestResult(result);
                            documentEvent = new CreateEvent(eventDocumentType);
                        } else {
                            // error case: we don't have a input stream
                            ErrorCode errorCode = null;
                            if (null != conversionFormat) {
                                errorCode = ErrorCode.CREATEDOCUMENT_CONVERSION_FAILED_ERROR;
                            } else {
                                errorCode = ErrorCode.CREATEDOCUMENT_CANNOT_READ_TEMPLATEFILE_ERROR;
                            }
                            JSONObject result = errorCode.getAsJSON();
                            requestResult = new AJAXRequestResult(result);
                            documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                            LOG.warn(errorCode.getDescription());
                        }
                    } else {
                        // no path found for the provided id
                        ErrorCode errorCode = ErrorCode.LOCALFILE_INVALID_ID_ERROR;
                        LOG.error(errorCode.getDescription());
                        try {
                            requestResult = new AJAXRequestResult(createErrorObject(errorCode));
                            documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                        } catch (final JSONException e) {
                            LOG.warn("Couldn't create JSON object while creating new document from template");
                        }
                    }
                } else {
                    // required arguments are null or not provided
                    ErrorCode errorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
                    LOG.error(errorCode.getDescription());
                    try {
                        requestResult = new AJAXRequestResult(createErrorObject(errorCode));
                        documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                    } catch (final JSONException e) {
                        LOG.warn("Couldn't create JSON object while creating new document from template");
                    }
                }
            } catch (final Exception e) {
                ErrorCode errorCode = ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR;
                if (e instanceof OXException) {
                    errorCode = ExceptionToErrorCode.map((OXException)e, errorCode, false);
                }

                LOG.error(errorCode.getDescription(), e);
                try {
                    requestResult = new AJAXRequestResult(createErrorObject(errorCode));
                    documentEvent = new DocumentEvent(eventDocumentType, ErrorType.CREATE);
                } catch (final JSONException je) {
                    LOG.warn("Couldn't create JSON object while creating new document from template");
                }
            } finally {
                // Roll-back (if needed) and finish
                if (rollback) {
                    TransactionAwares.rollbackSafe(fileAccess);
                }

                TransactionAwares.finishSafe(fileAccess);
                IOHelper.closeQuietly(inputStm);
            }
        }

        // update statistics with a possibly created event
        statistics.handleDocumentEvent(documentEvent);

        return requestResult;
    }

    /**
     * Create the necessary JSON error object in case of an error.
     *
     * @param errorCode
     *  A valid ErrorCode.
     *
     * @return
     *  A JSON object containing the error information to be sent to the
     *  client.
     *
     * @throws JSONException
     */
    private JSONObject createErrorObject(ErrorCode errorCode) throws JSONException {
        final JSONObject jsonObj = new JSONObject(4);
        jsonObj.put(ErrorCode.JSON_ERRORCLASS, errorCode.getErrorClass());
        jsonObj.put(ErrorCode.JSON_ERRORCODE, errorCode.getCode());
        jsonObj.put(ErrorCode.JSON_ERROR, errorCode.getCodeAsStringConstant());
        jsonObj.put(ErrorCode.JSON_ERRORDESCRIPTION, errorCode.getDescription());
        return jsonObj;
    }
}
