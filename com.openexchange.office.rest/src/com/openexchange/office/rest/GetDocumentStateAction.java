package com.openexchange.office.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.message.MessagePropertyKey;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.DocumentMetaData;
import com.openexchange.office.tools.doc.SyncInfo;
import com.openexchange.tools.session.ServerSession;

@SuppressWarnings("rawtypes")
@RestController
public class GetDocumentStateAction extends DocumentRESTAction {

    private static final Logger LOG = LoggerFactory.getLogger(GetDocumentStateAction.class);
    private static final String[] REQUIRED_PARAMS = { ParameterDefinitions.PARAM_FILE_ID, ParameterDefinitions.PARAM_FOLDER_ID };

    @Autowired
    protected OXDocumentFactory oxDocumentFactory;

    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult requestResult = null;

        JSONObject jsonResult = new JSONObject();

        if (!paramValidatorService.areAllParamsNonEmpty(requestData, REQUIRED_PARAMS)) {
            final ErrorCode errorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
            errorCode.setValue(paramValidatorService.paramsMissing(requestData, REQUIRED_PARAMS).toString());
            setErrorCode(jsonResult, errorCode);
            return new AJAXRequestResult(jsonResult);
        }

        final String fileId = requestData.getParameter(ParameterDefinitions.PARAM_FILE_ID);
        final String folderId = requestData.getParameter(ParameterDefinitions.PARAM_FOLDER_ID);
        final IResourceManager resManager = null;
        final OXDocument oxDocData = oxDocumentFactory.createDocumentAccess(session, folderId, fileId, false, resManager, false, null);
        if (oxDocData.getLastError().isNoError()) {
            final DocumentMetaData docMetaData = oxDocData.getDocumentMetaData();
            final SyncInfo aSyncInfo = new SyncInfo(oxDocData.getFolderId(), oxDocData.getFileId());
            final String sDocVersion = oxDocData.getVersion();
            aSyncInfo.updateSyncInfo(sDocVersion, oxDocData.getUniqueDocumentId());
            if (docMetaData != null) {
                aSyncInfo.setFileName(docMetaData.getFileName());
            }

            try {
                jsonResult.put(SyncInfo.SYNC_INFO, aSyncInfo.toJSON());
            } catch (JSONException e) {
                LOG.warn("SyncInfo couldn't be put into JSONObject", e);
            }
        } else {
            setErrorCode(jsonResult, oxDocData.getLastError());
        }

        requestResult = new AJAXRequestResult(jsonResult);
        return requestResult;
    }

    private void setErrorCode(JSONObject jsonResult, ErrorCode errorCode) {
        try {
            jsonResult.put(MessagePropertyKey.KEY_ERROR_DATA, errorCode.getAsJSON());
        } catch (final JSONException e) { 
            LOG.warn("ErrorCode couldn't be put into JSONObject", e);
        }
    }
}
