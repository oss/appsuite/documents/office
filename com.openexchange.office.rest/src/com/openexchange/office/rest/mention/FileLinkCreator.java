/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import java.text.MessageFormat;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.office.tools.doc.DocumentType;

public class FileLinkCreator {

	public static final String SHARED_FOLDER_ID = "10";

	public static final MessageFormat MSG_PATTERN = new MessageFormat("{0}office?app={1}#!!&app=io.ox/office/{1}&folder={2}&id={3}&comment={4}");

	public static String getLinkToFile(AJAXRequestData requestData, String folderId, String fileId, DocumentType docType, String commentId) {
		Object [] args = {requestData.constructURL("", false), docType.name().toLowerCase(), folderId, fileId, commentId};
		return MSG_PATTERN.format(args);
	}

	public static String getLinkToSharedFile(AJAXRequestData requestData, String folderId, String fileId, DocumentType docType, String commentId) {
		Object [] args = {requestData.constructURL("", false), docType.name().toLowerCase(), SHARED_FOLDER_ID, fileId.replace(folderId, SHARED_FOLDER_ID), commentId};
		return MSG_PATTERN.format(args);
	}

	private FileLinkCreator() {}
}
