/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import com.openexchange.user.User;

public class CommentNotificationRecipientPositionInComment {

	private final Integer posInComment;
	
	private final Integer lengthInComment;

	private final User owner;
	
	public CommentNotificationRecipientPositionInComment(User owner, Integer posInComment, Integer lengthInComment) {
		this.posInComment = posInComment;
		this.lengthInComment = lengthInComment;
		this.owner = owner;
	}

	public Integer getPosInComment() {
		return posInComment;
	}

	public Integer getLengthInComment() {
		return lengthInComment;
	}

	public User getOwner() {
		return owner;
	}

	@Override
	public String toString() {
		return String.format(
				"CommentNotificationRecipientPositionInComment [posInComment=%s, lengthInComment=%s, ownerId=%s, ownerMail=%s]",
				posInComment, lengthInComment, owner.getId(), owner.getMail());
	}
}
