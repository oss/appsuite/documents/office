/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention.to;

import com.openexchange.office.tools.service.validation.annotation.NotNull;
import com.openexchange.office.tools.service.validation.annotation.OneOfMustNotBeNull;

@OneOfMustNotBeNull(attributesToValidate= {"emailAddress", "userId"})
public class CommentNotificationRecipientTO {
	
	private String emailAddress;
	
	private Integer userId;
	
	@NotNull
	private Integer posInComment;
	
	@NotNull
	private Integer lengthInComment;
	
	public CommentNotificationRecipientTO() {
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPosInComment() {
		return posInComment;
	}

	public void setPosInComment(Integer posInComment) {
		this.posInComment = posInComment;
	}

	public Integer getLengthInComment() {
		return lengthInComment;
	}

	public void setLengthInComment(Integer lengthInComment) {
		this.lengthInComment = lengthInComment;
	}
}
