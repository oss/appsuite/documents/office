/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention.to;

import java.util.ArrayList;
import java.util.List;

import com.openexchange.office.tools.service.validation.annotation.NotNull;
import com.openexchange.office.tools.service.validation.annotation.Size;
import com.openexchange.office.tools.service.validation.annotation.Valid;

public class CommentNotificationRequestDataTO {
	
	@Size(min=1)
	@Valid
	private List<CommentNotificationRecipientTO> recipients = new ArrayList<>();
	
	@NotNull
	private String comment;
	
	@NotNull
	private String commentId;
	
	@NotNull
	private String filename;
	
	@NotNull
	private String fileId;
	
	@NotNull
	private String folderId;	
	
	public List<CommentNotificationRecipientTO> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<CommentNotificationRecipientTO> recipients) {
		this.recipients = recipients;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}
}
