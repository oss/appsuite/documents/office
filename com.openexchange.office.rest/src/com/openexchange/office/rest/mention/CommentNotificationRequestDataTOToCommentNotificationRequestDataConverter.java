/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.office.rest.mention.to.CommentNotificationRecipientTO;
import com.openexchange.office.rest.mention.to.CommentNotificationRequestDataTO;
import com.openexchange.office.tools.common.http.HttpModelConverter;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

@Service
public class CommentNotificationRequestDataTOToCommentNotificationRequestDataConverter extends HttpModelConverter<CommentNotificationRequestData, CommentNotificationRequestDataTO> {
	
	@Autowired
	private FileHelperServiceFactory fileHelperServiceFactory;

	@Autowired
	private UserService userService;
	
	@Autowired
	private ContextService contextService;
	
	public CommentNotificationRequestData convert(Session session, CommentNotificationRequestDataTO to) throws OXException {
		
		FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
		com.openexchange.file.storage.File fileMetadata = fileHelperService.getFileMetaData(to.getFileId());
		
		// User do not override equals and hashcode!!! 
		Map<UserAdapter, List<CommentNotificationRecipientPositionInComment>> userMap = new HashMap<>();
		List<CommentNotificationRecipient> recipients = new ArrayList<>();
				
		Context context = contextService.getContext(session.getContextId());
		
		for (CommentNotificationRecipientTO recipientTO : to.getRecipients()) {
			User user = null;
			if ((recipientTO.getUserId() != null) && (recipientTO.getUserId() > 0)) {
				user = userService.getUser(recipientTO.getUserId(), session.getContextId());
			} else {
				if (recipientTO.getEmailAddress() != null) {
					user = userService.searchUser(recipientTO.getEmailAddress(), context, true, true, false);
				} 			
			}
			if (user != null) {
				UserAdapter userAdapter = new UserAdapter(user);
				if (!userMap.containsKey(userAdapter)) {
					userMap.put(userAdapter, new ArrayList<>());
				} 
				userMap.get(userAdapter).add(new CommentNotificationRecipientPositionInComment(user, recipientTO.getPosInComment(), recipientTO.getLengthInComment()));
			} else {
				throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("userId", recipientTO.getUserId(), "email", recipientTO.getEmailAddress());
			}	 			
		}
		userMap.forEach((k,v) -> {
			recipients.add(new CommentNotificationRecipient(k.getUser(), v));
		});
		CommentNotificationRequestData res = new CommentNotificationRequestData(recipients, to.getComment(), to.getCommentId(), fileMetadata);
		return res;
	}
	
	private class UserAdapter {
		private final User user;
		
		public UserAdapter(User user) {		
			this.user = user;
		}
		
		public User getUser() {
			return user;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + new Integer(user.getId()).hashCode();
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserAdapter other = (UserAdapter) obj;
			if (user == null) {
				if (other.user != null)
					return false;
			} else if (user.getId() != other.getUser().getId())
				return false;
			return true;
		}
	}

	@Override
	public String getWebserviceName() {
		return "SendCommentNotificationAction";
	}
}
