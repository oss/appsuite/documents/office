/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.rest.mention;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.rest.DocumentRESTAction;
import com.openexchange.office.rest.tools.FileWritePermissionHelper;
import com.openexchange.office.rest.tools.RequestDataHelper;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

@RestController
public class GetDocumentUserWriteAccess extends DocumentRESTAction<Object, Object> {

    private static final Logger log = LoggerFactory.getLogger(GetDocumentUserWriteAccess.class);

    private ObjectMapper objMapper = new ObjectMapper();

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    @Autowired
    private UserService userService;

    @Autowired
    private FileWritePermissionHelper fileRightHelper;

    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        String requestBodyData = requestData.getParameter(RequestDataHelper.KEY_REQUESTDATA);

        DocumentUserWriteAccessRequestData requestDataObject;

        try {
            requestDataObject = objMapper.readValue(requestBodyData, DocumentUserWriteAccessRequestData.class);
        } catch (JsonParseException | JsonMappingException e) {
            log.info(e.getMessage());
            return getAJAXRequestResultWithStatusCode(HttpStatusCode.BAD_REQUEST.getStatusCode());
        } catch (IOException e) {
            log.info(e.getMessage());
            return getAJAXRequestResultWithStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode());
        }

        final FileHelperService fileHelper = fileHelperServiceFactory.create(session);

        Set<Integer> userIdsWithoutWriteAccess = new HashSet<>();
        Set<String> userEmailsWithoutWriteAccess = new HashSet<>();

        // Test if the session user can write to file
        if (fileHelper.canReadFile(requestDataObject.getFolderId(), requestDataObject.getFileId(), session.getUserId())) {

            // All users to test if the users can write to document
            ArrayList<Integer> userIds = new ArrayList<>();
            userIds.addAll(requestDataObject.getUserIds());

            // Map of email to userId and add userIds to the userId list to test the write access
            Map<String, Integer> userEmails = new HashMap<>();
            for (String email : requestDataObject.getEmails()) {
                try {
                    User user = userService.searchUser(email, session.getContext(), true, true, false);
                    userEmails.put(email, user.getId());
                    userIds.add(user.getId());
                } catch (Exception e) {
                    // If the email is not known a exception occurred
                    userEmails.put(email, null);
                    // do nothing
                }
            }


            Map<Integer, Entry<String, String>> userFile = fileRightHelper.getUserWithWriteAccessFile(requestDataObject.getFolderId(), requestDataObject.getFileId(), userIds, session);

            // filter user Ids without write access
            for (int userId : requestDataObject.getUserIds()) {
                if (!userFile.containsKey(userId) && requestDataObject.getUserIds().contains(userId)) {
                    userIdsWithoutWriteAccess.add(userId);
                }
            }

            // filter user emails without write access
            for (Entry<String, Integer> userEmail : userEmails.entrySet()) {
                if (userEmail.getValue() == null || !userFile.containsKey(userEmail.getValue())) {
                    userEmailsWithoutWriteAccess.add(userEmail.getKey());
                }
            }
        }

        try {
            // send the users Ids/emails without write access
            return getAJAXRequestResultWithUsersWithoutWriteAccess(userIdsWithoutWriteAccess, userEmailsWithoutWriteAccess);
        } catch (JSONException e) {
            log.info(e.getMessage());
            return getAJAXRequestResultWithStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

    private AJAXRequestResult getAJAXRequestResultWithStatusCode(int statusCode) {
        final AJAXRequestResult requestResult = new AJAXRequestResult();
        requestResult.setHttpStatusCode(statusCode);

        return requestResult;
    }

    private AJAXRequestResult getAJAXRequestResultWithUsersWithoutWriteAccess(Set<Integer> userIdsWithoutWriteAccess, Set<String> userEmailsWithoutWriteAccess) throws JSONException {
        JSONObject resultObject = new JSONObject();

        if (!userIdsWithoutWriteAccess.isEmpty()) {
            resultObject.put("userIds", new JSONArray(userIdsWithoutWriteAccess));
        }

        if (!userEmailsWithoutWriteAccess.isEmpty()) {
            resultObject.put("emails", new JSONArray(userEmailsWithoutWriteAccess));
        }

        return new AJAXRequestResult(resultObject);
    }

}
