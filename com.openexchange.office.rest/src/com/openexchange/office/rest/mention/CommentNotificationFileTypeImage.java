/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import com.google.common.io.BaseEncoding;

class CommentNotificationFileTypeImage {

    private final String contentType;

    private final byte[] data;

    private String cid;

    CommentNotificationFileTypeImage(String contentType, byte[] data) {
        this.contentType = contentType;
        this.data = data;
    }

    /**
     * Gets the content type, e.g. <code>image/png</code>
     *
     * @return The content type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Gets the content id that has been set as the <code>src</code> attributes
     * value.
     *
     * @return The plain content id without any cid-prefix, brackets or applied encoding.
     */
    public String getContentId() {
        return cid;
    }

    /**
     * Gets the image data as byte array
     *
     * @return The data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Gets the image data as base64 encoded string
     *
     * @return The data
     */
    public String getB64Data() {
        return BaseEncoding.base64().encode(data);
    }

    void setContentId(String cid) {
        this.cid = cid;
    }
}
