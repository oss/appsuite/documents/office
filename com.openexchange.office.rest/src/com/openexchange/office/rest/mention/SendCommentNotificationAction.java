/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.mention;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;
import com.google.common.base.Joiner;
import com.google.common.io.ByteStreams;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.notify.hostname.HostData;
import com.openexchange.i18n.Translator;
import com.openexchange.i18n.TranslatorFactory;
import com.openexchange.i18n.misc.Messages;
import com.openexchange.java.Strings;
import com.openexchange.mail.dataobjects.compose.ComposeType;
import com.openexchange.mail.dataobjects.compose.ComposedMailMessage;
import com.openexchange.mail.mime.MimeType2ExtMap;
import com.openexchange.mail.mime.QuotedInternetAddress;
import com.openexchange.mail.transport.MailTransport;
import com.openexchange.mail.transport.TransportProvider;
import com.openexchange.mail.transport.TransportProviderRegistry;
import com.openexchange.notification.FullNameBuilder;
import com.openexchange.notification.mail.MailData;
import com.openexchange.notification.mail.NotificationMailFactory;
import com.openexchange.office.rest.DocumentRESTAction;
import com.openexchange.office.rest.mention.to.CommentNotificationRequestDataTO;
import com.openexchange.office.rest.tools.FileWritePermissionHelper;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentType;
import com.openexchange.serverconfig.ServerConfig;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.session.Session;
import com.openexchange.share.ShareTargetPath;
import com.openexchange.share.core.tools.ShareLinks;
import com.openexchange.share.core.tools.ShareToken;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

@RestController
public class SendCommentNotificationAction extends DocumentRESTAction<CommentNotificationRequestDataTO, CommentNotificationRequestData> {

    private static final Logger log = LoggerFactory.getLogger(SendCommentNotificationAction.class);

    private static final String SEND_COMMENT_NOTIFICATION_TMPL_NAME = "notify.comment.mention.html.tmpl";

    private static final String USER_EMAIL_WHO_MENTIONED = "user_email_who_mentioned";

    private static final String USER_FULLNAME_WHO_MENTIONED = "user_fullname_who_mentioned";

    private static final String MENTIONED_SENTENCE = "subject";

    private static final String RECIPIENT = "recipient";

    private static final String COMMENT = "comment";

    private static final String FILENAME = "filename";

    private static final String CLICK_BUTTON_TXT = "click_button_txt";

    private static final String CLICK_BUTTON_LINK = "click_button_link";

    private static final String CLICK_BUTTON_LABEL = "click_button_label";

    private static final String FILE_TYPE_IMG = "file_type_img";

    private static final String FILE_ICON_DEFAULT = "file-icon-default.png";

    private static final String FILE_ICON_SPREADSHEET = "file-icon-spreadsheet.png";

    private static final String FILE_ICON_PRESENTATION = "file-icon-presentation.png";

    private static final String FILE_ICON_TEXT = "file-icon-text.png";

    private static final MessageFormat MAIL_TO_MSG_FMT = new MessageFormat("<a href=\"mailto:{0}\">{1}</a>");

    @Autowired
    private ServerConfigService serverConfigService;

    @Autowired
    private ContextService contextService;

    @Autowired
    private NotificationMailFactory notificationMailFactory;

    @Autowired
    private TranslatorFactory translatorFactory;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private FileWritePermissionHelper fileRightHelper;

    @Override
    protected AJAXRequestResult perform(CommentNotificationRequestData requestModel, AJAXRequestData requestData, ServerSession session) throws OXException {

        String userEmailWhoMentioned = session.getUser().getMail();

        MailTransport mailTransport = null;
        try {
            mailTransport = createMailTransport(session);
            List<Integer> userIds = requestModel.getRecipients().stream().map(r -> r.getUser().getId()).collect(Collectors.toList());

            Map<Integer, Entry<String, String>> userFile = fileRightHelper.getUserWithWriteAccessFile(requestModel.getFileMetadata().getFolderId(), requestModel.getFileMetadata().getId(), userIds, session);

            for (CommentNotificationRecipient recipient : requestModel.getRecipients()) {
                Translator translator = determineTranslatorForRecipient(recipient);

                String userWhoMentioned = FullNameBuilder.buildFullName(session.getUser(), translator);

                String mentionedSentence = translator.translate(Messages.USER_HAS_MENTIONED_YOU);
                mentionedSentence = mentionedSentence.replace("%1$s", userWhoMentioned);
                mentionedSentence = mentionedSentence.replace("%2$s", MAIL_TO_MSG_FMT.format(new Object[] { HtmlUtils.htmlEscape(userEmailWhoMentioned), HtmlUtils.htmlEscape(userEmailWhoMentioned) }));
                String mentionedSubject = String.format(translator.translate(Messages.USER_HAS_MENTIONED_YOU_SUBJECT), userWhoMentioned, requestModel.getFileMetadata().getFileName());

                Map<String, Object> templateVars = new HashMap<>();
                templateVars.put(USER_FULLNAME_WHO_MENTIONED, userWhoMentioned);
                templateVars.put(USER_EMAIL_WHO_MENTIONED, userEmailWhoMentioned);
                templateVars.put(MENTIONED_SENTENCE, mentionedSentence);

                try {
                    fillTemplateVars(templateVars, requestModel, requestData, session, recipient, translator, userFile);

                    InternetAddress recipientInternetAddress = new QuotedInternetAddress(recipient.getUser().getMail());
                    ComposedMailMessage message = composeMailMessage(session, mentionedSubject, templateVars, recipientInternetAddress);
                    mailTransport.sendMailMessage(message, ComposeType.NEW);
                    log.debug("Sending comment notification email to {} with template variables: {}", recipientInternetAddress, Joiner.on(",").withKeyValueSeparator("=").join(templateVars));
                } catch (AddressException e) {
                    log.info(e.getMessage());
                    return getAJAXRequestResultWithStatusCode(HttpStatusCode.BAD_REQUEST.getStatusCode());
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return getAJAXRequestResultWithStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode());
        } finally {
            if (mailTransport != null) {
                mailTransport.close();
            }
        }
        return getAJAXRequestResultWithStatusCode(HttpStatusCode.OK.getStatusCode());
    }

    private Translator determineTranslatorForRecipient(CommentNotificationRecipient recipient) {
        Locale locale = recipient.getUser().getLocale();
        Translator translator = translatorFactory.translatorFor(locale);
        return translator;
    }

    MailTransport createMailTransport(ServerSession session) throws OXException {
        TransportProvider transportProvider = TransportProviderRegistry.getTransportProvider("smtp");
        MailTransport mailTransport = null;
        if (!session.getUser().isMailEnabled()) {
            mailTransport = transportProvider.createNewNoReplyTransport(session.getContextId());
        } else {
            mailTransport = transportProvider.createNewMailTransport(session);
        }
        return mailTransport;
    }

    private ComposedMailMessage composeMailMessage(ServerSession session, String mentionedSubject, Map<String, Object> templateVars, InternetAddress recipientInternetAddress) throws OXException {
        Context ctx = contextService.getContext(session.getContextId());
        ServerConfig serverConfig = serverConfigService.getServerConfig("all", session.getUserId(), session.getContextId());
        MailData.Builder mailDataBuilder = MailData.newBuilder().setRecipient(recipientInternetAddress).setSubject(mentionedSubject).setHtmlTemplate(SEND_COMMENT_NOTIFICATION_TMPL_NAME).setTemplateVars(templateVars).setMailConfig(serverConfig.getNotificationMailConfig()).setContext(ctx);
        if (session.getUser().isMailEnabled()) {
            mailDataBuilder.setSendingUser(session.getUser());
        }
        MailData mailData = mailDataBuilder.build();
        ComposedMailMessage message = notificationMailFactory.createMail(mailData);
        return message;
    }

    void fillTemplateVars(Map<String, Object> templateVars, CommentNotificationRequestData requestModel, AJAXRequestData requestData, ServerSession session, CommentNotificationRecipient recipient, Translator translator, Map<Integer, Entry<String, String>> userFile) throws OXException {

        templateVars.put(RECIPIENT, FullNameBuilder.buildFullName(recipient.getUser(), translator));

        String escapedComment = markRecipientsInComment(requestModel);

        templateVars.put(COMMENT, escapedComment);
        templateVars.put(FILENAME, requestModel.getFileMetadata().getFileName());
        templateVars.put(CLICK_BUTTON_TXT, translator.translate(Messages.CLICK_BUTTON));
        String fileLink = "";
        if (recipient.isGuest()) {
            log.debug("Generating link to shared file for guest {}", recipient.getEmailAddress());
            fileLink = generateUrlForGuest(recipient.getUser(), requestData.getHostData(), session, requestModel.getFileMetadata());
        } else {
            com.openexchange.file.storage.File fileMetadata = requestModel.getFileMetadata();
            DocumentType documentType = getDocumentType(fileMetadata.getFileName());

            Entry<String, String> file = userFile.get(recipient.getUser().getId());
            if (file != null) {
                fileLink = FileLinkCreator.getLinkToFile(requestData, file.getKey(), file.getValue(), documentType, requestModel.getCommentId());
            }
        }
        templateVars.put(CLICK_BUTTON_LINK, fileLink);
        templateVars.put(CLICK_BUTTON_LABEL, translator.translate(Messages.CLICK_BUTTON_LABEL));
        CommentNotificationFileTypeImage commentNotificationFileTypeImage = loadFiletypeImage(requestModel.getFileMetadata().getFileName());
        templateVars.put(FILE_TYPE_IMG, "data:" + commentNotificationFileTypeImage.getContentType() + ";base64," + commentNotificationFileTypeImage.getB64Data());
    }

    private String generateUrlForGuest(User user, HostData hostData, Session session, com.openexchange.file.storage.File fileMetadata) throws OXException {
        ShareTargetPath shareTargetPath = new ShareTargetPath(FolderObject.INFOSTORE, fileMetadata.getFolderId(), fileMetadata.getId());
        String shareUrl = ShareLinks.generateExternal(hostData, new ShareToken(session.getContextId(), user).getToken(), shareTargetPath);
        log.debug("shareUrl: " + shareUrl);
        return shareUrl;
    }

    String markRecipientsInComment(CommentNotificationRequestData requestDataObject) {
        List<CommentNotificationRecipient> recipientList = new ArrayList<>(requestDataObject.getRecipients());
        List<CommentNotificationRecipientPositionInComment> positions = new ArrayList<>();
        recipientList.forEach(r -> positions.addAll(r.getCommentPositions()));

        Collections.sort(positions, new Comparator<CommentNotificationRecipientPositionInComment>() {

            @Override
            public int compare(CommentNotificationRecipientPositionInComment o1, CommentNotificationRecipientPositionInComment o2) {
                return o1.getPosInComment().compareTo(o2.getPosInComment());
            }
        });

        String comment = requestDataObject.getComment();
        StringBuilder strBuilder = new StringBuilder();
        int lastPos = 0;
        for (CommentNotificationRecipientPositionInComment posInfo : positions) {
            if (posInfo.getPosInComment() > lastPos) {
                strBuilder.append(HtmlUtils.htmlEscape(comment.substring(lastPos, posInfo.getPosInComment())));
            }
            lastPos = posInfo.getPosInComment() + posInfo.getLengthInComment();
            String recipientInCommentStr = HtmlUtils.htmlEscape(comment.substring(posInfo.getPosInComment(), lastPos));
            String emailAddress = posInfo.getOwner().getMail();
            strBuilder.append(MAIL_TO_MSG_FMT.format(new Object[] { emailAddress, recipientInCommentStr }));
        }
        if (lastPos < comment.length()) {
            strBuilder.append(comment.substring(lastPos));
        }
        return strBuilder.toString();
    }

    private DocumentType getDocumentType(String filename) {
        String ext = FileHelper.getExtension(filename, true);
        DocumentFormat docFmt = DocumentFormat.valueOf(ext.toUpperCase());
        return DocumentFormat.toDocumentType(docFmt);
    }

    private CommentNotificationFileTypeImage loadFiletypeImage(@NotNull String filename) {
        String fileIconName;
        switch (getDocumentType(filename)) {
            case TEXT:
                fileIconName = FILE_ICON_TEXT;
                break;
            case PRESENTATION:
                fileIconName = FILE_ICON_PRESENTATION;
                break;
            case SPREADSHEET:
                fileIconName = FILE_ICON_SPREADSHEET;
                break;
            default:
                fileIconName = FILE_ICON_DEFAULT;
        }

        var mimeType = MimeType2ExtMap.getContentType(fileIconName);
        if (Strings.isEmpty(mimeType) || !mimeType.startsWith("image/")) {
            log.warn("File type image {} seems not to be a valid image file. Ensure its file ending matches a common 'image/*' mime type.", fileIconName);
            return null;
        }

        var templatesPath = getTemplatesPath();
        if (Strings.isEmpty(templatesPath)) {
            return null;
        }

        var image = new File(new File(templatesPath), fileIconName);
        try (var inStream = new FileInputStream(image)) {
            byte[] imageBytes = ByteStreams.toByteArray(inStream);
            return new CommentNotificationFileTypeImage(mimeType, imageBytes);
        } catch (IOException e) {
            log.warn("Could not load and convert footer image {} from path {}.", fileIconName, templatesPath, e);
            return null;
        }
    }

    private String getTemplatesPath() {
        return configurationService.getProperty("com.openexchange.templating.path");
    }

    /**
     * Provide a AJAXRequestResult with a set status code.
     *
     * @param statusCode the status code to be set at the resulting AJAXRequestResult instance
     * @return the AJAXRequestResult with the provided status code set
     */
    private AJAXRequestResult getAJAXRequestResultWithStatusCode(int statusCode) {
        final AJAXRequestResult requestResult = new AJAXRequestResult();
        requestResult.setHttpStatusCode(statusCode);
        return requestResult;
    }
}
