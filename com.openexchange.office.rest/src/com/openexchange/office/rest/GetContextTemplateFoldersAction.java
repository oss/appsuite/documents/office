/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.common.session.FakeSession;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

@RestController
public class GetContextTemplateFoldersAction extends DocumentRESTAction {

	@Autowired
	private UserConfigurationFactory userConfigFactory;
	
    /* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        Object jsonObj = new JSONObject();
        AJAXRequestResult requestResult = null;

        if ((null != request) && (null != session)) {
            Session fakeSession = new FakeSession(session.getContext().getMailadmin(), session);
            UserConfigurationHelper userConfHelper = userConfigFactory.create(fakeSession, "io.ox/office", Mode.WRITE_BACK);
            final JSONArray array = userConfHelper.getJSONArray("ContextTemplateFolders");
            if (null != array){
                jsonObj = array;
            }
        }

        requestResult = new AJAXRequestResult(jsonObj);

        return requestResult;
    }
}
