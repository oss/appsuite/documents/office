/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

public class ParameterDefinitions {
    private ParameterDefinitions() { /* nothing to do */ }

    public final static String PARAM_ATTACHMENT = "attachment";
    public final static String PARAM_AUTH_CODE = "cryptoAuth";
    public final static String PARAM_FILE_ID = "id";
    public final static String PARAM_FOLDER_ID = "folder_id";
    public final static String PARAM_REMOVE_FILE_ID = "remove_file_id";
    public final static String PARAM_SUB_ACTION = "subaction";
    public final static String PARAM_SUBACTION_CLOSE = "close";
    public final static String PARAM_SUBACTION_OPEN = "open";
    public final static String PARAM_SUBACTION_PREFETCH = "prefetch";
    public final static String PARAM_TYPE = "type";
    public final static String PARAM_VERSION = "version";
    public final static String PARAM_FILENAME = "filename";
    public final static String PARAM_MIMETYPE = "mimetype";
    public final static String PARAM_CLIENTUID = "client_uid";
    public final static String PARAM_DOCUID = "doc_uid";
    public final static String PARAM_SOURCE = "source";
    public final static String PARAM_MODULE = "module";

}
