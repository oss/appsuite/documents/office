/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.backup.restore.RestoreData;
import com.openexchange.office.rest.tools.ParamValidatorService;
import com.openexchange.office.rest.tools.SessionIdCheck;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link SaveAsWithBackupAction}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
@RestController
public class SaveAsWithBackupAction extends DocumentRESTAction {

	private static final Logger LOG = LoggerFactory.getLogger(SaveAsWithBackupAction.class);

	@Autowired
	private IRestoreDocument restoreDocument;

    /* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        final String origFileId = request.getParameter("file_id");
        final String targetFolderId = request.getParameter("target_folder_id");
        final String targetFileName = request.getParameter("target_filename");

        // check parameter values and deny processing if missing or empty
        String missing = null;
        if (StringUtils.isEmpty(origFileId)) {
            missing = "file_id";
        } else if (StringUtils.isEmpty(targetFolderId)) {
            missing = "target_folder_id";
        } else if (StringUtils.isEmpty(targetFileName)) {
            missing = "target_filename";
        }

        if (null != missing) {
            throw AjaxExceptionCodes.MISSING_PARAMETER.create("Mandatory parameter " + missing + " is missing");
        }

        // #60801: we need to check for an illegal CSRF attempt by checking the
        // correct sessionId; set errorcode to 400 in case of an invalid request
        if (SessionIdCheck.isNotValid(request.getParameter("session"), session)) {
            LOG.warn("SaveAsWithBackupAction detected invalid session id - request rejected!");
            return ParamValidatorService.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());
        }

        final String clientOperations = request.getParameter("operations");
        final String restoreID = request.getParameter("restore_id");

        JSONObject jsonResult = null;
        try {
            // create JSONArray from the string parameter
            JSONArray operations = null;

            // The client operations property can also be set to JSON "null"
            // therefore we have to check this, too.
            // See #44052
            if (StringUtils.isNotEmpty(clientOperations) && !JSONObject.NULL.equals(clientOperations)) {
                operations = new JSONArray(clientOperations);
            }

            // try to initiate the save as with backup document
            RestoreData result = restoreDocument.saveAsWithBackupDocumentAndOperations(session, origFileId, restoreID, targetFolderId, targetFileName, operations);

            // create json result object from the result of saveAsWithBackupDocumentAndOperations
            jsonResult = result.getErrorCode().getAsJSONResultObject();
            jsonResult.put(RestoreData.KEY_RESTORE_FILENAME, (result.getRestoredFileName() == null) ? JSONObject.NULL : result.getRestoredFileName());
            jsonResult.put(RestoreData.KEY_RESTORE_FILEID, (result.getRestoredFileId() == null) ? JSONObject.NULL : result.getRestoredFileId());
            jsonResult.put(RestoreData.KEY_RESTORE_FOLDERID, (result.getRestoredFolderId() == null) ? JSONObject.NULL : result.getRestoredFolderId());
        } catch (JSONException e) {
            jsonResult = ErrorCode.BACKUPDOCUMENT_CLIENTACTIONS_MALFORMED.getAsJSONResultObject();
			LOG.warn ("Save as with backup failed due to JSONException - restoreId: " + ((restoreID == null) ? "unknown" : restoreID), e);
        } catch (Exception e) {
            jsonResult = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE.getAsJSONResultObject();
            LOG.warn("Save as with backup failed due to Exception - restoreId: "+ ((restoreID == null) ? "unknown" : restoreID), e);
        }

        return getAjaxRequestResult(jsonResult);
    }

}
