/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

public abstract class AsyncFileWriter<T> {

    private String filePath = null;
    private Queue<T> dataQueue = new LinkedList<T>();
    private Runnable asyncWriterImpl = null;
    private Thread asyncWriterThread = null;
    private AtomicBoolean shutdown = new AtomicBoolean(false);
    private Object queueNotEmpty = new Object();
    private final String threadName;

    public AsyncFileWriter(String threadName) {
        this.threadName = threadName;
    }

    public void init(String filePathToWrite) throws Exception {
        Validate.notEmpty(filePathToWrite);

        synchronized (dataQueue) {
            if (null == filePath)
                filePath = filePathToWrite;
        }
    }

    protected String getFilePath() {
        return filePath;
    }

    public boolean add(final T data) {
        boolean added = false;

        synchronized (dataQueue) {
            if (!shutdown.get() && StringUtils.isNotEmpty(filePath)) {
                dataQueue.add(data);
                added = true;
            }

            if (added && (null == asyncWriterImpl)) {
                asyncWriterImpl = createRunnable();
                asyncWriterThread = new Thread(asyncWriterImpl, threadName);
                asyncWriterThread.start();
            }
        }

        if (added) {
            synchronized (queueNotEmpty) {
                queueNotEmpty.notifyAll();
            }
        }

        return added;
    }

    public void reset() {
        synchronized (dataQueue) {
            dataQueue.clear();
        }
    }

    public boolean hasData() {
        synchronized (dataQueue) {
            return !dataQueue.isEmpty();
        }
    }

    public void shutdown() {
        Thread aAsyncWriterThread = null;

        if (shutdown.compareAndSet(false, true)) {
            synchronized (dataQueue) {
                dataQueue.clear();
                asyncWriterImpl = null;
                aAsyncWriterThread = asyncWriterThread;
                asyncWriterThread = null;
            }
        }

        if (null != aAsyncWriterThread) {
            synchronized (queueNotEmpty) {
                queueNotEmpty.notifyAll();
            }

            try {
                aAsyncWriterThread.join(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    protected abstract void writeData(final T dataToWrite);

    protected abstract void logErrorMessage(final String sMsg, final Exception e);

    private Runnable createRunnable() {
        return new Runnable() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void run() {
                while (!shutdown.get()) {
                    try {
                        synchronized (queueNotEmpty) {
                            queueNotEmpty.wait(50);
                        }

                        T nextDataToWrite = null;

                        synchronized (dataQueue) {
                            nextDataToWrite = dataQueue.poll();
                        }

                        if (null != nextDataToWrite) {
                            writeData(nextDataToWrite);
                        }

                    } catch (final InterruptedException e) {
                        Thread.currentThread().interrupt();
                    } catch (final Exception e) {
                        logErrorMessage("Exception caught trying to determine and write performance data asynchronously", e);
                    }
                }
            }
        };
    }
}
