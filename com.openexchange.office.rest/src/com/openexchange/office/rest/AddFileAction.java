/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.upload.UploadFile;
import com.openexchange.imagetransformation.ImageInformation;
import com.openexchange.java.BoolReference;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.rest.tools.FileActionHelper;
import com.openexchange.office.rest.tools.RESTException;
import com.openexchange.office.rest.tools.RequestDataHelper;
import com.openexchange.office.tools.common.DigestHelper;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.StreamInfo;
import com.openexchange.tools.encoding.Base64;
import com.openexchange.tools.images.ImageTransformationUtility;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link AddFileAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@SuppressWarnings("rawtypes")
@RestController
public class AddFileAction extends DocumentRESTAction implements InitializingBean {

    @SuppressWarnings("deprecation")
	private static final Log LOG = com.openexchange.log.Log.loggerFor(AddFileAction.class);

    static final protected String RESOURCE_ID_KEY = "added_fileid";
    static final protected String RESOURCE_NAME_KEY = "added_filename";
    static final protected String IMAGE_TRANSFORMATION_KEY = "image_transformation";
    static final protected String EXIF_ORIENTATION_KEY = "exif_orientation";

    // - Members ---------------------------------------------------------------

    @Autowired
    private IResourceManagerFactory resourceManagerFactory = null;

    @Autowired
    private DocFileServiceFactory docFileServiceFactory;

    @Autowired
    private FileActionHelper fileActionHelper;

    private IResourceManager resourceManager;

    @Override
	public void afterPropertiesSet() throws Exception {
	      resourceManager = resourceManagerFactory.createInstance(AddFileAction.class.getCanonicalName());
	}

	/*
     * (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData,
     * com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) throws OXException {
        final RequestDataHelper requestDataHelper = new RequestDataHelper(request);
        final JSONObject resultData = new JSONObject();
        AJAXRequestResult ajaxResult = null;
        String sha256 = null;

        final String p = requestDataHelper.getParameter("add_hash");
        if (StringUtils.isNotEmpty(p)) {
            if (DigestHelper.isSHA256HexString(p)) {
                sha256 = p;
            } else {
                LOG.error("Hash parameter is not a valid SHA-256 hex string: " + p);
                final ErrorCode errorCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
                errorCode.setValue(p);
                final JSONObject requestResult = errorCode.getAsJSONResultObject();
                return getAjaxRequestResult(requestResult);
            }
        }

        final String extension = requestDataHelper.getParameter("add_ext");
        final String data64 = requestDataHelper.getParameter("add_filedata");
        final String fileId = requestDataHelper.getParameter("add_fileid");
        final String folderId = requestDataHelper.getParameter("add_folderid");
        final String imageUrlAsStr = requestDataHelper.getParameter("add_imageurl");

        byte[] data = null;

        LOG.debug("AddFileAction called with ext=" + extension + ", fileId=" + fileId + ", folderId=" + folderId + ", sha256=" + sha256);

        if ((null != fileId) && (null != folderId)) {
            final StreamInfo streamInfo = docFileServiceFactory.createInstance().getDocumentStream(session, folderId, fileId, "");
            final InputStream inputStream = streamInfo.getDocumentStream();
            if (null != inputStream) {
                try {
                    data = IOUtils.toByteArray(inputStream);
                } catch (IOException e) {
                    LOG.error("Could not read file stream", e);
                } finally {
                    IOHelper.closeQuietly(inputStream);
                    try {
                        streamInfo.close();
                    } catch (IOException e) {
                        LOG.warn("Could not correctly close IDBasedFileAccess instance", e);
                    }
                }
            } else {
                try {
                    streamInfo.close();
                } catch (IOException e) {
                    LOG.warn("Could not correctly close IDBasedFileAccess instance", e);
                }
            }
        } else if (StringUtils.isNotEmpty(imageUrlAsStr)) {
            try {
                data = fileActionHelper.getImageDataFromUrl(imageUrlAsStr);
            } catch (RESTException e) {
                final ErrorCode errorCode = e.getErrorCode();
                final JSONObject requestResult = errorCode.getAsJSONResultObject();
                ajaxResult = getAjaxRequestResult(requestResult);
            }
        } else {
            if (null != data64) {

                final String searchPattern = "base64,";
                int pos = data64.indexOf(searchPattern);

                if ((-1 != pos) && ((pos += searchPattern.length()) < (data64.length() - 1))) {
                    data = Base64.decode(data64.substring(pos));
                }
            } else { // in IE9 data64 is not readable from request
                InputStream uploadStm = null;

                try {
                    if (request.hasUploads()) {
                        final UploadFile uploadFile = request.getFiles().get(0);
                        uploadStm = new FileInputStream(uploadFile.getTmpFile());
                    }
                } catch (final Exception e) {
                    LOG.error("Could not create file stream", e);
                }

                if (null != uploadStm) {
                    try {
                        data = IOUtils.toByteArray(uploadStm);
                    } catch (IOException e) {
                        LOG.error("Could not read file stream", e);
                    } finally {
                    	IOHelper.closeQuietly(uploadStm);
                    }
                }
            }
        }

        if ((data != null) && (data.length > 0)) {
            final long maxIMageSizeSupported = fileActionHelper.getMaxImageSizeSupported();
            if (data.length > maxIMageSizeSupported) {
            	FileActionHelper.throwMaxImageSizeExceeding(maxIMageSizeSupported);
            }

            try {
                if (StringUtils.isEmpty(sha256)) {
                    sha256 = Resource.getSHA256(data);
                }

                String uid = resourceManager.addResource(sha256, data);

                String resname = null;
                final DocumentFormat docFormat = DocFileHelper.getDocumentFormat(request.getParameter("filename"));
                switch (docFormat) {
                    case DOCX: resname = "word/media/"; break;
                    case PPTX: resname = "ppt/media/"; break;
                    case ODT:
                    case ODS:
                    case ODP: resname = "Pictures/"; break;
                    case XLSX: resname = "xl/media/"; break;
                    case ODG:
                    case NONE:
                    default:
                        LOG.warn("Unsupported document format used " + docFormat);
                        break;
                }

                if (resname != null) {
                    resname = resname + Resource.RESOURCE_UID_PREFIX + uid + "." + extension;
                    final Resource resource = resourceManager.getResource(uid);
                    String managedResourceId = null;

                    // add the managed file id to the result object
                    if ((null != resource) && (null != (managedResourceId = resource.getManagedId()))) {
                        resultData.put(RESOURCE_ID_KEY, managedResourceId);
                    }

                    // add the filename to the result object
                    resultData.put(RESOURCE_NAME_KEY, resname);
                }

                if (data.length > 2 && ((data[0] & 0x00ff) == 0x00ff) && ((data[1] & 0x00ff) == 0x00d8)) {
                    final ImageInformation information = ImageTransformationUtility.readImageInformation(new BufferedInputStream(new ByteArrayInputStream(data)), new BoolReference(false));
                    if (information.orientation > 1) {
                        final JSONObject imageTransformation = new JSONObject(1);
                        imageTransformation.put(EXIF_ORIENTATION_KEY, information.orientation);
                        resultData.put(IMAGE_TRANSFORMATION_KEY, imageTransformation);
                    }
                }

            } catch (Exception e1) {
                LOG.error("Could not add to resource manager", e1);
            }
        }

        // set resource id at connection object; return the
        // object with fragment name and id in case of success
        if (resultData.has(RESOURCE_ID_KEY)) {
            try {
                ajaxResult = getAjaxRequestResult(resultData);

                LOG.debug("AddFileAction with sha256=" + sha256 + " results in " + RESOURCE_ID_KEY + "=" + resultData.getString(RESOURCE_ID_KEY));
            } catch (JSONException e) {
                LOG.error("Could not send resource to realtime session", e);
            }
        }

        return ajaxResult;
    }
}
