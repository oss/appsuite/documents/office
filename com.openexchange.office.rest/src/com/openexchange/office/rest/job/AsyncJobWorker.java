/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest.job;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncJobWorker<T extends Job> implements Runnable {

    // ---------------------------------------------------------------
    protected static final Logger log = LoggerFactory.getLogger(AsyncJobWorker.class);

    // ---------------------------------------------------------------
    private ExecutorService pool;

    // ---------------------------------------------------------------
    private Queue<T> aJobQueue = new ConcurrentLinkedQueue<>();

    // ---------------------------------------------------------------
    private AtomicBoolean shutdown = new AtomicBoolean(false);

    // ---------------------------------------------------------------
    private AtomicReference<Thread> started = new AtomicReference<>(null);

    // ---------------------------------------------------------------
    public AsyncJobWorker(int threadCount) {
        pool = getThreadPool(threadCount);
    }

    // ---------------------------------------------------------------
    public boolean hasJobs() { return (aJobQueue.size() > 0); }

    // ---------------------------------------------------------------
    public void addJob(final T job) {
        aJobQueue.offer(job);

        if (started.compareAndSet(null, new Thread(this, "com.openexchange.office.rest.AsyncOperationsPrefetcher"))) {
            started.get().start();
        }

        synchronized (aJobQueue) {
            aJobQueue.notifyAll();
        }
    }

    // ---------------------------------------------------------------
    public void shutdown() {
        if (shutdown.compareAndSet(false, true)) {
            pool.shutdown(); // Disable new tasks from being submitted

            try {
                if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
                  pool.shutdownNow();

                  if (!pool.awaitTermination(5, TimeUnit.SECONDS))
                      log.warn("AsyncJobWorker pool did not terminate");
                }
            } catch (InterruptedException e) {
                pool.shutdownNow();
                Thread.currentThread().interrupt();
            }
        }
    }

    // ---------------------------------------------------------------
    @Override
    public void run() {
        while (!shutdown.get()) {
            final T job = aJobQueue.poll();

            if (null != job) {
                pool.submit(new JobRunner<>(job));
            } else {
                try {
                    synchronized (aJobQueue) {
                        aJobQueue.wait(15);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        log.debug("AsyncOperationsPrefetcher stopped");
    }

    // ---------------------------------------------------------------
    private static ExecutorService getThreadPool(int threadCount)
    {
        return Executors.newFixedThreadPool(threadCount);
    }

}
