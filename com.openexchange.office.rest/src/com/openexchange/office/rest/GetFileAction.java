/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.ajax.requesthandler.DispatcherNotes;
import com.openexchange.annotation.NonNull;
import com.openexchange.config.ConfigurationService;
import com.openexchange.configuration.ServerConfig;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.datasource.access.DataSourceAccess;
import com.openexchange.office.datasource.access.DataSourceAccessFactory;
import com.openexchange.office.datasource.access.DataSourceException;
import com.openexchange.office.datasource.access.MetaData;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.imagemgr.ResourceFactory;
import com.openexchange.office.tools.common.IdLocker;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.string.StringHelper;
import com.openexchange.office.tools.common.user.AuthorizationCache;
import com.openexchange.tika.util.TikaUtils;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link GetFileAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@SuppressWarnings("rawtypes")
/*
 * MH/KA compile fix for buildsystem
 *
 * @Action(method = RequestMethod.GET, name = "getfile", description =
 * "Get the whole document as PDF file or get the substream of an infostore file in its naive format.", parameters = {
 *
 * @Parameter(name = "session", description = "A session ID previously obtained from the login module."),
 *
 * @Parameter(name = "id", description = "Object ID of the requested infoitem."),
 *
 * @Parameter(name = "folder_id", description = "Folder ID of the requested infoitem."),
 *
 * @Parameter(name = "uid", description = "The unique id of the client application."),
 *
 * @Parameter(name = "version", optional=true, description =
 * "If present, the infoitem data descresourceManagerribes the given version. Otherwise the current version is returned."),
 *
 * @Parameter(name = "filter_format", optional=true, description =
 * "If this value is set to 'pdf', the whole document is as converted PDF file, in all other cases, the content of the document's substream, described by the parameter 'fragment, is returned as unmodified file."
 * ),
 *
 * @Parameter(name = "fragment", optional=true, description =
 * "If this value is set and the filter_format is not set or not set to 'pdf', this parameter describes the substream name of the document internal file to be returned."
 * ),
 *
 * @Parameter(name = "filename", optional=true, description =
 * "If present, this parameter contains the name of the infostore item to be used as initial part of the filename for the returned file, other the filename is set to 'file'."
 * ), }, responseDescription = "Response with timestamp: The content of the documents substream in its native format, or converted to PNG in
 * the case of EMF, WMF and SVM graphic file formats)
 */
@DispatcherNotes(defaultFormat = "file", allowPublicSession = true)
@RestController
public class GetFileAction extends DocumentRESTAction implements InitializingBean {

    /**
     * {@link Cache}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.5
     */
    private class Cache {

        /**
         * CACHE_ROOT_DIRECTORY_NAME (subdirectory of given tmp directory)
         */
        final private static String CACHE_ROOT_DIRECTORY_NAME = ".OGFC";

        /**
         * CLEAR_ALL_ENTRIES
         */
        final private static long CLEAR_ALL_ENTRIES = 0L;

        /**
         * LOG
         */
        final private static String ENCRYPTION_ALGORITHM = "AES";

        /**
         * {@link Entry}
         *
         * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
         * @since v7.10.5
         */
        private class Entry {

            /**
             * Initializes a new {@link Entry}.
             * 
             * @param rootDir
             * @param documentStm
             */
            public Entry(@NonNull final File rootDir, @NonNull InputStream documentStm) throws Exception {
                final long startTimeMillis = System.currentTimeMillis();

                FileUtils.forceMkdir(m_entryDir = new File(rootDir, UUID.randomUUID().toString()));

                if (m_entryDir.canWrite()) {
                    // create and init en/decryption KeyGenerator with current system time
                    final KeyGenerator encryptionKeyGenerator = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);

                    encryptionKeyGenerator.init(new SecureRandom(Long.toString(startTimeMillis).getBytes()));

                    // create en/decrypt ciphers
                    final SecretKey secretKey = encryptionKeyGenerator.generateKey();

                    (m_encryptCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM)).init(Cipher.ENCRYPT_MODE, secretKey);
                    (m_decryptCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM)).init(Cipher.DECRYPT_MODE, secretKey);

                    // extract resources and write to tmp folder using the encryption cipher
                    implExtractResources(documentStm);

                    // update timestamp after extraction of all resources
                    touch();

                    if (log.isTraceEnabled()) {
                        log.trace("GetFile cache creating new entry {} took {}ms to cache {} resources", m_entryDir.getAbsolutePath(), System.currentTimeMillis() - startTimeMillis, m_entryNames.size());
                    }
                } else {
                    throw new IllegalArgumentException("GetFile cache entry is not writable: " + m_entryDir.getAbsolutePath());
                }
            }

            /**
             *
             */
            public void touch() {
                m_timestampMillis = System.currentTimeMillis();
            }

            /**
             * @return
             */
            public long getTimestampMillis() {
                return m_timestampMillis;
            }

            /**
             *
             */
            public synchronized void clear() {
                FileUtils.deleteQuietly(m_entryDir);
                m_timestampMillis = 0;
            }

            /**
             * @param resourceName
             * @return
             */
            public synchronized byte[] getResourceBuffer(final String resourceName, final Dimension imageExtents, final String[] mimeType) {
                if (null != resourceName) {
                    String entryName = resourceName;

                    // some resources might be specified by an uid fragment only =>
                    // try to find a possible match for this within all entries
                    if (!m_entryNames.contains(entryName)) {
                        final String uidResourceName = FileHelper.getBaseName(resourceName);

                        if ((null != uidResourceName) && uidResourceName.startsWith(Resource.RESOURCE_UID_PREFIX) && (uidResourceName.length() > Resource.RESOURCE_UID_PREFIX.length())) {
                            for (final String curEntryName : m_entryNames) {
                                if (curEntryName.contains(uidResourceName)) {
                                    // use first entry whose uid fragment is contained in entry name
                                    entryName = curEntryName;
                                    break;
                                }
                            }
                        }
                    }

                    try {
                        final File resourceFile = new File(m_entryDir, entryName);

                        if (resourceFile.canRead()) {
                            final String fileExtension = FilenameUtils.getExtension(entryName).trim().toLowerCase();

                            // read from encrypted file with defined decryption cipher
                            try (final InputStream inputStm = new CipherInputStream(FileUtils.openInputStream(resourceFile), m_decryptCipher)) {
                                return convertToValidDestinationFormat(IOUtils.toByteArray(inputStm), resourceName, fileExtension, imageExtents, mimeType);
                            }
                        }
                    } catch (Exception e) {
                        log.error("GetFile cache caught exception in getResourceBuffer for " + resourceName, e);
                    }
                }

                return null;
            }

            // - Implementation ----------------------------------------------------

            /**
             * @param documentStm
             * @return The number of all extracted resources
             */
            void implExtractResources(@NonNull InputStream documentStm) throws Exception {
                try (final ZipInputStream zipInputStm = new ZipInputStream(documentStm)) {
                    final String canonicalEntryDirPath = m_entryDir.getCanonicalPath();
                    boolean suspiciousPathFound = false;

                    for (ZipEntry zipEntry = null; null != (zipEntry = zipInputStm.getNextEntry());) {
                        try {
                            final String curEntryName = zipEntry.getName();

                            if (curEntryName.equals(DocFileHelper.OX_RESCUEDOCUMENT_DOCUMENTSTREAM_NAME)) {
                                // check for resources contained within the original rescue document
                                byte[] entryBuffer = IOUtils.toByteArray(zipInputStm);

                                if (null != entryBuffer) {
                                    try (final InputStream entryInputStream = new ByteArrayInputStream(entryBuffer)) {
                                        implExtractResources(entryInputStream);
                                    }
                                }
                            } else if (!zipEntry.isDirectory()) {
                                try {
                                    final String fileExtension = FilenameUtils.getExtension(curEntryName).trim().toLowerCase();

                                    // extract image to cache entry dir as encrypted file
                                    if (IMAGE_EXTENSION_TO_MIMETYPE_MAP.containsKey(fileExtension)) {
                                        final File outputFile = new File(m_entryDir, curEntryName);
                                        final String canonicalOutputPath = outputFile.getCanonicalPath();

                                        if (canonicalOutputPath.startsWith(canonicalEntryDirPath)) {
                                            // write to encrypted file with defined encryption cipher
                                            try (OutputStream outputStm = new CipherOutputStream(FileUtils.openOutputStream(outputFile), m_encryptCipher)) {

                                                // !!! use IOUtils.copy in order to leave the zip input stream open !!!
                                                IOUtils.copy(zipInputStm, outputStm);
                                            }

                                            // store entry names to be able to search for resources,
                                            // specified by uid fragment only when retrieving files
                                            m_entryNames.add(curEntryName);
                                        } else if (!suspiciousPathFound) {
                                            suspiciousPathFound = true;
                                            log.warn("GetFile cache detected suspicious document with entries having a relative path pointing outside of archive root, ignored");
                                        }
                                    }
                                } catch (IOException e) {
                                    log.error("GetFile cache exception caught while extracting image entry from document stream: " + curEntryName, e);
                                }
                            }
                        } finally {
                            zipInputStm.closeEntry();
                        }
                    }
                }
            }

            // - Members -----------------------------------------------------------

            final private File m_entryDir;

            final private Set<String> m_entryNames = new LinkedHashSet<>();

            final private Cipher m_encryptCipher;

            final private Cipher m_decryptCipher;

            private volatile long m_timestampMillis = 0;
        }

        /**
         * Initializes a new {@link Cache}.
         * Each call to this class is thread safe.
         * If subsequent calls to this class are made with the same document id,
         * the call sequence needs to be made between calls to the lock(documentId)/
         * unlock(documentId) method calls (blocking of subsequent calls with same documentId).
         * E.g. <code> lock(documentId) =>
         * try { hasEntry(documentId) => has and/or add entry followed by getEntry }
         * finally { unlock(documentId) } </code>
         */
        public Cache(final File rootDir, final long maxTimeToLiveMillis) throws IllegalArgumentException {
            super();

            m_maxTimeToLiveMillis = maxTimeToLiveMillis;
            m_cacheDir = new File((null != rootDir) ? rootDir : FileUtils.getTempDirectory(), CACHE_ROOT_DIRECTORY_NAME);

            try {
                // clean all possibly left over dir entries from last session and create new working directory
                FileUtils.deleteQuietly(m_cacheDir);
                FileUtils.forceMkdir(m_cacheDir);
            } catch (IOException e) {
                log.error("GetFile cache Ctor could not create working directory {}: {}", m_cacheDir.getAbsolutePath(), Throwables.getRootCause(e));
            }

            // we don't have a file directory to work on => disable cache
            if (!m_cacheDir.canWrite()) {
                throw new IllegalArgumentException("GetFile cache is not able to create cache working directory: " + m_cacheDir.getAbsolutePath());
            }
        }

        /**
         *
         */
        public void shutdown() {
            if (m_isRunning.compareAndSet(true, false)) {
                final boolean trace = log.isTraceEnabled();
                long startTimeMillis = 0;

                if (trace) {
                    startTimeMillis = System.currentTimeMillis();
                    log.trace("GetFile cache starting shutdown");
                }

                m_timer.cancel();
                m_timer.purge();

                // clear all entries
                implCleanupEntries(CLEAR_ALL_ENTRIES);

                FileUtils.deleteQuietly(m_cacheDir);

                if (trace) {
                    log.trace("GetFile cache finished shutdown in {}ms", System.currentTimeMillis() - startTimeMillis);
                }
            }
        }

        /**
         * @return
         */
        public boolean isRunning() {
            return m_isRunning.get();
        }

        /**
         * @param documentId
         */
        public boolean lock(final String documentId) {
            if (null != documentId) {
                return IdLocker.lock(documentId);
            }

            return false;
        }

        /**
         * @param documentId
         */
        public void unlock(final String documentId) {
            if (null != documentId) {
                IdLocker.unlock(documentId);
            }
        }

        /**
         * @param documentId
         * @return
         */
        public boolean hasEntry(String documentId) {
            if (isRunning() && lock(documentId)) {
                try {
                    return m_entries.containsKey(documentId);
                } finally {
                    unlock(documentId);
                }
            }

            return false;
        }

        /**
         * @param documentId
         * @param documentStm
         */
        public void addEntry(String documentId, InputStream documentStm) {
            boolean restartTimer = false;

            if (isRunning() && (null != documentStm) && lock(documentId)) {
                try {
                    m_entries.put(documentId, new Entry(m_cacheDir, documentStm));
                    restartTimer = true;
                } catch (Exception e) {
                    log.error("GetFile cache not able to create new entry: {}", Throwables.getRootCause(e));
                } finally {
                    IdLocker.unlock(documentId);
                }
            }

            if (restartTimer) {
                implRestartTimer(m_maxTimeToLiveMillis);
            }
        }

        /**
         * @param documentId
         * @param resourceName
         * @param mimeType
         * @param imageExtents
         * @return
         */
        public byte[] getEntryResource(String documentId, String resourceName, Dimension imageExtents, String[] mimeType) {
            if (isRunning() && (null != resourceName) && lock(documentId)) {
                try {
                    Entry entryToUse = null;

                    synchronized (m_entries) {
                        // reorder touched entry to end of list
                        entryToUse = m_entries.remove(documentId);

                        if (null != entryToUse) {
                            entryToUse.touch();
                            m_entries.put(documentId, entryToUse);
                        }
                    }

                    // return content of touched entry as return value
                    if (null != entryToUse) {
                        return entryToUse.getResourceBuffer(resourceName, imageExtents, mimeType);
                    }
                } finally {
                    unlock(documentId);
                }
            }

            return null;
        }

        // - Implementation --------------------------------------------------------

        /**
         * @param timeoutMillis
         */
        private void implRestartTimer(final long timeoutMillis) {
            if (isRunning() && m_timerRunning.compareAndSet(false, true)) {
                m_timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        m_timerRunning.set(false);
                        implCleanupEntries(System.currentTimeMillis());
                    }
                }, Math.max(0L, timeoutMillis));
            }
        }

        /**
         * @param curTimeMillis
         */
        private void implCleanupEntries(final long curTimeMillis) {
            long nextTimestampMillis = 0;

            // clear all entries from map that are currently accessible via tryLock
            synchronized (m_entries) {
                final Iterator<String> iter = m_entries.keySet().iterator();
                final boolean clearAll = (CLEAR_ALL_ENTRIES == curTimeMillis);
                final int startCount = m_entries.size();
                int removedCount = 0;

                while (iter.hasNext()) {
                    final String curDocumentId = iter.next();

                    if (IdLocker.lock(curDocumentId, IdLocker.Mode.TRY_LOCK)) {
                        try {
                            // get entry to be cleared and remove from map
                            final Entry curEntry = m_entries.get(curDocumentId);
                            final boolean timeoutReached = (curTimeMillis - curEntry.getTimestampMillis()) >= m_maxTimeToLiveMillis;

                            if (timeoutReached || clearAll) {
                                iter.remove();
                                curEntry.clear();
                                ++removedCount;
                            }

                            // finish cleanup in case of timeout based cleanup and all
                            // ordered, next entries did not reach their timeout yet
                            if (!timeoutReached && !clearAll) {
                                break;
                            }
                        } finally {
                            unlock(curDocumentId);
                        }
                    }
                }

                final int sizeLeft = m_entries.size();

                if (log.isTraceEnabled() && (removedCount > 0)) {
                    log.trace("GetFile cache automatically removed {}/{} entries ({} entries available after removal)", removedCount, startCount, sizeLeft);
                }

                // restart timer based on next timeout entry in case there are entries left
                if (sizeLeft > 0) {
                    final Entry nextTimeoutEntry = m_entries.get(m_entries.keySet().iterator().next());

                    if (null != nextTimeoutEntry) {
                        nextTimestampMillis = nextTimeoutEntry.getTimestampMillis();
                    }
                }
            }

            // restart timer if there are entries left
            if (0 != nextTimestampMillis) {
                implRestartTimer(nextTimestampMillis + m_maxTimeToLiveMillis - System.currentTimeMillis());
            }
        }

        // - Members ---------------------------------------------------------------

        final private Map<String, Entry> m_entries = Collections.synchronizedMap(new LinkedHashMap<>());

        final private AtomicBoolean m_isRunning = new AtomicBoolean(true);

        final private Timer m_timer = new Timer();

        final private AtomicBoolean m_timerRunning = new AtomicBoolean(false);

        final private File m_cacheDir;

        final private long m_maxTimeToLiveMillis;
    }

    /**
     *
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        resourceManager = resourceManagerFactory.createInstance(GetFileAction.class.getCanonicalName());

        try {
            // create Cache instance to speedup GetFile requests for a document. Config item description:
            //
            //  # Specifies the timeout value for a GetFile Ajax request cache entry.
            //  # Each GetFile request for an open document updates the cache entries timestamp,
            //  # so that after the last GetFile Ajax request for a document all resources cached for
            //  # this document will be removed after the configured timeout.
            //  # If no cache entry exists for a document yet, a new cache entry will be created
            //  # with the following, next GetFile Ajax request.
            //  # Set to 0 to disable the GetFile Ajax request cache.
            //  # Default value: 120000
            //  com.openexchange.office.cache.getFile.timeout=120000
            final long cacheTimeoutMillis = (null != configService) ? configService.getIntProperty("com.openexchange.office.cache.getFile.timeout", 120000) : 120000;

            if (cacheTimeoutMillis > 0) {
                cache = new Cache(ServerConfig.getTmpDir(), cacheTimeoutMillis);
                log.info("AjaxRequest GetFileAction cache is enabled with a timeout of {}ms", cacheTimeoutMillis);
            } else {
                log.info("AjaxRequest GetFileAction cache is disabled");
            }
        } catch (Exception e) {
            log.error("AjaxRequest GetFileAction exception caught while creating cache => disabling cache)", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData,
     * com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData request, ServerSession session) {
        AJAXRequestResult requestResult = null;
        String resourceName = request.getParameter("get_filename");
        String getFormat = request.getParameter("get_format");
        final String id = request.getParameter("id");
        final String folderId = request.getParameter("folder_id");
        final String versionOrAttachment = request.getParameter("version");
        final String source = request.getParameter("source");

        // check fileId/folderId
        if (isEmpty(id) || isEmpty(folderId)) {
            requestResult = new AJAXRequestResult();
            requestResult.setHttpStatusCode(400);

            log.error("AjaxRequest GetFileAction: for source {} and id {} / folderId {} / version {} not valid: ", source, id, folderId, versionOrAttachment);
            return requestResult;
        }

        DataSourceAccess dataSourceAccess = null;

        int userId = Integer.MIN_VALUE;
        // retrieve user id and create data source access
        try {
            userId = session.getUserId();

            dataSourceAccess = dataSourceAccessFactory.createDataSourceAccess(session, source);
        } catch (DataSourceException e) {
            final String errMsg = StringHelper.replacePlaceholdersWithArgs("AjaxRequest GetFileAction DataSourceException caught trying to create data source access for data source {}, folder {}, id {}, versionOrAttachment {}",
                source, folderId, id, versionOrAttachment);
            log.error(errMsg, e);

            final AJAXRequestResult result = new AJAXRequestResult();
            result.setHttpStatusCode(400);
            return result;
        } catch (Exception e) {
            log.error("AjaxRequest GetFileAction exception caught while trying to get user id", e);

            final AJAXRequestResult result = new AJAXRequestResult();
            result.setHttpStatusCode(403);
            return result;
        }

        // check read access rights
        try {
            if ((Integer.MIN_VALUE == userId) || !dataSourceAccess.canRead(folderId, id, versionOrAttachment)) {
                final AJAXRequestResult result = new AJAXRequestResult();
                result.setHttpStatusCode(403);

                log.error("AjaxRequest GetFileAction error while checking file access rights");
                return result;
            }
        } catch (DataSourceException e) {
            final AJAXRequestResult result = new AJAXRequestResult();
            result.setHttpStatusCode(403);

            log.error("AjaxRequest GetFileAction error while checking file access rights", e);
            return result;
        }

        // check and sanitize resourceName
        if (isEmpty(resourceName)) {
            final AJAXRequestResult result = new AJAXRequestResult();
            result.setHttpStatusCode(400);

            log.error("AjaxRequest GetFileAction error while checking resource name");

            return result;
        } else  if (resourceName.startsWith("./")) {
            resourceName = resourceName.substring(2);
        }

        String filename = FileHelper.getBaseName(resourceName);

        log.debug("AjaxRequest GetFileAction request for " + filename);

        if (getFormat == null || !"json".equals(getFormat)) {
            getFormat = "file";
        }

        boolean fileNameStartWithXID = filename.startsWith(Resource.RESOURCE_UID_PREFIX);

        // try to get the graphic from the resource manager(s) first
        if (fileNameStartWithXID) {
            // 1. try: resource is already contained within this nodes resource manager
            final String uid = Resource.getUIDFromResourceName(filename);
            final Resource nodeResource = resourceManager.getResource(uid);
            byte[] resdata = ((nodeResource != null) ? nodeResource.getBuffer() : null);

            log.trace("AjaxRequest GetFileAction " + filename + " not found in resourceManager");

            // 2. try: create a managed resource with the given uid
            if (null == resdata) {
                try {
                    final String managedId = Resource.getManagedIdFromUID(uid);

                    if ((null != managedId) && (managedId.length() > 0)) {
                        final Resource managedResource = resourceFactory.create(managedId);

                        // add a new managed resource to this nodes resource manager for later speedup
                        // if we have data with content, otherwise we fall-back to read the doc stream
                        resdata = (null != managedResource) ? managedResource.getBuffer() : null;
                        if ((null != resdata) && (resdata.length > 0)) {
                            resourceManager.addResource(managedResource);
                        } else {
                            resdata = null;
                        }
                    }
                } catch (Exception e) {
                    // This is just an information: a cleanly loaded document contains the images
                    // and does not provide it via managed file. The 3rd access should provide the
                    // image. Therefore change this log into info.
                    log.debug("AjaxRequest GetFileAction exception caught while trying to create managed resource",  e);
                }
            }

            // if we've got the resource data, set it as request result
            if (null != resdata) {
                requestResult = createFileRequestResult(request, resdata, getFormat, DocFileHelper.getMimeType(resourceName), resourceName);
            }
        }

        // 3. try getting the graphic from the Cache (preferred) or the document's zip archive itself
        if (null == requestResult) {
            final String authCode = AuthorizationCache.getKey(""+session.getContextId(), ""+session.getUserId(), id);
            final Optional<String> authCodeData = (null != authCode) ? Optional.of(authCode) : Optional.empty();
            final String widthParam = request.getParameter("width");
            final String heightParam = request.getParameter("height");
            final Dimension imageExtents = getImageExtents(widthParam, heightParam);
            final String fileRawExtension = FilenameUtils.getExtension(resourceName);
            final String fileExtension = (null != fileRawExtension) ? fileRawExtension.trim().toLowerCase() : null;
            final boolean isShape2PngConversion = "shape2png".equals(fileExtension);
            final String[] mimeType = { "" };
            String documentId = null;

            // get documentId from metadata
            try {
                final MetaData metaData = dataSourceAccess.getMetaData(folderId, id, versionOrAttachment, authCodeData);
                documentId = (null != metaData) ? metaData.toString() : null;
            } catch (Exception e) {
                log.trace("AjaxRequest GetFileAction could not get metadata " + filename, e);
            }

            // 3.1 trying to retrieve the result from the Cache first
            if ((null != cache) && (null != documentId) && cache.lock(documentId)) {
                try {
                    if (!cache.hasEntry(documentId)) {
                        try (final InputStream documentStm = dataSourceAccess.getContentStream(folderId, id, versionOrAttachment, authCodeData)) {
                            cache.addEntry(documentId, documentStm);
                        }
                    }

                    if (!isShape2PngConversion) {
                        final byte[] resourceBuffer = cache.getEntryResource(documentId, resourceName, imageExtents, mimeType);

                        if (null != resourceBuffer) {
                            if (fileNameStartWithXID) {
                                // add resource to global resource manager for a faster second access
                                resourceManager.addResource(resourceBuffer);
                            }

                            requestResult = createFileRequestResult(request, resourceBuffer, getFormat, mimeType[0], filename);

                            log.trace("AjaxRequest GetFileAction {} successfully retrieved from cache", filename);
                        } else {
                            log.warn("AjaxRequest GetFileAction {} not able to be retrieved from cache", filename);
                        }
                    }
                } catch (Exception e) {
                    log.trace("AjaxRequest GetFileAction could not access file via GetFile cache " + filename, e);
                } finally {
                    cache.unlock(documentId);
                }
            }

            // 3.2 we got no result from cache => image must be retrieved from document stream
            if (null == requestResult) {
                log.trace("AjaxRequest GetFileAction image must be retrieved from document stream");

                try (final InputStream documentStm = dataSourceAccess.getContentStream(folderId, id, versionOrAttachment, authCodeData)) {

                    if (null != documentStm) {

                        log.trace("AjaxRequest GetFileAction document stream successfully retrieved - try to get substream for " + filename);

                        byte[] buffer = null;
                        if(isShape2PngConversion) {
                            try (final InputStream resultStm = convertShape2PNG(documentStm, resourceName, imageExtents, mimeType)) {
                                if(resultStm != null) {
                                    buffer = IOUtils.toByteArray(resultStm);
                                }
                            }
                        }
                        else {
                            buffer = convertToValidDestinationFormat(getZipEntryBuffer(documentStm, resourceName, mimeType), resourceName, fileExtension, imageExtents, mimeType);
                        }
                        if (null != buffer) {
                            if (fileNameStartWithXID) {
                                // add resource to global resource manager for a faster second access
                                resourceManager.addResource(buffer);
                            }
                            requestResult = createFileRequestResult(request, buffer, getFormat, mimeType[0], filename);

                            log.trace("AjaxRequest GetFileAction " + filename + " successfully retrieved");
                        } else {
                            log.warn("AjaxRequest GetFileAction " + filename + " not found");
                        }
                    }
                } catch (DataSourceException e) {
                    log.error("AjaxRequest GetFileAction exception caught trying to retrieving image data from document stream", e);
                } catch (IOException e) {
                    log.error("AjaxRequest GetFileAction I/O exception caught while reading data from document stream", e);
                }
            }
        }

        if (null == requestResult) {
            log.warn("AjaxRequest: GetFileAction request not successfully processed for resource: " + ((null == resourceName) ? "unknown resource name" : resourceName) + " in: " + filename);

            request.setFormat("json");
            requestResult = new AJAXRequestResult();
            requestResult.setHttpStatusCode(404);
        }
        return requestResult;
    }

    // A list of format extensions that are always converted to png, For these formats
    // we rely on the extension because they can't be detected via tika.
    final private static ImmutableSet<String> imageExtensionsToBeConverted = ImmutableSet.<String> builder()
        .add("svm")
        .add("svg")
        .add("emf")
        .add("wmf")
        .build();

    // Formats whose content can be used in the browser without conversion, these formats are detected via tika
    final private static ImmutableSet<String> nativeSupportedWebImageFormats = ImmutableSet.<String> builder()
        .add("image/jpeg")
        .add("application/x-webp")
        .add("image/gif")
        .add("image/png")
        .add("image/apng")
        .add("image/bmp")
        .add("image/tiff")
        .build();

    // Following formats, detected by tika will need a conversion to png.
    final private static ImmutableMap<String, String> imageMimeTypesToBeConverted = ImmutableMap.<String, String> builder()
        .put("image/emf",           "emf")
        .put("image/x-emf",         "emf")
        .put("image/wmf",           "wmf")
        .put("image/x-wmf",         "wmf")
        .put("svm",                 "svm")
        .put("image/svg+xml",       "svg")
        .build();

    private byte[] convertToValidDestinationFormat(byte[] buffer,@NonNull String resourceName, String fileExtension, Dimension extents, String[] mimeType) {
        if(buffer == null || buffer.length == 0) {
            mimeType[0] = "";
            return null;
        }
        try {
            if(fileExtension != null && imageExtensionsToBeConverted.contains(fileExtension) || (StringUtils.isNotEmpty(resourceName) && (resourceName.indexOf("ObjectReplacements/") > -1))) {
                try (InputStream inputStm = new ByteArrayInputStream(buffer)) {
                    return convert2png(inputStm, resourceName, fileExtension, extents, mimeType);
                }
            }

            final String detectedFormat = TikaUtils.detectMimeType(new ByteArrayInputStream(buffer));
            if(nativeSupportedWebImageFormats.contains(detectedFormat)) {
                return buffer;
            }
            final String sourceExtension = imageMimeTypesToBeConverted.get(detectedFormat);
            if(sourceExtension != null) {
                try (InputStream inputStm = new ByteArrayInputStream(buffer)) {
                    return convert2png(inputStm, resourceName, sourceExtension, extents, mimeType);
                }
            }
        }
        catch(IOException e) {
            log.debug("GetFileAction::convertToValidDestinationFormat, could not convert file", e);
        }
        mimeType[0] = "";
        return null;
    }

    // - Public API ------------------------------------------------------------

    /**
     * @throws OXException
     */
    public void shutdown() {
        if (null != cache) {
            cache.shutdown();
        }
    }

    /**
     * Determines if a content stream format must be converted to be ensure that
     * a browser is able to display the content.
     *
     * @param fileExtension the file extension of the stream
     * @param resourceName the resource name of the stream
     * @return TRUE if the format must be converted or otherwise FALSE.
     */
    public boolean needsConversion(final String resourceName, final String fileExtension) {
        return "emf".equals(fileExtension) || "wmf".equals(fileExtension) || "svm".equals(fileExtension) || "svg".equals(fileExtension) || (StringUtils.isNotEmpty(resourceName) && (resourceName.indexOf("ObjectReplacements/") > -1));
    }

    /**
     * @param inputStm Image input stream to be converted.
     * @param resourceName The name of the current resource
     * @param fileExtension The extension of the input image resource
     * @param extents The target image extents, migh be <code>null</code>
     * @param mimeType contains the mime type of the converted result stream (out param) or ""
     * @return The converted result image buffer or null if conversion was not possible
     */
    public byte[] convert2png(final InputStream inputStm, final String resourceName, final String fileExtension, final Dimension extents, final String[] mimeType) throws IOException {
        if ((null != documentConverter) && (null != fileExtension) && (null != resourceName)) {
            final HashMap<String, Object> jobProperties = new HashMap<>(8);
            final HashMap<String, Object> resultProperties = new HashMap<>(8);

            jobProperties.put(Properties.PROP_INPUT_STREAM, inputStm);
            jobProperties.put(Properties.PROP_INPUT_TYPE, fileExtension);
            jobProperties.put(Properties.PROP_MIME_TYPE, "image/png");
            jobProperties.put(Properties.PROP_INFO_FILENAME, resourceName);

            if (null != extents) {
                jobProperties.put(Properties.PROP_PIXEL_WIDTH, Integer.valueOf(extents.width));
                jobProperties.put(Properties.PROP_PIXEL_HEIGHT, Integer.valueOf(extents.height));
            }

            // this is a user request in every case
            jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);

            try (final InputStream resultStm = documentConverter.convert("graphic", jobProperties, resultProperties, null)) {
                if (null != resultStm) {
                    mimeType[0] = "image/png";
                    return IOUtils.toByteArray(resultStm);
                }
            }
        }
        mimeType[0] = "";
        return null;
    }

    /**
     * @param documentStm, the stream of the document
     * @param resourceName The name of the shape resource that is requested,
     *            if split with "\\." the last four String entries have to be as followed:
     *            - "h" + hash value (string)
     *            - "p" + page index
     *            - "s" + shape index
     *            - "i" + input type ("docx", "xlsx" or "pptx")
     *            - "shape2png"
     *            A resourceName like this: "ppt/charts/chart1.xml.h8069F5DC-76BB-0395-EFB3-78555DB6F778-11109325-4298950.p0.s3.ipptx.shape2png"
     *            will export the fourth shape on the first page with a hash value "8069F5DC-76BB-0395-EFB3-78555DB6F778-11109325-4298950"
     * @param extents The target image extents, migh be <code>null</code>
     * @param mimeType contains the mime type of the converted result stream (out param)
     * @return The converted result image buffer or null of conversion was not possible
     * @throws IOException
     */
    public InputStream convertShape2PNG(final InputStream documentStm, final String resourceName, final Dimension extents, final String[] mimeType) {
        String[] params = resourceName.split("\\.", -1);
        try {
            final int l = params.length;
            if (params[l - 5].startsWith("h") && params[l - 4].startsWith("p") && params[l - 3].startsWith("s") && params[l - 2].startsWith("i")) {

                final String hash = params[l - 5].substring(1) + "-shape2png";
                final int pageIndex = Integer.parseInt(params[l - 4].substring(1));
                final int shapeIndex = Integer.parseInt(params[l - 3].substring(1));
                final String inputType = params[l - 2].substring(1);

                if (null != documentConverter && !hash.isEmpty()) {
                    final HashMap<String, Object> jobProperties = new HashMap<>(8);
                    final HashMap<String, Object> resultProperties = new HashMap<>(8);

                    jobProperties.put(Properties.PROP_INPUT_STREAM, documentStm);
                    jobProperties.put(Properties.PROP_INPUT_TYPE, inputType);
                    jobProperties.put(Properties.PROP_MIME_TYPE, "image/png");
                    jobProperties.put(Properties.PROP_INFO_FILENAME, resourceName);
                    jobProperties.put(Properties.PROP_PAGE_NUMBER, pageIndex + 1);
                    jobProperties.put(Properties.PROP_SHAPE_NUMBER, shapeIndex + 1);
                    jobProperties.put(Properties.PROP_CACHE_HASH, hash);
                    if (null != extents) {
                        jobProperties.put(Properties.PROP_PIXEL_WIDTH, Integer.valueOf(extents.width));
                        jobProperties.put(Properties.PROP_PIXEL_HEIGHT, Integer.valueOf(extents.height));
                    }

                    // this is a user request in every case
                    jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);

                    final InputStream resultStm = documentConverter.convert("shape2png", jobProperties, resultProperties, null);
                    if (null != resultStm) {
                        mimeType[0] = DocFileHelper.IMAGE_PNG;
                        return resultStm;
                    }
                }
            }
        } catch (RuntimeException e) {
           log.debug("GetFileAction::convertShape2PNG, shape could not be converted into png image", e);
        }
        mimeType[0] = "";
        return null;
    }

    // - Implementation --------------------------------------------

    /**
     * @param documentStm
     * @param resourceName
     * @param mimeType
     * @param closeDocumentStream
     * @return
     */
    final private byte[] getZipEntryBuffer(InputStream documentStm, String resourceName, String[] mimeType) {
        if (null != documentStm) {
            final String filename = FileHelper.getBaseName(resourceName);
            String uidResourceName = null;

            // check for uid Resources within all entries
            if (filename.startsWith(Resource.RESOURCE_UID_PREFIX) && (filename.length() > Resource.RESOURCE_UID_PREFIX.length())) {
                uidResourceName = filename;
            }

            try (final ZipInputStream zipInputStm = new ZipInputStream(documentStm)) {
                for (ZipEntry zipEntry = null; ((zipEntry = zipInputStm.getNextEntry()) != null);) {
                    try {
                        final String curEntryName = zipEntry.getName();

                        if (curEntryName.equals(DocFileHelper.OX_RESCUEDOCUMENT_DOCUMENTSTREAM_NAME)) {
                            // check for resources contained within the original rescue document
                            try (final InputStream entryInputStream = new ByteArrayInputStream(IOUtils.toByteArray(zipInputStm))) {
                                byte[] rescueStreamResult = getZipEntryBuffer(entryInputStream, resourceName, mimeType);
                                if (rescueStreamResult != null) {
                                    return rescueStreamResult;
                                }
                            }
                        } else if (curEntryName.equals(resourceName) || ((null != uidResourceName && curEntryName.contains(uidResourceName)))) {
                            // check for resources equal the the given name and for resources matching the uid resource manager naming schema
                            mimeType[0] = DocFileHelper.getMimeType(resourceName);
                            return IOUtils.toByteArray(zipInputStm);
                        }
                    } catch (IOException e) {
                        log.error("AjaxRequest GetFileAction.getZipEntryBuffer exception caught while trying to read data from document stream", e);
                    } finally {
                        zipInputStm.closeEntry();
                    }
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);
                log.error("AjaxRequest GetFileAction.getZipEntryBuffer exception caught while trying to read data from document stream", e);
            }
        }
        return null;
    }

    /**
     * @param request
     * @return
     */
    private Dimension getImageExtents(String widthParam, String heightParam) {
        // get optional extents (width/height)
        try {
            int width = -1;
            int height = -1;

            if (isNotEmpty(widthParam)) {
                width = Integer.parseInt(widthParam);
            }

            if (isNotEmpty(heightParam)) {
                height = Integer.parseInt(heightParam);
            }

            if ((width != 0) || (height != 0)) {
                return new Dimension((width > 0) ? width : -1, (height > 0) ? height : -1);
            }
        } catch (NumberFormatException e) {
            log.error("AjaxRequest GetFileAction exception caught while parsing width/height parameters", e);
        }

        return null;

    }

    // - Members ---------------------------------------------------------------

    @Autowired
    private final ConfigurationService configService = null;

    @Autowired
    private final IResourceManagerFactory resourceManagerFactory = null;

    @Autowired
    private DataSourceAccessFactory dataSourceAccessFactory;

    @Autowired(required = false)
    private IDocumentConverter documentConverter;

    @Autowired
    private ResourceFactory resourceFactory;

    private IResourceManager resourceManager = null;

    private Cache cache = null;

    // - Static members -------------------------------------------------------.

    /**
     * log
     */
    final private static Logger log = LoggerFactory.getLogger(GetFileAction.class);

    /**
     * IMAGE_EXTENSION_TO_MIMETYPE_MAP
     */
    final private static Map<String, String> IMAGE_EXTENSION_TO_MIMETYPE_MAP = new HashMap<>(24);

    /**
     * initialization of static class member
     */
    {
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("bmp", " image/bmp");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("emf", "image/x-emf");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("gif", "image/gif");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("heic", "image/heic");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("heif", "image/heif");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("ico", "image/x-icon");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("jpg", "image/jpeg");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("jpe", "image/jpeg");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("jpeg", "image/jpeg");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("pcx", "image/x-pcx");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("pct", "image/x-pict");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("pict", "image/x-pcx");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("pbm", "image/x-portable-bitmap");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("pnm", "image/x-portable-anymap");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("png", "image/png");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("psd", "image/vnd.adobe.photoshop");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("svm", "image/svm");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("tif", "image/tiff");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("tiff", "image/tiff");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("tga", "image/x-tga");
        IMAGE_EXTENSION_TO_MIMETYPE_MAP.put("wmf", "windows/metafile");
    }

}
