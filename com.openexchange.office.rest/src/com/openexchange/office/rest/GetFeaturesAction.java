/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.exception.OXException;
import com.openexchange.tools.session.ServerSession;


/**
 * {@link GetFeaturesAction}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@RestController
public class GetFeaturesAction extends DocumentRESTAction {

	private static final Logger LOG = LoggerFactory.getLogger(GetFeaturesAction.class);

	@Autowired(required=false)
    private IDocumentConverter documentConverter;
	
    /* (non-Javadoc)
     * @see com.openexchange.ajax.requesthandler.AJAXActionService#perform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {
        AJAXRequestResult requestResult = null;
        final StringBuilder featureBuilder = new StringBuilder();
        final JSONObject resultObj = new JSONObject(1);

        if (null != documentConverter) {
            featureBuilder.append("documentconverter");
        }

        // additional features need to be appended to the
        // string builder with a comma as feature separator

        // currently supported runtime features are:
        //  - 'documentconverter'

        try {
            resultObj.put("features", featureBuilder.toString());
            requestResult = new AJAXRequestResult(resultObj);
        } catch (final JSONException e) {
            LOG.error("Exception caught while trying to process GetFeaturesAction request", e);
        }

        return requestResult;
    }

}
