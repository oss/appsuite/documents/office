/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.openexchange.office.tools.common.csv.CSVEntryParser;

public class FieldsParserCSV
        implements
        FieldsParser
{
    private List<String> allLines;

    public FieldsParserCSV(
            List<String> allLines)
                throws Exception
    {
        this.allLines = allLines;
    }

    @Override
    public Map<String, Object> getFields()
        throws Exception
    {

        List<String> header = null;
        List<String> first = null;
        String firstLine = allLines.get(0);
        if (firstLine.indexOf("sep=") == 0)
        {
            String[] seps = new String[] { firstLine.substring(4, 5) };
            header = CSVEntryParser.getCSVContentFromLine(allLines.get(1), seps);
            first = CSVEntryParser.getCSVContentFromLine(allLines.get(2), seps);
        }
        else
        {
            String[] seps = new String[] { ";", ",", "\t" };
            header = CSVEntryParser.getCSVContentFromLine(allLines.get(0), seps);
            first = CSVEntryParser.getCSVContentFromLine(allLines.get(1), seps);
        }

        Map<String, Object> fields = new HashMap<String, Object>();

        for (int i = 0; i < header.size(); i++)
        {
            fields.put(header.get(i), first.get(i));
        }
        return fields;
    }

}
