/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.office.tools.doc.DocumentFormat;

public class FieldsParserJSONSheets implements FieldsParser {

	private IDBasedFileAccess fileAccess;
    private final String         fileId;
    private final DocumentFormat format;

    public FieldsParserJSONSheets(IDBasedFileAccess fileAccess, String fileId, DocumentFormat format)
    {
    	this.fileAccess = fileAccess;
        this.fileId = fileId;
        this.format = format;
    }

    @Override
    public Map<String, Object> getFields()
        throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileAccess.getDocument(fileId, FileStorageFileAccess.CURRENT_VERSION), "UTF-8"));

        List<String> allLines = new ArrayList<String>();
        StringBuilder all = new StringBuilder();
        String line = reader.readLine();
        while (null != line)
        {
            allLines.add(line);
            all.append(line);
            line = reader.readLine();
        }
        String param = all.toString();

        try
        {
            new JSONObject(param);

            if (format == DocumentFormat.XLSX || format == DocumentFormat.ODS)
            {
                return new FieldsParserSheets(param).getFields();
            }
            else
            {
                return new FieldsParserParam(param).getFields();
            }
        }
        catch (JSONException e)
        {
            return new FieldsParserCSV(allLines).getFields();           
        }
    }

}
