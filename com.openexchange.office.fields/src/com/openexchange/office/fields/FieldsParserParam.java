/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class FieldsParserParam
        implements
        FieldsParser
{
    private String param;

    public FieldsParserParam(
            String param)
        throws Exception
    {
        this.param = param;
    }

    @Override
    public Map<String, Object> getFields()
        throws Exception
    {
        Map<String, Object> values;
        try
        {
            JSONObject fields = new JSONObject(param);
            values = fields.asMap();
        }
        catch (JSONException e)
        {
            String[] fields = param.split(";");
            values = new HashMap<String, Object>();
            for (int i = 0; i < fields.length; i += 2)
            {
                values.put(fields[i], fields[i + 1]);
            }
        }
        return values;
    }

}
