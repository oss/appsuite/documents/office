/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.tools.session.ServerSession;

@Service
@RegisteredService
public class DynamicFieldsService {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicFieldsService.class);

    @Autowired
    private ImExporterAccessFactory imExporterFactory;

    @Autowired
    private IDBasedFileAccessFactory idBasedFileAccessFactory;

	public InputStream insertOptionalFields(InputStream inputStm, String extensionType, AJAXRequestData request, JSONObject result,
											ServerSession session) throws Exception {
		try {

	        final String sfs = request.getParameter("sourcefields");
	        final String fs = request.getParameter("fields");

	        final DocumentFormat format = DocFileHelper.getDocumentFormat("." + extensionType);
	        final IImporter importer = imExporterFactory.getImporterAccess(format);
	        final IExporter exporter = imExporterFactory.getExporterAccess(format);

	        final DocumentProperties documentProperties = new DocumentProperties();
	        documentProperties.put(DocumentProperties.PROP_SAVE_TEMPLATE_DOCUMENT, false);

	        FieldsParser parser = null;
	        if (StringUtils.isNotBlank(sfs)) {
	        	final IDBasedFileAccess fileAccess = idBasedFileAccessFactory.createAccess(session);
	            parser = new FieldsParserJSONSheets(fileAccess, sfs, format);
	        }
	        else if (StringUtils.isNotBlank(fs)) {
	            if (format == DocumentFormat.XLSX || format == DocumentFormat.ODS) {
	                parser = new FieldsParserSheets(fs);
	            } else {
	                parser = new FieldsParserParam(fs);
	            }
	        } else {
	            return inputStm;
	        }

	        FieldsHandler handler = null;
	        if (format == DocumentFormat.XLSX || format == DocumentFormat.ODS) {
	            handler = new FieldsHandlerSheet();
	        } else {
	            handler = new FieldsHandlerText();
	        }

	        Map<String, Object> fields = parser.getFields();

	        boolean resetable = false;//inputStm.markSupported();
	        // TODO: 10.20.70.123/appsuite/#debug-js=true&autologin=1&app=io.ox/office/text&folder=369&convert=1&id=415&destfolderid=371&fields=BSStrasse;Coole%20strasse%2047;BSPLZ;777
	        JSONObject doc = null;

			if (resetable) {
				doc = importer.createOperations(session, inputStm, documentProperties, false);
				inputStm.reset();
			} else {
				List<InputStream> inputStreams = copyStream(inputStm);
			    inputStm = inputStreams.get(0);
			    InputStream newInputStm = inputStreams.get(1);
			    doc = importer.createOperations(session, newInputStm, documentProperties, false);
			}


	        final JSONArray ops = doc.getJSONArray("operations");
	        final JSONArray newOps = handler.getMergeOperations(ops, fields);

	        if (newOps.length() > 0) {
	            final String mergeOperations = newOps.toString();

	            inputStm = exporter.createDocument(session, inputStm, mergeOperations, null, documentProperties, true);
	            result.put("fieldChanges", newOps.length());
	        } else {
	            result.put("fieldChanges", "none");
	        }

        } catch (Throwable e) {
        	ExceptionUtils.handleThrowable(e);
            LOG.error("RT connection: Exception caught trying to handle dynamic fields!", e);
			throw new Exception(e);
		}
        return inputStm;
    }

    private static List<InputStream> copyStream(InputStream input) throws Exception {
        final List<InputStream> res = new ArrayList<InputStream>();
        final byte[] content = IOUtils.toByteArray(input);

        res.add(new ByteArrayInputStream(content));
        res.add(new ByteArrayInputStream(content));

        return res;
    }

}
