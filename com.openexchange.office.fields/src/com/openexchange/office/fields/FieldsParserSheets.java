/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

public class FieldsParserSheets
        implements
        FieldsParser
{
    private String param;

    public FieldsParserSheets(
            String param)
        throws Exception
    {
        this.param = param;
    }

    @Override
    public Map<String, Object> getFields()
        throws Exception
    {
        Map<String, Object> fields = new HashMap<String, Object>();
        
        try
        {
            Map<String, Object> sheets = new JSONObject(param).asMap();        

            for (Entry<String, Object> sheet : sheets.entrySet())
            {
                Map<String, Object> entries = (Map<String, Object>) sheet.getValue();
                for (Entry<String, Object> entry : entries.entrySet())
                {
                    Map<String, Object> json = (Map<String, Object>) entry.getValue();

                    fields.put(sheet.getKey() + "!" + entry.getKey(), new JSONObject(json));
                }
            }
        }
        catch (JSONException e)
        {
            String[] fieldsource = param.split(";");
            for (int i = 0; i < fieldsource.length; i += 2)
            {
                fields.put(fieldsource[i], fieldsource[i + 1]);
            }
        }
        return fields;

    }

    /*
     * {sheet1: { B2: { type: 'Text'. value:'Prospecting' }, C2: { type: 'currency' value: '1234,45 EUR' } } }
     */
}
