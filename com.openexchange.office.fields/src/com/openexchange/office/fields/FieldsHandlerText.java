/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

public class FieldsHandlerText
        implements
        FieldsHandler
{

    @Override
    public JSONArray getMergeOperations(
        JSONArray ops,
        Map<String, Object> fields)
        throws Exception
    {
        JSONArray newOps = new JSONArray();

        Map<JSONArray, JSONObject> insertFields = new HashMap<JSONArray, JSONObject>(fields.size());

        for (int i = 0; i < ops.length(); i++)
        {
            JSONObject op = ops.getJSONObject(i);
            String name = op.getString(OCKey.NAME.value());

            if (name.equals(OCValue.INSERT_FIELD.value()) && op.optString(OCKey.TYPE.value(), "").equals("database-display")) {

            	insertFields.put(op.getJSONArray(OCKey.START.value()), op);

            } else if (name.equals(OCValue.SET_ATTRIBUTES.value())) {

            	JSONArray start = op.optJSONArray(OCKey.START.value());

            	JSONObject insertField = insertFields.get(start);

            	if (null != insertField) {
            		insertFields.remove(start);

            		String columnName = op.getJSONObject(OCKey.ATTRS.value()).getJSONObject(OCKey.CHARACTER.value()).getJSONObject(OCKey.FIELD.value()).optString("text:column-name", "");
            		Object value = fields.get(columnName);

            		if (value != null) {

            			System.out.println(columnName + " -> " + start + " " + value);

            			JSONObject del = new JSONObject(3);
            			del.put(OCKey.NAME.value(), OCValue.DELETE.value());
                        del.put(OCKey.START.value(), start);
                        del.put(OCKey.END.value(), start);

                        insertField.put(OCKey.REPRESENTATION.value(), value.toString());

                        newOps.put(del);
                        newOps.put(insertField);
                        newOps.put(op);
            		}
            	}

            }

        }
        return newOps;
    }
}
