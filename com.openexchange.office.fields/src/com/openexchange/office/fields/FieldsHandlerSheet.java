/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.fields;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.ColumnRef;
import com.openexchange.office.filter.core.spreadsheet.Interval;

public class FieldsHandlerSheet
        implements
        FieldsHandler
{
    static final Map<String, String> TYPES;

    static
    {
        TYPES = new HashMap<String, String>();

        TYPES.put("text", "@");
        TYPES.put("currency", "[$$-409]#,##0");
        TYPES.put("date", "DD/MM/YYYY");
        TYPES.put("number", "#,##0.00");
    }

    @Override
    public JSONArray getMergeOperations(
        JSONArray ops,
        Map<String, Object> fields)
            throws Exception
    {

        int autostyleIndex = 0;

        Map<String, Integer> sheetMap = new HashMap<String, Integer>();

        for (int i = 0; i < ops.length(); i++)
        {
            JSONObject op = ops.getJSONObject(i);
            String name = op.getString(OCKey.NAME.value());

            if (name.equals(OCValue.INSERT_SHEET.value()))
            {
                sheetMap.put(op.getString(OCKey.SHEET_NAME.value()), Integer.decode(op.getString(OCKey.SHEET.value())));
            }
            else if (name.equals(OCValue.INSERT_STYLE_SHEET.value()) && op.optBoolean("auto", false))
            {
                int autoStyleId = Integer.parseInt(op.getString(OCKey.STYLE_ID.value()).substring(1));
                autostyleIndex = Math.max(autostyleIndex, autoStyleId);
            }
        }

        JSONArray newOps = new JSONArray();
        for (Entry<String, Object> field : fields.entrySet())
        {
            JSONObject newOp = new JSONObject();

            Integer sheet = sheetMap.get(field.getKey().split("!")[0]);
            JSONArray cellRef = createCellRef(field.getKey().split("!")[1]);

            newOp.put(OCKey.NAME.value(), OCValue.CHANGE_CELLS.value());
            newOp.put(OCKey.SHEET.value(), sheet);

            final JSONObject contents = new JSONObject(1);
            final JSONObject cell = new JSONObject();
            contents.put(CellRef.getCellRef(new CellRef(cellRef)), cell);
            newOp.put(OCKey.CONTENTS.value(), contents);

            JSONObject content = null;
            try
            {
                content = new JSONObject(field.getValue().toString());
                cell.put(OCKey.V.value(), content.get(OCKey.VALUE.value()));
            }
            catch (JSONException e)
            {
            	cell.put(OCKey.V.value(), field.getValue().toString());
            }

            if (null != content)
            {
                String type = content.optString(OCKey.TYPE.value());

                if (StringUtils.equals(type, "number") || StringUtils.equals(type, "currency")) {
                    Object value = content.get(OCKey.VALUE.value());
                    if (value instanceof String) {
                        String valueString = (String) value;
                        int lastKomma = valueString.lastIndexOf(",");
                        int lastPoint = valueString.lastIndexOf(".");
                        if (lastKomma > lastPoint) {
                            valueString = valueString.substring(0, lastPoint) + valueString.substring(lastPoint+1, lastKomma) + '.' +   valueString.substring(lastKomma+1, valueString.length());
                            lastPoint = valueString.lastIndexOf(".");
                        }
                        if (lastPoint >= 0) {
                            try
                            {
                                newOp.put(OCKey.VALUE.value(), Float.parseFloat(valueString));
                            }
                            catch (NumberFormatException e)
                            {
                                newOp.put(OCKey.ERROR.value(), e.getMessage());
                            }
                        }
                    }
                }

                String nf = TYPES.get(type);
                if (StringUtils.isNotEmpty(nf))
                {
                	autostyleIndex++;
                    String styleId = "a" + autostyleIndex;
                    cell.put(OCKey.S.value(), styleId);

                    JSONObject cellAttrs = new JSONObject();
                    cellAttrs.put(OCKey.FORMAT_CODE.value(), nf);

                    /*
                     * JSONObject color = new JSONObject(); color.put(OCKey.TYPE.shortName(), "scheme"); color.put(OCKey.VALUE.shortName(), "accent4"); cell.put("fillColor", color);
                     */

                    // {sheet:0,start:[2,1],attrs:{cell:{fillColor:{type:"scheme",value:"accent1",fallbackValue:"5b9bd5"}}}}


                    JSONObject attrs = new JSONObject();
                    attrs.put(OCKey.CELL.value(), cellAttrs);

                    JSONObject autostyle = new JSONObject(4);
                    autostyle.put(OCKey.NAME.value(), OCValue.INSERT_AUTO_STYLE.value());
                    autostyle.put(OCKey.TYPE.value(), "cell");
                    autostyle.put(OCKey.STYLE_ID.value(), styleId);
                    autostyle.put(OCKey.ATTRS.value(), attrs);
                    newOps.put(autostyle);
                }

                Object width = content.opt(OCKey.WIDTH.value());
                if (width != null)
                {
                    JSONObject col = new JSONObject(4);
                    col.put(OCKey.NAME.value(), OCValue.CHANGE_COLUMNS.value());
                    col.put(OCKey.SHEET.value(), sheet);
                    col.put(OCKey.INTERVALS.value(), new Interval(new ColumnRef(cellRef.getInt(0)), new ColumnRef(cellRef.getInt(0))).toString());

                    JSONObject column = new JSONObject(2);
                    column.put(OCKey.CUSTOM_WIDTH.value(), true);
                    column.put(OCKey.WIDTH.value(), width);

                    JSONObject attrs = new JSONObject(1);
                    attrs.put(OCKey.COLUMN.value(), column);

                    col.put(OCKey.ATTRS.value(), attrs);
                    newOps.put(col);

                }
            }
            newOps.put(newOp);

        }

        return newOps;
    }

    /*
     * copied from SmlUtils
     */
    public static JSONArray createCellRef(
        String cellRef)
    {

        if (cellRef == null || cellRef.isEmpty())
        {
            return null;
        }

        int characters = 0;
        int characterStartIndex = cellRef.charAt(0) == '$' ? 1 : 0;

        while ((characters + characterStartIndex) < cellRef.length())
        {
            char character = cellRef.charAt(characterStartIndex + characters);
            if ((character < 'A') || (character > 'Z'))
            {
                break;
            }
            characters++;
        }
        if (characters > 0 && (characters + characterStartIndex) < cellRef.length() && characters < 6)
        {

            final int[] exMap =
            { 1, 26, 676, 17576, 456976 };

            int ex = characters - 1;
            int x = 0;

            for (int i = 0; i < characters; i++)
            {
                x += ((cellRef.charAt(i + characterStartIndex) - 'A') + 1) * exMap[ex - i];
            }

            int numbers = 0;
            int numberStartIndex = characterStartIndex + characters;

            if (cellRef.charAt(numberStartIndex) == '$')
            {
                numberStartIndex++;
            }
            while (numbers + numberStartIndex < cellRef.length())
            {
                char character = cellRef.charAt(numberStartIndex + numbers);
                if ((character < '0') || (character > '9'))
                {
                    break;
                }
                numbers++;
            }
            if (numbers > 0)
            {
                int y = 0;
                for (int i = 0; i < numbers; i++)
                {
                    y *= 10;
                    y += cellRef.charAt(i + numberStartIndex) - '0';
                }
                if (y > 0)
                {
                    JSONArray res = new JSONArray();
                    res.put(x - 1);
                    res.put(y - 1);
                    return res;
                }
            }
        }
        return null;
    }

}
