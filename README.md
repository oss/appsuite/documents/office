# OX Documents

The OX Documents implementation are additional bundles for the core-mw service which need to be part of the core-mw image. That's why the office repository contains no Helm chart or Dockerfile defintion. Everything is configured and built by the OX Appsuite core middleware chart and/or stackchart / built by the integration build.

## Requirements

The Java sources need Java 21, OSGi runtime, the OX Appsuite core middleware bundles to build. Hazelcast 5.x will be provided by the core middleware bundle (com.hazelcast) and ActiveMQ Classic is part of the OX Documents com.openexchange.office.dependencies bundle.

## Development environment

Use Eclipse Version: 2022-06 (4.24.0) or later to build the code for development.

## Structure

The implementation consists of the following parts:

  * Realtime 2 (including ActiveMQ client)
  * Realtime 2 performance test client
  * ODF filter
  * OOXML filter
  * Monitoring (JMX/Prometheus)
  * Tools (JMS/database/monitoring/services etc.)
  * Dependencies shadow Jar

## Dependencies

### com.openenexchange.office.dependencies

This bundles exports some commonly used 3rd party libraries aggregated into a shadow jar via Gradle.

  * ch.qos.logback:logback-classic:1.2.11
  * org.apache.commons:commons-text:1.10.0
  * javax.activation:activation:1.1.1
  * javax.validation:validation-api:2.0.1.Final
  * org.apache.activemq:activemq-pool:5.18.2
  * org.apache.activemq:activemq-client:5.18.2
  * org.glassfish.jersey.containers:jersey-container-servlet-core:2.39
  * org.springframework:spring-web:5.3.21
  * org.springframework:spring-jms:5.3.21
  * org.springframework:spring-context:5.3.21
  * org.springframework.boot:spring-boot-autoconfigure:2.7.9
  * com.google.protobuf:protobuf-java:3.6.1
  * io.dropwizard.metrics:metrics-core:4.2.19
  * io.dropwizard.metrics:metrics-jmx:4.2.19
  * com.udojava:JMXWrapper:1.4
  * io.leangen.geantyref:geantyref:1.3.14

### com.openexchange.office.tools.service

This bundle includes some checked in jars in the lib folder:

  * classmate-1.5.1.jar  / com.fasterxml:classmate:1.5.1
  * hibernate-validator-6.2.3.Final / org.hibernate.validator:hibernate-validator:6.2.3.Final
  * hibernate-validation-annotation-processor-6.2.3.Final / org.hibernate.validator:hibernate-validator-annotation-processor:6.2.3.Final
  * jakarta.validation-api-2.0.2 / jakarta.validation:jakarta.validation-api:2.0.2
  * javax.el-3.0.1-b12 / org.glassfish:javax.el:3.0.1-b12
  * jboss-logging-3.5.0.Final / org.jboss.logging:jboss-logging:3.5.0.Final

### com.openexchange.office.filter.odf

This bundle contains the ODF filter implementation including some checked in jars in the lib folder:

  * serializer-2.7.2.jar / xalan:serializer:2.7.2
  * xerces2-xsd11-2.11.2.jar / com.rackspace.apache:xerces2-xsd11:2.11.2
  * xml-resolver-1.2.jar / xml-resolver:xml-resolver:1.2

### com.openexchange.office.docx4j

This bundle contains the low-level OOXML filter implementation including some checked in jars in the lib folder:

  * jaxb-core-4.0.4.jar / org.glassfish.jaxb:jaxb-core:4.0.4
  * jaxb-runtime-4.0.4.jar / org.glassfish.jaxb:jaxb-runtime:4.0.4
  * serializer-2.7.2.jar / xalan:serializer:2.7.2
  * stringtemplate-4.0.2.jar / org.antlr:stringtemplate:4.0.2
  * xalan-2.7.2.jar / xalan:xalan:2.7.2

### com.openexchange.office.spellcheck

This bundle contains the client part to access the Spellcheck service. It contains a checked in jar in the lib folder:

  * jna.3.5.1.jar / net.java.dev.jna:jna:3.5.1 (should be obsolete)
