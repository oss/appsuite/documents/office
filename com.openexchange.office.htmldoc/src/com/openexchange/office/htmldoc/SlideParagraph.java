/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SlideParagraph extends Paragraph {

    private String containerId;
    private String target;

    public SlideParagraph(String containerId, String target, JSONObject attrs) throws Exception {
        super(attrs);

        this.containerId = containerId;
        this.target = target;
    }

    @Override
    protected final void appendHTMLCLasses(StringBuilder document) throws Exception {
        document.append(" slide");

        super.appendHTMLCLasses(document);
    }

    @Override
    protected final void appendHTMLAttributes(StringBuilder document) throws Exception {
        document.append(" data-container-id='" + GenDocHelper.escapeUIString(containerId) + "' ");
        document.append(" target='" + GenDocHelper.escapeUIString(target) + "' ");
    }

    @Override
    protected void appendHTMLStyles(StringBuilder document) throws Exception {
        // not implemented
    }

    @Override
    public final void insert(JSONArray start, INode newChild) throws Exception {
        if (start.length() == 1) {
            if (newChild instanceof Text) {
                ((Text) newChild).disableEmptySpanStyle();
            } else {
                ((Drawing) newChild).disableEmptySpanStyle();
            }
        }
        super.insert(start, newChild);
    }

    @Override
    protected void addEmptySpan(StringBuilder document, JSONObject attrs) throws JSONException {
        GenDocHelper.addEmptySpan(document, null, false);
    }
}
