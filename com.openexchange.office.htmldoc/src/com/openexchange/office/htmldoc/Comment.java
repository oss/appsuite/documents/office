/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class Comment extends NodeHolder {

    private static final String  COMMENT_THREADOPEN      = "<div class='commentthread";
    private static final String  COMMENT_THREADCLOSE     = "'>";
    private static final String  COMMENT_START           = "<div contenteditable='false' class='drawing comment'";
    private static final String  COMMENT_METAINFO        = "<div class='commentmetainfo'>";
    private static final String  COMMENT_PICTURE_START   = "<div class='commentpicture app-tooltip'";
    private static final String  COMMENT_PICTURE_END     = "></div>";
    private static final String  COMMENT_AUTHOR_DATE     = "<div class='commentauthordate'>";
    private static final String  COMMENT_AUTHOR_START    = "<a class='commentauthor'>";
    private static final String  COMMENT_AUTHOR_END      = "</a>";
    private static final String  COMMENT_DATE            = "<div class='commentdate'></div>";
    private static final String  COMMENT_BUTTON          = "<div class='commentbutton'>";
    private static final String  COMMENT_METAINFO_CLOSER = "<a class='reply commentreply' tabindex='1'><i class='fa fa-reply' aria-hidden='true'></i></a><a class='closer commentcloser' tabindex='1'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div></div>";
    private static final String  CONTENT_START           = "<div class='content textframecontent autoresizeheight'>";
    private static final String  COMMENT_BOTTOM          = "<div class='commentbottom'><a class='shrinkarrow' tabindex='1'><i class='fa fa-sort-desc' aria-hidden='true'></i></a></div>";
    private static final String  TEXTFRAME_START         = "<div class='textframe' contenteditable='true' data-gramm='false'>";

    private final String id;
    private final String author;
    private final String uid;
    private final String date;
    private final String target;
    private final String parentId;

    public Comment(String id, String author, String uid, String date, String target, String parentId) {
        super(null);

        this.id = id;
        this.author = author;
        this.uid = uid;
        this.date = date;
        this.target = target;
        this.parentId = parentId;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception

    {
        // special handling for the author (51863)
        String authorStr = GenDocHelper.escapeUIString(this.author);
        String dateStr = GenDocHelper.escapeUIString(this.date);
        String uidStr = GenDocHelper.escapeUIString(this.uid);
        String idStr = GenDocHelper.escapeUIString(this.id);

    	// appending the comment thread node
    	document.append(COMMENT_THREADOPEN);
    	if (!this.target.isEmpty()) {
    	    document.append(" targetcomment");
    	}
    	document.append(COMMENT_THREADCLOSE);

    	// appending the comment node
        document.append(COMMENT_START);
        document.append("data-container-id=" + idStr + " data-container-author='" + authorStr + "'");
        if (StringUtils.isNotEmpty(this.uid)) {
            document.append(" data-container-uid=" + uidStr);
        }
        document.append(" data-container-date=" + dateStr);
        if (StringUtils.isNotEmpty(this.parentId)) {
            document.append(" data-container-parentid=" + GenDocHelper.escapeUIString(this.parentId));
        }

        JSONObject params = new JSONObject();
        params.put("containerId", idStr);
        params.put("containerAuthor", authorStr);
        if (StringUtils.isNotEmpty(this.uid)) {
            params.put("containerUid", uidStr);
        }
        params.put("containerDate", dateStr);
        params.put(OCKey.TYPE.value(), "comment");
        params.put("model", "null");

        document.append(" jquerydata='");
        document.append(params.toString());
        document.append("'>");

        // the meta info div element (open and close)
        document.append(COMMENT_METAINFO);
        document.append(COMMENT_PICTURE_START);
        document.append(" aria-label='" + authorStr + "' data-original-title='" + authorStr + "'");
        document.append(COMMENT_PICTURE_END);
        document.append(COMMENT_AUTHOR_DATE);
        document.append(COMMENT_AUTHOR_START + authorStr + COMMENT_AUTHOR_END);
        document.append(COMMENT_DATE);
        document.append("</div>"); // the common author-date node
        document.append(COMMENT_BUTTON);
        document.append(COMMENT_METAINFO_CLOSER);

        // the content node
        document.append(CONTENT_START);

        // the text frame node
        document.append(TEXTFRAME_START);

        super.appendContent(document);

        document.append("</div>"); // the text frame node
        document.append("</div>"); // the content node

        document.append(COMMENT_BOTTOM);

        document.append("</div>"); // the comment node
        document.append("</div>"); // the comment thread node

        return true;
    }

}
