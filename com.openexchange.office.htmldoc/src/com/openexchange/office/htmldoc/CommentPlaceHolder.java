/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import org.json.JSONArray;

public class CommentPlaceHolder extends SubNode {

    private static final String PLACEHOLDERSTART = "<div contenteditable=\"false\" class=\"inline commentplaceholder\" data-container-id=\"";
    private static final String PLACEHOLDERCLOSER = "\"";
    private static final String PLACEHOLDEREND = "></div>";

    private String              id;
    private String              author;
    private String              target;
    private String              userId;
    private String              providerId;
    private String              authorId;
    private String              date;
    private String              parentId;
    private String              text;
    private String              initials;
    private boolean             restorable;
    private JSONArray           mentions;

    // ///////////////////////////////////////////////////////////

    public CommentPlaceHolder(int position, String id, String target, String author, String userId, String providerId, String authorId, String date, String parentId, String text, String initials, boolean restorable, JSONArray mentions, JSONObject attrs)
        throws Exception {

        super(position, 1);

        this.id = id;
        this.author = author;
        this.target = target;
        this.userId = userId;
        this.providerId = providerId;
        this.authorId = authorId;
        this.date = date;
        this.parentId = parentId;
        this.text = text;
        this.initials = initials;
        this.restorable = restorable;
        this.mentions = mentions;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(PLACEHOLDERSTART + GenDocHelper.escapeUIString(this.id) + PLACEHOLDERCLOSER);

        JSONObject params = new JSONObject();
        params.put("containerId", this.id);
        params.put(OCKey.TARGET.value(true), this.target);
        params.put(OCKey.AUTHOR.value(true), this.author);
        params.put(OCKey.USER_ID.value(true), this.userId);
        params.put(OCKey.PROVIDER_ID.value(true), this.providerId);
        params.put(OCKey.AUTHOR_ID.value(true), this.authorId);
        params.put(OCKey.DATE.value(true), this.date);
        params.put(OCKey.PARENT_ID.value(true), this.parentId);
        params.put(OCKey.TEXT.value(true), this.text);
        params.put(OCKey.INITIALS.value(true), this.initials);
        params.put(OCKey.RESTORABLE.value(true), this.restorable);
        params.put(OCKey.MENTIONS.value(true), this.mentions);

        document.append(" jquerydata='");
        document.append(GenDocHelper.escapeUIString(params.toString()));
        document.append("'");
        document.append(PLACEHOLDEREND);

        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return "CommentPlaceHolder";
    }
}
