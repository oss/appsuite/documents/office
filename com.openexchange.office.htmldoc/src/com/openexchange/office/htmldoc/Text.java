/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class Text extends SubNode {

    private static final String SPANBEFORE = "<span ";
    private static final String SPANMIDDLE = "style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-style: normal; text-decoration: none; color: rgb(0, 0, 0); font-size: 11pt; line-height: 20px; background-color: transparent;\">";
    private static final String SPANEND    = "</span>";

    // ////////////////////////////////////////////////////

    private final String text;
    private boolean style = true;

    public Text(String text, int position, JSONObject attrs) throws Exception {
        super(position, text.length());
        this.text = text;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    public String getText()
    {
        return text;
    }

    @Override
    public boolean appendContent(StringBuilder document) throws Exception {
        GenDocHelper.addSpan(document, getAttribute(), isStyle(), text);
        return true;
    }

    @Override
    public String toString()
    {
        return "Text: " + getText();
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    public boolean isStyle() {
        return style;
    }

    public void disableEmptySpanStyle() {
        this.style = false;
    }
}
