/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

public class TextHtmlDocumentBuilder
{
    private final static Logger         LOG        = LoggerFactory.getLogger(TextHtmlDocumentBuilder.class);
    private final static Set<String>    IGNOREOPS  = new HashSet<String>();

    static
    {
        IGNOREOPS.add(OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        IGNOREOPS.add(OCValue.INSERT_FONT_DESCRIPTION.value());
        IGNOREOPS.add(OCValue.INSERT_STYLE_SHEET.value());
        IGNOREOPS.add(OCValue.INSERT_THEME.value());
        IGNOREOPS.add(OCValue.INSERT_LIST_STYLE.value());
    }

    static public Logger getLogger()
    {
        return LOG;
    }

    public static String buildHtmlDocument(
        JSONObject documentOperations,
        DocMetaData docMetaData,
        TextTableLimits tableLimits) {

        return buildHtmlDocument(documentOperations, docMetaData.getId(), docMetaData.getFolderId(), docMetaData.getVersionOrAttachment(), docMetaData.getFileName(), docMetaData.getSource(), tableLimits);
    }

    // public because of being used by some performance tests
    static public String buildHtmlDocument(
        JSONObject documentOperations,
        String fileId,
        String folderId,
        String fileVersion,
        String fileName,
        String source,
        TextTableLimits textTableLimits)
    {
        int fv = 0;
        String result = null;
        Map<String, NodeHolder> headerFooterNodes = new HashMap<String, NodeHolder>();
        Map<String, NodeHolder> commentNodes = new HashMap<String, NodeHolder>();
        final Stack<String> complexFieldIdStack = new Stack<String>();
        int nextComplexFieldId = 0;

        DocumentNode doc = new DocumentNode();

        try
        {
            final long time = System.currentTimeMillis();
            final JSONArray operations = documentOperations.getJSONArray("operations");
            final JSONArray reducedOperations = new JSONArray();

            for (int i = 0; i < operations.length(); i++)
            {
                JSONObject operation = operations.getJSONObject(i);
                try
                {
                    final String opName = operation.getString(OCKey.NAME.value());
                    if (IGNOREOPS.contains(opName)) {
                        reducedOperations.put(operation);
                        if(fv == 0 && opName.equals(OCValue.SET_DOCUMENT_ATTRIBUTES.value())) {
                            fv = GenericHtmlDocumentBuilder.getFvFromSetDocumentOperation(operation);
                        }
                        continue;
                    }
                    JSONArray start = operation.optJSONArray(OCKey.START.value());
                    JSONArray end = operation.optJSONArray(OCKey.END.value());
                    JSONObject attrs = operation.optJSONObject(OCKey.ATTRS.value());

                    if (opName.equals(OCValue.INSERT_HEADER_FOOTER.value()))
                    {
                        String id = operation.getString(OCKey.ID.value());
                        String type = operation.getString(OCKey.TYPE.value());
                        boolean ignore = false;
                        for (Entry<String, NodeHolder> e : headerFooterNodes.entrySet()){
                        	NodeHolder hf = e.getValue();
                        	if (hf instanceof HeaderFooter && StringUtils.equals(((HeaderFooter)hf).getType(), type)){
                        		ignore = true;
                        		break;
                        	}
                        }
                        if (!ignore) {
                        	headerFooterNodes.put(id, new HeaderFooter(id, type, attrs));
                        }
                        continue;
                    }

                    final String target = operation.optString(OCKey.TARGET.value(), null);
                    final NodeHolder documentNode;
                    if (target == null)
                    {
                        documentNode = doc;
                    }
                    else
                    {
                    	if (headerFooterNodes.get(target) != null) {
                    		documentNode = headerFooterNodes.get(target);
                    	} else if (commentNodes.get(target) != null) {
                    		documentNode = commentNodes.get(target);
                    	} else {
                    		continue;
                    	}
                    }

                    if (null == start)
                    {
                        throw new Exception("RT connection: no start Info found " + opName);
                    }

                    if (null == end)
                    {
                        end = start;
                    }

                    final INode insert;
                    if (opName.equals(OCValue.INSERT_TEXT.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String text = operation.getString(OCKey.TEXT.value());
                        end = new JSONArray(start);
                        end.put(end.length() - 1, (start.getInt(end.length() - 1) + text.length()) - 1);
                        insert = new Text(text, textIndex, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_HARD_BREAK.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String type = operation.optString(OCKey.TYPE.value());
                        insert = new HardBreak(textIndex, type, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_TAB.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new Tab(textIndex, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_BOOKMARK.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String id = operation.optString(OCKey.ID.value());
                        final String anchorName = operation.optString(OCKey.ANCHOR_NAME.value());
                        final String position = operation.optString(OCKey.POSITION.value());

                        insert = new Bookmark(textIndex, id, anchorName, position, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_PARAGRAPH.value())) {
                        insert = new Paragraph(attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_TABLE.value())) {
                        insert = new Table(textTableLimits, attrs, operation.optJSONObject(OCKey.SIZE_EXCEEDED.value()));
                    }
                    else if (opName.equals(OCValue.INSERT_DRAWING.value())) {
                        insert = new Drawing(operation.optString(OCKey.TYPE.value(), null), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_ROWS.value())) {
                        insert = new Row(operation.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_CELLS.value())) {
                        insert = new Cell(attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_FIELD.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new SimpleField(textIndex, operation.optString(OCKey.REPRESENTATION.value(), ""), operation.optString(OCKey.TYPE.value(), null), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_COMPLEX_FIELD.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new ComplexField(textIndex, complexFieldIdStack.isEmpty() ? "" : complexFieldIdStack.peek(), operation.optString(OCKey.INSTRUCTION.value(), ""), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_COMMENT.value())) {
                        String id = operation.getString(OCKey.ID.value());
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new CommentPlaceHolder(textIndex, id, operation.optString(OCKey.TARGET.value()) ,operation.optString(OCKey.AUTHOR.value()), operation.optString(OCKey.USER_ID.value()), operation.optString(OCKey.PROVIDER_ID.value()), operation.optString(OCKey.AUTHOR_ID.value()), operation.optString(OCKey.DATE.value()), operation.optString(OCKey.PARENT_ID.value()), operation.optString(OCKey.TEXT.value()), operation.optString(OCKey.INITIALS.value()), operation.optBoolean(OCKey.RESTORABLE.value(), true), operation.optJSONArray(OCKey.MENTIONS.value()), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_RANGE.value()))
                    {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String type = operation.optString(OCKey.TYPE.value());
                        final String pos = operation.optString(OCKey.POSITION.value());

                        String id = null;
                        if(type.equals("field")) {
                        	if(pos.equals("start")) {
                        		id = "fld" + Integer.toString(nextComplexFieldId++);
                        		complexFieldIdStack.push(id);
                        	}
                        	else {
                        		id = complexFieldIdStack.pop();
                        	}
                        }
                        else {
                        	id = operation.optString(OCKey.ID.value());
                        }
                        insert = new RangeMarker(textIndex, id, type, pos, attrs);
                    }
                    else
                    {
                        LOG.warn(opName + " not implemented yet");
                        continue;
                    }
                    documentNode.insert(start, insert);
                }
                catch (final Exception e)
                {
                    throw new Exception("operation: " + operation, e);
                }
            }

            JSONObject res = new JSONObject();

            // now create the document attributes
            if(fv!=0) {
                res.put("fv", fv);
            }

            // now create the document html string
            final StringBuilder mainDocument = new StringBuilder();
            doc.appendContent(mainDocument);
            res.put("mainDocument", mainDocument);

            // handling header/footer string
            JSONObject headerFooter = new JSONObject();

            for (Entry<String, NodeHolder> e : headerFooterNodes.entrySet())
            {
                final StringBuilder document = new StringBuilder();

                e.getValue().appendContent(document);

                headerFooter.put(e.getKey(), document);
            }

            res.put("headerFooter", headerFooter);

            // handling comments string
            JSONObject allComments = new JSONObject();

            for (Entry<String, NodeHolder> e : commentNodes.entrySet())
            {
                final StringBuilder document = new StringBuilder();

                e.getValue().appendContent(document);

                allComments.put(e.getKey(), document);
            }

            res.put("comments", allComments);

            // set result
            result = res.toString();

            for (int i = reducedOperations.length() - 1; i >= 0; i--) {
            	JSONObject op = reducedOperations.getJSONObject(i);
            	String target = op.optString(OCKey.TARGET.value(), null);

            	if (null != target && !headerFooterNodes.containsKey(target)) {
            		reducedOperations.remove(i);
            	}
			}

            documentOperations.put("operations", reducedOperations);

            LOG.debug("RT2: fastLoad optimizations used for loading document: " + ((null != folderId) ? folderId : "") + "." + ((null != fileId) ? fileId : ""));
            LOG.trace("RT2: TIME creating the generic html document took: " + (System.currentTimeMillis() - time) + "ms");
        }
        catch (final Exception e)
        {
            LOG.warn("RT2: Exception while creating the generic html document string from operations. docDetails: " + fileName + " fileId: " + fileId + " folderId: " + folderId + " fileVersion: " + fileVersion, e);
        }
        return result;
    }
}
