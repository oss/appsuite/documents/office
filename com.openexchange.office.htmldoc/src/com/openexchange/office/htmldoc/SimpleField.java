/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class SimpleField extends SubNode {

    private static final String BEFORECLASS = "<div contenteditable=\"false\" class=\"inline field";
    private static final String BEFORETYPE  = "\"";
    private static final String AFTERTYPE   = "><span ";
    private static final String AFTERATTRS  = " >";
    private static final String CLOSEFIELD  = "</span></div>";

    private final String        fieldText;
    private final String              type;

    // ///////////////////////////////////////////////////////////

    public SimpleField(int position, String representation, String type, JSONObject attrs) throws Exception {
        super(position, 1);

        this.fieldText = representation;
        this.type = type;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

	@Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(BEFORECLASS);

        JSONObject attrs = getAttribute();
        if (attrs == null)
        {
            attrs = new JSONObject();
        }

        JSONObject field = attrs.optJSONObject(OCKey.FIELD.value());
        JSONObject jq = GenDocHelper.getJQueryData(attrs);
        if (type != null)
        {
            jq.put(OCKey.TYPE.value(), type);
        }
		if (null != field) {
			for (Entry<String, Object> e : field.entrySet()) {
				jq.put(e.getKey(), e.getValue());
			}
		}

        if (type != null)
        {
            String type = this.type;
            // System.out.println("Field.appendContent() before: '" + type + "'");

            type = type.trim().split(" ")[0];
            // System.out.println("Field.appendContent() after : '" + type + "'");

            document.append(" field-");

            document.append(GenDocHelper.escapeUIString(type));
        }

        if (StringUtils.isEmpty(fieldText))
        {
            if (type != null)
            {
                document.append(" ");
            }
            document.append("empty-field");
        }

        document.append(BEFORETYPE);



        GenDocHelper.appendJQueryData(document, jq);

        document.append(AFTERTYPE);

        JSONObject jq2 = GenDocHelper.getJQueryData(getAttribute());
        GenDocHelper.appendJQueryData(document, jq2);

        document.append(AFTERATTRS);

        document.append(GenDocHelper.escapeUIString(fieldText));

        document.append(CLOSEFIELD);

        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return "Field: " + fieldText;
    }

}
