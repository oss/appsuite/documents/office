/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Paragraph implements INode, ISplitpoint {

    // private final static Logger LOG = LoggerFactory.getLogger(Paragraph.class);

    public final static String  EMPTYSPAN             = "<span nonjquerydata=\"{&quot;isempty&quot;:true}\"></span><br jquerydata=\"{&quot;dummy&quot;:true}\"/>";
    public final static String  PARASTART             = "<div class=\"p\" ";
    private static final String PARASTARTWITHOUTCLASS = "<div class=\"p";
    private final static String DEFPARAATTRS          = "text-align: left; border: none; padding: 0px 0px 0px 0mm; margin: 0mm 0mm 3.52mm; text-indent: 0mm; background-color: transparent;";
    private final static String DEFPARAATTRSINTABLE   = "text-align: left; border: none; padding: 0px 0px 0px 0mm; margin: 0mm 0mm 0mm; text-indent: 0mm; background-color: transparent;";

    // ///////////////////////////////////////////////////////////////////////////////////////////

    private final List<INode>   nodes                 = new ArrayList<INode>();
    private boolean             inTable               = false;
    private boolean             isImplicit            = false;
    private JSONObject          myAttrs               = null;
    private boolean             splitpoint;
    private boolean             manualPageBreak       = false;

    public Paragraph(JSONObject attrs) throws Exception {
        if(attrs!=null) {
            myAttrs = attrs;
        }
    }

    @Override
    public final boolean isSplitpoint() {
        return splitpoint;
    }

    @Override
    public final void setSplitpoint(boolean splitpoint) {
        this.splitpoint = splitpoint;
    }

    @Override
    public void insert(JSONArray start, INode newChild) throws Exception {
        if (start.length() == 1) {
            nodes.add(newChild);

            if (newChild instanceof SubNode) {
                if (((SubNode) newChild).hasManualPageBreak()) {
                    this.manualPageBreak = true;
                }
            }
        }
        else {
            int startTextIndex = start.getInt(0);

            int current = 0;

            for (final INode node : nodes) {
                if (current == startTextIndex) {
                    start = GenDocHelper.shiftedCopy(start);
                    node.insert(start, newChild);
                    return;
                }
                current += node.getTextLength();
            }
            throw new Exception("insert is not handled " + current + " " + newChild);
        }
    }

    protected void appendHTMLCLasses(StringBuilder document) throws Exception {
        if (splitpoint && manualPageBreak) {
            document.append(" splitpoint manual-page-break");
        }
        else if (manualPageBreak) {
            document.append(" manual-page-break");
        }
        else if (splitpoint) {
            document.append(" splitpoint");
        }
    }

    protected void appendHTMLAttributes(StringBuilder document) throws Exception {

    }

    protected void appendHTMLStyles(StringBuilder document) throws Exception {
        if (inTable) {
            document.append(DEFPARAATTRSINTABLE);
        }
        else {
            document.append(DEFPARAATTRS);
        }
    }

    @Override
    public final boolean appendContent(StringBuilder document) throws Exception {

        document.append(PARASTARTWITHOUTCLASS);

        appendHTMLCLasses(document);

        document.append("\"");

        appendHTMLAttributes(document);

        GenDocHelper.appendAttributes(myAttrs, document, isImplicit);

        document.append(" style=\"");
        appendHTMLStyles(document);
        document.append("\">");

        int len = getTextLength();

        if (len > 0) {

            // now iterate through the paragraph nodes and create the node
            // specific html parts
            boolean addEmptySpan = true;
            boolean contentAdded = false;
            boolean firstNode = true;

            for (final INode node : nodes) {
                if (!firstNode && !contentAdded && node.needsEmptySpan()) {
                    // In case our previous node didn't add content and the
                    // current node MUST have a previous node, we have to force
                    // the creation now setting addEmptySpan to true.
                    // e.g. happens in the case: <drawing node><tab node> where
                    // the drawing node doesn't add an empty span and is inserted
                    // later via operations where the code assumes that there is
                    // an empty text node before the tab!
                    addEmptySpan = true;
                }
                if (addEmptySpan && node.needsEmptySpan()) {
                    addEmptySpan(document, node.getAttribute());
                }
                addEmptySpan = node.needsEmptySpan();

                contentAdded |= node.appendContent(document);
                firstNode = false;
            }
            if (addEmptySpan || !contentAdded) {
                addEmptySpan(document, nodes.isEmpty() ? null : nodes.get(nodes.size() - 1).getAttribute()); // if possible we use the attributes of the last node
            }
        }
        else {
            document.append(EMPTYSPAN);
        }
        document.append("</div>");
        return true;
    }

    protected void addEmptySpan(StringBuilder document, JSONObject attrs) throws JSONException {
        GenDocHelper.addEmptySpan(document, attrs, true);
    }

    @Override
    public String toString() {
        return "Paragraph " + nodes;
    }

    public final void setInTable(boolean inTable) {
        this.inTable = inTable;
    }

    public final void setImplicit(boolean isImplicit) {
        this.isImplicit = isImplicit;
    }

    @Override
    public final int getTextLength() {
        int res = 0;
        for (INode sn : nodes) {
            res += sn.getTextLength();
        }
        return res;
    }

    @Override
    public final boolean needsEmptySpan() {
        return false;
    }

    @Override
    public final JSONObject getAttribute() {
        return myAttrs;
    }
}
