/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class Table extends NodeHolder {

    private final static String   TABLE_SIZEEXCEEDED_PLACEHOLDER_START       = "<table role=\"grid\" class=\"size-exceeded\" contenteditable=\"false\" size-exceed-values=";
    private final static String   TABLE_SIZEEXCEEDED_PLACEHOLDER_END         = "><colgroup></table>";
    private final static String   TABLE_SIZEEXCEEDED_PLACEHOLDER_SPLIT_START = "<table role=\"grid\" class=\"size-exceeded splitpoint\" contenteditable=\"false\" size-exceed-values=";

    private final TextTableLimits tableLimits;
    private final JSONArray       tableGrid;
    private final int             allW;
    private int                   cellCount                                  = 0;
    private int                   rowCount                                   = 0;
    private int                   columnCount                                = 0;
    private final boolean               splitpoint                                 = false;
    private final JSONObject            sizeExceeded;

    public Table(final TextTableLimits textTableLimits, JSONObject attrs, JSONObject sizeExceeded) throws Exception {
        super(attrs);

        this.sizeExceeded = sizeExceeded;

        final JSONObject table = attrs.getJSONObject(OCKey.TABLE.value());

        this.tableGrid = table.getJSONArray(OCKey.TABLE_GRID.value());
        this.tableLimits = textTableLimits;

        int allW = 0;
        for (int i = 0; i < tableGrid.length(); i++)
        {
            allW += tableGrid.getInt(i);
        }
        this.allW = allW;
        columnCount = tableGrid.length();
    }

    @Override
    public void insert(
        JSONArray start,
        INode newChild)
        throws Exception
    {

        if (start.length() == 1 && newChild instanceof Row)
        {
            ((Row) newChild).setCellCount(tableGrid.length());
            rowCount++;
            cellCount += columnCount;
        }
        else if (newChild instanceof Paragraph)
        {
            ((Paragraph) newChild).setInTable(true);
        }

        super.insert(start, newChild);
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        if (sizeExceeded == null || sizeExceeded.isEmpty())
        {
            document.append("<table");
            if (splitpoint)
            {
                document.append(" class=\"splitpoint\" ");
            }
            GenDocHelper.appendAttributes(getAttribute(), document);
            document.append("role='grid' style='width: 100%; background-color: transparent;'><colgroup>");
            for (int i = 0; i < tableGrid.length(); i++)
            {

                final double w = Math.round((tableGrid.getDouble(i) / allW) * 1000.0) / 10.0;

                document.append("<col style='width: " + w + "%;'>");
            }
            document.append("</colgroup><tbody>");

            super.appendContent(document);

            document.append("</tbody></table>");
        }
        else
        {
            if (splitpoint)
                document.append(TABLE_SIZEEXCEEDED_PLACEHOLDER_SPLIT_START);
            else
                document.append(TABLE_SIZEEXCEEDED_PLACEHOLDER_START);

            if (sizeExceeded.has(OCKey.ROWS.value()))
            {
                rowCount = sizeExceeded.getInt(OCKey.ROWS.value());
            }
            if (sizeExceeded.has(OCKey.COLUMNS.value()))
            {
                columnCount = sizeExceeded.getInt(OCKey.COLUMNS.value());
            }

        	cellCount = columnCount * rowCount;

            if (rowCount > tableLimits.maxTableRows)
            {
                document.append("\"rows," + rowCount + "," + tableLimits.maxTableRows + "\"");
            }
            else if (columnCount > tableLimits.maxTableColumns)
            {
                document.append("\"cols," + columnCount + "," + tableLimits.maxTableColumns + "\"");
            }
            else if (cellCount > tableLimits.maxTableCells)
            {
                document.append("\"cells," + cellCount + "," + tableLimits.maxTableCells + "\"");
            }
            document.append(TABLE_SIZEEXCEEDED_PLACEHOLDER_END);
        }
        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    public int getRowCount()
    {
        return rowCount;
    }
}
