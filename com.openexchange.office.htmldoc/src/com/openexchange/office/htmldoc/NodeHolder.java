/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class NodeHolder implements INode {

    private final List<INode> children = new ArrayList<INode>();
    private JSONObject        attrs;

    private static final Paragraph IMPLICITPARA;

    static {
        Paragraph paragraph;
        try {
            paragraph = new Paragraph(null);
            paragraph.setImplicit(true);
        }
        catch(Exception e) {
            paragraph = null;
        }
        IMPLICITPARA = paragraph;
    }

    public NodeHolder(JSONObject attrs) {
        this.attrs = attrs;
    }

    @Override
    public JSONObject getAttribute()
    {
        return attrs;
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        boolean appendedContent = false;
        if (children.size() > 0) {
	        for (final INode child : children) {
	            appendedContent |= child.appendContent(document);
	        }

	        // check type of last child and append implicit
	        // paragraph if necessary (behind table).
	        INode lastChild = children.get(children.size()-1);
        	if (lastChild instanceof Table) {
                IMPLICITPARA.appendContent(document);
        	}
        }
        return appendedContent;
    }

    @Override
    public void insert(
        JSONArray start,
        INode newChild)
        throws Exception
    {
        if (start.length() == 1)
        {
            if (newChild instanceof SubNode)
            {
                throw new Exception("subnode are only allowed under Pragraphs! myClass: " + getClass().getSimpleName() + " childClass: " + newChild.getClass().getSimpleName() + " " + start);
            }
            final int childIndex = start.getInt(0);
            children.add(childIndex, newChild);
        }
        else
        {
            final int childIndex = start.getInt(0);
            final INode child = children.get(childIndex);
            final JSONArray subStart = GenDocHelper.shiftedCopy(start);
            child.insert(subStart, newChild);
        }
    }

    protected int getChildrenCount()
    {
        return children.size();
    }

    protected List<INode> getChildren()
    {
        return children;
    }

    @Override
    public int getTextLength()
    {
        int res = 0;
        for (INode sn : children)
        {
            res += sn.getTextLength();
        }
        return res;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName() + " " + children;
    }

}
