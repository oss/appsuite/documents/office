/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

public class PresentationHtmlDocumentBuilder
{
    private final static Logger      LOG       = LoggerFactory.getLogger(PresentationHtmlDocumentBuilder.class);
    private final static Set<String> IGNOREOPS = new HashSet<String>();

    static
    {
        IGNOREOPS.add(OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        IGNOREOPS.add(OCValue.INSERT_FONT_DESCRIPTION.value());
        IGNOREOPS.add(OCValue.INSERT_STYLE_SHEET.value());
        IGNOREOPS.add(OCValue.INSERT_THEME.value());
        IGNOREOPS.add(OCValue.INSERT_LIST_STYLE.value());
        IGNOREOPS.add(OCValue.INSERT_COMMENT.value());
    }

    static public
            Logger
            getLogger()
    {
        return LOG;
    }

    static public
            String
            buildHtmlDocument(
                JSONObject documentOperations,
                DocMetaData docMetaData,
                TextTableLimits tableLimits)
    {

        return buildHtmlDocument(documentOperations, docMetaData.getId(), docMetaData.getFolderId(), docMetaData.getVersionOrAttachment(), docMetaData.getFileName(), docMetaData.getSource(), tableLimits);
    }

    static private
            String
            buildHtmlDocument(
                JSONObject documentOperations,
                String fileId,
                String folderId,
                String fileVersion,
                String fileName,
                String source,
                TextTableLimits textTableLimits)
    {
        String result = null;

        Comparator<Entry<String, Container>> slideSorter = new Comparator<Entry<String, Container>>()
        {
            @Override
            public
                    int
                    compare(
                        Entry<String, Container> o1,
                        Entry<String, Container> o2)
            {
                return Integer.compare(o1.getValue().getIndex(), o2.getValue().getIndex());
            }
        };

        try
        {

            int fv = 0;
            int layoutSlideIndex = 0;
            int masterSlideIndex = 0;
            Map<String, Container> layoutSlides = new HashMap<String, Container>();
            Map<String, Container> masterSlides = new HashMap<String, Container>();
            Container pagecontent = new Container(-1, "pagecontent", null, false, false, null);

            final long time = System.currentTimeMillis();
            final JSONArray operations = documentOperations.getJSONArray("operations");
            final JSONArray reducedOperations = new JSONArray();

            for (int i = 0; i < operations.length(); i++)
            {
                JSONObject operation = operations.getJSONObject(i);
                try
                {
                    final String opName = operation.getString(OCKey.NAME.value());
                    if (IGNOREOPS.contains(opName))
                    {
                        reducedOperations.put(operation);
                        if(fv == 0 && opName.equals(OCValue.SET_DOCUMENT_ATTRIBUTES.value())) {
                            fv = GenericHtmlDocumentBuilder.getFvFromSetDocumentOperation(operation);
                        }
                        continue;
                    }

                    JSONArray start = operation.optJSONArray(OCKey.START.value());
                    JSONArray end = operation.optJSONArray(OCKey.END.value());
                    JSONObject attrs = operation.optJSONObject(OCKey.ATTRS.value());
                    String id = operation.optString(OCKey.ID.value());
                    String target = operation.optString(OCKey.TARGET.value());

                    INode insert = null;

                    if (OCValue.INSERT_MASTER_SLIDE.value().equals(opName)) {
                        masterSlides.put(id, new Container(masterSlideIndex, id, target, true, true, attrs));
                        masterSlideIndex++;
                    }
                    else if (OCValue.INSERT_LAYOUT_SLIDE.value().equals(opName)) {
                        layoutSlides.put(id, new Container(layoutSlideIndex, id, target, true, true, attrs));
                        layoutSlideIndex++;
                    }
                    else if (OCValue.INSERT_SLIDE.value().equals(opName)) {
                        long slideId = operation.getLong(OCKey.START.value());
                        insert = new SlideParagraph("slide_" + (slideId + 1), target, attrs == null ? new JSONObject() : attrs);

                        target = null;
                        start = new JSONArray().put(slideId);
                    }
                    else if (OCValue.INSERT_DRAWING.value().equals(opName)) {
                        insert = new Drawing(operation.optString(OCKey.TYPE.value(), null), attrs).setPresentation(true).setTarget(target);

                        // this setAttributes is somehow necessary for placeholder formattings
                        if (attrs != null) {
                            JSONObject newOp = new JSONObject();
                            newOp.put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());
                            newOp.put(OCKey.START.value(), start);
                            newOp.put(OCKey.ATTRS.value(), attrs);
                            newOp.put(OCKey.TARGET.value(), target);
                            reducedOperations.put(newOp);
                        }
                    }
                    else if (OCValue.INSERT_ROWS.value().equals(opName)) {
                        insert = new Row(operation.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), attrs);
                    }
                    else if (OCValue.INSERT_CELLS.value().equals(opName)) {
                        insert = new Cell(attrs);
                    }
                    else if (OCValue.INSERT_PARAGRAPH.value().equals(opName)) {
                        insert = new Paragraph(attrs);
                    }
                    else if (OCValue.INSERT_TEXT.value().equals(opName)) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String text = operation.getString(OCKey.TEXT.value());
                        insert = new Text(text, textIndex, attrs);
                    }
                    else if (OCValue.INSERT_FIELD.value().equals(opName)) {
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new SimpleField(textIndex, operation.optString(OCKey.REPRESENTATION.value(), ""), operation.optString(OCKey.TYPE.value(), null), attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_HARD_BREAK.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String type = operation.optString(OCKey.TYPE.value());
                        insert = new HardBreak(textIndex, type, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_TAB.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        insert = new Tab(textIndex, attrs);
                    }
                    else if (opName.equals(OCValue.INSERT_RANGE.value())) {
                        final int textIndex = start.getInt(start.length() - 1);
                        final String type = operation.optString(OCKey.TYPE.value());
                        final String pos = operation.optString(OCKey.POSITION.value());
                        insert = new RangeMarker(textIndex, operation.optString(OCKey.ID.value()), type, pos, attrs);
                    }

                    if (null == end)
                    {
                        end = start;
                    }

                    Container targetContainer = null;
                    if (!StringUtils.isEmpty(target))
                    {
                        targetContainer = masterSlides.get(target);

                        if (null == targetContainer)
                        {
                            targetContainer = layoutSlides.get(target);
                        }
                    }
                    else
                    {
                        targetContainer = pagecontent;
                    }

                    if (insert != null)
                    {
                        targetContainer.insert(start, insert);
                    }
                }
                catch (final Exception e)
                {
                    throw new Exception("operation: " + operation, e);
                }

            }

            final StringBuilder document = new StringBuilder();

            document.append("<div class='masterslidelayer'>");
            ArrayList<Entry<String, Container>> masterSorted = new ArrayList<Entry<String, Container>>(masterSlides.entrySet());
            Collections.sort(masterSorted, slideSorter);
            for (Entry<String, Container> e : masterSorted)
            {
                e.getValue().appendContent(document);
            }
            document.append("</div>");

            document.append("<div class='layoutslidelayer'>");
            ArrayList<Entry<String, Container>> layoutSorted = new ArrayList<Entry<String, Container>>(layoutSlides.entrySet());
            Collections.sort(layoutSorted, slideSorter);
            for (Entry<String, Container> e : layoutSorted)
            {
                e.getValue().appendContent(document);
            }
            document.append("</div>");

            pagecontent.appendContent(document);

            String content = document.toString();

            JSONObject res = new JSONObject();

            // now create the document attributes
            if(fv!=0) {
                res.put("fv", fv);
            }
            res.put("mainDocument", content);
            result = res.toString();

            documentOperations.put("operations", reducedOperations);

            LOG.debug("RT connection: fastLoad optimizations used for loading document: " + ((null != folderId) ? folderId : "") + "." + ((null != fileId) ? fileId : ""));
            LOG.trace("RT connection: TIME creating the generic html document took: " + (System.currentTimeMillis() - time) + "ms");
        }
        catch (

        final Exception e)

        {
            LOG.error("RT2: Exception while creating the generic html document string from operations. docDetails: " + fileName + " fileId: " + fileId + " folderId: " + folderId + " fileVersion: " + fileVersion, e);
        }
        return result;
    }

}
