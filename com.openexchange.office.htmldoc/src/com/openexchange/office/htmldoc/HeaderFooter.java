/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class HeaderFooter extends NodeHolder {

    private final String type;
    private final String id;

    public HeaderFooter(String id, String type, JSONObject attrs) throws Exception {
        super(attrs);

        this.type = type;
        this.id = id;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {

        document.append("<div class='inactive-selection " + type.split("_")[0] + " " + GenDocHelper.escapeUIString(id) + " " + GenDocHelper.escapeUIString(type) + "'");

        final JSONObject adapter = GenDocHelper.getJQueryData(getAttribute(), false);
        adapter.put(OCKey.ID.value(), id);
        adapter.put(OCKey.TYPE.value(), type);
        GenDocHelper.appendJQueryData(document, adapter);

        document.append(">");

        super.appendContent(document);

        document.append("</div>");

        return true;
    }

	public String getType() {
		return type;
	}



}
