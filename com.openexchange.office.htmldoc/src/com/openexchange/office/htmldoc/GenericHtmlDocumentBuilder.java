/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.doc.ApplicationType;
import com.openexchange.office.tools.service.config.UserConfigurationFactory;
import com.openexchange.office.tools.service.config.UserConfigurationHelper;
import com.openexchange.office.tools.service.config.UserConfigurationHelper.Mode;
import com.openexchange.session.Session;

@Service
@RegisteredService
public class GenericHtmlDocumentBuilder
{
    private final static Logger LOG = LoggerFactory.getLogger(GenericHtmlDocumentBuilder.class);
    
    @Autowired
    private UserConfigurationFactory userConfigFactory;

    @Autowired
    private ConfigurationService configService;
    
    public String buildHtmlDocument(String module, JSONObject documentOperations, DocMetaData docMetaData, Session session) {
        if (StringUtils.endsWith(docMetaData.getFileName(), "_ox")) {
            // TODO: is this check good enough
            LOG.debug("RT connection: Fastload is not running for broken files!!!. docDetails: {}, id: {}, source: {} ", docMetaData.getFileName(), docMetaData.getId(), docMetaData.getSource());
            return null;
        }

        if ( !documentOperations.hasAndNotNull("operations")) {
            LOG.info("RT connection: Fastload for module {} could not run with missing operations. docDetails: {}, id: {}, source: {}", docMetaData.getFileName(), docMetaData.getId(), docMetaData.getSource());
            return null;
        }

        final TextTableLimits tableLimits = ConfHelper.getTextTableLimits(getUserHelper(session));

        if (module.equalsIgnoreCase("text")) {
            return TextHtmlDocumentBuilder.buildHtmlDocument(documentOperations, docMetaData, tableLimits);
        }
        return PresentationHtmlDocumentBuilder.buildHtmlDocument(documentOperations, docMetaData, tableLimits);
    }

    private UserConfigurationHelper getUserHelper(Session session) {
        return userConfigFactory.create(session, "io.ox/office", Mode.WRITE_BACK);
    }

    public boolean isFastLoadActive(String module, Session session) {
        if (configService.getBoolProperty("io.ox/office//module/debugavailable", false)) {
            return userConfigFactory.create(session, "io.ox/office", Mode.WRITE_BACK).getBoolean(module + "/useFastLoad", true);
        }
        return true;
    }

    public static boolean isFastLoadSupported(String module)
    {
        if (ApplicationType.APP_PRESENTATION_STRING.equalsIgnoreCase(module))
            return true;
        else if (ApplicationType.APP_TEXT_STRING.equalsIgnoreCase(module))
            return true;
        return false;
    }

    /*
     * returns filterVersion from given SetDocumentOperation or 0 
     */
    public static int getFvFromSetDocumentOperation(JSONObject op) {
        final JSONObject attrs = op.optJSONObject(OCKey.ATTRS.value());
        final JSONObject documentAttrs = attrs != null ? attrs.optJSONObject(OCKey.DOCUMENT.value()) : null;
        return documentAttrs != null ? documentAttrs.optInt(OCKey.FV.value()) : 0;
    }
}
