/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class Bookmark extends SubNode {

    private final String bmId;
    private final String anchor;
    private final String bmPos;

    public Bookmark(int subPos, String id, String anchorName, String position, JSONObject attrs) throws Exception {
        super(subPos, 1);

        this.bmId = id;
        this.anchor = anchorName;
        this.bmPos = position;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(StringBuilder document) throws Exception {
        document.append("<div class=\"bookmark inline\" ");

        String anchorStr = GenDocHelper.escapeUIString(this.anchor);
        String bmIdStr = GenDocHelper.escapeUIString(this.bmId);
        String bmPosStr = GenDocHelper.escapeUIString(this.bmPos);

        document.append("anchor=\"" + anchorStr + "\" ");
        document.append("bmId=\"" + bmIdStr + "\" ");
        document.append("bmPos=\"" + bmPosStr + "\" ");

        GenDocHelper.appendAttributes(getAttribute(), document);

        document.append(" ></div>");
        return true;
    }

    @Override
    public boolean needsEmptySpan() {
        return true;
    }

}
