/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class HardBreak extends SubNode {

    private String textContent           = "<br/>";
    private String type                  = "textWrapping";
    private String before                = "";
    private String after                 = "";
    private String hardBreakSpanTypePage = "";

    // ///////////////////////////////////////////////////////////

    public HardBreak(int position, String type, JSONObject attrs) throws Exception {
        super(position, 1);
        if ((type != null) && (!type.isEmpty()))
        {
            this.type = type;
        }
        if (this.type.equalsIgnoreCase("page") || this.type.equalsIgnoreCase("column"))
        {
            this.textContent = " ";
            this.hardBreakSpanTypePage = "class='ms-hardbreak-page'";
        }
        this.before = "<div contenteditable='false' class='inline hardbreak' jquerydata=\"{&quot;type&quot;:&quot;" + this.type + "&quot;}\"><span ";
        this.after = " " + this.hardBreakSpanTypePage + ">" + this.textContent + "</span></div>";

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(this.before);
        GenDocHelper.appendAttributes(getAttribute(), document);
        document.append(this.after);
        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }

    @Override
    public boolean hasManualPageBreak()
    {
        if (this.type.equalsIgnoreCase("page"))
        {
            return true;
        }
        return super.hasManualPageBreak();
    }

    public String getType()
    {
        return type;
    }
}
