/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class ComplexField extends SubNode {

    private static final String FIELDSTART = "<div contenteditable=\"false\" class=\"inline complexfield\" ";
    private static final String FIELDCLOSER = "\" ";
    private static final String FIELDID = "data-field-id=\"";
    private static final String FIELDINSTRUCTION = "data-field-instruction=\"";
    private static final String FIELDEND = "></div>";

    private final String              id;
    private final String              instruction;

    // ///////////////////////////////////////////////////////////

    public ComplexField(int logicalPosition, String id, String instruction, JSONObject attrs) throws Exception {
        super(logicalPosition, 1);

        this.instruction = instruction;
        this.id = id;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(FIELDSTART);
        document.append(FIELDID + GenDocHelper.escapeUIString(this.id) + FIELDCLOSER);
        document.append(FIELDINSTRUCTION + GenDocHelper.escapeUIString(this.instruction) + FIELDCLOSER);

        JSONObject attrs = getAttribute();
        if (attrs == null)
        {
            attrs = new JSONObject();
        }

        JSONObject jq = GenDocHelper.getJQueryData(attrs);
    	jq.put("fieldId", id);
        jq.put("fieldInstructions", instruction);

        GenDocHelper.appendJQueryData(document, jq);

        document.append(FIELDEND);

        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return "ComplexField";
    }

}
