/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GenDocHelper {

    private static final String SPAN_BEFORE = "<span ";
    private static final String SPAN_NON_JQUERY_DATA = "nonjquerydata=\"{&quot;isempty&quot;:true}\" ";
    private static final String SPAN_MIDDLE = "style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-style: normal; text-decoration: none; color: rgb(0, 0, 0); font-size: 11pt; line-height: 20px; background-color: transparent;\">";
    private static final String SPAN_END    = "</span>";

    static JSONArray shiftedCopy(JSONArray input) {
        final List<Object> list = input.asList();
        list.remove(0);
        return new JSONArray(list);
    }

    public static void appendAttributes(JSONObject attrs, StringBuilder document) throws JSONException {
        appendAttributes(attrs, document, false);
    }

    public static JSONObject getJQueryData(JSONObject attrs) throws JSONException {
        return getJQueryData(attrs, false);
    }

    public static void appendAttributes(JSONObject attrs, StringBuilder document, boolean implicit) throws JSONException {
        final JSONObject adapter = getJQueryData(attrs, implicit);
        appendJQueryData(document, adapter);
    }

    public static void appendJQueryData(StringBuilder document, JSONObject jquerydata) {
        if (jquerydata.length() > 0) {
            document.append(" jquerydata='");
            String str = escapeUIString(jquerydata.toString());
            document.append(str);
            document.append("' ");
        }
    }

    public static final String escapeUIString(final String text) {
        String str = StringEscapeUtils.escapeHtml4(text);
        str = str.replaceAll("'", "&#39;"); // escaping all single quotes
        return str;
    }

    public static JSONObject getJQueryData(JSONObject attrs, boolean implicit) throws JSONException {
        final JSONObject adapter = new JSONObject();
        if (attrs != null) {
            adapter.put("attributes", attrs);
        }
        if (implicit) {
            adapter.put("implicit", true);
        }
        return adapter;
    }

    public static void addEmptySpan(StringBuilder document, JSONObject attrs, boolean style) throws JSONException {
        if(!StringUtils.endsWith(document, "</span>")) { // check, if the document already ends with such an empty span (55174)
            addSpan(document, attrs, style, null);
        }
    }

    public static void addSpan(StringBuilder document, JSONObject attrs, boolean style, String text) throws JSONException {
        document.append(SPAN_BEFORE);
        if(text==null || text.isEmpty()) {
            document.append(SPAN_NON_JQUERY_DATA);
        }
        if(attrs != null && !attrs.isEmpty()) {
            GenDocHelper.appendAttributes(attrs, document);
        }
        if(style) {
            document.append(SPAN_MIDDLE);
        } else {
            document.append(">");
        }
        if(text!=null) {
            document.append(GenDocHelper.escapeUIString(text));
        }
        document.append(SPAN_END);
    }

    private static final Map<String, Float> FACTORS;

    static {
        FACTORS = new HashMap<String, Float>();
        FACTORS.put("px", 1F);
        FACTORS.put("pc", 1F / 9F);
        FACTORS.put("pt", 4F / 3F);
        FACTORS.put("in", 96F);
        FACTORS.put("cm", 96F / 2.54F);
        FACTORS.put("mm", 96F / 25.4F);
    }

    public static float convertLength(float value, String fromUnit, String toUnit) {
        value *= FACTORS.get(fromUnit) / FACTORS.get(toUnit);
        return value;
    }

    public static float convertHmmToLength(float value, String toUnit) {
        return convertLength(value / 100, "mm", toUnit);
    };

    public static float convertHmmToPx(float value) {
        return convertHmmToLength(value, "px");
    };
}
