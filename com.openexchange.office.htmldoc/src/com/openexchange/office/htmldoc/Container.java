/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class Container extends NodeHolder {

    private final String id;
    private final String target;
    private final int index;

    public Container(int index, String id, String target, boolean invisible, boolean notSelectable, JSONObject attrs) throws Exception {
        super(null);

        this.index = index;
        this.id = id;
        this.target = target;

        if (!"pagecontent".equals(id))
        {
            this.insert(new JSONArray().put(0), new SlideParagraph(id, target, attrs));
        }
    }

    public int getIndex()
    {
        return index;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
            throws Exception
    {

        if ("pagecontent".equals(id))
        {
            document.append("<div class='pagecontent'");
        }
        else
        {
            document.append("<div class='slidecontainer' data-container-id='" + GenDocHelper.escapeUIString(id) + "'");

            final JSONObject adapter = GenDocHelper.getJQueryData(getAttribute(), false);
            adapter.put(OCKey.ID.value(), id);

            if (target != null)
            {
                adapter.put(OCKey.TARGET.value(), target);
            }
            GenDocHelper.appendJQueryData(document, adapter);
        }

        document.append(">");

        super.appendContent(document);

        document.append("</div>");

        return true;
    }

}
