/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import com.openexchange.file.storage.File;

public class DocMetaData {

    private final String folder_id;
    private final String id;
    private final String versionOrAttachment;
    private final long fileSize;
    private final String fileName;
    private String mimeType;
    private String source = "drive";

    public DocMetaData(File fileMetaData) {
        this.folder_id = fileMetaData.getFolderId();
        this.id = fileMetaData.getId();
        this.versionOrAttachment = fileMetaData.getVersion();
        this.fileSize = fileMetaData.getFileSize();
        this.fileName = fileMetaData.getFileName();
        this.mimeType = fileMetaData.getFileMIMEType();
        this.source = "drive";
    }

    public DocMetaData(String folderId, String id, String versionOrAttachment, long fileSize, String fileName, String source) {
        this.folder_id = folderId;
        this.id = id;
        this.versionOrAttachment = versionOrAttachment;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.mimeType = "";
        this.source = source;
    }

    public DocMetaData(String folderId, String id, String versionOrAttachment, long fileSize, String fileName, String source, String mimeType) {
        this(folderId, id, versionOrAttachment, fileSize, fileName, source);
        this.mimeType = mimeType;
    }

    public String getFolderId() {
        return folder_id;
    }

    public String getId() {
        return id;
    }

    public String getVersionOrAttachment() {
        return versionOrAttachment;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getSource() {
        return source;
    }

}
