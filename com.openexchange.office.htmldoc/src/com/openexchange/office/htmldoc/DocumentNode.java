/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentNode extends NodeHolder {

    private final static Logger LOG                = LoggerFactory.getLogger(DocumentNode.class);
    private final static String IMPLICIT_PARAGRAPH = Paragraph.PARASTART + "jquerydata=\"{&quot;implicit&quot;:true}\" >" + Paragraph.EMPTYSPAN + "</div>";

    public DocumentNode() {
        super(null);
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {

        List<INode> children = getChildren();
        final int count = getChildrenCount();
        boolean mustSetSplitpoint = false;

        int paraCount = 0;
        int textLength = 0;
        int i = 0;
        for (; i < count; i++)
        {
            INode child = children.get(i);
            textLength += child.getTextLength();

            // Bug 35671
            // Formatting of tables is done asynchronously, therefore we
            // don't want to have a splitpoint at all, if we find a table.
            if (child instanceof Table) {
                break;
            }

            paraCount += determineParagraphCount(child);
            if ((textLength > 2000) && (paraCount > 25))
                mustSetSplitpoint = true;

            if (mustSetSplitpoint && (child instanceof ISplitpoint))
            {
                ((ISplitpoint) child).setSplitpoint(true);
                LOG.debug("set splitpoint on " + i + " of " + count + " elements");
                break;
            }

            child.appendContent(document);
        }

        for (; i < count; i++)
        {
            INode child = children.get(i);
            child.appendContent(document);
        }

        if (count == 0 || getChildren().get(count - 1) instanceof Table)
        {
            // implicit Paragraph
            document.append(IMPLICIT_PARAGRAPH);
        }

        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }

    private int determineParagraphCount(
        final INode node)
    {
        int result = 0;

        if (node instanceof Paragraph)
            return 1;
        else if (node instanceof Table)
        {
            final Table table = (Table) node;
            return table.getRowCount();
        }

        return result;
    }
}
