/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class Drawing extends NodeHolder {

    private static final String  CONTENT_START       = "<div contenteditable='false' ";
    private static final String  START               = " class='drawing inline' contenteditable='false' style='width: 50px; height: 50px; border: none;'>";
    private static final String  DRAWING_CONTENT     = "<div class='content'>";
    private static final String  PLACEHOLDER_CONTENT = "<div class='content placeholder'>";
    private static final String  GROUP_CONTENT       = "<div class='content groupcontent'>";
    private static final String  SHAPE_CONTENT       = "<div class='content textframecontent'><div class='textframe' contenteditable='true' data-gramm='false'>";
    private static final String  DIV_END             = "</div>";

    // ///////////////////////////////////////////////////////////

    private static AtomicInteger idCounter           = new AtomicInteger();

    // ///////////////////////////////////////////////////////////

    private final String         id;
    private final String         type;

    private boolean              emptySpans          = true;
    private boolean              emptySpanStyle      = true;

    private boolean              presentation        = false;
    private String target;

    public Drawing(String type, JSONObject attrs) throws Exception {
        super(attrs);

        this.type = type;
        this.id = "frame" + idCounter.getAndIncrement();
    }

    public Drawing setPresentation(
        boolean presentation)
    {
        this.presentation = presentation;

        return this;
    }

    @Override
    public boolean appendContent(StringBuilder document) throws Exception {

        if (emptySpans) {
            GenDocHelper.addEmptySpan(document, null, emptySpanStyle);
        }

        document.append(CONTENT_START);

        final JSONObject adapter = new JSONObject();

        adapter.put(OCKey.TYPE.value(), this.type);
        adapter.put(OCKey.USER_ID.value(), id);
        adapter.put("model", JSONObject.NULL);

        adapter.put("attributes", getAttribute());

        GenDocHelper.appendJQueryData(document, adapter);

        document.append(" data-type='");
        document.append(GenDocHelper.escapeUIString(type));
        document.append("' ");

        // System.out.println("Drawing.appendContent() " + this.type);

        String content = START;

        if (presentation) {
            content = "";
            if (null != target) {
                content += " target='" + GenDocHelper.escapeUIString(target) + "'";
            }

            content += "contenteditable='false' class='drawing absolute'>";
        }

        document.append(content);
        // System.out.println("Drawing.appendContent() " + content);
        if (type.equalsIgnoreCase("image")) {
            document.append(DRAWING_CONTENT);
        }
        else if (type.equalsIgnoreCase("group")) {
            document.append(GROUP_CONTENT);
            super.appendContent(document);
        }
        else if (type.equalsIgnoreCase("shape") || type.equalsIgnoreCase("connector")) {
            document.append(SHAPE_CONTENT);
            super.appendContent(document);
            document.append(DIV_END);
        }
        else if (type.equalsIgnoreCase("table")) {
            document.append(SHAPE_CONTENT);

        	// Handling for the table node

            document.append("<table ");
//             GenDocHelper.appendAttributes(getAttribute(), document);
            document.append("role='grid' class='isdrawingtablenode' style='width: 100%; background-color: transparent;'><colgroup>");

            // TODO: Table formatting already on server side?
//            for (int i = 0; i < tableGrid.length(); i++)
//            {
//                final double w = Math.round((tableGrid.getDouble(i) / allW) * 1000.0) / 10.0;
//                document.append("<col style='width: " + w + "%;'>");
//            }
            document.append("</colgroup><tbody>");

            super.appendContent(document);

            document.append("</tbody></table>");

            document.append(DIV_END);
        }
        else {
            // default behavior for types we cant handle yet (chart)
            document.append(PLACEHOLDER_CONTENT);
            super.appendContent(document);
        }

        document.append(DIV_END);
        document.append(DIV_END);

        if (!presentation && emptySpans) {
            GenDocHelper.addEmptySpan(document, null, emptySpanStyle);
        }
        return false;
    }

    public String getPHType() {
        JSONObject attrs = getAttribute();
        if (null == attrs) {
            return null;
        }

        JSONObject pres = attrs.optJSONObject(OCKey.PRESENTATION.value());
        if (null == pres) {
            return null;
        }
        String phType = pres.optString(OCKey.PH_TYPE.value());
        if (StringUtils.isEmpty(phType)) {
            return null;
        }
        return phType;
    }

    @Override
    public boolean needsEmptySpan() {
        return false;
    }

    @Override
    public int getTextLength() {
        return 1;
    }

    @Override
    public void insert(JSONArray start, INode newChild) throws Exception {
        // System.out.println("Drawing.insert() " + start + " " + newChild.getClass());
        if (start.length() == 1) {
            if (newChild instanceof Drawing) {
                ((Drawing) newChild).disableEmptySpans();
            }
        }
        super.insert(start, newChild);
    }

    public void disableEmptySpans() {
        this.emptySpans = false;
    }

    public void disableEmptySpanStyle() {
        this.emptySpanStyle = false;
    }

    public Drawing setTarget(String target) {
        this.target = target;
        return this;
    }
}
