/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class Tab extends SubNode {

    private static final String BEFOREATTRS = "<div contenteditable=\"false\" class=\"inline tab\"><span ";
    private static final String AFTERATTRS  = " >&nbsp</span></div>";

    // ///////////////////////////////////////////////////////////

    public Tab(int position, JSONObject attrs) throws Exception {
        super(position, 1);

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(BEFOREATTRS);

        GenDocHelper.appendAttributes(getAttribute(), document);

        document.append(AFTERATTRS);
        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }
}
