/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class SubNode implements INode {

    private final int  position;
    private final int  size;
    private JSONObject attrs;

    public SubNode(int position, int size) {
        this.position = position;
        this.size = size;
    }

    public int getParagraphPos()
    {
        return position;
    }

    @Override
    public int getTextLength()
    {
        return size;
    }

    @Override
    public JSONObject getAttribute()
    {
        return attrs;
    }

    public void setAttribute(JSONObject attrs) throws Exception {
        this.attrs = attrs;
    }

    @Override
    public void insert(
        JSONArray start,
        INode node)
        throws Exception
    {
        throw new UnsupportedOperationException("Subnode from class:" + this.getClass().getSimpleName() + " does not support insert! " + start + " " + node);

    }

    @Override
    public boolean needsEmptySpan()
    {
        throw new UnsupportedOperationException("Subnode from class:" + this.getClass().getSimpleName() + " does not support needsEmptySpan!");
    }

    public boolean hasManualPageBreak()
    {
        return false;
    }

}
