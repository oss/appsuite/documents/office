/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import org.json.JSONObject;

public class RangeMarker extends SubNode {

    private static final String RANGEMARKERSTART = "<div contenteditable=\"false\" class=\"inline rangemarker ";
    private static final String RANGEMARKERSTARTCLASS = "rangestart";
    private static final String RANGEMARKERENDCLASS = "rangeend";
    private static final String RANGEMARKERCLOSER = "\" ";
    private static final String RANGEMARKERTYPE = "data-range-type=\"";
    private static final String RANGEMARKERID = "data-range-id=\"";
    private static final String RANGEMARKEREND = "></div>";

    private String              id;
    private String              type;
    private String              position;
    private JSONObject          attrs		 = null;

    // ///////////////////////////////////////////////////////////

    public RangeMarker(int logicalPosition, String id, String type, String position, JSONObject attrs) throws Exception {
        super(logicalPosition, 1);

        this.id = id;
        this.type = type;
        this.position = position;

        if(attrs!=null) {
            setAttribute(attrs);
        }
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {
        document.append(RANGEMARKERSTART);

        if (this.position.equalsIgnoreCase("start"))
        {
            document.append(RANGEMARKERSTARTCLASS);
        }
        else if (this.position.equalsIgnoreCase("end"))
        {
            document.append(RANGEMARKERENDCLASS);
        }

        document.append(RANGEMARKERCLOSER);
        document.append(RANGEMARKERTYPE + GenDocHelper.escapeUIString(this.type) + RANGEMARKERCLOSER);
        document.append(RANGEMARKERID + GenDocHelper.escapeUIString(this.id) + RANGEMARKERCLOSER);


        JSONObject params = new JSONObject();
        if(id!=null) {
        	params.put("rangeId", id);
        }
        params.put("rangeType", type);

        attrs = getAttribute();
        if(attrs!=null) {
        	params.put("attributes", attrs);
        }

        GenDocHelper.appendJQueryData(document, params);

        document.append(RANGEMARKEREND);

        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return "RangeMarker";
    }

}
