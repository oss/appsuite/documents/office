/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.htmldoc;

import java.util.List;
import org.json.JSONObject;

public class Cell extends NodeHolder {

    private final static Paragraph EXPLICITPARA;

    static {
        Paragraph paragraph;
        try {
            paragraph = new Paragraph(null);
            paragraph.setInTable(true);
        }
        catch(Exception e) {
            paragraph = null;
        }
        EXPLICITPARA = paragraph;
    }

    public Cell(JSONObject attrs) throws Exception {
        super(attrs);
    }

    @Override
    public boolean appendContent(
        StringBuilder document)
        throws Exception
    {

        document.append("<td");
        GenDocHelper.appendAttributes(getAttribute(), document);
        document.append(" role='gridcell' colspan='1' style='border: 1px solid rgb(0, 0, 0); padding: 2px 7px; background-color: transparent;'>");
        document.append("<div class='cell' contenteditable='false'>");
        document.append("<div class='resize bottom'></div>");
        document.append("<div class='resize right'></div>");
        document.append("<div class='cellcontent' contenteditable='true' data-gramm='false' data-focus-role='cell'>");

        List<INode> children = getChildren();
        int childCount = children.size();

        if (childCount > 0)
        {
            // There must be a implicit paragraph at the end, if the content of
            // the table cell doesn't have any paragraph. So we have to check
            // our children. See #35498
        	boolean hasParagraph = false;

        	for (int i = 0; i < childCount; i++) {
        	    final INode child = children.get(i);

                if (child instanceof Paragraph) {
                    hasParagraph = true;
                }
            }
            super.appendContent(document);

            // if needed, create implicit paragraph
            if (!hasParagraph)
            {
            	EXPLICITPARA.appendContent(document);
            }
        }
        else
        {
        	EXPLICITPARA.appendContent(document);
        }

        document.append("</div>");
        document.append("</div>");
        document.append("</td>");
        return true;
    }

    @Override
    public boolean needsEmptySpan()
    {
        return false;
    }
}