/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.distributed;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.IStreamIDCollection;

/**
 * Wrapper class to store document dependent stream data associated
 * with a unique document restore id.
 *
 * @author Carsten Driesner
 * @since 7.8.1
 */
public class DocStreams implements Cloneable, IStreamIDCollection {
    private final DocRestoreID docRestoreID;
    private String docStreamID;
    private String opsStreamID;
    private long timeStamp;

    public DocStreams(final DocRestoreID docRestoreID) {
        this.docRestoreID = docRestoreID;
    }

    public DocStreams(final DocRestoreID docRestoreID, final String docStreamID, final String opsStreamID, long timeStamp) {
        this.docRestoreID = docRestoreID;
        this.docStreamID = docStreamID;
        this.opsStreamID = opsStreamID;
        this.timeStamp = timeStamp;
    }

    public DocRestoreID getDocRestoreID() {
        return this.docRestoreID;
    }

    public String getDocStreamID() {
        return this.docStreamID;
    }

    public String getOpsStreamID() {
        return this.opsStreamID;
    }

    public void setDocStreamID(final String docStreamID) {
        this.docStreamID = docStreamID;
    }

    public void setOpsStreamID(final String opsStreamID) {
        this.opsStreamID = opsStreamID;
    }

    @Override
    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void touch() {
        this.timeStamp = new Date().getTime();
    }

    @Override
    public void touch(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean contains(final String streamID) {
        if (null != this.docStreamID) {
            if (this.docStreamID.equals(streamID))
                return true;
            else if ((null != this.opsStreamID) && (this.opsStreamID.equals(streamID)))
                return true;
        }

        return false;
    }

    @Override
    public DocStreams clone() {
        return new DocStreams(this.docRestoreID, this.docStreamID, this.opsStreamID, this.timeStamp);
    }

    @Override
    public Set<String> getStreamIDs() {
        final HashSet<String> streamIDs = new HashSet<String>();

        if (StringUtils.isNotEmpty(docStreamID)) { streamIDs.add(docStreamID); }
        if (StringUtils.isNotEmpty(opsStreamID)) { streamIDs.add(opsStreamID); }

        return streamIDs;
    }

    @Override
    public DocRestoreID getUniqueCollectionID() {
        return docRestoreID;
    }

}

