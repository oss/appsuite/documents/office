/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.distributed;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFile;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.office.tools.common.IOHelper;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.IStreamIDCollection;
import com.openexchange.office.tools.monitoring.RestoreDocEvent;
import com.openexchange.office.tools.monitoring.RestoreDocEventType;
import com.openexchange.office.tools.monitoring.Statistics;

/**
 * Manages the document specific streams in a distributed managed file manager.
 * It refreshes the managed files to keep them alive.
 *
 * @author Carsten Driesner
 * @since 7.8.1
 */
@Service
public class DistributedDocumentStreamManager implements IDistributedManageFileCollection, InitializingBean {

    private static final String DOC_STREAM_PREFIX = "office:doc-stream:";
    private static final String OPS_STREAM_PREFIX = "office:ops-stream:";
    
    @Autowired
    private ManagedFileManagement fileManager;
    
    @Autowired
    private DistributedManagedFileLocker locker;

    @Autowired
    private GCManagedFiles gcManagedFiles;
    
    @Autowired
    private Statistics statistics;
    
    private final HashMap<DocRestoreID, DocStreams> docStreamsMap = new HashMap<DocRestoreID, DocStreams>();
        
    private final String uniqueID = UUID.randomUUID().toString();

    @Override
	public void afterPropertiesSet() throws Exception {
        locker.addCollectionToLock(this);		
	}

    /**
     * Determine if the DistributedDocumentStreamManager has a control structure for
     * the provided document.
     *
     * @return true if the DocRestoreID is known, otherwise false
     */
    public boolean knowsDocument(final DocRestoreID docRestoreID) throws OXException {
    	Validate.notNull(docRestoreID);

        boolean result = false;

        synchronized(docStreamsMap) {
            final DocStreams aDocStreams = docStreamsMap.get(docRestoreID);
            result = (aDocStreams != null);
        }

        return result;
    }

    /**
     * Adds/sets the provided document data as distributed managed file.
     *
     * @param docResourceID the unique document id
     * @param docData the document content to be stored
     * @return
     * @throws OXException
     */
    public String addDocumentStreams(final DocRestoreID docRestoreID, final byte[] docData) throws OXException {
         return setDocumentStream(docRestoreID, docData, DOC_STREAM_PREFIX);
    }

    /**
     * Adds/sets the provided document operation data as distributed managed file.
     *
     * @param docResourceID the unique document id
     * @param opsData the document operations applied to the document stream
     * @return
     * @throws OXException
     */
    public String addOperationStream(final DocRestoreID docRestoreID, final byte[] opsData) throws OXException {
        return setDocumentStream(docRestoreID, opsData, OPS_STREAM_PREFIX);
    }

    /**
     * Retrieves the stream of a unique stream ID.
     *
     * @param uniqueStreamID the unique stream ID
     * @return the stream or null if no stream is associated to the stream ID.
     */
    public InputStream getStream(final String uniqueStreamID) {
        InputStream stream = null;

        try {
            final ManagedFile managedFile = fileManager.getByID(uniqueStreamID);
            stream = managedFile.getInputStream();
        } catch (OXException e) {
            // nothing to do - it's possible that the managed file doesn't exists
        }

        return stream;
    }

    /**
     * Removes the document related distributed managed files from this
     * manager and the responsible structures.
     *
     * @param docResourceID the document unqiue ID
     */
    public void removeDocumentStreams(final DocRestoreID docRestoreID) {
        // managed files will be automatically removed by garbage collector
        synchronized(docStreamsMap) {
            final DocStreams docStreams = docStreamsMap.remove(docRestoreID);
            if (null != docStreams) {
                if (StringUtils.isNotEmpty(docStreams.getDocStreamID())) {
                    statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_DEC));
                }
                if (StringUtils.isNotEmpty(docStreams.getOpsStreamID())) {
                    statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_DEC));
                }
            }
        }
    }

    /**
     * Determines if a stream ID is known to the distributed document stream
     * manager.
     *
     * @param uniqueStreamID
     * @return
     */
    public boolean hasStream(final String uniqueStreamID) {
        return (StringUtils.isNotEmpty(uniqueStreamID)) ? fileManager.contains(uniqueStreamID) : false;
    }

    @Override
    public Set<IStreamIDCollection> getStreamIDCollectionCollection() {
        Set<IStreamIDCollection> result = null;

        synchronized(docStreamsMap) {
            if (docStreamsMap.size() > 0) {
                final HashSet<IStreamIDCollection> copy = new HashSet<IStreamIDCollection>();
                final Collection<DocStreams> docStreams = docStreamsMap.values();

                // make a deep copy of the map to enable parallel access
                for (DocStreams doc : docStreams) {
                    copy.add(doc.clone());
                }

                result = copy;
            }
        }

        return result;
    }

    @Override
    public boolean touch(final DocRestoreID uniqueCollectionID, String streamID) {
        return fileManager.contains(streamID);
    }

    @Override
    public void updateStates(Set<IStreamIDCollection> updatedCollections) {
        synchronized(docStreamsMap) {

            for (final IStreamIDCollection updated : updatedCollections) {
                final DocStreams docStream = docStreamsMap.get(updated.getUniqueCollectionID());
                if (null != docStream) {
                    docStream.touch(updated.getTimeStamp());
                }
            }
        }
    }

    @Override
    public String getUniqueID() {
        return uniqueID;
    }

    /**
     *
     * @param docResourceID
     * @param data
     * @param PREFIX
     * @return
     * @throws OXException
     */
    private String setDocumentStream(final DocRestoreID docRestoreID, final byte[] data, final String PREFIX) throws OXException {
        Validate.notNull(docRestoreID);

        final String uniqueStreamID = createUniqueStreamID(docRestoreID, PREFIX);
        final InputStream inStream = new ByteArrayInputStream(data);
        fileManager.createManagedFile(uniqueStreamID, inStream, true);

        synchronized(docStreamsMap) {
            String obsoleteStreamID = null;
            DocStreams docStreams = docStreamsMap.get(docRestoreID);
            if (null == docStreams) {
                docStreams = new DocStreams(docRestoreID);
            }

            if (DOC_STREAM_PREFIX.equals(PREFIX)) {
                obsoleteStreamID = docStreams.getDocStreamID();
                docStreams.setDocStreamID(uniqueStreamID);
            } else if (OPS_STREAM_PREFIX.equals(PREFIX)) {
                obsoleteStreamID = docStreams.getOpsStreamID();
                docStreams.setOpsStreamID(uniqueStreamID);
            }
            docStreamsMap.put(docRestoreID, docStreams);

            // add now obsolete managed file id to the gc
            gcManagedFiles.addObseleteManagedFileId(obsoleteStreamID);
        }
        IOHelper.closeQuietly(inStream);

        return uniqueStreamID;
    }

    /**
     * Creates a unique stream ID to be used to reference the document/operation
     * stream.
     *
     * @param docResourceID the document resource id to be used to associate the stream
     * @param PREFIX a prefix to be used for the unique id
     * @return a unique stream ID
     */
    private static String createUniqueStreamID(final DocRestoreID docRestoreID, final String PREFIX) {
        Validate.notNull(docRestoreID);

        final StringBuffer strBuf = new StringBuffer(PREFIX);
        strBuf.append(docRestoreID.toString());
        strBuf.append(":");
        strBuf.append(UUID.randomUUID());
        return strBuf.toString();
    }

}
