/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.distributed;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.backup.manager.impl.DocumentBackupManager;
import com.openexchange.office.tools.directory.IStreamIDCollection;

@Service
public class DistributedManagedFileLocker implements InitializingBean, DisposableBean {

    static private final Logger LOG = LoggerFactory.getLogger(DistributedManagedFileLocker.class);
    static final long TOUCH_MANAGEDFILE_TIMER_PERIOD = 90000;
    
    @Autowired
    private DocumentBackupManager documentBackupManager;
    
    private Timer timer = new Timer("com.openexchange.office.backup.DistributedManagedFileLocker");
    
    private final Map<String, WeakReference<IDistributedManageFileCollection>> lockCollections = Collections.synchronizedMap(new HashMap<String, WeakReference<IDistributedManageFileCollection>>());
    private TimerTask timerTask = null;
    

	@Override
	public void afterPropertiesSet() throws Exception {
        // create the task to touch the held resources at at specified interval
        timerTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    final List<IDistributedManageFileCollection> collections = new ArrayList<IDistributedManageFileCollection>();

                    synchronized(lockCollections) {
                        for (WeakReference<IDistributedManageFileCollection> weak : lockCollections.values()) {
                            final IDistributedManageFileCollection collection = weak.get();

                            if (null != collection) {
                                collections.add(collection);
                            }
                        }
                    }

                    for (final IDistributedManageFileCollection aCollection : collections) {
                        final Set<IStreamIDCollection> streamIDCollections = aCollection.getStreamIDCollectionCollection();

                        if (null != streamIDCollections) {

                            // touch each collected document streams
                            for (final Iterator<IStreamIDCollection> streamIDCollectionIter = streamIDCollections.iterator(); streamIDCollectionIter.hasNext();) {
                                final IStreamIDCollection streamIDCollection = streamIDCollectionIter.next();
                                final long timeStamp = new Date().getTime();
                                final Set<String> streamIDs = streamIDCollection.getStreamIDs();
                                final Set<String> touchFailedStreamIDs = new HashSet<String>();

                                boolean successfullyTouched = true;
                                if (null != streamIDs) {
                                    // touch the managed files using the stream id
                                    for (final Iterator<String> streamsIDsIter = streamIDs.iterator(); streamsIDsIter.hasNext();) {
                                        final String  streamID        = streamsIDsIter.next();
                                        final boolean touchSuccessful = aCollection.touch(streamIDCollection.getUniqueCollectionID(), streamID);
                                        successfullyTouched &= touchSuccessful;
                                        if (!touchSuccessful) {
                                            touchFailedStreamIDs.add(streamID);
                                        }
                                    }
                                }

                                if (successfullyTouched) {
                                    // set new time stamp for the collection
                                    streamIDCollection.touch(timeStamp);
                                } else {
                                    // check if the document has been removed by the clean up thread - could be
                                    // possible as we work on a copy of data - if true don't show any message              
                                    if (!documentBackupManager.knowsDocument(streamIDCollection.getUniqueCollectionID())) {
                                    	// Make sure that we remove the complete structures in this case, too
                                    	// knowsDocument provides true only, if all needed structure are filled
                                    	documentBackupManager.removeDocument(streamIDCollection.getUniqueCollectionID());
                                        continue;
                                    }

                                    // Create string containing the failed stream IDs for diagnosis purpose
                                    String failedStreamIDs = "unknown";

                                    if (!touchFailedStreamIDs.isEmpty()) {
                                        final StringBuffer failedStreamsStrBuffer = new StringBuffer(256);
                                        int count = 0;

                                        for (String s : touchFailedStreamIDs) {
                                            if (count >= 1) {
                                                failedStreamsStrBuffer.append(", ");
                                            }
                                            failedStreamsStrBuffer.append(s);
                                            count++;
                                        }
                                        failedStreamIDs = failedStreamsStrBuffer.toString();
                                    }

                                    LOG.warn("RT connection: Touching distributed managed file(s) failed - remove restore entry for: " + streamIDCollection.getUniqueCollectionID() + ", last successful touch was: " + streamIDCollection.getTimeStamp() + ", failed stream(s): " + failedStreamIDs);

                                    // remove document from document backup manager to prevent endless failed touches
                                   	documentBackupManager.removeDocument(streamIDCollection.getUniqueCollectionID());
                                }
                            }

                            // update the state of the stream ID collections
                            aCollection.updateStates(streamIDCollections);
                        }
                    }
                } catch (Throwable e) {
                	ExceptionUtils.handleThrowable(e);
                    LOG.warn("RT connection: Exception caught while trying to touch active document resources", e);
                }
            }
        };

        // start the periodic timer with the specified timeout
        timer.schedule(timerTask, TOUCH_MANAGEDFILE_TIMER_PERIOD, TOUCH_MANAGEDFILE_TIMER_PERIOD);
    }

    @Override
	public void destroy() throws Exception {
        final Timer aTimer = timer;

        if (aTimer != null) {
            aTimer.cancel();
            aTimer.purge();
            timer = null;
        }
        lockCollections.clear();
    }

    /**
     *
     * @param collection
     */
    public void addCollectionToLock(final IDistributedManageFileCollection collection) {
        if (null != collection) {
            final String uniqueID = collection.getUniqueID();
            lockCollections.put(uniqueID, new WeakReference<IDistributedManageFileCollection>(collection));
        }
    }

    public void removeCollectionToLock(final IDistributedManageFileCollection collection) {
        if (null != collection) {
        	final String uniqueID = collection.getUniqueID();
            lockCollections.remove(uniqueID);
        }
    }
}
