/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.distributed;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.office.hazelcast.core.doc.DocumentResourcesDirectory;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentResources;
import com.openexchange.office.tools.directory.IStreamIDCollection;
import com.openexchange.office.tools.monitoring.RestoreDocEvent;
import com.openexchange.office.tools.monitoring.RestoreDocEventType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.cluster.ClusterService;

/**
 * Manages the document resources a document references images and
 * controls the life-cycle of them in the distributed managed file management.
 *
 * @author Carsten Driesner
 * @since 7.8.1
 *
 */
@Service
public class DistributedDocumentResourceManager implements IDistributedManageFileCollection, InitializingBean {

    static private final Logger LOG = LoggerFactory.getLogger(DistributedDocumentResourceManager.class);
        
    private final HashMap<DocRestoreID, DocumentResources> docResourcesMap = new HashMap<DocRestoreID, DocumentResources>();
    private final String uniqueID = UUID.randomUUID().toString();
    
    @Autowired
    private DocumentResourcesDirectory globalDocumentResourcesMap;
    
    @Autowired
    private ManagedFileManagement fileManager;
    
    @Autowired
    private DistributedManagedFileLocker locker;
    
    @Autowired
    private ClusterService clusterService;
    
    @Autowired
    private Statistics statistics;

    @Override
	public void afterPropertiesSet() throws Exception {
        locker.addCollectionToLock(this);    	
	}

	public boolean addResourceRef(final DocRestoreID docRestoreID, final Resource resource) {
        Validate.notNull(docRestoreID);
        Validate.notNull(resource);

        boolean result = false;

        try {
            DocumentResources docResources = globalDocumentResourcesMap.get(docRestoreID);

            if (null == docResources) {
                final long timeStamp = new Date().getTime();
                final Set<String> resources = new HashSet<String>();

                docResources = new DocumentResources(docRestoreID, timeStamp, clusterService.getLocalMemberUuid(), resources);
            }

            if (null != docResources) {
                Set<String> resourcesSet = docResources.getResources();
                if (resourcesSet.add(resource.getManagedId())) {
                    // set new resource in set and write back
                    docResources.setResources(resourcesSet);
                    globalDocumentResourcesMap.set(docRestoreID, docResources);

                    // update our local hash map
                    synchronized(docResourcesMap) {
                        final DocumentResources localDocResources = docResourcesMap.get(docRestoreID);
                        if (null == localDocResources) {
                            docResourcesMap.put(docRestoreID, docResources);
                        } else {
                            Set<String> set = localDocResources.getResources();
                            if (null == set) { set = new HashSet<String>(); }
                            set.add(resource.getManagedId());
                            localDocResources.setResources(set);
                            docResourcesMap.put(docRestoreID, localDocResources);
                        }
                    }
                }
                result = true;
            }
        } catch (Exception e) {
            LOG.warn("RT connection: Exception caught while trying to set new resource to global hazelcast doc resource directory", e);
        }

        return result;
    }

    public boolean removeResourceRef(final DocRestoreID docRestoreID, final Resource resource) {
        Validate.notNull(docRestoreID);
        Validate.notNull(resource);

        boolean result = false;

        try {
            final DocumentResources docResources = globalDocumentResourcesMap.get(docRestoreID);

            if (null != docResources) {
                Set<String> resourcesSet = docResources.getResources();
                if (resourcesSet.remove(resource.getManagedId())) {
                    // set new resource in set and write back
                    docResources.setResources(resourcesSet);
                    globalDocumentResourcesMap.set(docRestoreID, docResources);

                    // update our local hash map
                    synchronized(docResourcesMap) {
                        final DocumentResources localDocResources = docResourcesMap.get(docRestoreID);
                        if (null != localDocResources) {
                            Set<String> set = localDocResources.getResources();
                            if (null != set) {
                                set.remove(resource.getManagedId());
                                localDocResources.setResources(set);
                                docResourcesMap.put(docRestoreID, localDocResources);
                            }
                        }
                    }
                }
                result = true;
            }
        } catch (Exception e) {
            LOG.warn("RT connection: Exception caught while trying to set new resource to global hazelcast doc resource directory", e);
        }

        return result;
    }

    public void removeDocResources(final DocRestoreID docRestoreID) {
        Validate.notNull(docRestoreID);

        try {
            final DocumentResources docResources = globalDocumentResourcesMap.get(docRestoreID);

            if (null != docResources) {
                globalDocumentResourcesMap.remove(docRestoreID);
                statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_DEC));
            }

            // remove the resource ids from our local map
            synchronized(docResourcesMap) {
                docResourcesMap.remove(docRestoreID);
            }
        } catch (Exception e) {
            LOG.warn("RT connection: Exception caught while trying to set new resource to global hazelcast doc resource directory", e);
        }
    }

    public Set<String> getDocResources(final DocRestoreID docRestoreID) {
        Validate.notNull(docRestoreID);

        Set<String> result = null;

        synchronized(docResourcesMap) {
            final DocumentResources docResources = docResourcesMap.get(docRestoreID);

            if (null != docResources) {
                // first, provide the local resources map
                result = docResources.getResources();
            }
        }

        // Try the global hazelcast map now - it's possible that a client asks us
        // but the document was first opened on a different node.
        try {
            if (null == result) {
                final DocumentResources docResources = globalDocumentResourcesMap.get(docRestoreID);

                if (null != docResources) {
                    result = docResources.getResources();
                }
            }
        } catch (final OXException e) {
            LOG.warn("RT connection: Exception caught while trying to get resources from global hazelcast doc resource directory", e);
        }

        return result;
    }

    @Override
    public Set<IStreamIDCollection> getStreamIDCollectionCollection() {
        Set<IStreamIDCollection> result = null;

        synchronized(docResourcesMap) {
            if (docResourcesMap.size() > 0) {
                final HashSet<IStreamIDCollection> copy = new HashSet<IStreamIDCollection>();
                final Collection<DocumentResources> docResources = docResourcesMap.values();

                // make a deep copy of the map to enable parallel access
                for (DocumentResources docRes : docResources) {
                    copy.add(docRes.clone());
                }

                result = copy;
            }
        }

        return result;
    }

    @Override
    public boolean touch(final DocRestoreID uniqueCollectionID, String streamID) {
        return fileManager.contains(streamID);
    }

    @Override
    public void updateStates(Set<IStreamIDCollection> updatedCollections) {
        final Collection<DocRestoreID> docResIds = new HashSet<DocRestoreID>(updatedCollections.size());
        final HashMap<DocRestoreID, IStreamIDCollection> tmpMap = new HashMap<DocRestoreID, IStreamIDCollection>(updatedCollections.size());

        for (final IStreamIDCollection updated : updatedCollections) {
            final DocRestoreID docResId = updated.getUniqueCollectionID();
            tmpMap.put(docResId, updated);
            docResIds.add(docResId);
        }

        try {
            final Map<DocRestoreID, DocumentResources> documentResourcesMap = globalDocumentResourcesMap.get(docResIds);

            for (final DocRestoreID docRestoreId : docResIds) {
                final DocumentResources docResources = documentResourcesMap.get(docRestoreId);
                final IStreamIDCollection streamIDCollection = tmpMap.get(docRestoreId);

                if ((null != docResources) && (null != streamIDCollection)) {
                   docResources.setTimeStamp(streamIDCollection.getTimeStamp());
                   globalDocumentResourcesMap.set(docRestoreId, docResources);
                }
            }
        } catch (Exception e) {
            LOG.warn("RT connection: Exception caught while trying to update time stamp of global hazelcast document resources", e);
        }

        synchronized(docResourcesMap) {

            for (final IStreamIDCollection updated : updatedCollections) {
                final DocumentResources docResources = docResourcesMap.get(updated.getUniqueCollectionID());
                if (null != docResources) {
                    docResources.touch(updated.getTimeStamp());
                }
            }
        }
    }

    @Override
    public String getUniqueID() {
        return this.uniqueID;
    }
}
