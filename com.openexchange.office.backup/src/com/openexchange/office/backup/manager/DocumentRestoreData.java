/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager;

import java.io.InputStream;

import org.json.JSONArray;

import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.common.error.ErrorCode;

public class DocumentRestoreData {

    private IResourceManager resourceManager;
    private InputStream inputStream;
    private JSONArray operations;
    private ErrorCode errorCode = ErrorCode.NO_ERROR;
    private int baseOSN;
    private String baseVersion;
    private int osn;
    private String fileName;
    private String mimeType;

    public DocumentRestoreData() { }

    public DocumentRestoreData(final IResourceManager resourceManager, final InputStream inputStream, final JSONArray operations, final ErrorCode errorCode, int baseOSN, final String baseVersion) {
        this.resourceManager = resourceManager;
        this.inputStream = inputStream;
        this.operations = operations;
        this.errorCode = errorCode;
        this.baseOSN = baseOSN;
        this.baseVersion = baseVersion;
        this.osn = baseOSN;
    }

    public final IResourceManager getResourceManager() { return this.resourceManager; }
    public final InputStream getInputStream() { return this.inputStream; }
    public final JSONArray getOperations() { return this.operations; }
    public final ErrorCode getErrorCode() { return this.errorCode; }
    public final int getBaseOSN() { return this.baseOSN; }
    public final String getBaseVersion() { return this.baseVersion; }
    public final int getOSN() { return this.osn; }
    public final String getFileName() { return this.fileName; }
    public final String getMimeType() { return this.mimeType; }

    public void setInputStream(final InputStream inputStream) { this.inputStream = inputStream; }
    public void setOperations(final JSONArray operations) { this.operations = operations; }
    public void setErrorCode(final ErrorCode errorCode) { this.errorCode = errorCode; }
    public void setResourceManager(final IResourceManager resourceManager) { this.resourceManager = resourceManager; }
    public void setBaseOSN(final int baseOSN) { this.baseOSN = baseOSN; }
    public void setBaseVersion(final String baseVersion) { this.baseVersion = baseVersion; }
    public void setOSN(int osn) { this.osn = osn; }
    public void setFileName(final String fileName) { this.fileName = fileName; }
    public void setMimeType(final String mimeType) { this.mimeType = mimeType; }
}
