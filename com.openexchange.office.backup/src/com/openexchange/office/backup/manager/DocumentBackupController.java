/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager;

import java.util.Set;

import org.json.JSONArray;

import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.tools.directory.DocRestoreID;

/**
 * Central interface to access the functions of the OX Documents
 * Backup service, which can restore documents and merge operations
 * dependent on the osn/opl.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 */
public interface DocumentBackupController {

	public boolean registerDocument(final DocRestoreID docResId, final String fileName, final String mimeType, final int osn, final String version, final byte[] docContent);

	public boolean updateDocumentInitState(final DocRestoreID docResId, final int osn);

	public boolean deregisterDocument(final DocRestoreID docResId);

	public void removeDocument(final DocRestoreID docResId);

	public boolean addOperations(final DocRestoreID docResId, final JSONArray operations, int newOSN) throws Exception;

	public boolean addResource(final DocRestoreID docResId, final Resource resource);

	public DocumentRestoreData getDocumentRestoreData(final DocRestoreID docResId);

	public boolean knowsDocument(final DocRestoreID docResId);

	public Set<DocRestoreID> getDocuments();

	public String getUniqueInstanceID();

}
