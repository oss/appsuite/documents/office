/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.monitoring.Statistics;

/**
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.10.0
 */
@Service
public class AsyncBackupWorker implements InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(AsyncBackupWorker.class);

    private static final String WRITEBACK_THREAD_NAME = ("com.openexchange.office.backup.DocumentBackupManager.AsyncBackupWorker"); 
    private static final long WRITEBACK_OPERATIONS_TIMER_PERIOD = 30000;

    private final Queue<OperationDocData> pendingDocData = new LinkedList<OperationDocData>();
    private Thread writeBackThread;
    
    @Autowired
    private StoreOperationData storeOperationDataProcessor;
    
    @Autowired
    private Statistics statistics;

    /**
     * Initializes a new AsyncBackupWorker instance which used a thread-based
     * background processing - to combine operations collected for a document
     * and write them into managed files.
     *
	*/
	@Override
	public void afterPropertiesSet() throws Exception {
		writeBackThread = new Thread() {

            @Override
            public void run() {
                boolean bInterrupted = false;

                long inactiveTime = WRITEBACK_OPERATIONS_TIMER_PERIOD;

        		while (!bInterrupted) {
                	try {
                        Thread.sleep(inactiveTime);

                        int countStored = 0;
                        long timeStart = System.currentTimeMillis();

                        final Queue<OperationDocData> workingQueue = retrieveCurrentWorkingSetAsCopy();
                        while (!workingQueue.isEmpty()) {
                            try {
                                final int countBefore = workingQueue.size();
                                final OperationDocData currOpsDocData = pollOperationDocDataFromFront(workingQueue);
                                final int countAfter = workingQueue.size();

                                if (null == currOpsDocData)
                                    break;

    	                        // store the pending operations to hazelcast/distributed managed file
                                storeOperationDataProcessor.storePendingOperationsToDocumentState(
    	                            currOpsDocData.getID(), currOpsDocData.getOperations(), currOpsDocData.getOSN());
    	                        countStored += Math.max(0, (countBefore - countAfter));

                                // make sure that we bail out as soon as possible after being interrupted
                                if (Thread.interrupted())
                                	throw new InterruptedException();
                                
                        	} catch (InterruptedException e) {
                            	Thread.currentThread().interrupt();
                            	bInterrupted = true;
                            	throw new InterruptedException();
                            } catch (Exception e) {
                                log.warn("RT2: Exception caught while trying to save operations to backup file", e);
                            }
                        }

                        final long runTime = Math.max(0, (System.currentTimeMillis() - timeStart));
                        statistics.setRestoreDocOpsMsgStored(countStored);
                        statistics.setRestoreDocMsgStoredTime(runTime);

                        inactiveTime = calcInactivityTime(runTime, 1000, WRITEBACK_OPERATIONS_TIMER_PERIOD);
                	} catch (InterruptedException e) {
                    	Thread.currentThread().interrupt();
                    	bInterrupted = true;
                    }
        		}
            }
        };

        writeBackThread.setName(WRITEBACK_THREAD_NAME);
        writeBackThread.setDaemon(true);
        writeBackThread.setPriority(Thread.NORM_PRIORITY - 1);
        writeBackThread.start();

        log.trace("AsyncBackupWorker thread created");
	}



	/**
	 * Appends
	 * @param docResId a DocRestoreID instance which references a document restore instances - must not be null
	 * @param operations a JSONArray containing operations - can be empty but must not be null
	 */
	public void appendOperations(final DocRestoreID docResId, final JSONArray operations, int newOSN) {
        final OperationDocData opsData = new OperationDocData(docResId, operations, newOSN);

        synchronized (pendingDocData) {
            pendingDocData.add(opsData);
        }
	}

	/**
	 *
	 */
	@Override
	public void destroy() throws Exception {
		log.trace("AsyncBackupWorker.dispose() called");

		synchronized (pendingDocData) {
			pendingDocData.clear();
		}

	    final Thread aThread = writeBackThread;
	    if (aThread != null) {
            try
            {
            	aThread.interrupt();
            	aThread.join(1000);
            	writeBackThread = null;
            }
            catch (final InterruptedException e) {
                log.debug("AsyncBackupWorker: InterruptedException preparing shutdown of the writeback thread", e);
                Thread.currentThread().interrupt();
            }
	    }
	}

	/**
	 * Provides the current set of pending operations as a copied queue. The
	 * original queue is emptied.
	 *
	 * @return a working set of pending operations
	 */
	private Queue<OperationDocData> retrieveCurrentWorkingSetAsCopy() {
		final Queue<OperationDocData> copy = new LinkedList<>();
		synchronized (pendingDocData) {
			CollectionUtils.addAll(copy, pendingDocData.iterator());
			pendingDocData.clear();
		}

		return copy;
	}

	/**
	 * Retrieve all operations pending for the front entry in the pending document data
	 * queue.
	 * The implementation tries to collect all operations which are pending for one document
	 * restore id instance and provide a OperationDocData instance with all collected operations.
	 * 
	 * @return OperationDocData with all collected operations and updated OSN
	 * @throws JSONException in case something went wrong with the JSON objects
	 */
	private static OperationDocData pollOperationDocDataFromFront(Queue<OperationDocData> queuedDocData) throws JSONException {
        final OperationDocData currOpsDocData = queuedDocData.peek();

        if (null != currOpsDocData) {
            final DocRestoreID docRestoreID = currOpsDocData.getID();

            List<OperationDocData> aPendingDocOps = null;
            aPendingDocOps = queuedDocData.stream().filter(p -> { return isOpsDataForDocRestoreID(p, docRestoreID); }).collect(Collectors.toList());
            queuedDocData.removeAll(aPendingDocOps);

            // compact collection of OperationDocData into one instance
            if (null != aPendingDocOps) {
                final JSONArray opsCollectedArray = new JSONArray();
                appendCollectedOperations(opsCollectedArray, aPendingDocOps);

                final int newOSN = OperationHelper.getFollowUpOSN(opsCollectedArray);
                return new OperationDocData(docRestoreID, opsCollectedArray, newOSN);
            }
        }

        return currOpsDocData;
	}

	/**
	 * Determines if a OperationDocData instance contains data for a specified DocRestoreID.
	 *
	 * @param aOpsDocData a OperationDocData instance - must not be null
	 * @param docRestoreID a DocRestoreID instance - must not be null
	 * @return true if the OperationDocData is associated with the DocRestoreID or false if not
	 */
	private static boolean isOpsDataForDocRestoreID(OperationDocData aOpsDocData, DocRestoreID docRestoreID) {
		return aOpsDocData.getID().equals(docRestoreID);
	}

    /**
     * Append a collection of operations stored in OperationDocData instances to a provided JSONArray instance.
     *
     * @param target a JSONArray instance which stores the collected operations - must not be null
     * @param collectedOpsData a list of OperationDocData instances - the list must not contain any null reference
     * @throws JSONException is thrown when something went wrong with JSON objects/arrays
     */
    private static void appendCollectedOperations(final JSONArray target, List<OperationDocData> collectedOpsData)
        throws JSONException {
        for (final OperationDocData aDocData : collectedOpsData) {
            JSONHelper.appendArray(target, aDocData.getOperations());
        }
	}

    /**
     * 
     * @param runtime
     * @param minInactivity
     * @param maxInactivity
     * @return
     */
    private static long calcInactivityTime(long runtime, long minInactivity, long maxInactivity) {
    	return Math.max(minInactivity, maxInactivity - runtime);
    }

}
