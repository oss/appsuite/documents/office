/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager.impl;

import org.json.JSONArray;

import com.openexchange.office.tools.directory.DocRestoreID;

/**
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.10.0
 */
public class OperationDocData {
    private final DocRestoreID docResId;
    private final JSONArray appliedOperations;
    private final int newOSN;

    /**
     *
     * @param docResId
     * @param appliedOperations
     */
    public OperationDocData(final DocRestoreID docResId, final JSONArray appliedOperations, int newOSN) {
        this.docResId = docResId;
        this.appliedOperations = appliedOperations;
        this.newOSN = newOSN;
    }

    /**
     *
     * @return
     */
    public final DocRestoreID getID() {
        return this.docResId;
    }

    /**
     *
     * @return
     */
    public final JSONArray getOperations() {
        return this.appliedOperations;
    }

    /**
     *
     * @return
     */
    public final int getOSN() {
    	return this.newOSN;
    }
}

