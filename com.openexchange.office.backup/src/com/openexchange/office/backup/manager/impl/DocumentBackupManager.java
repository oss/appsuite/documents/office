/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.log.Log;
import com.openexchange.office.backup.distributed.DistributedDocumentResourceManager;
import com.openexchange.office.backup.distributed.DistributedDocumentStreamManager;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.backup.manager.DocumentRestoreData;
import com.openexchange.office.hazelcast.core.doc.DocumentDirectory;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentState;
import com.openexchange.office.tools.directory.IStreamIDCollection;
import com.openexchange.office.tools.monitoring.RestoreDocEvent;
import com.openexchange.office.tools.monitoring.RestoreDocEventType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.cluster.ClusterService;

/**
 * Central class to control storing and retrieving document restore data
 * to enable a user to request a restore of his/her private document content.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.8.1
 *
 */
@Service
@RegisteredService(registeredClass=DocumentBackupController.class)
@SuppressWarnings("deprecation")
public class DocumentBackupManager implements DocumentBackupController, StoreOperationData, InitializingBean {

    static private final org.apache.commons.logging.Log LOG = Log.loggerFor(DocumentBackupManager.class);
    
	private static final int OSN_UNDEFINED = -1;
    
    @Autowired
    private IResourceManagerFactory resourceManagerFactory;    
    
    @Autowired
	private DocumentDirectory documentDir;
    
    @Autowired
	private AsyncBackupWorker backupWriter;
    
    @Autowired
	private DistributedDocumentStreamManager distributedDocumentStreamManager;
	
    @Autowired
	private DistributedDocumentResourceManager distributedDocumentResourceManager;

    @Autowired
    private Statistics statistics;
    
    @Autowired
    private ClusterService clusterService;

	//--------------------------------------------------------------------------------------------------------
	private final Object sync = new Object();
	
	private String uniqueInstanceID;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.uniqueInstanceID = clusterService.getLocalMemberUuid();
	}
		
	/**
	 * Registers a new document state in the global document state directory
	 * managed by Hazelcast.
	 *
	 * @param docResId a unique document resource id which references the document
	 * @param osn the current operation state number
	 * @param version the current version of the document, can be null
	 * @param docContent the document content
	 * @return
	 */
	@Override
	public boolean registerDocument(final DocRestoreID docRestoreId, final String fileName, final String mimeType, final int osn, final String version, final byte[] docContent) {
		try {
			DocumentState storedDocState;
			synchronized(sync) {
				if (documentDir != null) {
	                storedDocState = documentDir.get(docRestoreId);
				} else {
					LOG.warn("RT connection: Hazelcast based data structure DocumentDirectory is not available - Document backup service cannot work correclty!");
					return false;
				}
			}

			if (null == storedDocState) {
				LOG.debug("RT connection: Document Restore creates new entry for " + docRestoreId.toString() + " at " + System.currentTimeMillis());

				storedDocState = new DocumentState(docRestoreId, uniqueInstanceID, -1, version);
				storedDocState.setFileName(fileName);
				storedDocState.setMimeType(mimeType);

				// store the document content using the distributed document stream manager
				final String managedDocFileId = distributedDocumentStreamManager.addDocumentStreams(docRestoreId, docContent);

                // store the backup managed file in the global hazelcast structures
                storedDocState.setManagedDocFileId(managedDocFileId);

                statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.NEW_DOC));
                statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_INC));
			} else {
				LOG.debug("RT connection: Document Restore overwrites entry for " + docRestoreId.toString() + " at " + System.currentTimeMillis());

			    // overwrite a possible document stream with the latest
				String managedDocFileId = distributedDocumentStreamManager.addDocumentStreams(docRestoreId, docContent);
                // make sure that no resources are held by the old document entry, too.
				distributedDocumentResourceManager.removeDocResources(docRestoreId);

				// overwrite base/current osn and version
				storedDocState.setBaseOSN(osn);
				storedDocState.setBaseVersion(version);
				storedDocState.setCurrentOSN(osn);
				storedDocState.setCurrentVersion(version);

				// reset operations managed file
				storedDocState.setManagedOpsFileId(null);

                // store the backup managed file in the global hazelcast structures
                storedDocState.setManagedDocFileId(managedDocFileId);
                storedDocState.setFileName(fileName);
                storedDocState.setMimeType(mimeType);

                statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_INC));
			}

			// register this node instance as responsible and set document as active with current time stamp
			storedDocState.setActive(true);
			storedDocState.setTimeStamp(new Date().getTime());
			storedDocState.setUniqueInstanceId(this.uniqueInstanceID);

			synchronized(sync) {
				if (null != documentDir) {
                    documentDir.set(docRestoreId, storedDocState);
				}
			}

			return true;
		} catch (OXException e) {
			LOG.warn("RT2: Exception while creating global backup structure for document", e);
		}

		return false;
	}

    @Override
    public boolean updateDocumentInitState(final DocRestoreID docRestoreId, final int osn) {
        if (docRestoreId == null) {
            LOG.debug("RT2: DocumentBackupManager called with DocRestoreId set to null, ignore request!");
            return false;
        }

        try {
            DocumentState storedDocState = null;
            synchronized(sync) {
				if (documentDir != null) {
					storedDocState = documentDir.get(docRestoreId);
				}
            }

            if (null == storedDocState) {
                // shouldn't happen - we want to set the initial osn if not set
                return false;
            } else if (storedDocState.getBaseOSN() == OSN_UNDEFINED) {
                storedDocState.setBaseOSN(osn);

                synchronized(sync) {
                    if (null != documentDir) {
                        documentDir.set(docRestoreId, storedDocState);
                    }
                }
            }

            return true;
        } catch (OXException e) {
            LOG.warn("RT2: Exception while creating global backup structure for document", e);
        }

        return false;
    }

    @Override
    public boolean deregisterDocument(final DocRestoreID docRestoreId) {
        if (docRestoreId == null) {
            LOG.debug("RT2: DocumentBackupManager called with DocRestoreId set to null, ignore request!");
            return false;
        }

        try {
            DocumentState storedDocState = null;
            synchronized(sync) {
				if (documentDir != null) {
					storedDocState = documentDir.get(docRestoreId);
				}
            }

            if (null == storedDocState) {
                // shouldn't happen - we want to deregister a document
                return true;
            } else {
                // deregister means that we want to set the hazelcast data
                // to non-active, which starts the automatic cleanup
                // timeout process
                storedDocState.setActive(false);
                storedDocState.setTimeStamp(new Date().getTime());
                storedDocState.setUniqueInstanceId(this.uniqueInstanceID);

                synchronized(sync) {
                    if (null != documentDir) {
                        documentDir.set(docRestoreId, storedDocState);
                    }
                }
            }

            return true;
        } catch (OXException e) {
            LOG.warn("RT2: Exception while creating global backup structure for document", e);
        }

        return false;
    }

	/**
	 * @param docResId
	 * @return
	 */
    @Override
	public void removeDocument(final DocRestoreID docRestoreId) {
        Validate.notNull(docRestoreId);

        synchronized(sync) {

			try {
				if (documentDir != null) {
					documentDir.remove(docRestoreId);
					statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.REMOVED_DOC));
				}
    		} catch (OXException e) {
                LOG.warn("RT2: Exception caught trying to remove timed out hazeclast document structure, docResId=" + docRestoreId, e);
    		}
		}

        // remove distributed managed files for document & resources
		distributedDocumentStreamManager.removeDocumentStreams(docRestoreId);
		distributedDocumentResourceManager.removeDocResources(docRestoreId);
	}

	/**
	 *
	 * @param docResId
	 * @param actionChunk
	 * @param newOSN
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean addOperations(final DocRestoreID docRestoreId, final JSONArray operations, int newOSN) throws Exception {
	    if ((null != operations) && operations.length() > 0) {
	        backupWriter.appendOperations(docRestoreId, operations, newOSN);
            return true;
		}

		return false;
	}

	/**
	 * @param docResId
	 * @param resource
	 * @return
	 */
	@Override
    public boolean addResource(final DocRestoreID docRestoreId, final Resource resource) {
        Validate.notNull(docRestoreId);

        boolean result = true;

        DocumentState storedDocState = null;
        synchronized(sync) {
        	try {
				if (documentDir != null) {
					storedDocState = documentDir.get(docRestoreId);
				}
        	} catch (OXException e) {
                LOG.warn("RT connection: Exception caught trying to retrieve document state from hazeclast document structure, docResId=" + docRestoreId, e);
        	}
        }

        if (storedDocState != null) {
        	// only add resource if document is known and stored in Hazelcast
            result = distributedDocumentResourceManager.addResourceRef(docRestoreId, resource);
        }

        return result;
    }

	/**
	 *
	 */
    @Override
    public DocumentRestoreData getDocumentRestoreData(DocRestoreID docRestoreId) {
        Validate.notNull(docRestoreId);

        final DocumentRestoreData docResourceData = new DocumentRestoreData();

        try {
            DocumentState storedDocState = null;
            synchronized(sync) {
				if (documentDir != null) {
					storedDocState = documentDir.get(docRestoreId);
				}
            }

            if (null != storedDocState) {
                final String managedDocFileId = storedDocState.getManagedDocFileId();
                final String managedOpsFileId = storedDocState.getManagedOpsFileId();
                final Set<String> docResources = distributedDocumentResourceManager.getDocResources(docRestoreId);

                // set the base version/osn to have synchronization data
                docResourceData.setBaseOSN(storedDocState.getBaseOSN());
                docResourceData.setBaseVersion(storedDocState.getBaseVersion());
                docResourceData.setOSN(storedDocState.getCurrentOSN());
                docResourceData.setFileName(storedDocState.getFileName());
                docResourceData.setMimeType(storedDocState.getMimeType());

                if (StringUtils.isNotEmpty(managedDocFileId)) {
                    final InputStream docInputStream = distributedDocumentStreamManager.getStream(managedDocFileId);

                    // retrieve input stream from managed document resource
                    if (null != docInputStream) {
                        docResourceData.setInputStream(docInputStream);
                    } else {
                        LOG.warn("RT2: couldn't retrieve backup document stream from managed file: " + managedDocFileId);
                        docResourceData.setErrorCode(ErrorCode.BACKUPDOCUMENT_BASEDOCSTREAM_NOT_FOUND);
                    }

                    // we always need a resource manager, but it can contain no resources
                    final IResourceManager resManager = resourceManagerFactory.createInstance(docRestoreId.toString());
                    if ((null != resManager) && (null != docResources) && (!docResources.isEmpty())) {
                        for (final String managedResId : docResources) {
                            final Resource managedResource = resManager.createManagedResource(managedResId);
                            resManager.addResource(managedResource);
                        }
                    }
                    docResourceData.setResourceManager(resManager);

                    // retrieve operations from the managed resource
                    if (StringUtils.isNotEmpty(managedOpsFileId)) {

                        InputStream opsInputStream = null;
                        try {
                            opsInputStream = distributedDocumentStreamManager.getStream(managedOpsFileId);

                            // retrieve input stream from managed document resource
                            if (null != opsInputStream) {
                                final JSONArray operationsArray = new JSONArray();
                                final BufferedReader in = new BufferedReader(new InputStreamReader(opsInputStream, "UTF-8"));

                                // create a JSONArray containing all the operations
                                String line;
                                while((line = in.readLine()) != null) {
                                    try {
                                        final JSONArray operations = new JSONArray(line);
                                        JSONHelper.appendArray(operationsArray, operations);
                                    } catch (JSONException e) {
                                        LOG.warn("RT2: Exception caught while trying to parse JSON operation from backup data", e);
                                    }
                                }

                                docResourceData.setOperations(operationsArray);
                            } else {
                                LOG.warn("RT2: couldn't retrieve backup document stream from managed file: " + managedDocFileId);
                                docResourceData.setErrorCode(ErrorCode.BACKUPDOCUMENT_BASEDOCSTREAM_NOT_FOUND);
                            }
                        } catch (Exception e) {
                            LOG.warn("RT2: Exception caught while trying to read operations JSON from backup data", e);
                        } finally {
                            IOUtils.closeQuietly(opsInputStream);
                        }
                    }
                } else {
                    LOG.warn("RT2: couldn't find backup managed file id for document: " + docRestoreId);
                }
            } else {
                LOG.warn("RT2: couldn't find global document state for document: " + docRestoreId);
            }
        } catch (Exception e) {
            docResourceData.setErrorCode(ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE);
            LOG.warn("RT2: Exception while retrieving global backup structure for document to restore.", e);
        }

        return docResourceData;
    }

	/**
	 * @param docResId
	 * @param operations
	 * @return
	 */
	@Override
	public boolean storePendingOperationsToDocumentState(final DocRestoreID docRestoreId, final JSONArray operations, int newOSN) {
		boolean result = false;

		DocumentState storedDocState = null;
        synchronized(sync) {
        	try {
				if (documentDir != null) {
					storedDocState = documentDir.get(docRestoreId);
				}
        	} catch (OXException e) {
                LOG.warn("RT2: Exception caught while trying to retrieve document data from Hazelcast", e);
        	}
		}

		if (null != storedDocState) {
			storedDocState.setCurrentOSN(newOSN);

			String managedOpsFileId = storedDocState.getManagedOpsFileId();

			if (null != managedOpsFileId) {
				// append new operations to the managed operations file
			    final InputStream opsStream = distributedDocumentStreamManager.getStream(managedOpsFileId);

                if (null != opsStream) {
                    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                    final PrintStream writer = new PrintStream(outStream);

                    try {
                        // copy the content of the original operation stream
                        IOUtils.copy(opsStream, outStream);

                        // append new operations using newline as separator
	                    writer.println();
	                    outStream.write(operations.toString().getBytes(Charset.forName("UTF-8")));
                        IOUtils.closeQuietly(outStream);

	                    // overwrite old managed resoure file
                        final byte[] newOpsFile = outStream.toByteArray();
                        managedOpsFileId = distributedDocumentStreamManager.addOperationStream(docRestoreId, newOpsFile);

	                    // store the backup managed file via distributed managed file management
	                    storedDocState.setManagedOpsFileId(managedOpsFileId);

	                    statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_INC));
                    } catch (Exception e) {
                        LOG.warn("RT2: Couldn't update document operation stream", e);
                        return false;
                    } finally {
                        IOUtils.closeQuietly(outStream);
                    }
                } else {
                    LOG.warn("RT2: couldn't retrieve backup operations stream from managed file: " + managedOpsFileId);
                }
			} else {
			    // create a new operations file
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                try {
                    outStream.write(operations.toString().getBytes(Charset.forName("UTF-8")));
                    IOUtils.closeQuietly(outStream);

                    final byte[] newOpsFile = outStream.toByteArray();
                    managedOpsFileId = distributedDocumentStreamManager.addOperationStream(docRestoreId, newOpsFile);

                    // store the backup managed file via distributed managed file management
                    storedDocState.setManagedOpsFileId(managedOpsFileId);

                    statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_INC));
                } catch (Exception e) {
                    LOG.warn("RT2: Couldn't create document operation stream", e);
                    return false;
                } finally {
                    IOUtils.closeQuietly(outStream);
                }
			}
		} else
			return false; // no backup data set - bail out with false

		synchronized(sync) {
			if (null != documentDir) {
				try {
                    documentDir.set(docRestoreId, storedDocState);
                    result = true;
				} catch (OXException e) {
	                LOG.warn("RT2: Exception caught while trying to set updated document data to Hazelcast", e);
				}
			}
		}

        return result;
	}

	@Override
	public boolean knowsDocument(DocRestoreID docResId) {
		boolean result = false;

		try {
	        DocumentState storedDocState = null;
	        synchronized(sync) {
				if (documentDir != null) {
					storedDocState = documentDir.get(docResId);
				}
	        }

	        if (null != storedDocState) {
	            result = distributedDocumentStreamManager.knowsDocument(docResId);
	        }
		} catch (final Exception e) {
			LOG.warn("RT connection: Exception caught while trying to determine document status", e);
		}

        return result;
    }

	@Override
    public Set<DocRestoreID> getDocuments() {
	    final Set<IStreamIDCollection> docCollection = distributedDocumentStreamManager.getStreamIDCollectionCollection();
	    final Set<DocRestoreID> docResCollection = new HashSet<>();

	    if (null != docCollection) {
	        for (IStreamIDCollection collection : docCollection) {
	            final DocRestoreID id = collection.getUniqueCollectionID();

                docResCollection.add(id);
	        }
	    }

	    return docResCollection;
	}

	@Override
    public String getUniqueInstanceID() {
        return this.uniqueInstanceID;
    }
}
