/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager.impl;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.hazelcast.core.doc.DocumentDirectory;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.directory.DocumentState;

@Service
public class CleanupBackupDocTask implements InitializingBean, DisposableBean {
    static private final Logger LOG = LoggerFactory.getLogger(CleanupBackupDocTask.class);

    private static final long CLEANUP_BACKUP_DOCUMENTS_TIMER_PERIOD = 300000; // 5 minutes
    private static final long MAX_IDLE_TIME = 4 * 60 * 60 * 1000; // 4h
    private Timer timer = new Timer("com.openexchange.office.backup.CleanupBackupTask");
    private TimerTask timerTask = null;

    @Autowired
    private DocumentBackupController documentBackupController;
    
    @Autowired
    private DocumentDirectory documentDir;
    
	@Override
	public void afterPropertiesSet() throws Exception {
        // create the task to touch the held resources at at specified interval
        timerTask = new TimerTask() {

            @Override
            public void run() {
                // read document states of the global hazelcast map
                try {
                    LOG.debug("RT connection: Restore document checks document backup states");
                    final Set<DocRestoreID> docsToCheck = documentBackupController.getDocuments();
                    final Map<DocRestoreID, DocumentState> docStates = documentDir.get(docsToCheck);
                    final String uniqueInstanceID = documentBackupController.getUniqueInstanceID();
                    final long currTimeStamp = new Date().getTime();

                    for (final DocRestoreID docResId : docsToCheck) {
                        final DocumentState docState = docStates.get(docResId);

                        if (null != docState) {
                            final String respInstanceId = docState.getUniqueInstanceId();

                            if (uniqueInstanceID.equals(respInstanceId)) {
                                final boolean active = docState.isActive();
                                final long timeStamp = docState.getTimeStamp();

                                if (!active && (currTimeStamp - timeStamp) >= MAX_IDLE_TIME) {
                                    // non-active document & max idle time reached -> remove dependent resources
                                    documentBackupController.removeDocument(docResId);
                                    LOG.debug("RT connection: Restore document removed from Hazecast due to max idle time reached, restoreId: " + docResId.toString());
                                }
                            }
                        } else {
                            // no entries in the hazelcast map -> remove dependent resources
                            // like document & operation streams
                            documentBackupController.removeDocument(docResId);
                            LOG.debug("RT connection: Restore document removed document data, because Hazelcast entry not found for restoreId: " + docResId.toString());
                        }
                    }
                }
                catch (Throwable e) {
                	ExceptionUtils.handleThrowable(e);
                    LOG.warn("RT connection: Exception caught while checking hazelcast map entries", e);
                }
            }
        };

        // start the periodic timer with the specified timeout
        timer.schedule(timerTask, CLEANUP_BACKUP_DOCUMENTS_TIMER_PERIOD, CLEANUP_BACKUP_DOCUMENTS_TIMER_PERIOD);
    }

    @Override
	public void destroy() throws Exception {
        final Timer aTimer = timer;

        if (aTimer != null) {
            aTimer.cancel();
            aTimer.purge();
            timer = null;
        }
    }

}
