/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.manager.impl;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.filemanagement.ManagedFileManagement;
import com.openexchange.office.backup.distributed.GCManagedFiles;
import com.openexchange.office.tools.monitoring.RestoreDocEvent;
import com.openexchange.office.tools.monitoring.RestoreDocEventType;
import com.openexchange.office.tools.monitoring.Statistics;

@Service
public class GCObsoleteStreams implements GCManagedFiles, InitializingBean, DisposableBean {
    
	private static final Logger log = LoggerFactory.getLogger(AsyncBackupWorker.class);
	
	private static final long GC_TIMEOUT_FOR_ENTRY = 30000;
	private static final String THREAD_NAME = "com.openexchange.office.backup.GCObsoleteStreams";

    @Autowired
    private ManagedFileManagement managedFileManagementService;
    
    @Autowired
    private Statistics statistics;    
    
    private final Queue<GCObsoleteStreamEntry> gcManagedFileIds = new ConcurrentLinkedQueue<>();
    
    private Thread gcTaskThread;

    /**
     * Initializes a new GCObsoleteStreams instance which uses a thread-based
     * background processing-to remove obsolete managed files.
     *
     */
	@Override
	public void afterPropertiesSet() throws Exception {
        gcTaskThread = new Thread() {

            @Override
            public void run() {
                boolean bInterrupted = false;

                while (!bInterrupted) {
                	try {
                        final long timeStart = System.currentTimeMillis();

                        log.trace("GCObsoleteStreams starts garbage collection round - {} entries in gc queue", gcManagedFileIds.size());

                    	boolean allTimeupRemoved = false;
                        int nCount = 0; 
                    	while (!allTimeupRemoved) {
    	                    try {
    	                        final GCObsoleteStreamEntry managedFileToBeGC = gcManagedFileIds.peek();

    	                        if (managedFileToBeGC != null) {
        	                        final long diff = Math.abs(System.currentTimeMillis() - managedFileToBeGC.timeAdded());
        	                        if (diff >= GC_TIMEOUT_FOR_ENTRY) {
        	                            final String managedFileId = managedFileToBeGC.managedFileId();
        	
        	                            log.trace("Managed file {} removed by GCObsoleteStreams ", managedFileId);
        	
        	                            nCount++;
        	                            managedFileManagementService.removeByID(managedFileId);
        	                            gcManagedFileIds.poll();
        	
        	                            statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_CURRENT_DEC));
        	                        } else {
        	                            allTimeupRemoved = true;
        	                        }
    	                        } else {
    	                        	allTimeupRemoved = true;
    	                        }

    	                        // make sure that we bail out as soon as possible after being interrupted
    	                        if (Thread.interrupted()) {
    	                        	throw new InterruptedException();
    	                        }
    	                    } catch (InterruptedException e) {
    	                    	Thread.currentThread().interrupt();
    	                    	bInterrupted = true;
    	                    	throw new InterruptedException();
    	                    } catch (Exception e) {
    	                        log.info("RT2: Exception caught while trying to remove an obsolete managed file", e);
    	                    }
                    	}

                        final long nTimeUsed = System.currentTimeMillis() - timeStart;

                        statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_LAST_CYCLE_TIME, nTimeUsed));
                        statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_LAST_CYCLE_REMOVED, nCount));
                        statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.MANAGED_FILES_TO_BE_REMOVED, gcManagedFileIds.size()));

                        log.trace("GCObsoleteStreams garbage collected {} managed files in {} milliseconds", nCount, (Math.abs(System.currentTimeMillis() - timeStart)));

                        Thread.sleep(2000);
                	} catch (InterruptedException e) {
                    	Thread.currentThread().interrupt();
                    	bInterrupted = true;
                    }
                }
            }
        };

        gcTaskThread.setName(THREAD_NAME);
        gcTaskThread.setDaemon(true);
        gcTaskThread.setPriority(Thread.NORM_PRIORITY - 1);
        gcTaskThread.start();
	}	

	/**
	 * Removes pending jobs to be processed and stop the background
	 * processing. 
	 */	
	@Override
	public void destroy() throws Exception {
		log.trace("GCObsoleteStreams.dispose() called");

	    gcManagedFileIds.clear();

	    final Thread aThread = gcTaskThread;
	    if (aThread != null) {
            try
            {
            	aThread.interrupt();
            	aThread.join(1000);
            	gcTaskThread = null;
            }
            catch (final InterruptedException e) {
                log.debug("GCObsoleteStreams: InterruptedException preparing shutdown of the gc thread", e);
                Thread.currentThread().interrupt();
            }
	    }
	}

	/**
	 * Adds a managed file id to be removed by garbage collector.
	 *
	 * @param fileId a managed file id - empty strings or null will be ignored
	 */
	public void addObseleteManagedFileId(String fileId) {
		if (StringUtils.isNotEmpty(fileId)) {

			final GCObsoleteStreamEntry entry = new GCObsoleteStreamEntry(fileId);
			gcManagedFileIds.add(entry);
		}
	}

}
