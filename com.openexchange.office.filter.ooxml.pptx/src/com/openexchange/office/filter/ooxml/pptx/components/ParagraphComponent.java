/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.XmlUtils;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTextCharacterProperties;
import org.docx4j.dml.CTTextField;
import org.docx4j.dml.CTTextLineBreak;
import org.docx4j.dml.CTTextListStyle;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.CTTextParagraphProperties;
import org.docx4j.dml.ITextCharacterProperties;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CTPlaceholder;
import org.pptx4j.pml.STPlaceholderType;
import org.pptx4j.pml.Shape;
import org.pptx4j.pml.SldMaster;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IParagraph;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ParagraphComponent extends PptxComponent implements IParagraph {

    final CTTextParagraph paragraph;

    public ParagraphComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
        paragraph = (CTTextParagraph)_node.getData();
    }

    public CTTextParagraph getParagraph() {
        return paragraph;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> paragraphNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)paragraphNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, paragraphNode.getData());
            if(o instanceof CTRegularTextRun) {
                if(((CTRegularTextRun)o).getT()!=null&&!((CTRegularTextRun)o).getT().isEmpty()) {
                    nextComponent = new TextComponent(this, childNode, nextComponentNumber);
                }
            }
            else if(o instanceof CTTextField) {
                nextComponent = new TextFieldComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTTextLineBreak) {
                nextComponent = new HardBreakComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public void insertText(int textPosition, String text, JSONObject attrs) throws Exception {

        if(text.length()>0) {
            IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
            IComponent<OfficeOpenXMLOperationDocument> cRet = null;

            if(childComponent!=null) {
                if(textPosition>0) {
                    childComponent = childComponent.getComponent(textPosition-1);
                }
                // check if the character could be inserted into an existing text:
                if(childComponent instanceof TextComponent) {
                    final CTRegularTextRun t = (CTRegularTextRun)childComponent.getObject();
                    final StringBuffer s = new StringBuffer(t.getT());
                    s.insert(textPosition-((TextComponent)childComponent).getComponentNumber(), text);
                    t.setT(s.toString());
                    cRet = childComponent;
                }
                else {
                    final CTRegularTextRun newRun = Context.getDmlObjectFactory().createCTRegularTextRun();
                    newRun.setParent(getObject());
                    newRun.setT(text);
                    (paragraph.getContent()).addNode(childComponent.getNode(), new DLNode<Object>(newRun), textPosition == 0 ? true : false);

                    CTTextCharacterProperties referenceRPr = null;
                    if(childComponent.getObject() instanceof ITextCharacterProperties) {
                        referenceRPr = ((ITextCharacterProperties)childComponent.getObject()).getRPr(false);
                    }
                    if(referenceRPr!=null) {
                        final CTTextCharacterProperties newRPr = XmlUtils.deepCopy(referenceRPr, operationDocument.getPackage());
                        if(newRPr!=null) {
                            newRun.setRPr(newRPr);
                        }
                    }
                    if(textPosition==0) {
                        cRet = getNextChildComponent(null, null);
                    }
                    else {
                        cRet = childComponent.getNextComponent();
                    }
                }
          }
          else {
              // the paragraph is empty, we have to create R and its text
              final CTRegularTextRun newRun = Context.getDmlObjectFactory().createCTRegularTextRun();
              newRun.setParent(getObject());
              paragraph.getContent().add(newRun);
              if(paragraph.getEndParaRPr(false)!=null) {
                  newRun.setRPr(XmlUtils.deepCopy(paragraph.getEndParaRPr(true), operationDocument.getPackage()));
              }
              else if(paragraph.getPPr(false)!=null&&paragraph.getPPr(false).getDefRPr(false)!=null) {
                  newRun.setRPr(XmlUtils.deepCopy(paragraph.getPPr(false).getDefRPr(true), operationDocument.getPackage()));
              }
              newRun.setT(text);
              cRet = getNextChildComponent(null, null);
            }
            ((CTRegularTextRun)cRet.getObject()).getRPr(true).setDirty(Boolean.valueOf(false));
            if(attrs!=null) {
                cRet.splitStart(textPosition, SplitMode.ATTRIBUTE);
                cRet.splitEnd(textPosition+text.length()-1, SplitMode.ATTRIBUTE);
                cRet.applyAttrsFromJSON(attrs);
            }
        }
    }

    @Override
    public void splitParagraph(int textPosition) {
        // creating and inserting our new paragraph
        final CTTextParagraph destParagraph = Context.getDmlObjectFactory().createCTTextParagraph();
        destParagraph.setParent(paragraph.getParent());
        final DLNode<Object> destParagraphNode = new DLNode<Object>(destParagraph);
        ((DLList<Object>)((IContentAccessor)paragraph.getParent()).getContent()).addNode(getNode(), destParagraphNode, textPosition==0);

        CTTextCharacterProperties lastRPr = null;
        if(textPosition>0) {
            // splitting the paragraph
            IComponent<OfficeOpenXMLOperationDocument> childComponent = getChildComponent(textPosition-1);
            if(childComponent!=null) {
                lastRPr = ((ITextCharacterProperties)childComponent.getObject()).getRPr(false);
                childComponent.splitEnd(textPosition-1, SplitMode.DELETE);

                // moving text runs into the new paragraph
                DLNode<Object> sourceNode = childComponent.getNode().getNext();
                if(sourceNode!=null) {
                    paragraph.getContent().moveNodes(sourceNode, -1, destParagraph.getContent(), null, true, destParagraph);
                }
            }
        }

        final CTTextParagraphProperties sourceParagraphProperties = paragraph.getPPr(false);
        if(sourceParagraphProperties!=null){
            final CTTextParagraphProperties destParagraphProperties = XmlUtils.deepCopy(sourceParagraphProperties, getOperationDocument().getPackage());
            destParagraph.setPPr(destParagraphProperties);
        }

        // taking care of paragraph attributes
        if(lastRPr!=null) {
            // if available, we have to get the character attributes from the last textrun
            destParagraph.getPPr(true).setDefRPr(XmlUtils.deepCopy(lastRPr, getOperationDocument().getPackage()));
        }
    }

    @Override
    public void mergeParagraph() {
        final IComponent<OfficeOpenXMLOperationDocument> nextParagraphComponent = getNextComponent();
        if(nextParagraphComponent instanceof ParagraphComponent) {
            final DLList<Object> sourceContent = ((CTTextParagraph)nextParagraphComponent.getObject()).getContent();
            final DLList<Object> destContent = paragraph.getContent();
            if(sourceContent.size()>0) {
                sourceContent.moveNodes(destContent, paragraph);
            }
            DLNode<Object> parentContextNode = nextParagraphComponent.getParentContext().getNode();
            ((IContentAccessor)parentContextNode.getData()).getContent().remove(nextParagraphComponent.getObject());
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs)
        throws JSONException, InvalidFormatException, PartUnrecognisedException {

        if(attrs==null) {
            return;
        }
        final boolean hasParagraphAttrs = attrs.hasAndNotNull(OCKey.PARAGRAPH.value());
        final boolean hasCharacterAttrs = attrs.hasAndNotNull(OCKey.CHARACTER.value());
        if(hasParagraphAttrs||hasCharacterAttrs) {
            final IComponent<OfficeOpenXMLOperationDocument> parentComponent = getParentComponent();
            if(parentComponent instanceof IShapeComponent&&((IShapeComponent)parentComponent).isPresentationObject()) {;
                final ShapeComponent shapeComponent = (ShapeComponent)parentComponent;
                final SlideComponent slideComponent = PMLShapeHelper.getSlideComponent(shapeComponent);
                final Part slidePart = (Part)slideComponent.getObject();
                if(slidePart instanceof SlideMasterPart) {
                    final Shape shape = (Shape)shapeComponent.getObject();
                    final CTPlaceholder placeholder = shape.getNvSpPr().getNvPr().getPh();
                    if(placeholder.getType()==STPlaceholderType.TITLE) {
                        final SldMaster sldMaster = ((SlideMasterPart)slidePart).getJaxbElement();
                        applyStyleFormatting(sldMaster.getTxStyles(true).getTitleStyle(true), attrs);
                    }
                    else if(placeholder.getType()==STPlaceholderType.BODY) {
                        final SldMaster sldMaster = ((SlideMasterPart)slidePart).getJaxbElement();
                        applyStyleFormatting(sldMaster.getTxStyles(true).getBodyStyle(true), attrs);
                    }
                    else {
                        // presentation object on master slide
                        applyObjectStyleFormatting(shapeComponent, attrs);
                    }
                }
                else if(slidePart instanceof SlideLayoutPart) {
                    // presentation object on layout slide -> the object style is attributed
                    applyObjectStyleFormatting(shapeComponent, attrs);
                }
                else {
                    // presentation object on normal slide -> the object is hard formatted
                    applyHardFormatting(attrs);
                }
            }
            else {
                // always hard formatting
                applyHardFormatting(attrs);
            }
        }
    }

    private void applyStyleFormatting(CTTextListStyle textListStyle, JSONObject attrs)
        throws InvalidFormatException, PartUnrecognisedException, JSONException {

        checkSetLevel(attrs);
        DMLHelper.applyTextParagraphPropertiesFromJson(textListStyle.getPPr(getLevel(), true), attrs, operationDocument);
    }

    private void applyObjectStyleFormatting(ShapeComponent shapeComponent, JSONObject attrs)
        throws InvalidFormatException, PartUnrecognisedException, JSONException {

        checkSetLevel(attrs);
        DMLHelper.applyTextParagraphPropertiesFromJson(shapeComponent.getTextBody(true), getLevel(), attrs, operationDocument);
    }

    private void applyHardFormatting(JSONObject attrs)
        throws InvalidFormatException, PartUnrecognisedException, JSONException {

        DMLHelper.applyTextParagraphPropertiesFromJson(paragraph.getPPr(true), attrs, operationDocument);
        if(attrs.hasAndNotNull(OCKey.CHARACTER.value())) {
            DMLHelper.applyTextCharacterPropertiesFromJson(paragraph.getEndParaRPr(true), attrs, operationDocument.getContextPart());
        }
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
            throws JSONException, FilterException {

        DMLHelper.createJsonFromTextParagraphProperties(getOperationDocument(), attrs, paragraph.getPPr(false));
        if(paragraph.getEndParaRPr(false)!=null&&paragraph.getContent().isEmpty()) {
            DMLHelper.createJsonFromTextCharacterProperties(getOperationDocument(), attrs, paragraph.getEndParaRPr(false));
        }
        return attrs;
    }

    public void checkSetLevel(JSONObject attrs) {
        if(attrs!=null) {
            final JSONObject paragraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
            if(paragraphAttrs!=null) {
                final Object l = paragraphAttrs.opt(OCKey.LEVEL.value());
                if(l!=null) {
                    paragraph.getPPr(true).setLvl(l instanceof Number ? ((Number)l).intValue() : 0);
                }
            }
        }
    }

    public int getLevel() {
        final CTTextParagraphProperties properties = paragraph.getPPr(false);
        final Integer lvl = properties!=null ? properties.getLvl() : null;
        return lvl!=null ? lvl.intValue() : 0;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(int textPosition, JSONObject attrs, ComponentType childType) throws Exception {

        Object newChild = null;

        CTTextCharacterProperties textCharacterProperties = null;
        if(childType == ComponentType.HARDBREAK_DEFAULT) {
            newChild = Context.getDmlObjectFactory().createCTTextLineBreak();
        }
        else if(childType == ComponentType.SIMPLE_FIELD) {
            newChild = Context.getDmlObjectFactory().createCTTextField();
        }
        IComponent<OfficeOpenXMLOperationDocument> childComponent = getNextChildComponent(null, null);
        if(childComponent!=null) {
            if(textPosition>0) {
                childComponent = childComponent.getComponent(textPosition-1);
                childComponent.splitEnd(textPosition-1, SplitMode.ATTRIBUTE);
            }
            if(childComponent.getObject() instanceof ITextCharacterProperties) {
                textCharacterProperties = ((ITextCharacterProperties)childComponent.getObject()).getRPr(false);
            }
            final IContentAccessor parentContextObject = (IContentAccessor)childComponent.getParentContext().getNode().getData();
            ((DLList<Object>)parentContextObject.getContent()).addNode(childComponent.getNode(), new DLNode<Object>(newChild), textPosition==0);
        }
        else {
            paragraph.getContent().addNode(new DLNode<Object>(newChild));
            textCharacterProperties = paragraph.getEndParaRPr(false);
        }
        if(textPosition>0) {
            childComponent = childComponent.getNextComponent();
        }
        else {
            childComponent = getNextChildComponent(null, null);
        }
        if(textCharacterProperties!=null&&childComponent.getObject() instanceof ITextCharacterProperties) {
            ((ITextCharacterProperties)childComponent.getObject()).setRPr(XmlUtils.deepCopy(textCharacterProperties, operationDocument.getPackage()));
        }
        if(attrs!=null) {
            childComponent.applyAttrsFromJSON(attrs);
        }
        return childComponent;
    }
}
