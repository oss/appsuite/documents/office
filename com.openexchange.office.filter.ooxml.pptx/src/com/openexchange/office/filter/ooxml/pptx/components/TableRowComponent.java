/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTTableCell;
import org.docx4j.dml.CTTableRow;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;

public class TableRowComponent extends PptxComponent {

	final private CTTableRow tableRow;

	public TableRowComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.tableRow = (CTTableRow)getObject();
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLList<Object> nodeList = tableRow.getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        final int nextGridPosition = previousChildComponent instanceof TableCellComponent ? ((TableCellComponent)previousChildComponent).getNextGridPosition() : 0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, tableRow);
            if(o instanceof CTTableCell&&!((CTTableCell)o).isHMerge()) {
            	nextComponent = new TableCellComponent(this, childNode, nextComponentNumber, nextGridPosition);
            }
        }
        return nextComponent;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {

    	if(DMLHelper.applyTableRowFromJson(tableRow, attrs)) {
    		((ShapeGraphicComponent)getParentComponent()).recalcTableHeight();
    	}
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	DMLHelper.createJsonFromTableRow(attrs, tableRow);
    	return attrs;
    }

	public void insertCells(int cellPosition, int count, JSONObject attrs) throws Exception {

	    IComponent<OfficeOpenXMLOperationDocument> c = getChildComponent(cellPosition);
        final DLList<Object> trContent = tableRow.getContent();
        for (int i=0; i<count; i++) {
            final CTTableCell tc = new CTTableCell();
            tc.setParent(tableRow);
            trContent.addNode(c!=null?c.getNode():null, new DLNode<Object>(tc), true);
        }
        if(attrs!=null) {
            c = getChildComponent(cellPosition);
            for(int i=0; i<count; i++) {
            	c.applyAttrsFromJSON(attrs);
            	c = c.getNextComponent();
            }
        }
	}
}
