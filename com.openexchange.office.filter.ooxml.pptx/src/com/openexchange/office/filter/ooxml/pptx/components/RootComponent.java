/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.operations.PptxApplyOperationHelper;

public class RootComponent extends PptxComponent {

	public RootComponent(PptxApplyOperationHelper operationHelper, String target) {
		super(operationHelper.getOperationDocument(), new DLNode<Object>(operationHelper.setContextPartByTarget(target)), 0);
        final IContentAccessor contentAccessor = (IContentAccessor)operationDocument.getContextPart();
        if((contentAccessor instanceof SlideMasterPart) || (contentAccessor instanceof SlideLayoutPart)) {
        	// adding dummy content as the first json address is always 0 and to be ignored
        	this.setNode(new DLNode<Object>(new DummyContentAccessor(contentAccessor)));
        }
    }

	public RootComponent(PptxOperationDocument operationDocument, SerializationPart<?> targetPart) {
	    super(operationDocument, new DLNode<Object>(operationDocument.setContextPart(targetPart)), 0);
        final IContentAccessor contentAccessor = (IContentAccessor)operationDocument.getContextPart();
        if((contentAccessor instanceof SlideMasterPart) || (contentAccessor instanceof SlideLayoutPart)) {
            // adding dummy content as the first json address is always 0 and to be ignored
            this.setNode(new DLNode<Object>(new DummyContentAccessor(contentAccessor)));
        }
	}

    private class DummyContentAccessor implements IContentAccessor {

    	final DLList<Object> content;

    	public DummyContentAccessor(IContentAccessor c) {
    		content = new DLList<Object>();
    		content.add(c);
    	}
		@Override
		public DLList<Object> getContent() {
			return content;
		}
    }

    @Override
    public OfficeOpenXMLComponent getNextComponent() {
        return null;
    }
    @Override
    public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> rootNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)rootNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, rootNode.getData());
            if(o instanceof SlidePart) {
            	operationDocument.setContextPart((SlidePart)o);
            	nextComponent = new SlideComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SlideMasterPart) {
            	operationDocument.setContextPart((SlideMasterPart)o);
            	nextComponent = new SlideComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof SlideLayoutPart) {
            	operationDocument.setContextPart((SlideLayoutPart)o);
            	nextComponent = new SlideComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }
    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

        DLList<Object> DLList;
        DLNode<Object> referenceNode = null;
        if(child!=null&&child.getComponentNumber()==number) {
            final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = child.getContextChild(null);
            DLList = (DLList<Object>)((IContentAccessor)contextChild.getParentContext().getNode().getData()).getContent();
            referenceNode = contextChild.getNode();
        }
        else {
            DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        }

        switch(type) {
            case PARAGRAPH : {
                final Child newChild = Context.getDmlObjectFactory().createCTTextParagraph();
                newChild.setParent(contextNode.getData());
                final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
                DLList.addNode(referenceNode, newChildNode, true);
                return new ParagraphComponent(parentContext, newChildNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }
    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {
        //
    }
    @Override
    public JSONObject createJSONAttrs(JSONObject attrs) {
        return attrs;
    }
}
