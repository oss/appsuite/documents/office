/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTTableCell;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.jaxb.Context;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;

public class TableCellComponent extends PptxComponent {

    private final int gridPosition;
	final private CTTableCell tableCell;

	public TableCellComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber, int _gridPosition) {
        super(parentContext, _node, _componentNumber);

        this.tableCell = (CTTableCell)getObject();
        gridPosition = _gridPosition;
    }

    public int getGridPosition() {
        return gridPosition;
    }

    public int getNextGridPosition() {
        return gridPosition + tableCell.getGridSpan();
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLList<Object> nodeList = tableCell.getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, tableCell.getTxBody());
            if(o instanceof CTTextParagraph) {
            	nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
        	case PARAGRAPH: {
	            final Child newChild = Context.getDmlObjectFactory().createCTTextParagraph();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ParagraphComponent(parentContext, newChildNode, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws JSONException {

    	DMLHelper.applyTableCellFromJson(tableCell, attrs);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	DMLHelper.createJsonFromTableCell(attrs, tableCell, operationDocument.getThemePart(false), operationDocument.getContextPart());
    	return attrs;
    }
}
