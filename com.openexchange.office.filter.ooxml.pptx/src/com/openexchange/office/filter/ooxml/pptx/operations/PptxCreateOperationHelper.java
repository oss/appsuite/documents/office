/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.operations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.docx4j.dml.CTColorMappingOverride;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTableStyle;
import org.docx4j.dml.CTTableStyleList;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextField;
import org.docx4j.dml.CTTextLineBreak;
import org.docx4j.dml.CTTextListStyle;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PresentationML.JaxbPmlPart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.docx4j.openpackaging.parts.PresentationML.TableStylesPart;
import org.docx4j.openpackaging.parts.PresentationML.ViewPropertiesPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CTCommentAuthor;
import org.pptx4j.pml.CTNormalViewPortion;
import org.pptx4j.pml.CTNormalViewProperties;
import org.pptx4j.pml.CTSlideMasterTextStyles;
import org.pptx4j.pml.IComment;
import org.pptx4j.pml.ICommentAuthor;
import org.pptx4j.pml.ICommentAuthorList;
import org.pptx4j.pml.ICommentList;
import org.pptx4j.pml.Presentation;
import org.pptx4j.pml.Presentation.SldMasterIdLst;
import org.pptx4j.pml.Presentation.SldMasterIdLst.SldMasterId;
import org.pptx4j.pml.Presentation.SldSz;
import org.pptx4j.pml.Sld;
import org.pptx4j.pml.SldLayout;
import org.pptx4j.pml.SldMaster;
import org.pptx4j.pml.SlideLayoutIdList;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import org.pptx4j.pml.ViewPr;
import org.pptx4j.pml_2012.CTCommentThreading;
import org.pptx4j.pml_2012.CTParentCommentIdentifier;
import org.pptx4j.pml_2018_8.CTCommentReply;
import org.pptx4j.pml_2018_8.CTCommentReplyList;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.operations.CreateOperationHelper;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.components.HardBreakComponent;
import com.openexchange.office.filter.ooxml.pptx.components.ParagraphComponent;
import com.openexchange.office.filter.ooxml.pptx.components.RootComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TableCellComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TableRowComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TextComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TextFieldComponent;

public class PptxCreateOperationHelper extends CreateOperationHelper {

    private final PresentationMLPackage presentationMLPackage;

    // used only for layout ids ...
    private final HashMap<String, String> relTargetToId = new HashMap<String, String>();

    public PptxCreateOperationHelper(PptxOperationDocument _operationDocument, JSONArray mOperationsArray)
        throws FilterException {

        super(_operationDocument, mOperationsArray);
        presentationMLPackage = _operationDocument.getPackage();
    }

    @Override
    public PptxOperationDocument getOperationDocument() {
        return (PptxOperationDocument)operationDocument;
    }

    public void createDocumentDefaults(JSONObject documentAttributes, String userLanguage)
        throws JSONException, InvalidFormatException  {

        final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
        final RelationshipsPart mainPresentationRelations = mainPresentationPart.getRelationshipsPart();

        getOperationDocument().setContextPart(mainPresentationPart);
        final Presentation presentation = mainPresentationPart.getJaxbElement();

        // core settings (modified/created)
        createCoreProperties(documentAttributes);;
        
        // page settings
        final JSONObject jsonPageSettings = new JSONObject(3);
        final SldSz slideSz = presentation.getSldSz(true);
        jsonPageSettings.put(OCKey.WIDTH.value(), (int)((slideSz.getCx()/360)+0.5));
        jsonPageSettings.put(OCKey.HEIGHT.value(), (int)((slideSz.getCy()/360)+0.5));
        documentAttributes.put(OCKey.PAGE.value(), jsonPageSettings);

        // text default styles
        final CTTextListStyle defaultTextStyle = presentation.getDefaultTextStyle();
        if(defaultTextStyle!=null) {
            final JSONObject textDefaults = DMLHelper.createJsonFromTextListStyle(getOperationDocument(), defaultTextStyle);
            if(textDefaults!=null&&!textDefaults.isEmpty()) {
                final Object defaultCharacterAttributes = textDefaults.remove(OCKey.DEFAULT.value());
                if(!textDefaults.isEmpty()) {
                    documentAttributes.put(OCKey.DEFAULT_TEXT_LIST_STYLES.value(), textDefaults);
                }
                if(defaultCharacterAttributes instanceof JSONObject) {
                    final JSONObject language = ((JSONObject)defaultCharacterAttributes).optJSONObject(OCKey.CHARACTER.value());
                    if(language != null && language.optString(OCKey.LANGUAGE.value(), null) != null) {
                        documentAttributes.put(OCKey.CHARACTER.value(), language);
                    }
                }
            }
        }

        // layout settings
        final Relationship viewRel = mainPresentationRelations.getRelationshipByType(Namespaces.PRESENTATIONML_VIEW_PROPS);
        if(viewRel!=null) {
            final JSONObject layoutSettings = new JSONObject(1);
            final ViewPropertiesPart viewPropertiesPart = (ViewPropertiesPart)mainPresentationRelations.getPart(viewRel);
            final ViewPr viewPr = viewPropertiesPart.getJaxbElement();
            final CTNormalViewProperties normalViewProperties = viewPr.getNormalViewPr();
            if(normalViewProperties!=null) {
                final CTNormalViewPortion leftViewPortion = normalViewProperties.getRestoredLeft();
                if(leftViewPortion!=null) {
                    final double leftViewPortionSz = ((double)leftViewPortion.getSz()) / 1000;
                    layoutSettings.put(OCKey.SLIDE_PANE_WIDTH.value(), leftViewPortionSz);
                }
            }
            if(!layoutSettings.isEmpty()) {
                documentAttributes.put(OCKey.LAYOUT.value(), layoutSettings);
            }
        }

        // create comment authors
        final ICommentAuthorList<?> commentAuthorList = mainPresentationPart.getCommentAuthorList(false);
        if(commentAuthorList!=null) {
            final JSONArray authors = new JSONArray();
            DLNode<? extends ICommentAuthor> commentAuthorNode = (DLNode<? extends ICommentAuthor>) commentAuthorList.getContent().getFirstNode();
            while(commentAuthorNode!=null) {
                final JSONObject author = new JSONObject();
                final ICommentAuthor commentAuthor = commentAuthorNode.getData();
                String authorName = commentAuthor.getName();
                author.put(OCKey.AUTHOR_ID.value(), commentAuthor.getId());
                author.put(OCKey.INITIALS.value(), commentAuthor.getInitials());
                if(commentAuthor instanceof CTCommentAuthor) {
                    author.put(OCKey.COLOR_INDEX.value(), ((CTCommentAuthor)commentAuthor).getClrIdx());
                }
                final String userId = commentAuthor.getUserId();
                if(userId!=null) {
                    author.put(OCKey.USER_ID.value(), userId);
                }
                final String providerId = commentAuthor.getProviderId();
                if(providerId!=null) {
                    author.put(OCKey.PROVIDER_ID.value(), providerId);
                    // removing [ox] if provider id is ox
                    final String suffix = " [~ox]";
                    if("ox".equals(providerId) && authorName.length() > suffix.length() && authorName.endsWith(suffix)) {
                        authorName = authorName.substring(0, authorName.length() - suffix.length());
                    }
                }
                author.put(OCKey.AUTHOR_NAME.value(), authorName);
                authors.put(author);
                commentAuthorNode = commentAuthorNode.getNext();
            }
            documentAttributes.put(OCKey.AUTHORS.value(), authors);
        }
    }

    public void createMasterSlides() throws Exception {

        final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
        final SldMasterIdLst slideMasterIds = mainPresentationPart.getJaxbElement().getSldMasterIdLst();
        if(slideMasterIds!=null) {
            final List<SldMasterId> slideMasterIdList = slideMasterIds.getSldMasterId();
            for(int i=0; i<slideMasterIdList.size(); i++) {
                final SldMasterId slideMasterId = slideMasterIdList.get(i);
                final SlideMasterPart slideMasterPart = (SlideMasterPart)mainPresentationPart.getRelationshipsPart().getPart(slideMasterId.getRid());
                getOperationDocument().setContextPart(slideMasterPart);

                // context part is important to be able to access the correct theme
                if(i==0) {
                    createTableStyles();
                }

                final String masterTarget = slideMasterId.getId().toString();
                createMasterSlideOperations(slideMasterPart, masterTarget);

                // retrieving layouts
                final SlideLayoutIdList slideLayoutIds = slideMasterPart.getJaxbElement().getSldLayoutIdLst(false);
                if(slideLayoutIds!=null) {
                    final List<SldLayoutId> slideLayoutIdList = slideLayoutIds.getSldLayoutId();
                    for(int j=0; j<slideLayoutIdList.size(); j++) {
                        final SldLayoutId slideLayoutId = slideLayoutIdList.get(j);
                        relTargetToId.put(slideMasterPart.getRelationshipsPart().getRelationshipByID(slideLayoutId.getRid()).getTarget(), slideLayoutId.getId().toString());
                        createLayoutSlideOperations((SlideLayoutPart)slideMasterPart.getRelationshipsPart().getPart(slideLayoutId.getRid()), slideLayoutId.getId().toString(), masterTarget);
                    }
                }
                presentationMLPackage.getSourcePartStore().removeSourcePart(slideMasterPart);
                slideMasterPart.setJaxbElement(null);
            }
        }
    }

    public void createSlides() throws Exception {

        final RootComponent rootComponent = new RootComponent(getOperationDocument(), presentationMLPackage.getMainPresentationPart());
        IComponent<OfficeOpenXMLOperationDocument> slideComponent = rootComponent.getNextChildComponent(null, null);
        while (slideComponent!=null) {

            getOperationDocument().setContextPart((Part)slideComponent.getObject());

            final JSONObject jsonInsertSlideOperation = new JSONObject(4);
            final int slideComponentNumber = slideComponent.getComponentNumber();
            jsonInsertSlideOperation.put(OCKey.NAME.value(), OCValue.INSERT_SLIDE.value());
            jsonInsertSlideOperation.put(OCKey.START.value(), slideComponentNumber);
            final SlidePart slidePart = (SlidePart)slideComponent.getObject();

            // retrieving the layout target that has to be used for this slide
            final String slideLayoutTarget = slidePart.getRelationshipsPart().getRelationshipByType(Namespaces.PRESENTATIONML_SLIDE_LAYOUT).getTarget();
            if(slideLayoutTarget!=null) {
                final String slideLayoutTargetId = relTargetToId.get(slideLayoutTarget);
                if(slideLayoutTargetId!=null) {
                    jsonInsertSlideOperation.put(OCKey.TARGET.value(), slideLayoutTargetId);
                }
            }
            createCommonSlideDataOperations(slideComponent, slideComponentNumber, null, jsonInsertSlideOperation, new JSONObject());
            createCommentOperations(slidePart, slideComponentNumber);

            final JaxbPmlPart<?> part = ((JaxbPmlPart<?>)getOperationDocument().getContextPart());
            presentationMLPackage.getSourcePartStore().removeSourcePart(part);
            part.setJaxbElement(null);
            slideComponent = slideComponent.getNextComponent();
        }
    }

    public void createCommentOperations(SlidePart slidePart, int slideComponentNumber) throws InvalidFormatException, JSONException {
        final ICommentList<IComment> comments = (ICommentList<IComment>)presentationMLPackage.getMainPresentationPart().getCommentList(slidePart, false);
        if(comments!=null) {
            final MutableInt cmIdx = new MutableInt(Integer.valueOf(0));
            DLNode<IComment> commentNode = comments.getContent().getFirstNode();
            while(commentNode!=null) {
                final IComment comment = commentNode.getData();
                final JSONObject commentOperation = new JSONObject();
                commentOperation.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
                final JSONArray start = new JSONArray(2);
                start.put(slideComponentNumber);
                start.put(cmIdx.getAndIncrement());
                commentOperation.put(OCKey.START.value(), start);

                final CTPoint2D pos2d = comment.getPos(true);   // TODO: pos, frontend should not require this attribute, it should be optional
                final JSONArray pos = new JSONArray(2);
                pos.put(pos2d.getX());
                pos.put(pos2d.getY());
                commentOperation.put(OCKey.POS.value(), pos);

                final MutableBoolean restorable = new MutableBoolean(true);
                commentOperation.put(OCKey.TEXT.value(), createMentions(getSimpleText(comment, restorable), commentOperation));
                if(!restorable.booleanValue()) {
                    commentOperation.put(OCKey.RESTORABLE.value(), false);
                }
                if (comment.getDt() != null) {
                    String date = comment.getDt().toXMLFormat();
                    if (!date.toLowerCase().endsWith("z")) {
                        date += "Z";
                    }
                    commentOperation.put(OCKey.DATE.value(), date);
                }
                commentOperation.put(OCKey.AUTHOR_ID.value(), comment.getAuthorId());
                commentOperation.put(OCKey.COLOR_INDEX.value(), comment.getIdx());
                if(comment instanceof org.pptx4j.pml.CTComment) {
                    final CTCommentThreading commentThreading = ((org.pptx4j.pml.CTComment)comment).getCommentThreading(false);
                    if(commentThreading!=null) {
                        final CTParentCommentIdentifier parentCm = commentThreading.getParentCm(false);
                        if(parentCm!=null) {
                            if(parentCm.getIdx()!=null) {
                                final IComment parent = (IComment)comments.getCommentByIdx(parentCm.getIdx());
                                if(parent!=null) {
                                    commentOperation.put(OCKey.PARENT.value(), comments.getContent().indexOf(parent));
                                }
                            }
                        }
                    }
                }
                operationsArray.put(commentOperation);
                if(comment instanceof org.pptx4j.pml_2018_8.CTComment) {
                    createCommentReplyOperations((org.pptx4j.pml_2018_8.CTComment)comment, slideComponentNumber, cmIdx);
                }
                commentNode = commentNode.getNext();
            }
        }
    }

    private void createCommentReplyOperations(org.pptx4j.pml_2018_8.CTComment comment, int slideComponentNumber, MutableInt cmIdx) throws JSONException {
        final CTCommentReplyList replyLst = comment.getReplyLst(false);
        if(replyLst!=null) {
            final int parentIdx = cmIdx.getValue() - 1;
            DLNode<CTCommentReply> commentReplyNode = replyLst.getContent().getFirstNode();
            while(commentReplyNode!=null) {
                final CTCommentReply commentReply = commentReplyNode.getData();
                final JSONObject commentOperation = new JSONObject();
                commentOperation.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
                final JSONArray start = new JSONArray(2);
                start.put(slideComponentNumber);
                start.put(cmIdx.getAndIncrement());
                commentOperation.put(OCKey.START.value(), start);

                final JSONArray pos = new JSONArray(2); // TODO: pos, frontend should not require this attribute, it should be optional
                pos.put(0);
                pos.put(0);
                commentOperation.put(OCKey.POS.value(), pos);

                final MutableBoolean restorable = new MutableBoolean(true);
                commentOperation.put(OCKey.TEXT.value(), createMentions(getSimpleText(commentReply.getTxBody(false), restorable), commentOperation));
                if(!restorable.booleanValue()) {
                    commentOperation.put(OCKey.RESTORABLE.value(), false);
                }
                if (commentReply.getDt() != null) {
                    String date = commentReply.getDt().toXMLFormat();
                    if (!date.toLowerCase().endsWith("z")) {
                        date += "Z";
                    }
                    commentOperation.put(OCKey.DATE.value(), date);
                }
                commentOperation.put(OCKey.AUTHOR_ID.value(), commentReply.getAuthorId());
                commentOperation.put(OCKey.PARENT.value(), parentIdx);

                operationsArray.put(commentOperation);
                commentReplyNode = commentReplyNode.getNext();
            }
        }
    }

    private String getSimpleText(IComment comment, MutableBoolean restorable) {
        if(comment instanceof org.pptx4j.pml.CTComment) {
            return ((org.pptx4j.pml.CTComment)comment).getText();
        }
        else if (comment instanceof org.pptx4j.pml_2018_8.CTComment) {
            return getSimpleText(((org.pptx4j.pml_2018_8.CTComment)comment).getTxBody(false), restorable);
        }
        return "";
    }

    private String getSimpleText(CTTextBody txBody, MutableBoolean restorable) {
        if(txBody==null) {
            return "";
        }
        final StringBuilder builder = new StringBuilder();
        boolean firstParagraph = true;
        DLNode<Object> node = txBody.getContent().getFirstNode();
        while(node!=null) {
            if(node.getData() instanceof CTTextParagraph) {
                appendSimpleTextAndMentionsFromParagraph(builder, (CTTextParagraph)node.getData(), firstParagraph, restorable);
                firstParagraph = false;
            }
            else {
                restorable.setFalse();
            }
            node = node.getNext();
        }
        return builder.toString();
    }

    private void appendSimpleTextAndMentionsFromParagraph(StringBuilder builder, CTTextParagraph paragraph, boolean firstParagraph, MutableBoolean restorable) {

        // insert paragraph and apply paragraph attributes
        if(!firstParagraph) {
            builder.append('\n');   // new line except for the first paragraph
        }

        DLNode<Object> node = paragraph.getContent().getFirstNode();
        while(node!=null) {
            final Object o = node.getData();
            if (o instanceof CTRegularTextRun) {
                final String t = ((CTRegularTextRun)o).getT();
                if(t!=null) {
                    builder.append(t);
                }
            }
            else if(o instanceof CTTextField) {
                final String t = ((CTTextField)o).getT();
                if(t!=null) {
                    builder.append(t);
                }
            }
            else if(o instanceof CTTextLineBreak) {
                builder.append('\n');
            }
            else {
                restorable.setFalse(); // everything else seems to be created with versions prior Word 365
            }
            node = node.getNext();
        }
    }

    /*
     * removing each [@...] of a mention
     *
     * returned: the text without '[@]'... a corresponding mentions array is put into the commentOperation
     */
    private String createMentions(String text, JSONObject commentOperation) throws JSONException {
        if(text==null || text.isEmpty()) {
            return text;
        }
        JSONArray jsonArray = null;
        final StringBuilder builder = new StringBuilder(text);
        for(int j, i = 0; (i + 1) < builder.length(); i++) {
            if(builder.charAt(i) == '[' && builder.charAt(i + 1) == '@') {
                for(j = i + 2; j < builder.length(); j++) {
                    if(builder.charAt(j)==']') {
                        final JSONObject mention = new JSONObject(2);
                        mention.put(OCKey.POS.value(), i);
                        // mention.put(OCKey.LENGHT.value(), j - (i + 2));
                        mention.put(OCKey.LENGTH.value(), j - (i + 1));
                        if(jsonArray==null) {
                            jsonArray = new JSONArray();
                            commentOperation.put(OCKey.MENTIONS.value(), jsonArray);
                        }
                        jsonArray.put(mention);
                        builder.deleteCharAt(j);
                        builder.deleteCharAt(i);
                        // builder.deleteCharAt(i);
                        // i = j - 2;
                        i = j - 1;
                        break;
                    }
                }
                if(j == builder.length()) { // there is no ']' -> there are no more mentions
                    break;
                }
            }
        }
        return builder.toString();
    }

    public void createMasterSlideOperations(SlideMasterPart slideMasterPart, String target) throws Exception {

        final RootComponent rootComponent = new RootComponent(getOperationDocument(), slideMasterPart);
        final IComponent<OfficeOpenXMLOperationDocument> slideComponent = rootComponent.getNextChildComponent(null, null);
        if(slideComponent!=null) {
            final JSONObject jsonInsertMasterSlideOperation = new JSONObject(3);
            jsonInsertMasterSlideOperation.put(OCKey.NAME.value(), OCValue.INSERT_MASTER_SLIDE.value());
            jsonInsertMasterSlideOperation.put(OCKey.ID.value(), target);
            final SldMaster slideMaster = (SldMaster)(((JaxbPmlPart<?>)slideComponent.getObject()).getJaxbElement());
            final CTSlideMasterTextStyles txStyles = slideMaster.getTxStyles(false);
            final JSONObject attrs = new JSONObject();
            if(txStyles!=null) {
                final JSONObject styles = new JSONObject();
                final JSONObject titleStyle = DMLHelper.createJsonFromTextListStyle(getOperationDocument(), txStyles.getTitleStyle(true));
                if(titleStyle!=null&&!titleStyle.isEmpty()) {
                    styles.put(OCKey.TITLE.value(), titleStyle);
                }
                final JSONObject bodyStyle = DMLHelper.createJsonFromTextListStyle(getOperationDocument(), txStyles.getBodyStyle(true));
                if(bodyStyle!=null&&!bodyStyle.isEmpty()) {
                    styles.put(OCKey.BODY.value(), bodyStyle);
                }
                final JSONObject otherStyle = DMLHelper.createJsonFromTextListStyle(getOperationDocument(), txStyles.getOtherStyle(true));
                if(otherStyle!=null&&!otherStyle.isEmpty()) {
                    styles.put(OCKey.OTHER.value(), otherStyle);
                }
                if(!styles.isEmpty()) {
                    attrs.put(OCKey.LIST_STYLES.value(), styles);
                }
            }
            createCommonSlideDataOperations(slideComponent, 0, target, jsonInsertMasterSlideOperation, attrs);
        }
    }

    public void createLayoutSlideOperations(SlideLayoutPart slideLayoutPart, String target, String referencedMaster) throws Exception {

        final RootComponent rootComponent = new RootComponent(getOperationDocument(), slideLayoutPart);
        final IComponent<OfficeOpenXMLOperationDocument> slideComponent = rootComponent.getNextChildComponent(null, null);
        if(slideComponent!=null) {
            final JSONObject jsonInsertLayoutSlideOperation = new JSONObject(4);
            jsonInsertLayoutSlideOperation.put(OCKey.NAME.value(), OCValue.INSERT_LAYOUT_SLIDE.value());
            jsonInsertLayoutSlideOperation.put(OCKey.ID.value(), target);
            jsonInsertLayoutSlideOperation.put(OCKey.TARGET.value(), referencedMaster);
            createCommonSlideDataOperations(slideComponent, 0, target, jsonInsertLayoutSlideOperation, new JSONObject());
        }
        final JaxbPmlPart<?> part = ((JaxbPmlPart<?>)getOperationDocument().getContextPart());
        presentationMLPackage.getSourcePartStore().removeSourcePart(part);
        part.setJaxbElement(null);
    }

    public void createCommonSlideDataOperations(IComponent<OfficeOpenXMLOperationDocument> slideComponent, int slide, String target, JSONObject slideInsertOperation, JSONObject attrs) throws Exception {

        ((OfficeOpenXMLComponent)slideComponent).createJSONAttrs(attrs);

        final JaxbPmlPart<?> commonSlidePart = ((JaxbPmlPart<?>)slideComponent.getObject());
        final Object sldElement = commonSlidePart.getJaxbElement();

        if(!attrs.isEmpty()) {
            slideInsertOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(slideInsertOperation);

        if(sldElement instanceof SldMaster) {
            createThemeOperations(target, null, ((SldMaster)sldElement).getClrMap());
        }
        else if(sldElement instanceof SldLayout) {
            createClrMapOvrThemeOperation(commonSlidePart, target, null, ((SldLayout)sldElement).getClrMapOvr());
        }
        else if(sldElement instanceof Sld) {
            final JSONArray start = new JSONArray(1);
            start.put(slide);
            createClrMapOvrThemeOperation(commonSlidePart, target, start, ((Sld)sldElement).getClrMapOvr());
        }

        final ArrayList<Integer> position = new ArrayList<Integer>();
        position.add(slide);
        IComponent<OfficeOpenXMLOperationDocument> shapeComponent = slideComponent.getNextChildComponent(null, null);
        while(shapeComponent!=null) {
            createShapeOperations(shapeComponent, position, target);
            shapeComponent = shapeComponent.getNextComponent();
        }
    }

    public void createClrMapOvrThemeOperation(JaxbPmlPart<?> commonSlidePart, String target, JSONArray start, CTColorMappingOverride colorOverride)
        throws JSONException {

        if(colorOverride!=null) {
            if(colorOverride.getOverrideClrMapping()!=null) {
                createThemeOperations(target, start, colorOverride.getOverrideClrMapping());
            }
            else if(colorOverride.getMasterClrMapping()!=null) {
                // check if the parent slideLayout and or master is using color mapping that is not default...
                if(commonSlidePart instanceof SlidePart) {
                    final SlideLayoutPart slideLayoutPart = ((SlidePart)commonSlidePart).getSlideLayoutPart();
                    if(slideLayoutPart!=null) {
                        // TODO: how is it possible to have an slide layout part without jaxbelement ?
                        if(slideLayoutPart.getJaxbElement()!=null) {
                            if(!slideLayoutPart.isDefaultColorMapping()) {
                                createThemeOperations(target, start, null);
                            }
                        }
                    }
                }
/* docs-2436 ... the mapping of the master page has to be used if the MasterClrMapping element is set
                else if (commonSlidePart instanceof SlideLayoutPart) {
                    if(!((SlideLayoutPart)commonSlidePart).isDefaultColorMapping()) {
                        createThemeOperations(target, start, null);
                    }
                }
*/
            }
        }
    }

    public void createShapeOperations(IComponent<OfficeOpenXMLOperationDocument> shapeComponent, List<Integer> parentPosition, String target) throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(shapeComponent.getComponentNumber());

        final JSONObject jsonInsertOperation = new JSONObject(5);
        jsonInsertOperation.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        jsonInsertOperation.put(OCKey.TYPE.value(), ((IShapeType)shapeComponent).getType().toString());
        jsonInsertOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = ((OfficeOpenXMLComponent)shapeComponent).createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertOperation);
        IComponent<OfficeOpenXMLOperationDocument> shapeChildComponent = shapeComponent.getNextChildComponent(null, null);
        while(shapeChildComponent!=null) {
            if(shapeChildComponent instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)shapeChildComponent, position, target);
            }
            else if(shapeChildComponent instanceof TableRowComponent) {
                createTableRowOperation((TableRowComponent)shapeChildComponent, position, target);
            }
            else {
                createShapeOperations(shapeChildComponent, position, target);
            }
            shapeChildComponent = shapeChildComponent.getNextComponent();
        }
    }

    private void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> parentPosition, String target)
        throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(paragraphComponent.getComponentNumber());
        final JSONObject jsonInsertParagraphOperation = new JSONObject(4);
        jsonInsertParagraphOperation.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        jsonInsertParagraphOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertParagraphOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject paragraphAttrs = paragraphComponent.createJSONAttrs(new JSONObject());
        if(!paragraphAttrs.isEmpty()) {
            jsonInsertParagraphOperation.put(OCKey.ATTRS.value(), paragraphAttrs);
        }
        operationsArray.put(jsonInsertParagraphOperation);

        JSONObject currentAttrs = new JSONObject();
        IComponent<OfficeOpenXMLOperationDocument> paragraphChild = paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            final JSONObject attrs = getOperationDocument().isCreateFastLoadOperations() ? ((OfficeOpenXMLComponent)paragraphChild).createJSONAttrs(new JSONObject()) : createDifference(currentAttrs, ((OfficeOpenXMLComponent)paragraphChild).createJSONAttrs(new JSONObject()));
            if(paragraphChild instanceof TextComponent) {
                int n = paragraphChild.getComponentNumber();
                final String[] tokens =
                    (((CTRegularTextRun)(((TextComponent)paragraphChild).getObject())).getT()).split("\\t", -1);
                for(int i=0; i < tokens.length; i++) {
                    final String t = tokens[i];
                    if(!t.isEmpty()) {
                        createTextInsertOperation(t, n, position, target, attrs);
                        n += t.length();
                    }
                    if(tokens.length!=1&&i!=tokens.length-1) {
                        createInsertTabOperation(n, position, target, attrs);
                        n++;
                    }
                }
            }
            else if(paragraphChild instanceof TextFieldComponent) {
                createInsertFieldOperation((TextFieldComponent)paragraphChild, position, target, attrs);
            }
            else if(paragraphChild instanceof HardBreakComponent) {
                createHardBreakOperation((HardBreakComponent)paragraphChild, position, target, attrs);
            }
            paragraphChild = paragraphChild.getNextComponent();
        }
    }

    public void createTableRowOperation(TableRowComponent tableRowComponent, List<Integer> parentPosition, String target)
        throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(tableRowComponent.getComponentNumber());
        final JSONObject jsonInsertRowOperation = new JSONObject(4);
        jsonInsertRowOperation.put(OCKey.NAME.value(), OCValue.INSERT_ROWS.value());
        jsonInsertRowOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertRowOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = tableRowComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertRowOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertRowOperation);

        IComponent<OfficeOpenXMLOperationDocument> tableRowChild = tableRowComponent.getNextChildComponent(null, null);
        while(tableRowChild!=null) {
            if(tableRowChild instanceof TableCellComponent) {
                createTableCellOperation((TableCellComponent)tableRowChild, position, target);
            }
            tableRowChild = tableRowChild.getNextComponent();
        }
    }

    public void createTableCellOperation(TableCellComponent tableCellComponent, List<Integer> parentPosition, String target)
        throws Exception {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(tableCellComponent.getComponentNumber());
        final JSONObject jsonInsertRowOperation = new JSONObject(4);
        jsonInsertRowOperation.put(OCKey.NAME.value(), OCValue.INSERT_CELLS.value());
        jsonInsertRowOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertRowOperation.put(OCKey.TARGET.value(), target);
        }
        final JSONObject attrs = tableCellComponent.createJSONAttrs(new JSONObject());
        if(!attrs.isEmpty()) {
            jsonInsertRowOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertRowOperation);

        IComponent<OfficeOpenXMLOperationDocument> tableCellChild = tableCellComponent.getNextChildComponent(null, null);
        while(tableCellChild!=null) {
            if(tableCellChild instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)tableCellChild, position, target);
            }
            tableCellChild = tableCellChild.getNextComponent();
        }
    }

    public void createTextInsertOperation(String text, int textPosition, List<Integer> parentPosition, String target, JSONObject attrs)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textPosition);

        final JSONObject jsonInsertTextOperation = new JSONObject(4);
        jsonInsertTextOperation.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        jsonInsertTextOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTextOperation.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null) {
            jsonInsertTextOperation.put(OCKey.ATTRS.value(), attrs);
        }
        jsonInsertTextOperation.put(OCKey.TEXT.value(), text);
        operationsArray.put(jsonInsertTextOperation);
    }

    public void createInsertTabOperation(int textPosition, List<Integer> parentPosition, String target, JSONObject attrs)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textPosition);

        final JSONObject jsonInsertTabOperation = new JSONObject(3);
        jsonInsertTabOperation.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        jsonInsertTabOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTabOperation.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null) {
            jsonInsertTabOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertTabOperation);
    }

    public void createInsertFieldOperation(TextFieldComponent textFieldComponent, List<Integer> parentPosition, String target, JSONObject attrs)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(textFieldComponent.getComponentNumber());

        final JSONObject jsonInsertSimpleFieldOperation = new JSONObject(6);
        jsonInsertSimpleFieldOperation.put(OCKey.NAME.value(), OCValue.INSERT_FIELD.value());
        jsonInsertSimpleFieldOperation.put(OCKey.START.value(), position);
        final CTTextField textField = (CTTextField)textFieldComponent.getObject();
        jsonInsertSimpleFieldOperation.put(OCKey.TYPE.value(), textField.getType());
        jsonInsertSimpleFieldOperation.put(OCKey.REPRESENTATION.value(), textField.getT());
        if(target!=null) {
            jsonInsertSimpleFieldOperation.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            jsonInsertSimpleFieldOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertSimpleFieldOperation);
    }

    public void createHardBreakOperation(HardBreakComponent hardBreakComponent, List<Integer> parentPosition, String target, JSONObject attrs)
        throws JSONException {

        final List<Integer> position = new ArrayList<Integer>(parentPosition);
        position.add(hardBreakComponent.getComponentNumber());

        final JSONObject jsonInsertHardBreakOperation = new JSONObject(4);
        jsonInsertHardBreakOperation.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        jsonInsertHardBreakOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertHardBreakOperation.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            jsonInsertHardBreakOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationsArray.put(jsonInsertHardBreakOperation);
    }

    public void createTableStyles()
        throws JSONException, InvalidFormatException {

        final TableStylesPart tableStylesPart = this.getOperationDocument().getTableStylesPart(false);
        if(tableStylesPart!=null) {
            final Object o = tableStylesPart.getJaxbElement();
            if(o instanceof CTTableStyleList) {
                final CTTableStyleList tableStyleList = (CTTableStyleList)o;
                final Iterator<CTTableStyle> tableStyleIter = tableStyleList.getTblStyle().iterator();
                while(tableStyleIter.hasNext()) {
                    final CTTableStyle tableStyle = tableStyleIter.next();
                    final String tableStyleId = tableStyle.getStyleId();
                    if(tableStyleId!=null&&!tableStyleId.isEmpty()) {
                        final JSONObject attrs = new JSONObject();
                        DMLHelper.createJsonFromTableStyle(attrs, tableStyle, getOperationDocument().getThemePart(false), tableStylesPart);
                        if(!attrs.isEmpty()) {
                            operationsArray.put(createInsertStyleSheetOperation("table", false, tableStyleId, tableStyle.getStyleName(), attrs, null, false, null, tableStyleId.equals(tableStyleList.getDef()), null));
                        }
                    }
                }
            }
        }
    }
}
