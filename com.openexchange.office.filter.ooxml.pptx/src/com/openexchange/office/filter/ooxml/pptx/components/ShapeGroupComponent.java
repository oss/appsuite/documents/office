/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTNonVisualDrawingProps;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CTGraphicalObjectFrame;
import org.pptx4j.pml.CxnSp;
import org.pptx4j.pml.GroupShape;
import org.pptx4j.pml.Pic;
import org.pptx4j.pml.Shape;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ShapeGroupComponent extends PptxComponent implements IShapeComponent, IShapeType {

	final GroupShape groupShape;

	public ShapeGroupComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        groupShape = (GroupShape)getObject();
	}

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> groupShapeNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)groupShapeNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, groupShapeNode.getData());
            if(o instanceof GroupShape) {
            	nextComponent = new ShapeGroupComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Shape) {
            	nextComponent = new ShapeComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTGraphicalObjectFrame) {
            	nextComponent = new ShapeGraphicComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CxnSp) {
            	nextComponent = new ShapeConnectorComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Pic) {
            	nextComponent = new ShapePicComponent(this, childNode, nextComponentNumber);
            }
/*
            else if(o instanceof CTRel) {
            	nextComponent = new ShapeRelComponent(this, childNode, nextComponentNumber);
            }
*/
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        groupShape.setModified();

        switch(type) {
        	case AC_SHAPE: {
	            final Child newChild = PMLShapeHelper.createShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeComponent(parentContext, newChildNode, number);
        	}
        	case AC_GROUP: {
	            final Child newChild = PMLShapeHelper.createGroupShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeGroupComponent(parentContext, newChildNode, number);
        	}
        	case AC_CONNECTOR: {
	            final Child newChild = PMLShapeHelper.createConnectorShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeConnectorComponent(parentContext, newChildNode, number);
        	}
        	case AC_IMAGE: {
	            final Child newChild = PMLShapeHelper.createImage();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapePicComponent(parentContext, newChildNode, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

    	PMLShapeHelper.applyAttrsFromJSON(operationDocument, attrs, groupShape);
    	com.openexchange.office.filter.ooxml.drawingml.GroupShape.applyAttrsFromJSON(operationDocument, attrs, groupShape, !(getParentComponent() instanceof ShapeGroupComponent));
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	PMLShapeHelper.createJSONAttrs(attrs, groupShape);
        com.openexchange.office.filter.ooxml.drawingml.GroupShape.createJSONAttrs(operationDocument, attrs, groupShape, !(getParentComponent() instanceof ShapeGroupComponent));
    	return attrs;
    }

	@Override
	public Integer getId() {
		final CTNonVisualDrawingProps nvdProps = groupShape.getNonVisualDrawingProperties(false);
		return nvdProps!=null ? nvdProps.getId() : null;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.GROUP;
	}

	@Override
	public boolean isPresentationObject() {
		return false;
	}
}
