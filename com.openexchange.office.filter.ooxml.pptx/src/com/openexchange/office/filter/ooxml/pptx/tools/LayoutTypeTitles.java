/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.tools;

import java.util.Map;
import com.google.common.collect.ImmutableMap;

//everything in this file has to be ascii encoded. a conversion and vice versa can be done with the tool native2ascii

final public class LayoutTypeTitles {

	static final public ImmutableMap<String, String> en = new ImmutableMap.Builder<String, String>()
	    .put("master",                  "Master")
	    .put("blank",                   "Blank")
	    .put("chart",                   "Title and chart")
	    .put("chartAndTx",              "Title, chart on left and text on right")
	    .put("clipArtAndTx",            "Title, clipart on left, text on right")
	    .put("clipArtAndVertTx",        "Title, clip art on left, vertical text on right")
	    .put("cust",                    "Custom")
	    .put("dgm",                     "Title and diagram")
	    .put("fourObj",                 "Title and four objects")
	    .put("mediaAndTx",              "Title, media on left, text on right")
	    .put("obj",                     "Title and Content")
	    .put("objAndTwoObj",            "Title, one object on left, two objects on right")
	    .put("objAndTx",                "Title, object on left, text on right")
	    .put("objOnly",                 "Object only")
	    .put("objOverTx",               "Title, object on top, text on bottom")
	    .put("objTx",                   "Content with Caption")
	    .put("picTx",                   "Picture with Caption")
	    .put("secHead",                 "Section Header")
	    .put("tbl",                     "Title and table")
	    .put("title",                   "Title Slide")
	    .put("titleOnly",               "Title Only")
	    .put("twoColTx",                "Title, text on left, text on right")
	    .put("twoObj",                  "Two Content")
	    .put("twoObjAndObj",            "Title, two objects on left, one object on right")
	    .put("twoObjAndTx",             "Title, two objects on left, text on right")
	    .put("twoObjOverTx",            "Title, two objects on top, text on bottom")
	    .put("twoTxTwoObj",             "Title, 2 Content and Text")
	    .put("tx",                      "Title and text")
	    .put("txAndChart",              "Title, text on left and chart on right")
	    .put("txAndClipArt",            "Title, text on left, clip art on right")
	    .put("txAndMedia",              "Title, text on left, media on right")
	    .put("txAndObj",                "Title, text on left, object on right")
	    .put("txAndTwoObj",             "Title, text on left, two objects on right")
	    .put("txOverObj",               "Title, text on top, object on bottom")
	    .put("vertTitleAndTx",          "Vertical Title and Text")
	    .put("vertTitleAndTxOverChart", "Vertical title on right, vertical text on top, chart on bottom")
	    .put("vertTx",                  "Title and Vertical Text")
	    .build();

	static final ImmutableMap<String, String> fr = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Principale")
	    .put("cust",            "Personnalis\u00e9")
		.put("title",           "Diapositive de titre")
	    .put("obj",             "Titre et contenu")
	    .put("secHead",         "Titre de section")
	    .put("twoObj",          "Deux contenus")
	    .put("twoTxTwoObj",     "Titre. 2 contenus et texte")
	    .put("titleOnly",       "Titre")
	    .put("blank",           "Vide")
	    .put("objTx",           "Contenu avec l\u00e9gende")
	    .put("picTx",           "Image avec l\u00e9gende")
	    .put("vertTx",          "Titre et texte vertical")
	    .put("vertTitleAndTx",  "Titre vertical et texte")
	    .build();

	static final ImmutableMap<String, String> pt = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Modelo Global")
	    .put("cust",            "Personalizada")
	    .put("title",           "Diapositivo de T\u00edtulo")
	    .put("obj",             "T\u00edtulo e Objeto")
	    .put("secHead",         "Cabe\u00e7alho da Sec\u00e7\u00e3o")
	    .put("twoObj",          "Conte\u00fado Duplo")
	    .put("twoTxTwoObj",     "T\u00edtulo, 2 objetos e texto")
	    .put("titleOnly",       "S\u00f3 T\u00edtulo")
	    .put("blank",           "Em branco")
	    .put("objTx",           "Conte\u00fado com Legenda")
	    .put("picTx",           "Imagem com Legenda")
	    .put("vertTx",          "T\u00edtulo e Texto Vertical")
	    .put("vertTitleAndTx",  "T\u00edtulo Vertical e Texto")
		.build();

	static final ImmutableMap<String, String> ptBR = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Mestre")
	    .put("cust",            "Personalizado")
	    .put("title",           "Slide de T\u00edtulo")
	    .put("obj",             "T\u00edtulo e Conte\u00fado")
	    .put("secHead",         "Cabe\u00e7alho da Se\u00e7\u00e3o")
	    .put("twoObj",          "Duas Partes de Conte\u00fado")
	    .put("twoTxTwoObj",     "T\u00edtulo, 2 partes de conte\u00fado e texto")
	    .put("titleOnly",       "Somente T\u00edtulo")
	    .put("blank",           "Em branco")
	    .put("objTx",           "Conte\u00fado com Legenda")
	    .put("picTx",           "Imagem com Legenda")
	    .put("vertTx",          "T\u00edtulo e Texto Vertical")
	    .put("vertTitleAndTx",  "T\u00edtulo Vertical e Texto")
	    .build();

	static final ImmutableMap<String, String> ca = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Patr\u00f3")
	    .put("cust",            "Personalitzat")
	    .put("title",           "Diapositiva de t\u00edtol")
	    .put("obj",             "T\u00edtol i objectes")
	    .put("secHead",         "Cap\u00e7alera de la secci\u00f3")
	    .put("twoObj",          "Dos objectes")
	    .put("twoTxTwoObj",     "T\u00edtol, 2 objectes i text")
	    .put("titleOnly",       "Nom\u00e9s t\u00edtol")
	    .put("blank",           "En blanc")
	    .put("objTx",           "Contingut amb llegenda")
	    .put("picTx",           "Imatge amb llegenda")
	    .put("vertTx",          "T\u00edtol i text vertical")
	    .put("vertTitleAndTx",  "T\u00edtol vertical i text")
	    .build();

	static final ImmutableMap<String, String> es = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Patr\u00f3n")
	    .put("cust",            "Personalizado")
	    .put("title",           "Diapositiva de t\u00edtulo")
	    .put("obj",             "T\u00edtulo y objetos")
	    .put("secHead",         "Encabezado de secci\u00f3n")
	    .put("twoObj",          "Dos objetos")
	    .put("twoTxTwoObj",     "T\u00edtulo, 2 objetos y texto")
	    .put("titleOnly",       "T\u00edtulo")
	    .put("blank",           "En blanco")
	    .put("objTx",           "Contenido con t\u00edtulo")
	    .put("picTx",           "Imagen con t\u00edtulo")
	    .put("vertTx",          "T\u00edtulo y texto vertical")
	    .put("vertTitleAndTx",  "T\u00edtulo vertical y texto")
	    .build();

	static final ImmutableMap<String, String> it = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Schema")
	    .put("cust",            "Personalizzato")
	    .put("title",           "Diapositiva titolo")
	    .put("obj",             "Titolo e contenuto")
	    .put("secHead",         "Intestazione sezione")
	    .put("twoObj",          "Due contenuti")
	    .put("twoTxTwoObj",     "Titolo, contenuto 2 e testo")
	    .put("titleOnly",       "Titolo")
	    .put("blank",           "Vuota")
	    .put("objTx",           "Contenuto con didascalia")
	    .put("picTx",           "Immagine con didascalia")
	    .put("vertTx",          "Titolo e testo verticale")
	    .put("vertTitleAndTx",  "Titolo verticale e testo")
	    .build();

	static final ImmutableMap<String, String> ro = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Coordonator")
	    .put("cust",            "Particularizare")
	    .put("title",           "Titlu diapozitiv")
	    .put("obj",             "Titlu \u0219i con\u021binut")
	    .put("secHead",         "Antet sec\u021biune")
	    .put("twoObj",          "Dou\u0103 tipuri de con\u021binut")
	    .put("twoTxTwoObj",     "Titlu, con\u021binut 2 \u0219i text")
	    .put("titleOnly",       "Doar titlu")
	    .put("blank",           "Necompletat")
	    .put("objTx",           "Con\u021binut cu legend\u0103")
	    .put("picTx",           "Imagine cu legend\u0103")
	    .put("vertTx",          "Text vertical \u0219i titlu")
	    .put("vertTitleAndTx",  "Titlu vertical \u0219i text")
	    .build();

	static final ImmutableMap<String, String> cs = new ImmutableMap.Builder<String, String>()
	    .put("master",          "P\u0159edloha")
	    .put("cust",            "Vlastn\u00ed")
	    .put("title",           "\u00davodn\u00ed sn\u00edmek")
	    .put("obj",             "Nadpis a obsah")
	    .put("secHead",         "Z\u00e1hlav\u00ed \u010d\u00e1sti")
	    .put("twoObj",          "Dva obsahy")
	    .put("twoTxTwoObj",     "Nadpis, 2 obsahy a text")
	    .put("titleOnly",       "Nadpis")
	    .put("blank",           "Pr\u00e1zdn\u00fd")
	    .put("objTx",           "Obsah s titulkem")
	    .put("picTx",           "Obr\u00e1zek s titulkem")
	    .put("vertTx",          "Nadpis a svisl\u00fd text")
	    .put("vertTitleAndTx",  "Svisl\u00fd nadpis a text")
	    .build();

	static final ImmutableMap<String, String> da = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Master")
	    .put("cust",            "Brugerdefineret")
	    .put("title",           "Titelslide")
	    .put("obj",             "Titel og indholdsobjekt")
	    .put("secHead",         "Afsnitsoverskrift")
	    .put("twoObj",          "To indholdsobjekter")
	    .put("twoTxTwoObj",     "Titel, to indholdsobjekter og tekst")
	    .put("titleOnly",       "Kun titel")
	    .put("blank",           "Tom")
	    .put("objTx",           "Indhold med billedtekst")
	    .put("picTx",           "Billede med billedtekst")
	    .put("vertTx",          "Titel og lodret tekst")
	    .put("vertTitleAndTx",  "Lodret titel og tekst")
	    .build();

	static final ImmutableMap<String, String> de = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Master")
	    .put("cust",            "Benutzerdefiniert")
	    .put("title",           "Titelfolie")
	    .put("obj",             "Titel und Inhalt")
	    .put("secHead",         "Abschnitts\u00fcberschrift")
	    .put("twoObj",          "Zwei Inhalte")
	    .put("twoTxTwoObj",     "Titel, zwei Inhalte und Text")
	    .put("titleOnly",       "Nur Titel")
	    .put("blank",           "Leer")
	    .put("objTx",           "Inhalt mit Beschriftung")
	    .put("picTx",           "Bild mit Beschriftung")
	    .put("vertTx",          "Titel und vertikaler Text")
	    .put("vertTitleAndTx",  "Vertikaler Titel und Text")
	    .build();

    static final ImmutableMap<String, String> el = new ImmutableMap.Builder<String, String>()
        .put("master",          "\u03a5\u03c0\u03cc\u03b4\u03b5\u03b9\u03b3\u03bc\u03b1")
        .put("cust",            "\u0395\u03b8\u03b9\u03bc\u03bf")
        .put("title",           "\u0394\u03b9\u03b1\u03c6\u03ac\u03bd\u03b5\u03b9\u03b1 \u03c4\u03af\u03c4\u03bb\u03bf\u03c5")
        .put("obj",             "\u03a4\u03af\u03c4\u03bb\u03bf\u03c2 \u03ba\u03b1\u03b9 \u03c0\u03b5\u03c1\u03b9\u03b5\u03c7\u03cc\u03bc\u03b5\u03bd\u03bf")
        .put("secHead",         "\u039a\u03b5\u03c6\u03b1\u03bb\u03af\u03b4\u03b1 \u03b5\u03bd\u03cc\u03c4\u03b7\u03c4\u03b1\u03c2")
        .put("twoObj",          "\u0394\u03cd\u03bf \u03c0\u03b5\u03c1\u03b9\u03b5\u03c7\u03cc\u03bc\u03b5\u03bd\u03b1")
        .put("twoTxTwoObj",     "\u03a3\u03cd\u03b3\u03ba\u03c1\u03b9\u03c3\u03b7")
        .put("titleOnly",       "\u039c\u03cc\u03bd\u03bf \u03c4\u03af\u03c4\u03bb\u03bf\u03c2")
        .put("blank",           "\u039a\u03b5\u03bd\u03cc")
        .put("objTx",           "\u03a0\u03b5\u03c1\u03b9\u03b5\u03c7\u03cc\u03bc\u03b5\u03bd\u03bf \u03bc\u03b5 \u03bb\u03b5\u03b6\u03ac\u03bd\u03c4\u03b1")
        .put("picTx",           "\u0395\u03b9\u03ba\u03cc\u03bd\u03b1 \u03bc\u03b5 \u03bb\u03b5\u03b6\u03ac\u03bd\u03c4\u03b1")
        .put("vertTx",          "\u03a4\u03af\u03c4\u03bb\u03bf\u03c2 \u03ba\u03b1\u03b9 \u039a\u03b1\u03c4\u03b1\u03ba\u03cc\u03c1\u03c5\u03c6\u03bf \u03ba\u03b5\u03af\u03bc\u03b5\u03bd\u03bf")
        .put("vertTitleAndTx",  "\u039a\u03b1\u03c4\u03b1\u03ba\u03cc\u03c1\u03c5\u03c6\u03bf\u03c2 \u03c4\u03af\u03c4\u03bb\u03bf\u03c2 \u03ba\u03b1\u03b9 \u039a\u03b5\u03af\u03bc\u03b5\u03bd\u03bf")
        .build();

	static final ImmutableMap<String, String> fi = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Perustyyli")
	    .put("cust",            "Mukautettu")
	    .put("title",           "Otsikkodia")
	    .put("obj",             "Otsikko ja sis\u00e4lt\u00f6")
	    .put("secHead",         "Osan yl\u00e4tunniste")
	    .put("twoObj",          "Kaksi sis\u00e4lt\u00f6kohdetta")
	    .put("twoTxTwoObj",     "Otsikko, kaksi sis\u00e4lt\u00f6kohdetta ja teksti")
	    .put("titleOnly",       "Otsikko")
	    .put("blank",           "Tyhj\u00e4")
	    .put("objTx",           "Otsikollinen sis\u00e4lt\u00f6")
	    .put("picTx",           "Otsikollinen kuva")
	    .put("vertTx",          "Otsikko ja pystysuora teksti")
	    .put("vertTitleAndTx",  "Pystysuora otsikko ja teksti")
	    .build();

	static final ImmutableMap<String, String> hu = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Minta")
	    .put("cust",            "Egy\u00e9ni")
	    .put("title",           "C\u00edmdia")
	    .put("obj",             "C\u00edm \u00e9s tartalom")
	    .put("secHead",         "Szakaszfejl\u00e9c")
	    .put("twoObj",          "2 tartalomr\u00e9sz")
	    .put("twoTxTwoObj",     "C\u00edm, 2 objektum \u00e9s sz\u00f6veg")
	    .put("titleOnly",       "Csak c\u00edm")
	    .put("blank",           "\u00dcres")
	    .put("objTx",           "Tartalomr\u00e9sz k\u00e9pal\u00e1\u00edr\u00e1ssal")
	    .put("picTx",           "K\u00e9p k\u00e9pal\u00e1\u00edr\u00e1ssal")
	    .put("vertTx",          "C\u00edm \u00e9s f\u00fcgg\u0151leges sz\u00f6veg")
	    .put("vertTitleAndTx",  "F\u00fcgg\u0151leges c\u00edm \u00e9s sz\u00f6veg")
	    .build();

	static final ImmutableMap<String, String> ja = new ImmutableMap.Builder<String, String>()
	    .put("master",          "\u30de\u30b9\u30bf\u30fc")
	    .put("cust",            "\u30e6\u30fc\u30b6\u30fc\u8a2d\u5b9a")
	    .put("title",           "\u30bf\u30a4\u30c8\u30eb \u30b9\u30e9\u30a4\u30c9")
	    .put("obj",             "\u30bf\u30a4\u30c8\u30eb\u3068\u30b3\u30f3\u30c6\u30f3\u30c4")
	    .put("secHead",         "\u30bb\u30af\u30b7\u30e7\u30f3\u898b\u51fa\u3057")
	    .put("twoObj",          "2 \u3064\u306e\u30b3\u30f3\u30c6\u30f3\u30c4")
	    .put("twoTxTwoObj",     "\u30bf\u30a4\u30c8\u30eb\u30012 \u3064\u306e\u30b3\u30f3\u30c6\u30f3\u30c4\u3001\u30c6\u30ad\u30b9\u30c8")
	    .put("titleOnly",       "\u30bf\u30a4\u30c8\u30eb\u306e\u307f")
	    .put("blank",           "\u767d\u7d19")
	    .put("objTx",           "\u30bf\u30a4\u30c8\u30eb\u4ed8\u304d\u306e\u30b3\u30f3\u30c6\u30f3\u30c4")
	    .put("picTx",           "\u30bf\u30a4\u30c8\u30eb\u4ed8\u304d\u306e\u56f3")
	    .put("vertTx",          "\u30bf\u30a4\u30c8\u30eb\u3068\u7e26\u66f8\u304d\u30c6\u30ad\u30b9\u30c8")
	    .put("vertTitleAndTx",  "\u7e26\u66f8\u304d\u30bf\u30a4\u30c8\u30eb\u3068\u7e26\u66f8\u304d\u30c6\u30ad\u30b9\u30c8")
	    .build();

	static final ImmutableMap<String, String> lv = new ImmutableMap.Builder<String, String>()
	    .put("master",          "\u0160ablons")
	    .put("cust",            "Piel\u0101gots")
	    .put("title",           "Virsraksta slaids")
	    .put("obj",             "Virsraksts un saturs")
	    .put("secHead",         "Sada\u013cas galvene")
	    .put("twoObj",          "Divi satura bloki")
	    .put("twoTxTwoObj",     "Virsraksts, 2 saturi un teksts")
	    .put("titleOnly",       "Tikai virsraksts")
	    .put("blank",           "Tuk\u0161s")
	    .put("objTx",           "Saturs ar parakstu")
	    .put("picTx",           "Att\u0113ls ar parakstu")
	    .put("vertTx",          "Virsraksts un vertik\u0101ls teksts")
	    .put("vertTitleAndTx",  "Vertik\u0101ls virsraksts un teksts")
	    .build();

	static final ImmutableMap<String, String> nb = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Mal")
	    .put("cust",            "Egendefinert")
	    .put("title",           "Tittellysbilde")
	    .put("obj",             "Tittel og innhold")
	    .put("secHead",         "Deloverskrift")
	    .put("twoObj",          "To innholdsdeler")
	    .put("twoTxTwoObj",     "Tittel, to innholdsdeler og tekst")
	    .put("titleOnly",       "Bare tittel")
	    .put("blank",           "Tomt")
	    .put("objTx",           "Innhold med tekst")
	    .put("picTx",           "Bilde med tekst")
	    .put("vertTx",          "Loddrett tekst")
	    .put("vertTitleAndTx",  "Loddrett tittel og tekst")
	    .build();

	static final ImmutableMap<String, String> nl = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Model")
	    .put("cust",            "Aangepast")
	    .put("title",           "Titeldia")
	    .put("obj",             "Titel en object")
	    .put("secHead",         "Sectiekop")
	    .put("twoObj",          "Inhoud van twee")
	    .put("twoTxTwoObj",     "Titel, 2 inhoudselementen en tekst")
	    .put("titleOnly",       "Alleen titel")
	    .put("blank",           "Leeg")
	    .put("objTx",           "Inhoud met bijschrift")
	    .put("picTx",           "Afbeelding met bijschrift")
	    .put("vertTx",          "Titel en verticale tekst")
	    .put("vertTitleAndTx",  "Verticale titel en tekst")
	    .build();

	static final ImmutableMap<String, String> pl = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Wzorzec")
	    .put("cust",            "Niestandardowy")
	    .put("title",           "Slajd tytu\u0142owy")
	    .put("obj",             "Tytu\u0142 i zawarto\u015b\u0107")
	    .put("secHead",         "Nag\u0142\u00f3wek sekcji")
	    .put("twoObj",          "Dwa elementy zawarto\u015bci")
	    .put("twoTxTwoObj",     "Tytu\u0142, 2 elementy zawarto\u015bci i tekst")
	    .put("titleOnly",       "Tylko tytu\u0142")
	    .put("blank",           "Pusty")
	    .put("objTx",           "Zawarto\u015b\u0107 z podpisem")
	    .put("picTx",           "Obraz z podpisem")
	    .put("vertTx",          "Tytu\u0142 i tekst pionowy")
	    .put("vertTitleAndTx",  "Tytu\u0142 pionowy i tekst")
	    .build();

	static final ImmutableMap<String, String> ru = new ImmutableMap.Builder<String, String>()
	    .put("master",          "\u041e\u0431\u0440\u0430\u0437\u0435\u0446")
	    .put("cust",            "\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u043b\u044c\u043d\u044b\u0439")
	    .put("title",           "\u0422\u0438\u0442\u0443\u043b\u044c\u043d\u044b\u0439 \u0441\u043b\u0430\u0439\u0434")
	    .put("obj",             "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0438 \u043e\u0431\u044a\u0435\u043a\u0442")
	    .put("secHead",         "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0440\u0430\u0437\u0434\u0435\u043b\u0430")
	    .put("twoObj",          "\u0414\u0432\u0430 \u043e\u0431\u044a\u0435\u043a\u0442\u0430")
	    .put("twoTxTwoObj",     "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a, \u0434\u0432\u0430 \u043e\u0431\u044a\u0435\u043a\u0442\u0430 \u0438 \u0442\u0435\u043a\u0441\u0442")
	    .put("titleOnly",       "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a")
	    .put("blank",           "\u041f\u0443\u0441\u0442\u043e\u0439 \u0441\u043b\u0430\u0439\u0434")
	    .put("objTx",           "\u041e\u0431\u044a\u0435\u043a\u0442 \u0441 \u043f\u043e\u0434\u043f\u0438\u0441\u044c\u044e")
	    .put("picTx",           "\u0420\u0438\u0441\u0443\u043d\u043e\u043a \u0441 \u043f\u043e\u0434\u043f\u0438\u0441\u044c\u044e")
	    .put("vertTx",          "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0438 \u0432\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442")
	    .put("vertTitleAndTx",  "\u0412\u0435\u0440\u0442\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0438 \u0442\u0435\u043a\u0441\u0442")
	    .build();

	static final ImmutableMap<String, String> sk = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Predloha")
	    .put("cust",            "Vlastn\u00fd")
	    .put("title",           "\u00davodn\u00e1 sn\u00edmka")
	    .put("obj",             "Nadpis a obsah")
	    .put("secHead",         "Hlavi\u010dka sekcie")
	    .put("twoObj",          "Dva obsahy")
	    .put("twoTxTwoObj",     "Dva objekty a text")
	    .put("titleOnly",       "Len nadpis")
	    .put("blank",           "Pr\u00e1zdna")
	    .put("objTx",           "Obsah s popisom")
	    .put("picTx",           "Obr\u00e1zok s popisom")
	    .put("vertTx",          "Nadpis a zvisl\u00fd text")
	    .put("vertTitleAndTx",  "Zvisl\u00fd nadpis a text")
	    .build();

	static final ImmutableMap<String, String> sv = new ImmutableMap.Builder<String, String>()
	    .put("master",          "Bakgrund")
	    .put("cust",            "Anpassad")
	    .put("title",           "Rubrikbild")
	    .put("obj",             "Rubrik och inneh\u00e5ll")
	    .put("secHead",         "Avsnittsrubrik")
	    .put("twoObj",          "Tv\u00e5 delar")
	    .put("twoTxTwoObj",     "Rubrik, tv\u00e5 inneh\u00e5llsdelar och text")
	    .put("titleOnly",       "Rubrik")
	    .put("blank",           "Tom")
	    .put("objTx",           "Text med bildtext")
	    .put("picTx",           "Bild med bildtext")
	    .put("vertTx",          "Rubrik och lodr\u00e4t text")
	    .put("vertTitleAndTx",  "Lodr\u00e4t rubrik och text")
	    .build();

    static final ImmutableMap<String, String> tr = new ImmutableMap.Builder<String, String>()
        .put("master",          "As\u0131l")
        .put("cust",            "\u00d6zel D\u00fczen")
        .put("title",           "Ba\u015fl\u0131k Slayd\u0131")
        .put("obj",             "Ba\u015fl\u0131k ve \u0130\u00e7erik")
        .put("secHead",         "B\u00f6l\u00fcm \u00dcst Bilgisi")
        .put("twoObj",          "\u0130ki \u0130\u00e7erik")
        .put("twoTxTwoObj",     "Kar\u015f\u0131la\u015ft\u0131rma")
        .put("titleOnly",       "Yaln\u0131zca Ba\u015fl\u0131k")
        .put("blank",           "Bo\u015f")
        .put("objTx",           "Ba\u015fl\u0131kl\u0131 \u0130\u00e7erik")
        .put("picTx",           "Ba\u015fl\u0131kl\u0131 Resim")
        .put("vertTx",          "Ba\u015fl\u0131k ve Dikey Metin")
        .put("vertTitleAndTx",  "Dikey Ba\u015fl\u0131k ve Metin")
        .build();

	static final ImmutableMap<String, String> zhCN = new ImmutableMap.Builder<String, String>()
	    .put("master",          "\u6bcd\u7248")
	    .put("cust",            "\u81ea\u5b9a\u4e49")
	    .put("title",           "\u6807\u9898\u5e7b\u706f\u7247")
	    .put("obj",             "\u6807\u9898\u548c\u5185\u5bb9")
	    .put("secHead",         "\u8282\u6807\u9898")
	    .put("twoObj",          "\u4e24\u680f\u5185\u5bb9")
	    .put("twoTxTwoObj",     "\u6807\u9898\uff0c\u4e24\u9879\u5185\u5bb9\u4e0e\u6587\u672c")
	    .put("titleOnly",       "\u4ec5\u6807\u9898")
	    .put("blank",           "\u7a7a\u767d")
	    .put("objTx",           "\u5185\u5bb9\u4e0e\u6807\u9898")
	    .put("picTx",           "\u56fe\u7247\u4e0e\u6807\u9898")
	    .put("vertTx",          "\u6807\u9898\u548c\u7ad6\u6392\u6587\u5b57")
	    .put("vertTitleAndTx",  "\u7ad6\u6392\u6807\u9898\u4e0e\u6587\u672c")
	    .build();

	static final ImmutableMap<String, String> zhTW = new ImmutableMap.Builder<String, String>()
	    .put("master",          "\u6bcd\u7247")
	    .put("cust",            "\u81ea\u8a02")
	    .put("title",           "\u6a19\u984c\u6295\u5f71\u7247")
	    .put("obj",             "\u6a19\u984c\u53ca\u7269\u4ef6")
	    .put("secHead",         "\u7ae0\u7bc0\u6a19\u984c")
	    .put("twoObj",          "\u5169\u9805\u7269\u4ef6")
	    .put("twoTxTwoObj",     "\u6a19\u984c\uff0c\u5169\u9805\u7269\u4ef6\u53ca\u6587\u5b57")
	    .put("titleOnly",       "\u6a19\u984c")
	    .put("blank",           "\u7a7a\u767d")
	    .put("objTx",           "\u542b\u6a19\u984c\u7684\u5167\u5bb9")
	    .put("picTx",           "\u542b\u6a19\u984c\u7684\u5716\u7247")
	    .put("vertTx",          "\u6a19\u984c\u53ca\u76f4\u6392\u6587\u5b57")
	    .put("vertTitleAndTx",  "\u76f4\u6392\u6a19\u984c\u53ca\u6587\u5b57")
		.build();

	static private Map<String, ImmutableMap<String, String>> languageToMap = null;

	public static Map<String, ImmutableMap<String, String>> getLanguageToMap() {
		if(languageToMap==null) {
			languageToMap = ImmutableMap.<String, ImmutableMap<String, String>> builder()
			    .put("en",    en)
			    .put("en-GB", en)
			    .put("en-US", en)

			    .put("fr",    fr)
			    .put("fr-FR", fr)
			    .put("fr-BE", fr)
			    .put("fr-CA", fr)

			    .put("pt",    pt)
			    .put("pt-PT", pt)
			    .put("pt-BR", ptBR)

			    .put("ca",    ca)
			    .put("ca-ES", ca)

			    .put("es",    es)
			    .put("es-ES", es)

			    .put("it",    it)
			    .put("it-IT", it)

			    .put("ro",    ro)
			    .put("ro-RO", ro)

			    .put("cs",    cs)
			    .put("cs-CZ", cs)

			    .put("da",    da)
			    .put("da-DK", da)

			    .put("de",    de)
			    .put("de-DE", de)
			    .put("de-AT", de)
			    .put("de-CH", de)
			    .put("de-LI", de)
			    .put("de-LU", de)

                .put("el",    el)
                .put("el-GR", el)

			    .put("fi",    fi)
			    .put("fi-FI", fi)

			    .put("hu",    hu)
			    .put("hu-HU", hu)

			    .put("ja",    ja)
			    .put("ja-JP", ja)

			    .put("lv",    lv)
			    .put("lv-LV", lv)

			    .put("nb",    nb)   /* Bokm\u00e5l, Norway (NO) */
			    .put("no",    nb)
			    .put("no-NO", nb)

			    .put("nl",    nl)
			    .put("nl-NL", nl)
			    .put("nl-BR", nl)

			    .put("pl",    pl)
			    .put("pl-PL", pl)

			    .put("ru",    ru)
			    .put("ru-RU", ru)

			    .put("sk",    sk)
			    .put("sk-SK", sk)

			    .put("sv",    sv)
			    .put("sv-SE", sv)
			    .put("sv-FI", sv)

                .put("tr",    tr)
                .put("tr-TR", tr)

                .put("zh",    zhCN)
			    .put("zh-HK", zhCN)
			    .put("zh-TW", zhTW)
			    .build();
		}
		return languageToMap;
	}
}
