/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTextCharacterProperties;
import org.docx4j.dml.CTTextListStyle;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.CTTextParagraphProperties;
import org.docx4j.dml.ITextCharacterProperties;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.docx4j.openpackaging.parts.PresentationML.TableStylesPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.pptx4j.pml.CTPlaceholder;
import org.pptx4j.pml.NvPr;
import org.pptx4j.pml.Presentation;
import org.pptx4j.pml.Presentation.SldMasterIdLst;
import org.pptx4j.pml.Presentation.SldMasterIdLst.SldMasterId;
import org.pptx4j.pml.STPlaceholderType;
import org.pptx4j.pml.STSlideLayoutType;
import org.pptx4j.pml.Shape;
import org.pptx4j.pml.SldLayout;
import org.pptx4j.pml.SlideLayoutIdList;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import com.google.common.collect.ImmutableMap;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DocumentComplexity;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.PresentationObjectTitles;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.pptx.components.ParagraphComponent;
import com.openexchange.office.filter.ooxml.pptx.components.RootComponent;
import com.openexchange.office.filter.ooxml.pptx.components.ShapeComponent;
import com.openexchange.office.filter.ooxml.pptx.components.SlideComponent;
import com.openexchange.office.filter.ooxml.pptx.operations.PptxApplyOperationHelper;
import com.openexchange.office.filter.ooxml.pptx.operations.PptxCreateOperationHelper;
import com.openexchange.office.filter.ooxml.pptx.tools.LayoutTypeTitles;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class PptxOperationDocument extends com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument {

    private PresentationMLPackage opcPackage;
    private ThemePart themePart = null;
    private boolean sortOperations = true;

    public PptxOperationDocument(Session session, IResourceManager _resourceManager, DocumentProperties _documentProperties)
        throws FilterException {

        super(session, _resourceManager, _documentProperties);
    }

    @Override
    protected ByteArrayInputStream _loadDocument(InputStream _inputStream, boolean XMLReadOnly)
        throws Docx4JException {

        final ByteArrayInputStream documentInputStream = DocumentComplexity.checkDocumentComplexity(_inputStream, -1, -1);
        opcPackage = (PresentationMLPackage)OpcPackage.load(documentInputStream, XMLReadOnly);
        themePart = null;
        return documentInputStream;
    }

    @Override
    protected void _createPackage()
        throws Exception  {

        opcPackage = PresentationMLPackage.createPackage();
        themePart = null;
    }

    public void setSortOperations(boolean sortOperations) {
        this.sortOperations = sortOperations;
    }

    @Override
    public void _applyDefaultDocumentProperties()
        throws JSONException, InvalidFormatException {

        final DocumentProperties documentProperties = getDocumentProperties();
        if(documentProperties==null) {
            return;
        }
        final PptxApplyOperationHelper applyOperationHelper = new PptxApplyOperationHelper(this);
        final String userLanguage = documentProperties.optString(DocumentProperties.PROP_USER_LANGUAGE, null);
        if(userLanguage!=null) {
            final MainPresentationPart mainPresentationPart = getPackage().getMainPresentationPart();
            final Presentation presentation = mainPresentationPart.getJaxbElement();
            CTTextListStyle defaultTextListStyle = presentation.getDefaultTextStyle();
            if(defaultTextListStyle==null) {
                defaultTextListStyle = new CTTextListStyle();
                presentation.setDefaultTextStyle(defaultTextListStyle);
            }
            CTTextParagraphProperties defaultTextParagraphProperties = defaultTextListStyle.getDefPPr();
            if(defaultTextParagraphProperties==null) {
                defaultTextParagraphProperties = new CTTextParagraphProperties();
                defaultTextListStyle.setDefPPr(defaultTextParagraphProperties);
            }
            final CTTextCharacterProperties textCharacterProperties = defaultTextParagraphProperties.getDefRPr(true);
            textCharacterProperties.setLang(userLanguage);
        }
        adaptPresentationSlides(userLanguage != null ? userLanguage : "en", documentProperties.optBoolean(DocumentProperties.PROP_CREATED_BY_DEFAULT_TEMPLATE, false));

        final JSONObject attrs = new JSONObject(1);
        final JSONObject pageAttrs = new JSONObject(2);
        final Object pageSizeWidth = documentProperties.get(DocumentProperties.PROP_PAGESIZE_WIDTH);
        final Object pageSizeHeight = documentProperties.get(DocumentProperties.PROP_PAGESIZE_HEIGHT);
        if(pageSizeWidth instanceof Number) {
            pageAttrs.put(OCKey.WIDTH.value(), pageSizeWidth);
        }
        if(pageSizeHeight instanceof Number) {
            pageAttrs.put(OCKey.HEIGHT.value(), pageSizeHeight);
        }
        if(!pageAttrs.isEmpty()) {
            attrs.put(OCKey.PAGE.value(), pageAttrs);
        }
        if(!attrs.isEmpty()) {
            applyOperationHelper.setDocumentAttributes(attrs);
        }
    }

    @Override
    public JSONObject _getOperations()
        throws Exception {

        final JSONObject aOperations = new JSONObject();
        final JSONArray operationsArray = new JSONArray();

        final PptxCreateOperationHelper createOperationHelper = new PptxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createDocumentDefaults(documentAttributes, getUserLanguage());
        createOperationHelper.createMasterSlides();
        createOperationHelper.createSlides();
        createOperationHelper.createUsedLanguages(documentAttributes);
        aOperations.put("operations", operationsArray);
        return aOperations;
    }

    @Override
	public Map<String, Object> _getMetaData()
		throws Exception {

		final JSONArray operationsArray = new JSONArray();
		final PptxCreateOperationHelper createOperationHelper = new PptxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createDocumentDefaults(documentAttributes, getUserLanguage());
		final Map<String, Object> operations = new HashMap<String, Object>(1);
        operations.put("operations", operationsArray);
        return operations;
	}

    @Override
	public Map<String, Object> _getActivePart()
		throws Exception {

		final JSONArray operationsArray = new JSONArray();
		final PptxCreateOperationHelper createOperationHelper = new PptxCreateOperationHelper(this, operationsArray);
        final JSONObject documentAttributes = createOperationHelper.addSetDocumentAttributesOperation();
        createOperationHelper.createMasterSlides();
        createOperationHelper.createSlides();
        createOperationHelper.createUsedLanguages(documentAttributes);
		final Map<String, Object> operations = new HashMap<String, Object>(1);
        operations.put("operations", operationsArray);
        return operations;
	}

    @Override
	public Map<String, Object> _getNextPart()
		throws Exception {

		return null;
	}

    @Override
    public void _applyOperations(String applyOperations) throws JSONException {
        opcPackage.setSuccessfulAppliedOperations(0);
        if (applyOperations != null) {
            final PptxApplyOperationHelper applyOperationHelper = new PptxApplyOperationHelper(this);
            final JSONArray aOperations = new JSONArray(new JSONTokener(applyOperations));
            if(sortOperations) {
                applyOperationHelper.applyOperationsSorted(aOperations);
            }
            else {
                applyOperationHelper.applyOperations(aOperations);
            }
        }
    }

    @Override
    public Part setContextPart(Part part) {
    	if(contextPart!=part) {
    		contextPart = part;
    		themePart = null;
    		themeFonts = null;
    	}
    	return part;
    }

    @Override
    public Part getContextPart() {
    	return contextPart;
    }

    @Override
    public String getImagePath() {
    	return "ppt/media/";
    }

    /*
     * adapting correct language, deleting slideNames if default template
     */
    private void adaptPresentationSlides(String lang, boolean createdByDefaultTemplate) {
        ImmutableMap<String, String> languageMap = PresentationObjectTitles.getLanguageToMap(LayoutTypeTitles.getLanguageToMap(), lang);
        if(languageMap==null) {
            languageMap = LayoutTypeTitles.en;
        }

        // set master & layout names
        final MainPresentationPart mainPresentationPart = opcPackage.getMainPresentationPart();
        final SldMasterIdLst slideMasterIds = mainPresentationPart.getJaxbElement().getSldMasterIdLst();
        if(slideMasterIds!=null) {
            String masterName = languageMap.get("master");
            if(masterName==null) {
                // fallback.. en
                masterName = LayoutTypeTitles.en.get("master");
            }
            final List<SldMasterId> slideMasterIdList = slideMasterIds.getSldMasterId();
            for(int i=0; i<slideMasterIdList.size(); i++) {
                final SldMasterId slideMasterId = slideMasterIdList.get(i);
                final SlideMasterPart slideMasterPart = (SlideMasterPart)mainPresentationPart.getRelationshipsPart().getPart(slideMasterId.getRid());
                if(createdByDefaultTemplate||slideMasterPart.getCSld().getName().isEmpty()) {
                    slideMasterPart.getCSld().setName(masterName);
                }
                translatePresentationObjects(slideMasterPart, lang);

                // retrieving layouts
                final SlideLayoutIdList slideLayoutIds = slideMasterPart.getJaxbElement().getSldLayoutIdLst(false);
                if(slideLayoutIds!=null) {
                    final RelationshipsPart masterRelationships = slideMasterPart.getRelationshipsPart();
                    final List<SldLayoutId> slideLayoutIdList = slideLayoutIds.getSldLayoutId();
                    for(SldLayoutId layoutId:slideLayoutIdList) {
                        SlideLayoutPart slideLayoutPart = ((SlideLayoutPart)masterRelationships.getPart(masterRelationships.getRelationshipByID(layoutId.getRid())));
                        final SldLayout slideLayout = slideLayoutPart.getJaxbElement();
                        if(createdByDefaultTemplate||slideLayout.getCSld().getName().isEmpty()) {
                            STSlideLayoutType layoutType = slideLayout.getType();
                            if(layoutType==null) {
                                layoutType = STSlideLayoutType.CUST;
                            }
                            String slideLayoutName = languageMap.get(layoutType.value());
                            if(slideLayoutName==null) {
                                slideLayoutName = LayoutTypeTitles.en.get(layoutType.value());
                            }
                            if(slideLayoutName!=null) {
                                slideLayout.getCSld().setName(slideLayoutName);
                            }
                        }
                        translatePresentationObjects(slideLayoutPart, lang);
                    }
                }
            }
        }
    }

    private void translatePresentationObjects(Part slidePart, String language) {
        IComponent<OfficeOpenXMLOperationDocument> child = new SlideComponent(this, new DLNode<Object>(slidePart), 0).getNextChildComponent(null, null);
        while(child!=null) {
            if(child instanceof ShapeComponent) {
                final Object o = child.getObject();
                if(o instanceof Shape) {
                    final NvPr nvPr = ((Shape)o).getNvPr(false);
                    if(nvPr!=null) {
                        final CTPlaceholder placeholder = nvPr.getPh();
                        if(placeholder!=null) {
                            if(needsTranslation(placeholder.getType())) {
                                translatePresentationObject((ShapeComponent)child, placeholder.getType(), language);
                            }
                        }
                    }
                }
            }
            child = child.getNextComponent();
        }
    }

    public static boolean needsTranslation(STPlaceholderType placeholderType) {
        return getTranslationKey(placeholderType, 0)!=null;
    }

    /* returns null if there is no translation for the placeholderType */
    private static String getTranslationKey(STPlaceholderType placeholderType, int level) {
        switch(placeholderType) {
            case TITLE:
            case CTR_TITLE:
                return "title";
            case BODY:
            case OBJ:
                return level>=0 && level<=4 ? "level" + Integer.valueOf(level+1).toString() : null;
            case SUB_TITLE:
                return "subTitle";

            case CHART:
            case CLIP_ART:
            case DGM:
            case HDR:
            case MEDIA:
            case PIC:
            case SLD_IMG:
            case TBL:
                // PASSTROUGH INTENDED
            case SLD_NUM:
            case FTR:
            case DT:
                // PASSTROUGH INTENDED
            default:
                return null;
        }
    }

    private void translatePresentationObject(ShapeComponent presShape, STPlaceholderType placeholderType, String language) {
        IComponent<OfficeOpenXMLOperationDocument> child = presShape.getNextChildComponent(null, null);
        while(child!=null) {
            if(child instanceof ParagraphComponent) {
                translatePresentationParagraph((ParagraphComponent)child, placeholderType, language);
            }
            child = child.getNextComponent();
        }
    }

    private void translatePresentationParagraph(ParagraphComponent paragraphComponent, STPlaceholderType placeholderType, String language) {
        IComponent<OfficeOpenXMLOperationDocument> child = paragraphComponent.getNextChildComponent(null, null);
        if(child==null) {
            return;
        }
        CTTextCharacterProperties firstCharacterProperties = null;
        while(child!=null) {
            if(!(child.getObject() instanceof ITextCharacterProperties)) {
                return;
            }
            final CTTextCharacterProperties textCharacterProperties = ((ITextCharacterProperties)child.getObject()).getRPr(false);
            if(!textCharacterProperties.isDirty()) {
                return;
            }
            if(firstCharacterProperties==null) {
                firstCharacterProperties = textCharacterProperties;
            }
            child = child.getNextComponent();
        }

        // now we have a paragraph with only dirty text runs

        int level = 0;
        final CTTextParagraph paragraph = (CTTextParagraph)paragraphComponent.getObject();
        final CTTextParagraphProperties paragraphPr = paragraph.getPPr(false);
        if(paragraphPr!=null) {
            final Integer l = paragraphPr.getLvl();
            if(l!=null) {
                level = l.intValue();
            }
        }
        final String translation = PresentationObjectTitles.getTranslation(getTranslationKey(placeholderType, level), language, null);
        if(translation!=null) {
            final DLList<Object> content = paragraph.getContent();
            content.clear();
            final CTRegularTextRun textRun = new CTRegularTextRun();
            textRun.setParent(paragraph);
            content.add(textRun);
            if(firstCharacterProperties==null) {
                firstCharacterProperties = new CTTextCharacterProperties();
            }
            textRun.setRPr(firstCharacterProperties);
            firstCharacterProperties.setLang(language);
            firstCharacterProperties.setDirty(null);
            textRun.setT(translation);
        }
    }

    @Override
    public PresentationMLPackage getPackage() {
        return opcPackage;
    }

    @Override
    public void setPackage(OpcPackage p) {
       opcPackage = (PresentationMLPackage)p;
    }

    @Override
	public OfficeOpenXMLComponent getRootComponent() {
    	return new RootComponent(this, opcPackage.getMainPresentationPart());
	}

    @Override
    public ThemePart getThemePart(boolean createIfMissing)
    	throws FilterException {

    	try {
	    	if(themePart==null) {
	    		Part context = contextPart;
	    		if(context instanceof SlidePart) {
	    			context = ((SlidePart)context).getSlideLayoutPart();
	    		}
	    		if(context instanceof SlideLayoutPart) {
	    			context = ((SlideLayoutPart)context).getSlideMasterPart();
	    		}
	    		if(context!=null) {
		    		final RelationshipsPart masterRelations = context.getRelationshipsPart();
		    		final Relationship rel = masterRelations.getRelationshipByType(Namespaces.THEME);
		    		if(rel!=null) {
		    			themePart = (ThemePart)masterRelations.getPart(rel);
		    		}
		    		if(themePart==null&&createIfMissing) {
		        		final PartName themePartName = new PartName("/ppt/theme/theme1.xml");
			    		themePart = getDefaultThemePart(themePartName);
			    		context.addTargetPart(themePart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
		    		}
	    		}
	    	}
	    	return themePart;
    	}
    	catch(InvalidFormatException e) {
    		throw new FilterException("xlsx filter, could not create default theme", ErrorCode.CRITICAL_ERROR, e);
    	}
    }

    public TableStylesPart getTableStylesPart(boolean createIfMissing)
    	throws InvalidFormatException {

    	TableStylesPart tableStylesPart = null;
    	final RelationshipsPart relPart = opcPackage.getMainPresentationPart().getRelationshipsPart();
		final Relationship relation = relPart.getRelationshipByType(Namespaces.PRESENTATIONML_TABLE_STYLES);
		if(relation!=null) {
			tableStylesPart = (TableStylesPart)relPart.getPart(relation);
		}
		if(tableStylesPart==null&&createIfMissing) {
			tableStylesPart = new TableStylesPart();
			opcPackage.getMainPresentationPart().addTargetPart(tableStylesPart, AddPartBehaviour.OVERWRITE_IF_NAME_EXISTS);
		}
		return tableStylesPart;
    }
}
