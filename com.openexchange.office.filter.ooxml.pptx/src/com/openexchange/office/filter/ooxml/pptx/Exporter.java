/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx;

import java.io.InputStream;

import org.pptx4j.jaxb.Context;
import org.springframework.stereotype.Service;

import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.session.Session;

/**
 * {@link Exporter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@Service
@RegisteredService
public class Exporter implements IExporter, OsgiBundleContextAware {

	private OsgiBundleContextAndActivator bundleCtx;
	
    /**
     * Initializes a new {@link Exporter}.
     */
    public Exporter() {
        Context.getpmlObjectFactory();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.office.IExporter#createDocument(java.io.InputStream, org.json.JSONObject)
     */
    @Override
    public InputStream createDocument(Session session, InputStream documentStm, String applyOperations, IResourceManager resourceManager, DocumentProperties documentProperties, boolean createFinalDocument) {
        try {
            return _createDocument(session, documentStm, applyOperations, resourceManager, documentProperties, createFinalDocument, true);
        }
        catch(FilterException e) {
              if(e.getErrorcode()!=ErrorCode.CRITICAL_ERROR) {
                  e.setOperationCount(0);
                  throw e;
              }
        }
        try {
            documentStm.reset();
        }
        catch(Exception e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
        // fallback, try without sorting operations
        return _createDocument(session, documentStm, applyOperations, resourceManager, documentProperties, createFinalDocument, false);
    }

    public InputStream _createDocument(Session session, InputStream documentStm, String applyOperations, IResourceManager resourceManager, DocumentProperties documentProperties, boolean createFinalDocument, boolean sortOperations) {
        InputStream document = null;
        try(PptxOperationDocument operationDocument = new PptxOperationDocument(session, resourceManager, documentProperties)) {
        	bundleCtx.prepareObject(operationDocument);
            operationDocument.loadDocument(documentStm, false);
            operationDocument.setSortOperations(sortOperations);
            operationDocument.setCreateFinalDocument(createFinalDocument);
            operationDocument.updateDocumentProperties();
            operationDocument.applyOperations(applyOperations);
            document = operationDocument.debugSave(applyOperations);
        }
        catch(Throwable e) {
            OfficeOpenXMLOperationDocument.rethrowFilterException(e, null);
        }
        return document;
    }

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
