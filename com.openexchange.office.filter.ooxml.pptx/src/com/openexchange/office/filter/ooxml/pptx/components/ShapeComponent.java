/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.IShapeBasic;
import org.docx4j.jaxb.Context;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.Shape;
import org.pptx4j.pml.Shape.NvSpPr;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ShapeComponent extends PptxComponent implements IShapeComponent, IShapeType {

	final Shape shape;
    private String imageReplacementUrl = null;
	private ShapeType shapeType = ShapeType.UNDEFINED;

	public ShapeComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        shape = (Shape)getObject();
        if(shape.isWordArt()) {
            imageReplacementUrl = DMLHelper.getReplacementUrl(DMLHelper.getHashFromShape(org.pptx4j.jaxb.Context.getJcPml(), getOperationDocument().getPackage(), shape), PMLShapeHelper.getSlideNumberFromComponent(this), getComponentNumber(), "pptx");
            if(imageReplacementUrl != null) {
                getOperationDocument().registerShape2pngReplacementImage(imageReplacementUrl);
            }
        }
        if(imageReplacementUrl == null) {
            shapeType = ShapeType.SHAPE;
        }
 	}

	@Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
	    // no child shapes if we are using an image replacement
	    if(imageReplacementUrl != null) {
            return null;
        }

	    final DLNode<Object> shapeNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)shapeNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        OfficeOpenXMLComponent nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, ((Shape)shapeNode.getData()).getTxBody(false));
            if(o instanceof CTTextParagraph) {
            	nextComponent = new ParagraphComponent(this, childNode, nextComponentNumber);
            }
        }
        return nextComponent;
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
        	case PARAGRAPH: {
	            final Child newChild = Context.getDmlObjectFactory().createCTTextParagraph();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ParagraphComponent(parentContext, newChildNode, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

    	PMLShapeHelper.applyAttrsFromJSON(operationDocument, attrs, shape);
    	final JSONObject listStyle = attrs.optJSONObject(OCKey.LIST_STYLE.value());
    	if(listStyle!=null) {
    		DMLHelper.applyTextListStyleFromJson(shape.getTxBody(true), listStyle, operationDocument);
    	}
		com.openexchange.office.filter.ooxml.drawingml.Shape.applyAttrsFromJSON(operationDocument, attrs, shape, !(getParentComponent() instanceof ShapeGroupComponent), true);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	PMLShapeHelper.createJSONAttrs(attrs, shape);
    	if(imageReplacementUrl != null) {
            com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(getOperationDocument(), attrs, (IShapeBasic)shape, !(getParentComponent() instanceof ShapeGroupComponent));
            Tools.addFamilyAttribute(attrs, OCKey.IMAGE, OCKey.IMAGE_URL, imageReplacementUrl);
            Tools.addFamilyAttribute(attrs, OCKey.DRAWING, OCKey.NOT_RESTORABLE, Boolean.TRUE);
    	}
    	else {
    
        	final CTTextBody txBody = shape.getTxBody(false);
            if(txBody!=null) {
            	final JSONObject style = DMLHelper.createJsonFromTextListStyle(getOperationDocument(), txBody.getLstStyle());
            	if(style!=null&&!style.isEmpty()) {
            		attrs.put(OCKey.LIST_STYLE.value(), style);
            	}
            }
            com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(getOperationDocument(), attrs, shape, !(getParentComponent() instanceof ShapeGroupComponent));
    	}
        return attrs;
    }

	@Override
	public ShapeType getType() {
	    return shapeType;
	}

	@Override
	public Integer getId() {
		final CTNonVisualDrawingProps nvdProps = shape.getNonVisualDrawingProperties(false);
		return nvdProps!=null ? nvdProps.getId() : null;
	}

	@Override
	public boolean isPresentationObject() {
		final NvSpPr nvSpPr = shape.getNvSpPr();
		return nvSpPr!=null&&nvSpPr.getNvPr()!=null&&nvSpPr.getNvPr().getPh()!=null ? true : false;
	}

	public CTTextBody getTextBody(boolean forceCreate) {
		return shape.getTxBody(forceCreate);
	}
}
