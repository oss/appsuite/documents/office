/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.operations;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.commons.lang3.tuple.Pair;
import org.docx4j.dml.CTGroupShapeProperties;
import org.docx4j.dml.CTGroupTransform2D;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTableStyle;
import org.docx4j.dml.CTTableStyleList;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTextField;
import org.docx4j.dml.CTTextListStyle;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.ITransform2D;
import org.docx4j.dml.ITransform2DAccessor;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.docx4j.openpackaging.parts.PresentationML.TableStylesPart;
import org.docx4j.openpackaging.parts.PresentationML.ViewPropertiesPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.Pptx4jException;
import org.pptx4j.pml.CTCommentAuthor;
import org.pptx4j.pml.CTCommentAuthorList;
import org.pptx4j.pml.CTNormalViewPortion;
import org.pptx4j.pml.CTNormalViewProperties;
import org.pptx4j.pml.CommonSlideData;
import org.pptx4j.pml.GroupShape;
import org.pptx4j.pml.GroupShape.NvGrpSpPr;
import org.pptx4j.pml.IComment;
import org.pptx4j.pml.ICommentAuthor;
import org.pptx4j.pml.ICommentAuthorList;
import org.pptx4j.pml.ICommentList;
import org.pptx4j.pml.ObjectFactory;
import org.pptx4j.pml.Presentation.SldIdLst.SldId;
import org.pptx4j.pml.Presentation.SldMasterIdLst;
import org.pptx4j.pml.Presentation.SldMasterIdLst.SldMasterId;
import org.pptx4j.pml.Sld;
import org.pptx4j.pml.SldLayout;
import org.pptx4j.pml.SldMaster;
import org.pptx4j.pml.SlideLayoutIdList;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import org.pptx4j.pml.ViewPr;
import org.pptx4j.pml_2012.CTParentCommentIdentifier;
import org.pptx4j.pml_2013.CTDocumentMoniker;
import org.pptx4j.pml_2013.CTSlideMoniker;
import org.pptx4j.pml_2013.CTSlideMonikerList;
import org.pptx4j.pml_2018_8.CTComment;
import org.pptx4j.pml_2018_8.CTCommentReply;
import org.pptx4j.pml_2018_8.CTCommentReplyList;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IParagraph;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.operations.ApplyOperationHelper;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.components.RootComponent;
import com.openexchange.office.filter.ooxml.pptx.components.ShapeGraphicComponent;
import com.openexchange.office.filter.ooxml.pptx.components.ShapeGroupComponent;
import com.openexchange.office.filter.ooxml.pptx.components.SlideComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TableRowComponent;
import com.openexchange.office.filter.ooxml.pptx.components.TextFieldComponent;

public class PptxApplyOperationHelper extends ApplyOperationHelper {

    private final PptxOperationDocument operationDocument;
    private final PresentationMLPackage presentationMLPackage;

    private HashMap<String, Part> idToPart = null;
    private HashMap<String, String> relTargetToId = null;

    public PptxApplyOperationHelper(PptxOperationDocument _operationDocument) {
        super();

        operationDocument = _operationDocument;
        presentationMLPackage = _operationDocument.getPackage();
    }

    @Override
    public PptxOperationDocument getOperationDocument() {
        return operationDocument;
    }

    private void initIdTables() {

        idToPart = new HashMap<String, Part>();
        relTargetToId = new HashMap<String, String>();

        final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
        final SldMasterIdLst slideMasterIds = mainPresentationPart.getJaxbElement().getSldMasterIdLst();
        if(slideMasterIds!=null) {
            final MutableLong nextId = new MutableLong(0x80000001L);
            final HashSet<Long> usedIds = new HashSet<Long>();

            final List<SldMasterId> slideMasterIdList = slideMasterIds.getSldMasterId();
            for(int i=0; i<slideMasterIdList.size(); i++) {
                final SldMasterId slideMasterId = slideMasterIdList.get(i);
                final SlideMasterPart slideMasterPart = (SlideMasterPart)mainPresentationPart.getRelationshipsPart().getPart(slideMasterId.getRid());
                slideMasterId.setId(ensureUniqueId(usedIds, slideMasterId.getId(), nextId));
                final String slideMasterIdString = slideMasterId.getId().toString();
                idToPart.put(slideMasterIdString, slideMasterPart);
                relTargetToId.put("../" + mainPresentationPart.getRelationshipsPart().getRelationshipByID(slideMasterId.getRid()).getTarget(), slideMasterIdString);

                // retrieving layouts
                final SlideLayoutIdList slideLayoutIds = slideMasterPart.getJaxbElement().getSldLayoutIdLst(false);
                if(slideLayoutIds!=null) {
                    final List<SldLayoutId> slideLayoutIdList = slideLayoutIds.getSldLayoutId();
                    for(int j=0; j<slideLayoutIdList.size(); j++) {
                        final SldLayoutId slideLayoutId = slideLayoutIdList.get(j);
                        slideLayoutId.setId(ensureUniqueId(usedIds, slideLayoutId.getId(), nextId));
                        final String slideLayoutIdString = slideLayoutId.getId().toString();
                        idToPart.put(slideLayoutIdString, slideMasterPart.getRelationshipsPart().getPart(slideLayoutId.getRid()));
                        relTargetToId.put(slideMasterPart.getRelationshipsPart().getRelationshipByID(slideLayoutId.getRid()).getTarget(), slideLayoutIdString);
                    }
                }
            }
        }
    }

    public HashMap<String, Part> getIdToPartMap() {
        if(idToPart==null) {
            initIdTables();
        }
        return idToPart;
    }

    public HashMap<String, String> getRelTargetToIdMap() {
        if(relTargetToId==null) {
            initIdTables();
        }
        return relTargetToId;
    }

    // - the target can be "" then a normal slide is addressed, otherwise it points to a layout or masterslide
    public Part setContextPartByTarget(String target) {
        return operationDocument.setContextPart(getPartByTarget(target));
    }

    public Part getPartByTarget(String target) {
        Part newContext = null;
        if(target!=null&&!target.isEmpty()) {
            newContext = getIdToPartMap().get(target);
        }
        return newContext == null ? presentationMLPackage.getMainPresentationPart() : newContext;
    }

    private Long ensureUniqueId(HashSet<Long> usedIds, Long idToCheck, MutableLong nextId) {
        if(idToCheck==null||usedIds.contains(idToCheck)) {
            idToCheck = nextId.toLong();
            while(usedIds.contains(idToCheck)) {
                idToCheck++;
            }
            nextId.setValue(idToCheck + 1);
        }
        usedIds.add(idToCheck);
        return idToCheck;
    }

    public void applyOperations(JSONArray aOperations) {

        int i = 0;
        JSONObject op = null;
        OCValue opName = null;

        try {

            for (i = 0; i < aOperations.length(); i++) {
                presentationMLPackage.setSuccessfulAppliedOperations(i);
                op = (JSONObject) aOperations.get(i);
                final String target = op.optString(OCKey.TARGET.value(), "");
                opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
                applyOperation(op, opName, target);
            }
            presentationMLPackage.setSuccessfulAppliedOperations(aOperations.length());
        }
        catch(Exception e) {
            String message = e.getMessage();
            if(op!=null) {
                message += ", operation: " + op.toString();
            }
            throw new FilterException(message, e, ErrorCode.CRITICAL_ERROR);
        }
    }

    public void applyOperationsSorted(JSONArray aOperations) {
        try {

            final JSONArray globalOperations = new JSONArray();
            final Map<String, JSONArray> targetOperations = new HashMap<String, JSONArray>();
            final int slidePages = presentationMLPackage.getMainPresentationPart().getContent().size();
            final List<JSONArray> slideOperations = new ArrayList<JSONArray>(slidePages);
            for(int i=0; i<slidePages; i++) {
                slideOperations.add(null);
            }
            for(int i=0; i<aOperations.length(); i++) {
                final JSONObject op = (JSONObject) aOperations.get(i);
                final String target = op.optString(OCKey.TARGET.value(), "");
                final String opName = op.getString(OCKey.NAME.value());
                final String id = op.optString(OCKey.ID.value());
                final OCValue name = OCValue.fromValue(opName);
                final JSONArray start = op.optJSONArray(OCKey.START.value());

                switch(name) {
                    case INSERT_MASTER_SLIDE : {
                        insertMasterSlide(id, op.optInt(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                        break;
                    }
                    case INSERT_LAYOUT_SLIDE : {
                        insertLayoutSlide(id, op.getString(OCKey.TARGET.value()), op.optInt(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                        break;
                    }
                    case MOVE_LAYOUT_SLIDE : {
                        moveLayoutSlide(id, op.getInt(OCKey.START.value()), target);
                        break;
                    }
                    case INSERT_SLIDE : {
                        insertSlide(start, target, op.optJSONObject(OCKey.ATTRS.value()));
                        slideOperations.add(start.getInt(0), null);
                        break;
                    }
                    case MOVE_SLIDE : {
                        final JSONArray end = op.getJSONArray(OCKey.END.value());
                        moveSlide(start, end);
                        slideOperations.add(end.getInt(0), slideOperations.remove(start.getInt(0)));
                        break;
                    }
                    case DELETE : {
                        if(start.length()==1) {
                            if(target!=null&&!target.isEmpty()) {
                                // deleting master or layout page
                                CUtil.delete(new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)), start, op.optJSONArray(OCKey.END.value()));
                                targetOperations.remove(target);
                            }
                            else {
                                // deleting normal page
                                CUtil.delete(new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)), start, op.optJSONArray(OCKey.END.value()));
                                slideOperations.remove(start.getInt(0));
                            }
                        }
                        else if(target!=null&&!target.isEmpty()) {
                            JSONArray targetArray = targetOperations.get(target);
                            if(targetArray==null) {
                                targetArray = new JSONArray();
                                targetOperations.put(target, targetArray);
                            }
                            targetArray.put(op);
                        }
                        else {
                            JSONArray array = slideOperations.get(start.getInt(0));
                            if(array==null) {
                                array = new JSONArray();
                                slideOperations.set(start.getInt(0), array);
                            }
                            array.put(op);
                        }
                        break;
                    }
                    default: {

                        if(start==null) {
                            globalOperations.put(op);
                        }
                        else if(target!=null&&!target.isEmpty()) {
                            JSONArray targetArray = targetOperations.get(target);
                            if(targetArray==null) {
                                targetArray = new JSONArray();
                                targetOperations.put(target, targetArray);
                            }
                            targetArray.put(op);
                        }
                        else {
                            JSONArray array = slideOperations.get(start.getInt(0));
                            if(array==null) {
                                array = new JSONArray();
                                slideOperations.set(start.getInt(0), array);
                            }
                            array.put(op);
                        }
                    }
                }
            }
            if(!globalOperations.isEmpty()) {
                applyOperations(globalOperations);
            }
            final Iterator<Entry<String, JSONArray>> targetIter = targetOperations.entrySet().iterator();
            while(targetIter.hasNext()) {
                final Entry<String, JSONArray> nextOperationEntry = targetIter.next();
                applyOperations(nextOperationEntry.getValue());
                final SerializationPart<?> part = (SerializationPart<?>)getPartByTarget(nextOperationEntry.getKey());
                presentationMLPackage.getSourcePartStore().marshalToSourcePart(part);
                part.setJaxbElement(null);
            }
            for(int i=0; i<slideOperations.size(); i++) {
                final JSONArray array = slideOperations.get(i);
                if(array!=null&&!array.isEmpty()) {
                    for(int j=0; j<array.length(); j++)  {
                        final JSONObject op = array.getJSONObject(j);
                        final JSONArray start = op.optJSONArray(OCKey.START.value());
                        if(start!=null&&start.getInt(0)!=i) {
                            start.remove(0);
                            start.add(0, i);
                        }
                        final JSONArray end = op.optJSONArray(OCKey.END.value());
                        if(end!=null&&end.getInt(0)!=i) {
                            end.remove(0);
                            end.add(0, i);
                        }
                        applyOperation(op, OCValue.fromValue(op.getString(OCKey.NAME.value())), null);
                    }
                    final JSONArray start = new JSONArray(1);
                    start.put(i);
                    final IComponent<OfficeOpenXMLOperationDocument> slideComponent = new RootComponent(getOperationDocument(), presentationMLPackage.getMainPresentationPart()).getComponent(start, 1);
                    final SlidePart slidePart = (SlidePart)slideComponent.getObject();
                    presentationMLPackage.getSourcePartStore().marshalToSourcePart(slidePart);
                    slidePart.setJaxbElement(null);
                }
            }
        }
        catch(Exception e) {
            throw new FilterException(e.getMessage(), e, ErrorCode.CRITICAL_ERROR);
        }
    }

    public void applyOperation(JSONObject op, OCValue opName, String target) throws Exception {
        switch(opName) {
            case INSERT_PARAGRAPH : {
                insertParagraph(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case SPLIT_PARAGRAPH : {
                splitParagraph(op.getJSONArray(OCKey.START.value()), target);
                break;
            }
            case MERGE_PARAGRAPH : {
                mergeParagraph(op.getJSONArray(OCKey.START.value()), target);
                break;
            }
            case INSERT_TEXT : {
                insertText(op.getJSONArray(OCKey.START.value()), target, op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case DELETE : {
                CUtil.delete(new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                break;
            }
            case SET_ATTRIBUTES : {
                setAttributes(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.END.value()), op.getJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_SLIDE : {
                insertSlide(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_MASTER_SLIDE : {
                insertMasterSlide(op.getString(OCKey.ID.value()), op.optInt(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_LAYOUT_SLIDE : {
                insertLayoutSlide(op.getString(OCKey.ID.value()), op.getString(OCKey.TARGET.value()), op.optInt(OCKey.START.value()), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_DRAWING : {
                insertDrawing(op.getJSONArray(OCKey.START.value()), target, op.getString(OCKey.TYPE.value()).toUpperCase(), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case SET_DOCUMENT_ATTRIBUTES : {
                setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_HARD_BREAK : {
                insertHardBreak(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case CHANGE_LAYOUT : {
                changeLayout(op.getJSONArray(OCKey.START.value()), target);
                break;
            }
            case MOVE_SLIDE : {
                moveSlide(op.getJSONArray(OCKey.START.value()), op.getJSONArray(OCKey.END.value()));
                break;
            }
            case MOVE_LAYOUT_SLIDE : {
                moveLayoutSlide(op.getString(OCKey.ID.value()), op.getInt(OCKey.START.value()), target);
                break;
            }
            case INSERT_TAB : {
                insertTab(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_FIELD : {
                insertField(op.getJSONArray(OCKey.START.value()), target, op.getString(OCKey.TYPE.value()), op.getString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case UPDATE_FIELD : {
                updateField(op.getJSONArray(OCKey.START.value()), target, op.optString(OCKey.TYPE.value()), op.optString(OCKey.REPRESENTATION.value()));
                break;
            }
            case GROUP : {
                group(op.getJSONArray(OCKey.START.value()), target, op.getJSONArray(OCKey.DRAWINGS.value()), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case UNGROUP : {
                ungroup(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.DRAWINGS.value()));
                break;
            }
            case MOVE : {
                move(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                break;
            }
            case INSERT_ROWS : {
                insertRows(op.getJSONArray(OCKey.START.value()), target, op.optInt(OCKey.COUNT.value(), 1), op.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), op.optInt(OCKey.REFERENCE_ROW.value(), -1), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_CELLS : {
                insertCells(op.getJSONArray(OCKey.START.value()), target, op.optInt(OCKey.COUNT.value(), 1), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case INSERT_COLUMN : {
                insertColumn(op.getJSONArray(OCKey.START.value()), target, op.getJSONArray(OCKey.TABLE_GRID.value()), op.getInt(OCKey.GRID_POSITION.value()), op.optString(OCKey.INSERT_MODE.value(), "before"));
                break;
            }
            case DELETE_COLUMNS : {
                deleteColumns(op.getJSONArray(OCKey.START.value()), target, op.getInt(OCKey.START_GRID.value()), op.optInt(OCKey.END_GRID.value(), op.getInt(OCKey.START_GRID.value())));
                break;
            }
            case INSERT_STYLE_SHEET : {
                changeOrInsertStyleSheet(true, op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.getJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case CHANGE_STYLE_SHEET : {
                changeOrInsertStyleSheet(false, op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()));
                break;
            }
            case DELETE_STYLE_SHEET : {
                deleteStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()));
                break;
            }
            case INSERT_COMMENT : {
                insertComment(op.getJSONArray(OCKey.START.value()),  op.getJSONArray(OCKey.POS.value()), applyMentions(op.getString(OCKey.TEXT.value()), op.optJSONArray(OCKey.MENTIONS.value())), op.getString(OCKey.DATE.value()), op.optInt(OCKey.COLOR_INDEX.value(), -1), op.optInt(OCKey.AUTHOR_ID.value(), -1), op.optString(OCKey.AUTHOR.value(), null), op.optString(OCKey.INITIALS.value(), ""), op.optString(OCKey.PROVIDER_ID.value(), null), op.optString(OCKey.USER_ID.value(), null), op.optInt(OCKey.PARENT.value(), -1));
                break;
            }
            case CHANGE_COMMENT : {
                changeComment(op.getJSONArray(OCKey.START.value()),  op.optJSONArray(OCKey.POS.value()), applyMentions(op.optString(OCKey.TEXT.value(), null), op.optJSONArray(OCKey.MENTIONS.value())));
                break;
            }
            case DELETE_COMMENT : {
                deleteComment(op.getJSONArray(OCKey.START.value()));
                break;
            }
            case NO_OP : {
                break;
            }
            case CREATE_ERROR : {
                throw new FilterException("createError operation detected: " + opName.value(true), ErrorCode.UNSUPPORTED_OPERATION_USED);
            }
            default: {
                final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
                OfficeOpenXMLOperationDocument.logMessage("warn", "Ignoring unsupported operation: " + unsupportedOps);
            }
        }
    }

    public void insertParagraph(JSONArray start, String target, JSONObject attrs) throws Exception {

        new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
    }

    public void splitParagraph(JSONArray start, String target)
        throws JSONException {

        ((IParagraph)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1)).splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start, String target) {

    	((IParagraph)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length())).mergeParagraph();
    }

    public void insertText(JSONArray start, String target, String text, JSONObject attrs) throws Exception {

    	if(text.length()>0) {
    		final IComponent<OfficeOpenXMLOperationDocument> component = new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1);
    		((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
    	}
    }

    public void insertTab(JSONArray start, String target, JSONObject attrs) throws Exception {

    	insertText(start, target, "\t", attrs);
    }

    public void setAttributes(JSONArray start, String target, JSONArray end, JSONObject attrs) throws Exception {

    	if(attrs==null) {
    		return;
    	}
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OfficeOpenXMLOperationDocument> component = new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length());
		component.splitStart(startIndex, SplitMode.ATTRIBUTE);
        while(component!=null&&component.getComponentNumber()<=endIndex) {
        	if(component.getNextComponentNumber()>=endIndex+1) {
        		component.splitEnd(endIndex, SplitMode.ATTRIBUTE);
        	}
        	component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
    }

    public void insertSlide(JSONArray start, String target, JSONObject attrs)
    	throws InvalidFormatException, Pptx4jException, JSONException, PartUnrecognisedException {

    	final SlideLayoutPart slideLayoutPart = (SlideLayoutPart)getPartByTarget(target);
    	final SlidePart newSlidePart = new SlidePart();
    	final Sld newSlide = new Sld();
    	final CommonSlideData commonSlideData = Context.getpmlObjectFactory().createCommonSlideData();
    	newSlide.setCSld(commonSlideData);
    	commonSlideData.setSpTree(createSpTree());
    	newSlidePart.setJaxbElement(newSlide);
    	final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
    	mainPresentationPart.addSlide(start.getInt(0), newSlidePart);
    	newSlidePart.addTargetPart(slideLayoutPart, AddPartBehaviour.REUSE_EXISTING);
    	if(attrs!=null) {
    		getOperationDocument().setContextPart(newSlidePart);
    		new SlideComponent(operationDocument, new DLNode<Object>(newSlidePart), 0).applyAttrsFromJSON(attrs);
    	}
    }

    public void insertMasterSlide(String id, Integer start, JSONObject attrs)
    	throws InvalidFormatException, PartUnrecognisedException, JSONException {

    	final SlideMasterPart newSlideMasterPart = new SlideMasterPart();
    	final SldMaster newSlideMaster = new SldMaster();
    	final CommonSlideData commonSlideData = Context.getpmlObjectFactory().createCommonSlideData();
    	newSlideMaster.setCSld(commonSlideData);
    	commonSlideData.setSpTree(createSpTree());
    	newSlideMasterPart.setJaxbElement(newSlideMaster);
    	final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
    	final SldMasterIdLst sldMasterIdLst = mainPresentationPart.getJaxbElement().getSldMasterIdLst();

    	final SldMasterId sldMasterId = Context.getpmlObjectFactory().createPresentationSldMasterIdLstSldMasterId();
    	sldMasterId.setId(Long.valueOf(id));
    	if(start!=null) {
    		sldMasterIdLst.getSldMasterId().add(start, sldMasterId);
    	}
    	else {
    		sldMasterIdLst.getSldMasterId().add(sldMasterId);
    	}
    	final Relationship rel = mainPresentationPart.addTargetPart(newSlideMasterPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
    	sldMasterId.setRid(rel.getId());
    	getIdToPartMap().put(id, newSlideMasterPart);
    	getRelTargetToIdMap().put("../" + mainPresentationPart.getRelationshipsPart().getRelationshipByID(sldMasterId.getRid()).getTarget(), sldMasterId.getId().toString());
        getOperationDocument().setContextPart(newSlideMasterPart);

        // add a theme for the master page, the theme is necessary otherwise pptx will report the document is to be repaired
        operationDocument.getThemePart(true);

    	if(attrs!=null) {
    		new SlideComponent(operationDocument, new DLNode<Object>(newSlideMasterPart), 0).applyAttrsFromJSON(attrs);
    	}
    }

    public void insertLayoutSlide(String id, String target, Integer start, JSONObject attrs)
    	throws InvalidFormatException, PartUnrecognisedException, JSONException {

    	final SlideMasterPart slideMasterPart = (SlideMasterPart)getPartByTarget(target);
    	final SlideLayoutPart newLayoutSlidePart = new SlideLayoutPart();
    	final SldLayout newLayoutSlide = new SldLayout();
    	final CommonSlideData commonSlideData = Context.getpmlObjectFactory().createCommonSlideData();
    	newLayoutSlide.setCSld(commonSlideData);
    	commonSlideData.setSpTree(createSpTree());
    	newLayoutSlidePart.setJaxbElement(newLayoutSlide);
    	final Relationship rel = slideMasterPart.addTargetPart(newLayoutSlidePart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
    	final SldMaster sldMaster = slideMasterPart.getJaxbElement();
    	final SldLayoutId sldLayoutId = Context.getpmlObjectFactory().createSlideLayoutIdListSldLayoutId();
    	sldLayoutId.setId(Long.valueOf(id));
    	final List<SldLayoutId> sldLayoutIdLst = sldMaster.getSldLayoutIdLst(true).getSldLayoutId();
    	if(start!=null) {
    		sldLayoutIdLst.add(start, sldLayoutId);
    	}
    	else {
    		sldLayoutIdLst.add(sldLayoutId);
    	}
    	sldLayoutId.setRid(rel.getId());
    	getIdToPartMap().put(id, newLayoutSlidePart);
    	getRelTargetToIdMap().put(slideMasterPart.getRelationshipsPart().getRelationshipByID(sldLayoutId.getRid()).getTarget(), sldLayoutId.getId().toString());
    	newLayoutSlidePart.addTargetPart(slideMasterPart, AddPartBehaviour.REUSE_EXISTING);
    	if(attrs!=null) {
    		getOperationDocument().setContextPart(newLayoutSlidePart);
    		new SlideComponent(operationDocument, new DLNode<Object>(newLayoutSlidePart), 0).applyAttrsFromJSON(attrs);
    	}
    }

    public GroupShape createSpTree() {
    	final ObjectFactory pmlFactory = Context.getpmlObjectFactory();
    	final org.docx4j.dml.ObjectFactory dmlFactory = Context.getDmlObjectFactory();
    	final GroupShape spTree = pmlFactory.createGroupShape();
    	final NvGrpSpPr nvGrpSpPr = pmlFactory.createGroupShapeNvGrpSpPr();
    	spTree.setNvGrpSpPr(nvGrpSpPr);
    	final CTNonVisualDrawingProps cNvPr = dmlFactory.createCTNonVisualDrawingProps();
    	cNvPr.setName("");
    	nvGrpSpPr.setCNvPr(cNvPr);
    	nvGrpSpPr.setCNvGrpSpPr(dmlFactory.createCTNonVisualGroupDrawingShapeProps());
    	nvGrpSpPr.setNvPr(pmlFactory.createNvPr());
    	final CTGroupShapeProperties grpSpPr = dmlFactory.createCTGroupShapeProperties();
    	final CTGroupTransform2D xFrm = grpSpPr.getXfrm(true);
    	final CTPoint2D off = dmlFactory.createCTPoint2D();
    	off.setX(0);
    	off.setY(0);
    	xFrm.setOff(off);
    	final CTPositiveSize2D ext = dmlFactory.createCTPositiveSize2D();
    	ext.setCx(0);
    	ext.setCy(0);
    	xFrm.setExt(ext);
    	final CTPoint2D chOff = dmlFactory.createCTPoint2D();
    	chOff.setX(0);
    	chOff.setY(0);
    	xFrm.setChOff(chOff);
    	final CTPositiveSize2D chExt = dmlFactory.createCTPositiveSize2D();
    	chExt.setCx(0);
    	chExt.setCy(0);
    	xFrm.setChExt(chExt);
    	spTree.setGrpSpPr(grpSpPr);
    	return spTree;
    }

    public IComponent<OfficeOpenXMLOperationDocument> insertDrawing(JSONArray start, String target, String type, JSONObject attrs) throws Exception {

    	ComponentType cType = ComponentType.AC_SHAPE;
    	if(type.equals("IMAGE")) {
    		cType = ComponentType.AC_IMAGE;
    	}
    	else if(type.equals("GROUP")) {
    		cType = ComponentType.AC_GROUP;
    	}
    	else if(type.equals("CONNECTOR")) {
    		cType = ComponentType.AC_CONNECTOR;
    	}
    	else if(type.equals("TABLE")) {
    		cType = ComponentType.TABLE;
    	}
    	return new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1).insertChildComponent(start.getInt(start.length()-1), attrs, cType);
    }

    public void setDocumentAttributes(JSONObject attrs)
    	throws InvalidFormatException {

    	final MainPresentationPart mainPresentationPart = presentationMLPackage.getMainPresentationPart();
        final RelationshipsPart mainPresentationRelations = mainPresentationPart.getRelationshipsPart();
    	final JSONObject layoutSettings = attrs.optJSONObject(OCKey.LAYOUT.value());
    	if(layoutSettings!=null) {
    		ViewPropertiesPart viewPropertiesPart = null;
            final Relationship viewRel = mainPresentationRelations.getRelationshipByType(Namespaces.PRESENTATIONML_VIEW_PROPS);
            if(viewRel!=null) {
            	viewPropertiesPart = (ViewPropertiesPart)mainPresentationRelations.getPart(viewRel);
            }
            else {
            	viewPropertiesPart = new ViewPropertiesPart();
            	viewPropertiesPart.setJaxbElement(new ViewPr());
            	mainPresentationPart.addTargetPart(viewPropertiesPart);
            }
            final ViewPr viewPr = viewPropertiesPart.getJaxbElement();
            final Object slidePaneWidth = layoutSettings.opt(OCKey.SLIDE_PANE_WIDTH.value());
            if(slidePaneWidth instanceof Number) {
            	CTNormalViewProperties normalViewProperties = viewPr.getNormalViewPr();
            	if(normalViewProperties==null) {
            		normalViewProperties = Context.getpmlObjectFactory().createCTNormalViewProperties();
            		viewPr.setNormalViewPr(normalViewProperties);
            	}
            	CTNormalViewPortion leftViewPortion = normalViewProperties.getRestoredLeft();
            	if(leftViewPortion==null) {
            		leftViewPortion = Context.getpmlObjectFactory().createCTNormalViewPortion();
            		normalViewProperties.setRestoredLeft(leftViewPortion);
            	}
            	final Double sz = (((Number)slidePaneWidth).doubleValue() * 1000);
            	leftViewPortion.setSz(sz.intValue());
            }
    	}
    	final JSONObject pageSettings = attrs.optJSONObject(OCKey.PAGE.value());
    	if(pageSettings!=null) {
    		final Object width = pageSettings.opt(OCKey.WIDTH.value());
    		if(width instanceof Number) {
    			mainPresentationPart.getJaxbElement().getSldSz(true).setCx(((Number)width).intValue() * 360);
    		}
    		final Object height = pageSettings.opt(OCKey.HEIGHT.value());
    		if(height instanceof Number) {
    			mainPresentationPart.getJaxbElement().getSldSz(true).setCy(((Number)height).intValue() * 360);
    		}
    	}
    }

    public void insertHardBreak(JSONArray start, String target, JSONObject attrs) throws Exception {

        new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1)
			.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.HARDBREAK_DEFAULT);
    }

    public void changeLayout(JSONArray start, String target)
    	throws InvalidFormatException {

    	setContextPartByTarget(target);
    	final SlideLayoutPart slideLayoutPart = (SlideLayoutPart)operationDocument.getContextPart();
    	final SlideComponent slideComponent = (SlideComponent)new RootComponent(operationDocument, presentationMLPackage.getMainPresentationPart()).getComponent(start, start.length());
    	final SlidePart slidePart = (SlidePart)slideComponent.getObject();
    	final RelationshipsPart slideRelationshipsPart = slidePart.getRelationshipsPart();
    	Relationship relationship = slideRelationshipsPart.getRelationshipByType(Namespaces.PRESENTATIONML_SLIDE_LAYOUT);
    	if(relationship!=null) {
    		slideRelationshipsPart.removeRelationship(relationship);
    	}
    	slidePart.addTargetPart(slideLayoutPart);
    }

    public void moveSlide(JSONArray start, JSONArray end)
    	throws JSONException {

    	final int source = start.getInt(0);
    	final int dest = end.getInt(0);

    	final DLList<Object> sldList = presentationMLPackage.getMainPresentationPart().getContent();
    	Object sld = sldList.remove(source);
    	sldList.add(dest, sld);
    	final List<SldId> sldIdList = presentationMLPackage.getMainPresentationPart().getJaxbElement().getSldIdLst(false).getSldId();
    	final SldId sldId = sldIdList.remove(source);
    	sldIdList.add(dest, sldId);
    }

    public void moveLayoutSlide(String id, int start, String target)
    	throws InvalidFormatException {

    	setContextPartByTarget(target);
    	final SlideMasterPart targetMasterPart = (SlideMasterPart)getOperationDocument().getContextPart();
    	final SlideLayoutPart sourceLayoutPart = (SlideLayoutPart)getPartByTarget(id);
    	final RelationshipsPart sourceLayoutRelationshipsPart = sourceLayoutPart.getRelationshipsPart();
    	final Relationship sourceLayoutTargetRelation = sourceLayoutRelationshipsPart.getRelationshipByType(Namespaces.PRESENTATIONML_SLIDE_MASTER);

    	// retrieving the layout target that has to be used for this slide
		final String sourceLayoutTargetId = getRelTargetToIdMap()
			.get(sourceLayoutTargetRelation.getTarget());
		final SlideMasterPart sourceMasterPart = (SlideMasterPart)getIdToPartMap().get(sourceLayoutTargetId);

		// removing the layout relation to the source master page
		final List<SldLayoutId> sourceLayoutIdList = sourceMasterPart.getJaxbElement().getSldLayoutIdLst(true).getSldLayoutId();
		SldLayoutId sldLayoutId = null;
		for(int i = 0; i < sourceLayoutIdList.size(); i++) {
			sldLayoutId = sourceLayoutIdList.get(i);
			if(sldLayoutId.getId().toString().equals(id)) {
				sourceLayoutIdList.remove(i);
				sourceMasterPart.getRelationshipsPart().removeRelationship(sourceMasterPart.getRelationshipsPart().getRelationshipByID(sldLayoutId.getRid()));
				break;
			}
		}
		targetMasterPart.getJaxbElement().getSldLayoutIdLst(true).getSldLayoutId().add(start, sldLayoutId);
		sldLayoutId.setRid(targetMasterPart.addTargetPart(sourceLayoutPart, AddPartBehaviour.OVERWRITE_IF_NAME_EXISTS).getId());
		sourceLayoutRelationshipsPart.removeRelationship(sourceLayoutTargetRelation);
		sourceLayoutPart.addTargetPart(targetMasterPart);
	}

    public void insertField(JSONArray start, String target, String type, String representation, JSONObject attrs) throws Exception {

    	final CTTextField textField = (CTTextField)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1)
			.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.SIMPLE_FIELD).getObject();

    	textField.setId("{2640DB9D-575D-4FF8-9117-D1A6111C2CD0}");
    	textField.setType(type);
    	textField.setT(representation);
    }

    public void updateField(JSONArray start, String target, String type, String representation)
        	throws UnsupportedOperationException {

    	final TextFieldComponent tfComponent = (TextFieldComponent)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length());
    	if(type!=null) {
    		((CTTextField)tfComponent.getObject()).setType(type);
    	}
    	if(representation!=null) {
    		((CTTextField)tfComponent.getObject()).setT(representation);
    	}
    }

    public void group(JSONArray start, String target, JSONArray drawings, JSONObject attrs) throws Exception {

    	final List<Object> drawingChildObjects = new ArrayList<Object>();
    	for(int i = drawings.length()-1; i >= 0; i--) {
    		final IComponent<OfficeOpenXMLOperationDocument> child = new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1).getChildComponent(drawings.getInt(i));
    		drawingChildObjects.add(0, child.getObject());
    		child.splitStart(child.getComponentNumber(), SplitMode.DELETE);
    		child.delete(1);
    	}
    	final IComponent<OfficeOpenXMLOperationDocument> groupComponent = insertDrawing(start, target, "GROUP", attrs);
    	final GroupShape groupShape = (GroupShape)groupComponent.getObject();
    	groupShape.getContent().addAll(drawingChildObjects);

    	final JSONObject drawingAttrs = attrs!=null ? attrs.optJSONObject(OCKey.DRAWING.value()) : null;
    	Double grpRotation = null;
        double grpCenterX = 0;
        double grpCenterY = 0;
    	if(drawingAttrs!=null) {
    	    final Object rot = drawingAttrs.opt(OCKey.ROTATION.value());
    	    if(rot instanceof Number) {
    	        grpRotation = ((Number)rot).doubleValue();
    	    }
    	    if(grpRotation!=null) {
    	        // create the childsRect for the group, this is necessary to get the group center point
                Rectangle2D childsRect = new Rectangle2D.Double();
                for(Object child:groupShape.getContent()) {
                    final ITransform2D iTransform2D = ((ITransform2DAccessor)child).getXfrm(true);
                    final CTPoint2D chOff = iTransform2D.getOff(true);
                    final CTPositiveSize2D chExt = iTransform2D.getExt(true);

                    double chOffX = chOff.getX();
                    double chOffY = chOff.getY();
                    double chExtX = chExt.getCx();
                    double chExtY = chExt.getCy();

                    long rotation = iTransform2D.getRot() - Double.valueOf(grpRotation * 60000).longValue() % 21600000;
                    if(rotation < 0) {
                        rotation += 21600000;
                    }
                    if((rotation > 2700000 && rotation <= 8100000) || (rotation > 13500000 && rotation <= 18900000)) {

                        final double chHalfExtX = chExtX * 0.5;
                        final double chHalfExtY = chExtY * 0.5;

                        // swap chHalfExtX and chHalfExtY
                        chOffX = chOffX + chHalfExtX - chHalfExtY;
                        chOffY = chOffY + chHalfExtY - chHalfExtX;

                        // swap width and height
                        final double t = chExtX;
                        chExtX = chExtY;
                        chExtY = t;
                    }

                    final Rectangle2D childRect = new Rectangle2D.Double(chOffX, chOffY, chExtX, chExtY);
                    if(childsRect.isEmpty()) {
                        childsRect = childRect;
                    }
                    else {
                        childsRect = childsRect.createUnion(childRect);
                    }
                }
                if(!childsRect.isEmpty()) {
                    grpCenterX = childsRect.getCenterX();
                    grpCenterY = childsRect.getCenterY();
                }
    	    }
    	}

    	Rectangle2D childsRect = new Rectangle2D.Double();
    	for(Object child:groupShape.getContent()) {
			final ITransform2D iTransform2D = ((ITransform2DAccessor)child).getXfrm(true);
			final CTPoint2D chOff = iTransform2D.getOff(true);
			final CTPositiveSize2D chExt = iTransform2D.getExt(true);

            int newChildRotation =  grpRotation!=null ? Double.valueOf(iTransform2D.getRot() - grpRotation.doubleValue() * 60000.0).intValue() % 21600000 : iTransform2D.getRot();
            if(newChildRotation < 0) {
                newChildRotation += 21600000;
            }
            iTransform2D.setRot(newChildRotation);

            double chOffX = chOff.getX();
            double chOffY = chOff.getY();
            double chExtX = chExt.getCx();
            double chExtY = chExt.getCy();
            if(grpRotation!=null||newChildRotation!=0) {
    			double chHalfExtX = chExtX * 0.5;
                double chHalfExtY = chExtY * 0.5;
                double chCenterX = chOffX + chHalfExtX;
                double chCenterY = chOffY + chHalfExtY;
                if(grpRotation!=null) {
    			    final double[] pts = Tools.rotatePoint(chCenterX, chCenterY, grpCenterX, grpCenterY, -grpRotation);
    			    chCenterX = pts[0];
    			    chCenterY = pts[1];
    			    chOff.setX(Double.valueOf(chCenterX - chHalfExtX + 0.5).longValue());
                    chOff.setY(Double.valueOf(chCenterY - chHalfExtY + 0.5).longValue());
    			}
    			if(iTransform2D.getRot()!=0) {
    			    int rotation = iTransform2D.getRot() % 21600000;
    			    if(rotation < 0) {
    			        rotation += 21600000;
    			    }
    			    if((rotation > 2700000 && rotation <= 8100000) || (rotation > 13500000 && rotation <= 18900000)) {

    			        // swap chHalfExtX and chHalfExtY
    			        final double t1 = chHalfExtX;
    			        chHalfExtX = chHalfExtY;
    			        chHalfExtY = t1;

                        // swap width and height
                        final double t2 = chExtY;
                        chExtY = chExtX;
                        chExtX = t2;
    			    }
    			}
    			chOffX = Double.valueOf(chCenterX - chHalfExtX + 0.5).longValue();
    			chOffY = Double.valueOf(chCenterY - chHalfExtY + 0.5).longValue();
            }
			final Rectangle2D childRect = new Rectangle2D.Double(chOffX, chOffY, chExtX, chExtY);
			if(childsRect.isEmpty()) {
				childsRect = childRect;
			}
			else {
				childsRect = childsRect.createUnion(childRect);
			}
    	}
    	final CTGroupTransform2D iTransform2D = groupShape.getXfrm(true);
    	final CTPoint2D groupChildOffset = iTransform2D.getChOff(true);
    	groupChildOffset.setX(Double.valueOf(childsRect.getX()).longValue());
    	groupChildOffset.setY(Double.valueOf(childsRect.getY()).longValue());
    	final CTPositiveSize2D groupChildExt = iTransform2D.getChExt(true);
    	groupChildExt.setCx(Double.valueOf(childsRect.getWidth()).longValue());
    	groupChildExt.setCy(Double.valueOf(childsRect.getHeight()).longValue());
    	groupComponent.applyAttrsFromJSON(attrs);
    }

    public void ungroup(JSONArray start, String target, JSONArray drawings)
    	throws JSONException {

    	final int groupPosition = start.getInt(start.length()-1);
    	final IComponent<OfficeOpenXMLOperationDocument> parentComponent = new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1);
    	final DLList<Object> parentContent = (DLList<Object>)((IContentAccessor)parentComponent.getObject()).getContent();

    	final ShapeGroupComponent groupComponent = (ShapeGroupComponent)parentComponent.getChildComponent(groupPosition);
    	final GroupShape group = (GroupShape)groupComponent.getObject();
    	final CTGroupTransform2D groupTransform = group.getXfrm(true);
    	final CTPoint2D grpOff = groupTransform.getOff(true);
    	final CTPositiveSize2D grpExt = groupTransform.getExt(true);
    	final CTPoint2D grpChOff = groupTransform.getChOff(true);
    	final CTPositiveSize2D grpChExt = groupTransform.getChExt(true);

    	final double fXScale = (double)grpExt.getCx() / (grpChExt.getCx()==0 ? 1 : grpChExt.getCx());
    	final double fYScale = (double)grpExt.getCy() / (grpChExt.getCy()==0 ? 1 : grpChExt.getCy());

    	final double groupRotation = group.getXfrm(true).getRot() / 60000.0;
    	final DLList<Object> groupContent = group.getContent();

		groupComponent.splitStart(groupComponent.getComponentNumber(), SplitMode.DELETE);;
		groupComponent.delete(1);

		IComponent<OfficeOpenXMLOperationDocument> referenceComponent = parentComponent.getChildComponent(groupPosition);
		DLNode<Object> referenceNode = referenceComponent!=null ? referenceComponent.getNode() : null;

		for(int i = 0; i < groupContent.size(); i++) {
			if(drawings!=null&&i<drawings.length()) {
				referenceComponent = parentComponent.getChildComponent(drawings.getInt(i));
				referenceNode = referenceComponent!=null ? referenceComponent.getNode() : null;
			}
			final Object newChild = groupContent.get(i);
			if(newChild instanceof Child) {
				((Child)newChild).setParent(parentComponent.getObject());
			}
			if(newChild instanceof ITransform2DAccessor) {
				final ITransform2D childTransform = ((ITransform2DAccessor)newChild).getXfrm(true);
				final CTPositiveSize2D chExt = childTransform.getExt(true);
				final CTPoint2D chOff = childTransform.getOff(true);
				double chOffX = chOff.getX();
				double chOffY = chOff.getY();
				chExt.setCx(Double.valueOf(chExt.getCx()*fXScale).longValue());
				chExt.setCy(Double.valueOf(chExt.getCy()*fYScale).longValue());
				chOffX = (chOffX - grpChOff.getX())*fXScale + grpOff.getX();
				chOffY = (chOffY - grpChOff.getY())*fXScale + grpOff.getY();
				if(groupRotation!=0) {
					double[] pts = Tools.rotatePoint(chOffX, chOffY, chOffX + chExt.getCx() * 0.5, chOffY + chExt.getCy() * 0.5, -groupRotation);
					pts = Tools.rotatePoint(pts[0], pts[1], grpOff.getX() + (grpExt.getCx() * 0.5), grpOff.getY() + (grpExt.getCy() * 0.5), groupRotation);
					chOffX = pts[0];
					chOffY = pts[1];
					childTransform.setRot(Double.valueOf(childTransform.getRot()+groupRotation*60000.0).intValue()%21600000);
				}
				chOff.setX(Double.valueOf(chOffX + 0.5).longValue());
				chOff.setY(Double.valueOf(chOffY + 0.5).longValue());
                if(groupTransform.isFlipH()) {
                    // calculate new position after fliping via group x center axis
                    double gxc = getCenterX(groupTransform.getOff(true), groupTransform.getExt(true));
                    double cxc = getCenterX(childTransform.getOff(true), childTransform.getExt(true));
                    setCenterX(childTransform.getOff(true), childTransform.getExt(true), (gxc - cxc) + gxc);
                }
                if(groupTransform.isFlipV()) {
                    // calculate new position after fliping via group y center axis
                    double gyc = getCenterY(groupTransform.getOff(true), groupTransform.getExt(true));
                    double cyc = getCenterY(childTransform.getOff(true), childTransform.getExt(true));
                    setCenterY(childTransform.getOff(true), childTransform.getExt(true), (gyc - cyc) + gyc);
                }
				if(groupTransform.isFlipH()) {
				    childTransform.setFlipH(childTransform.isFlipH()?null:true);
				    if(childTransform.getRot()!=0) {
				        childTransform.setRot((21600000-childTransform.getRot())%21600000);
				    }
				}
				if(groupTransform.isFlipV()) {
				    childTransform.setFlipV(childTransform.isFlipV()?null:true);
                    if(childTransform.getRot()!=0) {
                        childTransform.setRot((21600000-childTransform.getRot())%21600000);
                    }
				}
			}
	        final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	        parentContent.addNode(referenceNode, newChildNode, true);
		}
    }

    private double getCenterX(CTPoint2D pos, CTPositiveSize2D ext) {
        return ext.getCx() * 0.5 + pos.getX();
    }

    private double getCenterY(CTPoint2D pos, CTPositiveSize2D ext) {
        return ext.getCy() * 0.5 + pos.getY();
    }

    private void setCenterX(CTPoint2D pos, CTPositiveSize2D ext, double cx) {
        pos.setX(Double.valueOf(cx - ext.getCx() * 0.5d).intValue());
    }

    private void setCenterY(CTPoint2D pos, CTPositiveSize2D ext, double cy) {
        pos.setY(Double.valueOf(cy - ext.getCy() * 0.5d).intValue());
    }

    public void move(JSONArray startPosition, String target, JSONArray endPosition, JSONArray toPosition)
    	throws JSONException {

        if(endPosition==null) {
            endPosition = startPosition;
        }
        else if(startPosition.length()!=endPosition.length())
            throw new JSONException("ooxml export: move operation, size of startPosition != size of endPosition");

        int i=0;
        boolean before = true;
        for(;i<startPosition.length()-1; i++) {
            if(startPosition.getInt(i)!=endPosition.getInt(i)) {
                break;
            }
        }
        if(i==startPosition.length()-1) {
            before = startPosition.getInt(i)>toPosition.getInt(i);
        }
        final IComponent<OfficeOpenXMLOperationDocument> sourceBegComponent = operationDocument.getRootComponent().getComponent(startPosition, startPosition.length());
        final IComponent<OfficeOpenXMLOperationDocument> sourceEndComponent = sourceBegComponent.getComponent(endPosition.getInt(startPosition.length()-1));
        final DLList<Object> sourceContent = (DLList<Object>)((IContentAccessor)sourceBegComponent.getParentComponent().getNode().getData()).getContent();
        sourceBegComponent.splitStart(startPosition.getInt(startPosition.length()-1), SplitMode.DELETE);
        sourceEndComponent.splitEnd(endPosition.getInt(startPosition.length()-1), SplitMode.DELETE);

        // destComponent is empty if the content is to be appended
        final IComponent<OfficeOpenXMLOperationDocument> destComponent = operationDocument.getRootComponent().getComponent(toPosition, toPosition.length());
        IComponent<OfficeOpenXMLOperationDocument> destParentComponent;
        if(destComponent!=null) {
            destComponent.splitStart(toPosition.getInt(toPosition.length()-1), SplitMode.DELETE);
            destParentComponent = destComponent.getParentComponent();
        }
        else {
            destParentComponent = operationDocument.getRootComponent().getComponent(toPosition, toPosition.length()-1);
        }
        if(destComponent!=null) {
            sourceContent.moveNodes(sourceBegComponent.getContextChild(null).getNode(), sourceEndComponent.getContextChild(null).getNode(), (DLList<Object>)((IContentAccessor)destParentComponent.getNode().getData()).getContent(),
                destComponent.getContextChild(null).getNode(), before, destParentComponent.getNode().getData());
        }
        else {
            sourceContent.moveNodes(sourceBegComponent.getContextChild(null).getNode(), sourceEndComponent.getContextChild(null).getNode(), (DLList<Object>)((IContentAccessor)destParentComponent.getNode().getData()).getContent(),
                ((DLList<Object>)((IContentAccessor)destParentComponent.getNode().getData()).getContent()).getLastNode(), false, destParentComponent.getNode().getData());
        }
    }

    public void insertRows(JSONArray start, String target, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

    	((ShapeGraphicComponent)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1))
    		.insertRows(start.getInt(start.length()-1), count, insertDefaultCells, referenceRow, attrs);
    }

    public void insertCells(JSONArray start, String target, int count, JSONObject attrs) throws Exception {

    	((TableRowComponent)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length()-1)).insertCells(start.getInt(start.length()-1), count, attrs);
    }

    public void insertColumn(JSONArray start, String target, JSONArray tableGrid, int gridPosition, String insertMode) {

    	((ShapeGraphicComponent)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length())).insertColumn(tableGrid, gridPosition, insertMode);
    }

    public void deleteColumns(JSONArray start, String target, int startGrid, int endGrid) {

    	((ShapeGraphicComponent)new RootComponent(operationDocument, (SerializationPart<?>)getPartByTarget(target)).getComponent(start, start.length())).deleteColumns(startGrid, endGrid);
    }

    public void changeOrInsertStyleSheet(boolean insert, String type, String styleId, String styleName, JSONObject attrs)
    	throws InvalidFormatException, FilterException, JSONException, PartUnrecognisedException {

    	if(!type.equals("table")) {
    		throw new RuntimeException("PPTX: only StyleSheetOpertions of type table allowed!");
    	}
    	final TableStylesPart tableStylesPart = getOperationDocument().getTableStylesPart(true);
		final Object o = tableStylesPart.getJaxbElement();
		if(o instanceof CTTableStyleList) {
			final CTTableStyleList tableStyles = (CTTableStyleList)o;
			final List<CTTableStyle> tableStyleList = tableStyles.getTblStyle();

			CTTableStyle tableStyle = null;
			if(insert) {
				tableStyle = new CTTableStyle();
				tableStyleList.add(tableStyle);
				tableStyle.setStyleId(styleId);
			}
			else {
				final Iterator<CTTableStyle> tableStyleIter = tableStyleList.iterator();
				while(tableStyleIter.hasNext()) {
					final CTTableStyle ts = tableStyleIter.next();
					if(styleId.equals(ts.getStyleId())) {
						tableStyle = ts;
						break;
					}
				}
			}
			if(tableStyle != null && styleName!=null&&!styleName.isEmpty()) {
				tableStyle.setStyleName(styleName);
			}
			if(attrs!=null) {
				DMLHelper.applyTableStyleFromJson(tableStyle, attrs, operationDocument, tableStylesPart);
			}
		}
    }

    public void deleteStyleSheet(String type, String styleId)
    	throws InvalidFormatException {

    	if(!type.equals("table")) {
    		throw new RuntimeException("PPTX: StyleSheetOpertions of type table allowed!");
    	}
    	final TableStylesPart tableStylesPart = getOperationDocument().getTableStylesPart(false);
		final Object o = tableStylesPart.getJaxbElement();
		final CTTableStyleList tableStyles = (CTTableStyleList)o;
		final List<CTTableStyle> tableStyleList = tableStyles.getTblStyle();
		final Iterator<CTTableStyle> tableStyleIter = tableStyleList.iterator();
		while(tableStyleIter.hasNext()) {
			final CTTableStyle tableStyle = tableStyleIter.next();
			if(styleId.equals(tableStyle.getStyleId())) {
				tableStyleIter.remove();
				return;
			}
		}
		throw new RuntimeException("PPTX: deleteStyleSheet, could not find style!");
    }

    private void insertComment(JSONArray start, JSONArray pos, String text, String date, int colorIndex, int authorId, String author, String initials, String providerId, String userId, int parentIndex)
        throws JSONException, InvalidFormatException, DatatypeConfigurationException {

        // we are adding a suffix to the author to prevent same authors with different provider which make problems
        final ICommentAuthorList<?> commentAuthorList = presentationMLPackage.getMainPresentationPart().getCommentAuthorList(true);
        final ICommentAuthor commentAuthor = authorId==-1 ? presentationMLPackage.getMainPresentationPart().getCommentAuthor(author, initials, providerId, userId, true) : getCommentAuthor(commentAuthorList, authorId);
        if(commentAuthor instanceof CTCommentAuthor && commentAuthorList instanceof CTCommentAuthorList) {
            if(colorIndex>=0) {
                ((CTCommentAuthor)commentAuthor).setClrIdx(colorIndex);
            }
            ((CTCommentAuthor)commentAuthor).setLastIdx(((CTCommentAuthorList)commentAuthorList).getNextIdx());
        }

        // getting the slide component will also set the current contextPart
        new RootComponent(getOperationDocument(), presentationMLPackage.getMainPresentationPart()).getComponent(start, 1);
        // so it is safe to assume the context part to be a SlidePart
        final ICommentList<?> iCommentList = presentationMLPackage.getMainPresentationPart().getCommentList((SlidePart)getOperationDocument().getContextPart(), true);
        final XMLGregorianCalendar dt = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
        final int index = start.getInt(1);

        final CTPoint2D point2d = pos != null ? new CTPoint2D(pos.getLong(0), pos.getLong(1)) : null;
        if(iCommentList instanceof org.pptx4j.pml.CTCommentList) {
            final org.pptx4j.pml.CTCommentList commentList = (org.pptx4j.pml.CTCommentList)iCommentList;
            final org.pptx4j.pml.CTComment comment = commentList.createComment(index);
            comment.setIdx(index);
            comment.setPos(point2d);
            comment.setAuthorId(commentAuthor.getId());
            comment.setDt(dt);
            comment.setText(text);
            if(parentIndex>=0) {
                final IComment parentComment = commentList.getContent().get(parentIndex);
                final CTParentCommentIdentifier parentCm = comment.getCommentThreading(true).getParentCm(true);
                parentCm.setIdx(parentComment.getIdx());
                parentCm.setAuthorId(parentComment.getAuthorId());
            }
        }
        else if(iCommentList instanceof org.pptx4j.pml_2018_8.CTCommentList) {
            final org.pptx4j.pml_2018_8.CTCommentList commentList = (org.pptx4j.pml_2018_8.CTCommentList)iCommentList;
            final Pair<Integer, Integer> indexes = commentList.getRootAndChildFromLinearIndex(index);
            if(parentIndex >= 0) {
                final Pair<Integer, Integer> parentIndexes = commentList.getRootAndChildFromLinearIndex(parentIndex);
                final CTComment parentComment = commentList.getContent().get(parentIndexes.getLeft().intValue());
                final DLList<CTCommentReply> replyList = parentComment.getReplyLst(true).getContent();
                final CTCommentReply reply = new CTCommentReply();
                final int replyIndex = indexes.getRight();
                replyList.add(replyIndex == -1 ? replyList.size() : replyIndex, reply);
                reply.setAuthorId(commentAuthor.getId());
                reply.setDt(dt);
                setSimpleText(reply.getTxBody(true), text);
            }
            else {
                final org.pptx4j.pml_2018_8.CTComment comment = new org.pptx4j.pml_2018_8.CTComment();
                final CTSlideMonikerList slideMonikerList = new CTSlideMonikerList();
                slideMonikerList.setDocMk(new CTDocumentMoniker());
                final CTSlideMoniker slideMoniker = new CTSlideMoniker();
                slideMonikerList.setSldMk(slideMoniker);
                slideMoniker.setSld(start.getInt(0) + 256);
                comment.getCommentAnchor().add(slideMonikerList);

                final int commentIndex = indexes.getLeft().intValue();
                commentList.getContent().add(commentIndex == -1 ? commentList.getContent().size() : commentIndex, comment);
                if(pos!=null) {
                    comment.setPos(point2d);
                }
                comment.setAuthorId(commentAuthor.getId());
                comment.setDt(dt);
                setSimpleText(comment.getTxBody(true), text);
            }
        }
    }

    private void changeComment(JSONArray start, JSONArray pos, String text) throws InvalidFormatException, JSONException {
        // getting the slide component will also set the current contextPart
        new RootComponent(getOperationDocument(), presentationMLPackage.getMainPresentationPart()).getComponent(start, 1);
        // so it is safe to assume the context part to be a SlidePart
        final ICommentList<?> iCommentList = presentationMLPackage.getMainPresentationPart().getCommentList((SlidePart)getOperationDocument().getContextPart(), true);
        final CTPoint2D point2d = pos != null ? new CTPoint2D(pos.getLong(0), pos.getLong(1)) : null;
        final int index = start.getInt(1);

        if(iCommentList instanceof org.pptx4j.pml.CTCommentList) {
            final org.pptx4j.pml.CTCommentList commentList = (org.pptx4j.pml.CTCommentList)iCommentList;
            final org.pptx4j.pml.CTComment comment = commentList.getContent().get(index);
            if(point2d != null) {
                comment.setPos(point2d);
            }
            if(text != null) {
                comment.setText(text);
            }
        }
        else if(iCommentList instanceof org.pptx4j.pml_2018_8.CTCommentList) {
            final org.pptx4j.pml_2018_8.CTCommentList commentList = (org.pptx4j.pml_2018_8.CTCommentList)iCommentList;
            final Pair<Integer, Integer> indexes = commentList.getRootAndChildFromLinearIndex(index);
            final CTComment comment = commentList.getContent().get(indexes.getLeft().intValue());
            if(indexes.getRight()!=-1 && text!=null) {
                setSimpleText(comment.getReplyLst(false).getContent().get(indexes.getRight().intValue()).getTxBody(false), text);
            }
            else {
                if(point2d != null) {
                    comment.setPos(point2d);
                }
                if(text != null) {
                    setSimpleText(comment.getTxBody(false), text);
                }
            }
        }
    }

    private ICommentAuthor getCommentAuthor(ICommentAuthorList<?> commentAuthorList, long id) {
        DLNode<ICommentAuthor> commentAuthorNode = (DLNode<ICommentAuthor>)commentAuthorList.getContent().getFirstNode();
        while(commentAuthorNode!=null) {
            if(commentAuthorNode.getData().getId()==id) {
                return commentAuthorNode.getData();
            }
            commentAuthorNode = commentAuthorNode.getNext();
        }
        return null;
    }

    private String applyMentions(String text, JSONArray mentions) throws JSONException, InvalidFormatException {
        if(text == null || mentions == null || mentions.length() == 0) {
            return text;
        }
        final ICommentAuthorList<?> commentAuthorList = presentationMLPackage.getMainPresentationPart().getCommentAuthorList(true);
        final StringBuilder builder = new StringBuilder(text);
        for(int i = mentions.length() - 1; i >= 0; i--) {
            final JSONObject mention = mentions.getJSONObject(i);
            final int pos = mention.getInt(OCKey.POS.value());
            final int length = mention.getInt(OCKey.LENGTH.value());
            builder.insert(pos + length, ']');
            builder.insert(pos, "[");

            // adding mentioned person to the commentAuthors list
            final String displayName = mention.optString(OCKey.DISPLAY_NAME.value(), "");
            final String userId = mention.optString(OCKey.USER_ID.value(), "");
            final String providerId = mention.optString(OCKey.PROVIDER_ID.value(), "");
            if(!displayName.isEmpty() && !userId.isEmpty() && !providerId.isEmpty()) {
                presentationMLPackage.getMainPresentationPart().getCommentAuthor(applyProviderNameExtension(displayName, providerId), "", providerId, userId, true);
            }
        }
        return builder.toString();
    }

    private void setSimpleText(CTTextBody textBody, String text) {
        textBody.setBodyPr(new CTTextBodyProperties());
        textBody.setLstStyle(new CTTextListStyle());
        final String[] paragraphs = text.split("\\n", -1);
        for(int i=0; i < paragraphs.length; i++) {
            final CTTextParagraph paragraph = new CTTextParagraph();
            textBody.getContent().add(paragraph);
            final CTRegularTextRun tr = new CTRegularTextRun();
            paragraph.getContent().add(tr);
            tr.setT(paragraphs[i]);
        }
    }

    private String applyProviderNameExtension(String author, String providerId) {
        if("ox".equals(providerId)) {
            return author + " [~ox]";
        }
        return author;
    }

    private void deleteComment(JSONArray start)
        throws InvalidFormatException, JSONException {

        // getting the slide component will also set the current contextPart
        new RootComponent(getOperationDocument(), presentationMLPackage.getMainPresentationPart()).getComponent(start, 1);
        // so it is safe to assume the context part to be a SlidePart
        final ICommentList<?> iCommentList = presentationMLPackage.getMainPresentationPart().getCommentList((SlidePart)getOperationDocument().getContextPart(), true);
        if(iCommentList instanceof org.pptx4j.pml.CTCommentList) {
            iCommentList.getContent().remove(start.getInt(1));

            // remove comments part from slide
            if(iCommentList.getContent().isEmpty()) {
                operationDocument.getContextPart().getRelationshipsPart().removeRelationshipByType(Namespaces.COMMENTS);
            }
        }
        else if(iCommentList instanceof org.pptx4j.pml_2018_8.CTCommentList) {
            org.pptx4j.pml_2018_8.CTCommentList commentList = (org.pptx4j.pml_2018_8.CTCommentList)iCommentList;

            final Pair<Integer, Integer> indexes = commentList.getRootAndChildFromLinearIndex(start.getInt(1));
            if(indexes.getRight().intValue() != -1) {
                final org.pptx4j.pml_2018_8.CTComment comment = commentList.getContent().getNode(indexes.getLeft()).getData(); // removing a reply
                final CTCommentReplyList replyList = comment.getReplyLst(true);
                replyList.getContent().remove(indexes.getRight().intValue());
                if(replyList.getContent().isEmpty()) {
                    comment.setReplyLst(null);
                }
            }
            else {
                iCommentList.getContent().remove(indexes.getLeft().intValue());

                // remove comments part from slide
                if(iCommentList.getContent().isEmpty()) {
                    operationDocument.getContextPart().getRelationshipsPart().removeRelationshipByType(Namespaces.MODERN_COMMENTS);
                }
            }
        }
    }
}
