/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.tools;

import java.util.HashSet;
import java.util.Iterator;
import jakarta.xml.bind.JAXBException;
import org.docx4j.dml.CTAudioCD;
import org.docx4j.dml.CTBlipFillProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.dml.CTPresetGeometry2D;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.ITransform2D;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PresentationML.ICommonTimingAccessor;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CTBuildList;
import org.pptx4j.pml.CTGraphicalObjectFrame;
import org.pptx4j.pml.CTPlaceholder;
import org.pptx4j.pml.CTSlideTiming;
import org.pptx4j.pml.CTTLCommonBehaviorData;
import org.pptx4j.pml.CTTLCommonMediaNodeData;
import org.pptx4j.pml.CTTLCommonTimeNodeData;
import org.pptx4j.pml.CTTLShapeTargetElement;
import org.pptx4j.pml.CTTLTimeCondition;
import org.pptx4j.pml.CTTLTimeConditionList;
import org.pptx4j.pml.CTTLTimeNodeSequence;
import org.pptx4j.pml.CTTLTimeTargetElement;
import org.pptx4j.pml.CTTimeNodeList;
import org.pptx4j.pml.CxnSp;
import org.pptx4j.pml.CxnSp.NvCxnSpPr;
import org.pptx4j.pml.GroupShape;
import org.pptx4j.pml.GroupShape.NvGrpSpPr;
import org.pptx4j.pml.ICommonBehaviorDataAccessor;
import org.pptx4j.pml.ICommonMediaNodeDataAccessor;
import org.pptx4j.pml.ICommonTimeNodeDataAccessor;
import org.pptx4j.pml.INvPrAccessor;
import org.pptx4j.pml.ISpidAccessor;
import org.pptx4j.pml.NvPr;
import org.pptx4j.pml.ObjectFactory;
import org.pptx4j.pml.Pic;
import org.pptx4j.pml.Pic.NvPicPr;
import org.pptx4j.pml.STPlaceholderSize;
import org.pptx4j.pml.STPlaceholderType;
import org.pptx4j.pml.Shape;
import org.pptx4j.pml.Shape.NvSpPr;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.OfficeOpenXMLComponent;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.components.PptxComponent;
import com.openexchange.office.filter.ooxml.pptx.components.SlideComponent;

final public class PMLShapeHelper {

	public static void applyAttrsFromJSON(OfficeOpenXMLOperationDocument operationDocument, JSONObject attrs, INvPrAccessor iNvPrAccessor) throws Exception {

		final JSONObject presentationAttrs = attrs.optJSONObject(OCKey.PRESENTATION.value());
		if(presentationAttrs!=null) {
			final Object phType = presentationAttrs.opt(OCKey.PH_TYPE.value());
			if(phType instanceof String) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, true);
				placeholder.setType(STPlaceholderType.fromValue((String)phType));
			}
			else if(phType==JSONObject.NULL) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, false);
				if(placeholder!=null) {
					placeholder.setType(null);
				}
			}
 			final Object phSize = presentationAttrs.opt(OCKey.PH_SIZE.value());
 			if(phSize instanceof String) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, true);
				placeholder.setSz(STPlaceholderSize.fromValue((String)phSize));
 			}
 			else if(phSize==JSONObject.NULL) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, false);
				if(placeholder!=null) {
					placeholder.setSz(null);
				}
 			}
			final Object phIndex = presentationAttrs.opt(OCKey.PH_INDEX.value());
			if(phIndex instanceof Number) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, true);
				placeholder.setIdx(((Number)phIndex).longValue());
			}
			else if(phIndex==JSONObject.NULL) {
				final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, false);
				if(placeholder!=null) {
					placeholder.setIdx(null);
				}
			}
			final Object customPrompt = presentationAttrs.opt(OCKey.CUSTOM_PROMPT.value());
			if(customPrompt!=null) {
			    if(customPrompt==JSONObject.NULL) {
	                final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, false);
	                if(placeholder!=null) {
	                    placeholder.setHasCustomPrompt(null);
	                }
			    }
			    else {
	                final CTPlaceholder placeholder = getCTPlaceholder(iNvPrAccessor, true);
	                placeholder.setHasCustomPrompt((Boolean)customPrompt);
			    }
			}
		}
	}

	private static CTPlaceholder getCTPlaceholder(INvPrAccessor iNvPrAccessor, boolean forceCreate) {
		NvPr nvPr = iNvPrAccessor.getNvPr(forceCreate);
		if(nvPr==null) {
			return null;
		}
		CTPlaceholder placeholder = nvPr.getPh();
		if(placeholder==null) {
			if(!forceCreate) {
				return null;
			}
			placeholder = Context.getpmlObjectFactory().createCTPlaceholder();
			nvPr.setPh(placeholder);
		}
		return placeholder;
	}

	public static JSONObject createJSONAttrs(JSONObject attrs, INvPrAccessor iNvPrAccessor)
		throws JSONException {

		final NvPr nvPr = iNvPrAccessor.getNvPr(false);
		if(nvPr==null) {
			return attrs;
		}

		final JSONObject initialPresentationAttrs = attrs.optJSONObject(OCKey.PRESENTATION.value());
		final JSONObject presentationAttrs = initialPresentationAttrs!=null ? initialPresentationAttrs : new JSONObject();

		final CTPlaceholder placeholder = nvPr.getPh();
		if(placeholder!=null) {
			if(placeholder.getType()!=STPlaceholderType.OBJ) {
				presentationAttrs.put(OCKey.PH_TYPE.value(), placeholder.getType().value());
			}
			if(placeholder.getSz()!=STPlaceholderSize.FULL) {
				presentationAttrs.put(OCKey.PH_SIZE.value(), placeholder.getSz().value());
			}
			if(placeholder.getIdx()>0) {
				presentationAttrs.put(OCKey.PH_INDEX.value(), placeholder.getIdx());
			}
			if(placeholder.isHasCustomPrompt()!=null) {
			    presentationAttrs.put(OCKey.CUSTOM_PROMPT.value(), placeholder.isHasCustomPrompt().booleanValue());
			}
		}
		final CTAudioCD audioCD = nvPr.getAudioCd();
		if(audioCD!=null) {
		    presentationAttrs.put(OCKey.PH_AUDIO_CD.value(), true);
		}
		if(nvPr.getWavAudioFile()!=null || nvPr.getAudioFile()!=null) {
		    presentationAttrs.put(OCKey.PH_AUDIO_FILE.value(), true);
		}
		if(nvPr.getVideoFile()!=null || nvPr.getQuickTimeFile()!=null) {
		    presentationAttrs.put(OCKey.PH_VIDEO.value(), true);
		}

        if(initialPresentationAttrs==null&&!presentationAttrs.isEmpty()) {
			attrs.put(OCKey.PRESENTATION.value(), presentationAttrs);
		}
        return attrs;
	}

	public static SlideComponent getSlideComponent(IComponent<OfficeOpenXMLOperationDocument> component) {
		while(!(component instanceof SlideComponent)) {
			component = component.getParentComponent();
		}
		return (SlideComponent)component;
	}

	public static Shape createShape() {
		final ObjectFactory pmlObjectFactory = Context.getpmlObjectFactory();
		final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();

		final Shape shape = pmlObjectFactory.createShape();
		final NvSpPr nvSpPr = pmlObjectFactory.createShapeNvSpPr();
		nvSpPr.setCNvPr(createNonVisualDrawingProps());
		nvSpPr.setCNvSpPr(dmlObjectFactory.createCTNonVisualDrawingShapeProps());
		nvSpPr.setNvPr(pmlObjectFactory.createNvPr());
		shape.setNvSpPr(nvSpPr);
		return shape;
	}

	public static GroupShape createGroupShape() {
		final ObjectFactory pmlObjectFactory = Context.getpmlObjectFactory();
		final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();

		final GroupShape groupShape = pmlObjectFactory.createGroupShape();
		final NvGrpSpPr nvSpPr = pmlObjectFactory.createGroupShapeNvGrpSpPr();
		nvSpPr.setCNvPr(createNonVisualDrawingProps());
		nvSpPr.setCNvGrpSpPr(dmlObjectFactory.createCTNonVisualGroupDrawingShapeProps());
		nvSpPr.setNvPr(pmlObjectFactory.createNvPr());
		groupShape.setNvGrpSpPr(nvSpPr);
		groupShape.setGrpSpPr(dmlObjectFactory.createCTGroupShapeProperties());
		createXFrm(groupShape.getXfrm(true));
		return groupShape;
	}

	public static CxnSp createConnectorShape() {
		final ObjectFactory pmlObjectFactory = Context.getpmlObjectFactory();
		final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();

		final CxnSp cxnSp = pmlObjectFactory.createCxnSp();
		final NvCxnSpPr nvSpPr = pmlObjectFactory.createCxnSpNvCxnSpPr();
		nvSpPr.setCNvPr(createNonVisualDrawingProps());
		nvSpPr.setCNvCxnSpPr(dmlObjectFactory.createCTNonVisualConnectorProperties());
		nvSpPr.setNvPr(pmlObjectFactory.createNvPr());
		cxnSp.setNvCxnSpPr(nvSpPr);
		cxnSp.setSpPr(dmlObjectFactory.createCTShapeProperties());
		createXFrm(cxnSp.getXfrm(true));
		return cxnSp;
	}

	public static Pic createImage() {
		final ObjectFactory pmlObjectFactory = Context.getpmlObjectFactory();
		final org.docx4j.dml.ObjectFactory dmlObjectFactory = Context.getDmlObjectFactory();

		final Pic pic = Context.getpmlObjectFactory().createPic();
		final NvPicPr nvSpPr = pmlObjectFactory.createPicNvPicPr();
		nvSpPr.setCNvPr(createNonVisualDrawingProps());
		nvSpPr.setCNvPicPr(dmlObjectFactory.createCTNonVisualPictureProperties());
		nvSpPr.getCNvPicPr().getLocks(true).setNoChangeAspect(Boolean.TRUE);
		nvSpPr.setNvPr(pmlObjectFactory.createNvPr());
		pic.setNvPicPr(nvSpPr);
		final CTShapeProperties spPr = dmlObjectFactory.createCTShapeProperties();
		final CTPresetGeometry2D presetGeometry = dmlObjectFactory.createCTPresetGeometry2D();
		presetGeometry.setPrst(STShapeType.RECT);
		presetGeometry.setAvLst(dmlObjectFactory.createCTGeomGuideList());
		spPr.setPrstGeom(presetGeometry);
		pic.setSpPr(spPr);
		final CTBlipFillProperties blipFillProperties = pic.getBlipFill(true);
		blipFillProperties.getStretch(true).setFillRect(dmlObjectFactory.createCTRelativeRect());
		return pic;
	}

	final private static String graphicalObjectFrameTableTemplate =
		"<p:graphicFrame xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" xmlns:p=\"http://schemas.openxmlformats.org/presentationml/2006/main\">" +
		"	<p:nvGraphicFramePr>" +
		"		<p:cNvGraphicFramePr>" +
		"			<a:graphicFrameLocks noGrp=\"1\"/>" +
		"		</p:cNvGraphicFramePr>" +
		"		<p:nvPr/>" +
		"	</p:nvGraphicFramePr>" +
		"	<p:xfrm>" +
		"		<a:off x=\"0\" y=\"0\"/>" +
		"		<a:ext cx=\"1\" cy=\"1\"/>" +
		"	</p:xfrm>" +
		"	<a:graphic>" +
		"		<a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/table\">" +
		"			<a:tbl>" +
		"				<a:tblPr firstRow=\"1\" bandRow=\"1\"/>" +
		"			</a:tbl>" +
		"		</a:graphicData>" +
		"	</a:graphic>" +
		"</p:graphicFrame>";

	public static CTGraphicalObjectFrame createGraphicalObjectFrame(ComponentType type)
		throws JAXBException {

		final java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();
		final CTGraphicalObjectFrame frame = (CTGraphicalObjectFrame)org.docx4j.XmlUtils.unmarshallFromTemplate(graphicalObjectFrameTableTemplate, /*TODO provide package*/ null, mappings, org.pptx4j.jaxb.Context.getJcPml());
        final CTNonVisualDrawingProps cNvPr = frame.getNonVisualDrawingProperties(true);
        cNvPr.setName("Frame 1");
        cNvPr.setId(null);
        frame.getNonVisualDrawingShapeProperties(true);
        return frame;
	}

	public static CTNonVisualDrawingProps createNonVisualDrawingProps() {
		final CTNonVisualDrawingProps nonVisualDrawingProps = Context.getDmlObjectFactory().createCTNonVisualDrawingProps();
		nonVisualDrawingProps.setId(null);
		nonVisualDrawingProps.setName("");
		return nonVisualDrawingProps;
	}

	public static ITransform2D createXFrm(ITransform2D xFrm) {
		final CTPoint2D off = Context.getDmlObjectFactory().createCTPoint2D();
		off.setX(0);
		off.setY(0);
		xFrm.setOff(off);
		final CTPositiveSize2D ext = Context.getDmlObjectFactory().createCTPositiveSize2D();
		ext.setCx(0);
		ext.setCy(0);
		xFrm.setExt(ext);
		return xFrm;
	}

	public static void removeShapeReferences(PptxOperationDocument operationDocument, int id) {
		CTSlideTiming slideTiming = null;
		final Part contextPart = operationDocument.getContextPart();
		if(contextPart instanceof ICommonTimingAccessor) {
			slideTiming = ((ICommonTimingAccessor)contextPart).getTiming();
		}
		if(slideTiming!=null) {
			final CTTimeNodeList timeNodeList = slideTiming.getTnLst();
			if(timeNodeList!=null) {
				final HashSet<String> targetSpids = new HashSet<String>();
				if(getTargetSpids(timeNodeList, targetSpids, id)) {
					slideTiming.setTnLst(null);
				}
			}
			final CTBuildList buildList = slideTiming.getBldLst();
			if(buildList!=null) {
				final Iterator<Object> buildListIter = buildList.getContent().iterator();
				while(buildListIter.hasNext()) {
					final Object build = buildListIter.next();
					if(build instanceof ISpidAccessor) {
						final String spid = ((ISpidAccessor)build).getSpid();
						if(spid!=null) {
							try {
								final long s = Integer.parseInt(spid);
								if(s==id) {
									buildListIter.remove();
								}
							}
							catch (NumberFormatException e) {
							    //
							}
						}
					}
				}
			}
		}
	}

	private static boolean getTargetSpids(IContentAccessor contentAccessor, HashSet<String> spids, int id) {
		final Iterator<Object> contentIter = contentAccessor.getContent().iterator();
		while(contentIter.hasNext()) {
			final Object content = contentIter.next();
			switch(content.getClass().getSimpleName()) {
				case "CTTLTimeNodeParallel": {
					break;
				}
				case "CTTLTimeNodeSequence": {
					if(getTargetSpidForTimeConditionList(((CTTLTimeNodeSequence)content).getPrevCondLst(), spids, id)) {
						return true;
					}
					if(getTargetSpidForTimeConditionList(((CTTLTimeNodeSequence)content).getNextCondLst(), spids, id)) {
						return true;
					}
					break;
				}
				case "CTTLTimeNodeExclusive": {
					break;
				}
				case "CTTLAnimateBehavior": {
					break;
				}
				case "CTTLAnimateColorBehavior": {
					break;
				}
				case "CTTLAnimateEffectBehavior": {
					break;
				}
				case "CTTLAnimateMotionBehavior": {
					break;
				}
				case "CTTLAnimateRotationBehavior": {
					break;
				}
				case "CTTLAnimateScaleBehavior": {
					break;
				}
				case "CTTLCommandBehavior": {
					break;
				}
				case "CTTLSetBehavior": {
					break;
				}
				case "CTTLMediaNodeAudio":
				// PASSTROUGH INTENDED !!!
				case "CTTLMediaNodeVideo": {
					final CTTLCommonMediaNodeData commonMediaNode = ((ICommonMediaNodeDataAccessor)content).getCMediaNode();
					if(commonMediaNode!=null) {
						if(getTargetSpidForCommonTimeNodeData((ICommonTimeNodeDataAccessor)commonMediaNode.getCTn(), spids, id)) {
							return true;
						}
						if(getTargetSpidForTimeTargetElement(commonMediaNode.getTgtEl(), spids, id)) {
							return true;
						}
					}
					break;
				}
			}
			if(content instanceof ICommonTimeNodeDataAccessor) {
				if(getTargetSpidForCommonTimeNodeData((ICommonTimeNodeDataAccessor)content, spids, id)) {
					return true;
				}
			}
			if(content instanceof ICommonBehaviorDataAccessor) {
				final CTTLCommonBehaviorData commonBehaviorData = ((ICommonBehaviorDataAccessor)content).getCBhvr();
				if(commonBehaviorData!=null) {
					if(getTargetSpidForCommonTimeNodeData(commonBehaviorData, spids, id)) {
						return true;
					}
					if(getTargetSpidForTimeTargetElement(commonBehaviorData.getTgtEl(), spids, id)) {
						return true;
					}
				}
			}
			if(content instanceof IContentAccessor) {
				if(getTargetSpids((IContentAccessor)content, spids, id)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean getTargetSpidForCommonTimeNodeData(ICommonTimeNodeDataAccessor iCommonTimeNodeDataAccessor, HashSet<String> spids, int id) {
		final CTTLCommonTimeNodeData commonTimeNodeData = iCommonTimeNodeDataAccessor.getCTn();
		if(commonTimeNodeData!=null) {
			CTTimeNodeList timeNodeList = commonTimeNodeData.getChildTnLst();
			if(timeNodeList!=null) {
				if(getTargetSpids(timeNodeList, spids, id)) {
					return true;
				}
			}
			timeNodeList = commonTimeNodeData.getSubTnLst();
			if(timeNodeList!=null) {
				if(getTargetSpids(timeNodeList, spids, id)) {
					return true;
				}
			}
			if(getTargetSpidForTimeConditionList(commonTimeNodeData.getEndCondLst(), spids, id)) {
				return true;
			}
			if(getTargetSpidForTimeConditionList(commonTimeNodeData.getStCondLst(), spids, id)) {
				return true;
			}
			if(getTargetSpidForTimeCondition(commonTimeNodeData.getEndSync(), spids, id)) {
				return true;
			}
		}
		return false;
	}
	private static boolean getTargetSpidForTimeConditionList(CTTLTimeConditionList timeConditionList, HashSet<String> spids, int id) {
		if(timeConditionList!=null) {
			final Iterator<CTTLTimeCondition> timeConditionListIter = timeConditionList.getCond().iterator();
			while(timeConditionListIter.hasNext()) {
				if(getTargetSpidForTimeCondition(timeConditionListIter.next(), spids, id)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean getTargetSpidForTimeCondition(CTTLTimeCondition timeCondition, HashSet<String> spids, int id) {
		if(timeCondition!=null) {
			if(getTargetSpidForTimeTargetElement(timeCondition.getTgtEl(), spids, id)) {
				return true;
			}
		}
		return false;
	}

	private static boolean getTargetSpidForTimeTargetElement(CTTLTimeTargetElement timeTargetElement, HashSet<String> spids, int id) {
		if(timeTargetElement!=null) {
			if(getTargetSpidForShapeTargetElement(timeTargetElement.getSpTgt(), spids, id)) {
				return true;
			}
			if(getTargetSpid(timeTargetElement.getInkTgt(), spids, id)) {
				return true;
			}
		}
		return false;
	}

	private static boolean getTargetSpidForShapeTargetElement(CTTLShapeTargetElement shapeTargetElement, HashSet<String> spids, int id) {
		if(shapeTargetElement!=null) {
			if(getTargetSpid(shapeTargetElement, spids, id)) {
				return true;
			}
			if(getTargetSpid(shapeTargetElement.getSubSp(), spids, id)) {
				return true;
			}
		}
		return false;
	}

	private static boolean getTargetSpid(ISpidAccessor spidAccessor, HashSet<String> spids, int id) {
		if(spidAccessor!=null) {
			final String spid = spidAccessor.getSpid();
			if(spid!=null) {
				spids.add(spid);
				try {
					if(Long.parseLong(spid)==id) {
						return true;
					}
				}
				catch(NumberFormatException e) {
				    //
				};
			}
		}
		return false;
	}

    public static int getSlideNumberFromComponent(PptxComponent component) {
        ComponentContext<OfficeOpenXMLOperationDocument> context = component.getParentContext();
        while(context != null && !(context instanceof SlideComponent)) {
            context = context.getParentContext();
        }
        return context instanceof SlideComponent ? ((SlideComponent)context).getComponentNumber() : 0;
    }
}
