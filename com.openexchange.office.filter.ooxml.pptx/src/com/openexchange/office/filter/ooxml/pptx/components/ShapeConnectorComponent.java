/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTNonVisualDrawingProps;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CxnSp;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ShapeConnectorComponent extends PptxComponent implements IShapeComponent, IShapeType {

	final CxnSp connectorShape;

	public ShapeConnectorComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
        connectorShape = (CxnSp)getObject();
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        return null;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        PMLShapeHelper.applyAttrsFromJSON(operationDocument, attrs, connectorShape);
        DMLHelper.applyConnectionPropertiesFromJson(attrs, connectorShape.getNvCxnSpPr().getCNvCxnSpPr());
        com.openexchange.office.filter.ooxml.drawingml.Shape.applyAttrsFromJSON(operationDocument, attrs, connectorShape, !(getParentComponent() instanceof ShapeGroupComponent), true);
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

        PMLShapeHelper.createJSONAttrs(attrs, connectorShape);
        DMLHelper.createJsonFromConnectionProperties(attrs, connectorShape.getNvCxnSpPr().getCNvCxnSpPr());
        com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(operationDocument, attrs, connectorShape, !(getParentComponent() instanceof ShapeGroupComponent));
    	return attrs;
    }

	@Override
	public Integer getId() {
		final CTNonVisualDrawingProps nvdProps = connectorShape.getNonVisualDrawingProperties(false);
		return nvdProps!=null ? nvdProps.getId() : null;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.CONNECTOR;
	}

	@Override
	public boolean isPresentationObject() {
		return false;
	}
}
