/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import org.docx4j.dml.CTBlipFillProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTShapeProperties;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.Pic;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.drawingml.Picture;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ShapePicComponent extends PptxComponent implements IShapeComponent, IShapeType {

	final Pic pic;

	public ShapePicComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.pic = (Pic)getObject();
	}

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        return null;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

    	PMLShapeHelper.applyAttrsFromJSON(operationDocument, attrs, pic);
    	Picture.applyAttrsFromJSON(operationDocument, attrs, operationDocument.getContextPart(), pic, !(getParentComponent() instanceof ShapeGroupComponent));
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	PMLShapeHelper.createJSONAttrs(attrs, pic);
    	if (getType()== ShapeType.IMAGE) {
    	    Picture.createJSONAttrs(operationDocument, attrs, operationDocument.getContextPart(), pic, !(getParentComponent() instanceof ShapeGroupComponent));
    	}
    	else {
            com.openexchange.office.filter.ooxml.drawingml.Shape.createJSONAttrs(getOperationDocument(), attrs, pic, !(getParentComponent() instanceof ShapeGroupComponent));
            Tools.addFamilyAttribute(attrs, OCKey.SHAPE, OCKey.ANCHOR, "centered");
            final CTBlipFillProperties blipFill = pic.getBlipFill(false);
            if(blipFill!=null) {
                JSONObject fillAttrs = attrs.optJSONObject(OCKey.FILL.value());
                if(fillAttrs == null) {
                    fillAttrs = new JSONObject();
                    attrs.put(OCKey.FILL.value(), fillAttrs);
                }
                JSONObject bitmapAttrs = fillAttrs.optJSONObject(OCKey.BITMAP.value());
                if(bitmapAttrs == null) {
                    bitmapAttrs = new JSONObject();
                    fillAttrs.put(OCKey.BITMAP.value(), bitmapAttrs);
                }
                DMLHelper.createJsonFromBlipFillProperties(operationDocument.getContextPart(), bitmapAttrs, blipFill, null);
                fillAttrs.put(OCKey.TYPE.value(), "bitmap");
            }
    	}
    	return attrs;
    }

	@Override
	public Integer getId() {
		final CTNonVisualDrawingProps nvdProps = pic.getNonVisualDrawingProperties(false);
		return nvdProps!=null ? nvdProps.getId() : null;
	}

	@Override
	public ShapeType getType() {
		return hasShapeGeometry() ? ShapeType.SHAPE : ShapeType.IMAGE;
	}

	@Override
	public boolean isPresentationObject() {
		return false;
	}

	private boolean hasShapeGeometry() {
	    final CTShapeProperties spPr = pic.getSpPr();
	    return spPr != null && spPr.getCustGeom(false) != null;
	}
}
