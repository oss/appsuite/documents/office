/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.XmlUtils;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTRegularTextRun;
import org.docx4j.dml.CTTable;
import org.docx4j.dml.CTTableCell;
import org.docx4j.dml.CTTableCellProperties;
import org.docx4j.dml.CTTableCol;
import org.docx4j.dml.CTTableGrid;
import org.docx4j.dml.CTTableRow;
import org.docx4j.dml.CTTextParagraph;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.GraphicData;
import org.docx4j.mce.AlternateContent;
import org.docx4j.mce.AlternateContent.Choice;
import org.docx4j.mce.AlternateContent.Fallback;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.relationships.Relationship;
import org.docx4j.vml.CTShape;
import org.docx4j.vml.root.Xml;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.pml.CTGraphicalObjectFrame;
import org.pptx4j.pml.CTOleObject;
import org.pptx4j.pml.Pic;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.components.IShapeType;
import com.openexchange.office.filter.ooxml.components.ShapeType;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.drawingml.Picture;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class ShapeGraphicComponent extends PptxComponent implements IShapeComponent, IShapeType {

	final private CTGraphicalObjectFrame graphicalObjectFrame;
	private ShapeType shapeType = ShapeType.UNDEFINED;
	private Pic pic = null;
	private CTTable tbl = null;
	private String shape2png = null;

	public ShapeGraphicComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);

        this.graphicalObjectFrame = (CTGraphicalObjectFrame)getObject();

        if(shapeType == ShapeType.UNDEFINED) {
            pic = getPicture(false);
            if(pic!=null) {
                shapeType = ShapeType.IMAGE;
            }
        }
        if(shapeType == ShapeType.UNDEFINED) {
            tbl = getTable();
            if(tbl!=null) {
                shapeType = ShapeType.TABLE;
            }
        }
        if(shapeType == ShapeType.UNDEFINED) {
            shape2png = DMLHelper.getReplacementUrlFromGraphicFrame(parentContext, graphicalObjectFrame, graphicalObjectFrame.getXfrm(false), PMLShapeHelper.getSlideNumberFromComponent(this),  this.getComponentNumber(), "pptx");
            if(shape2png!=null) { // the documentconverter needs to be used to create png replacement for this shape
                getOperationDocument().registerShape2pngReplacementImage(shape2png);
            }
        }
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
    	if(shapeType==ShapeType.TABLE) {
            final DLList<Object> nodeList = tbl.getContent();
            final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
            DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

            IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
            for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
                final Object o = getContentModel(childNode, tbl);
                if(o instanceof CTTableRow) {
                	nextComponent = new TableRowComponent(this, childNode, nextComponentNumber);
                }
            }
            return nextComponent;
    	}
		return null;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

    	PMLShapeHelper.applyAttrsFromJSON(operationDocument, attrs, graphicalObjectFrame);
		DMLHelper.applyTransform2DFromJson(graphicalObjectFrame, attrs, true);
        DMLHelper.applyNonVisualDrawingProperties(graphicalObjectFrame, attrs.optJSONObject(OCKey.DRAWING.value()));
        DMLHelper.applyNonVisualDrawingShapeProperties(graphicalObjectFrame, attrs.optJSONObject(OCKey.DRAWING.value()));

        if(pic!=null) {
        	Picture.applyAttrsFromJSON(operationDocument, attrs, operationDocument.getContextPart(), pic, !(getParentComponent() instanceof ShapeGroupComponent));
    	}
    	else if(tbl!=null) {
    		DMLHelper.applyTableFromJson(attrs, tbl, graphicalObjectFrame.getXfrm(true).getExt(true).getCx(), operationDocument);
    	}
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

    	PMLShapeHelper.createJSONAttrs(attrs, graphicalObjectFrame);
        DMLHelper.createJsonFromTransform2D(attrs, graphicalObjectFrame.getXfrm(false), !(getParentComponent() instanceof ShapeGroupComponent));
        DMLHelper.createJsonFromNonVisualDrawingProperties(attrs, graphicalObjectFrame);
    	DMLHelper.createJsonFromNonVisualDrawingShapeProperties(attrs, graphicalObjectFrame);

    	if(pic!=null) {
    		Picture.createJSONAttrs(operationDocument, attrs, operationDocument.getContextPart(), pic, !(getParentComponent() instanceof ShapeGroupComponent));
    	}
    	else if(tbl!=null) {
        	DMLHelper.createJsonFromTable(attrs, tbl, operationDocument.getThemePart(false), getOperationDocument().getContextPart());
    	}
        else if(shape2png!=null) {
            Tools.addFamilyAttribute(attrs, OCKey.IMAGE, OCKey.IMAGE_URL, shape2png);
            Tools.addFamilyAttribute(attrs, OCKey.DRAWING, OCKey.NOT_RESTORABLE, Boolean.TRUE);
        }
    	return attrs;
    }

	@Override
	public Integer getId() {
		final CTNonVisualDrawingProps nvdProps = graphicalObjectFrame.getNonVisualDrawingProperties(false);
		return nvdProps!=null ? nvdProps.getId() : null;
	}

	@Override
	public ShapeType getType() {
		return shapeType;
	}

	@Override
	public boolean isPresentationObject() {
		return false;
	}

	private Pic getPicture(boolean removeVML) {

		final Graphic graphic = graphicalObjectFrame.getGraphic();
	    if(graphic!=null&&graphic.getGraphicData()!=null) {
	    	final GraphicData graphicData = graphic.getGraphicData();
	    	final String uri = graphicData.getUri();
	        final List<Object> any = graphicData.getAny();
	    	if(uri!=null&&!any.isEmpty()) {
	            if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/presentationml/2006/ole")) {
	            	CTOleObject oleObject = null;
	                Object anyO = any.get(0);
	                if(anyO instanceof AlternateContent) {
	                	final Fallback fallback = ((AlternateContent)anyO).getFallback();
	                	if(fallback!=null&&!fallback.getAny().isEmpty()) {
	                		oleObject = tryGetOle(fallback.getAny().get(0));
	                		if(oleObject!=null) {
	                		    // this is only for #47483 ... otherwise the ole placeholder representation is doubled after removing all choices :-(
	                		    final ComponentContext<OfficeOpenXMLOperationDocument> rootContext = getRootContext();
	                		    if(rootContext instanceof RootComponent) {
	                		        final Part contextPart = ((RootComponent)rootContext).getOperationDocument().getContextPart();
	                		        if(contextPart!=null) {
	                		            final Relationship relationShip = contextPart.getRelationshipsPart().getRelationshipByType(Namespaces.VML);
	                		            if(relationShip!=null) {
        	                		        // as we remove all choices and we have a drawingVML relation, we need to check if a corresponding vml shape has to be deleted also
	                		                // :( below used spids are not part within the ooxml spec and also a drawingPart relation is missing on the slide... -> double failure
	                		                final VMLPart vmlPart = OfficeOpenXMLOperationDocument.getVMLPart(contextPart, null);
	                		                if(vmlPart!=null) {
	                		                    final Xml vml = vmlPart.getJaxbElement();
	                		                    if(vml!=null) {
                	                		        final Iterator<Choice> choiceIter = ((AlternateContent)anyO).getChoice().iterator();
                	                		        while(choiceIter.hasNext()) {
                	                		            final List<Object> choiceAny = choiceIter.next().getAny();
                	                		            if(!choiceAny.isEmpty()) {
                	                		                final CTOleObject oleChoice = tryGetOle(choiceAny.get(0));
                	                		                if(oleChoice!=null) {
                	                		                    final String spid = oleChoice.getSpid();
                	                		                    if(spid!=null&&!spid.isEmpty()) {
                                                                    // we have a spid, now we check if we the corresponding DrawingVmlPart
                	                		                        final Iterator<Object> vmlEntryIter = vml.getContent().iterator();
                	                		                        while(vmlEntryIter.hasNext()) {
                	                		                            final Object o = vmlEntryIter.next();
                	                		                            CTShape shape = null;
                	                		                            if(o instanceof CTShape) {
                	                		                                shape = (CTShape)o;
                	                		                            }
                	                		                            else if(o instanceof JAXBElement<?>&&((JAXBElement<?>) o).getDeclaredType().getName().equals("org.docx4j.vml.CTShape")) {
                	                		                                shape =(CTShape)((JAXBElement<?>)o ).getValue();
                	                		                            }
                	                		                            if(shape!=null) {
                	                		                                final String id = shape.getVmlId();
                	                		                                if(id!=null&&id.equals(spid)) {
                	                		                                    vmlEntryIter.remove();
                	                		                                    break;
                	                		                                }
                	                		                            }
                	                		                        }
                	                		                    }
                	                		                }
                	                		            }
                	                		        }
	                		                    }
	                		                }
	                		            }
	                		        }
	                		    }
	                		}
	                	}
	                }
	                else {
	                	oleObject = tryGetOle(anyO);
	                }
	                if(oleObject!=null) {
	                	any.set(0, oleObject);
	                	return oleObject.getPic();
	                }
	            }
	    	}
	    }
	    return null;
	}

	private static CTOleObject tryGetOle(Object o) {
		if(o instanceof JAXBElement && ((JAXBElement<?>)o).getDeclaredType().getName().equals("org.pptx4j.pml.CTOleObject") ) {
        	o = ((JAXBElement<?>)o).getValue();
        }
		if(o instanceof CTOleObject) {
        	return (CTOleObject)o;
        }
		return null;
	}

	private CTTable getTable() {
		final Graphic graphic = graphicalObjectFrame.getGraphic();
	    if(graphic!=null&&graphic.getGraphicData()!=null) {
	    	final GraphicData graphicData = graphic.getGraphicData();
	    	final String uri = graphicData.getUri();
	        final List<Object> any = graphicData.getAny();
	    	if(uri!=null&&!any.isEmpty()) {
	            if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/table")) {
	            	Object o = any.get(0);
	                if(o instanceof JAXBElement && ((JAXBElement<?>)o).getDeclaredType().getName().equals("org.docx4j.dml.CTTable") ) {
	                	o = ((JAXBElement<?>)o).getValue();
	                	any.set(0, o);
	                }
	                if(o instanceof CTTable) {
	                	return (CTTable)o;
	                }
	            }
	    	}
	    }
	    return null;
	}

	public void insertRows(int destinationRowComponent, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

    	final TableRowComponent trReferenceRow = referenceRow!=-1 ? (TableRowComponent)getChildComponent(referenceRow) : null;
        final DLList<Object> tblContent = tbl.getContent();
        final IComponent<OfficeOpenXMLOperationDocument> oldTr = getChildComponent(destinationRowComponent);
        DLNode<Object> referenceNode = oldTr!=null?oldTr.getNode():null;

        for(int i = 0; i<count; i++) {
            final CTTableRow tr = new CTTableRow();
            tr.setParent(tbl);
            final DLNode<Object> rowNode = new DLNode<Object>(tr);
            tblContent.addNode(referenceNode, rowNode, true);
            referenceNode = rowNode;
            if(insertDefaultCells) {
            	final CTTableGrid tableGrid = tbl.getTblGrid(false);
            	if(tableGrid!=null) {
            		for(CTTableCol tableCol:tableGrid.getGridCol()) {
            			final CTTableCell tc = new CTTableCell();
            			tc.setParent(tr);
            			tr.getContent().add(tc);
            			final CTTextParagraph tp = new CTTextParagraph();
            			tp.setParent(tc);
            			tc.getContent().addNode(new DLNode<Object>(tp));
            		}
            	}
            }
            else if(trReferenceRow!=null) {       // we try to create the new row from the referenceRow
                final CTTableRow trReference = (CTTableRow)trReferenceRow.getObject();
                tr.setH(trReference.getH());
                TableCellComponent tcReferenceComponent = (TableCellComponent)trReferenceRow.getNextChildComponent(null, null);
                while(tcReferenceComponent!=null) {
                    final CTTableCell tcReference = (CTTableCell)tcReferenceComponent.getObject();
                    final CTTableCell newTc = new CTTableCell();
                    newTc.setParent(tr);
                    tr.getContent().add(newTc);
                    final CTTableCellProperties tcPrReference = tcReference.getTcPr(false);
                    if(tcPrReference!=null) {
                        final CTTableCellProperties newTcPr = XmlUtils.deepCopy(tcPrReference, operationDocument.getPackage());
                        newTc.setTcPr(newTcPr);
                    }
        			final CTTextParagraph tp = new CTTextParagraph();
        			tp.setParent(newTc);
        			newTc.getContent().addNode(new DLNode<Object>(tp));
        			applyReferenceCellAttributes(tp, tcReferenceComponent);
                    tcReferenceComponent = (TableCellComponent)tcReferenceComponent.getNextComponent();
                }
            }
        }
        if(attrs!=null) {
            IComponent<OfficeOpenXMLOperationDocument> c = getNextChildComponent(null, null).getComponent(destinationRowComponent);
	        for(int i=0; i<count; i++) {
	        	c.applyAttrsFromJSON(attrs);
	        	c = c.getNextComponent();
	        }
        }
        recalcTableHeight();
	}

	public void applyReferenceCellAttributes(CTTextParagraph p, TableCellComponent referenceCell) {
	    if(referenceCell!=null) {
	        final ParagraphComponent referenceParagraphComponent = (ParagraphComponent)referenceCell.getNextChildComponent(null, null);
	        if(referenceParagraphComponent!=null) {
	            final CTTextParagraph referenceP = (CTTextParagraph)referenceParagraphComponent.getObject();
	            if(referenceP.getPPr(false)!=null) {
	                p.setPPr(XmlUtils.deepCopy(referenceP.getPPr(false), operationDocument.getPackage()));
	            }
	            IComponent<OfficeOpenXMLOperationDocument> c = referenceParagraphComponent.getNextChildComponent(null, null);
	            if(c instanceof TextComponent) {
	                final CTRegularTextRun textRun = ((CTRegularTextRun)c.getObject());
	                if(textRun.getRPr(false)!=null) {
	                    p.setEndParaRPr(XmlUtils.deepCopy(textRun.getRPr(false), operationDocument.getPackage()));
	                }
	            }
	        }
	    }
	}

	public void recalcTableHeight() {
		long newTableHeight = 0;
		final Iterator<Object> rowIter = tbl.getContent().iterator();
		while(rowIter.hasNext()) {
			newTableHeight += ((CTTableRow)rowIter.next()).getH();
		}
		graphicalObjectFrame.getXfrm(true).getExt(true).setCy(newTableHeight);
	}

	public void insertColumn(JSONArray tableGrid, int gridPosition, String insertMode) {
		boolean before = insertMode.equals("before");
        TableRowComponent trComponent = (TableRowComponent)getNextChildComponent(null, null);
        while(trComponent!=null) {
            TableCellComponent tcReference = (TableCellComponent)trComponent.getNextChildComponent(null, null);
            TableCellComponent tcReferenceLast = null;
            TableCellComponent destination = null;
            while(tcReference!=null) {
                tcReferenceLast = tcReference;
                if(gridPosition>=tcReference.getGridPosition()&&gridPosition<tcReference.getNextGridPosition()) {
                    destination = tcReference;
                    break;
                }
                tcReference = (TableCellComponent)tcReference.getNextComponent();
            }
            final CTTableCell tc = new CTTableCell();
            if(destination!=null) {
            	CTTableCellProperties referenceTcPr = ((CTTableCell)destination.getObject()).getTcPr(false);
                tc.setParent(trComponent.getObject());
                if(referenceTcPr!=null) {
                    tc.setTcPr(XmlUtils.deepCopy(referenceTcPr, operationDocument.getPackage()));
                }
            }
			final CTTextParagraph tp = new CTTextParagraph();
			tp.setParent(tc);
			tc.getContent().addNode(new DLNode<Object>(tp));

			applyReferenceCellAttributes(tp, destination!=null ? destination : tcReferenceLast);

            final DLList<Object> rowContent = ((CTTableRow)trComponent.getObject()).getContent();
            if(destination==null) {
            	rowContent.add(tc);
            }
            else {
            	final ComponentContext<OfficeOpenXMLOperationDocument> contextChild = destination.getContextChild(null);
            	rowContent.addNode(contextChild.getNode(), new DLNode<Object>(tc), before);
            }
            trComponent = (TableRowComponent)trComponent.getNextComponent();
        }
        DMLHelper.applyTableGridFromJson(tbl, graphicalObjectFrame.getXfrm(true).getExt(true).getCx(), tableGrid);
	}

    public void deleteColumns(int gridStart, int gridEnd) {
        IComponent<OfficeOpenXMLOperationDocument> trComponent = getNextChildComponent(null, null);
        while(trComponent!=null) {
            final DLNode<Object> trNode = trComponent.getNode();
        	final DLList<Object> rowContent = ((CTTableRow)trComponent.getObject()).getContent();
        	for(int i=gridEnd; i>=gridStart; i--) {
        		final CTTableCell cell = (CTTableCell)rowContent.get(i);
        		if(cell.isHMerge()) {
        			for(int j = i-1; j>=0; j--) {
        				final CTTableCell gridSpanCell = (CTTableCell)rowContent.get(j);
        				if(!gridSpanCell.isHMerge()) {
        					if(gridSpanCell.getGridSpan()>1) {
        						gridSpanCell.setGridSpan(gridSpanCell.getGridSpan()-1);
        					}
        					break;
        				}
        			}
        		}
        		if(cell.getGridSpan()>1) {
        			rowContent.remove(i+1);
        			cell.setGridSpan(cell.getGridSpan()-1);
        		}
        		else {
        			rowContent.remove(i);
        		}
        	}
            trComponent = trComponent.getNextComponent();
            if(rowContent.isEmpty()) {
                tbl.getContent().removeNode(trNode);
            }
        }
        final CTTableGrid tableGrid = tbl.getTblGrid(false);
        if(tableGrid!=null) {
        	final List<CTTableCol> tableCol = tableGrid.getGridCol();
        	int count = gridEnd - gridStart;
        	while(count>=0) {
        		tableCol.remove(count--);
        	}
        }
	}
}
