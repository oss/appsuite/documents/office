/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.ooxml.pptx.components;

import java.util.List;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PresentationML.ICommonSlideAccessor;
import org.docx4j.openpackaging.parts.PresentationML.JaxbPmlPart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.json.JSONException;
import org.json.JSONObject;
import org.pptx4j.Pptx4jException;
import org.pptx4j.pml.CTBackground;
import org.pptx4j.pml.CTGraphicalObjectFrame;
import org.pptx4j.pml.CTHeaderFooter;
import org.pptx4j.pml.CommonSlideData;
import org.pptx4j.pml.CxnSp;
import org.pptx4j.pml.GroupShape;
import org.pptx4j.pml.IHeaderFooterAccessor;
import org.pptx4j.pml.Pic;
import org.pptx4j.pml.Presentation;
import org.pptx4j.pml.Presentation.SldMasterIdLst.SldMasterId;
import org.pptx4j.pml.STSlideLayoutType;
import org.pptx4j.pml.Shape;
import org.pptx4j.pml.Sld;
import org.pptx4j.pml.SldLayout;
import org.pptx4j.pml.SldMaster;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.ooxml.OfficeOpenXMLOperationDocument;
import com.openexchange.office.filter.ooxml.drawingml.DMLHelper;
import com.openexchange.office.filter.ooxml.pptx.PptxOperationDocument;
import com.openexchange.office.filter.ooxml.pptx.tools.PMLShapeHelper;

public class SlideComponent extends PptxComponent {

    public SlideComponent(PptxOperationDocument operationDocument, DLNode<Object> _node, int _componentNumber) {
        super(operationDocument, _node, _componentNumber);
    }

    public SlideComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
    }

    @Override
	public IComponent<OfficeOpenXMLOperationDocument> getNextChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> previousChildContext, IComponent<OfficeOpenXMLOperationDocument> previousChildComponent) {
        final DLNode<Object> paragraphNode = getNode();
        final DLList<Object> nodeList = (DLList<Object>)((IContentAccessor)paragraphNode.getData()).getContent();
        final int nextComponentNumber = previousChildComponent!=null?previousChildComponent.getNextComponentNumber():0;
        DLNode<Object> childNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : nodeList.getFirstNode();

        IComponent<OfficeOpenXMLOperationDocument> nextComponent = null;
        for(; nextComponent==null&&childNode!=null; childNode = childNode.getNext()) {
            final Object o = getContentModel(childNode, paragraphNode.getData());
            if(o instanceof GroupShape) {
            	nextComponent = new ShapeGroupComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Shape) {
            	nextComponent = new ShapeComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CTGraphicalObjectFrame) {
            	nextComponent = new ShapeGraphicComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof CxnSp) {
            	nextComponent = new ShapeConnectorComponent(this, childNode, nextComponentNumber);
            }
            else if(o instanceof Pic) {
            	nextComponent = new ShapePicComponent(this, childNode, nextComponentNumber);
            }
/*
            else if(o instanceof CTRel) {
            	nextComponent = new ShapeRelComponent(this, childNode, nextComponentNumber);
            }
*/
        }
        return nextComponent;
    }

    @Override
    public void delete(int count) {
    	final Part contextPart = operationDocument.getContextPart();
    	for(int i=(getComponentNumber()+count)-1; i>=getComponentNumber(); i--) {
			try {
				if(contextPart instanceof SlideLayoutPart) {

					final SlideLayoutPart slideLayoutPart = (SlideLayoutPart)contextPart;
					final SlideMasterPart slideMasterPart = slideLayoutPart.getSlideMasterPart();
					final RelationshipsPart slideMasterRelationshipsPart = slideMasterPart.getRelationshipsPart();
					final Relationship slideLayoutRel = slideMasterRelationshipsPart.getRel(slideLayoutPart.getPartName());
					slideMasterRelationshipsPart.removeRelationship(slideLayoutRel);
					final List<SldLayoutId> slideLayoutIdList = slideMasterPart.getJaxbElement().getSldLayoutIdLst(false).getSldLayoutId();
					for(int j = 0; j < slideLayoutIdList.size(); j++) {
						if(slideLayoutIdList.get(j).getRid().equals(slideLayoutRel.getId())) {
							slideLayoutIdList.remove(j);
							break;
						}
					}
				}
				else if(contextPart instanceof SlideMasterPart) {
					final SlideMasterPart slideMasterPart = (SlideMasterPart)contextPart;
					final MainPresentationPart mainPresentationPart = (((PptxOperationDocument)operationDocument).getPackage()).getMainPresentationPart();
					final Presentation mainPresentation = mainPresentationPart.getJaxbElement();
					final RelationshipsPart mainPresentationRelationshipsPart = mainPresentationPart.getRelationshipsPart();
					final Relationship slideMasterRel = mainPresentationRelationshipsPart.getRel(slideMasterPart.getPartName());
					mainPresentationRelationshipsPart.removeRelationship(slideMasterRel);
					final List<SldMasterId> sldMasterIdList = mainPresentation.getSldMasterIdLst().getSldMasterId();
					for(int j = 0; j < sldMasterIdList.size(); j++) {
						if(sldMasterIdList.get(j).getRid().equals(slideMasterRel.getId())) {
							sldMasterIdList.remove(j);
							break;
						}
					}
				}
				else {
					(((PptxOperationDocument)operationDocument).getPackage()).getMainPresentationPart().removeSlide(i);
				}
			} catch (Pptx4jException e) {
				throw new FilterException("SlideComponent: invalid format", ErrorCode.CRITICAL_ERROR);
			}
    	}
    }

    @Override
    public IComponent<OfficeOpenXMLOperationDocument> insertChildComponent(ComponentContext<OfficeOpenXMLOperationDocument> parentContext, DLNode<Object> contextNode, int number, IComponent<OfficeOpenXMLOperationDocument> child, ComponentType type, JSONObject attrs)
    	throws Exception {

    	DLList<Object> DLList = (DLList<Object>)((IContentAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child!=null ? child.getNode() : null;

        switch(type) {
        	case AC_SHAPE: {
	            final Child newChild = PMLShapeHelper.createShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeComponent(parentContext, newChildNode, number);
        	}
        	case AC_GROUP: {
	            final Child newChild = PMLShapeHelper.createGroupShape();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeGroupComponent(parentContext, newChildNode, number);
        	}
        	case AC_CONNECTOR: {
        		final Child newChild = PMLShapeHelper.createConnectorShape();
        		newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeConnectorComponent(parentContext, newChildNode, number);
        	}
        	case AC_IMAGE: {
	            final Child newChild = PMLShapeHelper.createImage();
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapePicComponent(parentContext, newChildNode, number);
        	}
        	case TABLE: {
        		final Child newChild = PMLShapeHelper.createGraphicalObjectFrame(ComponentType.TABLE);
	            newChild.setParent(contextNode.getData());
	            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
	            DLList.addNode(referenceNode, newChildNode, true);
	            return new ShapeGraphicComponent(parentContext, newChildNode, number);
        	}
        	default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs)
    	throws InvalidFormatException, PartUnrecognisedException, JSONException {

		final ICommonSlideAccessor commonSlideAccess = (ICommonSlideAccessor)getObject();
		final CommonSlideData commonSlideData = commonSlideAccess.getCSld();

		final JSONObject slideAttrs = attrs.optJSONObject(OCKey.SLIDE.value());
		if(slideAttrs!=null) {
			final Object slideName = slideAttrs.opt(OCKey.NAME.value());
			if(slideName!=null) {
				if(slideName==JSONObject.NULL) {
					commonSlideData.setName(null);
				}
				else {
					commonSlideData.setName((String)slideName);
				}
			}
			final Object sldElement = ((JaxbPmlPart<?>)getObject()).getJaxbElement();
			// hidden only available at normal slides
			if(sldElement instanceof Sld) {
				final Object hidden = slideAttrs.opt(OCKey.HIDDEN.value());
				if(hidden!=null) {
					if(hidden instanceof Boolean) {
						((Sld)sldElement).setShow(((Boolean)hidden).booleanValue() ? false : null);
					}
					else {
						((Sld)sldElement).setShow(null);
					}
				}
				final Object isFollowMasterShape = slideAttrs.opt(OCKey.FOLLOW_MASTER_SHAPES.value());
				if(isFollowMasterShape!=null) {
					if(isFollowMasterShape instanceof Boolean&&!((Boolean)isFollowMasterShape).booleanValue()) {
						((Sld)sldElement).setShowMasterSp(false);
					}
					else {
						((Sld)sldElement).setShowMasterSp(null);
					}
				}
			}
			else if(sldElement instanceof SldLayout) {
				final Object isFollowMasterShape = slideAttrs.opt(OCKey.FOLLOW_MASTER_SHAPES.value());
				if(isFollowMasterShape!=null) {
					if(isFollowMasterShape instanceof Boolean&&!((Boolean)isFollowMasterShape).booleanValue()) {
						((SldLayout)sldElement).setShowMasterSp(false);
					}
					else {
						((SldLayout)sldElement).setShowMasterSp(null);
					}
				}
				final Object type = slideAttrs.opt(OCKey.TYPE.value());
				if(type instanceof String) {
					((SldLayout)sldElement).setType(STSlideLayoutType.fromValue((String)type));
				}
				else if(type==JSONObject.NULL) {
					((SldLayout)sldElement).setType(null);
				}
				applyHeaderFooterAttrs((SldLayout)sldElement, slideAttrs);
			}
			else if(sldElement instanceof SldMaster) {
				applyHeaderFooterAttrs((SldMaster)sldElement, slideAttrs);
			}
		}
    	if(attrs.hasAndNotNull(OCKey.FILL.value())) {
    		commonSlideData.getBg(true).setBgRef(null);
			DMLHelper.applyFillPropertiesFromJson(commonSlideData.getBg(true).getBgPr(true), attrs, operationDocument, operationDocument.getContextPart());
		}
    }

    private void applyHeaderFooterAttrs(IHeaderFooterAccessor iHeaderFooterAccessor, JSONObject attrs) {
    	final Object isHeader = attrs.opt(OCKey.IS_HEADER.value());
    	final Object isFooter = attrs.opt(OCKey.IS_FOOTER.value());
    	final Object isDate = attrs.opt(OCKey.IS_DATE.value());
    	final Object isSlideNum = attrs.opt(OCKey.IS_SLIDE_NUM.value());
    	if(isHeader!=null||isFooter!=null||isDate!=null||isSlideNum!=null) {
    		final CTHeaderFooter hf = iHeaderFooterAccessor.getHf(true);
	    	if(isHeader!=null) {
	    		if(isHeader instanceof Boolean) {
	    			hf.setHdr((Boolean)isHeader);
	    		}
	    		else {
	    			hf.setHdr(null);
	    		}
	    	}
	    	if(isFooter!=null) {
	    		if(isFooter instanceof Boolean) {
	    			hf.setFtr((Boolean)isFooter);
	    		}
	    		else {
	    			hf.setFtr(null);
	    		}
	    	}
	    	if(isDate!=null) {
	    		if(isDate instanceof Boolean) {
	    			hf.setDt((Boolean)isDate);
	    		}
	    		else {
	    			hf.setDt(null);
	    		}
	    	}
	    	if(isSlideNum!=null) {
	    		if(isSlideNum instanceof Boolean) {
	    			hf.setSldNum((Boolean)isSlideNum);
	    		}
	    		else {
	    			hf.setSldNum(null);
	    		}
	    	}
    	}
    }

    @Override
    public JSONObject createJSONAttrs(JSONObject attrs)
    	throws JSONException {

		final JSONObject slideAttrs = new JSONObject();

    	final ICommonSlideAccessor commonSlideAccess = (ICommonSlideAccessor)getObject();
		final CommonSlideData commonSlideData = commonSlideAccess.getCSld();
		final String slideName = commonSlideData.getName();
		if(slideName!=null) {
			slideAttrs.put(OCKey.NAME.value(), slideName);
		}
		final Object sldElement = ((JaxbPmlPart<?>)getObject()).getJaxbElement();
		if(sldElement instanceof Sld) {
			if(!((Sld)sldElement).isShowMasterSp()) {
				slideAttrs.put(OCKey.FOLLOW_MASTER_SHAPES.value(), false);
			}
			if(!((Sld)sldElement).isShow()) {
				slideAttrs.put(OCKey.HIDDEN.value(), true);
			}
		}
		else if(sldElement instanceof SldLayout) {
			final SldLayout sldLayout = (SldLayout)sldElement;
			if(!sldLayout.isShowMasterSp()) {
				slideAttrs.put(OCKey.FOLLOW_MASTER_SHAPES.value(), false);
			}
			if(sldLayout.getType()!=null) {
				slideAttrs.put(OCKey.TYPE.value(), sldLayout.getType().value());
			}
			createHeaderFooterOperations((SldLayout)sldElement, slideAttrs);
		}
		else if(sldElement instanceof SldMaster) {
			createHeaderFooterOperations((SldMaster)sldElement, slideAttrs);
		}
		if(!slideAttrs.isEmpty()) {
			attrs.put(OCKey.SLIDE.value(), slideAttrs);
		}
		final CTBackground background = commonSlideData.getBg(false);
		if(background!=null) {
			DMLHelper.createJsonFromFillProperties(attrs, background.getBgPr(true), operationDocument.getThemePart(false), background.getBgRef(), operationDocument.getContextPart());
		}
		return attrs;
    }

	private void createHeaderFooterOperations(IHeaderFooterAccessor iHeaderFooterAccessor, JSONObject attrs)
		throws JSONException {

		final CTHeaderFooter hf = iHeaderFooterAccessor.getHf(false);
		if(hf==null) {
			return;
		}
		if(hf.isDt()==null || hf.isDt().booleanValue()) {
			attrs.put(OCKey.IS_DATE.value(), true);
		}
		else {
			attrs.put(OCKey.IS_DATE.value(), false);
		}
		if(hf.isFtr()==null || hf.isFtr().booleanValue()) {
			attrs.put(OCKey.IS_FOOTER.value(), true);
		}
		else {
			attrs.put(OCKey.IS_FOOTER.value(), false);
		}
		if(hf.isHdr()==null || hf.isHdr().booleanValue()) {
			attrs.put(OCKey.IS_HEADER.value(), true);
		}
		else {
			attrs.put(OCKey.IS_HEADER.value(), false);
		}
		if(hf.isSldNum()==null || hf.isSldNum().booleanValue()) {
			attrs.put(OCKey.IS_SLIDE_NUM.value(), true);
		}
		else {
			attrs.put(OCKey.IS_SLIDE_NUM.value(), false);
		}
	}
}
