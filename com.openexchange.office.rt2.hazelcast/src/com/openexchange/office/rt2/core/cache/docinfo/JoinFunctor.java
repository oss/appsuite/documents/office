/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.cache.docinfo;

import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap.DistributedDocInfoStatus;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeDistributedDocInfo;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class JoinFunctor implements BaseFunctor {

	private final RT2CliendUidType clientUid;
	
	public JoinFunctor(RT2CliendUidType clientUid) {
		this.clientUid = clientUid;
	}

	@Override
	public PortableNodeDistributedDocInfo apply(String t, PortableNodeDistributedDocInfo u) {
		PortableNodeDistributedDocInfo newPortableNodeDistributedDocInfo;
		if (u == null) {
			newPortableNodeDistributedDocInfo = new PortableNodeDistributedDocInfo(DistributedDocInfoStatus.RUNNING);
		} else {			
			newPortableNodeDistributedDocInfo = new PortableNodeDistributedDocInfo(u.getStatus(), u.getClientIds());
		} 		
		newPortableNodeDistributedDocInfo.getClientIds().add(clientUid.getValue());
		return newPortableNodeDistributedDocInfo;
	}
}
