/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.core.cache.docinfo;

import java.util.Set;
import java.util.stream.Collectors;

import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeDistributedDocInfo;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class LeavesFunctor implements BaseFunctor {

	private final Set<String> clientUids;
	
	public LeavesFunctor(Set<RT2CliendUidType> clientUids) {
		this.clientUids = clientUids.stream().map(c -> c.getValue()).collect(Collectors.toSet());
	}

	@Override
	public PortableNodeDistributedDocInfo apply(String t, PortableNodeDistributedDocInfo u) {		 
		if (u != null) {			
			PortableNodeDistributedDocInfo res = new PortableNodeDistributedDocInfo(u.getStatus(), u.getClientIds());
			res.getClientIds().removeAll(clientUids);
			if (res.getClientIds().isEmpty()) {
				// Triggers a remove of PortableNodeDistributedDocInfo-Object from Map 
				return null;
			}
			return res;
		}
		return null;
	}	
}
