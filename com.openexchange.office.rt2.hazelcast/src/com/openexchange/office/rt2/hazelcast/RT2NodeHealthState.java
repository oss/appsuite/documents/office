/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast;

public class RT2NodeHealthState
{
    //-------------------------------------------------------------------------
	private String m_sUUID;

    //-------------------------------------------------------------------------
	private String m_sNodeName;

    //-------------------------------------------------------------------------
	private String m_sState;

    //-------------------------------------------------------------------------
	private String m_sNodeType;

    //-------------------------------------------------------------------------
	private String m_sCleanUpUUID;

    //-------------------------------------------------------------------------
    public RT2NodeHealthState(String sNodeUUID) {
        this.m_sUUID = sNodeUUID;
    }

    //-------------------------------------------------------------------------
    public RT2NodeHealthState(String sNodeUUID, String sState) {
        this(sNodeUUID);
        this.m_sState = sState;
    }

    //-------------------------------------------------------------------------
    public RT2NodeHealthState(String sNodeUUID, String sNodeName, String sHealthState, String sNodeType, String sCleanupUUID) {
        this(sNodeUUID, sHealthState);
        this.m_sNodeName = sNodeName;
        this.m_sCleanUpUUID = sCleanupUUID;
        this.m_sNodeType = sNodeType;
    }

    //-------------------------------------------------------------------------
    public String getNodeUUID() {
        return m_sUUID;
    }

    //-------------------------------------------------------------------------
    public String getNodeName() {
        return m_sNodeName;
    }

    //-------------------------------------------------------------------------
    public String getState() {
        return m_sState;
    }

    //-------------------------------------------------------------------------
    public String getNodeType() {
        return m_sNodeType;
    }

    //-------------------------------------------------------------------------
    public String getCleanupUUID() {
        return m_sCleanUpUUID;
    }

    //-------------------------------------------------------------------------
    public void setState(String sNewState) {
        m_sState = sNewState;
    }

    //-------------------------------------------------------------------------
    public void setCleanupUUID(String sNodeUUID) {
        m_sCleanUpUUID = sNodeUUID;
    }

}
