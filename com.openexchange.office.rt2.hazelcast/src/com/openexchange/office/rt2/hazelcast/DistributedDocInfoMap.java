/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.core.cache.docinfo.JoinFunctor;
import com.openexchange.office.rt2.core.cache.docinfo.LeaveFunctor;
import com.openexchange.office.rt2.core.cache.docinfo.LeavesFunctor;
import com.openexchange.office.rt2.core.cache.docinfo.StatusChangeFunctor;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeDistributedDocInfo;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.caching.CachingFacade;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.office.tools.service.logging.SpecialLogService;

@Service
@RegisteredService
public class DistributedDocInfoMap implements InitializingBean {

	public enum DistributedDocInfoStatus {
		NOT_ACTIVE, RUNNING, DOCUMENT_ON_CRASHED_NODE;
	}

	private static final Logger log = LoggerFactory.getLogger(DistributedDocInfoMap.class);

	@Autowired
	private CachingFacade cachingFacade;

	@Autowired
	private SpecialLogService specialLogService;

	private DistributedMap<String, PortableNodeDistributedDocInfo> clientRefCountMap;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.clientRefCountMap = cachingFacade.getDistributedMap(RT2HazelcastNames.RT2_CLIENT_COUNTER_MAP, String.class, PortableNodeDistributedDocInfo.class);
	}

	//-------------------------------------------------------------------------
	public void clear() {
		clientRefCountMap.clear();
	}

	//-------------------------------------------------------------------------
	public Set<RT2DocUidType> getAllRegisteredDocIds() {
		Set<RT2DocUidType> docUids = clientRefCountMap.keySet().stream().map(s -> new RT2DocUidType(s)).collect(Collectors.toSet());
		return docUids;
	}

	//-------------------------------------------------------------------------
	/** count clients which has joined a document (as an atomic operation)
	 *
	 *	@return	the actual count of clients after increment.
	 */
	public Set<RT2CliendUidType> addClient(RT2DocUidType docUid,  RT2CliendUidType cliendUid) {
		PortableNodeDistributedDocInfo res = clientRefCountMap.compute(docUid.getValue(), new JoinFunctor(cliendUid));
		specialLogService.log(Level.DEBUG, log, docUid.getValue(), new Throwable(), "addedClient to clientRefCountMap for com.openexchange.rt2.document.uid {} to {}", docUid, res);
		return res.getClientIds().stream().map(c -> new RT2CliendUidType(c)).collect(Collectors.toSet());
	}

	//-------------------------------------------------------------------------
	/** forget client which has left a document (as an atomic operation)
	 *
	 *	@return	the actual count of clients after decrement.
	 */
	public Set<RT2CliendUidType> removeClient(RT2DocUidType docUid, RT2CliendUidType cliendUid) {
		PortableNodeDistributedDocInfo res = clientRefCountMap.computeIfPresent(docUid.getValue(), new LeaveFunctor(cliendUid));
		if (res == null) {
			return null;
		}
		specialLogService.log(Level.DEBUG, log, docUid.getValue(), new Throwable(), "removeClient for com.openexchange.rt2.document.uid {}. Remaining clients: {}", docUid, res);
		return res.getClientIds().stream().map(c -> new RT2CliendUidType(c)).collect(Collectors.toSet());
	}

	//-------------------------------------------------------------------------
	public Set<RT2CliendUidType> removeClients(RT2DocUidType docUid, Set<RT2CliendUidType> cliendUids) {
		PortableNodeDistributedDocInfo res = clientRefCountMap.computeIfPresent(docUid.getValue(), new LeavesFunctor(cliendUids));
		if (res == null) {
			return null;
		}
		specialLogService.log(Level.DEBUG, log, docUid.getValue(), new Throwable(), "removeClients for com.openexchange.rt2.document.uid {}. Remaining clients: {}", docUid, res);
		return res.getClientIds().stream().map(c -> new RT2CliendUidType(c)).collect(Collectors.toSet());
	}

	//-------------------------------------------------------------------------
	public int getRefCount4Clients(RT2DocUidType docUid) {
		PortableNodeDistributedDocInfo res = clientRefCountMap.get(docUid.getValue());
		if (res == null) {
			return -1;
		}
		return res.getClientIds().size();
	}

	//-------------------------------------------------------------------------
	public Set<String> getClientsOfDocUid(RT2DocUidType docUid) {
		PortableNodeDistributedDocInfo portableNodeDistributedDocInfo = clientRefCountMap.get(docUid.getValue());
		if (portableNodeDistributedDocInfo == null) {
			return Collections.emptySet();
		}
		return portableNodeDistributedDocInfo.getClientIds();
	}

	//-------------------------------------------------------------------------
	public void setStatus(RT2DocUidType docUid, DistributedDocInfoStatus status) {
		clientRefCountMap.computeIfPresent(docUid.getValue(), new StatusChangeFunctor(status));
	}

	//-------------------------------------------------------------------------
	public DistributedDocInfoStatus getStatus(RT2DocUidType docUid) {
		PortableNodeDistributedDocInfo distributedDocInfoValue = clientRefCountMap.get(docUid.getValue());
		if  (distributedDocInfoValue == null) {
			return DistributedDocInfoStatus.NOT_ACTIVE;
		}
		return distributedDocInfoValue.getStatus();
	}


	//-------------------------------------------------------------------------
	/** clean up all resources bound to that document.
	 *
	 *	@param	sDocUID [IN]
	 *			the unique ID for the document where all info object should be removed.
	 */
	public boolean freeDocInfos (final RT2DocUidType docUID) {
	    String sDocUid = docUID.getValue();
	    MDCHelper.safeMDCPut(MDCEntries.DOC_UID, sDocUid);
	    try {
	        log.debug("freeDocInfos for com.openexchange.rt2.document.uid [{}]", sDocUid);
	        return clientRefCountMap.remove(sDocUid) != null;
	    } finally {
	        MDC.remove(MDCEntries.DOC_UID);
	    }
	}
}
