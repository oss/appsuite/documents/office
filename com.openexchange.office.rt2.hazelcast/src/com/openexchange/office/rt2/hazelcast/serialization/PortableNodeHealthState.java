/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.serialization;

import java.io.IOException;

import org.apache.commons.lang3.Validate;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.rt2.hazelcast.RT2NodeHealthState;

public class PortableNodeHealthState implements CustomPortable
{
	//-------------------------------------------------------------------------
    public static final int CLASS_ID = 209;

	//-------------------------------------------------------------------------
    private String nodeUUID;
    private static final String FIELD_NODE_UUID = "nodeUUID";

	//-------------------------------------------------------------------------
    private String nodeName;
    private static final String FIELD_NODE_NAME = "nodeName";

	//-------------------------------------------------------------------------
    private String nodeState;
    private static final String FIELD_NODE_STATE = "nodeState";

	//-------------------------------------------------------------------------
    private String nodeType;
    private static final String FIELD_NODE_TYPE = "nodeType";

	//-------------------------------------------------------------------------
    private String nodeCleanupUUID;
    private static final String FIELD_NODE_CLEANUP_UUID = "nodeCleanupUUID";

	//-------------------------------------------------------------------------
    public static ClassDefinition CLASS_DEFINITION = null;

	//-------------------------------------------------------------------------
    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addUTFField(FIELD_NODE_UUID)
        .addUTFField(FIELD_NODE_NAME)
        .addUTFField(FIELD_NODE_STATE)
        .addUTFField(FIELD_NODE_TYPE)
        .addUTFField(FIELD_NODE_CLEANUP_UUID)
        .build();
    }

	//-------------------------------------------------------------------------
    public PortableNodeHealthState() {
        super();
    }

	//-------------------------------------------------------------------------
    public PortableNodeHealthState(final RT2NodeHealthState aState) {
        Validate.notNull(aState, "Mandatory argument missing: state");
        nodeUUID = aState.getNodeUUID();
        nodeName = aState.getNodeName();
        nodeState = aState.getState();
        nodeType = aState.getNodeType();
        nodeCleanupUUID = aState.getCleanupUUID();
    }

	//-------------------------------------------------------------------------
    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_NODE_UUID, nodeUUID);
        writer.writeUTF(FIELD_NODE_NAME, nodeName);
        writer.writeUTF(FIELD_NODE_STATE, nodeState);
        writer.writeUTF(FIELD_NODE_TYPE, nodeType);
        writer.writeUTF(FIELD_NODE_CLEANUP_UUID, nodeCleanupUUID);
    }

	//-------------------------------------------------------------------------
    @Override
    public void readPortable(PortableReader reader) throws IOException {
        nodeUUID = reader.readUTF(FIELD_NODE_UUID);
        nodeName = reader.readUTF(FIELD_NODE_NAME);
        nodeState = reader.readUTF(FIELD_NODE_STATE);
        nodeType = reader.readUTF(FIELD_NODE_TYPE);
        nodeCleanupUUID = reader.readUTF(FIELD_NODE_CLEANUP_UUID);
    }

	//-------------------------------------------------------------------------
    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

	//-------------------------------------------------------------------------
    @Override
    public int getClassId() {
        return CLASS_ID;
    }

	//-------------------------------------------------------------------------
    /**
     * Gets the document resource id
     *
     * @return The from
     */
    public String getNodeUUID() {
        return nodeUUID;
    }

	//-------------------------------------------------------------------------
    /**
     * Sets the from
     *
     * @param from The from to set
     */
    public void setNodeUUID(String sNodeUUID) {
        nodeUUID = sNodeUUID;
    }

	//-------------------------------------------------------------------------
    /**
     * Gets the save state
     *
     * @return The state
     */
    public String getNodeName() {
        return this.nodeName;
    }

	//-------------------------------------------------------------------------
    /**
     * Sets the save state
     *
     * @param state The state to set
     */
    public void setNodeName(String sName) {
        this.nodeName = sName;
    }

	//-------------------------------------------------------------------------
    /**
     * Gets the save state
     *
     * @return The state
     */
    public String getNodeState() {
        return this.nodeState;
    }

	//-------------------------------------------------------------------------
    /**
     * Sets the save state
     *
     * @param state The state to set
     */
    public void setNodeState(String sState) {
        this.nodeState = sState;
    }

	//-------------------------------------------------------------------------
    /**
     * Gets the save state
     *
     * @return The state
     */
    public String getNodeType() {
        return this.nodeType;
    }

	//-------------------------------------------------------------------------
    /**
     * Sets the save state
     *
     * @param state The state to set
     */
    public void setNodeType(String sType) {
        this.nodeType = sType;
    }

	//-------------------------------------------------------------------------
    /**
     * Gets the save state
     *
     * @return The state
     */
    public String getCleanupNodeUUID() {
        return this.nodeCleanupUUID;
    }

	//-------------------------------------------------------------------------
    /**
     * Sets the save state
     *
     * @param state The state to set
     */
    public void setCleanupNodeUUID(String sUUID) {
        this.nodeCleanupUUID = sUUID;
    }

	//-------------------------------------------------------------------------
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nodeUUID == null) ? 0 : nodeUUID.hashCode());
        result = prime * result + ((nodeName == null) ? 0 : nodeName.hashCode());
        result = prime * result + ((nodeState == null) ? 0 : nodeState.hashCode());
        result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
        result = prime * result + ((nodeCleanupUUID == null) ? 0 : nodeCleanupUUID.hashCode());
        return result;
    }

	//-------------------------------------------------------------------------
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PortableNodeHealthState))
            return false;
        PortableNodeHealthState other = (PortableNodeHealthState) obj;
        if (nodeUUID == null) {
            if (other.nodeUUID != null)
                return false;
        } else if (!nodeUUID.equals(other.nodeUUID))
            return false;
        if (nodeName == null) {
            if (other.nodeName != null)
                return false;
        } else if (!nodeName.equals(other.nodeName))
            return false;
        if (nodeState == null) {
            if (other.nodeState != null)
                return false;
        } else if (!nodeState.equals(other.nodeState))
            return false;
        if (nodeType == null) {
            if (other.nodeType != null)
                return false;
        } else if (!nodeType.equals(other.nodeType))
            return false;
        if (nodeCleanupUUID == null) {
            if (other.nodeCleanupUUID != null)
                return false;
        } else if (!nodeCleanupUUID.equals(other.nodeCleanupUUID))
            return false;
        return true;
    }

	//-------------------------------------------------------------------------
    @Override
    public String toString() {
        return "PortableNodeHealthState [uuid=" + nodeUUID + ", name=" + nodeName + ", state=" + nodeState + ", nodeType=" + nodeType + ", cleanupUUID=" + nodeCleanupUUID + "]";
    }

	//-------------------------------------------------------------------------
    public static RT2NodeHealthState createFrom(final PortableNodeHealthState portableNodeHealthState) {
        RT2NodeHealthState aNodeHealthState = null;

        if (null != portableNodeHealthState) {
            aNodeHealthState = new RT2NodeHealthState(
                                       portableNodeHealthState.getNodeUUID(),
                                       portableNodeHealthState.getNodeName(),
                                       portableNodeHealthState.getNodeState(),
                                       portableNodeHealthState.getNodeType(),
                                       portableNodeHealthState.getCleanupNodeUUID());
        }

        return aNodeHealthState;
    }
}
