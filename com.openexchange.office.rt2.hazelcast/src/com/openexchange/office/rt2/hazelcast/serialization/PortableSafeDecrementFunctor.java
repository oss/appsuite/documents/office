/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.serialization;

import java.io.IOException;
import com.hazelcast.core.IFunction;
import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;


public class PortableSafeDecrementFunctor implements CustomPortable, IFunction<Long, Long> {

    //-------------------------------------------------------------------------
    public static final int CLASS_ID = 214;

    private static final long serialVersionUID = -7833322817711705268L;

    private Long value = 1L;
    private static final String FIELD_VALUE = "value";

    //-------------------------------------------------------------------------
    public static ClassDefinition CLASS_DEFINITION = null;

    //-------------------------------------------------------------------------
    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addLongField(FIELD_VALUE)
        .build();
    }

    //-------------------------------------------------------------------------
    public PortableSafeDecrementFunctor(long value) {
        this.value = value;
    }

    //-------------------------------------------------------------------------
    public PortableSafeDecrementFunctor() {
        super();
    }

    //-------------------------------------------------------------------------
    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeLong(FIELD_VALUE, value);
    }

    //-------------------------------------------------------------------------
    @Override
    public void readPortable(PortableReader reader) throws IOException {
        value = reader.readLong(FIELD_VALUE);
    }

    //-------------------------------------------------------------------------
    @Override
    public Long apply(Long input) {
        if ((input - value ) > 0)
            return input - value;
        else
            return 0L;
    }

    //-------------------------------------------------------------------------
    @Override
    public int getClassId() {
        return CLASS_ID;
    }

    //-------------------------------------------------------------------------
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    //-------------------------------------------------------------------------
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PortableSafeDecrementFunctor))
            return false;
        PortableSafeDecrementFunctor other = (PortableSafeDecrementFunctor) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

}
