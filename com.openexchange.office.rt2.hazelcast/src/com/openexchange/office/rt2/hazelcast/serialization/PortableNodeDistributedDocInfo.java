/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.serialization;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.openexchange.hazelcast.serialization.CustomPortable;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap.DistributedDocInfoStatus;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;

public class PortableNodeDistributedDocInfo implements CustomPortable {

	//-------------------------------------------------------------------------
    public static final int CLASS_ID = 213;

	//-------------------------------------------------------------------------
    private String status;
    private static final String FIELD_STATUS = "status";

	//-------------------------------------------------------------------------
    private Set<String> clientIds = new HashSet<>();
    private static final String FIELD_CLIENT_IDS = "clientIds";

	//-------------------------------------------------------------------------
    public static ClassDefinition CLASS_DEFINITION = null;

	//-------------------------------------------------------------------------
    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
        .addUTFField(FIELD_STATUS)
        .addUTFField(FIELD_CLIENT_IDS)
        .build();
    }

    //-------------------------------------------------------------------------
    public PortableNodeDistributedDocInfo() {    	
    }

    //-------------------------------------------------------------------------    
    public PortableNodeDistributedDocInfo(DistributedDocInfoStatus status) {
    	this.status = status.name();
    }
    
	//-------------------------------------------------------------------------    
    public PortableNodeDistributedDocInfo(RT2CliendUidType clientUid, DistributedDocInfoStatus status) {
    	this.clientIds.add(clientUid.getValue());
    	this.status = status.name();
    }
        
	//-------------------------------------------------------------------------       
	public PortableNodeDistributedDocInfo(DistributedDocInfoStatus status, Set<String> clientIds) {
		this.status = status.name();
		this.clientIds = new HashSet<>(clientIds);
	}

	//-------------------------------------------------------------------------
    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_STATUS, status);        
        String [] array = new String[clientIds.size()];
        writer.writeUTFArray(FIELD_CLIENT_IDS, clientIds.toArray(array));
    }

	//-------------------------------------------------------------------------
    @Override
    public void readPortable(PortableReader reader) throws IOException {
    	status = reader.readUTF(FIELD_STATUS);
    	String [] clientIdsArray = reader.readUTFArray(FIELD_CLIENT_IDS);
    	clientIds = new HashSet<>();
    	for (int i=0;i<clientIdsArray.length;++i) {
    		clientIds.add(clientIdsArray[i]);
    	}
    }

	//-------------------------------------------------------------------------
    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

	//-------------------------------------------------------------------------
    @Override
    public int getClassId() {
        return CLASS_ID;
    }

	public DistributedDocInfoStatus getStatus() {
		return DistributedDocInfoStatus.valueOf(status);
	}

	public void setStatus(DistributedDocInfoStatus status) {
		this.status = status.name();
	}

	public Set<String> getClientIds() {
		return clientIds;
	}

	public void setClientIds(Set<String> clientIds) {
		this.clientIds = clientIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientIds == null) ? 0 : clientIds.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PortableNodeDistributedDocInfo other = (PortableNodeDistributedDocInfo) obj;
		if (clientIds == null) {
			if (other.clientIds != null)
				return false;
		} else if (!clientIds.equals(other.clientIds))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("PortableNodeDistributedDocInfo [status=%s, clientIds=%s]", status, clientIds);
	}
}
