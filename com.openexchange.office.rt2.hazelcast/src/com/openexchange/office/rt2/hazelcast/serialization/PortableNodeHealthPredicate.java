/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.serialization;

import java.io.IOException;
import java.util.Map.Entry;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.hazelcast.query.Predicate;
import com.openexchange.hazelcast.serialization.CustomPortable;

public class PortableNodeHealthPredicate implements Predicate<String, PortableNodeHealthState>, CustomPortable
{
	private static final long serialVersionUID = 4973630189320203390L;

	public static final int CLASS_ID = 210;

    private final static String FIELD_UUID="uuid";

    public static ClassDefinition CLASS_DEFINITION = null;

    private String uuid;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
            .addUTFField(FIELD_UUID)
            .build();
    }

    public PortableNodeHealthPredicate() {
        super();
    }

    public PortableNodeHealthPredicate(String sNodeUUID) {
        this.uuid = sNodeUUID;
    }

    @Override
    public boolean apply(Entry<String, PortableNodeHealthState> mapEntry) {
    	final PortableNodeHealthState aNodeHealthState = mapEntry.getValue();
        return uuid.equals(aNodeHealthState.getCleanupNodeUUID());
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_UUID, uuid);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        uuid = reader.readUTF(FIELD_UUID);
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }
}
