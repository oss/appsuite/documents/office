/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.serialization;

import java.io.IOException;
import java.util.Map.Entry;

import com.hazelcast.nio.serialization.ClassDefinition;
import com.hazelcast.nio.serialization.ClassDefinitionBuilder;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import com.hazelcast.query.Predicate;
import com.openexchange.hazelcast.serialization.CustomPortable;

public class PortableNodeHealthStatePredicate implements Predicate<String, PortableNodeHealthState>, CustomPortable
{
    private static final long serialVersionUID = -637870331566389052L;

    public static final int CLASS_ID = 212;

    private final static String FIELD_STATE = "state";

    public static ClassDefinition CLASS_DEFINITION = null;

    private String sState;

    static {
        CLASS_DEFINITION = new ClassDefinitionBuilder(FACTORY_ID, CLASS_ID)
            .addUTFField(FIELD_STATE)
            .build();
    }

    public PortableNodeHealthStatePredicate() {
        super();
    }

    public PortableNodeHealthStatePredicate(String sState) {
        this.sState = sState;
    }

    @Override
    public boolean apply(Entry<String, PortableNodeHealthState> mapEntry) {
        final PortableNodeHealthState aNodeHealthState = mapEntry.getValue();
        return sState.equals(aNodeHealthState.getNodeState());
    }

    @Override
    public void writePortable(PortableWriter writer) throws IOException {
        writer.writeUTF(FIELD_STATE, sState);
    }

    @Override
    public void readPortable(PortableReader reader) throws IOException {
        sState = reader.readUTF(FIELD_STATE);
    }

    @Override
    public int getFactoryId() {
        return FACTORY_ID;
    }

    @Override
    public int getClassId() {
        return CLASS_ID;
    }
}
