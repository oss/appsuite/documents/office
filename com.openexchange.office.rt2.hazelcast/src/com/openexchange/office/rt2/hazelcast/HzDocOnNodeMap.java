/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.rt2.hazelcast;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeDocsPredicate;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.logging.MDCEntries;

@Service
@RegisteredService(registeredClass = RT2DocOnNodeMap.class)
public class HzDocOnNodeMap extends RT2HazelcastService implements RT2DocOnNodeMap {

    private static final Logger log = LoggerFactory.getLogger(HzDocOnNodeMap.class);

    //-------------------------------------------------------------------------
    @Override
    public void afterPropertiesSet() throws Exception {
        mapIdentifier = distributedMapnameDisposer.discoverMapName("rt2DocOnNodeMap-");
        if (StringUtils.isEmpty(mapIdentifier)) {
            final String msg = "Distributed rt2 doc node map couldn't be found in hazelcast configuration";
            throw new IllegalStateException(msg, new BundleException(msg, BundleException.ACTIVATOR_ERROR));
        }
        log.info("Registered rt2DocOnNodeMap to Hazelcast");
    }

    //-------------------------------------------------------------------------
    @Override
    public String get(String sDocUID) {
        final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
        return allDocToNodeMappings.get(sDocUID);
    }

    //-------------------------------------------------------------------------
    @Override
    public Map<String, String> get(Collection<String> aDocUIDs) {
        Map<String, String> aFoundNodesMap = new HashMap<>();
        final Set<String> aDocUIDSet = new HashSet<>();

        for (final String sDocUID : aDocUIDs) {
            aDocUIDSet.add(sDocUID);
        }

        if (!aDocUIDSet.isEmpty()) {
            final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
            aFoundNodesMap = allDocToNodeMappings.getAll(aDocUIDSet);
        }

        return aFoundNodesMap;
    }

    //-------------------------------------------------------------------------
    @Override
    public String set(String sDocUID) {
        String thisNodeUUID = clusterService.getLocalMemberUuid();
        final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
        String sPrevNodeUID = allDocToNodeMappings.get(sDocUID);

        if (sPrevNodeUID != null) {
            // but the previous resource provides a presence
            allDocToNodeMappings.put(sDocUID, thisNodeUUID);
        } else {
            sPrevNodeUID = allDocToNodeMappings.putIfAbsent(sDocUID, thisNodeUUID);
        }
        return sPrevNodeUID;
    }

    //-------------------------------------------------------------------------
    @Override
    public String set(String docUID, String nodeUUID) {
        final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
        String sPrevNodeUID = allDocToNodeMappings.get(docUID);
        if(sPrevNodeUID != null) {
            return allDocToNodeMappings.put(docUID, nodeUUID);
        }
        return null;
    }

    //-------------------------------------------------------------------------
    @Override
    public void setIfAbsent(String sDocUID) {
        String thisNodeUUID = clusterService.getLocalMemberUuid();
        final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
        String aPrevNodeUUID = allDocToNodeMappings.get(sDocUID);

        if (aPrevNodeUUID == null) {
            allDocToNodeMappings.put(sDocUID, thisNodeUUID);
        }
    }

    //-------------------------------------------------------------------------
    @Override
    public String remove(String sDocUID) {
        final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
        String sPrevNodeUUID = allDocToNodeMappings.get(sDocUID);

        if (sPrevNodeUUID != null) {
            allDocToNodeMappings.remove(sDocUID);
        }
        return sPrevNodeUUID;
    }

    //-------------------------------------------------------------------------
    @Override
    public Map<String, String> remove(Collection<String> aDocUIDs) {
        final Map<String, String> aRemovedMap = new HashMap<>();
        final Set<String> aDocUIDSet = new HashSet<>();

        for (final String sDocUID : aDocUIDs) {
            aDocUIDSet.add(sDocUID);
        }

        if (!aDocUIDSet.isEmpty()) {
            final DistributedMap<String, String> allDocToNodeMappings = getDocNodeMapping();
            final Map<String, String> aMatchStates = allDocToNodeMappings.getAll(aDocUIDSet);

            if (aMatchStates != null) {
                for (Entry<String, String> entry : aMatchStates.entrySet()) {
                    final String sFoundUID = entry.getKey();
                    allDocToNodeMappings.remove(sFoundUID);
                    aRemovedMap.put(sFoundUID, entry.getValue());
                }
            }
        }

        return aRemovedMap;
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<String> getDocsOfMember() {
        String thisNodeUUID = clusterService.getLocalMemberUuid();
        return getDocsOfMember(thisNodeUUID);
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<String> getDocsOfMember(final String sNodeUUID) {
        final DistributedMap<String, String> allResources = getDocNodeMapping();
        final PortableNodeDocsPredicate nodeDocsPredicate = new PortableNodeDocsPredicate(sNodeUUID);
        final Set<Entry<String, String>> matchingResources = allResources.entrySet(nodeDocsPredicate);

        final Set<String> foundIds = new HashSet<String>();
        final Iterator<Entry<String, String>> iterator = matchingResources.iterator();
        while (iterator.hasNext()) {
            try {
                Entry<String, String> next = iterator.next();

                foundIds.add(next.getKey());
            } catch (Exception e) {
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, sNodeUUID);
                log.error("Couldn't add resource that was found for member com.openexchange.rt2.backend.uid " + sNodeUUID, e);
                MDC.remove(MDCEntries.BACKEND_UID);
            }
        }

        return foundIds;
    }

    //-------------------------------------------------------------------------
    @Override
    public String getUniqueMapName() {
        return mapIdentifier;
    }

    //-------------------------------------------------------------------------
    public DistributedMap<String, String> getDocNodeMapping() {
        return cachingFacade.getDistributedMap(mapIdentifier, String.class, String.class);
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<String> getMember() {
        return new HashSet<>(getDocNodeMapping().values());
    }

}
