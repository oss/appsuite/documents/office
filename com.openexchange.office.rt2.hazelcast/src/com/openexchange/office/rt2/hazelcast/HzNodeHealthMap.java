/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeHealthPredicate;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeHealthState;
import com.openexchange.office.rt2.hazelcast.serialization.PortableNodeHealthStatePredicate;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.osgi.ExceptionUtils;

@Service
@RegisteredService(registeredClass=RT2NodeHealthMap.class)
public class HzNodeHealthMap extends RT2HazelcastService implements RT2NodeHealthMap
{
	private static final Logger log = LoggerFactory.getLogger(HzNodeHealthMap.class);

    @Override
	public void afterPropertiesSet() throws Exception {
    	mapIdentifier = distributedMapnameDisposer.discoverMapName("rt2NodeHealthMap-");
        if(StringUtils.isEmpty(mapIdentifier)) {
            final String msg = "Distributed rt2 node health map couldn't be found in hazelcast configuration";
            throw new IllegalStateException(msg, new BundleException(msg, BundleException.ACTIVATOR_ERROR));
        }
        log.info("Registered rt2NodeHealthMap to Hazelcast");
	}


    //-------------------------------------------------------------------------
	@Override
	public RT2NodeHealthState get(String sNodeUID) throws OXException {
		RT2NodeHealthState aNodeHealthState = null;

		DistributedMap<String, PortableNodeHealthState> allNodeHealthStates = getHealthStateMapping();
        PortableNodeHealthState aPortableNodeHealthState = allNodeHealthStates.get(sNodeUID);
        if (aPortableNodeHealthState != null) {
        	aNodeHealthState = PortableNodeHealthState.createFrom(aPortableNodeHealthState);
        }

        return aNodeHealthState;
	}

    //-------------------------------------------------------------------------
	@Override
	public Map<String, RT2NodeHealthState> get(Collection<String> nodeUIDs) throws OXException {
        final Map<String, RT2NodeHealthState> aFoundNodesMap = new HashMap<>();
        final Set<String> aNodeUIDSet = new HashSet<>();

        for (final String sNodeUID : nodeUIDs) {
        	aNodeUIDSet.add(sNodeUID);
        }

        if (!aNodeUIDSet.isEmpty()) {
        	DistributedMap<String, PortableNodeHealthState> allResources = getHealthStateMapping();
            Map<String, PortableNodeHealthState> aMatchNodeHealthStates = allResources.getAll(aNodeUIDSet);
            if (aMatchNodeHealthStates != null) {
                for (Entry<String, PortableNodeHealthState> entry : aMatchNodeHealthStates.entrySet()) {
                    final String sNodeUID = entry.getKey();
                    final PortableNodeHealthState aFoundNodeHealthState = entry.getValue();
                    aFoundNodesMap.put(sNodeUID, PortableNodeHealthState.createFrom(aFoundNodeHealthState));
                }
            }
        }

        return aFoundNodesMap;
	}

    //-------------------------------------------------------------------------
	@Override
	public RT2NodeHealthState set(String sNodeUID, RT2NodeHealthState state) throws OXException {
        final PortableNodeHealthState aCurrPortableNodeHealthState = new PortableNodeHealthState(state);

        PortableNodeHealthState aPrevPortableNodeHealthState = null;
        RT2NodeHealthState      aPrevNodeHealthStates        = null;

        try {
            final DistributedMap<String, PortableNodeHealthState> allNodeHealthStates = getHealthStateMapping();
           	aPrevPortableNodeHealthState = allNodeHealthStates.put(sNodeUID, aCurrPortableNodeHealthState);
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != aPrevPortableNodeHealthState) {
        	aPrevNodeHealthStates = PortableNodeHealthState.createFrom(aPrevPortableNodeHealthState);
        }

        return aPrevNodeHealthStates;
	}

    //-------------------------------------------------------------------------
	@Override
	public void setIfAbsent(String sNodeUID, RT2NodeHealthState state) throws OXException {
        final PortableNodeHealthState aCurrPortableNodeHealthState = new PortableNodeHealthState(state);
        try {
            final DistributedMap<String, PortableNodeHealthState> allNodeHealthStates = getHealthStateMapping();
            allNodeHealthStates.putIfAbsent(sNodeUID, aCurrPortableNodeHealthState);
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }
	}

    //-------------------------------------------------------------------------
	@Override
	public RT2NodeHealthState remove(String sNodeUID) throws OXException {
		PortableNodeHealthState aPrevPortableNodeHealthState = null;
		RT2NodeHealthState      aPrevNodeHealthState         = null;

        try {
            final DistributedMap<String, PortableNodeHealthState> allDocStates = getHealthStateMapping();
            aPrevPortableNodeHealthState = allDocStates.remove(sNodeUID);
        } catch (Throwable t) {
            ExceptionUtils.handleThrowable(t);
            throw new OXException(t);
        }

        if (null != aPrevPortableNodeHealthState) {
        	aPrevNodeHealthState = PortableNodeHealthState.createFrom(aPrevPortableNodeHealthState);
        }

        return aPrevNodeHealthState;
	}

    //-------------------------------------------------------------------------
	@Override
	public Map<String, RT2NodeHealthState> remove(Collection<String> aNodeUIDs) throws OXException {
        final Map<String, RT2NodeHealthState> aRemovedMap = new HashMap<>();
        final Set<String>                     aNodeUIDSet = new HashSet<>();

        for (final String sNodeUID : aNodeUIDs) {
        	aNodeUIDSet.add(sNodeUID);
        }

        if (!aNodeUIDSet.isEmpty()) {
            DistributedMap<String, PortableNodeHealthState> aAllNodeHealthStates = getHealthStateMapping();
            Map<String, PortableNodeHealthState>  aMatchPortableNodeHealthStates = aAllNodeHealthStates.getAll(aNodeUIDSet);

            if (aMatchPortableNodeHealthStates != null) {
                for (Entry<String, PortableNodeHealthState> entry : aMatchPortableNodeHealthStates.entrySet()) {
                    final String sFoundUID = entry.getKey();
                    final PortableNodeHealthState aFoundNodeHealthState = entry.getValue();
                    aRemovedMap.put(sFoundUID, PortableNodeHealthState.createFrom(aFoundNodeHealthState));

                    aAllNodeHealthStates.remove(sFoundUID);
                }
            }
        }

        return aRemovedMap;
	}

    //-------------------------------------------------------------------------
    /**
     * Find all member nodes in this directory that are clean-up by a given member node.
     *
     * @param member The cluster member
     * @return all Resources in this directory that are located on the given member node.
     * @throws OXException
     */
    @Override
    public Set<RT2NodeHealthState> getCleanupNodesOfMember(final String sNodeUUID)throws OXException {
        final DistributedMap<String, PortableNodeHealthState>       allResources  = getHealthStateMapping();
        final PortableNodeHealthPredicate                 nodePredicate = new PortableNodeHealthPredicate(sNodeUUID);
        final Set<Entry<String, PortableNodeHealthState>> matching      = allResources.entrySet(nodePredicate);

        final Set<RT2NodeHealthState> foundIds = new HashSet<>();
        final Iterator<Entry<String, PortableNodeHealthState>> iterator = matching.iterator();
        while(iterator.hasNext()) {
            try {
                final Entry<String, PortableNodeHealthState> next = iterator.next();
                foundIds.add(PortableNodeHealthState.createFrom(next.getValue()));
            } catch (Exception e) {
                MDCHelper.safeMDCPut(MDCEntries.BACKEND_UID, sNodeUUID);
                log.error("Couldn't add node to cleanup that was found for member " + sNodeUUID, e);
                MDC.remove(MDCEntries.BACKEND_UID);
            }
        }

        return foundIds;
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<RT2NodeHealthState> getMembersOfState(final String sState) {
        final DistributedMap<String, PortableNodeHealthState> allResources   = getHealthStateMapping();
        final PortableNodeHealthStatePredicate statePredicate      = new PortableNodeHealthStatePredicate(sState);
        final Set<Entry<String, PortableNodeHealthState>> matching = allResources.entrySet(statePredicate);

        final Set<RT2NodeHealthState> foundIds = new HashSet<>();
        final Iterator<Entry<String, PortableNodeHealthState>> iterator = matching.iterator();
        while(iterator.hasNext()) {
            try {
                final Entry<String, PortableNodeHealthState> next = iterator.next();
                foundIds.add(PortableNodeHealthState.createFrom(next.getValue()));
            } catch (Exception e) {
            	log.error("Couldn't add member that was found for state " + sState, e);
            }
        }

        return foundIds;
    }

    //-------------------------------------------------------------------------
    @Override
    public Set<RT2NodeHealthState> getAllMembers() {
        final DistributedMap<String, PortableNodeHealthState> allResources = getHealthStateMapping();

        final Set<RT2NodeHealthState> result = new HashSet<>();
        allResources.forEach((s, p) -> {
            result.add(PortableNodeHealthState.createFrom(p));
        });

        return result;
    }

    //-------------------------------------------------------------------------
    @Override
    public String getUniqueMapName() {
        return mapIdentifier;
	}

    //-------------------------------------------------------------------------
    public DistributedMap<String, PortableNodeHealthState> getHealthStateMapping() {
        return cachingFacade.getDistributedMap(mapIdentifier, String.class, PortableNodeHealthState.class);
    }

}
