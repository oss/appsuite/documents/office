/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.hazelcast.HzDocOnNodeMap;
import com.openexchange.office.tools.service.caching.DistributedMap;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office.rt2:name=DocOnNodeMap")
public class HzDocOnNodeMapManagement
{
	//-------------------------------------------------------------------------
	@Autowired
    private HzDocOnNodeMap docNodeMap;

	//-------------------------------------------------------------------------
	@JMXBeanAttribute(name="DocNodeMapping")
    public Map<String, String> getDocNodeMapping() 
    {
       final DistributedMap<String, String> docNodeMapping = docNodeMap.getDocNodeMapping();
       final Map<String, String> jmxMap = new HashMap<>(docNodeMapping.size());
       for (Entry<String, String> entry : docNodeMapping.entrySet()) {
           final String sDocUUID = entry.getKey();
           final String sNodeUUID = entry.getValue();
           jmxMap.put(sDocUUID, sNodeUUID);
       }

       return jmxMap;
	}

	@JMXBeanAttribute(name="Size")
	public long getSize() {
		return getDocNodeMapping().size();
	}
}
