/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.hazelcast.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanAttribute;

@Service
@JMXBean
@ManagedResource(objectName="com.openexchange.office.rt2:name=RefCountClientsMap")
public class HzRefCountClientsMapManagement {

    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;

	//-------------------------------------------------------------------------
	@JMXBeanAttribute(name="DocUidToClients")
    public Map<String, Set<String>> getDocNodeMapping() {
		Map<String, Set<String>> res = new HashMap<>();
		Set<RT2DocUidType> docUids = distributedDocInfoMap.getAllRegisteredDocIds();
		docUids.forEach(d -> {
			Set<String> clients = distributedDocInfoMap.getClientsOfDocUid(d);
			res.put(d.getValue(), clients);
		});
		return res;
	}

	@JMXBeanAttribute(name="Size")
	public long getSize() {
		return getDocNodeMapping().size();
	}
    
}
