/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package org.pptx4j.pml_2018_8;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

    private final static QName _AUTHORLST_QNAME = new QName("http://schemas.microsoft.com/office/powerpoint/2018/8/main", "authorLst");
    private final static QName _CMLST_QNAME = new QName("http://schemas.microsoft.com/office/powerpoint/2018/8/main", "cmLst");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.pptx4j.pml_2018_8;
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTAuthorList }
     *
     */
    public CTAuthorList createCTAuthorList() {
        return new CTAuthorList();
    }

    /**
     * Create an instance of {@link CTCommentList }
     *
     */
    public CTCommentList createCTCommentList() {
        return new CTCommentList();
    }

    /**
     * Create an instance of {@link CTCommentReplyList }
     *
     */
    public CTCommentReplyList createCTCommentReplyList() {
        return new CTCommentReplyList();
    }

    /**
     * Create an instance of {@link CTComment }
     *
     */
    public CTComment createCTComment() {
        return new CTComment();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTAuthorList }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/powerpoint/2018/8/main", name = "authorLst")
    public JAXBElement<CTAuthorList> createCTAuthorList(CTAuthorList value) {
        return new JAXBElement<CTAuthorList>(_AUTHORLST_QNAME, CTAuthorList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCommentList }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/powerpoint/2018/8/main", name = "cmLst")
    public JAXBElement<CTCommentList> createCTCommentList(CTCommentList value) {
        return new JAXBElement<CTCommentList>(_CMLST_QNAME, CTCommentList.class, null, value);
    }
}
