/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package org.pptx4j.pml_2018_8;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml_2013_command.CTDrawingElementMonikerList;
import org.docx4j.dml_2013_command.CTTextCharRangeMonikerList;
import org.pptx4j.pml.CTExtensionList;
import org.pptx4j.pml.IComment;
import org.pptx4j.pml_2013.CTSlideMonikerList;
import com.openexchange.office.filter.core.DLList;

/**
 * <p>Java class for CT_Comment complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *

<xsd:group name="EG_CommentAnchor" xmlns:p216="http://schemas.microsoft.com/office/powerpoint/2021/06/main">
  <xsd:choice>
    <xsd:element ref="pc:sldMkLst" minOccurs="1" maxOccurs="1"/>
    <xsd:element ref="ac:deMkLst" minOccurs="0" maxOccurs="unbounded"/>
    <xsd:element ref="ac:txMkLst" minOccurs="0" maxOccurs="unbounded"/>
    <xsd:element name="unknownAnchor" type="CT_CommentUnknownAnchor" minOccurs="1" maxOccurs="1"/>
  </xsd:choice>
</xsd:group>
<xsd:group name="EG_CommentProperties" xmlns:p216="http://schemas.microsoft.com/office/powerpoint/2021/06/main">
  <xsd:sequence>
    <xsd:element name="txBody" type="a:CT_TextBody" minOccurs="0" maxOccurs="1"/>
    <xsd:element name="extLst" type="p:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
  </xsd:sequence>
</xsd:group>
<xsd:complexType name="CT_Comment">
   <xsd:sequence>
     <xsd:group ref="EG_CommentAnchor" minOccurs="1" maxOccurs="1"/>
     <xsd:element name="pos" type="a:CT_Point2D" minOccurs="0" maxOccurs="1"/>
     <xsd:element name="replyLst" type="CT_CommentReplyList" minOccurs="0" maxOccurs="1"/>
     <xsd:group ref="EG_CommentProperties" minOccurs="1" maxOccurs="1"/>
   </xsd:sequence>
   <xsd:attributeGroup ref="AG_CommentProperties"/>
   <xsd:attribute name="startDate" type="xsd:dateTime" use="optional"/>
   <xsd:attribute name="assignedTo" type="ST_AuthorIdList" use="optional" default=""/>
   <xsd:attribute name="complete" type="s:ST_PositiveFixedPercentage" default="0%" use="optional"/>
 </xsd:complexType>

 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Comment", propOrder = {
    "commentAnchor",
    "pos",
    "replyLst",
    "txBody",
    "extLst"
})
@XmlRootElement(name="cm")
public class CTComment extends AGCommentProperties implements IComment {

    @XmlElementRefs({
        @XmlElementRef(name = "sldMkLst", namespace = "http://schemas.microsoft.com/office/powerpoint/2013/main/command", type = CTSlideMonikerList.class),
        @XmlElementRef(name = "deMkLst", namespace = "http://schemas.microsoft.com/office/drawing/2013/main/command", type = CTDrawingElementMonikerList.class),
        @XmlElementRef(name = "txMkLst", namespace = "http://schemas.microsoft.com/office/drawing/2013/main/command", type = CTTextCharRangeMonikerList.class)
    })
    protected DLList<Object> commentAnchor;

    @XmlElement
    protected CTPoint2D pos;

    @XmlElement
    protected CTCommentReplyList replyLst;

    @XmlElement
    protected CTTextBody txBody;

    @XmlElement
    protected CTExtensionList extLst;

    @XmlAttribute(name = "startDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;

    @XmlAttribute
    protected String assignedTo;

    @XmlAttribute
    protected Integer complete;

    public DLList<Object> getCommentAnchor() {
        if (commentAnchor== null) {
            commentAnchor = new DLList<Object>();
        }
        return commentAnchor;
    }

    /*
     *
     * returning position of bubble in 100thmm
     *
     * (non-Javadoc)
     * @see org.pptx4j.pml.IComment#getPos(boolean)
     */
    @Override
    public CTPoint2D getPos(boolean forceCreate) {
        if(pos == null && forceCreate) {
            pos = new CTPoint2D();
        }
        return pos != null ? new CTPoint2D((long)(pos.getX() + 0.5) / 360, (long)(pos.getY() + 0.5) / 360) : null;
    }

    @Override
    public void setPos(CTPoint2D value) {
        if(value == null) {
            pos = null;
        }
        else {
            pos = new CTPoint2D(value.getX() * 360, value.getY() * 360);
        }
    }

    public CTCommentReplyList getReplyLst(boolean forceCreate) {
        if(replyLst == null && forceCreate) {
            replyLst = new CTCommentReplyList();
        }
        return replyLst;
    }

    public void setReplyLst(CTCommentReplyList value) {
        replyLst = value;
    }

    public CTTextBody getTxBody(boolean forceCreate) {
        if(txBody==null && forceCreate)  {
            txBody = new CTTextBody();
        }
        return txBody;
    }

    public void setTxBody(CTTextBody value) {
        txBody = value;
    }

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(XMLGregorianCalendar value) {
        startDate = value;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String value) {
        assignedTo = value;
    }

    public Integer getComplete() {
        return complete;
    }

    public void setComplete(Integer value) {
        complete = value;
    }

    @Override
    public CTExtensionList getExtLst(boolean forceCreate) {
        if(extLst==null && forceCreate) {
            extLst = new CTExtensionList();
        }
        return null;
    }

    @Override
    public long getIdx() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setIdx(long value) {
        // TODO Auto-generated method stub

    }
}
