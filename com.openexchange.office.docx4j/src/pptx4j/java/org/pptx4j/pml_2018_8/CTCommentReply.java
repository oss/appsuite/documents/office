/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package org.pptx4j.pml_2018_8;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTTextBody;
import org.pptx4j.pml.CTExtensionList;

/**
 * <p>Java class for CT_CommentReply complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *

<xsd:group name="EG_CommentProperties" xmlns:p216="http://schemas.microsoft.com/office/powerpoint/2021/06/main">
  <xsd:sequence>
    <xsd:element name="txBody" type="a:CT_TextBody" minOccurs="0" maxOccurs="1"/>
    <xsd:element name="extLst" type="p:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
  </xsd:sequence>
</xsd:group>
<xsd:complexType name="CT_CommentReply">
   <xsd:sequence>
     <xsd:group ref="EG_CommentProperties" minOccurs="1" maxOccurs="1"/>
   </xsd:sequence>
   <xsd:attributeGroup ref="AG_CommentProperties"/>
 </xsd:complexType>

 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentReply", propOrder = {
    "txBody",
    "extLst"
})
@XmlRootElement(name="reply")
public class CTCommentReply extends AGCommentProperties {

    @XmlElement
    protected CTTextBody txBody;

    @XmlElement
    protected CTExtensionList extLst;

    public CTTextBody getTxBody(boolean forceCreate) {
        if(txBody==null && forceCreate)  {
            txBody = new CTTextBody();
        }
        return txBody;
    }

    public void setTxBody(CTTextBody value) {
        txBody = value;
    }

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }
}
