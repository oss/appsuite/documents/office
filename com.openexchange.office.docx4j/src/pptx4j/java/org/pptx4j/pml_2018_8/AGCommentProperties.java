/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package org.pptx4j.pml_2018_8;

import java.util.Random;
import java.util.UUID;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>Java class for AG_CommentProperties
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>

<xsd:attributeGroup name="AG_CommentProperties" xmlns:p216="http://schemas.microsoft.com/office/powerpoint/2021/06/main">
     <xsd:attribute name="id" type="ST_CommentId" use="required"/>
     <xsd:attribute name="authorId" type="ST_AuthorId" use="required"/>
     <xsd:attribute name="status" type="ST_CommentStatus" use="optional" default="active"/>
     <xsd:attribute name="created" type="xsd:dateTime" use="required"/>
</xsd:attributeGroup>

 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AG_CommentProperties")
public class AGCommentProperties {

    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "authorId", required = true)
    protected String authorId;
    @XmlAttribute(name = "status")
    protected STCommentStatus status;
    @XmlAttribute(name = "created", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar created;

    /**
     * Gets the value of the id property.
     *
     * The id will be in following GUID format: "{1234ABCD-AABB-AABB-AABB-AABBAABB1122}"
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * The id has to be in following GUID format: "{1234ABCD-AABB-AABB-AABB-AABBAABB1122}"
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        id = value;
    }

    /**
     * Gets the value of the id property.
     *
     * The id will be in following GUID format: "{1234ABCD-AABB-AABB-AABB-AABBAABB1122}"
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public long getAuthorId() {
        return CTAuthor.authorIdToLong(authorId);
    }

    /**
     * Sets the value of the id property.
     *
     * The id has to be in following GUID format: "{1234ABCD-AABB-AABB-AABB-AABBAABB1122}"
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAuthorId(long value) {
        authorId = CTAuthor.longToAuthorId(value);
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link STCommentStatus }
     *
     */
    public STCommentStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link STCommentStatus }
     *
     */
    public void setCommentStatus(STCommentStatus value) {
        status = value;
    }

    /**
     * Gets the value of the dt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDt() {
        return created;
    }

    /**
     * Sets the value of the dt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDt(XMLGregorianCalendar value) {
        this.created = value;
    }

    protected static Random random = new Random();

    final public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        if(id==null) {
            final StringBuilder guid = new StringBuilder(38);
            guid.append('{')
                .append(UUID.randomUUID().toString())
                .append('}');
            id = guid.toString();
        }
    }
}
