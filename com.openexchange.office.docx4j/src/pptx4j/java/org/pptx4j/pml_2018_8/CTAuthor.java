/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package org.pptx4j.pml_2018_8;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.pptx4j.pml.CTExtensionList;
import org.pptx4j.pml.ICommentAuthor;

/**
 * <p>Java class for CT_Comment complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *

   <xsd:simpleType name="ST_AuthorId">
     <xsd:restriction base="s:ST_Guid"/>
   </xsd:simpleType>
   <xsd:complexType name="CT_Author">
     <xsd:sequence>
       <xsd:element name="extLst" type="p:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
     </xsd:sequence>
     <xsd:attribute name="id" type="ST_AuthorId" use="required"/>
     <xsd:attribute name="name" type="xsd:string" use="required"/>
     <xsd:attribute name="initials" type="xsd:string" use="optional"/>
     <xsd:attribute name="userId" type="xsd:string" use="required"/>
     <xsd:attribute name="providerId" type="xsd:string" use="required"/>
   </xsd:complexType>

 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Author", propOrder = {
    "extLst"
})
@XmlRootElement(name="author")
public class CTAuthor implements ICommentAuthor {

    @XmlElement
    protected CTExtensionList extLst;

    @XmlAttribute(required = true) // has to be in this format: {AABBCCDD-AABB-AABB-AABB-AABBCCDD1122}
    protected String id;

    @XmlAttribute(required = true)
    protected String name;

    @XmlAttribute
    protected String initials;

    @XmlAttribute(required = true)
    protected String userId;

    @XmlAttribute(required = true)
    protected String providerId;

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    @Override
    public long getId() {
        return authorIdToLong(id);
    }

    @Override
    public void setId(long value) {
        id = longToAuthorId(value);
    }

    public static long authorIdToLong(String authorId) {
        // TODO: value should be guid in this format: {AABBCCDD-AABB-AABB-AABB-AABBCCDD1122}
        if(authorId != null && authorId.length()==38) {
            final StringBuilder b = new StringBuilder();
            b.append(authorId.subSequence(1, 9));
            b.append(authorId.subSequence(10, 14));
            b.append(authorId.subSequence(15, 19));
            try {
                return Long.parseUnsignedLong(b.toString(), 16);
            }
            catch(NumberFormatException e) {
                //
            }
        }
        return 0;
    }

    public static String longToAuthorId(long value) {
        // TODO: value should be guid in this format: {AABBCCDD-AABB-AABB-AABB-AABBCCDD1122}
        final String hexChars = String.format("%016X", value);
        final StringBuilder b = new StringBuilder();
        b.append('{');
        b.append(hexChars.subSequence(0, 8));
        b.append('-');
        b.append(hexChars.subSequence(8, 12));
        b.append('-');
        b.append(hexChars.subSequence(12, 16));
        b.append("-0000-AABBCCDD1122}");
        return b.toString();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String value) {
        name = value;
    }

    @Override
    public String getInitials() {
        return initials;
    }

    @Override
    public void setInitials(String value) {
        initials = value;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String value) {
        userId = value;
    }

    @Override
    public String getProviderId() {
        return providerId;
    }

    @Override
    public void setProviderId(String value) {
        providerId = value;
    }
}
