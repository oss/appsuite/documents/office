/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *   
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.pptx4j.pml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualGraphicFrameProperties;
import org.docx4j.dml.CTTable;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.GraphicData;
import org.docx4j.dml.IGraphicalObjectFrame;
import org.docx4j.dml.ITransform2DAccessor;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.component.Child;

/**
 * <p>Java class for CT_GraphicalObjectFrame complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_GraphicalObjectFrame">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvGraphicFramePr" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_GraphicalObjectFrameNonVisual"/>
 *         &lt;element name="xfrm" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Transform2D"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/drawingml/2006/main}graphic"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ExtensionListModify" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

@XmlRootElement(name="graphicFrame")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_GraphicalObjectFrame", propOrder = {
    "nvGraphicFramePr",
    "xfrm",
    "graphic",
    "extLst"
})
public class CTGraphicalObjectFrame implements Child, IContentAccessor<Object>, ITransform2DAccessor, INvPrAccessor, IGraphicalObjectFrame {

    @XmlElement(required = true)
    protected CTGraphicalObjectFrameNonVisual nvGraphicFramePr;
    @XmlElement(required = true)
    protected CTTransform2D xfrm;
    @XmlElement(namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", required = true)
    protected Graphic graphic;
    protected CTExtensionListModify extLst;
    @XmlTransient
    private Object parent;

    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	if(xfrm==null&&forceCreate) {
    		xfrm = new CTTransform2D();
    	}
    	return xfrm;
    }

    @Override
    public void removeXfrm() {
        xfrm = null;
    }

	@Override
	public NvPr getNvPr(boolean forceCreate) {
		if(nvGraphicFramePr.nvPr==null&&forceCreate) {
			nvGraphicFramePr.nvPr = new NvPr();
		}
		return nvGraphicFramePr.nvPr;
	}

    /**
     * Gets the value of the nvGraphicFramePr property.
     * 
     * @return
     *     possible object is
     *     {@link CTGraphicalObjectFrameNonVisual }
     *     
     */
    public CTGraphicalObjectFrameNonVisual getNvGraphicFramePr() {
        return nvGraphicFramePr;
    }

    /**
     * Sets the value of the nvGraphicFramePr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTGraphicalObjectFrameNonVisual }
     *     
     */
    public void setNvGraphicFramePr(CTGraphicalObjectFrameNonVisual value) {
        this.nvGraphicFramePr = value;
    }

    /**
     * Gets the value of the graphic property.
     * 
     * @return
     *     possible object is
     *     {@link Graphic }
     *     
     */
    @Override
    public Graphic getGraphic() {
        return graphic;
    }

    /**
     * Sets the value of the graphic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Graphic }
     *     
     */
    @Override
    public void setGraphic(Graphic value) {
        this.graphic = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionListModify }
     *     
     */
    public CTExtensionListModify getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionListModify }
     *     
     */
    public void setExtLst(CTExtensionListModify value) {
        this.extLst = value;
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvGraphicFramePr==null&&createIfMissing) {
			nvGraphicFramePr = new CTGraphicalObjectFrameNonVisual();
		}
		if(nvGraphicFramePr!=null) {
			if(nvGraphicFramePr.getCNvPr()==null&&createIfMissing) {
				nvGraphicFramePr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvGraphicFramePr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualGraphicFrameProperties getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvGraphicFramePr==null&&createIfMissing) {
			nvGraphicFramePr = new CTGraphicalObjectFrameNonVisual();
		}
		if(nvGraphicFramePr!=null) {
			if(nvGraphicFramePr.getCNvGraphicFramePr()==null&&createIfMissing) {
				nvGraphicFramePr.setCNvGraphicFramePr(new CTNonVisualGraphicFrameProperties());
			}
			return nvGraphicFramePr.getCNvGraphicFramePr();
		}
		return null;
	}

	@Override
	public List<Object> getContent() {
	    if(graphic!=null&&graphic.getGraphicData()!=null) {
	    	final GraphicData graphicData = graphic.getGraphicData();
	    	final String uri = graphicData.getUri();
	        final List<Object> any = graphicData.getAny();
	    	if(uri!=null&&!any.isEmpty()) {
	            if(uri.equalsIgnoreCase("http://schemas.openxmlformats.org/drawingml/2006/table")) {
	            	Object o = any.get(0);
	                if(o instanceof JAXBElement && ((JAXBElement<?>)o).getDeclaredType().getName().equals("org.docx4j.dml.CTTable") ) {
	                	o = ((JAXBElement<?>)o).getValue();
	                	any.set(0, o);
	                }
	                if(o instanceof CTTable) {
	                	return ((CTTable)o).getContent();
	                }
	            }
	    	}
	    }
		return null;
	}

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     * 
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
