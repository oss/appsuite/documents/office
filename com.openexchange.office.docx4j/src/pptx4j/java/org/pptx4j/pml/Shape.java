/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *   
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.pptx4j.pml;

import java.util.List;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualDrawingShapeProps;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTShapeStyle;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.IShape;
import org.docx4j.dml.ITransform2DAccessor;
import org.docx4j.dml.ObjectFactory;
import org.docx4j.jaxb.Context;
import com.openexchange.office.filter.core.IContentAccessor;

/**
 * <p>Java class for CT_Shape complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Shape">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvSpPr">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps"/>
 *                   &lt;element name="cNvSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingShapeProps"/>
 *                   &lt;element name="nvPr" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ApplicationNonVisualDrawingProps"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="spPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeProperties"/>
 *         &lt;element name="style" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeStyle" minOccurs="0"/>
 *         &lt;element name="txBody" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TextBody" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ExtensionListModify" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="useBgFill" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Shape", propOrder = {
    "nvSpPr",
    "spPr",
    "style",
    "txBody",
    "extLst"
})
@XmlRootElement(name="sp")
public class Shape implements ITransform2DAccessor, IShape, INvPrAccessor, IContentAccessor<Object> {

    @XmlElement(required = true)
    protected Shape.NvSpPr nvSpPr;
    @XmlElement(required = true)
    protected CTShapeProperties spPr;
    protected CTShapeStyle style;
    protected CTTextBody txBody;
    protected CTExtensionListModify extLst;
    @XmlAttribute(name = "useBgFill")
    protected Boolean useBgFill;

    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	return spPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        spPr.removeXfrm();
    }

	@Override
	public NvPr getNvPr(boolean forceCreate) {
		if(nvSpPr.nvPr==null&&forceCreate) {
			nvSpPr.nvPr = new NvPr();
		}
		return nvSpPr.nvPr;
	}

    /**
     * Gets the value of the nvSpPr property.
     * 
     * @return
     *     possible object is
     *     {@link Shape.NvSpPr }
     *     
     */
    public Shape.NvSpPr getNvSpPr() {
    	return nvSpPr;
    }

    /**
     * Sets the value of the nvSpPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shape.NvSpPr }
     *     
     */
    public void setNvSpPr(Shape.NvSpPr value) {
        this.nvSpPr = value;
    }

    /**
     * Gets the value of the spPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public CTShapeProperties getSpPr() {
        return spPr;
    }

    /**
     * Sets the value of the spPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public void setSpPr(CTShapeProperties value) {
        this.spPr = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public CTShapeStyle getStyle(boolean forceCreate) {
    	if(style==null&&forceCreate) {
    		style = new CTShapeStyle();
    	}
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public void setStyle(CTShapeStyle value) {
        this.style = value;
    }

    /**
     * Gets the value of the txBody property.
     * 
     * @return
     *     possible object is
     *     {@link CTTextBody }
     *     
     */
    public CTTextBody getTxBody(boolean forceCreate) {
    	if(txBody==null&&forceCreate)  {
    		txBody = new CTTextBody();
    	}
        return txBody;
    }

    /**
     * Sets the value of the txBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTextBody }
     *     
     */
    public void setTxBody(CTTextBody value) {
        this.txBody = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionListModify }
     *     
     */
    public CTExtensionListModify getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionListModify }
     *     
     */
    public void setExtLst(CTExtensionListModify value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the useBgFill property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseBgFill() {
        if (useBgFill == null) {
            return false;
        }
        return useBgFill;
    }

    /**
     * Sets the value of the useBgFill property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseBgFill(Boolean value) {
        this.useBgFill = value;
    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps"/>
     *         &lt;element name="cNvSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingShapeProps"/>
     *         &lt;element name="nvPr" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ApplicationNonVisualDrawingProps"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cNvPr",
        "cNvSpPr",
        "nvPr"
    })
    public static class NvSpPr {

        @XmlElement(required = true)
        protected CTNonVisualDrawingProps cNvPr;
        @XmlElement(required = true)
        protected CTNonVisualDrawingShapeProps cNvSpPr;
        @XmlElement(required = true)
        protected NvPr nvPr;

        /**
         * Gets the value of the cNvPr property.
         * 
         * @return
         *     possible object is
         *     {@link CTNonVisualDrawingProps }
         *     
         */
        public CTNonVisualDrawingProps getCNvPr() {
            return cNvPr;
        }

        /**
         * Sets the value of the cNvPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link CTNonVisualDrawingProps }
         *     
         */
        public void setCNvPr(CTNonVisualDrawingProps value) {
            this.cNvPr = value;
        }

        /**
         * Gets the value of the cNvSpPr property.
         * 
         * @return
         *     possible object is
         *     {@link CTNonVisualDrawingShapeProps }
         *     
         */
        public CTNonVisualDrawingShapeProps getCNvSpPr() {
            return cNvSpPr;
        }

        /**
         * Sets the value of the cNvSpPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link CTNonVisualDrawingShapeProps }
         *     
         */
        public void setCNvSpPr(CTNonVisualDrawingShapeProps value) {
            this.cNvSpPr = value;
        }

        /**
         * Gets the value of the nvPr property.
         * 
         * @return
         *     possible object is
         *     {@link NvPr }
         *     
         */
        public NvPr getNvPr() {
            return nvPr;
        }

        /**
         * Sets the value of the nvPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link NvPr }
         *     
         */
        public void setNvPr(NvPr value) {
            this.nvPr = value;
        }
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvSpPr==null&&createIfMissing) {
			nvSpPr = new NvSpPr();
		}
		if(nvSpPr!=null) {
			if(nvSpPr.getCNvPr()==null&&createIfMissing) {
				nvSpPr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvSpPr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualDrawingShapeProps getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvSpPr==null&&createIfMissing) {
			nvSpPr = new NvSpPr();
		}
		if(nvSpPr!=null) {
			if(nvSpPr.getCNvSpPr()==null&&createIfMissing) {
				nvSpPr.setCNvSpPr(new CTNonVisualDrawingShapeProps());
			}
			return nvSpPr.getCNvSpPr();
		}
		return null;
	}

    @Override
	public List<Object> getContent() {
    	if(txBody==null) {
    		final ObjectFactory objectFactory = Context.getDmlObjectFactory();
    		txBody = objectFactory.createCTTextBody();
    		txBody.setBodyPr(objectFactory.createCTTextBodyProperties());
    		txBody.setLstStyle(objectFactory.createCTTextListStyle());
    	}
		return txBody.getContent();
	}

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
    	if(txBody!=null) {
	    	if(txBody.getContent().isEmpty()) {
				txBody = null;
	    	}
	    	else if(txBody.getBodyPr()==null) {
	    		txBody.setBodyPr(Context.getDmlObjectFactory().createCTTextBodyProperties());
	    	}
    	}
    }

	@Override
	public boolean supportsTextBody() {
		return true;
	}

	@Override
	public CTTextBodyProperties getTextBodyProperties(boolean forceCreate) {
		if(txBody!=null && txBody.getBodyPr()!=null) {
			return txBody.getBodyPr();
		}
		else if (forceCreate) {
			if(txBody==null) {
				txBody = Context.getDmlObjectFactory().createCTTextBody();
			}
			final CTTextBodyProperties txBodyPr = Context.getDmlObjectFactory().createCTTextBodyProperties();
			txBody.setBodyPr(txBodyPr);
		}
		return txBody!=null ? txBody.getBodyPr() : null;
	}

	@Override
    public void setTextBody(CTTextBodyProperties textBodyProperties) {
		if(txBody==null) {
			txBody = Context.getDmlObjectFactory().createCTTextBody();
		}
		txBody.setBodyPr(textBodyProperties);
 	}

    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }

    public boolean isWordArt() {
        return txBody != null && txBody.isWordArt();
    }
}
