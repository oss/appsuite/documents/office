/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.pptx4j.pml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;

/**
 * <p>Java class for CT_CommentAuthorList complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_CommentAuthorList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cmAuthor" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_CommentAuthor" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentAuthorList", propOrder = {
    "content"
})
@XmlRootElement(name = "cmAuthorLst")
public class CTCommentAuthorList implements ICommentAuthorList<CTCommentAuthor> {

    @XmlElementRefs({
        @XmlElementRef(name = "cmAuthor", namespace = "http://schemas.openxmlformats.org/presentationml/2006/main", type = CTCommentAuthor.class),
    })
    protected DLList<CTCommentAuthor> content;

    @Override
    public DLList<CTCommentAuthor> getContent() {
        if (content== null) {
            content = new DLList<CTCommentAuthor>();
        }
        return content;
    }

    public long getNextIdx() {
        long nextIdx = 1;
        DLNode<CTCommentAuthor> commentAuthorNode = getContent().getFirstNode();
        while (commentAuthorNode!=null) {
            long lastIdx = commentAuthorNode.getData().getLastIdx();
            if(lastIdx>=nextIdx) {
                nextIdx = lastIdx+1;
            }
            commentAuthorNode = commentAuthorNode.getNext();
        }
        return nextIdx;
    }

    @Override
    public CTCommentAuthor createCommentAuthor() {
        final CTCommentAuthor author = new CTCommentAuthor();
        getContent().add(author);
        return author;
    }
}
