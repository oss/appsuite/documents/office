

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.pptx4j.pml;

public interface INvPrAccessor {

	public NvPr getNvPr(boolean forceCreate);
}
