
package org.pptx4j.pml;

public interface ICommonMediaNodeDataAccessor {

    /**
     * Gets the value of the cMediaNode property.
     * 
     * @return
     *     possible object is
     *     {@link CTTLCommonMediaNodeData }
     *     
     */
    public CTTLCommonMediaNodeData getCMediaNode();

    /**
     * Sets the value of the cMediaNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTLCommonMediaNodeData }
     *     
     */
    public void setCMediaNode(CTTLCommonMediaNodeData value);
}
