/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.pptx4j.pml;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.pptx4j.pml_2012.CTPresenceInfo;


/**
 * <p>Java class for CT_CommentAuthor complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_CommentAuthor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/presentationml/2006/main}ST_Name" />
 *       &lt;attribute name="initials" use="required" type="{http://schemas.openxmlformats.org/presentationml/2006/main}ST_Name" />
 *       &lt;attribute name="lastIdx" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="clrIdx" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentAuthor", propOrder = {
    "extLst"
})
@XmlRootElement(name = "cmAuthor")
public class CTCommentAuthor implements ICommentAuthor {

    protected CTExtensionList extLst;
    @XmlAttribute(name = "id", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "initials", required = true)
    protected String initials;
    @XmlAttribute(name = "lastIdx", required = true)                    // last used comment index
    @XmlSchemaType(name = "unsignedInt")
    protected long lastIdx;
    @XmlAttribute(name = "clrIdx", required = true)                     // last used color index
    @XmlSchemaType(name = "unsignedInt")
    protected long clrIdx;

    public CTPresenceInfo getPresenceInfo(boolean forceCreate) {

        final CTExtensionList lst = getExtLst(forceCreate);
        if(lst!=null) {
            final CTExtension ext = lst.getExtensionByUri(CT_PRESENCE_INFO, forceCreate);
            if(ext!=null) {
                Object o = ext.getAny();
                if(o instanceof JAXBElement) {
                    // unmarshalling
                    o = ((JAXBElement<?>)o).getValue();
                }
                if(!(o instanceof CTPresenceInfo)) {
                    o = new CTPresenceInfo();
                }
                ext.setAny(o);
                return (CTPresenceInfo)o;
            }
        }
        return null;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    public CTExtensionList getExtLst(boolean forceCreate) {
        if(extLst==null && forceCreate) {
            extLst = new CTExtensionList();
        }
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the id property.
     *
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     */
    @Override
    public void setId(long value) {
        id = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Override
    public void setName(String value) {
        name = value;
    }

    /**
     * Gets the value of the initials property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Override
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Override
    public void setInitials(String value) {
        initials = value;
    }

    /**
     * Gets the value of the lastIdx property.
     *
     */
    public long getLastIdx() {
        return lastIdx;
    }

    /**
     * Sets the value of the lastIdx property.
     *
     */
    public void setLastIdx(long value) {
        lastIdx = value;
    }

    /**
     * Gets the value of the clrIdx property.
     *
     */
    public long getClrIdx() {
        return clrIdx;
    }

    /**
     * Sets the value of the clrIdx property.
     *
     */
    public void setClrIdx(long value) {
        clrIdx = value;
    }

    @Override
    public String getUserId() {
        final CTPresenceInfo presenceInfo = getPresenceInfo(false);
        return presenceInfo!=null ? presenceInfo.getUserId() : null;
    }

    @Override
    public void setUserId(String value) {
        getPresenceInfo(true).setUserId(value);
    }

    @Override
    public String getProviderId() {
        final CTPresenceInfo presenceInfo = getPresenceInfo(false);
        return presenceInfo!=null ? presenceInfo.getProviderId() : null;
    }

    @Override
    public void setProviderId(String value) {
        getPresenceInfo(true).setProviderId(value);
    }

    final static String CT_PRESENCE_INFO = "{19B8F6BF-5375-455C-9EA6-DF929625EA0E}";
}
