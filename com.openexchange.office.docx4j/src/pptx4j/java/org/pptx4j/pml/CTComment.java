/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.pptx4j.pml;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.docx4j.dml.CTPoint2D;
import org.pptx4j.pml_2012.CTCommentThreading;

/**
 * <p>Java class for CT_Comment complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Comment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pos" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Point2D"/>
 *         &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ExtensionListModify" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="authorId" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="dt" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="idx" use="required" type="{http://schemas.openxmlformats.org/presentationml/2006/main}ST_Index" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Comment", propOrder = {
    "pos",
    "text",
    "extLst"
})
@XmlRootElement(name="cm")
public class CTComment implements IComment {

    @XmlElement(required = true)
    protected CTPoint2D pos;
    @XmlElement(required = true)
    protected String text;
    protected CTExtensionListModify extLst;
    @XmlAttribute(name = "authorId", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long authorId;
    @XmlAttribute(name = "dt")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dt;
    @XmlAttribute(name = "idx", required = true)
    protected long idx;

    /**
     * Gets the value of the pos property.
     *
     * @return
     *     possible object is
     *     {@link CTPoint2D }
     *
     */
    @Override
    public CTPoint2D getPos(boolean forceCreate) {
        if(pos == null && forceCreate) {
            pos = new CTPoint2D();
        }
        return pos != null ? new CTPoint2D(Double.valueOf(((pos.getX() * 2540.0) / 576.0) + 0.5).longValue(), Double.valueOf(((pos.getY() * 2540.0) / 576.0) + 0.5).longValue()) : null;
    }

    /**
     * Sets the value of the pos property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPoint2D }
     *
     */
    @Override
    public void setPos(CTPoint2D value) {
        if(value == null) {
            pos = null;
        }
        else {
            pos = new CTPoint2D((long)(((value.getX() / 2540.0) * 576.0) + 0.5), (long)(((value.getY() / 2540.0) * 576.0) + 0.5));
        }
    }

    /**
     * Gets the value of the text property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionListModify }
     *
     */
    @Override
    public CTExtensionListModify getExtLst(boolean forceCreate) {
        if(extLst==null&&forceCreate) {
            extLst = new CTExtensionListModify();
        }
        return extLst;
    }

    /**
     * Gets the value of the authorId property.
     *
     */
    @Override
    public long getAuthorId() {
        return authorId;
    }

    /**
     * Sets the value of the authorId property.
     *
     */
    @Override
    public void setAuthorId(long value) {
        this.authorId = value;
    }

    /**
     * Gets the value of the dt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Override
    public XMLGregorianCalendar getDt() {
        return dt;
    }

    /**
     * Sets the value of the dt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Override
    public void setDt(XMLGregorianCalendar value) {
        this.dt = value;
    }

    /**
     * Gets the value of the idx property.
     *
     */
    @Override
    public long getIdx() {
        return idx;
    }

    /**
     * Sets the value of the idx property.
     *
     */
    @Override
    public void setIdx(long value) {
        this.idx = value;
    }

    private static String commentThreadingUri = "{C676402C-5697-4E1C-873F-D02D1690AC5C}";

    public CTCommentThreading getCommentThreading(boolean forceCreate) {
        final CTExtensionListModify extLstMod = getExtLst(forceCreate);
        if(extLstMod!=null) {
            CTCommentThreading commentThreading = null;
            for(CTExtension extension:extLstMod.getExt()) {
                if(commentThreadingUri.equals(extension.getUri())) {
                    final Object any = extension.getAny();
                    if(any instanceof JAXBElement) {
                        commentThreading = ((JAXBElement<CTCommentThreading>)extension.getAny()).getValue();
                        extension.setAny(commentThreading);
                    }
                    else if(any instanceof CTCommentThreading) {
                        commentThreading = (CTCommentThreading)any;
                    }
                    break;
                }
            }
            if(commentThreading==null && forceCreate) {
                final CTExtension extension = new CTExtension();
                extension.setUri(commentThreadingUri);
                extLstMod.getExt().add(extension);
                commentThreading = new CTCommentThreading();
                commentThreading.setTimeZoneBias(0);
                extension.setAny(commentThreading);
            }
            return commentThreading;

        }
        return null;
    }
}
