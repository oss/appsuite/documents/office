/*
 *  Copyright 2010-2012, Plutext Pty Ltd.
 *   
 *  This file is part of pptx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.pptx4j.pml;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualConnectorProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTShapeStyle;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.IShape;
import org.docx4j.dml.ITransform2DAccessor;

/**
 * <p>Java class for CT_Connector complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Connector">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvCxnSpPr">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps"/>
 *                   &lt;element name="cNvCxnSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualConnectorProperties"/>
 *                   &lt;element name="nvPr" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ApplicationNonVisualDrawingProps"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="spPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeProperties"/>
 *         &lt;element name="style" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeStyle" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ExtensionListModify" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Connector", propOrder = {
    "nvCxnSpPr",
    "spPr",
    "style",
    "extLst"
})
public class CxnSp implements IShape, ITransform2DAccessor, INvPrAccessor {

    @XmlElement(required = true)
    protected CxnSp.NvCxnSpPr nvCxnSpPr;
    @XmlElement(required = true)
    protected CTShapeProperties spPr;
    protected CTShapeStyle style;
    protected CTExtensionListModify extLst;

    @Override
    public NvPr getNvPr(boolean forceCreate) {
        if(nvCxnSpPr.nvPr==null&&forceCreate) {
            nvCxnSpPr.nvPr = new NvPr();
        }
        return nvCxnSpPr.nvPr;
    }

    /**
     * Gets the value of the xfrm property.
     *
     * @return
     *     possible object is
     *     {@link CTTransform2D }
     *
     */
    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
        return spPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        spPr.removeXfrm();
    }

    /**
     * Gets the value of the nvCxnSpPr property.
     * 
     * @return
     *     possible object is
     *     {@link CxnSp.NvCxnSpPr }
     *     
     */
    public CxnSp.NvCxnSpPr getNvCxnSpPr() {
        return nvCxnSpPr;
    }

    /**
     * Sets the value of the nvCxnSpPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CxnSp.NvCxnSpPr }
     *     
     */
    public void setNvCxnSpPr(CxnSp.NvCxnSpPr value) {
        this.nvCxnSpPr = value;
    }

    /**
     * Gets the value of the spPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public CTShapeProperties getSpPr() {
        return spPr;
    }

    /**
     * Sets the value of the spPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public void setSpPr(CTShapeProperties value) {
        this.spPr = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public CTShapeStyle getStyle(boolean forceCreate) {
    	if(style==null&&forceCreate) {
    		style = new CTShapeStyle();
    	}
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public void setStyle(CTShapeStyle value) {
        this.style = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionListModify }
     *     
     */
    public CTExtensionListModify getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionListModify }
     *     
     */
    public void setExtLst(CTExtensionListModify value) {
        this.extLst = value;
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvCxnSpPr==null&&createIfMissing) {
			nvCxnSpPr = new NvCxnSpPr();
		}
		if(nvCxnSpPr!=null) {
			if(nvCxnSpPr.getCNvPr()==null&&createIfMissing) {
				nvCxnSpPr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvCxnSpPr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualConnectorProperties getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvCxnSpPr==null&&createIfMissing) {
			nvCxnSpPr = new NvCxnSpPr();
		}
		if(nvCxnSpPr!=null) {
			if(nvCxnSpPr.getCNvCxnSpPr()==null&&createIfMissing) {
				nvCxnSpPr.setCNvCxnSpPr(new CTNonVisualConnectorProperties());
			}
			return nvCxnSpPr.getCNvCxnSpPr();
		}
		return null;
	}

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps"/>
     *         &lt;element name="cNvCxnSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualConnectorProperties"/>
     *         &lt;element name="nvPr" type="{http://schemas.openxmlformats.org/presentationml/2006/main}CT_ApplicationNonVisualDrawingProps"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cNvPr",
        "cNvCxnSpPr",
        "nvPr"
    })
    public static class NvCxnSpPr {

        @XmlElement(required = true)
        protected CTNonVisualDrawingProps cNvPr;
        @XmlElement(required = true)
        protected CTNonVisualConnectorProperties cNvCxnSpPr;
        @XmlElement(required = true)
        protected NvPr nvPr;

        /**
         * Gets the value of the cNvPr property.
         * 
         * @return
         *     possible object is
         *     {@link CTNonVisualDrawingProps }
         *     
         */
        public CTNonVisualDrawingProps getCNvPr() {
            return cNvPr;
        }

        /**
         * Sets the value of the cNvPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link CTNonVisualDrawingProps }
         *     
         */
        public void setCNvPr(CTNonVisualDrawingProps value) {
            this.cNvPr = value;
        }

        /**
         * Gets the value of the cNvCxnSpPr property.
         * 
         * @return
         *     possible object is
         *     {@link CTNonVisualConnectorProperties }
         *     
         */
        public CTNonVisualConnectorProperties getCNvCxnSpPr() {
            return cNvCxnSpPr;
        }

        /**
         * Sets the value of the cNvCxnSpPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link CTNonVisualConnectorProperties }
         *     
         */
        public void setCNvCxnSpPr(CTNonVisualConnectorProperties value) {
            this.cNvCxnSpPr = value;
        }

        /**
         * Gets the value of the nvPr property.
         * 
         * @return
         *     possible object is
         *     {@link NvPr }
         *     
         */
        public NvPr getNvPr() {
            return nvPr;
        }

        /**
         * Sets the value of the nvPr property.
         * 
         * @param value
         *     allowed object is
         *     {@link NvPr }
         *     
         */
        public void setNvPr(NvPr value) {
            this.nvPr = value;
        }
    }

	@Override
	public boolean supportsTextBody() {
		return false;
	}

	@Override
	public CTTextBodyProperties getTextBodyProperties(boolean forceCreate) {
		return null;
	}

	@Override
	public void setTextBody(CTTextBodyProperties textBodyProperties) {
	    //
	}

    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
