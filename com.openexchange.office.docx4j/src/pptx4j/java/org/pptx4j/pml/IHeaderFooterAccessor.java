

/**
 *
 * @author sven.jacobi@open-xchange.com
 */
package org.pptx4j.pml;

public interface IHeaderFooterAccessor {

    public CTHeaderFooter getHf(boolean forceCreate);

    public void setHf(CTHeaderFooter value);
}
