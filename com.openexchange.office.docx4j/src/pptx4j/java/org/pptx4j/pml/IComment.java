/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.pptx4j.pml;

import javax.xml.datatype.XMLGregorianCalendar;
import org.docx4j.dml.CTPoint2D;

public interface IComment {

    // returns the position of the target in 1/100thmm, the returned point
    // is no live object, it has to be set again via setPos when changed
    public CTPoint2D getPos(boolean forceCreate);

    public void setPos(CTPoint2D value);

    public IExtensionList getExtLst(boolean forceCreate);

    public long getAuthorId();

    public void setAuthorId(long value);

    public XMLGregorianCalendar getDt();

    public void setDt(XMLGregorianCalendar value);

    public long getIdx();

    public void setIdx(long value);
}
