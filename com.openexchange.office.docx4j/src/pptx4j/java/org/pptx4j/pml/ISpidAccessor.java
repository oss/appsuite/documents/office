
package org.pptx4j.pml;

public interface ISpidAccessor {

    /**
     * Gets the value of the spid property.
     * 
     */
    public String getSpid();

    /**
     * Sets the value of the spid property.
     * 
     */
    public void setSpid(String value);
}
