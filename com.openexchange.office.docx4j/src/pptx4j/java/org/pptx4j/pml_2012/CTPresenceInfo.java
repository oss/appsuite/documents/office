
package org.pptx4j.pml_2012;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 *
Java class for CT_PresenceInfo complex type.
 *
 *

The following schema fragment specifies the expected content contained within this class.
 *
 *

 * <complexType name="CT_PresenceInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       <attribute name="providerId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     </restriction>
 *   </complexContent>
 * </complexType>
 *

 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PresenceInfo")
@XmlRootElement(name="presenceInfo")
public class CTPresenceInfo
{
    // default required for jaxb
    public CTPresenceInfo() {
        this.userId = "";
        this.providerId = "";
    }

    public CTPresenceInfo(String userId, String providerId) {
        this.userId = userId;
        this.providerId = providerId;
    }

    @XmlAttribute(name = "userId", required = true)
    protected String userId;
    @XmlAttribute(name = "providerId", required = true)
    protected String providerId;

    /**
     * Gets the value of the userId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the providerId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     * Sets the value of the providerId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProviderId(String value) {
        this.providerId = value;
    }
}