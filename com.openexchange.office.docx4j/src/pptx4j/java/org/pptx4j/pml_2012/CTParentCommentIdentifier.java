
package org.pptx4j.pml_2012;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;

/*
<xsd:complexType name="CT_ParentCommentIdentifier">
   <xsd:attribute name="authorId" type="xsd:unsignedInt"/>
   <xsd:attribute name="idx" type="xsd:unsignedInt"/>
 </xsd:complexType>
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ParentCommentIdentifier", propOrder = {
    "authorId",
    "idx"
})
public class CTParentCommentIdentifier {

    @XmlAttribute
    @XmlSchemaType(name = "unsignedInt")
    protected Long authorId;
    @XmlAttribute
    @XmlSchemaType(name = "unsignedInt")
    protected Long idx;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }
}
