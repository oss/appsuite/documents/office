
package org.pptx4j.pml_2012;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

    private final static QName _presenceInfo_QNAME = new QName("http://schemas.microsoft.com/office/powerpoint/2012/main", "presenceInfo");
    private final static QName _threadingInfo_QNAME = new QName("http://schemas.microsoft.com/office/powerpoint/2012/main", "threadingInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.pptx4j.pml_2012;
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTPresenceInfo }
     *
     */
    public CTPresenceInfo createCTPresenceInfo() {
        return new CTPresenceInfo();
    }

    /**
     * Create an instance of {@link CTCommentThreading }
     *
     */
    public CTCommentThreading createCTThreadingInfo() {
        return new CTCommentThreading();
    }

    /**
     * Create an instance of {@link CTParentCommentIdentifier }
     *
     */
    public CTParentCommentIdentifier createCTParentCommentIdentifier() {
        return new CTParentCommentIdentifier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWorksheet }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/powerpoint/2012/main", name = "presenceInfo")
    public JAXBElement<CTPresenceInfo> createMacrosheet(CTPresenceInfo value) {
        return new JAXBElement<CTPresenceInfo>(_presenceInfo_QNAME, CTPresenceInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWorksheet }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/powerpoint/2012/main", name = "threadingInfo")
    public JAXBElement<CTCommentThreading> createMacrosheet(CTCommentThreading value) {
        return new JAXBElement<CTCommentThreading>(_threadingInfo_QNAME, CTCommentThreading.class, null, value);
    }
}
