
package org.pptx4j.pml_2012;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/*
<XSD:complexType Name = "CT_CommentThreading" >
    <XSD: Sequence >
        <XSD: Element Name = "ParentCm" Type = "CT_ParentCommentIdentifier" MinOccurs = "0" MaxOccurs = "1" / >
    </XSD:Sequence >
    <XSD: Attribute Name = "TimeZoneBias" Type = "xsd: int" / >
</XSD:complexType >
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentThreading", propOrder = {
    "parentCm"
})
@XmlRootElement(name="threadingInfo")
public class CTCommentThreading {

    @XmlElement(required = true)
    protected CTParentCommentIdentifier parentCm;
    @XmlAttribute
    protected Integer timeZoneBias;

    public CTParentCommentIdentifier getParentCm(boolean forceCreate) {
        if(parentCm==null&&forceCreate) {
            parentCm = new CTParentCommentIdentifier();
        }
        return parentCm;
    }

    public void setParentCm(CTParentCommentIdentifier parentCm) {
        this.parentCm = parentCm;
    }

    public Integer getTimeZoneBias() {
        return timeZoneBias;
    }

    public void setTimeZoneBias(Integer timeZoneBias) {
        this.timeZoneBias = timeZoneBias;
    }
}
