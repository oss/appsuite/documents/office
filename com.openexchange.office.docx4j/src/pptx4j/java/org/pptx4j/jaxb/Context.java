/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.pptx4j.jaxb;

import jakarta.xml.bind.JAXBContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Context {

    private static Thread jcThread;
    private static boolean jcThreadJoined = false;

	/*
	 * Two reasons for having a separate class for this:
	 * 1. so that loading PML context does not slow
	 *    down docx4j operation on docx files
	 * 2. to try to maintain clean delineation between
	 *    docx4j and pptx4j
	 */

	private static JAXBContext jcPML;

	private static Logger log = LoggerFactory.getLogger(Context.class);

	static {
        jcThread = new Thread(
            new Runnable() {
                @Override
                public void run() {
                    try {
						java.lang.ClassLoader classLoader = Context.class.getClassLoader();

						jcPML = JAXBContext.newInstance("org.pptx4j.pml:" +
								"org.docx4j.dml:org.docx4j.dml_2013_command:org.docx4j.dml.chart:org.docx4j.dml.chartDrawing:org.docx4j.dml.compatibility:org.docx4j.dml.diagram:org.docx4j.dml.lockedCanvas:org.docx4j.dml.picture:org.docx4j.dml.wordprocessingDrawing:org.docx4j.dml.spreadsheetDrawing:" +
								"org.docx4j.mce:org.pptx4j.pml_2012:org.pptx4j.pml_2013:org.pptx4j.pml_2018_8",
								classLoader );

						if (jcPML.getClass().getName().equals("org.eclipse.persistence.jaxb.JAXBContext")) {
							log.debug("MOXy JAXB implementation is in use!");
						} else {
							log.debug("Not using MOXy.");
						}
					} catch (Exception ex) {
						log.error("Cannot initialize context", ex);
					}
                }
            }
        );
        jcThread.start();
	}

    /*
     * the jcThread is joined, additional it is checked if it should
     * aborted because of low memory
     *
     */
    private static final void initializeContexts() {
        if(!jcThreadJoined) {
            try {
                jcThread.join();
            } catch (InterruptedException e) {
                log.info("PPTX4J, could not get Context");
            }
            jcThreadJoined = true;
        }
    }

	public static org.pptx4j.pml.ObjectFactory pmlObjectFactory;
	public static org.pptx4j.pml.ObjectFactory getpmlObjectFactory() {
		if (pmlObjectFactory==null) {
			pmlObjectFactory = new org.pptx4j.pml.ObjectFactory();
		}
		return pmlObjectFactory;
	}

    public static final JAXBContext getJcPml() {
    	initializeContexts();
    	return jcPML;
    }
}
