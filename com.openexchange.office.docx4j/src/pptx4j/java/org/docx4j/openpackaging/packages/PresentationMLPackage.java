/*
 *  Copyright 2010, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.openpackaging.packages;

import jakarta.xml.bind.JAXBException;
import org.docx4j.Docx4jProperties;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsCustomPart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.NotesSlidePart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.pptx4j.model.SlideSizesWellKnown;

/**
 * @author jharrop
 *
 */
public class PresentationMLPackage  extends OpcPackage {

	/**
	 * Constructor.  Also creates a new content type manager
	 * 
	 */	
	public PresentationMLPackage() {
		super();
		setContentType(new ContentType(ContentTypes.PRESENTATIONML_MAIN)); 		
	}
	/**
	 * Constructor.
	 *  
	 * @param contentTypeManager
	 *            The content type manager to use 
	 */
	public PresentationMLPackage(ContentTypeManager contentTypeManager) {
		super(contentTypeManager);
		setContentType(new ContentType(ContentTypes.PRESENTATIONML_MAIN));
	}

	@Override
    public boolean setPartShortcut(Part part, String relationshipType) {
		if (relationshipType.equals(Namespaces.PROPERTIES_CORE)) {
			docPropsCorePart = (DocPropsCorePart)part;
			log.info("Set shortcut for docPropsCorePart");
			return true;			
		} else if (relationshipType.equals(Namespaces.PROPERTIES_EXTENDED)) {
			docPropsExtendedPart = (DocPropsExtendedPart)part;
			log.info("Set shortcut for docPropsExtendedPart");
			return true;			
		} else if (relationshipType.equals(Namespaces.PROPERTIES_CUSTOM)) {
			docPropsCustomPart = (DocPropsCustomPart)part;
			log.info("Set shortcut for docPropsCustomPart");
			return true;			
		} else if (relationshipType.equals(Namespaces.PRESENTATIONML_MAIN)) {
			mainPresentationPart = (MainPresentationPart)part;
			log.info("Set shortcut for mainPresentationPart");
			return true;			
		} else {	
			return false;
		}
	}
	
	private MainPresentationPart mainPresentationPart;	
	public MainPresentationPart getMainPresentationPart() {
		return mainPresentationPart;
	}
	
	/**
	 * Create an empty presentation.
	 * 
	 * @return
	 * @throws InvalidFormatException
	 */
	public static PresentationMLPackage createPackage() throws InvalidFormatException {
		
		String slideSize= Docx4jProperties.getProperties().getProperty("pptx4j.PageSize", "A4");
		log.info("Using paper size: " + slideSize);
		
		boolean landscape= Docx4jProperties.getProperty("pptx4j.PageOrientationLandscape", true);
		log.info("Landscape orientation: " + landscape);
		
		return createPackage(
				SlideSizesWellKnown.valueOf(slideSize), landscape); 
		
	}
	
	/**
	 * Create an empty presentation.
	 * 
	 * @return
	 * @throws InvalidFormatException
	 * @since 2.7
	 */
	public static PresentationMLPackage createPackage(SlideSizesWellKnown sz, boolean landscape) throws InvalidFormatException {
		
		
		// Create a package
		PresentationMLPackage pmlPack = new PresentationMLPackage();

		// Presentation part
		MainPresentationPart pp;
		try {
			pp = new MainPresentationPart();
			pp.setJaxbElement(MainPresentationPart.createJaxbPresentationElement(pmlPack, sz, landscape) );
			pmlPack.addTargetPart(pp);
			
//			// Slide part
//			SlidePart slidePart = new SlidePart();
//			pp.addSlideIdListEntry(slidePart);
//
//			slidePart.setJaxbElement( SlidePart.createSld() );
			
			// Slide layout part
			SlideLayoutPart layoutPart = new SlideLayoutPart();
			layoutPart.setJaxbElement(SlideLayoutPart.createSldLayout(pmlPack));

//			slidePart.addTargetPart(layoutPart);

			// Slide Master part
			SlideMasterPart masterPart = new SlideMasterPart();
			pp.addSlideMasterIdListEntry(masterPart);

			masterPart.setJaxbElement(SlideMasterPart.createSldMaster(pmlPack));
			masterPart.addSlideLayoutIdListEntry(layoutPart);

			layoutPart.addTargetPart(masterPart);

			// Theme part
			ThemePart themePart = new ThemePart(new PartName("/ppt/theme/theme1.xml"));
			java.io.InputStream is = org.docx4j.utils.ResourceUtils.getResource(
						"org/docx4j/openpackaging/parts/PresentationML/theme.xml");
			themePart.unmarshal(is);

			// .. add it in 2 places ..
			masterPart.addTargetPart(themePart);
			pp.addTargetPart(themePart);

		} catch (Exception e) {
			throw new InvalidFormatException("Couldn't create package", e);
		}
		return pmlPack;
	}

	/**
	 * Create a notes slide and add it to slide relationships
	 * 
	 * @param sourcePart
	 * @param partName
	 * @return the notes slide
	 * @throws InvalidFormatException
	 * @throws JAXBException
	 */
	public static NotesSlidePart createNotesSlidePart(Part sourcePart, PartName partName) throws Exception {

        String proposedRelId = sourcePart.getRelationshipsPart().getNextId();

        NotesSlidePart notesSlidePart = new NotesSlidePart(partName);

        notesSlidePart.getSourceRelationships().add(sourcePart.addTargetPart(notesSlidePart, proposedRelId));
        notesSlidePart.setJaxbElement(NotesSlidePart.createNotes(sourcePart.getPackage()));
        return notesSlidePart;

    }
}
