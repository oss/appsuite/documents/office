
package org.docx4j.openpackaging.parts.PresentationML;

import org.pptx4j.pml.CommonSlideData;

public interface ICommonSlideAccessor {

	public CommonSlideData getCSld();
}
