/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts.PresentationML;

import java.io.InputStream;
import java.util.List;
import jakarta.xml.bind.JAXBException;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.pptx4j.jaxb.Context;
import org.pptx4j.pml.CTCommentList;
import org.pptx4j.pml.CTSlideTiming;
import org.pptx4j.pml.CommonSlideData;
import org.pptx4j.pml.ObjectFactory;
import org.pptx4j.pml.Sld;
import com.openexchange.office.filter.core.IContentAccessor;

public final class SlidePart extends JaxbPmlPart<Sld> implements IContentAccessor<Object>, ICommonSlideAccessor, ICommonTimingAccessor {

	public SlidePart(PartName partName) {
		super(partName);
		init();
	}

	public SlidePart() throws InvalidFormatException {
		super(new PartName("/ppt/slides/slide1.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_SLIDE));

		// Used when this Part is added to a rels
		setRelationshipType(Namespaces.PRESENTATIONML_SLIDE);

	}

	@Override
	public List<Object> getContent() {
		final Sld slide = this.getJaxbElement();
		if(slide.getCSld()==null) {
			slide.setCSld(Context.getpmlObjectFactory().createCommonSlideData());
		}
		return slide.getCSld().getContent();
	}

	@Override
	public CommonSlideData getCSld() {
		return getJaxbElement().getCSld();
	}

	@Override
	public CTSlideTiming getTiming() {
		return getJaxbElement().getTiming();
	}

	@Override
	public void setTiming(CTSlideTiming value) {
		getJaxbElement().setTiming(value);
	}

	public static Sld createSld(OpcPackage opcPackage) throws JAXBException {

		ObjectFactory factory = Context.getpmlObjectFactory();
		Sld sld = factory.createSld();
		sld.setCSld((CommonSlideData)XmlUtils.unmarshalString(COMMON_SLIDE_DATA, opcPackage, Context.getJcPml(), CommonSlideData.class));
		return sld;
	}

    NotesSlidePart notes;

	public boolean setPartShortcut(Part part) {

		if (part == null ){
			return false;
		}
        return setPartShortcut(part, part.getRelationshipType() );
	}

	@Override
    public boolean setPartShortcut(Part part, String relationshipType) {

		// Since each part knows its relationshipsType,
		// why is this passed in as an arg?
		// Answer: where the relationshipType is ascertained
		// from the rel itself, it is the most authoritative.
		// Note that we normally use the info in [Content_Types]
		// to create a part of the correct type.  This info
		// will not necessary correspond to the info in the rel!

		if (relationshipType==null) {
			log.info("trying to set part shortcut against a null relationship type.");
			return false;
		}

		if (relationshipType.equals(Namespaces.PRESENTATIONML_NOTES_SLIDE)) {
			notes = (NotesSlidePart)part;
			return true;
		} else if (relationshipType.equals(Namespaces.PRESENTATIONML_SLIDE_LAYOUT)) {
			layout = (SlideLayoutPart)part;
			return true;
		} else {
			return false;
		}
	}

	public NotesSlidePart getNotesSlidePart() {
		return notes;
	}

    SlideLayoutPart layout = null;

    public SlideLayoutPart getSlideLayoutPart() {
        if (layout!=null) {
            return layout;
        }
        final Relationship layoutRel = getRelationshipsPart().getRelationshipByType(
            Namespaces.PRESENTATIONML_SLIDE_LAYOUT);
        if (layoutRel==null) {
            log.info(this.partName.getName() + " has no master!");
        } else {
            layout = (SlideLayoutPart)getRelationshipsPart().getPart(layoutRel);
        }
        return layout;
    }

    @Override
    public Sld unmarshal(InputStream is) {
        // TODO Auto-generated method stub
        return super.unmarshal(is);
    }

    public org.pptx4j.pml.CTCommentList _getComments(boolean forceCreate)
        throws InvalidFormatException {

        final RelationshipsPart relationshipsPart = getRelationshipsPart(forceCreate);
        if(relationshipsPart!=null) {
            Relationship relationship = relationshipsPart.getRelationshipByType(Namespaces.COMMENTS);
            if(relationship!=null) {
                return ((CommentListPart)relationshipsPart.getPart(relationship)).getJaxbElement();
            }
            else if(forceCreate) {
                final CommentListPart commentListPart = new CommentListPart();
                final CTCommentList commentList = new CTCommentList();
                commentListPart.setJaxbElement(commentList);
                addTargetPart(commentListPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
                return commentList;
            }
        }
        return null;
    }

    public org.pptx4j.pml_2018_8.CTCommentList _getModernComments(boolean forceCreate)
        throws InvalidFormatException {

        final RelationshipsPart relationshipsPart = getRelationshipsPart(forceCreate);
        if(relationshipsPart!=null) {
            Relationship relationship = relationshipsPart.getRelationshipByType(Namespaces.MODERN_COMMENTS);
            if(relationship!=null) {
                return ((ModernCommentsPart)relationshipsPart.getPart(relationship)).getJaxbElement();
            }
            else if(forceCreate) {
                final ModernCommentsPart modernCommentsPart = new ModernCommentsPart();
                final org.pptx4j.pml_2018_8.CTCommentList commentList = new org.pptx4j.pml_2018_8.CTCommentList();
                modernCommentsPart.setJaxbElement(commentList);
                addTargetPart(modernCommentsPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
                return commentList;
            }
        }
        return null;
    }
}
