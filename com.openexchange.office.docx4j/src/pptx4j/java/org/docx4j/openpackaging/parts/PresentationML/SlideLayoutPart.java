/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.openpackaging.parts.PresentationML;

import java.util.List;
import jakarta.xml.bind.JAXBException;
import org.docx4j.XmlUtils;
import org.docx4j.dml.CTColorMappingOverride;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.relationships.Relationship;
import org.pptx4j.jaxb.Context;
import org.pptx4j.pml.CTSlideTiming;
import org.pptx4j.pml.CommonSlideData;
import org.pptx4j.pml.ObjectFactory;
import org.pptx4j.pml.SldLayout;
import com.openexchange.office.filter.core.IContentAccessor;

public final class SlideLayoutPart extends JaxbPmlPart<SldLayout> implements IContentAccessor<Object>, ICommonSlideAccessor, ICommonTimingAccessor {
	
	public SlideLayoutPart(PartName partName) {
		super(partName);
		init();
	}

	public SlideLayoutPart() throws InvalidFormatException {
		super(new PartName("/ppt/slideLayouts/slideLayout1.xml"));
		init();
	}
	
	public void init() {		
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_SLIDE_LAYOUT));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.PRESENTATIONML_SLIDE_LAYOUT);
		
	}

	@Override
	public List<Object> getContent() {
		final SldLayout slideLayout = this.getJaxbElement();
		if(slideLayout.getCSld()==null) {
			slideLayout.setCSld(Context.getpmlObjectFactory().createCommonSlideData());
		}
		return slideLayout.getCSld().getContent();
	}

	@Override
	public CommonSlideData getCSld() {
		return getJaxbElement().getCSld();
	}

	@Override
	public CTSlideTiming getTiming() {
		return getJaxbElement().getTiming();
	}

	@Override
	public void setTiming(CTSlideTiming value) {
		getJaxbElement().setTiming(value);
	}

	SlideMasterPart master = null;
	public SlideMasterPart getSlideMasterPart() {
		
		if (master!=null) {
			return master;
		}
		
		Relationship masterRel = getRelationshipsPart().getRelationshipByType(
				Namespaces.PRESENTATIONML_SLIDE_MASTER);
		if (masterRel==null) {
			log.info(this.partName.getName() + " has no master!");
		} else {
			master = (SlideMasterPart)getRelationshipsPart().getPart(masterRel);
		}
		return master;
	}

    public boolean isDefaultColorMapping() {
        final SldLayout slideLayout = getJaxbElement();
        final CTColorMappingOverride clrMappingOverride = slideLayout.getClrMapOvr();
        if(clrMappingOverride!=null) {
            if(clrMappingOverride.getOverrideClrMapping()!=null) {
                return SlideMasterPart.isDefaultColorMapping(clrMappingOverride.getOverrideClrMapping());
            }
            else if(clrMappingOverride.getMasterClrMapping()!=null) {
                return getSlideMasterPart().isDefaultColorMapping();
            }
        }
        return getSlideMasterPart().isDefaultColorMapping();
    }

	public static SldLayout createSldLayout(OpcPackage opcPackage) throws JAXBException {

		ObjectFactory factory = Context.getpmlObjectFactory(); 
		SldLayout sldLayout = factory.createSldLayout();
		
		CommonSlideData cSld = (CommonSlideData)XmlUtils.unmarshalString(COMMON_SLIDE_DATA, opcPackage,
				Context.getJcPml(),
				CommonSlideData.class);
		cSld.setName("Title Slide");
		
		sldLayout.setCSld( cSld );
		return sldLayout;		
	}
}
