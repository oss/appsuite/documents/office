
package org.docx4j.openpackaging.parts.PresentationML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.pptx4j.pml.CTCommentAuthorList;

public final class CommentAuthorsPart extends JaxbPmlPart<CTCommentAuthorList> {

	public CommentAuthorsPart(PartName partName) throws InvalidFormatException {
		super(partName);
		init();
	}

	public CommentAuthorsPart() throws InvalidFormatException {
		super(new PartName("/ppt/commentAuthors.xml"));
		init();
	}

	public void init() {		
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_COMMENT_AUTHORS));

		setRelationshipType(Namespaces.PRESENTATIONML_COMMENT_AUTHORS);
	}
}
