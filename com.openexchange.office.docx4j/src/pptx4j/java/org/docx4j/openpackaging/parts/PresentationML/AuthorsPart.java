
package org.docx4j.openpackaging.parts.PresentationML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.pptx4j.pml_2018_8.CTAuthorList;

public final class AuthorsPart extends JaxbPmlPart<CTAuthorList> {

	public AuthorsPart(PartName partName) throws InvalidFormatException {
		super(partName);
		init();
	}

	public AuthorsPart() throws InvalidFormatException {
		super(new PartName("/ppt/authors.xml"));
		init();
	}

	public void init() {
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_AUTHORS));

		setRelationshipType(Namespaces.PRESENTATIONML_AUTHORS);
	}
}
