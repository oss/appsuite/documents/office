
package org.docx4j.openpackaging.parts.PresentationML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.pptx4j.pml.CTCommentList;

public final class CommentListPart extends JaxbPmlPart<CTCommentList> {

	public CommentListPart(PartName partName) throws InvalidFormatException {
		super(partName);
		init();
	}

	public CommentListPart() throws InvalidFormatException {
		super(new PartName("/ppt/comments/comment1.xml"));
		init();
	}

	public void init() {
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_COMMENTS));

		setRelationshipType(Namespaces.COMMENTS);
	}
}
