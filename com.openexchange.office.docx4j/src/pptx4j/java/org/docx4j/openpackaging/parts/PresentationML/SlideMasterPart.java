/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.openpackaging.parts.PresentationML;

import java.util.List;
import jakarta.xml.bind.JAXBException;
import org.docx4j.XmlUtils;
import org.docx4j.dml.CTColorMapping;
import org.docx4j.dml.STColorSchemeIndex;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.relationships.Relationship;
import org.pptx4j.jaxb.Context;
import org.pptx4j.pml.CTSlideTiming;
import org.pptx4j.pml.CommonSlideData;
import org.pptx4j.pml.ObjectFactory;
import org.pptx4j.pml.SldMaster;
import org.pptx4j.pml.SlideLayoutIdList;
import org.pptx4j.pml.SlideLayoutIdList.SldLayoutId;
import com.openexchange.office.filter.core.IContentAccessor;

public final class SlideMasterPart extends JaxbPmlPart<SldMaster> implements IContentAccessor<Object>, ICommonSlideAccessor, ICommonTimingAccessor {
	
	public SlideMasterPart(PartName partName) {
		super(partName);
		init();
	}

	public SlideMasterPart() throws InvalidFormatException {
		super(new PartName("/ppt/slideMasters/slideMaster1.xml"));
		init();
	}
	
	public void init() {		
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.PRESENTATIONML_SLIDE_MASTER));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.PRESENTATIONML_SLIDE_MASTER);
		
	}

	@Override
	public List<Object> getContent() {
		final SldMaster slideMaster = this.getJaxbElement();
		if(slideMaster.getCSld()==null) {
			slideMaster.setCSld(Context.getpmlObjectFactory().createCommonSlideData());
		}
		return slideMaster.getCSld().getContent();
	}

	@Override
	public CommonSlideData getCSld() {
		return getJaxbElement().getCSld();
	}

	@Override
	public CTSlideTiming getTiming() {
		return getJaxbElement().getTiming();
	}

	@Override
	public void setTiming(CTSlideTiming value) {
		getJaxbElement().setTiming(value);
	}

	public boolean isDefaultColorMapping() {
	    return isDefaultColorMapping(getJaxbElement().getClrMap());
	}

	public static boolean isDefaultColorMapping(CTColorMapping colorMapping) {
        if(colorMapping!=null) {
            if(colorMapping.getBg1()!=STColorSchemeIndex.LT_1) {
                return false;
            }
            if(colorMapping.getBg2()!=STColorSchemeIndex.LT_2) {
                return false;
            }
            if(colorMapping.getTx1()!=STColorSchemeIndex.DK_1) {
                return false;
            }
            if(colorMapping.getTx2()!=STColorSchemeIndex.DK_2) {
                return false;
            }
            if(colorMapping.getAccent1()!=STColorSchemeIndex.ACCENT_1) {
                return false;
            }
            if(colorMapping.getAccent2()!=STColorSchemeIndex.ACCENT_2) {
                return false;
            }
            if(colorMapping.getAccent3()!=STColorSchemeIndex.ACCENT_3) {
                return false;
            }
            if(colorMapping.getAccent4()!=STColorSchemeIndex.ACCENT_4) {
                return false;
            }
            if(colorMapping.getAccent5()!=STColorSchemeIndex.ACCENT_5) {
                return false;
            }
            if(colorMapping.getAccent6()!=STColorSchemeIndex.ACCENT_6) {
                return false;
            }
            if(colorMapping.getFolHlink()!=STColorSchemeIndex.FOL_HLINK) {
                return false;
            }
            if(colorMapping.getHlink()!=STColorSchemeIndex.HLINK) {
                return false;
            }
        }
        return true;
	}

	public static SldMaster createSldMaster(OpcPackage opcPackage) throws JAXBException {
		ObjectFactory factory = Context.getpmlObjectFactory(); 
		SldMaster sldMaster = factory.createSldMaster();
		sldMaster.setCSld( 
				(CommonSlideData)XmlUtils.unmarshalString(COMMON_SLIDE_DATA, opcPackage, Context.getJcPml(),
						CommonSlideData.class) );
		sldMaster.setClrMap(
				(CTColorMapping)XmlUtils.unmarshalString(COLOR_MAPPING, opcPackage, Context.getJcPml(), CTColorMapping.class ) );

		SlideLayoutIdList slideLayoutIdList = factory.createSlideLayoutIdList();
		sldMaster.setSldLayoutIdLst(slideLayoutIdList);
		
		return sldMaster;		
	}
		
	public SldLayoutId addSlideLayoutIdListEntry(SlideLayoutPart slideLayoutPart) 
		throws InvalidFormatException {
		
		Relationship rel = this.addTargetPart(slideLayoutPart);
		
		SldLayoutId entry = Context.getpmlObjectFactory().createSlideLayoutIdListSldLayoutId();
		
		entry.setId( Long.valueOf(getSlideLayoutOrMasterId()) );
		entry.setRid(rel.getId());

		this.getJaxbElement().getSldLayoutIdLst(true).getSldLayoutId().add(entry);

		return entry;
	}
}
