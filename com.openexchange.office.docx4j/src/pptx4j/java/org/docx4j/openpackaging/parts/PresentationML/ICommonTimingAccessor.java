
package org.docx4j.openpackaging.parts.PresentationML;

import org.pptx4j.pml.CTSlideTiming;

public interface ICommonTimingAccessor {

	/**
     * Gets the value of the timing property.
     * 
     * @return
     *     possible object is
     *     {@link CTSlideTiming }
     *     
     */
    public CTSlideTiming getTiming();

    /**
     * Sets the value of the timing property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSlideTiming }
     *     
     */
    public void setTiming(CTSlideTiming value);
}
