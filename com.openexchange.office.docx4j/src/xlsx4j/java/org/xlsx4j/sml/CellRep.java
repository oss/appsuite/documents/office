

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

import com.openexchange.office.filter.core.spreadsheet.CellRef;

public class CellRep {

    protected Object column;

    /*
     * type represented by the combination
     * of following features that were supported:
     * 
     * bit 0==1 -> value is supported
     * bit 1==1 -> whitespace preserved (only if bit 0==1)
     * bit 2==1 -> formula is supported
     * 
     */

    public int getType() {
    	return 0;
    }

    public Integer getColumn() {
    	if (column instanceof String) {
    		column = CellRef.createCellRef((String)column).getColumn();
    	}
        return (Integer)column;
    }

    public void setColumn(int _column) {
    	column = Integer.valueOf(_column);
    }

    public String getR() {
    	return (String)column;
    }

    public void setR(String r) {
    	column = r;
    }

    public CellRep createNewCellRep(int newType) {
    	final int currentType = getType();
    	if(currentType==newType) {
    		return this;
    	}
    	CellRep newCellRep = null;
    	switch(newType) {
    		case 0 : {
    			newCellRep = new CellRep();
    			break;
    		}
    		case 1 : {
    			newCellRep = new CellRepV();
    			break;
    		}
    		case 3 : {
    			newCellRep = new CellRepPreserveV();
    			break;
    		}
    		case 4 : {
    			newCellRep = new CellRepF();
    			break;
    		}
    		case 5 : {
    			newCellRep = new CellRepVF();
    			break;
    		}
    		case 7 : {
    			newCellRep = new CellRepPreserveVF();
    			break;
    		}
    	}
    	newCellRep.column = column;

    	if(newCellRep instanceof iCellRepV && this instanceof iCellRepV) {
    		((iCellRepV)newCellRep).setV((((iCellRepV)this).getV()));
    	}
    	if(newCellRep instanceof iCellRepF && this instanceof iCellRepF) {
    		((iCellRepF)newCellRep).setF((((iCellRepF)this).getF()));
    	}
    	return newCellRep;
    }
}
