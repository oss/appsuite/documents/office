
package org.xlsx4j.sml;

public interface ICfvo {

    public String getVal();

    public void setVal(String value);

    public Enum<?> getType();

    public boolean isGte();

    public void setGte(Boolean value);

}
