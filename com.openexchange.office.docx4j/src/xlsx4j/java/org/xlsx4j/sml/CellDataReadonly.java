
package org.xlsx4j.sml;

public class CellDataReadonly {

	final static public CellDataReadonly EmptyCell = new CellDataReadonly();

	protected CTRst is;
    protected CTExtensionList extLst;
    protected Long s;
    protected STCellType t;
    protected Long cm;
    protected Long vm;
    protected Boolean ph;

    CellDataReadonly() {}
	CellDataReadonly(CellDataReadonly cellData) {
		is = cellData.is;
		extLst = cellData.extLst;
		s = cellData.s;
		t = cellData.t;
		cm = cellData.cm;
		vm = cellData.vm;
		ph = cellData.ph;
	}
    @Override
    public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cm == null) ? 0 : cm.hashCode());
		result = prime * result + ((ph == null) ? 0 : ph.hashCode());
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		result = prime * result + ((t == null) ? 0 : t.hashCode());
		result = prime * result + ((vm == null) ? 0 : vm.hashCode());
		result = prime * result + ((is == null) ? 0 : 12345678);			// TODO: create proper hash for is instance
		result = prime * result + ((extLst == null) ? 0 : 87654321);		// TODO: create proper hash for extLst
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CellDataReadonly other = (CellDataReadonly) obj;
		if (cm == null) {
			if (other.cm != null)
				return false;
		} else if (!cm.equals(other.cm))
			return false;
		if (ph == null) {
			if (other.ph != null)
				return false;
		} else if (!ph.equals(other.ph))
			return false;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		if (t != other.t)
			return false;
		if (vm == null) {
			if (other.vm != null)
				return false;
		} else if (!vm.equals(other.vm))
			return false;
		if(is==null) {
			if (other.is!=null) {
				return false;
			}
		} else {
			return false;	// TODO ... equality check needs to be implemented
		}
		if(extLst==null) {
			if(other.extLst!=null) {
				return false;
			}
		} else {
			return false;	// TODO ... equality check needs to be implemented
		}
		return true;
	}

	public CTRst getIs() {
        return is;
    }
    public CTExtensionList getExtLst() {
        return extLst;
    }
    public Long getS() {
    	return s;
    }
    public STCellType getT() {
    	return t;
    }
    public Long getCm() {
        return cm;
    }
    public Long getVm() {
        return vm;
    }
    public Boolean isPh() {
    	return ph;
    }
}
