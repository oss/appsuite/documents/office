
package org.xlsx4j.sml;

import java.util.List;

public interface IDataBar {

    public List<? extends ICfvo> getCfvo();

	public CTColor getColor();

    public boolean isShowValue();

    public void setShowValue(Boolean value);
    
    public long getMinLength();
    
    public void setMinLength(Long value);

    public long getMaxLength();

    public void setMaxLength(Long value);
    
}
