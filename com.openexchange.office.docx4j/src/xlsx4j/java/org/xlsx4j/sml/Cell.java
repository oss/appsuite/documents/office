/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.jaxb.Context;

/**
 * <p>Java class for CT_Cell complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Cell">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="f" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CellFormula" minOccurs="0"/>
 *         &lt;element name="v" type="{http://schemas.openxmlformats.org/officeDocument/2006/sharedTypes}ST_Xstring" minOccurs="0"/>
 *         &lt;element name="is" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Rst" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="r" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_CellRef" />
 *       &lt;attribute name="s" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *       &lt;attribute name="t" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_CellType" default="n" />
 *       &lt;attribute name="cm" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *       &lt;attribute name="vm" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *       &lt;attribute name="ph" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Cell", propOrder = {
    "f",
    "XString",
    "is",
    "extLst",
    "r",
    "s",
    "t",
    "cm",
    "vm",
    "ph"
})
public class Cell
{
    @XmlTransient
    protected CellRep cellRep = new CellRep();
    @XmlTransient
    private CellDataReadonly cellData = CellDataReadonly.EmptyCell;

    public CellDataReadonly getCellData() {
    	return cellData;
    }

    public void setCellData(CellDataReadonly value) {
    	cellData = value;
    }

    /**
     * Gets the value of the f property.
     *
     * @return
     *     possible object is
     *     {@link CTCellFormula }
     *
     */
    @XmlElement
    public CTCellFormula getF() {
    	if(cellRep instanceof iCellRepF) {
    		return ((iCellRepF)cellRep).getF();
    	}
        return null;
    }

    /**
     * Sets the value of the f property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCellFormula }
     *
     */
    public void setF(CTCellFormula value) {
    	final int type = cellRep.getType();
    	if(value!=null) {
    		cellRep = cellRep.createNewCellRep(type|4);
    		((iCellRepF)cellRep).setF(value);
    	}
    	else {
    		cellRep = cellRep.createNewCellRep(type&~4);
    	}
    }

    /**
     * Gets the value of the v property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name="v")
    public CTXstringWhitespace getXString() {
    	if(cellRep instanceof iCellRepV) {
    		return new CTXstringWhitespace(((iCellRepV)cellRep).getV(), ((iCellRepV)cellRep).isPreserveWhitespace());
    	}
        return null;
    }

    /**
     * Sets the value of the v property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setXString(CTXstringWhitespace wsString) {
    	final int type = cellRep.getType();
    	if(wsString==null||wsString.getValue()==null) {
    		cellRep = cellRep.createNewCellRep(type&~3);
    		return;
    	}
    	else if(wsString.getSpace()!=null && wsString.getSpace().equals("preserve")) {
    		cellRep = cellRep.createNewCellRep(type|3);
    	}
    	else {
    		cellRep = cellRep.createNewCellRep(type|1);
    	}
    	((iCellRepV)cellRep).setV(wsString.getValue());
    }

    public String getV() {
    	if(cellRep instanceof iCellRepV) {
    		return ((iCellRepV)cellRep).getV();
    	}
    	return null;
    }

    public void setV(String value) {
    	final int type = cellRep.getType();
    	if(value!=null) {
    		if(value.indexOf(' ')!=-1) {
    			cellRep = cellRep.createNewCellRep(type|3);
    		}
    		else {
    			cellRep = cellRep.createNewCellRep(type|1);
    		}
    		((iCellRepV)cellRep).setV(value);
    	}
    	else {
    		cellRep = cellRep.createNewCellRep(type&~3);
    	}
    }

    /**
     * Gets the value of the is property.
     *
     * @return
     *     possible object is
     *     {@link CTRst }
     *
     */
    @XmlElement
    public CTRst getIs() {
        return cellData.getIs();
    }

    /**
     * Sets the value of the is property.
     *
     * @param value
     *     allowed object is
     *     {@link CTRst }
     *
     */
    public void setIs(CTRst value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setIs(value);
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    @XmlElement
    public CTExtensionList getExtLst() {
        return cellData.getExtLst();
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    public void setExtLst(CTExtensionList value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setExtLst(value);
    }

    public int getColumn() {
        return cellRep.getColumn();
    }

    public void setColumn(int column) {
    	cellRep.setColumn(column);
    }

    @XmlAttribute
    public String getR() {
    	return cellRep.getR();
    }

    public void setR(String r) {
    	cellRep.setR(r);
    }

    /**
     * Gets the value of the s property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @XmlAttribute(name = "s")
    @XmlSchemaType(name = "unsignedInt")
    public Long getS() {
        return cellData.getS();
    }

    @XmlTransient
    public long getStyle() {
    	final Long s = cellData.getS();
    	if(s==null) {
    		return 0;
    	}
    	return s.longValue();
    }

    /**
     * Sets the value of the s property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setS(Long value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setS(value);
    }

    /**
     * Gets the value of the t property.
     *
     * @return
     *     possible object is
     *     {@link STCellType }
     *
     */
    @XmlAttribute(name = "t")
    public STCellType getT() {
    	return cellData.getT();
    }

    /**
     * Sets the value of the t property.
     *
     * @param value
     *     allowed object is
     *     {@link STCellType }
     *
     */
    public void setT(STCellType value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setT(value);
    }

    /**
     * Gets the value of the cm property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @XmlAttribute(name = "cm")
    @XmlSchemaType(name = "unsignedInt")
    public Long getCm() {
        return cellData.getCm();
    }

    /**
     * Sets the value of the cm property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setCm(Long value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setCm(value);
    }

    /**
     * Gets the value of the vm property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @XmlAttribute(name = "vm")
    @XmlSchemaType(name = "unsignedInt")
    public Long getVm() {
    	return cellData.getVm();
    }

    /**
     * Sets the value of the vm property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setVm(Long value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setVm(value);
    }

    /**
     * Gets the value of the ph property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @XmlAttribute(name = "ph")
    public Boolean isPh() {
    	return cellData.isPh();
    }

    /**
     * Sets the value of the ph property.
     *
     *     {@link Boolean }
     * @param value
     *     allowed object is
     *
     */
    public void setPh(Boolean value) {
    	if(!(cellData instanceof CellDataWritable)) {
    		cellData = new CellDataWritable(cellData);
    	}
    	((CellDataWritable)cellData).setPh(value);
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(Unmarshaller unmarshaller, @SuppressWarnings("unused") Object parent) {
        Context.abortOnLowMemory(unmarshaller);
    }
}
