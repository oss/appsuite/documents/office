/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *   
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.xlsx4j.sml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

import org.docx4j.sharedtypes.STVerticalAlignRun;
import org.xlsx4j.jaxb.Context;


/**
 * <p>Java class for CT_VerticalAlignFontProperty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_VerticalAlignFontProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="val" use="required" type="{http://schemas.openxmlformats.org/officeDocument/2006/sharedTypes}ST_VerticalAlignRun" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_VerticalAlignFontProperty")
public class CTVerticalAlignFontProperty implements Cloneable
{
    @XmlAttribute(name = "val", required = true)
    protected STVerticalAlignRun val;

    @Override
    public CTVerticalAlignFontProperty clone() {
    	final CTVerticalAlignFontProperty clone = Context.getsmlObjectFactory().createCTVerticalAlignFontProperty();
    	clone.val = val;
    	return clone;
    }

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link STVerticalAlignRun }
     *     
     */
    public STVerticalAlignRun getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link STVerticalAlignRun }
     *     
     */
    public void setVal(STVerticalAlignRun value) {
        this.val = value;
    }
}
