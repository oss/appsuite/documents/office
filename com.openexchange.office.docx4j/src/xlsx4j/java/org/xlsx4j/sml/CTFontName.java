/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.util.StringCodec;


/**
 * <p>Java class for CT_FontName complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_FontName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="val" use="required" type="{http://schemas.openxmlformats.org/officeDocument/2006/sharedTypes}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_FontName")
public class CTFontName implements Cloneable
{
    @XmlAttribute(name = "val", required = true)
    protected String val;

    @Override
    public CTFontName clone() {
    	final CTFontName clone = Context.getsmlObjectFactory().createCTFontName();
    	clone.val = val;
    	return clone;
    }

    /**
     * Gets the value of the val property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setVal(String value) {
        this.val = value;
    }

    /**
     * Encodes strings before writing XML.
     */
    @SuppressWarnings("unused")
    public void beforeMarshal(Marshaller marshaller) {
        val = StringCodec.encode(val);
    }

    /**
     * Decodes strings after parsing XML.
     */
    @SuppressWarnings("unused")
    public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        val = StringCodec.decode(val);
    }
}
