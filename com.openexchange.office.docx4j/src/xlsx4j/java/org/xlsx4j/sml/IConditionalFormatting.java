
package org.xlsx4j.sml;

import java.util.List;
import com.openexchange.office.filter.core.component.Child;

public interface IConditionalFormatting extends Child {

	public List<? extends ICfRule> getCfRule();

	public List<String> getSqrefList();

    public CTExtensionList getExtLst();

    public void setExtLst(CTExtensionList value);

    public boolean isPivot();

    public void setPivot(Boolean value);

    public ICfRule createCfRule();
}
