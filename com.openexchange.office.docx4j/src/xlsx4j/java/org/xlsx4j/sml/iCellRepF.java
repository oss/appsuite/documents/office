

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public interface iCellRepF {

	public CTCellFormula getF();

	public void setF(CTCellFormula value);

}
