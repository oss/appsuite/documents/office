
package org.xlsx4j.sml;


public class RowDataReadonly {

	final public static RowDataReadonly EmptyRow = new RowDataReadonly();

    protected Long s;
    protected Double ht;
    protected Short outlineLevel;
//  @XmlAttribute(name = "customFormat")	// 0x0003
//  protected Boolean customFormat;
//  @XmlAttribute(name = "hidden")			// 0x000c
//  protected Boolean hidden;
//  @XmlAttribute(name = "customHeight")	// 0x0030
//  protected Boolean customHeight;
//  @XmlAttribute(name = "collapsed")		// 0x00c0
//  protected Boolean collapsed;
//  @XmlAttribute(name = "thickTop")		// 0x0300
//  protected Boolean thickTop;
//  @XmlAttribute(name = "thickBot")		// 0x0c00
//  protected Boolean thickBot;
//  @XmlAttribute(name = "ph")				// 0x3000
//  protected Boolean ph;
    protected int flags;
    protected CTExtensionList extLst;

	RowDataReadonly() {};
	RowDataReadonly(RowDataReadonly rowData) {
		s = rowData.s;
	    ht = rowData.ht;
	    outlineLevel = rowData.outlineLevel;
	    flags = rowData.flags;
		extLst = rowData.extLst;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + flags;
		result = prime * result + ((ht == null) ? 0 : ht.hashCode());
		result = prime * result
				+ ((outlineLevel == null) ? 0 : outlineLevel.hashCode());
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		result = prime * result + ((extLst == null) ? 0 : 12345678);		// TODO: create proper hash for extLst
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RowDataReadonly other = (RowDataReadonly) obj;
		if (flags != other.flags)
			return false;
		if (ht == null) {
			if (other.ht != null)
				return false;
		} else if (!ht.equals(other.ht))
			return false;
		if (outlineLevel == null) {
			if (other.outlineLevel != null)
				return false;
		} else if (!outlineLevel.equals(other.outlineLevel))
			return false;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		if(extLst==null) {
			if(other.extLst!=null) {
				return false;
			}
		} else {
			return false;	// TODO ... equality check needs to be implemented
		}
		return true;
	}
	public Long getS() {
		return s;
	}
	public Double getHt() {
		return ht;
	}
    public Short getOutlineLevel() {
    	return outlineLevel;
    }
    public Boolean isCustomFormat() {
        if ((flags&0x1)==0) {
            return null;
        }
        return (flags&0x2)!=0;
    }
    public Boolean isHidden() {
        if ((flags&0x4)==0) {
            return null;
        }
        return (flags&0x8)!=0;
    }
    public Boolean isCustomHeight() {
        if ((flags&0x10)==0) {
            return null;
        }
        return (flags&0x20)!=0;
    }
    public Boolean isCollapsed() {
        if ((flags&0x40)==0) {
            return null;
        }
        return (flags&0x80)!=0;
    }
    public Boolean isThickTop() {
        if ((flags&0x100)==0) {
            return null;
        }
        return (flags&0x200)!=0;
    }
    public Boolean isThickBot() {
        if ((flags&0x400)==0) {
            return null;
        }
        return (flags&0x800)!=0;
    }
    public Boolean isPh() {
        if ((flags&0x1000)==0) {
            return null;
        }
        return (flags&0x2000)!=0;
    }
    public CTExtensionList getExtLst() {
        return extLst;
    }
}
