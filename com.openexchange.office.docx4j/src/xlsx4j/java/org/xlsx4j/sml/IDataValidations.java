
package org.xlsx4j.sml;

import java.util.List;
import com.openexchange.office.filter.core.component.Child;


public interface IDataValidations extends Child {

    /**
     * Gets the value of the dataValidation property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataValidation property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataValidation().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTDataValidation }
     *
     *
     */
    public List<? extends IDataValidation> getDataValidation();

    /**
     * Gets the value of the disablePrompts property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isDisablePrompts();

    /**
     * Sets the value of the disablePrompts property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setDisablePrompts(Boolean value);

    /**
     * Gets the value of the xWindow property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getXWindow();

    /**
     * Sets the value of the xWindow property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setXWindow(Long value);

    /**
     * Gets the value of the yWindow property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getYWindow();

    /**
     * Sets the value of the yWindow property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
     public void setYWindow(Long value);
}
