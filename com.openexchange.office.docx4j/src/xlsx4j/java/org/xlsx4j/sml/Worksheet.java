/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.apache.commons.lang3.mutable.MutableInt;
import org.xlsx4j.sml.CfRule.CfRuleIter;


/**
 * <p>Java class for CT_Worksheet complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Worksheet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sheetPr" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetPr" minOccurs="0"/>
 *         &lt;element name="dimension" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetDimension" minOccurs="0"/>
 *         &lt;element name="sheetViews" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetViews" minOccurs="0"/>
 *         &lt;element name="sheetFormatPr" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetFormatPr" minOccurs="0"/>
 *         &lt;element name="cols" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Cols" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sheetData" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetData"/>
 *         &lt;element name="sheetCalcPr" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetCalcPr" minOccurs="0"/>
 *         &lt;element name="sheetProtection" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetProtection" minOccurs="0"/>
 *         &lt;element name="protectedRanges" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ProtectedRanges" minOccurs="0"/>
 *         &lt;element name="scenarios" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Scenarios" minOccurs="0"/>
 *         &lt;element name="autoFilter" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_AutoFilter" minOccurs="0"/>
 *         &lt;element name="sortState" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SortState" minOccurs="0"/>
 *         &lt;element name="dataConsolidate" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_DataConsolidate" minOccurs="0"/>
 *         &lt;element name="customSheetViews" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CustomSheetViews" minOccurs="0"/>
 *         &lt;element name="mergeCells" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_MergeCells" minOccurs="0"/>
 *         &lt;element name="phoneticPr" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PhoneticPr" minOccurs="0"/>
 *         &lt;element name="conditionalFormatting" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ConditionalFormatting" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dataValidations" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_DataValidations" minOccurs="0"/>
 *         &lt;element name="hyperlinks" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Hyperlinks" minOccurs="0"/>
 *         &lt;element name="printOptions" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PrintOptions" minOccurs="0"/>
 *         &lt;element name="pageMargins" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PageMargins" minOccurs="0"/>
 *         &lt;element name="pageSetup" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PageSetup" minOccurs="0"/>
 *         &lt;element name="headerFooter" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_HeaderFooter" minOccurs="0"/>
 *         &lt;element name="rowBreaks" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PageBreak" minOccurs="0"/>
 *         &lt;element name="colBreaks" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PageBreak" minOccurs="0"/>
 *         &lt;element name="customProperties" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CustomProperties" minOccurs="0"/>
 *         &lt;element name="cellWatches" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CellWatches" minOccurs="0"/>
 *         &lt;element name="ignoredErrors" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_IgnoredErrors" minOccurs="0"/>
 *         &lt;element name="smartTags" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SmartTags" minOccurs="0"/>
 *         &lt;element name="drawing" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Drawing" minOccurs="0"/>
 *         &lt;element name="legacyDrawing" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_LegacyDrawing" minOccurs="0"/>
 *         &lt;element name="legacyDrawingHF" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_LegacyDrawing" minOccurs="0"/>
 *         &lt;element name="drawingHF" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_DrawingHF" minOccurs="0"/>
 *         &lt;element name="picture" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_SheetBackgroundPicture" minOccurs="0"/>
 *         &lt;element name="oleObjects" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_OleObjects" minOccurs="0"/>
 *         &lt;element name="controls" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Controls" minOccurs="0"/>
 *         &lt;element name="webPublishItems" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_WebPublishItems" minOccurs="0"/>
 *         &lt;element name="tableParts" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_TableParts" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Worksheet", propOrder = {
    "sheetPr",
    "dimension",
    "sheetViews",
    "sheetFormatPr",
    "cols",
    "sheetData",
    "sheetCalcPr",
    "sheetProtection",
    "protectedRanges",
    "scenarios",
    "autoFilter",
    "sortState",
    "dataConsolidate",
    "customSheetViews",
    "mergeCells",
    "phoneticPr",
    "conditionalFormatting",
    "dataValidations",
    "hyperlinks",
    "printOptions",
    "pageMargins",
    "pageSetup",
    "headerFooter",
    "rowBreaks",
    "colBreaks",
    "customProperties",
    "cellWatches",
    "ignoredErrors",
    "smartTags",
    "drawing",
    "legacyDrawing",
    "legacyDrawingHF",
    "drawingHF",
    "picture",
    "oleObjects",
    "controls",
    "webPublishItems",
    "tableParts",
    "extLst"
})
@XmlRootElement(name = "worksheet")
public class Worksheet implements ISheetAccess, IAutoFilterAccessor, ISortStateAccessor
{
    protected CTSheetPr sheetPr;
    protected CTSheetDimension dimension;
    protected SheetViews sheetViews;
    protected CTSheetFormatPr sheetFormatPr;
    protected List<Cols> cols;
    @XmlElement(required = true)
    protected SheetData sheetData;
    protected CTSheetCalcPr sheetCalcPr;
    protected CTSheetProtection sheetProtection;
    protected CTProtectedRanges protectedRanges;
    protected CTScenarios scenarios;
    protected CTAutoFilter autoFilter;
    protected CTSortState sortState;
    protected CTDataConsolidate dataConsolidate;
    protected CTCustomSheetViews customSheetViews;
    protected CTMergeCells mergeCells;
    protected CTPhoneticPr phoneticPr;
    protected List<CTConditionalFormatting> conditionalFormatting;
    protected CTDataValidations dataValidations;
    protected CTHyperlinks hyperlinks;
    protected CTPrintOptions printOptions;
    protected CTPageMargins pageMargins;
    protected CTPageSetup pageSetup;
    protected CTHeaderFooter headerFooter;
    protected CTPageBreak rowBreaks;
    protected CTPageBreak colBreaks;
    protected CTCustomProperties customProperties;
    protected CTCellWatches cellWatches;
    protected CTIgnoredErrors ignoredErrors;
    protected CTSmartTags smartTags;
    protected CTDrawing drawing;
    protected CTLegacyDrawing legacyDrawing;
    protected CTLegacyDrawing legacyDrawingHF;
    protected CTDrawingHF drawingHF;
    protected CTSheetBackgroundPicture picture;
    protected CTOleObjects oleObjects;
    protected CTControls controls;
    protected CTWebPublishItems webPublishItems;
    protected CTTableParts tableParts;
    protected CTExtensionList extLst;
    @XmlTransient
    private HashMap<String, CfRule> conditionalFormattingRules = null;

    /**
     * Gets the value of the sheetPr property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetPr }
     *
     */
    public CTSheetPr getSheetPr() {
        return sheetPr;
    }

    /**
     * Sets the value of the sheetPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetPr }
     *
     */
    public void setSheetPr(CTSheetPr value) {
        this.sheetPr = value;
    }

    /**
     * Gets the value of the dimension property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetDimension }
     *
     */
    public CTSheetDimension getDimension() {
        return dimension;
    }

    /**
     * Sets the value of the dimension property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetDimension }
     *
     */
    public void setDimension(CTSheetDimension value) {
        this.dimension = value;
    }

    /**
     * Gets the value of the sheetViews property.
     *
     * @return
     *     possible object is
     *     {@link SheetViews }
     *
     */
    public SheetViews getSheetViews() {
        return sheetViews;
    }

    /**
     * Sets the value of the sheetViews property.
     *
     * @param value
     *     allowed object is
     *     {@link SheetViews }
     *
     */
    public void setSheetViews(SheetViews value) {
        this.sheetViews = value;
    }

    /**
     * Gets the value of the sheetFormatPr property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetFormatPr }
     *
     */
    public CTSheetFormatPr getSheetFormatPr() {
        return sheetFormatPr;
    }

    /**
     * Sets the value of the sheetFormatPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetFormatPr }
     *
     */
    public void setSheetFormatPr(CTSheetFormatPr value) {
        this.sheetFormatPr = value;
    }

    /**
     * Gets the value of the cols property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cols property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCols().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cols }
     *
     *
     */
    public List<Cols> getCols() {
        if (cols == null) {
            cols = new ArrayList<Cols>();
        }
        return this.cols;
    }

    /**
     * Gets the value of the sheetData property.
     *
     * @return
     *     possible object is
     *     {@link SheetData }
     *
     */
    public SheetData getSheetData() {
        return sheetData;
    }

    /**
     * Sets the value of the sheetData property.
     *
     * @param value
     *     allowed object is
     *     {@link SheetData }
     *
     */
    public void setSheetData(SheetData value) {
        this.sheetData = value;
    }

    /**
     * Gets the value of the sheetCalcPr property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetCalcPr }
     *
     */
    public CTSheetCalcPr getSheetCalcPr() {
        return sheetCalcPr;
    }

    /**
     * Sets the value of the sheetCalcPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetCalcPr }
     *
     */
    public void setSheetCalcPr(CTSheetCalcPr value) {
        this.sheetCalcPr = value;
    }

    /**
     * Gets the value of the sheetProtection property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetProtection }
     *
     */
    public CTSheetProtection getSheetProtection() {
        return sheetProtection;
    }

    /**
     * Sets the value of the sheetProtection property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetProtection }
     *
     */
    public void setSheetProtection(CTSheetProtection value) {
        this.sheetProtection = value;
    }

    /**
     * Gets the value of the protectedRanges property.
     *
     * @return
     *     possible object is
     *     {@link CTProtectedRanges }
     *
     */
    public CTProtectedRanges getProtectedRanges() {
        return protectedRanges;
    }

    /**
     * Sets the value of the protectedRanges property.
     *
     * @param value
     *     allowed object is
     *     {@link CTProtectedRanges }
     *
     */
    public void setProtectedRanges(CTProtectedRanges value) {
        this.protectedRanges = value;
    }

    /**
     * Gets the value of the scenarios property.
     *
     * @return
     *     possible object is
     *     {@link CTScenarios }
     *
     */
    public CTScenarios getScenarios() {
        return scenarios;
    }

    /**
     * Sets the value of the scenarios property.
     *
     * @param value
     *     allowed object is
     *     {@link CTScenarios }
     *
     */
    public void setScenarios(CTScenarios value) {
        this.scenarios = value;
    }

    /**
     * Gets the value of the autoFilter property.
     *
     * @return
     *     possible object is
     *     {@link CTAutoFilter }
     *
     */
    @Override
    public CTAutoFilter getAutoFilter(boolean forceCreate) {
        if(autoFilter==null&&forceCreate) {
            autoFilter = new CTAutoFilter();
        }
        return autoFilter;
    }

    /**
     * Sets the value of the autoFilter property.
     *
     * @param value
     *     allowed object is
     *     {@link CTAutoFilter }
     *
     */
    @Override
    public void setAutoFilter(CTAutoFilter value) {
        this.autoFilter = value;
    }

    /**
     * Gets the value of the sortState property.
     *
     * @return
     *     possible object is
     *     {@link CTSortState }
     *
     */
    @Override
    public CTSortState getSortState(boolean forceCreate) {
        if(sortState==null&&forceCreate) {
            sortState = new CTSortState();
        }
        return sortState;
    }

    /**
     * Sets the value of the sortState property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSortState }
     *
     */
    @Override
    public void setSortState(CTSortState value) {
        this.sortState = value;
    }

    /**
     * Gets the value of the dataConsolidate property.
     *
     * @return
     *     possible object is
     *     {@link CTDataConsolidate }
     *
     */
    public CTDataConsolidate getDataConsolidate() {
        return dataConsolidate;
    }

    /**
     * Sets the value of the dataConsolidate property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDataConsolidate }
     *
     */
    public void setDataConsolidate(CTDataConsolidate value) {
        this.dataConsolidate = value;
    }

    /**
     * Gets the value of the customSheetViews property.
     *
     * @return
     *     possible object is
     *     {@link CTCustomSheetViews }
     *
     */
    public CTCustomSheetViews getCustomSheetViews() {
        return customSheetViews;
    }

    /**
     * Sets the value of the customSheetViews property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCustomSheetViews }
     *
     */
    public void setCustomSheetViews(CTCustomSheetViews value) {
        this.customSheetViews = value;
    }

    /**
     * Gets the value of the mergeCells property.
     *
     * @return
     *     possible object is
     *     {@link CTMergeCells }
     *
     */
    public CTMergeCells getMergeCells() {
        return mergeCells;
    }

    /**
     * Sets the value of the mergeCells property.
     *
     * @param value
     *     allowed object is
     *     {@link CTMergeCells }
     *
     */
    public void setMergeCells(CTMergeCells value) {
        this.mergeCells = value;
    }

    /**
     * Gets the value of the phoneticPr property.
     *
     * @return
     *     possible object is
     *     {@link CTPhoneticPr }
     *
     */
    public CTPhoneticPr getPhoneticPr() {
        return phoneticPr;
    }

    /**
     * Sets the value of the phoneticPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPhoneticPr }
     *
     */
    public void setPhoneticPr(CTPhoneticPr value) {
        this.phoneticPr = value;
    }

    /**
     * Gets the value of the dataValidations property.
     *
     * @return
     *     possible object is
     *     {@link CTDataValidations }
     *
     */
    public CTDataValidations getDataValidations() {
        return dataValidations;
    }

    /**
     * Sets the value of the dataValidations property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDataValidations }
     *
     */
    public void setDataValidations(CTDataValidations value) {
        this.dataValidations = value;
    }

    /**
     * Gets the value of the hyperlinks property.
     *
     * @return
     *     possible object is
     *     {@link CTHyperlinks }
     *
     */
    public CTHyperlinks getHyperlinks() {
        return hyperlinks;
    }

    /**
     * Sets the value of the hyperlinks property.
     *
     * @param value
     *     allowed object is
     *     {@link CTHyperlinks }
     *
     */
    public void setHyperlinks(CTHyperlinks value) {
        this.hyperlinks = value;
    }

    /**
     * Gets the value of the printOptions property.
     *
     * @return
     *     possible object is
     *     {@link CTPrintOptions }
     *
     */
    public CTPrintOptions getPrintOptions() {
        return printOptions;
    }

    /**
     * Sets the value of the printOptions property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPrintOptions }
     *
     */
    public void setPrintOptions(CTPrintOptions value) {
        this.printOptions = value;
    }

    /**
     * Gets the value of the pageMargins property.
     *
     * @return
     *     possible object is
     *     {@link CTPageMargins }
     *
     */
    @Override
    public CTPageMargins getPageMargins() {
        return pageMargins;
    }

    /**
     * Sets the value of the pageMargins property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPageMargins }
     *
     */
    @Override
    public void setPageMargins(CTPageMargins value) {
        this.pageMargins = value;
    }

    /**
     * Gets the value of the pageSetup property.
     *
     * @return
     *     possible object is
     *     {@link CTPageSetup }
     *
     */
    public CTPageSetup getPageSetup() {
        return pageSetup;
    }

    /**
     * Sets the value of the pageSetup property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPageSetup }
     *
     */
    public void setPageSetup(CTPageSetup value) {
        this.pageSetup = value;
    }

    /**
     * Gets the value of the headerFooter property.
     *
     * @return
     *     possible object is
     *     {@link CTHeaderFooter }
     *
     */
    @Override
    public CTHeaderFooter getHeaderFooter() {
        return headerFooter;
    }

    /**
     * Sets the value of the headerFooter property.
     *
     * @param value
     *     allowed object is
     *     {@link CTHeaderFooter }
     *
     */
    @Override
    public void setHeaderFooter(CTHeaderFooter value) {
        this.headerFooter = value;
    }

    /**
     * Gets the value of the rowBreaks property.
     *
     * @return
     *     possible object is
     *     {@link CTPageBreak }
     *
     */
    public CTPageBreak getRowBreaks() {
        return rowBreaks;
    }

    /**
     * Sets the value of the rowBreaks property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPageBreak }
     *
     */
    public void setRowBreaks(CTPageBreak value) {
        this.rowBreaks = value;
    }

    /**
     * Gets the value of the colBreaks property.
     *
     * @return
     *     possible object is
     *     {@link CTPageBreak }
     *
     */
    public CTPageBreak getColBreaks() {
        return colBreaks;
    }

    /**
     * Sets the value of the colBreaks property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPageBreak }
     *
     */
    public void setColBreaks(CTPageBreak value) {
        this.colBreaks = value;
    }

    /**
     * Gets the value of the customProperties property.
     *
     * @return
     *     possible object is
     *     {@link CTCustomProperties }
     *
     */
    public CTCustomProperties getCustomProperties() {
        return customProperties;
    }

    /**
     * Sets the value of the customProperties property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCustomProperties }
     *
     */
    public void setCustomProperties(CTCustomProperties value) {
        this.customProperties = value;
    }

    /**
     * Gets the value of the cellWatches property.
     *
     * @return
     *     possible object is
     *     {@link CTCellWatches }
     *
     */
    public CTCellWatches getCellWatches() {
        return cellWatches;
    }

    /**
     * Sets the value of the cellWatches property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCellWatches }
     *
     */
    public void setCellWatches(CTCellWatches value) {
        this.cellWatches = value;
    }

    /**
     * Gets the value of the ignoredErrors property.
     *
     * @return
     *     possible object is
     *     {@link CTIgnoredErrors }
     *
     */
    public CTIgnoredErrors getIgnoredErrors() {
        return ignoredErrors;
    }

    /**
     * Sets the value of the ignoredErrors property.
     *
     * @param value
     *     allowed object is
     *     {@link CTIgnoredErrors }
     *
     */
    public void setIgnoredErrors(CTIgnoredErrors value) {
        this.ignoredErrors = value;
    }

    /**
     * Gets the value of the smartTags property.
     *
     * @return
     *     possible object is
     *     {@link CTSmartTags }
     *
     */
    public CTSmartTags getSmartTags() {
        return smartTags;
    }

    /**
     * Sets the value of the smartTags property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSmartTags }
     *
     */
    public void setSmartTags(CTSmartTags value) {
        this.smartTags = value;
    }

    /**
     * Gets the value of the drawing property.
     *
     * @return
     *     possible object is
     *     {@link CTDrawing }
     *
     */
    @Override
    public CTDrawing getDrawing() {
        return drawing;
    }

    /**
     * Sets the value of the drawing property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDrawing }
     *
     */
    @Override
    public void setDrawing(CTDrawing value) {
        this.drawing = value;
    }

    /**
     * Gets the value of the legacyDrawing property.
     *
     * @return
     *     possible object is
     *     {@link CTLegacyDrawing }
     *
     */
    @Override
    public CTLegacyDrawing getLegacyDrawing(boolean forceCreate) {
        if(legacyDrawing==null&&forceCreate) {
            legacyDrawing = new CTLegacyDrawing();
        }
        return legacyDrawing;
    }

    /**
     * Sets the value of the legacyDrawing property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLegacyDrawing }
     *
     */
    @Override
    public void setLegacyDrawing(CTLegacyDrawing value) {
        this.legacyDrawing = value;
    }

    /**
     * Gets the value of the legacyDrawingHF property.
     *
     * @return
     *     possible object is
     *     {@link CTLegacyDrawing }
     *
     */
    @Override
    public CTLegacyDrawing getLegacyDrawingHF() {
        return legacyDrawingHF;
    }

    /**
     * Sets the value of the legacyDrawingHF property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLegacyDrawing }
     *
     */
    @Override
    public void setLegacyDrawingHF(CTLegacyDrawing value) {
        this.legacyDrawingHF = value;
    }

    /**
     * Gets the value of the drawingHF property.
     *
     * @return
     *     possible object is
     *     {@link CTDrawingHF }
     *
     */
    @Override
    public CTDrawingHF getDrawingHF() {
        return drawingHF;
    }

    /**
     * Sets the value of the drawingHF property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDrawingHF }
     *
     */
    @Override
    public void setDrawingHF(CTDrawingHF value) {
        this.drawingHF = value;
    }

    /**
     * Gets the value of the picture property.
     *
     * @return
     *     possible object is
     *     {@link CTSheetBackgroundPicture }
     *
     */
    @Override
    public CTSheetBackgroundPicture getPicture() {
        return picture;
    }

    /**
     * Sets the value of the picture property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSheetBackgroundPicture }
     *
     */
    @Override
    public void setPicture(CTSheetBackgroundPicture value) {
        this.picture = value;
    }

    /**
     * Gets the value of the oleObjects property.
     *
     * @return
     *     possible object is
     *     {@link CTOleObjects }
     *
     */
    public CTOleObjects getOleObjects() {
        return oleObjects;
    }

    /**
     * Sets the value of the oleObjects property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOleObjects }
     *
     */
    public void setOleObjects(CTOleObjects value) {
        this.oleObjects = value;
    }

    /**
     * Gets the value of the controls property.
     *
     * @return
     *     possible object is
     *     {@link CTControls }
     *
     */
    public CTControls getControls() {
        return controls;
    }

    /**
     * Sets the value of the controls property.
     *
     * @param value
     *     allowed object is
     *     {@link CTControls }
     *
     */
    public void setControls(CTControls value) {
        this.controls = value;
    }

    /**
     * Gets the value of the webPublishItems property.
     *
     * @return
     *     possible object is
     *     {@link CTWebPublishItems }
     *
     */
    @Override
    public CTWebPublishItems getWebPublishItems() {
        return webPublishItems;
    }

    /**
     * Sets the value of the webPublishItems property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWebPublishItems }
     *
     */
    @Override
    public void setWebPublishItems(CTWebPublishItems value) {
        this.webPublishItems = value;
    }

    /**
     * Gets the value of the tableParts property.
     *
     * @return
     *     possible object is
     *     {@link CTTableParts }
     *
     */
    public CTTableParts getTableParts(boolean forceCreate) {
    	if(tableParts==null&&forceCreate) {
    		tableParts = new CTTableParts();
    	}
        return tableParts;
    }

    /**
     * Sets the value of the tableParts property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTableParts }
     *
     */
    public void setTableParts(CTTableParts value) {
        this.tableParts = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    @Override
    public CTExtensionList getExtLst(boolean forceCreate) {
    	if(extLst==null&&forceCreate) {
    		extLst = new CTExtensionList();
    	}
    	return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    @Override
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    public long getBaseColWidth() {
        return sheetFormatPr!=null ? sheetFormatPr.getBaseColWidth() : 8L;
    }

    public boolean isColumnVisible(int column) {
        // TODO: column access needs to be improved
        column++;
        final List<Cols> colsList = getCols();
        if(!colsList.isEmpty()) {
            for(Col col:colsList.get(0).getCol()) {
                if(col.getMin()>column) {
                    break;
                }
                if(col.getMin()<=column&&col.getMax()>=column) {
                    return !col.isHidden();
                }
            }
        }
        return true;
    }

    public boolean isRowVisible(int row) {
        final Row r = getSheetData().getRow(row, false);
        if(r!=null) {
            final RowDataReadonly rowData = r.getRowData();
            if(rowData!=null) {
                final Boolean isHidden = rowData.isHidden();
                return isHidden==null ? true : !isHidden.booleanValue();
            }
        }
        return true;
    }

    public HashMap<String, CfRule> getConditionalFormattingRules() {
    	if(conditionalFormattingRules==null) {
    		conditionalFormattingRules = new HashMap<String, CfRule>();
    	}
    	return conditionalFormattingRules;
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {

    	// deleting conditional formatting elements, they will newly be constructed
    	conditionalFormatting = null;
		final CTExtensionList lst = getExtLst(false);
		if(lst!=null) {
			final CTExtension ext = lst.getExtensionByUri("{78C0D931-6437-407d-A8EE-F0AAD7539E65}", false);
			if(ext!=null) {
				lst.getExt().remove(ext);
			}
		}

    	// filling the cfMap, key is now the range
		final Map<String, List<CfRule>> cfMap = new HashMap<String, List<CfRule>>();
    	final Iterator<CfRule> conditionalFormattingRuleIter = getConditionalFormattingRules().values().iterator();
    	while(conditionalFormattingRuleIter.hasNext()) {
    		final CfRule cfRule = conditionalFormattingRuleIter.next();
    		final String ranges = cfRule.getRanges().toString();
    		List<CfRule> cfRuleList = cfMap.get(ranges);
    		if(cfRuleList==null) {
    			cfRuleList = new ArrayList<CfRule>();
    			cfMap.put(ranges, cfRuleList);
    		}
    		cfRuleList.add(cfRule);
    	}

    	// now creating and filling the conditonalFormatting container
    	final Iterator<List<CfRule>> rulesIter = cfMap.values().iterator();
    	org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings cf2 = null;

    	while(rulesIter.hasNext()) {
    		final List<CfRule> rules = rulesIter.next();
    		CTConditionalFormatting lst1 = null;
    		org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormatting lst2 = null;
    		for(CfRule cfRule:rules) {
    			final CfRuleIter ruleIter = cfRule.iterator();
    			while(ruleIter.hasNext()) {
    				final ICfRule iCfRule = ruleIter.next();
    				if(iCfRule instanceof CTCfRule) {
    					final CTCfRule ctCfRule = (CTCfRule)iCfRule;
    					if(conditionalFormatting==null) {
    						conditionalFormatting = new ArrayList<CTConditionalFormatting>();
    					}
    					if(lst1==null) {
    						lst1 = new CTConditionalFormatting();
    						conditionalFormatting.add(lst1);
    						lst1.setSqrefList(cfRule.getRanges());
    					}
    					lst1.getCfRule().add(ctCfRule);
    				}
    				else if(iCfRule instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule) {
    					final org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule ctCfRule
    						= (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule)iCfRule;
    					if(cf2==null) {
        		    		final CTExtension ext = getExtLst(true).getExtensionByUri("{78C0D931-6437-407d-A8EE-F0AAD7539E65}", true);
        		    		cf2 = new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings();
        		    		ext.setAny(new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.ObjectFactory().createConditionalFormattings(cf2));
    					}
    					if(lst2==null) {
    						lst2 = new org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormatting();
    						cf2.getConditionalFormatting().add(lst2);
    						lst2.setSqrefList(cfRule.getRanges());
    					}
    					lst2.getCfRule().add(ctCfRule);
    				}
    			}
    		}
    	}

    	if(cols!=null&&cols.isEmpty()) {
    	    cols = null;
    	}

    	// check if there are no extensions, then we have to remove this element
    	if(extLst!=null) {
    		if(extLst.getExt().isEmpty()) {
    			extLst = null;
    		}
    	}

    	// empty data validations not allowed -> leads to load error in Excel
    	if(dataValidations!=null&&(dataValidations.dataValidation==null||dataValidations.dataValidation.isEmpty())) {
    	    dataValidations = null;
    	}
    }

    /*
     * Bringing together conditonalFormatting and extLst->conditionalFormatting,
     * Only rules will be available then via getConditionalFormattingRules()
     * Each rule which does not have an id will get a new unique id.
     *
     */
    final public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, @SuppressWarnings("unused") Object parent) {
    	if(conditionalFormattingRules==null) {
    		conditionalFormattingRules = getConditionalFormattingRules();

    		HashSet<String> ids = null;
    		final MutableInt uuIdCurrent = new MutableInt(0x10000000);

    		// first we will collect the old standard conditional formattings
    		if(conditionalFormatting!=null) {
	    		for(CTConditionalFormatting cf:conditionalFormatting) {
    				final List<String> ranges = cf.getSqrefList();
    				if(!ranges.isEmpty()) {
		    			final List<CTCfRule> cfRuleList = cf.getCfRule();
		    			for(CTCfRule cfRule:cfRuleList) {
		    				String id = cfRule.getId();
		    				if(!checkUUID(id)) {
		    					if(ids==null) {
		    						ids = collectIds();
		    					}
		    					id = createUUID(ids, uuIdCurrent);
		        				cfRule.setId(id);
		    				}
	    					conditionalFormattingRules.put(id, new CfRule(cfRule, new ArrayList<String>(ranges)));
		    			}
    				}
	    		}
	    		conditionalFormatting = null;
	    	}

    		// second is to collect conditionals from the extension
    		final CTExtensionList lst = getExtLst(false);
    		if(lst!=null) {
    			final CTExtension ext = lst.getExtensionByUri("{78C0D931-6437-407d-A8EE-F0AAD7539E65}", false);
    			if(ext!=null) {
    				final org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings _conditionalFormatting
    					=  (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings)((JAXBElement<?>)ext.getAny()).getValue();
    				for(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormatting cf:_conditionalFormatting.getConditionalFormatting()) {
	    				final List<String> ranges = cf.getSqrefList();
	    				if(!ranges.isEmpty()) {
	    					final List<org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule> cfRuleList = cf.getCfRule();
	    					for(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule cfRule:cfRuleList) {
	    						String id = cfRule.getId();
	    						if(!checkUUID(id)) {
	    							if(ids==null) {
	    								ids = collectIds();
	    							}
			    					id = createUUID(ids, uuIdCurrent);
			        				cfRule.setId(id);
	    						}
	    						final CfRule rule = conditionalFormattingRules.get(id);
    							if(rule!=null) {
    								rule.setR2(cfRule);
    							}
    							else {
    								conditionalFormattingRules.put(id, new CfRule(cfRule, new ArrayList<String>(ranges)));
    							}
	    					}
	    				}
	    			}
    				lst.getExt().remove(ext);
    			}
    		}
    	}
    }

    private HashSet<String> collectIds() {
    	final HashSet<String> ids = new HashSet<String>();
		if(conditionalFormatting!=null) {
    		for(CTConditionalFormatting cf:conditionalFormatting) {
    			final List<CTCfRule> cfRuleList = cf.getCfRule();
    			for(CTCfRule cfRule:cfRuleList) {
    				final String id = cfRule.getId();
    				if(id!=null&&!id.isEmpty()) {
    					ids.add(id.toUpperCase());
    				}
    			}
    		}
		}
		final CTExtensionList lst = getExtLst(false);
		if(lst!=null) {
			final CTExtension ext = lst.getExtensionByUri("{78C0D931-6437-407d-A8EE-F0AAD7539E65}", false);
			if(ext!=null) {
				final org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings _conditionalFormatting
					=  (org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormattings)((JAXBElement<?>)ext.getAny()).getValue();
				for(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTConditionalFormatting cf:_conditionalFormatting.getConditionalFormatting()) {
					final List<org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule> cfRuleList = cf.getCfRule();
					for(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule cfRule:cfRuleList) {
						final String id = cfRule.getId();
						if(id!=null&&!id.isEmpty()) {
							ids.add(id.toUpperCase());
						}
					}
				}
			}
		}
    	return ids;
    }

    private String createUUID(HashSet<String> ids, MutableInt uuIdCurrent) {
    	final String uId = "{00112233-0011-4011-8011-0011";
    	String nextId;
    	do {
    		uuIdCurrent.increment();
    		nextId = uId + Integer.toHexString(uuIdCurrent.intValue()) + "}";
    	}
    	while (ids.contains(nextId));
    	return nextId;
    }

    private boolean checkUUID(String id) {
    	if(id!=null&&id.length()==38) {
    		try {
	    		UUID.fromString(id.substring(1, 36));
	    		if(id.charAt(0)=='{'&&id.charAt(37)=='}') {
	    			return true;
	    		}
    		}
    		catch(IllegalArgumentException e) {
    			//
    		}
    	}
    	return false;
    }
}
