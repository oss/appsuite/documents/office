

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public class CellRepF extends CellRep implements iCellRepF {

	private CTCellFormula f;

	@Override
    public int getType() {
    	return 4;
    }

	@Override
    public CTCellFormula getF() {
        return f;
    }

	@Override
    public void setF(CTCellFormula value) {
        f = value;
    }
}
