
package org.xlsx4j.sml;

import java.util.List;
import com.openexchange.office.filter.core.component.Child;


public interface IConditionalFormats extends Child {

    public List<? extends IConditionalFormat> getConditionalFormat();
}
