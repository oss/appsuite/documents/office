

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public class CellRepVF extends CellRep implements iCellRepV, iCellRepF {

	private String v;
	private CTCellFormula f;

	@Override
    public int getType() {
    	return 5;
    }

	@Override
	public String getV() {
        return v;
    }

	@Override
    public void setV(String value) {
        v = value;
    }

	@Override
	public boolean isPreserveWhitespace() {
		return false;
	}

	@Override
    public CTCellFormula getF() {
        return f;
    }

	@Override
    public void setF(CTCellFormula value) {
        f = value;
    }
}
