
package org.xlsx4j.sml;

import java.util.List;

public interface IColorScale {

    public List<? extends ICfvo> getCfvo();

	public List<CTColor> getColor();
}
