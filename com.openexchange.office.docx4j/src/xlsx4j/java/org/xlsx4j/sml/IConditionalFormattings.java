
package org.xlsx4j.sml;

import java.util.List;

public interface IConditionalFormattings {

    public List<? extends IConditionalFormatting> getConditionalFormatting();
}
