/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.XmlUtils;
import org.xlsx4j.jaxb.Context;


/**
 * <p>Java class for CT_ExtensionList complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_ExtensionList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}EG_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ExtensionList", propOrder = {
    "ext"
})
public class CTExtensionList implements Cloneable
{
    protected List<CTExtension> ext;

    @Override
    public CTExtensionList clone() {
        final CTExtensionList clone = Context.getsmlObjectFactory().createCTExtensionList();
        if(ext!=null) {
            clone.ext = new ArrayList<CTExtension>(ext.size());
            for(CTExtension extension:ext) {
                clone.ext.add(XmlUtils.deepCopy(extension, /* TODO provide package */ null, Context.getJcSML()));
            }
        }
    	return clone;
    }

    /**
     * Gets the value of the ext property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ext property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExt().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTExtension }
     *
     *
     */
    public List<CTExtension> getExt() {
        if (ext == null) {
            ext = new ArrayList<CTExtension>();
        }
        return this.ext;
    }

    public CTExtension getExtensionByUri(String uri, boolean forceCreate) {
    	for(CTExtension extension:getExt()) {
    		if(extension.getUri().equals(uri)) {
    			return extension;
    		}
    	}
    	if(forceCreate) {
    		final CTExtension extension = new CTExtension();
    		extension.setUri(uri);
    		getExt().add(extension);
    		return extension;
    	}
    	return null;
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        if(ext!=null) {
            for(int i=ext.size()-1; i>=0; i--) {
                final CTExtension e = ext.get(i);

                // #56639# worksheets with empty dataValidations element will be reported to be an error, so we have to remove it...
                if(e.getUri().equals("{CCE6A557-97BC-4b89-ADB6-D9C93CAAB3DF}")) {
                    final Object o = e.getAny();
                    if(o instanceof JAXBElement<?>) {
                        final Object value = ((JAXBElement<?>)o).getValue();
                        if(value instanceof org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations) {
                            if(((org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTDataValidations)value).getDataValidation().isEmpty()) {
                                ext.remove(i);
                            }
                        }
                    }
                }
            }
        }
    }
}
