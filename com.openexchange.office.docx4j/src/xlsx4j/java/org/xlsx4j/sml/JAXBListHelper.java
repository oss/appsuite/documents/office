
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.CRC32;
import jakarta.xml.bind.JAXBContext;
import org.docx4j.XmlUtils;


public class JAXBListHelper<T> {

    final JAXBContext                   jc;
    final private List<T>               values;
    final private List<Integer>         hashedValues = new ArrayList<Integer>();

    public JAXBListHelper(List<T> t, JAXBContext jc) {
        this.values = t;
        this.jc = jc;
    }

    private static int getHash(byte[] data) {
        final CRC32 crc32 = new CRC32();
        crc32.update(data);
        return (int)crc32.getValue() | 1;
    }

    public int getOrApply(T t) {

        final byte[] sourceData = XmlUtils.marshalToByteArray(null, t, true, jc);
        final int sourceHash = getHash(sourceData);

        int i;
        for(i = 0; i<values.size();i++) {

            byte[] destData = null;
            T dest = values.get(i);

            // creating hash
            if(i>=hashedValues.size()) {
                destData = XmlUtils.marshalToByteArray(null, dest, true, jc);
                hashedValues.add(getHash(destData));
            }
            if(sourceHash==hashedValues.get(i)) {
                // hash found... now check for identity
                if(destData==null) {
                    destData = XmlUtils.marshalToByteArray(null, dest, true, jc);
                }
                if(Arrays.equals(sourceData, destData)) {
                    return i;
                }
            }
        }

        // adding new object
        hashedValues.add(sourceHash);
        values.add(t);
        return i;
    }
}
