

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public class CellRepPreserveV extends CellRepV {

	@Override
    public int getType() {
    	return 3;
    }

	@Override
	public boolean isPreserveWhitespace() {
		return true;
	}
}
