
package org.xlsx4j.sml;

public interface ISheetAccess {

    public CTDrawing getDrawing();

    public void setDrawing(CTDrawing value);

    public CTLegacyDrawing getLegacyDrawing(boolean forceCreate);

    public void setLegacyDrawing(CTLegacyDrawing value);
    
    public CTLegacyDrawing getLegacyDrawingHF();
    
    public void setLegacyDrawingHF(CTLegacyDrawing value);

    public CTDrawingHF getDrawingHF();

    public void setDrawingHF(CTDrawingHF value);

    public CTPageMargins getPageMargins();

    public void setPageMargins(CTPageMargins value);
    
    public CTHeaderFooter getHeaderFooter();

    public void setHeaderFooter(CTHeaderFooter value);

    public CTSheetBackgroundPicture getPicture();

    public void setPicture(CTSheetBackgroundPicture value);

    public CTWebPublishItems getWebPublishItems();

    public void setWebPublishItems(CTWebPublishItems value);

    public CTExtensionList getExtLst(boolean forceCreate);

    public void setExtLst(CTExtensionList value);
}
