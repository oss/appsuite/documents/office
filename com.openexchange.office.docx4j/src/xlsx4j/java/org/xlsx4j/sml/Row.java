/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.jaxb.Context;
import com.openexchange.office.filter.core.spreadsheet.CellRef;


/**
 * <p>Java class for CT_Row complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Row">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="c" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Cell" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="r" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="spans" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_CellSpans" />
 *       &lt;attribute name="s" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *       &lt;attribute name="customFormat" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="ht" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="hidden" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="customHeight" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="outlineLevel" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" default="0" />
 *       &lt;attribute name="collapsed" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="thickTop" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="thickBot" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="ph" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Row", propOrder = {
    "c",
    "extLst",
    "r",
    "spans",
    "s",
    "customFormat",
    "ht",
    "hidden",
    "customHeight",
    "outlineLevel",
    "collapsed",
    "thickTop",
    "thickBot",
    "ph"
})
@XmlRootElement(name = "row")
public class Row
{
    protected List<Cell> c;
    @XmlTransient
    private TreeMap<Integer, Cell> cells_impl;
    @XmlTransient
    private RowDataReadonly rowData = RowDataReadonly.EmptyRow;
    @XmlAttribute(name = "r")
    @XmlSchemaType(name = "unsignedInt")
    protected Long r;

    public RowDataReadonly getRowData() {
    	return rowData;
    }

    public void setRowData(RowDataReadonly value) {
    	rowData = value;
    }

    /**
     * Gets the value of the c property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the c property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getC().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cell }
     *
     *
     */
    private List<Cell> getListArray() {
        if (c == null) {
        	if(cells_impl!=null) {
        		c = new ArrayList<Cell>(cells_impl.values());
        	}
        	else {
        		c = new ArrayList<Cell>();
        	}
        	cells_impl = null;
        }
        return c;
    }

    private Map<Integer, Cell> getTreeMap() {
        if(cells_impl==null) {
            cells_impl = new TreeMap<Integer, Cell>();
            if(c!=null) {
            	for(Cell cell:c) {
            		cells_impl.put(cell.getColumn(), cell);
            	}
            	c = null;
            }
        }
        return cells_impl;
    }

    public Cell getCell(int column, boolean forceCreate) {
        final Map<Integer, Cell> treeMap = getTreeMap();
        Cell cell = treeMap.get(column);
        if(forceCreate&&cell==null) {
            if(column<0||column>16383) {
                throw new RuntimeException();
            }
            // creating a new cell...
            cell = Context.getsmlObjectFactory().createCell();
            cell.setColumn(column);
            treeMap.put(column, cell);
        }
        return cell;
    }

    public void insertCells(int start, int insertCount) {

    	final List<Cell> cells = getListArray();
        for(int i=0; i<cells.size(); ++i) {
            final Cell cell = cells.get(i);
            final int col = cell.getColumn();
            final int to = col+insertCount;
            if (to>=16384) {
                cells.remove(i--);
            } else if (col>=start) {
                cell.setColumn(to);
            }
        }
    }

    public void deleteCells(int start, int deleteCount) {

    	final List<Cell> cells = getListArray();
    	final int end = start+deleteCount-1;
    	for(int i=0; i<cells.size(); i++) {
    		final Cell cell = cells.get(i);
    		final int col = cell.getColumn();
    		if (end<col) {
    			cell.setColumn(col - deleteCount);
    		} else if(start<=col) {
    			cells.remove(i--);
    		}
    	}
    }

    public void clearCellRange(int start, int repetition) {

    	final Map<Integer, Cell> treeMap = getTreeMap();
    	for(int i=0; i<repetition; i++) {
    		treeMap.remove(start++);
    	}
    }

    public int getCellCount() {
        return c!=null?c.size():cells_impl!=null?cells_impl.size():0;
    }

    public boolean isDefault() {
    	return rowData.equals(RowDataReadonly.EmptyRow);
    }

    public Iterator<Cell> createCellIterator() {
    	if(c!=null) {
    		return c.iterator();
    	}
    	else if (cells_impl!=null) {
    		return cells_impl.values().iterator();
    	}
    	return getListArray().iterator();
    }

    /**
     * Gets the value of the r property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getR() {
        return r;
    }

    /**
     * Sets the value of the r property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setR(Long value) {
        this.r = value;
    }

    public int getRow() {
    	return getR()!=null?getR().intValue()-1:-1;
    }

    public void setRow(int row) {
    	r = Long.valueOf(row+1);
    }

    /**
     * Gets the value of the s property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @XmlAttribute(name = "s")
    @XmlSchemaType(name = "unsignedInt")
    public Long getS() {
    	return rowData.getS();
    }

    public void setS(Long value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData  = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setS(value);
    }

    public long getStyle() {
    	if(rowData.getS()==null) {
    		return 0L;
    	}
    	return rowData.getS().longValue();
    }

    @XmlAttribute(name = "ht")
    public Double getHt() {
        return rowData.getHt();
    }

    public void setHt(Double value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setHt(value);
    }

    @XmlAttribute(name = "outlineLevel")
    @XmlSchemaType(name = "unsignedByte")
    public Short getOutlineLevel() {
    	return rowData.getOutlineLevel();
    }

    public void setOutlineLevel(Short value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setOutlineLevel(value);
    }

    @XmlAttribute(name = "spans")
    public List<String> getSpans() {
        return null;
    }

    public void setSpans(@SuppressWarnings("unused") List<String> value) {
        // we are removing spans when writing as this is only an
        // optimizing feature.. so we don't need to update these spans
    }

    @XmlElement
    public CTExtensionList getExtLst() {
        return rowData.getExtLst();
    }

    public void setExtLst(CTExtensionList value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setExtLst(value);
    }

    @XmlAttribute
    public Boolean isCustomFormat() {
    	return rowData.isCustomFormat();
    }

    public void setCustomFormat(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setCustomFormat(value);
    }

    @XmlAttribute
    public Boolean isHidden() {
    	return rowData.isHidden();
    }

    public void setHidden(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setHidden(value);
    }

    @XmlAttribute
    public Boolean isCustomHeight() {
    	return rowData.isCustomHeight();
    }

    public void setCustomHeight(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setCustomHeight(value);
    }

    @XmlAttribute
    public Boolean isCollapsed() {
    	return rowData.isCollapsed();
    }

    public void setCollapsed(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setCollapsed(value);
    }

    @XmlAttribute
    public Boolean isThickTop() {
    	return rowData.isThickTop();
    }

    public void setThickTop(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setThickTop(value);
    }

    @XmlAttribute
    public Boolean isThickBot() {
    	return rowData.isThickBot();
    }

    public void setThickBot(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setThickBot(value);
    }

    @XmlAttribute
    public Boolean isPh() {
    	return rowData.isPh();
    }

    public void setPh(Boolean value) {
    	if(!(rowData instanceof RowDataWritable)) {
    		rowData = new RowDataWritable(rowData);
    	}
    	((RowDataWritable)rowData).setPh(value);
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        final List<Cell> cells = getListArray();
        for(Cell cell:cells) {
        	// we have to convert cell column into cell.R
    		cell.setR(CellRef.getCellRef(cell.getColumn(), getRow()));
        }
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(Unmarshaller unmarshaller, Object _parent) {
        if(r==null) {
        	setRow(((SheetData)_parent).getLastRowNumber()+1);
        }

        int currentColumn = 0;
        Long maxSi = null;
        final Iterator<Cell> iterator = createCellIterator();
        while(iterator.hasNext()) {
        	final Cell cell = iterator.next();
        	// converting cell.R to column
        	final String r1 = cell.getR();
        	if(r1!=null) {
        		currentColumn = CellRef.createCellRef(r1).getColumn();
        	}
        	cell.setColumn(currentColumn++);
        	final CTCellFormula f = cell.getF();
        	if(f!=null) {
        		final Long si = f.getSi();
	        	if(si!=null) {
	        		if(maxSi==null) {
	        			maxSi = si;
	        		}
	        		else if(si>maxSi) {
	        			maxSi = si;
	        		}
	        	}
        	}
        	// using cell cache
        	((SheetData)_parent).applyCachedCellData(cell);
        }
        // using the row cache
        ((SheetData)_parent).applyCachedRowData(this);

        if(maxSi!=null) {
            ((SheetData)_parent).setSi(maxSi);
        }
        org.docx4j.jaxb.Context.abortOnLowMemory(unmarshaller);
    }
}
