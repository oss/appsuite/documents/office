

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public interface IAutoFilterAccessor {

    public CTAutoFilter getAutoFilter(boolean forceCreate);

    public void setAutoFilter(CTAutoFilter value);
}
