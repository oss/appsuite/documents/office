
package org.xlsx4j.sml;

import java.util.List;

public interface ICfRule {

	public String getId();

	public void setId(String id);

	public IColorScale getColorScale();

    public IDataBar getDataBar();

    public List<String> getFormula();

	public boolean isPercent();

    public void setPercent(Boolean value);

    public STCfType getType();

    public void setType(STCfType value);

    public Integer getStdDev();

    public void setStdDev(Integer value);

    public boolean isEqualAverage();

    public void setEqualAverage(Boolean value);

    public boolean isAboveAverage();

    public void setAboveAverage(Boolean value);

    public boolean isBottom();

    public void setBottom(Boolean value);

    public STConditionalFormattingOperator getOperator();

    public void setOperator(STConditionalFormattingOperator value);

    public String getText();

    public void setText(String value);

    public Integer getPriority();

    public void setPriority(int value);

    public boolean isStopIfTrue();

    public void setStopIfTrue(Boolean value);

    public STTimePeriod getTimePeriod();

    public void setTimePeriod(STTimePeriod value);

    public Long getRank();

    public void setRank(Long value);

    public CTDxf getDxf(CTStylesheet stylesheet);

    public void setDxf(CTStylesheet stylesheet, CTDxf dxf);

    public IIconSet getIconSet();

}
