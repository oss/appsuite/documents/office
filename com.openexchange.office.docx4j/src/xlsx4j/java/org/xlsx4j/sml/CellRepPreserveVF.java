

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public class CellRepPreserveVF extends CellRepVF {

	@Override
    public int getType() {
    	return 7;
    }

	@Override
    public boolean isPreserveWhitespace() {
    	return true;
    }
}
