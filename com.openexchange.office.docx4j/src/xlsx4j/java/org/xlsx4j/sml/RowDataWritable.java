
package org.xlsx4j.sml;

import org.docx4j.XmlUtils;
import org.xlsx4j.jaxb.Context;

public class RowDataWritable extends RowDataReadonly {

	RowDataWritable(RowDataReadonly rowData) {
		s = rowData.s;
		ht = rowData.ht;
		outlineLevel = rowData.outlineLevel;
		flags = rowData.flags;
		if(rowData.extLst!=null) {
			extLst = XmlUtils.deepCopy(rowData.extLst, /* TODO provide package */ null, Context.getJcSML());
		}
	}
	void setS(Long value) {
		s = value;
	}
	void setHt(Double value) {
		ht = value;
	}
	void setOutlineLevel(Short value) {
		outlineLevel = value;
	}
    public void setCustomFormat(Boolean value) {
		flags&=~0x3;
    	if(value!=null) {
    		flags|=value.booleanValue()?0x3:0x1;
    	}
    }
    public void setHidden(Boolean value) {
		flags&=~0xc;
    	if(value!=null) {
    		flags|=value.booleanValue()?0xc:0x4;
    	}
    }
    public void setCustomHeight(Boolean value) {
		flags&=~0x30;
    	if(value!=null) {
    		flags|=value.booleanValue()?0x30:0x10;
    	}
    }
    public void setCollapsed(Boolean value) {
		flags&=~0xc0;
		if(value!=null) {
			flags|=value.booleanValue()?0xc0:0x40;
		}
    }
    public void setThickTop(Boolean value) {
		flags&=~0x300;
    	if(value!=null) {
    		flags|=value.booleanValue()?0x300:0x100;
    	}
    }
    public void setThickBot(Boolean value) {
		flags&=~0xc00;
    	if(value!=null) {
    		flags|=value.booleanValue()?0xc00:0x400;
    	}
    }
    public void setPh(Boolean value) {
		flags&=~0x3000;
    	if(value!=null) {
    		flags|=value.booleanValue()?0x3000:0x1000;
    	}
    }
	void setExtLst(CTExtensionList value) {
		extLst = value;
	}
}
