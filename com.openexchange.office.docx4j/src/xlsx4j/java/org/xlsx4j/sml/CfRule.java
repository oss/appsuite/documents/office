

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

import java.util.Iterator;
import java.util.List;

public class CfRule {

	public class CfRuleIter implements Iterator<ICfRule>{

		private ICfRule r1;
		private ICfRule r2;

		CfRuleIter(CfRule cfRule) {
			this.r1 = cfRule.getR1();
			this.r2 = cfRule.getR2();
		}

		@Override
		public boolean hasNext() {
			return r1!=null||r2!=null;
		}

		@Override
		public ICfRule next() {
			ICfRule ret = null;
			if(r1!=null) {
				ret = r1;
				r1 = null;
			}
			else {
				ret = r2;
				r2 = null;
			}
			return ret;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private List<String> ranges;
	private CTCfRule r1;
	private org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule r2;

	public CfRule(CTCfRule r1, List<String> ranges) {
		this.r1 = r1;
		this.r2 = null;
		this.ranges = ranges;
	}

	public CfRule(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule r2, List<String> ranges) {
		this.r1 = null;
		this.r2 = r2;
		this.ranges = ranges;
	}

	public String getType() {
		if(r1!=null&&r2==null) {
			return "A";
		}
		else if(r1==null&&r2!=null) {
			return "B";
		}
		return "C";
	}

	public List<String> getRanges() {
		return ranges;
	}

	public void setRanges(List<String> ranges) {
		this.ranges = ranges;
	}

	public CTCfRule getR1() {
		return r1;
	}

	public org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule getR2() {
		return r2;
	}

	public void setR2(org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTCfRule r2) {
		this.r2 = r2;
	}

	public CfRuleIter iterator() {
		return new CfRuleIter(this);
	}
}
