/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;


import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import org.xlsx4j.jaxb.Context;

/**
 * <p>Java class for CT_Font complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Font">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="name" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_FontName" minOccurs="0"/>
 *         &lt;element name="charset" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_IntProperty" minOccurs="0"/>
 *         &lt;element name="family" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_FontFamily" minOccurs="0"/>
 *         &lt;element name="b" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="i" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="strike" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="outline" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="shadow" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="condense" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="extend" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_BooleanProperty" minOccurs="0"/>
 *         &lt;element name="color" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="sz" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_FontSize" minOccurs="0"/>
 *         &lt;element name="u" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_UnderlineProperty" minOccurs="0"/>
 *         &lt;element name="vertAlign" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_VerticalAlignFontProperty" minOccurs="0"/>
 *         &lt;element name="scheme" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_FontScheme" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Font", propOrder = {
		"name",
		"charset",
		"family",
		"b",
		"i",
		"strike",
		"outline",
		"shadow",
		"condense",
		"extend",
		"color",
		"sz",
		"u",
		"vertAlign",
		"scheme"
})
@XmlRootElement(name="font")
public class CTFont implements Cloneable
{
	@XmlElement
    protected CTBooleanProperty strike;
    @XmlElement
    protected CTBooleanProperty shadow;
    @XmlElement
    protected CTBooleanProperty b;
    @XmlElement
    protected CTBooleanProperty outline;
    @XmlElement
    protected CTFontSize sz;
    @XmlElement
    protected CTFontScheme scheme;
    @XmlElement
    protected CTBooleanProperty extend;
    @XmlElement
    protected CTFontFamily family;
    @XmlElement
    protected CTFontName name;
    @XmlElement
    protected CTColor color;
    @XmlElement
    protected CTUnderlineProperty u;
    @XmlElement
    protected CTBooleanProperty i;
    @XmlElement
    protected CTIntProperty charset;
    @XmlElement
    protected CTVerticalAlignFontProperty vertAlign;
    @XmlElement
    protected CTBooleanProperty condense;

    @Override
    public CTFont clone() {
    	final CTFont clone = Context.getsmlObjectFactory().createCTFont();
    	if(strike!=null) {
    		clone.strike = strike.clone();
    	}
    	if(shadow!=null) {
    		clone.shadow = shadow.clone();
    	}
    	if(b!=null) {
    		clone.b = b.clone();
    	}
    	if(outline!=null) {
    		clone.outline = outline.clone();
    	}
    	if(sz!=null) {
    		clone.sz = sz.clone();
    	}
    	if(scheme!=null) {
    		clone.scheme = scheme.clone();
    	}
    	if(extend!=null) {
    		clone.extend = extend.clone();
    	}
    	if(family!=null) {
    		clone.family = family.clone();
    	}
    	if(name!=null) {
    		clone.name = name.clone();
    	}
    	if(color!=null) {
    		clone.color = color.clone();
    	}
    	if(u!=null) {
    		clone.u = u.clone();
    	}
    	if(i!=null) {
    		clone.i = i.clone();
    	}
    	if(charset!=null) {
    		clone.charset = charset.clone();
    	}
    	if(vertAlign!=null) {
    		clone.vertAlign = vertAlign.clone();
    	}
    	if(condense!=null) {
    		clone.condense = condense.clone();
    	}
    	return clone;
    }
    
    public CTBooleanProperty getStrike() {
		return strike;
	}
	public void setStrike(CTBooleanProperty strike) {
		this.strike = strike;
	}
	public CTBooleanProperty getShadow() {
		return shadow;
	}
	public void setShadow(CTBooleanProperty shadow) {
		this.shadow = shadow;
	}
	public CTBooleanProperty getB() {
		return b;
	}
	public void setB(CTBooleanProperty b) {
		this.b = b;
	}
	public CTBooleanProperty getOutline() {
		return outline;
	}
	public void setOutline(CTBooleanProperty outline) {
		this.outline = outline;
	}
	public CTFontSize getSz() {
		return sz;
	}
	public void setSz(CTFontSize sz) {
		this.sz = sz;
	}
	public CTFontScheme getScheme() {
		return scheme;
	}
	public void setScheme(CTFontScheme scheme) {
		this.scheme = scheme;
	}
	public CTBooleanProperty getExtend() {
		return extend;
	}
	public void setExtend(CTBooleanProperty extend) {
		this.extend = extend;
	}
	public CTFontFamily getFamily() {
		return family;
	}
	public void setFamily(CTFontFamily family) {
		this.family = family;
	}
	public CTFontName getName() {
		return name;
	}
	public void setName(CTFontName name) {
		this.name = name;
	}
	public CTColor getColor() {
		return color;
	}
	public void setColor(CTColor color) {
		this.color = color;
	}
	public CTUnderlineProperty getU() {
		return u;
	}
	public void setU(CTUnderlineProperty u) {
		this.u = u;
	}
	public CTBooleanProperty getI() {
		return i;
	}
	public void setI(CTBooleanProperty i) {
		this.i = i;
	}
	public CTIntProperty getCharset() {
		return charset;
	}
	public void setCharset(CTIntProperty charset) {
		this.charset = charset;
	}
	public CTVerticalAlignFontProperty getVertAlign() {
		return vertAlign;
	}
	public void setVertAlign(CTVerticalAlignFontProperty vertAlign) {
		this.vertAlign = vertAlign;
	}
	public CTBooleanProperty getCondense() {
		return condense;
	}
	public void setCondense(CTBooleanProperty condense) {
		this.condense = condense;
	}
}
