
package org.xlsx4j.sml;

import java.util.List;
import com.openexchange.office.filter.core.component.Child;


public interface IDataValidation extends Child {

    /**
     * Gets the value of the formula1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormula1();

    /**
     * Sets the value of the formula1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormula1(String value);

    /**
     * Gets the value of the formula2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormula2();

    /**
     * Sets the value of the formula2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormula2(String value);

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link STDataValidationType }
     *
     */
    public STDataValidationType getType();

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link STDataValidationType }
     *
     */
    public void setType(STDataValidationType value);

    /**
     * Gets the value of the errorStyle property.
     *
     * @return
     *     possible object is
     *     {@link STDataValidationErrorStyle }
     *
     */
    public STDataValidationErrorStyle getErrorStyle();

    /**
     * Sets the value of the errorStyle property.
     *
     * @param value
     *     allowed object is
     *     {@link STDataValidationErrorStyle }
     *
     */
    public void setErrorStyle(STDataValidationErrorStyle value);

    /**
     * Gets the value of the imeMode property.
     *
     * @return
     *     possible object is
     *     {@link STDataValidationImeMode }
     *
     */
    public STDataValidationImeMode getImeMode();

    /**
     * Sets the value of the imeMode property.
     *
     * @param value
     *     allowed object is
     *     {@link STDataValidationImeMode }
     *
     */
    public void setImeMode(STDataValidationImeMode value);

    /**
     * Gets the value of the operator property.
     *
     * @return
     *     possible object is
     *     {@link STDataValidationOperator }
     *
     */
    public STDataValidationOperator getOperator();

    /**
     * Sets the value of the operator property.
     *
     * @param value
     *     allowed object is
     *     {@link STDataValidationOperator }
     *
     */
    public void setOperator(STDataValidationOperator value);

    /**
     * Gets the value of the allowBlank property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isAllowBlank();

    /**
     * Sets the value of the allowBlank property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setAllowBlank(Boolean value);

    /**
     * Gets the value of the showDropDown property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isShowDropDown();

    /**
     * Sets the value of the showDropDown property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setShowDropDown(Boolean value);

    /**
     * Gets the value of the showInputMessage property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isShowInputMessage();

    /**
     * Sets the value of the showInputMessage property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setShowInputMessage(Boolean value);

    /**
     * Gets the value of the showErrorMessage property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isShowErrorMessage();

    /**
     * Sets the value of the showErrorMessage property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setShowErrorMessage(Boolean value);

    /**
     * Gets the value of the errorTitle property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorTitle();

    /**
     * Sets the value of the errorTitle property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorTitle(String value);

    /**
     * Gets the value of the error property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getError();

    /**
     * Sets the value of the error property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setError(String value);

    /**
     * Gets the value of the promptTitle property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPromptTitle();

    /**
     * Sets the value of the promptTitle property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPromptTitle(String value);

    /**
     * Gets the value of the prompt property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPrompt();

    /**
     * Sets the value of the prompt property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPrompt(String value);

    /**
     * Gets the value of the sqref property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sqref property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSqref().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    public List<String> getsqref();
}
