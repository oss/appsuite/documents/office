

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public interface iCellRepV {

    public String getV();

    public void setV(String value);

    public boolean isPreserveWhitespace();
}
