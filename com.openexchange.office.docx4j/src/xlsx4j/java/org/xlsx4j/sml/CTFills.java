/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.jaxb.Context;

/**
 * <p>Java class for CT_Fills complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Fills">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fill" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Fill" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Fills", propOrder = {
    "fill"
})
public class CTFills
{
    protected List<CTFill> fill;
    @XmlAttribute(name = "count")
    @XmlSchemaType(name = "unsignedInt")
    protected Long count;
    @XmlTransient
    protected JAXBListHelper<CTFill> fillHelper;

    /**
     * Gets the value of the fill property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fill property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFill().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTFill }
     *
     *
     */
    public List<CTFill> getFill() {
        if (fill == null) {
            fill = new ArrayList<CTFill>();
        }
        return this.fill;
    }

    /*
     * returns the index of the given CTFont, it is appended
     * to the list if the CTFont is not equal to another
     */
    public int getOrApply(CTFill ctFill) {
        if(fillHelper==null) {
            fillHelper = new JAXBListHelper<CTFill>(getFill(), Context.getJcSML());
        }
        return fillHelper.getOrApply(ctFill);
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        this.count = fill!=null?(long)fill.size():0;
    }
}
