/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.List;
import java.util.Map;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_Stylesheet complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Stylesheet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numFmts" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_NumFmts" minOccurs="0"/>
 *         &lt;element name="fonts" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Fonts" minOccurs="0"/>
 *         &lt;element name="fills" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Fills" minOccurs="0"/>
 *         &lt;element name="borders" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Borders" minOccurs="0"/>
 *         &lt;element name="cellStyleXfs" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CellStyleXfs" minOccurs="0"/>
 *         &lt;element name="cellXfs" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CellXfs" minOccurs="0"/>
 *         &lt;element name="cellStyles" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CellStyles" minOccurs="0"/>
 *         &lt;element name="dxfs" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Dxfs" minOccurs="0"/>
 *         &lt;element name="tableStyles" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_TableStyles" minOccurs="0"/>
 *         &lt;element name="colors" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Colors" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Stylesheet", propOrder = {
    "numFmts",
    "fonts",
    "fills",
    "borders",
    "cellStyleXfs",
    "cellXfs",
    "cellStyles",
    "dxfs",
    "tableStyles",
    "colors",
    "extLst"
})
@XmlRootElement(name = "styleSheet")
public class CTStylesheet
{
    protected CTNumFmts numFmts;
    protected CTFonts fonts;
    protected CTFills fills;
    protected CTBorders borders;
    protected CTCellStyleXfs cellStyleXfs;
    protected CTCellXfs cellXfs;
    protected CTCellStyles cellStyles;
    protected CTDxfs dxfs;
    protected CTTableStyles tableStyles;
    protected CTColors colors;
    protected CTExtensionList extLst;

    /**
     * Returns the list of all number format codes.
     */
    public CTNumFmts getNumberFormats(boolean forceCreate) {
    	if (forceCreate && numFmts==null) {
    		numFmts = new CTNumFmts();
    	}
        return numFmts;
    }

    /**
     * Returns the CTNumFmt by Id, if the Id matches a predefined
     * numberFormat then null might be returned.
     */
    public CTNumFmt getNumberFormatById(long id) {
        if(numFmts!=null) {
            final List<CTNumFmt> numFmtList = numFmts.getNumFmt();
            for (CTNumFmt numFmt:numFmtList) {
                if(numFmt.getNumFmtId()==id) {
                    return numFmt;
                }
            }
        }
        return null;
    }

    public void insertNumberFormat(long formatId, String formatCode) {
    	getNumberFormats(true).insertNumberFormat(formatId, formatCode);
    }

    public void insertNumberFormats(Map<Long, String> formatCodes) {
    	CTNumFmts numF = getNumberFormats(true);
    	for (Map.Entry<Long, String> entry : formatCodes.entrySet()) {
    		numF.insertNumberFormat(entry.getKey(), entry.getValue());
		}
    }

    public void deleteNumberFormat(long formatId) {
    	if (numFmts != null) { numFmts.deleteNumberFormat(formatId); }
    }

    /**
     * Gets the value of the fonts property.
     *
     * @return
     *     possible object is
     *     {@link CTFonts }
     *
     */
    public CTFonts getFonts() {
        return fonts;
    }

    /**
     * Sets the value of the fonts property.
     *
     * @param value
     *     allowed object is
     *     {@link CTFonts }
     *
     */
    public void setFonts(CTFonts value) {
        this.fonts = value;
    }


    /**
     * Returns the CTFont by index. On error null is
     * returned.
     */
    public CTFont getFontByIndex(Long index) {
        CTFont ret = null;
        if(fonts!=null&&index!=null) {
            List<CTFont> fontList = fonts.getFont();
            if(index>=0 && index<=Integer.MAX_VALUE && index<fontList.size()) {
                ret = fontList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /*
    *
    */
   public long getOrApplyFont(CTFont font) {
       return fonts.getOrApply(font);
   }

    /**
     * Gets the value of the fills property.
     *
     * @return
     *     possible object is
     *     {@link CTFills }
     *
     */
    public CTFills getFills() {
        return fills;
    }

    /**
     * Sets the value of the fills property.
     *
     * @param value
     *     allowed object is
     *     {@link CTFills }
     *
     */
    public void setFills(CTFills value) {
        this.fills = value;
    }

    /**
     * Returns CTFill by index. On error null is
     * returned.
     */
    public CTFill getFillByIndex(Long index) {
        CTFill ret = null;
        if(fills!=null&&index!=null) {
            final List<CTFill> fillList = fills.getFill();
            if(index>=0 && index<=Integer.MAX_VALUE && index<fillList.size()) {
                ret = fillList.get((int)index.longValue());
            }
        }
        return ret;
    }

    public long getOrApplyFill(CTFill fill) {
        return fills.getOrApply(fill);
    }

    /**
     * Gets the value of the borders property.
     *
     * @return
     *     possible object is
     *     {@link CTBorders }
     *
     */
    public CTBorders getBorders() {
        return borders;
    }

    /**
     * Sets the value of the borders property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBorders }
     *
     */
    public void setBorders(CTBorders value) {
        this.borders = value;
    }

    /**
     * Returns CTBorder by index. On error null is
     * returned.
     */
    public CTBorder getBorderByIndex(Long index) {
        CTBorder ret = null;
        if(borders!=null&&index!=null) {
            final List<CTBorder> borderList = borders.getBorder();
            if(index>=0 && index<=Integer.MAX_VALUE && index<borderList.size()) {
                ret = borderList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /*
    *
    */
   public long getOrApplyBorder(CTBorder border) {
       return borders.getOrApply(border);
   }

    /**
     * Gets the value of the cellStyleXfs property.
     *
     * @return
     *     possible object is
     *     {@link CTCellStyleXfs }
     *
     */
    public CTCellStyleXfs getCellStyleXfs() {
        return cellStyleXfs;
    }

    /**
     * Sets the value of the cellStyleXfs property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCellStyleXfs }
     *
     */
    public void setCellStyleXfs(CTCellStyleXfs value) {
        this.cellStyleXfs = value;
    }

    /**
     * Returns CellStyleXf by index. On error null is
     * returned.
     */
    public CTXf getCellStyleXfsByIndex(Long index) {
        CTXf ret = null;
        if(cellStyleXfs!=null&&index!=null) {
            final List<CTXf> cellStyleXfList = cellStyleXfs.getXf();
            if(index>=0 && index<=Integer.MAX_VALUE && index<cellStyleXfList.size()) {
                ret = cellStyleXfList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /**
     * Gets the value of the cellXfs property.
     *
     * @return
     *     possible object is
     *     {@link CTCellXfs }
     *
     */
    public CTCellXfs getCellXfs() {
        return cellXfs;
    }

    /**
     * Sets the value of the cellXfs property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCellXfs }
     *
     */
    public void setCellXfs(CTCellXfs value) {
        this.cellXfs = value;
    }

    /**
     * Returns CellXf by index. On error null is
     * returned.
     */
    public CTXf getCellXfByIndex(Long index) {
        CTXf ret = null;
        if(cellXfs!=null&&index!=null) {
            final List<CTXf> cellXfList = cellXfs.getXf();
            if(index>=0 && index<=Integer.MAX_VALUE && index<cellXfList.size()) {
                ret = cellXfList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /*
     *
     */
    public long getOrApplyCellXf(CTXf xf) {
        return cellXfs.getOrApply(xf);
    }


    /**
     * Gets the value of the cellStyles property.
     *
     * @return
     *     possible object is
     *     {@link CTCellStyles }
     *
     */
    public CTCellStyles getCellStyles() {
        return cellStyles;
    }

    /**
     * Sets the value of the cellStyles property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCellStyles }
     *
     */
    public void setCellStyles(CTCellStyles value) {
        this.cellStyles = value;
    }

    /**
     * Returns CellStyle by index. On error null is
     * returned.
     */
    public CTCellStyle getCellStyleByIndex(Long index) {
        CTCellStyle ret = null;
        if(cellStyles!=null&&index!=null) {
            final List<CTCellStyle> cellStyleList = cellStyles.getCellStyle();
            if(index>=0 && index<=Integer.MAX_VALUE && index<cellStyleList.size()) {
                ret = cellStyleList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /**
     * Gets the value of the dxfs property.
     *
     * @return
     *     possible object is
     *     {@link CTDxfs }
     *
     */
    public CTDxfs getDxfs() {
        return dxfs;
    }

    /**
     * Sets the value of the dxfs property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDxfs }
     *
     */
    public void setDxfs(CTDxfs value) {
        this.dxfs = value;
    }

    /**
     * Returns Dxf by index. On error null is
     * returned.
     */
    public CTDxf getDxfsByIndex(Long index) {
        CTDxf ret = null;
        if(dxfs!=null&&index!=null) {
            final List<CTDxf> dxfList = dxfs.getDxf();
            if(index>=0 && index<=Integer.MAX_VALUE && index<dxfList.size()) {
                ret = dxfList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /**
     * Gets the value of the tableStyles property.
     *
     * @return
     *     possible object is
     *     {@link CTTableStyles }
     *
     */
    public CTTableStyles getTableStyles(boolean forceCreate) {
        if(tableStyles==null&&forceCreate) {
            tableStyles = new CTTableStyles();
            tableStyles.setDefaultTableStyle("TableStyleMedium2");
            tableStyles.setDefaultPivotStyle("PivotStyleLight16");
        }
        return tableStyles;
    }

    /**
     * Sets the value of the tableStyles property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTableStyles }
     *
     */
    public void setTableStyles(CTTableStyles value) {
        this.tableStyles = value;
    }

    /**
     * Returns the TableStyle by index. On error null is
     * returned.
     */
    public CTTableStyle getTableStylesByIndex(Long index) {
        CTTableStyle ret = null;
        if(tableStyles!=null&&index!=null) {
            final List<CTTableStyle> tableStyleList = tableStyles.getTableStyle();
            if(index>=0 && index<=Integer.MAX_VALUE && index<tableStyleList.size()) {
                ret = tableStyleList.get((int)index.longValue());
            }
        }
        return ret;
    }

    /**
     * Gets the value of the colors property.
     *
     * @return
     *     possible object is
     *     {@link CTColors }
     *
     */
    public CTColors getColors() {
        return colors;
    }

    /**
     * Sets the value of the colors property.
     *
     * @param value
     *     allowed object is
     *     {@link CTColors }
     *
     */
    public void setColors(CTColors value) {
        this.colors = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }
}
