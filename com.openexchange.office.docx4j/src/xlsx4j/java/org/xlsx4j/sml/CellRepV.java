

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public class CellRepV extends CellRep implements iCellRepV {

	private String v;

	@Override
    public int getType() {
    	return 1;
    }

	@Override
	public String getV() {
        return v;
    }

	@Override
    public void setV(String value) {
        v = value;
    }

	@Override
	public boolean isPreserveWhitespace() {
		return false;
	}
}
