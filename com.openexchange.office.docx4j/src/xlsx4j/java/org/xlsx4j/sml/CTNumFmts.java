/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_NumFmts complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_NumFmts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numFmt" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_NumFmt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_NumFmts", propOrder = {
    "numFmt"
})
public class CTNumFmts
{
    protected List<CTNumFmt> numFmt;
    @XmlAttribute(name = "count")
    @XmlSchemaType(name = "unsignedInt")
    protected Long count;

    /**
     * Gets the value of the numFmt property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the numFmt property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumFmt().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTNumFmt }
     *
     *
     */
    public List<CTNumFmt> getNumFmt() {
        if (numFmt == null) {
            numFmt = new ArrayList<CTNumFmt>();
        }
        return this.numFmt;
    }

    /**
     * Replaces an existing number format, or appends a new number format.
     */
    public void insertNumberFormat(long formatId, String formatCode) {

    	// force creation of the missing list
    	getNumFmt();

    	// try to find an existing number format, and replace its format code
        for (CTNumFmt fmt: numFmt) {
        	if (fmt.getNumFmtId() == formatId) {
        		fmt.setFormatCode(formatCode);
        		return;
        	}
        }

        // create a new entry in the list
        final CTNumFmt newFmt = new CTNumFmt();
        newFmt.setNumFmtId(formatId);
        newFmt.setFormatCode(formatCode);
        numFmt.add(newFmt);
    }

    /**
     * Deletes an existing number format from the list.
     */
    public void deleteNumberFormat(long formatId) {

    	// try to find an existing number format
        if (numFmt != null) {
	        for (CTNumFmt fmt: numFmt) {
	        	if (fmt.getNumFmtId() == formatId) {
	        		numFmt.remove(fmt);
	        		return;
	        	}
	        }
        }
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        this.count = numFmt!=null?(long)numFmt.size():0;
    }
}
