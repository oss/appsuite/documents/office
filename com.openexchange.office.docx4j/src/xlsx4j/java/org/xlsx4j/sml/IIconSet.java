
package org.xlsx4j.sml;

import java.util.List;

public interface IIconSet {

	 String getIconSet();
	 
	 void setIconSet(String iconSet);
	 
	 List<? extends ICfvo> getCfvo();
	 
	 boolean isPercent();	 
	 
	 void setPercent(Boolean value);
	 
	 boolean isReverse() ;
	 
	 void setReverse(Boolean value);
	 
	 boolean isShowValue();
	 
	 void setShowValue(Boolean value);
}
