
package org.xlsx4j.sml;

import org.docx4j.XmlUtils;
import org.xlsx4j.jaxb.Context;

public class CellDataWritable extends CellDataReadonly {

	CellDataWritable(CellDataReadonly cellData) {
		if(cellData.is!=null) {
			is = XmlUtils.deepCopy(cellData.is, /* TODO provide package */ null, Context.getJcSML());
		}
		if(cellData.extLst!=null) {
			extLst = XmlUtils.deepCopy(cellData.extLst, /* TODO provide package */ null, Context.getJcSML());
		}
		s = cellData.s;
		t = cellData.t;
		cm = cellData.cm;
		vm = cellData.vm;
		ph = cellData.ph;
	}
    public void setIs(CTRst value) {
        this.is = value;
    }
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }
    public void setS(Long value) {
    	if(value!=null&&value==0) {
    		this.s = null;
    	}
    	else {
    		this.s = value;
    	}
    }
    public void setT(STCellType value) {
        this.t = value;
    }
    public void setCm(Long value) {
        this.cm = value;
    }
    public void setVm(Long value) {
        this.vm = value;
    }
    public void setPh(Boolean value) {
        this.ph = value;
    }
}
