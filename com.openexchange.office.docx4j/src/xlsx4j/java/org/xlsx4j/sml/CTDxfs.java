/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

import org.xlsx4j.jaxb.Context;

/**
 * <p>Java class for CT_Dxfs complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Dxfs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dxf" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Dxf" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Dxfs", propOrder = {
    "dxf"
})
public class CTDxfs
{
    protected List<CTDxf> dxf;
    @XmlAttribute(name = "count")
    @XmlSchemaType(name = "unsignedInt")
    protected Long count;
    @XmlTransient
    protected JAXBListHelper<CTDxf> xfHelper;

    /**
     * Gets the value of the dxf property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dxf property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDxf().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTDxf }
     *
     *
     */
    public List<CTDxf> getDxf() {
        if (dxf == null) {
            dxf = new ArrayList<CTDxf>();
        }
        return this.dxf;
    }

    /*
     * returns the index of the given CTXf, it is appended
     * to the list if the CTDxf is not equal to another
     */
    public int getOrApply(CTDxf ctDxf) {
        if(xfHelper==null) {
            xfHelper = new JAXBListHelper<CTDxf>(getDxf(), Context.getJcSML());
        }
        return xfHelper.getOrApply(ctDxf);
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        this.count = dxf!=null?(long)dxf.size():0;
    }
}
