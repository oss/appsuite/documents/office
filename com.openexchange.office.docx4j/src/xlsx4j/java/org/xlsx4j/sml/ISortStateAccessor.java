

/**
 * @author sven.jacobi@open-xchange.com
 */
package org.xlsx4j.sml;

public interface ISortStateAccessor {

    public CTSortState getSortState(boolean forceCreate);

    public void setSortState(CTSortState value);
}
