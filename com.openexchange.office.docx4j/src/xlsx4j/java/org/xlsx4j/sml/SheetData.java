/*
 *  Copyright 2010-2013, Plutext Pty Ltd.
 *
 *  This file is part of xlsx4j, a component of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.sml;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xlsx4j.jaxb.Context;


/**
 * <p>Java class for CT_SheetData complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_SheetData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="row" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Row" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SheetData", propOrder = {
    "row"
})
@XmlRootElement(name = "sheetData")
public class SheetData
{
    @XmlTransient
	private static Logger log = LoggerFactory.getLogger(SheetData.class);
    protected List<Row> row;
    @XmlTransient
    private TreeMap<Integer, Row> rows_impl;
    @XmlTransient
    private Long maxSi;
    @XmlTransient
    private HashMap<Integer, List<WeakReference<CellDataReadonly>>> cellCache = new HashMap<Integer, List<WeakReference<CellDataReadonly>>>();
    @XmlTransient
    private HashMap<Integer, List<WeakReference<RowDataReadonly>>> rowCache = new HashMap<Integer, List<WeakReference<RowDataReadonly>>>();

    public void applyCachedCellData(Cell cell) {

    	CellDataReadonly cellData = cell.getCellData();
    	if(cellData instanceof CellDataWritable) {
    		cellData = new CellDataReadonly(cellData);
    		cell.setCellData(cellData);
    	}
    	final int cellHash = cellData.hashCode();
    	List<WeakReference<CellDataReadonly>> cells = cellCache.get(cellHash);
    	if(cells!=null) {
    		for(int i=0; i<cells.size(); i++) {
    			final WeakReference<CellDataReadonly> weakCellData = cells.get(i);
    			final CellDataReadonly cellDataReadonly = weakCellData.get();
    			if(cellDataReadonly==null) {
    				cells.remove(i--);
    			}
    			else if(cellDataReadonly.equals(cellData)) {
	    			// using existing cellData instance
	    			cell.setCellData(cellDataReadonly);
	    			return;
    			}
	    	}
    	}
    	else {
    		cells = new ArrayList<WeakReference<CellDataReadonly>>(1);
    		cellCache.put(cellHash, cells);
    	}
    	// adding new CachedCellData to the cache
    	cells.add(new WeakReference<CellDataReadonly>(cellData));
    }

    public void applyCachedRowData(Row _row) {

    	RowDataReadonly rowData = _row.getRowData();
    	if(rowData instanceof RowDataWritable) {
    		rowData = new RowDataReadonly(rowData);
    		_row.setRowData(rowData);
    	}
    	final int rowHash = rowData.hashCode();
    	List<WeakReference<RowDataReadonly>> rows = rowCache.get(rowHash);
    	if(rows!=null) {
    		for(int i=0; i<rows.size(); i++) {
    			final WeakReference<RowDataReadonly> weakRowData = rows.get(i);
    			final RowDataReadonly rowDataReadonly = weakRowData.get();
    			if(rowDataReadonly==null) {
    				rows.remove(i--);
    			}
    			else if(rowDataReadonly.equals(rowData)) {
	    			// using existing cellData instance
	    			_row.setRowData(rowDataReadonly);
	    			return;
    			}
	    	}
    	}
    	else {
    		rows = new ArrayList<WeakReference<RowDataReadonly>>(1);
    		rowCache.put(rowHash, rows);
    	}
    	// adding new CachedCellData to the cache
    	rows.add(new WeakReference<RowDataReadonly>(rowData));
    }

    /**
     * Gets the value of the row property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Row }
     *
     *
     */
    // this method is now private to ensure that Map<Long, Row> stays up to date..
    private List<Row> getListArray() {

        if (row==null) {
        	if(rows_impl!=null) {
        		row = new ArrayList<Row>(rows_impl.values());
        	}
        	else {
        		row = new ArrayList<Row>();
        	}
        	rows_impl = null;
        }
        return row;
    }

    // the List<Row> may include multiple Rows addressing the same row index, so our Map<Long, Row>
    // takes care only of the first (TODO merging of rows with the same index)
    // if a row is not having an explicit index its index is always previous rowIndex + 1..
    // if a rowIndex is greater than the index of the previous row we have an invalid document
    private Map<Integer, Row> getTreeMap() {
        if(rows_impl==null) {
            rows_impl = new TreeMap<Integer, Row>();
            if(row!=null) {
            	for(Row r:row) {
            		rows_impl.put(r.getRow(), r);
            	}
            	row = null;
            }
        }
        return rows_impl;
    }

    public Row getRow(int index, boolean forceCreate) {

    	final Map<Integer, Row> treeMap = getTreeMap();
        Row r = treeMap.get(index);
        if(forceCreate&&r==null) {
            if(index<0||index>1048575) {
                throw new RuntimeException();
            }

            // creating a new row...
            r = Context.getsmlObjectFactory().createRow();
            r.setRow(index);
            treeMap.put(index, r);
        }
        return r;
    }

    public void insertRows(int start, int insertCount) {

    	final List<Row> rows = getListArray();

        for(int i = 0; i < rows.size(); ++i) {
        	final Row _row = rows.get(i);
        	final int r = _row.getRow();
        	final int to = r + insertCount;
        	if (to>=1048576) {
    			rows.remove(i--);
        	} else if (r>=start) {
                _row.setRow(to);
            }
        }
    }

    public void deleteRows(int start, int deleteCount) {

    	final List<Row> rows = getListArray();
    	final int end = start+deleteCount-1;

    	for(int i=0; i<rows.size(); ++i) {
    		final Row _row = rows.get(i);
    		final int r = _row.getRow();
    		if (r>end) {
    			_row.setRow(r - deleteCount);
    		} else if (r>=start) {
    			rows.remove(i--);
    		}
    	}
    }

    public void insertColumns(int start, int insertCount) {
        final Iterator<Row> rowIterator = createRowIterator();
        while(rowIterator.hasNext()) {
            final Row r = rowIterator.next();
            r.insertCells(start, insertCount);
        }
    }

    public void deleteColumns(int start, int deleteCount) {
    	final Iterator<Row> rowIterator = createRowIterator();
    	while(rowIterator.hasNext()) {
    		final Row r = rowIterator.next();
    		r.deleteCells(start, deleteCount);
    	}
    }

    public Iterator<Row> createRowIterator() {
    	if(row!=null) {
    		return row.iterator();
    	}
    	else if (rows_impl!=null) {
    		return rows_impl.values().iterator();
    	}
    	return getListArray().iterator();
    }

    public void setSi(long si) {
        if(maxSi==null) {
            maxSi = si;
        }
        else if(si>maxSi) {
            maxSi = si;
        }
    }

    public long getNextSi() {
        if(maxSi==null) {
            maxSi = 0L;
        }
        else {
            maxSi++;
        }
        return maxSi;
    }

    public int getLastRowNumber() {
    	if(row!=null&&!row.isEmpty()) {
    		return row.get(row.size()-1).getRow();
    	}
    	else if (rows_impl!=null&&!rows_impl.isEmpty()) {
    		return rows_impl.lastKey();
    	}
    	return -1;
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        getListArray();
    }
}
