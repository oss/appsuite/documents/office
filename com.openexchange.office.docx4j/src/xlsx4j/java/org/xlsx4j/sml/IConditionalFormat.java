
package org.xlsx4j.sml;

import com.openexchange.office.filter.core.component.Child;


public interface IConditionalFormat extends Child {

    public CTPivotAreas getPivotAreas();

    public void setPivotAreas(CTPivotAreas value);

    public CTExtensionList getExtLst();

    public void setExtLst(CTExtensionList value);

    public STScope getScope();

    public void setScope(STScope value);

    public STType getType();

    public void setType(STType value);
}
