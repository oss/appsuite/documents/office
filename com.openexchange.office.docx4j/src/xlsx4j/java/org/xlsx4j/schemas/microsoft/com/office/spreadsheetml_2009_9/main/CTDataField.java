
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_DataField complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_DataField">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="pivotShowAs" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_PivotShowAs" />
 *       &lt;attribute name="sourceField" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="uniqueName" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DataField")
public class CTDataField {

    @XmlAttribute(name = "pivotShowAs")
    protected STPivotShowAs pivotShowAs;
    @XmlAttribute(name = "sourceField")
    @XmlSchemaType(name = "unsignedInt")
    protected Long sourceField;
    @XmlAttribute(name = "uniqueName")
    protected String uniqueName;

    /**
     * Gets the value of the pivotShowAs property.
     * 
     * @return
     *     possible object is
     *     {@link STPivotShowAs }
     *     
     */
    public STPivotShowAs getPivotShowAs() {
        return pivotShowAs;
    }

    /**
     * Sets the value of the pivotShowAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link STPivotShowAs }
     *     
     */
    public void setPivotShowAs(STPivotShowAs value) {
        this.pivotShowAs = value;
    }

    /**
     * Gets the value of the sourceField property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSourceField() {
        return sourceField;
    }

    /**
     * Sets the value of the sourceField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSourceField(Long value) {
        this.sourceField = value;
    }

    /**
     * Gets the value of the uniqueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueName() {
        return uniqueName;
    }

    /**
     * Sets the value of the uniqueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueName(String value) {
        this.uniqueName = value;
    }
}
