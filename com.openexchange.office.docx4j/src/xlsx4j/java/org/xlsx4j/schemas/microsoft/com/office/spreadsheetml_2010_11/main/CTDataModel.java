
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_DataModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_DataModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="modelTables" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_ModelTables" minOccurs="0"/>
 *         &lt;element name="modelRelationships" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_ModelRelationships" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="minVersionLoad" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" default="5" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DataModel", propOrder = {
    "modelTables",
    "modelRelationships",
    "extLst"
})
public class CTDataModel {

    protected CTModelTables modelTables;
    protected CTModelRelationships modelRelationships;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "minVersionLoad")
    @XmlSchemaType(name = "unsignedByte")
    protected Short minVersionLoad;

    /**
     * Gets the value of the modelTables property.
     * 
     * @return
     *     possible object is
     *     {@link CTModelTables }
     *     
     */
    public CTModelTables getModelTables() {
        return modelTables;
    }

    /**
     * Sets the value of the modelTables property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTModelTables }
     *     
     */
    public void setModelTables(CTModelTables value) {
        this.modelTables = value;
    }

    /**
     * Gets the value of the modelRelationships property.
     * 
     * @return
     *     possible object is
     *     {@link CTModelRelationships }
     *     
     */
    public CTModelRelationships getModelRelationships() {
        return modelRelationships;
    }

    /**
     * Sets the value of the modelRelationships property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTModelRelationships }
     *     
     */
    public void setModelRelationships(CTModelRelationships value) {
        this.modelRelationships = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the minVersionLoad property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public short getMinVersionLoad() {
        if (minVersionLoad == null) {
            return ((short) 5);
        }
        return minVersionLoad;
    }

    /**
     * Sets the value of the minVersionLoad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setMinVersionLoad(Short value) {
        this.minVersionLoad = value;
    }
}
