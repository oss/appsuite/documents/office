
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_QuestionFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_QuestionFormat">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="generalDate"/>
 *     &lt;enumeration value="longDate"/>
 *     &lt;enumeration value="shortDate"/>
 *     &lt;enumeration value="longTime"/>
 *     &lt;enumeration value="shortTime"/>
 *     &lt;enumeration value="generalNumber"/>
 *     &lt;enumeration value="standard"/>
 *     &lt;enumeration value="fixed"/>
 *     &lt;enumeration value="percent"/>
 *     &lt;enumeration value="currency"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_QuestionFormat")
@XmlEnum
public enum STQuestionFormat {

    @XmlEnumValue("generalDate")
    GENERAL_DATE("generalDate"),
    @XmlEnumValue("longDate")
    LONG_DATE("longDate"),
    @XmlEnumValue("shortDate")
    SHORT_DATE("shortDate"),
    @XmlEnumValue("longTime")
    LONG_TIME("longTime"),
    @XmlEnumValue("shortTime")
    SHORT_TIME("shortTime"),
    @XmlEnumValue("generalNumber")
    GENERAL_NUMBER("generalNumber"),
    @XmlEnumValue("standard")
    STANDARD("standard"),
    @XmlEnumValue("fixed")
    FIXED("fixed"),
    @XmlEnumValue("percent")
    PERCENT("percent"),
    @XmlEnumValue("currency")
    CURRENCY("currency");
    private final String value;

    STQuestionFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STQuestionFormat fromValue(String v) {
        for (STQuestionFormat c: STQuestionFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
