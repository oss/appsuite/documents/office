
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.schemas.microsoft.com.office.excel_2006.main.CTSqref;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.IConditionalFormatting;


/**
 * <p>Java class for CT_ConditionalFormatting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ConditionalFormatting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cfRule" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_CfRule" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}sqref" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="pivot" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ConditionalFormatting", propOrder = {
    "cfRule",
    "sqref",
    "extLst"
})
public class CTConditionalFormatting implements IConditionalFormatting {

    protected List<CTCfRule> cfRule;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected CTSqref sqref;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "pivot")
    protected Boolean pivot;
    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the cfRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCfRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTCfRule }
     * 
     * 
     */
    @Override
    public List<CTCfRule> getCfRule() {
        if (cfRule == null) {
            cfRule = new ArrayList<CTCfRule>();
        }
        return this.cfRule;
    }

    @Override
    public CTCfRule createCfRule() {
    	return new CTCfRule();
    }

    /**
     * Gets the value of the sqref property.
     * 
     * @return
     *     possible object is
     *     {@link CTSqref }
     *     
     */
    public CTSqref getSqref() {
        return sqref;
    }

    /**
     * Sets the value of the sqref property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSqref }
     *     
     */
    public void setSqref(CTSqref value) {
        this.sqref = value;
    }

    @Override
    public List<String> getSqrefList() {
    	if(sqref==null) {
    		sqref = new CTSqref();
    	}
    	return sqref.getSqrefList();
    }

    public void setSqrefList(List<String> sqrefList) {
    	if(sqref==null) {
    		sqref = new CTSqref();
    	}
    	sqref.setSqrefList(sqrefList);
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    @Override
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    @Override
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the pivot property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isPivot() {
        if (pivot == null) {
            return false;
        }
        return pivot;
    }

    /**
     * Sets the value of the pivot property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setPivot(Boolean value) {
        this.pivot = value;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
