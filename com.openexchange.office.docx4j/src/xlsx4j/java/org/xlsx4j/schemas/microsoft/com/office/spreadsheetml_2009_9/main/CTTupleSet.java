
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TupleSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TupleSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="headers" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TupleSetHeaders"/>
 *         &lt;element name="rows" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TupleSetRows"/>
 *       &lt;/sequence>
 *       &lt;attribute name="rowCount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="1" />
 *       &lt;attribute name="columnCount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="1" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TupleSet", propOrder = {
    "headers",
    "rows"
})
public class CTTupleSet {

    @XmlElement(required = true)
    protected CTTupleSetHeaders headers;
    @XmlElement(required = true)
    protected CTTupleSetRows rows;
    @XmlAttribute(name = "rowCount")
    @XmlSchemaType(name = "unsignedInt")
    protected Long rowCount;
    @XmlAttribute(name = "columnCount")
    @XmlSchemaType(name = "unsignedInt")
    protected Long columnCount;

    /**
     * Gets the value of the headers property.
     * 
     * @return
     *     possible object is
     *     {@link CTTupleSetHeaders }
     *     
     */
    public CTTupleSetHeaders getHeaders() {
        return headers;
    }

    /**
     * Sets the value of the headers property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTupleSetHeaders }
     *     
     */
    public void setHeaders(CTTupleSetHeaders value) {
        this.headers = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link CTTupleSetRows }
     *     
     */
    public CTTupleSetRows getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTupleSetRows }
     *     
     */
    public void setRows(CTTupleSetRows value) {
        this.rows = value;
    }

    /**
     * Gets the value of the rowCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getRowCount() {
        if (rowCount == null) {
            return  1L;
        }
        return rowCount;
    }

    /**
     * Sets the value of the rowCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRowCount(Long value) {
        this.rowCount = value;
    }

    /**
     * Gets the value of the columnCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getColumnCount() {
        if (columnCount == null) {
            return  1L;
        }
        return columnCount;
    }

    /**
     * Sets the value of the columnCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setColumnCount(Long value) {
        this.columnCount = value;
    }
}
