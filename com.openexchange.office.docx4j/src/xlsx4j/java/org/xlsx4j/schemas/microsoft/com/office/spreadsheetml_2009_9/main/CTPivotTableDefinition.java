
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotTableDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotTableDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotEdits" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_PivotEdits" minOccurs="0"/>
 *         &lt;element name="pivotChanges" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_PivotChanges" minOccurs="0"/>
 *         &lt;element name="conditionalFormats" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_ConditionalFormats" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="fillDownLabelsDefault" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="visualTotalsForSets" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="calculatedMembersInFilters" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="altText" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="altTextSummary" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="enableEdit" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="autoApply" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="allocationMethod" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_AllocationMethod" default="equalAllocation" />
 *       &lt;attribute name="weightExpression" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="hideValuesRow" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotTableDefinition", propOrder = {
    "pivotEdits",
    "pivotChanges",
    "conditionalFormats"
})
public class CTPivotTableDefinition {

    protected CTPivotEdits pivotEdits;
    protected CTPivotChanges pivotChanges;
    protected CTConditionalFormats conditionalFormats;
    @XmlAttribute(name = "fillDownLabelsDefault")
    protected Boolean fillDownLabelsDefault;
    @XmlAttribute(name = "visualTotalsForSets")
    protected Boolean visualTotalsForSets;
    @XmlAttribute(name = "calculatedMembersInFilters")
    protected Boolean calculatedMembersInFilters;
    @XmlAttribute(name = "altText")
    protected String altText;
    @XmlAttribute(name = "altTextSummary")
    protected String altTextSummary;
    @XmlAttribute(name = "enableEdit")
    protected Boolean enableEdit;
    @XmlAttribute(name = "autoApply")
    protected Boolean autoApply;
    @XmlAttribute(name = "allocationMethod")
    protected STAllocationMethod allocationMethod;
    @XmlAttribute(name = "weightExpression")
    protected String weightExpression;
    @XmlAttribute(name = "hideValuesRow")
    protected Boolean hideValuesRow;

    /**
     * Gets the value of the pivotEdits property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotEdits }
     *     
     */
    public CTPivotEdits getPivotEdits() {
        return pivotEdits;
    }

    /**
     * Sets the value of the pivotEdits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotEdits }
     *     
     */
    public void setPivotEdits(CTPivotEdits value) {
        this.pivotEdits = value;
    }

    /**
     * Gets the value of the pivotChanges property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotChanges }
     *     
     */
    public CTPivotChanges getPivotChanges() {
        return pivotChanges;
    }

    /**
     * Sets the value of the pivotChanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotChanges }
     *     
     */
    public void setPivotChanges(CTPivotChanges value) {
        this.pivotChanges = value;
    }

    /**
     * Gets the value of the conditionalFormats property.
     * 
     * @return
     *     possible object is
     *     {@link CTConditionalFormats }
     *     
     */
    public CTConditionalFormats getConditionalFormats() {
        return conditionalFormats;
    }

    /**
     * Sets the value of the conditionalFormats property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTConditionalFormats }
     *     
     */
    public void setConditionalFormats(CTConditionalFormats value) {
        this.conditionalFormats = value;
    }

    /**
     * Gets the value of the fillDownLabelsDefault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFillDownLabelsDefault() {
        if (fillDownLabelsDefault == null) {
            return false;
        }
        return fillDownLabelsDefault;
    }

    /**
     * Sets the value of the fillDownLabelsDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFillDownLabelsDefault(Boolean value) {
        this.fillDownLabelsDefault = value;
    }

    /**
     * Gets the value of the visualTotalsForSets property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isVisualTotalsForSets() {
        if (visualTotalsForSets == null) {
            return false;
        }
        return visualTotalsForSets;
    }

    /**
     * Sets the value of the visualTotalsForSets property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisualTotalsForSets(Boolean value) {
        this.visualTotalsForSets = value;
    }

    /**
     * Gets the value of the calculatedMembersInFilters property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCalculatedMembersInFilters() {
        if (calculatedMembersInFilters == null) {
            return false;
        }
        return calculatedMembersInFilters;
    }

    /**
     * Sets the value of the calculatedMembersInFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCalculatedMembersInFilters(Boolean value) {
        this.calculatedMembersInFilters = value;
    }

    /**
     * Gets the value of the altText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltText() {
        return altText;
    }

    /**
     * Sets the value of the altText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltText(String value) {
        this.altText = value;
    }

    /**
     * Gets the value of the altTextSummary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltTextSummary() {
        return altTextSummary;
    }

    /**
     * Sets the value of the altTextSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltTextSummary(String value) {
        this.altTextSummary = value;
    }

    /**
     * Gets the value of the enableEdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEnableEdit() {
        if (enableEdit == null) {
            return false;
        }
        return enableEdit;
    }

    /**
     * Sets the value of the enableEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEdit(Boolean value) {
        this.enableEdit = value;
    }

    /**
     * Gets the value of the autoApply property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAutoApply() {
        if (autoApply == null) {
            return false;
        }
        return autoApply;
    }

    /**
     * Sets the value of the autoApply property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoApply(Boolean value) {
        this.autoApply = value;
    }

    /**
     * Gets the value of the allocationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link STAllocationMethod }
     *     
     */
    public STAllocationMethod getAllocationMethod() {
        if (allocationMethod == null) {
            return STAllocationMethod.EQUAL_ALLOCATION;
        }
        return allocationMethod;
    }

    /**
     * Sets the value of the allocationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link STAllocationMethod }
     *     
     */
    public void setAllocationMethod(STAllocationMethod value) {
        this.allocationMethod = value;
    }

    /**
     * Gets the value of the weightExpression property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightExpression() {
        return weightExpression;
    }

    /**
     * Sets the value of the weightExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightExpression(String value) {
        this.weightExpression = value;
    }

    /**
     * Gets the value of the hideValuesRow property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHideValuesRow() {
        if (hideValuesRow == null) {
            return false;
        }
        return hideValuesRow;
    }

    /**
     * Sets the value of the hideValuesRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHideValuesRow(Boolean value) {
        this.hideValuesRow = value;
    }
}
