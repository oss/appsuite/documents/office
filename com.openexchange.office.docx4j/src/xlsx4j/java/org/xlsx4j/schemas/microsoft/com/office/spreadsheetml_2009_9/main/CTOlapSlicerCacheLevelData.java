
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_OlapSlicerCacheLevelData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_OlapSlicerCacheLevelData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ranges" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_OlapSlicerCacheRanges" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="uniqueName" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="sourceCaption" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="sortOrder" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_OlapSlicerCacheSortOrder" default="natural" />
 *       &lt;attribute name="crossFilter" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SlicerCacheCrossFilter" default="showItemsWithDataAtTop" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_OlapSlicerCacheLevelData", propOrder = {
    "ranges"
})
public class CTOlapSlicerCacheLevelData {

    protected CTOlapSlicerCacheRanges ranges;
    @XmlAttribute(name = "uniqueName", required = true)
    protected String uniqueName;
    @XmlAttribute(name = "sourceCaption")
    protected String sourceCaption;
    @XmlAttribute(name = "count", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long count;
    @XmlAttribute(name = "sortOrder")
    protected STOlapSlicerCacheSortOrder sortOrder;
    @XmlAttribute(name = "crossFilter")
    protected STSlicerCacheCrossFilter crossFilter;

    /**
     * Gets the value of the ranges property.
     * 
     * @return
     *     possible object is
     *     {@link CTOlapSlicerCacheRanges }
     *     
     */
    public CTOlapSlicerCacheRanges getRanges() {
        return ranges;
    }

    /**
     * Sets the value of the ranges property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOlapSlicerCacheRanges }
     *     
     */
    public void setRanges(CTOlapSlicerCacheRanges value) {
        this.ranges = value;
    }

    /**
     * Gets the value of the uniqueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueName() {
        return uniqueName;
    }

    /**
     * Sets the value of the uniqueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueName(String value) {
        this.uniqueName = value;
    }

    /**
     * Gets the value of the sourceCaption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceCaption() {
        return sourceCaption;
    }

    /**
     * Sets the value of the sourceCaption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceCaption(String value) {
        this.sourceCaption = value;
    }

    /**
     * Gets the value of the count property.
     * 
     */
    public long getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     */
    public void setCount(long value) {
        this.count = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link STOlapSlicerCacheSortOrder }
     *     
     */
    public STOlapSlicerCacheSortOrder getSortOrder() {
        if (sortOrder == null) {
            return STOlapSlicerCacheSortOrder.NATURAL;
        }
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link STOlapSlicerCacheSortOrder }
     *     
     */
    public void setSortOrder(STOlapSlicerCacheSortOrder value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the crossFilter property.
     * 
     * @return
     *     possible object is
     *     {@link STSlicerCacheCrossFilter }
     *     
     */
    public STSlicerCacheCrossFilter getCrossFilter() {
        if (crossFilter == null) {
            return STSlicerCacheCrossFilter.SHOW_ITEMS_WITH_DATA_AT_TOP;
        }
        return crossFilter;
    }

    /**
     * Sets the value of the crossFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSlicerCacheCrossFilter }
     *     
     */
    public void setCrossFilter(STSlicerCacheCrossFilter value) {
        this.crossFilter = value;
    }
}
