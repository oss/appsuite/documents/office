
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_SurveyQuestion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SurveyQuestion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="questionPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyElementPr" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="binding" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="text" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="type" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}ST_QuestionType" />
 *       &lt;attribute name="format" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}ST_QuestionFormat" />
 *       &lt;attribute name="helpText" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="required" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="defaultValue" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="decimalPlaces" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="rowSource" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SurveyQuestion", propOrder = {
    "questionPr",
    "extLst"
})
public class CTSurveyQuestion {

    protected CTSurveyElementPr questionPr;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "binding", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long binding;
    @XmlAttribute(name = "text")
    protected String text;
    @XmlAttribute(name = "type")
    protected STQuestionType type;
    @XmlAttribute(name = "format")
    protected STQuestionFormat format;
    @XmlAttribute(name = "helpText")
    protected String helpText;
    @XmlAttribute(name = "required")
    protected Boolean required;
    @XmlAttribute(name = "defaultValue")
    protected String defaultValue;
    @XmlAttribute(name = "decimalPlaces")
    @XmlSchemaType(name = "unsignedInt")
    protected Long decimalPlaces;
    @XmlAttribute(name = "rowSource")
    protected String rowSource;

    /**
     * Gets the value of the questionPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public CTSurveyElementPr getQuestionPr() {
        return questionPr;
    }

    /**
     * Sets the value of the questionPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public void setQuestionPr(CTSurveyElementPr value) {
        this.questionPr = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the binding property.
     * 
     */
    public long getBinding() {
        return binding;
    }

    /**
     * Sets the value of the binding property.
     * 
     */
    public void setBinding(long value) {
        this.binding = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link STQuestionType }
     *     
     */
    public STQuestionType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link STQuestionType }
     *     
     */
    public void setType(STQuestionType value) {
        this.type = value;
    }

    /**
     * Gets the value of the format property.
     * 
     * @return
     *     possible object is
     *     {@link STQuestionFormat }
     *     
     */
    public STQuestionFormat getFormat() {
        return format;
    }

    /**
     * Sets the value of the format property.
     * 
     * @param value
     *     allowed object is
     *     {@link STQuestionFormat }
     *     
     */
    public void setFormat(STQuestionFormat value) {
        this.format = value;
    }

    /**
     * Gets the value of the helpText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpText() {
        return helpText;
    }

    /**
     * Sets the value of the helpText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpText(String value) {
        this.helpText = value;
    }

    /**
     * Gets the value of the required property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRequired() {
        if (required == null) {
            return false;
        }
        return required;
    }

    /**
     * Sets the value of the required property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequired(Boolean value) {
        this.required = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the decimalPlaces property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Sets the value of the decimalPlaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDecimalPlaces(Long value) {
        this.decimalPlaces = value;
    }

    /**
     * Gets the value of the rowSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRowSource() {
        return rowSource;
    }

    /**
     * Sets the value of the rowSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRowSource(String value) {
        this.rowSource = value;
    }
}
