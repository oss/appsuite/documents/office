
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_PivotShowAs.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_PivotShowAs">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="percentOfParent"/>
 *     &lt;enumeration value="percentOfParentRow"/>
 *     &lt;enumeration value="percentOfParentCol"/>
 *     &lt;enumeration value="percentOfRunningTotal"/>
 *     &lt;enumeration value="rankAscending"/>
 *     &lt;enumeration value="rankDescending"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_PivotShowAs")
@XmlEnum
public enum STPivotShowAs {

    @XmlEnumValue("percentOfParent")
    PERCENT_OF_PARENT("percentOfParent"),
    @XmlEnumValue("percentOfParentRow")
    PERCENT_OF_PARENT_ROW("percentOfParentRow"),
    @XmlEnumValue("percentOfParentCol")
    PERCENT_OF_PARENT_COL("percentOfParentCol"),
    @XmlEnumValue("percentOfRunningTotal")
    PERCENT_OF_RUNNING_TOTAL("percentOfRunningTotal"),
    @XmlEnumValue("rankAscending")
    RANK_ASCENDING("rankAscending"),
    @XmlEnumValue("rankDescending")
    RANK_DESCENDING("rankDescending");
    private final String value;

    STPivotShowAs(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STPivotShowAs fromValue(String v) {
        for (STPivotShowAs c: STPivotShowAs.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
