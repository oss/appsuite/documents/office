
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineStyleElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineStyleElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="type" use="required" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}ST_TimelineStyleType" />
 *       &lt;attribute name="dxfId" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_DxfId" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineStyleElement")
public class CTTimelineStyleElement {

    @XmlAttribute(name = "type", required = true)
    protected STTimelineStyleType type;
    @XmlAttribute(name = "dxfId")
    protected Long dxfId;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link STTimelineStyleType }
     *     
     */
    public STTimelineStyleType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTimelineStyleType }
     *     
     */
    public void setType(STTimelineStyleType value) {
        this.type = value;
    }

    /**
     * Gets the value of the dxfId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDxfId() {
        return dxfId;
    }

    /**
     * Sets the value of the dxfId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDxfId(Long value) {
        this.dxfId = value;
    }
}
