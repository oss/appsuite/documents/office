
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CacheHierarchy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CacheHierarchy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="aggregatedColumn" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CacheHierarchy")
public class CTCacheHierarchy {

    @XmlAttribute(name = "aggregatedColumn", required = true)
    protected int aggregatedColumn;

    /**
     * Gets the value of the aggregatedColumn property.
     * 
     */
    public int getAggregatedColumn() {
        return aggregatedColumn;
    }

    /**
     * Sets the value of the aggregatedColumn property.
     * 
     */
    public void setAggregatedColumn(int value) {
        this.aggregatedColumn = value;
    }
}
