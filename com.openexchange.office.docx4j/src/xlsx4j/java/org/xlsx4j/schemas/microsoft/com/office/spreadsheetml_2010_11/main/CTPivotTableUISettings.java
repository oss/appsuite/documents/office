
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_PivotTableUISettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotTableUISettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activeTabTopLevelEntity" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_FieldListActiveTabTopLevelEntity" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="sourceDataName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="relNeededHidden" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotTableUISettings", propOrder = {
    "activeTabTopLevelEntity",
    "extLst"
})
public class CTPivotTableUISettings {

    protected List<CTFieldListActiveTabTopLevelEntity> activeTabTopLevelEntity;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "sourceDataName")
    protected String sourceDataName;
    @XmlAttribute(name = "relNeededHidden")
    protected Boolean relNeededHidden;

    /**
     * Gets the value of the activeTabTopLevelEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activeTabTopLevelEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActiveTabTopLevelEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTFieldListActiveTabTopLevelEntity }
     * 
     * 
     */
    public List<CTFieldListActiveTabTopLevelEntity> getActiveTabTopLevelEntity() {
        if (activeTabTopLevelEntity == null) {
            activeTabTopLevelEntity = new ArrayList<CTFieldListActiveTabTopLevelEntity>();
        }
        return this.activeTabTopLevelEntity;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the sourceDataName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceDataName() {
        return sourceDataName;
    }

    /**
     * Sets the value of the sourceDataName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceDataName(String value) {
        this.sourceDataName = value;
    }

    /**
     * Gets the value of the relNeededHidden property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRelNeededHidden() {
        if (relNeededHidden == null) {
            return false;
        }
        return relNeededHidden;
    }

    /**
     * Sets the value of the relNeededHidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRelNeededHidden(Boolean value) {
        this.relNeededHidden = value;
    }
}
