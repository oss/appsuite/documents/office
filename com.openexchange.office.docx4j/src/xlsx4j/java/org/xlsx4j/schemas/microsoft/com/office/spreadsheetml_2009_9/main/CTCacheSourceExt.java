
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CacheSourceExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CacheSourceExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}sourceConnection"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CacheSourceExt", propOrder = {
    "sourceConnection"
})
public class CTCacheSourceExt {

    @XmlElement(required = true)
    protected CTSourceConnection sourceConnection;

    /**
     * Gets the value of the sourceConnection property.
     * 
     * @return
     *     possible object is
     *     {@link CTSourceConnection }
     *     
     */
    public CTSourceConnection getSourceConnection() {
        return sourceConnection;
    }

    /**
     * Sets the value of the sourceConnection property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSourceConnection }
     *     
     */
    public void setSourceConnection(CTSourceConnection value) {
        this.sourceConnection = value;
    }
}
