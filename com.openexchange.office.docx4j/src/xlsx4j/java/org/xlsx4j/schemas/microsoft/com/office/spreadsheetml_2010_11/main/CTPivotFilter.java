
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="useWholeDay" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotFilter")
public class CTPivotFilter {

    @XmlAttribute(name = "useWholeDay", required = true)
    protected boolean useWholeDay;

    /**
     * Gets the value of the useWholeDay property.
     * 
     */
    public boolean isUseWholeDay() {
        return useWholeDay;
    }

    /**
     * Sets the value of the useWholeDay property.
     * 
     */
    public void setUseWholeDay(boolean value) {
        this.useWholeDay = value;
    }
}
