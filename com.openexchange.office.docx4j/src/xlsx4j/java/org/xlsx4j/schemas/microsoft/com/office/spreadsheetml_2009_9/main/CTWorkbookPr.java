
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_WorkbookPr complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_WorkbookPr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="defaultImageDpi" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="220" />
 *       &lt;attribute name="discardImageEditData" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="accuracyVersion" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WorkbookPr")
public class CTWorkbookPr {

    @XmlAttribute(name = "defaultImageDpi")
    @XmlSchemaType(name = "unsignedInt")
    protected Long defaultImageDpi;
    @XmlAttribute(name = "discardImageEditData")
    protected Boolean discardImageEditData;
    @XmlAttribute(name = "accuracyVersion")
    @XmlSchemaType(name = "unsignedInt")
    protected Long accuracyVersion;

    /**
     * Gets the value of the defaultImageDpi property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getDefaultImageDpi() {
        if (defaultImageDpi == null) {
            return  220L;
        }
        return defaultImageDpi;
    }

    /**
     * Sets the value of the defaultImageDpi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDefaultImageDpi(Long value) {
        this.defaultImageDpi = value;
    }

    /**
     * Gets the value of the discardImageEditData property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDiscardImageEditData() {
        if (discardImageEditData == null) {
            return false;
        }
        return discardImageEditData;
    }

    /**
     * Sets the value of the discardImageEditData property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDiscardImageEditData(Boolean value) {
        this.discardImageEditData = value;
    }

    /**
     * Gets the value of the accuracyVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getAccuracyVersion() {
        if (accuracyVersion == null) {
            return  0L;
        }
        return accuracyVersion;
    }

    /**
     * Sets the value of the accuracyVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAccuracyVersion(Long value) {
        this.accuracyVersion = value;
    }
}
