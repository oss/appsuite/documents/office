
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotCacheIdVersion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotCacheIdVersion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="cacheIdSupportedVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" />
 *       &lt;attribute name="cacheIdCreatedVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotCacheIdVersion")
public class CTPivotCacheIdVersion {

    @XmlAttribute(name = "cacheIdSupportedVersion", required = true)
    @XmlSchemaType(name = "unsignedByte")
    protected short cacheIdSupportedVersion;
    @XmlAttribute(name = "cacheIdCreatedVersion", required = true)
    @XmlSchemaType(name = "unsignedByte")
    protected short cacheIdCreatedVersion;

    /**
     * Gets the value of the cacheIdSupportedVersion property.
     * 
     */
    public short getCacheIdSupportedVersion() {
        return cacheIdSupportedVersion;
    }

    /**
     * Sets the value of the cacheIdSupportedVersion property.
     * 
     */
    public void setCacheIdSupportedVersion(short value) {
        this.cacheIdSupportedVersion = value;
    }

    /**
     * Gets the value of the cacheIdCreatedVersion property.
     * 
     */
    public short getCacheIdCreatedVersion() {
        return cacheIdCreatedVersion;
    }

    /**
     * Sets the value of the cacheIdCreatedVersion property.
     * 
     */
    public void setCacheIdCreatedVersion(short value) {
        this.cacheIdCreatedVersion = value;
    }
}
