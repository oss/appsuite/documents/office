
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineStyle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineStyle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timelineStyleElements" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineStyleElements" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineStyle", propOrder = {
    "timelineStyleElements"
})
public class CTTimelineStyle {

    protected CTTimelineStyleElements timelineStyleElements;
    @XmlAttribute(name = "name", required = true)
    protected String name;

    /**
     * Gets the value of the timelineStyleElements property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelineStyleElements }
     *     
     */
    public CTTimelineStyleElements getTimelineStyleElements() {
        return timelineStyleElements;
    }

    /**
     * Sets the value of the timelineStyleElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelineStyleElements }
     *     
     */
    public void setTimelineStyleElements(CTTimelineStyleElements value) {
        this.timelineStyleElements = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }
}
