
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_TimelineStyleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_TimelineStyleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="selectionLabel"/>
 *     &lt;enumeration value="timeLevel"/>
 *     &lt;enumeration value="periodLabel1"/>
 *     &lt;enumeration value="periodLabel2"/>
 *     &lt;enumeration value="selectedTimeBlock"/>
 *     &lt;enumeration value="unselectedTimeBlock"/>
 *     &lt;enumeration value="selectedTimeBlockSpace"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_TimelineStyleType")
@XmlEnum
public enum STTimelineStyleType {

    @XmlEnumValue("selectionLabel")
    SELECTION_LABEL("selectionLabel"),
    @XmlEnumValue("timeLevel")
    TIME_LEVEL("timeLevel"),
    @XmlEnumValue("periodLabel1")
    PERIOD_LABEL_1("periodLabel1"),
    @XmlEnumValue("periodLabel2")
    PERIOD_LABEL_2("periodLabel2"),
    @XmlEnumValue("selectedTimeBlock")
    SELECTED_TIME_BLOCK("selectedTimeBlock"),
    @XmlEnumValue("unselectedTimeBlock")
    UNSELECTED_TIME_BLOCK("unselectedTimeBlock"),
    @XmlEnumValue("selectedTimeBlockSpace")
    SELECTED_TIME_BLOCK_SPACE("selectedTimeBlockSpace");
    private final String value;

    STTimelineStyleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STTimelineStyleType fromValue(String v) {
        for (STTimelineStyleType c: STTimelineStyleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
