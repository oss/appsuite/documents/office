
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerStyle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerStyle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="slicerStyleElements" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SlicerStyleElements" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerStyle", propOrder = {
    "slicerStyleElements"
})
public class CTSlicerStyle {

    protected CTSlicerStyleElements slicerStyleElements;
    @XmlAttribute(name = "name", required = true)
    protected String name;

    /**
     * Gets the value of the slicerStyleElements property.
     * 
     * @return
     *     possible object is
     *     {@link CTSlicerStyleElements }
     *     
     */
    public CTSlicerStyleElements getSlicerStyleElements() {
        return slicerStyleElements;
    }

    /**
     * Sets the value of the slicerStyleElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSlicerStyleElements }
     *     
     */
    public void setSlicerStyleElements(CTSlicerStyleElements value) {
        this.slicerStyleElements = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }
}
