
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_OledbPr complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_OledbPr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="dbTables" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_DbTables"/>
 *         &lt;element name="dbCommand" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_DbCommand"/>
 *       &lt;/choice>
 *       &lt;attribute name="connection" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_OledbPr", propOrder = {
    "dbTables",
    "dbCommand"
})
public class CTOledbPr {

    protected CTDbTables dbTables;
    protected CTDbCommand dbCommand;
    @XmlAttribute(name = "connection")
    protected String connection;

    /**
     * Gets the value of the dbTables property.
     * 
     * @return
     *     possible object is
     *     {@link CTDbTables }
     *     
     */
    public CTDbTables getDbTables() {
        return dbTables;
    }

    /**
     * Sets the value of the dbTables property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDbTables }
     *     
     */
    public void setDbTables(CTDbTables value) {
        this.dbTables = value;
    }

    /**
     * Gets the value of the dbCommand property.
     * 
     * @return
     *     possible object is
     *     {@link CTDbCommand }
     *     
     */
    public CTDbCommand getDbCommand() {
        return dbCommand;
    }

    /**
     * Sets the value of the dbCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDbCommand }
     *     
     */
    public void setDbCommand(CTDbCommand value) {
        this.dbCommand = value;
    }

    /**
     * Gets the value of the connection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnection() {
        return connection;
    }

    /**
     * Sets the value of the connection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnection(String value) {
        this.connection = value;
    }
}
