
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.schemas.microsoft.com.office.excel_2006.main.CTSqref;


/**
 * <p>Java class for CT_IgnoredError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_IgnoredError">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}sqref"/>
 *       &lt;/sequence>
 *       &lt;attribute name="evalError" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="twoDigitTextYear" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="numberStoredAsText" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="formula" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="formulaRange" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="unlockedFormula" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="emptyCellReference" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="listDataValidation" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="calculatedColumn" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_IgnoredError", propOrder = {
    "sqref"
})
public class CTIgnoredError {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main", required = true)
    protected CTSqref sqref;
    @XmlAttribute(name = "evalError")
    protected Boolean evalError;
    @XmlAttribute(name = "twoDigitTextYear")
    protected Boolean twoDigitTextYear;
    @XmlAttribute(name = "numberStoredAsText")
    protected Boolean numberStoredAsText;
    @XmlAttribute(name = "formula")
    protected Boolean formula;
    @XmlAttribute(name = "formulaRange")
    protected Boolean formulaRange;
    @XmlAttribute(name = "unlockedFormula")
    protected Boolean unlockedFormula;
    @XmlAttribute(name = "emptyCellReference")
    protected Boolean emptyCellReference;
    @XmlAttribute(name = "listDataValidation")
    protected Boolean listDataValidation;
    @XmlAttribute(name = "calculatedColumn")
    protected Boolean calculatedColumn;

    /**
     * Gets the value of the sqref property.
     * 
     * @return
     *     possible object is
     *     {@link CTSqref }
     *     
     */
    public CTSqref getSqref() {
        return sqref;
    }

    /**
     * Sets the value of the sqref property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSqref }
     *     
     */
    public void setSqref(CTSqref value) {
        this.sqref = value;
    }

    /**
     * Gets the value of the evalError property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEvalError() {
        if (evalError == null) {
            return false;
        }
        return evalError;
    }

    /**
     * Sets the value of the evalError property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEvalError(Boolean value) {
        this.evalError = value;
    }

    /**
     * Gets the value of the twoDigitTextYear property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isTwoDigitTextYear() {
        if (twoDigitTextYear == null) {
            return false;
        }
        return twoDigitTextYear;
    }

    /**
     * Sets the value of the twoDigitTextYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTwoDigitTextYear(Boolean value) {
        this.twoDigitTextYear = value;
    }

    /**
     * Gets the value of the numberStoredAsText property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNumberStoredAsText() {
        if (numberStoredAsText == null) {
            return false;
        }
        return numberStoredAsText;
    }

    /**
     * Sets the value of the numberStoredAsText property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNumberStoredAsText(Boolean value) {
        this.numberStoredAsText = value;
    }

    /**
     * Gets the value of the formula property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFormula() {
        if (formula == null) {
            return false;
        }
        return formula;
    }

    /**
     * Sets the value of the formula property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormula(Boolean value) {
        this.formula = value;
    }

    /**
     * Gets the value of the formulaRange property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFormulaRange() {
        if (formulaRange == null) {
            return false;
        }
        return formulaRange;
    }

    /**
     * Sets the value of the formulaRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormulaRange(Boolean value) {
        this.formulaRange = value;
    }

    /**
     * Gets the value of the unlockedFormula property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUnlockedFormula() {
        if (unlockedFormula == null) {
            return false;
        }
        return unlockedFormula;
    }

    /**
     * Sets the value of the unlockedFormula property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnlockedFormula(Boolean value) {
        this.unlockedFormula = value;
    }

    /**
     * Gets the value of the emptyCellReference property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEmptyCellReference() {
        if (emptyCellReference == null) {
            return false;
        }
        return emptyCellReference;
    }

    /**
     * Sets the value of the emptyCellReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmptyCellReference(Boolean value) {
        this.emptyCellReference = value;
    }

    /**
     * Gets the value of the listDataValidation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isListDataValidation() {
        if (listDataValidation == null) {
            return false;
        }
        return listDataValidation;
    }

    /**
     * Sets the value of the listDataValidation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setListDataValidation(Boolean value) {
        this.listDataValidation = value;
    }

    /**
     * Gets the value of the calculatedColumn property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCalculatedColumn() {
        if (calculatedColumn == null) {
            return false;
        }
        return calculatedColumn;
    }

    /**
     * Sets the value of the calculatedColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCalculatedColumn(Boolean value) {
        this.calculatedColumn = value;
    }
}
