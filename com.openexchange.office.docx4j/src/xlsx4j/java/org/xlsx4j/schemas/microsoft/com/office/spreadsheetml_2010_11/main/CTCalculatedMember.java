
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CalculatedMember complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CalculatedMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="measureGroup" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="numberFormat" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}ST_CalcMemNumberFormat" default="default" />
 *       &lt;attribute name="measure" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CalculatedMember")
public class CTCalculatedMember {

    @XmlAttribute(name = "measureGroup")
    protected String measureGroup;
    @XmlAttribute(name = "numberFormat")
    protected STCalcMemNumberFormat numberFormat;
    @XmlAttribute(name = "measure")
    protected Boolean measure;

    /**
     * Gets the value of the measureGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureGroup() {
        return measureGroup;
    }

    /**
     * Sets the value of the measureGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureGroup(String value) {
        this.measureGroup = value;
    }

    /**
     * Gets the value of the numberFormat property.
     * 
     * @return
     *     possible object is
     *     {@link STCalcMemNumberFormat }
     *     
     */
    public STCalcMemNumberFormat getNumberFormat() {
        if (numberFormat == null) {
            return STCalcMemNumberFormat.DEFAULT;
        }
        return numberFormat;
    }

    /**
     * Sets the value of the numberFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link STCalcMemNumberFormat }
     *     
     */
    public void setNumberFormat(STCalcMemNumberFormat value) {
        this.numberFormat = value;
    }

    /**
     * Gets the value of the measure property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMeasure() {
        if (measure == null) {
            return false;
        }
        return measure;
    }

    /**
     * Sets the value of the measure property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeasure(Boolean value) {
        this.measure = value;
    }
}
