
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SlicerStyleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SlicerStyleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="unselectedItemWithData"/>
 *     &lt;enumeration value="selectedItemWithData"/>
 *     &lt;enumeration value="unselectedItemWithNoData"/>
 *     &lt;enumeration value="selectedItemWithNoData"/>
 *     &lt;enumeration value="hoveredUnselectedItemWithData"/>
 *     &lt;enumeration value="hoveredSelectedItemWithData"/>
 *     &lt;enumeration value="hoveredUnselectedItemWithNoData"/>
 *     &lt;enumeration value="hoveredSelectedItemWithNoData"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SlicerStyleType")
@XmlEnum
public enum STSlicerStyleType {

    @XmlEnumValue("unselectedItemWithData")
    UNSELECTED_ITEM_WITH_DATA("unselectedItemWithData"),
    @XmlEnumValue("selectedItemWithData")
    SELECTED_ITEM_WITH_DATA("selectedItemWithData"),
    @XmlEnumValue("unselectedItemWithNoData")
    UNSELECTED_ITEM_WITH_NO_DATA("unselectedItemWithNoData"),
    @XmlEnumValue("selectedItemWithNoData")
    SELECTED_ITEM_WITH_NO_DATA("selectedItemWithNoData"),
    @XmlEnumValue("hoveredUnselectedItemWithData")
    HOVERED_UNSELECTED_ITEM_WITH_DATA("hoveredUnselectedItemWithData"),
    @XmlEnumValue("hoveredSelectedItemWithData")
    HOVERED_SELECTED_ITEM_WITH_DATA("hoveredSelectedItemWithData"),
    @XmlEnumValue("hoveredUnselectedItemWithNoData")
    HOVERED_UNSELECTED_ITEM_WITH_NO_DATA("hoveredUnselectedItemWithNoData"),
    @XmlEnumValue("hoveredSelectedItemWithNoData")
    HOVERED_SELECTED_ITEM_WITH_NO_DATA("hoveredSelectedItemWithNoData");
    private final String value;

    STSlicerStyleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSlicerStyleType fromValue(String v) {
        for (STSlicerStyleType c: STSlicerStyleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
