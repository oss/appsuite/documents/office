
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SurveyQuestions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SurveyQuestions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="questionsPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyElementPr" minOccurs="0"/>
 *         &lt;element name="question" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyQuestion" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SurveyQuestions", propOrder = {
    "questionsPr",
    "question"
})
public class CTSurveyQuestions {

    protected CTSurveyElementPr questionsPr;
    @XmlElement(required = true)
    protected List<CTSurveyQuestion> question;

    /**
     * Gets the value of the questionsPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public CTSurveyElementPr getQuestionsPr() {
        return questionsPr;
    }

    /**
     * Sets the value of the questionsPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public void setQuestionsPr(CTSurveyElementPr value) {
        this.questionsPr = value;
    }

    /**
     * Gets the value of the question property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the question property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSurveyQuestion }
     * 
     * 
     */
    public List<CTSurveyQuestion> getQuestion() {
        if (question == null) {
            question = new ArrayList<CTSurveyQuestion>();
        }
        return this.question;
    }
}
