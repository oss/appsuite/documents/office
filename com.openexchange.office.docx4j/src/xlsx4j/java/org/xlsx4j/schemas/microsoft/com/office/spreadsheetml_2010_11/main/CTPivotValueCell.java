
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotValueCell complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotValueCell">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="v" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring"/>
 *         &lt;element name="x" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_PivotValueCellExtra" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="i" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="t" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}ST_SXVCellType" default="n" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotValueCell", propOrder = {
    "v",
    "x"
})
public class CTPivotValueCell {

    @XmlElement(required = true)
    protected String v;
    protected CTPivotValueCellExtra x;
    @XmlAttribute(name = "i")
    @XmlSchemaType(name = "unsignedInt")
    protected Long i;
    @XmlAttribute(name = "t")
    protected STSXVCellType t;

    /**
     * Gets the value of the v property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getV() {
        return v;
    }

    /**
     * Sets the value of the v property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setV(String value) {
        this.v = value;
    }

    /**
     * Gets the value of the x property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotValueCellExtra }
     *     
     */
    public CTPivotValueCellExtra getX() {
        return x;
    }

    /**
     * Sets the value of the x property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotValueCellExtra }
     *     
     */
    public void setX(CTPivotValueCellExtra value) {
        this.x = value;
    }

    /**
     * Gets the value of the i property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getI() {
        return i;
    }

    /**
     * Sets the value of the i property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setI(Long value) {
        this.i = value;
    }

    /**
     * Gets the value of the t property.
     * 
     * @return
     *     possible object is
     *     {@link STSXVCellType }
     *     
     */
    public STSXVCellType getT() {
        if (t == null) {
            return STSXVCellType.N;
        }
        return t;
    }

    /**
     * Sets the value of the t property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSXVCellType }
     *     
     */
    public void setT(STSXVCellType value) {
        this.t = value;
    }
}
