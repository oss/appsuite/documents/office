
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerCacheDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerCacheDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotTables" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SlicerCachePivotTables" minOccurs="0"/>
 *         &lt;element name="data" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SlicerCacheData" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="sourceName" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerCacheDefinition", propOrder = {
    "pivotTables",
    "data",
    "extLst"
})
public class CTSlicerCacheDefinition {

    protected CTSlicerCachePivotTables pivotTables;
    protected CTSlicerCacheData data;
    protected Object extLst;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "sourceName", required = true)
    protected String sourceName;

    /**
     * Gets the value of the pivotTables property.
     * 
     * @return
     *     possible object is
     *     {@link CTSlicerCachePivotTables }
     *     
     */
    public CTSlicerCachePivotTables getPivotTables() {
        return pivotTables;
    }

    /**
     * Sets the value of the pivotTables property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSlicerCachePivotTables }
     *     
     */
    public void setPivotTables(CTSlicerCachePivotTables value) {
        this.pivotTables = value;
    }

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link CTSlicerCacheData }
     *     
     */
    public CTSlicerCacheData getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSlicerCacheData }
     *     
     */
    public void setData(CTSlicerCacheData value) {
        this.data = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setExtLst(Object value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the sourceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * Sets the value of the sourceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceName(String value) {
        this.sourceName = value;
    }
}
