
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.STFilterOperator;


/**
 * <p>Java class for CT_CustomFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CustomFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="operator" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_FilterOperator" default="equal" />
 *       &lt;attribute name="val" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CustomFilter")
public class CTCustomFilter {

    @XmlAttribute(name = "operator")
    protected STFilterOperator operator;
    @XmlAttribute(name = "val")
    protected String val;

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link STFilterOperator }
     *     
     */
    public STFilterOperator getOperator() {
        if (operator == null) {
            return STFilterOperator.EQUAL;
        }
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link STFilterOperator }
     *     
     */
    public void setOperator(STFilterOperator value) {
        this.operator = value;
    }

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal(String value) {
        this.val = value;
    }
}
