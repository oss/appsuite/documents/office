
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xlsx4j.sml.CTDxf;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTStylesheet;
import org.xlsx4j.sml.ICfRule;
import org.xlsx4j.sml.STCfType;
import org.xlsx4j.sml.STConditionalFormattingOperator;
import org.xlsx4j.sml.STTimePeriod;
import org.xlsx4j.util.StringCodec;


/**
 * <p>Java class for CT_CfRule complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_CfRule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="colorScale" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_ColorScale" minOccurs="0"/>
 *         &lt;element name="dataBar" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_DataBar" minOccurs="0"/>
 *         &lt;element name="iconSet" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_IconSet" minOccurs="0"/>
 *         &lt;element name="dxf" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Dxf" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_CfType" />
 *       &lt;attribute name="priority" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="stopIfTrue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="aboveAverage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="percent" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="bottom" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="operator" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_ConditionalFormattingOperator" />
 *       &lt;attribute name="text" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="timePeriod" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_TimePeriod" />
 *       &lt;attribute name="rank" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="stdDev" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="equalAverage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="activePresent" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="id" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Guid" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CfRule", propOrder = {
    "f",
    "colorScale",
    "dataBar",
    "iconSet",
    "dxf",
    "extLst"
})
public class CTCfRule implements ICfRule {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected List<String> f;
    protected CTColorScale colorScale;
    protected CTDataBar dataBar;
    protected CTIconSet iconSet;
    protected CTDxf dxf;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "type")
    protected STCfType type;
    @XmlAttribute(name = "priority")
    protected Integer priority;
    @XmlAttribute(name = "stopIfTrue")
    protected Boolean stopIfTrue;
    @XmlAttribute(name = "aboveAverage")
    protected Boolean aboveAverage;
    @XmlAttribute(name = "percent")
    protected Boolean percent;
    @XmlAttribute(name = "bottom")
    protected Boolean bottom;
    @XmlAttribute(name = "operator")
    protected STConditionalFormattingOperator operator;
    @XmlAttribute(name = "text")
    protected String text;
    @XmlAttribute(name = "timePeriod")
    protected STTimePeriod timePeriod;
    @XmlAttribute(name = "rank")
    @XmlSchemaType(name = "unsignedInt")
    protected Long rank;
    @XmlAttribute(name = "stdDev")
    protected Integer stdDev;
    @XmlAttribute(name = "equalAverage")
    protected Boolean equalAverage;
    @XmlAttribute(name = "activePresent")
    protected Boolean activePresent;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;

    /**
     * Gets the value of the f property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the f property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getF().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Override
    public List<String> getFormula() {
        if (f == null) {
            f = new ArrayList<String>();
        }
        return f;
    }

    /**
     * Gets the value of the colorScale property.
     *
     * @return
     *     possible object is
     *     {@link CTColorScale }
     *
     */
    @Override
    public CTColorScale getColorScale() {
        return colorScale;
    }

    /**
     * Sets the value of the colorScale property.
     *
     * @param value
     *     allowed object is
     *     {@link CTColorScale }
     *
     */
    public void setColorScale(CTColorScale value) {
        this.colorScale = value;
    }

    /**
     * Gets the value of the dataBar property.
     *
     * @return
     *     possible object is
     *     {@link CTDataBar }
     *
     */
    @Override
    public CTDataBar getDataBar() {
        return dataBar;
    }

    /**
     * Sets the value of the dataBar property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDataBar }
     *
     */
    public void setDataBar(CTDataBar value) {
        this.dataBar = value;
    }

    /**
     * Gets the value of the iconSet property.
     *
     * @return
     *     possible object is
     *     {@link CTIconSet }
     *
     */
    @Override
    public CTIconSet getIconSet() {
        return iconSet;
    }

    /**
     * Sets the value of the iconSet property.
     *
     * @param value
     *     allowed object is
     *     {@link CTIconSet }
     *
     */
    public void setIconSet(CTIconSet value) {
        this.iconSet = value;
    }

    /**
     * Sets the value of the dxf property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDxf }
     *
     */
    @Override
    public void setDxf(CTStylesheet stylesheet, CTDxf value) {
        this.dxf = value;
    }

    @Override
    public CTDxf getDxf(CTStylesheet styleSheet) {
    	return dxf;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link STCfType }
     *
     */
    @Override
    public STCfType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link STCfType }
     *
     */
    @Override
    public void setType(STCfType value) {
        this.type = value;
    }

    /**
     * Gets the value of the priority property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    @Override
    public Integer getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    @Override
    public void setPriority(int value) {
        this.priority = value;
    }

    /**
     * Gets the value of the stopIfTrue property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isStopIfTrue() {
        if (stopIfTrue == null) {
            return false;
        }
        return stopIfTrue;
    }

    /**
     * Sets the value of the stopIfTrue property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setStopIfTrue(Boolean value) {
        this.stopIfTrue = value;
    }

    /**
     * Gets the value of the aboveAverage property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isAboveAverage() {
        if (aboveAverage == null) {
            return true;
        }
        return aboveAverage;
    }

    /**
     * Sets the value of the aboveAverage property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setAboveAverage(Boolean value) {
        this.aboveAverage = value;
    }

    /**
     * Gets the value of the percent property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isPercent() {
        if (percent == null) {
            return false;
        }
        return percent;
    }

    /**
     * Sets the value of the percent property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setPercent(Boolean value) {
        this.percent = value;
    }

    /**
     * Gets the value of the bottom property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isBottom() {
        if (bottom == null) {
            return false;
        }
        return bottom;
    }

    /**
     * Sets the value of the bottom property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setBottom(Boolean value) {
        this.bottom = value;
    }

    /**
     * Gets the value of the operator property.
     *
     * @return
     *     possible object is
     *     {@link STConditionalFormattingOperator }
     *
     */
    @Override
    public STConditionalFormattingOperator getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     *
     * @param value
     *     allowed object is
     *     {@link STConditionalFormattingOperator }
     *
     */
    @Override
    public void setOperator(STConditionalFormattingOperator value) {
        this.operator = value;
    }

    /**
     * Gets the value of the text property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Override
    public String getText() {
        // Excel does NOT encode the "text" attribute!
        return text;
    }

    /**
     * Sets the value of the text property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Override
    public void setText(String value) {
        // Excel does NOT encode the "text" attribute!
        this.text = value;
    }

    /**
     * Gets the value of the timePeriod property.
     *
     * @return
     *     possible object is
     *     {@link STTimePeriod }
     *
     */
    @Override
    public STTimePeriod getTimePeriod() {
        return timePeriod;
    }

    /**
     * Sets the value of the timePeriod property.
     *
     * @param value
     *     allowed object is
     *     {@link STTimePeriod }
     *
     */
    @Override
    public void setTimePeriod(STTimePeriod value) {
        this.timePeriod = value;
    }

    /**
     * Gets the value of the rank property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @Override
    public Long getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    @Override
    public void setRank(Long value) {
        this.rank = value;
    }

    /**
     * Gets the value of the stdDev property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    @Override
    public Integer getStdDev() {
        return stdDev;
    }

    /**
     * Sets the value of the stdDev property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    @Override
    public void setStdDev(Integer value) {
        this.stdDev = value;
    }

    /**
     * Gets the value of the equalAverage property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isEqualAverage() {
        if (equalAverage == null) {
            return false;
        }
        return equalAverage;
    }

    /**
     * Sets the value of the equalAverage property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setEqualAverage(Boolean value) {
        this.equalAverage = value;
    }

    /**
     * Gets the value of the activePresent property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isActivePresent() {
        if (activePresent == null) {
            return false;
        }
        return activePresent;
    }

    /**
     * Sets the value of the activePresent property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setActivePresent(Boolean value) {
        this.activePresent = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Override
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Encodes strings before writing XML.
     */
    @SuppressWarnings("unused")
    public void beforeMarshal(Marshaller marshaller) {
        f = StringCodec.encode(f);
    }

    /**
     * Decodes strings after parsing XML.
     */
    @SuppressWarnings("unused")
    public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        f = StringCodec.decode(f);
    }
}
