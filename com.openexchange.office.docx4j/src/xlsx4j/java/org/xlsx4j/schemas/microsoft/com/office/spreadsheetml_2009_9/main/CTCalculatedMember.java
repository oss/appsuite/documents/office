
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CalculatedMember complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CalculatedMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tupleSet" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TupleSet" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="displayFolder" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="flattenHierarchies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="dynamicSet" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="hierarchizeDistinct" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="mdxLong" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CalculatedMember", propOrder = {
    "tupleSet"
})
public class CTCalculatedMember {

    protected CTTupleSet tupleSet;
    @XmlAttribute(name = "displayFolder")
    protected String displayFolder;
    @XmlAttribute(name = "flattenHierarchies")
    protected Boolean flattenHierarchies;
    @XmlAttribute(name = "dynamicSet")
    protected Boolean dynamicSet;
    @XmlAttribute(name = "hierarchizeDistinct")
    protected Boolean hierarchizeDistinct;
    @XmlAttribute(name = "mdxLong")
    protected String mdxLong;

    /**
     * Gets the value of the tupleSet property.
     * 
     * @return
     *     possible object is
     *     {@link CTTupleSet }
     *     
     */
    public CTTupleSet getTupleSet() {
        return tupleSet;
    }

    /**
     * Sets the value of the tupleSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTupleSet }
     *     
     */
    public void setTupleSet(CTTupleSet value) {
        this.tupleSet = value;
    }

    /**
     * Gets the value of the displayFolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayFolder() {
        return displayFolder;
    }

    /**
     * Sets the value of the displayFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayFolder(String value) {
        this.displayFolder = value;
    }

    /**
     * Gets the value of the flattenHierarchies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFlattenHierarchies() {
        if (flattenHierarchies == null) {
            return true;
        }
        return flattenHierarchies;
    }

    /**
     * Sets the value of the flattenHierarchies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlattenHierarchies(Boolean value) {
        this.flattenHierarchies = value;
    }

    /**
     * Gets the value of the dynamicSet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDynamicSet() {
        if (dynamicSet == null) {
            return false;
        }
        return dynamicSet;
    }

    /**
     * Sets the value of the dynamicSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDynamicSet(Boolean value) {
        this.dynamicSet = value;
    }

    /**
     * Gets the value of the hierarchizeDistinct property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHierarchizeDistinct() {
        if (hierarchizeDistinct == null) {
            return true;
        }
        return hierarchizeDistinct;
    }

    /**
     * Sets the value of the hierarchizeDistinct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHierarchizeDistinct(Boolean value) {
        this.hierarchizeDistinct = value;
    }

    /**
     * Gets the value of the mdxLong property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdxLong() {
        return mdxLong;
    }

    /**
     * Sets the value of the mdxLong property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdxLong(String value) {
        this.mdxLong = value;
    }
}
