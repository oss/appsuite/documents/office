
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotCacheDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotCacheDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="slicerData" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="pivotCacheId" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="supportSubqueryNonVisual" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="supportSubqueryCalcMem" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="supportAddCalcMems" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotCacheDefinition")
public class CTPivotCacheDefinition {

    @XmlAttribute(name = "slicerData")
    protected Boolean slicerData;
    @XmlAttribute(name = "pivotCacheId")
    @XmlSchemaType(name = "unsignedInt")
    protected Long pivotCacheId;
    @XmlAttribute(name = "supportSubqueryNonVisual")
    protected Boolean supportSubqueryNonVisual;
    @XmlAttribute(name = "supportSubqueryCalcMem")
    protected Boolean supportSubqueryCalcMem;
    @XmlAttribute(name = "supportAddCalcMems")
    protected Boolean supportAddCalcMems;

    /**
     * Gets the value of the slicerData property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSlicerData() {
        if (slicerData == null) {
            return false;
        }
        return slicerData;
    }

    /**
     * Sets the value of the slicerData property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSlicerData(Boolean value) {
        this.slicerData = value;
    }

    /**
     * Gets the value of the pivotCacheId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPivotCacheId() {
        return pivotCacheId;
    }

    /**
     * Sets the value of the pivotCacheId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPivotCacheId(Long value) {
        this.pivotCacheId = value;
    }

    /**
     * Gets the value of the supportSubqueryNonVisual property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSupportSubqueryNonVisual() {
        if (supportSubqueryNonVisual == null) {
            return false;
        }
        return supportSubqueryNonVisual;
    }

    /**
     * Sets the value of the supportSubqueryNonVisual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportSubqueryNonVisual(Boolean value) {
        this.supportSubqueryNonVisual = value;
    }

    /**
     * Gets the value of the supportSubqueryCalcMem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSupportSubqueryCalcMem() {
        if (supportSubqueryCalcMem == null) {
            return false;
        }
        return supportSubqueryCalcMem;
    }

    /**
     * Sets the value of the supportSubqueryCalcMem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportSubqueryCalcMem(Boolean value) {
        this.supportSubqueryCalcMem = value;
    }

    /**
     * Gets the value of the supportAddCalcMems property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSupportAddCalcMems() {
        if (supportAddCalcMems == null) {
            return false;
        }
        return supportAddCalcMems;
    }

    /**
     * Sets the value of the supportAddCalcMems property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportAddCalcMems(Boolean value) {
        this.supportAddCalcMems = value;
    }
}
