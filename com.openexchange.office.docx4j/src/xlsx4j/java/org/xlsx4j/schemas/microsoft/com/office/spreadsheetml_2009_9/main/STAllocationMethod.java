
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_AllocationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_AllocationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="equalAllocation"/>
 *     &lt;enumeration value="equalIncrement"/>
 *     &lt;enumeration value="weightedAllocation"/>
 *     &lt;enumeration value="weightedIncrement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_AllocationMethod")
@XmlEnum
public enum STAllocationMethod {

    @XmlEnumValue("equalAllocation")
    EQUAL_ALLOCATION("equalAllocation"),
    @XmlEnumValue("equalIncrement")
    EQUAL_INCREMENT("equalIncrement"),
    @XmlEnumValue("weightedAllocation")
    WEIGHTED_ALLOCATION("weightedAllocation"),
    @XmlEnumValue("weightedIncrement")
    WEIGHTED_INCREMENT("weightedIncrement");
    private final String value;

    STAllocationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STAllocationMethod fromValue(String v) {
        for (STAllocationMethod c: STAllocationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
