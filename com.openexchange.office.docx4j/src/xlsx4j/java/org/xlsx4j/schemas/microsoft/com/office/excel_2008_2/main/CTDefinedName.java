
package org.xlsx4j.schemas.microsoft.com.office.excel_2008_2.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;



/**
 * <p>Java class for CT_DefinedName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_DefinedName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="argumentDescriptions" type="{http://schemas.microsoft.com/office/excel/2008/2/main}CT_DefinedNameArgumentDescriptions" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/officeDocument/2006/sharedTypes}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DefinedName", propOrder = {
    "argumentDescriptions"
})
public class CTDefinedName
{
    protected CTDefinedNameArgumentDescriptions argumentDescriptions;
    @XmlAttribute(name = "name", required = true)
    protected String name;

    /**
     * Gets the value of the argumentDescriptions property.
     * 
     * @return
     *     possible object is
     *     {@link CTDefinedNameArgumentDescriptions }
     *     
     */
    public CTDefinedNameArgumentDescriptions getArgumentDescriptions() {
        return argumentDescriptions;
    }

    /**
     * Sets the value of the argumentDescriptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDefinedNameArgumentDescriptions }
     *     
     */
    public void setArgumentDescriptions(CTDefinedNameArgumentDescriptions value) {
        this.argumentDescriptions = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }
}
