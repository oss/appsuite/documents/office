
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTCalculatedMembers;


/**
 * <p>Java class for CT_Connection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Connection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="calculatedMembers" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_CalculatedMembers" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="culture" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="embeddedDataId" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Connection", propOrder = {
    "calculatedMembers"
})
public class CTConnection {

    protected CTCalculatedMembers calculatedMembers;
    @XmlAttribute(name = "culture")
    protected String culture;
    @XmlAttribute(name = "embeddedDataId")
    protected String embeddedDataId;

    /**
     * Gets the value of the calculatedMembers property.
     * 
     * @return
     *     possible object is
     *     {@link CTCalculatedMembers }
     *     
     */
    public CTCalculatedMembers getCalculatedMembers() {
        return calculatedMembers;
    }

    /**
     * Sets the value of the calculatedMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTCalculatedMembers }
     *     
     */
    public void setCalculatedMembers(CTCalculatedMembers value) {
        this.calculatedMembers = value;
    }

    /**
     * Gets the value of the culture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCulture() {
        return culture;
    }

    /**
     * Sets the value of the culture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCulture(String value) {
        this.culture = value;
    }

    /**
     * Gets the value of the embeddedDataId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmbeddedDataId() {
        return embeddedDataId;
    }

    /**
     * Sets the value of the embeddedDataId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmbeddedDataId(String value) {
        this.embeddedDataId = value;
    }
}
