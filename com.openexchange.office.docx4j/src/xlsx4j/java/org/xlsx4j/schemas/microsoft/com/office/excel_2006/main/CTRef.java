
package org.xlsx4j.schemas.microsoft.com.office.excel_2006.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for CT_Ref complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Ref">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://schemas.microsoft.com/office/excel/2006/main>ST_Ref">
 *       &lt;attribute name="edited" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="adjusted" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="adjust" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Ref", propOrder = {
    "value"
})
public class CTRef {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "edited")
    protected Boolean edited;
    @XmlAttribute(name = "adjusted")
    protected Boolean adjusted;
    @XmlAttribute(name = "adjust")
    protected Boolean adjust;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the edited property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEdited() {
        return edited;
    }

    /**
     * Sets the value of the edited property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEdited(Boolean value) {
        this.edited = value;
    }

    /**
     * Gets the value of the adjusted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdjusted() {
        return adjusted;
    }

    /**
     * Sets the value of the adjusted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdjusted(Boolean value) {
        this.adjusted = value;
    }

    /**
     * Gets the value of the adjust property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdjust() {
        return adjust;
    }

    /**
     * Sets the value of the adjust property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdjust(Boolean value) {
        this.adjust = value;
    }
}
