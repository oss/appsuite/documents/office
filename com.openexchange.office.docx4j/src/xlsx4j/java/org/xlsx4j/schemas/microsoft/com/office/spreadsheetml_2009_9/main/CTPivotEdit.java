
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTPivotArea;


/**
 * <p>Java class for CT_PivotEdit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotEdit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userEdit" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_PivotUserEdit"/>
 *         &lt;element name="tupleItems" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TupleItems"/>
 *         &lt;element name="pivotArea" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PivotArea"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotEdit", propOrder = {
    "userEdit",
    "tupleItems",
    "pivotArea",
    "extLst"
})
public class CTPivotEdit {

    @XmlElement(required = true)
    protected CTPivotUserEdit userEdit;
    @XmlElement(required = true)
    protected CTTupleItems tupleItems;
    @XmlElement(required = true)
    protected CTPivotArea pivotArea;
    protected CTExtensionList extLst;

    /**
     * Gets the value of the userEdit property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotUserEdit }
     *     
     */
    public CTPivotUserEdit getUserEdit() {
        return userEdit;
    }

    /**
     * Sets the value of the userEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotUserEdit }
     *     
     */
    public void setUserEdit(CTPivotUserEdit value) {
        this.userEdit = value;
    }

    /**
     * Gets the value of the tupleItems property.
     * 
     * @return
     *     possible object is
     *     {@link CTTupleItems }
     *     
     */
    public CTTupleItems getTupleItems() {
        return tupleItems;
    }

    /**
     * Sets the value of the tupleItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTupleItems }
     *     
     */
    public void setTupleItems(CTTupleItems value) {
        this.tupleItems = value;
    }

    /**
     * Gets the value of the pivotArea property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotArea }
     *     
     */
    public CTPivotArea getPivotArea() {
        return pivotArea;
    }

    /**
     * Sets the value of the pivotArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotArea }
     *     
     */
    public void setPivotArea(CTPivotArea value) {
        this.pivotArea = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }
}
