
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_DropStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_DropStyle">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="combo"/>
 *     &lt;enumeration value="comboedit"/>
 *     &lt;enumeration value="simple"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_DropStyle")
@XmlEnum
public enum STDropStyle {

    @XmlEnumValue("combo")
    COMBO("combo"),
    @XmlEnumValue("comboedit")
    COMBOEDIT("comboedit"),
    @XmlEnumValue("simple")
    SIMPLE("simple");
    private final String value;

    STDropStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STDropStyle fromValue(String v) {
        for (STDropStyle c: STDropStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
