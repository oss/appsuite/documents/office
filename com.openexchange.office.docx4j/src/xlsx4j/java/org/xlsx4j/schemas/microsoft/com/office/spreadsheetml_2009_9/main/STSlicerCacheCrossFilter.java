
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SlicerCacheCrossFilter.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SlicerCacheCrossFilter">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="none"/>
 *     &lt;enumeration value="showItemsWithDataAtTop"/>
 *     &lt;enumeration value="showItemsWithNoData"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SlicerCacheCrossFilter")
@XmlEnum
public enum STSlicerCacheCrossFilter {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("showItemsWithDataAtTop")
    SHOW_ITEMS_WITH_DATA_AT_TOP("showItemsWithDataAtTop"),
    @XmlEnumValue("showItemsWithNoData")
    SHOW_ITEMS_WITH_NO_DATA("showItemsWithNoData");
    private final String value;

    STSlicerCacheCrossFilter(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSlicerCacheCrossFilter fromValue(String v) {
        for (STSlicerCacheCrossFilter c: STSlicerCacheCrossFilter.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
