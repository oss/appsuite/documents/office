
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerStyleElements complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerStyleElements">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="slicerStyleElement" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SlicerStyleElement" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerStyleElements", propOrder = {
    "slicerStyleElement"
})
public class CTSlicerStyleElements {

    @XmlElement(required = true)
    protected List<CTSlicerStyleElement> slicerStyleElement;

    /**
     * Gets the value of the slicerStyleElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the slicerStyleElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSlicerStyleElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSlicerStyleElement }
     * 
     * 
     */
    public List<CTSlicerStyleElement> getSlicerStyleElement() {
        if (slicerStyleElement == null) {
            slicerStyleElement = new ArrayList<CTSlicerStyleElement>();
        }
        return this.slicerStyleElement;
    }
}
