
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_DispBlanksAs.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_DispBlanksAs">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="span"/>
 *     &lt;enumeration value="gap"/>
 *     &lt;enumeration value="zero"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_DispBlanksAs")
@XmlEnum
public enum STDispBlanksAs {

    @XmlEnumValue("span")
    SPAN("span"),
    @XmlEnumValue("gap")
    GAP("gap"),
    @XmlEnumValue("zero")
    ZERO("zero");
    private final String value;

    STDispBlanksAs(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STDispBlanksAs fromValue(String v) {
        for (STDispBlanksAs c: STDispBlanksAs.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
