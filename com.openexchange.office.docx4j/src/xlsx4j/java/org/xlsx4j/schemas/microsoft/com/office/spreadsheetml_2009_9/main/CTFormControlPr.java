
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_FormControlPr complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_FormControlPr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itemLst" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_ListItems" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="objectType" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_ObjectType" />
 *       &lt;attribute name="checked" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_Checked" />
 *       &lt;attribute name="colored" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="dropLines" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="8" />
 *       &lt;attribute name="dropStyle" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_DropStyle" />
 *       &lt;attribute name="dx" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="80" />
 *       &lt;attribute name="firstButton" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="fmlaGroup" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Formula" />
 *       &lt;attribute name="fmlaLink" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Formula" />
 *       &lt;attribute name="fmlaRange" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Formula" />
 *       &lt;attribute name="fmlaTxbx" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Formula" />
 *       &lt;attribute name="horiz" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="inc" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="1" />
 *       &lt;attribute name="justLastX" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="lockText" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="max" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="min" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *       &lt;attribute name="multiSel" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="noThreeD" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="noThreeD2" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="page" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="sel" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="seltype" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SelType" default="single" />
 *       &lt;attribute name="textHAlign" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_TextHAlign" default="left" />
 *       &lt;attribute name="textVAlign" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_TextVAlign" default="top" />
 *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="widthMin" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="editVal" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_EditValidation" />
 *       &lt;attribute name="multiLine" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="verticalBar" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="passwordEdit" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_FormControlPr", propOrder = {
    "itemLst",
    "extLst"
})
public class CTFormControlPr {

    protected CTListItems itemLst;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "objectType")
    protected STObjectType objectType;
    @XmlAttribute(name = "checked")
    protected STChecked checked;
    @XmlAttribute(name = "colored")
    protected Boolean colored;
    @XmlAttribute(name = "dropLines")
    @XmlSchemaType(name = "unsignedInt")
    protected Long dropLines;
    @XmlAttribute(name = "dropStyle")
    protected STDropStyle dropStyle;
    @XmlAttribute(name = "dx")
    @XmlSchemaType(name = "unsignedInt")
    protected Long dx;
    @XmlAttribute(name = "firstButton")
    protected Boolean firstButton;
    @XmlAttribute(name = "fmlaGroup")
    protected String fmlaGroup;
    @XmlAttribute(name = "fmlaLink")
    protected String fmlaLink;
    @XmlAttribute(name = "fmlaRange")
    protected String fmlaRange;
    @XmlAttribute(name = "fmlaTxbx")
    protected String fmlaTxbx;
    @XmlAttribute(name = "horiz")
    protected Boolean horiz;
    @XmlAttribute(name = "inc")
    @XmlSchemaType(name = "unsignedInt")
    protected Long inc;
    @XmlAttribute(name = "justLastX")
    protected Boolean justLastX;
    @XmlAttribute(name = "lockText")
    protected Boolean lockText;
    @XmlAttribute(name = "max")
    @XmlSchemaType(name = "unsignedInt")
    protected Long max;
    @XmlAttribute(name = "min")
    @XmlSchemaType(name = "unsignedInt")
    protected Long min;
    @XmlAttribute(name = "multiSel")
    protected String multiSel;
    @XmlAttribute(name = "noThreeD")
    protected Boolean noThreeD;
    @XmlAttribute(name = "noThreeD2")
    protected Boolean noThreeD2;
    @XmlAttribute(name = "page")
    @XmlSchemaType(name = "unsignedInt")
    protected Long page;
    @XmlAttribute(name = "sel")
    @XmlSchemaType(name = "unsignedInt")
    protected Long sel;
    @XmlAttribute(name = "seltype")
    protected STSelType seltype;
    @XmlAttribute(name = "textHAlign")
    protected STTextHAlign textHAlign;
    @XmlAttribute(name = "textVAlign")
    protected STTextVAlign textVAlign;
    @XmlAttribute(name = "val")
    @XmlSchemaType(name = "unsignedInt")
    protected Long val;
    @XmlAttribute(name = "widthMin")
    @XmlSchemaType(name = "unsignedInt")
    protected Long widthMin;
    @XmlAttribute(name = "editVal")
    protected STEditValidation editVal;
    @XmlAttribute(name = "multiLine")
    protected Boolean multiLine;
    @XmlAttribute(name = "verticalBar")
    protected Boolean verticalBar;
    @XmlAttribute(name = "passwordEdit")
    protected Boolean passwordEdit;

    /**
     * Gets the value of the itemLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTListItems }
     *     
     */
    public CTListItems getItemLst() {
        return itemLst;
    }

    /**
     * Sets the value of the itemLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTListItems }
     *     
     */
    public void setItemLst(CTListItems value) {
        this.itemLst = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the objectType property.
     * 
     * @return
     *     possible object is
     *     {@link STObjectType }
     *     
     */
    public STObjectType getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STObjectType }
     *     
     */
    public void setObjectType(STObjectType value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the checked property.
     * 
     * @return
     *     possible object is
     *     {@link STChecked }
     *     
     */
    public STChecked getChecked() {
        return checked;
    }

    /**
     * Sets the value of the checked property.
     * 
     * @param value
     *     allowed object is
     *     {@link STChecked }
     *     
     */
    public void setChecked(STChecked value) {
        this.checked = value;
    }

    /**
     * Gets the value of the colored property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isColored() {
        if (colored == null) {
            return false;
        }
        return colored;
    }

    /**
     * Sets the value of the colored property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setColored(Boolean value) {
        this.colored = value;
    }

    /**
     * Gets the value of the dropLines property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getDropLines() {
        if (dropLines == null) {
            return  8L;
        }
        return dropLines;
    }

    /**
     * Sets the value of the dropLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDropLines(Long value) {
        this.dropLines = value;
    }

    /**
     * Gets the value of the dropStyle property.
     * 
     * @return
     *     possible object is
     *     {@link STDropStyle }
     *     
     */
    public STDropStyle getDropStyle() {
        return dropStyle;
    }

    /**
     * Sets the value of the dropStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDropStyle }
     *     
     */
    public void setDropStyle(STDropStyle value) {
        this.dropStyle = value;
    }

    /**
     * Gets the value of the dx property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getDx() {
        if (dx == null) {
            return  80L;
        }
        return dx;
    }

    /**
     * Sets the value of the dx property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDx(Long value) {
        this.dx = value;
    }

    /**
     * Gets the value of the firstButton property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFirstButton() {
        if (firstButton == null) {
            return false;
        }
        return firstButton;
    }

    /**
     * Sets the value of the firstButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirstButton(Boolean value) {
        this.firstButton = value;
    }

    /**
     * Gets the value of the fmlaGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFmlaGroup() {
        return fmlaGroup;
    }

    /**
     * Sets the value of the fmlaGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFmlaGroup(String value) {
        this.fmlaGroup = value;
    }

    /**
     * Gets the value of the fmlaLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFmlaLink() {
        return fmlaLink;
    }

    /**
     * Sets the value of the fmlaLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFmlaLink(String value) {
        this.fmlaLink = value;
    }

    /**
     * Gets the value of the fmlaRange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFmlaRange() {
        return fmlaRange;
    }

    /**
     * Sets the value of the fmlaRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFmlaRange(String value) {
        this.fmlaRange = value;
    }

    /**
     * Gets the value of the fmlaTxbx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFmlaTxbx() {
        return fmlaTxbx;
    }

    /**
     * Sets the value of the fmlaTxbx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFmlaTxbx(String value) {
        this.fmlaTxbx = value;
    }

    /**
     * Gets the value of the horiz property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHoriz() {
        if (horiz == null) {
            return false;
        }
        return horiz;
    }

    /**
     * Sets the value of the horiz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHoriz(Boolean value) {
        this.horiz = value;
    }

    /**
     * Gets the value of the inc property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getInc() {
        if (inc == null) {
            return  1L;
        }
        return inc;
    }

    /**
     * Sets the value of the inc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInc(Long value) {
        this.inc = value;
    }

    /**
     * Gets the value of the justLastX property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isJustLastX() {
        if (justLastX == null) {
            return false;
        }
        return justLastX;
    }

    /**
     * Sets the value of the justLastX property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJustLastX(Boolean value) {
        this.justLastX = value;
    }

    /**
     * Gets the value of the lockText property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLockText() {
        if (lockText == null) {
            return false;
        }
        return lockText;
    }

    /**
     * Sets the value of the lockText property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockText(Boolean value) {
        this.lockText = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMax(Long value) {
        this.max = value;
    }

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getMin() {
        if (min == null) {
            return  0L;
        }
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMin(Long value) {
        this.min = value;
    }

    /**
     * Gets the value of the multiSel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMultiSel() {
        return multiSel;
    }

    /**
     * Sets the value of the multiSel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMultiSel(String value) {
        this.multiSel = value;
    }

    /**
     * Gets the value of the noThreeD property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoThreeD() {
        if (noThreeD == null) {
            return false;
        }
        return noThreeD;
    }

    /**
     * Sets the value of the noThreeD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoThreeD(Boolean value) {
        this.noThreeD = value;
    }

    /**
     * Gets the value of the noThreeD2 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoThreeD2() {
        if (noThreeD2 == null) {
            return false;
        }
        return noThreeD2;
    }

    /**
     * Sets the value of the noThreeD2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoThreeD2(Boolean value) {
        this.noThreeD2 = value;
    }

    /**
     * Gets the value of the page property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPage(Long value) {
        this.page = value;
    }

    /**
     * Gets the value of the sel property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSel() {
        return sel;
    }

    /**
     * Sets the value of the sel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSel(Long value) {
        this.sel = value;
    }

    /**
     * Gets the value of the seltype property.
     * 
     * @return
     *     possible object is
     *     {@link STSelType }
     *     
     */
    public STSelType getSeltype() {
        if (seltype == null) {
            return STSelType.SINGLE;
        }
        return seltype;
    }

    /**
     * Sets the value of the seltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSelType }
     *     
     */
    public void setSeltype(STSelType value) {
        this.seltype = value;
    }

    /**
     * Gets the value of the textHAlign property.
     * 
     * @return
     *     possible object is
     *     {@link STTextHAlign }
     *     
     */
    public STTextHAlign getTextHAlign() {
        if (textHAlign == null) {
            return STTextHAlign.LEFT;
        }
        return textHAlign;
    }

    /**
     * Sets the value of the textHAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTextHAlign }
     *     
     */
    public void setTextHAlign(STTextHAlign value) {
        this.textHAlign = value;
    }

    /**
     * Gets the value of the textVAlign property.
     * 
     * @return
     *     possible object is
     *     {@link STTextVAlign }
     *     
     */
    public STTextVAlign getTextVAlign() {
        if (textVAlign == null) {
            return STTextVAlign.TOP;
        }
        return textVAlign;
    }

    /**
     * Sets the value of the textVAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTextVAlign }
     *     
     */
    public void setTextVAlign(STTextVAlign value) {
        this.textVAlign = value;
    }

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVal(Long value) {
        this.val = value;
    }

    /**
     * Gets the value of the widthMin property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getWidthMin() {
        return widthMin;
    }

    /**
     * Sets the value of the widthMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setWidthMin(Long value) {
        this.widthMin = value;
    }

    /**
     * Gets the value of the editVal property.
     * 
     * @return
     *     possible object is
     *     {@link STEditValidation }
     *     
     */
    public STEditValidation getEditVal() {
        return editVal;
    }

    /**
     * Sets the value of the editVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link STEditValidation }
     *     
     */
    public void setEditVal(STEditValidation value) {
        this.editVal = value;
    }

    /**
     * Gets the value of the multiLine property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMultiLine() {
        if (multiLine == null) {
            return false;
        }
        return multiLine;
    }

    /**
     * Sets the value of the multiLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiLine(Boolean value) {
        this.multiLine = value;
    }

    /**
     * Gets the value of the verticalBar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isVerticalBar() {
        if (verticalBar == null) {
            return false;
        }
        return verticalBar;
    }

    /**
     * Sets the value of the verticalBar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVerticalBar(Boolean value) {
        this.verticalBar = value;
    }

    /**
     * Gets the value of the passwordEdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPasswordEdit() {
        if (passwordEdit == null) {
            return false;
        }
        return passwordEdit;
    }

    /**
     * Sets the value of the passwordEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPasswordEdit(Boolean value) {
        this.passwordEdit = value;
    }
}
