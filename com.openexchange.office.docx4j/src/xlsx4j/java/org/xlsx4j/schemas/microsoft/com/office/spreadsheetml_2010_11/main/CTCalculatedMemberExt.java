
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CalculatedMemberExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CalculatedMemberExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}calculatedMember"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CalculatedMemberExt", propOrder = {
    "calculatedMember"
})
public class CTCalculatedMemberExt {

    @XmlElement(required = true)
    protected CTCalculatedMember calculatedMember;

    /**
     * Gets the value of the calculatedMember property.
     * 
     * @return
     *     possible object is
     *     {@link CTCalculatedMember }
     *     
     */
    public CTCalculatedMember getCalculatedMember() {
        return calculatedMember;
    }

    /**
     * Sets the value of the calculatedMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTCalculatedMember }
     *     
     */
    public void setCalculatedMember(CTCalculatedMember value) {
        this.calculatedMember = value;
    }
}
