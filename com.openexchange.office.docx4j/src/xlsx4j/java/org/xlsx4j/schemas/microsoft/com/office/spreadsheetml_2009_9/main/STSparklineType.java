
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SparklineType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SparklineType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="line"/>
 *     &lt;enumeration value="column"/>
 *     &lt;enumeration value="stacked"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SparklineType")
@XmlEnum
public enum STSparklineType {

    @XmlEnumValue("line")
    LINE("line"),
    @XmlEnumValue("column")
    COLUMN("column"),
    @XmlEnumValue("stacked")
    STACKED("stacked");
    private final String value;

    STSparklineType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSparklineType fromValue(String v) {
        for (STSparklineType c: STSparklineType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
