
package org.xlsx4j.schemas.microsoft.com.office.excel_2006.main;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.xlsx4j.schemas.microsoft.com.office.excel_2006.main package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _F_QNAME = new QName("http://schemas.microsoft.com/office/excel/2006/main", "f");
    private final static QName _Ref_QNAME = new QName("http://schemas.microsoft.com/office/excel/2006/main", "ref");
    private final static QName _Sqref_QNAME = new QName("http://schemas.microsoft.com/office/excel/2006/main", "sqref");
    private final static QName _Macrosheet_QNAME = new QName("http://schemas.microsoft.com/office/excel/2006/main", "macrosheet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.xlsx4j.schemas.microsoft.com.office.excel_2006.main
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTRef }
     * 
     */
    public CTRef createCTRef() {
        return new CTRef();
    }

    /**
     * Create an instance of {@link CTSqref }
     * 
     */
    public CTSqref createCTSqref() {
        return new CTSqref();
    }

    /**
     * Create an instance of {@link CTWorksheet }
     * 
     */
    public CTWorksheet createCTWorksheet() {
        return new CTWorksheet();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/excel/2006/main", name = "f")
    public JAXBElement<String> createF(String value) {
        return new JAXBElement<String>(_F_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/excel/2006/main", name = "ref")
    public JAXBElement<CTRef> createRef(CTRef value) {
        return new JAXBElement<CTRef>(_Ref_QNAME, CTRef.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSqref }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/excel/2006/main", name = "sqref")
    public JAXBElement<CTSqref> createSqref(CTSqref value) {
        return new JAXBElement<CTSqref>(_Sqref_QNAME, CTSqref.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWorksheet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/excel/2006/main", name = "macrosheet")
    public JAXBElement<CTWorksheet> createMacrosheet(CTWorksheet value) {
        return new JAXBElement<CTWorksheet>(_Macrosheet_QNAME, CTWorksheet.class, null, value);
    }
}
