
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTPivotAreas;
import org.xlsx4j.sml.IConditionalFormat;
import org.xlsx4j.sml.STScope;
import org.xlsx4j.sml.STType;


/**
 * <p>Java class for CT_ConditionalFormat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ConditionalFormat">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotAreas" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_PivotAreas" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="scope" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Scope" default="selection" />
 *       &lt;attribute name="type" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Type" default="none" />
 *       &lt;attribute name="priority" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="id" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Guid" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ConditionalFormat", propOrder = {
    "pivotAreas",
    "extLst"
})
public class CTConditionalFormat implements IConditionalFormat {

    protected CTPivotAreas pivotAreas;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "scope")
    protected STScope scope;
    @XmlAttribute(name = "type")
    protected STType type;
    @XmlAttribute(name = "priority")
    @XmlSchemaType(name = "unsignedInt")
    protected Long priority;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;
    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the pivotAreas property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotAreas }
     *     
     */
    @Override
    public CTPivotAreas getPivotAreas() {
        return pivotAreas;
    }

    /**
     * Sets the value of the pivotAreas property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotAreas }
     *     
     */
    @Override
    public void setPivotAreas(CTPivotAreas value) {
        this.pivotAreas = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    @Override
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    @Override
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the scope property.
     * 
     * @return
     *     possible object is
     *     {@link STScope }
     *     
     */
    @Override
    public STScope getScope() {
        if (scope == null) {
            return STScope.SELECTION;
        }
        return scope;
    }

    /**
     * Sets the value of the scope property.
     * 
     * @param value
     *     allowed object is
     *     {@link STScope }
     *     
     */
    @Override
    public void setScope(STScope value) {
        this.scope = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link STType }
     *     
     */
    @Override
    public STType getType() {
        if (type == null) {
            return STType.NONE;
        }
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link STType }
     *     
     */
    @Override
    public void setType(STType value) {
        this.type = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPriority(Long value) {
        this.priority = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
