
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_DataBarDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_DataBarDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="context"/>
 *     &lt;enumeration value="leftToRight"/>
 *     &lt;enumeration value="rightToLeft"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_DataBarDirection")
@XmlEnum
public enum STDataBarDirection {

    @XmlEnumValue("context")
    CONTEXT("context"),
    @XmlEnumValue("leftToRight")
    LEFT_TO_RIGHT("leftToRight"),
    @XmlEnumValue("rightToLeft")
    RIGHT_TO_LEFT("rightToLeft");
    private final String value;

    STDataBarDirection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STDataBarDirection fromValue(String v) {
        for (STDataBarDirection c: STDataBarDirection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
