
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_QueryTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_QueryTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="clipped" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="sourceDataName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="drillThrough" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_QueryTable")
public class CTQueryTable {

    @XmlAttribute(name = "clipped")
    protected Boolean clipped;
    @XmlAttribute(name = "sourceDataName")
    protected String sourceDataName;
    @XmlAttribute(name = "drillThrough")
    protected Boolean drillThrough;

    /**
     * Gets the value of the clipped property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isClipped() {
        if (clipped == null) {
            return false;
        }
        return clipped;
    }

    /**
     * Sets the value of the clipped property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClipped(Boolean value) {
        this.clipped = value;
    }

    /**
     * Gets the value of the sourceDataName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceDataName() {
        return sourceDataName;
    }

    /**
     * Sets the value of the sourceDataName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceDataName(String value) {
        this.sourceDataName = value;
    }

    /**
     * Gets the value of the drillThrough property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDrillThrough() {
        if (drillThrough == null) {
            return false;
        }
        return drillThrough;
    }

    /**
     * Sets the value of the drillThrough property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDrillThrough(Boolean value) {
        this.drillThrough = value;
    }
}
