
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerCacheHideNoData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerCacheHideNoData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="slicerCacheOlapLevelName" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SlicerCacheOlapLevelName" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="count" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerCacheHideNoData", propOrder = {
    "slicerCacheOlapLevelName"
})
public class CTSlicerCacheHideNoData {

    protected List<CTSlicerCacheOlapLevelName> slicerCacheOlapLevelName;
    @XmlAttribute(name = "count")
    @XmlSchemaType(name = "unsignedInt")
    protected Long count;

    /**
     * Gets the value of the slicerCacheOlapLevelName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the slicerCacheOlapLevelName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSlicerCacheOlapLevelName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSlicerCacheOlapLevelName }
     * 
     * 
     */
    public List<CTSlicerCacheOlapLevelName> getSlicerCacheOlapLevelName() {
        if (slicerCacheOlapLevelName == null) {
            slicerCacheOlapLevelName = new ArrayList<CTSlicerCacheOlapLevelName>();
        }
        return this.slicerCacheOlapLevelName;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public long getCount() {
        if (count == null) {
            return  0L;
        }
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCount(Long value) {
        this.count = value;
    }
}
