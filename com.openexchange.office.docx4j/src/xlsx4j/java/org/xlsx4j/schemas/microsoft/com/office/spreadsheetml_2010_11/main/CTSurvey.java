
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_Survey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Survey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="surveyPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyElementPr" minOccurs="0"/>
 *         &lt;element name="titlePr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyElementPr" minOccurs="0"/>
 *         &lt;element name="descriptionPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyElementPr" minOccurs="0"/>
 *         &lt;element name="questions" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_SurveyQuestions"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="guid" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Guid" />
 *       &lt;attribute name="title" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="description" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Survey", propOrder = {
    "surveyPr",
    "titlePr",
    "descriptionPr",
    "questions",
    "extLst"
})
public class CTSurvey {

    protected CTSurveyElementPr surveyPr;
    protected CTSurveyElementPr titlePr;
    protected CTSurveyElementPr descriptionPr;
    @XmlElement(required = true)
    protected CTSurveyQuestions questions;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "id", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long id;
    @XmlAttribute(name = "guid", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String guid;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "description")
    protected String description;

    /**
     * Gets the value of the surveyPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public CTSurveyElementPr getSurveyPr() {
        return surveyPr;
    }

    /**
     * Sets the value of the surveyPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public void setSurveyPr(CTSurveyElementPr value) {
        this.surveyPr = value;
    }

    /**
     * Gets the value of the titlePr property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public CTSurveyElementPr getTitlePr() {
        return titlePr;
    }

    /**
     * Sets the value of the titlePr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public void setTitlePr(CTSurveyElementPr value) {
        this.titlePr = value;
    }

    /**
     * Gets the value of the descriptionPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public CTSurveyElementPr getDescriptionPr() {
        return descriptionPr;
    }

    /**
     * Sets the value of the descriptionPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyElementPr }
     *     
     */
    public void setDescriptionPr(CTSurveyElementPr value) {
        this.descriptionPr = value;
    }

    /**
     * Gets the value of the questions property.
     * 
     * @return
     *     possible object is
     *     {@link CTSurveyQuestions }
     *     
     */
    public CTSurveyQuestions getQuestions() {
        return questions;
    }

    /**
     * Sets the value of the questions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSurveyQuestions }
     *     
     */
    public void setQuestions(CTSurveyQuestions value) {
        this.questions = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the guid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuid() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuid(String value) {
        this.guid = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }
}
