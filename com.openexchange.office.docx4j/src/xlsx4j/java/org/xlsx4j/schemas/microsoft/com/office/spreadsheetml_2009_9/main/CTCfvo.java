
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.ICfvo;
import org.xlsx4j.util.StringCodec;


/**
 * <p>Java class for CT_Cfvo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Cfvo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" use="required" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_CfvoType" />
 *       &lt;attribute name="gte" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Cfvo", propOrder = {
    "f",
    "extLst"
})
public class CTCfvo implements ICfvo {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected String f;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "type", required = true)
    protected STCfvoType type;
    @XmlAttribute(name = "gte")
    protected Boolean gte;

    /**
     * Gets the value of the f property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Override
    public String getVal() {
        return f;
    }

    /**
     * Sets the value of the f property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Override
    public void setVal(String value) {
        f = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link STCfvoType }
     *
     */
    @Override
    public STCfvoType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link STCfvoType }
     *
     */
    public void setType(STCfvoType value) {
        this.type = value;
    }

    /**
     * Gets the value of the gte property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isGte() {
        if (gte == null) {
            return true;
        }
        return gte;
    }

    /**
     * Sets the value of the gte property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setGte(Boolean value) {
        this.gte = value;
    }

    /**
     * Encodes strings before writing XML.
     */
    @SuppressWarnings("unused")
    public void beforeMarshal(Marshaller marshaller) {
        f = StringCodec.encode(f);
    }

    /**
     * Decodes strings after parsing XML.
     */
    @SuppressWarnings("unused")
    public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        f = StringCodec.decode(f);
    }
}
