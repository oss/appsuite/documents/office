
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTColor;


/**
 * <p>Java class for CT_SparklineGroup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SparklineGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="colorSeries" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorNegative" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorAxis" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorMarkers" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorFirst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorLast" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorHigh" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="colorLow" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f" minOccurs="0"/>
 *         &lt;element name="sparklines" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_Sparklines"/>
 *       &lt;/sequence>
 *       &lt;attribute name="manualMax" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="manualMin" type="{http://www.w3.org/2001/XMLSchema}double" />
 *       &lt;attribute name="lineWeight" type="{http://www.w3.org/2001/XMLSchema}double" default="0.75" />
 *       &lt;attribute name="type" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SparklineType" default="line" />
 *       &lt;attribute name="dateAxis" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="displayEmptyCellsAs" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_DispBlanksAs" default="zero" />
 *       &lt;attribute name="markers" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="high" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="low" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="first" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="last" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="negative" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="displayXAxis" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="displayHidden" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="minAxisType" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SparklineAxisMinMax" default="individual" />
 *       &lt;attribute name="maxAxisType" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SparklineAxisMinMax" default="individual" />
 *       &lt;attribute name="rightToLeft" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SparklineGroup", propOrder = {
    "colorSeries",
    "colorNegative",
    "colorAxis",
    "colorMarkers",
    "colorFirst",
    "colorLast",
    "colorHigh",
    "colorLow",
    "f",
    "sparklines"
})
public class CTSparklineGroup {

    protected CTColor colorSeries;
    protected CTColor colorNegative;
    protected CTColor colorAxis;
    protected CTColor colorMarkers;
    protected CTColor colorFirst;
    protected CTColor colorLast;
    protected CTColor colorHigh;
    protected CTColor colorLow;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected String f;
    @XmlElement(required = true)
    protected CTSparklines sparklines;
    @XmlAttribute(name = "manualMax")
    protected Double manualMax;
    @XmlAttribute(name = "manualMin")
    protected Double manualMin;
    @XmlAttribute(name = "lineWeight")
    protected Double lineWeight;
    @XmlAttribute(name = "type")
    protected STSparklineType type;
    @XmlAttribute(name = "dateAxis")
    protected Boolean dateAxis;
    @XmlAttribute(name = "displayEmptyCellsAs")
    protected STDispBlanksAs displayEmptyCellsAs;
    @XmlAttribute(name = "markers")
    protected Boolean markers;
    @XmlAttribute(name = "high")
    protected Boolean high;
    @XmlAttribute(name = "low")
    protected Boolean low;
    @XmlAttribute(name = "first")
    protected Boolean first;
    @XmlAttribute(name = "last")
    protected Boolean last;
    @XmlAttribute(name = "negative")
    protected Boolean negative;
    @XmlAttribute(name = "displayXAxis")
    protected Boolean displayXAxis;
    @XmlAttribute(name = "displayHidden")
    protected Boolean displayHidden;
    @XmlAttribute(name = "minAxisType")
    protected STSparklineAxisMinMax minAxisType;
    @XmlAttribute(name = "maxAxisType")
    protected STSparklineAxisMinMax maxAxisType;
    @XmlAttribute(name = "rightToLeft")
    protected Boolean rightToLeft;

    /**
     * Gets the value of the colorSeries property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorSeries() {
        return colorSeries;
    }

    /**
     * Sets the value of the colorSeries property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorSeries(CTColor value) {
        this.colorSeries = value;
    }

    /**
     * Gets the value of the colorNegative property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorNegative() {
        return colorNegative;
    }

    /**
     * Sets the value of the colorNegative property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorNegative(CTColor value) {
        this.colorNegative = value;
    }

    /**
     * Gets the value of the colorAxis property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorAxis() {
        return colorAxis;
    }

    /**
     * Sets the value of the colorAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorAxis(CTColor value) {
        this.colorAxis = value;
    }

    /**
     * Gets the value of the colorMarkers property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorMarkers() {
        return colorMarkers;
    }

    /**
     * Sets the value of the colorMarkers property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorMarkers(CTColor value) {
        this.colorMarkers = value;
    }

    /**
     * Gets the value of the colorFirst property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorFirst() {
        return colorFirst;
    }

    /**
     * Sets the value of the colorFirst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorFirst(CTColor value) {
        this.colorFirst = value;
    }

    /**
     * Gets the value of the colorLast property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorLast() {
        return colorLast;
    }

    /**
     * Sets the value of the colorLast property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorLast(CTColor value) {
        this.colorLast = value;
    }

    /**
     * Gets the value of the colorHigh property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorHigh() {
        return colorHigh;
    }

    /**
     * Sets the value of the colorHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorHigh(CTColor value) {
        this.colorHigh = value;
    }

    /**
     * Gets the value of the colorLow property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getColorLow() {
        return colorLow;
    }

    /**
     * Sets the value of the colorLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setColorLow(CTColor value) {
        this.colorLow = value;
    }

    /**
     * Gets the value of the f property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getF() {
        return f;
    }

    /**
     * Sets the value of the f property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setF(String value) {
        this.f = value;
    }

    /**
     * Gets the value of the sparklines property.
     * 
     * @return
     *     possible object is
     *     {@link CTSparklines }
     *     
     */
    public CTSparklines getSparklines() {
        return sparklines;
    }

    /**
     * Sets the value of the sparklines property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSparklines }
     *     
     */
    public void setSparklines(CTSparklines value) {
        this.sparklines = value;
    }

    /**
     * Gets the value of the manualMax property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getManualMax() {
        return manualMax;
    }

    /**
     * Sets the value of the manualMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setManualMax(Double value) {
        this.manualMax = value;
    }

    /**
     * Gets the value of the manualMin property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getManualMin() {
        return manualMin;
    }

    /**
     * Sets the value of the manualMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setManualMin(Double value) {
        this.manualMin = value;
    }

    /**
     * Gets the value of the lineWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public double getLineWeight() {
        if (lineWeight == null) {
            return  0.75D;
        }
        return lineWeight;
    }

    /**
     * Sets the value of the lineWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLineWeight(Double value) {
        this.lineWeight = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link STSparklineType }
     *     
     */
    public STSparklineType getType() {
        if (type == null) {
            return STSparklineType.LINE;
        }
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSparklineType }
     *     
     */
    public void setType(STSparklineType value) {
        this.type = value;
    }

    /**
     * Gets the value of the dateAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDateAxis() {
        if (dateAxis == null) {
            return false;
        }
        return dateAxis;
    }

    /**
     * Sets the value of the dateAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDateAxis(Boolean value) {
        this.dateAxis = value;
    }

    /**
     * Gets the value of the displayEmptyCellsAs property.
     * 
     * @return
     *     possible object is
     *     {@link STDispBlanksAs }
     *     
     */
    public STDispBlanksAs getDisplayEmptyCellsAs() {
        if (displayEmptyCellsAs == null) {
            return STDispBlanksAs.ZERO;
        }
        return displayEmptyCellsAs;
    }

    /**
     * Sets the value of the displayEmptyCellsAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDispBlanksAs }
     *     
     */
    public void setDisplayEmptyCellsAs(STDispBlanksAs value) {
        this.displayEmptyCellsAs = value;
    }

    /**
     * Gets the value of the markers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMarkers() {
        if (markers == null) {
            return false;
        }
        return markers;
    }

    /**
     * Sets the value of the markers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarkers(Boolean value) {
        this.markers = value;
    }

    /**
     * Gets the value of the high property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHigh() {
        if (high == null) {
            return false;
        }
        return high;
    }

    /**
     * Sets the value of the high property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHigh(Boolean value) {
        this.high = value;
    }

    /**
     * Gets the value of the low property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLow() {
        if (low == null) {
            return false;
        }
        return low;
    }

    /**
     * Sets the value of the low property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLow(Boolean value) {
        this.low = value;
    }

    /**
     * Gets the value of the first property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFirst() {
        if (first == null) {
            return false;
        }
        return first;
    }

    /**
     * Sets the value of the first property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirst(Boolean value) {
        this.first = value;
    }

    /**
     * Gets the value of the last property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLast() {
        if (last == null) {
            return false;
        }
        return last;
    }

    /**
     * Sets the value of the last property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLast(Boolean value) {
        this.last = value;
    }

    /**
     * Gets the value of the negative property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNegative() {
        if (negative == null) {
            return false;
        }
        return negative;
    }

    /**
     * Sets the value of the negative property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNegative(Boolean value) {
        this.negative = value;
    }

    /**
     * Gets the value of the displayXAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDisplayXAxis() {
        if (displayXAxis == null) {
            return false;
        }
        return displayXAxis;
    }

    /**
     * Sets the value of the displayXAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayXAxis(Boolean value) {
        this.displayXAxis = value;
    }

    /**
     * Gets the value of the displayHidden property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDisplayHidden() {
        if (displayHidden == null) {
            return false;
        }
        return displayHidden;
    }

    /**
     * Sets the value of the displayHidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayHidden(Boolean value) {
        this.displayHidden = value;
    }

    /**
     * Gets the value of the minAxisType property.
     * 
     * @return
     *     possible object is
     *     {@link STSparklineAxisMinMax }
     *     
     */
    public STSparklineAxisMinMax getMinAxisType() {
        if (minAxisType == null) {
            return STSparklineAxisMinMax.INDIVIDUAL;
        }
        return minAxisType;
    }

    /**
     * Sets the value of the minAxisType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSparklineAxisMinMax }
     *     
     */
    public void setMinAxisType(STSparklineAxisMinMax value) {
        this.minAxisType = value;
    }

    /**
     * Gets the value of the maxAxisType property.
     * 
     * @return
     *     possible object is
     *     {@link STSparklineAxisMinMax }
     *     
     */
    public STSparklineAxisMinMax getMaxAxisType() {
        if (maxAxisType == null) {
            return STSparklineAxisMinMax.INDIVIDUAL;
        }
        return maxAxisType;
    }

    /**
     * Sets the value of the maxAxisType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSparklineAxisMinMax }
     *     
     */
    public void setMaxAxisType(STSparklineAxisMinMax value) {
        this.maxAxisType = value;
    }

    /**
     * Gets the value of the rightToLeft property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRightToLeft() {
        if (rightToLeft == null) {
            return false;
        }
        return rightToLeft;
    }

    /**
     * Sets the value of the rightToLeft property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRightToLeft(Boolean value) {
        this.rightToLeft = value;
    }
}
