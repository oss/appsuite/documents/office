
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_TimelineCacheDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineCacheDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotTables" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineCachePivotTables" minOccurs="0"/>
 *         &lt;element name="state" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineState"/>
 *         &lt;element name="timelinePivotFilter" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelinePivotFilter" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="sourceName" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineCacheDefinition", propOrder = {
    "pivotTables",
    "state",
    "timelinePivotFilter",
    "extLst"
})
public class CTTimelineCacheDefinition {

    protected CTTimelineCachePivotTables pivotTables;
    @XmlElement(required = true)
    protected CTTimelineState state;
    protected CTTimelinePivotFilter timelinePivotFilter;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "sourceName", required = true)
    protected String sourceName;

    /**
     * Gets the value of the pivotTables property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelineCachePivotTables }
     *     
     */
    public CTTimelineCachePivotTables getPivotTables() {
        return pivotTables;
    }

    /**
     * Sets the value of the pivotTables property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelineCachePivotTables }
     *     
     */
    public void setPivotTables(CTTimelineCachePivotTables value) {
        this.pivotTables = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelineState }
     *     
     */
    public CTTimelineState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelineState }
     *     
     */
    public void setState(CTTimelineState value) {
        this.state = value;
    }

    /**
     * Gets the value of the timelinePivotFilter property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelinePivotFilter }
     *     
     */
    public CTTimelinePivotFilter getTimelinePivotFilter() {
        return timelinePivotFilter;
    }

    /**
     * Sets the value of the timelinePivotFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelinePivotFilter }
     *     
     */
    public void setTimelinePivotFilter(CTTimelinePivotFilter value) {
        this.timelinePivotFilter = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the sourceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * Sets the value of the sourceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceName(String value) {
        this.sourceName = value;
    }
}
