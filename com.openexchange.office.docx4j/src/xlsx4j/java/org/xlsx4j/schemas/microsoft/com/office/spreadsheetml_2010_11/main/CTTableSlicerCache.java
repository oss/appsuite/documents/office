
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.STSlicerCacheCrossFilter;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.STTabularSlicerCacheSortOrder;


/**
 * <p>Java class for CT_TableSlicerCache complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TableSlicerCache">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="tableId" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="column" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="sortOrder" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_TabularSlicerCacheSortOrder" default="ascending" />
 *       &lt;attribute name="customListSort" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="crossFilter" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_SlicerCacheCrossFilter" default="showItemsWithDataAtTop" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TableSlicerCache", propOrder = {
    "extLst"
})
public class CTTableSlicerCache {

    protected CTExtensionList extLst;
    @XmlAttribute(name = "tableId", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long tableId;
    @XmlAttribute(name = "column", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long column;
    @XmlAttribute(name = "sortOrder")
    protected STTabularSlicerCacheSortOrder sortOrder;
    @XmlAttribute(name = "customListSort")
    protected Boolean customListSort;
    @XmlAttribute(name = "crossFilter")
    protected STSlicerCacheCrossFilter crossFilter;

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the tableId property.
     * 
     */
    public long getTableId() {
        return tableId;
    }

    /**
     * Sets the value of the tableId property.
     * 
     */
    public void setTableId(long value) {
        this.tableId = value;
    }

    /**
     * Gets the value of the column property.
     * 
     */
    public long getColumn() {
        return column;
    }

    /**
     * Sets the value of the column property.
     * 
     */
    public void setColumn(long value) {
        this.column = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link STTabularSlicerCacheSortOrder }
     *     
     */
    public STTabularSlicerCacheSortOrder getSortOrder() {
        if (sortOrder == null) {
            return STTabularSlicerCacheSortOrder.ASCENDING;
        }
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTabularSlicerCacheSortOrder }
     *     
     */
    public void setSortOrder(STTabularSlicerCacheSortOrder value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the customListSort property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCustomListSort() {
        if (customListSort == null) {
            return true;
        }
        return customListSort;
    }

    /**
     * Sets the value of the customListSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomListSort(Boolean value) {
        this.customListSort = value;
    }

    /**
     * Gets the value of the crossFilter property.
     * 
     * @return
     *     possible object is
     *     {@link STSlicerCacheCrossFilter }
     *     
     */
    public STSlicerCacheCrossFilter getCrossFilter() {
        if (crossFilter == null) {
            return STSlicerCacheCrossFilter.SHOW_ITEMS_WITH_DATA_AT_TOP;
        }
        return crossFilter;
    }

    /**
     * Sets the value of the crossFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link STSlicerCacheCrossFilter }
     *     
     */
    public void setCrossFilter(STSlicerCacheCrossFilter value) {
        this.crossFilter = value;
    }
}
