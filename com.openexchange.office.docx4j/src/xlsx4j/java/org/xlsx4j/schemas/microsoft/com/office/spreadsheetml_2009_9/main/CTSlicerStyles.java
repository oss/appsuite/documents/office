
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerStyles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerStyles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="slicerStyle" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SlicerStyle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="defaultSlicerStyle" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerStyles", propOrder = {
    "slicerStyle"
})
public class CTSlicerStyles {

    protected List<CTSlicerStyle> slicerStyle;
    @XmlAttribute(name = "defaultSlicerStyle", required = true)
    protected String defaultSlicerStyle;

    /**
     * Gets the value of the slicerStyle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the slicerStyle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSlicerStyle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSlicerStyle }
     * 
     * 
     */
    public List<CTSlicerStyle> getSlicerStyle() {
        if (slicerStyle == null) {
            slicerStyle = new ArrayList<CTSlicerStyle>();
        }
        return this.slicerStyle;
    }

    /**
     * Gets the value of the defaultSlicerStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultSlicerStyle() {
        return defaultSlicerStyle;
    }

    /**
     * Sets the value of the defaultSlicerStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultSlicerStyle(String value) {
        this.defaultSlicerStyle = value;
    }
}
