
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.STPivotFilterType;


/**
 * <p>Java class for CT_TimelineState complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineState">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="selection" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineRange" minOccurs="0"/>
 *         &lt;element name="bounds" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineRange"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="singleRangeFilterState" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="minimalRefreshVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="lastRefreshVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="pivotCacheId" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="filterType" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_PivotFilterType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineState", propOrder = {
    "selection",
    "bounds",
    "extLst"
})
public class CTTimelineState {

    protected CTTimelineRange selection;
    @XmlElement(required = true)
    protected CTTimelineRange bounds;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "singleRangeFilterState")
    protected Boolean singleRangeFilterState;
    @XmlAttribute(name = "minimalRefreshVersion", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long minimalRefreshVersion;
    @XmlAttribute(name = "lastRefreshVersion", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long lastRefreshVersion;
    @XmlAttribute(name = "pivotCacheId", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long pivotCacheId;
    @XmlAttribute(name = "filterType", required = true)
    protected STPivotFilterType filterType;

    /**
     * Gets the value of the selection property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelineRange }
     *     
     */
    public CTTimelineRange getSelection() {
        return selection;
    }

    /**
     * Sets the value of the selection property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelineRange }
     *     
     */
    public void setSelection(CTTimelineRange value) {
        this.selection = value;
    }

    /**
     * Gets the value of the bounds property.
     * 
     * @return
     *     possible object is
     *     {@link CTTimelineRange }
     *     
     */
    public CTTimelineRange getBounds() {
        return bounds;
    }

    /**
     * Sets the value of the bounds property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTimelineRange }
     *     
     */
    public void setBounds(CTTimelineRange value) {
        this.bounds = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the singleRangeFilterState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSingleRangeFilterState() {
        if (singleRangeFilterState == null) {
            return true;
        }
        return singleRangeFilterState;
    }

    /**
     * Sets the value of the singleRangeFilterState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSingleRangeFilterState(Boolean value) {
        this.singleRangeFilterState = value;
    }

    /**
     * Gets the value of the minimalRefreshVersion property.
     * 
     */
    public long getMinimalRefreshVersion() {
        return minimalRefreshVersion;
    }

    /**
     * Sets the value of the minimalRefreshVersion property.
     * 
     */
    public void setMinimalRefreshVersion(long value) {
        this.minimalRefreshVersion = value;
    }

    /**
     * Gets the value of the lastRefreshVersion property.
     * 
     */
    public long getLastRefreshVersion() {
        return lastRefreshVersion;
    }

    /**
     * Sets the value of the lastRefreshVersion property.
     * 
     */
    public void setLastRefreshVersion(long value) {
        this.lastRefreshVersion = value;
    }

    /**
     * Gets the value of the pivotCacheId property.
     * 
     */
    public long getPivotCacheId() {
        return pivotCacheId;
    }

    /**
     * Sets the value of the pivotCacheId property.
     * 
     */
    public void setPivotCacheId(long value) {
        this.pivotCacheId = value;
    }

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link STPivotFilterType }
     *     
     */
    public STPivotFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STPivotFilterType }
     *     
     */
    public void setFilterType(STPivotFilterType value) {
        this.filterType = value;
    }
}
