
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

import org.xlsx4j.sml.IIconSet;


/**
 * <p>Java class for CT_IconSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_IconSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cfvo" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_Cfvo" maxOccurs="unbounded" minOccurs="2"/>
 *         &lt;element name="cfIcon" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_CfIcon" maxOccurs="5" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="iconSet" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_IconSetType" default="3TrafficLights1" />
 *       &lt;attribute name="showValue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="percent" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="reverse" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="custom" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_IconSet", propOrder = {
    "cfvo",
    "cfIcon"
})
public class CTIconSet implements IIconSet {

    @XmlElement(required = true)
    protected List<CTCfvo> cfvo;
    protected List<CTCfIcon> cfIcon;
    @XmlAttribute(name = "iconSet")
    protected String iconSet;
    @XmlAttribute(name = "showValue")
    protected Boolean showValue;
    @XmlAttribute(name = "percent")
    protected Boolean percent;
    @XmlAttribute(name = "reverse")
    protected Boolean reverse;
    @XmlAttribute(name = "custom")
    protected Boolean custom;

    /**
     * Gets the value of the cfvo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfvo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCfvo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTCfvo }
     * 
     * 
     */
    @Override
    public List<CTCfvo> getCfvo() {
        if (cfvo == null) {
            cfvo = new ArrayList<CTCfvo>();
        }
        return this.cfvo;
    }

    /**
     * Gets the value of the cfIcon property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfIcon property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCfIcon().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTCfIcon }
     * 
     * 
     */
    public List<CTCfIcon> getCfIcon() {
        if (cfIcon == null) {
            cfIcon = new ArrayList<CTCfIcon>();
        }
        return this.cfIcon;
    }

    /**
     * Gets the value of the iconSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Override
    public String getIconSet() {
        if (iconSet == null) {
            return "3TrafficLights1";
        }
        return iconSet;
    }

    /**
     * Sets the value of the iconSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Override
    public void setIconSet(String value) {
        this.iconSet = value;
    }

    /**
     * Gets the value of the showValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isShowValue() {
        if (showValue == null) {
            return true;
        }
        return showValue;
    }

    /**
     * Sets the value of the showValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setShowValue(Boolean value) {
        this.showValue = value;
    }

    /**
     * Gets the value of the percent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isPercent() {
        if (percent == null) {
            return true;
        }
        return percent;
    }

    /**
     * Sets the value of the percent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setPercent(Boolean value) {
        this.percent = value;
    }

    /**
     * Gets the value of the reverse property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isReverse() {
        if (reverse == null) {
            return false;
        }
        return reverse;
    }

    /**
     * Sets the value of the reverse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setReverse(Boolean value) {
        this.reverse = value;
    }

    /**
     * Gets the value of the custom property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCustom() {
        if (custom == null) {
            return false;
        }
        return custom;
    }

    /**
     * Sets the value of the custom property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustom(Boolean value) {
        this.custom = value;
    }
}
