
package org.xlsx4j.schemas.microsoft.com.office.excel_2006.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for CT_Sqref complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Sqref">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://schemas.microsoft.com/office/excel/2006/main>ST_Sqref">
 *       &lt;attribute name="edited" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="split" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="adjusted" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="adjust" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Sqref", propOrder = {
    "value"
})
public class CTSqref {

    @XmlValue
    protected List<String> value;
    @XmlAttribute(name = "edited")
    protected Boolean edited;
    @XmlAttribute(name = "split")
    protected Boolean split;
    @XmlAttribute(name = "adjusted")
    protected Boolean adjusted;
    @XmlAttribute(name = "adjust")
    protected Boolean adjust;

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSqrefList() {
        if (value == null) {
            value = new ArrayList<String>();
        }
        return this.value;
    }

    public void setSqrefList(List<String> sqref) {
    	value = sqref;
    }

    /**
     * Gets the value of the edited property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEdited() {
        return edited;
    }

    /**
     * Sets the value of the edited property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEdited(Boolean value) {
        this.edited = value;
    }

    /**
     * Gets the value of the split property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSplit() {
        return split;
    }

    /**
     * Sets the value of the split property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSplit(Boolean value) {
        this.split = value;
    }

    /**
     * Gets the value of the adjusted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdjusted() {
        return adjusted;
    }

    /**
     * Sets the value of the adjusted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdjusted(Boolean value) {
        this.adjusted = value;
    }

    /**
     * Gets the value of the adjust property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdjust() {
        return adjust;
    }

    /**
     * Sets the value of the adjust property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdjust(Boolean value) {
        this.adjust = value;
    }
}
