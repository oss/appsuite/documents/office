
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_ConditionalFormattings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ConditionalFormattings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="conditionalFormatting" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_ConditionalFormatting" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ConditionalFormattings", propOrder = {
    "conditionalFormatting"
})
public class CTConditionalFormattings {

    @XmlElement(required = true)
    protected List<CTConditionalFormatting> conditionalFormatting;

    /**
     * Gets the value of the conditionalFormatting property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionalFormatting property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionalFormatting().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTConditionalFormatting }
     * 
     * 
     */
    public List<CTConditionalFormatting> getConditionalFormatting() {
        if (conditionalFormatting == null) {
            conditionalFormatting = new ArrayList<CTConditionalFormatting>();
        }
        return this.conditionalFormatting;
    }
}
