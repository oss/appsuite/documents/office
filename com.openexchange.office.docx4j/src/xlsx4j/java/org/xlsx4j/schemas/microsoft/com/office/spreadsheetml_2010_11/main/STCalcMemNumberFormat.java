
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_CalcMemNumberFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_CalcMemNumberFormat">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="default"/>
 *     &lt;enumeration value="number"/>
 *     &lt;enumeration value="percent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_CalcMemNumberFormat")
@XmlEnum
public enum STCalcMemNumberFormat {

    @XmlEnumValue("default")
    DEFAULT("default"),
    @XmlEnumValue("number")
    NUMBER("number"),
    @XmlEnumValue("percent")
    PERCENT("percent");
    private final String value;

    STCalcMemNumberFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STCalcMemNumberFormat fromValue(String v) {
        for (STCalcMemNumberFormat c: STCalcMemNumberFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
