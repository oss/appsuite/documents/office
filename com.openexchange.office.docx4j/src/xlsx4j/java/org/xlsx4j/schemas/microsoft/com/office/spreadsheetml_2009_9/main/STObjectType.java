
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_ObjectType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_ObjectType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Button"/>
 *     &lt;enumeration value="CheckBox"/>
 *     &lt;enumeration value="Drop"/>
 *     &lt;enumeration value="GBox"/>
 *     &lt;enumeration value="Label"/>
 *     &lt;enumeration value="List"/>
 *     &lt;enumeration value="Radio"/>
 *     &lt;enumeration value="Scroll"/>
 *     &lt;enumeration value="Spin"/>
 *     &lt;enumeration value="EditBox"/>
 *     &lt;enumeration value="Dialog"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_ObjectType")
@XmlEnum
public enum STObjectType {

    @XmlEnumValue("Button")
    BUTTON("Button"),
    @XmlEnumValue("CheckBox")
    CHECK_BOX("CheckBox"),
    @XmlEnumValue("Drop")
    DROP("Drop"),
    @XmlEnumValue("GBox")
    G_BOX("GBox"),
    @XmlEnumValue("Label")
    LABEL("Label"),
    @XmlEnumValue("List")
    LIST("List"),
    @XmlEnumValue("Radio")
    RADIO("Radio"),
    @XmlEnumValue("Scroll")
    SCROLL("Scroll"),
    @XmlEnumValue("Spin")
    SPIN("Spin"),
    @XmlEnumValue("EditBox")
    EDIT_BOX("EditBox"),
    @XmlEnumValue("Dialog")
    DIALOG("Dialog");
    private final String value;

    STObjectType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STObjectType fromValue(String v) {
        for (STObjectType c: STObjectType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
