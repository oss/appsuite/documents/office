
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CacheHierarchy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CacheHierarchy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setLevels" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_SetLevels" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="flattenHierarchies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="measuresSet" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="hierarchizeDistinct" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="ignore" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CacheHierarchy", propOrder = {
    "setLevels"
})
public class CTCacheHierarchy {

    protected CTSetLevels setLevels;
    @XmlAttribute(name = "flattenHierarchies")
    protected Boolean flattenHierarchies;
    @XmlAttribute(name = "measuresSet")
    protected Boolean measuresSet;
    @XmlAttribute(name = "hierarchizeDistinct")
    protected Boolean hierarchizeDistinct;
    @XmlAttribute(name = "ignore")
    protected Boolean ignore;

    /**
     * Gets the value of the setLevels property.
     * 
     * @return
     *     possible object is
     *     {@link CTSetLevels }
     *     
     */
    public CTSetLevels getSetLevels() {
        return setLevels;
    }

    /**
     * Sets the value of the setLevels property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSetLevels }
     *     
     */
    public void setSetLevels(CTSetLevels value) {
        this.setLevels = value;
    }

    /**
     * Gets the value of the flattenHierarchies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFlattenHierarchies() {
        if (flattenHierarchies == null) {
            return true;
        }
        return flattenHierarchies;
    }

    /**
     * Sets the value of the flattenHierarchies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlattenHierarchies(Boolean value) {
        this.flattenHierarchies = value;
    }

    /**
     * Gets the value of the measuresSet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMeasuresSet() {
        if (measuresSet == null) {
            return false;
        }
        return measuresSet;
    }

    /**
     * Sets the value of the measuresSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeasuresSet(Boolean value) {
        this.measuresSet = value;
    }

    /**
     * Gets the value of the hierarchizeDistinct property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isHierarchizeDistinct() {
        if (hierarchizeDistinct == null) {
            return true;
        }
        return hierarchizeDistinct;
    }

    /**
     * Sets the value of the hierarchizeDistinct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHierarchizeDistinct(Boolean value) {
        this.hierarchizeDistinct = value;
    }

    /**
     * Gets the value of the ignore property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIgnore() {
        if (ignore == null) {
            return false;
        }
        return ignore;
    }

    /**
     * Sets the value of the ignore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnore(Boolean value) {
        this.ignore = value;
    }
}
