
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.xlsx4j.sml.CTDxfs;
import org.xlsx4j.sml.CTPivotCaches;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTSlicerCachePivotTables;
import org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main.CTSlicerCaches;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.xlsx4j.spreadsheetml.main201011 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TableSlicerCache_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "tableSlicerCache");
    private final static QName _Survey_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "survey");
    private final static QName _TimelineRefs_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelineRefs");
    private final static QName _CacheHierarchy_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "cacheHierarchy");
    private final static QName _DataField_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "dataField");
    private final static QName _Connection_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "connection");
    private final static QName _WorkbookPr_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "workbookPr");
    private final static QName _TimelineCachePivotCaches_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelineCachePivotCaches");
    private final static QName _PivotFilter_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotFilter");
    private final static QName _PivotCacheIdVersion_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotCacheIdVersion");
    private final static QName _TimelinePivotCacheDefinition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelinePivotCacheDefinition");
    private final static QName _Timelines_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelines");
    private final static QName _CalculatedMember_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "calculatedMember");
    private final static QName _TimelineStyles_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelineStyles");
    private final static QName _SlicerCaches_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "slicerCaches");
    private final static QName _QueryTable_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "queryTable");
    private final static QName _PivotCacheDecoupled_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotCacheDecoupled");
    private final static QName _WebExtensions_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "webExtensions");
    private final static QName _PivotTableUISettings_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotTableUISettings");
    private final static QName _DataModel_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "dataModel");
    private final static QName _PivotTableReferences_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotTableReferences");
    private final static QName _SlicerCacheHideItemsWithNoData_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "slicerCacheHideItemsWithNoData");
    private final static QName _PivotCaches_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotCaches");
    private final static QName _TimelineCacheRefs_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelineCacheRefs");
    private final static QName _SlicerCachePivotTables_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "slicerCachePivotTables");
    private final static QName _Dxfs_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "dxfs");
    private final static QName _TimelineCacheDefinition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "timelineCacheDefinition");
    private final static QName _CachedUniqueNames_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "cachedUniqueNames");
    private final static QName _PivotTableData_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", "pivotTableData");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.xlsx4j.spreadsheetml.main201011
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTSlicerCacheHideNoData }
     * 
     */
    public CTSlicerCacheHideNoData createCTSlicerCacheHideNoData() {
        return new CTSlicerCacheHideNoData();
    }

    /**
     * Create an instance of {@link CTTimelineRefs }
     * 
     */
    public CTTimelineRefs createCTTimelineRefs() {
        return new CTTimelineRefs();
    }

    /**
     * Create an instance of {@link CTPivotTableReferences }
     * 
     */
    public CTPivotTableReferences createCTPivotTableReferences() {
        return new CTPivotTableReferences();
    }

    /**
     * Create an instance of {@link CTDataModel }
     * 
     */
    public CTDataModel createCTDataModel() {
        return new CTDataModel();
    }

    /**
     * Create an instance of {@link CTPivotFilter }
     * 
     */
    public CTPivotFilter createCTPivotFilter() {
        return new CTPivotFilter();
    }

    /**
     * Create an instance of {@link CTPivotCacheIdVersion }
     * 
     */
    public CTPivotCacheIdVersion createCTPivotCacheIdVersion() {
        return new CTPivotCacheIdVersion();
    }

    /**
     * Create an instance of {@link CTCacheHierarchy }
     * 
     */
    public CTCacheHierarchy createCTCacheHierarchy() {
        return new CTCacheHierarchy();
    }

    /**
     * Create an instance of {@link CTTableSlicerCache }
     * 
     */
    public CTTableSlicerCache createCTTableSlicerCache() {
        return new CTTableSlicerCache();
    }

    /**
     * Create an instance of {@link CTSurvey }
     * 
     */
    public CTSurvey createCTSurvey() {
        return new CTSurvey();
    }

    /**
     * Create an instance of {@link CTPivotTableUISettings }
     * 
     */
    public CTPivotTableUISettings createCTPivotTableUISettings() {
        return new CTPivotTableUISettings();
    }

    /**
     * Create an instance of {@link CTWorkbookPr }
     * 
     */
    public CTWorkbookPr createCTWorkbookPr() {
        return new CTWorkbookPr();
    }

    /**
     * Create an instance of {@link CTTimelineCacheRefs }
     * 
     */
    public CTTimelineCacheRefs createCTTimelineCacheRefs() {
        return new CTTimelineCacheRefs();
    }

    /**
     * Create an instance of {@link CTConnection }
     * 
     */
    public CTConnection createCTConnection() {
        return new CTConnection();
    }

    /**
     * Create an instance of {@link CTQueryTable }
     * 
     */
    public CTQueryTable createCTQueryTable() {
        return new CTQueryTable();
    }

    /**
     * Create an instance of {@link CTCachedUniqueNames }
     * 
     */
    public CTCachedUniqueNames createCTCachedUniqueNames() {
        return new CTCachedUniqueNames();
    }

    /**
     * Create an instance of {@link CTWebExtensions }
     * 
     */
    public CTWebExtensions createCTWebExtensions() {
        return new CTWebExtensions();
    }

    /**
     * Create an instance of {@link CTPivotCacheDecoupled }
     * 
     */
    public CTPivotCacheDecoupled createCTPivotCacheDecoupled() {
        return new CTPivotCacheDecoupled();
    }

    /**
     * Create an instance of {@link CTPivotTableData }
     * 
     */
    public CTPivotTableData createCTPivotTableData() {
        return new CTPivotTableData();
    }

    /**
     * Create an instance of {@link CTTimelines }
     * 
     */
    public CTTimelines createCTTimelines() {
        return new CTTimelines();
    }

    /**
     * Create an instance of {@link CTTimelinePivotCacheDefinition }
     * 
     */
    public CTTimelinePivotCacheDefinition createCTTimelinePivotCacheDefinition() {
        return new CTTimelinePivotCacheDefinition();
    }

    /**
     * Create an instance of {@link CTTimelineStyles }
     * 
     */
    public CTTimelineStyles createCTTimelineStyles() {
        return new CTTimelineStyles();
    }

    /**
     * Create an instance of {@link CTDataField }
     * 
     */
    public CTDataField createCTDataField() {
        return new CTDataField();
    }

    /**
     * Create an instance of {@link CTCalculatedMember }
     * 
     */
    public CTCalculatedMember createCTCalculatedMember() {
        return new CTCalculatedMember();
    }

    /**
     * Create an instance of {@link CTTimelineCacheDefinition }
     * 
     */
    public CTTimelineCacheDefinition createCTTimelineCacheDefinition() {
        return new CTTimelineCacheDefinition();
    }

    /**
     * Create an instance of {@link CTTimelineCachePivotTables }
     * 
     */
    public CTTimelineCachePivotTables createCTTimelineCachePivotTables() {
        return new CTTimelineCachePivotTables();
    }

    /**
     * Create an instance of {@link CTTimelineCachePivotTable }
     * 
     */
    public CTTimelineCachePivotTable createCTTimelineCachePivotTable() {
        return new CTTimelineCachePivotTable();
    }

    /**
     * Create an instance of {@link CTSlicerCacheOlapLevelName }
     * 
     */
    public CTSlicerCacheOlapLevelName createCTSlicerCacheOlapLevelName() {
        return new CTSlicerCacheOlapLevelName();
    }

    /**
     * Create an instance of {@link CTTimelineStyleElements }
     * 
     */
    public CTTimelineStyleElements createCTTimelineStyleElements() {
        return new CTTimelineStyleElements();
    }

    /**
     * Create an instance of {@link CTModelRelationship }
     * 
     */
    public CTModelRelationship createCTModelRelationship() {
        return new CTModelRelationship();
    }

    /**
     * Create an instance of {@link CTSurveyElementPr }
     * 
     */
    public CTSurveyElementPr createCTSurveyElementPr() {
        return new CTSurveyElementPr();
    }

    /**
     * Create an instance of {@link CTPivotTableServerFormats }
     * 
     */
    public CTPivotTableServerFormats createCTPivotTableServerFormats() {
        return new CTPivotTableServerFormats();
    }

    /**
     * Create an instance of {@link CTDbCommand }
     * 
     */
    public CTDbCommand createCTDbCommand() {
        return new CTDbCommand();
    }

    /**
     * Create an instance of {@link CTTimelineRange }
     * 
     */
    public CTTimelineRange createCTTimelineRange() {
        return new CTTimelineRange();
    }

    /**
     * Create an instance of {@link CTTimelineCacheRef }
     * 
     */
    public CTTimelineCacheRef createCTTimelineCacheRef() {
        return new CTTimelineCacheRef();
    }

    /**
     * Create an instance of {@link CTModelTables }
     * 
     */
    public CTModelTables createCTModelTables() {
        return new CTModelTables();
    }

    /**
     * Create an instance of {@link CTCalculatedMemberExt }
     * 
     */
    public CTCalculatedMemberExt createCTCalculatedMemberExt() {
        return new CTCalculatedMemberExt();
    }

    /**
     * Create an instance of {@link CTOledbPr }
     * 
     */
    public CTOledbPr createCTOledbPr() {
        return new CTOledbPr();
    }

    /**
     * Create an instance of {@link CTCachedUniqueName }
     * 
     */
    public CTCachedUniqueName createCTCachedUniqueName() {
        return new CTCachedUniqueName();
    }

    /**
     * Create an instance of {@link CTTimeline }
     * 
     */
    public CTTimeline createCTTimeline() {
        return new CTTimeline();
    }

    /**
     * Create an instance of {@link CTTimelineRef }
     * 
     */
    public CTTimelineRef createCTTimelineRef() {
        return new CTTimelineRef();
    }

    /**
     * Create an instance of {@link CTSurveyQuestion }
     * 
     */
    public CTSurveyQuestion createCTSurveyQuestion() {
        return new CTSurveyQuestion();
    }

    /**
     * Create an instance of {@link CTTimelinePivotFilter }
     * 
     */
    public CTTimelinePivotFilter createCTTimelinePivotFilter() {
        return new CTTimelinePivotFilter();
    }

    /**
     * Create an instance of {@link CTModelTextPr }
     * 
     */
    public CTModelTextPr createCTModelTextPr() {
        return new CTModelTextPr();
    }

    /**
     * Create an instance of {@link CTDbTable }
     * 
     */
    public CTDbTable createCTDbTable() {
        return new CTDbTable();
    }

    /**
     * Create an instance of {@link CTPivotRow }
     * 
     */
    public CTPivotRow createCTPivotRow() {
        return new CTPivotRow();
    }

    /**
     * Create an instance of {@link CTTimelineState }
     * 
     */
    public CTTimelineState createCTTimelineState() {
        return new CTTimelineState();
    }

    /**
     * Create an instance of {@link CTFieldListActiveTabTopLevelEntity }
     * 
     */
    public CTFieldListActiveTabTopLevelEntity createCTFieldListActiveTabTopLevelEntity() {
        return new CTFieldListActiveTabTopLevelEntity();
    }

    /**
     * Create an instance of {@link CTTimelineStyleElement }
     * 
     */
    public CTTimelineStyleElement createCTTimelineStyleElement() {
        return new CTTimelineStyleElement();
    }

    /**
     * Create an instance of {@link CTPivotTableReference }
     * 
     */
    public CTPivotTableReference createCTPivotTableReference() {
        return new CTPivotTableReference();
    }

    /**
     * Create an instance of {@link CTPivotValueCellExtra }
     * 
     */
    public CTPivotValueCellExtra createCTPivotValueCellExtra() {
        return new CTPivotValueCellExtra();
    }

    /**
     * Create an instance of {@link CTRangePr }
     * 
     */
    public CTRangePr createCTRangePr() {
        return new CTRangePr();
    }

    /**
     * Create an instance of {@link CTModelTable }
     * 
     */
    public CTModelTable createCTModelTable() {
        return new CTModelTable();
    }

    /**
     * Create an instance of {@link CTSurveyQuestions }
     * 
     */
    public CTSurveyQuestions createCTSurveyQuestions() {
        return new CTSurveyQuestions();
    }

    /**
     * Create an instance of {@link CTTimelineStyle }
     * 
     */
    public CTTimelineStyle createCTTimelineStyle() {
        return new CTTimelineStyle();
    }

    /**
     * Create an instance of {@link CTModelRelationships }
     * 
     */
    public CTModelRelationships createCTModelRelationships() {
        return new CTModelRelationships();
    }

    /**
     * Create an instance of {@link CTWebExtension }
     * 
     */
    public CTWebExtension createCTWebExtension() {
        return new CTWebExtension();
    }

    /**
     * Create an instance of {@link CTDataFeedPr }
     * 
     */
    public CTDataFeedPr createCTDataFeedPr() {
        return new CTDataFeedPr();
    }

    /**
     * Create an instance of {@link CTPivotValueCell }
     * 
     */
    public CTPivotValueCell createCTPivotValueCell() {
        return new CTPivotValueCell();
    }

    /**
     * Create an instance of {@link CTDbTables }
     * 
     */
    public CTDbTables createCTDbTables() {
        return new CTDbTables();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTableSlicerCache }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "tableSlicerCache")
    public JAXBElement<CTTableSlicerCache> createTableSlicerCache(CTTableSlicerCache value) {
        return new JAXBElement<CTTableSlicerCache>(_TableSlicerCache_QNAME, CTTableSlicerCache.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSurvey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "survey")
    public JAXBElement<CTSurvey> createSurvey(CTSurvey value) {
        return new JAXBElement<CTSurvey>(_Survey_QNAME, CTSurvey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelineRefs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelineRefs")
    public JAXBElement<CTTimelineRefs> createTimelineRefs(CTTimelineRefs value) {
        return new JAXBElement<CTTimelineRefs>(_TimelineRefs_QNAME, CTTimelineRefs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCacheHierarchy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "cacheHierarchy")
    public JAXBElement<CTCacheHierarchy> createCacheHierarchy(CTCacheHierarchy value) {
        return new JAXBElement<CTCacheHierarchy>(_CacheHierarchy_QNAME, CTCacheHierarchy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDataField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "dataField")
    public JAXBElement<CTDataField> createDataField(CTDataField value) {
        return new JAXBElement<CTDataField>(_DataField_QNAME, CTDataField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTConnection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "connection")
    public JAXBElement<CTConnection> createConnection(CTConnection value) {
        return new JAXBElement<CTConnection>(_Connection_QNAME, CTConnection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWorkbookPr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "workbookPr")
    public JAXBElement<CTWorkbookPr> createWorkbookPr(CTWorkbookPr value) {
        return new JAXBElement<CTWorkbookPr>(_WorkbookPr_QNAME, CTWorkbookPr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCaches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelineCachePivotCaches")
    public JAXBElement<CTPivotCaches> createTimelineCachePivotCaches(CTPivotCaches value) {
        return new JAXBElement<CTPivotCaches>(_TimelineCachePivotCaches_QNAME, CTPivotCaches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotFilter")
    public JAXBElement<CTPivotFilter> createPivotFilter(CTPivotFilter value) {
        return new JAXBElement<CTPivotFilter>(_PivotFilter_QNAME, CTPivotFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCacheIdVersion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotCacheIdVersion")
    public JAXBElement<CTPivotCacheIdVersion> createPivotCacheIdVersion(CTPivotCacheIdVersion value) {
        return new JAXBElement<CTPivotCacheIdVersion>(_PivotCacheIdVersion_QNAME, CTPivotCacheIdVersion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelinePivotCacheDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelinePivotCacheDefinition")
    public JAXBElement<CTTimelinePivotCacheDefinition> createTimelinePivotCacheDefinition(CTTimelinePivotCacheDefinition value) {
        return new JAXBElement<CTTimelinePivotCacheDefinition>(_TimelinePivotCacheDefinition_QNAME, CTTimelinePivotCacheDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelines")
    public JAXBElement<CTTimelines> createTimelines(CTTimelines value) {
        return new JAXBElement<CTTimelines>(_Timelines_QNAME, CTTimelines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCalculatedMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "calculatedMember")
    public JAXBElement<CTCalculatedMember> createCalculatedMember(CTCalculatedMember value) {
        return new JAXBElement<CTCalculatedMember>(_CalculatedMember_QNAME, CTCalculatedMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelineStyles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelineStyles")
    public JAXBElement<CTTimelineStyles> createTimelineStyles(CTTimelineStyles value) {
        return new JAXBElement<CTTimelineStyles>(_TimelineStyles_QNAME, CTTimelineStyles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerCaches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "slicerCaches")
    public JAXBElement<CTSlicerCaches> createSlicerCaches(CTSlicerCaches value) {
        return new JAXBElement<CTSlicerCaches>(_SlicerCaches_QNAME, CTSlicerCaches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTQueryTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "queryTable")
    public JAXBElement<CTQueryTable> createQueryTable(CTQueryTable value) {
        return new JAXBElement<CTQueryTable>(_QueryTable_QNAME, CTQueryTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCacheDecoupled }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotCacheDecoupled")
    public JAXBElement<CTPivotCacheDecoupled> createPivotCacheDecoupled(CTPivotCacheDecoupled value) {
        return new JAXBElement<CTPivotCacheDecoupled>(_PivotCacheDecoupled_QNAME, CTPivotCacheDecoupled.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWebExtensions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "webExtensions")
    public JAXBElement<CTWebExtensions> createWebExtensions(CTWebExtensions value) {
        return new JAXBElement<CTWebExtensions>(_WebExtensions_QNAME, CTWebExtensions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotTableUISettings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotTableUISettings")
    public JAXBElement<CTPivotTableUISettings> createPivotTableUISettings(CTPivotTableUISettings value) {
        return new JAXBElement<CTPivotTableUISettings>(_PivotTableUISettings_QNAME, CTPivotTableUISettings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDataModel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "dataModel")
    public JAXBElement<CTDataModel> createDataModel(CTDataModel value) {
        return new JAXBElement<CTDataModel>(_DataModel_QNAME, CTDataModel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotTableReferences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotTableReferences")
    public JAXBElement<CTPivotTableReferences> createPivotTableReferences(CTPivotTableReferences value) {
        return new JAXBElement<CTPivotTableReferences>(_PivotTableReferences_QNAME, CTPivotTableReferences.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerCacheHideNoData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "slicerCacheHideItemsWithNoData")
    public JAXBElement<CTSlicerCacheHideNoData> createSlicerCacheHideItemsWithNoData(CTSlicerCacheHideNoData value) {
        return new JAXBElement<CTSlicerCacheHideNoData>(_SlicerCacheHideItemsWithNoData_QNAME, CTSlicerCacheHideNoData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCaches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotCaches")
    public JAXBElement<CTPivotCaches> createPivotCaches(CTPivotCaches value) {
        return new JAXBElement<CTPivotCaches>(_PivotCaches_QNAME, CTPivotCaches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelineCacheRefs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelineCacheRefs")
    public JAXBElement<CTTimelineCacheRefs> createTimelineCacheRefs(CTTimelineCacheRefs value) {
        return new JAXBElement<CTTimelineCacheRefs>(_TimelineCacheRefs_QNAME, CTTimelineCacheRefs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerCachePivotTables }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "slicerCachePivotTables")
    public JAXBElement<CTSlicerCachePivotTables> createSlicerCachePivotTables(CTSlicerCachePivotTables value) {
        return new JAXBElement<CTSlicerCachePivotTables>(_SlicerCachePivotTables_QNAME, CTSlicerCachePivotTables.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDxfs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "dxfs")
    public JAXBElement<CTDxfs> createDxfs(CTDxfs value) {
        return new JAXBElement<CTDxfs>(_Dxfs_QNAME, CTDxfs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTimelineCacheDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "timelineCacheDefinition")
    public JAXBElement<CTTimelineCacheDefinition> createTimelineCacheDefinition(CTTimelineCacheDefinition value) {
        return new JAXBElement<CTTimelineCacheDefinition>(_TimelineCacheDefinition_QNAME, CTTimelineCacheDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCachedUniqueNames }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "cachedUniqueNames")
    public JAXBElement<CTCachedUniqueNames> createCachedUniqueNames(CTCachedUniqueNames value) {
        return new JAXBElement<CTCachedUniqueNames>(_CachedUniqueNames_QNAME, CTCachedUniqueNames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotTableData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main", name = "pivotTableData")
    public JAXBElement<CTPivotTableData> createPivotTableData(CTPivotTableData value) {
        return new JAXBElement<CTPivotTableData>(_PivotTableData_QNAME, CTPivotTableData.class, null, value);
    }
}
