
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotTableData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotTableData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotRow" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_PivotRow" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="rowCount" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="columnCount" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="cacheId" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotTableData", propOrder = {
    "pivotRow"
})
public class CTPivotTableData {

    @XmlElement(required = true)
    protected List<CTPivotRow> pivotRow;
    @XmlAttribute(name = "rowCount", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long rowCount;
    @XmlAttribute(name = "columnCount", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long columnCount;
    @XmlAttribute(name = "cacheId", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long cacheId;

    /**
     * Gets the value of the pivotRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pivotRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPivotRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTPivotRow }
     * 
     * 
     */
    public List<CTPivotRow> getPivotRow() {
        if (pivotRow == null) {
            pivotRow = new ArrayList<CTPivotRow>();
        }
        return this.pivotRow;
    }

    /**
     * Gets the value of the rowCount property.
     * 
     */
    public long getRowCount() {
        return rowCount;
    }

    /**
     * Sets the value of the rowCount property.
     * 
     */
    public void setRowCount(long value) {
        this.rowCount = value;
    }

    /**
     * Gets the value of the columnCount property.
     * 
     */
    public long getColumnCount() {
        return columnCount;
    }

    /**
     * Sets the value of the columnCount property.
     * 
     */
    public void setColumnCount(long value) {
        this.columnCount = value;
    }

    /**
     * Gets the value of the cacheId property.
     * 
     */
    public long getCacheId() {
        return cacheId;
    }

    /**
     * Sets the value of the cacheId property.
     * 
     */
    public void setCacheId(long value) {
        this.cacheId = value;
    }
}
