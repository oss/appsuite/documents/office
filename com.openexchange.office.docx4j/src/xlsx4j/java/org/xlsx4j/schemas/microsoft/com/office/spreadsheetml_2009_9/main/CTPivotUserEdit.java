
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PivotUserEdit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotUserEdit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f"/>
 *         &lt;element name="editValue" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_PivotEditValue"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotUserEdit", propOrder = {
    "f",
    "editValue"
})
public class CTPivotUserEdit {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected String f;
    protected CTPivotEditValue editValue;

    /**
     * Gets the value of the f property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getF() {
        return f;
    }

    /**
     * Sets the value of the f property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setF(String value) {
        this.f = value;
    }

    /**
     * Gets the value of the editValue property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotEditValue }
     *     
     */
    public CTPivotEditValue getEditValue() {
        return editValue;
    }

    /**
     * Sets the value of the editValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotEditValue }
     *     
     */
    public void setEditValue(CTPivotEditValue value) {
        this.editValue = value;
    }
}
