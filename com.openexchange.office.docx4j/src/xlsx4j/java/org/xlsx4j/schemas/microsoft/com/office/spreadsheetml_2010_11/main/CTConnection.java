
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTTextPr;


/**
 * <p>Java class for CT_Connection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Connection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="textPr" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_TextPr" minOccurs="0"/>
 *         &lt;element name="modelTextPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_ModelTextPr" minOccurs="0"/>
 *         &lt;element name="rangePr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_RangePr" minOccurs="0"/>
 *         &lt;element name="oledbPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_OledbPr" minOccurs="0"/>
 *         &lt;element name="dataFeedPr" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_DataFeedPr" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="model" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="excludeFromRefreshAll" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="autoDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="usedByAddin" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Connection", propOrder = {
    "textPr",
    "modelTextPr",
    "rangePr",
    "oledbPr",
    "dataFeedPr"
})
public class CTConnection {

    protected CTTextPr textPr;
    protected CTModelTextPr modelTextPr;
    protected CTRangePr rangePr;
    protected CTOledbPr oledbPr;
    protected CTDataFeedPr dataFeedPr;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "model")
    protected Boolean model;
    @XmlAttribute(name = "excludeFromRefreshAll")
    protected Boolean excludeFromRefreshAll;
    @XmlAttribute(name = "autoDelete")
    protected Boolean autoDelete;
    @XmlAttribute(name = "usedByAddin")
    protected Boolean usedByAddin;

    /**
     * Gets the value of the textPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTTextPr }
     *     
     */
    public CTTextPr getTextPr() {
        return textPr;
    }

    /**
     * Sets the value of the textPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTextPr }
     *     
     */
    public void setTextPr(CTTextPr value) {
        this.textPr = value;
    }

    /**
     * Gets the value of the modelTextPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTModelTextPr }
     *     
     */
    public CTModelTextPr getModelTextPr() {
        return modelTextPr;
    }

    /**
     * Sets the value of the modelTextPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTModelTextPr }
     *     
     */
    public void setModelTextPr(CTModelTextPr value) {
        this.modelTextPr = value;
    }

    /**
     * Gets the value of the rangePr property.
     * 
     * @return
     *     possible object is
     *     {@link CTRangePr }
     *     
     */
    public CTRangePr getRangePr() {
        return rangePr;
    }

    /**
     * Sets the value of the rangePr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTRangePr }
     *     
     */
    public void setRangePr(CTRangePr value) {
        this.rangePr = value;
    }

    /**
     * Gets the value of the oledbPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTOledbPr }
     *     
     */
    public CTOledbPr getOledbPr() {
        return oledbPr;
    }

    /**
     * Sets the value of the oledbPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOledbPr }
     *     
     */
    public void setOledbPr(CTOledbPr value) {
        this.oledbPr = value;
    }

    /**
     * Gets the value of the dataFeedPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTDataFeedPr }
     *     
     */
    public CTDataFeedPr getDataFeedPr() {
        return dataFeedPr;
    }

    /**
     * Sets the value of the dataFeedPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDataFeedPr }
     *     
     */
    public void setDataFeedPr(CTDataFeedPr value) {
        this.dataFeedPr = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isModel() {
        if (model == null) {
            return false;
        }
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModel(Boolean value) {
        this.model = value;
    }

    /**
     * Gets the value of the excludeFromRefreshAll property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeFromRefreshAll() {
        if (excludeFromRefreshAll == null) {
            return false;
        }
        return excludeFromRefreshAll;
    }

    /**
     * Sets the value of the excludeFromRefreshAll property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeFromRefreshAll(Boolean value) {
        this.excludeFromRefreshAll = value;
    }

    /**
     * Gets the value of the autoDelete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAutoDelete() {
        if (autoDelete == null) {
            return false;
        }
        return autoDelete;
    }

    /**
     * Sets the value of the autoDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoDelete(Boolean value) {
        this.autoDelete = value;
    }

    /**
     * Gets the value of the usedByAddin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUsedByAddin() {
        if (usedByAddin == null) {
            return false;
        }
        return usedByAddin;
    }

    /**
     * Sets the value of the usedByAddin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsedByAddin(Boolean value) {
        this.usedByAddin = value;
    }
}
