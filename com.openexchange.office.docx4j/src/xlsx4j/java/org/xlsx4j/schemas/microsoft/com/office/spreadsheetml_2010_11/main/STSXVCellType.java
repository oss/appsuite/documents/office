
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SXVCellType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SXVCellType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="b"/>
 *     &lt;enumeration value="n"/>
 *     &lt;enumeration value="e"/>
 *     &lt;enumeration value="str"/>
 *     &lt;enumeration value="d"/>
 *     &lt;enumeration value="bl"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SXVCellType")
@XmlEnum
public enum STSXVCellType {

    @XmlEnumValue("b")
    B("b"),
    @XmlEnumValue("n")
    N("n"),
    @XmlEnumValue("e")
    E("e"),
    @XmlEnumValue("str")
    STR("str"),
    @XmlEnumValue("d")
    D("d"),
    @XmlEnumValue("bl")
    BL("bl");
    private final String value;

    STSXVCellType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSXVCellType fromValue(String v) {
        for (STSXVCellType c: STSXVCellType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
