
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_WebExtension complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_WebExtension">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f"/>
 *       &lt;/sequence>
 *       &lt;attribute name="appRef" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WebExtension", propOrder = {
    "f"
})
public class CTWebExtension {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main", required = true)
    protected String f;
    @XmlAttribute(name = "appRef", required = true)
    protected String appRef;

    /**
     * Gets the value of the f property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getF() {
        return f;
    }

    /**
     * Sets the value of the f property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setF(String value) {
        this.f = value;
    }

    /**
     * Gets the value of the appRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppRef() {
        return appRef;
    }

    /**
     * Sets the value of the appRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppRef(String value) {
        this.appRef = value;
    }
}
