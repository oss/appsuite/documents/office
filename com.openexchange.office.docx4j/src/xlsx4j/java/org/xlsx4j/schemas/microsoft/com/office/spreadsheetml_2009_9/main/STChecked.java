
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_Checked.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_Checked">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Unchecked"/>
 *     &lt;enumeration value="Checked"/>
 *     &lt;enumeration value="Mixed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_Checked")
@XmlEnum
public enum STChecked {

    @XmlEnumValue("Unchecked")
    UNCHECKED("Unchecked"),
    @XmlEnumValue("Checked")
    CHECKED("Checked"),
    @XmlEnumValue("Mixed")
    MIXED("Mixed");
    private final String value;

    STChecked(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STChecked fromValue(String v) {
        for (STChecked c: STChecked.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
