
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;
import org.xlsx4j.sml.CTDxfs;
import org.xlsx4j.sml.CTPivotCaches;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.xlsx4j.spreadsheetml.main200909 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PivotCaches_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "pivotCaches");
    private final static QName _Table_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "table");
    private final static QName _ConditionalFormattings_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "conditionalFormattings");
    private final static QName _Slicers_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "slicers");
    private final static QName _SourceConnection_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "sourceConnection");
    private final static QName _SlicerStyles_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "slicerStyles");
    private final static QName _PivotTableDefinition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "pivotTableDefinition");
    private final static QName _SlicerList_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "slicerList");
    private final static QName _Dxfs_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "dxfs");
    private final static QName _CacheField_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "cacheField");
    private final static QName _OleItem_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "oleItem");
    private final static QName _ProtectedRanges_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "protectedRanges");
    private final static QName _IconFilter_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "iconFilter");
    private final static QName _PivotHierarchy_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "pivotHierarchy");
    private final static QName _Connection_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "connection");
    private final static QName _SparklineGroups_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "sparklineGroups");
    private final static QName _Filter_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "filter");
    private final static QName _DataField_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "dataField");
    private final static QName _CacheHierarchy_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "cacheHierarchy");
    private final static QName _DataValidations_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "dataValidations");
    private final static QName _IgnoredErrors_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "ignoredErrors");
    private final static QName _PivotCacheDefinition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "pivotCacheDefinition");
    private final static QName _FormControlPr_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "formControlPr");
    private final static QName _DatastoreItem_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "datastoreItem");
    private final static QName _CalculatedMember_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "calculatedMember");
    private final static QName _SlicerCaches_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "slicerCaches");
    private final static QName _PivotField_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "pivotField");
    private final static QName _CustomFilters_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "customFilters");
    private final static QName _WorkbookPr_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "workbookPr");
    private final static QName _SortCondition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "sortCondition");
    private final static QName _SlicerCacheDefinition_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "slicerCacheDefinition");
    private final static QName _Id_QNAME = new QName("http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", "id");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.xlsx4j.spreadsheetml.main200909
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTCustomFilters }
     * 
     */
    public CTCustomFilters createCTCustomFilters() {
        return new CTCustomFilters();
    }

    /**
     * Create an instance of {@link CTSourceConnection }
     * 
     */
    public CTSourceConnection createCTSourceConnection() {
        return new CTSourceConnection();
    }

    /**
     * Create an instance of {@link CTDataValidations }
     * 
     */
    public CTDataValidations createCTDataValidations() {
        return new CTDataValidations();
    }

    /**
     * Create an instance of {@link CTPivotField }
     * 
     */
    public CTPivotField createCTPivotField() {
        return new CTPivotField();
    }

    /**
     * Create an instance of {@link CTOleItem }
     * 
     */
    public CTOleItem createCTOleItem() {
        return new CTOleItem();
    }

    /**
     * Create an instance of {@link CTSlicers }
     * 
     */
    public CTSlicers createCTSlicers() {
        return new CTSlicers();
    }

    /**
     * Create an instance of {@link CTCacheHierarchy }
     * 
     */
    public CTCacheHierarchy createCTCacheHierarchy() {
        return new CTCacheHierarchy();
    }

    /**
     * Create an instance of {@link CTCacheField }
     * 
     */
    public CTCacheField createCTCacheField() {
        return new CTCacheField();
    }

    /**
     * Create an instance of {@link CTIgnoredErrors }
     * 
     */
    public CTIgnoredErrors createCTIgnoredErrors() {
        return new CTIgnoredErrors();
    }

    /**
     * Create an instance of {@link CTSlicerCacheDefinition }
     * 
     */
    public CTSlicerCacheDefinition createCTSlicerCacheDefinition() {
        return new CTSlicerCacheDefinition();
    }

    /**
     * Create an instance of {@link CTSortCondition }
     * 
     */
    public CTSortCondition createCTSortCondition() {
        return new CTSortCondition();
    }

    /**
     * Create an instance of {@link CTIconFilter }
     * 
     */
    public CTIconFilter createCTIconFilter() {
        return new CTIconFilter();
    }

    /**
     * Create an instance of {@link CTProtectedRanges }
     * 
     */
    public CTProtectedRanges createCTProtectedRanges() {
        return new CTProtectedRanges();
    }

    /**
     * Create an instance of {@link CTSlicerStyles }
     * 
     */
    public CTSlicerStyles createCTSlicerStyles() {
        return new CTSlicerStyles();
    }

    /**
     * Create an instance of {@link CTWorkbookPr }
     * 
     */
    public CTWorkbookPr createCTWorkbookPr() {
        return new CTWorkbookPr();
    }

    /**
     * Create an instance of {@link CTConnection }
     * 
     */
    public CTConnection createCTConnection() {
        return new CTConnection();
    }

    /**
     * Create an instance of {@link CTPivotTableDefinition }
     * 
     */
    public CTPivotTableDefinition createCTPivotTableDefinition() {
        return new CTPivotTableDefinition();
    }

    /**
     * Create an instance of {@link CTPivotHierarchy }
     * 
     */
    public CTPivotHierarchy createCTPivotHierarchy() {
        return new CTPivotHierarchy();
    }

    /**
     * Create an instance of {@link CTTable }
     * 
     */
    public CTTable createCTTable() {
        return new CTTable();
    }

    /**
     * Create an instance of {@link CTFormControlPr }
     * 
     */
    public CTFormControlPr createCTFormControlPr() {
        return new CTFormControlPr();
    }

    /**
     * Create an instance of {@link CTDatastoreItem }
     * 
     */
    public CTDatastoreItem createCTDatastoreItem() {
        return new CTDatastoreItem();
    }

    /**
     * Create an instance of {@link CTPivotCacheDefinition }
     * 
     */
    public CTPivotCacheDefinition createCTPivotCacheDefinition() {
        return new CTPivotCacheDefinition();
    }

    /**
     * Create an instance of {@link CTConditionalFormattings }
     * 
     */
    public CTConditionalFormattings createCTConditionalFormattings() {
        return new CTConditionalFormattings();
    }

    /**
     * Create an instance of {@link CTSlicerCaches }
     * 
     */
    public CTSlicerCaches createCTSlicerCaches() {
        return new CTSlicerCaches();
    }

    /**
     * Create an instance of {@link CTSlicerRefs }
     * 
     */
    public CTSlicerRefs createCTSlicerRefs() {
        return new CTSlicerRefs();
    }

    /**
     * Create an instance of {@link CTCalculatedMember }
     * 
     */
    public CTCalculatedMember createCTCalculatedMember() {
        return new CTCalculatedMember();
    }

    /**
     * Create an instance of {@link CTDataField }
     * 
     */
    public CTDataField createCTDataField() {
        return new CTDataField();
    }

    /**
     * Create an instance of {@link CTFilter }
     * 
     */
    public CTFilter createCTFilter() {
        return new CTFilter();
    }

    /**
     * Create an instance of {@link CTSparklineGroups }
     * 
     */
    public CTSparklineGroups createCTSparklineGroups() {
        return new CTSparklineGroups();
    }

    /**
     * Create an instance of {@link CTProtectedRange }
     * 
     */
    public CTProtectedRange createCTProtectedRange() {
        return new CTProtectedRange();
    }

    /**
     * Create an instance of {@link CTPivotEdit }
     * 
     */
    public CTPivotEdit createCTPivotEdit() {
        return new CTPivotEdit();
    }

    /**
     * Create an instance of {@link CTTabularSlicerCache }
     * 
     */
    public CTTabularSlicerCache createCTTabularSlicerCache() {
        return new CTTabularSlicerCache();
    }

    /**
     * Create an instance of {@link CTSlicerCachePivotTables }
     * 
     */
    public CTSlicerCachePivotTables createCTSlicerCachePivotTables() {
        return new CTSlicerCachePivotTables();
    }

    /**
     * Create an instance of {@link CTSparklines }
     * 
     */
    public CTSparklines createCTSparklines() {
        return new CTSparklines();
    }

    /**
     * Create an instance of {@link CTIconSet }
     * 
     */
    public CTIconSet createCTIconSet() {
        return new CTIconSet();
    }

    /**
     * Create an instance of {@link CTPivotChange }
     * 
     */
    public CTPivotChange createCTPivotChange() {
        return new CTPivotChange();
    }

    /**
     * Create an instance of {@link CTSlicerCachePivotTable }
     * 
     */
    public CTSlicerCachePivotTable createCTSlicerCachePivotTable() {
        return new CTSlicerCachePivotTable();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheRange }
     * 
     */
    public CTOlapSlicerCacheRange createCTOlapSlicerCacheRange() {
        return new CTOlapSlicerCacheRange();
    }

    /**
     * Create an instance of {@link CTSlicerCacheData }
     * 
     */
    public CTSlicerCacheData createCTSlicerCacheData() {
        return new CTSlicerCacheData();
    }

    /**
     * Create an instance of {@link CTCustomFilter }
     * 
     */
    public CTCustomFilter createCTCustomFilter() {
        return new CTCustomFilter();
    }

    /**
     * Create an instance of {@link CTCfRule }
     * 
     */
    public CTCfRule createCTCfRule() {
        return new CTCfRule();
    }

    /**
     * Create an instance of {@link CTSlicerRef }
     * 
     */
    public CTSlicerRef createCTSlicerRef() {
        return new CTSlicerRef();
    }

    /**
     * Create an instance of {@link CTSparklineGroup }
     * 
     */
    public CTSparklineGroup createCTSparklineGroup() {
        return new CTSparklineGroup();
    }

    /**
     * Create an instance of {@link CTTupleSetHeader }
     * 
     */
    public CTTupleSetHeader createCTTupleSetHeader() {
        return new CTTupleSetHeader();
    }

    /**
     * Create an instance of {@link CTPivotChanges }
     * 
     */
    public CTPivotChanges createCTPivotChanges() {
        return new CTPivotChanges();
    }

    /**
     * Create an instance of {@link CTSparkline }
     * 
     */
    public CTSparkline createCTSparkline() {
        return new CTSparkline();
    }

    /**
     * Create an instance of {@link CTPivotUserEdit }
     * 
     */
    public CTPivotUserEdit createCTPivotUserEdit() {
        return new CTPivotUserEdit();
    }

    /**
     * Create an instance of {@link CTSetLevels }
     * 
     */
    public CTSetLevels createCTSetLevels() {
        return new CTSetLevels();
    }

    /**
     * Create an instance of {@link CTSlicer }
     * 
     */
    public CTSlicer createCTSlicer() {
        return new CTSlicer();
    }

    /**
     * Create an instance of {@link CTConditionalFormats }
     * 
     */
    public CTConditionalFormats createCTConditionalFormats() {
        return new CTConditionalFormats();
    }

    /**
     * Create an instance of {@link CTDataBar }
     * 
     */
    public CTDataBar createCTDataBar() {
        return new CTDataBar();
    }

    /**
     * Create an instance of {@link CTSlicerStyleElement }
     * 
     */
    public CTSlicerStyleElement createCTSlicerStyleElement() {
        return new CTSlicerStyleElement();
    }

    /**
     * Create an instance of {@link CTConditionalFormatting }
     * 
     */
    public CTConditionalFormatting createCTConditionalFormatting() {
        return new CTConditionalFormatting();
    }

    /**
     * Create an instance of {@link CTDataValidation }
     * 
     */
    public CTDataValidation createCTDataValidation() {
        return new CTDataValidation();
    }

    /**
     * Create an instance of {@link CTListItems }
     * 
     */
    public CTListItems createCTListItems() {
        return new CTListItems();
    }

    /**
     * Create an instance of {@link CTSlicerStyleElements }
     * 
     */
    public CTSlicerStyleElements createCTSlicerStyleElements() {
        return new CTSlicerStyleElements();
    }

    /**
     * Create an instance of {@link CTTupleSetHeaders }
     * 
     */
    public CTTupleSetHeaders createCTTupleSetHeaders() {
        return new CTTupleSetHeaders();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheItemParent }
     * 
     */
    public CTOlapSlicerCacheItemParent createCTOlapSlicerCacheItemParent() {
        return new CTOlapSlicerCacheItemParent();
    }

    /**
     * Create an instance of {@link CTPivotEdits }
     * 
     */
    public CTPivotEdits createCTPivotEdits() {
        return new CTPivotEdits();
    }

    /**
     * Create an instance of {@link CTTabularSlicerCacheItems }
     * 
     */
    public CTTabularSlicerCacheItems createCTTabularSlicerCacheItems() {
        return new CTTabularSlicerCacheItems();
    }

    /**
     * Create an instance of {@link CTListItem }
     * 
     */
    public CTListItem createCTListItem() {
        return new CTListItem();
    }

    /**
     * Create an instance of {@link CTColorScale }
     * 
     */
    public CTColorScale createCTColorScale() {
        return new CTColorScale();
    }

    /**
     * Create an instance of {@link CTTupleSetRow }
     * 
     */
    public CTTupleSetRow createCTTupleSetRow() {
        return new CTTupleSetRow();
    }

    /**
     * Create an instance of {@link CTConditionalFormat }
     * 
     */
    public CTConditionalFormat createCTConditionalFormat() {
        return new CTConditionalFormat();
    }

    /**
     * Create an instance of {@link CTTupleSet }
     * 
     */
    public CTTupleSet createCTTupleSet() {
        return new CTTupleSet();
    }

    /**
     * Create an instance of {@link CTSlicerStyle }
     * 
     */
    public CTSlicerStyle createCTSlicerStyle() {
        return new CTSlicerStyle();
    }

    /**
     * Create an instance of {@link CTTupleItems }
     * 
     */
    public CTTupleItems createCTTupleItems() {
        return new CTTupleItems();
    }

    /**
     * Create an instance of {@link CTTabularSlicerCacheItem }
     * 
     */
    public CTTabularSlicerCacheItem createCTTabularSlicerCacheItem() {
        return new CTTabularSlicerCacheItem();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCache }
     * 
     */
    public CTOlapSlicerCache createCTOlapSlicerCache() {
        return new CTOlapSlicerCache();
    }

    /**
     * Create an instance of {@link CTCfvo }
     * 
     */
    public CTCfvo createCTCfvo() {
        return new CTCfvo();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheItem }
     * 
     */
    public CTOlapSlicerCacheItem createCTOlapSlicerCacheItem() {
        return new CTOlapSlicerCacheItem();
    }

    /**
     * Create an instance of {@link CTDataValidationFormula }
     * 
     */
    public CTDataValidationFormula createCTDataValidationFormula() {
        return new CTDataValidationFormula();
    }

    /**
     * Create an instance of {@link CTTupleSetRowItem }
     * 
     */
    public CTTupleSetRowItem createCTTupleSetRowItem() {
        return new CTTupleSetRowItem();
    }

    /**
     * Create an instance of {@link CTSlicerCache }
     * 
     */
    public CTSlicerCache createCTSlicerCache() {
        return new CTSlicerCache();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheLevelData }
     * 
     */
    public CTOlapSlicerCacheLevelData createCTOlapSlicerCacheLevelData() {
        return new CTOlapSlicerCacheLevelData();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheRanges }
     * 
     */
    public CTOlapSlicerCacheRanges createCTOlapSlicerCacheRanges() {
        return new CTOlapSlicerCacheRanges();
    }

    /**
     * Create an instance of {@link CTCfIcon }
     * 
     */
    public CTCfIcon createCTCfIcon() {
        return new CTCfIcon();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheSelection }
     * 
     */
    public CTOlapSlicerCacheSelection createCTOlapSlicerCacheSelection() {
        return new CTOlapSlicerCacheSelection();
    }

    /**
     * Create an instance of {@link CTSetLevel }
     * 
     */
    public CTSetLevel createCTSetLevel() {
        return new CTSetLevel();
    }

    /**
     * Create an instance of {@link CTPivotEditValue }
     * 
     */
    public CTPivotEditValue createCTPivotEditValue() {
        return new CTPivotEditValue();
    }

    /**
     * Create an instance of {@link CTIgnoredError }
     * 
     */
    public CTIgnoredError createCTIgnoredError() {
        return new CTIgnoredError();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheSelections }
     * 
     */
    public CTOlapSlicerCacheSelections createCTOlapSlicerCacheSelections() {
        return new CTOlapSlicerCacheSelections();
    }

    /**
     * Create an instance of {@link CTCacheSourceExt }
     * 
     */
    public CTCacheSourceExt createCTCacheSourceExt() {
        return new CTCacheSourceExt();
    }

    /**
     * Create an instance of {@link CTTupleSetRows }
     * 
     */
    public CTTupleSetRows createCTTupleSetRows() {
        return new CTTupleSetRows();
    }

    /**
     * Create an instance of {@link CTOlapSlicerCacheLevelsData }
     * 
     */
    public CTOlapSlicerCacheLevelsData createCTOlapSlicerCacheLevelsData() {
        return new CTOlapSlicerCacheLevelsData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCaches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "pivotCaches")
    public JAXBElement<CTPivotCaches> createPivotCaches(CTPivotCaches value) {
        return new JAXBElement<CTPivotCaches>(_PivotCaches_QNAME, CTPivotCaches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "table")
    public JAXBElement<CTTable> createTable(CTTable value) {
        return new JAXBElement<CTTable>(_Table_QNAME, CTTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTConditionalFormattings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "conditionalFormattings")
    public JAXBElement<CTConditionalFormattings> createConditionalFormattings(CTConditionalFormattings value) {
        return new JAXBElement<CTConditionalFormattings>(_ConditionalFormattings_QNAME, CTConditionalFormattings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "slicers")
    public JAXBElement<CTSlicers> createSlicers(CTSlicers value) {
        return new JAXBElement<CTSlicers>(_Slicers_QNAME, CTSlicers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSourceConnection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "sourceConnection")
    public JAXBElement<CTSourceConnection> createSourceConnection(CTSourceConnection value) {
        return new JAXBElement<CTSourceConnection>(_SourceConnection_QNAME, CTSourceConnection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerStyles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "slicerStyles")
    public JAXBElement<CTSlicerStyles> createSlicerStyles(CTSlicerStyles value) {
        return new JAXBElement<CTSlicerStyles>(_SlicerStyles_QNAME, CTSlicerStyles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotTableDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "pivotTableDefinition")
    public JAXBElement<CTPivotTableDefinition> createPivotTableDefinition(CTPivotTableDefinition value) {
        return new JAXBElement<CTPivotTableDefinition>(_PivotTableDefinition_QNAME, CTPivotTableDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerRefs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "slicerList")
    public JAXBElement<CTSlicerRefs> createSlicerList(CTSlicerRefs value) {
        return new JAXBElement<CTSlicerRefs>(_SlicerList_QNAME, CTSlicerRefs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDxfs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "dxfs")
    public JAXBElement<CTDxfs> createDxfs(CTDxfs value) {
        return new JAXBElement<CTDxfs>(_Dxfs_QNAME, CTDxfs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCacheField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "cacheField")
    public JAXBElement<CTCacheField> createCacheField(CTCacheField value) {
        return new JAXBElement<CTCacheField>(_CacheField_QNAME, CTCacheField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTOleItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "oleItem")
    public JAXBElement<CTOleItem> createOleItem(CTOleItem value) {
        return new JAXBElement<CTOleItem>(_OleItem_QNAME, CTOleItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTProtectedRanges }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "protectedRanges")
    public JAXBElement<CTProtectedRanges> createProtectedRanges(CTProtectedRanges value) {
        return new JAXBElement<CTProtectedRanges>(_ProtectedRanges_QNAME, CTProtectedRanges.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTIconFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "iconFilter")
    public JAXBElement<CTIconFilter> createIconFilter(CTIconFilter value) {
        return new JAXBElement<CTIconFilter>(_IconFilter_QNAME, CTIconFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotHierarchy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "pivotHierarchy")
    public JAXBElement<CTPivotHierarchy> createPivotHierarchy(CTPivotHierarchy value) {
        return new JAXBElement<CTPivotHierarchy>(_PivotHierarchy_QNAME, CTPivotHierarchy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTConnection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "connection")
    public JAXBElement<CTConnection> createConnection(CTConnection value) {
        return new JAXBElement<CTConnection>(_Connection_QNAME, CTConnection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSparklineGroups }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "sparklineGroups")
    public JAXBElement<CTSparklineGroups> createSparklineGroups(CTSparklineGroups value) {
        return new JAXBElement<CTSparklineGroups>(_SparklineGroups_QNAME, CTSparklineGroups.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "filter")
    public JAXBElement<CTFilter> createFilter(CTFilter value) {
        return new JAXBElement<CTFilter>(_Filter_QNAME, CTFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDataField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "dataField")
    public JAXBElement<CTDataField> createDataField(CTDataField value) {
        return new JAXBElement<CTDataField>(_DataField_QNAME, CTDataField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCacheHierarchy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "cacheHierarchy")
    public JAXBElement<CTCacheHierarchy> createCacheHierarchy(CTCacheHierarchy value) {
        return new JAXBElement<CTCacheHierarchy>(_CacheHierarchy_QNAME, CTCacheHierarchy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDataValidations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "dataValidations")
    public JAXBElement<CTDataValidations> createDataValidations(CTDataValidations value) {
        return new JAXBElement<CTDataValidations>(_DataValidations_QNAME, CTDataValidations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTIgnoredErrors }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "ignoredErrors")
    public JAXBElement<CTIgnoredErrors> createIgnoredErrors(CTIgnoredErrors value) {
        return new JAXBElement<CTIgnoredErrors>(_IgnoredErrors_QNAME, CTIgnoredErrors.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotCacheDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "pivotCacheDefinition")
    public JAXBElement<CTPivotCacheDefinition> createPivotCacheDefinition(CTPivotCacheDefinition value) {
        return new JAXBElement<CTPivotCacheDefinition>(_PivotCacheDefinition_QNAME, CTPivotCacheDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTFormControlPr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "formControlPr")
    public JAXBElement<CTFormControlPr> createFormControlPr(CTFormControlPr value) {
        return new JAXBElement<CTFormControlPr>(_FormControlPr_QNAME, CTFormControlPr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDatastoreItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "datastoreItem")
    public JAXBElement<CTDatastoreItem> createDatastoreItem(CTDatastoreItem value) {
        return new JAXBElement<CTDatastoreItem>(_DatastoreItem_QNAME, CTDatastoreItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCalculatedMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "calculatedMember")
    public JAXBElement<CTCalculatedMember> createCalculatedMember(CTCalculatedMember value) {
        return new JAXBElement<CTCalculatedMember>(_CalculatedMember_QNAME, CTCalculatedMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerCaches }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "slicerCaches")
    public JAXBElement<CTSlicerCaches> createSlicerCaches(CTSlicerCaches value) {
        return new JAXBElement<CTSlicerCaches>(_SlicerCaches_QNAME, CTSlicerCaches.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPivotField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "pivotField")
    public JAXBElement<CTPivotField> createPivotField(CTPivotField value) {
        return new JAXBElement<CTPivotField>(_PivotField_QNAME, CTPivotField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCustomFilters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "customFilters")
    public JAXBElement<CTCustomFilters> createCustomFilters(CTCustomFilters value) {
        return new JAXBElement<CTCustomFilters>(_CustomFilters_QNAME, CTCustomFilters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTWorkbookPr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "workbookPr")
    public JAXBElement<CTWorkbookPr> createWorkbookPr(CTWorkbookPr value) {
        return new JAXBElement<CTWorkbookPr>(_WorkbookPr_QNAME, CTWorkbookPr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSortCondition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "sortCondition")
    public JAXBElement<CTSortCondition> createSortCondition(CTSortCondition value) {
        return new JAXBElement<CTSortCondition>(_SortCondition_QNAME, CTSortCondition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSlicerCacheDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "slicerCacheDefinition")
    public JAXBElement<CTSlicerCacheDefinition> createSlicerCacheDefinition(CTSlicerCacheDefinition value) {
        return new JAXBElement<CTSlicerCacheDefinition>(_SlicerCacheDefinition_QNAME, CTSlicerCacheDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main", name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createId(String value) {
        return new JAXBElement<String>(_Id_QNAME, String.class, null, value);
    }
}
