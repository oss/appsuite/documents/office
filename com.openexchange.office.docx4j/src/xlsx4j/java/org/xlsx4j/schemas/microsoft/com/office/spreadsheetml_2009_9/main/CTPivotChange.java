
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_PivotChange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PivotChange">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="editValue" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_PivotEditValue"/>
 *         &lt;element name="tupleItems" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TupleItems"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="allocationMethod" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_AllocationMethod" default="equalAllocation" />
 *       &lt;attribute name="weightExpression" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PivotChange", propOrder = {
    "editValue",
    "tupleItems",
    "extLst"
})
public class CTPivotChange {

    @XmlElement(required = true)
    protected CTPivotEditValue editValue;
    @XmlElement(required = true)
    protected CTTupleItems tupleItems;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "allocationMethod")
    protected STAllocationMethod allocationMethod;
    @XmlAttribute(name = "weightExpression")
    protected String weightExpression;

    /**
     * Gets the value of the editValue property.
     * 
     * @return
     *     possible object is
     *     {@link CTPivotEditValue }
     *     
     */
    public CTPivotEditValue getEditValue() {
        return editValue;
    }

    /**
     * Sets the value of the editValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPivotEditValue }
     *     
     */
    public void setEditValue(CTPivotEditValue value) {
        this.editValue = value;
    }

    /**
     * Gets the value of the tupleItems property.
     * 
     * @return
     *     possible object is
     *     {@link CTTupleItems }
     *     
     */
    public CTTupleItems getTupleItems() {
        return tupleItems;
    }

    /**
     * Sets the value of the tupleItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTupleItems }
     *     
     */
    public void setTupleItems(CTTupleItems value) {
        this.tupleItems = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the allocationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link STAllocationMethod }
     *     
     */
    public STAllocationMethod getAllocationMethod() {
        if (allocationMethod == null) {
            return STAllocationMethod.EQUAL_ALLOCATION;
        }
        return allocationMethod;
    }

    /**
     * Sets the value of the allocationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link STAllocationMethod }
     *     
     */
    public void setAllocationMethod(STAllocationMethod value) {
        this.allocationMethod = value;
    }

    /**
     * Gets the value of the weightExpression property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightExpression() {
        return weightExpression;
    }

    /**
     * Sets the value of the weightExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightExpression(String value) {
        this.weightExpression = value;
    }
}
