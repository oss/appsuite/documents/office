
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_EditValidation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_EditValidation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="text"/>
 *     &lt;enumeration value="integer"/>
 *     &lt;enumeration value="number"/>
 *     &lt;enumeration value="reference"/>
 *     &lt;enumeration value="formula"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_EditValidation")
@XmlEnum
public enum STEditValidation {

    @XmlEnumValue("text")
    TEXT("text"),
    @XmlEnumValue("integer")
    INTEGER("integer"),
    @XmlEnumValue("number")
    NUMBER("number"),
    @XmlEnumValue("reference")
    REFERENCE("reference"),
    @XmlEnumValue("formula")
    FORMULA("formula");
    private final String value;

    STEditValidation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STEditValidation fromValue(String v) {
        for (STEditValidation c: STEditValidation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
