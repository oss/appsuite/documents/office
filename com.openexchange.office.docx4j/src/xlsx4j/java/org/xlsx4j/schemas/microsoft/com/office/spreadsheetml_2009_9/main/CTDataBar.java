
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTColor;
import org.xlsx4j.sml.IDataBar;


/**
 * <p>Java class for CT_DataBar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_DataBar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cfvo" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_Cfvo" maxOccurs="2" minOccurs="2"/>
 *         &lt;element name="fillColor" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="borderColor" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="negativeFillColor" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="negativeBorderColor" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *         &lt;element name="axisColor" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_Color" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="minLength" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="10" />
 *       &lt;attribute name="maxLength" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" default="90" />
 *       &lt;attribute name="showValue" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="border" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="gradient" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="direction" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_DataBarDirection" default="context" />
 *       &lt;attribute name="negativeBarColorSameAsPositive" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="negativeBarBorderColorSameAsPositive" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="axisPosition" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}ST_DataBarAxisPosition" default="automatic" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DataBar", propOrder = {
    "cfvo",
    "fillColor",
    "borderColor",
    "negativeFillColor",
    "negativeBorderColor",
    "axisColor"
})
public class CTDataBar implements IDataBar {

    @XmlElement(required = true)
    protected List<CTCfvo> cfvo;
    protected CTColor fillColor;
    protected CTColor borderColor;
    protected CTColor negativeFillColor;
    protected CTColor negativeBorderColor;
    protected CTColor axisColor;
    @XmlAttribute(name = "minLength")
    @XmlSchemaType(name = "unsignedInt")
    protected Long minLength;
    @XmlAttribute(name = "maxLength")
    @XmlSchemaType(name = "unsignedInt")
    protected Long maxLength;
    @XmlAttribute(name = "showValue")
    protected Boolean showValue;
    @XmlAttribute(name = "border")
    protected Boolean border;
    @XmlAttribute(name = "gradient")
    protected Boolean gradient;
    @XmlAttribute(name = "direction")
    protected STDataBarDirection direction;
    @XmlAttribute(name = "negativeBarColorSameAsPositive")
    protected Boolean negativeBarColorSameAsPositive;
    @XmlAttribute(name = "negativeBarBorderColorSameAsPositive")
    protected Boolean negativeBarBorderColorSameAsPositive;
    @XmlAttribute(name = "axisPosition")
    protected STDataBarAxisPosition axisPosition;

    /**
     * Gets the value of the cfvo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cfvo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCfvo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTCfvo }
     * 
     * 
     */
    @Override
    public List<CTCfvo> getCfvo() {
        if (cfvo == null) {
            cfvo = new ArrayList<CTCfvo>();
        }
        return this.cfvo;
    }

    /**
     * Gets the value of the fillColor property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    @Override
    public CTColor getColor() {
        return fillColor;
    }

    /**
     * Sets the value of the fillColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setFillColor(CTColor value) {
        this.fillColor = value;
    }

    /**
     * Gets the value of the borderColor property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getBorderColor() {
        return borderColor;
    }

    /**
     * Sets the value of the borderColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setBorderColor(CTColor value) {
        this.borderColor = value;
    }

    /**
     * Gets the value of the negativeFillColor property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getNegativeFillColor() {
        return negativeFillColor;
    }

    /**
     * Sets the value of the negativeFillColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setNegativeFillColor(CTColor value) {
        this.negativeFillColor = value;
    }

    /**
     * Gets the value of the negativeBorderColor property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getNegativeBorderColor() {
        return negativeBorderColor;
    }

    /**
     * Sets the value of the negativeBorderColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setNegativeBorderColor(CTColor value) {
        this.negativeBorderColor = value;
    }

    /**
     * Gets the value of the axisColor property.
     * 
     * @return
     *     possible object is
     *     {@link CTColor }
     *     
     */
    public CTColor getAxisColor() {
        return axisColor;
    }

    /**
     * Sets the value of the axisColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTColor }
     *     
     */
    public void setAxisColor(CTColor value) {
        this.axisColor = value;
    }

    /**
     * Gets the value of the minLength property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Override
    public long getMinLength() {
        if (minLength == null) {
            return  10L;
        }
        return minLength;
    }

    /**
     * Sets the value of the minLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    @Override
    public void setMinLength(Long value) {
        this.minLength = value;
    }

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    @Override
    public long getMaxLength() {
        if (maxLength == null) {
            return  90L;
        }
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    @Override
    public void setMaxLength(Long value) {
        this.maxLength = value;
    }

    /**
     * Gets the value of the showValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isShowValue() {
        if (showValue == null) {
            return true;
        }
        return showValue;
    }

    /**
     * Sets the value of the showValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setShowValue(Boolean value) {
        this.showValue = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBorder() {
        if (border == null) {
            return false;
        }
        return border;
    }

    /**
     * Sets the value of the border property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBorder(Boolean value) {
        this.border = value;
    }

    /**
     * Gets the value of the gradient property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGradient() {
        if (gradient == null) {
            return true;
        }
        return gradient;
    }

    /**
     * Sets the value of the gradient property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGradient(Boolean value) {
        this.gradient = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     * @return
     *     possible object is
     *     {@link STDataBarDirection }
     *     
     */
    public STDataBarDirection getDirection() {
        if (direction == null) {
            return STDataBarDirection.CONTEXT;
        }
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataBarDirection }
     *     
     */
    public void setDirection(STDataBarDirection value) {
        this.direction = value;
    }

    /**
     * Gets the value of the negativeBarColorSameAsPositive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNegativeBarColorSameAsPositive() {
        if (negativeBarColorSameAsPositive == null) {
            return false;
        }
        return negativeBarColorSameAsPositive;
    }

    /**
     * Sets the value of the negativeBarColorSameAsPositive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNegativeBarColorSameAsPositive(Boolean value) {
        this.negativeBarColorSameAsPositive = value;
    }

    /**
     * Gets the value of the negativeBarBorderColorSameAsPositive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNegativeBarBorderColorSameAsPositive() {
        if (negativeBarBorderColorSameAsPositive == null) {
            return true;
        }
        return negativeBarBorderColorSameAsPositive;
    }

    /**
     * Sets the value of the negativeBarBorderColorSameAsPositive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNegativeBarBorderColorSameAsPositive(Boolean value) {
        this.negativeBarBorderColorSameAsPositive = value;
    }

    /**
     * Gets the value of the axisPosition property.
     * 
     * @return
     *     possible object is
     *     {@link STDataBarAxisPosition }
     *     
     */
    public STDataBarAxisPosition getAxisPosition() {
        if (axisPosition == null) {
            return STDataBarAxisPosition.AUTOMATIC;
        }
        return axisPosition;
    }

    /**
     * Sets the value of the axisPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataBarAxisPosition }
     *     
     */
    public void setAxisPosition(STDataBarAxisPosition value) {
        this.axisPosition = value;
    }
}
