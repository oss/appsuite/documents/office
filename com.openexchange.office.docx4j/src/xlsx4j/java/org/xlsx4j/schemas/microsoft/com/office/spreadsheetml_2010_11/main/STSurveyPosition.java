
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SurveyPosition.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SurveyPosition">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="absolute"/>
 *     &lt;enumeration value="fixed"/>
 *     &lt;enumeration value="relative"/>
 *     &lt;enumeration value="static"/>
 *     &lt;enumeration value="inherit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SurveyPosition")
@XmlEnum
public enum STSurveyPosition {

    @XmlEnumValue("absolute")
    ABSOLUTE("absolute"),
    @XmlEnumValue("fixed")
    FIXED("fixed"),
    @XmlEnumValue("relative")
    RELATIVE("relative"),
    @XmlEnumValue("static")
    STATIC("static"),
    @XmlEnumValue("inherit")
    INHERIT("inherit");
    private final String value;

    STSurveyPosition(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSurveyPosition fromValue(String v) {
        for (STSurveyPosition c: STSurveyPosition.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
