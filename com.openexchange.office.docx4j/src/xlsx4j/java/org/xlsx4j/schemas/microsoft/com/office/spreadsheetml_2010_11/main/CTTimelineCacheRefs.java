
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineCacheRefs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineCacheRefs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timelineCacheRef" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineCacheRef" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineCacheRefs", propOrder = {
    "timelineCacheRef"
})
public class CTTimelineCacheRefs {

    @XmlElement(required = true)
    protected List<CTTimelineCacheRef> timelineCacheRef;

    /**
     * Gets the value of the timelineCacheRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timelineCacheRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimelineCacheRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTTimelineCacheRef }
     * 
     * 
     */
    public List<CTTimelineCacheRef> getTimelineCacheRef() {
        if (timelineCacheRef == null) {
            timelineCacheRef = new ArrayList<CTTimelineCacheRef>();
        }
        return this.timelineCacheRef;
    }
}
