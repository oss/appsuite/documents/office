
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SlicerCacheData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SlicerCacheData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="olap" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_OlapSlicerCache"/>
 *         &lt;element name="tabular" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_TabularSlicerCache"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SlicerCacheData", propOrder = {
    "olap",
    "tabular"
})
public class CTSlicerCacheData {

    protected CTOlapSlicerCache olap;
    protected CTTabularSlicerCache tabular;

    /**
     * Gets the value of the olap property.
     * 
     * @return
     *     possible object is
     *     {@link CTOlapSlicerCache }
     *     
     */
    public CTOlapSlicerCache getOlap() {
        return olap;
    }

    /**
     * Sets the value of the olap property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOlapSlicerCache }
     *     
     */
    public void setOlap(CTOlapSlicerCache value) {
        this.olap = value;
    }

    /**
     * Gets the value of the tabular property.
     * 
     * @return
     *     possible object is
     *     {@link CTTabularSlicerCache }
     *     
     */
    public CTTabularSlicerCache getTabular() {
        return tabular;
    }

    /**
     * Sets the value of the tabular property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTabularSlicerCache }
     *     
     */
    public void setTabular(CTTabularSlicerCache value) {
        this.tabular = value;
    }
}
