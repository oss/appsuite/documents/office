
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineCachePivotTables complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineCachePivotTables">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pivotTable" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineCachePivotTable" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineCachePivotTables", propOrder = {
    "pivotTable"
})
public class CTTimelineCachePivotTables {

    @XmlElement(required = true)
    protected List<CTTimelineCachePivotTable> pivotTable;

    /**
     * Gets the value of the pivotTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pivotTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPivotTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTTimelineCachePivotTable }
     * 
     * 
     */
    public List<CTTimelineCachePivotTable> getPivotTable() {
        if (pivotTable == null) {
            pivotTable = new ArrayList<CTTimelineCachePivotTable>();
        }
        return this.pivotTable;
    }
}
