
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import java.util.List;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.schemas.microsoft.com.office.excel_2006.main.CTSqref;
import org.xlsx4j.sml.IDataValidation;
import org.xlsx4j.sml.STDataValidationErrorStyle;
import org.xlsx4j.sml.STDataValidationImeMode;
import org.xlsx4j.sml.STDataValidationOperator;
import org.xlsx4j.sml.STDataValidationType;


/**
 * <p>Java class for CT_DataValidation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_DataValidation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formula1" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_DataValidationFormula" minOccurs="0"/>
 *         &lt;element name="formula2" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_DataValidationFormula" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}sqref"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_DataValidationType" default="none" />
 *       &lt;attribute name="errorStyle" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_DataValidationErrorStyle" default="stop" />
 *       &lt;attribute name="imeMode" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_DataValidationImeMode" default="noControl" />
 *       &lt;attribute name="operator" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_DataValidationOperator" default="between" />
 *       &lt;attribute name="allowBlank" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="showDropDown" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="showInputMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="showErrorMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="errorTitle" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="error" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="promptTitle" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="prompt" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DataValidation", propOrder = {
    "formula1",
    "formula2",
    "sqref"
})
public class CTDataValidation implements IDataValidation {

    protected CTDataValidationFormula formula1;
    protected CTDataValidationFormula formula2;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main", required = true)
    protected CTSqref sqref;
    @XmlAttribute(name = "type")
    protected STDataValidationType type;
    @XmlAttribute(name = "errorStyle")
    protected STDataValidationErrorStyle errorStyle;
    @XmlAttribute(name = "imeMode")
    protected STDataValidationImeMode imeMode;
    @XmlAttribute(name = "operator")
    protected STDataValidationOperator operator;
    @XmlAttribute(name = "allowBlank")
    protected Boolean allowBlank;
    @XmlAttribute(name = "showDropDown")
    protected Boolean showDropDown;
    @XmlAttribute(name = "showInputMessage")
    protected Boolean showInputMessage;
    @XmlAttribute(name = "showErrorMessage")
    protected Boolean showErrorMessage;
    @XmlAttribute(name = "errorTitle")
    protected String errorTitle;
    @XmlAttribute(name = "error")
    protected String error;
    @XmlAttribute(name = "promptTitle")
    protected String promptTitle;
    @XmlAttribute(name = "prompt")
    protected String prompt;
    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the formula1 property.
     * 
     * @return
     *     possible object is
     *     {@link CTDataValidationFormula }
     *     
     */
    @Override
    public String getFormula1() {
        return formula1!=null ? formula1.getF() : null;
    }

    /**
     * Sets the value of the formula1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDataValidationFormula }
     *     
     */
    @Override
    public void setFormula1(String value) {
    	if(value!=null) {
    		if(formula1==null) {
    			formula1 = new CTDataValidationFormula();
    		}
    		formula1.setF(value);
    	}
    	else {
    		formula1 = null;
    	}
    }

    /**
     * Gets the value of the formula2 property.
     * 
     * @return
     *     possible object is
     *     {@link CTDataValidationFormula }
     *     
     */
    @Override
    public String getFormula2() {
        return formula2!=null ? formula2.getF() : null;
    }

    /**
     * Sets the value of the formula2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTDataValidationFormula }
     *     
     */
    @Override
    public void setFormula2(String value) {
    	if(value!=null) {
    		if(formula2==null) {
    			formula2 = new CTDataValidationFormula();
    		}
    		formula2.setF(value);
    	}
    	else {
    		formula2 = null;
    	}
    }

    @Override
    public List<String> getsqref() {
        if (sqref == null) {
            sqref = new CTSqref();
        }
        return sqref.getSqrefList();
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link STDataValidationType }
     *     
     */
    @Override
    public STDataValidationType getType() {
        if (type == null) {
            return STDataValidationType.NONE;
        }
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataValidationType }
     *     
     */
    @Override
    public void setType(STDataValidationType value) {
        this.type = value;
    }

    /**
     * Gets the value of the errorStyle property.
     * 
     * @return
     *     possible object is
     *     {@link STDataValidationErrorStyle }
     *     
     */
    @Override
    public STDataValidationErrorStyle getErrorStyle() {
        if (errorStyle == null) {
            return STDataValidationErrorStyle.STOP;
        }
        return errorStyle;
    }

    /**
     * Sets the value of the errorStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataValidationErrorStyle }
     *     
     */
    @Override
    public void setErrorStyle(STDataValidationErrorStyle value) {
        this.errorStyle = value;
    }

    /**
     * Gets the value of the imeMode property.
     * 
     * @return
     *     possible object is
     *     {@link STDataValidationImeMode }
     *     
     */
    @Override
    public STDataValidationImeMode getImeMode() {
        if (imeMode == null) {
            return STDataValidationImeMode.NO_CONTROL;
        }
        return imeMode;
    }

    /**
     * Sets the value of the imeMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataValidationImeMode }
     *     
     */
    @Override
    public void setImeMode(STDataValidationImeMode value) {
        this.imeMode = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link STDataValidationOperator }
     *     
     */
    @Override
    public STDataValidationOperator getOperator() {
        if (operator == null) {
            return STDataValidationOperator.BETWEEN;
        }
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link STDataValidationOperator }
     *     
     */
    @Override
    public void setOperator(STDataValidationOperator value) {
        this.operator = value;
    }

    /**
     * Gets the value of the allowBlank property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isAllowBlank() {
        if (allowBlank == null) {
            return false;
        }
        return allowBlank;
    }

    /**
     * Sets the value of the allowBlank property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setAllowBlank(Boolean value) {
        this.allowBlank = value;
    }

    /**
     * Gets the value of the showDropDown property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isShowDropDown() {
        if (showDropDown == null) {
            return false;
        }
        return showDropDown;
    }

    /**
     * Sets the value of the showDropDown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setShowDropDown(Boolean value) {
        this.showDropDown = value;
    }

    /**
     * Gets the value of the showInputMessage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isShowInputMessage() {
        if (showInputMessage == null) {
            return false;
        }
        return showInputMessage;
    }

    /**
     * Sets the value of the showInputMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setShowInputMessage(Boolean value) {
        this.showInputMessage = value;
    }

    /**
     * Gets the value of the showErrorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isShowErrorMessage() {
        if (showErrorMessage == null) {
            return false;
        }
        return showErrorMessage;
    }

    /**
     * Sets the value of the showErrorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setShowErrorMessage(Boolean value) {
        this.showErrorMessage = value;
    }

    /**
     * Gets the value of the errorTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Override
    public String getErrorTitle() {
        return errorTitle;
    }

    /**
     * Sets the value of the errorTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Override
    public void setErrorTitle(String value) {
        this.errorTitle = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Override
    public String getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Override
    public void setError(String value) {
        this.error = value;
    }

    /**
     * Gets the value of the promptTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Override
    public String getPromptTitle() {
        return promptTitle;
    }

    /**
     * Sets the value of the promptTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Override
    public void setPromptTitle(String value) {
        this.promptTitle = value;
    }

    /**
     * Gets the value of the prompt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Override
    public String getPrompt() {
        return prompt;
    }

    /**
     * Sets the value of the prompt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Override
    public void setPrompt(String value) {
        this.prompt = value;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
    	setParent(_parent);
    }
}
