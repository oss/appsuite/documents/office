
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_QuestionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_QuestionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="checkBox"/>
 *     &lt;enumeration value="choice"/>
 *     &lt;enumeration value="date"/>
 *     &lt;enumeration value="time"/>
 *     &lt;enumeration value="multipleLinesOfText"/>
 *     &lt;enumeration value="number"/>
 *     &lt;enumeration value="singleLineOfText"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_QuestionType")
@XmlEnum
public enum STQuestionType {

    @XmlEnumValue("checkBox")
    CHECK_BOX("checkBox"),
    @XmlEnumValue("choice")
    CHOICE("choice"),
    @XmlEnumValue("date")
    DATE("date"),
    @XmlEnumValue("time")
    TIME("time"),
    @XmlEnumValue("multipleLinesOfText")
    MULTIPLE_LINES_OF_TEXT("multipleLinesOfText"),
    @XmlEnumValue("number")
    NUMBER("number"),
    @XmlEnumValue("singleLineOfText")
    SINGLE_LINE_OF_TEXT("singleLineOfText");
    private final String value;

    STQuestionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STQuestionType fromValue(String v) {
        for (STQuestionType c: STQuestionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
