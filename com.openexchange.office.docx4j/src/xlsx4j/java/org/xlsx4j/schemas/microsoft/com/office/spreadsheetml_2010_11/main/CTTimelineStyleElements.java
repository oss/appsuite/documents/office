
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineStyleElements complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineStyleElements">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timelineStyleElement" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineStyleElement" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineStyleElements", propOrder = {
    "timelineStyleElement"
})
public class CTTimelineStyleElements {

    @XmlElement(required = true)
    protected List<CTTimelineStyleElement> timelineStyleElement;

    /**
     * Gets the value of the timelineStyleElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timelineStyleElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimelineStyleElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTTimelineStyleElement }
     * 
     * 
     */
    public List<CTTimelineStyleElement> getTimelineStyleElement() {
        if (timelineStyleElement == null) {
            timelineStyleElement = new ArrayList<CTTimelineStyleElement>();
        }
        return this.timelineStyleElement;
    }
}
