
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SparklineAxisMinMax.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SparklineAxisMinMax">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="individual"/>
 *     &lt;enumeration value="group"/>
 *     &lt;enumeration value="custom"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SparklineAxisMinMax")
@XmlEnum
public enum STSparklineAxisMinMax {

    @XmlEnumValue("individual")
    INDIVIDUAL("individual"),
    @XmlEnumValue("group")
    GROUP("group"),
    @XmlEnumValue("custom")
    CUSTOM("custom");
    private final String value;

    STSparklineAxisMinMax(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSparklineAxisMinMax fromValue(String v) {
        for (STSparklineAxisMinMax c: STSparklineAxisMinMax.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
