
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_OlapSlicerCache complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_OlapSlicerCache">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="levels" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_OlapSlicerCacheLevelsData"/>
 *         &lt;element name="selections" type="{http://schemas.microsoft.com/office/spreadsheetml/2009/9/main}CT_OlapSlicerCacheSelections"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="pivotCacheId" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_OlapSlicerCache", propOrder = {
    "levels",
    "selections",
    "extLst"
})
public class CTOlapSlicerCache {

    @XmlElement(required = true)
    protected CTOlapSlicerCacheLevelsData levels;
    @XmlElement(required = true)
    protected CTOlapSlicerCacheSelections selections;
    protected CTExtensionList extLst;
    @XmlAttribute(name = "pivotCacheId", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long pivotCacheId;

    /**
     * Gets the value of the levels property.
     * 
     * @return
     *     possible object is
     *     {@link CTOlapSlicerCacheLevelsData }
     *     
     */
    public CTOlapSlicerCacheLevelsData getLevels() {
        return levels;
    }

    /**
     * Sets the value of the levels property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOlapSlicerCacheLevelsData }
     *     
     */
    public void setLevels(CTOlapSlicerCacheLevelsData value) {
        this.levels = value;
    }

    /**
     * Gets the value of the selections property.
     * 
     * @return
     *     possible object is
     *     {@link CTOlapSlicerCacheSelections }
     *     
     */
    public CTOlapSlicerCacheSelections getSelections() {
        return selections;
    }

    /**
     * Sets the value of the selections property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOlapSlicerCacheSelections }
     *     
     */
    public void setSelections(CTOlapSlicerCacheSelections value) {
        this.selections = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the pivotCacheId property.
     * 
     */
    public long getPivotCacheId() {
        return pivotCacheId;
    }

    /**
     * Sets the value of the pivotCacheId property.
     * 
     */
    public void setPivotCacheId(long value) {
        this.pivotCacheId = value;
    }
}
