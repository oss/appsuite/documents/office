
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.schemas.microsoft.com.office.excel_2006.main.CTSqref;


/**
 * <p>Java class for CT_Sparkline complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Sparkline">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}f" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/excel/2006/main}sqref"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Sparkline", propOrder = {
    "f",
    "sqref"
})
public class CTSparkline {

    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main")
    protected String f;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/excel/2006/main", required = true)
    protected CTSqref sqref;

    /**
     * Gets the value of the f property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getF() {
        return f;
    }

    /**
     * Sets the value of the f property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setF(String value) {
        this.f = value;
    }

    /**
     * Gets the value of the sqref property.
     * 
     * @return
     *     possible object is
     *     {@link CTSqref }
     *     
     */
    public CTSqref getSqref() {
        return sqref;
    }

    /**
     * Sets the value of the sqref property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTSqref }
     *     
     */
    public void setSqref(CTSqref value) {
        this.sqref = value;
    }
}
