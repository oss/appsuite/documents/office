
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_TimelineStyles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TimelineStyles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="timelineStyle" type="{http://schemas.microsoft.com/office/spreadsheetml/2010/11/main}CT_TimelineStyle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="defaultTimelineStyle" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TimelineStyles", propOrder = {
    "timelineStyle"
})
public class CTTimelineStyles {

    protected List<CTTimelineStyle> timelineStyle;
    @XmlAttribute(name = "defaultTimelineStyle", required = true)
    protected String defaultTimelineStyle;

    /**
     * Gets the value of the timelineStyle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timelineStyle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimelineStyle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTTimelineStyle }
     * 
     * 
     */
    public List<CTTimelineStyle> getTimelineStyle() {
        if (timelineStyle == null) {
            timelineStyle = new ArrayList<CTTimelineStyle>();
        }
        return this.timelineStyle;
    }

    /**
     * Gets the value of the defaultTimelineStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultTimelineStyle() {
        return defaultTimelineStyle;
    }

    /**
     * Sets the value of the defaultTimelineStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultTimelineStyle(String value) {
        this.defaultTimelineStyle = value;
    }
}
