
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_ModelRelationship complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ModelRelationship">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="fromTable" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="fromColumn" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="toTable" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="toColumn" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ModelRelationship")
public class CTModelRelationship {

    @XmlAttribute(name = "fromTable", required = true)
    protected String fromTable;
    @XmlAttribute(name = "fromColumn", required = true)
    protected String fromColumn;
    @XmlAttribute(name = "toTable", required = true)
    protected String toTable;
    @XmlAttribute(name = "toColumn", required = true)
    protected String toColumn;

    /**
     * Gets the value of the fromTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromTable() {
        return fromTable;
    }

    /**
     * Sets the value of the fromTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromTable(String value) {
        this.fromTable = value;
    }

    /**
     * Gets the value of the fromColumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromColumn() {
        return fromColumn;
    }

    /**
     * Sets the value of the fromColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromColumn(String value) {
        this.fromColumn = value;
    }

    /**
     * Gets the value of the toTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToTable() {
        return toTable;
    }

    /**
     * Sets the value of the toTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToTable(String value) {
        this.toTable = value;
    }

    /**
     * Gets the value of the toColumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToColumn() {
        return toColumn;
    }

    /**
     * Sets the value of the toColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToColumn(String value) {
        this.toColumn = value;
    }
}
