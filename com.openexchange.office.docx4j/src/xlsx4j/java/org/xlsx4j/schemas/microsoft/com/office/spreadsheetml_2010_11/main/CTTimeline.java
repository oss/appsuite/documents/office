
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.xlsx4j.sml.CTExtensionList;


/**
 * <p>Java class for CT_Timeline complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Timeline">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}CT_ExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="cache" use="required" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="caption" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *       &lt;attribute name="showHeader" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="showSelectionLabel" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="showTimeLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="showHorizontalScrollbar" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="level" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="selectionLevel" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="scrollPosition" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="style" type="{http://schemas.openxmlformats.org/spreadsheetml/2006/main}ST_Xstring" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Timeline", propOrder = {
    "extLst"
})
public class CTTimeline {

    protected CTExtensionList extLst;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "cache", required = true)
    protected String cache;
    @XmlAttribute(name = "caption")
    protected String caption;
    @XmlAttribute(name = "showHeader")
    protected Boolean showHeader;
    @XmlAttribute(name = "showSelectionLabel")
    protected Boolean showSelectionLabel;
    @XmlAttribute(name = "showTimeLevel")
    protected Boolean showTimeLevel;
    @XmlAttribute(name = "showHorizontalScrollbar")
    protected Boolean showHorizontalScrollbar;
    @XmlAttribute(name = "level", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long level;
    @XmlAttribute(name = "selectionLevel", required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long selectionLevel;
    @XmlAttribute(name = "scrollPosition")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scrollPosition;
    @XmlAttribute(name = "style")
    protected String style;

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTExtensionList }
     *     
     */
    public CTExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTExtensionList }
     *     
     */
    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the cache property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCache() {
        return cache;
    }

    /**
     * Sets the value of the cache property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCache(String value) {
        this.cache = value;
    }

    /**
     * Gets the value of the caption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Sets the value of the caption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaption(String value) {
        this.caption = value;
    }

    /**
     * Gets the value of the showHeader property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShowHeader() {
        if (showHeader == null) {
            return true;
        }
        return showHeader;
    }

    /**
     * Sets the value of the showHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowHeader(Boolean value) {
        this.showHeader = value;
    }

    /**
     * Gets the value of the showSelectionLabel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShowSelectionLabel() {
        if (showSelectionLabel == null) {
            return true;
        }
        return showSelectionLabel;
    }

    /**
     * Sets the value of the showSelectionLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowSelectionLabel(Boolean value) {
        this.showSelectionLabel = value;
    }

    /**
     * Gets the value of the showTimeLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShowTimeLevel() {
        if (showTimeLevel == null) {
            return true;
        }
        return showTimeLevel;
    }

    /**
     * Sets the value of the showTimeLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowTimeLevel(Boolean value) {
        this.showTimeLevel = value;
    }

    /**
     * Gets the value of the showHorizontalScrollbar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShowHorizontalScrollbar() {
        if (showHorizontalScrollbar == null) {
            return true;
        }
        return showHorizontalScrollbar;
    }

    /**
     * Sets the value of the showHorizontalScrollbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowHorizontalScrollbar(Boolean value) {
        this.showHorizontalScrollbar = value;
    }

    /**
     * Gets the value of the level property.
     * 
     */
    public long getLevel() {
        return level;
    }

    /**
     * Sets the value of the level property.
     * 
     */
    public void setLevel(long value) {
        this.level = value;
    }

    /**
     * Gets the value of the selectionLevel property.
     * 
     */
    public long getSelectionLevel() {
        return selectionLevel;
    }

    /**
     * Sets the value of the selectionLevel property.
     * 
     */
    public void setSelectionLevel(long value) {
        this.selectionLevel = value;
    }

    /**
     * Gets the value of the scrollPosition property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScrollPosition() {
        return scrollPosition;
    }

    /**
     * Sets the value of the scrollPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScrollPosition(XMLGregorianCalendar value) {
        this.scrollPosition = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStyle(String value) {
        this.style = value;
    }
}
