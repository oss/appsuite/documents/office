
package org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_OlapSlicerCacheSortOrder.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_OlapSlicerCacheSortOrder">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="natural"/>
 *     &lt;enumeration value="ascending"/>
 *     &lt;enumeration value="descending"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_OlapSlicerCacheSortOrder")
@XmlEnum
public enum STOlapSlicerCacheSortOrder {

    @XmlEnumValue("natural")
    NATURAL("natural"),
    @XmlEnumValue("ascending")
    ASCENDING("ascending"),
    @XmlEnumValue("descending")
    DESCENDING("descending");
    private final String value;

    STOlapSlicerCacheSortOrder(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STOlapSlicerCacheSortOrder fromValue(String v) {
        for (STOlapSlicerCacheSortOrder c: STOlapSlicerCacheSortOrder.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
