/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.xlsx4j.sml_2017;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;

/*
    <xsd:complexType name="CT_DynamicArrayProperties">
        <xsd:sequence>
            <xsd:element name="extLst"
                         type="x:CT_ExtensionList"
                         minOccurs="0"
                         maxOccurs="1"/>
        </xsd:sequence>
        <xsd:attribute name="fDynamic"
                       type="xsd:boolean"
                       use="optional"/>
        <xsd:attribute name="fCollapsed"
                       type="xsd:boolean"
                       use="optional"/>
    </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_DynamicArrayProperties", propOrder = {
    "extLst"
})
public class CTDynamicArrayProperties {

    protected CTExtensionList extLst;

    @XmlAttribute(name="fDynamic")
    protected Boolean fDynamic;

    @XmlAttribute(name="fCollapsed")
    protected Boolean fCollapsed;

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList value) {
        this.extLst = value;
    }

    public Boolean getfDynamic() {
        return fDynamic;
    }

    public void setfDynamic(Boolean fDynamic) {
        this.fDynamic = fDynamic;
    }

    public Boolean getfCollapsed() {
        return fCollapsed;
    }

    public void setfCollapsed(Boolean fCollapsed) {
        this.fCollapsed = fCollapsed;
    }
}
