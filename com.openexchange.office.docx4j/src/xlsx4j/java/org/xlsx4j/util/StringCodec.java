/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.xlsx4j.util;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class StringCodec {

    /** Regular expression to find "_xhhhh_" sequences to be decoded. */
    private final static Pattern DECODE_PATTERN = Pattern.compile("_x([0-9a-f]{4})_", Pattern.CASE_INSENSITIVE);

    /** Regular expression to find literal "_xhhhh_" sequences to be encoded. */
    private final static Pattern ENCODE_PATTERN_1 = Pattern.compile("_x[0-9a-f]{4}(?=_)", Pattern.CASE_INSENSITIVE);

    /** Regular expression to find ASCII control characters to be encoded. */
    private final static Pattern ENCODE_PATTERN_2 = Pattern.compile("[\\x00-\\x08\\x0b-\\x1f]");

    /**
     * Decodes a string that was stored in an SpreadsheetML part.
     *
     * All "_xhhhh_" sequences where "hhhh" is a hexadecimal number will be
     * replaced with the respective UniCode character, e.g. "a_x0018_b" => "a\x18b".
     *
     * @param value
     *  The string to be decoded.
     *
     * @return
     *  The decoded string.
     */
    public final static String decode(final String value) {

        // silently skip nulls
        if (value == null) return null;

        // replace all _xhhhh_ sequences with respective UniCode character
        final StringBuffer buffer = new StringBuffer();
        final Matcher matcher = DECODE_PATTERN.matcher(value);
        while (matcher.find()) {
            final MatchResult result = matcher.toMatchResult();
            final int code = Integer.parseInt(result.group(1), 16);
            matcher.appendReplacement(buffer, Character.toString(code));
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    /**
     * Encodes a string that may contain any UniCode characters to a string that
     * can be stored in a SpreadsheetML part.
     *
     * All ASCII control characters ("\x00"-"\x1F" except "\x09" and "\x0A") will
     * be encoded with "_xhhhh_" sequences, e.g. "a\x18b" => "a_x0018_b".
     *
     * The leading underscore character of literal "_xhhhh_" sequences will be
     * encoded too, e.g. "a_x0000_b" => "a_x005F_0000_b".
     *
     * @param value
     *  The string to be encoded.
     *
     * @return
     *  The encoded string.
     */
    public final static String encode(final String value) {

        // silently skip nulls
        if (value == null) return null;

        // replace the leading underscore in existing _xhhhh_ sequences with _x005F_
        final StringBuffer buffer1 = new StringBuffer();
        final Matcher matcher1 = ENCODE_PATTERN_1.matcher(value);
        while (matcher1.find()) {
            final MatchResult result = matcher1.toMatchResult();
            matcher1.appendReplacement(buffer1, "_x005F");
            buffer1.append(result.group());
        }
        matcher1.appendTail(buffer1);

        // replace ASCII control characters with _xhhhh_ sequences
        final StringBuffer buffer2 = new StringBuffer();
        final Matcher matcher2 = ENCODE_PATTERN_2.matcher(buffer1);
        while (matcher2.find()) {
            final MatchResult result = matcher2.toMatchResult();
            final int code = result.group().codePointAt(0);
            matcher2.appendReplacement(buffer2, "_x");
            buffer2.append(String.format("%04x", code));
            buffer2.append('_');
        }
        matcher2.appendTail(buffer2);

        return buffer2.toString();
    }

    /**
     * Decodes a list of strings that was stored in an SpreadsheetML part.
     *
     * @param value
     *  The list of strings to be decoded.
     *
     * @return
     *  The decoded string list.
     */
    public final static List<String> decode(final List<String> list) {

        // silently skip nulls
        if (list == null) return null;

        // create a new string list with decoded strings
        return list.stream().map(value -> StringCodec.decode(value)).collect(Collectors.toList());
    }

    /**
     * Encodes a list of strings to be stored in an SpreadsheetML part.
     *
     * @param value
     *  The list of strings to be encoded.
     *
     * @return
     *  The encoded string list.
     */
    public final static List<String> encode(final List<String> list) {

        // silently skip nulls
        if (list == null) return null;

        // create a new string list with encoded strings
        return list.stream().map(value -> StringCodec.encode(value)).collect(Collectors.toList());
    }
}
