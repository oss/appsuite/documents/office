/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.xlsx4j.jaxb;

import jakarta.xml.bind.JAXBContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Context {

    private static Thread jcThread;
    private static boolean jcThreadJoined = false;

    /*
     * Two reasons for having a separate class for this:
     * 1. so that loading SML context does not slow
     *    down docx4j operation on docx files
     * 2. to try to maintain clean delineation between
     *    docx4j and xlsx4j
     */

    private static JAXBContext jcSML;

    private static Logger log = LoggerFactory.getLogger(Context.class);

    static {
        jcThread = new Thread(
            new Runnable() {
                @Override
                public void run() {
                    try {
                        java.lang.ClassLoader classLoader = Context.class.getClassLoader();

                        jcSML = JAXBContext.newInstance("org.xlsx4j.sml:" +
                                   "org.xlsx4j.schemas.microsoft.com.office.excel_2006.main:" +
                                "org.xlsx4j.schemas.microsoft.com.office.excel_2008_2.main:" +
                                "org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2009_9.main:" +
                                "org.xlsx4j.schemas.microsoft.com.office.spreadsheetml_2010_11.main:" +
                                "org.xlsx4j.sml_2017:" +
                                "org.xlsx4j.sml_2018", classLoader );

                        if (jcSML.getClass().getName().equals("org.eclipse.persistence.jaxb.JAXBContext")) {
                            log.debug("MOXy JAXB implementation is in use!");
                        } else {
                            log.debug("Not using MOXy." + jcSML.getClass().getName());
                        }

                    } catch (Exception ex) {
                        log.error("Cannot initialize context", ex);
                    }
                }
            }
        );
        jcThread.start();
    }

    /*
     * the jcThread is joined, additional it is checked if it should
     * aborted because of low memory
     *
     */
    private static final void initializeContexts() {
        if(!jcThreadJoined) {
            try {
                jcThread.join();
            } catch (InterruptedException e) {
                log.info("XLSX4J, could not get Context");
            }
            jcThreadJoined = true;
        }
    }

    public static org.xlsx4j.sml.ObjectFactory smlObjectFactory;

    public static org.xlsx4j.sml.ObjectFactory getsmlObjectFactory() {
        if (smlObjectFactory==null) {
            smlObjectFactory = new org.xlsx4j.sml.ObjectFactory();
        }
        return smlObjectFactory;
    }

    public static org.xlsx4j.sml_2018.ObjectFactory sml2018ObjectFactory;

    public static org.xlsx4j.sml_2018.ObjectFactory sml2018ObjectFactory() {
        if (sml2018ObjectFactory==null) {
            sml2018ObjectFactory = new org.xlsx4j.sml_2018.ObjectFactory();
        }
        return sml2018ObjectFactory;
    }

    public static final JAXBContext getJcSML() {
        initializeContexts();
        return jcSML;
    }
}
