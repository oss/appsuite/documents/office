
package org.xlsx4j.sml_2018;

import jakarta.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.xlsx4j.schemas.microsoft.com.office.excel_2008_2.main package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.xlsx4j.schemas.microsoft.com.office.excel_2008_2.main
     * 
     */
    public ObjectFactory() {
    }

    public CTMention createCTCtMention() {
        return new CTMention();
    }
    
    public CTThreadedComment createCTThreadedComment() {
        return new CTThreadedComment();
    }
    
    public CTThreadedCommentMentions createCTThreadedCommentMentions() {
        return new CTThreadedCommentMentions();
    }
    
    public CTThreadedComments createCTThreadedComments() {
        return new CTThreadedComments();
    }
    
    public CTPerson createCTPerson() {
        return new CTPerson();
    }
    
    public CTPersonList createCTPersonList() {
        return new CTPersonList();
    }
}
