
package org.xlsx4j.sml_2018;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;

/**
 *
 * This complex type specifies a collection of persons.
 *
 * <xsd:complexType name="CT_PersonList">
 *  <xsd:sequence>
 *   <xsd:element name="person" type="CT_Person" minOccurs="0" maxOccurs="unbounded"/>
 *   <xsd:element name="extLst" type="x:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
 *  </xsd:sequence>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CTPersonList", propOrder = {})
@XmlRootElement(name="personList")
public class CTPersonList {

    @XmlElement
    protected CTExtensionList extLst;

    @XmlElement(name = "person")
    protected List<CTPerson> persons;

    public void setPersons(List<CTPerson> persons) {
        this.persons = persons;
    }

    public List<CTPerson> getPersons() {
        if (persons == null) {
            persons = new ArrayList<>();
        }
        return persons;
    }

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList extLst) {
        this.extLst = extLst;
    }

    public CTPerson getPersonById(String id) {

        if (persons != null && id != null && !id.isEmpty()) {
            for (CTPerson p : persons) {
                if (id.equals(p.getId())) {
                    return p;
                }
            }
        }

        return null;
    }

    public CTPerson getPerson(String displayName, String userId, String providerId) {
        CTPerson person = null;
        if (persons != null && displayName != null && !displayName.isEmpty() && userId != null) {
            for (CTPerson p : persons) {
                if (displayName.equals(p.getDisplayName()) && userId.equals(p.getUserId()) && ((providerId == null && p.getProviderId() == null) || (providerId != null && providerId.equals(p.getProviderId())))) {
                    person = p;
                }
            }
        }
        return person;
    }
}
