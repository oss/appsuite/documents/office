
package org.xlsx4j.sml_2018;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This complex type specifies a collection of mentions in a threaded comment.
 * 
 * <xsd:complexType name="CT_ThreadedCommentMentions">
 *  <xsd:sequence>
 *   <xsd:element name="mention" type="CT_Mention" minOccurs="0" maxOccurs="unbounded"/>
 *  </xsd:sequence>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ThreadedCommentMentions", propOrder = {})
@XmlRootElement(name="threadedCommentMentions")
public class CTThreadedCommentMentions {

    @XmlElement(name = "mention")
    protected List<CTMention> mentions;
    
    public List<CTMention> getMentions() {
        if (mentions == null) {
            mentions = new ArrayList<>();
        }
        return mentions;
    }
    
    public void addMention(CTMention mention) {
        getMentions().add(mention);
    }
    
}
