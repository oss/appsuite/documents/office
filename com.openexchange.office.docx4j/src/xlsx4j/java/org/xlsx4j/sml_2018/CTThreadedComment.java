
package org.xlsx4j.sml_2018;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import org.xlsx4j.sml.CTExtensionList;
import org.xlsx4j.sml.CTXstringWhitespace;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.ICellRefAccess;

/**
 *
 * This complex type specifies the properties of a threaded comment.
 *
 * <xsd:complexType name="CT_ThreadedComment">
 *  <xsd:sequence>
 *    <xsd:element name="text" type="x:ST_Xstring" minOccurs="0" maxOccurs="1"/>
 *    <xsd:element name="mentions" type="CT_ThreadedCommentMentions" minOccurs="0" maxOccurs="1"/>
 *    <xsd:element name="extLst" type="x:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
 *  </xsd:sequence>
 *  <xsd:attribute name="ref" type="x:ST_Ref" use="optional"/> // optional is wrong. Excel crashes if the ref is missed
 *  <xsd:attribute name="dT" type="xsd:dateTime" use="optional"/>
 *  <xsd:attribute name="personId" type="x:ST_Guid" use="required"/>
 *  <xsd:attribute name="id" type="x:ST_Guid" use="required"/>
 *  <xsd:attribute name="parentId" type="x:ST_Guid" use="optional"/>
 *  <xsd:attribute name="done" type="xsd:boolean" use="optional"/>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ThreadedComment", propOrder = {})
@XmlRootElement(name="threadedComment")
public class CTThreadedComment implements ICellRefAccess {

    @XmlElement
    protected CTXstringWhitespace text;

    @XmlElement
    protected CTThreadedCommentMentions mentions;

    @XmlElement
    protected CTExtensionList extLst;

    // dt attribute that specifies the UTC time that the comment was authored for the first time.
    @XmlAttribute(name = "dT")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTime;

    // personId: : An ST_Guid attribute that specifies a unique identifier for the person mentioned. This attribute MUST correspond to the id specified in CT_Person
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String personId;

    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;

    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String parentId;

    @XmlAttribute
    @XmlSchemaType(name = "boolean")
    protected Boolean done;

    @XmlTransient
    protected CellRef cellRef;
    @XmlAttribute(name = "ref", required = true)
    protected String getRef() {
        if(cellRef==null) {
            return "";
        }
        return CellRef.getCellRef(cellRef);
    }

    /**
     * Sets the value of the ref property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    protected void setRef(String value) {
        cellRef = CellRef.createCellRef(value);
    }

    @Override
    public CellRef getCellRef(boolean forceCreate) {
        if(cellRef==null&&forceCreate) {
            cellRef = new CellRef(0, 0);
        }
        return cellRef;
    }

    @Override
    public void setCellRef(CellRef cellRef) {
        this.cellRef = cellRef;
    }

    public CTExtensionList getExtLst() {
        return extLst;
    }

    public void setExtLst(CTExtensionList extLst) {
        this.extLst = extLst;
    }

    public String getText() {
        return text == null ? null : text.getValue();
    }

    public void setText(String text) {
        if(text==null) {
            this.text = null;
        }
        this.text = new CTXstringWhitespace(text, true);
    }

    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }


    public void setDateTime(XMLGregorianCalendar dateTime) {
        this.dateTime = dateTime;
    }


    public String getPersonId() {
        return personId;
    }


    public void setPersonId(String personId) {
        this.personId = personId;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getParentId() {
        return parentId;
    }


    public void setParentId(String parentId) {
        this.parentId = parentId;
    }


    public Boolean getDone() {
        return done;
    }


    public void setDone(Boolean done) {
        this.done = done;
    }


    public CTThreadedCommentMentions getMentions() {
        return mentions;
    }


    public void setMentions(CTThreadedCommentMentions mentions) {
        this.mentions = mentions;
    }
}
