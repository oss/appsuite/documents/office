
package org.xlsx4j.sml_2018;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xlsx4j.sml.CTExtensionList;

/**
 * 
 * This complex type specifies the information about an author of a comment or a person mentioned in a comment.
 * 
 * <xsd:complexType name="CT_Person">
 *  <xsd:sequence>
 *    <xsd:element name="extLst" type="x:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
 *  </xsd:sequence>
 *  <xsd:attribute name="displayName" type="x:ST_Xstring" use="required"/>
 *  <xsd:attribute name="id" type="x:ST_Guid" use="required"/>
 *  <xsd:attribute name="userId" type="x:ST_Xstring" use="optional"/>
 *  <xsd:attribute name="providerId" type="x:ST_Xstring" use="optional"/>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CTPerson", propOrder = {})
@XmlRootElement(name="person")
public class CTPerson {
    
    @XmlElement
    protected CTExtensionList extLst;
    
    @XmlAttribute(required = true)
    protected String displayName;
    
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;    
    
    @XmlAttribute
    protected String userId;    
    
    @XmlAttribute
    protected String providerId;        
    
    
    public CTExtensionList getExtLst() {
        return extLst;
    }

    
    public void setExtLst(CTExtensionList extLst) {
        this.extLst = extLst;
    }

    
    public String getId() {
        return id;
    }

    
    public void setId(String id) {
        this.id = id;
    }

    
    public String getDisplayName() {
        return displayName;
    }

    
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    
    public String getUserId() {
        return userId;
    }

    
    public void setUserId(String userId) {
        this.userId = userId;
    }

    
    public String getProviderId() {
        return providerId;
    }

    
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
