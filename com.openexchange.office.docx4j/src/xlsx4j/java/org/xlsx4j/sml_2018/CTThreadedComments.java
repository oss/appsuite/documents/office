
package org.xlsx4j.sml_2018;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.xlsx4j.sml.CTExtensionList;

/**
 * This complex type specifies the properties of a threaded comment.
 * 
 * <xsd:complexType name="CT_ThreadedComments">
 *  <xsd:sequence>
 *   <xsd:element name="threadedComment" type="CT_ThreadedComment" minOccurs="0" maxOccurs="unbounded"/>
 *   <xsd:element name="extLst" type="x:CT_ExtensionList" minOccurs="0" maxOccurs="1"/>
 *  </xsd:sequence>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ThreadedComments", propOrder = {})
@XmlRootElement(name="ThreadedComments")
public class CTThreadedComments {

    @XmlElement(name = "threadedComment")
    protected List<CTThreadedComment> threadedComments;

    @XmlElement
    protected CTExtensionList extLst;
    
    public List<CTThreadedComment> getThreadedComments() {
        if (threadedComments == null) {
            threadedComments = new ArrayList<>();
        }
        return threadedComments;
    }
    
    public void setThreadedComments(List<CTThreadedComment> threadedComments) {
        this.threadedComments = threadedComments;
    }
        
    public CTExtensionList getExtLst() {
        return extLst;
    }
    
    public void setExtLst(CTExtensionList extLst) {
        this.extLst = extLst;
    }
}
