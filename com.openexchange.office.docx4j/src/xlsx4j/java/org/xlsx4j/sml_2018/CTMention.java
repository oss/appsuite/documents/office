
package org.xlsx4j.sml_2018;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * This complex type specifies the properties of a mention in a threaded comment.
 *
 * <xsd:complexType name="CT_Mention">
 *  <xsd:attribute name="mentionpersonId" type="x:ST_Guid" use="required"/>
 *  <xsd:attribute name="mentionId" type="x:ST_Guid" use="required"/>
 *  <xsd:attribute name="startIndex" type="xsd:unsignedInt" use="required"/>
 *  <xsd:attribute name="length" type="xsd:unsignedInt" use="required"/>
 * </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CTMention", propOrder = {})
@XmlRootElement(name="mention")
public class CTMention {

    // mentionpersonId: An ST_Guid attribute that specifies a unique identifier for the person mentioned. This attribute MUST correspond to the id specified in CT_Person (2018)
    @XmlAttribute(required = true, name = "mentionpersonId")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String personId;

    @XmlAttribute(required = true, name = "mentionId")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;

    @XmlAttribute(required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long startIndex;

    @XmlAttribute(required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long length;

    /**
     * Get the PersonId correspond to the sml._2018.CTPerson.java.
     * (Required)
     * @return the id of the person.
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * Set the PersonId correspond to the sml._2018.CTPerson.java.
     * (Required)
     * @param personId
     */
    public void setPersonId(String personId) {
        this.personId = personId;
    }

    /**
     * Get the id of the mention.
     * (Required)
     */
    public String getId() {
        return id;
    }

    /**
     * Set the id of the mention.
     * (Required)
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get the start index of the mention start in the comment text.
     * (Required)
     */
    public long getStartIndex() {
        return startIndex;
    }

    /**
     * Set the start index of the mention start in the comment text.
     * (Required)
     * @param startIndex
     */
    public void setStartIndex(long startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Get the number of characters in the mention from startIndex.
     * (Required)
     * @return
     */
    public long getLength() {
        return length;
    }

    /**
     * Set the number of characters in the mention from startIndex.
     * (Required)
     * @param length
     */
    public void setLength(long length) {
        this.length = length;
    }
}
