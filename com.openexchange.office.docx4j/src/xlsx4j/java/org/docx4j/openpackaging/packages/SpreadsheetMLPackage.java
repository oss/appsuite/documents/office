/*
 *  Copyright 2010, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.packages;

import jakarta.xml.bind.JAXBException;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsCustomPart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.relationships.Relationship;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.BookViews;
import org.xlsx4j.sml.CTBookView;
import org.xlsx4j.sml.Sheet;
import org.xlsx4j.sml.Sheets;
import org.xlsx4j.sml.Worksheet;

/**
 * @author jharrop
 *
 */
public class SpreadsheetMLPackage extends OpcPackage {

	/**
	 * Constructor.  Also creates a new content type manager
	 *
	 */
	public SpreadsheetMLPackage() {
		super();
		setContentType(new ContentType(ContentTypes.PRESENTATIONML_MAIN));
	}
	/**
	 * Constructor.
	 *
	 * @param contentTypeManager
	 *            The content type manager to use
	 */
	public SpreadsheetMLPackage(ContentTypeManager contentTypeManager) {
		super(contentTypeManager);
		setContentType(new ContentType(ContentTypes.PRESENTATIONML_MAIN));
	}

	// Workbook part
	WorkbookPart wb;
	public WorkbookPart getWorkbookPart() {
		return wb;
	}

	@Override
    public boolean setPartShortcut(Part part, String relationshipType) {
		if (relationshipType.equals(Namespaces.PROPERTIES_CORE)) {
			docPropsCorePart = (DocPropsCorePart)part;
			log.debug("Set shortcut for docPropsCorePart");
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_EXTENDED)) {
			docPropsExtendedPart = (DocPropsExtendedPart)part;
			log.debug("Set shortcut for docPropsExtendedPart");
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_CUSTOM)) {
			docPropsCustomPart = (DocPropsCustomPart)part;
			log.debug("Set shortcut for docPropsCustomPart");
			return true;
		} else if (relationshipType.equals(Namespaces.SPREADSHEETML_WORKBOOK)) {
			wb = (WorkbookPart)part;
			log.debug("Set shortcut for WorkbookPart");
			return true;
		} else {
			return false;
		}
	}



	/**
	 * Create an empty presentation.
	 *
	 * @return
	 * @throws InvalidFormatException
	 */
	public static SpreadsheetMLPackage createPackage() throws InvalidFormatException {


		// Create a package
		SpreadsheetMLPackage xlsPack = new SpreadsheetMLPackage();

		try {

			xlsPack.wb = new WorkbookPart();
			xlsPack.wb.setJaxbElement(
					Context.getsmlObjectFactory().createWorkbook()
			);
			xlsPack.addTargetPart(xlsPack.wb);

			/* Without the following, Excel 2010 might crash if you try to print
			 * (it seems to depend on the content of your sheet).
			 *
			 * <bookViews>
				    <workbookView />
				  </bookViews>
			 */
			BookViews bookview = Context.getsmlObjectFactory().createBookViews();
			CTBookView ctBookview = Context.getsmlObjectFactory().createCTBookView();
			bookview.getWorkbookView().add(ctBookview);
			xlsPack.wb.getJaxbElement().setBookViews(bookview);

			xlsPack.wb.getJaxbElement().setSheets(
					Context.getsmlObjectFactory().createSheets()
			);

		} catch (Exception e) {
			throw new InvalidFormatException("Couldn't create package", e);
		}

		// Return the new package
		return xlsPack;

	}

	/**
	 * Create a worksheet and add it to the package
	 *
	 * @param wb
	 * @param _partName
	 * @param sheetName
	 * @param sheetId
	 * @return
	 * @throws InvalidFormatException
	 * @throws JAXBException
	 */
	public WorksheetPart createWorksheetPart(PartName _partName, String sheetName, long sheetId)
	    throws InvalidFormatException {

		WorksheetPart worksheetPart = new WorksheetPart(_partName);

		Relationship r = wb.addTargetPart(worksheetPart);

		Sheets sheets = wb.getJaxbElement().getSheets();

		Sheet s = Context.getsmlObjectFactory().createSheet();
		s.setName(sheetName);
		s.setId(r.getId());
		s.setSheetId(sheetId);

		sheets.getContent().add(s);

		// minimal content for the part
		Worksheet ws = Context.getsmlObjectFactory().createWorksheet();
		worksheetPart.setJaxbElement(ws);
		ws.setSheetData(Context.getsmlObjectFactory().createSheetData());

		return worksheetPart;
	}
}
