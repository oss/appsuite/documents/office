
package org.docx4j.openpackaging.parts.SpreadsheetML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.xlsx4j.sml_2018.CTThreadedComments;

public class ThreadedCommentsPart  extends JaxbSmlPart<CTThreadedComments> {
	
	public ThreadedCommentsPart(PartName partName) {
		super(partName);
		init();
	}

	public ThreadedCommentsPart() throws InvalidFormatException {
		super(new PartName("/xl/threadedComments/threadedComment1.xml"));
		init();
	}
	
	public void init() {
				
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.SPREADSHEETML_THREADED_COMMENTS));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.SPREADSHEETML_THREADED_COMMENTS);
		
	}

}