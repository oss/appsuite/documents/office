
package org.docx4j.openpackaging.parts.SpreadsheetML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.xlsx4j.jaxb.Context;

public abstract class JaxbSmlPart<E> extends JaxbXmlPart<E> {

	public JaxbSmlPart(PartName partName) {
		super(partName);
		setJAXBContext(Context.getJcSML());
	}

	public JaxbSmlPart() throws InvalidFormatException {
		super(new PartName("/xl/blagh.xml"));
		setJAXBContext(Context.getJcSML());
	}
}
