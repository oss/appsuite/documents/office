
package org.docx4j.openpackaging.parts.SpreadsheetML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.xlsx4j.sml_2018.CTPersonList;

public class PersonsPart  extends JaxbSmlPart<CTPersonList> {
	
	public PersonsPart(PartName partName) {
		super(partName);
		init();
	}

	public PersonsPart() throws InvalidFormatException {
		super(new PartName("/xl/persons/person.xml"));
		init();
	}
	
	public void init() {
				
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.SPREADSHEETML_PERSONS));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.SPREADSHEET_PERSONS);
		
	}

}