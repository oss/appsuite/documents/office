
package org.docx4j.openpackaging.parts.SpreadsheetML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart.AddPartBehaviour;
import org.docx4j.relationships.Relationship;
import org.docx4j.vml.root.Xml;
import org.xlsx4j.sml.CTAuthors;
import org.xlsx4j.sml.CTCommentList;
import org.xlsx4j.sml.CTComments;
import org.xlsx4j.sml.CTLegacyDrawing;
import org.xlsx4j.sml.Worksheet;
import org.xlsx4j.sml_2018.CTThreadedComments;

public class WorksheetPart extends JaxbSmlPart<Worksheet> {

	public WorksheetPart(PartName partName) {
		super(partName);
		init();
	}

	public WorksheetPart() throws InvalidFormatException {
		super(new PartName("/xl/worksheets/sheet1.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.SPREADSHEETML_WORKSHEET));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.SPREADSHEETML_WORKSHEET);
	}

	final public SpreadsheetCommentsPart getCommentsPart(boolean forceCreate) throws InvalidFormatException {
        SpreadsheetCommentsPart commentsPart = (SpreadsheetCommentsPart)getRelationshipsPart().getPartByType(Namespaces.COMMENTS);
        if(commentsPart==null&&forceCreate) {
            commentsPart = new SpreadsheetCommentsPart();
            final CTComments c = new CTComments();
            c.setCommentList(new CTCommentList());
            c.setAuthors(new CTAuthors());
            commentsPart.setJaxbElement(c);
            addTargetPart(commentsPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
        }
        return commentsPart;
	}

    final public CTComments getComments(boolean forceCreate) throws InvalidFormatException {
        final SpreadsheetCommentsPart commentsPart = getCommentsPart(forceCreate);
        return commentsPart!=null ? commentsPart.getJaxbElement() : null;
    }

    final public VMLPart getVMLPart(boolean forceCreate) throws InvalidFormatException {
	    VMLPart vmlPart = null;
	    final Worksheet worksheet = getJaxbElement();
	    CTLegacyDrawing legacyDrawing = worksheet.getLegacyDrawing(false);
	    if(legacyDrawing==null) {
	        if(forceCreate) {
    	        vmlPart = new VMLPart();
    	        vmlPart.setJaxbElement(new Xml());
    	        final Relationship relationship = addTargetPart(vmlPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS, null);
    	        legacyDrawing = new CTLegacyDrawing();
    	        worksheet.setLegacyDrawing(legacyDrawing);
    	        legacyDrawing.setId(relationship.getId());
	        }
	    }
	    else {
	        vmlPart = (VMLPart)getRelationshipsPart().getPart(legacyDrawing.getId());
	    }
        return vmlPart;
    }
    
    final public ThreadedCommentsPart getThreadedCommentsPart(boolean forceCreate) throws InvalidFormatException {
        ThreadedCommentsPart threadedCommentsPart = (ThreadedCommentsPart)getRelationshipsPart().getPartByType(Namespaces.SPREADSHEETML_THREADED_COMMENTS);
        
        if (threadedCommentsPart == null && forceCreate) {
            threadedCommentsPart = new ThreadedCommentsPart();
            final CTThreadedComments ct =  new CTThreadedComments();
            threadedCommentsPart.setJaxbElement(ct);
            addTargetPart(threadedCommentsPart, AddPartBehaviour.RENAME_IF_NAME_EXISTS);
        }
        
        return threadedCommentsPart;
    }
    
    final public CTThreadedComments getThreadedComments(boolean forceCreate) throws InvalidFormatException {
        final ThreadedCommentsPart threadedCommentsPart = getThreadedCommentsPart(forceCreate);
        return threadedCommentsPart == null ? null : threadedCommentsPart.getJaxbElement();
    }
    
    final public void deleteThreadedCommentsPartIfEmpty() throws InvalidFormatException {
        CTThreadedComments threadedComments = getThreadedComments(false);
        if (threadedComments != null && threadedComments.getThreadedComments().isEmpty()) {
            getRelationshipsPart().removeRelationshipByType(Namespaces.SPREADSHEETML_THREADED_COMMENTS);
        }
    }
}
