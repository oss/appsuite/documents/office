
package org.docx4j.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class HexNumberIntegerAdapter extends XmlAdapter<String, Integer> {

    @Override
    public Integer unmarshal(String v) throws Exception {
        if(v==null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(v, 16));
        }
        catch(NumberFormatException a) {
            return Integer.valueOf(0);
        }
    }

    @Override
    public String marshal(Integer v) throws Exception {
        if(v==null) {
            return null;
        }
        return Integer.toHexString(v).toUpperCase();
    }
}
