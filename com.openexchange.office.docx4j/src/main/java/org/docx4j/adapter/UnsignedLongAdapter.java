
package org.docx4j.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class UnsignedLongAdapter extends XmlAdapter<String, Long> {

    @Override
    public Long unmarshal(String v) throws Exception {
        if(v==null) {
            return null;
        }
        try {
            return Long.valueOf(Long.parseUnsignedLong(v));
        }
        catch(NumberFormatException a) {
            try {
                // we now try to load float values such as 5000.0
                final long l = Double.valueOf(Double.parseDouble(v)).longValue();
                if(l>=0) {
                    return Long.valueOf(l);
                }
            }
            catch(NumberFormatException b) {
                //
            }
            return Long.valueOf(0);
        }
    }

    @Override
    public String marshal(Long v) throws Exception {
        if(v==null) {
            return null;
        }
        final long quot = (v >>> 1) / 5;
        final String rem = Long.valueOf(v - quot * 10).toString();
        return quot!=0 ? Long.toString(quot) + rem : rem;
    }
}
