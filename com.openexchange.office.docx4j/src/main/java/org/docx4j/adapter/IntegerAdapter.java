
package org.docx4j.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class IntegerAdapter extends XmlAdapter<String, Integer> {

    @Override
    public Integer unmarshal(String v) throws Exception {
        if(v==null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(v));
        }
        catch(NumberFormatException a) {
            try {
                // we now try to load float values such as 5000.0
                return Integer.valueOf(Double.valueOf(Double.parseDouble(v)).intValue());
            }
            catch(NumberFormatException b) {
                return Integer.valueOf(0);
            }
        }
    }

    @Override
    public String marshal(Integer v) throws Exception {
        if(v==null) {
            return null;
        }
        return v.toString();
    }
}
