
package org.docx4j.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class LongAdapter extends XmlAdapter<String, Long> {

    @Override
    public Long unmarshal(String v) throws Exception {
        if(v==null) {
            return null;
        }
        try {
            return Long.valueOf(Long.parseLong(v));
        }
        catch(NumberFormatException a) {
            try {
                // we now try to load float values such as 5000.0
                return Long.valueOf(Double.valueOf(Double.parseDouble(v)).longValue());
            }
            catch(NumberFormatException b) {
                return Long.valueOf(0);
            }
        }
    }

    @Override
    public String marshal(Long v) throws Exception {
        if(v==null) {
            return null;
        }
        return v.toString();
    }
}
