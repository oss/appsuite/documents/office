
package org.docx4j.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class UnsignedIntegerAdapter extends XmlAdapter<String, Integer>  {

    @Override
    public Integer unmarshal(String v) throws Exception {
        if(v==null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseUnsignedInt(v));
        }
        catch(NumberFormatException a) {
            try {
                // we now try to load float values such as 5000.0
                final int l = Double.valueOf(Double.parseDouble(v)).intValue();
                if(l>=0) {
                    return Integer.valueOf(l);
                }
            }
            catch(NumberFormatException b) {
                //
            }
            return Integer.valueOf(0);
        }
    }

    @Override
    public String marshal(Integer v) throws Exception {
        if(v==null) {
            return null;
        }
        final int quot = (v >>> 1) / 5;
        final String rem = Integer.valueOf(v - quot * 10).toString();
        return quot!=0 ? Integer.toString(quot) + rem : rem;
    }
}
