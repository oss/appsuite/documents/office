/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.vml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.vml.officedrawing.CTCallout;
import org.docx4j.vml.officedrawing.CTClipPath;
import org.docx4j.vml.officedrawing.CTExtrusion;
import org.docx4j.vml.officedrawing.CTInk;
import org.docx4j.vml.officedrawing.CTSignatureLine;
import org.docx4j.vml.officedrawing.CTSkew;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.wordprocessingDrawing.CTAnchorLock;
import org.docx4j.vml.wordprocessingDrawing.CTWrap;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_Shape complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Shape">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;group ref="{urn:schemas-microsoft-com:vml}EG_ShapeElements"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:office:office}ink"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:office:powerpoint}iscomment"/>
 *       &lt;/choice>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_Type"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_AllCoreAttributes"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_Adj"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_Path"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_AllShapeAttributes"/>
 *       &lt;attribute ref="{urn:schemas-microsoft-com:office:office}gfxdata"/>
 *       &lt;attribute name="equationxml" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name="shape")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "urn:schemas-microsoft-com:vml", name = "CT_Shape", propOrder = {
    "egShapeElements",
    "textpath",
    "wrap",
    "fill",
    "stroke",
    "textbox",
    "pathElement",
    "imagedata",
    "lock",
    "clientData"
})
public class CTShape extends VmlShapeCore
{
    @XmlElementRefs({
        @XmlElementRef(name = "clippath", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "borderleft", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "shadow", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "signatureline", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "borderbottom", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "iscomment", namespace = "urn:schemas-microsoft-com:office:powerpoint", type = JAXBElement.class),
        @XmlElementRef(name = "ink", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "skew", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "handles", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "anchorlock", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "borderright", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "extrusion", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "formulas", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "bordertop", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "textdata", namespace = "urn:schemas-microsoft-com:office:powerpoint", type = JAXBElement.class),
        @XmlElementRef(name = "callout", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class)
    })
    protected List<Object> egShapeElements;
    @XmlAttribute(name = "gfxdata", namespace = "urn:schemas-microsoft-com:office:office")
    protected byte[] gfxdata;
    @XmlAttribute(name = "equationxml")
    protected String equationxml;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "adj")
    protected String adj;
    @XmlAttribute(name = "path")
    protected String path;

    /**
     * Gets the value of the pathOrFormulasOrHandles property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pathOrFormulasOrHandles property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEGShapeElements().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CTClientData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTClipPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTShadow }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSignatureLine }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTEmpty }{@code >}
     * {@link JAXBElement }{@code <}{@link CTWrap }{@code >}
     * {@link JAXBElement }{@code <}{@link CTInk }{@code >}
     * {@link JAXBElement }{@code <}{@link CTImageData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSkew }{@code >}
     * {@link JAXBElement }{@code <}{@link CTHandles }{@code >}
     * {@link JAXBElement }{@code <}{@link CTAnchorLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTExtrusion }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextbox }{@code >}
     * {@link JAXBElement }{@code <}{@link CTFormulas }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRel }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCallout }{@code >}
     *
     *
     */
    @Override
    public List<Object> getContent() {
        if (egShapeElements == null) {
            egShapeElements = new DLList<Object>();
        }
        return this.egShapeElements;
    }

    /**
     * Encoded Package
     *
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGfxdata() {
        return gfxdata;
    }

    /**
     * Sets the value of the gfxdata property.
     *
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGfxdata(byte[] value) {
        this.gfxdata = (value);
    }

    /**
     * Gets the value of the equationxml property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEquationxml() {
        return equationxml;
    }

    /**
     * Sets the value of the equationxml property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEquationxml(String value) {
        this.equationxml = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the adj property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAdj() {
        return adj;
    }

    /**
     * Sets the value of the adj property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAdj(String value) {
        this.adj = value;
    }

    /**
     * Gets the value of the path property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the value of the path property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPath(String value) {
        this.path = value;
    }
}
