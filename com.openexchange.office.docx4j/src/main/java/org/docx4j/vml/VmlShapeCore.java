
package org.docx4j.vml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.vml.officedrawing.STBWMode;
import org.docx4j.vml.officedrawing.STConnectorType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlTransient
public abstract class VmlShapeCore extends VmlCore {

    @XmlAttribute(name = "opacity")
    protected String opacity;
    @XmlAttribute(name = "stroked")
    protected org.docx4j.vml.STTrueFalse stroked;
    @XmlAttribute(name = "strokecolor")
    protected String strokecolor;
    @XmlAttribute(name = "strokeweight")
    protected String strokeweight;
    @XmlAttribute(name = "insetpen")
    protected org.docx4j.vml.STTrueFalse insetpen;
    @XmlAttribute(name = "chromakey")
    protected String chromakey;
    @XmlAttribute(name = "spt", namespace = "urn:schemas-microsoft-com:office:office")
    protected Integer spt;
    @XmlAttribute(name = "connectortype", namespace = "urn:schemas-microsoft-com:office:office")
    protected STConnectorType connectortype;
    @XmlAttribute(name = "bwmode", namespace = "urn:schemas-microsoft-com:office:office")
    protected STBWMode bwmode;
    @XmlAttribute(name = "bwpure", namespace = "urn:schemas-microsoft-com:office:office")
    protected STBWMode bwpure;
    @XmlAttribute(name = "bwnormal", namespace = "urn:schemas-microsoft-com:office:office")
    protected STBWMode bwnormal;
    @XmlAttribute(name = "forcedash", namespace = "urn:schemas-microsoft-com:office:office")
    protected org.docx4j.vml.officedrawing.STTrueFalse forcedash;
    @XmlAttribute(name = "oleicon", namespace = "urn:schemas-microsoft-com:office:office")
    protected org.docx4j.vml.officedrawing.STTrueFalse oleicon;
    @XmlAttribute(name = "ole", namespace = "urn:schemas-microsoft-com:office:office")
    protected String ole;
    @XmlAttribute(name = "preferrelative", namespace = "urn:schemas-microsoft-com:office:office")
    protected org.docx4j.vml.officedrawing.STTrueFalse preferrelative;
    @XmlAttribute(name = "cliptowrap", namespace = "urn:schemas-microsoft-com:office:office")
    protected org.docx4j.vml.officedrawing.STTrueFalse cliptowrap;
    @XmlAttribute(name = "clip", namespace = "urn:schemas-microsoft-com:office:office")
    protected org.docx4j.vml.officedrawing.STTrueFalse clip;

    /**
     * Gets the value of the opacity property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpacity() {
        return opacity;
    }

    /**
     * Sets the value of the opacity property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpacity(String value) {
        this.opacity = value;
    }

    /**
     * Gets the value of the stroked property.
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public org.docx4j.vml.STTrueFalse getStroked() {
        return stroked;
    }

    /**
     * Sets the value of the stroked property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public void setStroked(org.docx4j.vml.STTrueFalse value) {
        this.stroked = value;
    }

    /**
     * Gets the value of the strokecolor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStrokecolor() {
        return strokecolor;
    }

    /**
     * Sets the value of the strokecolor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStrokecolor(String value) {
        this.strokecolor = value;
    }

    /**
     * Gets the value of the strokeweight property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStrokeweight() {
        return strokeweight;
    }

    /**
     * Sets the value of the strokeweight property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStrokeweight(String value) {
        this.strokeweight = value;
    }

    /**
     * Gets the value of the insetpen property.
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public org.docx4j.vml.STTrueFalse getInsetpen() {
        return insetpen;
    }

    /**
     * Sets the value of the insetpen property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public void setInsetpen(org.docx4j.vml.STTrueFalse value) {
        this.insetpen = value;
    }

    /**
     * Gets the value of the chromakey property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getChromakey() {
        return chromakey;
    }

    /**
     * Sets the value of the chromakey property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setChromakey(String value) {
        this.chromakey = value;
    }

    /**
     * Optional Number
     *
     * @return
     *     possible object is
     *     {@link Float }
     *
     */
    public Integer getSpt() {
        return spt;
    }

    /**
     * Sets the value of the spt property.
     *
     * @param value
     *     allowed object is
     *     {@link Float }
     *
     */
    public void setSpt(Integer value) {
        this.spt = value;
    }

    /**
     * Shape Connector Type
     *
     * @return
     *     possible object is
     *     {@link STConnectorType }
     *
     */
    public STConnectorType getConnectortype() {
        if (connectortype == null) {
            return STConnectorType.STRAIGHT;
        }
        return connectortype;
    }

    /**
     * Sets the value of the connectortype property.
     *
     * @param value
     *     allowed object is
     *     {@link STConnectorType }
     *
     */
    public void setConnectortype(STConnectorType value) {
        this.connectortype = value;
    }

    /**
     * Black-and-White Mode
     *
     * @return
     *     possible object is
     *     {@link STBWMode }
     *
     */
    public STBWMode getBwmode() {
        return bwmode;
    }

    /**
     * Sets the value of the bwmode property.
     *
     * @param value
     *     allowed object is
     *     {@link STBWMode }
     *
     */
    public void setBwmode(STBWMode value) {
        this.bwmode = value;
    }

    /**
     * Pure Black-and-White Mode
     *
     * @return
     *     possible object is
     *     {@link STBWMode }
     *
     */
    public STBWMode getBwpure() {
        return bwpure;
    }

    /**
     * Sets the value of the bwpure property.
     *
     * @param value
     *     allowed object is
     *     {@link STBWMode }
     *
     */
    public void setBwpure(STBWMode value) {
        this.bwpure = value;
    }

    /**
     * Normal Black-and-White Mode
     *
     * @return
     *     possible object is
     *     {@link STBWMode }
     *
     */
    public STBWMode getBwnormal() {
        return bwnormal;
    }

    /**
     * Sets the value of the bwnormal property.
     *
     * @param value
     *     allowed object is
     *     {@link STBWMode }
     *
     */
    public void setBwnormal(STBWMode value) {
        this.bwnormal = value;
    }

    /**
     * Force Dashed Outline
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public org.docx4j.vml.officedrawing.STTrueFalse getForcedash() {
        return forcedash;
    }

    /**
     * Sets the value of the forcedash property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public void setForcedash(org.docx4j.vml.officedrawing.STTrueFalse value) {
        this.forcedash = value;
    }

    /**
     * Embedded Object Icon Toggle
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public org.docx4j.vml.officedrawing.STTrueFalse getOleicon() {
        return oleicon;
    }

    /**
     * Sets the value of the oleicon property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public void setOleicon(org.docx4j.vml.officedrawing.STTrueFalse value) {
        this.oleicon = value;
    }

    /**
     * Embedded Object Toggle
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOle() {
        return ole;
    }

    /**
     * Sets the value of the ole property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOle(String value) {
        this.ole = value;
    }

    /**
     * Relative Resize Toggle
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public org.docx4j.vml.officedrawing.STTrueFalse getPreferrelative() {
        return preferrelative;
    }

    /**
     * Sets the value of the preferrelative property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public void setPreferrelative(org.docx4j.vml.officedrawing.STTrueFalse value) {
        this.preferrelative = value;
    }

    /**
     * Clip to Wrapping Polygon
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public org.docx4j.vml.officedrawing.STTrueFalse getCliptowrap() {
        return cliptowrap;
    }

    /**
     * Sets the value of the cliptowrap property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public void setCliptowrap(org.docx4j.vml.officedrawing.STTrueFalse value) {
        this.cliptowrap = value;
    }

    /**
     * Clipping Toggle
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public org.docx4j.vml.officedrawing.STTrueFalse getClip() {
        return clip;
    }

    /**
     * Sets the value of the clip property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
     *
     */
    public void setClip(org.docx4j.vml.officedrawing.STTrueFalse value) {
        this.clip = value;
    }
}
