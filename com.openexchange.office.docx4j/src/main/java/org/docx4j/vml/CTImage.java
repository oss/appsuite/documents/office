/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.vml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.vml.officedrawing.CTCallout;
import org.docx4j.vml.officedrawing.CTClipPath;
import org.docx4j.vml.officedrawing.CTExtrusion;
import org.docx4j.vml.officedrawing.CTSignatureLine;
import org.docx4j.vml.officedrawing.CTSkew;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.wordprocessingDrawing.CTAnchorLock;
import org.docx4j.vml.wordprocessingDrawing.CTWrap;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_Image complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Image">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{urn:schemas-microsoft-com:vml}EG_ShapeElements" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_AllCoreAttributes"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_ImageAttributes"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_AllShapeAttributes"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name="image")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Image", propOrder = {
    "egShapeElements",
    "textpath",
    "wrap",
    "fill",
    "stroke",
    "textbox",
    "pathElement",
    "imagedata",
    "lock",
    "clientData"
})
public class CTImage extends VmlShapeCore
{
    @XmlElementRefs({
        @XmlElementRef(name = "clippath", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "handles", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "skew", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "anchorlock", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "borderright", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "extrusion", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "borderleft", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "formulas", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "shadow", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "bordertop", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "signatureline", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "borderbottom", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "textdata", namespace = "urn:schemas-microsoft-com:office:powerpoint", type = JAXBElement.class),
        @XmlElementRef(name = "callout", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class)
    })
    protected List<Object> egShapeElements;
    @XmlAttribute(name = "src")
    protected String src;
    @XmlAttribute(name = "cropleft")
    protected String cropleft;
    @XmlAttribute(name = "croptop")
    protected String croptop;
    @XmlAttribute(name = "cropright")
    protected String cropright;
    @XmlAttribute(name = "cropbottom")
    protected String cropbottom;
    @XmlAttribute(name = "gain")
    protected String gain;
    @XmlAttribute(name = "blacklevel")
    protected String blacklevel;
    @XmlAttribute(name = "gamma")
    protected String gamma;
    @XmlAttribute(name = "grayscale")
    protected org.docx4j.vml.STTrueFalse grayscale;
    @XmlAttribute(name = "bilevel")
    protected org.docx4j.vml.STTrueFalse bilevel;

    /**
     * Gets the value of the egShapeElements property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the egShapeElements property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEGShapeElements().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CTClientData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTClipPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTImageData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSkew }{@code >}
     * {@link JAXBElement }{@code <}{@link CTHandles }{@code >}
     * {@link JAXBElement }{@code <}{@link CTAnchorLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTExtrusion }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextbox }{@code >}
     * {@link JAXBElement }{@code <}{@link CTFormulas }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTShadow }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSignatureLine }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCallout }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRel }{@code >}
     * {@link JAXBElement }{@code <}{@link CTWrap }{@code >}
     *
     *
     */
    @Override
    public List<Object> getContent() {
        if (egShapeElements == null) {
            egShapeElements = new DLList<Object>();
        }
        return this.egShapeElements;
    }

    /**
     * Gets the value of the src property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSrc() {
        return src;
    }

    /**
     * Sets the value of the src property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSrc(String value) {
        this.src = value;
    }

    /**
     * Gets the value of the cropleft property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCropleft() {
        return cropleft;
    }

    /**
     * Sets the value of the cropleft property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCropleft(String value) {
        this.cropleft = value;
    }

    /**
     * Gets the value of the croptop property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCroptop() {
        return croptop;
    }

    /**
     * Sets the value of the croptop property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCroptop(String value) {
        this.croptop = value;
    }

    /**
     * Gets the value of the cropright property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCropright() {
        return cropright;
    }

    /**
     * Sets the value of the cropright property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCropright(String value) {
        this.cropright = value;
    }

    /**
     * Gets the value of the cropbottom property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCropbottom() {
        return cropbottom;
    }

    /**
     * Sets the value of the cropbottom property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCropbottom(String value) {
        this.cropbottom = value;
    }

    /**
     * Gets the value of the gain property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGain() {
        return gain;
    }

    /**
     * Sets the value of the gain property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGain(String value) {
        this.gain = value;
    }

    /**
     * Gets the value of the blacklevel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBlacklevel() {
        return blacklevel;
    }

    /**
     * Sets the value of the blacklevel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBlacklevel(String value) {
        this.blacklevel = value;
    }

    /**
     * Gets the value of the gamma property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGamma() {
        return gamma;
    }

    /**
     * Sets the value of the gamma property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGamma(String value) {
        this.gamma = value;
    }

    /**
     * Gets the value of the grayscale property.
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public org.docx4j.vml.STTrueFalse getGrayscale() {
        return grayscale;
    }

    /**
     * Sets the value of the grayscale property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public void setGrayscale(org.docx4j.vml.STTrueFalse value) {
        this.grayscale = value;
    }

    /**
     * Gets the value of the bilevel property.
     *
     * @return
     *     possible object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public org.docx4j.vml.STTrueFalse getBilevel() {
        return bilevel;
    }

    /**
     * Sets the value of the bilevel property.
     *
     * @param value
     *     allowed object is
     *     {@link org.docx4j.vml.STTrueFalse }
     *
     */
    public void setBilevel(org.docx4j.vml.STTrueFalse value) {
        this.bilevel = value;
    }
}
