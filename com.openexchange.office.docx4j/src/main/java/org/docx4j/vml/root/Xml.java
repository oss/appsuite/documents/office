/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.vml.root;

import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.vml.CTArc;
import org.docx4j.vml.CTCurve;
import org.docx4j.vml.CTGroup;
import org.docx4j.vml.CTImage;
import org.docx4j.vml.CTLine;
import org.docx4j.vml.CTOval;
import org.docx4j.vml.CTPolyLine;
import org.docx4j.vml.CTRect;
import org.docx4j.vml.CTRoundRect;
import org.docx4j.vml.CTShape;
import org.docx4j.vml.CTShapetype;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;any namespace='urn:schemas-microsoft-com:vml' maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "xml")
public class Xml implements IContentAccessor<Object> {

    @XmlAnyElement(lax = true)
    @XmlElementRefs({
        @XmlElementRef(name="group", namespace="urn:schemas-microsoft-com:vml", type=CTGroup.class),
        @XmlElementRef(name="arc", namespace="urn:schemas-microsoft-com:vml", type=CTArc.class),
        @XmlElementRef(name="curve", namespace="urn:schemas-microsoft-com:vml", type=CTCurve.class),
        @XmlElementRef(name="image", namespace="urn:schemas-microsoft-com:vml", type=CTImage.class),
        @XmlElementRef(name="line", namespace="urn:schemas-microsoft-com:vml", type=CTLine.class),
        @XmlElementRef(name="oval", namespace="urn:schemas-microsoft-com:vml", type=CTOval.class),
        @XmlElementRef(name="polyline", namespace="urn:schemas-microsoft-com:vml", type=CTPolyLine.class),
        @XmlElementRef(name="rect", namespace="urn:schemas-microsoft-com:vml", type=CTRect.class),
        @XmlElementRef(name="roundrect", namespace="urn:schemas-microsoft-com:vml", type=CTRoundRect.class),
        @XmlElementRef(name="shape", namespace="urn:schemas-microsoft-com:vml", type=CTShape.class),
        @XmlElementRef(name="shapetype", namespace="urn:schemas-microsoft-com:vml", type=CTShapetype.class)
    })
    protected DLList<Object> content;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    @Override
    public List<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        return content;
    }
}
