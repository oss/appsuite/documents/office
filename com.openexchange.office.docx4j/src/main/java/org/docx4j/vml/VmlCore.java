
package org.docx4j.vml;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.jaxb.Context;
import org.docx4j.vml.officedrawing.CTLock;
import org.docx4j.vml.officedrawing.STHrAlign;
import org.docx4j.vml.officedrawing.STInsetMode;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.wordprocessingDrawing.CTWrap;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.component.Child;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlTransient
public abstract class VmlCore implements Child, IContentAccessor<Object> {

	@XmlAttribute(name = "href")
	protected String href;
	@XmlAttribute(name = "target")
	protected String target;
	@XmlAttribute(name = "class")
	protected String clazz;
	@XmlAttribute(name = "title")
	protected String title;
	@XmlAttribute(name = "alt")
	protected String alt;
	@XmlAttribute(name = "coordsize")
	protected String coordsize;
	@XmlAttribute(name = "coordorigin")
	protected String coordorigin;
	@XmlAttribute(name = "wrapcoords")
	protected String wrapcoords;
	@XmlAttribute(name = "print")
	protected org.docx4j.vml.STTrueFalse print;
	@XmlAttribute(name = "id")
	protected String vmlId;
	@XmlAttribute(name = "spid", namespace = "urn:schemas-microsoft-com:office:office")
	protected String spid;
	@XmlAttribute(name = "oned", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse oned;
	@XmlAttribute(name = "regroupid", namespace = "urn:schemas-microsoft-com:office:office")
	protected BigInteger regroupid;
	@XmlAttribute(name = "doubleclicknotify", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse doubleclicknotify;
	@XmlAttribute(name = "button", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse button;
	@XmlAttribute(name = "userhidden", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse userhidden;
	@XmlAttribute(name = "bullet", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse bullet;
	@XmlAttribute(name = "hr", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse hr;
	@XmlAttribute(name = "hrstd", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse hrstd;
	@XmlAttribute(name = "hrnoshade", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse hrnoshade;
	@XmlAttribute(name = "hrpct", namespace = "urn:schemas-microsoft-com:office:office")
	protected Float hrpct;
	@XmlAttribute(name = "hralign", namespace = "urn:schemas-microsoft-com:office:office")
	protected STHrAlign hralign;
	@XmlAttribute(name = "allowincell", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse allowincell;
	@XmlAttribute(name = "allowoverlap", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse allowoverlap;
	@XmlAttribute(name = "userdrawn", namespace = "urn:schemas-microsoft-com:office:office")
	protected org.docx4j.vml.officedrawing.STTrueFalse userdrawn;
	@XmlAttribute(name = "bordertopcolor", namespace = "urn:schemas-microsoft-com:office:office")
	protected String bordertopcolor;
	@XmlAttribute(name = "borderleftcolor", namespace = "urn:schemas-microsoft-com:office:office")
	protected String borderleftcolor;
	@XmlAttribute(name = "borderbottomcolor", namespace = "urn:schemas-microsoft-com:office:office")
	protected String borderbottomcolor;
	@XmlAttribute(name = "borderrightcolor", namespace = "urn:schemas-microsoft-com:office:office")
	protected String borderrightcolor;
	@XmlAttribute(name = "dgmlayout", namespace = "urn:schemas-microsoft-com:office:office")
	protected BigInteger dgmlayout;
	@XmlAttribute(name = "dgmnodekind", namespace = "urn:schemas-microsoft-com:office:office")
	protected BigInteger dgmnodekind;
	@XmlAttribute(name = "dgmlayoutmru", namespace = "urn:schemas-microsoft-com:office:office")
	protected BigInteger dgmlayoutmru;
	@XmlAttribute(name = "insetmode", namespace = "urn:schemas-microsoft-com:office:office")
	protected STInsetMode insetmode;
	@XmlAttribute(name = "filled")
	protected org.docx4j.vml.STTrueFalse filled;
	@XmlAttribute(name = "fillcolor")
	protected String fillcolor;
	@XmlTransient
    private Object parent;
	@XmlTransient
	private String style;
	@XmlTransient
	private HashMap<String, String> styleMap;
    @Override
    public abstract List<Object> getContent();
    @XmlElement(name = "stroke", namespace = "urn:schemas-microsoft-com:vml")
    protected CTStroke stroke;
    @XmlElement(name = "textpath", namespace = "urn:schemas-microsoft-com:vml")
    protected CTTextPath textpath;
    @XmlElement(name = "fill", namespace = "urn:schemas-microsoft-com:vml")
    protected CTFill fill;
    @XmlElement(name = "textbox", namespace = "urn:schemas-microsoft-com:vml")
    protected CTTextbox textbox;
    @XmlElement(name = "wrap", namespace = "urn:schemas-microsoft-com:office:word")
    protected CTWrap wrap;
    @XmlElement(name = "path", namespace = "urn:schemas-microsoft-com:vml")
    protected CTPath pathElement;
    @XmlElement(name = "imagedata", namespace = "urn:schemas-microsoft-com:vml")
    protected CTImageData imagedata;
    @XmlElement(name = "lock", namespace = "urn:schemas-microsoft-com:office:office")
    protected CTLock lock;
    @XmlElement(name = "ClientData", namespace = "urn:schemas-microsoft-com:office:excel")
    protected CTClientData clientData;

   /**
    * Gets the value of the href property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getHref() {
       return href;
   }

   /**
    * Sets the value of the href property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setHref(String value) {
       this.href = value;
   }

   /**
    * Gets the value of the target property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getTarget() {
       return target;
   }

   /**
    * Sets the value of the target property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setTarget(String value) {
       this.target = value;
   }

   /**
    * Gets the value of the clazz property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getClazz() {
       return clazz;
   }

   /**
    * Sets the value of the clazz property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setClazz(String value) {
       this.clazz = value;
   }

   /**
    * Gets the value of the title property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getTitle() {
       return title;
   }

   /**
    * Sets the value of the title property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setTitle(String value) {
       this.title = value;
   }

   /**
    * Gets the value of the alt property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getAlt() {
       return alt;
   }

   /**
    * Sets the value of the alt property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setAlt(String value) {
       this.alt = value;
   }

   /**
    * Gets the value of the coordsize property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getCoordsize() {
       return coordsize;
   }

   /**
    * Sets the value of the coordsize property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setCoordsize(String value) {
       this.coordsize = value;
   }

   /**
    * Gets the value of the coordorigin property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getCoordorigin() {
       return coordorigin;
   }

   /**
    * Sets the value of the coordorigin property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setCoordorigin(String value) {
       this.coordorigin = value;
   }

   /**
    * Gets the value of the wrapcoords property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getWrapcoords() {
       return wrapcoords;
   }

   /**
    * Sets the value of the wrapcoords property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setWrapcoords(String value) {
       this.wrapcoords = value;
   }

   /**
    * Gets the value of the print property.
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.STTrueFalse }
    *
    */
   public org.docx4j.vml.STTrueFalse getPrint() {
       return print;
   }

   /**
    * Sets the value of the print property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.STTrueFalse }
    *
    */
   public void setPrint(org.docx4j.vml.STTrueFalse value) {
       this.print = value;
   }

   /**
    * Gets the value of the style property.
    *
    * It is appreciated to use getStyleMap only as this.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   @XmlAttribute(name = "style")
   public String getStyle() {
	   if(styleMap!=null) {
	       style = getStyleFromStyleMap(styleMap);
	   }
       return style;
   }

   /**
    * Sets the value of the style property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setStyle(String value) {
       this.style = value;
       styleMap = null;
   }

   public HashMap<String, String> getStyleMap() {
	   if(styleMap==null) {
	       styleMap = getStyleMap(style);
	   }
	   return styleMap;
   }

   /**
    * Gets the value of the vmlId property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getVmlId() {
       return vmlId;
   }

   /**
    * Sets the value of the vmlId property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setVmlId(String value) {
       this.vmlId = value;
   }

   /**
    * Optional String
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getSpid() {
       return spid;
   }

   /**
    * Sets the value of the spid property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setSpid(String value) {
       this.spid = value;
   }

   /**
    * Shape Handle Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getOned() {
       return oned;
   }

   /**
    * Sets the value of the oned property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setOned(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.oned = value;
   }

   /**
    * Regroup ID
    *
    * @return
    *     possible object is
    *     {@link BigInteger }
    *
    */
   public BigInteger getRegroupid() {
       return regroupid;
   }

   /**
    * Sets the value of the regroupid property.
    *
    * @param value
    *     allowed object is
    *     {@link BigInteger }
    *
    */
   public void setRegroupid(BigInteger value) {
       this.regroupid = value;
   }

   /**
    * Double-click Notification Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getDoubleclicknotify() {
       return doubleclicknotify;
   }

   /**
    * Sets the value of the doubleclicknotify property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setDoubleclicknotify(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.doubleclicknotify = value;
   }

   /**
    * Button Behavior Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getButton() {
       return button;
   }

   /**
    * Sets the value of the button property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setButton(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.button = value;
   }

   /**
    * Hide Script Anchors
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getUserhidden() {
       return userhidden;
   }

   /**
    * Sets the value of the userhidden property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setUserhidden(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.userhidden = value;
   }

   /**
    * Graphical Bullet
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getBullet() {
       return bullet;
   }

   /**
    * Sets the value of the bullet property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setBullet(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.bullet = value;
   }

   /**
    * Horizontal Rule Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getHr() {
       return hr;
   }

   /**
    * Sets the value of the hr property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setHr(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.hr = value;
   }

   /**
    * Horizontal Rule Standard Display Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getHrstd() {
       return hrstd;
   }

   /**
    * Sets the value of the hrstd property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setHrstd(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.hrstd = value;
   }

   /**
    * Horizontal Rule 3D Shading Toggle
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getHrnoshade() {
       return hrnoshade;
   }

   /**
    * Sets the value of the hrnoshade property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setHrnoshade(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.hrnoshade = value;
   }

   /**
    * Horizontal Rule Length Percentage
    *
    * @return
    *     possible object is
    *     {@link Float }
    *
    */
   public Float getHrpct() {
       return hrpct;
   }

   /**
    * Sets the value of the hrpct property.
    *
    * @param value
    *     allowed object is
    *     {@link Float }
    *
    */
   public void setHrpct(Float value) {
       this.hrpct = value;
   }

   /**
    * Horizontal Rule Alignment
    *
    * @return
    *     possible object is
    *     {@link STHrAlign }
    *
    */
   public STHrAlign getHralign() {
       if (hralign == null) {
           return STHrAlign.LEFT;
       }
       return hralign;
   }

   /**
    * Sets the value of the hralign property.
    *
    * @param value
    *     allowed object is
    *     {@link STHrAlign }
    *
    */
   public void setHralign(STHrAlign value) {
       this.hralign = value;
   }

   /**
    * Allow in Table Cell
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getAllowincell() {
       return allowincell;
   }

   /**
    * Sets the value of the allowincell property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setAllowincell(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.allowincell = value;
   }

   /**
    * Allow Shape Overlap
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getAllowoverlap() {
       return allowoverlap;
   }

   /**
    * Sets the value of the allowoverlap property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setAllowoverlap(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.allowoverlap = value;
   }

   /**
    * Exists In Master Slide
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public org.docx4j.vml.officedrawing.STTrueFalse getUserdrawn() {
       return userdrawn;
   }

   /**
    * Sets the value of the userdrawn property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.officedrawing.STTrueFalse }
    *
    */
   public void setUserdrawn(org.docx4j.vml.officedrawing.STTrueFalse value) {
       this.userdrawn = value;
   }

   /**
    * Border Top Color
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getBordertopcolor() {
       return bordertopcolor;
   }

   /**
    * Sets the value of the bordertopcolor property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setBordertopcolor(String value) {
       this.bordertopcolor = value;
   }

   /**
    * Border Left Color
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getBorderleftcolor() {
       return borderleftcolor;
   }

   /**
    * Sets the value of the borderleftcolor property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setBorderleftcolor(String value) {
       this.borderleftcolor = value;
   }

   /**
    * Bottom Border Color
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getBorderbottomcolor() {
       return borderbottomcolor;
   }

   /**
    * Sets the value of the borderbottomcolor property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setBorderbottomcolor(String value) {
       this.borderbottomcolor = value;
   }

   /**
    * Border Right Color
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getBorderrightcolor() {
       return borderrightcolor;
   }

   /**
    * Sets the value of the borderrightcolor property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setBorderrightcolor(String value) {
       this.borderrightcolor = value;
   }

   /**
    * Diagram Node Layout Identifier
    *
    * @return
    *     possible object is
    *     {@link BigInteger }
    *
    */
   public BigInteger getDgmlayout() {
       return dgmlayout;
   }

   /**
    * Sets the value of the dgmlayout property.
    *
    * @param value
    *     allowed object is
    *     {@link BigInteger }
    *
    */
   public void setDgmlayout(BigInteger value) {
       this.dgmlayout = value;
   }

   /**
    * Diagram Node Identifier
    *
    * @return
    *     possible object is
    *     {@link BigInteger }
    *
    */
   public BigInteger getDgmnodekind() {
       return dgmnodekind;
   }

   /**
    * Sets the value of the dgmnodekind property.
    *
    * @param value
    *     allowed object is
    *     {@link BigInteger }
    *
    */
   public void setDgmnodekind(BigInteger value) {
       this.dgmnodekind = value;
   }

   /**
    * Diagram Node Recent Layout Identifier
    *
    * @return
    *     possible object is
    *     {@link BigInteger }
    *
    */
   public BigInteger getDgmlayoutmru() {
       return dgmlayoutmru;
   }

   /**
    * Sets the value of the dgmlayoutmru property.
    *
    * @param value
    *     allowed object is
    *     {@link BigInteger }
    *
    */
   public void setDgmlayoutmru(BigInteger value) {
       this.dgmlayoutmru = value;
   }

   /**
    * Text Inset Mode
    *
    * @return
    *     possible object is
    *     {@link STInsetMode }
    *
    */
   public STInsetMode getInsetmode() {
       if (insetmode == null) {
           return STInsetMode.CUSTOM;
       }
       return insetmode;
   }

   /**
    * Sets the value of the insetmode property.
    *
    * @param value
    *     allowed object is
    *     {@link STInsetMode }
    *
    */
   public void setInsetmode(STInsetMode value) {
       this.insetmode = value;
   }

   /**
    * Gets the value of the filled property.
    *
    * @return
    *     possible object is
    *     {@link org.docx4j.vml.STTrueFalse }
    *
    */
   public org.docx4j.vml.STTrueFalse getFilled() {
       return filled;
   }

   /**
    * Sets the value of the filled property.
    *
    * @param value
    *     allowed object is
    *     {@link org.docx4j.vml.STTrueFalse }
    *
    */
   public void setFilled(org.docx4j.vml.STTrueFalse value) {
       this.filled = value;
   }

   /**
    * Gets the value of the fillcolor property.
    *
    * @return
    *     possible object is
    *     {@link String }
    *
    */
   public String getFillcolor() {
       return fillcolor;
   }

   /**
    * Sets the value of the fillcolor property.
    *
    * @param value
    *     allowed object is
    *     {@link String }
    *
    */
   public void setFillcolor(String value) {
       this.fillcolor = value;
   }

   public CTTextPath getTextPath(boolean forceCreate) {
       if(textpath==null&&forceCreate) {
           textpath = new CTTextPath();
       }
       return textpath;
   }

   public void setTextPath(CTTextPath textpath) {
       this.textpath = textpath;
   }

   public CTLock getLock(boolean forceCreate) {
       if(lock==null&&forceCreate) {
           lock = new CTLock();
       }
       return lock;
   }

   public void setLock(CTLock lock) {
       this.lock = lock;
   }

   public CTFill getFill(boolean forceCreate) {
       if(fill==null&&forceCreate) {
           fill = new CTFill();
       }
       return fill;
   }

   public void setFill(CTFill fill) {
       this.fill = fill;
   }

   public CTStroke getStroke(boolean forceCreate) {
       if(stroke==null&&forceCreate) {
           stroke = new CTStroke();
       }
       return stroke;
   }

   public void setLine(CTStroke stroke) {
       this.stroke = stroke;
   }

   public CTTextbox getTextBox(boolean forceCreate) {
       if(textbox==null&&forceCreate) {
           textbox = new CTTextbox();
       }
       return textbox;
   }

   public void setTextBox(CTTextbox textbox) {
       this.textbox = textbox;
   }

   public CTWrap getWrap(boolean forceCreate) {
       if(wrap==null&&forceCreate) {
           wrap = new CTWrap();
       }
       return wrap;
   }

   public void setWrap(CTWrap wrap) {
       this.wrap = wrap;
   }

   public CTPath getPathElement(boolean forceCreate) {
       if(pathElement==null&&forceCreate) {
           pathElement = new CTPath();
       }
       return pathElement;
   }

   public void setPathElement(CTPath pathElement) {
       this.pathElement = pathElement;
   }

   public CTImageData getImageData(boolean forceCreate) {
       if(imagedata==null&&forceCreate) {
           imagedata = new CTImageData();
       }
       return imagedata;
   }

   public void setImageData(CTImageData imagedata) {
       this.imagedata = imagedata;
   }

   // client data is having the excel namespace, so it should be used only in excel documents
   public CTClientData getClientData(boolean forceCreate) {
       if(clientData==null&&forceCreate) {
           clientData = new CTClientData();
       }
       return clientData;
   }

   public void setClientData(CTClientData clientData) {
       this.clientData = clientData;
   }

   /**
    * Gets the parent object in the object tree representing the unmarshalled xml document.
    *
    * @return
    *     The parent object.
    */
   @Override
   public Object getParent() {
	   return this.parent;
   }

   @Override
   public void setParent(Object parent) {
	   this.parent = parent;
   }

   final public void beforeMarshal(Marshaller marshaller) {
	   if(vmlId==null) {
		   vmlId = Context.getNextMarkupId(marshaller).toString();
	   }
   }

   /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
   public void afterUnmarshal(Unmarshaller unmarshaller, Object p) {
	   setParent(p);
       if(vmlId!=null) {
    	   try {
    		   Context.addMarkupId(unmarshaller, Integer.parseInt(vmlId));
    		   Context.getCustomShapePresets();
    	   }
    	   catch (NumberFormatException e) {
    		   // not an integer!, this Id is unimportant for us as we only
    		   // give away unique integer ids
    	   }
       }
   }

   public static HashMap<String, String> getStyleMap(String s) {
       HashMap<String, String> sm = new HashMap<String, String>();
       if(s!=null&&s.length()>0) {
           final String[] styleTokens = s.split(" *?; *?");
           for(String token : styleTokens) {
               int index = token.indexOf(":");
               if(index>0&&index<(token.length()+1)) {
                   String key = token.substring(0, index).trim().toLowerCase();
                   String value = token.substring(index+1).trim().toLowerCase();
                   if(key.length()>0&&value.length()>0) {
                       sm.put(key,  value);
                   }
               }
           }
       }
       return sm;
   }

   public static String getStyleFromStyleMap(HashMap<String, String> sm) {
       boolean first = true;
       final StringBuffer styleBuf = new StringBuffer();
       for(String key : sm.keySet()) {
           if(!first) {
               styleBuf.append(";");
           }
           first = false;
           styleBuf.append(key);
           styleBuf.append(":");
           styleBuf.append(sm.get(key));
       }
       return styleBuf.toString();
   }
}
