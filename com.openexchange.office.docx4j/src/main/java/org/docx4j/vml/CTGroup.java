/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.vml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.vml.officedrawing.CTCallout;
import org.docx4j.vml.officedrawing.CTClipPath;
import org.docx4j.vml.officedrawing.CTDiagram;
import org.docx4j.vml.officedrawing.CTExtrusion;
import org.docx4j.vml.officedrawing.CTSignatureLine;
import org.docx4j.vml.officedrawing.CTSkew;
import org.docx4j.vml.spreadsheetDrawing.CTClientData;
import org.docx4j.vml.wordprocessingDrawing.CTAnchorLock;
import org.docx4j.vml.wordprocessingDrawing.CTWrap;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_Group complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Group">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;group ref="{urn:schemas-microsoft-com:vml}EG_ShapeElements"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}group"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}shape"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}shapetype"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}arc"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}curve"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}image"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}line"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}oval"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}polyline"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}rect"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:vml}roundrect"/>
 *         &lt;element ref="{urn:schemas-microsoft-com:office:office}diagram"/>
 *       &lt;/choice>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_Fill"/>
 *       &lt;attGroup ref="{urn:schemas-microsoft-com:vml}AG_AllCoreAttributes"/>
 *       &lt;attribute name="editas" type="{urn:schemas-microsoft-com:vml}ST_EditAs" />
 *       &lt;attribute ref="{urn:schemas-microsoft-com:office:office}tableproperties"/>
 *       &lt;attribute ref="{urn:schemas-microsoft-com:office:office}tablelimits"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name="group")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Group", propOrder = {
    "egShapeElements",
    "textpath",
    "wrap",
    "fill",
    "stroke",
    "textbox",
    "pathElement",
    "imagedata",
    "lock",
    "clientData"
})
public class CTGroup extends VmlCore
{
    @XmlElementRefs({
        @XmlElementRef(name = "rect", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "clippath", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "curve", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "borderleft", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "polyline", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "shadow", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "shapetype", namespace = "urn:schemas-microsoft-com:vml", type = CTShapetype.class),
        @XmlElementRef(name = "signatureline", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "borderbottom", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "oval", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "image", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "group", namespace = "urn:schemas-microsoft-com:vml", type = CTGroup.class),
        @XmlElementRef(name = "handles", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "skew", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "anchorlock", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "borderright", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "line", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "extrusion", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "formulas", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "diagram", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class),
        @XmlElementRef(name = "roundrect", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "bordertop", namespace = "urn:schemas-microsoft-com:office:word", type = JAXBElement.class),
        @XmlElementRef(name = "arc", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "shape", namespace = "urn:schemas-microsoft-com:vml", type = JAXBElement.class),
        @XmlElementRef(name = "textdata", namespace = "urn:schemas-microsoft-com:office:powerpoint", type = JAXBElement.class),
        @XmlElementRef(name = "callout", namespace = "urn:schemas-microsoft-com:office:office", type = JAXBElement.class)
    })
    protected DLList<Object> egShapeElements;
    @XmlAttribute(name = "editas")
    protected STEditAs editas;
    @XmlAttribute(name = "tableproperties", namespace = "urn:schemas-microsoft-com:office:office")
    protected String tableproperties;
    @XmlAttribute(name = "tablelimits", namespace = "urn:schemas-microsoft-com:office:office")
    protected String tablelimits;

    /**
     * Gets the value of the pathOrFormulasOrHandles property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pathOrFormulasOrHandles property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPathOrFormulasOrHandles().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CTClientData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRect }{@code >}
     * {@link JAXBElement }{@code <}{@link CTClipPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCurve }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPolyLine }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTShadow }{@code >}
     * {@link JAXBElement }{@code <}{@link CTShapetype }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSignatureLine }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTOval }{@code >}
     * {@link JAXBElement }{@code <}{@link CTImage }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTWrap }{@code >}
     * {@link JAXBElement }{@code <}{@link CTImageData }{@code >}
     * {@link JAXBElement }{@code <}{@link CTGroup }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSkew }{@code >}
     * {@link JAXBElement }{@code <}{@link CTHandles }{@code >}
     * {@link JAXBElement }{@code <}{@link CTAnchorLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTLine }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTExtrusion }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTextbox }{@code >}
     * {@link JAXBElement }{@code <}{@link CTFormulas }{@code >}
     * {@link JAXBElement }{@code <}{@link CTDiagram }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBorder }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRoundRect }{@code >}
     * {@link JAXBElement }{@code <}{@link CTArc }{@code >}
     * {@link JAXBElement }{@code <}{@link CTShape }{@code >}
     * {@link JAXBElement }{@code <}{@link CTLock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCallout }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRel }{@code >}
     *
     *
     */
    @Override
    public List<Object> getContent() {
        if (egShapeElements == null) {
        	egShapeElements = new DLList<Object>();
        }
        return egShapeElements;
    }

    /**
     * Gets the value of the editas property.
     *
     * @return
     *     possible object is
     *     {@link STEditAs }
     *
     */
    public STEditAs getEditas() {
        return editas;
    }

    /**
     * Sets the value of the editas property.
     *
     * @param value
     *     allowed object is
     *     {@link STEditAs }
     *
     */
    public void setEditas(STEditAs value) {
        this.editas = value;
    }

    /**
     * Table Properties
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTableproperties() {
        return tableproperties;
    }

    /**
     * Sets the value of the tableproperties property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTableproperties(String value) {
        this.tableproperties = value;
    }

    /**
     * Table Row Height Limits
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTablelimits() {
        return tablelimits;
    }

    /**
     * Sets the value of the tablelimits property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTablelimits(String value) {
        this.tablelimits = value;
    }
}
