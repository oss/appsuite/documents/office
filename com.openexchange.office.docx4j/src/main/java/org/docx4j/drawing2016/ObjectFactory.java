/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.docx4j.drawing2016;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
 
    private final static QName _SVG_BLIP_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2016/SVG/main", "svgBlip");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.docx4j.drawing2016
     * 
     */
    public ObjectFactory() {
        //
    }

    /**
     * Create an instance of {@link CTSVGBlip }
     * 
     */
    public CTSVGBlip createCTSVGBlip() {
        return new CTSVGBlip();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSVGBlip }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2016/SVG/main", name = "svgBlip")
    public JAXBElement<CTSVGBlip> createSvgBlip(CTSVGBlip value) {
        return new JAXBElement<CTSVGBlip>(_SVG_BLIP_QNAME, CTSVGBlip.class, null, value);
    }
}
