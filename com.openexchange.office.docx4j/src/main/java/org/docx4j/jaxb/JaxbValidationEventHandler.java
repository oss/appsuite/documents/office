/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.jaxb;

import java.io.IOException;
import jakarta.xml.bind.ValidationEvent;
import jakarta.xml.bind.ValidationEventLocator;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JaxbValidationEventHandler implements IValidationEventHandler {

	private static Logger log = LoggerFactory.getLogger(JaxbValidationEventHandler.class);

	private boolean shouldContinue = true;

	private OpcPackage opcPackage = null;

	public void setContinue(boolean val) {
		shouldContinue = val;
	}

	public JaxbValidationEventHandler(OpcPackage opcPackage) {
	    this.opcPackage = opcPackage;
	}

    @Override
    public OpcPackage getPackage() {
        return opcPackage;
    }

	static Templates mcPreprocessorXslt;

	public static Templates getMcPreprocessor() throws IOException, TransformerConfigurationException {

		if (mcPreprocessorXslt==null) {
			try {
				Source xsltSource  = new StreamSource(
						org.docx4j.utils.ResourceUtils.getResource(
								"org/docx4j/jaxb/mc-preprocessor.xslt"));
				mcPreprocessorXslt = XmlUtils.getTransformerTemplate(xsltSource);
			} catch (IOException e) {
				log.error("Problem setting up  mc-preprocessor.xslt", e);
				throw(e);
			} catch (TransformerConfigurationException e) {
				log.error("Problem setting up  mc-preprocessor.xslt", e);
				throw(e);
			}
		}
		return mcPreprocessorXslt;
	}

	@Override
    public boolean handleEvent(ValidationEvent ve) {

	    Context.abortOnLowMemory(opcPackage);

	    if (ve.getSeverity()==ValidationEvent.FATAL_ERROR || ve.getSeverity()==ValidationEvent.ERROR) {
	        if(ve.getLinkedException()!=null) {
	            if(ve.getLinkedException().getCause() instanceof OutOfMemoryError) {
	                throw new OutOfMemoryError();
	            }
	            if (log.isDebugEnabled() ) {
	                ve.getLinkedException().printStackTrace();
	            }
	        }
	    }
	    if(log.isDebugEnabled()) {
    	    final ValidationEventLocator locator = ve.getLocator();
    	    if(locator!=null) {
    	        log.debug("line: " + Integer.toString(locator.getLineNumber()) + ", column: " + Integer.toString(locator.getColumnNumber()) + ", " + ve.getMessage());
    	    }
	    }
	    return shouldContinue;
    }
}
