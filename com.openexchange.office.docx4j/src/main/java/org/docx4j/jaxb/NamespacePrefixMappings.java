/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.jaxb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import org.apache.commons.lang3.text.StrTokenizer;

/**
 * NamespacePrefixMappings, required for JAXB, and XPath.
 *
 * The intent is to define the namespace prefix mappings in a
 * single place.
 *
 * This class implements NamespaceContext, so it can be used as follows:
 *
 *  	XPathFactory factory = XPathFactory.newInstance();
 *		XPath xPath = factory.newXPath();
 *		xPath.setNamespaceContext(new NamespacePrefixMappings());
 *
 * For JAXB, NamespacePrefixMapper (for RI) and NamespacePrefixMapperSunInternal
 * (for Java 6) both refer to this class.
 *
 * @author jharrop
 *
 */
public class NamespacePrefixMappings implements NamespaceContext {

    /**
     * Returns a preferred prefix for the given namespace URI.
     *
     * @param namespaceUri
     *      The namespace URI for which the prefix needs to be found.
     *      Never be null. "" is used to denote the default namespace.
     * @param suggestion
     *      When the content tree has a suggestion for the prefix
     *      to the given namespaceUri, that suggestion is passed as a
     *      parameter. Typically this value comes from QName.getPrefix()
     *      to show the preference of the content tree. This parameter
     *      may be null, and this parameter may represent an already
     *      occupied prefix.
     * @param requirePrefix
     *      If this method is expected to return non-empty prefix.
     *      When this flag is true, it means that the given namespace URI
     *      cannot be set as the default namespace.
     *
     * @return
     *      null if there's no preferred prefix for the namespace URI.
     *      In this case, the system will generate a prefix for you.
     *
     *      Otherwise the system will try to use the returned prefix,
     *      but generally there's no guarantee if the prefix will be
     *      actually used or not.
     *
     *      return "" to map this namespace URI to the default namespace.
     *      Again, there's no guarantee that this preference will be
     *      honored.
     *
     *      If this method returns "" when requirePrefix=true, the return
     *      value will be ignored and the system will generate one.
     */
    protected static String getPreferredPrefixStatic(String namespaceUri, String suggestion) {

        switch(namespaceUri) {
            case "http://schemas.openxmlformats.org/wordprocessingml/2006/main": return "w";
            case "http://schemas.microsoft.com/office/2006/xmlPackage": return "pkg";
            case "http://schemas.openxmlformats.org/presentationml/2006/main": return "p";
            case "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties": return "prop";
            case "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties": return "properties";
            case "http://schemas.openxmlformats.org/package/2006/metadata/core-properties": return "cp";
            case "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes": return "vt";
            case "http://schemas.openxmlformats.org/package/2006/relationships": return "rel";
            case "http://schemas.openxmlformats.org/officeDocument/2006/relationships": return "r";
            case "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing": return "wp";
            case "http://schemas.microsoft.com/office/word/2010/wordprocessingShape": return "wps";
            case "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup": return "wpg";
            case "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas": return "wpc";
            case "http://schemas.openxmlformats.org/drawingml/2006/chart": return "c";
            case "http://schemas.openxmlformats.org/drawingml/2006/main": return "a";
            case "http://schemas.openxmlformats.org/drawingml/2006/picture": return "pic";
            case "http://schemas.openxmlformats.org/drawingml/2006/diagram": return "dgm";
            case "http://schemas.microsoft.com/office/drawing/2008/diagram": return "dsp";
            case "urn:schemas-microsoft-com:office:office": return "o";
            case "urn:schemas-microsoft-com:vml": return "v";
            case "http://schemas.microsoft.com/office/word/2003/auxHint": return "WX";
            case "http://schemas.microsoft.com/office/word/2006/wordml": return "wne";
            case "http://schemas.microsoft.com/office/word/2010/wordml": return "w14";
            case "http://schemas.microsoft.com/office/word/2012/wordml": return "w15";
            case "http://schemas.microsoft.com/aml/2001/core": return "aml";
            case "urn:schemas-microsoft-com:office:word": return "w10";
            case "http://schemas.openxmlformats.org/officeDocument/2006/math": return "m";
            case "http://www.w3.org/2001/XMLSchema-instance": return "xsi";
            case "http://purl.org/dc/elements/1.1/": return "dc";
            case "http://purl.org/dc/terms/": return "dcterms";
            case "http://www.w3.org/XML/1998/namespace": return "xml";
            case "http://schemas.openxmlformats.org/officeDocument/2006/customXml": return "ds";
            case "http://opendope.org/xpaths": return "odx";
            case "http://opendope.org/conditions": return "odc";
            case "http://opendope.org/components": return "odi";
            case "http://opendope.org/questions": return "odq";
            case "http://opendope.org/answers": return "oda";
            case "http://opendope.org/SmartArt/DataHierarchy": return "odgm";
            case "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing": return "xdr";
            case "http://www.w3.org/2002/xforms": return "xf";
            case "http://www.w3.org/2001/xml-events": return "xe";
            case "http://www.w3.org/2001/XMLSchema": return "xs";
            case "http://schemas.openxmlformats.org/markup-compatibility/2006": return "mc";
//          case "urn:schemas-microsoft-com:office:excel": return "?";
//          case "http://schemas.openxmlformats.org/drawingml/2006/compatibility": return "?";
//          case "http://schemas.openxmlformats.org/drawingml/2006/lockedCanvas": return "?";
//          case "http://schemas.openxmlformats.org/drawingml/2006/chartDrawing": return "?";
//          case "http://schemas.openxmlformats.org/officeDocument/2006/bibliography": return "?";
//          case "http://schemas.openxmlformats.org/schemaLibrary/2006/main": return "?";
        }
    	return suggestion;
    }

    // ----------------------------------------------------
    // implement NamespaceContext,
    // for use with for use with javax.xml.xpath

	@Override
    public String getNamespaceURI(String prefix) {  // implementing NamespaceContext

		// Excel uses a default namespace, not a prefix.  But it is convenient
		// to be able to use a prefix in XPath
		if (prefix.equals("s"))
			return "http://schemas.openxmlformats.org/spreadsheetml/2006/main";

		return getNamespaceURIStatic(prefix);
	}

	protected static String getNamespaceURIStatic(String prefix) {

	    switch(prefix) {
	        case "w": return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
            case "pkg": return "http://schemas.microsoft.com/office/2006/xmlPackage";
            case "p": return "http://schemas.openxmlformats.org/presentationml/2006/main";
	        case "prop": return "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";
	        case "properties": return "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties";
	        case "cp": return "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
	        case "vt": return "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";
	        case "rel": return "http://schemas.openxmlformats.org/package/2006/relationships";
            case "r": return "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
	        case "wp": return "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
	        case "wps": return "http://schemas.microsoft.com/office/word/2010/wordprocessingShape";
	        case "wpg": return "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup";
	        case "wpc":return "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas";
	        case "c" : return "http://schemas.openxmlformats.org/drawingml/2006/chart";
	        case "a": return "http://schemas.openxmlformats.org/drawingml/2006/main";
	        case "pic": return "http://schemas.openxmlformats.org/drawingml/2006/picture";
	        case "dgm": return "http://schemas.openxmlformats.org/drawingml/2006/diagram";
	        case "dsp": return "http://schemas.microsoft.com/office/drawing/2008/diagram";
	        case "o": return "urn:schemas-microsoft-com:office:office";
	        case "v": return "urn:schemas-microsoft-com:vml";
	        case "WX": return "http://schemas.microsoft.com/office/word/2003/auxHint";
	        case "wne": return "http://schemas.microsoft.com/office/word/2006/wordml";
	        case "w14": return "http://schemas.microsoft.com/office/word/2010/wordml";
	        case "w15": return "http://schemas.microsoft.com/office/word/2012/wordml";
	        case "aml": return "http://schemas.microsoft.com/aml/2001/core";
	        case "w10": return "urn:schemas-microsoft-com:office:word";
	        case "m": return "http://schemas.openxmlformats.org/officeDocument/2006/math";
	        case "xsi": return "http://www.w3.org/2001/XMLSchema-instance";
	        case "dc": return "http://purl.org/dc/elements/1.1/";
	        case "dcterms": return "http://purl.org/dc/terms/";
	        case "xml": return "http://www.w3.org/XML/1998/namespace";
	        case "ds": return "http://schemas.openxmlformats.org/officeDocument/2006/customXml";
            case "odx": return "http://opendope.org/xpaths";
            case "odc": return "http://opendope.org/conditions";
            case "odi": return "http://opendope.org/components";
            case "odq": return "http://opendope.org/questions";
            case "oda": return "http://opendope.org/answers";
            case "odgm": return "http://opendope.org/SmartArt/DataHierarchy";
            case "xdr": return "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing";
	        case "xf": return "http://www.w3.org/2002/xforms";
	        case "xe": return "http://www.w3.org/2001/xml-events";
	        case "xs": return "http://www.w3.org/2001/XMLSchema";
            case "mc": return "http://schemas.openxmlformats.org/markup-compatibility/2006";
	    }

	    // Registered prefixes
	    final String result = namespaces.get(prefix);
		if (result==null) {
			return XMLConstants.NULL_NS_URI;
		}
        return result;
	}

	@Override
    public String getPrefix(String namespaceURI) {  // implementing NamespaceContext

    	// Excel uses a default namespace, not a prefix.
    	if (namespaceURI.equals("http://schemas.openxmlformats.org/spreadsheetml/2006/main")) {
    		return "";
    	}
		return getPreferredPrefixStatic(namespaceURI, null);
	}

	@Override
    public Iterator getPrefixes(String namespaceURI) {
		return null;
	}

	private static Map<String, String> namespaces = new HashMap<String, String>();
	public static void registerPrefixMappings(String prefixMappings) {
		// eg  w:prefixMappings="xmlns:ns0='http://schemas.medchart'"
		// according to the spec, whitespace is the delimiter

		if (prefixMappings==null || prefixMappings.equals("") ) return;

		// we get one of these each time we encounter a w:dataBinding
		// element in a content control; pity it is not done just
		// once!

		// first tokenise on space
		StrTokenizer tokens = new StrTokenizer(prefixMappings);
		while (tokens.hasNext() ) {
			String token = tokens.nextToken();
			//log.debug("Got: " + token);
			int pos = token.indexOf("=");
			String prefix = token.substring(6, pos); // drop xmlns:
			//log.debug("Got: " + prefix);
			String uri = token.substring(pos+2, token.lastIndexOf("'"));
			//log.debug("Got: " + uri);
			namespaces.put(prefix, uri);
		}
	}
}
