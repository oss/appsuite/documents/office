
package org.docx4j.jaxb;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

public class NamespacePrefixMapperUtils {

	private static JAXBContext testContext;

	private static NamespacePrefixMapper prefixMapper;
	private static NamespacePrefixMapperRelationshipsPart prefixMapperRels;

	public static NamespacePrefixMapper getPrefixMapper() throws JAXBException {
		
		if (prefixMapper==null) {
    		if (testContext==null) {
    			java.lang.ClassLoader classLoader = NamespacePrefixMapperUtils.class.getClassLoader();
    			testContext = JAXBContext.newInstance("org.docx4j.relationships",classLoader );
    		}
    		if (testContext==null) {
    			throw new JAXBException("Couldn't create context for org.docx4j.relationships.  Everything is broken!");
    		}

    		Marshaller m=testContext.createMarshaller();
            try {
                // Try RI suitable one
                m.setProperty("org.glassfish.jaxb.namespacePrefixMapper", new NamespacePrefixMapper() );
                prefixMapper = new NamespacePrefixMapper();
            } catch (java.lang.NoClassDefFoundError notRIEither) {
                throw new JAXBException("JAXB: no Reference Implementation nor Java present?");
            } catch (jakarta.xml.bind.PropertyException notRIEither) {
                throw new JAXBException("JAXB: no Reference Implementation nor Java present?");
            }
		}
		return prefixMapper;
	}

	public static NamespacePrefixMapperRelationshipsPart getPrefixMapperRelationshipsPart() throws JAXBException {

		if (prefixMapperRels==null) {
	        if (testContext==null) {
	            java.lang.ClassLoader classLoader = NamespacePrefixMapperUtils.class.getClassLoader();
	            testContext = JAXBContext.newInstance("org.docx4j.relationships",classLoader );
	        }
	        
	        Marshaller m=testContext.createMarshaller();
	        try {
	            // Try RI suitable one
	            m.setProperty("org.glassfish.jaxb.namespacePrefixMapper", new NamespacePrefixMapperRelationshipsPart() );
	            prefixMapperRels = new NamespacePrefixMapperRelationshipsPart();
	        } catch (java.lang.NoClassDefFoundError notRIEither) {
	            throw new JAXBException("JAXB: no Reference Implementation present?");
	        } catch (jakarta.xml.bind.PropertyException notRIEither) {
	            throw new JAXBException("JAXB: no Reference Implementation present?");
	        }
		}
        return prefixMapperRels;
	}
}
