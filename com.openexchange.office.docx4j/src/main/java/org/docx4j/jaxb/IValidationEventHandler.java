

/**
 *
 * @author sven.jacobi@open-xchange.com
 */
package org.docx4j.jaxb;

import jakarta.xml.bind.ValidationEventHandler;
import org.docx4j.openpackaging.packages.OpcPackage;

public interface IValidationEventHandler extends ValidationEventHandler {

    public OpcPackage getPackage();
}
