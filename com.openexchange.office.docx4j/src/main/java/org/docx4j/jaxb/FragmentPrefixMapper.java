
package org.docx4j.jaxb;

public class FragmentPrefixMapper extends org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper {

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        return NamespacePrefixMappings.getPreferredPrefixStatic(namespaceUri, suggestion);
    }

    public static String getPreferredPrefix(String namespaceUri) {    	
    	return NamespacePrefixMappings.getPreferredPrefixStatic(namespaceUri, null);
    }
}
