
package org.docx4j.jaxb;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.docx4j.XmlUtils;
import org.docx4j.dml.CTAdjPoint2D;
import org.docx4j.dml.CTAdjustHandleList;
import org.docx4j.dml.CTConnectionSite;
import org.docx4j.dml.CTConnectionSiteList;
import org.docx4j.dml.CTCustomGeometry2D;
import org.docx4j.dml.CTGeomGuide;
import org.docx4j.dml.CTGeomGuideList;
import org.docx4j.dml.CTGeomRect;
import org.docx4j.dml.CTPath2D;
import org.docx4j.dml.CTPath2DArcTo;
import org.docx4j.dml.CTPath2DClose;
import org.docx4j.dml.CTPath2DCubicBezierTo;
import org.docx4j.dml.CTPath2DLineTo;
import org.docx4j.dml.CTPath2DList;
import org.docx4j.dml.CTPath2DMoveTo;
import org.docx4j.dml.CTPath2DQuadBezierTo;
import org.docx4j.dml.CTPolarAdjustHandle;
import org.docx4j.dml.CTXYAdjustHandle;
import org.docx4j.dml.STPathFillMode;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import com.openexchange.office.filter.core.STShapeType;

public class CustomGeometryFactory {

	public static HashMap<STShapeType, CTCustomGeometry2D> createCustomGeometries() {

		final HashMap<STShapeType, CTCustomGeometry2D> customGeometryMap = new HashMap<STShapeType, CTCustomGeometry2D>(186);
		InputStream input = null;
		try {
			input = org.docx4j.utils.ResourceUtils.getResource("org/docx4j/dml/presetShapeDefinitions.xml");
			if(input!=null) {
				final XMLReader xmlReader = XmlUtils.getXMLReader();
				xmlReader.setContentHandler(new CustomGeometryContentHandler(xmlReader, customGeometryMap));
				xmlReader.parse(new InputSource(input));
			}
		}
		catch (IOException e) {
		    //
		}
		catch (ParserConfigurationException e) {
		    //
		}
		catch (SAXException e) {
		    //
		}
		finally {
			IOUtils.closeQuietly(input);
		}
		return customGeometryMap;
	}

	abstract protected static class BaseHandler extends DefaultHandler {

		final XMLReader xmlReader;
		final Stack<BaseHandler> contextStack;

		protected BaseHandler(XMLReader xmlReader) {
			super();
			this.xmlReader = xmlReader;
			contextStack = new Stack<BaseHandler>();
		}

		protected BaseHandler(BaseHandler parent, @SuppressWarnings("unused") Attributes attributes) {
			super();
			this.xmlReader = parent.xmlReader;
			this.contextStack = parent.contextStack;
		}

		protected BaseHandler startElement(@SuppressWarnings("unused") String localName, @SuppressWarnings("unused") Attributes attributes) {
			return this;
		}

		protected void endElement(@SuppressWarnings("unused") String localName) {
		    //
		}

		@Override
		final public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
			super.startElement(uri, localName, qName, attributes);
			contextStack.push((BaseHandler)xmlReader.getContentHandler());
			final BaseHandler newBaseHandler = startElement(localName, attributes);
			xmlReader.setContentHandler(newBaseHandler);
		}

		@Override
		final public void endElement(String uri, String localName, String qName) throws SAXException {
			super.endElement(uri, localName, qName);
			final BaseHandler defaultHandler = contextStack.pop();
			defaultHandler.endElement(localName);
			xmlReader.setContentHandler(defaultHandler);
		}
	}

	private static class CustomGeometryContentHandler extends BaseHandler {

		final HashMap<STShapeType, CTCustomGeometry2D> map;

		protected CustomGeometryContentHandler(XMLReader xmlReader, HashMap<STShapeType, CTCustomGeometry2D> map) {
			super(xmlReader);
			this.map = map;
		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("presetShapeDefinitons")) {
				return this;
			}
			final CTCustomGeometry2D newGeometry = new CTCustomGeometry2D();
			map.put(STShapeType.fromValue(localName), newGeometry);
			return new CustomGeometry2DHandler(this, attributes, newGeometry);
		}
	}

	private static class CustomGeometry2DHandler extends BaseHandler {

		final CTCustomGeometry2D geometry;

		protected CustomGeometry2DHandler(BaseHandler parent, Attributes attributes, CTCustomGeometry2D geometry) {
			super(parent, attributes);
			this.geometry = geometry;
 		}

		@Override
		final protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("avLst")) {
				geometry.setAvLst(new CTGeomGuideList());
				return new GeomGuideListHandler(this, attributes, geometry.getAvLst());
			}
			else if(localName.equals("gdLst")) {
				geometry.setGdLst(new CTGeomGuideList());
				return new GeomGuideListHandler(this, attributes, geometry.getGdLst());
			}
			else if(localName.equals("ahLst")) {
				geometry.setAhLst(new CTAdjustHandleList());
				return new AdjustHandleListHandler(this, attributes, geometry.getAhLst());
			}
			else if(localName.equals("cxnLst")) {
				geometry.setCxnLst(new CTConnectionSiteList());
				return new ConnectionSiteListHandler(this, attributes, geometry.getCxnLst());
			}
			else if(localName.equals("rect")) {
				geometry.setRect(new CTGeomRect());
				return new TextRectHandler(this, attributes, geometry.getRect());
			}
			else if(localName.equals("pathLst")) {
				geometry.setPathLst(new CTPath2DList());
				return new PathListHandler(this, attributes, geometry.getPathLst());
			}
			return this;
		}

		@Override
		final public void endElement(String localName) {
			if(localName.equals("avLst")) {
				((ArrayList<CTGeomGuide>)geometry.getAvLst().getGd()).trimToSize();
			}
			else if(localName.equals("ahLst")) {
			    ((ArrayList<Object>)geometry.getAhLst().getAhXYOrAhPolar()).trimToSize();
			}
			else if(localName.equals("gdLst")) {
				((ArrayList<CTGeomGuide>)geometry.getGdLst().getGd()).trimToSize();
			}
			else if(localName.equals("pathLst")) {
				((ArrayList<CTPath2D>)geometry.getPathLst().getPath()).trimToSize();
				for(CTPath2D path2D:geometry.getPathLst().getPath()) {
					((ArrayList<Object>)path2D.getCloseOrMoveToOrLnTo()).trimToSize();
				}
			}
		}
	}

	private static class GeomGuideListHandler extends BaseHandler {

		final CTGeomGuideList guideLst;

		protected GeomGuideListHandler(BaseHandler parent, Attributes attributes, CTGeomGuideList guideLst) {
			super(parent, attributes);
			this.guideLst = guideLst;
 		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("gd")) {
				final CTGeomGuide gd = new CTGeomGuide();
				guideLst.getGd().add(gd);
				return new gdHandler(this, attributes, gd);
			}
			return this;
		}
	}

	private static class AdjustHandleListHandler extends BaseHandler {

		final CTAdjustHandleList adjustHandleLst;

		protected AdjustHandleListHandler(BaseHandler parent, Attributes attributes, CTAdjustHandleList adjustHandleLst) {
			super(parent, attributes);
			this.adjustHandleLst = adjustHandleLst;
 		}

        @Override
        final protected BaseHandler startElement(String localName, Attributes attributes) {
            if(localName.equals("ahXY")) {
                final CTXYAdjustHandle xYAdjustHandle = new CTXYAdjustHandle();
                adjustHandleLst.getAhXYOrAhPolar().add(xYAdjustHandle);
                return new XYAdjustHandleHandler(this, attributes, xYAdjustHandle);
            }
            else if(localName.equals("ahPolar")) {
                final CTPolarAdjustHandle polarAdjustHandle = new CTPolarAdjustHandle();
                adjustHandleLst.getAhXYOrAhPolar().add(polarAdjustHandle);
                return new PolarAdjustHandleHandler(this, attributes, polarAdjustHandle);
            }
            return this;
        }
	}

    private static class XYAdjustHandleHandler extends BaseHandler {

        final CTXYAdjustHandle xYAdjustHandle;

        protected XYAdjustHandleHandler(BaseHandler parent, Attributes attributes, CTXYAdjustHandle xYAdjustHandle) {
            super(parent, attributes);
            this.xYAdjustHandle = xYAdjustHandle;

            for(int i=0; i<attributes.getLength(); i++) {
                final String name = attributes.getLocalName(i);
                final String val = attributes.getValue(i);

                if("gdRefX".equals(name)) {
                    xYAdjustHandle.setGdRefX(val);
                }
                else if("minX".equals(name)) {
                    xYAdjustHandle.setMinX(val);
                }
                else if("maxX".equals(name)) {
                    xYAdjustHandle.setMaxX(val);
                }
                else if("gdRefY".equals(name)) {
                    xYAdjustHandle.setGdRefY(val);
                }
                else if("minY".equals(name)) {
                    xYAdjustHandle.setMinY(val);
                }
                else if("maxY".equals(name)) {
                    xYAdjustHandle.setMaxY(val);
                }
            }
        }

        @Override
        protected BaseHandler startElement(String localName, Attributes attributes) {
            if(localName.equals("pos")) {
                final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
                xYAdjustHandle.setPos(adjPoint2D);
                return new AdjPoint2DHandler(this, attributes, adjPoint2D);
            }
            return this;
        }
    }

    private static class PolarAdjustHandleHandler extends BaseHandler {

        final CTPolarAdjustHandle polarAdjustHandle;

        protected PolarAdjustHandleHandler(BaseHandler parent, Attributes attributes, CTPolarAdjustHandle polarAdjustHandle) {
            super(parent, attributes);
            this.polarAdjustHandle = polarAdjustHandle;

            for(int i=0; i<attributes.getLength(); i++) {
                final String name = attributes.getLocalName(i);
                final String val = attributes.getValue(i);

                if("gdRefR".equals(name)) {
                    polarAdjustHandle.setGdRefR(val);
                }
                else if("minR".equals(name)) {
                    polarAdjustHandle.setMinR(val);
                }
                else if("maxR".equals(name)) {
                    polarAdjustHandle.setMaxR(val);
                }
                else if("gdRefAng".equals(name)) {
                    polarAdjustHandle.setGdRefAng(val);
                }
                else if("minAng".equals(name)) {
                    polarAdjustHandle.setMinAng(val);
                }
                else if("maxAng".equals(name)) {
                    polarAdjustHandle.setMaxAng(val);
                }
            }
        }

        @Override
        protected BaseHandler startElement(String localName, Attributes attributes) {
            if(localName.equals("pos")) {
                final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
                polarAdjustHandle.setPos(adjPoint2D);
                return new AdjPoint2DHandler(this, attributes, adjPoint2D);
            }
            return this;
        }
    }

	private static class ConnectionSiteListHandler extends BaseHandler {

		final CTConnectionSiteList connectionSiteLst;

		protected ConnectionSiteListHandler(BaseHandler parent, Attributes attributes, CTConnectionSiteList connectionSiteLst) {
			super(parent, attributes);
			this.connectionSiteLst = connectionSiteLst;
 		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
		    if(localName.equals("cxn")) {
		        final List<CTConnectionSite> cxnList = connectionSiteLst.getCxn();
		        final CTConnectionSite connectionSite = new CTConnectionSite();
		        cxnList.add(connectionSite);
		        return new CxnHandler(this, attributes, connectionSite);
            }
			return this;
		}
	}

    private static class CxnHandler extends BaseHandler {

        final CTConnectionSite connectionSite;

        protected CxnHandler(BaseHandler parent, Attributes attributes, CTConnectionSite connectionSite) {
            super(parent, attributes);
            this.connectionSite = connectionSite;
            connectionSite.setAng(attributes.getValue("ang"));
        }

        @Override
        protected BaseHandler startElement(String localName, Attributes attributes) {
            if(localName.equals("pos")) {
                final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
                connectionSite.setPos(adjPoint2D);
                return new AdjPoint2DHandler(this, attributes, adjPoint2D);
            }
            return this;
        }
    }

	private static class TextRectHandler extends BaseHandler {

		protected TextRectHandler(BaseHandler parent, Attributes attributes, CTGeomRect geomRect) {
			super(parent, attributes);
			for(int i=0; i<attributes.getLength(); i++) {
				final String name = attributes.getLocalName(i);
				final String val = attributes.getValue(i);
				if(name.equals("l")) {
					geomRect.setL(val);
				}
				else if(name.equals("t")) {
					geomRect.setT(val);
				}
				else if(name.equals("r")) {
					geomRect.setR(val);
				}
				else if(name.equals("b")) {
					geomRect.setB(val);
				}
			}
 		}
	}

	private static class PathListHandler extends BaseHandler {

		final CTPath2DList pathes;

		protected PathListHandler(BaseHandler parent, Attributes attributes, CTPath2DList pathes) {
			super(parent, attributes);
			this.pathes = pathes;
 		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("path")) {
				final List<CTPath2D> pathList = pathes.getPath();
				final CTPath2D path = new CTPath2D();
				pathList.add(path);
				return new PathHandler(this, attributes, path);
			}
			return this;
		}
	}

	private static class gdHandler extends BaseHandler {

		protected gdHandler(BaseHandler parent, Attributes attributes, CTGeomGuide gd) {
			super(parent, attributes);
			for(int i=0; i<attributes.getLength(); i++) {
				final String name = attributes.getLocalName(i);
				final String val = attributes.getValue(i);
				if(name.equals("name")) {
					gd.setName(val);
				}
				else if(name.equals("fmla")) {
					gd.setFmla(val);
				}
			}
 		}
	}

	private static class PathHandler extends BaseHandler {

		final CTPath2D path;

		protected PathHandler(BaseHandler parent, Attributes attributes, CTPath2D path) {
			super(parent, attributes);
			this.path = path;
			for(int i=0; i<attributes.getLength(); i++) {
				final String name = attributes.getLocalName(i);
				final String val = attributes.getValue(i);
				if(name.equals("fill")) {
					path.setFill(STPathFillMode.fromValue(val));
				}
				else if(name.equals("stroke")) {
					path.setStroke(val.equals("true"));
				}
				else if(name.equals("extrusionOk")) {
					path.setExtrusionOk(val.equals("true"));
				}
				else if(name.equals("w")) {
					path.setW(Long.valueOf(val));
				}
				else if(name.equals("h")) {
					path.setH(Long.valueOf(val));
				}
			}
 		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {

			if(localName.equals("close")) {
				path.getCloseOrMoveToOrLnTo().add(new CTPath2DClose());
			}
			else if(localName.equals("moveTo")) {
				final CTPath2DMoveTo moveTo = new CTPath2DMoveTo();
				path.getCloseOrMoveToOrLnTo().add(moveTo);
				return new MoveToHandler(this, attributes, moveTo);
			}
			else if(localName.equals("lnTo")) {
				final CTPath2DLineTo lineTo = new CTPath2DLineTo();
				path.getCloseOrMoveToOrLnTo().add(lineTo);
				return new LineToHandler(this, attributes, lineTo);
			}
			else if(localName.equals("arcTo")) {
				final CTPath2DArcTo arcTo = new CTPath2DArcTo();
				path.getCloseOrMoveToOrLnTo().add(arcTo);
				return new ArcToHandler(this, attributes, arcTo);
			}
			else if(localName.equals("quadBezTo")) {
				final CTPath2DQuadBezierTo quadBezTo = new CTPath2DQuadBezierTo();
				path.getCloseOrMoveToOrLnTo().add(quadBezTo);
				return new QuadBezToHandler(this, attributes, quadBezTo);
			}
			else if(localName.equals("cubicBezTo")) {
				final CTPath2DCubicBezierTo cubicBezTo = new CTPath2DCubicBezierTo();
				path.getCloseOrMoveToOrLnTo().add(cubicBezTo);
				return new CubicBezToHandler(this, attributes, cubicBezTo);
			}
			return this;
		}
	}

	private static class MoveToHandler extends BaseHandler {

		final CTPath2DMoveTo moveTo;

		protected MoveToHandler(BaseHandler parent, Attributes attributes, CTPath2DMoveTo moveTo) {
			super(parent, attributes);
			this.moveTo = moveTo;
		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {

			if(localName.equals("pt")) {
				final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
				moveTo.setPt(adjPoint2D);
				return new AdjPoint2DHandler(this, attributes, adjPoint2D);
			}
			return this;
		}
	}

	private static class LineToHandler extends BaseHandler {

		final CTPath2DLineTo lineTo;

		protected LineToHandler(BaseHandler parent, Attributes attributes, CTPath2DLineTo lineTo) {
			super(parent, attributes);
			this.lineTo = lineTo;
		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {

			if(localName.equals("pt")) {
				final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
				lineTo.setPt(adjPoint2D);
				return new AdjPoint2DHandler(this, attributes, adjPoint2D);
			}
			return this;
		}
	}

	private static class ArcToHandler extends BaseHandler {

		protected ArcToHandler(BaseHandler parent, Attributes attributes, CTPath2DArcTo arcTo) {
			super(parent, attributes);
			for(int i=0; i<attributes.getLength(); i++) {
				final String name = attributes.getLocalName(i);
				final String val = attributes.getValue(i);
				if(name.equals("wR")) {
					arcTo.setWR(val);
				}
				else if(name.equals("hR")) {
					arcTo.setHR(val);
				}
				else if(name.equals("stAng")) {
					arcTo.setStAng(val);
				}
				else if(name.equals("swAng")) {
					arcTo.setSwAng(val);
				}
			}
		}
	}

	private static class QuadBezToHandler extends BaseHandler {

		final CTPath2DQuadBezierTo quadBezTo;

		protected QuadBezToHandler(BaseHandler parent, Attributes attributes, CTPath2DQuadBezierTo quadBezTo) {
			super(parent, attributes);
			this.quadBezTo = quadBezTo;
		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("pt")) {
				final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
				quadBezTo.getPt().add(adjPoint2D);
				return new AdjPoint2DHandler(this, attributes, adjPoint2D);
			}
			return this;
		}
	}

	private static class CubicBezToHandler extends BaseHandler {

		final CTPath2DCubicBezierTo cubicBezTo;

		protected CubicBezToHandler(BaseHandler parent, Attributes attributes, CTPath2DCubicBezierTo cubicBezTo) {
			super(parent, attributes);
			this.cubicBezTo = cubicBezTo;
		}

		@Override
		protected BaseHandler startElement(String localName, Attributes attributes) {
			if(localName.equals("pt")) {
				final CTAdjPoint2D adjPoint2D = new CTAdjPoint2D();
				cubicBezTo.getPt().add(adjPoint2D);
				return new AdjPoint2DHandler(this, attributes, adjPoint2D);
			}
			return this;
		}
	}

	private static class AdjPoint2DHandler extends BaseHandler {

		protected AdjPoint2DHandler(BaseHandler parent, Attributes attributes, CTAdjPoint2D pt) {
			super(parent, attributes);
			for(int i=0; i<attributes.getLength(); i++) {
				final String name = attributes.getLocalName(i);
				final String val = attributes.getValue(i);
				if(name.equals("x")) {
					pt.setX(val);
				}
				else if(name.equals("y")) {
					pt.setY(val);
				}
			}
		}
	}
}
