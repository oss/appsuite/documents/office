/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.jaxb;

import java.util.HashMap;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.ValidationEventHandler;
import javax.xml.stream.XMLInputFactory;
import org.docx4j.dml.CTCustomGeometry2D;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.filter.core.STShapeType;

final public class Context {

    private static Thread jcThread;
    private static boolean jcThreadJoined = false;

    protected static JAXBContext jc;

	protected static JAXBContext jcDocPropsCore;
	protected static JAXBContext jcDocPropsCustom;
	protected static JAXBContext jcDocPropsExtended;
	protected static JAXBContext jcRelationships;
	protected static JAXBContext jcCustomXmlProperties;
	protected static JAXBContext jcContentTypes;

	protected static JAXBContext jcXmlPackage;

	protected static JAXBContext jcSectionModel;

	protected static HashMap<STShapeType, CTCustomGeometry2D> customGeometries;

	/** @since 3.0.1 */
	protected static JAXBContext jcMCE;

	protected static Logger log = LoggerFactory.getLogger(Context.class);

	static {
		// Diagnostics regarding JAXB implementation
		try {
			Object namespacePrefixMapper = NamespacePrefixMapperUtils.getPrefixMapper();
			if ( namespacePrefixMapper.getClass().getName().equals("org.docx4j.jaxb.NamespacePrefixMapperSunInternal") ) {
				// Java 6
				log.info("Using Java 6/7 JAXB implementation");
			} else {
				log.info("Using JAXB Reference Implementation");
			}
		} catch (JAXBException e) {
			log.error("PANIC! No suitable JAXB implementation available");
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}

		jcThread = new Thread(
            new Runnable() {
                @Override
                public void run() {
                    try {
                        // JAXBContext.newInstance uses the context class loader of the current thread.
                        // To specify the use of a different class loader,
                        // either set it via the Thread.setContextClassLoader() api
                        // or use the newInstance method.
                        // JBOSS (older versions only?) might use a different class loader to load JAXBContext,
                        // which caused problems, so in docx4j we explicitly specify our class loader.
                        // IKVM 7.3.4830 also needs this to be done
                        // (java.lang.Thread.currentThread().setContextClassLoader doesn't seem to help!)
                        // and there are no environments in which this approach is known to be problematic

                        java.lang.ClassLoader classLoader = Context.class.getClassLoader();

                        jc = JAXBContext.newInstance("org.docx4j.wml:org.docx4j.w14:org.docx4j.w15:org.docx4j.wp14:org.docx4j.w16cid:" +
                                "org.docx4j.schemas.microsoft.com.office.word_2006.wordml:" +
                                "org.docx4j.dml:org.docx4j.dml.chart:org.docx4j.dml.chartStyle2012:org.docx4j.dml_2013_command:org.docx4j.dml.chartDrawing:org.docx4j.dml.compatibility:org.docx4j.dml.diagram:org.docx4j.dml.lockedCanvas:org.docx4j.dml.picture:org.docx4j.dml.wordprocessingDrawing:org.docx4j.dml.spreadsheetDrawing:org.docx4j.dml.diagram2008:" +
                                // All VML stuff is here, since compiling it requires WML and DML (and MathML), but not PML or SML
                                "org.docx4j.vml:org.docx4j.vml.officedrawing:org.docx4j.vml.wordprocessingDrawing:org.docx4j.vml.presentationDrawing:org.docx4j.vml.spreadsheetDrawing:org.docx4j.vml.root:" +
                                "org.docx4j.docProps.coverPageProps:" +
                                "org.docx4j.math:" +
                                "org.docx4j.sharedtypes:org.docx4j.bibliography:org.docx4j.mce:" +
                                "org.docx4j.drawing2010:" +
                                "org.docx4j.drawing2014:" +
                                "org.docx4j.drawing2016:" +
                                "org.docx4j.dml.wordprocessingCanvas2010:org.docx4j.dml.wordprocessingGroup2010:org.docx4j.dml.wordprocessingShape2010" + ":org.docx4j.dml.chartex2014", classLoader);

                        if (jc.getClass().getName().equals("org.eclipse.persistence.jaxb.JAXBContext")) {
                            log.debug("MOXy JAXB implementation is in use!");
                        } else {
                            log.debug("Not using MOXy; using " + jc.getClass().getName());
                        }
                        jcDocPropsCore = JAXBContext.newInstance("org.docx4j.docProps.core:org.docx4j.docProps.core.dc.elements:org.docx4j.docProps.core.dc.terms", classLoader);
                        jcDocPropsCustom = JAXBContext.newInstance("org.docx4j.docProps.custom", classLoader);
                        jcDocPropsExtended = JAXBContext.newInstance("org.docx4j.docProps.extended", classLoader);
                        jcXmlPackage = JAXBContext.newInstance("org.docx4j.xmlPackage", classLoader);
                        jcRelationships = JAXBContext.newInstance("org.docx4j.relationships", classLoader);
                        jcCustomXmlProperties = JAXBContext.newInstance("org.docx4j.customXmlProperties", classLoader);
                        jcContentTypes = JAXBContext.newInstance("org.docx4j.openpackaging.contenttype", classLoader);

                        jcSectionModel = JAXBContext.newInstance("org.docx4j.model.structure.jaxb", classLoader);

                        jcMCE = JAXBContext.newInstance("org.docx4j.mce", classLoader );

                   		log.debug(".. other contexts loaded ..");


                    } catch (Exception ex) {
                        log.error("Cannot initialize context", ex);
                    }
                }
            }
        );
        jcThread.start();
	}

	public static boolean isLowMemoryAbort(OpcPackage opcPackage) {
	    return opcPackage!=null ? opcPackage.isLowMemoryAbort() : false;
	}

	public static void abortOnLowMemory(OpcPackage opcPackage) {
	    if(isLowMemoryAbort(opcPackage)) {
	        throw new RuntimeException();
	    }
	}

	public static void abortOnLowMemory(Unmarshaller unmarshaller) {
	    try {
	        final ValidationEventHandler eventHandler = unmarshaller.getEventHandler();
    	    if(eventHandler instanceof IValidationEventHandler) {
    	        abortOnLowMemory(((IValidationEventHandler)eventHandler).getPackage());
    	    }
	    }
	    catch(JAXBException e) {
	        //
	    }
	}

	public static Integer getNextMarkupId(Marshaller marshaller) {
	    try {
	        final ValidationEventHandler eventHandler = marshaller.getEventHandler();
	        if(eventHandler instanceof IValidationEventHandler) {
	            final OpcPackage opcPackage = ((IValidationEventHandler)eventHandler).getPackage();
	            if(opcPackage!=null) {
	                return opcPackage.getNextMarkupId();
	            }
	        }
	    }
	    catch(JAXBException e) {
	        //
	    }
	    return Integer.valueOf(0);
	}

	public static void addMarkupId(Unmarshaller unmarshaller, Integer id) {
	    try {
	        final ValidationEventHandler eventHandler = unmarshaller.getEventHandler();
	        if(eventHandler instanceof IValidationEventHandler) {
	            final OpcPackage opcPackage = ((IValidationEventHandler)eventHandler).getPackage();
	            if(opcPackage!=null) {
	                opcPackage.addMarkupId(id);
	            }
	        }
	    }
	    catch(JAXBException e) {
	        //
	    }
	}

	/*
	 * the jcThread is joined, additional it is checked if it should
	 * aborted because of low memory
	 *
	 */
	private static final void initializeContexts() {
        if(!jcThreadJoined) {
            try {
                jcThread.join();
            } catch (InterruptedException e) {
                log.info("DOCX4J, could not get Context");
            }
            jcThreadJoined = true;
        }
	}

	/*
	 * provide unmarshaller with event handler providing access to the document package, the event handler parameter is allowed to be zero
	 */
	public static Unmarshaller createUnmarshaller(JAXBContext context, IValidationEventHandler eventHandler) throws JAXBException {
        final XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setEventHandler(eventHandler);
        return unmarshaller;
	}

    /*
     * provide marshaller with event handler providing access to the document package, the event handler parameter is allowed to be zero
     */
	public static Marshaller createMarshaller(JAXBContext context, org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper namespacePrefixMapper, IValidationEventHandler eventHandler) throws JAXBException {
	    final Marshaller marshaller = context.createMarshaller();
	    if(namespacePrefixMapper!=null) {
	        marshaller.setProperty("org.glassfish.jaxb.namespacePrefixMapper", namespacePrefixMapper);
	    }
        marshaller.setEventHandler(eventHandler);
	    return marshaller;
	}

	public static final JAXBContext getJc() {
        initializeContexts();
        return jc;
    }

    public static final JAXBContext getJcDocPropsCore() {
        initializeContexts();
        return jcDocPropsCore;
    }

    public static final JAXBContext getJcDocPropsCustom() {
        initializeContexts();
        return jcDocPropsCustom;
    }

    public static final JAXBContext getJcDocPropsExtended() {
        initializeContexts();
        return jcDocPropsExtended;
    }

    public static final JAXBContext getJcRelationships() {
        initializeContexts();
        return jcRelationships;
    }

    public static final JAXBContext getJcCustomXmlProperties() {
        initializeContexts();
        return jcCustomXmlProperties;
    }

    public static final JAXBContext getJcContentTypes() {
        initializeContexts();
        return jcContentTypes;
    }

    public static final JAXBContext getJcXmlPackage() {
        initializeContexts();
        return jcXmlPackage;
    }

    public static final JAXBContext getJcSectionModel() {
        initializeContexts();
        return jcSectionModel;
    }

    public static final JAXBContext getJcMCE() {
        initializeContexts();
        return jcMCE;
    }

    public static final CTCustomGeometry2D getCustomShapeForPreset(STShapeType presetType) {
    	if(customGeometries==null) {
    		customGeometries = CustomGeometryFactory.createCustomGeometries();
    	}
    	return customGeometries.get(presetType);
    }

    public static final HashMap<STShapeType, CTCustomGeometry2D> getCustomShapePresets() {
    	if(customGeometries==null) {
    		customGeometries = CustomGeometryFactory.createCustomGeometries();
    	}
    	return customGeometries;
    }

    private static org.docx4j.wml.ObjectFactory wmlObjectFactory;
	public static org.docx4j.wml.ObjectFactory getWmlObjectFactory() {
		if (wmlObjectFactory==null) {
			wmlObjectFactory = new org.docx4j.wml.ObjectFactory();
		}
		return wmlObjectFactory;
	}

    private static org.pptx4j.pml.ObjectFactory pmlObjectFactory;
    public static org.pptx4j.pml.ObjectFactory getpmlObjectFactory() {
        if (pmlObjectFactory==null) {
            pmlObjectFactory = new org.pptx4j.pml.ObjectFactory();
        }
        return pmlObjectFactory;
    }

    private static org.docx4j.dml.ObjectFactory dmlObjectFactory;
    public static org.docx4j.dml.ObjectFactory getDmlObjectFactory() {
        if (dmlObjectFactory==null) {
            dmlObjectFactory = new org.docx4j.dml.ObjectFactory();
        }
        return dmlObjectFactory;
    }

    private static org.docx4j.dml.chart.ObjectFactory dmlChartObjectFactory;
    public static org.docx4j.dml.chart.ObjectFactory getDmlChartObjectFactory() {
        if(dmlChartObjectFactory==null) {
            dmlChartObjectFactory = new org.docx4j.dml.chart.ObjectFactory();
        }
        return dmlChartObjectFactory;
    }

    private static org.docx4j.vml.wordprocessingDrawing.ObjectFactory vmlWordprocessingDrawingObjectFactory;
    public static org.docx4j.vml.wordprocessingDrawing.ObjectFactory getVmlWordprocessingDrawingObjectFactory() {
        if(vmlWordprocessingDrawingObjectFactory==null) {
            vmlWordprocessingDrawingObjectFactory = new org.docx4j.vml.wordprocessingDrawing.ObjectFactory();
        }
        return vmlWordprocessingDrawingObjectFactory;
    }

    private static org.docx4j.dml.spreadsheetDrawing.ObjectFactory dmlSpreadsheetDrawingObjectFactory;
    public static org.docx4j.dml.spreadsheetDrawing.ObjectFactory getDmlSpreadsheetDrawingObjectFactory() {
        if(dmlSpreadsheetDrawingObjectFactory==null) {
            dmlSpreadsheetDrawingObjectFactory = new org.docx4j.dml.spreadsheetDrawing.ObjectFactory();
        }
        return dmlSpreadsheetDrawingObjectFactory;
    }
}
