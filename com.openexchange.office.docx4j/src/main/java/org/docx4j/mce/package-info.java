
@jakarta.xml.bind.annotation.XmlSchema(namespace = "http://schemas.openxmlformats.org/markup-compatibility/2006", xmlns = {
    @jakarta.xml.bind.annotation.XmlNs(prefix = "wps", namespaceURI = "http://schemas.microsoft.com/office/word/2010/wordprocessingShape"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "wpg", namespaceURI = "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "wpc", namespaceURI = "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "cx1", namespaceURI = "http://schemas.microsoft.com/office/drawing/2014/chartex"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "cx2", namespaceURI = "http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "cx4", namespaceURI = "http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"),
    @jakarta.xml.bind.annotation.XmlNs(prefix = "v", namespaceURI = "urn:schemas-microsoft-com:vml") }, elementFormDefault = jakarta.xml.bind.annotation.XmlNsForm.QUALIFIED)
package org.docx4j.mce;
