@jakarta.xml.bind.annotation.XmlSchema(namespace = "http://schemas.microsoft.com/office/word/2016/wordml/cid", xmlns = {
    @jakarta.xml.bind.annotation.XmlNs(prefix = "mc", namespaceURI = "http://schemas.openxmlformats.org/markup-compatibility/2006") },
elementFormDefault = jakarta.xml.bind.annotation.XmlNsForm.QUALIFIED)
package org.docx4j.w16cid;
