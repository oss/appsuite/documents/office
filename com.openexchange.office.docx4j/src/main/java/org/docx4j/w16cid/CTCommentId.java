
package org.docx4j.w16cid;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.HexNumberIntegerAdapter;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
//   <xsd:complexType name="CT_CommentId" >
//     <xsd:attribute name="paraId" type="w12:ST_LongHexNumber"/>
//     <xsd:attribute name="durableId" type="w12:ST_LongHexNumber"/>
//   </xsd:complexType> *
 *
 */
@XmlRootElement(name="commentId")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentId")
public class CTCommentId
{
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    @XmlAttribute(name="paraId", namespace="http://schemas.microsoft.com/office/word/2016/wordml/cid")
    protected Integer paraId;
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    @XmlAttribute(name="durableId", namespace="http://schemas.microsoft.com/office/word/2016/wordml/cid")
    protected Integer durableId;

    /**
     * Gets the value of the paraId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getParaId() {
        return paraId;
    }

    /**
     * Sets the value of the paraId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParaId(Integer value) {
        this.paraId = value;
    }

    /**
     * Gets the value of the paraIdParent property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the paraIdParent property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDurableId(Integer value) {
        this.durableId = value;
    }
}
