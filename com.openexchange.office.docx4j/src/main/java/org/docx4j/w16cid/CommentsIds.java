
package org.docx4j.w16cid;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
//<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:w12="http://schemas.openxmlformats.org/wordprocessingml/2006/main" elementFormDefault="qualified" attributeFormDefault="qualified" blockDefault="#all"
// xmlns="http://schemas.microsoft.com/office/word/2016/wordml/cid" targetNamespace="http://schemas.microsoft.com/office/word/2016/wordml/cid">
//   <xsd:import id="w12" namespace="http://schemas.openxmlformats.org/wordprocessingml/2006/main" schemaLocation="word12.xsd"/>
//   <xsd:complexType name="CT_CommentsIds">
//     <xsd:sequence>
//       <xsd:element name="commentId" type="CT_CommentId" minOccurs="0" maxOccurs="unbounded"/>
//     </xsd:sequence>
//   </xsd:complexType>
// </xsd:schema>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "commentsIds")
public class CommentsIds extends DocumentSerialization implements INodeAccessor<CTCommentId>
{
    static final public String[] CommentIDS_Namespaces = {
        "w16cid", "http://schemas.microsoft.com/office/word/2016/wordml/cid"
    };

    public CommentsIds() {
        super(new QName("http://schemas.microsoft.com/office/word/2016/wordml/cid", "commentsIds", "w16cid"), CommentIDS_Namespaces, "w16cid");
    }
    protected DLList<CTCommentId> commentId;

    @Override
    public DLList<CTCommentId> getContent() {
        if(commentId==null) {
            commentId = new DLList<CTCommentId>();
        }
        return commentId;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                final String localName = reader.getLocalName();
                if("commentId".equals(localName)) {
                    final Object result = documentPart.getUnmarshaller().unmarshal(reader);
                    advanceEvent = reader.getEventType()==XMLStreamReader.END_ELEMENT;
                    if(result instanceof JAXBElement) {
                        getContent().add(((JAXBElement<CTCommentId>)result).getValue());
                    }
                    else if(result instanceof CTCommentId) {
                        getContent().add((CTCommentId)result);
                    }
                }
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        if(commentId!=null) {
            DLNode<CTCommentId> commentIdIter = commentId.getFirstNode();
            while(commentIdIter!=null) {
                marshaller.marshal(commentIdIter.getData(), writer);
                commentIdIter = commentIdIter.getNext();
            }
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }
}
