
package org.docx4j.w16cid;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.docx4j.wml.BooleanDefaultTrue;

@XmlRegistry
public class ObjectFactory {

    private final static QName _CommentId_QNAME = new QName("http://schemas.microsoft.com/office/word/2016/wordml/cid", "commentId");
    
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.docx4j.w15
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTCommentId }
     * 
     */
    public CommentsIds createCommentsIds() {
        return new CommentsIds();
    }

    /**
     * Create an instance of {@link CTCommentId }
     * 
     */
    public CTCommentId createCTCommentId() {
        return new CTCommentId();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanDefaultTrue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2016/wordml/cid", name = "commentId")
    public JAXBElement<CTCommentId> createCTCommentId(CTCommentId value) {
        return new JAXBElement<CTCommentId>(_CommentId_QNAME, CTCommentId.class, null, value);
    }
}
