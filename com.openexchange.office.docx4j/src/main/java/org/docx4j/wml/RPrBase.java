
package org.docx4j.wml;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.w14.CTFillTextEffect;
import org.docx4j.w14.CTGlow;
import org.docx4j.w14.CTLigatures;
import org.docx4j.w14.CTNumForm;
import org.docx4j.w14.CTNumSpacing;
import org.docx4j.w14.CTOnOff;
import org.docx4j.w14.CTProps3D;
import org.docx4j.w14.CTReflection;
import org.docx4j.w14.CTScene3D;
import org.docx4j.w14.CTShadow;
import org.docx4j.w14.CTStylisticSets;
import org.docx4j.w14.CTTextOutlineEffect;

@XmlTransient
@XmlAccessorType(XmlAccessType.PROPERTY)
public abstract class RPrBase implements Cloneable {

    @XmlTransient
    private static final int FLAG_BOOLEAN_b          = 0x1;
    @XmlTransient
    private static final int FLAG_BOOLEAN_bCs        = 0x2;
    @XmlTransient
    private static final int FLAG_BOOLEAN_i          = 0x4;
    @XmlTransient
    private static final int FLAG_BOOLEAN_iCs        = 0x8;
    @XmlTransient
    private static final int FLAG_BOOLEAN_caps       = 0x10;
    @XmlTransient
    private static final int FLAG_BOOLEAN_smallCaps  = 0x20;
    @XmlTransient
    private static final int FLAG_BOOLEAN_strike     = 0x40;
    @XmlTransient
    private static final int FLAG_BOOLEAN_dstrike    = 0x80;
    @XmlTransient
    private static final int FLAG_BOOLEAN_outline    = 0x100;
    @XmlTransient
    private static final int FLAG_BOOLEAN_shadow     = 0x200;
    @XmlTransient
    private static final int FLAG_BOOLEAN_emboss     = 0x400;
    @XmlTransient
    private static final int FLAG_BOOLEAN_imprint    = 0x800;
    @XmlTransient
    private static final int FLAG_BOOLEAN_noProof    = 0x1000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_snapToGrid = 0x2000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_vanish     = 0x4000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_webHidden  = 0x8000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_rtl        = 0x10000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_cs         = 0x20000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_specVanish = 0x40000;
    @XmlTransient
    private static final int FLAG_BOOLEAN_oMath      = 0x80000;

    @XmlTransient
    private int flagsUsed = 0;
    @XmlTransient
    private int flags = 0;

    private final BooleanDefaultTrue getBoolean(int flag) {
        if((flagsUsed & flag)==0) {
            return null;
        }
        return new BooleanDefaultTrue((flags & flag) != 0 ? null : Boolean.FALSE);
    }

    private void setBoolean(int flag, BooleanDefaultTrue v) {
        if(v==null) {
            flagsUsed &=~ flag;
            flags &=~ flag;
        }
        else {
            flagsUsed |= flag;
            if(v.isVal()) {
                flags |= flag;
            }
            else {
                flags &=~ flag;
            }
        }
    }

    /**
     * Gets the value of the b property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "b", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getB() {
        return getBoolean(FLAG_BOOLEAN_b);
    }

    /**
     * Sets the value of the b property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setB(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_b, value);
    }

    /**
     * Gets the value of the bCs property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "bCs", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getbCs() {
        return getBoolean(FLAG_BOOLEAN_bCs);
    }

    /**
     * Sets the value of the bCs property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setbCs(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_bCs, value);
    }

    /**
     * Gets the value of the i property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "i", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getI() {
        return getBoolean(FLAG_BOOLEAN_i);
    }

    /**
     * Sets the value of the i property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setI(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_i, value);
    }

    /**
     * Gets the value of the iCs property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "iCs", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getiCs() {
        return getBoolean(FLAG_BOOLEAN_iCs);
    }

    /**
     * Sets the value of the iCs property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setiCs(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_iCs, value);
    }

    /**
     * Gets the value of the caps property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "caps", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getCaps() {
        return getBoolean(FLAG_BOOLEAN_caps);
    }

    /**
     * Sets the value of the caps property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setCaps(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_caps, value);
    }

    /**
     * Gets the value of the smallCaps property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "smallCaps", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getSmallCaps() {
        return getBoolean(FLAG_BOOLEAN_smallCaps);
    }

    /**
     * Sets the value of the smallCaps property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSmallCaps(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_smallCaps, value);
    }

    /**
     * Gets the value of the strike property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "strike", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getStrike() {
        return getBoolean(FLAG_BOOLEAN_strike);
    }

    /**
     * Sets the value of the strike property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setStrike(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_strike, value);
    }

    /**
     * Gets the value of the dstrike property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "dstrike", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getDstrike() {
        return getBoolean(FLAG_BOOLEAN_dstrike);
    }

    /**
     * Sets the value of the dstrike property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDstrike(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_dstrike, value);
    }

    /**
     * Gets the value of the outline property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "outline", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getOutline() {
        return getBoolean(FLAG_BOOLEAN_outline);
    }

    /**
     * Sets the value of the outline property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setOutline(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_outline, value);
    }

    /**
     * Gets the value of the shadow property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "shadow", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getShadow() {
        return getBoolean(FLAG_BOOLEAN_shadow);
    }

    /**
     * Sets the value of the shadow property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setShadow(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_shadow, value);
    }

    /**
     * Gets the value of the emboss property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "emboss", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getEmboss() {
        return getBoolean(FLAG_BOOLEAN_emboss);
    }

    /**
     * Sets the value of the emboss property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setEmboss(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_emboss, value);
    }

    /**
     * Gets the value of the imprint property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "imprint", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getImprint() {
        return getBoolean(FLAG_BOOLEAN_imprint);
    }

    /**
     * Sets the value of the imprint property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setImprint(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_imprint, value);
    }

    /**
     * Gets the value of the noProof property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "noProof", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getNoProof() {
        return getBoolean(FLAG_BOOLEAN_noProof);
    }

    /**
     * Sets the value of the noProof property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setNoProof(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_noProof, value);
    }

    /**
     * Gets the value of the snapToGrid property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "snapToGrid", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getSnapToGrid() {
        return getBoolean(FLAG_BOOLEAN_snapToGrid);
    }

    /**
     * Sets the value of the snapToGrid property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSnapToGrid(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_snapToGrid, value);
    }

    /**
     * Gets the value of the vanish property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "vanish", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getVanish() {
        return getBoolean(FLAG_BOOLEAN_vanish);
    }

    /**
     * Sets the value of the vanish property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setVanish(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_vanish, value);
    }

    /**
     * Gets the value of the webHidden property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "webHidden", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getWebHidden() {
        return getBoolean(FLAG_BOOLEAN_webHidden);
    }

    /**
     * Sets the value of the webHidden property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setWebHidden(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_webHidden, value);
    }

    /**
     * Gets the value of the rtl property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "rtl", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getRtl() {
        return getBoolean(FLAG_BOOLEAN_rtl);
    }

    /**
     * Sets the value of the rtl property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setRtl(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_rtl, value);
    }

    /**
     * Gets the value of the cs property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "cs", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getCs() {
        return getBoolean(FLAG_BOOLEAN_cs);
    }

    /**
     * Sets the value of the cs property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setCs(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_cs, value);
    }

    /**
     * Gets the value of the specVanish property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "specVanish", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getSpecVanish() {
        return getBoolean(FLAG_BOOLEAN_specVanish);
    }

    /**
     * Sets the value of the specVanish property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSpecVanish(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_specVanish, value);
    }

    /**
     * Gets the value of the oMath property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    @XmlElement(name = "oMath", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public BooleanDefaultTrue getoMath() {
        return getBoolean(FLAG_BOOLEAN_oMath);
    }

    /**
     * Sets the value of the oMath property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setoMath(BooleanDefaultTrue value) {
        setBoolean(FLAG_BOOLEAN_oMath, value);
    }

    //-----------------------------------------------------------------------------------------------------

    @XmlTransient
    private static final int COMMON_FLAG_rStyle          = 0x1;
    @XmlTransient
    private static final int COMMON_FLAG_rFonts          = 0x2;
    @XmlTransient
    private static final int COMMON_FLAG_color           = 0x4;
    @XmlTransient
    private static final int COMMON_FLAG_spacing         = 0x8;
    @XmlTransient
    private static final int COMMON_FLAG_w               = 0x10;
    @XmlTransient
    private static final int COMMON_FLAG_kern            = 0x20;
    @XmlTransient
    private static final int COMMON_FLAG_position        = 0x40;
    @XmlTransient
    private static final int COMMON_FLAG_sz              = 0x80;
    @XmlTransient
    private static final int COMMON_FLAG_szCs            = 0x100;
    @XmlTransient
    private static final int COMMON_FLAG_highlight       = 0x200;
    @XmlTransient
    private static final int COMMON_FLAG_u               = 0x400;
    @XmlTransient
    private static final int COMMON_FLAG_effect          = 0x800;
    @XmlTransient
    private static final int COMMON_FLAG_bdr             = 0x1000;
    @XmlTransient
    private static final int COMMON_FLAG_shd             = 0x2000;
    @XmlTransient
    private static final int COMMON_FLAG_fitText         = 0x4000;
    @XmlTransient
    private static final int COMMON_FLAG_vertAlign       = 0x8000;
    @XmlTransient
    private static final int COMMON_FLAG_em              = 0x10000;
    @XmlTransient
    private static final int COMMON_FLAG_lang            = 0x20000;
    @XmlTransient
    private static final int COMMON_FLAG_eastAsianLayout = 0x40000;

    @XmlTransient
    protected CommonProps commonProps = null;

    @XmlTransient
    public static class CommonProps implements Cloneable {

        @XmlTransient
        protected int commonPropsUsed = 0;
        @XmlTransient
        protected RStyle rStyle;
        @XmlTransient
        protected RFonts rFonts;
        @XmlTransient
        protected Color color;
        @XmlTransient
        protected CTSignedTwipsMeasure spacing;
        @XmlTransient
        protected CTTextScale w;
        @XmlTransient
        protected HpsMeasure kern;
        @XmlTransient
        protected CTSignedHpsMeasure position;
        @XmlTransient
        protected HpsMeasure sz;
        @XmlTransient
        protected HpsMeasure szCs;
        @XmlTransient
        protected Highlight highlight;
        @XmlTransient
        protected U u;
        @XmlTransient
        protected CTTextEffect effect;
        @XmlTransient
        protected CTBorder bdr;
        @XmlTransient
        protected CTShd shd;
        @XmlTransient
        protected CTFitText fitText;
        @XmlTransient
        protected CTVerticalAlignRun vertAlign;
        @XmlTransient
        protected CTEm em;
        @XmlTransient
        protected CTLanguage lang;
        @XmlTransient
        protected CTEastAsianLayout eastAsianLayout;

        @Override
        public CommonProps clone() {

            try {
                final CommonProps clone = (CommonProps)super.clone();
                if(rStyle!=null) {
                    clone.rStyle = rStyle.clone();
                }
                if(rFonts!=null) {
                    clone.rFonts = rFonts.clone();
                }
                if(color!=null) {
                    clone.color = color.clone();
                }
                if(spacing!=null) {
                    clone.spacing = spacing.clone();
                }
                if(w!=null) {
                    clone.w = w.clone();
                }
                if(kern!=null) {
                    clone.kern = kern.clone();
                }
                if(position!=null) {
                    clone.position = position.clone();
                }
                if(sz!=null) {
                    clone.sz = sz.clone();
                }
                if(szCs!=null) {
                    clone.szCs = szCs.clone();
                }
                if(highlight!=null) {
                    clone.highlight = highlight.clone();
                }
                if(u!=null) {
                    clone.u = u.clone();
                }
                if(effect!=null) {
                    clone.effect = effect.clone();
                }
                if(bdr!=null) {
                    clone.bdr = bdr.clone();
                }
                if(shd!=null) {
                    clone.shd = shd.clone();
                }
                if(fitText!=null) {
                    clone.fitText = fitText.clone();
                }
                if(vertAlign!=null) {
                    clone.vertAlign = vertAlign.clone();
                }
                if(em!=null) {
                    clone.em = em.clone();
                }
                if(lang!=null) {
                    clone.lang = lang.clone();
                }
                if(eastAsianLayout!=null) {
                    clone.eastAsianLayout = eastAsianLayout.clone();
                }
                return clone;
            } catch (CloneNotSupportedException e) {
                return null;
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + commonPropsUsed;
            if (commonPropsUsed!=0) {
                result = prime * result + ((bdr == null) ? 0 : bdr.hashCode());
                result = prime * result + ((color == null) ? 0 : color.hashCode());
                result = prime * result + ((eastAsianLayout == null) ? 0 : eastAsianLayout.hashCode());
                result = prime * result + ((effect == null) ? 0 : effect.hashCode());
                result = prime * result + ((em == null) ? 0 : em.hashCode());
                result = prime * result + ((fitText == null) ? 0 : fitText.hashCode());
                result = prime * result + ((highlight == null) ? 0 : highlight.hashCode());
                result = prime * result + ((kern == null) ? 0 : kern.hashCode());
                result = prime * result + ((lang == null) ? 0 : lang.hashCode());
                result = prime * result + ((position == null) ? 0 : position.hashCode());
                result = prime * result + ((rFonts == null) ? 0 : rFonts.hashCode());
                result = prime * result + ((rStyle == null) ? 0 : rStyle.hashCode());
                result = prime * result + ((shd == null) ? 0 : shd.hashCode());
                result = prime * result + ((spacing == null) ? 0 : spacing.hashCode());
                result = prime * result + ((sz == null) ? 0 : sz.hashCode());
                result = prime * result + ((szCs == null) ? 0 : szCs.hashCode());
                result = prime * result + ((u == null) ? 0 : u.hashCode());
                result = prime * result + ((vertAlign == null) ? 0 : vertAlign.hashCode());
                result = prime * result + ((w == null) ? 0 : w.hashCode());
            }
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CommonProps other = (CommonProps) obj;
            if (commonPropsUsed != other.commonPropsUsed) {
                return false;
            }
            if (bdr == null) {
                if (other.bdr != null)
                    return false;
            } else if (!bdr.equals(other.bdr))
                return false;
            if (color == null) {
                if (other.color != null)
                    return false;
            } else if (!color.equals(other.color))
                return false;
            if (eastAsianLayout == null) {
                if (other.eastAsianLayout != null)
                    return false;
            } else if (!eastAsianLayout.equals(other.eastAsianLayout))
                return false;
            if (effect == null) {
                if (other.effect != null)
                    return false;
            } else if (!effect.equals(other.effect))
                return false;
            if (em == null) {
                if (other.em != null)
                    return false;
            } else if (!em.equals(other.em))
                return false;
            if (fitText == null) {
                if (other.fitText != null)
                    return false;
            } else if (!fitText.equals(other.fitText))
                return false;
            if (highlight == null) {
                if (other.highlight != null)
                    return false;
            } else if (!highlight.equals(other.highlight))
                return false;
            if (kern == null) {
                if (other.kern != null)
                    return false;
            } else if (!kern.equals(other.kern))
                return false;
            if (lang == null) {
                if (other.lang != null)
                    return false;
            } else if (!lang.equals(other.lang))
                return false;
            if (position == null) {
                if (other.position != null)
                    return false;
            } else if (!position.equals(other.position))
                return false;
            if (rFonts == null) {
                if (other.rFonts != null)
                    return false;
            } else if (!rFonts.equals(other.rFonts))
                return false;
            if (rStyle == null) {
                if (other.rStyle != null)
                    return false;
            } else if (!rStyle.equals(other.rStyle))
                return false;
            if (shd == null) {
                if (other.shd != null)
                    return false;
            } else if (!shd.equals(other.shd))
                return false;
            if (spacing == null) {
                if (other.spacing != null)
                    return false;
            } else if (!spacing.equals(other.spacing))
                return false;
            if (sz == null) {
                if (other.sz != null)
                    return false;
            } else if (!sz.equals(other.sz))
                return false;
            if (szCs == null) {
                if (other.szCs != null)
                    return false;
            } else if (!szCs.equals(other.szCs))
                return false;
            if (u == null) {
                if (other.u != null)
                    return false;
            } else if (!u.equals(other.u))
                return false;
            if (vertAlign == null) {
                if (other.vertAlign != null)
                    return false;
            } else if (!vertAlign.equals(other.vertAlign))
                return false;
            if (w == null) {
                if (other.w != null)
                    return false;
            } else if (!w.equals(other.w))
                return false;
            return true;
        }

        public boolean isEmpty() {
            return commonPropsUsed == 0;
        }
    }

    private void update_CommonProps(int flag, Object v) {
        if(v==null) {
            if(commonProps!=null) {
                commonProps.commonPropsUsed &=~ flag;
                if(commonProps.commonPropsUsed==0) {
                    commonProps = null;
                }
            }
        }
        else {
            if(commonProps==null) {
                commonProps = new CommonProps();
            }
            commonProps.commonPropsUsed |= flag;
        }
    }

    /**
     * Gets the value of the rStyle property.
     *
     * @return
     *     possible object is
     *     {@link RStyle }
     *
     */
    @XmlElement(name = "rStyle", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public RStyle getrStyle() {
        return commonProps!=null ? commonProps.rStyle : null;
    }

    public RStyle getrStyle(boolean forceCreate) {
        RStyle rStyle = getrStyle();
        if(rStyle==null&&forceCreate) {
            rStyle = new RStyle();
            setrStyle(rStyle);
        }
        return rStyle;
    }

    /**
     * Sets the value of the rStyle property.
     *
     * @param value
     *     allowed object is
     *     {@link RStyle }
     *
     */
    public void setrStyle(RStyle rStyle) {
        update_CommonProps(COMMON_FLAG_rStyle, rStyle);
        if(commonProps!=null) {
            commonProps.rStyle = rStyle;
        }
    }

    /**
     * Gets the value of the rFonts property.
     *
     * @return
     *     possible object is
     *     {@link RFonts }
     *
     */
    @XmlElement(name = "rFonts", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public RFonts getrFonts() {
        return commonProps!=null ? commonProps.rFonts : null;
    }

    public RFonts getrFonts(boolean forceCreate) {
        RFonts rFonts = getrFonts();
        if(rFonts==null&&forceCreate) {
            rFonts = new RFonts();
            setrFonts(rFonts);
        }
        return rFonts;
    }

    /**
     * Sets the value of the rFonts property.
     *
     * @param value
     *     allowed object is
     *     {@link RFonts }
     *
     */
    public void setrFonts(RFonts rFonts) {
        update_CommonProps(COMMON_FLAG_rFonts, rFonts);
        if(commonProps!=null) {
            commonProps.rFonts = rFonts;
        }
    }

    /**
     * Gets the value of the color property.
     *
     * @return
     *     possible object is
     *     {@link Color }
     *
     */
    @XmlElement(name = "color", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public Color getColor() {
        return commonProps!=null ? commonProps.color : null;
    }

    public Color getColor(boolean forceCreate) {
        Color color = getColor();
        if(color==null&&forceCreate) {
            color = new Color();
            setColor(color);
        }
        return color;
    }

    /**
     * Sets the value of the color property.
     *
     * @param value
     *     allowed object is
     *     {@link Color }
     *
     */
    public void setColor(Color color) {
        update_CommonProps(COMMON_FLAG_color, color);
        if(commonProps!=null) {
            commonProps.color = color;
        }
    }

    /**
     * Gets the value of the spacing property.
     *
     * @return
     *     possible object is
     *     {@link CTSignedTwipsMeasure }
     *
     */
    @XmlElement(name = "spacing", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTSignedTwipsMeasure getSpacing() {
        return commonProps!=null ? commonProps.spacing : null;
    }

    /**
     * Sets the value of the spacing property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSignedTwipsMeasure }
     *
     */
    public void setSpacing(CTSignedTwipsMeasure spacing) {
        update_CommonProps(COMMON_FLAG_color, spacing);
        if(commonProps!=null) {
            commonProps.spacing = spacing;
        }
    }

    /**
     * Gets the value of the w property.
     *
     * @return
     *     possible object is
     *     {@link CTTextScale }
     *
     */
    @XmlElement(name = "w", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTTextScale getW() {
        return commonProps!=null ? commonProps.w : null;
    }

    /**
     * Sets the value of the w property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTextScale }
     *
     */
    public void setW(CTTextScale w) {
        update_CommonProps(COMMON_FLAG_w, w);
        if(commonProps!=null) {
            commonProps.w = w;
        }
    }

    /**
     * Gets the value of the kern property.
     *
     * @return
     *     possible object is
     *     {@link HpsMeasure }
     *
     */
    @XmlElement(name = "kern", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public HpsMeasure getKern() {
        return commonProps!=null ? commonProps.kern : null;
    }

    /**
     * Sets the value of the kern property.
     *
     * @param value
     *     allowed object is
     *     {@link HpsMeasure }
     *
     */
    public void setKern(HpsMeasure kern) {
        update_CommonProps(COMMON_FLAG_kern, kern);
        if(commonProps!=null) {
            commonProps.kern = kern;
        }
    }

    /**
     * Gets the value of the position property.
     *
     * @return
     *     possible object is
     *     {@link CTSignedHpsMeasure }
     *
     */
    @XmlElement(name = "position", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTSignedHpsMeasure getPosition() {
        return commonProps!=null ? commonProps.position : null;
    }

    /**
     * Sets the value of the position property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSignedHpsMeasure }
     *
     */
    public void setPosition(CTSignedHpsMeasure position) {
        update_CommonProps(COMMON_FLAG_position, position);
        if(commonProps!=null) {
            commonProps.position = position;
        }
    }

    /**
     * Gets the value of the sz property.
     *
     * @return
     *     possible object is
     *     {@link HpsMeasure }
     *
     */
    @XmlElement(name = "sz", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public HpsMeasure getSz() {
        return commonProps!=null ? commonProps.sz : null;
    }

    /**
     * Sets the value of the sz property.
     *
     * @param value
     *     allowed object is
     *     {@link HpsMeasure }
     *
     */
    public void setSz(HpsMeasure sz) {
        update_CommonProps(COMMON_FLAG_sz, sz);
        if(commonProps!=null) {
            commonProps.sz = sz;
        }
    }

    /**
     * Gets the value of the szCs property.
     *
     * @return
     *     possible object is
     *     {@link HpsMeasure }
     *
     */
    @XmlElement(name = "szCs", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public HpsMeasure getSzCs() {
        return commonProps!=null ? commonProps.szCs : null;
    }

    /**
     * Sets the value of the szCs property.
     *
     * @param value
     *     allowed object is
     *     {@link HpsMeasure }
     *
     */
    public void setSzCs(HpsMeasure szCs) {
        update_CommonProps(COMMON_FLAG_szCs, szCs);
        if(commonProps!=null) {
            commonProps.szCs = szCs;
        }
    }

    /**
     * Gets the value of the highlight property.
     *
     * @return
     *     possible object is
     *     {@link Highlight }
     *
     */
    @XmlElement(name = "highlight", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public Highlight getHighlight() {
        return commonProps!=null ? commonProps.highlight : null;
    }

    /**
     * Sets the value of the highlight property.
     *
     * @param value
     *     allowed object is
     *     {@link Highlight }
     *
     */
    public void setHighlight(Highlight highlight) {
        update_CommonProps(COMMON_FLAG_highlight, highlight);
        if(commonProps!=null) {
            commonProps.highlight = highlight;
        }
    }

    /**
     * Gets the value of the u property.
     *
     * @return
     *     possible object is
     *     {@link U }
     *
     */
    @XmlElement(name = "u", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public U getU() {
        return commonProps!=null ? commonProps.u : null;
    }

    /**
     * Sets the value of the u property.
     *
     * @param value
     *     allowed object is
     *     {@link U }
     *
     */
    public void setU(U u) {
        update_CommonProps(COMMON_FLAG_u, u);
        if(commonProps!=null) {
            commonProps.u = u;
        }
    }

    /**
     * Gets the value of the effect property.
     *
     * @return
     *     possible object is
     *     {@link CTTextEffect }
     *
     */
    @XmlElement(name = "effect", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTTextEffect getEffect() {
        return commonProps!=null ? commonProps.effect : null;
    }

    /**
     * Sets the value of the effect property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTextEffect }
     *
     */
    public void setEffect(CTTextEffect effect) {
        update_CommonProps(COMMON_FLAG_effect, effect);
        if(commonProps!=null) {
            commonProps.effect = effect;
        }
    }

    /**
     * Gets the value of the bdr property.
     *
     * @return
     *     possible object is
     *     {@link CTBorder }
     *
     */
    @XmlElement(name = "bdr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTBorder getBdr() {
        return commonProps!=null ? commonProps.bdr : null;
    }

    /**
     * Sets the value of the bdr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBorder }
     *
     */
    public void setBdr(CTBorder bdr) {
        update_CommonProps(COMMON_FLAG_bdr, bdr);
        if(commonProps!=null) {
            commonProps.bdr = bdr;
        }
    }

    /**
     * Gets the value of the shd property.
     *
     * @return
     *     possible object is
     *     {@link CTShd }
     *
     */
    @XmlElement(name = "shd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTShd getShd() {
        return commonProps!=null ? commonProps.shd : null;
    }

    public CTShd getShd(boolean forceCreate) {
        CTShd shd = getShd();
        if(shd==null&&forceCreate) {
            shd = new CTShd();
            setShd(shd);
        }
        return shd;
    }

    /**
     * Sets the value of the shd property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShd }
     *
     */
    public void setShd(CTShd shd) {
        update_CommonProps(COMMON_FLAG_shd, shd);
        if(commonProps!=null) {
            commonProps.shd = shd;
        }
    }

    /**
     * Gets the value of the fitText property.
     *
     * @return
     *     possible object is
     *     {@link CTFitText }
     *
     */
    @XmlElement(name = "fitText", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTFitText getFitText() {
        return commonProps!=null ? commonProps.fitText : null;
    }

    /**
     * Sets the value of the fitText property.
     *
     * @param value
     *     allowed object is
     *     {@link CTFitText }
     *
     */
    public void setFitText(CTFitText fitText) {
        update_CommonProps(COMMON_FLAG_fitText, fitText);
        if(commonProps!=null) {
            commonProps.fitText = fitText;
        }
    }

    /**
     * Gets the value of the vertAlign property.
     *
     * @return
     *     possible object is
     *     {@link CTVerticalAlignRun }
     *
     */
    @XmlElement(name = "vertAlign", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTVerticalAlignRun getVertAlign() {
        return commonProps!=null ? commonProps.vertAlign : null;
    }

    /**
     * Sets the value of the vertAlign property.
     *
     * @param value
     *     allowed object is
     *     {@link CTVerticalAlignRun }
     *
     */
    public void setVertAlign(CTVerticalAlignRun vertAlign) {
        update_CommonProps(COMMON_FLAG_vertAlign, vertAlign);
        if(commonProps!=null) {
            commonProps.vertAlign = vertAlign;
        }
    }

    /**
     * Gets the value of the em property.
     *
     * @return
     *     possible object is
     *     {@link CTEm }
     *
     */
    @XmlElement(name = "em", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTEm getEm() {
        return commonProps!=null ? commonProps.em : null;
    }

    /**
     * Sets the value of the em property.
     *
     * @param value
     *     allowed object is
     *     {@link CTEm }
     *
     */
    public void setEm(CTEm em) {
        update_CommonProps(COMMON_FLAG_em, em);
        if(commonProps!=null) {
            commonProps.em = em;
        }
    }

    /**
     * Gets the value of the lang property.
     *
     * @return
     *     possible object is
     *     {@link CTLanguage }
     *
     */
    @XmlElement(name = "lang", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTLanguage getLang() {
        return commonProps!=null ? commonProps.lang : null;
    }

    public CTLanguage getLang(boolean forceCreate) {
        CTLanguage lang = getLang();
        if(lang==null&&forceCreate) {
            lang = new CTLanguage();
            setLang(lang);
        }
        return lang;
    }

    /**
     * Sets the value of the lang property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLanguage }
     *
     */
    public void setLang(CTLanguage lang) {
        update_CommonProps(COMMON_FLAG_lang, lang);
        if(commonProps!=null) {
            commonProps.lang = lang;
        }
    }

    /**
     * Gets the value of the eastAsianLayout property.
     *
     * @return
     *     possible object is
     *     {@link CTEastAsianLayout }
     *
     */
    @XmlElement(name = "eastAsianLayout", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public CTEastAsianLayout getEastAsianLayout() {
        return commonProps!=null ? commonProps.eastAsianLayout : null;
    }

    /**
     * Sets the value of the eastAsianLayout property.
     *
     * @param value
     *     allowed object is
     *     {@link CTEastAsianLayout }
     *
     */
    public void setEastAsianLayout(CTEastAsianLayout eastAsianLayout) {
        update_CommonProps(COMMON_FLAG_eastAsianLayout, eastAsianLayout);
        if(commonProps!=null) {
            commonProps.eastAsianLayout = eastAsianLayout;
        }
    }

    //-----------------------------------------------------------------------------------------------------

    @XmlTransient
    private static final int W14_FLAG_glow          = 0x1;
    @XmlTransient
    private static final int W14_FLAG_shadow2       = 0x2;
    @XmlTransient
    private static final int W14_FLAG_reflection    = 0x4;
    @XmlTransient
    private static final int W14_FLAG_textOutline   = 0x8;
    @XmlTransient
    private static final int W14_FLAG_textFill      = 0x10;
    @XmlTransient
    private static final int W14_FLAG_scene3d       = 0x20;
    @XmlTransient
    private static final int W14_FLAG_props3d       = 0x40;
    @XmlTransient
    private static final int W14_FLAG_ligatures     = 0x80;
    @XmlTransient
    private static final int W14_FLAG_numForm       = 0x100;
    @XmlTransient
    private static final int W14_FLAG_numSpacing    = 0x200;
    @XmlTransient
    private static final int W14_FLAG_stylisticSets = 0x400;
    @XmlTransient
    private static final int W14_FLAG_cntxtAlts     = 0x800;

    @XmlTransient
    protected w14Props w14Props = null;

    @XmlTransient
    public static class w14Props implements Cloneable {

        @XmlTransient
        protected int w14PropsUsed = 0;
        @XmlTransient
        protected CTGlow glow;
        @XmlTransient
        protected CTShadow shadow2;
        @XmlTransient
        protected CTReflection reflection;
        @XmlTransient
        protected CTTextOutlineEffect textOutline;
        @XmlTransient
        protected CTFillTextEffect textFill;
        @XmlTransient
        protected CTScene3D scene3d;
        @XmlTransient
        protected CTProps3D props3d;
        @XmlTransient
        protected CTLigatures ligatures;
        @XmlTransient
        protected CTNumForm numForm;
        @XmlTransient
        protected CTNumSpacing numSpacing;
        @XmlTransient
        protected CTStylisticSets stylisticSets;
        @XmlTransient
        protected CTOnOff cntxtAlts;

        @Override
        public w14Props clone() {
            try {

                final w14Props clone = (w14Props)super.clone();
/*
 * shallow copy is sufficient, we do not change w14 attributes
 *
                if(glow!=null) {
                    clone.glow = glow.clone();
                }
                if(shadow2!=null) {
                    clone.shadow2 = shadow2.clone();
                }
                if(reflection!=null) {
                    clone.reflection = reflection.clone();
                }
                if(textOutline!=null) {
                    clone.textOutline = textOutline.clone();
                }
                if(textFill!=null) {
                    clone.textFill = textFill.clone();
                }
                if(scene3d!=null) {
                    clone.scene3d = scene3d.clone();
                }
                if(props3d!=null) {
                    clone.props3d = props3d.clone();
                }
                if(ligatures!=null) {
                    clone.ligatures = ligatures.clone();
                }
                if(numForm!=null) {
                    clone.numForm = numForm.clone();
                }
                if(numSpacing!=null) {
                    clone.numSpacing = numSpacing.clone();
                }
                if(stylisticSets!=null) {
                    clone.stylisticSets = stylisticSets.clone();
                }
                if(cntxtAlts!=null) {
                    clone.cntxtAlts = cntxtAlts.clone();
                }
*/
                return clone;
            } catch (CloneNotSupportedException e) {
                return null;
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + w14PropsUsed;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            w14Props other = (w14Props) obj;
            if (w14PropsUsed != other.w14PropsUsed) {
                return false;
            }
            if (glow != other.glow) {
                return false;
            }
            if (shadow2 != other.shadow2) {
                return false;
            }
            if (reflection != other.reflection) {
                return false;
            }
            if (textOutline != other.textOutline) {
                return false;
            }
            if (textFill != other.textFill) {
                return false;
            }
            if (scene3d != other.scene3d) {
                return false;
            }
            if (props3d != other.props3d) {
                return false;
            }
            if (ligatures != other.ligatures) {
                return false;
            }
            if (numForm != other.numForm) {
                return false;
            }
            if (numSpacing != other.numSpacing) {
                return false;
            }
            if (stylisticSets != other.stylisticSets) {
                return false;
            }
            if (cntxtAlts != other.cntxtAlts) {
                return false;
            }
            return true;
        }

        public boolean isEmpty() {
            return w14PropsUsed == 0;
        }
    }

    private void update_W14Props(int flag, Object v) {
        if(v==null) {
            if(w14Props!=null) {
                w14Props.w14PropsUsed &=~ flag;
                if(w14Props.w14PropsUsed==0) {
                    w14Props = null;
                }
            }
        }
        else {
            if(w14Props==null) {
                w14Props = new w14Props();
            }
            w14Props.w14PropsUsed |= flag;
        }
    }

    @XmlElement(name= "glow", namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTGlow getGlow() {
        return w14Props!=null ? w14Props.glow : null;
    }

    public void setGlow(CTGlow glow) {
        update_W14Props(W14_FLAG_glow, glow);
        if(w14Props!=null) {
            w14Props.glow = glow;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false, name = "shadow")
    public CTShadow getShadow2() {
        return w14Props!=null ? w14Props.shadow2 : null;
    }

    public void setShadow2(CTShadow shadow2) {
        update_W14Props(W14_FLAG_shadow2, shadow2);
        if(w14Props!=null) {
            w14Props.shadow2 = shadow2;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTReflection getReflection() {
        return w14Props!=null ? w14Props.reflection : null;
    }

    public void setReflection(CTReflection reflection) {
        update_W14Props(W14_FLAG_reflection, reflection);
        if(w14Props!=null) {
            w14Props.reflection = reflection;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTTextOutlineEffect getTextOutline() {
        return w14Props!=null ? w14Props.textOutline : null;
    }

    public void setTextOutline(CTTextOutlineEffect textOutline) {
        update_W14Props(W14_FLAG_textOutline, textOutline);
        if(w14Props!=null) {
            w14Props.textOutline = textOutline;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTFillTextEffect getTextFill() {
        return w14Props!=null ? w14Props.textFill : null;
    }

    public void setTextFill(CTFillTextEffect textFill) {
        update_W14Props(W14_FLAG_textFill, textFill);
        if(w14Props!=null) {
            w14Props.textFill = textFill;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTScene3D getScene3d() {
        return w14Props!=null ? w14Props.scene3d : null;
    }

    public void setScene3d(CTScene3D scene3d) {
        update_W14Props(W14_FLAG_scene3d, scene3d);
        if(w14Props!=null) {
            w14Props.scene3d = scene3d;
        }
    }

    @XmlElement(name = "props3d", namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTProps3D getprops3d() {
        return w14Props!=null ? w14Props.props3d : null;
    }

    public void setprops3d(CTProps3D props3d) {
        update_W14Props(W14_FLAG_props3d, props3d);
        if(w14Props!=null) {
            w14Props.props3d = props3d;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTLigatures getLigatures() {
        return w14Props!=null ? w14Props.ligatures : null;
    }

    public void setLigatures(CTLigatures ligatures) {
        update_W14Props(W14_FLAG_ligatures, ligatures);
        if(w14Props!=null) {
            w14Props.ligatures = ligatures;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTNumForm getNumForm() {
        return w14Props!=null ? w14Props.numForm : null;
    }

    public void setNumForm(CTNumForm numForm) {
        update_W14Props(W14_FLAG_numForm, numForm);
        if(w14Props!=null) {
            w14Props.numForm = numForm;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTNumSpacing getNumSpacing() {
        return w14Props!=null ? w14Props.numSpacing : null;
    }

    public void setNumSpacing(CTNumSpacing numSpacing) {
        update_W14Props(W14_FLAG_numSpacing, numSpacing);
        if(w14Props!=null) {
            w14Props.numSpacing = numSpacing;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTStylisticSets getStylisticSets() {
        return w14Props!=null ? w14Props.stylisticSets : null;
    }

    public void setStylisticSets(CTStylisticSets stylisticSets) {
        update_W14Props(W14_FLAG_stylisticSets, stylisticSets);
        if(w14Props!=null) {
            w14Props.stylisticSets = stylisticSets;
        }
    }

    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml", required = false)
    public CTOnOff getCntxtAlts() {
        return w14Props!=null ? w14Props.cntxtAlts : null;
    }

    public void setCntxtAlts(CTOnOff cntxtAlts) {
        update_W14Props(W14_FLAG_cntxtAlts, cntxtAlts);
        if(w14Props!=null) {
            w14Props.cntxtAlts = cntxtAlts;
        }
    }

    final public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
    	if(getLang()!=null&&getLang().isEmpty()) {
    		setLang(null);
    	}
    }

    public static void cloneRPrBase(RPrBase rDest, RPrBase rSource) {
        rDest.flagsUsed = rSource.flagsUsed;
        rDest.flags = rSource.flags;

        rDest.commonProps = rSource.commonProps!=null ? rSource.commonProps.clone() : null;

        rDest.w14Props = rSource.w14Props!=null ? rSource.w14Props.clone() : null;
    }

    @Override
    public RPrBase clone() {
        try {
            final RPrBase clone = (RPrBase)super.clone();
            if(commonProps!=null) {
                clone.commonProps = commonProps.clone();
            }
            if(w14Props!=null) {
                clone.w14Props = w14Props.clone();
            }
            return clone;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commonProps == null) ? 0 : commonProps.hashCode());
        result = prime * result + flags;
        result = prime * result + flagsUsed;
        result = prime * result + ((w14Props == null) ? 0 : w14Props.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RPrBase other = (RPrBase) obj;
        if (flags != other.flags)
            return false;
        if (flagsUsed != other.flagsUsed)
            return false;
        if (commonProps == null) {
            if (other.commonProps != null)
                return false;
        } else if (!commonProps.equals(other.commonProps))
            return false;
        if (w14Props == null) {
            if (other.w14Props != null)
                return false;
        } else if (!w14Props.equals(other.w14Props))
            return false;
        return true;
    }

    public boolean isEmpty() {
        return (flagsUsed == 0) && (commonProps == null || commonProps.isEmpty()) && (w14Props == null || w14Props.isEmpty());
    }
}
