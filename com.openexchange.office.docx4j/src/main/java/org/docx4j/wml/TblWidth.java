/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.LongAdapter;


/**
 * <p>Java class for CT_TblWidth complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_TblWidth">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="w" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_DecimalNumber" />
 *       &lt;attribute name="type">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="nil"/>
 *             &lt;enumeration value="pct"/>
 *             &lt;enumeration value="dxa"/>
 *             &lt;enumeration value="auto"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TblWidth")
public class TblWidth {

	public final static String TYPE_AUTO = "auto";
	public final static String TYPE_DXA = "dxa"; // twips
	public final static String TYPE_PCT = "pct"; // percentage (0-5000)

	// w is specified as integer but many applications are writing floats, so we will accept floats but will
	// write create non float via setW only
    @XmlAttribute(name = "w", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    @XmlJavaTypeAdapter(LongAdapter.class)
    protected Long w;
    @XmlAttribute(name = "type", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected String type;

    protected TblWidth() {
        // default constructor for JAXB
    }

    public TblWidth(String type, long width) {
        this.type = type;
        this.w = Long.valueOf(width);
    }

    /**
     * Gets the value of the w property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getW() {
        return w;
    }

    /**
     * Sets the value of the w property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setW(Long value) {
        w = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }
}
