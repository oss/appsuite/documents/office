
package org.docx4j.wml;

import com.openexchange.office.filter.core.component.Child;

/**
 * Get the list of Series from a generic graphic.
 */
public interface IText extends Child {

	public String getValue();
    public void setValue(String value);
    public String getSpace();
    public void setSpace(String value);
    public IText createText();
}
