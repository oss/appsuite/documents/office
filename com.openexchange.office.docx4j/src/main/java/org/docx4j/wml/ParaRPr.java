/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_ParaRPr complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_ParaRPr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_ParaRPrTrackChanges" minOccurs="0"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_RPrBase" minOccurs="0"/>
 *         &lt;element name="rPrChange" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ParaRPrChange" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ParaRPr", propOrder = {
    "ins",
    "del",
    "moveFrom",
    "moveTo",
    "rStyle",
    "rFonts",
    "b",
    "bCs",
    "i",
    "iCs",
    "caps",
    "smallCaps",
    "strike",
    "dstrike",
    "outline",
    "shadow",
    "emboss",
    "imprint",
    "noProof",
    "snapToGrid",
    "vanish",
    "webHidden",
    "color",
    "spacing",
    "w",
    "kern",
    "position",
    "sz",
    "szCs",
    "highlight",
    "u",
    "effect",
    "bdr",
    "shd",
    "fitText",
    "vertAlign",
    "rtl",
    "cs",
    "em",
    "lang",
    "eastAsianLayout",
    "specVanish",
    "oMath",
    "glow",
    "shadow2",
    "reflection",
    "textOutline",
    "textFill",
    "scene3d",
    "props3d",
    "ligatures",
    "numForm",
    "numSpacing",
    "stylisticSets",
    "cntxtAlts",
    "rPrChange"
})
public class ParaRPr extends RPrBase
{
    protected CTTrackChange ins;
    protected CTTrackChange del;
    protected CTTrackChange moveFrom;
    protected CTTrackChange moveTo;
    protected ParaRPrChange rPrChange;

    /**
     * Gets the value of the ins property.
     *
     * @return
     *     possible object is
     *     {@link CTTrackChange }
     *
     */
    public CTTrackChange getIns(boolean forceCreate) {
        if(ins==null&&forceCreate) {
            ins = new CTTrackChange();
        }
        return ins;
    }

    /**
     * Sets the value of the ins property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTrackChange }
     *
     */
    public void setIns(CTTrackChange value) {
        this.ins = value;
    }

    /**
     * Gets the value of the del property.
     *
     * @return
     *     possible object is
     *     {@link CTTrackChange }
     *
     */
    public CTTrackChange getDel(boolean forceCreate) {
        if(del==null&&forceCreate) {
            del = new CTTrackChange();
        }
        return del;
    }

    /**
     * Sets the value of the del property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTrackChange }
     *
     */
    public void setDel(CTTrackChange value) {
        this.del = value;
    }

    /**
     * Gets the value of the moveFrom property.
     *
     * @return
     *     possible object is
     *     {@link CTTrackChange }
     *
     */
    public CTTrackChange getMoveFrom(boolean forceCreate) {
        if(moveFrom==null&&forceCreate) {
            moveFrom = new CTTrackChange();
        }
        return moveFrom;
    }

    /**
     * Sets the value of the moveFrom property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTrackChange }
     *
     */
    public void setMoveFrom(CTTrackChange value) {
        this.moveFrom = value;
    }

    /**
     * Gets the value of the moveTo property.
     *
     * @return
     *     possible object is
     *     {@link CTTrackChange }
     *
     */
    public CTTrackChange getMoveTo(boolean forceCreate) {
        if(moveTo==null&&forceCreate) {
            moveTo = new CTTrackChange();
        }
        return moveTo;
    }

    /**
     * Sets the value of the moveTo property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTrackChange }
     *
     */
    public void setMoveTo(CTTrackChange value) {
        this.moveTo = value;
    }

    /**
     * Gets the value of the rPrChange property.
     *
     * @return
     *     possible object is
     *     {@link ParaRPrChange }
     *
     */
    public ParaRPrChange getRPrChange(boolean forceCreate) {
        if(rPrChange==null&&forceCreate) {
            rPrChange = new ParaRPrChange();
        }
        return rPrChange;
    }

    /**
     * Sets the value of the rPrChange property.
     *
     * @param value
     *     allowed object is
     *     {@link ParaRPrChange }
     *
     */
    public void setRPrChange(ParaRPrChange value) {
        this.rPrChange = value;
    }

    public static ParaRPr cloneFromRPrBase(RPrBase rSource) {
        if(rSource==null) {
            return null;
        }
        final ParaRPr rPr = new ParaRPr();
        RPrBase.cloneRPrBase(rPr, rSource);
        return rPr;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((del == null) ? 0 : del.hashCode());
        result = prime * result + ((ins == null) ? 0 : ins.hashCode());
        result = prime * result + ((moveFrom == null) ? 0 : moveFrom.hashCode());
        result = prime * result + ((moveTo == null) ? 0 : moveTo.hashCode());
        result = prime * result + ((rPrChange == null) ? 0 : rPrChange.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ParaRPr other = (ParaRPr) obj;
        if (del == null) {
            if (other.del != null)
                return false;
        } else if (!del.equals(other.del))
            return false;
        if (ins == null) {
            if (other.ins != null)
                return false;
        } else if (!ins.equals(other.ins))
            return false;
        if (moveFrom == null) {
            if (other.moveFrom != null)
                return false;
        } else if (!moveFrom.equals(other.moveFrom))
            return false;
        if (moveTo == null) {
            if (other.moveTo != null)
                return false;
        } else if (!moveTo.equals(other.moveTo))
            return false;
        if (rPrChange == null) {
            if (other.rPrChange != null)
                return false;
        } else if (!rPrChange.equals(other.rPrChange))
            return false;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return isEmpty() && del == null && ins == null && moveFrom == null && moveTo == null && rPrChange == null;
    }
}
