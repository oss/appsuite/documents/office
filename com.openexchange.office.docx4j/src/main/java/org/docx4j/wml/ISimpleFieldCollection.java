
package org.docx4j.wml;

import java.util.List;
import com.openexchange.office.filter.core.DLList;

public interface ISimpleFieldCollection {

	List<CTSimpleField> getSimpleFieldCollection(boolean forceCreate);

    // returns content without field conversion
    public DLList<Object> getRawContent();
}
