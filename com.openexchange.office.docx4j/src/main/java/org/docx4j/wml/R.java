/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.IntegerAdapter;
import org.docx4j.mce.AlternateContent;
import org.docx4j.mce.AlternateContent.Choice;
import org.docx4j.mce.AlternateContent.Fallback;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IChildNodeCombiner;
import com.openexchange.office.filter.core.component.Child;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_RPr" minOccurs="0"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_RunInnerContent" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="rsidRPr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidDel" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidR" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rPr",
    "content"
})
@XmlRootElement(name = "r")
public class R implements Child, IChildNodeCombiner, INodeAccessor<Object> {

    protected RPr rPr;
    @XmlElementRefs({
        @XmlElementRef(name = "ptab", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = Ptab.class),
        @XmlElementRef(name = "yearLong", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "dayShort", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "noBreakHyphen", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "endnoteRef", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "pgNum", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "softHyphen", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "pict", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = Pict.class),
        @XmlElementRef(name = "tab", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = R.Tab.class),
        @XmlElementRef(name = "drawing", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = Drawing.class),
        @XmlElementRef(name = "br", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = Br.class),
        @XmlElementRef(name = "separator", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "lastRenderedPageBreak", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "t", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = Text.class),
        @XmlElementRef(name = "object", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "endnoteReference", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "instrText", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = InstrText.class),
        @XmlElementRef(name = "footnoteReference", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "monthShort", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "monthLong", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "continuationSeparator", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "ruby", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "annotationRef", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "cr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "yearShort", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "fldChar", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = FldChar.class),
        @XmlElementRef(name = "sym", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "commentReference", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = R.CommentReference.class),
        @XmlElementRef(name = "delInstrText", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = DelInstrText.class),
        @XmlElementRef(name = "footnoteRef", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "dayLong", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "delText", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = DelText.class),
        @XmlElementRef(name = "alternateContent", namespace = "http://schemas.openxmlformats.org/markup-compatibility/2006", type = AlternateContent.class)
    })
    protected DLList<Object> content;

    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the rPr property.
     *
     * @return
     *     possible object is
     *     {@link RPr }
     *
     */
    public RPr getRPr(boolean forceCreate) {
        if(rPr==null && forceCreate) {
            rPr = new RPr();
        }
        return rPr;
    }

    /**
     * Sets the value of the rPr property.
     *
     * @param value
     *     allowed object is
     *     {@link RPr }
     *
     */
    public void setRPr(RPr value) {
        this.rPr = value;
    }

    /**
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link R.Ptab }{@code >}
     * {@link JAXBElement }{@code <}{@link R.YearLong }{@code >}
     * {@link JAXBElement }{@code <}{@link R.DayShort }{@code >}
     * {@link JAXBElement }{@code <}{@link R.NoBreakHyphen }{@code >}
     * {@link JAXBElement }{@code <}{@link R.EndnoteRef }{@code >}
     * {@link JAXBElement }{@code <}{@link R.PgNum }{@code >}
     * {@link JAXBElement }{@code <}{@link R.SoftHyphen }{@code >}
     * {@link Pict }
     * {@link R.Tab }
     * {@link Drawing }
     * {@link Br }
     * {@link JAXBElement }{@code <}{@link R.Separator }{@code >}
     * {@link JAXBElement }{@code <}{@link R.LastRenderedPageBreak }{@code >}
     * {@link JAXBElement }{@code <}{@link Text }{@code >}
     * {@link JAXBElement }{@code <}{@link CTObject }{@code >}
     * {@link JAXBElement }{@code <}{@link CTFtnEdnRef }{@code >}
     * {@link JAXBElement }{@code <}{@link Text }{@code >}
     * {@link JAXBElement }{@code <}{@link CTFtnEdnRef }{@code >}
     * {@link JAXBElement }{@code <}{@link R.MonthShort }{@code >}
     * {@link JAXBElement }{@code <}{@link R.MonthLong }{@code >}
     * {@link JAXBElement }{@code <}{@link R.ContinuationSeparator }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRuby }{@code >}
     * {@link JAXBElement }{@code <}{@link R.AnnotationRef }{@code >}
     * {@link JAXBElement }{@code <}{@link R.Cr }{@code >}
     * {@link JAXBElement }{@code <}{@link R.YearShort }{@code >}
     * {@link FldChar }
     * {@link JAXBElement }{@code <}{@link R.Sym }{@code >}
     * {@link R.CommentReference }
     * {@link JAXBElement }{@code <}{@link Text }{@code >}
     * {@link JAXBElement }{@code <}{@link R.FootnoteRef }{@code >}
     * {@link DelText }
     * {@link AlternateContent }
     * {@link JAXBElement }{@code <}{@link R.DayLong }{@code >}
     *
     * @since 2.7
     */
    @Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        return this.content;
    }

    @XmlAttribute(name = "rsidRPr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidRPr() {
        return null;
    }
    public void setRsidRPr(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidDel", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidDel() {
        return null;
    }
    public void setRsidDel(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidR", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidR() {
        return null;
    }
    public void setRsidR(@SuppressWarnings("unused") String value) {
        // // this value is always ignored and removed
    }

    public boolean notVanished() {
        BooleanDefaultTrue vanish = null;
        if (rPr != null) {
            vanish = rPr.getVanish();
        }
        return vanish == null || vanish.isVal() == false;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    final public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        // remove empty RPr elements
        if(rPr!=null && rPr.isEmpty()) {
            rPr = null;
        }
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param _parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);

        final DLList<Object> nodeList = getContent();
        for(int i=0; i<nodeList.size(); i++) {
        	final Object o = nodeList.get(i);
        	if(o instanceof AlternateContent) {
                // removing each alternate content that we don't understand
        		final List<Choice> choices = ((AlternateContent)o).getChoice();
                for(int j=0; j<choices.size(); j++) {
                	final String requires = choices.get(j).getRequires();
                	if(requires.equals("wps")) {		// WordprocessingShape
                		continue;
                	}
                	else if(requires.equals("wpg")) {	// WordprocessingGroup
                		continue;
                	}
                	else if(requires.equals("wpc")) {	// WordprocessingCanvas
                		continue;
                	}
                    else if (requires.equals("cx1") || requires.equals("cx2") || requires.equals("cx4")) {  // Excel Chart Ex
                        continue;
                    }
                	else {
                		// removing unsupported choices
                		choices.remove(j--);
                	}
                }
                if(choices.isEmpty()) {
                	// removing alternate content and replacing it with the fallback value
                	nodeList.remove(i);
                	final Fallback fallback = ((AlternateContent)o).getFallback();
                	if(fallback!=null) {
                		for(Object f:fallback.getAny()) {
                			if(f instanceof Child) {
                				((Child)f).setParent(this);
                			}
                			nodeList.add(i++, f);
                		}
                	}
                }
        	}
        }
    }

    @Override
    public boolean tryCombineWithNextNode(INodeAccessor<Object> parentNode, DLNode<Object> nextNode) {
        if(nextNode==null || !(nextNode.getData() instanceof R)) {
            return false;
        }
        final R nextRun = (R)nextNode.getData();
        final DLNode<Object> child = getContent().getFirstNode();
        final DLNode<Object> nextChild = nextRun.getContent().getFirstNode();

        // ensure that each run has exact one IText child...
        if((child==null || !(child.getData() instanceof IText) || child.getNext()!=null) || (nextChild==null || !(nextChild.getData() instanceof IText) || nextChild.getNext()!=null)) {
            return false;
        }
        final IText text = (IText)child.getData();
        final IText nextText = (IText)nextChild.getData();

        // ensure that we do not combine text with delText
        if(text.getClass()!=nextText.getClass()) {
            return false;
        }

        final RPr rPrRun = getRPr(false);
        final RPr rPrNextRun = nextRun.getRPr(false);
        if((rPrRun==null&&rPrNextRun==null)||(rPrRun!=null&&rPrRun.equals(rPrNextRun))) {
            if("preserved".equals(nextText.getSpace())) {
                text.setSpace("preserved");
            }
            // text can be combined
            text.setValue(text.getValue() + nextText.getValue());
            parentNode.getContent().removeNode(nextNode);
            return true;
        }
        return false;
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "annotationRef")
    public static class AnnotationRef
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="id" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_DecimalNumber" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "commentReference")
    public static class CommentReference
        implements Child
    {

        @XmlAttribute(name = "id", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        @XmlJavaTypeAdapter(IntegerAdapter.class)
        protected Integer id;
        @XmlTransient
        private Object parent;

        /**
         * Gets the value of the id property.
         *
         * @return
         *     possible object is
         *     {@link Long }
         *
         */
        public Integer getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         *
         * @param value
         *     allowed object is
         *     {@link Long }
         *
         */
        public void setId(Integer value) {
            this.id = value;
        }

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "continuationSeparator")
    public static class ContinuationSeparator
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "cr")
    public static class Cr
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "dayLong")
    public static class DayLong
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "dayShort")
    public static class DayShort
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "endnoteRef")
    public static class EndnoteRef
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "footnoteRef")
    public static class FootnoteRef
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "lastRenderedPageBreak")
    public static class LastRenderedPageBreak
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "monthLong")
    public static class MonthLong
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "monthShort")
    public static class MonthShort
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "noBreakHyphen")
    public static class NoBreakHyphen
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "pgNum")
    public static class PgNum
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="alignment" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_PTabAlignment" />
     *       &lt;attribute name="relativeTo" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_PTabRelativeTo" />
     *       &lt;attribute name="leader" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_PTabLeader" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "ptab")
    public static class Ptab
        implements Child
    {

        @XmlAttribute(namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected STPTabAlignment alignment;
        @XmlAttribute(namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected STPTabRelativeTo relativeTo;
        @XmlAttribute(namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected STPTabLeader leader;
        @XmlTransient
        private Object parent;

        /**
         * Gets the value of the alignment property.
         *
         * @return
         *     possible object is
         *     {@link STPTabAlignment }
         *
         */
        public STPTabAlignment getAlignment() {
            return alignment;
        }

        /**
         * Sets the value of the alignment property.
         *
         * @param value
         *     allowed object is
         *     {@link STPTabAlignment }
         *
         */
        public void setAlignment(STPTabAlignment value) {
            this.alignment = value;
        }

        /**
         * Gets the value of the relativeTo property.
         *
         * @return
         *     possible object is
         *     {@link STPTabRelativeTo }
         *
         */
        public STPTabRelativeTo getRelativeTo() {
            return relativeTo;
        }

        /**
         * Sets the value of the relativeTo property.
         *
         * @param value
         *     allowed object is
         *     {@link STPTabRelativeTo }
         *
         */
        public void setRelativeTo(STPTabRelativeTo value) {
            this.relativeTo = value;
        }

        /**
         * Gets the value of the leader property.
         *
         * @return
         *     possible object is
         *     {@link STPTabLeader }
         *
         */
        public STPTabLeader getLeader() {
            return leader;
        }

        /**
         * Sets the value of the leader property.
         *
         * @param value
         *     allowed object is
         *     {@link STPTabLeader }
         *
         */
        public void setLeader(STPTabLeader value) {
            this.leader = value;
        }

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "separator")
    public static class Separator
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "softHyphen")
    public static class SoftHyphen
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="font" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
     *       &lt;attribute name="char" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_ShortHexNumber" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "sym")
    public static class Sym
        implements Child
    {

        @XmlAttribute(namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String font;
        @XmlAttribute(name = "char", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String _char;
        @XmlTransient
        private Object parent;

        /**
         * Gets the value of the font property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getFont() {
            return font;
        }

        /**
         * Sets the value of the font property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setFont(String value) {
            this.font = value;
        }

        /**
         * Gets the value of the char property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getChar() {
            return _char;
        }

        /**
         * Sets the value of the char property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setChar(String value) {
            this._char = value;
        }

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "tab")
    public static class Tab
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "yearLong")
    public static class YearLong
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    @XmlRootElement(name = "yearShort")
    public static class YearShort
        implements Child
    {

        @XmlTransient
        private Object parent;

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }
    }
}
