/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.wml; 

import java.math.BigInteger;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_EastAsianLayout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_EastAsianLayout">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="id" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_DecimalNumber" />
 *       &lt;attribute name="combine" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="combineBrackets" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_CombineBrackets" />
 *       &lt;attribute name="vert" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="vertCompress" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_EastAsianLayout")
public class CTEastAsianLayout implements Cloneable {

    @XmlAttribute(name = "id", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected BigInteger id;
    @XmlAttribute(name = "combine", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected Boolean combine;
    @XmlAttribute(name = "combineBrackets", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STCombineBrackets combineBrackets;
    @XmlAttribute(name = "vert", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected Boolean vert;
    @XmlAttribute(name = "vertCompress", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected Boolean vertCompress;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the combine property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isCombine() {
        if (combine == null) {
            return true;
        }
        return combine;
    }

    /**
     * Sets the value of the combine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCombine(Boolean value) {
        this.combine = value;
    }

    /**
     * Gets the value of the combineBrackets property.
     * 
     * @return
     *     possible object is
     *     {@link STCombineBrackets }
     *     
     */
    public STCombineBrackets getCombineBrackets() {
        return combineBrackets;
    }

    /**
     * Sets the value of the combineBrackets property.
     * 
     * @param value
     *     allowed object is
     *     {@link STCombineBrackets }
     *     
     */
    public void setCombineBrackets(STCombineBrackets value) {
        this.combineBrackets = value;
    }

    /**
     * Gets the value of the vert property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isVert() {
        if (vert == null) {
            return true;
        }
        return vert;
    }

    /**
     * Sets the value of the vert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVert(Boolean value) {
        this.vert = value;
    }

    /**
     * Gets the value of the vertCompress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isVertCompress() {
        if (vertCompress == null) {
            return true;
        }
        return vertCompress;
    }

    /**
     * Sets the value of the vertCompress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVertCompress(Boolean value) {
        this.vertCompress = value;
    }

    @Override
    public CTEastAsianLayout clone() {
        try {
            return (CTEastAsianLayout)super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((combine == null) ? 0 : combine.hashCode());
        result = prime * result + ((combineBrackets == null) ? 0 : combineBrackets.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((vert == null) ? 0 : vert.hashCode());
        result = prime * result + ((vertCompress == null) ? 0 : vertCompress.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CTEastAsianLayout other = (CTEastAsianLayout) obj;
        if (combine == null) {
            if (other.combine != null)
                return false;
        } else if (!combine.equals(other.combine))
            return false;
        if (combineBrackets != other.combineBrackets)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (vert == null) {
            if (other.vert != null)
                return false;
        } else if (!vert.equals(other.vert))
            return false;
        if (vertCompress == null) {
            if (other.vertCompress != null)
                return false;
        } else if (!vertCompress.equals(other.vertCompress))
            return false;
        return true;
    }
}
