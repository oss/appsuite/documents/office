/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.util.List;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.vml.CTArc;
import org.docx4j.vml.CTCurve;
import org.docx4j.vml.CTGroup;
import org.docx4j.vml.CTImage;
import org.docx4j.vml.CTLine;
import org.docx4j.vml.CTOval;
import org.docx4j.vml.CTPolyLine;
import org.docx4j.vml.CTRect;
import org.docx4j.vml.CTRoundRect;
import org.docx4j.vml.CTShape;
import org.docx4j.vml.CTShapetype;
import org.w3c.dom.Element;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;


/**
 * <p>Java class for CT_PictureBase complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_PictureBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:vml' minOccurs="0"/>
 *         &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:office:office' minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PictureBase", propOrder = {
    "anyAndAny"
})
public class CTPictureBase implements Child, IContentAccessor<Object> {

    @XmlAnyElement(lax = true)
    @XmlElementRefs({
        @XmlElementRef(name="group", namespace="urn:schemas-microsoft-com:vml", type=CTGroup.class),
        @XmlElementRef(name="arc", namespace="urn:schemas-microsoft-com:vml", type=CTArc.class),
        @XmlElementRef(name="curve", namespace="urn:schemas-microsoft-com:vml", type=CTCurve.class),
        @XmlElementRef(name="image", namespace="urn:schemas-microsoft-com:vml", type=CTImage.class),
        @XmlElementRef(name="line", namespace="urn:schemas-microsoft-com:vml", type=CTLine.class),
        @XmlElementRef(name="oval", namespace="urn:schemas-microsoft-com:vml", type=CTOval.class),
        @XmlElementRef(name="polyline", namespace="urn:schemas-microsoft-com:vml", type=CTPolyLine.class),
        @XmlElementRef(name="rect", namespace="urn:schemas-microsoft-com:vml", type=CTRect.class),
        @XmlElementRef(name="roundrect", namespace="urn:schemas-microsoft-com:vml", type=CTRoundRect.class),
        @XmlElementRef(name="shape", namespace="urn:schemas-microsoft-com:vml", type=CTShape.class),
        @XmlElementRef(name="shapetype", namespace="urn:schemas-microsoft-com:vml", type=CTShapetype.class)
    })
    protected DLList<Object> anyAndAny;
    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the anyAndAny property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anyAndAny property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnyAndAny().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     *
     *
     */
    @Override
    public List<Object> getContent() {
        if (anyAndAny == null) {
            anyAndAny = new DLList<Object>();
        }
        return this.anyAndAny;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object p) {
        setParent(p);
    }
}
