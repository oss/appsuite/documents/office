/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.customxml.SchemaLibrary;
import org.docx4j.math.CTMathPr;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import org.docx4j.w14.CTDefaultImageDpi;
import org.docx4j.w14.CTLongHexNumber;
import org.docx4j.w14.CTOnOff;
import org.docx4j.w15.CTGuid;


/**
 * <p>Java class for CT_Settings complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Settings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="writeProtection" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_WriteProtection" minOccurs="0"/>
 *         &lt;element name="view" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_View" minOccurs="0"/>
 *         &lt;element name="zoom" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Zoom" minOccurs="0"/>
 *         &lt;element name="removePersonalInformation" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="removeDateAndTime" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotDisplayPageBoundaries" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="displayBackgroundShape" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="printPostScriptOverText" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="printFractionalCharacterWidth" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="printFormsData" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="embedTrueTypeFonts" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="embedSystemFonts" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="saveSubsetFonts" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="saveFormsData" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="mirrorMargins" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="alignBordersAndEdges" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="bordersDoNotSurroundHeader" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="bordersDoNotSurroundFooter" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="gutterAtTop" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="hideSpellingErrors" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="hideGrammaticalErrors" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="activeWritingStyle" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_WritingStyle" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="proofState" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Proof" minOccurs="0"/>
 *         &lt;element name="formsDesign" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="attachedTemplate" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Rel" minOccurs="0"/>
 *         &lt;element name="linkStyles" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="stylePaneFormatFilter" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ShortHexNumber" minOccurs="0"/>
 *         &lt;element name="stylePaneSortMethod" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ShortHexNumber" minOccurs="0"/>
 *         &lt;element name="documentType" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_DocType" minOccurs="0"/>
 *         &lt;element name="mailMerge" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_MailMerge" minOccurs="0"/>
 *         &lt;element name="revisionView" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TrackChangesView" minOccurs="0"/>
 *         &lt;element name="trackRevisions" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotTrackMoves" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotTrackFormatting" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="documentProtection" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_DocProtect" minOccurs="0"/>
 *         &lt;element name="autoFormatOverride" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="styleLockTheme" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="styleLockQFSet" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="defaultTabStop" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="autoHyphenation" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="consecutiveHyphenLimit" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="hyphenationZone" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="doNotHyphenateCaps" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="showEnvelope" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="summaryLength" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="clickAndTypeStyle" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="defaultTableStyle" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="evenAndOddHeaders" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="bookFoldRevPrinting" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="bookFoldPrinting" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="bookFoldPrintingSheets" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="drawingGridHorizontalSpacing" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="drawingGridVerticalSpacing" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="displayHorizontalDrawingGridEvery" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="displayVerticalDrawingGridEvery" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="doNotUseMarginsForDrawingGridOrigin" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="drawingGridHorizontalOrigin" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="drawingGridVerticalOrigin" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TwipsMeasure" minOccurs="0"/>
 *         &lt;element name="doNotShadeFormData" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="noPunctuationKerning" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="characterSpacingControl" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_CharacterSpacing" minOccurs="0"/>
 *         &lt;element name="printTwoOnOne" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="strictFirstAndLastChars" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="noLineBreaksAfter" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Kinsoku" minOccurs="0"/>
 *         &lt;element name="noLineBreaksBefore" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Kinsoku" minOccurs="0"/>
 *         &lt;element name="savePreviewPicture" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotValidateAgainstSchema" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="saveInvalidXml" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="ignoreMixedContent" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="alwaysShowPlaceholderText" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotDemarcateInvalidXml" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="saveXmlDataOnly" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="useXSLTWhenSaving" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="saveThroughXslt" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_SaveThroughXslt" minOccurs="0"/>
 *         &lt;element name="showXMLTags" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="alwaysMergeEmptyNamespace" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="updateFields" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="hdrShapeDefaults" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ShapeDefaults" minOccurs="0"/>
 *         &lt;element name="footnotePr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_FtnDocProps" minOccurs="0"/>
 *         &lt;element name="endnotePr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_EdnDocProps" minOccurs="0"/>
 *         &lt;element name="compat" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Compat" minOccurs="0"/>
 *         &lt;element name="docVars" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_DocVars" minOccurs="0"/>
 *         &lt;element name="rsids" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_DocRsids" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/officeDocument/2006/math}mathPr" minOccurs="0"/>
 *         &lt;element name="uiCompat97To2003" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="attachedSchema" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="themeFontLang" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Language" minOccurs="0"/>
 *         &lt;element name="clrSchemeMapping" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ColorSchemeMapping" minOccurs="0"/>
 *         &lt;element name="doNotIncludeSubdocsInStats" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="doNotAutoCompressPictures" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="forceUpgrade" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="captions" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Captions" minOccurs="0"/>
 *         &lt;element name="readModeInkLockDown" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ReadingModeInkLockDown" minOccurs="0"/>
 *         &lt;element name="smartTagType" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_SmartTagType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/schemaLibrary/2006/main}schemaLibrary" minOccurs="0"/>
 *         &lt;element name="shapeDefaults" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_ShapeDefaults" minOccurs="0"/>
 *         &lt;element name="doNotEmbedSmartTags" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}BooleanDefaultTrue" minOccurs="0"/>
 *         &lt;element name="decimalSymbol" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="listSeparator" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2012/wordml}chartTrackingRefBased" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}docId" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2012/wordml}docId" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}conflictMode" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}discardImageEditingData" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}defaultImageDpi" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute ref="{http://schemas.openxmlformats.org/markup-compatibility/2006}Ignorable"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Settings", propOrder = {
    "writeProtection",
    "view",
    "zoom",
    "removePersonalInformation",
    "removeDateAndTime",
    "doNotDisplayPageBoundaries",
    "displayBackgroundShape",
    "printPostScriptOverText",
    "printFractionalCharacterWidth",
    "printFormsData",
    "embedTrueTypeFonts",
    "embedSystemFonts",
    "saveSubsetFonts",
    "saveFormsData",
    "mirrorMargins",
    "alignBordersAndEdges",
    "bordersDoNotSurroundHeader",
    "bordersDoNotSurroundFooter",
    "gutterAtTop",
    "hideSpellingErrors",
    "hideGrammaticalErrors",
    "activeWritingStyle",
    "proofState",
    "formsDesign",
    "attachedTemplate",
    "linkStyles",
    "stylePaneFormatFilter",
    "stylePaneSortMethod",
    "documentType",
    "mailMerge",
    "revisionView",
    "trackRevisions",
    "doNotTrackMoves",
    "doNotTrackFormatting",
    "documentProtection",
    "autoFormatOverride",
    "styleLockTheme",
    "styleLockQFSet",
    "defaultTabStop",
    "autoHyphenation",
    "consecutiveHyphenLimit",
    "hyphenationZone",
    "doNotHyphenateCaps",
    "showEnvelope",
    "summaryLength",
    "clickAndTypeStyle",
    "defaultTableStyle",
    "evenAndOddHeaders",
    "bookFoldRevPrinting",
    "bookFoldPrinting",
    "bookFoldPrintingSheets",
    "drawingGridHorizontalSpacing",
    "drawingGridVerticalSpacing",
    "displayHorizontalDrawingGridEvery",
    "displayVerticalDrawingGridEvery",
    "doNotUseMarginsForDrawingGridOrigin",
    "drawingGridHorizontalOrigin",
    "drawingGridVerticalOrigin",
    "doNotShadeFormData",
    "noPunctuationKerning",
    "characterSpacingControl",
    "printTwoOnOne",
    "strictFirstAndLastChars",
    "noLineBreaksAfter",
    "noLineBreaksBefore",
    "savePreviewPicture",
    "doNotValidateAgainstSchema",
    "saveInvalidXml",
    "ignoreMixedContent",
    "alwaysShowPlaceholderText",
    "doNotDemarcateInvalidXml",
    "saveXmlDataOnly",
    "useXSLTWhenSaving",
    "saveThroughXslt",
    "showXMLTags",
    "alwaysMergeEmptyNamespace",
    "updateFields",
    "hdrShapeDefaults",
    "footnotePr",
    "endnotePr",
    "compat",
    "docVars",
    "rsids",
    "mathPr",
    "uiCompat97To2003",
    "attachedSchema",
    "themeFontLang",
    "clrSchemeMapping",
    "doNotIncludeSubdocsInStats",
    "doNotAutoCompressPictures",
    "forceUpgrade",
    "captions",
    "readModeInkLockDown",
    "smartTagType",
    "schemaLibrary",
    "shapeDefaults",
    "doNotEmbedSmartTags",
    "decimalSymbol",
    "listSeparator",
    "chartTrackingRefBased",
    "docId14",
    "docId15",
    "conflictMode",
    "discardImageEditingData",
    "defaultImageDpi"
})
@XmlRootElement(name = "settings")
public class CTSettings extends DocumentSerialization {

    public CTSettings() {
        super(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "settings", "w"), DocumentSerialization.Standard_DOCX_Namespaces, DocumentSerialization.Standard_DOCX_Ignorables);
    }

    protected CTWriteProtection writeProtection;
    protected CTView view;
    protected CTZoom zoom;
    protected BooleanDefaultTrue removePersonalInformation;
    protected BooleanDefaultTrue removeDateAndTime;
    protected BooleanDefaultTrue doNotDisplayPageBoundaries;
    protected BooleanDefaultTrue displayBackgroundShape;
    protected BooleanDefaultTrue printPostScriptOverText;
    protected BooleanDefaultTrue printFractionalCharacterWidth;
    protected BooleanDefaultTrue printFormsData;
    protected BooleanDefaultTrue embedTrueTypeFonts;
    protected BooleanDefaultTrue embedSystemFonts;
    protected BooleanDefaultTrue saveSubsetFonts;
    protected BooleanDefaultTrue saveFormsData;
    protected BooleanDefaultTrue mirrorMargins;
    protected BooleanDefaultTrue alignBordersAndEdges;
    protected BooleanDefaultTrue bordersDoNotSurroundHeader;
    protected BooleanDefaultTrue bordersDoNotSurroundFooter;
    protected BooleanDefaultTrue gutterAtTop;
    protected BooleanDefaultTrue hideSpellingErrors;
    protected BooleanDefaultTrue hideGrammaticalErrors;
    protected List<CTWritingStyle> activeWritingStyle;
    protected CTProof proofState;
    protected BooleanDefaultTrue formsDesign;
    protected CTRel attachedTemplate;
    protected BooleanDefaultTrue linkStyles;
    protected CTShortHexNumber stylePaneFormatFilter;
    protected CTShortHexNumber stylePaneSortMethod;
    protected CTDocType documentType;
    protected CTMailMerge mailMerge;
    protected CTTrackChangesView revisionView;
    protected BooleanDefaultTrue trackRevisions;
    protected BooleanDefaultTrue doNotTrackMoves;
    protected BooleanDefaultTrue doNotTrackFormatting;
    protected CTDocProtect documentProtection;
    protected BooleanDefaultTrue autoFormatOverride;
    protected BooleanDefaultTrue styleLockTheme;
    protected BooleanDefaultTrue styleLockQFSet;
    protected CTTwipsMeasure defaultTabStop;
    protected BooleanDefaultTrue autoHyphenation;
    protected CTSettings.ConsecutiveHyphenLimit consecutiveHyphenLimit;
    protected CTTwipsMeasure hyphenationZone;
    protected BooleanDefaultTrue doNotHyphenateCaps;
    protected BooleanDefaultTrue showEnvelope;
    protected CTSettings.SummaryLength summaryLength;
    protected CTSettings.ClickAndTypeStyle clickAndTypeStyle;
    protected CTSettings.DefaultTableStyle defaultTableStyle;
    protected BooleanDefaultTrue evenAndOddHeaders;
    protected BooleanDefaultTrue bookFoldRevPrinting;
    protected BooleanDefaultTrue bookFoldPrinting;
    protected CTSettings.BookFoldPrintingSheets bookFoldPrintingSheets;
    protected CTTwipsMeasure drawingGridHorizontalSpacing;
    protected CTTwipsMeasure drawingGridVerticalSpacing;
    protected CTSettings.DisplayHorizontalDrawingGridEvery displayHorizontalDrawingGridEvery;
    protected CTSettings.DisplayVerticalDrawingGridEvery displayVerticalDrawingGridEvery;
    protected BooleanDefaultTrue doNotUseMarginsForDrawingGridOrigin;
    protected CTTwipsMeasure drawingGridHorizontalOrigin;
    protected CTTwipsMeasure drawingGridVerticalOrigin;
    protected BooleanDefaultTrue doNotShadeFormData;
    protected BooleanDefaultTrue noPunctuationKerning;
    protected CTCharacterSpacing characterSpacingControl;
    protected BooleanDefaultTrue printTwoOnOne;
    protected BooleanDefaultTrue strictFirstAndLastChars;
    protected CTKinsoku noLineBreaksAfter;
    protected CTKinsoku noLineBreaksBefore;
    protected BooleanDefaultTrue savePreviewPicture;
    protected BooleanDefaultTrue doNotValidateAgainstSchema;
    protected BooleanDefaultTrue saveInvalidXml;
    protected BooleanDefaultTrue ignoreMixedContent;
    protected BooleanDefaultTrue alwaysShowPlaceholderText;
    protected BooleanDefaultTrue doNotDemarcateInvalidXml;
    protected BooleanDefaultTrue saveXmlDataOnly;
    protected BooleanDefaultTrue useXSLTWhenSaving;
    protected CTSaveThroughXslt saveThroughXslt;
    protected BooleanDefaultTrue showXMLTags;
    protected BooleanDefaultTrue alwaysMergeEmptyNamespace;
    protected BooleanDefaultTrue updateFields;
    protected CTShapeDefaults hdrShapeDefaults;
    protected CTFtnDocProps footnotePr;
    protected CTEdnDocProps endnotePr;
    protected CTCompat compat;
    protected CTDocVars docVars;
    protected CTDocRsids rsids;
    protected BooleanDefaultTrue uiCompat97To2003;
    protected List<CTSettings.AttachedSchema> attachedSchema;
    protected CTLanguage themeFontLang;
    protected CTColorSchemeMapping clrSchemeMapping;
    protected BooleanDefaultTrue doNotIncludeSubdocsInStats;
    protected BooleanDefaultTrue doNotAutoCompressPictures;
    protected CTSettings.ForceUpgrade forceUpgrade;
    protected CTCaptions captions;
    protected CTReadingModeInkLockDown readModeInkLockDown;
    protected List<CTSmartTagType> smartTagType;
    protected CTShapeDefaults shapeDefaults;
    protected BooleanDefaultTrue doNotEmbedSmartTags;
    protected CTSettings.DecimalSymbol decimalSymbol;
    protected CTSettings.ListSeparator listSeparator;
    @XmlElement(namespace = "http://schemas.openxmlformats.org/schemaLibrary/2006/main")
    protected SchemaLibrary schemaLibrary;
    @XmlElement(namespace = "http://schemas.openxmlformats.org/officeDocument/2006/math")
    protected CTMathPr mathPr;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2012/wordml")
    protected BooleanDefaultTrue chartTrackingRefBased;
    @XmlElement(name = "docId", namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    protected CTLongHexNumber docId14;
    @XmlElement(name = "docId", namespace = "http://schemas.microsoft.com/office/word/2012/wordml")
    protected CTGuid docId15;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    protected CTOnOff conflictMode;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    protected CTOnOff discardImageEditingData;
    @XmlElement(namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    protected CTDefaultImageDpi defaultImageDpi;

    /**
     * Gets the value of the writeProtection property.
     *
     * @return
     *     possible object is
     *     {@link CTWriteProtection }
     *
     */
    public CTWriteProtection getWriteProtection() {
        return writeProtection;
    }

    /**
     * Sets the value of the writeProtection property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWriteProtection }
     *
     */
    public void setWriteProtection(CTWriteProtection value) {
        this.writeProtection = value;
    }

    /**
     * Gets the value of the view property.
     *
     * @return
     *     possible object is
     *     {@link CTView }
     *
     */
    public CTView getView() {
        return view;
    }

    /**
     * Sets the value of the view property.
     *
     * @param value
     *     allowed object is
     *     {@link CTView }
     *
     */
    public void setView(CTView value) {
        this.view = value;
    }

    /**
     * Gets the value of the zoom property.
     *
     * @return
     *     possible object is
     *     {@link CTZoom }
     *
     */
    public CTZoom getZoom() {
        return zoom;
    }

    /**
     * Sets the value of the zoom property.
     *
     * @param value
     *     allowed object is
     *     {@link CTZoom }
     *
     */
    public void setZoom(CTZoom value) {
        this.zoom = value;
    }

    /**
     * Gets the value of the removePersonalInformation property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getRemovePersonalInformation() {
        return removePersonalInformation;
    }

    /**
     * Sets the value of the removePersonalInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setRemovePersonalInformation(BooleanDefaultTrue value) {
        this.removePersonalInformation = value;
    }

    /**
     * Gets the value of the removeDateAndTime property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getRemoveDateAndTime() {
        return removeDateAndTime;
    }

    /**
     * Sets the value of the removeDateAndTime property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setRemoveDateAndTime(BooleanDefaultTrue value) {
        this.removeDateAndTime = value;
    }

    /**
     * Gets the value of the doNotDisplayPageBoundaries property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotDisplayPageBoundaries() {
        return doNotDisplayPageBoundaries;
    }

    /**
     * Sets the value of the doNotDisplayPageBoundaries property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotDisplayPageBoundaries(BooleanDefaultTrue value) {
        this.doNotDisplayPageBoundaries = value;
    }

    /**
     * Gets the value of the displayBackgroundShape property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDisplayBackgroundShape() {
        return displayBackgroundShape;
    }

    /**
     * Sets the value of the displayBackgroundShape property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDisplayBackgroundShape(BooleanDefaultTrue value) {
        this.displayBackgroundShape = value;
    }

    /**
     * Gets the value of the printPostScriptOverText property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getPrintPostScriptOverText() {
        return printPostScriptOverText;
    }

    /**
     * Sets the value of the printPostScriptOverText property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setPrintPostScriptOverText(BooleanDefaultTrue value) {
        this.printPostScriptOverText = value;
    }

    /**
     * Gets the value of the printFractionalCharacterWidth property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getPrintFractionalCharacterWidth() {
        return printFractionalCharacterWidth;
    }

    /**
     * Sets the value of the printFractionalCharacterWidth property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setPrintFractionalCharacterWidth(BooleanDefaultTrue value) {
        this.printFractionalCharacterWidth = value;
    }

    /**
     * Gets the value of the printFormsData property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getPrintFormsData() {
        return printFormsData;
    }

    /**
     * Sets the value of the printFormsData property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setPrintFormsData(BooleanDefaultTrue value) {
        this.printFormsData = value;
    }

    /**
     * Gets the value of the embedTrueTypeFonts property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getEmbedTrueTypeFonts() {
        return embedTrueTypeFonts;
    }

    /**
     * Sets the value of the embedTrueTypeFonts property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setEmbedTrueTypeFonts(BooleanDefaultTrue value) {
        this.embedTrueTypeFonts = value;
    }

    /**
     * Gets the value of the embedSystemFonts property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getEmbedSystemFonts() {
        return embedSystemFonts;
    }

    /**
     * Sets the value of the embedSystemFonts property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setEmbedSystemFonts(BooleanDefaultTrue value) {
        this.embedSystemFonts = value;
    }

    /**
     * Gets the value of the saveSubsetFonts property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getSaveSubsetFonts() {
        return saveSubsetFonts;
    }

    /**
     * Sets the value of the saveSubsetFonts property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSaveSubsetFonts(BooleanDefaultTrue value) {
        this.saveSubsetFonts = value;
    }

    /**
     * Gets the value of the saveFormsData property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getSaveFormsData() {
        return saveFormsData;
    }

    /**
     * Sets the value of the saveFormsData property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSaveFormsData(BooleanDefaultTrue value) {
        this.saveFormsData = value;
    }

    /**
     * Gets the value of the mirrorMargins property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getMirrorMargins() {
        return mirrorMargins;
    }

    /**
     * Sets the value of the mirrorMargins property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setMirrorMargins(BooleanDefaultTrue value) {
        this.mirrorMargins = value;
    }

    /**
     * Gets the value of the alignBordersAndEdges property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getAlignBordersAndEdges() {
        return alignBordersAndEdges;
    }

    /**
     * Sets the value of the alignBordersAndEdges property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setAlignBordersAndEdges(BooleanDefaultTrue value) {
        this.alignBordersAndEdges = value;
    }

    /**
     * Gets the value of the bordersDoNotSurroundHeader property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getBordersDoNotSurroundHeader() {
        return bordersDoNotSurroundHeader;
    }

    /**
     * Sets the value of the bordersDoNotSurroundHeader property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setBordersDoNotSurroundHeader(BooleanDefaultTrue value) {
        this.bordersDoNotSurroundHeader = value;
    }

    /**
     * Gets the value of the bordersDoNotSurroundFooter property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getBordersDoNotSurroundFooter() {
        return bordersDoNotSurroundFooter;
    }

    /**
     * Sets the value of the bordersDoNotSurroundFooter property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setBordersDoNotSurroundFooter(BooleanDefaultTrue value) {
        this.bordersDoNotSurroundFooter = value;
    }

    /**
     * Gets the value of the gutterAtTop property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getGutterAtTop() {
        return gutterAtTop;
    }

    /**
     * Sets the value of the gutterAtTop property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setGutterAtTop(BooleanDefaultTrue value) {
        this.gutterAtTop = value;
    }

    /**
     * Gets the value of the hideSpellingErrors property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getHideSpellingErrors() {
        return hideSpellingErrors;
    }

    /**
     * Sets the value of the hideSpellingErrors property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setHideSpellingErrors(BooleanDefaultTrue value) {
        this.hideSpellingErrors = value;
    }

    /**
     * Gets the value of the hideGrammaticalErrors property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getHideGrammaticalErrors() {
        return hideGrammaticalErrors;
    }

    /**
     * Sets the value of the hideGrammaticalErrors property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setHideGrammaticalErrors(BooleanDefaultTrue value) {
        this.hideGrammaticalErrors = value;
    }

    public List<CTWritingStyle> getActiveWritingStyle() {
        if (activeWritingStyle == null) {
            activeWritingStyle = new ArrayList<CTWritingStyle>();
        }
        return this.activeWritingStyle;
    }

    /**
     * Gets the value of the proofState property.
     *
     * @return
     *     possible object is
     *     {@link CTProof }
     *
     */
    public CTProof getProofState() {
        return proofState;
    }

    /**
     * Sets the value of the proofState property.
     *
     * @param value
     *     allowed object is
     *     {@link CTProof }
     *
     */
    public void setProofState(CTProof value) {
        this.proofState = value;
    }

    /**
     * Gets the value of the formsDesign property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getFormsDesign() {
        return formsDesign;
    }

    /**
     * Sets the value of the formsDesign property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setFormsDesign(BooleanDefaultTrue value) {
        this.formsDesign = value;
    }

    /**
     * Gets the value of the attachedTemplate property.
     *
     * @return
     *     possible object is
     *     {@link CTRel }
     *
     */
    public CTRel getAttachedTemplate() {
        return attachedTemplate;
    }

    /**
     * Sets the value of the attachedTemplate property.
     *
     * @param value
     *     allowed object is
     *     {@link CTRel }
     *
     */
    public void setAttachedTemplate(CTRel value) {
        this.attachedTemplate = value;
    }

    /**
     * Gets the value of the linkStyles property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getLinkStyles() {
        return linkStyles;
    }

    /**
     * Sets the value of the linkStyles property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setLinkStyles(BooleanDefaultTrue value) {
        this.linkStyles = value;
    }

    /**
     * Gets the value of the stylePaneFormatFilter property.
     *
     * @return
     *     possible object is
     *     {@link CTShortHexNumber }
     *
     */
    public CTShortHexNumber getStylePaneFormatFilter() {
        return stylePaneFormatFilter;
    }

    /**
     * Sets the value of the stylePaneFormatFilter property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShortHexNumber }
     *
     */
    public void setStylePaneFormatFilter(CTShortHexNumber value) {
        this.stylePaneFormatFilter = value;
    }

    /**
     * Gets the value of the stylePaneSortMethod property.
     *
     * @return
     *     possible object is
     *     {@link CTShortHexNumber }
     *
     */
    public CTShortHexNumber getStylePaneSortMethod() {
        return stylePaneSortMethod;
    }

    /**
     * Sets the value of the stylePaneSortMethod property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShortHexNumber }
     *
     */
    public void setStylePaneSortMethod(CTShortHexNumber value) {
        this.stylePaneSortMethod = value;
    }

    /**
     * Gets the value of the documentType property.
     *
     * @return
     *     possible object is
     *     {@link CTDocType }
     *
     */
    public CTDocType getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDocType }
     *
     */
    public void setDocumentType(CTDocType value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the mailMerge property.
     *
     * @return
     *     possible object is
     *     {@link CTMailMerge }
     *
     */
    public CTMailMerge getMailMerge() {
        return mailMerge;
    }

    /**
     * Sets the value of the mailMerge property.
     *
     * @param value
     *     allowed object is
     *     {@link CTMailMerge }
     *
     */
    public void setMailMerge(CTMailMerge value) {
        this.mailMerge = value;
    }

    /**
     * Gets the value of the revisionView property.
     *
     * @return
     *     possible object is
     *     {@link CTTrackChangesView }
     *
     */
    public CTTrackChangesView getRevisionView() {
        return revisionView;
    }

    /**
     * Sets the value of the revisionView property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTrackChangesView }
     *
     */
    public void setRevisionView(CTTrackChangesView value) {
        this.revisionView = value;
    }

    /**
     * Gets the value of the trackRevisions property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getTrackRevisions() {
        return trackRevisions;
    }

    /**
     * Sets the value of the trackRevisions property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setTrackRevisions(BooleanDefaultTrue value) {
        this.trackRevisions = value;
    }

    /**
     * Gets the value of the doNotTrackMoves property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotTrackMoves() {
        return doNotTrackMoves;
    }

    /**
     * Sets the value of the doNotTrackMoves property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotTrackMoves(BooleanDefaultTrue value) {
        this.doNotTrackMoves = value;
    }

    /**
     * Gets the value of the doNotTrackFormatting property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotTrackFormatting() {
        return doNotTrackFormatting;
    }

    /**
     * Sets the value of the doNotTrackFormatting property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotTrackFormatting(BooleanDefaultTrue value) {
        this.doNotTrackFormatting = value;
    }

    /**
     * Gets the value of the documentProtection property.
     *
     * @return
     *     possible object is
     *     {@link CTDocProtect }
     *
     */
    public CTDocProtect getDocumentProtection() {
        return documentProtection;
    }

    /**
     * Sets the value of the documentProtection property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDocProtect }
     *
     */
    public void setDocumentProtection(CTDocProtect value) {
        this.documentProtection = value;
    }

    /**
     * Gets the value of the autoFormatOverride property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getAutoFormatOverride() {
        return autoFormatOverride;
    }

    /**
     * Sets the value of the autoFormatOverride property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setAutoFormatOverride(BooleanDefaultTrue value) {
        this.autoFormatOverride = value;
    }

    /**
     * Gets the value of the styleLockTheme property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getStyleLockTheme() {
        return styleLockTheme;
    }

    /**
     * Sets the value of the styleLockTheme property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setStyleLockTheme(BooleanDefaultTrue value) {
        this.styleLockTheme = value;
    }

    /**
     * Gets the value of the styleLockQFSet property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getStyleLockQFSet() {
        return styleLockQFSet;
    }

    /**
     * Sets the value of the styleLockQFSet property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setStyleLockQFSet(BooleanDefaultTrue value) {
        this.styleLockQFSet = value;
    }

    /**
     * Gets the value of the defaultTabStop property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getDefaultTabStop() {
        return defaultTabStop;
    }

    /**
     * Sets the value of the defaultTabStop property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setDefaultTabStop(CTTwipsMeasure value) {
        this.defaultTabStop = value;
    }

    /**
     * Gets the value of the autoHyphenation property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getAutoHyphenation() {
        return autoHyphenation;
    }

    /**
     * Sets the value of the autoHyphenation property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setAutoHyphenation(BooleanDefaultTrue value) {
        this.autoHyphenation = value;
    }

    /**
     * Gets the value of the consecutiveHyphenLimit property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.ConsecutiveHyphenLimit }
     *
     */
    public CTSettings.ConsecutiveHyphenLimit getConsecutiveHyphenLimit() {
        return consecutiveHyphenLimit;
    }

    /**
     * Sets the value of the consecutiveHyphenLimit property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.ConsecutiveHyphenLimit }
     *
     */
    public void setConsecutiveHyphenLimit(CTSettings.ConsecutiveHyphenLimit value) {
        this.consecutiveHyphenLimit = value;
    }

    /**
     * Gets the value of the hyphenationZone property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getHyphenationZone() {
        return hyphenationZone;
    }

    /**
     * Sets the value of the hyphenationZone property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setHyphenationZone(CTTwipsMeasure value) {
        this.hyphenationZone = value;
    }

    /**
     * Gets the value of the doNotHyphenateCaps property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotHyphenateCaps() {
        return doNotHyphenateCaps;
    }

    /**
     * Sets the value of the doNotHyphenateCaps property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotHyphenateCaps(BooleanDefaultTrue value) {
        this.doNotHyphenateCaps = value;
    }

    /**
     * Gets the value of the showEnvelope property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getShowEnvelope() {
        return showEnvelope;
    }

    /**
     * Sets the value of the showEnvelope property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setShowEnvelope(BooleanDefaultTrue value) {
        this.showEnvelope = value;
    }

    /**
     * Gets the value of the summaryLength property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.SummaryLength }
     *
     */
    public CTSettings.SummaryLength getSummaryLength() {
        return summaryLength;
    }

    /**
     * Sets the value of the summaryLength property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.SummaryLength }
     *
     */
    public void setSummaryLength(CTSettings.SummaryLength value) {
        this.summaryLength = value;
    }

    /**
     * Gets the value of the clickAndTypeStyle property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.ClickAndTypeStyle }
     *
     */
    public CTSettings.ClickAndTypeStyle getClickAndTypeStyle() {
        return clickAndTypeStyle;
    }

    /**
     * Sets the value of the clickAndTypeStyle property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.ClickAndTypeStyle }
     *
     */
    public void setClickAndTypeStyle(CTSettings.ClickAndTypeStyle value) {
        this.clickAndTypeStyle = value;
    }

    /**
     * Gets the value of the defaultTableStyle property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.DefaultTableStyle }
     *
     */
    public CTSettings.DefaultTableStyle getDefaultTableStyle() {
        return defaultTableStyle;
    }

    /**
     * Sets the value of the defaultTableStyle property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.DefaultTableStyle }
     *
     */
    public void setDefaultTableStyle(CTSettings.DefaultTableStyle value) {
        this.defaultTableStyle = value;
    }

    /**
     * Gets the value of the evenAndOddHeaders property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getEvenAndOddHeaders() {
        return evenAndOddHeaders;
    }

    /**
     * Sets the value of the evenAndOddHeaders property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setEvenAndOddHeaders(BooleanDefaultTrue value) {
        this.evenAndOddHeaders = value;
    }

    /**
     * Gets the value of the bookFoldRevPrinting property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getBookFoldRevPrinting() {
        return bookFoldRevPrinting;
    }

    /**
     * Sets the value of the bookFoldRevPrinting property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setBookFoldRevPrinting(BooleanDefaultTrue value) {
        this.bookFoldRevPrinting = value;
    }

    /**
     * Gets the value of the bookFoldPrinting property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getBookFoldPrinting() {
        return bookFoldPrinting;
    }

    /**
     * Sets the value of the bookFoldPrinting property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setBookFoldPrinting(BooleanDefaultTrue value) {
        this.bookFoldPrinting = value;
    }

    /**
     * Gets the value of the bookFoldPrintingSheets property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.BookFoldPrintingSheets }
     *
     */
    public CTSettings.BookFoldPrintingSheets getBookFoldPrintingSheets() {
        return bookFoldPrintingSheets;
    }

    /**
     * Sets the value of the bookFoldPrintingSheets property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.BookFoldPrintingSheets }
     *
     */
    public void setBookFoldPrintingSheets(CTSettings.BookFoldPrintingSheets value) {
        this.bookFoldPrintingSheets = value;
    }

    /**
     * Gets the value of the drawingGridHorizontalSpacing property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getDrawingGridHorizontalSpacing() {
        return drawingGridHorizontalSpacing;
    }

    /**
     * Sets the value of the drawingGridHorizontalSpacing property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setDrawingGridHorizontalSpacing(CTTwipsMeasure value) {
        this.drawingGridHorizontalSpacing = value;
    }

    /**
     * Gets the value of the drawingGridVerticalSpacing property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getDrawingGridVerticalSpacing() {
        return drawingGridVerticalSpacing;
    }

    /**
     * Sets the value of the drawingGridVerticalSpacing property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setDrawingGridVerticalSpacing(CTTwipsMeasure value) {
        this.drawingGridVerticalSpacing = value;
    }

    /**
     * Gets the value of the displayHorizontalDrawingGridEvery property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.DisplayHorizontalDrawingGridEvery }
     *
     */
    public CTSettings.DisplayHorizontalDrawingGridEvery getDisplayHorizontalDrawingGridEvery() {
        return displayHorizontalDrawingGridEvery;
    }

    /**
     * Sets the value of the displayHorizontalDrawingGridEvery property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.DisplayHorizontalDrawingGridEvery }
     *
     */
    public void setDisplayHorizontalDrawingGridEvery(CTSettings.DisplayHorizontalDrawingGridEvery value) {
        this.displayHorizontalDrawingGridEvery = value;
    }

    /**
     * Gets the value of the displayVerticalDrawingGridEvery property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.DisplayVerticalDrawingGridEvery }
     *
     */
    public CTSettings.DisplayVerticalDrawingGridEvery getDisplayVerticalDrawingGridEvery() {
        return displayVerticalDrawingGridEvery;
    }

    /**
     * Sets the value of the displayVerticalDrawingGridEvery property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.DisplayVerticalDrawingGridEvery }
     *
     */
    public void setDisplayVerticalDrawingGridEvery(CTSettings.DisplayVerticalDrawingGridEvery value) {
        this.displayVerticalDrawingGridEvery = value;
    }

    /**
     * Gets the value of the doNotUseMarginsForDrawingGridOrigin property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotUseMarginsForDrawingGridOrigin() {
        return doNotUseMarginsForDrawingGridOrigin;
    }

    /**
     * Sets the value of the doNotUseMarginsForDrawingGridOrigin property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotUseMarginsForDrawingGridOrigin(BooleanDefaultTrue value) {
        this.doNotUseMarginsForDrawingGridOrigin = value;
    }

    /**
     * Gets the value of the drawingGridHorizontalOrigin property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getDrawingGridHorizontalOrigin() {
        return drawingGridHorizontalOrigin;
    }

    /**
     * Sets the value of the drawingGridHorizontalOrigin property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setDrawingGridHorizontalOrigin(CTTwipsMeasure value) {
        this.drawingGridHorizontalOrigin = value;
    }

    /**
     * Gets the value of the drawingGridVerticalOrigin property.
     *
     * @return
     *     possible object is
     *     {@link CTTwipsMeasure }
     *
     */
    public CTTwipsMeasure getDrawingGridVerticalOrigin() {
        return drawingGridVerticalOrigin;
    }

    /**
     * Sets the value of the drawingGridVerticalOrigin property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTwipsMeasure }
     *
     */
    public void setDrawingGridVerticalOrigin(CTTwipsMeasure value) {
        this.drawingGridVerticalOrigin = value;
    }

    /**
     * Gets the value of the doNotShadeFormData property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotShadeFormData() {
        return doNotShadeFormData;
    }

    /**
     * Sets the value of the doNotShadeFormData property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotShadeFormData(BooleanDefaultTrue value) {
        this.doNotShadeFormData = value;
    }

    /**
     * Gets the value of the noPunctuationKerning property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getNoPunctuationKerning() {
        return noPunctuationKerning;
    }

    /**
     * Sets the value of the noPunctuationKerning property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setNoPunctuationKerning(BooleanDefaultTrue value) {
        this.noPunctuationKerning = value;
    }

    /**
     * Gets the value of the characterSpacingControl property.
     *
     * @return
     *     possible object is
     *     {@link CTCharacterSpacing }
     *
     */
    public CTCharacterSpacing getCharacterSpacingControl() {
        return characterSpacingControl;
    }

    /**
     * Sets the value of the characterSpacingControl property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCharacterSpacing }
     *
     */
    public void setCharacterSpacingControl(CTCharacterSpacing value) {
        this.characterSpacingControl = value;
    }

    /**
     * Gets the value of the printTwoOnOne property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getPrintTwoOnOne() {
        return printTwoOnOne;
    }

    /**
     * Sets the value of the printTwoOnOne property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setPrintTwoOnOne(BooleanDefaultTrue value) {
        this.printTwoOnOne = value;
    }

    /**
     * Gets the value of the strictFirstAndLastChars property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getStrictFirstAndLastChars() {
        return strictFirstAndLastChars;
    }

    /**
     * Sets the value of the strictFirstAndLastChars property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setStrictFirstAndLastChars(BooleanDefaultTrue value) {
        this.strictFirstAndLastChars = value;
    }

    /**
     * Gets the value of the noLineBreaksAfter property.
     *
     * @return
     *     possible object is
     *     {@link CTKinsoku }
     *
     */
    public CTKinsoku getNoLineBreaksAfter() {
        return noLineBreaksAfter;
    }

    /**
     * Sets the value of the noLineBreaksAfter property.
     *
     * @param value
     *     allowed object is
     *     {@link CTKinsoku }
     *
     */
    public void setNoLineBreaksAfter(CTKinsoku value) {
        this.noLineBreaksAfter = value;
    }

    /**
     * Gets the value of the noLineBreaksBefore property.
     *
     * @return
     *     possible object is
     *     {@link CTKinsoku }
     *
     */
    public CTKinsoku getNoLineBreaksBefore() {
        return noLineBreaksBefore;
    }

    /**
     * Sets the value of the noLineBreaksBefore property.
     *
     * @param value
     *     allowed object is
     *     {@link CTKinsoku }
     *
     */
    public void setNoLineBreaksBefore(CTKinsoku value) {
        this.noLineBreaksBefore = value;
    }

    /**
     * Gets the value of the savePreviewPicture property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getSavePreviewPicture() {
        return savePreviewPicture;
    }

    /**
     * Sets the value of the savePreviewPicture property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSavePreviewPicture(BooleanDefaultTrue value) {
        this.savePreviewPicture = value;
    }

    /**
     * Gets the value of the doNotValidateAgainstSchema property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotValidateAgainstSchema() {
        return doNotValidateAgainstSchema;
    }

    /**
     * Sets the value of the doNotValidateAgainstSchema property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotValidateAgainstSchema(BooleanDefaultTrue value) {
        this.doNotValidateAgainstSchema = value;
    }

    /**
     * Gets the value of the saveInvalidXml property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getSaveInvalidXml() {
        return saveInvalidXml;
    }

    /**
     * Sets the value of the saveInvalidXml property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSaveInvalidXml(BooleanDefaultTrue value) {
        this.saveInvalidXml = value;
    }

    /**
     * Gets the value of the ignoreMixedContent property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getIgnoreMixedContent() {
        return ignoreMixedContent;
    }

    /**
     * Sets the value of the ignoreMixedContent property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setIgnoreMixedContent(BooleanDefaultTrue value) {
        this.ignoreMixedContent = value;
    }

    /**
     * Gets the value of the alwaysShowPlaceholderText property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getAlwaysShowPlaceholderText() {
        return alwaysShowPlaceholderText;
    }

    /**
     * Sets the value of the alwaysShowPlaceholderText property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setAlwaysShowPlaceholderText(BooleanDefaultTrue value) {
        this.alwaysShowPlaceholderText = value;
    }

    /**
     * Gets the value of the doNotDemarcateInvalidXml property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotDemarcateInvalidXml() {
        return doNotDemarcateInvalidXml;
    }

    /**
     * Sets the value of the doNotDemarcateInvalidXml property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotDemarcateInvalidXml(BooleanDefaultTrue value) {
        this.doNotDemarcateInvalidXml = value;
    }

    /**
     * Gets the value of the saveXmlDataOnly property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getSaveXmlDataOnly() {
        return saveXmlDataOnly;
    }

    /**
     * Sets the value of the saveXmlDataOnly property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setSaveXmlDataOnly(BooleanDefaultTrue value) {
        this.saveXmlDataOnly = value;
    }

    /**
     * Gets the value of the useXSLTWhenSaving property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getUseXSLTWhenSaving() {
        return useXSLTWhenSaving;
    }

    /**
     * Sets the value of the useXSLTWhenSaving property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setUseXSLTWhenSaving(BooleanDefaultTrue value) {
        this.useXSLTWhenSaving = value;
    }

    /**
     * Gets the value of the saveThroughXslt property.
     *
     * @return
     *     possible object is
     *     {@link CTSaveThroughXslt }
     *
     */
    public CTSaveThroughXslt getSaveThroughXslt() {
        return saveThroughXslt;
    }

    /**
     * Sets the value of the saveThroughXslt property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSaveThroughXslt }
     *
     */
    public void setSaveThroughXslt(CTSaveThroughXslt value) {
        this.saveThroughXslt = value;
    }

    /**
     * Gets the value of the showXMLTags property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getShowXMLTags() {
        return showXMLTags;
    }

    /**
     * Sets the value of the showXMLTags property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setShowXMLTags(BooleanDefaultTrue value) {
        this.showXMLTags = value;
    }

    /**
     * Gets the value of the alwaysMergeEmptyNamespace property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getAlwaysMergeEmptyNamespace() {
        return alwaysMergeEmptyNamespace;
    }

    /**
     * Sets the value of the alwaysMergeEmptyNamespace property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setAlwaysMergeEmptyNamespace(BooleanDefaultTrue value) {
        this.alwaysMergeEmptyNamespace = value;
    }

    /**
     * Gets the value of the updateFields property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getUpdateFields() {
        return updateFields;
    }

    /**
     * Sets the value of the updateFields property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setUpdateFields(BooleanDefaultTrue value) {
        this.updateFields = value;
    }

    /**
     * Gets the value of the hdrShapeDefaults property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeDefaults }
     *
     */
    public CTShapeDefaults getHdrShapeDefaults() {
        return hdrShapeDefaults;
    }

    /**
     * Sets the value of the hdrShapeDefaults property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeDefaults }
     *
     */
    public void setHdrShapeDefaults(CTShapeDefaults value) {
        this.hdrShapeDefaults = value;
    }

    /**
     * Gets the value of the footnotePr property.
     *
     * @return
     *     possible object is
     *     {@link CTFtnDocProps }
     *
     */
    public CTFtnDocProps getFootnotePr() {
        return footnotePr;
    }

    /**
     * Sets the value of the footnotePr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTFtnDocProps }
     *
     */
    public void setFootnotePr(CTFtnDocProps value) {
        this.footnotePr = value;
    }

    /**
     * Gets the value of the endnotePr property.
     *
     * @return
     *     possible object is
     *     {@link CTEdnDocProps }
     *
     */
    public CTEdnDocProps getEndnotePr() {
        return endnotePr;
    }

    /**
     * Sets the value of the endnotePr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTEdnDocProps }
     *
     */
    public void setEndnotePr(CTEdnDocProps value) {
        this.endnotePr = value;
    }

    /**
     * Gets the value of the compat property.
     *
     * @return
     *     possible object is
     *     {@link CTCompat }
     *
     */
    public CTCompat getCompat() {
        return compat;
    }

    /**
     * Sets the value of the compat property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCompat }
     *
     */
    public void setCompat(CTCompat value) {
        this.compat = value;
    }

    /**
     * Gets the value of the docVars property.
     *
     * @return
     *     possible object is
     *     {@link CTDocVars }
     *
     */
    public CTDocVars getDocVars() {
        return docVars;
    }

    /**
     * Sets the value of the docVars property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDocVars }
     *
     */
    public void setDocVars(CTDocVars value) {
        this.docVars = value;
    }

    /**
     * Gets the value of the rsids property.
     *
     * @return
     *     possible object is
     *     {@link CTDocRsids }
     *
     */
    public CTDocRsids getRsids() {
        return rsids;
    }

    /**
     * Sets the value of the rsids property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDocRsids }
     *
     */
    public void setRsids(CTDocRsids value) {
        this.rsids = value;
    }

    /**
     * properties of math in the document
     *
     * @return
     *     possible object is
     *     {@link CTMathPr }
     *
     */
    public CTMathPr getMathPr() {
        return mathPr;
    }

    /**
     * Sets the value of the mathPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTMathPr }
     *
     */
    public void setMathPr(CTMathPr value) {
        this.mathPr = value;
    }

    /**
     * Gets the value of the uiCompat97To2003 property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getUiCompat97To2003() {
        return uiCompat97To2003;
    }

    /**
     * Sets the value of the uiCompat97To2003 property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setUiCompat97To2003(BooleanDefaultTrue value) {
        this.uiCompat97To2003 = value;
    }

    /**
     * Gets the value of the attachedSchema property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachedSchema property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachedSchema().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSettings.AttachedSchema }
     *
     *
     */
    public List<CTSettings.AttachedSchema> getAttachedSchema() {
        if (attachedSchema == null) {
            attachedSchema = new ArrayList<CTSettings.AttachedSchema>();
        }
        return this.attachedSchema;
    }

    /**
     * Gets the value of the themeFontLang property.
     *
     * @return
     *     possible object is
     *     {@link CTLanguage }
     *
     */
    public CTLanguage getThemeFontLang() {
        return themeFontLang;
    }

    /**
     * Sets the value of the themeFontLang property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLanguage }
     *
     */
    public void setThemeFontLang(CTLanguage value) {
        this.themeFontLang = value;
    }

    /**
     * Gets the value of the clrSchemeMapping property.
     *
     * @return
     *     possible object is
     *     {@link CTColorSchemeMapping }
     *
     */
    public CTColorSchemeMapping getClrSchemeMapping() {
        return clrSchemeMapping;
    }

    /**
     * Sets the value of the clrSchemeMapping property.
     *
     * @param value
     *     allowed object is
     *     {@link CTColorSchemeMapping }
     *
     */
    public void setClrSchemeMapping(CTColorSchemeMapping value) {
        this.clrSchemeMapping = value;
    }

    /**
     * Gets the value of the doNotIncludeSubdocsInStats property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotIncludeSubdocsInStats() {
        return doNotIncludeSubdocsInStats;
    }

    /**
     * Sets the value of the doNotIncludeSubdocsInStats property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotIncludeSubdocsInStats(BooleanDefaultTrue value) {
        this.doNotIncludeSubdocsInStats = value;
    }

    /**
     * Gets the value of the doNotAutoCompressPictures property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotAutoCompressPictures() {
        return doNotAutoCompressPictures;
    }

    /**
     * Sets the value of the doNotAutoCompressPictures property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotAutoCompressPictures(BooleanDefaultTrue value) {
        this.doNotAutoCompressPictures = value;
    }

    /**
     * Gets the value of the forceUpgrade property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.ForceUpgrade }
     *
     */
    public CTSettings.ForceUpgrade getForceUpgrade() {
        return forceUpgrade;
    }

    /**
     * Sets the value of the forceUpgrade property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.ForceUpgrade }
     *
     */
    public void setForceUpgrade(CTSettings.ForceUpgrade value) {
        this.forceUpgrade = value;
    }

    /**
     * Gets the value of the captions property.
     *
     * @return
     *     possible object is
     *     {@link CTCaptions }
     *
     */
    public CTCaptions getCaptions() {
        return captions;
    }

    /**
     * Sets the value of the captions property.
     *
     * @param value
     *     allowed object is
     *     {@link CTCaptions }
     *
     */
    public void setCaptions(CTCaptions value) {
        this.captions = value;
    }

    /**
     * Gets the value of the readModeInkLockDown property.
     *
     * @return
     *     possible object is
     *     {@link CTReadingModeInkLockDown }
     *
     */
    public CTReadingModeInkLockDown getReadModeInkLockDown() {
        return readModeInkLockDown;
    }

    /**
     * Sets the value of the readModeInkLockDown property.
     *
     * @param value
     *     allowed object is
     *     {@link CTReadingModeInkLockDown }
     *
     */
    public void setReadModeInkLockDown(CTReadingModeInkLockDown value) {
        this.readModeInkLockDown = value;
    }

    /**
     * Gets the value of the smartTagType property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the smartTagType property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSmartTagType().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTSmartTagType }
     *
     *
     */
    public List<CTSmartTagType> getSmartTagType() {
        if (smartTagType == null) {
            smartTagType = new ArrayList<CTSmartTagType>();
        }
        return this.smartTagType;
    }

    /**
     * Custom XML Schema List
     *
     * @return
     *     possible object is
     *     {@link SchemaLibrary }
     *
     */
    public SchemaLibrary getSchemaLibrary() {
        return schemaLibrary;
    }

    /**
     * Sets the value of the schemaLibrary property.
     *
     * @param value
     *     allowed object is
     *     {@link SchemaLibrary }
     *
     */
    public void setSchemaLibrary(SchemaLibrary value) {
        this.schemaLibrary = value;
    }

    /**
     * Gets the value of the shapeDefaults property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeDefaults }
     *
     */
    public CTShapeDefaults getShapeDefaults() {
        return shapeDefaults;
    }

    /**
     * Sets the value of the shapeDefaults property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeDefaults }
     *
     */
    public void setShapeDefaults(CTShapeDefaults value) {
        this.shapeDefaults = value;
    }

    /**
     * Gets the value of the doNotEmbedSmartTags property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getDoNotEmbedSmartTags() {
        return doNotEmbedSmartTags;
    }

    /**
     * Sets the value of the doNotEmbedSmartTags property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setDoNotEmbedSmartTags(BooleanDefaultTrue value) {
        this.doNotEmbedSmartTags = value;
    }

    /**
     * Gets the value of the decimalSymbol property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.DecimalSymbol }
     *
     */
    public CTSettings.DecimalSymbol getDecimalSymbol() {
        return decimalSymbol;
    }

    /**
     * Sets the value of the decimalSymbol property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.DecimalSymbol }
     *
     */
    public void setDecimalSymbol(CTSettings.DecimalSymbol value) {
        this.decimalSymbol = value;
    }

    /**
     * Gets the value of the listSeparator property.
     *
     * @return
     *     possible object is
     *     {@link CTSettings.ListSeparator }
     *
     */
    public CTSettings.ListSeparator getListSeparator() {
        return listSeparator;
    }

    /**
     * Sets the value of the listSeparator property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSettings.ListSeparator }
     *
     */
    public void setListSeparator(CTSettings.ListSeparator value) {
        this.listSeparator = value;
    }

    /**
     * Gets the value of the chartTrackingRefBased property.
     *
     * @return
     *     possible object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public BooleanDefaultTrue getChartTrackingRefBased() {
        return chartTrackingRefBased;
    }

    /**
     * Sets the value of the chartTrackingRefBased property.
     *
     * @param value
     *     allowed object is
     *     {@link BooleanDefaultTrue }
     *
     */
    public void setChartTrackingRefBased(BooleanDefaultTrue value) {
        this.chartTrackingRefBased = value;
    }

    /**
     * Gets the value of the docId14 property.
     *
     * @return
     *     possible object is
     *     {@link CTLongHexNumber }
     *
     */
    public CTLongHexNumber getDocId14() {
        return docId14;
    }

    /**
     * Sets the value of the docId14 property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLongHexNumber }
     *
     */
    public void setDocId14(CTLongHexNumber value) {
        this.docId14 = value;
    }

    /**
     * Gets the value of the docId15 property.
     *
     * @return
     *     possible object is
     *     {@link CTGuid }
     *
     */
    public CTGuid getDocId15() {
        return docId15;
    }

    /**
     * Sets the value of the docId15 property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGuid }
     *
     */
    public void setDocId15(CTGuid value) {
        this.docId15 = value;
    }

    /**
     * Gets the value of the conflictMode property.
     *
     * @return
     *     possible object is
     *     {@link CTOnOff }
     *
     */
    public CTOnOff getConflictMode() {
        return conflictMode;
    }

    /**
     * Sets the value of the conflictMode property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOnOff }
     *
     */
    public void setConflictMode(CTOnOff value) {
        this.conflictMode = value;
    }

    /**
     * Gets the value of the discardImageEditingData property.
     *
     * @return
     *     possible object is
     *     {@link CTOnOff }
     *
     */
    public CTOnOff getDiscardImageEditingData() {
        return discardImageEditingData;
    }

    /**
     * Sets the value of the discardImageEditingData property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOnOff }
     *
     */
    public void setDiscardImageEditingData(CTOnOff value) {
        this.discardImageEditingData = value;
    }

    /**
     * Gets the value of the defaultImageDpi property.
     *
     * @return
     *     possible object is
     *     {@link CTDefaultImageDpi }
     *
     */
    public CTDefaultImageDpi getDefaultImageDpi() {
        return defaultImageDpi;
    }

    /**
     * Sets the value of the defaultImageDpi property.
     *
     * @param value
     *     allowed object is
     *     {@link CTDefaultImageDpi }
     *
     */
    public void setDefaultImageDpi(CTDefaultImageDpi value) {
        this.defaultImageDpi = value;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        final Unmarshaller unmarshaller = documentPart.getUnmarshaller();
        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                boolean unmarshalled = false;
                if("http://schemas.openxmlformats.org/wordprocessingml/2006/main".equals(reader.getNamespaceURI())) {
                    unmarshalled = true;
                    switch(reader.getLocalName()) {
                        case "writeProtection": writeProtection = unmarshaller.unmarshal(reader, CTWriteProtection.class).getValue(); break;
                        case "view": view = unmarshaller.unmarshal(reader, CTView.class).getValue(); break;
                        case "zoom": zoom = unmarshaller.unmarshal(reader, CTZoom.class).getValue(); break;
                        case "removePersonalInformation": removePersonalInformation = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "removeDateAndTime": removeDateAndTime = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotDisplayPageBoundaries": doNotDisplayPageBoundaries = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "displayBackgroundShape": displayBackgroundShape = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "printPostScriptOverText": printPostScriptOverText = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "printFractionalCharacterWidth": printFractionalCharacterWidth = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "printFormsData": printFormsData = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "embedTrueTypeFonts": embedTrueTypeFonts = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "embedSystemFonts": embedSystemFonts = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "saveSubsetFonts": saveSubsetFonts = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "saveFormsData": saveFormsData = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "mirrorMargins": mirrorMargins = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "alignBordersAndEdges": alignBordersAndEdges = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "bordersDoNotSurroundHeader": bordersDoNotSurroundHeader = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "bordersDoNotSurroundFooter": bordersDoNotSurroundFooter = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "gutterAtTop": gutterAtTop = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "hideSpellingErrors": hideSpellingErrors = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "hideGrammaticalErrors": hideGrammaticalErrors = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "activeWritingStyle": this.getActiveWritingStyle().add(unmarshaller.unmarshal(reader, CTWritingStyle.class).getValue()); break;
                        case "proofState": proofState = unmarshaller.unmarshal(reader, CTProof.class).getValue(); break;
                        case "formsDesign": formsDesign = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "attachedTemplate": attachedTemplate = unmarshaller.unmarshal(reader, CTRel.class).getValue(); break;
                        case "linkStyles": linkStyles = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "stylePaneFormatFilter": stylePaneFormatFilter = unmarshaller.unmarshal(reader, CTShortHexNumber.class).getValue(); break;
                        case "stylePaneSortMethod": stylePaneSortMethod = unmarshaller.unmarshal(reader, CTShortHexNumber.class).getValue(); break;
                        case "documentType": documentType = unmarshaller.unmarshal(reader, CTDocType.class).getValue(); break;
                        case "mailMerge": mailMerge = unmarshaller.unmarshal(reader, CTMailMerge.class).getValue(); break;
                        case "revisionView": revisionView = unmarshaller.unmarshal(reader, CTTrackChangesView.class).getValue(); break;
                        case "trackRevisions": trackRevisions = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotTrackMoves": doNotTrackMoves = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotTrackFormatting": doNotTrackFormatting = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "documentProtection": documentProtection = unmarshaller.unmarshal(reader, CTDocProtect.class).getValue(); break;
                        case "autoFormatOverride": autoFormatOverride = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "styleLockTheme": styleLockTheme = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "styleLockQFSet": styleLockQFSet = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "defaultTabStop": defaultTabStop = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "autoHyphenation": autoHyphenation = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "consecutiveHyphenLimit": consecutiveHyphenLimit = unmarshaller.unmarshal(reader, CTSettings.ConsecutiveHyphenLimit.class).getValue(); break;
                        case "hyphenationZone": hyphenationZone = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "doNotHyphenateCaps": doNotHyphenateCaps = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "showEnvelope": showEnvelope = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "summaryLength": summaryLength = unmarshaller.unmarshal(reader, CTSettings.SummaryLength.class).getValue(); break;
                        case "clickAndTypeStyle": clickAndTypeStyle = unmarshaller.unmarshal(reader, CTSettings.ClickAndTypeStyle.class).getValue(); break;
                        case "defaultTableStyle": defaultTableStyle = unmarshaller.unmarshal(reader, CTSettings.DefaultTableStyle.class).getValue(); break;
                        case "evenAndOddHeaders": evenAndOddHeaders = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "bookFoldRevPrinting": bookFoldRevPrinting = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "bookFoldPrinting": bookFoldPrinting = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "bookFoldPrintingSheets": bookFoldPrintingSheets = unmarshaller.unmarshal(reader, CTSettings.BookFoldPrintingSheets.class).getValue(); break;
                        case "drawingGridHorizontalSpacing": drawingGridHorizontalSpacing = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "drawingGridVerticalSpacing": drawingGridVerticalSpacing = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "displayHorizontalDrawingGridEvery": displayHorizontalDrawingGridEvery = unmarshaller.unmarshal(reader, CTSettings.DisplayHorizontalDrawingGridEvery.class).getValue(); break;
                        case "displayVerticalDrawingGridEvery": displayVerticalDrawingGridEvery = unmarshaller.unmarshal(reader, CTSettings.DisplayVerticalDrawingGridEvery.class).getValue(); break;
                        case "doNotUseMarginsForDrawingGridOrigin": doNotUseMarginsForDrawingGridOrigin = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "drawingGridHorizontalOrigin": drawingGridHorizontalOrigin = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "drawingGridVerticalOrigin": drawingGridVerticalOrigin = unmarshaller.unmarshal(reader, CTTwipsMeasure.class).getValue(); break;
                        case "doNotShadeFormData": doNotShadeFormData = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "noPunctuationKerning": noPunctuationKerning = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "characterSpacingControl": characterSpacingControl = unmarshaller.unmarshal(reader, CTCharacterSpacing.class).getValue(); break;
                        case "printTwoOnOne": printTwoOnOne = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "strictFirstAndLastChars": strictFirstAndLastChars = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "noLineBreaksAfter": noLineBreaksAfter = unmarshaller.unmarshal(reader, CTKinsoku.class).getValue(); break;
                        case "noLineBreaksBefore": noLineBreaksBefore = unmarshaller.unmarshal(reader, CTKinsoku.class).getValue(); break;
                        case "savePreviewPicture": savePreviewPicture = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotValidateAgainstSchema": doNotValidateAgainstSchema = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "saveInvalidXml": saveInvalidXml = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "ignoreMixedContent": ignoreMixedContent = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "alwaysShowPlaceholderText": alwaysShowPlaceholderText = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotDemarcateInvalidXml": doNotDemarcateInvalidXml = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "saveXmlDataOnly": saveXmlDataOnly = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "useXSLTWhenSaving": useXSLTWhenSaving = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "saveThroughXslt": saveThroughXslt = unmarshaller.unmarshal(reader, CTSaveThroughXslt.class).getValue(); break;
                        case "showXMLTags": showXMLTags = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "alwaysMergeEmptyNamespace": alwaysMergeEmptyNamespace = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "updateFields": updateFields = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "hdrShapeDefaults": hdrShapeDefaults = unmarshaller.unmarshal(reader, CTShapeDefaults.class).getValue(); break;
                        case "footnotePr": footnotePr = unmarshaller.unmarshal(reader, CTFtnDocProps.class).getValue(); break;
                        case "endnotePr": endnotePr = unmarshaller.unmarshal(reader, CTEdnDocProps.class).getValue(); break;
                        case "compat": compat = unmarshaller.unmarshal(reader, CTCompat.class).getValue(); break;
                        case "docVars": docVars = unmarshaller.unmarshal(reader, CTDocVars.class).getValue(); break;
                        case "rsids": rsids = unmarshaller.unmarshal(reader, CTDocRsids.class).getValue(); break;
                        case "uiCompat97To2003": uiCompat97To2003 = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "attachedSchema": getAttachedSchema().add(unmarshaller.unmarshal(reader, CTSettings.AttachedSchema.class).getValue()); break;
                        case "themeFontLang": themeFontLang = unmarshaller.unmarshal(reader, CTLanguage.class).getValue(); break;
                        case "clrSchemeMapping": clrSchemeMapping = unmarshaller.unmarshal(reader, CTColorSchemeMapping.class).getValue(); break;
                        case "doNotIncludeSubdocsInStats": doNotIncludeSubdocsInStats = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "doNotAutoCompressPictures": doNotAutoCompressPictures = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "forceUpgrade": forceUpgrade = unmarshaller.unmarshal(reader, CTSettings.ForceUpgrade.class).getValue(); break;
                        case "captions": captions = unmarshaller.unmarshal(reader, CTCaptions.class).getValue(); break;
                        case "readModeInkLockDown": readModeInkLockDown = unmarshaller.unmarshal(reader, CTReadingModeInkLockDown.class).getValue(); break;
                        case "smartTagType": getSmartTagType().add(unmarshaller.unmarshal(reader, CTSmartTagType.class).getValue()); break;
                        case "shapeDefaults": shapeDefaults = unmarshaller.unmarshal(reader, CTShapeDefaults.class).getValue(); break;
                        case "doNotEmbedSmartTags": doNotEmbedSmartTags = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "decimalSymbol": decimalSymbol = unmarshaller.unmarshal(reader, CTSettings.DecimalSymbol.class).getValue(); break;
                        case "listSeparator": listSeparator = unmarshaller.unmarshal(reader, CTSettings.ListSeparator.class).getValue(); break;
                        default: unmarshalled = false;
                    }
                }
                else if("http://schemas.openxmlformats.org/schemaLibrary/2006/main".equals(reader.getNamespaceURI())) {
                    unmarshalled = true;
                    switch(reader.getLocalName()) {
                        case "schemaLibrary": schemaLibrary = unmarshaller.unmarshal(reader, SchemaLibrary.class).getValue(); break;
                        default: unmarshalled = false;
                    }
                }
                else if("http://schemas.openxmlformats.org/officeDocument/2006/math".equals(reader.getNamespaceURI())) {
                    unmarshalled = true;
                    switch(reader.getLocalName()) {
                        case "mathPr": mathPr = unmarshaller.unmarshal(reader, CTMathPr.class).getValue(); break;
                        default: unmarshalled = false;
                    }
                }
                else if("http://schemas.microsoft.com/office/word/2010/wordml".equals(reader.getNamespaceURI())) {
                    unmarshalled = true;
                    switch(reader.getLocalName()) {
                        case "docId14": docId14 = unmarshaller.unmarshal(reader, CTLongHexNumber.class).getValue(); break;
                        case "conflictMode": conflictMode = unmarshaller.unmarshal(reader, CTOnOff.class).getValue(); break;
                        case "discardImageEditingData": discardImageEditingData = unmarshaller.unmarshal(reader, CTOnOff.class).getValue(); break;
                        case "defaultImageDpi": defaultImageDpi = unmarshaller.unmarshal(reader, CTDefaultImageDpi.class).getValue(); break;
                        default: unmarshalled = false;
                    }
                }
                else if("http://schemas.microsoft.com/office/word/2012/wordml".equals(reader.getNamespaceURI())) {
                    unmarshalled = true;
                    switch(reader.getLocalName()) {
                        case "chartTrackingRefBased": chartTrackingRefBased = unmarshaller.unmarshal(reader, BooleanDefaultTrue.class).getValue(); break;
                        case "docId": docId15 = unmarshaller.unmarshal(reader, CTGuid.class).getValue(); break;
                        default: unmarshalled = false;
                    }
                }
                advanceEvent = unmarshalled ? reader.getEventType()==XMLStreamReader.END_ELEMENT : true;
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        if(writeProtection!=null) {
            marshaller.marshal(new JAXBElement<CTWriteProtection>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "writeProtection"), CTWriteProtection.class, writeProtection), writer);
        }
        if(view!=null) {
            marshaller.marshal(new JAXBElement<CTView>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "view"), CTView.class, view), writer);
        }
        if(zoom!=null) {
            marshaller.marshal(new JAXBElement<CTZoom>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "zoom"), CTZoom.class, zoom), writer);
        }
        if(removePersonalInformation!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "removePersonalInformation"), BooleanDefaultTrue.class, removePersonalInformation), writer);
        }
        if(removeDateAndTime!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "removeDateAndTime"), BooleanDefaultTrue.class, removeDateAndTime), writer);
        }
        if(doNotDisplayPageBoundaries!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotDisplayPageBoundaries"), BooleanDefaultTrue.class, doNotDisplayPageBoundaries), writer);
        }
        if(displayBackgroundShape!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "displayBackgroundShape"), BooleanDefaultTrue.class, displayBackgroundShape), writer);
        }
        if(printPostScriptOverText!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "printPostScriptOverText"), BooleanDefaultTrue.class, printPostScriptOverText), writer);
        }
        if(printFractionalCharacterWidth!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "printFractionalCharacterWidth"), BooleanDefaultTrue.class, printFractionalCharacterWidth), writer);
        }
        if(printFormsData!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "printFormsData"), BooleanDefaultTrue.class, printFormsData), writer);
        }
        if(embedTrueTypeFonts!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "embedTrueTypeFonts"), BooleanDefaultTrue.class, embedTrueTypeFonts), writer);
        }
        if(embedSystemFonts!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "embedSystemFonts"), BooleanDefaultTrue.class, embedSystemFonts), writer);
        }
        if(saveSubsetFonts!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "saveSubsetFonts"), BooleanDefaultTrue.class, saveSubsetFonts), writer);
        }
        if(saveFormsData!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "saveFormsData"), BooleanDefaultTrue.class, saveFormsData), writer);
        }
        if(mirrorMargins!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "mirrorMargins"), BooleanDefaultTrue.class, mirrorMargins), writer);
        }
        if(alignBordersAndEdges!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "alignBordersAndEdges"), BooleanDefaultTrue.class, alignBordersAndEdges), writer);
        }
        if(bordersDoNotSurroundHeader!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bordersDoNotSurroundHeader"), BooleanDefaultTrue.class, bordersDoNotSurroundHeader), writer);
        }
        if(bordersDoNotSurroundFooter!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bordersDoNotSurroundFooter"), BooleanDefaultTrue.class, bordersDoNotSurroundFooter), writer);
        }
        if(gutterAtTop!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "gutterAtTop"), BooleanDefaultTrue.class, gutterAtTop), writer);
        }
        if(hideSpellingErrors!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "hideSpellingErrors"), BooleanDefaultTrue.class, hideSpellingErrors), writer);
        }
        if(hideGrammaticalErrors!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "hideGrammaticalErrors"), BooleanDefaultTrue.class, hideGrammaticalErrors), writer);
        }
        for(CTWritingStyle writingStyle:getActiveWritingStyle()) {
            marshaller.marshal(new JAXBElement<CTWritingStyle>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "activeWritingStyle"),  CTWritingStyle.class, writingStyle), writer);
        }
        if(proofState!=null) {
            marshaller.marshal(new JAXBElement<CTProof>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "proofState"), CTProof.class, proofState), writer);
        }
        if(formsDesign!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "formsDesign"), BooleanDefaultTrue.class, formsDesign), writer);
        }
        if(attachedTemplate!=null) {
            marshaller.marshal(new JAXBElement<CTRel>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "attachedTemplate"), CTRel.class, attachedTemplate), writer);
        }
        if(linkStyles!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "linkStyles"), BooleanDefaultTrue.class, linkStyles), writer);
        }
        if(stylePaneFormatFilter!=null) {
            marshaller.marshal(new JAXBElement<CTShortHexNumber>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "stylePaneFormatFilter"), CTShortHexNumber.class, stylePaneFormatFilter), writer);
        }
        if(stylePaneSortMethod!=null) {
            marshaller.marshal(new JAXBElement<CTShortHexNumber>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "stylePaneSortMethod"), CTShortHexNumber.class, stylePaneSortMethod), writer);
        }
        if(documentType!=null) {
            marshaller.marshal(new JAXBElement<CTDocType>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "documentType"), CTDocType.class, documentType), writer);
        }
        if(mailMerge!=null) {
            marshaller.marshal(new JAXBElement<CTMailMerge>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "mailMerge"), CTMailMerge.class, mailMerge), writer);
        }
        if(revisionView!=null) {
            marshaller.marshal(new JAXBElement<CTTrackChangesView>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "revisionView"), CTTrackChangesView.class, revisionView), writer);
        }
        if(trackRevisions!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "trackRevisions"), BooleanDefaultTrue.class, trackRevisions), writer);
        }
        if(doNotTrackMoves!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotTrackMoves"), BooleanDefaultTrue.class, doNotTrackMoves), writer);
        }
        if(doNotTrackFormatting!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotTrackFormatting"), BooleanDefaultTrue.class, doNotTrackFormatting), writer);
        }
        if(documentProtection!=null) {
            marshaller.marshal(new JAXBElement<CTDocProtect>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "documentProtection"), CTDocProtect.class, documentProtection), writer);
        }
        if(autoFormatOverride!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "autoFormatOverride"), BooleanDefaultTrue.class, autoFormatOverride), writer);
        }
        if(styleLockTheme!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "styleLockTheme"), BooleanDefaultTrue.class, styleLockTheme), writer);
        }
        if(styleLockQFSet!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "styleLockQFSet"), BooleanDefaultTrue.class, styleLockQFSet), writer);
        }
        if(defaultTabStop!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "defaultTabStop"), CTTwipsMeasure.class, defaultTabStop), writer);
        }
        if(autoHyphenation!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "autoHyphenation"), BooleanDefaultTrue.class, autoHyphenation), writer);
        }
        if(consecutiveHyphenLimit!=null) {
            marshaller.marshal(new JAXBElement<ConsecutiveHyphenLimit>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "consecutiveHyphenLimit"), ConsecutiveHyphenLimit.class, consecutiveHyphenLimit), writer);
        }
        if(hyphenationZone!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "hyphenationZone"), CTTwipsMeasure.class, hyphenationZone), writer);
        }
        if(doNotHyphenateCaps!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotHyphenateCaps"), BooleanDefaultTrue.class, doNotHyphenateCaps), writer);
        }
        if(showEnvelope!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "showEnvelope"), BooleanDefaultTrue.class, showEnvelope), writer);
        }
        if(summaryLength!=null) {
            marshaller.marshal(new JAXBElement<SummaryLength>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "summaryLength"), SummaryLength.class, summaryLength), writer);
        }
        if(clickAndTypeStyle!=null) {
            marshaller.marshal(new JAXBElement<ClickAndTypeStyle>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "clickAndTypeStyle"), ClickAndTypeStyle.class, clickAndTypeStyle), writer);
        }
        if(defaultTableStyle!=null) {
            marshaller.marshal(new JAXBElement<DefaultTableStyle>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "defaultTableStyle"), DefaultTableStyle.class, defaultTableStyle), writer);
        }
        if(evenAndOddHeaders!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "evenAndOddHeaders"), BooleanDefaultTrue.class, evenAndOddHeaders), writer);
        }
        if(bookFoldRevPrinting!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookFoldRevPrinting"), BooleanDefaultTrue.class, bookFoldRevPrinting), writer);
        }
        if(bookFoldPrinting!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookFoldPrinting"), BooleanDefaultTrue.class, bookFoldPrinting), writer);
        }
        if(bookFoldPrintingSheets!=null) {
            marshaller.marshal(new JAXBElement<BookFoldPrintingSheets>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookFoldPrintingSheets"), BookFoldPrintingSheets.class, bookFoldPrintingSheets), writer);
        }
        if(drawingGridHorizontalSpacing!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "drawingGridHorizontalSpacing"), CTTwipsMeasure.class, drawingGridHorizontalSpacing), writer);
        }
        if(drawingGridVerticalSpacing!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "drawingGridVerticalSpacing"), CTTwipsMeasure.class, drawingGridVerticalSpacing), writer);
        }
        if(displayHorizontalDrawingGridEvery!=null) {
            marshaller.marshal(new JAXBElement<DisplayHorizontalDrawingGridEvery>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "displayHorizontalDrawingGridEvery"), DisplayHorizontalDrawingGridEvery.class, displayHorizontalDrawingGridEvery), writer);
        }
        if(displayVerticalDrawingGridEvery!=null) {
            marshaller.marshal(new JAXBElement<DisplayVerticalDrawingGridEvery>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "displayVerticalDrawingGridEvery"), DisplayVerticalDrawingGridEvery.class, displayVerticalDrawingGridEvery), writer);
        }
        if(doNotUseMarginsForDrawingGridOrigin!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotUseMarginsForDrawingGridOrigin"), BooleanDefaultTrue.class, doNotUseMarginsForDrawingGridOrigin), writer);
        }
        if(drawingGridHorizontalOrigin!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "drawingGridHorizontalOrigin"), CTTwipsMeasure.class, drawingGridHorizontalOrigin), writer);
        }
        if(drawingGridVerticalOrigin!=null) {
            marshaller.marshal(new JAXBElement<CTTwipsMeasure>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "drawingGridVerticalOrigin"), CTTwipsMeasure.class, drawingGridVerticalOrigin), writer);
        }
        if(doNotShadeFormData!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotShadeFormData"), BooleanDefaultTrue.class, doNotShadeFormData), writer);
        }
        if(noPunctuationKerning!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "noPunctuationKerning"), BooleanDefaultTrue.class, noPunctuationKerning), writer);
        }
        if(characterSpacingControl!=null) {
            marshaller.marshal(new JAXBElement<CTCharacterSpacing>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "characterSpacingControl"), CTCharacterSpacing.class, characterSpacingControl), writer);
        }
        if(printTwoOnOne!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "printTwoOnOne"), BooleanDefaultTrue.class, printTwoOnOne), writer);
        }
        if(strictFirstAndLastChars!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "strictFirstAndLastChars"), BooleanDefaultTrue.class, strictFirstAndLastChars), writer);
        }
        if(noLineBreaksAfter!=null) {
            marshaller.marshal(new JAXBElement<CTKinsoku>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "noLineBreaksAfter"), CTKinsoku.class, noLineBreaksAfter), writer);
        }
        if(noLineBreaksBefore!=null) {
            marshaller.marshal(new JAXBElement<CTKinsoku>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "noLineBreaksBefore"), CTKinsoku.class, noLineBreaksBefore), writer);
        }
        if(savePreviewPicture!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "savePreviewPicture"), BooleanDefaultTrue.class, savePreviewPicture), writer);
        }
        if(doNotValidateAgainstSchema!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotValidateAgainstSchema"), BooleanDefaultTrue.class, doNotValidateAgainstSchema), writer);
        }
        if(saveInvalidXml!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "saveInvalidXml"), BooleanDefaultTrue.class, saveInvalidXml), writer);
        }
        if(ignoreMixedContent!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ignoreMixedContent"), BooleanDefaultTrue.class, ignoreMixedContent), writer);
        }
        if(alwaysShowPlaceholderText!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "alwaysShowPlaceholderText"), BooleanDefaultTrue.class, alwaysShowPlaceholderText), writer);
        }
        if(doNotDemarcateInvalidXml!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotDemarcateInvalidXml"), BooleanDefaultTrue.class, doNotDemarcateInvalidXml), writer);
        }
        if(saveXmlDataOnly!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "saveXmlDataOnly"), BooleanDefaultTrue.class, saveXmlDataOnly), writer);
        }
        if(useXSLTWhenSaving!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "useXSLTWhenSaving"), BooleanDefaultTrue.class, useXSLTWhenSaving), writer);
        }
        if(saveThroughXslt!=null) {
            marshaller.marshal(new JAXBElement<CTSaveThroughXslt>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "saveThroughXslt"), CTSaveThroughXslt.class, saveThroughXslt), writer);
        }
        if(showXMLTags!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "showXMLTags"), BooleanDefaultTrue.class, showXMLTags), writer);
        }
        if(alwaysMergeEmptyNamespace!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "alwaysMergeEmptyNamespace"), BooleanDefaultTrue.class, alwaysMergeEmptyNamespace), writer);
        }
        if(updateFields!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "updateFields"), BooleanDefaultTrue.class, updateFields), writer);
        }
        if(hdrShapeDefaults!=null) {
            marshaller.marshal(new JAXBElement<CTShapeDefaults>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "hdrShapeDefaults"), CTShapeDefaults.class, hdrShapeDefaults), writer);
        }
        if(footnotePr!=null) {
            marshaller.marshal(new JAXBElement<CTFtnDocProps>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "footnotePr"), CTFtnDocProps.class, footnotePr), writer);
        }
        if(endnotePr!=null) {
            marshaller.marshal(new JAXBElement<CTEdnDocProps>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "endnotePr"), CTEdnDocProps.class, endnotePr), writer);
        }
        if(compat!=null) {
            marshaller.marshal(new JAXBElement<CTCompat>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "compat"), CTCompat.class, compat), writer);
        }
        if(docVars!=null) {
            marshaller.marshal(new JAXBElement<CTDocVars>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "docVars"), CTDocVars.class, docVars), writer);
        }
        if(rsids!=null) {
            marshaller.marshal(new JAXBElement<CTDocRsids>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rsids"), CTDocRsids.class, rsids), writer);
        }
        if(mathPr!=null) {
            marshaller.marshal(new JAXBElement<CTMathPr>(new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "mathPr"), CTMathPr.class, mathPr), writer);
        }
        if(uiCompat97To2003!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "uiCompat97To2003"), BooleanDefaultTrue.class, uiCompat97To2003), writer);
        }
        for(AttachedSchema a:getAttachedSchema()) {
            marshaller.marshal(new JAXBElement<AttachedSchema>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "attachedSchema"), AttachedSchema.class, a), writer);
        }
        if(themeFontLang!=null) {
            marshaller.marshal(new JAXBElement<CTLanguage>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "themeFontLang"), CTLanguage.class, themeFontLang), writer);
        }
        if(clrSchemeMapping!=null) {
            marshaller.marshal(new JAXBElement<CTColorSchemeMapping>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "clrSchemeMapping"), CTColorSchemeMapping.class, clrSchemeMapping), writer);
        }
        if(doNotIncludeSubdocsInStats!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotIncludeSubdocsInStats"), BooleanDefaultTrue.class, doNotIncludeSubdocsInStats), writer);
        }
        if(doNotAutoCompressPictures!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotAutoCompressPictures"), BooleanDefaultTrue.class, doNotAutoCompressPictures), writer);
        }
        if(forceUpgrade!=null) {
            marshaller.marshal(new JAXBElement<ForceUpgrade>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "forceUpgrade"), ForceUpgrade.class, forceUpgrade), writer);
        }
        if(captions!=null) {
            marshaller.marshal(new JAXBElement<CTCaptions>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "captions"), CTCaptions.class, captions), writer);
        }
        if(readModeInkLockDown!=null) {
            marshaller.marshal(new JAXBElement<CTReadingModeInkLockDown>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "readModeInkLockDown"), CTReadingModeInkLockDown.class, readModeInkLockDown), writer);
        }
        for(CTSmartTagType tag:getSmartTagType()) {
            marshaller.marshal(new JAXBElement<CTSmartTagType>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "smartTagType"), CTSmartTagType.class, tag), writer);
        }
        if(schemaLibrary!=null) {
            marshaller.marshal(new JAXBElement<SchemaLibrary>(new QName("http://schemas.openxmlformats.org/schemaLibrary/2006/main", "schemaLibrary"), SchemaLibrary.class, schemaLibrary), writer);
        }
        if(shapeDefaults!=null) {
            marshaller.marshal(new JAXBElement<CTShapeDefaults>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "shapeDefaults"), CTShapeDefaults.class, shapeDefaults), writer);
        }
        if(doNotEmbedSmartTags!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "doNotEmbedSmartTags"), BooleanDefaultTrue.class, doNotEmbedSmartTags), writer);
        }
        if(decimalSymbol!=null) {
            marshaller.marshal(new JAXBElement<DecimalSymbol>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "decimalSymbol"), DecimalSymbol.class, decimalSymbol), writer);
        }
        if(listSeparator!=null) {
            marshaller.marshal(new JAXBElement<ListSeparator>(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "listSeparator"), ListSeparator.class, listSeparator), writer);
        }
        if(chartTrackingRefBased!=null) {
            marshaller.marshal(new JAXBElement<BooleanDefaultTrue>(new QName("http://schemas.microsoft.com/office/word/2012/wordml", "chartTrackingRefBased"), BooleanDefaultTrue.class, chartTrackingRefBased), writer);
        }
        if(docId14!=null) {
            marshaller.marshal(new JAXBElement<CTLongHexNumber>(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "docId14"), CTLongHexNumber.class, docId14), writer);
        }
        if(docId15!=null) {
            marshaller.marshal(new JAXBElement<CTGuid>(new QName("http://schemas.microsoft.com/office/word/2012/wordml", "docId15"), CTGuid.class, docId15), writer);
        }
        if(conflictMode!=null) {
            marshaller.marshal(new JAXBElement<CTOnOff>(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "conflictMode"), CTOnOff.class, conflictMode), writer);
        }
        if(discardImageEditingData!=null) {
            marshaller.marshal(new JAXBElement<CTOnOff>(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "discardImageEditingData"), CTOnOff.class, discardImageEditingData), writer);
        }
        if(defaultImageDpi!=null) {
            marshaller.marshal(new JAXBElement<CTDefaultImageDpi>(new QName("http://schemas.microsoft.com/office/word/2010/wordml", "defaultImageDpi"), CTDefaultImageDpi.class, defaultImageDpi), writer);
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AttachedSchema {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVal(String value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BookFoldPrintingSheets {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected BigInteger val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setVal(BigInteger value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ClickAndTypeStyle {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVal(String value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ConsecutiveHyphenLimit {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected BigInteger val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setVal(BigInteger value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DecimalSymbol {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVal(String value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DefaultTableStyle {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVal(String value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DisplayHorizontalDrawingGridEvery {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected BigInteger val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setVal(BigInteger value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DisplayVerticalDrawingGridEvery {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected BigInteger val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setVal(BigInteger value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ForceUpgrade {
        //
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ListSeparator {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setVal(String value) {
            this.val = value;
        }
    }

    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="val" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SummaryLength {

        @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", required = true)
        protected BigInteger val;

        /**
         * Gets the value of the val property.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getVal() {
            return val;
        }

        /**
         * Sets the value of the val property.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setVal(BigInteger value) {
            this.val = value;
        }
    }
}
