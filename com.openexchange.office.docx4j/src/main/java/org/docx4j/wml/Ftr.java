/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.math.CTOMath;
import org.docx4j.math.CTOMathPara;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;

/**
 * Footer
 *
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_BlockLevelElts" maxOccurs="unbounded"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "ftr")
public class Ftr extends DocumentSerialization implements IContentAccessor<Object> {

    public Ftr() {
        super(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main",  "ftr", "w"), DocumentSerialization.Standard_DOCX_Namespaces, DocumentSerialization.Standard_DOCX_Ignorables);
    }

    protected DLList<Object> content;

    /**
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
     * {@link RunIns }
     * {@link P }
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
     * {@link JAXBElement }{@code <}{@link Tbl }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveToRangeEnd }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCustomXmlBlock }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
     * {@link ProofErr }
     * {@link CommentRangeEnd }
     * {@link RunDel }
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link SdtBlock }
     * {@link JAXBElement }{@code <}{@link CTOMathPara }{@code >}
     * {@link JAXBElement }{@code <}{@link CTAltChunk }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link CommentRangeStart }
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPerm }{@code >}
     * {@link JAXBElement }{@code <}{@link CTOMath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTBookmark }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveFromRangeEnd }{@code >}
     * {@link JAXBElement }{@code <}{@link RangePermissionStart }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkupRange }{@code >}
     *
     *
     */

    @Override
    public List<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        return this.content;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                if("http://schemas.openxmlformats.org/wordprocessingml/2006/main".equals(reader.getNamespaceURI())) {
                    final String localName = reader.getLocalName();
                    Object result;
                    switch(localName) {
                        case "p" :
                        case "tbl" :
                        case "ins" :
                        case "del" :
                        case "sdt" :
                        case "proofErr" :
                        case "moveTo" :
                        case "moveFrom" :
                        case "commentRangeEnd" :
                        case "commentRangeStart" : {
                            result = documentPart.getUnmarshaller().unmarshal(reader);
                        }
                        break;

                        default: { // JAXBElement is sufficient
                            result = documentPart.getUnmarshaller().unmarshal(reader, JAXBElement.class);
                        }
                    }
                    getContent().add(result);
                }
                advanceEvent = reader.getEventType() == XMLStreamReader.END_ELEMENT;
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        final Iterator<Object> contentIter = getContent().iterator();
        while(contentIter.hasNext()) {
            marshaller.marshal(contentIter.next(), writer);
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }
}
