/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.jaxb.Context;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;


/**
 * <p>Java class for CT_SdtRun complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_SdtRun">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sdtPr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_SdtPr" minOccurs="0"/>
 *         &lt;element name="sdtEndPr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_SdtEndPr" minOccurs="0"/>
 *         &lt;element name="sdtContent" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_SdtContentRun" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SdtRun", propOrder = {
    "sdtPr",
    "sdtEndPr",
    "sdtContent"
})
@XmlRootElement(name = "sdt")
public class SdtRun implements SdtElement, Child
{
    protected SdtPr sdtPr;
    protected CTSdtEndPr sdtEndPr;
    protected CTSdtContentRun sdtContent;
    @XmlTransient
    private Object parent;
    @XmlTransient
    private boolean sdtContentCreated = false;

    @Override
    public DLList<Object> getContent() {
        if(sdtContent==null) {
            sdtContent = Context.getWmlObjectFactory().createCTSdtContentRun();
            sdtContentCreated = true;
        }
        return sdtContent.getContent();
    }

    /**
     * Gets the value of the sdtPr property.
     *
     * @return
     *     possible object is
     *     {@link SdtPr }
     *
     */
    @Override
    public SdtPr getSdtPr() {
        return sdtPr;
    }

    /**
     * Sets the value of the sdtPr property.
     *
     * @param value
     *     allowed object is
     *     {@link SdtPr }
     *
     */
    @Override
    public void setSdtPr(SdtPr value) {
        this.sdtPr = value;
    }

    /**
     * Gets the value of the sdtEndPr property.
     *
     * @return
     *     possible object is
     *     {@link CTSdtEndPr }
     *
     */
    @Override
    public CTSdtEndPr getSdtEndPr() {
        return sdtEndPr;
    }

    /**
     * Sets the value of the sdtEndPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSdtEndPr }
     *
     */
    @Override
    public void setSdtEndPr(CTSdtEndPr value) {
        this.sdtEndPr = value;
    }

    /**
     * Gets the value of the sdtContent property.
     *
     * @return
     *     possible object is
     *     {@link CTSdtContentRun }
     *
     */
    @Override
    public IContentAccessor<?> getSdtContent() {
        return sdtContent;
    }

//    public SdtContent getContent() {
//        return sdtContent;
//    }

    /**
     * Sets the value of the sdtContent property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSdtContentRun }
     *
     */
    public void setSdtContent(CTSdtContentRun value) {
        this.sdtContent = value;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        if(sdtContentCreated&&getContent().isEmpty()) {
            sdtContent = null;
        }
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param _parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
        if(sdtContent!=null) {
            final DLList<Object> childList = getContent();
            for(int i=0; i<childList.size(); i++) {
                Object child = childList.get(i);
                if(child instanceof Child) {
                    ((Child) child).setParent(this);
                }
            }
        }
    }
}
