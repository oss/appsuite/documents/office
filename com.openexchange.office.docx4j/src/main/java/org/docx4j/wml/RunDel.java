/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.IChildNodeCombiner;
import com.openexchange.office.filter.core.INodeAccessor;

/**
 * Deleted Run Content
 *
 * <p>Java class for del element declaration.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;element name="del">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;extension base="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_TrackChange">
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_ContentRunContent"/>
 *           &lt;group ref="{http://schemas.openxmlformats.org/officeDocument/2006/math}EG_OMathMathElements"/>
 *         &lt;/choice>
 *       &lt;/extension>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "del")
public class RunDel extends RDBase implements IChildNodeCombiner {

    @Override
    public boolean tryCombineWithNextNode(INodeAccessor<Object> parentNode, DLNode<Object> nextNode) {
        if(nextNode!=null && nextNode.getData() instanceof RunDel) {
            final RunDel nextRunDel = (RunDel)nextNode.getData();
            if(getAuthor().equals(nextRunDel.getAuthor())) {
                final long time = getDate() == null ? Long.valueOf(0) : getDate().toGregorianCalendar().getTimeInMillis();
                final long nextTime = nextRunDel.getDate() == null ? Long.valueOf(0) : nextRunDel.getDate().toGregorianCalendar().getTimeInMillis();
                if(Math.abs(nextTime - time) < 5000) {
                    // runDel can be combined, moving all childs from the nextNode into this node
                    nextRunDel.getContent().moveNodes(getContent(), this);
                    parentNode.getContent().removeNode(nextNode);
                    if(nextTime > time) {
                        setDate(nextRunDel.getDate());
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    final public void beforeMarshal(Marshaller marshaller) {
        super.beforeMarshal(marshaller);
        if(content!=null) {
            IChildNodeCombiner.optimize(this);
        }
    }
}
