/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.wml; 

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="hint" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_Hint" />
 *       &lt;attribute name="ascii" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *       &lt;attribute name="hAnsi" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *       &lt;attribute name="eastAsia" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *       &lt;attribute name="cs" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *       &lt;attribute name="asciiTheme" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_Theme" />
 *       &lt;attribute name="hAnsiTheme" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_Theme" />
 *       &lt;attribute name="eastAsiaTheme" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_Theme" />
 *       &lt;attribute name="cstheme" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_Theme" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "rFonts")
public class RFonts implements Cloneable {

    @XmlAttribute(name = "hint", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STHint hint;
    @XmlAttribute(name = "ascii", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected String ascii;
    @XmlAttribute(name = "hAnsi", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected String hAnsi;
    @XmlAttribute(name = "eastAsia", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected String eastAsia;
    @XmlAttribute(name = "cs", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected String cs;
    @XmlAttribute(name = "asciiTheme", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STTheme asciiTheme;
    @XmlAttribute(name = "hAnsiTheme", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STTheme hAnsiTheme;
    @XmlAttribute(name = "eastAsiaTheme", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STTheme eastAsiaTheme;
    @XmlAttribute(name = "cstheme", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STTheme cstheme;

    /**
     * Gets the value of the hint property.
     * 
     * @return
     *     possible object is
     *     {@link STHint }
     *     
     */
    public STHint getHint() {
        return hint;
    }

    /**
     * Sets the value of the hint property.
     * 
     * @param value
     *     allowed object is
     *     {@link STHint }
     *     
     */
    public void setHint(STHint value) {
        this.hint = value;
    }

    /**
     * Gets the value of the ascii property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAscii() {
        return ascii;
    }

    /**
     * Sets the value of the ascii property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAscii(String value) {
        this.ascii = value;
    }

    /**
     * Gets the value of the hAnsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHAnsi() {
        return hAnsi;
    }

    /**
     * Sets the value of the hAnsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHAnsi(String value) {
        this.hAnsi = value;
    }

    /**
     * Gets the value of the eastAsia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEastAsia() {
        return eastAsia;
    }

    /**
     * Sets the value of the eastAsia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEastAsia(String value) {
        this.eastAsia = value;
    }

    /**
     * Gets the value of the cs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCs() {
        return cs;
    }

    /**
     * Sets the value of the cs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCs(String value) {
        this.cs = value;
    }

    /**
     * Gets the value of the asciiTheme property.
     * 
     * @return
     *     possible object is
     *     {@link STTheme }
     *     
     */
    public STTheme getAsciiTheme() {
        return asciiTheme;
    }

    /**
     * Sets the value of the asciiTheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTheme }
     *     
     */
    public void setAsciiTheme(STTheme value) {
        this.asciiTheme = value;
    }

    /**
     * Gets the value of the hAnsiTheme property.
     * 
     * @return
     *     possible object is
     *     {@link STTheme }
     *     
     */
    public STTheme getHAnsiTheme() {
        return hAnsiTheme;
    }

    /**
     * Sets the value of the hAnsiTheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTheme }
     *     
     */
    public void setHAnsiTheme(STTheme value) {
        this.hAnsiTheme = value;
    }

    /**
     * Gets the value of the eastAsiaTheme property.
     * 
     * @return
     *     possible object is
     *     {@link STTheme }
     *     
     */
    public STTheme getEastAsiaTheme() {
        return eastAsiaTheme;
    }

    /**
     * Sets the value of the eastAsiaTheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTheme }
     *     
     */
    public void setEastAsiaTheme(STTheme value) {
        this.eastAsiaTheme = value;
    }

    /**
     * Gets the value of the cstheme property.
     * 
     * @return
     *     possible object is
     *     {@link STTheme }
     *     
     */
    public STTheme getCstheme() {
        return cstheme;
    }

    /**
     * Sets the value of the cstheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link STTheme }
     *     
     */
    public void setCstheme(STTheme value) {
        this.cstheme = value;
    }

    @Override
    public RFonts clone() {
        try {
            return (RFonts)super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ascii == null) ? 0 : ascii.hashCode());
        result = prime * result + ((asciiTheme == null) ? 0 : asciiTheme.hashCode());
        result = prime * result + ((cs == null) ? 0 : cs.hashCode());
        result = prime * result + ((cstheme == null) ? 0 : cstheme.hashCode());
        result = prime * result + ((eastAsia == null) ? 0 : eastAsia.hashCode());
        result = prime * result + ((eastAsiaTheme == null) ? 0 : eastAsiaTheme.hashCode());
        result = prime * result + ((hAnsi == null) ? 0 : hAnsi.hashCode());
        result = prime * result + ((hAnsiTheme == null) ? 0 : hAnsiTheme.hashCode());
        result = prime * result + ((hint == null) ? 0 : hint.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RFonts other = (RFonts) obj;
        if (ascii == null) {
            if (other.ascii != null)
                return false;
        } else if (!ascii.equals(other.ascii))
            return false;
        if (asciiTheme != other.asciiTheme)
            return false;
        if (cs == null) {
            if (other.cs != null)
                return false;
        } else if (!cs.equals(other.cs))
            return false;
        if (cstheme != other.cstheme)
            return false;
        if (eastAsia == null) {
            if (other.eastAsia != null)
                return false;
        } else if (!eastAsia.equals(other.eastAsia))
            return false;
        if (eastAsiaTheme != other.eastAsiaTheme)
            return false;
        if (hAnsi == null) {
            if (other.hAnsi != null)
                return false;
        } else if (!hAnsi.equals(other.hAnsi))
            return false;
        if (hAnsiTheme != other.hAnsiTheme)
            return false;
        if (hint != other.hint)
            return false;
        return true;
    }
}
