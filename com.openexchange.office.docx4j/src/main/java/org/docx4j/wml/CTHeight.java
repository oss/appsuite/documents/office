/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.wml; 

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.UnsignedLongAdapter;

/**
 * <p>Java class for CT_Height complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Height">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="val" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_TwipsMeasure" />
 *       &lt;attribute name="hRule" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_HeightRule" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Height")
public class CTHeight {

    @XmlAttribute(name = "val", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    @XmlJavaTypeAdapter(UnsignedLongAdapter.class)
    protected Long val;
    @XmlAttribute(name = "hRule", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected STHeightRule hRule;

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVal(Long value) {
        this.val = value;
    }

    /**
     * Gets the value of the hRule property.
     * 
     * @return
     *     possible object is
     *     {@link STHeightRule }
     *     
     */
    public STHeightRule getHRule() {
        return hRule;
    }

    /**
     * Sets the value of the hRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link STHeightRule }
     *     
     */
    public void setHRule(STHeightRule value) {
        this.hRule = value;
    }
}
