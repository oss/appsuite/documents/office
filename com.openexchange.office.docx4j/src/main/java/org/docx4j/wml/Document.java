/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import com.openexchange.office.filter.core.IContentAccessor;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="background" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_Background" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}body" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute ref="{http://schemas.openxmlformats.org/markup-compatibility/2006}Ignorable"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "background",
    "body"
})
@XmlRootElement(name = "document")
public class Document extends DocumentSerialization implements IContentAccessor<Object> {

    public Document() {
        super(new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main",  "document", "w"), DocumentSerialization.Standard_DOCX_Namespaces, DocumentSerialization.Standard_DOCX_Ignorables);
    }

    protected CTBackground background;
    protected Body body;

    /**
     * Gets the value of the background property.
     *
     * @return
     *     possible object is
     *     {@link CTBackground }
     *
     */
    public CTBackground getBackground() {
        return background;
    }

    /**
     * Sets the value of the background property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBackground }
     *
     */
    public void setBackground(CTBackground value) {
        this.background = value;
    }

    /**
     * Gets the value of the body property.
     *
     * @return
     *     possible object is
     *     {@link Body }
     *
     */
    public Body getBody() {
        return body;
    }

    /**
     * Convenience method to getBody().getContent()
     * @since 2.7
     */
    @Override
    public List<Object> getContent() {
        return getBody().getContent();
    }

    /**
     * Sets the value of the body property.
     *
     * @param value
     *     allowed object is
     *     {@link Body }
     *
     */
    public void setBody(Body value) {
        this.body = value;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                final String localName = reader.getLocalName();
                if("body".equals(localName)) {
                    final Object result = documentPart.getUnmarshaller().unmarshal(reader);
                    advanceEvent = reader.getEventType()==XMLStreamReader.END_ELEMENT;
                    if(result instanceof JAXBElement) {
                        body = ((JAXBElement<Body>)result).getValue();
                    }
                    else if(result instanceof Body) {
                        body = (Body)result;
                    }
                }
                else if("background".equals(localName)) {
                    final Object result = documentPart.getUnmarshaller().unmarshal(reader);
                    advanceEvent = reader.getEventType()==XMLStreamReader.END_ELEMENT;
                    if(result instanceof JAXBElement) {
                        background = ((JAXBElement<CTBackground>)result).getValue();
                    }
                    else if(result instanceof CTBackground) {
                        background = (CTBackground)result;
                    }
                }
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        if(background!=null) {
            marshaller.marshal(background, writer);
        }
        if(body!=null) {
            marshaller.marshal(body, writer);
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }
}
