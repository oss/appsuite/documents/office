/*
 *  Copyright 2007-2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wml;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.ValidationEventHandler;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.HexNumberIntegerAdapter;
import org.docx4j.jaxb.IValidationEventHandler;
import org.docx4j.math.CTOMath;
import org.docx4j.math.CTOMathPara;
import org.docx4j.openpackaging.packages.OpcPackage;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.IChildNodeCombiner;
import com.openexchange.office.filter.core.component.Child;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pPr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}CT_PPr" minOccurs="0"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_PContent" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://schemas.microsoft.com/office/word/2010/wordml}AG_Parids"/>
 *       &lt;attribute name="rsidRPr" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidR" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidDel" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidP" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="rsidRDefault" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pPr",
    "content"
})
@XmlRootElement(name = "p")
public class P implements Child, INodeAccessor<Object>, ISimpleFieldCollection {

    protected PPr pPr;
    @XmlElementRefs({
        @XmlElementRef(name = "customXmlMoveFromRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "ins", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunIns.class),
        @XmlElementRef(name = "customXmlInsRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "subDoc", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "moveFromRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "customXmlMoveToRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "moveFrom", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunMoveFrom.class),
        @XmlElementRef(name = "customXmlMoveToRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "customXmlMoveFromRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "fldSimple", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = CTSimpleField.class),
        @XmlElementRef(name = "permStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "moveTo", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunMoveTo.class),
        @XmlElementRef(name = "moveToRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "customXmlInsRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "proofErr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = ProofErr.class),
        @XmlElementRef(name = "commentRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = CommentRangeEnd.class),
        @XmlElementRef(name = "moveToRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "r", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = R.class),
        @XmlElementRef(name = "del", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunDel.class),
        @XmlElementRef(name = "bookmarkEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "smartTag", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "oMathPara", namespace = "http://schemas.openxmlformats.org/officeDocument/2006/math", type = JAXBElement.class),
        @XmlElementRef(name = "permEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "commentRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = CommentRangeStart.class),
        @XmlElementRef(name = "moveFromRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "oMath", namespace = "http://schemas.openxmlformats.org/officeDocument/2006/math", type = JAXBElement.class),
        @XmlElementRef(name = "customXml", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "customXmlDelRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "customXmlDelRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
        @XmlElementRef(name = "hyperlink", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = P.Hyperlink.class),
        @XmlElementRef(name = "sdt", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = SdtRun.class),
        @XmlElementRef(name = "bookmarkStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class)
    })
    protected DLList<Object> content;
    @XmlAttribute(name = "paraId", namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    protected Integer paraId;
    @XmlAttribute(name = "textId", namespace = "http://schemas.microsoft.com/office/word/2010/wordml")
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    protected Integer textId;
    @XmlTransient
    private Object parent;
    @XmlTransient
    private List<CTSimpleField> simpleFieldCollection;
    @XmlTransient
    private boolean modified;

    /**
     * Gets the value of the pPr property.
     *
     * @return
     *     possible object is
     *     {@link PPr }
     *
     */
    public PPr getPPr(boolean forceCreate) {
        if(pPr==null&&forceCreate) {
            pPr = new PPr();
        }
        return pPr;
    }

    /**
     * Sets the value of the pPr property.
     *
     * @param value
     *     allowed object is
     *     {@link PPr }
     *
     */
    public void setPPr(PPr value) {
        this.pPr = value;
    }

    /**
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link RunIns }
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link CTRel }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSimpleField }{@code >}
     * {@link JAXBElement }{@code <}{@link RangePermissionStart }{@code >}
     * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMoveToRangeEnd }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link ProofErr }
     * {@link CommentRangeEnd }
     * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
     * {@link R }
     * {@link RunDel }
     * {@link JAXBElement }{@code <}{@link CTMarkupRange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTSmartTagRun }{@code >}
     * {@link JAXBElement }{@code <}{@link CTOMathPara }{@code >}
     * {@link JAXBElement }{@code <}{@link CTPerm }{@code >}
     * {@link CommentRangeStart }
     * {@link JAXBElement }{@code <}{@link CTMoveFromRangeEnd }{@code >}
     * {@link JAXBElement }{@code <}{@link CTOMath }{@code >}
     * {@link JAXBElement }{@code <}{@link CTCustomXmlRun }{@code >}
     * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
     * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
     * {@link JAXBElement }{@code <}{@link SdtRun }{@code >}
     * {@link P.Hyperlink }
     * {@link JAXBElement }{@code <}{@link CTBookmark }{@code >}
     *
     *
     */
    @Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        else if(simpleFieldCollection!=null&&!simpleFieldCollection.isEmpty()) {
            CTSimpleField.convertSimpleFieldToComplexField(this);
        }
        return this.content;
    }

    // returns content without field conversion
    @Override
    public DLList<Object> getRawContent() {
    	return content;
    }

    @XmlAttribute(name = "rsidRPr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidRPr() {
        return null;
    }
    public void setRsidRPr(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidR", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidR() {
        return null;
    }
    public void setRsidR(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidDel", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidDel() {
        return null;
    }
    public void setRsidDel(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidP", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidP() {
        return null;
    }
    public void setRsidP(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    @XmlAttribute(name = "rsidRDefault", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    public String getRsidRDefault() {
        return null;
    }
    public void setRsidRDefault(@SuppressWarnings("unused") String value) {
        // this value is always ignored and removed
    }

    /**
     * Gets the value of the paraId property.
     *
     * From [MS-DOCX], "an identifier for a paragraph that is unique within the document part
     * (as specified by [ISO/IEC29500-1:2011] section 11.3), with the exception that it need
     * not be unique across the choices or fallback of an Alternate Content block
     * (as specified by [ISO/IEC29500-1:2011] section 17.17.3).
     *
     * Values MUST be greater than 0 and less than 0x80000000.
     *
     * Any element having this attribute MUST also have the textId attribute"
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getParaId() {
        return paraId;
    }

    /**
     * Sets the value of the paraId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParaId(Integer value) {
        this.paraId = value;
    }

    /**
     * Gets the value of the textId property.
     *
     * From [MS-DOCX], "a version identifier for a paragraph.
     *
     * Values MUST be greater than 0 and less than 0x80000000.
     *
     * Any element having this attribute MUST also have the paraId attribute.
     *
     * If two documents have the same docId, then if two paragraphs within the same respective
     * document part (as specified by [ISO/IEC29500-1:2011] section 11.3) that have the same
     * paraId and textId SHOULD contain identical text, although formatting could differ."
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getTextId() {
        return textId;
    }

    /**
     * Sets the value of the textId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTextId(Integer value) {
        this.textId = value;
    }

	@Override
    public List<CTSimpleField> getSimpleFieldCollection(boolean forceCreate) {
        if (simpleFieldCollection == null && forceCreate) {
        	simpleFieldCollection = new ArrayList<CTSimpleField>();
        }
        return simpleFieldCollection;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public boolean notVanished() {
        BooleanDefaultTrue vanish = null;
        if (pPr != null && pPr.getRPr(false) != null) {
            vanish = pPr.getRPr(false).getVanish();
        }
        return vanish == null || vanish.isVal() == false;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     *
     * @param _parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
        if(paraId!=null) {
            try {
                final ValidationEventHandler eventHandler = unmarshaller.getEventHandler();
                if(eventHandler instanceof IValidationEventHandler) {
                    final OpcPackage opcPackage = ((IValidationEventHandler)eventHandler).getPackage();
                    if(opcPackage!=null) {
                        opcPackage.addParaId(paraId);
                    }
                }
            }
            catch(JAXBException e) {
                //
            }
        }
    }

    final public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        if(modified && content!=null) {
            IChildNodeCombiner.optimize(this);
        }
    }

    /**
     * Hyperlink
     *
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;group ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}EG_PContent" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;attribute name="tgtFrame" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
     *       &lt;attribute name="tooltip" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
     *       &lt;attribute name="docLocation" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
     *       &lt;attribute name="history" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
     *       &lt;attribute name="anchor" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
     *       &lt;attribute ref="{http://schemas.openxmlformats.org/officeDocument/2006/relationships}id"/>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "content"
    })
    @XmlRootElement(name = "hyperlink")
    public static class Hyperlink implements Child, INodeAccessor<Object> {

        @XmlElementRefs({
            @XmlElementRef(name = "moveToRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "subDoc", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "moveToRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "sdt", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "customXml", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "ins", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunIns.class),
            @XmlElementRef(name = "hyperlink", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "bookmarkEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "customXmlInsRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "moveFrom", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunMoveFrom.class),
            @XmlElementRef(name = "proofErr", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = ProofErr.class),
            @XmlElementRef(name = "customXmlDelRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "commentRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = CommentRangeEnd.class),
            @XmlElementRef(name = "r", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = R.class),
            @XmlElementRef(name = "del", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunDel.class),
            @XmlElementRef(name = "customXmlMoveToRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "oMathPara", namespace = "http://schemas.openxmlformats.org/officeDocument/2006/math", type = JAXBElement.class),
            @XmlElementRef(name = "permEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "moveFromRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "moveFromRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "customXmlDelRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "customXmlMoveFromRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "customXmlMoveFromRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "bookmarkStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "smartTag", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "commentRangeStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = CommentRangeStart.class),
            @XmlElementRef(name = "moveTo", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = RunMoveTo.class),
            @XmlElementRef(name = "customXmlMoveToRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "permStart", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "oMath", namespace = "http://schemas.openxmlformats.org/officeDocument/2006/math", type = JAXBElement.class),
            @XmlElementRef(name = "customXmlInsRangeEnd", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class),
            @XmlElementRef(name = "fldSimple", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main", type = JAXBElement.class)
        })
        protected DLList<Object> content;
        @XmlAttribute(name = "tgtFrame", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String tgtFrame;
        @XmlAttribute(name = "tooltip", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String tooltip;
        @XmlAttribute(name = "docLocation", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String docLocation;
        @XmlAttribute(name = "history", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected Boolean history;
        @XmlAttribute(name = "anchor", namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
        protected String anchor;
        @XmlAttribute(name = "id", namespace = "http://schemas.openxmlformats.org/officeDocument/2006/relationships")
        protected String id;
        @XmlTransient
        private Object parent;

        /**
         * Gets the value of the content property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link CTMoveToRangeEnd }{@code >}
         * {@link JAXBElement }{@code <}{@link CTRel }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
         * {@link JAXBElement }{@code <}{@link SdtRun }{@code >}
         * {@link JAXBElement }{@code <}{@link CTCustomXmlRun }{@code >}
         * {@link RunIns }
         * {@link JAXBElement }{@code <}{@link P.Hyperlink }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMarkupRange }{@code >}
         * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
         * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
         * {@link ProofErr }
         * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
         * {@link CommentRangeEnd }
         * {@link R }
         * {@link RunDel }
         * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
         * {@link JAXBElement }{@code <}{@link CTOMathPara }{@code >}
         * {@link JAXBElement }{@code <}{@link CTPerm }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMoveFromRangeEnd }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMoveBookmark }{@code >}
         * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
         * {@link JAXBElement }{@code <}{@link CTTrackChange }{@code >}
         * {@link CommentRangeStart }
         * {@link JAXBElement }{@code <}{@link CTBookmark }{@code >}
         * {@link JAXBElement }{@code <}{@link CTSmartTagRun }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
         * {@link JAXBElement }{@code <}{@link RunTrackChange }{@code >}
         * {@link JAXBElement }{@code <}{@link RangePermissionStart }{@code >}
         * {@link JAXBElement }{@code <}{@link CTOMath }{@code >}
         * {@link JAXBElement }{@code <}{@link CTMarkup }{@code >}
         * {@link JAXBElement }{@code <}{@link CTSimpleField }{@code >}
         *
         * @since 2.7
         */
        @Override
		public DLList<Object> getContent() {
            if (content == null) {
                content = new DLList<Object>();
            }
            return this.content;
        }

        /**
         * Gets the value of the tgtFrame property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTgtFrame() {
            return tgtFrame;
        }

        /**
         * Sets the value of the tgtFrame property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTgtFrame(String value) {
            this.tgtFrame = value;
        }

        /**
         * Gets the value of the tooltip property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTooltip() {
            return tooltip;
        }

        /**
         * Sets the value of the tooltip property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTooltip(String value) {
            this.tooltip = value;
        }

        /**
         * Gets the value of the docLocation property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDocLocation() {
            return docLocation;
        }

        /**
         * Sets the value of the docLocation property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDocLocation(String value) {
            this.docLocation = value;
        }

        /**
         * Gets the value of the history property.
         *
         * @return
         *     possible object is
         *     {@link Boolean }
         *
         */
        public Boolean isHistory() {
            return history;
        }

        /**
         * Sets the value of the history property.
         *
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *
         */
        public void setHistory(Boolean value) {
            this.history = value;
        }

        /**
         * Gets the value of the anchor property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAnchor() {
            return anchor;
        }

        /**
         * Sets the value of the anchor property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAnchor(String value) {
            this.anchor = value;
        }

        /**
         *
         * 								Hyperlink Target
         *
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Gets the parent object in the object tree representing the unmarshalled xml document.
         *
         * @return
         *     The parent object.
         */
        @Override
        public Object getParent() {
            return this.parent;
        }

        @Override
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
         *
         * @param parent
         *     The parent object in the object tree.
         * @param unmarshaller
         *     The unmarshaller that generated the instance.
         */
        public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
            setParent(_parent);
        }

        final public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
            if(content!=null) {
                IChildNodeCombiner.optimize(this);
            }
        }

        public boolean isEmpty() {
            return tgtFrame==null&&tooltip==null&&docLocation==null&&history==null&&anchor==null&&id==null;
        }
    }
}
