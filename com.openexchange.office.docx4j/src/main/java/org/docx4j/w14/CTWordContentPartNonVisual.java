
package org.docx4j.w14;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.drawing2010.CTNonVisualInkContentPartProperties;


/**
 * <p>Java class for CT_WordContentPartNonVisual complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_WordContentPartNonVisual">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps" minOccurs="0"/>
 *         &lt;element name="cNvContentPartPr" type="{http://schemas.microsoft.com/office/drawing/2010/main}CT_NonVisualInkContentPartProperties" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WordContentPartNonVisual", propOrder = {
    "cNvPr",
    "cNvContentPartPr"
})
public class CTWordContentPartNonVisual {

    protected CTNonVisualDrawingProps cNvPr;
    protected CTNonVisualInkContentPartProperties cNvContentPartPr;

    /**
     * Gets the value of the cNvPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTNonVisualDrawingProps }
     *     
     */
    public CTNonVisualDrawingProps getCNvPr() {
        return cNvPr;
    }

    /**
     * Sets the value of the cNvPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTNonVisualDrawingProps }
     *     
     */
    public void setCNvPr(CTNonVisualDrawingProps value) {
        this.cNvPr = value;
    }

    /**
     * Gets the value of the cNvContentPartPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTNonVisualInkContentPartProperties }
     *     
     */
    public CTNonVisualInkContentPartProperties getCNvContentPartPr() {
        return cNvContentPartPr;
    }

    /**
     * Sets the value of the cNvContentPartPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTNonVisualInkContentPartProperties }
     *     
     */
    public void setCNvContentPartPr(CTNonVisualInkContentPartProperties value) {
        this.cNvContentPartPr = value;
    }
}
