
package org.docx4j.dml;

public interface IAvList {

	public CTGeomGuideList getAvLst();

	public void setAvLst(CTGeomGuideList value);
}
