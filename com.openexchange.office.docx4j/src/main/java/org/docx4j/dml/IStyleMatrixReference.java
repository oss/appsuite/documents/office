
package org.docx4j.dml;

public interface IStyleMatrixReference extends IColorChoice {

    /**
     * Gets the value of the idx property.
     *
     */
    public long getIdx();

    /**
     * Sets the value of the idx property.
     *
     */
    public void setIdx(long value);
}
