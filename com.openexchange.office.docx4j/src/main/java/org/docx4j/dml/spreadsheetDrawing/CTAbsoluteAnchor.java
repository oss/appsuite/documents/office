/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.spreadsheetDrawing;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.jaxb.Context;

/**
 * <p>Java class for CT_AbsoluteAnchor complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_AbsoluteAnchor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pos" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Point2D"/>
 *         &lt;element name="ext" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_PositiveSize2D"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}EG_ObjectChoices"/>
 *         &lt;element name="clientData" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_AnchorClientData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_AbsoluteAnchor", propOrder = {
    "pos",
    "ext",
    "sp",
    "grpSp",
    "graphicFrame",
    "cxnSp",
    "pic",
    "alternateContent",
    "clientData"
})
@XmlRootElement(name = "absoluteAnchor")
public class CTAbsoluteAnchor extends AnchorBase {

    @XmlElement(required = true)
    protected CTPoint2D pos;
    @XmlElement(required = true)
    protected CTPositiveSize2D ext;

    public CTAbsoluteAnchor() {
        //
    }

    public CTAbsoluteAnchor(CTPoint2D pos, CTPositiveSize2D ext) {
        this.pos = pos;
        this.ext = ext;
    }

    public CTAbsoluteAnchor(CTPoint2D pos, CTPositiveSize2D ext, AnchorBase source) {
        this.pos = pos;
        this.ext = ext;

        shallowCopy(source);
    }

    /**
     * Gets the value of the pos property.
     *
     * @return
     *     possible object is
     *     {@link CTPoint2D }
     *
     */
    public CTPoint2D getPos(boolean forceCreate) {
        if(pos==null&&forceCreate) {
            pos = Context.getDmlObjectFactory().createCTPoint2D();
        }
        return pos;
    }

    /**
     * Sets the value of the pos property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPoint2D }
     *
     */
    public void setPos(CTPoint2D value) {
        this.pos = value;
    }

    /**
     * Gets the value of the ext property.
     *
     * @return
     *     possible object is
     *     {@link CTPositiveSize2D }
     *
     */
    public CTPositiveSize2D getExt(boolean forceCreate) {
        if(ext==null&&forceCreate) {
            ext = Context.getDmlObjectFactory().createCTPositiveSize2D();
        }
        return ext;
    }

    /**
     * Sets the value of the ext property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPositiveSize2D }
     *
     */
    public void setExt(CTPositiveSize2D value) {
        this.ext = value;
    }
}
