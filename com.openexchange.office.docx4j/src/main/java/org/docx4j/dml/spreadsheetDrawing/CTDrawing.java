/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.spreadsheetDrawing;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElements;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_Drawing complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Drawing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}EG_Anchor" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Drawing", propOrder = {
    "egAnchor"
})
@XmlRootElement(name = "wsDr")
public class CTDrawing extends DocumentSerialization implements INodeAccessor<AnchorBase> {

    static final public String[] XLSX_Drawing_Namespaces = {
        "r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
        "v", "urn:schemas-microsoft-com:vml",
        "w10", "urn:schemas-microsoft-com:office:word",
        "w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main",
        "sl", "http://schemas.openxmlformats.org/schemaLibrary/2006/main",
        "w15", "http://schemas.microsoft.com/office/word/2012/wordml",
        "w16cid", "http://schemas.microsoft.com/office/word/2016/wordml/cid",
        "w16se", "http://schemas.microsoft.com/office/word/2015/wordml/symex",
        "w14", "http://schemas.microsoft.com/office/word/2010/wordml",
        "wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing",
        "m", "http://schemas.openxmlformats.org/officeDocument/2006/math",
        "o", "urn:schemas-microsoft-com:office:office",
        "xsi", "http://www.w3.org/2001/XMLSchema-instance",
        "dsp", "http://schemas.microsoft.com/office/drawing/2008/diagram",
        "pic", "http://schemas.openxmlformats.org/drawingml/2006/picture",
        "dgm", "http://schemas.openxmlformats.org/drawingml/2006/diagram",
        "xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing",
        "c", "http://schemas.openxmlformats.org/drawingml/2006/chart",
        "a", "http://schemas.openxmlformats.org/drawingml/2006/main",
        "wne", "http://schemas.microsoft.com/office/word/2006/wordml",
        "wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing",
        "wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape",
        "cx1", "http://schemas.microsoft.com/office/drawing/2014/chartex",
        "cx2", "http://schemas.microsoft.com/office/drawing/2015/10/21/chartex",
        "cx4", "http://schemas.microsoft.com/office/drawing/2016/5/10/chartex",
        "wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas",
        "wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup",
        "sle15", "http://schemas.microsoft.com/office/drawing/2012/slicer",
        "a14", "http://schemas.microsoft.com/office/drawing/2010/main",
        "", "http://schemas.openxmlformats.org/drawingml/2006/lockedCanvas",
        "", "http://schemas.openxmlformats.org/drawingml/2006/compatibility",
        "", "http://schemas.openxmlformats.org/officeDocument/2006/bibliography",
        "", "http://schemas.microsoft.com/office/2006/coverPageProps",
        "", "http://schemas.microsoft.com/office/drawing/2012/chartStyle",
        "", "http://schemas.openxmlformats.org/drawingml/2006/chartDrawing",
        "", "urn:schemas-microsoft-com:office:excel",
        "", "urn:schemas-microsoft-com:office:powerpoint"
    };

    public CTDrawing() {
        super(new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing",  "wsDr", "xdr"), XLSX_Drawing_Namespaces, DocumentSerialization.Standard_DOCX_Ignorables);
    }

    @XmlElements({
        @XmlElement(name = "absoluteAnchor", type = CTAbsoluteAnchor.class),
        @XmlElement(name = "twoCellAnchor", type = CTTwoCellAnchor.class),
        @XmlElement(name = "oneCellAnchor", type = CTOneCellAnchor.class)
    })

    protected DLList<AnchorBase> egAnchor;

    /**
     * Gets the value of the egAnchor property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the egAnchor property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEGAnchor().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTAbsoluteAnchor }
     * {@link CTTwoCellAnchor }
     * {@link CTOneCellAnchor }
     *
     *
     */

    @Override
    public DLList<AnchorBase> getContent() {
        if(egAnchor==null) {
            egAnchor = new DLList<AnchorBase>();
        }
        return egAnchor;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                AnchorBase anchorBase = null;
                final Object result = documentPart.getUnmarshaller().unmarshal(reader);
                advanceEvent = reader.getEventType()==XMLStreamReader.END_ELEMENT;
                if(result instanceof JAXBElement) {
                    anchorBase = (AnchorBase)((JAXBElement<?>)result).getValue();
                }
                else if(result instanceof AnchorBase) {
                    anchorBase = (AnchorBase)result;
                }
                if(anchorBase!=null) {
                    getContent().add(anchorBase);
                }
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        for(Object o:getContent()) {
            marshaller.marshal(o, writer);
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }
}
