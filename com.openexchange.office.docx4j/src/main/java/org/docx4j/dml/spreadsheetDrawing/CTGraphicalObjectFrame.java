/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml.spreadsheetDrawing;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualGraphicFrameProperties;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.IGraphicalObjectFrame;
import org.docx4j.dml.ITransform2DAccessor;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.component.Child;

/**
 * <p>Java class for CT_GraphicalObjectFrame complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_GraphicalObjectFrame">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvGraphicFramePr" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_GraphicalObjectFrameNonVisual"/>
 *         &lt;element name="xfrm" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Transform2D"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/drawingml/2006/main}graphic"/>
 *       &lt;/sequence>
 *       &lt;attribute name="macro" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="fPublished" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_GraphicalObjectFrame", propOrder = {
    "nvGraphicFramePr",
    "xfrm",
    "graphic"
})
@XmlRootElement(name="graphicFrame")
public class CTGraphicalObjectFrame implements Child, IContentAccessor<Object>, ITransform2DAccessor, IGraphicalObjectFrame {

    @XmlElement(required = true)
    protected CTGraphicalObjectFrameNonVisual nvGraphicFramePr;
    @XmlElement(required = true)
    protected CTTransform2D xfrm;
    @XmlElement(namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", required = true)
    protected Graphic graphic;
    @XmlAttribute
    protected String macro;
    @XmlAttribute
    protected Boolean fPublished;
    @XmlTransient
    private Object parent;

    @Override
    public DLList<Object> getContent() {
        return null;
    }

    /**
     * Gets the value of the nvGraphicFramePr property.
     * 
     * @return
     *     possible object is
     *     {@link CTGraphicalObjectFrameNonVisual }
     *     
     */
    public CTGraphicalObjectFrameNonVisual getNvGraphicFramePr() {
        return nvGraphicFramePr;
    }

    /**
     * Sets the value of the nvGraphicFramePr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTGraphicalObjectFrameNonVisual }
     *     
     */
    public void setNvGraphicFramePr(CTGraphicalObjectFrameNonVisual value) {
        this.nvGraphicFramePr = value;
    }

    /**
     * Gets the value of the xfrm property.
     * 
     * @return
     *     possible object is
     *     {@link CTTransform2D }
     *     
     */
    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	if(xfrm==null&&forceCreate) {
    		xfrm = new CTTransform2D();
    	}
        return xfrm;
    }

    @Override
    public void removeXfrm() {
        xfrm = null;
    }

    /**
     * Sets the value of the xfrm property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTransform2D }
     *     
     */
    public void setXfrm(CTTransform2D value) {
        this.xfrm = value;
    }

    /**
     * Gets the value of the graphic property.
     * 
     * @return
     *     possible object is
     *     {@link Graphic }
     *     
     */
    @Override
    public Graphic getGraphic() {
        return graphic;
    }

    /**
     * Sets the value of the graphic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Graphic }
     *     
     */
    @Override
    public void setGraphic(Graphic value) {
        this.graphic = value;
    }

    /**
     * Gets the value of the macro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacro() {
        return macro;
    }

    /**
     * Sets the value of the macro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacro(String value) {
        this.macro = value;
    }

    /**
     * Gets the value of the fPublished property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFPublished() {
        if (fPublished == null) {
            return false;
        }
        return fPublished;
    }

    /**
     * Sets the value of the fPublished property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFPublished(Boolean value) {
        this.fPublished = value;
    }
	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvGraphicFramePr==null&&createIfMissing) {
			nvGraphicFramePr = new CTGraphicalObjectFrameNonVisual();
		}
		if(nvGraphicFramePr!=null) {
			if(nvGraphicFramePr.getCNvPr()==null&&createIfMissing) {
				nvGraphicFramePr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvGraphicFramePr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualGraphicFrameProperties getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvGraphicFramePr==null&&createIfMissing) {
			nvGraphicFramePr = new CTGraphicalObjectFrameNonVisual();
		}
		if(nvGraphicFramePr!=null) {
			if(nvGraphicFramePr.getCNvGraphicFramePr()==null&&createIfMissing) {
				nvGraphicFramePr.setCNvGraphicFramePr(new CTNonVisualGraphicFrameProperties());
			}
			return nvGraphicFramePr.getCNvGraphicFramePr();
		}
		return null;
	}

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
