
package org.docx4j.dml.spreadsheetDrawing;

import org.docx4j.mce.AlternateContent;
import com.openexchange.office.filter.core.IContentAccessor;

/**
 * Get the drawing/graphic from a generic anchor.
 */
public interface ICellAnchor extends IContentAccessor<Object> {

    public CTShape getSp();
    public void setSp(CTShape value);

    public CTGroupShape getGrpSp();
    public void setGrpSp(CTGroupShape value);

    public CTGraphicalObjectFrame getGraphicFrame();
    public void setGraphicFrame(CTGraphicalObjectFrame value);

    public CTConnector getCxnSp();
    public void setCxnSp(CTConnector value);

    public CTPicture getPic();
    public void setPic(CTPicture value);

    public CTAnchorClientData getClientData();
    public void setClientData(CTAnchorClientData value);

    public AlternateContent getAlternateContent();

    public void setAlternateContent(AlternateContent alternateContent);

}
