
package org.docx4j.dml.spreadsheetDrawing;

import java.util.List;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.mce.AlternateContent;

@XmlTransient
@XmlAccessorType(XmlAccessType.PROPERTY)
public abstract class AnchorBase implements ICellAnchor {

    @XmlElement(required = true)
    protected CTAnchorClientData clientData;

    protected CTShape sp;
    protected CTGroupShape grpSp;
    protected CTGraphicalObjectFrame graphicFrame;
    protected CTConnector cxnSp;
    protected CTPicture pic;

    @XmlElementRef(name = "alternateContent", namespace = "http://schemas.openxmlformats.org/markup-compatibility/2006", type = AlternateContent.class)
    protected AlternateContent alternateContent;

    @Override
    public List<Object> getContent() {
        if(sp!=null) {
            return sp.getContent();
        }
        else if(grpSp!=null) {
            return grpSp.getContent();
        }
        else if(graphicFrame!=null) {
            return graphicFrame.getContent();
        }
        return null;
    }

    /**
     * Gets the value of the sp property.
     *
     * @return
     *     possible object is
     *     {@link CTShape }
     *
     */
    @Override
    public CTShape getSp() {
        if (alternateContent != null) {
            for (Object alt : alternateContent.getFallback().getAny()) {
                if (alt instanceof CTShape) {
                    return (CTShape) alt;
                }
            }
        }
        return sp;
    }

    /**
     * Sets the value of the sp property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShape }
     *
     */
    @Override
    public void setSp(CTShape sp) {
        alternateContent = null;
        this.sp = sp;
    }

    /**
     * Gets the value of the grpSp property.
     *
     * @return
     *     possible object is
     *     {@link CTGroupShape }
     *
     */
    @Override
    public CTGroupShape getGrpSp() {
        if (alternateContent != null) {
            for (Object alt : alternateContent.getFallback().getAny()) {
                if (alt instanceof CTGroupShape) {
                    return (CTGroupShape) alt;
                }
            }
        }
        return grpSp;
    }

    /**
     * Sets the value of the grpSp property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGroupShape }
     *
     */
    @Override
    public void setGrpSp(CTGroupShape value) {
        alternateContent = null;
        this.grpSp = value;
    }

    /**
     * Gets the value of the graphicFrame property.
     *
     * @return
     *     possible object is
     *     {@link CTGraphicalObjectFrame }
     *
     */
    @Override
    public CTGraphicalObjectFrame getGraphicFrame() {
        if (alternateContent != null) {
            for (Object alt : alternateContent.getFallback().getAny()) {
                if (alt instanceof CTGraphicalObjectFrame) {
                    return (CTGraphicalObjectFrame) alt;
                }
            }
        }
        return graphicFrame;
    }

    /**
     * Sets the value of the graphicFrame property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGraphicalObjectFrame }
     *
     */
    @Override
    public void setGraphicFrame(CTGraphicalObjectFrame value) {
        alternateContent = null;
        this.graphicFrame = value;
    }

    /**
     * Gets the value of the cxnSp property.
     *
     * @return
     *     possible object is
     *     {@link CTConnector }
     *
     */
    @Override
    public CTConnector getCxnSp() {
        if (alternateContent != null) {
            for (Object alt : alternateContent.getFallback().getAny()) {
                if (alt instanceof CTConnector) {
                    return (CTConnector) alt;
                }
            }
        }
        return cxnSp;
    }

    /**
     * Sets the value of the cxnSp property.
     *
     * @param value
     *     allowed object is
     *     {@link CTConnector }
     *
     */
    @Override
    public void setCxnSp(CTConnector value) {
        alternateContent = null;
        this.cxnSp = value;
    }

    /**
     * Gets the value of the pic property.
     *
     * @return
     *     possible object is
     *     {@link CTPicture }
     *
     */
    @Override
    public CTPicture getPic() {
        if (alternateContent != null) {
            for (Object alt : alternateContent.getFallback().getAny()) {
                if (alt instanceof CTPicture) {
                    return (CTPicture) alt;
                }
            }
        }
        return pic;
    }

    /**
     * Sets the value of the pic property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPicture }
     *
     */
    @Override
    public void setPic(CTPicture value) {
        alternateContent = null;
        this.pic = value;
    }

    /**
     * Gets the value of the clientData property.
     *
     * @return
     *     possible object is
     *     {@link CTAnchorClientData }
     *
     */
    @Override
    public CTAnchorClientData getClientData() {
        return clientData;
    }

    /**
     * Sets the value of the clientData property.
     *
     * @param value
     *     allowed object is
     *     {@link CTAnchorClientData }
     *
     */
    @Override
    public void setClientData(CTAnchorClientData value) {
        this.clientData = value;
    }

    /* check if the txBody is valid */
    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
        if(clientData==null) {
            clientData = new CTAnchorClientData();
        }
    }

    @Override
    public AlternateContent getAlternateContent() {
        return alternateContent;
    }

    @Override
    public void setAlternateContent(AlternateContent alternateContent) {
        this.alternateContent = alternateContent;
    }

    protected void shallowCopy(AnchorBase source) {
        this.clientData = source.clientData;
        this.sp = source.sp;
        this.grpSp = source.grpSp;
        this.graphicFrame = source.graphicFrame;
        this.cxnSp = source.cxnSp;
        this.pic = source.pic;
        this.alternateContent = source.alternateContent;
    }
}
