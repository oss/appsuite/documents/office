/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml.spreadsheetDrawing;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualDrawingShapeProps;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTShapeStyle;
import org.docx4j.dml.CTTextBody;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTextListStyle;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.IShape;
import org.docx4j.dml.ITransform2DAccessor;
import org.docx4j.jaxb.Context;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;

/**
 * <p>Java class for CT_Shape complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Shape">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_ShapeNonVisual"/>
 *         &lt;element name="spPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeProperties"/>
 *         &lt;element name="style" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeStyle" minOccurs="0"/>
 *         &lt;element name="txBody" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TextBody" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="macro" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="textlink" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="fLocksText" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *       &lt;attribute name="fPublished" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Shape", propOrder = {
    "nvSpPr",
    "spPr",
    "style",
    "txBody"
})
@XmlRootElement(name="sp")
public class CTShape implements IContentAccessor<Object>, ITransform2DAccessor, IShape {

    @XmlElement(required = true)
    protected CTShapeNonVisual nvSpPr;
    @XmlElement(required = true)
    protected CTShapeProperties spPr;
    protected CTShapeStyle style;
    protected CTTextBody txBody;
    @XmlAttribute
    protected String macro;
    @XmlAttribute
    protected String textlink;
    @XmlAttribute
    protected Boolean fLocksText;
    @XmlAttribute
    protected Boolean fPublished;

    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	return spPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        spPr.removeXfrm();
    }

    @Override
    public DLList<Object> getContent() {
        if (txBody==null) {
            txBody = new CTTextBody();
            txBody.setBodyPr(new CTTextBodyProperties());
            txBody.setLstStyle(new CTTextListStyle());
        }
        return txBody.getContent();
    }

    /**
     * Gets the value of the nvSpPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeNonVisual }
     *     
     */
    public CTShapeNonVisual getNvSpPr() {
        return nvSpPr;
    }

    /**
     * Sets the value of the nvSpPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeNonVisual }
     *     
     */
    public void setNvSpPr(CTShapeNonVisual value) {
        this.nvSpPr = value;
    }

    /**
     * Gets the value of the spPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public CTShapeProperties getSpPr() {
        return spPr;
    }

    /**
     * Sets the value of the spPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *     
     */
    @Override
    public void setSpPr(CTShapeProperties value) {
        this.spPr = value;
    }

    /**
     * Gets the value of the style property.
     * 
     * @return
     *     possible object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public CTShapeStyle getStyle(boolean forceCreate) {
    	if(style==null&&forceCreate) {
    		style = new CTShapeStyle();
    	}
        return style;
    }

    /**
     * Sets the value of the style property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTShapeStyle }
     *     
     */
    @Override
    public void setStyle(CTShapeStyle value) {
        this.style = value;
    }

    /**
     * Gets the value of the txBody property.
     * 
     * @return
     *     possible object is
     *     {@link CTTextBody }
     *     
     */
    public CTTextBody getTxBody(boolean forceCreate) {
        if(txBody==null&&forceCreate) {
            txBody = new CTTextBody();
        }
        return txBody;
    }

    /**
     * Sets the value of the txBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTextBody }
     *     
     */
    public void setTxBody(CTTextBody value) {
        this.txBody = value;
    }

    /**
     * Gets the value of the macro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacro() {
        return macro;
    }

    /**
     * Sets the value of the macro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacro(String value) {
        this.macro = value;
    }

    /**
     * Gets the value of the textlink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextlink() {
        return textlink;
    }

    /**
     * Sets the value of the textlink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextlink(String value) {
        this.textlink = value;
    }

    /**
     * Gets the value of the fLocksText property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFLocksText() {
        if (fLocksText == null) {
            return true;
        } 
        return fLocksText;
    }

    /**
     * Sets the value of the fLocksText property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFLocksText(Boolean value) {
        this.fLocksText = value;
    }

    /**
     * Gets the value of the fPublished property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFPublished() {
        if (fPublished == null) {
            return false;
        }
        return fPublished;
    }

    /**
     * Sets the value of the fPublished property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFPublished(Boolean value) {
        this.fPublished = value;
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvSpPr==null&&createIfMissing) {
			nvSpPr = new CTShapeNonVisual();
		}
		if(nvSpPr!=null) {
			if(nvSpPr.getCNvPr()==null&&createIfMissing) {
				nvSpPr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvSpPr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualDrawingShapeProps getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvSpPr==null&&createIfMissing) {
			nvSpPr = new CTShapeNonVisual();
		}
		if(nvSpPr!=null) {
			if(nvSpPr.getCNvSpPr()==null&&createIfMissing) {
				nvSpPr.setCNvSpPr(new CTNonVisualDrawingShapeProps());
			}
			return nvSpPr.getCNvSpPr();
		}
		return null;
	}

	/* check if the txBody is valid */
    public void beforeMarshal(@SuppressWarnings("unused") Marshaller marshaller) {
    	if(txBody!=null) {
	    	if(txBody.getContent().isEmpty()) {
				txBody = null;
	    	}
	    	else if(txBody.getBodyPr()==null) {
	    		txBody.setBodyPr(Context.getDmlObjectFactory().createCTTextBodyProperties());
	    	}
    	}
    }

    @Override
	public boolean supportsTextBody() {
		return true;
	}

	@Override
	public CTTextBodyProperties getTextBodyProperties(boolean forceCreate) {
		if(txBody!=null && txBody.getBodyPr()!=null) {
			return txBody.getBodyPr();
		}
		else if (forceCreate) {
			if(txBody==null) {
				txBody = Context.getDmlObjectFactory().createCTTextBody();
			}
			final CTTextBodyProperties txBodyPr = Context.getDmlObjectFactory().createCTTextBodyProperties();
			txBody.setBodyPr(txBodyPr);
			return txBodyPr;
		}
		return null;
	}

	@Override
	public void setTextBody(CTTextBodyProperties textBodyProperties) {
		if(txBody==null) {
			txBody = Context.getDmlObjectFactory().createCTTextBody();
		}
		txBody.setBodyPr(textBodyProperties);
 	}


    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }

    public boolean isWordArt() {
        return txBody != null && txBody.isWordArt();
    }
}
