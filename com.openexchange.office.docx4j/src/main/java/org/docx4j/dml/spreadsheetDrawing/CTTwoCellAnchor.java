/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.spreadsheetDrawing;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.jaxb.Context;

/**
 * <p>Java class for CT_TwoCellAnchor complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_TwoCellAnchor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="from" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_Marker"/>
 *         &lt;element name="to" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_Marker"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}EG_ObjectChoices"/>
 *         &lt;element name="clientData" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}CT_AnchorClientData"/>
 *       &lt;/sequence>
 *       &lt;attribute name="editAs" type="{http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing}ST_EditAs" default="twoCell" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TwoCellAnchor", propOrder = {
    "from",
    "to",
    "sp",
    "grpSp",
    "graphicFrame",
    "cxnSp",
    "pic",
    "alternateContent",
    "clientData"
})
@XmlRootElement(name = "twoCellAnchor")
public class CTTwoCellAnchor extends AnchorBase {

    @XmlElement(required = true)
    protected CTMarker from;
    @XmlElement(required = true)
    protected CTMarker to;
    @XmlAttribute
    protected STEditAs           editAs;

    public CTTwoCellAnchor() {
        //
    }

    public CTTwoCellAnchor(CTMarker from, CTMarker to) {
        this.from = from;
        this.to = to;
    }

    public CTTwoCellAnchor(CTMarker from, CTMarker to, AnchorBase source) {
        this.from = from;
        this.to = to;

        shallowCopy(source);
    }

    /**
     * Gets the value of the from property.
     *
     * @return
     *     possible object is
     *     {@link CTMarker }
     *
     */
    public CTMarker getFrom(boolean forceCreate) {
        if(from==null&&forceCreate) {
            from = Context.getDmlSpreadsheetDrawingObjectFactory().createCTMarker();
        }
        return from;
    }

    /**
     * Sets the value of the from property.
     *
     * @param value
     *     allowed object is
     *     {@link CTMarker }
     *
     */
    public void setFrom(CTMarker value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     *
     * @return
     *     possible object is
     *     {@link CTMarker }
     *
     */
    public CTMarker getTo(boolean forceCreate) {
        if(to==null&&forceCreate) {
            to = Context.getDmlSpreadsheetDrawingObjectFactory().createCTMarker();
        }
        return to;
    }

    /**
     * Sets the value of the to property.
     *
     * @param value
     *     allowed object is
     *     {@link CTMarker }
     *
     */
    public void setTo(CTMarker value) {
        this.to = value;
    }

    /**
     * Gets the value of the editAs property.
     *
     * @return
     *     possible object is
     *     {@link STEditAs }
     *
     */
    public STEditAs getEditAs() {
        return (editAs == null) ? STEditAs.TWO_CELL : editAs;
    }

    /**
     * Sets the value of the editAs property.
     *
     * @param value
     *     allowed object is
     *     {@link STEditAs }
     *
     */
    public void setEditAs(STEditAs value) {
        this.editAs = value;
    }
}
