
package org.docx4j.dml;

public interface ILineFillProperties {

    /**
     * Gets the value of the noFill property.
     *
     * @return
     *     possible object is
     *     {@link CTNoFillProperties }
     *
     */
    public CTNoFillProperties getNoFill();

    /**
     * Sets the value of the noFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTNoFillProperties }
     *
     */
    public void setNoFill(CTNoFillProperties value);

    /**
     * Gets the value of the solidFill property.
     *
     * @return
     *     possible object is
     *     {@link CTSolidColorFillProperties }
     *
     */
    public CTSolidColorFillProperties getSolidFill();

    /**
     * Sets the value of the solidFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSolidColorFillProperties }
     *
     */
    public void setSolidFill(CTSolidColorFillProperties value);

    /**
     * Gets the value of the gradFill property.
     *
     * @return
     *     possible object is
     *     {@link CTGradientFillProperties }
     *
     */
    public CTGradientFillProperties getGradFill();

    /**
     * Sets the value of the gradFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGradientFillProperties }
     *
     */
    public void setGradFill(CTGradientFillProperties value);

    /**
     * Gets the value of the pattFill property.
     *
     * @return
     *     possible object is
     *     {@link CTPatternFillProperties }
     *
     */
    public CTPatternFillProperties getPattFill();

    /**
     * Sets the value of the pattFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPatternFillProperties }
     *
     */
    public void setPattFill(CTPatternFillProperties value);
}
