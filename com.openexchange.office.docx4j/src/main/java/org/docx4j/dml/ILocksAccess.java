/**
 *
 * @author sven.jacobiATopen-xchange.com
 */
package org.docx4j.dml;

public interface ILocksAccess {

    public CTLockingBase getLocks(boolean createIfMissing);
}
