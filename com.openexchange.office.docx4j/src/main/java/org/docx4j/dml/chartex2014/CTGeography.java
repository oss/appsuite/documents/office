
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for CT_Geography complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Geography">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="geoCache" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}CT_GeoCache" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="projectionType" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_GeoProjectionType" />
 *       &lt;attribute name="viewedRegionType" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_GeoMappingLevel" />
 *       &lt;attribute name="cultureLanguage" use="required" type="{http://www.w3.org/2001/XMLSchema}language" />
 *       &lt;attribute name="cultureRegion" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="attribution" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Geography", propOrder = {
    "geoCache"
})
public class CTGeography {

    protected CTGeoCache geoCache;
    @XmlAttribute(name = "projectionType")
    protected STGeoProjectionType projectionType;
    @XmlAttribute(name = "viewedRegionType")
    protected STGeoMappingLevel viewedRegionType;
    @XmlAttribute(name = "cultureLanguage", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String cultureLanguage;
    @XmlAttribute(name = "cultureRegion", required = true)
    protected String cultureRegion;
    @XmlAttribute(name = "attribution", required = true)
    protected String attribution;

    /**
     * Gets the value of the geoCache property.
     * 
     * @return
     *     possible object is
     *     {@link CTGeoCache }
     *     
     */
    public CTGeoCache getGeoCache() {
        return geoCache;
    }

    /**
     * Sets the value of the geoCache property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTGeoCache }
     *     
     */
    public void setGeoCache(CTGeoCache value) {
        this.geoCache = value;
    }

    /**
     * Gets the value of the projectionType property.
     * 
     * @return
     *     possible object is
     *     {@link STGeoProjectionType }
     *     
     */
    public STGeoProjectionType getProjectionType() {
        return projectionType;
    }

    /**
     * Sets the value of the projectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STGeoProjectionType }
     *     
     */
    public void setProjectionType(STGeoProjectionType value) {
        this.projectionType = value;
    }

    /**
     * Gets the value of the viewedRegionType property.
     * 
     * @return
     *     possible object is
     *     {@link STGeoMappingLevel }
     *     
     */
    public STGeoMappingLevel getViewedRegionType() {
        return viewedRegionType;
    }

    /**
     * Sets the value of the viewedRegionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link STGeoMappingLevel }
     *     
     */
    public void setViewedRegionType(STGeoMappingLevel value) {
        this.viewedRegionType = value;
    }

    /**
     * Gets the value of the cultureLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCultureLanguage() {
        return cultureLanguage;
    }

    /**
     * Sets the value of the cultureLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCultureLanguage(String value) {
        this.cultureLanguage = value;
    }

    /**
     * Gets the value of the cultureRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCultureRegion() {
        return cultureRegion;
    }

    /**
     * Sets the value of the cultureRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCultureRegion(String value) {
        this.cultureRegion = value;
    }

    /**
     * Gets the value of the attribution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribution() {
        return attribution;
    }

    /**
     * Sets the value of the attribution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribution(String value) {
        this.attribution = value;
    }
}
