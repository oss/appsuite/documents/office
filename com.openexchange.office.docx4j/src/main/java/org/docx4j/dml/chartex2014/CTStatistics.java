
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_Statistics complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Statistics">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="quartileMethod" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_QuartileMethod" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Statistics")
public class CTStatistics {

    @XmlAttribute(name = "quartileMethod")
    protected STQuartileMethod quartileMethod;

    /**
     * Gets the value of the quartileMethod property.
     * 
     * @return
     *     possible object is
     *     {@link STQuartileMethod }
     *     
     */
    public STQuartileMethod getQuartileMethod() {
        return quartileMethod;
    }

    /**
     * Sets the value of the quartileMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link STQuartileMethod }
     *     
     */
    public void setQuartileMethod(STQuartileMethod value) {
        this.quartileMethod = value;
    }
}
