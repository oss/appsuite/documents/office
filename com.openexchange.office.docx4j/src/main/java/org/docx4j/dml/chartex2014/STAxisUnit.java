
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_AxisUnit.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_AxisUnit">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="hundreds"/>
 *     &lt;enumeration value="thousands"/>
 *     &lt;enumeration value="tenThousands"/>
 *     &lt;enumeration value="hundredThousands"/>
 *     &lt;enumeration value="millions"/>
 *     &lt;enumeration value="tenMillions"/>
 *     &lt;enumeration value="hundredMillions"/>
 *     &lt;enumeration value="billions"/>
 *     &lt;enumeration value="trillions"/>
 *     &lt;enumeration value="percentage"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_AxisUnit")
@XmlEnum
public enum STAxisUnit {

    @XmlEnumValue("hundreds")
    HUNDREDS("hundreds"),
    @XmlEnumValue("thousands")
    THOUSANDS("thousands"),
    @XmlEnumValue("tenThousands")
    TEN_THOUSANDS("tenThousands"),
    @XmlEnumValue("hundredThousands")
    HUNDRED_THOUSANDS("hundredThousands"),
    @XmlEnumValue("millions")
    MILLIONS("millions"),
    @XmlEnumValue("tenMillions")
    TEN_MILLIONS("tenMillions"),
    @XmlEnumValue("hundredMillions")
    HUNDRED_MILLIONS("hundredMillions"),
    @XmlEnumValue("billions")
    BILLIONS("billions"),
    @XmlEnumValue("trillions")
    TRILLIONS("trillions"),
    @XmlEnumValue("percentage")
    PERCENTAGE("percentage");
    private final String value;

    STAxisUnit(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STAxisUnit fromValue(String v) {
        for (STAxisUnit c: STAxisUnit.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
