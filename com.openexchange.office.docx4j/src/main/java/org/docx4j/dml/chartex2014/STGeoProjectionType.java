
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_GeoProjectionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_GeoProjectionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="mercator"/>
 *     &lt;enumeration value="miller"/>
 *     &lt;enumeration value="robinson"/>
 *     &lt;enumeration value="albers"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_GeoProjectionType")
@XmlEnum
public enum STGeoProjectionType {

    @XmlEnumValue("mercator")
    MERCATOR("mercator"),
    @XmlEnumValue("miller")
    MILLER("miller"),
    @XmlEnumValue("robinson")
    ROBINSON("robinson"),
    @XmlEnumValue("albers")
    ALBERS("albers");
    private final String value;

    STGeoProjectionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STGeoProjectionType fromValue(String v) {
        for (STGeoProjectionType c: STGeoProjectionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
