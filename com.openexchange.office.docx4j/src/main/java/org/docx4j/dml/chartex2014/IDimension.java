
package org.docx4j.dml.chartex2014;

import java.util.List;
import jakarta.xml.bind.JAXBElement;

public interface IDimension {

    public List<JAXBElement<?>> getContent();

    public String getDimensionType();
}
