
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SeriesLayout.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SeriesLayout">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="boxWhisker"/>
 *     &lt;enumeration value="clusteredColumn"/>
 *     &lt;enumeration value="funnel"/>
 *     &lt;enumeration value="paretoLine"/>
 *     &lt;enumeration value="regionMap"/>
 *     &lt;enumeration value="sunburst"/>
 *     &lt;enumeration value="treemap"/>
 *     &lt;enumeration value="waterfall"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SeriesLayout")
@XmlEnum
public enum STSeriesLayout {

    @XmlEnumValue("boxWhisker")
    BOX_WHISKER("boxWhisker"),
    @XmlEnumValue("clusteredColumn")
    CLUSTERED_COLUMN("clusteredColumn"),
    @XmlEnumValue("funnel")
    FUNNEL("funnel"),
    @XmlEnumValue("paretoLine")
    PARETO_LINE("paretoLine"),
    @XmlEnumValue("regionMap")
    REGION_MAP("regionMap"),
    @XmlEnumValue("sunburst")
    SUNBURST("sunburst"),
    @XmlEnumValue("treemap")
    TREEMAP("treemap"),
    @XmlEnumValue("waterfall")
    WATERFALL("waterfall");
    private final String value;

    STSeriesLayout(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSeriesLayout fromValue(String v) {
        for (STSeriesLayout c: STSeriesLayout.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
