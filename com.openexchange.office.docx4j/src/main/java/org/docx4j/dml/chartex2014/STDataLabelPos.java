
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_DataLabelPos.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_DataLabelPos">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="bestFit"/>
 *     &lt;enumeration value="b"/>
 *     &lt;enumeration value="ctr"/>
 *     &lt;enumeration value="inBase"/>
 *     &lt;enumeration value="inEnd"/>
 *     &lt;enumeration value="l"/>
 *     &lt;enumeration value="outEnd"/>
 *     &lt;enumeration value="r"/>
 *     &lt;enumeration value="t"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_DataLabelPos")
@XmlEnum
public enum STDataLabelPos {

    @XmlEnumValue("bestFit")
    BEST_FIT("bestFit"),
    @XmlEnumValue("b")
    B("b"),
    @XmlEnumValue("ctr")
    CTR("ctr"),
    @XmlEnumValue("inBase")
    IN_BASE("inBase"),
    @XmlEnumValue("inEnd")
    IN_END("inEnd"),
    @XmlEnumValue("l")
    L("l"),
    @XmlEnumValue("outEnd")
    OUT_END("outEnd"),
    @XmlEnumValue("r")
    R("r"),
    @XmlEnumValue("t")
    T("t");
    private final String value;

    STDataLabelPos(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STDataLabelPos fromValue(String v) {
        for (STDataLabelPos c: STDataLabelPos.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
