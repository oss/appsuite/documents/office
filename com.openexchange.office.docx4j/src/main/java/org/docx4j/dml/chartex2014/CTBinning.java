
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_Binning complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Binning">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="binSize" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="binCount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *       &lt;/choice>
 *       &lt;attribute name="intervalClosed" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_IntervalClosedSide" />
 *       &lt;attribute name="underflow" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_DoubleOrAutomatic" />
 *       &lt;attribute name="overflow" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_DoubleOrAutomatic" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Binning", propOrder = {
    "binSize",
    "binCount"
})
public class CTBinning {

    protected Double binSize;
    @XmlSchemaType(name = "unsignedInt")
    protected Long binCount;
    @XmlAttribute(name = "intervalClosed")
    protected STIntervalClosedSide intervalClosed;
    @XmlAttribute(name = "underflow")
    protected String underflow;
    @XmlAttribute(name = "overflow")
    protected String overflow;

    /**
     * Gets the value of the binSize property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBinSize() {
        return binSize;
    }

    /**
     * Sets the value of the binSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBinSize(Double value) {
        this.binSize = value;
    }

    /**
     * Gets the value of the binCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBinCount() {
        return binCount;
    }

    /**
     * Sets the value of the binCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBinCount(Long value) {
        this.binCount = value;
    }

    /**
     * Gets the value of the intervalClosed property.
     * 
     * @return
     *     possible object is
     *     {@link STIntervalClosedSide }
     *     
     */
    public STIntervalClosedSide getIntervalClosed() {
        return intervalClosed;
    }

    /**
     * Sets the value of the intervalClosed property.
     * 
     * @param value
     *     allowed object is
     *     {@link STIntervalClosedSide }
     *     
     */
    public void setIntervalClosed(STIntervalClosedSide value) {
        this.intervalClosed = value;
    }

    /**
     * Gets the value of the underflow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnderflow() {
        return underflow;
    }

    /**
     * Sets the value of the underflow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnderflow(String value) {
        this.underflow = value;
    }

    /**
     * Gets the value of the overflow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverflow() {
        return overflow;
    }

    /**
     * Sets the value of the overflow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverflow(String value) {
        this.overflow = value;
    }
}
