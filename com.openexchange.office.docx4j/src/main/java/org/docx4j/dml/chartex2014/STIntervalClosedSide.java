
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_IntervalClosedSide.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_IntervalClosedSide">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="l"/>
 *     &lt;enumeration value="r"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_IntervalClosedSide")
@XmlEnum
public enum STIntervalClosedSide {

    @XmlEnumValue("l")
    L("l"),
    @XmlEnumValue("r")
    R("r");
    private final String value;

    STIntervalClosedSide(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STIntervalClosedSide fromValue(String v) {
        for (STIntervalClosedSide c: STIntervalClosedSide.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
