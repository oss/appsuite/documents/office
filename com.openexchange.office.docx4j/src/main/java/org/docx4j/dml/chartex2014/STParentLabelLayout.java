
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_ParentLabelLayout.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_ParentLabelLayout">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="none"/>
 *     &lt;enumeration value="banner"/>
 *     &lt;enumeration value="overlapping"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_ParentLabelLayout")
@XmlEnum
public enum STParentLabelLayout {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("banner")
    BANNER("banner"),
    @XmlEnumValue("overlapping")
    OVERLAPPING("overlapping");
    private final String value;

    STParentLabelLayout(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STParentLabelLayout fromValue(String v) {
        for (STParentLabelLayout c: STParentLabelLayout.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
