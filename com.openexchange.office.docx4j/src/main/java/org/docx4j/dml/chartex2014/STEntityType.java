
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_EntityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_EntityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Address"/>
 *     &lt;enumeration value="AdminDistrict"/>
 *     &lt;enumeration value="AdminDistrict2"/>
 *     &lt;enumeration value="AdminDistrict3"/>
 *     &lt;enumeration value="Continent"/>
 *     &lt;enumeration value="CountryRegion"/>
 *     &lt;enumeration value="Locality"/>
 *     &lt;enumeration value="Ocean"/>
 *     &lt;enumeration value="Planet"/>
 *     &lt;enumeration value="PostalCode"/>
 *     &lt;enumeration value="Region"/>
 *     &lt;enumeration value="Unsupported"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_EntityType")
@XmlEnum
public enum STEntityType {

    @XmlEnumValue("Address")
    ADDRESS("Address"),
    @XmlEnumValue("AdminDistrict")
    ADMIN_DISTRICT("AdminDistrict"),
    @XmlEnumValue("AdminDistrict2")
    ADMIN_DISTRICT_2("AdminDistrict2"),
    @XmlEnumValue("AdminDistrict3")
    ADMIN_DISTRICT_3("AdminDistrict3"),
    @XmlEnumValue("Continent")
    CONTINENT("Continent"),
    @XmlEnumValue("CountryRegion")
    COUNTRY_REGION("CountryRegion"),
    @XmlEnumValue("Locality")
    LOCALITY("Locality"),
    @XmlEnumValue("Ocean")
    OCEAN("Ocean"),
    @XmlEnumValue("Planet")
    PLANET("Planet"),
    @XmlEnumValue("PostalCode")
    POSTAL_CODE("PostalCode"),
    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("Unsupported")
    UNSUPPORTED("Unsupported");
    private final String value;

    STEntityType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STEntityType fromValue(String v) {
        for (STEntityType c: STEntityType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
