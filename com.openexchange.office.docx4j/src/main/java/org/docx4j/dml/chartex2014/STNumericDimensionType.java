
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_NumericDimensionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_NumericDimensionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="val"/>
 *     &lt;enumeration value="x"/>
 *     &lt;enumeration value="y"/>
 *     &lt;enumeration value="size"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_NumericDimensionType")
@XmlEnum
public enum STNumericDimensionType {

    @XmlEnumValue("val")
    VAL("val"),
    @XmlEnumValue("x")
    X("x"),
    @XmlEnumValue("y")
    Y("y"),
    @XmlEnumValue("size")
    SIZE("size");
    private final String value;

    STNumericDimensionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STNumericDimensionType fromValue(String v) {
        for (STNumericDimensionType c: STNumericDimensionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
