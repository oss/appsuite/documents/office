
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_ValueColorPositions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ValueColorPositions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="min" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}CT_ValueColorEndPosition" minOccurs="0"/>
 *         &lt;element name="mid" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}CT_ValueColorMiddlePosition" minOccurs="0"/>
 *         &lt;element name="max" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}CT_ValueColorEndPosition" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="count" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_ValueColorPositionCount" default="2" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ValueColorPositions", propOrder = {
    "min",
    "mid",
    "max"
})
public class CTValueColorPositions {

    protected CTValueColorEndPosition min;
    protected CTValueColorMiddlePosition mid;
    protected CTValueColorEndPosition max;
    @XmlAttribute(name = "count")
    protected Integer count;

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link CTValueColorEndPosition }
     *     
     */
    public CTValueColorEndPosition getMin() {
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTValueColorEndPosition }
     *     
     */
    public void setMin(CTValueColorEndPosition value) {
        this.min = value;
    }

    /**
     * Gets the value of the mid property.
     * 
     * @return
     *     possible object is
     *     {@link CTValueColorMiddlePosition }
     *     
     */
    public CTValueColorMiddlePosition getMid() {
        return mid;
    }

    /**
     * Sets the value of the mid property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTValueColorMiddlePosition }
     *     
     */
    public void setMid(CTValueColorMiddlePosition value) {
        this.mid = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link CTValueColorEndPosition }
     *     
     */
    public CTValueColorEndPosition getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTValueColorEndPosition }
     *     
     */
    public void setMax(CTValueColorEndPosition value) {
        this.max = value;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getCount() {
        if (count == null) {
            return  2;
        }
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCount(Integer value) {
        this.count = value;
    }
}
