
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_RegionLabelLayout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_RegionLabelLayout">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="val" use="required" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}ST_RegionLabelLayout" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_RegionLabelLayout")
public class CTRegionLabelLayout {

    @XmlAttribute(name = "val", required = true)
    protected STRegionLabelLayout val;

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link STRegionLabelLayout }
     *     
     */
    public STRegionLabelLayout getVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link STRegionLabelLayout }
     *     
     */
    public void setVal(STRegionLabelLayout value) {
        this.val = value;
    }
}
