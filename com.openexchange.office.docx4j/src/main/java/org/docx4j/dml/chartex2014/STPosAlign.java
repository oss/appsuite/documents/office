
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_PosAlign.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_PosAlign">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="min"/>
 *     &lt;enumeration value="ctr"/>
 *     &lt;enumeration value="max"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_PosAlign")
@XmlEnum
public enum STPosAlign {

    @XmlEnumValue("min")
    MIN("min"),
    @XmlEnumValue("ctr")
    CTR("ctr"),
    @XmlEnumValue("max")
    MAX("max");
    private final String value;

    STPosAlign(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STPosAlign fromValue(String v) {
        for (STPosAlign c: STPosAlign.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
