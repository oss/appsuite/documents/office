
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_RegionLabelLayout.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_RegionLabelLayout">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="none"/>
 *     &lt;enumeration value="bestFitOnly"/>
 *     &lt;enumeration value="showAll"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_RegionLabelLayout")
@XmlEnum
public enum STRegionLabelLayout {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("bestFitOnly")
    BEST_FIT_ONLY("bestFitOnly"),
    @XmlEnumValue("showAll")
    SHOW_ALL("showAll");
    private final String value;

    STRegionLabelLayout(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STRegionLabelLayout fromValue(String v) {
        for (STRegionLabelLayout c: STRegionLabelLayout.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
