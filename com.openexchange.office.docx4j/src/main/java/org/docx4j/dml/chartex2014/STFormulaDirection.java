
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_FormulaDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_FormulaDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="col"/>
 *     &lt;enumeration value="row"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_FormulaDirection")
@XmlEnum
public enum STFormulaDirection {

    @XmlEnumValue("col")
    COL("col"),
    @XmlEnumValue("row")
    ROW("row");
    private final String value;

    STFormulaDirection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STFormulaDirection fromValue(String v) {
        for (STFormulaDirection c: STFormulaDirection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
