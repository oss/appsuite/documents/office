
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_PageOrientation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_PageOrientation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="default"/>
 *     &lt;enumeration value="portrait"/>
 *     &lt;enumeration value="landscape"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_PageOrientation")
@XmlEnum
public enum STPageOrientation {

    @XmlEnumValue("default")
    DEFAULT("default"),
    @XmlEnumValue("portrait")
    PORTRAIT("portrait"),
    @XmlEnumValue("landscape")
    LANDSCAPE("landscape");
    private final String value;

    STPageOrientation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STPageOrientation fromValue(String v) {
        for (STPageOrientation c: STPageOrientation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
