
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_SidePos.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_SidePos">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="l"/>
 *     &lt;enumeration value="t"/>
 *     &lt;enumeration value="r"/>
 *     &lt;enumeration value="b"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_SidePos")
@XmlEnum
public enum STSidePos {

    @XmlEnumValue("l")
    L("l"),
    @XmlEnumValue("t")
    T("t"),
    @XmlEnumValue("r")
    R("r"),
    @XmlEnumValue("b")
    B("b");
    private final String value;

    STSidePos(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSidePos fromValue(String v) {
        for (STSidePos c: STSidePos.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
