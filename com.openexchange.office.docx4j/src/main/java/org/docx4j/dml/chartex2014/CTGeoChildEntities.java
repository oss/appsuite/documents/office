
package org.docx4j.dml.chartex2014;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_GeoChildEntities complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_GeoChildEntities">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="geoHierarchyEntity" type="{http://schemas.microsoft.com/office/drawing/2014/chartex}CT_GeoHierarchyEntity" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_GeoChildEntities", propOrder = {
    "geoHierarchyEntity"
})
public class CTGeoChildEntities {

    protected List<CTGeoHierarchyEntity> geoHierarchyEntity;

    /**
     * Gets the value of the geoHierarchyEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geoHierarchyEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeoHierarchyEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTGeoHierarchyEntity }
     * 
     * 
     */
    public List<CTGeoHierarchyEntity> getGeoHierarchyEntity() {
        if (geoHierarchyEntity == null) {
            geoHierarchyEntity = new ArrayList<CTGeoHierarchyEntity>();
        }
        return this.geoHierarchyEntity;
    }
}
