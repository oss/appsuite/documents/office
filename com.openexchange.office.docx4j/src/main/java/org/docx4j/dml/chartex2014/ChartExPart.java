
package org.docx4j.dml.chartex2014;

import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

public class ChartExPart extends JaxbXmlPart<CTChartSpace> {

    public ChartExPart(PartName partName) {
        super(partName);
        init();
    }

    public void init() {
        // Used if this Part is added to [Content_Types].xml
        setContentType(new org.docx4j.openpackaging.contenttype.ContentType(ContentTypes.DRAWINGML_CHART_EX));

        // Used when this Part is added to a rels
        setRelationshipType(Namespaces.SPREADSHEETML_CHART_EX);
    }
}
