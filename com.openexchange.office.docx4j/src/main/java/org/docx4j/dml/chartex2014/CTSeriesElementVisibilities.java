
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_SeriesElementVisibilities complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_SeriesElementVisibilities">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="connectorLines" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="meanLine" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="meanMarker" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="nonoutliers" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="outliers" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SeriesElementVisibilities")
public class CTSeriesElementVisibilities {

    @XmlAttribute(name = "connectorLines")
    protected Boolean connectorLines;
    @XmlAttribute(name = "meanLine")
    protected Boolean meanLine;
    @XmlAttribute(name = "meanMarker")
    protected Boolean meanMarker;
    @XmlAttribute(name = "nonoutliers")
    protected Boolean nonoutliers;
    @XmlAttribute(name = "outliers")
    protected Boolean outliers;

    /**
     * Gets the value of the connectorLines property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConnectorLines() {
        return connectorLines;
    }

    /**
     * Sets the value of the connectorLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConnectorLines(Boolean value) {
        this.connectorLines = value;
    }

    /**
     * Gets the value of the meanLine property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeanLine() {
        return meanLine;
    }

    /**
     * Sets the value of the meanLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeanLine(Boolean value) {
        this.meanLine = value;
    }

    /**
     * Gets the value of the meanMarker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeanMarker() {
        return meanMarker;
    }

    /**
     * Sets the value of the meanMarker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeanMarker(Boolean value) {
        this.meanMarker = value;
    }

    /**
     * Gets the value of the nonoutliers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonoutliers() {
        return nonoutliers;
    }

    /**
     * Sets the value of the nonoutliers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonoutliers(Boolean value) {
        this.nonoutliers = value;
    }

    /**
     * Gets the value of the outliers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutliers() {
        return outliers;
    }

    /**
     * Sets the value of the outliers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutliers(Boolean value) {
        this.outliers = value;
    }
}
