
package org.docx4j.dml.chartex2014;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_GeoMappingLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_GeoMappingLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="dataOnly"/>
 *     &lt;enumeration value="postalCode"/>
 *     &lt;enumeration value="county"/>
 *     &lt;enumeration value="state"/>
 *     &lt;enumeration value="countryRegion"/>
 *     &lt;enumeration value="countryRegionList"/>
 *     &lt;enumeration value="world"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_GeoMappingLevel")
@XmlEnum
public enum STGeoMappingLevel {

    @XmlEnumValue("dataOnly")
    DATA_ONLY("dataOnly"),
    @XmlEnumValue("postalCode")
    POSTAL_CODE("postalCode"),
    @XmlEnumValue("county")
    COUNTY("county"),
    @XmlEnumValue("state")
    STATE("state"),
    @XmlEnumValue("countryRegion")
    COUNTRY_REGION("countryRegion"),
    @XmlEnumValue("countryRegionList")
    COUNTRY_REGION_LIST("countryRegionList"),
    @XmlEnumValue("world")
    WORLD("world");
    private final String value;

    STGeoMappingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STGeoMappingLevel fromValue(String v) {
        for (STGeoMappingLevel c: STGeoMappingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
