
package org.docx4j.dml;

public interface ITextCharacterProperties {

	public CTTextCharacterProperties getRPr(boolean forceCreate);

	public void setRPr(CTTextCharacterProperties textCharacterProperties);
}
