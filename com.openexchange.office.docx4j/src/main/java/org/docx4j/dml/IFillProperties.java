
package org.docx4j.dml;

import com.openexchange.office.filter.core.component.Child;

public interface IFillProperties extends Child, ILineFillProperties {

    /**
     * Gets the value of the blipFill property.
     *
     * @return
     *     possible object is
     *     {@link CTBlipFillProperties }
     *
     */
    public CTBlipFillProperties getBlipFill();

    /**
     * Sets the value of the blipFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBlipFillProperties }
     *
     */
    public void setBlipFill(CTBlipFillProperties value);

    /**
     * Gets the value of the grpFill property.
     *
     * @return
     *     possible object is
     *     {@link CTGroupFillProperties }
     *
     */
    public CTGroupFillProperties getGrpFill();

    /**
     * Sets the value of the grpFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGroupFillProperties }
     *
     */
    public void setGrpFill(CTGroupFillProperties value);
}
