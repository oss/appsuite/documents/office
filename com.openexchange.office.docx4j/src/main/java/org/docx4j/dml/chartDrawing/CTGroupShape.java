/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml.chartDrawing;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElements;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTGroupShapeProperties;
import org.docx4j.dml.CTGroupTransform2D;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualGroupDrawingShapeProps;
import org.docx4j.dml.IGroupShape;
import org.docx4j.dml.ITransform2DAccessor;

/**
 * <p>Java class for CT_GroupShape complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_GroupShape">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvGrpSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_GroupShapeNonVisual"/>
 *         &lt;element name="grpSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_GroupShapeProperties"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="sp" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_Shape"/>
 *           &lt;element name="grpSp" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_GroupShape"/>
 *           &lt;element name="graphicFrame" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_GraphicFrame"/>
 *           &lt;element name="cxnSp" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_Connector"/>
 *           &lt;element name="pic" type="{http://schemas.openxmlformats.org/drawingml/2006/chartDrawing}CT_Picture"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_GroupShape", propOrder = {
    "nvGrpSpPr",
    "grpSpPr",
    "spOrGrpSpOrGraphicFrame"
})
public class CTGroupShape implements ITransform2DAccessor, IGroupShape {

    @XmlElement(required = true)
    protected CTGroupShapeNonVisual nvGrpSpPr;
    @XmlElement(required = true)
    protected CTGroupShapeProperties grpSpPr;
    @XmlElements({
        @XmlElement(name = "pic", type = CTPicture.class),
        @XmlElement(name = "cxnSp", type = CTConnector.class),
        @XmlElement(name = "grpSp", type = CTGroupShape.class),
        @XmlElement(name = "sp", type = CTShape.class),
        @XmlElement(name = "graphicFrame", type = CTGraphicFrame.class)
    })
    protected List<Object> spOrGrpSpOrGraphicFrame;

    @Override
    public CTGroupTransform2D getXfrm(boolean forceCreate) {
    	return grpSpPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        grpSpPr.removeXfrm();
    }

    /**
     * Gets the value of the nvGrpSpPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTGroupShapeNonVisual }
     *     
     */
    public CTGroupShapeNonVisual getNvGrpSpPr() {
        return nvGrpSpPr;
    }

    /**
     * Sets the value of the nvGrpSpPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTGroupShapeNonVisual }
     *     
     */
    public void setNvGrpSpPr(CTGroupShapeNonVisual value) {
        this.nvGrpSpPr = value;
    }

    /**
     * Gets the value of the grpSpPr property.
     * 
     * @return
     *     possible object is
     *     {@link CTGroupShapeProperties }
     *     
     */
    @Override
    public CTGroupShapeProperties getGrpSpPr() {
        return grpSpPr;
    }

    /**
     * Sets the value of the grpSpPr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTGroupShapeProperties }
     *     
     */
    @Override
    public void setGrpSpPr(CTGroupShapeProperties value) {
        this.grpSpPr = value;
    }

    /**
     * Gets the value of the spOrGrpSpOrGraphicFrame property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spOrGrpSpOrGraphicFrame property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpOrGrpSpOrGraphicFrame().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTPicture }
     * {@link CTConnector }
     * {@link CTGroupShape }
     * {@link CTShape }
     * {@link CTGraphicFrame }
     * 
     * 
     */
    public List<Object> getSpOrGrpSpOrGraphicFrame() {
        if (spOrGrpSpOrGraphicFrame == null) {
            spOrGrpSpOrGraphicFrame = new ArrayList<Object>();
        }
        return this.spOrGrpSpOrGraphicFrame;
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvGrpSpPr==null&&createIfMissing) {
			nvGrpSpPr = new CTGroupShapeNonVisual();
		}
		if(nvGrpSpPr!=null) {
			if(nvGrpSpPr.getCNvPr()==null&&createIfMissing) {
				nvGrpSpPr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvGrpSpPr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualGroupDrawingShapeProps getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvGrpSpPr==null&&createIfMissing) {
			nvGrpSpPr = new CTGroupShapeNonVisual();
		}
		if(nvGrpSpPr!=null) {
			if(nvGrpSpPr.getCNvGrpSpPr()==null&&createIfMissing) {
				nvGrpSpPr.setCNvGrpSpPr(new CTNonVisualGroupDrawingShapeProps());
			}
			return nvGrpSpPr.getCNvGrpSpPr();
		}
		return null;
	}

    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
