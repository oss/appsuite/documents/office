/**
 *
 * @author sven.jacobiATopen-xchange.com
 */
package org.docx4j.dml;

public interface IConnectionAccessor {

    public CTConnection getStartConnection(boolean forceCreate);

    public CTConnection getEndConnection(boolean forceCreate);

    public void setStartConnection(CTConnection connection);

    public void setEndConnection(CTConnection connection);
}
