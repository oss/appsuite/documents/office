
package org.docx4j.dml;

public interface ITransform2D {

    public CTPoint2D getOff(boolean forceCreate);

    public void setOff(CTPoint2D value);

    public CTPositiveSize2D getExt(boolean forceCreate);

    public void setExt(CTPositiveSize2D value);

    public int getRot();

    public void setRot(Integer value);

    public boolean isFlipH();

    public void setFlipH(Boolean value);

    public boolean isFlipV();

    public void setFlipV(Boolean value);
}
