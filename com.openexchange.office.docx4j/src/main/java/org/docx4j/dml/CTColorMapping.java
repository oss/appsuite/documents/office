/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_ColorMapping complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ColorMapping">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="bg1" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="tx1" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="bg2" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="tx2" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent1" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent2" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent3" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent4" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent5" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="accent6" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="hlink" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *       &lt;attribute name="folHlink" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_ColorSchemeIndex" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ColorMapping", propOrder = {
    "extLst"
})
public class CTColorMapping {

    protected CTOfficeArtExtensionList extLst;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex bg1;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex tx1;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex bg2;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex tx2;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent1;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent2;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent3;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent4;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent5;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex accent6;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex hlink;
    @XmlAttribute(required = true)
    protected STColorSchemeIndex folHlink;

    public CTColorMapping() {
        bg1 = STColorSchemeIndex.LT_1;
        tx1 = STColorSchemeIndex.DK_1;
        bg2 = STColorSchemeIndex.LT_2;
        tx2 = STColorSchemeIndex.DK_2;
        accent1 = STColorSchemeIndex.ACCENT_1;
        accent2 = STColorSchemeIndex.ACCENT_2;
        accent3 = STColorSchemeIndex.ACCENT_3;
        accent4 = STColorSchemeIndex.ACCENT_4;
        accent5 = STColorSchemeIndex.ACCENT_5;
        accent6 = STColorSchemeIndex.ACCENT_6;
        hlink = STColorSchemeIndex.HLINK;
        folHlink = STColorSchemeIndex.FOL_HLINK;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the bg1 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getBg1() {
        return bg1;
    }

    /**
     * Sets the value of the bg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setBg1(STColorSchemeIndex value) {
        this.bg1 = value;
    }

    /**
     * Gets the value of the tx1 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getTx1() {
        return tx1;
    }

    /**
     * Sets the value of the tx1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setTx1(STColorSchemeIndex value) {
        this.tx1 = value;
    }

    /**
     * Gets the value of the bg2 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getBg2() {
        return bg2;
    }

    /**
     * Sets the value of the bg2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setBg2(STColorSchemeIndex value) {
        this.bg2 = value;
    }

    /**
     * Gets the value of the tx2 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getTx2() {
        return tx2;
    }

    /**
     * Sets the value of the tx2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setTx2(STColorSchemeIndex value) {
        this.tx2 = value;
    }

    /**
     * Gets the value of the accent1 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent1() {
        return accent1;
    }

    /**
     * Sets the value of the accent1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent1(STColorSchemeIndex value) {
        this.accent1 = value;
    }

    /**
     * Gets the value of the accent2 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent2() {
        return accent2;
    }

    /**
     * Sets the value of the accent2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent2(STColorSchemeIndex value) {
        this.accent2 = value;
    }

    /**
     * Gets the value of the accent3 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent3() {
        return accent3;
    }

    /**
     * Sets the value of the accent3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent3(STColorSchemeIndex value) {
        this.accent3 = value;
    }

    /**
     * Gets the value of the accent4 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent4() {
        return accent4;
    }

    /**
     * Sets the value of the accent4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent4(STColorSchemeIndex value) {
        this.accent4 = value;
    }

    /**
     * Gets the value of the accent5 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent5() {
        return accent5;
    }

    /**
     * Sets the value of the accent5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent5(STColorSchemeIndex value) {
        this.accent5 = value;
    }

    /**
     * Gets the value of the accent6 property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getAccent6() {
        return accent6;
    }

    /**
     * Sets the value of the accent6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setAccent6(STColorSchemeIndex value) {
        this.accent6 = value;
    }

    /**
     * Gets the value of the hlink property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getHlink() {
        return hlink;
    }

    /**
     * Sets the value of the hlink property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setHlink(STColorSchemeIndex value) {
        this.hlink = value;
    }

    /**
     * Gets the value of the folHlink property.
     * 
     * @return
     *     possible object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public STColorSchemeIndex getFolHlink() {
        return folHlink;
    }

    /**
     * Sets the value of the folHlink property.
     * 
     * @param value
     *     allowed object is
     *     {@link STColorSchemeIndex }
     *     
     */
    public void setFolHlink(STColorSchemeIndex value) {
        this.folHlink = value;
    }

    /**
     * Returns the scheme color index of the passed color key as used in document operations.
     * 
     * @param colorKey
     *  The color key of a scheme color as used in document operations.
     *  
     * @return
     * 	The scheme color index associated to the passed color key.
     */
    public STColorSchemeIndex getByColorKey(final String colorKey) {
    	switch (colorKey) {
    		// dark/light colors are always fixed
	    	case "dark1": return getTx1();        // getSTColorSchemeIndex.DK_1;
	    	case "dark2": return getTx2();        // STColorSchemeIndex.DK_2;
	    	case "light1": return getBg1();       // STColorSchemeIndex.LT_1;
	    	case "light2": return getBg2();       // STColorSchemeIndex.LT_2;
	    	// text/background, and accents can be overridden by this mapping
	    	case "text1": return getTx1();
	    	case "text2": return getTx2();
	    	case "background1": return getBg1();
	    	case "background2": return getBg2();
	    	case "accent1": return getAccent1();
	    	case "accent2": return getAccent2();
	    	case "accent3": return getAccent3();
	    	case "accent4": return getAccent4();
	    	case "accent5": return getAccent5();
	    	case "accent6": return getAccent6();
	    	case "hyperlink": return getHlink();
	    	case "followedHyperlink": return getFolHlink();
	    	default: return null;
		}
    }

    /**
     * Returns the default scheme color index of the passed color key as used in document operations.
     * 
     * @param colorKey
     *  The color key of a scheme color as used in document operations.
     *  
     * @return
     * 	The default scheme color index associated to the passed color key.
     */
    public static STColorSchemeIndex getDefaultByColorKey(final String colorKey) {
    	switch (colorKey) {
	    	case "dark1": return STColorSchemeIndex.DK_1;
	    	case "dark2": return STColorSchemeIndex.DK_2;
	    	case "light1": return STColorSchemeIndex.LT_1;
	    	case "light2": return STColorSchemeIndex.LT_2;
	    	case "text1": return STColorSchemeIndex.DK_1;
	    	case "text2": return STColorSchemeIndex.DK_2;
	    	case "background1": return STColorSchemeIndex.LT_1;
	    	case "background2": return STColorSchemeIndex.LT_2;
	    	case "accent1": return STColorSchemeIndex.ACCENT_1;
	    	case "accent2": return STColorSchemeIndex.ACCENT_2;
	    	case "accent3": return STColorSchemeIndex.ACCENT_3;
	    	case "accent4": return STColorSchemeIndex.ACCENT_4;
	    	case "accent5": return STColorSchemeIndex.ACCENT_5;
	    	case "accent6": return STColorSchemeIndex.ACCENT_6;
	    	case "hyperlink": return STColorSchemeIndex.HLINK;
	    	case "followedHyperlink": return STColorSchemeIndex.FOL_HLINK;
	    	default: return null;
    	}
    }
}
