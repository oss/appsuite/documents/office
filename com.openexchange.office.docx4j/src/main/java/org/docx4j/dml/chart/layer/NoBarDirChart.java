
package org.docx4j.dml.chart.layer;

import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.dml.chart.IListSer;

@XmlTransient
public abstract class NoBarDirChart implements IListSer {

	@Override
	public final String getBarDirection() {
		return null;
	}

	@Override
	public final void setBarDirection(String barDir) {
	    //
	}
}
