
package org.docx4j.dml.chart;

public interface ISerSingleErrorBarContent extends ISerContent {
    public CTErrBars getErrBars();
}
