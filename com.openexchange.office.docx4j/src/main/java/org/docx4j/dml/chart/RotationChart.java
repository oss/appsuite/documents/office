
package org.docx4j.dml.chart;

public interface RotationChart {

    /**
     * Gets the value of the firstSliceAng property.
     *
     * @return
     *     possible object is
     *     {@link CTFirstSliceAng }
     *
     */
    CTFirstSliceAng getFirstSliceAng();

    /**
     * Sets the value of the firstSliceAng property.
     *
     * @param value
     *     allowed object is
     *     {@link CTFirstSliceAng }
     *
     */
    void setFirstSliceAng(CTFirstSliceAng value);
    
}
