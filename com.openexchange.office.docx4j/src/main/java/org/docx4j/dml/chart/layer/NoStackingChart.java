
package org.docx4j.dml.chart.layer;

import jakarta.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class NoStackingChart extends NoBarDirChart {

	@Override
	public final void setStacking(String stacking, String defaultStacking) {
	    //ignore
	}

	@Override
	public final String getStacking() {
		return null;
	}
}
