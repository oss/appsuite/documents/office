
package org.docx4j.dml.chart.layer;

import java.util.List;

import org.docx4j.dml.chart.CTUnsignedInt;

public abstract class NoAxIdChart extends NoStackingChart {

	@Override
	public List<CTUnsignedInt> getAxId() {
		return null;
	}
}
