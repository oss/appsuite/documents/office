
package org.docx4j.dml.chart;

import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTTextBody;

public interface IAxisDescription {

	public CTUnsignedInt getAxId();
	public void setAxId(CTUnsignedInt value);
	public CTAxPos getAxPos() ;
	public void setAxPos(CTAxPos value);
	public CTUnsignedInt getCrossAx();
	public void setCrossAx(CTUnsignedInt value);
    public CTScaling getScaling();
    public CTBoolean getDelete();
    public void setDelete(CTBoolean value);
    public CTShapeProperties getSpPr();
	public void setSpPr(CTShapeProperties shape);
    public CTChartLines getMajorGridlines();
	public void setMajorGridlines(CTChartLines value);
	public CTTickMark getMajorTickMark();
	public void setMajorTickMark(CTTickMark value);
	public CTTitle getTitle();
	public void setTitle(CTTitle title);
	public CTTickLblPos getTickLblPos();
	public void setTickLblPos(CTTickLblPos value);
	public CTTextBody getTxPr();
	public void setTxPr(CTTextBody value);

    public CTNumFmt getNumFmt();

    public void setNumFmt(CTNumFmt value);

}
