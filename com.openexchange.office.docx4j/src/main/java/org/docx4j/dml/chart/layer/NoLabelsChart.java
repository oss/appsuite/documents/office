
package org.docx4j.dml.chart.layer;

import jakarta.xml.bind.annotation.XmlTransient;

import org.docx4j.dml.chart.CTDLbls;

@XmlTransient
public abstract class NoLabelsChart extends NoVaryColorsChart {

	@Override
	public CTDLbls getDLbls()
	{
		return null;
	}

	@Override
	public void setDLbls(
		CTDLbls lbl)
	{
	    //
	}
}
