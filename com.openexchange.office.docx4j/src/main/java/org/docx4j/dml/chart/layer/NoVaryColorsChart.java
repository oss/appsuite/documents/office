
package org.docx4j.dml.chart.layer;

import jakarta.xml.bind.annotation.XmlTransient;

import org.docx4j.dml.chart.CTBoolean;

@XmlTransient
public abstract class NoVaryColorsChart extends NoStackingChart {

	@Override
	public final CTBoolean getVaryColors() {
		return null;
	}

	@Override
	public final void setVaryColors(CTBoolean vary) {
	    //
	}
}
