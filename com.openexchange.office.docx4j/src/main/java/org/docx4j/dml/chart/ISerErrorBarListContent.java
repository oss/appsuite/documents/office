
package org.docx4j.dml.chart;

import java.util.List;

public interface ISerErrorBarListContent extends ISerContent {
    public List<CTErrBars> getErrBars();
}
