
package org.docx4j.dml.chart;

import java.util.List;

/**
 * Set/Get the content for Bubble and scatter graphic  */

public interface ISerContentXY extends ISerErrorBarListContent {

    @Override
    public List<CTDPt> getDPt();

    @Override
    public CTDLbls getDLbls();

    @Override
    public void setDLbls(CTDLbls parValue);

    public List<CTTrendline> getTrendline();

}
