
package org.docx4j.dml.chart;

import java.util.List;

/**
 * Get the list of Series from a generic graphic.
 */
public interface IListSer {

	public List<CTUnsignedInt> getAxId();
    public List<ISerContent> getSer();
    public ISerContent createSer();
    public String getStacking();
    public void setStacking(String stacking, String defaultStacking);
    public CTBoolean getVaryColors();
    public void setVaryColors(CTBoolean vary);
	public String getBarDirection();
	public void setBarDirection(String barDir);
	public CTDLbls getDLbls();
	public void setDLbls(CTDLbls lbl);
}
