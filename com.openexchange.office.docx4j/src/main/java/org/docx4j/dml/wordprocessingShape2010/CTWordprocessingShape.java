
package org.docx4j.dml.wordprocessingShape2010;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTConnection;
import org.docx4j.dml.CTNonVisualConnectorProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualDrawingShapeProps;
import org.docx4j.dml.CTOfficeArtExtensionList;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTShapeStyle;
import org.docx4j.dml.CTTextBodyProperties;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.IConnectionAccessor;
import org.docx4j.dml.IShape;
import org.docx4j.dml.ITransform2DAccessor;
import org.docx4j.jaxb.Context;

/**
 * <p>Java class for CT_WordprocessingShape complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_WordprocessingShape">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="cNvSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingShapeProps"/>
 *           &lt;element name="cNvCnPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualConnectorProperties"/>
 *         &lt;/choice>
 *         &lt;element name="spPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeProperties"/>
 *         &lt;element name="style" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeStyle" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="txbx" type="{http://schemas.microsoft.com/office/word/2010/wordprocessingShape}CT_TextboxInfo"/>
 *           &lt;element name="linkedTxbx" type="{http://schemas.microsoft.com/office/word/2010/wordprocessingShape}CT_LinkedTextboxInformation"/>
 *         &lt;/choice>
 *         &lt;element name="bodyPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TextBodyProperties"/>
 *       &lt;/sequence>
 *       &lt;attribute name="normalEastAsianFlow" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name="wsp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WordprocessingShape", propOrder = {
    "cNvPr",
    "cNvSpPr",
    "cNvCnPr",
    "spPr",
    "style",
    "extLst",
    "txbx",
    "linkedTxbx",
    "bodyPr"
})
public class CTWordprocessingShape implements ITransform2DAccessor, IShape, IConnectionAccessor {

    protected CTNonVisualDrawingProps cNvPr;
    protected CTNonVisualDrawingShapeProps cNvSpPr;
    protected CTNonVisualConnectorProperties cNvCnPr;
    @XmlElement(required = true)
    protected CTShapeProperties spPr;
    protected CTShapeStyle style;
    protected CTOfficeArtExtensionList extLst;
    protected CTTextboxInfo txbx;
    protected CTLinkedTextboxInformation linkedTxbx;
    @XmlElement(required = true)
    protected CTTextBodyProperties bodyPr;
    @XmlAttribute(name = "normalEastAsianFlow")
    protected Boolean normalEastAsianFlow;

    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	return spPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        spPr.removeXfrm();
    }

	@Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(cNvPr==null&&createIfMissing) {
			cNvPr = new CTNonVisualDrawingProps();
		}
		return cNvPr;
	}

	
    /**
     * Gets the value of the cNvSpPr property.
     *
     * @return
     *     possible object is
     *     {@link CTNonVisualConnectorProperties }
     *
     */
	@Override
    public CTNonVisualDrawingShapeProps getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(cNvSpPr==null&&createIfMissing) {
			cNvSpPr = new CTNonVisualDrawingShapeProps();
		}
		return cNvSpPr;
    }

    /**
     * Sets the value of the cNvCnPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTNonVisualConnectorProperties }
     *
     */
    public CTNonVisualConnectorProperties getNonVisualConnectorProperties(boolean forceCreate) {
        if(cNvCnPr==null&&forceCreate) {
            cNvCnPr = new CTNonVisualConnectorProperties();
        }
        return cNvCnPr;
    }

    /**
     * Gets the value of the spPr property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *
     */
    @Override
    public CTShapeProperties getSpPr() {
        return spPr;
    }

    /**
     * Sets the value of the spPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *
     */
    @Override
    public void setSpPr(CTShapeProperties value) {
        this.spPr = value;
    }

    /**
     * Gets the value of the style property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeStyle }
     *
     */
    @Override
    public CTShapeStyle getStyle(boolean forceCreate) {
    	if(style==null&&forceCreate) {
    		style = new CTShapeStyle();
    	}
        return style;
    }

    /**
     * Sets the value of the style property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeStyle }
     *
     */
    @Override
    public void setStyle(CTShapeStyle value) {
        this.style = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the txbx property.
     *
     * @return
     *     possible object is
     *     {@link CTTextboxInfo }
     *
     */
    public CTTextboxInfo getTxbx(boolean forceCreate) {
        if(txbx==null&&forceCreate) {
            txbx=new CTTextboxInfo();
        }
        return txbx;
    }

    /**
     * Sets the value of the txbx property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTextboxInfo }
     *
     */
    public void setTxbx(CTTextboxInfo value) {
        this.txbx = value;
    }

    /**
     * Gets the value of the linkedTxbx property.
     *
     * @return
     *     possible object is
     *     {@link CTLinkedTextboxInformation }
     *
     */
    public CTLinkedTextboxInformation getLinkedTxbx() {
        return linkedTxbx;
    }

    /**
     * Sets the value of the linkedTxbx property.
     *
     * @param value
     *     allowed object is
     *     {@link CTLinkedTextboxInformation }
     *
     */
    public void setLinkedTxbx(CTLinkedTextboxInformation value) {
        this.linkedTxbx = value;
    }

    /**
     * Gets the value of the bodyPr property.
     *
     * @return
     *     possible object is
     *     {@link CTTextBodyProperties }
     *
     */
    public CTTextBodyProperties getBodyPr() {
        return bodyPr;
    }

    /**
     * Sets the value of the bodyPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTTextBodyProperties }
     *
     */
    public void setBodyPr(CTTextBodyProperties value) {
        this.bodyPr = value;
    }

    /**
     * Gets the value of the normalEastAsianFlow property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public boolean isNormalEastAsianFlow() {
        if (normalEastAsianFlow == null) {
            return false;
        }
        return normalEastAsianFlow;
    }

    /**
     * Sets the value of the normalEastAsianFlow property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setNormalEastAsianFlow(Boolean value) {
        this.normalEastAsianFlow = value;
    }

	@Override
	public boolean supportsTextBody() {
		return true;
	}

	@Override
	public CTTextBodyProperties getTextBodyProperties(boolean forceCreate) {
		if(bodyPr==null&&forceCreate) {
			bodyPr = Context.getDmlObjectFactory().createCTTextBodyProperties();
		}
		return bodyPr;
	}

	@Override
	public void setTextBody(CTTextBodyProperties textBodyProperties) {
		bodyPr = textBodyProperties;
 	}

    @Override
    public CTConnection getStartConnection(boolean forceCreate) {
        final CTNonVisualConnectorProperties connectorProperties = getNonVisualConnectorProperties(forceCreate);
        if(connectorProperties==null) {
            return null;
        }
        return connectorProperties.getStartConnection(forceCreate);
    }

    @Override
    public CTConnection getEndConnection(boolean forceCreate) {
        final CTNonVisualConnectorProperties connectorProperties = getNonVisualConnectorProperties(forceCreate);
        if(connectorProperties==null) {
            return null;
        }
        return connectorProperties.getEndConnection(forceCreate);
    }

    @Override
    public void setStartConnection(CTConnection connection) {
        getNonVisualConnectorProperties(true).setStartConnection(connection);
    }

    @Override
    public void setEndConnection(CTConnection connection) {
        getNonVisualConnectorProperties(true).setEndConnection(connection);
    }


    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
