
package org.docx4j.dml.wordprocessingShape2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTOfficeArtExtensionList;
import org.docx4j.wml.CTTxbxContent;


/**
 * <p>Java class for CT_TextboxInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TextboxInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}txbxContent" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" default="0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TextboxInfo", propOrder = {
    "txbxContent",
    "extLst"
})
public class CTTextboxInfo {

    @XmlElement(namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main")
    protected CTTxbxContent txbxContent;
    protected CTOfficeArtExtensionList extLst;
    @XmlAttribute(name = "id")
    @XmlSchemaType(name = "unsignedShort")
    protected Integer id;

    /**
     * Gets the value of the txbxContent property.
     * 
     * @return
     *     possible object is
     *     {@link CTTxbxContent }
     *     
     */
    public CTTxbxContent getTxbxContent(boolean forceCreate) {
        if(txbxContent==null&&forceCreate) {
            txbxContent = new CTTxbxContent();
        }
        return txbxContent;
    }

    /**
     * Sets the value of the txbxContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTxbxContent }
     *     
     */
    public void setTxbxContent(CTTxbxContent value) {
        this.txbxContent = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getId() {
        if (id == null) {
            return  0;
        }
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }
}
