/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_StyleMatrixReference complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_StyleMatrixReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/main}EG_ColorChoice" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="idx" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_StyleMatrixColumnIndex" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_StyleMatrixReference", propOrder = {
    "scrgbClr",
    "srgbClr",
    "hslClr",
    "sysClr",
    "schemeClr",
    "prstClr"
})
public class CTStyleMatrixReference implements IStyleMatrixReference {

	public CTStyleMatrixReference() {
		idx = 0;
	}

	protected CTScRgbColor scrgbClr;
    protected CTSRgbColor srgbClr;
    protected CTHslColor hslClr;
    protected CTSystemColor sysClr;
    protected CTSchemeColor schemeClr;
    protected CTPresetColor prstClr;
    @XmlAttribute(required = true)
    protected long idx;

    /**
     * Gets the value of the scrgbClr property.
     *
     * @return
     *     possible object is
     *     {@link CTScRgbColor }
     *
     */
    @Override
    public CTScRgbColor getScrgbClr() {
        return scrgbClr;
    }

    /**
     * Sets the value of the scrgbClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTScRgbColor }
     *
     */
    @Override
    public void setScrgbClr(CTScRgbColor value) {
        this.scrgbClr = value;
    }

    /**
     * Gets the value of the srgbClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSRgbColor }
     *
     */
    @Override
    public CTSRgbColor getSrgbClr() {
        return srgbClr;
    }

    /**
     * Sets the value of the srgbClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSRgbColor }
     *
     */
    @Override
    public void setSrgbClr(CTSRgbColor value) {
        this.srgbClr = value;
    }

    /**
     * Gets the value of the hslClr property.
     *
     * @return
     *     possible object is
     *     {@link CTHslColor }
     *
     */
    @Override
    public CTHslColor getHslClr() {
        return hslClr;
    }

    /**
     * Sets the value of the hslClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTHslColor }
     *
     */
    @Override
    public void setHslClr(CTHslColor value) {
        this.hslClr = value;
    }

    /**
     * Gets the value of the sysClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSystemColor }
     *
     */
    @Override
    public CTSystemColor getSysClr() {
        return sysClr;
    }

    /**
     * Sets the value of the sysClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSystemColor }
     *
     */
    @Override
    public void setSysClr(CTSystemColor value) {
        this.sysClr = value;
    }

    /**
     * Gets the value of the schemeClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSchemeColor }
     *
     */
    @Override
    public CTSchemeColor getSchemeClr() {
        return schemeClr;
    }

    /**
     * Sets the value of the schemeClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSchemeColor }
     *
     */
    @Override
    public void setSchemeClr(CTSchemeColor value) {
        this.schemeClr = value;
    }

    /**
     * Gets the value of the prstClr property.
     *
     * @return
     *     possible object is
     *     {@link CTPresetColor }
     *
     */
    @Override
    public CTPresetColor getPrstClr() {
        return prstClr;
    }

    /**
     * Sets the value of the prstClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPresetColor }
     *
     */
    @Override
    public void setPrstClr(CTPresetColor value) {
        this.prstClr = value;
    }

    /**
     * Gets the value of the idx property.
     *
     */
    @Override
    public long getIdx() {
        return idx;
    }

    /**
     * Sets the value of the idx property.
     *
     */
    @Override
    public void setIdx(long value) {
        this.idx = value;
    }
}
