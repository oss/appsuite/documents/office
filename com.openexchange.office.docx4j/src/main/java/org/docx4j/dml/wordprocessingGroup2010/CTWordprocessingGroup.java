
package org.docx4j.dml.wordprocessingGroup2010;

import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElements;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTGroupShapeProperties;
import org.docx4j.dml.CTGroupTransform2D;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualGroupDrawingShapeProps;
import org.docx4j.dml.CTOfficeArtExtensionList;
import org.docx4j.dml.IGroupShape;
import org.docx4j.dml.ITransform2DAccessor;
import org.docx4j.dml.picture.Pic;
import org.docx4j.dml.wordprocessingShape2010.CTWordprocessingShape;
import org.docx4j.w14.CTWordContentPart;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_WordprocessingGroup complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_WordprocessingGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cNvPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps" minOccurs="0"/>
 *         &lt;element name="cNvGrpSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualGroupDrawingShapeProps"/>
 *         &lt;element name="grpSpPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_GroupShapeProperties"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordprocessingShape}wsp"/>
 *           &lt;element name="grpSp" type="{http://schemas.microsoft.com/office/word/2010/wordprocessingGroup}CT_WordprocessingGroup"/>
 *           &lt;element name="graphicFrame" type="{http://schemas.microsoft.com/office/word/2010/wordprocessingGroup}CT_GraphicFrame"/>
 *           &lt;element ref="{http://schemas.openxmlformats.org/drawingml/2006/picture}pic"/>
 *           &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}contentPart"/>
 *         &lt;/choice>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name ="wgp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WordprocessingGroup", propOrder = {
    "cNvPr",
    "cNvGrpSpPr",
    "grpSpPr",
    "wspOrGrpSpOrGraphicFrame",
    "extLst"
})
public class CTWordprocessingGroup implements ITransform2DAccessor, IGroupShape, IContentAccessor<Object> {

    protected CTNonVisualDrawingProps cNvPr;
    @XmlElement(required = true)
    protected CTNonVisualGroupDrawingShapeProps cNvGrpSpPr;
    @XmlElement(required = true)
    protected CTGroupShapeProperties grpSpPr;
    @XmlElements({
        @XmlElement(name = "wsp", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingShape", type = CTWordprocessingShape.class),
        @XmlElement(name = "grpSp", type = CTWordprocessingGroup.class),
        @XmlElement(name = "graphicFrame", type = CTGraphicFrame.class),
        @XmlElement(name = "pic", namespace = "http://schemas.openxmlformats.org/drawingml/2006/picture", type = Pic.class),
        @XmlElement(name = "contentPart", namespace = "http://schemas.microsoft.com/office/word/2010/wordml", type = CTWordContentPart.class)
    })
    protected DLList<Object> wspOrGrpSpOrGraphicFrame;
    protected CTOfficeArtExtensionList extLst;

    /**
     * Gets the value of the xfrm property.
     *
     * @return
     *     possible object is
     *     {@link CTGroupTransform2D }
     *
     */
    @Override
    public CTGroupTransform2D getXfrm(boolean forceCreate) {
    	return grpSpPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        grpSpPr.removeXfrm();
    }

    /**
     * Gets the value of the wspOrGrpSpOrGraphicFrame property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wspOrGrpSpOrGraphicFrame property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWspOrGrpSpOrGraphicFrame().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTWordprocessingShape }
     * {@link CTWordprocessingGroup }
     * {@link CTGraphicFrame }
     * {@link Pic }
     * {@link CTWordContentPart }
     *
     *
     */

    @Override
    public DLList<Object> getContent() {
        if (wspOrGrpSpOrGraphicFrame == null) {
            wspOrGrpSpOrGraphicFrame = new DLList<Object>();
        }
        return this.wspOrGrpSpOrGraphicFrame;
    }
    /**
     * Gets the value of the cNvPr property.
     *
     * @return
     *     possible object is
     *     {@link CTNonVisualDrawingProps }
     *
     */
	@Override
    public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(cNvPr==null&&createIfMissing) {
			cNvPr = new CTNonVisualDrawingProps();
		}
        return cNvPr;
    }

    /**
     * Gets the value of the cNvGrpSpPr property.
     *
     * @return
     *     possible object is
     *     {@link CTNonVisualGroupDrawingShapeProps }
     *
     */
	@Override
    public CTNonVisualGroupDrawingShapeProps getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(cNvGrpSpPr==null&&createIfMissing) {
			cNvGrpSpPr = new CTNonVisualGroupDrawingShapeProps();
		}
        return cNvGrpSpPr;
    }

    /**
     * Sets the value of the cNvGrpSpPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTNonVisualGroupDrawingShapeProps }
     *
     */
    public void setCNvGrpSpPr(CTNonVisualGroupDrawingShapeProps value) {
        this.cNvGrpSpPr = value;
    }

    /**
     * Gets the value of the grpSpPr property.
     *
     * @return
     *     possible object is
     *     {@link CTGroupShapeProperties }
     *
     */
    @Override
    public CTGroupShapeProperties getGrpSpPr() {
        return grpSpPr;
    }

    /**
     * Sets the value of the grpSpPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGroupShapeProperties }
     *
     */
    @Override
    public void setGrpSpPr(CTGroupShapeProperties value) {
        this.grpSpPr = value;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }


    @XmlTransient
    private Object parent;

    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }

    /**
     * This method is invoked by the JAXB implementation on each instance when unmarshalling completes.
     * 
     * @param parent
     *     The parent object in the object tree.
     * @param unmarshaller
     *     The unmarshaller that generated the instance.
     */
    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object _parent) {
        setParent(_parent);
    }
}
