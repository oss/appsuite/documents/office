/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CT_Transform2D complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Transform2D">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="off" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Point2D" minOccurs="0"/>
 *         &lt;element name="ext" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_PositiveSize2D" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="rot" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_Angle" default="0" />
 *       &lt;attribute name="flipH" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="flipV" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Transform2D", propOrder = {
    "off",
    "ext"
})
public class CTTransform2D implements ITransform2D {

    protected CTPoint2D off;
    protected CTPositiveSize2D ext;
    @XmlAttribute
    protected Integer rot;
    @XmlAttribute
    protected Boolean flipH;
    @XmlAttribute
    protected Boolean flipV;

    /**
     * Gets the value of the off property.
     *
     * @return
     *     possible object is
     *     {@link CTPoint2D }
     *
     */
    @Override
    public CTPoint2D getOff(boolean forceCreate) {
    	if(off==null&&forceCreate) {
    		off = new CTPoint2D();
    	}
        return off;
    }

    /**
     * Sets the value of the off property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPoint2D }
     *
     */
    @Override
    public void setOff(CTPoint2D value) {
        this.off = value;
    }

    /**
     * Gets the value of the ext property.
     *
     * @return
     *     possible object is
     *     {@link CTPositiveSize2D }
     *
     */
    @Override
    public CTPositiveSize2D getExt(boolean forceCreate) {
    	if(ext==null&&forceCreate) {
    		ext = new CTPositiveSize2D();
    	}
    	return ext;
    }

    /**
     * Sets the value of the ext property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPositiveSize2D }
     *
     */
    @Override
    public void setExt(CTPositiveSize2D value) {
        this.ext = value;
    }

    /**
     * Gets the value of the rot property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    @Override
    public int getRot() {
        if (rot == null) {
            return  0;
        }
        return rot;
    }

    /**
     * Sets the value of the rot property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    @Override
    public void setRot(Integer value) {
        this.rot = value;
    }

    /**
     * Gets the value of the flipH property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isFlipH() {
        if (flipH == null) {
            return false;
        }
        return flipH;
    }

    /**
     * Sets the value of the flipH property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setFlipH(Boolean value) {
        this.flipH = value;
    }

    /**
     * Gets the value of the flipV property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Override
    public boolean isFlipV() {
        if (flipV == null) {
            return false;
        }
        return flipV;
    }

    /**
     * Sets the value of the flipV property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Override
    public void setFlipV(Boolean value) {
        this.flipV = value;
    }
}
