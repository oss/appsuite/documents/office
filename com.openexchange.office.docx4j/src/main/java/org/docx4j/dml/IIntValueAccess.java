
package org.docx4j.dml;

public interface IIntValueAccess {

    public int getVal();

    public void setVal(int value);
}
