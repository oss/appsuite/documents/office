/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.picture;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import org.docx4j.dml.CTBlipFillProperties;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualPictureProperties;
import org.docx4j.dml.CTShapeProperties;
import org.docx4j.dml.CTShapeStyle;
import org.docx4j.dml.CTTransform2D;
import org.docx4j.dml.IPicture;
import org.docx4j.dml.ITransform2DAccessor;

/**
 * <p>Java class for CT_Picture complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Picture">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nvPicPr" type="{http://schemas.openxmlformats.org/drawingml/2006/picture}CT_PictureNonVisual"/>
 *         &lt;element name="blipFill" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_BlipFillProperties"/>
 *         &lt;element name="spPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_ShapeProperties"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://schemas.openxmlformats.org/drawingml/2006/picture", name = "CT_Picture", propOrder = {
    "nvPicPr",
    "blipFill",
    "spPr"
})
@XmlRootElement(name = "pic")
public class Pic implements ITransform2DAccessor, IPicture {

    @XmlElement(required = true)
    protected CTPictureNonVisual nvPicPr;
    @XmlElement(required = true)
    protected CTBlipFillProperties blipFill;
    @XmlElement(required = true)
    protected CTShapeProperties spPr;

    @Override
    public CTTransform2D getXfrm(boolean forceCreate) {
    	return spPr.getXfrm(forceCreate);
    }

    @Override
    public void removeXfrm() {
        spPr.removeXfrm();
    }

    /**
     * Gets the value of the blipFill property.
     *
     * @return
     *     possible object is
     *     {@link CTBlipFillProperties }
     *
     */
    @Override
    public CTBlipFillProperties getBlipFill(boolean forceCreate) {
    	if(blipFill==null&&forceCreate) {
    		blipFill = new CTBlipFillProperties();
    	}
    	return blipFill;
    }

    /**
     * Sets the value of the blipFill property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBlipFillProperties }
     *
     */
    @Override
    public void setBlipFill(CTBlipFillProperties value) {
        this.blipFill = value;
    }

    /**
     * Gets the value of the spPr property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *
     */
    @Override
    public CTShapeProperties getSpPr() {
        return spPr;
    }

    /**
     * Sets the value of the spPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *
     */
    @Override
    public void setSpPr(CTShapeProperties value) {
        this.spPr = value;
    }

    /**
     *
     * Pic is not supporting a style, so we return null to fulfill the abstract interface
     *
     */
    @Override
    public CTShapeStyle getStyle(boolean forceCreate) {
    	return null;
    }

    @Override
	public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
		if(nvPicPr==null&&createIfMissing) {
			nvPicPr = new CTPictureNonVisual();
		}
		if(nvPicPr!=null) {
			if(nvPicPr.getCNvPr()==null&&createIfMissing) {
				nvPicPr.setCNvPr(new CTNonVisualDrawingProps());
			}
			return nvPicPr.getCNvPr();
		}
		return null;
	}

	@Override
	public CTNonVisualPictureProperties getNonVisualDrawingShapeProperties(boolean createIfMissing) {
		if(nvPicPr==null&&createIfMissing) {
			nvPicPr = new CTPictureNonVisual();
		}
		if(nvPicPr!=null) {
			if(nvPicPr.getCNvPicPr()==null&&createIfMissing) {
				nvPicPr.setCNvPicPr(new CTNonVisualPictureProperties());
			}
			return nvPicPr.getCNvPicPr();
		}
		return null;
	}
}
