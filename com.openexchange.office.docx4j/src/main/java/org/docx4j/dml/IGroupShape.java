
package org.docx4j.dml;

import com.openexchange.office.filter.core.component.Child;

public interface IGroupShape extends Child, INonVisualDrawingPropertyAccess, INonVisualDrawingShapePropertyAccess {

    /**
     * Gets the value of the grpSpPr property.
     *
     * @return
     *     possible object is
     *     {@link CTGroupShapeProperties }
     *
     */
    public CTGroupShapeProperties getGrpSpPr();

    /**
     * Sets the value of the grpSpPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTGroupShapeProperties }
     *
     */
    public void setGrpSpPr(CTGroupShapeProperties value);
}
