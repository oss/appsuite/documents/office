
package org.docx4j.dml;

public interface IShapeBasic extends INonVisualDrawingPropertyAccess, INonVisualDrawingShapePropertyAccess {

    /**
     * Gets the value of the spPr property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeProperties }
     *
     */
    public CTShapeProperties getSpPr();

    /**
     * Sets the value of the spPr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeProperties }
     *
     */
    public void setSpPr(CTShapeProperties value);

    /**
     * Gets the value of the style property.
     *
     * @return
     *     possible object is
     *     {@link CTShapeStyle }
     *
     */
    public CTShapeStyle getStyle(boolean forceCreate);
}
