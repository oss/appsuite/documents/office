/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml;

import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>Java class for CT_TableStyle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_TableStyle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tblBg" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TableBackgroundStyle" minOccurs="0"/>
 *         &lt;element name="wholeTbl" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="band1H" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="band2H" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="band1V" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="band2V" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="lastCol" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="firstCol" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="lastRow" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="seCell" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="swCell" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="firstRow" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="neCell" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="nwCell" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_TablePartStyle" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="styleId" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_Guid" />
 *       &lt;attribute name="styleName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_TableStyle", propOrder = {
    "tblBg",
    "wholeTbl",
    "band1H",
    "band2H",
    "band1V",
    "band2V",
    "lastCol",
    "firstCol",
    "lastRow",
    "seCell",
    "swCell",
    "firstRow",
    "neCell",
    "nwCell",
    "extLst"
})
public class CTTableStyle {

    protected CTTableBackgroundStyle tblBg;
    protected CTTablePartStyle wholeTbl;
    protected CTTablePartStyle band1H;
    protected CTTablePartStyle band2H;
    protected CTTablePartStyle band1V;
    protected CTTablePartStyle band2V;
    protected CTTablePartStyle lastCol;
    protected CTTablePartStyle firstCol;
    protected CTTablePartStyle lastRow;
    protected CTTablePartStyle seCell;
    protected CTTablePartStyle swCell;
    protected CTTablePartStyle firstRow;
    protected CTTablePartStyle neCell;
    protected CTTablePartStyle nwCell;
    protected CTOfficeArtExtensionList extLst;

    // the styleId has to be a GUID like "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" otherwise
    // otherwise PPT is showing a repair dialog when opening the document
    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String styleId;
    @XmlAttribute(required = true)
    protected String styleName;
    @XmlTransient
    final static public String[] tableStyleNames = {"wholeTbl", "band1H", "band2H", "band1V", "band2V", "lastCol", "firstCol", "lastRow", "seCell", "swCell", "firstRow", "neCell", "nwCell"};

    public CTTablePartStyle getTableStyleByName(String tableStyleName, boolean forceCreate) {
    	switch(tableStyleName) {
	    	case "wholeTbl" : return getWholeTbl(forceCreate);
	    	case "band1H"	: return getBand1H(forceCreate);
	    	case "band2H"	: return getBand2H(forceCreate);
	    	case "band1V"	: return getBand1V(forceCreate);
	    	case "band2V" 	: return getBand2V(forceCreate);
	    	case "lastCol"	: return getLastCol(forceCreate);
	    	case "firstCol"	: return getFirstCol(forceCreate);
	    	case "lastRow"	: return getLastRow(forceCreate);
	    	case "seCell"	: return getSeCell(forceCreate);
	    	case "swCell"	: return getSwCell(forceCreate);
	    	case "firstRow"	: return getFirstRow(forceCreate);
	    	case "neCell"	: return getNeCell(forceCreate);
	    	case "nwCell"	: return getNwCell(forceCreate);
    	}
    	return null;
    }

    public void setTableStyleByName(String tableStyleName, CTTablePartStyle tableStyle) {
    	switch(tableStyleName) {
	    	case "wholeTbl" : setWholeTbl(tableStyle); return;
	    	case "band1H"	: setBand1H(tableStyle); return;
	    	case "band2H"	: setBand2H(tableStyle); return;
	    	case "band1V"	: setBand1V(tableStyle); return;
	    	case "band2V" 	: setBand2V(tableStyle); return;
	    	case "lastCol"	: setLastCol(tableStyle); return;
	    	case "firstCol"	: setFirstCol(tableStyle); return;
	    	case "lastRow"	: setLastRow(tableStyle); return;
	    	case "seCell"	: setSeCell(tableStyle); return;
	    	case "swCell"	: setSwCell(tableStyle); return;
	    	case "firstRow"	: setFirstRow(tableStyle); return;
	    	case "neCell"	: setNeCell(tableStyle); return;
	    	case "nwCell"	: setNwCell(tableStyle); return;
    	}
    }

    /**
     * Gets the value of the tblBg property.
     * 
     * @return
     *     possible object is
     *     {@link CTTableBackgroundStyle }
     *     
     */
    public CTTableBackgroundStyle getTblBg() {
        return tblBg;
    }

    /**
     * Sets the value of the tblBg property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTableBackgroundStyle }
     *     
     */
    public void setTblBg(CTTableBackgroundStyle value) {
        this.tblBg = value;
    }

    /**
     * Gets the value of the wholeTbl property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getWholeTbl(boolean forceCreate) {
    	if(wholeTbl==null&&forceCreate) {
    		wholeTbl = new CTTablePartStyle();
    	}
        return wholeTbl;
    }

    /**
     * Sets the value of the wholeTbl property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setWholeTbl(CTTablePartStyle value) {
        this.wholeTbl = value;
    }

    /**
     * Gets the value of the band1H property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getBand1H(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return band1H;
    }

    /**
     * Sets the value of the band1H property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setBand1H(CTTablePartStyle value) {
        this.band1H = value;
    }

    /**
     * Gets the value of the band2H property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getBand2H(boolean forceCreate) {
    	if(band2H==null&&forceCreate) {
    		band2H = new CTTablePartStyle();
    	}
        return band2H;
    }

    /**
     * Sets the value of the band2H property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setBand2H(CTTablePartStyle value) {
        this.band2H = value;
    }

    /**
     * Gets the value of the band1V property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getBand1V(boolean forceCreate) {
    	if(band1V==null&&forceCreate) {
    		band1V = new CTTablePartStyle();
    	}
        return band1V;
    }

    /**
     * Sets the value of the band1V property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setBand1V(CTTablePartStyle value) {
        this.band1V = value;
    }

    /**
     * Gets the value of the band2V property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getBand2V(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return band2V;
    }

    /**
     * Sets the value of the band2V property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setBand2V(CTTablePartStyle value) {
        this.band2V = value;
    }

    /**
     * Gets the value of the lastCol property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getLastCol(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return lastCol;
    }

    /**
     * Sets the value of the lastCol property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setLastCol(CTTablePartStyle value) {
        this.lastCol = value;
    }

    /**
     * Gets the value of the firstCol property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getFirstCol(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return firstCol;
    }

    /**
     * Sets the value of the firstCol property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setFirstCol(CTTablePartStyle value) {
        this.firstCol = value;
    }

    /**
     * Gets the value of the lastRow property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getLastRow(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return lastRow;
    }

    /**
     * Sets the value of the lastRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setLastRow(CTTablePartStyle value) {
        this.lastRow = value;
    }

    /**
     * Gets the value of the seCell property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getSeCell(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return seCell;
    }

    /**
     * Sets the value of the seCell property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setSeCell(CTTablePartStyle value) {
        this.seCell = value;
    }

    /**
     * Gets the value of the swCell property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getSwCell(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return swCell;
    }

    /**
     * Sets the value of the swCell property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setSwCell(CTTablePartStyle value) {
        this.swCell = value;
    }

    /**
     * Gets the value of the firstRow property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getFirstRow(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return firstRow;
    }

    /**
     * Sets the value of the firstRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setFirstRow(CTTablePartStyle value) {
        this.firstRow = value;
    }

    /**
     * Gets the value of the neCell property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getNeCell(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return neCell;
    }

    /**
     * Sets the value of the neCell property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setNeCell(CTTablePartStyle value) {
        this.neCell = value;
    }

    /**
     * Gets the value of the nwCell property.
     * 
     * @return
     *     possible object is
     *     {@link CTTablePartStyle }
     *     
     */
    public CTTablePartStyle getNwCell(boolean forceCreate) {
    	if(band1H==null&&forceCreate) {
    		band1H = new CTTablePartStyle();
    	}
        return nwCell;
    }

    /**
     * Sets the value of the nwCell property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTTablePartStyle }
     *     
     */
    public void setNwCell(CTTablePartStyle value) {
        this.nwCell = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the styleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStyleId() {
        return styleId;
    }

    /**
     * Sets the value of the styleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStyleId(String value) {
        this.styleId = value;
    }

    /**
     * Gets the value of the styleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStyleName() {
        return styleName;
    }

    /**
     * Sets the value of the styleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStyleName(String value) {
        this.styleName = value;
    }

    public void beforeMarshal(Marshaller marshaller) {
    	for(int i=0; i<tableStyleNames.length; i++) {
    		final CTTablePartStyle tablePartStyle = this.getTableStyleByName(tableStyleNames[i], false);
    		if(tablePartStyle!=null&&tablePartStyle.isEmpty()) {
    			setTableStyleByName(tableStyleNames[i], null);
    		}
    	}
   	}
}
