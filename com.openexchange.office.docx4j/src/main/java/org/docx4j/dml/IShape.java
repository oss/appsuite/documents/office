
package org.docx4j.dml;

import com.openexchange.office.filter.core.component.Child;

public interface IShape extends Child, IShapeBasic {

    /**
     * Sets the value of the style property.
     *
     * @param value
     *     allowed object is
     *     {@link CTShapeStyle }
     *
     */
    public void setStyle(CTShapeStyle value);

    /* returns whether the shape is supporting a textBody */
    public boolean supportsTextBody();

    /* Returns current textBodyProperties. if they are missing they are created
     * if forceCreate is true. TextBodyProperties can only be saved if there also exists
     * at least one paragraph. Before marshalling this is checked and if necessary the
     * textBodyProperties are removed (if there exists no paragraph). Not each shape is
     * supporting a textBody, in this case zere is returned if forceCreate is true */
    public CTTextBodyProperties getTextBodyProperties(boolean forceCreate);

    public void setTextBody(CTTextBodyProperties textBodyProperties);
}
