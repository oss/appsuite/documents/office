/**
 * @author sven.jacobi@open-xchange.com
 */
package org.docx4j.dml;

public interface ITransform2DAccessor {

	public ITransform2D getXfrm(boolean forceCreate);

	public void removeXfrm();
}
