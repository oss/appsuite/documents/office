
package org.docx4j.dml.chartStyle2012;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_StyleColorEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_StyleColorEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="auto"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_StyleColorEnum")
@XmlEnum
public enum STStyleColorEnum {

    @XmlEnumValue("auto")
    AUTO("auto");
    private final String value;

    STStyleColorEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STStyleColorEnum fromValue(String v) {
        for (STStyleColorEnum c: STStyleColorEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
