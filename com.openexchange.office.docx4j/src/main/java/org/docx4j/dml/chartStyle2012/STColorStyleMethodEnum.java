
package org.docx4j.dml.chartStyle2012;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_ColorStyleMethodEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_ColorStyleMethodEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="cycle"/>
 *     &lt;enumeration value="withinLinear"/>
 *     &lt;enumeration value="acrossLinear"/>
 *     &lt;enumeration value="withinLinearReversed"/>
 *     &lt;enumeration value="acrossLinearReversed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_ColorStyleMethodEnum")
@XmlEnum
public enum STColorStyleMethodEnum {

    @XmlEnumValue("cycle")
    CYCLE("cycle"),
    @XmlEnumValue("withinLinear")
    WITHIN_LINEAR("withinLinear"),
    @XmlEnumValue("acrossLinear")
    ACROSS_LINEAR("acrossLinear"),
    @XmlEnumValue("withinLinearReversed")
    WITHIN_LINEAR_REVERSED("withinLinearReversed"),
    @XmlEnumValue("acrossLinearReversed")
    ACROSS_LINEAR_REVERSED("acrossLinearReversed");
    private final String value;

    STColorStyleMethodEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STColorStyleMethodEnum fromValue(String v) {
        for (STColorStyleMethodEnum c: STColorStyleMethodEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
