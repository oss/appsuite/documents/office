
package org.docx4j.dml.chartStyle2012;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ST_MarkerStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ST_MarkerStyle">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="circle"/>
 *     &lt;enumeration value="dash"/>
 *     &lt;enumeration value="diamond"/>
 *     &lt;enumeration value="dot"/>
 *     &lt;enumeration value="plus"/>
 *     &lt;enumeration value="square"/>
 *     &lt;enumeration value="star"/>
 *     &lt;enumeration value="triangle"/>
 *     &lt;enumeration value="x"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ST_MarkerStyle")
@XmlEnum
public enum STMarkerStyle {

    @XmlEnumValue("circle")
    CIRCLE("circle"),
    @XmlEnumValue("dash")
    DASH("dash"),
    @XmlEnumValue("diamond")
    DIAMOND("diamond"),
    @XmlEnumValue("dot")
    DOT("dot"),
    @XmlEnumValue("plus")
    PLUS("plus"),
    @XmlEnumValue("square")
    SQUARE("square"),
    @XmlEnumValue("star")
    STAR("star"),
    @XmlEnumValue("triangle")
    TRIANGLE("triangle"),
    @XmlEnumValue("x")
    X("x");
    private final String value;

    STMarkerStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STMarkerStyle fromValue(String v) {
        for (STMarkerStyle c: STMarkerStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
