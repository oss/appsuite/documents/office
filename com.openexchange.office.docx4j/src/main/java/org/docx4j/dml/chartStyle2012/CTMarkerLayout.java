
package org.docx4j.dml.chartStyle2012;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_MarkerLayout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_MarkerLayout">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="symbol" type="{http://schemas.microsoft.com/office/drawing/2012/chartStyle}ST_MarkerStyle" />
 *       &lt;attribute name="size" type="{http://schemas.microsoft.com/office/drawing/2012/chartStyle}ST_MarkerSize" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_MarkerLayout")
public class CTMarkerLayout {

    @XmlAttribute(name = "symbol")
    protected STMarkerStyle symbol;
    @XmlAttribute(name = "size")
    protected Short size;

    /**
     * Gets the value of the symbol property.
     * 
     * @return
     *     possible object is
     *     {@link STMarkerStyle }
     *     
     */
    public STMarkerStyle getSymbol() {
        return symbol;
    }

    /**
     * Sets the value of the symbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link STMarkerStyle }
     *     
     */
    public void setSymbol(STMarkerStyle value) {
        this.symbol = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSize(Short value) {
        this.size = value;
    }
}
