/**
 *
 * @author sven.jacobiATopen-xchange.com
 */
package org.docx4j.dml;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class CTLockingBase {

	public interface INoRot {

		public boolean isNoRot();

		public void setNoRot(Boolean value);
	}

	public interface INoTextEdit {

	    public boolean isNoTextEdit();

	    public void setNoTextEdit(Boolean value);
	}

	public interface INoEditPoints {

	    public boolean isNoEditPoints();

	    public void setNoEditPoints(Boolean value);
	}

	public interface INoAdjustHandles {

	    public void setNoEditPoints(Boolean value);

	    public boolean isNoAdjustHandles();
	}

	public interface INoChangeArrowheads {

	    public void setNoChangeArrowheads(Boolean value);

	    public boolean isNoChangeShapeType();
	}

	public interface INoChangeShapeType {

	    public boolean isNoChangeShapeType();

	    public void setNoChangeShapeType(Boolean value);
	}

	public interface INoCrop {

	    public boolean isNoCrop();

	    public void setNoCrop(Boolean value);
	}

	public interface INoUngrp {

	    public boolean isNoUngrp();

	    public void setNoUngrp(Boolean value);
	}

	public interface INoDrilldown {

	    public boolean isNoDrilldown();

	    public void setNoDrilldown(Boolean value);
	}

	protected CTOfficeArtExtensionList extLst;
    @XmlAttribute
    protected Boolean noGrp;
    @XmlAttribute
    protected Boolean noSelect;
    @XmlAttribute
    protected Boolean noChangeAspect;
    @XmlAttribute
    protected Boolean noMove;
    @XmlAttribute
    protected Boolean noResize;

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the noGrp property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoGrp() {
        if (noGrp == null) {
            return false;
        }
        return noGrp;
    }

    /**
     * Sets the value of the noGrp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoGrp(Boolean value) {
        this.noGrp = value;
    }

    /**
     * Gets the value of the noSelect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoSelect() {
        if (noSelect == null) {
            return false;
        }
        return noSelect;
    }

    /**
     * Sets the value of the noSelect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoSelect(Boolean value) {
        this.noSelect = value;
    }

    /**
     * Gets the value of the noChangeAspect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoChangeAspect() {
        if (noChangeAspect == null) {
            return false;
        }
        return noChangeAspect;
    }

    /**
     * Sets the value of the noChangeAspect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoChangeAspect(Boolean value) {
        this.noChangeAspect = value;
    }

    /**
     * Gets the value of the noMove property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoMove() {
        if (noMove == null) {
            return false;
        }
        return noMove;
    }

    /**
     * Sets the value of the noMove property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoMove(Boolean value) {
        this.noMove = value;
    }

    /**
     * Gets the value of the noResize property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoResize() {
        if (noResize == null) {
            return false;
        }
        return noResize;
    }

    /**
     * Sets the value of the noResize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoResize(Boolean value) {
        this.noResize = value;
    }
}
