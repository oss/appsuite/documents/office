/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>Java class for CT_GeomGuide complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_GeomGuide">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="name" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_GeomGuideName" />
 *       &lt;attribute name="fmla" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_GeomGuideFormula" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_GeomGuide")
public class CTGeomGuide {

    @XmlAttribute(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String name;
    @XmlTransient
    protected Object fmla;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the fmla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlAttribute(required = true)
    public String getFmla() {
    	if(fmla instanceof String) {
    		return (String)fmla;
    	}
    	final Object[] p = (Object[])fmla;
    	final StringBuilder stringBuf = new StringBuilder();
    	for(int i = 0; i < p.length; i++) {
    		if(i>0) {
    			stringBuf.append(" ");
    		}
    		stringBuf.append(p[i].toString());
    	}
        return stringBuf.toString();
    }

    /**
     * Sets the value of the fmla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFmla(String value) {
        this.fmla = value;
    }

    public Object[] getParsedFmla() {
    	if(fmla instanceof Object[]) {
    		return (Object[])fmla;
    	}
    	// String to Object[]
		final String[] formulaToken = ((String)fmla).split(" ");
		final int tokenLength = formulaToken.length > 4 ? 4 : formulaToken.length;

		final Object[] p = new Object[tokenLength];
		p[0] = formulaToken[0];
		for(int i = 1; i < tokenLength; i++) {
			final String token = formulaToken[i];
			if(!token.isEmpty()) {
				p[i] = CTAdjPoint2D.getLongIfPossible(token);
			}
		}
		fmla = p;
		return (Object[])fmla;
    }

    public void setParsedFmla(Object[] value) {
    	this.fmla = value;
    }
}
