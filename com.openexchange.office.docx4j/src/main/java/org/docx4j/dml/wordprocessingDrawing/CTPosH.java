/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.wordprocessingDrawing;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.wp14.STPercentage;

/**
 * <p>Java class for CT_PosH complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_PosH">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="align" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_AlignH"/>
 *           &lt;element name="posOffset" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_PositionOffset"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="relativeFrom" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_RelFromH" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PosH", propOrder = {
    "align",
    "posOffset",
    "pctPosHOffset"
})
public class CTPosH {

    protected STAlignH align;
    protected Integer posOffset;
    // Child element of: positionH (Horizontal positioning) as specified in [ISO/IEC29500-1:2016] section 20.4.2.10.
    @XmlElement(name = "pctPosHOffset", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
    protected STPercentage pctPosHOffset;
    @XmlAttribute(required = true)
    protected STRelFromH relativeFrom;


    /**
     * Gets the value of the align property.
     *
     * @return
     *     possible object is
     *     {@link STAlignH }
     *
     */
    public STAlignH getAlign() {
        return align;
    }

    /**
     * Sets the value of the align property.
     *
     * @param value
     *     allowed object is
     *     {@link STAlignH }
     *
     */
    public void setAlign(STAlignH value) {
        this.align = value;
    }

    /**
     * Gets the value of the posOffset property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getPosOffset() {
        return posOffset;
    }

    /**
     * Sets the value of the posOffset property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setPosOffset(Integer value) {
        this.posOffset = value;
    }

    /**
     * Gets the value of the relativeFrom property.
     *
     * @return
     *     possible object is
     *     {@link STRelFromH }
     *
     */
    public STRelFromH getRelativeFrom() {
        return relativeFrom;
    }

    /**
     * Sets the value of the relativeFrom property.
     *
     * @param value
     *     allowed object is
     *     {@link STRelFromH }
     *
     */
    public void setRelativeFrom(STRelFromH value) {
        this.relativeFrom = value;
    }

    /**
     * Gets the value of the pctPosHOffset property.
     *
     * @return
     *     possible object is
     *     {@link STPercentage }
     *
     */
    public STPercentage getPctPosHOffset() {
        return pctPosHOffset;
    }

    /**
     * Sets the value of the pctPosHOffset property.
     *
     * @param value
     *     allowed object is
     *     {@link STPercentage }
     *
     */
    public void setPctPosHOffset(STPercentage value) {
        this.pctPosHOffset = value;
    }
}
