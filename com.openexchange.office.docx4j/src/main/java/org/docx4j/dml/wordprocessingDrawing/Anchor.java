/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.dml.wordprocessingDrawing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTPoint2D;
import org.docx4j.mce.AlternateContent;
import org.docx4j.mce.AlternateContent.Fallback;
import org.docx4j.wp14.CTSizeRelH;
import org.docx4j.wp14.CTSizeRelV;
import org.docx4j.wp14.STPositivePercentage;

/**
 * <p>Java class for CT_Anchor complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Anchor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="simplePos" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_Point2D"/>
 *         &lt;element name="positionH" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}CT_PosH"/>
 *         &lt;element name="positionV" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}CT_PosV"/>
 *         &lt;element name="extent" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_PositiveSize2D"/>
 *         &lt;element name="effectExtent" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}CT_EffectExtent" minOccurs="0"/>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}EG_WrapType"/>
 *         &lt;element name="docPr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualDrawingProps"/>
 *         &lt;element name="cNvGraphicFramePr" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_NonVisualGraphicFrameProperties" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.openxmlformats.org/drawingml/2006/main}graphic"/>
 *       &lt;/sequence>
 *       &lt;attribute name="distT" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_WrapDistance" />
 *       &lt;attribute name="distB" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_WrapDistance" />
 *       &lt;attribute name="distL" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_WrapDistance" />
 *       &lt;attribute name="distR" type="{http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing}ST_WrapDistance" />
 *       &lt;attribute name="simplePos" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="relativeHeight" use="required" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="behindDoc" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="locked" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="layoutInCell" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="hidden" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="allowOverlap" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Anchor", propOrder = {
    "alternateContent", // multiple alternate contents are possible at different positions, so propOrder is incorrect for this kind of element, however unmarshalling works without error and we will not marshalling alternate Content back
    "simplePos",
    "positionH",
    "positionV",
    "extent",
    "effectExtent",
    "wrapNone",
    "wrapSquare",
    "wrapTight",
    "wrapThrough",
    "wrapTopAndBottom",
    "docPr",
    "cNvGraphicFramePr",
    "graphic",
    "sizeRelH",
    "sizeRelV"
})
public class Anchor extends GraphicBase {

    @XmlElement(required = true)
    protected CTPoint2D simplePos;
    @XmlElement(required = true)    // AlternateContent possible instead of positionH http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing
    protected CTPosH positionH;
    @XmlElement(required = true)    // AlternateContent possible instead of positionV http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing
    protected CTPosV positionV;
    protected CTWrapNone wrapNone;
    protected CTWrapSquare wrapSquare;
    protected CTWrapTight wrapTight;
    protected CTWrapThrough wrapThrough;
    protected CTWrapTopBottom wrapTopAndBottom;
    @XmlAttribute(name = "simplePos")
    protected Boolean simplePosAttr;
    @XmlAttribute(required = true)
    @XmlSchemaType(name = "unsignedInt")
    protected long relativeHeight;
    @XmlAttribute(required = true)
    protected boolean behindDoc;
    @XmlAttribute(required = true)
    protected boolean locked;
    @XmlAttribute(required = true)
    protected boolean layoutInCell;
    @XmlAttribute
    protected Boolean hidden;
    @XmlAttribute(required = true)
    protected boolean allowOverlap;

    // Optional child element of: anchor as specified in [ISO/IEC29500-1:2016] section 20.4.2.3.
    @XmlElement(name = "sizeRelH", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
    protected CTSizeRelH sizeRelH;
    // Optional child element of: anchor as specified in [ISO/IEC29500-1:2016] section 20.4.2.3.
    @XmlElement(name = "sizeRelV", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
    protected CTSizeRelV sizeRelV;

    /**
     * Gets the value of the simplePos property.
     *
     * @return
     *     possible object is
     *     {@link CTPoint2D }
     *
     */
    public CTPoint2D getSimplePos() {
        return simplePos;
    }

    /**
     * Sets the value of the simplePos property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPoint2D }
     *
     */
    public void setSimplePos(CTPoint2D value) {
        this.simplePos = value;
    }

    /**
     * Gets the value of the positionH property.
     *
     * @return
     *     possible object is
     *     {@link CTPosH }
     *
     */
    public CTPosH getPositionH() {
        return positionH;
    }

    /**
     * Sets the value of the positionH property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPosH }
     *
     */
    public void setPositionH(CTPosH value) {
        this.positionH = value;
    }

    /**
     * Gets the value of the positionV property.
     *
     * @return
     *     possible object is
     *     {@link CTPosV }
     *
     */
    public CTPosV getPositionV() {
        return positionV;
    }

    /**
     * Sets the value of the positionV property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPosV }
     *
     */
    public void setPositionV(CTPosV value) {
        this.positionV = value;
    }

    /**
     * Gets the value of the wrapNone property.
     *
     * @return
     *     possible object is
     *     {@link CTWrapNone }
     *
     */
    public CTWrapNone getWrapNone() {
        return wrapNone;
    }

    /**
     * Sets the value of the wrapNone property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWrapNone }
     *
     */
    public void setWrapNone(CTWrapNone value) {
        this.wrapNone = value;
    }

    /**
     * Gets the value of the wrapSquare property.
     *
     * @return
     *     possible object is
     *     {@link CTWrapSquare }
     *
     */
    public CTWrapSquare getWrapSquare() {
        return wrapSquare;
    }

    /**
     * Sets the value of the wrapSquare property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWrapSquare }
     *
     */
    public void setWrapSquare(CTWrapSquare value) {
        this.wrapSquare = value;
    }

    /**
     * Gets the value of the wrapTight property.
     *
     * @return
     *     possible object is
     *     {@link CTWrapTight }
     *
     */
    public CTWrapTight getWrapTight() {
        return wrapTight;
    }

    /**
     * Sets the value of the wrapTight property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWrapTight }
     *
     */
    public void setWrapTight(CTWrapTight value) {
        this.wrapTight = value;
    }

    /**
     * Gets the value of the wrapThrough property.
     *
     * @return
     *     possible object is
     *     {@link CTWrapThrough }
     *
     */
    public CTWrapThrough getWrapThrough() {
        return wrapThrough;
    }

    /**
     * Sets the value of the wrapThrough property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWrapThrough }
     *
     */
    public void setWrapThrough(CTWrapThrough value) {
        this.wrapThrough = value;
    }

    /**
     * Gets the value of the wrapTopAndBottom property.
     *
     * @return
     *     possible object is
     *     {@link CTWrapTopBottom }
     *
     */
    public CTWrapTopBottom getWrapTopAndBottom() {
        return wrapTopAndBottom;
    }

    /**
     * Sets the value of the wrapTopAndBottom property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWrapTopBottom }
     *
     */
    public void setWrapTopAndBottom(CTWrapTopBottom value) {
        this.wrapTopAndBottom = value;
    }

    /**
     * Gets the value of the simplePosAttr property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isSimplePosAttr() {
        return simplePosAttr;
    }

    /**
     * Sets the value of the simplePosAttr property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setSimplePosAttr(Boolean value) {
        this.simplePosAttr = value;
    }

    /**
     * Gets the value of the relativeHeight property.
     *
     */
    public long getRelativeHeight() {
        return relativeHeight;
    }

    /**
     * Sets the value of the relativeHeight property.
     *
     */
    public void setRelativeHeight(long value) {
        this.relativeHeight = value;
    }

    /**
     * Gets the value of the behindDoc property.
     *
     */
    public boolean isBehindDoc() {
        return behindDoc;
    }

    /**
     * Sets the value of the behindDoc property.
     *
     */
    public void setBehindDoc(boolean value) {
        this.behindDoc = value;
    }

    /**
     * Gets the value of the locked property.
     *
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * Sets the value of the locked property.
     *
     */
    public void setLocked(boolean value) {
        this.locked = value;
    }

    /**
     * Gets the value of the layoutInCell property.
     *
     */
    public boolean isLayoutInCell() {
        return layoutInCell;
    }

    /**
     * Sets the value of the layoutInCell property.
     *
     */
    public void setLayoutInCell(boolean value) {
        this.layoutInCell = value;
    }

    /**
     * Gets the value of the hidden property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isHidden() {
        return hidden;
    }

    /**
     * Sets the value of the hidden property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setHidden(Boolean value) {
        this.hidden = value;
    }

    /**
     * Gets the value of the allowOverlap property.
     *
     */
    public boolean isAllowOverlap() {
        return allowOverlap;
    }

    /**
     * Sets the value of the allowOverlap property.
     *
     */
    public void setAllowOverlap(boolean value) {
        this.allowOverlap = value;
    }

    /**
     * Gets the value of the sizeRelH property.
     *
     */
    public CTSizeRelH getSizeRelH(boolean forceCreate) {
        if(sizeRelH == null && forceCreate) {
            sizeRelH = new CTSizeRelH();
            sizeRelH.setPctWidth(new STPositivePercentage(100000));
        }
        return sizeRelH;
    }

    /**
     * Sets the value of the sizeRelH property.
     *
     */
    public void setSizeRelH(CTSizeRelH value) {
        this.sizeRelH = value;
    }

    /**
     * Gets the value of the sizeRelV property.
     *
     */
    public CTSizeRelV getSizeRelV(boolean forceCreate) {
        if(sizeRelV == null && forceCreate) {
            sizeRelV = new CTSizeRelV();
            sizeRelV.setPctHeight(new STPositivePercentage(100000));
        }
        return sizeRelV;
    }

    /**
     * Sets the value of the sizeRelV property.
     *
     */
    public void setSizeRelV(CTSizeRelV value) {
        this.sizeRelV = value;
    }

    // taking care of relative positions

    @XmlTransient
    private List<AlternateContent> alternateContent = null;
    @XmlElement(name = "AlternateContent", namespace = "http://schemas.openxmlformats.org/markup-compatibility/2006", type = AlternateContent.class)
    public List<AlternateContent> getAlternateContent() {
        if(alternateContent == null) {
            alternateContent = new ArrayList<AlternateContent>();
        }
        return alternateContent;
    }

    public void setAlternateContent(@SuppressWarnings("unused") List<AlternateContent> ac) {
        //
    }

    public void afterUnmarshal(@SuppressWarnings("unused") Unmarshaller unmarshaller, Object parent) {
        setParent(parent);

        // if the file contains alternate content then we will try to match the fallback
        if(alternateContent != null) {
            final Iterator<AlternateContent> iter = alternateContent.iterator();
            while(iter.hasNext()) {
                final Fallback fallback = iter.next().getFallback();
                final List<Object> any = fallback.getAny();
                if(any!=null) {
                    final Iterator<Object> anyIter = any.iterator();
                    while(anyIter.hasNext()) {
                        Object o = anyIter.next();
                        if(o instanceof JAXBElement) {
                            o = ((JAXBElement<?>)o).getValue();
                        }
                        if(o instanceof CTPosH) {
                            positionH = (CTPosH)o;
                        }
                        else if (o instanceof CTPosV) {
                            positionV = (CTPosV)o;
                        }
                    }
                }
            }
            alternateContent = null;
        }
    }
}
