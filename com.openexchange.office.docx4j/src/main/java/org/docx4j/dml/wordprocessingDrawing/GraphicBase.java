/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.docx4j.dml.wordprocessingDrawing;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import org.docx4j.dml.CTNonVisualDrawingProps;
import org.docx4j.dml.CTNonVisualGraphicFrameProperties;
import org.docx4j.dml.CTPositiveSize2D;
import org.docx4j.dml.Graphic;
import org.docx4j.dml.IGraphicalObjectFrame;
import com.openexchange.office.filter.core.component.Child;

@XmlTransient
public abstract class GraphicBase implements IGraphicalObjectFrame, Child {

    @XmlElement(required = true)
    protected CTPositiveSize2D extent;
    protected CTEffectExtent effectExtent;
    @XmlElement(required = true)
    protected CTNonVisualDrawingProps docPr;
    protected CTNonVisualGraphicFrameProperties cNvGraphicFramePr;
    @XmlElement(namespace = "http://schemas.openxmlformats.org/drawingml/2006/main", required = true)
    protected Graphic graphic;
    @XmlAttribute
    protected Long distT;
    @XmlAttribute
    protected Long distB;
    @XmlAttribute
    protected Long distL;
    @XmlAttribute
    protected Long distR;
    @XmlTransient
    private Object parent;

    /**
     * Gets the value of the extent property.
     *
     * @return
     *     possible object is
     *     {@link CTPositiveSize2D }
     *
     */
    public CTPositiveSize2D getExtent() {
        return extent;
    }

    /**
     * Sets the value of the extent property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPositiveSize2D }
     *
     */
    public void setExtent(CTPositiveSize2D value) {
        this.extent = value;
    }

    /**
     * Gets the value of the effectExtent property.
     *
     * @return
     *     possible object is
     *     {@link CTEffectExtent }
     *
     */
    public CTEffectExtent getEffectExtent(boolean forceCreate) {
        if(effectExtent==null&&forceCreate) {
            effectExtent = new CTEffectExtent();
        }
        return effectExtent;
    }

    /**
     * Sets the value of the effectExtent property.
     *
     * @param value
     *     allowed object is
     *     {@link CTEffectExtent }
     *
     */
    public void setEffectExtent(CTEffectExtent value) {
        this.effectExtent = value;
    }

    /**
     * Gets the value of the docPr property.
     *
     * @return
     *     possible object is
     *     {@link CTNonVisualDrawingProps }
     *
     */
    @Override
    public CTNonVisualDrawingProps getNonVisualDrawingProperties(boolean createIfMissing) {
    	if(docPr==null&&createIfMissing) {
    		docPr = new CTNonVisualDrawingProps();
    	}
    	return docPr;
    }

    public void setNonVisualDrawingProperties(CTNonVisualDrawingProps props) {
    	docPr = props;
    }

    /**
     * Gets the value of the cNvGraphicFramePr property.
     *
     * @return
     *     possible object is
     *     {@link CTNonVisualGraphicFrameProperties }
     *
     */
    @Override
    public CTNonVisualGraphicFrameProperties getNonVisualDrawingShapeProperties(boolean createIfMissing) {
    	if(cNvGraphicFramePr==null&&createIfMissing) {
    		cNvGraphicFramePr = new CTNonVisualGraphicFrameProperties();
    	}
    	return cNvGraphicFramePr;
    }

    public void setNonVisualDrawingShapeProperties(CTNonVisualGraphicFrameProperties props) {
    	cNvGraphicFramePr = props;
    }

    /**
     * Gets the value of the graphic property.
     *
     * @return
     *     possible object is
     *     {@link Graphic }
     *
     */
    @Override
    public Graphic getGraphic() {
        return graphic;
    }

    /**
     * Sets the value of the graphic property.
     *
     * @param value
     *     allowed object is
     *     {@link Graphic }
     *
     */
    @Override
    public void setGraphic(Graphic value) {
        this.graphic = value;
    }

    /**
     * Gets the value of the distT property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getDistT() {
        return distT;
    }

    /**
     * Sets the value of the distT property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setDistT(Long value) {
        this.distT = value;
    }

    /**
     * Gets the value of the distB property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getDistB() {
        return distB;
    }

    /**
     * Sets the value of the distB property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setDistB(Long value) {
        this.distB = value;
    }

    /**
     * Gets the value of the distL property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getDistL() {
        return distL;
    }

    /**
     * Sets the value of the distL property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setDistL(Long value) {
        this.distL = value;
    }

    /**
     * Gets the value of the distR property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getDistR() {
        return distR;
    }

    /**
     * Sets the value of the distR property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setDistR(Long value) {
        this.distR = value;
    }

    /**
     * Gets the parent object in the object tree representing the unmarshalled xml document.
     *
     * @return
     *     The parent object.
     */
    @Override
    public Object getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Object parent) {
        this.parent = parent;
    }
}
