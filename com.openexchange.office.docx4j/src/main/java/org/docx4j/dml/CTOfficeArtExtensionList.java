/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.dml;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import org.docx4j.jaxb.Context;
import org.w3c.dom.Element;

/**
 * <p>Java class for CT_OfficeArtExtensionList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_OfficeArtExtensionList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://schemas.openxmlformats.org/drawingml/2006/main}EG_OfficeArtExtensionList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_OfficeArtExtensionList", propOrder = {
    "ext"
})
public class CTOfficeArtExtensionList {

    protected List<CTOfficeArtExtension> ext;

    /**
     * Gets the value of the ext property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ext property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTOfficeArtExtension }
     * 
     * 
     */
    public List<CTOfficeArtExtension> getExt() {
        if (ext == null) {
            ext = new ArrayList<CTOfficeArtExtension>();
        }
        return this.ext;
    }

    public Object getExtensionByUriAndQName(String uri, QName qName) throws JAXBException {
        for(CTOfficeArtExtension extension:getExt()) {
            if(extension.getUri().equals(uri)) {
                Object any = extension.getAny();
                if(any instanceof Element) {
                    final Element ele = (Element)any;
                    if(qName.getNamespaceURI().equals(ele.getNamespaceURI()) && qName.getLocalPart().equals(ele.getLocalName())) {
                        any = Context.getJc().createUnmarshaller().unmarshal(ele);
                    }
                }
                if(any instanceof JAXBElement) {
                    any = ((JAXBElement)any).getValue();
                }
                return any;
            }
        }
        return null;
    }
}
