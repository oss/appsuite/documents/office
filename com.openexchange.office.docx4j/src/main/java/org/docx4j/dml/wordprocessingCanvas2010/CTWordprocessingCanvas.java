
package org.docx4j.dml.wordprocessingCanvas2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElements;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTBackgroundFormatting;
import org.docx4j.dml.CTOfficeArtExtensionList;
import org.docx4j.dml.CTWholeE2OFormatting;
import org.docx4j.dml.picture.Pic;
import org.docx4j.dml.wordprocessingGroup2010.CTGraphicFrame;
import org.docx4j.dml.wordprocessingGroup2010.CTWordprocessingGroup;
import org.docx4j.dml.wordprocessingShape2010.CTWordprocessingShape;
import org.docx4j.w14.CTWordContentPart;
import com.openexchange.office.filter.core.IContentAccessor;
import com.openexchange.office.filter.core.DLList;


/**
 * <p>Java class for CT_WordprocessingCanvas complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_WordprocessingCanvas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bg" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_BackgroundFormatting" minOccurs="0"/>
 *         &lt;element name="whole" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_WholeE2oFormatting" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordprocessingShape}wsp"/>
 *           &lt;element ref="{http://schemas.openxmlformats.org/drawingml/2006/picture}pic"/>
 *           &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordml}contentPart"/>
 *           &lt;element ref="{http://schemas.microsoft.com/office/word/2010/wordprocessingGroup}wgp"/>
 *           &lt;element name="graphicFrame" type="{http://schemas.microsoft.com/office/word/2010/wordprocessingGroup}CT_GraphicFrame"/>
 *         &lt;/choice>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name="wpc")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_WordprocessingCanvas", propOrder = {
    "bg",
    "whole",
    "wspOrPicOrContentPart",
    "extLst"
})
public class CTWordprocessingCanvas implements IContentAccessor<Object> {

    protected CTBackgroundFormatting bg;
    protected CTWholeE2OFormatting whole;
    @XmlElements({
        @XmlElement(name = "wsp", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingShape", type = CTWordprocessingShape.class),
        @XmlElement(name = "pic", namespace = "http://schemas.openxmlformats.org/drawingml/2006/picture", type = Pic.class),
        @XmlElement(name = "contentPart", namespace = "http://schemas.microsoft.com/office/word/2010/wordml", type = CTWordContentPart.class),
        @XmlElement(name = "wgp", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup", type = CTWordprocessingGroup.class),
        @XmlElement(name = "graphicFrame", type = CTGraphicFrame.class)
    })
    protected DLList<Object> wspOrPicOrContentPart;
    protected CTOfficeArtExtensionList extLst;

    /**
     * Gets the value of the bg property.
     *
     * @return
     *     possible object is
     *     {@link CTBackgroundFormatting }
     *
     */
    public CTBackgroundFormatting getBg() {
        return bg;
    }

    /**
     * Sets the value of the bg property.
     *
     * @param value
     *     allowed object is
     *     {@link CTBackgroundFormatting }
     *
     */
    public void setBg(CTBackgroundFormatting value) {
        this.bg = value;
    }

    /**
     * Gets the value of the whole property.
     *
     * @return
     *     possible object is
     *     {@link CTWholeE2OFormatting }
     *
     */
    public CTWholeE2OFormatting getWhole() {
        return whole;
    }

    /**
     * Sets the value of the whole property.
     *
     * @param value
     *     allowed object is
     *     {@link CTWholeE2OFormatting }
     *
     */
    public void setWhole(CTWholeE2OFormatting value) {
        this.whole = value;
    }

    /**
     * Gets the value of the wspOrPicOrContentPart property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wspOrPicOrContentPart property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWspOrPicOrContentPart().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTWordprocessingShape }
     * {@link Pic }
     * {@link CTWordContentPart }
     * {@link CTWordprocessingGroup }
     * {@link CTGraphicFrame }
     *
     *
     */
    @Override
    public DLList<Object> getContent() {
        if (wspOrPicOrContentPart == null) {
            wspOrPicOrContentPart = new DLList<Object>();
        }
        return this.wspOrPicOrContentPart;
    }

    /**
     * Gets the value of the extLst property.
     *
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     *
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }
}
