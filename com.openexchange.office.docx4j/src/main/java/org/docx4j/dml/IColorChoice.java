
package org.docx4j.dml;

public interface IColorChoice {

    /**
     * Gets the value of the scrgbClr property.
     *
     * @return
     *     possible object is
     *     {@link CTScRgbColor }
     *
     */
    public CTScRgbColor getScrgbClr();

    /**
     * Sets the value of the scrgbClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTScRgbColor }
     *
     */
    public void setScrgbClr(CTScRgbColor value);

    /**
     * Gets the value of the srgbClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSRgbColor }
     *
     */
    public CTSRgbColor getSrgbClr();

    /**
     * Sets the value of the srgbClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSRgbColor }
     *
     */
    public void setSrgbClr(CTSRgbColor value);

    /**
     * Gets the value of the hslClr property.
     *
     * @return
     *     possible object is
     *     {@link CTHslColor }
     *
     */
    public CTHslColor getHslClr();

    /**
     * Sets the value of the hslClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTHslColor }
     *
     */
    public void setHslClr(CTHslColor value);

    /**
     * Gets the value of the sysClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSystemColor }
     *
     */
    public CTSystemColor getSysClr();

    /**
     * Sets the value of the sysClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSystemColor }
     *
     */
    public void setSysClr(CTSystemColor value);

    /**
     * Gets the value of the schemeClr property.
     *
     * @return
     *     possible object is
     *     {@link CTSchemeColor }
     *
     */
    public CTSchemeColor getSchemeClr();

    /**
     * Sets the value of the schemeClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTSchemeColor }
     *
     */
    public void setSchemeClr(CTSchemeColor value);

    /**
     * Gets the value of the prstClr property.
     *
     * @return
     *     possible object is
     *     {@link CTPresetColor }
     *
     */
    public CTPresetColor getPrstClr();

    /**
     * Sets the value of the prstClr property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPresetColor }
     *
     */
    public void setPrstClr(CTPresetColor value);
}
