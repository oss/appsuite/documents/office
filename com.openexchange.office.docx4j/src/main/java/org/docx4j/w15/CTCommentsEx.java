/*
 *  Copyright 2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.w15;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.DocumentSerialization;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;

/**
 * <p>Java class for CT_CommentsEx complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_CommentsEx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commentEx" type="{http://schemas.microsoft.com/office/word/2012/wordml}CT_CommentEx" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentsEx", propOrder = {
    "commentEx"
})
public class CTCommentsEx extends DocumentSerialization implements INodeAccessor<CTCommentEx>
{
/*
    static final public String[] CommentEx_Namespaces = {
        "w15",  "http://schemas.microsoft.com/office/word/2012/wordml"
    };
*/

    public CTCommentsEx() {
        super(new QName("http://schemas.microsoft.com/office/word/2012/wordml", "commentsEx", "w15"), DocumentSerialization.Standard_DOCX_Namespaces, "w15");
    }
    protected DLList<CTCommentEx> commentEx;

    /**
     * Gets the value of the commentEx property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentEx property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentEx().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTCommentEx }
     *
     *
     */
    @Override
    public DLList<CTCommentEx> getContent() {
        if (commentEx == null) {
            commentEx = new DLList<CTCommentEx>();
        }
        return this.commentEx;
    }

    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.readObject(reader, documentPart);

        boolean advanceEvent = true;

        while(reader.hasNext()) {
            final int event;
            if(advanceEvent) {
                event = reader.next();
            }
            else {
                event = reader.getEventType();
                advanceEvent = true;
            }
            if(event==XMLStreamReader.START_ELEMENT) {
                final String localName = reader.getLocalName();
                if("commentEx".equals(localName)) {
                    final Object result = documentPart.getUnmarshaller().unmarshal(reader);
                    advanceEvent = reader.getEventType()==XMLStreamReader.END_ELEMENT;
                    if(result instanceof JAXBElement) {
                        getContent().add(((JAXBElement<CTCommentEx>)result).getValue());
                    }
                    else if(result instanceof CTCommentEx) {
                        getContent().add((CTCommentEx)result);
                    }
                }
            }
        }
    }

    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {
        super.writeObject(writer, documentPart);

        final Marshaller marshaller = documentPart.getFragmentMarshaller(prefixToUri, uriToPrefix);
        if(commentEx!=null) {
            DLNode<CTCommentEx> commentIdIter = commentEx.getFirstNode();
            while(commentIdIter!=null) {
                marshaller.marshal(new JAXBElement<CTCommentEx>(new QName("http://schemas.microsoft.com/office/word/2012/wordml", "commentEx"), CTCommentEx.class, commentIdIter.getData()), writer);
                commentIdIter = commentIdIter.getNext();
            }
        }
        writer.writeEndElement();
        writer.writeEndDocument();
    }
}
