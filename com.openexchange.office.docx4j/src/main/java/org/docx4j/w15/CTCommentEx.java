/*
 *  Copyright 2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.w15;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.docx4j.adapter.HexNumberIntegerAdapter;

/**
 * <p>Java class for CT_CommentEx complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_CommentEx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="paraId" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="paraIdParent" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_LongHexNumber" />
 *       &lt;attribute name="done" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_OnOff" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CommentEx")
@XmlRootElement(name="commentEx")
public class CTCommentEx
{
    @XmlAttribute(name = "paraId", namespace = "http://schemas.microsoft.com/office/word/2012/wordml", required = true)
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    protected Integer paraId;
    @XmlAttribute(name = "paraIdParent", namespace = "http://schemas.microsoft.com/office/word/2012/wordml")
    @XmlJavaTypeAdapter(HexNumberIntegerAdapter.class)
    protected Integer paraIdParent;
    @XmlAttribute(name = "done", namespace = "http://schemas.microsoft.com/office/word/2012/wordml")
    protected String done;

    /**
     * Gets the value of the paraId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getParaId() {
        return paraId;
    }

    /**
     * Sets the value of the paraId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParaId(Integer value) {
        this.paraId = value;
    }

    /**
     * Gets the value of the paraIdParent property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getParaIdParent() {
        return paraIdParent;
    }

    /**
     * Sets the value of the paraIdParent property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setParaIdParent(Integer value) {
        this.paraIdParent = value;
    }

    /**
     * Gets the value of the done property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDone() {
        return done;
    }

    /**
     * Sets the value of the done property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDone(String value) {
        this.done = value;
    }
}
