/*
 *  Copyright 2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.w15;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;



/**
 * <p>Java class for CT_Person complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CT_Person">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="presenceInfo" type="{http://schemas.microsoft.com/office/word/2012/wordml}CT_PresenceInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="author" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *       &lt;attribute name="contact" use="required" type="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}ST_String" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Person", propOrder = {
    "presenceInfo"
})
public class CTPerson {

    protected CTPresenceInfo presenceInfo;
    @XmlAttribute(name = "author", namespace = "http://schemas.microsoft.com/office/word/2012/wordml", required = true)
    protected String author;
    @XmlAttribute(name = "contact", namespace = "http://schemas.microsoft.com/office/word/2012/wordml", required = true)
    protected String contact;

    /**
     * Gets the value of the presenceInfo property.
     *
     * @return
     *     possible object is
     *     {@link CTPresenceInfo }
     *
     */
    public CTPresenceInfo getPresenceInfo(boolean forceCreate) {
        if(presenceInfo == null && forceCreate) {
            presenceInfo = new CTPresenceInfo();
        }
        return presenceInfo;
    }

    /**
     * Sets the value of the presenceInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPresenceInfo }
     *
     */
    public void setPresenceInfo(CTPresenceInfo value) {
        this.presenceInfo = value;
    }

    /**
     * Gets the value of the author property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAuthor(String value) {
        this.author = value;
    }

    /**
     * Gets the value of the contact property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setContact(String value) {
        this.contact = value;
    }
}
