/*
 *  Copyright 2013, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.wp14;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;

/**
  <xsd:simpleType name="ST_SizeRelFromH">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="margin"/>
      <xsd:enumeration value="page"/>
      <xsd:enumeration value="leftMargin"/>
      <xsd:enumeration value="rightMargin"/>
      <xsd:enumeration value="insideMargin"/>
      <xsd:enumeration value="outsideMargin"/>
    </xsd:restriction>
  </xsd:simpleType>
 */

@XmlType(name = "ST_SizeRelFromH", namespace="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
@XmlEnum
public enum STSizeRelFromH {

    @XmlEnumValue("margin")
    MARGIN("margin"),
    @XmlEnumValue("page")
    PAGE("page"),
    @XmlEnumValue("leftMargin")
    LEFT_MARGIN("leftMargin"),
    @XmlEnumValue("rightMargin")
    RIGHT_MARGIN("rightMargin"),
    @XmlEnumValue("insideMargin")
    INSIDE_MARGIN("insideMargin"),
    @XmlEnumValue("outsideMargin")
    OUTSIDE_MARGIN("outsideMargin");

    private final String value;

    STSizeRelFromH(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSizeRelFromH fromValue(String v) {
        for (STSizeRelFromH c: STSizeRelFromH.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
