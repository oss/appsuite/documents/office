
package org.docx4j.wp14;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ST_PositivePercentage")
public class STPositivePercentage {

    public STPositivePercentage() {
        // default constr. required for jaxb.
    }

    public STPositivePercentage(Integer value) {
        this.value = value;
    }

    @XmlValue
    protected Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value  = value;
    }
}
