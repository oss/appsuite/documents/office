
package org.docx4j.wp14;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
  <xsd:simpleType name="ST_SizeRelFromV">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="margin"/>
      <xsd:enumeration value="page"/>
      <xsd:enumeration value="topMargin"/>
      <xsd:enumeration value="bottomMargin"/>
      <xsd:enumeration value="insideMargin"/>
      <xsd:enumeration value="outsideMargin"/>
    </xsd:restriction>
  </xsd:simpleType>
 */

@XmlType(name = "ST_SizeRelFromV", namespace="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
@XmlEnum
public enum STSizeRelFromV {

    @XmlEnumValue("margin")
    MARGIN("margin"),
    @XmlEnumValue("page")
    PAGE("page"),
    @XmlEnumValue("topMargin")
    TOP_MARGIN("topMargin"),
    @XmlEnumValue("bottomMargin")
    BOTTOM_MARGIN("bottomMargin"),
    @XmlEnumValue("insideMargin")
    INSIDE_MARGIN("insideMargin"),
    @XmlEnumValue("outsideMargin")
    OUTSIDE_MARGIN("outsideMargin");

    private final String value;

    STSizeRelFromV(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static STSizeRelFromV fromValue(String v) {
        for (STSizeRelFromV c: STSizeRelFromV.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
