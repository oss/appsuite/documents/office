
package org.docx4j.wp14;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.docx4j.dml.wordprocessingDrawing.CTPosH;
import org.docx4j.dml.wordprocessingDrawing.CTPosV;

@XmlRegistry
public class ObjectFactory {

    private final static QName _SizeRelH_QNAME  = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "sizeRelH");
    private final static QName _SizeRelV_QNAME  = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "sizeRelV");
    private final static QName _PctHeight_QNAME = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "pctHeight");
    private final static QName _PctWidth_QNAME  = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "pctWidth");
    private final static QName _PctPosHOffset_QNAME = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "pctPosHOffset");
    private final static QName _PctPosVOffset_QNAME  = new QName("http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", "pctPosVOffset");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.docx4j.wp14
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTSizeRelH }
     *
     */
    public CTSizeRelH createSizeRelH() {
        return new CTSizeRelH();
    }

    /**
     * Create an instance of {@link CTSizeRelV }
     *
     */
    public CTSizeRelV createSizeRelV() {
        return new CTSizeRelV();
    }

    /**
     * Create an instance of {@link STPositivePercentage }
     *
     */
    public STPositivePercentage createPctHeight() {
        return new STPositivePercentage();
    }

    /**
     * Create an instance of {@link STPositivePercentage }
     *
     */
    public STPositivePercentage createPctWidth() {
        return new STPositivePercentage();
    }

    /**
     * Create an instance of {@link STPercentage }
     *
     */
    public STPercentage createPctPosHOffset() {
        return new STPercentage();
    }

    /**
     * Create an instance of {@link STPositivePercentage }
     *
     */
    public STPercentage createpctPosVOffset() {
        return new STPercentage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSizeRelH }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "sizeRelH")
    public JAXBElement<CTSizeRelH> createSizeRelH(CTSizeRelH value) {
        return new JAXBElement<CTSizeRelH>(_SizeRelH_QNAME, CTSizeRelH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTSizeRelV }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "sizeRelV")
    public JAXBElement<CTSizeRelV> createSizeRelV(CTSizeRelV value) {
        return new JAXBElement<CTSizeRelV>(_SizeRelV_QNAME, CTSizeRelV.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link STPositivePercentage }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "pctHeight")
    public JAXBElement<STPositivePercentage> createPctHeight(STPositivePercentage value) {
        return new JAXBElement<STPositivePercentage>(_PctHeight_QNAME, STPositivePercentage.class, CTSizeRelH.class, value);
    }
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link STPositivePercentage }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "pctWidth")
    public JAXBElement<STPositivePercentage> createPctWidth(STPositivePercentage value) {
        return new JAXBElement<STPositivePercentage>(_PctWidth_QNAME, STPositivePercentage.class, CTSizeRelV.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link STPercentage }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "pctPosHOffset", scope = CTPosH.class)
    public JAXBElement<STPercentage> createPctPosHOffset(STPercentage value) {
        return new JAXBElement<STPercentage>(_PctPosHOffset_QNAME, STPercentage.class, CTPosH.class, value);
    }
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link STPercentage }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing", name = "pctPosVOffset", scope = CTPosV.class)
    public JAXBElement<STPercentage> createPctPosVOffset(STPercentage value) {
        return new JAXBElement<STPercentage>(_PctPosVOffset_QNAME, STPercentage.class, CTPosV.class, value);
    }
}
