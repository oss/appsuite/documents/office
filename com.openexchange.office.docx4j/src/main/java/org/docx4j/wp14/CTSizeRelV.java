
package org.docx4j.wp14;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTPositivePercentage;

/**
  <xsd:complexType name="CT_SizeRelV">
    <xsd:sequence>
      <xsd:element name="pctHeight" type="a:ST_PositivePercentage" minOccurs="1" maxOccurs="1"/>
    </xsd:sequence>
    <xsd:attribute name="relativeFrom" type="ST_SizeRelFromV" use="required"/>
  </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SizeRelV", propOrder = {
    "pctHeight"
})
public class CTSizeRelV {

    @XmlElement(name = "pctHeight", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
    protected STPositivePercentage pctHeight;
    @XmlAttribute(name = "relativeFrom", required = true)
    protected STSizeRelFromV relativeFrom;

    /**
     * Gets the value of the pctHeight property.
     *
     * @return
     *     possible object is
     *     {@link CTPositivePercentage }
     *
     */
    public STPositivePercentage getPctHeight() {
        return pctHeight;
    }

    /**
     * Sets the value of the pctHeight property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPositivePercentage }
     *
     */
    public void setPctHeight(STPositivePercentage value) {
        this.pctHeight = value;
    }

    /**
     * Gets the value of the relativeFrom property.
     *
     * @return
     *     possible object is
     *     {@link STSizeRelFromV }
     *
     */
    public STSizeRelFromV getRelativeFrom() {
        return relativeFrom;
    }

    /**
     * Sets the value of the relativeFrom property.
     *
     * @param value
     *     allowed object is
     *     {@link STSizeRelFromV }
     *
     */
    public void setRelativeFrom(STSizeRelFromV value) {
        this.relativeFrom = value;
    }
}
