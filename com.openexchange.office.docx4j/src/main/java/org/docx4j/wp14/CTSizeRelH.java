
package org.docx4j.wp14;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTPositivePercentage;

/**
  <xsd:complexType name="CT_SizeRelH">
    <xsd:sequence>
      <xsd:element name="pctWidth" type="a:ST_PositivePercentage" minOccurs="1" maxOccurs="1"/>
    </xsd:sequence>
    <xsd:attribute name="relativeFrom" type="ST_SizeRelFromH" use="required"/>
  </xsd:complexType>
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_SizeRelH", propOrder = {
    "pctWidth"
})
public class CTSizeRelH {

    @XmlElement(name = "pctWidth", namespace = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing")
    protected STPositivePercentage pctWidth;
    @XmlAttribute(name = "relativeFrom", required = true)
    protected STSizeRelFromH relativeFrom;

    /**
     * Gets the value of the pctWidth property.
     *
     * @return
     *     possible object is
     *     {@link CTPositivePercentage }
     *
     */
    public STPositivePercentage getPctWidth() {
        return pctWidth;
    }

    /**
     * Sets the value of the pctWidth property.
     *
     * @param value
     *     allowed object is
     *     {@link CTPositivePercentage }
     *
     */
    public void setPctWidth(STPositivePercentage value) {
        this.pctWidth = value;
    }

    /**
     * Gets the value of the relativeFrom property.
     *
     * @return
     *     possible object is
     *     {@link STSizeRelFromH }
     *
     */
    public STSizeRelFromH getRelativeFrom() {
        return relativeFrom;
    }

    /**
     * Sets the value of the relativeFrom property.
     *
     * @param value
     *     allowed object is
     *     {@link STSizeRelFromH }
     *
     */
    public void setRelativeFrom(STSizeRelFromH value) {
        this.relativeFrom = value;
    }
}
