/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.docx4j.drawing2014;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
 
    private final static QName _CREATION_ID_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2014/main", "creationId");
    private final static QName _PRED_REF_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2014/main", "predDERef");
    private final static QName _CXN_REF_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2014/main", "cxnDERefs");
    private final static QName _ROW_ID_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2014/main", "rowId");
    private final static QName _COL_ID_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2014/main", "colId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.docx4j.drawing2014
     * 
     */
    public ObjectFactory() {
        //
    }

    /**
     * Create an instance of {@link CTCreationId }
     * 
     */
    public CTCreationId createCTCreationId() {
        return new CTCreationId();
    }

    /**
     * Create an instance of {@link CTPredecessorDrawingElementReference }
     * 
     */
    public CTPredecessorDrawingElementReference createCTPredecessorDrawingElementReference() {
        return new CTPredecessorDrawingElementReference();
    }

    /**
     * Create an instance of {@link CTConnectableReferences }
     * 
     */
    public CTConnectableReferences createCTConnectableReferences() {
        return new CTConnectableReferences();
    }

    /**
     * Create an instance of {@link CTIdentifier }
     * 
     */
    public CTIdentifier createCTIdentifier() {
        return new CTIdentifier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTCreationId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2014/main", name = "creationId")
    public JAXBElement<CTCreationId> createCreationId(CTCreationId value) {
        return new JAXBElement<CTCreationId>(_CREATION_ID_QNAME, CTCreationId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTPredecessorDrawingElementReference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2014/main", name = "predDERef")
    public JAXBElement<CTPredecessorDrawingElementReference> createPredecessorDrawingElementReference(CTPredecessorDrawingElementReference value) {
        return new JAXBElement<CTPredecessorDrawingElementReference>(_PRED_REF_QNAME, CTPredecessorDrawingElementReference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTConnectableReferences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2014/main", name = "cxnDERefs")
    public JAXBElement<CTConnectableReferences> createConnectableReferences(CTConnectableReferences value) {
        return new JAXBElement<CTConnectableReferences>(_CXN_REF_QNAME, CTConnectableReferences.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2014/main", name = "rowId")
    public JAXBElement<CTIdentifier> createRowId(CTIdentifier value) {
        return new JAXBElement<CTIdentifier>(_ROW_ID_QNAME, CTIdentifier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2014/main", name = "colId")
    public JAXBElement<CTIdentifier> createColId(CTIdentifier value) {
        return new JAXBElement<CTIdentifier>(_COL_ID_QNAME, CTIdentifier.class, null, value);
    }
}
