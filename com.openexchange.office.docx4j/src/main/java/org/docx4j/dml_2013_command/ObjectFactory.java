
package org.docx4j.dml_2013_command;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

    private final static QName _DEMKLST_QNAME = new QName("http://schemas.microsoft.com/office/drawing/2013/main/command", "deMkLst");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.pptx4j.pml_2013;
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CTDrawingElementMonikerList }
     *
     */
    public CTDrawingElementMonikerList createCTDrawingElementMonikerList() {
        return new CTDrawingElementMonikerList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CTDrawingElementMonikerList }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/office/drawing/2013/main/command", name = "deMkLst")
    public JAXBElement<CTDrawingElementMonikerList> createCTSmartTagRunMoveFromRangeStart(CTDrawingElementMonikerList value) {
        return new JAXBElement<CTDrawingElementMonikerList>(_DEMKLST_QNAME, CTDrawingElementMonikerList.class, null, value);
    }
}
