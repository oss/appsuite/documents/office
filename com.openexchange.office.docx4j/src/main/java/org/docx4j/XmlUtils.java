/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.util.JAXBResult;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.JaxbValidationEventHandler;
import org.docx4j.jaxb.NamespacePrefixMapperUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XmlUtils {

    private static Logger log = LoggerFactory.getLogger(XmlUtils.class);

    public static Object findElement(List<Object> jaxbElements, Class<?> c, String elementName) {
        for (int i=0; i<jaxbElements.size(); i++) {
            final Object o = jaxbElements.get(i);
            if(o.getClass()==c) {
                return c;
            }
            else if(o instanceof JAXBElement) {
                if(elementName.equals(((JAXBElement<?>)o).getName().getLocalPart())) {
                    return ((JAXBElement<?>)o).getValue();
                }
            }
        }
        return null;
    }

    public static void removeElement(List<Object> jaxbElements, Class<?> c, String elementName) {
        for(int i=0; i<jaxbElements.size(); i++) {
            final Object o = jaxbElements.get(i);
            if(o.getClass()==c) {
                jaxbElements.remove(i);
            }
            else if(o instanceof JAXBElement) {
                if(elementName.equals(((JAXBElement<?>)o).getName().getLocalPart())) {
                    jaxbElements.remove(i);
                }
            }
        }
    }

    public static XMLReader getXMLReader()
        throws SAXException, ParserConfigurationException {

        SAXParserFactory saxFactory = new org.apache.xerces.jaxp.SAXParserFactoryImpl();
        saxFactory.setNamespaceAware(true);
        saxFactory.setValidating(false);
        saxFactory.setXIncludeAware(false);
        saxFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        // removing potential vulnerability: see https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Processing
        saxFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        saxFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        saxFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

        SAXParser parser = saxFactory.newSAXParser();
        XMLReader xmlReader = parser.getXMLReader();
        // More details at http://xerces.apache.org/xerces2-j/features.html#namespaces
        xmlReader.setFeature("http://xml.org/sax/features/namespaces", true);
        // More details at http://xerces.apache.org/xerces2-j/features.html#namespace-prefixes
        xmlReader.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
        // More details at http://xerces.apache.org/xerces2-j/features.html#xmlns-uris
        xmlReader.setFeature("http://xml.org/sax/features/xmlns-uris", true);
        // removing potential vulnerability: see https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Processing
        xmlReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        xmlReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        xmlReader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        return xmlReader;
    }

    public static String TRANSFORMER_FACTORY_PROCESSOR_XALAN = "org.apache.xalan.processor.TransformerFactoryImpl";
    // TRANSFORMER_FACTORY_PROCESSOR_SUN .. JDK/JRE does not include anything like com.sun.org.apache.xalan.TransformerFactoryImpl

//    public static String TRANSFORMER_FACTORY_SAXON = "net.sf.saxon.TransformerFactoryImpl";

    // *.xsltc.trax.TransformerImpl don't
    // work with our extension functions in their current form.
    //public static String TRANSFORMER_FACTORY_XSLTC_XALAN = "org.apache.xalan.xsltc.trax.TransformerFactoryImpl";
    //public static String TRANSFORMER_FACTORY_XSLTC_SUN = "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl";

    private static javax.xml.transform.TransformerFactory transformerFactory;
    /**
     * @since 2.8.1
     */
    public static TransformerFactory getTransformerFactory() {
        return transformerFactory;
    }

    private static DocumentBuilderFactory documentBuilderFactory;
    /**
     * @since 2.8.1
     *
     * TODO replace the various DocumentBuilderFactory.newInstance()
     * throughout docx4j with a call to this.
     */
    public static DocumentBuilderFactory getDocumentBuilderFactory() {
        return documentBuilderFactory;
    }

    static {
        // JAXP factories

        // javax.xml.transform.TransformerFactory
        instantiateTransformerFactory();

        log.debug( System.getProperty("java.vendor") );
        log.debug( System.getProperty("java.version") );

        // javax.xml.parsers.SAXParserFactory
        String sp = Docx4jProperties.getProperty("javax.xml.parsers.SAXParserFactory");
        if (sp!=null) {

            System.setProperty("javax.xml.parsers.SAXParserFactory",sp);
            log.info("Using " + sp + " (from docx4j.properties)");

        } else if (Docx4jProperties.getProperty("docx4j.javax.xml.parsers.SAXParserFactory.donotset", false)) {

            // Since 3.0.0
            // Don't set docx4j.javax.xml.parsers.SAXParserFactory
            log.info("Not setting docx4j.javax.xml.parsers.SAXParserFactory");

        } else if ((System.getProperty("java.version").startsWith("1.6")
                        && System.getProperty("java.vendor").startsWith("Sun"))
                || (System.getProperty("java.version").startsWith("1.7")
                        && System.getProperty("java.vendor").startsWith("Oracle"))
                || (System.getProperty("java.version").startsWith("1.7")
                        && System.getProperty("java.vendor").startsWith("Jeroen")) // IKVM
                ) {

            // Crimson fails to parse the HTML XSLT, so use Xerces ..
            // .. this one is available in Java 6.
            // System.out.println(System.getProperty("java.vendor"));
            // System.out.println(System.getProperty("java.version"));

            System.setProperty("javax.xml.parsers.SAXParserFactory",
                    "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");

            log.info("Using com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");

        } else {

            // In this case suggest you add and use Xerces
            //        System.setProperty("javax.xml.parsers.SAXParserFactory",
            //                "org.apache.xerces.jaxp.SAXParserFactoryImpl");


            log.info("default SAXParserFactory property : " + System.getProperty("javax.xml.parsers.SAXParserFactory" ));
        }
        // Note that we don't restore the value to its original setting (unlike TransformerFactory),
        // since we want to avoid Crimson being used for the life of the application.

        // javax.xml.parsers.DocumentBuilderFactory
        String dbf = Docx4jProperties.getProperty("javax.xml.parsers.DocumentBuilderFactory");
        if (dbf!=null) {
            System.setProperty("javax.xml.parsers.DocumentBuilderFactory",dbf);
            log.info("Using " + dbf + " (from docx4j.properties)");

        } else if (Docx4jProperties.getProperty("docx4j.javax.xml.parsers.DocumentBuilderFactory.donotset", false)) {

            // Since 3.0.0
            // Don't set docx4j.javax.xml.parsers.DocumentBuilderFactory
            log.info("Not setting docx4j.javax.xml.parsers.DocumentBuilderFactory");

        } else if ((System.getProperty("java.version").startsWith("1.6")
                        && System.getProperty("java.vendor").startsWith("Sun"))
                || (System.getProperty("java.version").startsWith("1.7")
                        && System.getProperty("java.vendor").startsWith("Oracle"))
                || (System.getProperty("java.version").startsWith("1.7")
                        && System.getProperty("java.vendor").startsWith("Jeroen")) // IKVM
                ) {

            // Crimson doesn't support setTextContent; this.writeDocument also fails.
            // We've already worked around the problem with setTextContent,
            // but rather than do the same for writeDocument,
            // let's just stop using it.

            System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                    "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");

            log.info("Using com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");

        } else {

            // In this case suggest you add and use Xerces
            //     System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
            //            "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
            // which is what we used to do in XmlPart.

            log.info("default DocumentBuilderFactory property: "
                    + System.getProperty("javax.xml.parsers.DocumentBuilderFactory" ));
        }

        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setXIncludeAware(false);
        documentBuilderFactory.setExpandEntityReferences(false);
        try {
            documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        } catch (ParserConfigurationException e) {
            log.error("SAX Parser is not fully supporting security requirements");
            throw new RuntimeException(e);
        }

        // Note that we don't restore the value to its original setting (unlike TransformerFactory).
        // Maybe we could, if docx4j always used this documentBuilderFactory.

    }



    private static void instantiateTransformerFactory() {

        // docx4j requires real Xalan
        // See further docs/JAXP_TransformerFactory_XSLT_notes.txt
        String originalSystemProperty = System.getProperty("javax.xml.transform.TransformerFactory");

        try {
            System.setProperty("javax.xml.transform.TransformerFactory",
                    TRANSFORMER_FACTORY_PROCESSOR_XALAN);
//                    TRANSFORMER_FACTORY_SAXON);

            transformerFactory = javax.xml.transform.TransformerFactory
                    .newInstance();

            // We've got our factory now, so set it back again!
            if (originalSystemProperty == null) {
                System.clearProperty("javax.xml.transform.TransformerFactory");
            } else {
                System.setProperty("javax.xml.transform.TransformerFactory",
                        originalSystemProperty);
            }
        } catch (javax.xml.transform.TransformerFactoryConfigurationError e) {

            // Provider org.apache.xalan.processor.TransformerFactoryImpl not found
            log.error("Warning: Xalan jar missing from classpath; xslt not supported",e);

            // so try using whatever TransformerFactory is available
            if (originalSystemProperty == null) {
                System.clearProperty("javax.xml.transform.TransformerFactory");
            } else {
                System.setProperty("javax.xml.transform.TransformerFactory",
                        originalSystemProperty);
            }

            transformerFactory = javax.xml.transform.TransformerFactory
            .newInstance();
        }
        transformerFactory.setErrorListener(new LoggingErrorListener());
    }


    /**
     * If an object is wrapped in a JAXBElement, return the object.
     * Warning: be careful with this. If you are copying objects
     * into your document (rather than just reading them), you'll
     * probably want the object to remain wrapped (JAXB usually wraps them
     * for a reason; without the wrapper, you'll (probably?) need an
     * @XmlRootElement annotation in order to be able to marshall ie save your
     * document).
     *
     * @param o
     * @return
     */
    public static Object unwrap(Object o) {

        if (o==null) return null;

        if (o instanceof jakarta.xml.bind.JAXBElement) {
            log.debug("Unwrapped " + ((JAXBElement<?>)o).getDeclaredType().getName() );
            log.debug("name: " + ((JAXBElement<?>)o).getName() );
            return ((JAXBElement<?>)o).getValue();
        }
        return o;

        /*
         * Interestingly, DocumentSettingsPart (settings.xml) gets unwrapped,
         * and CTSettings does not have @XmlRootElement, but this does
         * not cause a problem when it is marshalled.
         */
    }

    /** Unmarshal an InputStream as an object in the package org.docx4j.jaxb.document.
     *  Note: you should ensure you include a namespace declaration for w: and
     *  any other namespace in the xml string.
     *  Also, the object you are attempting to unmarshall to might need to
     *  have an @XmlRootElement annotation for things to work.
     * @throws XMLStreamException */
    public static Object unmarshal(InputStream is, OpcPackage opcPackage)
        throws JAXBException, XMLStreamException {

        return unmarshal(is, Context.getJc(), new JaxbValidationEventHandler(opcPackage));
    }

    public static Object unmarshal(InputStream is, JAXBContext jc, JaxbValidationEventHandler eventHandler)
        throws JAXBException, XMLStreamException {

        final XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        final XMLStreamReader xReader = xif.createXMLStreamReader(is);
        final Unmarshaller u = Context.createUnmarshaller(jc, eventHandler);
        return u.unmarshal(xReader);
    }

    /** Unmarshal a String as an object in the package org.docx4j.jaxb.document.
     *  Note: you should ensure you include a namespace declaration for w: and
     *  any other namespace in the xml string.
     *  Also, the object you are attempting to unmarshall to might need to
     *  have an @XmlRootElement annotation for things to work.  */
    public static Object unmarshalString(String str, OpcPackage opcPackage) throws JAXBException {
        return unmarshalString(str, opcPackage, Context.getJc());
    }

    public static Object unmarshalString(String str, OpcPackage opcPackage, JAXBContext jc, Class<?> declaredType) throws JAXBException {
        final Unmarshaller u = Context.createUnmarshaller(jc, new org.docx4j.jaxb.JaxbValidationEventHandler(opcPackage));
        Object o = u.unmarshal( new javax.xml.transform.stream.StreamSource(new java.io.StringReader(str)), declaredType);
        if (o instanceof JAXBElement) {
            return ((JAXBElement<?>)o).getValue();
        }
        return o;
    }

    public static Object unmarshalString(String str, OpcPackage opcPackage, JAXBContext jc) throws JAXBException {
        log.debug("Unmarshalling '" + str + "'");
        final Unmarshaller u = Context.createUnmarshaller(jc, new org.docx4j.jaxb.JaxbValidationEventHandler(opcPackage));
        return u.unmarshal( new javax.xml.transform.stream.StreamSource(
                new java.io.StringReader(str)) );
    }

    public static Object unmarshal(Node n, OpcPackage opcPackage) throws JAXBException {
        return Context.createUnmarshaller(Context.getJc(), new org.docx4j.jaxb.JaxbValidationEventHandler(opcPackage)).unmarshal(n);
    }

    public static Object unmarshal(Node n, OpcPackage opcPackage, JAXBContext jc, Class<?> declaredType) throws JAXBException {

        // THIS DOESN"T WORK PROPERLY WITH 1.6.0.03 JAXB RI
        // (at least with CTTextParagraphProperties in
        //  a Xalan org.apache.xml.dtm.ref.DTMNodeProxy)
        // but converting the Node to a String and
        // unmarshalling that is fine!

        final Unmarshaller u = Context.createUnmarshaller(jc, new org.docx4j.jaxb.JaxbValidationEventHandler(opcPackage));
        Object o = u.unmarshal(n, declaredType);
        if ( o instanceof jakarta.xml.bind.JAXBElement) {
            return ((JAXBElement<?>)o).getValue();
        }
        return o;
    }

    /**
     * Give a string of wml containing ${key1}, ${key2}, return a suitable
     * object.
     *
     * @param wmlTemplateString
     * @param mappings
     * @return
     * @see JaxbXmlPart.variableReplace which can invoke this more efficiently
     */
    public static Object unmarshallFromTemplate(String wmlTemplateString, OpcPackage opcPackage,
            java.util.HashMap<String, String> mappings) throws JAXBException {
        return unmarshallFromTemplate(wmlTemplateString, opcPackage, mappings, Context.getJc());
    }

    public static Object unmarshallFromTemplate(String wmlTemplateString, OpcPackage opcPackage,
            java.util.HashMap<String, String> mappings, JAXBContext jc) throws JAXBException {
        String wmlString = replace(wmlTemplateString, 0, new StringBuilder(), mappings).toString();
        log.debug("Results of substitution: " + wmlString);
        return unmarshalString(wmlString, opcPackage, jc);
     }

    public static Object unmarshallFromTemplate(String wmlTemplateString, OpcPackage opcPackage,
            java.util.HashMap<String, String> mappings, JAXBContext jc, Class<?> declaredType) throws JAXBException {
          String wmlString = replace(wmlTemplateString, 0, new StringBuilder(), mappings).toString();
          return unmarshalString(wmlString, opcPackage, jc, declaredType);
       }


     private static StringBuilder replace(String wmlTemplateString, int offset, StringBuilder strB,
             java.util.HashMap<String, String> mappings) {

        int startKey = wmlTemplateString.indexOf("${", offset);
        if (startKey == -1)
           return strB.append(wmlTemplateString.substring(offset));
        strB.append(wmlTemplateString.substring(offset, startKey));
           int keyEnd = wmlTemplateString.indexOf('}', startKey);
           String key = wmlTemplateString.substring(startKey + 2, keyEnd);
           String val = mappings.get(key);
           if (val==null) {
               log.info("Invalid key '" + key + "' or key not mapped to a value");
               strB.append(key );
           } else {
               strB.append(val  );
           }
           return replace(wmlTemplateString, keyEnd + 1, strB, mappings);
     }

    /**
     * Marshal this object to a String, pretty printed, and without an XML declaration.
     * Useful for debugging.
     *
     * @param o
     * @return
     * @since 3.0.1
     */
    public static String marshaltoString(OpcPackage opcPackage, Object o) {

        return marshaltoString(opcPackage, o, true, true, Context.getJc());
    }

    /**
     * Use the specified JAXBContext to marshal this object to a String, pretty printed, and without an XML declaration.
     * Useful for debugging.
     *
     * @param o
     * @return
     * @since 3.0.1
     */
    public static String marshaltoString(OpcPackage opcPackage, Object o, JAXBContext jc ) {

        return marshaltoString(opcPackage, o, true, true, jc );
    }

    /** Marshal to a String */
    public static String marshaltoString(OpcPackage opcPackage, Object o, boolean suppressDeclaration, boolean prettyprint,
            JAXBContext jc ) {

        /* http://weblogs.java.net/blog/kohsuke/archive/2005/10/101_ways_to_mar.html
         *
         * If you are writing to a file, a socket, or memory, then you should use
         * the version that takes OutputStream. Unless you change the target
         * encoding to something else (default is UTF-8), there's a special
         * marshaller codepath for OutputStream, which makes it run really fast.
         * You also don't have to use BufferedOutputStream, since the JAXB RI
         * does the adequate buffering.
         *
         * You can also write to Writer, but in this case you'll be responsible
         * for encoding characters, so in general you need to be careful. If
         * you want to marshal XML into an encoding other than UTF-8, it's best
         *  to use the JAXB_ENCODING property and then write to OutputStream,
         *  as it escapes characters to things like &#x1824; correctly.
         */

        if(o==null) {
            return null;
        }

        try {
            final Marshaller m = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            if (prettyprint) {
                m.setProperty("jaxb.formatted.output", true);
            }

            /* Fix for:
             *         <t tstamp='1198193417585' snum='1' op='update'>
             * ~~~~~~~>    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
             *                 <ns1:sdt xmlns:ns1="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
             *                     <ns1:sdtPr><ns1:id ns1:val="1814108031"/><ns1:tag ns1:val="1"/></ns1:sdtPr><ns1:sdtContent><ns1:p><ns1:r><ns1:t>Lookin</ns1:t></ns1:r><ns1:r><ns1:t> pretty</ns1:t></ns1:r></ns1:p></ns1:sdtContent></ns1:sdt></t>
             */

            /* http://weblogs.java.net/blog/kohsuke/archive/2005/10/101_ways_to_mar.html
             *
             * JAXB_FRAGMENT prevents the marshaller from producing an XML declaration,
             * so the above works just fine. The downside of this approach is that if
             * the ancestor elements declare the namespaces, JAXB won't be able to take
             * advantage of them.
             */

            if (suppressDeclaration) {
                m.setProperty(Marshaller.JAXB_FRAGMENT,true);
            }

            StringWriter sWriter = new StringWriter();
            m.marshal(o, sWriter);
            return sWriter.toString();

/*          Alternative implementation

            java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
            marshaller.marshal(o, out);
            byte[] bytes = out.toByteArray();
            return new String(bytes);
 */

        } catch (JAXBException e) {
                    throw new RuntimeException(e);
        }
    }

    /** Marshal to a String, for object
     *  missing an @XmlRootElement annotation.  */
    public static String marshaltoString(OpcPackage opcPackage, Object o, boolean suppressDeclaration, boolean prettyprint,
            JAXBContext jc, String uri, String local, Class<Object> declaredType) {
        // TODO - refactor this.
        try {
            final Marshaller m = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            if (prettyprint) {
                m.setProperty("jaxb.formatted.output", true);
            }
            if (suppressDeclaration) {
                m.setProperty(Marshaller.JAXB_FRAGMENT,true);
            }
            StringWriter sWriter = new StringWriter();
            m.marshal(new JAXBElement<Object>(new QName(uri,local), declaredType, o ), sWriter);
            return sWriter.toString();

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] marshalToByteArray(OpcPackage opcPackage, Object o, boolean suppressDeclaration, JAXBContext jc) {
        try {
            final Marshaller m = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            if (suppressDeclaration) {
                m.setProperty(Marshaller.JAXB_FRAGMENT,true);
            }
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            m.marshal(o, os);
            return os.toByteArray();

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    public static java.io.InputStream marshaltoInputStream(OpcPackage opcPackage, Object o, boolean suppressDeclaration, JAXBContext jc ) {

        /* http://weblogs.java.net/blog/kohsuke/archive/2005/10/101_ways_to_mar.html
         *
         * If you are writing to a file, a socket, or memory, then you should use
         * the version that takes OutputStream. Unless you change the target
         * encoding to something else (default is UTF-8), there's a special
         * marshaller codepath for OutputStream, which makes it run really fast.
         * You also don't have to use BufferedOutputStream, since the JAXB RI
         * does the adequate buffering.
         *
         */

        try {
            final Marshaller m = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            if (suppressDeclaration) {
                m.setProperty(Marshaller.JAXB_FRAGMENT,true);
            }
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            m.marshal(o, os);

            // Now copy from the outputstream to inputstream
            // See http://ostermiller.org/convert_java_outputstream_inputstream.html

            return new java.io.ByteArrayInputStream(os.toByteArray());

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }



    /** Marshal to a W3C document */
    public static org.w3c.dom.Document marshaltoW3CDomDocument(OpcPackage opcPackage, Object o) {

        return marshaltoW3CDomDocument(opcPackage, o, Context.getJc());
    }

    /** Marshal to a W3C document */
    public static org.w3c.dom.Document marshaltoW3CDomDocument(OpcPackage opcPackage, Object o, JAXBContext jc) {
        // TODO - refactor this.
        try {
            final Marshaller marshaller = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            org.w3c.dom.Document doc = XmlUtils.getDocumentBuilderFactory().newDocumentBuilder().newDocument();
            marshaller.marshal(o, doc);

            return doc;
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /** Marshal to a W3C document, for object
     *  missing an @XmlRootElement annotation.  */
    public static org.w3c.dom.Document marshaltoW3CDomDocument(OpcPackage opcPackage, Object o, JAXBContext jc,
        String uri, String local, Class<Object> declaredType) {
        // TODO - refactor this.
        try {

            final Marshaller marshaller = Context.createMarshaller(jc, NamespacePrefixMapperUtils.getPrefixMapper(), new JaxbValidationEventHandler(opcPackage));
            org.w3c.dom.Document doc = XmlUtils.getDocumentBuilderFactory().newDocumentBuilder().newDocument();
            // See http://weblogs.java.net/blog/kohsuke/archive/2006/03/why_does_jaxb_p.html
            marshaller.marshal(new JAXBElement<Object>(new QName(uri,local), declaredType, o ), doc);
            return doc;
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /** Clone this JAXB object, using default JAXBContext. */
    public static <T> T deepCopy(T value, OpcPackage opcPackage) {
        return deepCopy(value, opcPackage, Context.getJc());
    }


    /** Clone this JAXB object
     * @param value
     * @param jc
     * @return
     */
    public static <T> T deepCopy(T value, OpcPackage opcPackage, JAXBContext jc) {

        if (value==null) {
            throw new IllegalArgumentException("Can't clone a null argument");
        }

        try {
            JAXBElement<?> elem;
            Class<?> valueClass;
            if (value instanceof JAXBElement<?>) {
                log.debug("deep copy of JAXBElement..");
                elem = (JAXBElement<?>) value;
                valueClass = elem.getDeclaredType();
            } else {
                log.debug("deep copy of " + value.getClass().getName() );
                @SuppressWarnings("unchecked")
                Class<T> classT = (Class<T>) value.getClass();
                elem = new JAXBElement<T>(new QName("temp"), classT, value);
                valueClass = classT;
            }

            final Marshaller mar = Context.createMarshaller(jc, null, new JaxbValidationEventHandler(opcPackage));
            ByteArrayOutputStream bout = new ByteArrayOutputStream(256);
            mar.marshal(elem, bout);

//            byte[] bytes = bout.toByteArray();
//            try {
//                System.out.println(new String(bytes, "UTF-8"));
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }

            final Unmarshaller unmar = Context.createUnmarshaller(jc, new org.docx4j.jaxb.JaxbValidationEventHandler(opcPackage));
            elem = unmar.unmarshal(new StreamSource(new ByteArrayInputStream(
                    bout.toByteArray())), valueClass);

            /*
             * Losing content here?
             *
             * First, make absolutely sure that what you have is valid.
             *
             * For example, Word 2010 is happy to open w:p/w:pict
             * (as opposed to w:p/w:r/w:pict).
             * docx4j is happy to marshall w:p/w:pict, but not unmarshall it,
             * so that content would get dropped here.
             */

            T res;
            if (value instanceof JAXBElement<?>) {
                @SuppressWarnings("unchecked")
                T resT = (T) elem;
                res = resT;
            } else {
                @SuppressWarnings("unchecked")
                T resT = (T) elem.getValue();
                res = resT;
            }
//            log.info("deep copy success!");
            return res;
        } catch (JAXBException ex) {
            throw new IllegalArgumentException(ex);
        }
    }


    public static String w3CDomNodeToString(Node n) {

        // Why doesn't Java have a nice neat way of getting
        // the XML as a String??

        StringWriter sw = new StringWriter();
        try {
                Transformer serializer = transformerFactory.newTransformer();
                serializer.setOutputProperty(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");
                serializer.setOutputProperty(javax.xml.transform.OutputKeys.METHOD, "xml");
                //serializer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
                serializer.transform( new DOMSource(n) , new StreamResult(sw) );
                return sw.toString();
                //log.debug("serialised:" + n);
            } catch (Exception e) {
                // Unexpected!
                e.printStackTrace();
                return null;
            }
    }

    /** Use DocumentBuilderFactory to create and return a new w3c dom Document. */
    public static org.w3c.dom.Document neww3cDomDocument() {

        try {
            return XmlUtils.getDocumentBuilderFactory().newDocumentBuilder().newDocument();
        } catch (ParserConfigurationException e) {
                    throw new RuntimeException(e);
        }
    }

      /**
       * @param docBuilder
       *          the parser
       * @param parent
       *          node to add fragment to
       * @param fragment
       *          a well formed XML fragment
     * @throws ParserConfigurationException
       */
    public static void appendXmlFragment(Document document, Node parent, String fragment)
            throws IOException, SAXException, ParserConfigurationException {

        Node fragmentNode = XmlUtils.getDocumentBuilderFactory().newDocumentBuilder().parse(
                new InputSource(new StringReader(fragment)))
                .getDocumentElement();

        fragmentNode = document.importNode(fragmentNode, true);

        parent.appendChild(fragmentNode);
    }

    /**
     * Prepare a JAXB transformation result for some given context.
     * @param context The JAXB context.
     * @return The result data structure created.
     * @throws Docx4JException In case of configuration errors.
     */
    public static JAXBResult prepareJAXBResult(OpcPackage opcPackage, JAXBContext context)
            throws Docx4JException {

        final JAXBResult result;
        try {
            final Unmarshaller unmarshaller = Context.createUnmarshaller(context, new JaxbValidationEventHandler(opcPackage));
            result = new JAXBResult(unmarshaller);

        } catch (JAXBException e) {
            throw new Docx4JException("Error preparing empty JAXB result", e);
        }
        return result;
    }

    public static void transform(org.w3c.dom.Document doc,
            javax.xml.transform.Templates template,
              Map<String, Object> transformParameters,
              javax.xml.transform.Result result) throws Docx4JException {

        if (doc == null ) {
            Throwable t = new Throwable();
            throw new Docx4JException( "Null DOM Doc", t);
        }

        javax.xml.transform.dom.DOMSource domSource = new javax.xml.transform.dom.DOMSource(doc);

        transform(domSource,
                template,
                 transformParameters,
                 result);
    }


    public static Templates getTransformerTemplate(
              javax.xml.transform.Source xsltSource) throws TransformerConfigurationException {

        return transformerFactory.newTemplates(xsltSource);
    }

    public static byte[] transformMcPreprocessorXslt(InputStream is)
        throws Docx4JException, SAXException, ParserConfigurationException, TransformerConfigurationException, IOException {

        final Templates mcPreprocessorXslt = JaxbValidationEventHandler.getMcPreprocessor();
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final StreamResult streamResult = new StreamResult();
        streamResult.setOutputStream(bos);
        transform(is, mcPreprocessorXslt, null, streamResult);
        return bos.toByteArray();
    }

    public static void transform(InputStream is, javax.xml.transform.Templates template, Map<String, Object> transformParameters, javax.xml.transform.Result result)
        throws Docx4JException, SAXException, ParserConfigurationException {

        final SAXSource saxSource = new SAXSource(new InputSource(is));
        saxSource.setXMLReader(getXMLReader());
        transform(saxSource, template, transformParameters, result);
    }

    /**
     *
     * Transform an input document using XSLT
     *
     * @param doc
     * @param xslt
     * @param transformParameters
     * @param result
     * @throws Docx4JException In case serious transformation errors occur
     */
    public static void transform(javax.xml.transform.Source source,
                          javax.xml.transform.Templates template,
                          Map<String, Object> transformParameters,
                          javax.xml.transform.Result result) throws Docx4JException {

        if (source==null || source instanceof StreamSource) {
            Throwable t = new Throwable();
            throw new Docx4JException( "Null Source doc", t);
        }

        // Use the template to create a transformer
        // A Transformer may not be used in multiple threads running concurrently.
        // Different Transformers may be used concurrently by different threads.
        // A Transformer may be used multiple times. Parameters and output properties
        // are preserved across transformations.
        javax.xml.transform.Transformer xformer;
    try {
      xformer = template.newTransformer();
    } catch (TransformerConfigurationException e) {
      throw new Docx4JException("The Transformer is ill-configured", e);
    }
        if (!xformer.getClass().getName().equals(
                "org.apache.xalan.transformer.TransformerImpl")) {
            log
                    .error("Detected "
                            + xformer.getClass().getName()
                            + ", but require org.apache.xalan.transformer.TransformerImpl. "
                            + "Ensure Xalan 2.7.0 is on your classpath!");
        }
        xformer.setErrorListener(new LoggingErrorListener());

        if (transformParameters != null) {
            final Iterator<Entry<String, Object>> parameterIterator = transformParameters.entrySet().iterator();
            while (parameterIterator.hasNext()) {
                Map.Entry<String, Object> pairs = parameterIterator.next();

                if (pairs.getKey() == null) {
                    log.info("Skipped null key");
                    // pairs = (Map.Entry)parameterIterator.next();
                    continue;
                }
                if (pairs.getKey().equals("customXsltTemplates")) continue;

                if (pairs.getValue() == null) {
                    log.info("parameter '" + pairs.getKey() + "' was null.");
                } else {
                    xformer.setParameter(pairs.getKey(), pairs.getValue());
                }
            }
        }

        /* SUPER DEBUGGING
            // http://xml.apache.org/xalan-j/usagepatterns.html#debugging
            // debugging
            // Set up a PrintTraceListener object to print to a file.
            java.io.FileWriter fw = new java.io.FileWriter("/tmp/xslt-events" + xsltCount++ + ".log");
            java.io.PrintWriter pw = new java.io.PrintWriter(fw);
            PrintTraceListener ptl = new PrintTraceListener(pw);

            // Print information as each node is 'executed' in the stylesheet.
            ptl.m_traceElements = true;
            // Print information after each result-tree generation event.
            ptl.m_traceGeneration = true;
            // Print information after each selection event.
            ptl.m_traceSelection = true;
            // Print information whenever a template is invoked.
            ptl.m_traceTemplates = true;
            // Print information whenever an extension is called.
            ptl.m_traceExtension = true;
            TransformerImpl transformerImpl = (TransformerImpl)xformer;

              // Register the TraceListener with the TraceManager associated
              // with the TransformerImpl.
              TraceManager trMgr = transformerImpl.getTraceManager();
              trMgr.addTraceListener(ptl);

*/
        // DEBUGGING
        // use the identity transform if you want to send wordDocument;
        // otherwise you'll get the XHTML
        // javax.xml.transform.Transformer xformer = tfactory.newTransformer();
        try {
            xformer.transform(source, result);
        } catch (TransformerException e) {
          throw new Docx4JException("Cannot perform the transformation", e);
        } finally {
            //pw.flush();
        }
    }

    static class LoggingErrorListener implements ErrorListener {

        // See http://www.cafeconleche.org/slides/sd2003west/xmlandjava/346.html

        public LoggingErrorListener() {
            //
        }

        @Override
        public void warning(TransformerException exception) {
            //
        }

        @Override
        public void error(TransformerException exception) {
            //
        }

        @Override
        public void fatalError(TransformerException exception) throws TransformerException {
            // This is an error which the processor cannot recover from;
            // e.g. a malformed stylesheet or input document
            // so I must throw this exception here.
            throw exception;
        }
    }
}
