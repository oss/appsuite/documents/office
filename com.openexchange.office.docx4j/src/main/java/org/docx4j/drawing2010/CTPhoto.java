
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_Photo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_Photo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="imgLayer" type="{http://schemas.microsoft.com/office/drawing/2010/main}CT_PictureLayer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_Photo", propOrder = {
    "imgLayer"
})
public class CTPhoto {

    @XmlElement(required = true)
    protected CTPictureLayer imgLayer;

    /**
     * Gets the value of the imgLayer property.
     * 
     * @return
     *     possible object is
     *     {@link CTPictureLayer }
     *     
     */
    public CTPictureLayer getImgLayer() {
        return imgLayer;
    }

    /**
     * Sets the value of the imgLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTPictureLayer }
     *     
     */
    public void setImgLayer(CTPictureLayer value) {
        this.imgLayer = value;
    }
}
