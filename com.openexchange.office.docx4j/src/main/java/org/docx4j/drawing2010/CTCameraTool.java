
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_CameraTool complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_CameraTool">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="cellRange" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="spid" type="{http://www.w3.org/2001/XMLSchema}string" default="0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_CameraTool")
public class CTCameraTool {

    @XmlAttribute(name = "cellRange")
    protected String cellRange;
    @XmlAttribute(name = "spid")
    protected String spid;

    /**
     * Gets the value of the cellRange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellRange() {
        return cellRange;
    }

    /**
     * Sets the value of the cellRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellRange(String value) {
        this.cellRange = value;
    }

    /**
     * Gets the value of the spid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpid() {
        if (spid == null) {
            return "0";
        }
        return spid;
    }

    /**
     * Sets the value of the spid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpid(String value) {
        this.spid = value;
    }
}
