
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;

import org.docx4j.dml.CTLockingBase;


/**
 * <p>Java class for CT_ContentPartLocking complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_ContentPartLocking">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://schemas.openxmlformats.org/drawingml/2006/main}AG_Locking"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_ContentPartLocking", propOrder = {
    "extLst"
})
public class CTContentPartLocking extends CTLockingBase implements CTLockingBase.INoRot,
	CTLockingBase.INoEditPoints, CTLockingBase.INoAdjustHandles, CTLockingBase.INoChangeArrowheads, CTLockingBase.INoChangeShapeType {

	@XmlAttribute(name = "noRot")
    protected Boolean noRot;
    @XmlAttribute(name = "noEditPoints")
    protected Boolean noEditPoints;
    @XmlAttribute(name = "noAdjustHandles")
    protected Boolean noAdjustHandles;
    @XmlAttribute(name = "noChangeArrowheads")
    protected Boolean noChangeArrowheads;
    @XmlAttribute(name = "noChangeShapeType")
    protected Boolean noChangeShapeType;

    /**
     * Gets the value of the noRot property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isNoRot() {
        if (noRot == null) {
            return false;
        }
        return noRot;
    }

    /**
     * Sets the value of the noRot property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setNoRot(Boolean value) {
        this.noRot = value;
    }

    /**
     * Gets the value of the noEditPoints property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isNoEditPoints() {
        if (noEditPoints == null) {
            return false;
        }
        return noEditPoints;
    }

    /**
     * Sets the value of the noEditPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setNoEditPoints(Boolean value) {
        this.noEditPoints = value;
    }

    /**
     * Gets the value of the noAdjustHandles property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isNoAdjustHandles() {
        if (noAdjustHandles == null) {
            return false;
        }
        return noAdjustHandles;
    }

    /**
     * Sets the value of the noAdjustHandles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoAdjustHandles(Boolean value) {
        this.noAdjustHandles = value;
    }

    /**
     * Gets the value of the noChangeArrowheads property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNoChangeArrowheads() {
        if (noChangeArrowheads == null) {
            return false;
        }
        return noChangeArrowheads;
    }

    /**
     * Sets the value of the noChangeArrowheads property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setNoChangeArrowheads(Boolean value) {
        this.noChangeArrowheads = value;
    }

    /**
     * Gets the value of the noChangeShapeType property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @Override
    public boolean isNoChangeShapeType() {
        if (noChangeShapeType == null) {
            return false;
        }
        return noChangeShapeType;
    }

    /**
     * Sets the value of the noChangeShapeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    @Override
    public void setNoChangeShapeType(Boolean value) {
        this.noChangeShapeType = value;
    }
}
