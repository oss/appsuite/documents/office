
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_IsGvmlCanvas complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_IsGvmlCanvas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="val" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_IsGvmlCanvas")
public class CTIsGvmlCanvas {

    @XmlAttribute(name = "val", required = true)
    protected boolean val;

    /**
     * Gets the value of the val property.
     * 
     */
    public boolean isVal() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     */
    public void setVal(boolean value) {
        this.val = value;
    }
}
