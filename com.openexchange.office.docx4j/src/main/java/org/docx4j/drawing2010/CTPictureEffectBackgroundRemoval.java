
package org.docx4j.drawing2010;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PictureEffectBackgroundRemoval complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PictureEffectBackgroundRemoval">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="foregroundMark" type="{http://schemas.microsoft.com/office/drawing/2010/main}CT_PictureEffectBackgroundRemovalForegroundMark" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="backgroundMark" type="{http://schemas.microsoft.com/office/drawing/2010/main}CT_PictureEffectBackgroundRemovalBackgroundMark" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="t" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_PositiveFixedPercentage" />
 *       &lt;attribute name="b" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_PositiveFixedPercentage" />
 *       &lt;attribute name="l" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_PositiveFixedPercentage" />
 *       &lt;attribute name="r" use="required" type="{http://schemas.openxmlformats.org/drawingml/2006/main}ST_PositiveFixedPercentage" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PictureEffectBackgroundRemoval", propOrder = {
    "foregroundMark",
    "backgroundMark"
})
public class CTPictureEffectBackgroundRemoval {

    protected List<CTPictureEffectBackgroundRemovalForegroundMark> foregroundMark;
    protected List<CTPictureEffectBackgroundRemovalBackgroundMark> backgroundMark;
    @XmlAttribute(name = "t", required = true)
    protected int t;
    @XmlAttribute(name = "b", required = true)
    protected int b;
    @XmlAttribute(name = "l", required = true)
    protected int l;
    @XmlAttribute(name = "r", required = true)
    protected int r;

    /**
     * Gets the value of the foregroundMark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the foregroundMark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForegroundMark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTPictureEffectBackgroundRemovalForegroundMark }
     * 
     * 
     */
    public List<CTPictureEffectBackgroundRemovalForegroundMark> getForegroundMark() {
        if (foregroundMark == null) {
            foregroundMark = new ArrayList<CTPictureEffectBackgroundRemovalForegroundMark>();
        }
        return this.foregroundMark;
    }

    /**
     * Gets the value of the backgroundMark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the backgroundMark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBackgroundMark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CTPictureEffectBackgroundRemovalBackgroundMark }
     * 
     * 
     */
    public List<CTPictureEffectBackgroundRemovalBackgroundMark> getBackgroundMark() {
        if (backgroundMark == null) {
            backgroundMark = new ArrayList<CTPictureEffectBackgroundRemovalBackgroundMark>();
        }
        return this.backgroundMark;
    }

    /**
     * Gets the value of the t property.
     * 
     */
    public int getT() {
        return t;
    }

    /**
     * Sets the value of the t property.
     * 
     */
    public void setT(int value) {
        this.t = value;
    }

    /**
     * Gets the value of the b property.
     * 
     */
    public int getB() {
        return b;
    }

    /**
     * Sets the value of the b property.
     * 
     */
    public void setB(int value) {
        this.b = value;
    }

    /**
     * Gets the value of the l property.
     * 
     */
    public int getL() {
        return l;
    }

    /**
     * Sets the value of the l property.
     * 
     */
    public void setL(int value) {
        this.l = value;
    }

    /**
     * Gets the value of the r property.
     * 
     */
    public int getR() {
        return r;
    }

    /**
     * Sets the value of the r property.
     * 
     */
    public void setR(int value) {
        this.r = value;
    }
}
