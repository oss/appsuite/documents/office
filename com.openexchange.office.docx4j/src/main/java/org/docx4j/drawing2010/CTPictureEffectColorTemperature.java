
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CT_PictureEffectColorTemperature complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_PictureEffectColorTemperature">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="colorTemp" type="{http://schemas.microsoft.com/office/drawing/2010/main}ST_ColorTemperature" default="6500" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_PictureEffectColorTemperature")
public class CTPictureEffectColorTemperature {

    @XmlAttribute(name = "colorTemp")
    protected Integer colorTemp;

    /**
     * Gets the value of the colorTemp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getColorTemp() {
        if (colorTemp == null) {
            return  6500;
        }
        return colorTemp;
    }

    /**
     * Sets the value of the colorTemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setColorTemp(Integer value) {
        this.colorTemp = value;
    }
}
