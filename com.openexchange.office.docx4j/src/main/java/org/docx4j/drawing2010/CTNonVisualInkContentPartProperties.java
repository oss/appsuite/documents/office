
package org.docx4j.drawing2010;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import org.docx4j.dml.CTOfficeArtExtensionList;
import org.docx4j.dml.ILocksAccess;

/**
 * <p>Java class for CT_NonVisualInkContentPartProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CT_NonVisualInkContentPartProperties">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cpLocks" type="{http://schemas.microsoft.com/office/drawing/2010/main}CT_ContentPartLocking" minOccurs="0"/>
 *         &lt;element name="extLst" type="{http://schemas.openxmlformats.org/drawingml/2006/main}CT_OfficeArtExtensionList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="isComment" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CT_NonVisualInkContentPartProperties", propOrder = {
    "cpLocks",
    "extLst"
})
public class CTNonVisualInkContentPartProperties implements ILocksAccess {

    protected CTContentPartLocking cpLocks;
    protected CTOfficeArtExtensionList extLst;
    @XmlAttribute(name = "isComment")
    protected Boolean isComment;

    /**
     * Gets the value of the cpLocks property.
     * 
     * @return
     *     possible object is
     *     {@link CTContentPartLocking }
     *     
     */
    @Override
    public CTContentPartLocking getLocks(boolean createIfMissing) {
    	if(cpLocks==null&&createIfMissing) {
    		cpLocks = new CTContentPartLocking();
    	}
        return cpLocks;
    }

    /**
     * Sets the value of the cpLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTContentPartLocking }
     *     
     */
    public void setCpLocks(CTContentPartLocking value) {
        this.cpLocks = value;
    }

    /**
     * Gets the value of the extLst property.
     * 
     * @return
     *     possible object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public CTOfficeArtExtensionList getExtLst() {
        return extLst;
    }

    /**
     * Sets the value of the extLst property.
     * 
     * @param value
     *     allowed object is
     *     {@link CTOfficeArtExtensionList }
     *     
     */
    public void setExtLst(CTOfficeArtExtensionList value) {
        this.extLst = value;
    }

    /**
     * Gets the value of the isComment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsComment() {
        if (isComment == null) {
            return true;
        }
        return isComment;
    }

    /**
     * Sets the value of the isComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsComment(Boolean value) {
        this.isComment = value;
    }
}
