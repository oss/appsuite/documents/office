

/*
 *  Copyright 2007-2012, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.io3;



import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jakarta.xml.bind.JAXBException;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.URIHelper;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.contenttype.ContentTypeManager.NonPersistContent;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io3.stores.PartStore;
import org.docx4j.openpackaging.io3.stores.ZipPartStore;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.SerializationPart;
import org.docx4j.openpackaging.parts.XmlPart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.relationships.Relationships;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.office.tools.common.parser.CharMatcher;

/**
 * Save a Package object using the PartStore
 * defined on the package.
 *
 * @author jharrop
 * @since 3.0
 */
public class Save {

	private static Logger log = LoggerFactory.getLogger(Save.class);

	public Save(OpcPackage p) {

		this.p = p;
		PartStore targetPartStore;
		if (p.getSourcePartStore()==null) // eg a newly created package
		{
			log.info("sourcePartStore undefined");
			targetPartStore = new ZipPartStore();
		} else {
			targetPartStore = p.getSourcePartStore();
			targetPartStore.setSourcePartStore(p.getSourcePartStore());
		}
		p.setTargetPartStore(targetPartStore);

	}

	public Save(OpcPackage p, PartStore targetPartStore) {

		this.p = p;
		if (p.getSourcePartStore()==null) {
			log.info("sourcePartStore undefined");
		} else {
			targetPartStore.setSourcePartStore(p.getSourcePartStore());
		}
		p.setTargetPartStore(targetPartStore);
	}

	// The package to save
	public OpcPackage p;

	private boolean checkUnusedRelations = false;

    public void setCheckUnusedRelations(boolean checkUnusedRelations) {
        this.checkUnusedRelations = checkUnusedRelations;
    }

    final static Set<String> partsToBeChecked = new HashSet<String>(Arrays.asList(
    	ContentTypes.WORDPROCESSINGML_DOCUMENT,
        ContentTypes.WORDPROCESSINGML_HEADER,
        ContentTypes.WORDPROCESSINGML_FOOTER,
        ContentTypes.WORDPROCESSINGML_COMMENTS,
        ContentTypes.SPREADSHEETML_WORKSHEET,
        ContentTypes.PRESENTATIONML_SLIDE,
        ContentTypes.PRESENTATIONML_SLIDE_MASTER,
        ContentTypes.PRESENTATIONML_NOTES_SLIDE,
        ContentTypes.PRESENTATIONML_NOTES_MASTER,
        ContentTypes.PRESENTATIONML_HANDOUT_MASTER,
        ContentTypes.DRAWINGML_DRAWING));

    final static Set<String> checkRelations = new HashSet<String>(Arrays.asList(
        Namespaces.IMAGE,
        Namespaces.SPREADSHEETML_CHART,
        Namespaces.SPREADSHEETML_DRAWING,
        Namespaces.HYPERLINK,
        Namespaces.DRAWINGML_DIAGRAM_COLORS,
        Namespaces.DRAWINGML_DIAGRAM_DATA,
        Namespaces.DRAWINGML_DIAGRAM_DRAWING,
        Namespaces.DRAWINGML_DIAGRAM_LAYOUT,
        Namespaces.DRAWINGML_DIAGRAM_STYLE));

    public static int indexOfBytes(byte[] array, byte[] pattern, int index)
    {
        int ident = 0;
        while(index < array.length) {
            ident = (array[index] == pattern[ident]) ? ++ident : 0;
            if (ident == pattern.length) {
                return index - ident + 1;
            }
            index++;
        }
        return -1;
    }

    // if the part has been marshalled the stream is returned so that the part is not marshalled twice
    public static ByteArrayOutputStream checkUnusedRelations(SerializationPart<?> part)
        throws JAXBException {

        if(!partsToBeChecked.contains(part.getContentType())) {
            return null;
        }
        final RelationshipsPart relationshipsPart = part.getRelationshipsPart();
        if(relationshipsPart==null) {
            return null;
        }
        Relationships relationships = relationshipsPart.getRelationships();
        if(relationships==null) {
            return null;
        }

        // only taking care of ids starting with : rId, other ids are ignored

        final List<Relationship> relationshipList = relationships.getRelationship();
        for(Relationship relationship:relationshipList) {
            if(checkRelations.contains(relationship.getType())) {

                // this part is executed only if the part is having a relationship which is worth to be checked

                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ((SerializationPart<?>)part).marshal(outputStream);

                // searching and collecting each "rId
                final byte[] out = outputStream.toByteArray();
                final byte[] pattern = {'"', 'r', 'I', 'd'};
                final Set<String> ids = CharMatcher.findAllMatches(out, pattern, '"', 3);

                // now each rId used within our part is kept within ids
                for(int i = 0; i<relationshipList.size();i++) {
                    final Relationship relShip = relationshipList.get(i);
                    if(relShip.getId()!=null && relShip.getId().startsWith("rId")) {
                        if(checkRelations.contains(relShip.getType())&&!ids.contains(relShip.getId())) {
                            relationshipList.remove(i--);
                        }
                    }
                }
                return outputStream;
            }
        }
        return null;
    }

	/**
	 * This HashMap is intended to prevent loops.
	 */
	private HashMap<String, String> handled;

	/**
	 * @param realOS - responsibility of the caller to close this.
	 * @return
	 * @throws Docx4JException
	 */
	public boolean save(OutputStream realOS) throws Docx4JException  {
		handled = new HashMap<String, String>();
		try {

            p.getTargetPartStore().setOutputStream(realOS);

			// 3. Save [Content_Types].xml
			ContentTypeManager ctm = p.getContentTypeManager();
			p.getTargetPartStore().saveContentTypes(ctm);

			// 4. Start with _rels/.rels

//			<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
//			  <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
//			  <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
//			  <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>
//			</Relationships>

			RelationshipsPart rp = p.getRelationshipsPart();
			saveRawXmlPart(rp );

			// 5. Now recursively

			addPartsFromRelationships(rp);

            // 6. writing non persist content (content that is removed after load/save)
            final List<NonPersistContent> nonPersistContentList = ctm.getNonPersistContent();
            for(NonPersistContent nonPersistContent : nonPersistContentList) {

                Context.abortOnLowMemory(rp.getPackage());

                final BinaryPart binPart = new BinaryPart(new PartName('/' + nonPersistContent.zipEntryName));
                binPart.setBinaryData(nonPersistContent.contentData);
                savePart(binPart);
            }

			p.getTargetPartStore().finishSave();
	    } catch (Exception e) {
			if (e instanceof Docx4JException) {
				throw (Docx4JException)e;
			}
			throw new Docx4JException("Failed to save package", e);
	    }
	    log.debug("...Done!" );
		return true;
	}

	public void  saveRawXmlPart(Part part) throws Docx4JException {

		try {
			if (part instanceof SerializationPart) {
				p.getTargetPartStore().saveSerializationPart(((SerializationPart<?>)part), checkUnusedRelations);
			}
			else if (part instanceof org.docx4j.openpackaging.parts.XmlPart) {
				p.getTargetPartStore().saveXmlPart((XmlPart)part);
			}
			else {
				log.error("PROBLEM - No suitable part found for: " + part.getPartName());
			}
		} catch (Exception e) {
			throw new Docx4JException("Problem saving part " + part.getPartName(), e);
		}
	}


	/* recursively
		(i) get each Part listed in the relationships
		(ii) add the Part to the zip file
		(iii) traverse its relationship
	*/
	public void addPartsFromRelationships(RelationshipsPart rp) throws Docx4JException {

		for (Relationship r : rp.getRelationships().getRelationship()) {

		    Context.abortOnLowMemory(rp.getPackage());

		    log.debug("For Relationship Id=" + r.getId()
					+ " Source is " + rp.getSourceP().getPartName()
					+ ", Target is " + r.getTarget() );

			if (r.getType().equals(Namespaces.HYPERLINK)) {
				continue;  // whether internal or external
			}

			if (r.getTargetMode() != null
					&& r.getTargetMode().equals("External") ) {

				// ie its EXTERNAL
				// As at 1 May 2008, we don't have a Part for these;
				// there is just the relationship.

				log.info("Encountered external resource " + r.getTarget()
						   + " of type " + r.getType() );

				// So
				continue;
			}

			try {
				//String resolvedPartUri = URIHelper.resolvePartUri(r.getSourceURI(), r.getTargetURI() ).toString();

				String resolvedPartUri = URIHelper.resolvePartUri(rp.getSourceURI(), new URI(r.getTarget() ) ).toString();

				// Now drop leading "/'
				resolvedPartUri = resolvedPartUri.substring(1);

				// Now normalise it .. ie abc/def/../ghi
				// becomes abc/ghi
				// Maybe this isn't necessary with a zip file,
				// - ZipFile class may be smart enough to do it.
				// But it is certainly necessary in the JCR case.
//				target = (new java.net.URI(target)).normalize().toString();
//				log.info("Normalised, it is " + target );

//				Document contents = getDocumentFromZippedPart( zf,  target);

				if (!false) {
					log.debug("Getting part /" + resolvedPartUri );

					//Part part = p.getParts().get(new PartName("/" + resolvedPartUri));
					Part part = rp.getPart(r);
						// 2012 09 26: If the part is actually attached to
						// a different package, using this you can still get it.
						// Use this 'feature' at your own risk!

					if (part==null) {
						log.error("Part " + resolvedPartUri + " not found!");
					} else {

						if (!part.getPackage().equals(p)) {
							log.info("Part " + resolvedPartUri + " is attached to some other package");
						}
						savePart(part);
					}

				}

			} catch (Exception e) {
				throw new Docx4JException("Failed to add parts from relationships of " + rp.getSourceP().getPartName(), e);
			}
		}
	}

	/**
	 * @param out
	 * @param resolvedPartUri
	 * @param part
	 * @throws Docx4JException
	 * @throws IOException
	 */
	public void savePart(Part part) throws Docx4JException {

		// Drop the leading '/'
	    final String resolvedPartUri = part.getPartName().getName().substring(1);
		if (handled.get(resolvedPartUri)!=null) {
			log.debug(".. duplicate save avoided .." );
			return;
		}

		if (part instanceof BinaryPart ) {
			log.debug(".. saving binary stuff" );
			p.getTargetPartStore().saveBinaryPart( part );

		} else {
			log.debug(".. saving " );
			saveRawXmlPart( part );
		}
		handled.put(resolvedPartUri, resolvedPartUri);

		// recurse via this parts relationships, if it has any
		RelationshipsPart rrp = part.getRelationshipsPart(false); //don't create
		if (rrp!= null ) {
			// Only save it if it actually has rels in it
			if (rrp.getRelationships().getRelationship().size()>0) {
				saveRawXmlPart(rrp);
				addPartsFromRelationships(rrp);
			}
		}
	}
}
