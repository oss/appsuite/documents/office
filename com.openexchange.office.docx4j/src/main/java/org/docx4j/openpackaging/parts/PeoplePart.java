/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.w15.CTPeople;

public class PeoplePart extends JaxbXmlPart<CTPeople> {

	 /**
	 */
	public PeoplePart(PartName partName) {
		super(partName);
		init();
	}

	public PeoplePart() throws InvalidFormatException {
		super(new PartName("/word/people.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
		    org.docx4j.openpackaging.contenttype.ContentTypes.WORDPROCESSINGML_PEOPLE));

		// Used when this Part is added to a rels
		setRelationshipType("http://schemas.microsoft.com/office/2011/relationships/people");
	}

    /**
     * Unmarshal XML data from the specified InputStream and return the
     * resulting content tree.  Validation event location information may
     * be incomplete when using this form of the unmarshal API.
     *
     * <p>
     * Implements <a href="#unmarshalGlobal">Unmarshal Global Root Element</a>.
     *
     * @param is the InputStream to unmarshal XML data from
     * @return the newly created root object of the java content tree
     *
     * @throws JAXBException
     *     If any unexpected errors occur while unmarshalling
     */
	@Override
    public CTPeople unmarshal( java.io.InputStream is ) {
		try {
			setJAXBContext(jc);

			log.debug("unmarshalling " + this.getClass().getName());

			jaxbElement = ((JAXBElement<CTPeople>) XmlUtils.unmarshal(is, jc, new org.docx4j.jaxb.JaxbValidationEventHandler(getPackage()))).getValue();

		} catch (Exception e ) {
			e.printStackTrace();
		}

		return jaxbElement;

    }
}


