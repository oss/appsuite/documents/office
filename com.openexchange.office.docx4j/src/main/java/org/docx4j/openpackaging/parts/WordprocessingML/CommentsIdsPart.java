
package org.docx4j.openpackaging.parts.WordprocessingML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.w16cid.CTCommentId;
import org.docx4j.w16cid.CommentsIds;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;

public final class CommentsIdsPart extends DocumentPart<CommentsIds> implements INodeAccessor<CTCommentId> {
	
	public CommentsIdsPart(PartName partName) {
		super(partName);
		init();
	}

	public CommentsIdsPart() throws InvalidFormatException {
		super(new PartName("/word/commentsIds.xml"));
		init();
	}

	public void init() {		
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
				org.docx4j.openpackaging.contenttype.ContentTypes.WORDPROCESSINGML_COMMENTS_IDS));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.COMMENTS_IDS);
	}

	@Override
    public DLList<CTCommentId> getContent() {

	    if (this.getJaxbElement()==null) {
    		this.setJaxbElement(new CommentsIds());
    	}
    	return this.getJaxbElement().getContent();
    }

    @Override
    protected CommentsIds createDocument() {
        return new CommentsIds();
    }
}
