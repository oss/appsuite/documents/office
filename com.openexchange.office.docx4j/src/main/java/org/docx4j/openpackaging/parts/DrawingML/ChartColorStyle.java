
package org.docx4j.openpackaging.parts.DrawingML;

import org.docx4j.dml.chartStyle2012.CTColorStyle;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

public class ChartColorStyle extends JaxbXmlPart<CTColorStyle> {

    public ChartColorStyle(PartName partName) {
        super(partName);
        init();
    }

    public ChartColorStyle() throws InvalidFormatException {
        super(new PartName("/xl/charts/color1.xml")); // In a .xlsx could be "/xl/charts/chart1.xml"?
        init();
    }

    public void init() {
        // Used if this Part is added to [Content_Types].xml
        setContentType(new org.docx4j.openpackaging.contenttype.ContentType(
            org.docx4j.openpackaging.contenttype.ContentTypes.CHART_COLOR_STYLE));

        // Used when this Part is added to a rels
        setRelationshipType(Namespaces.SPREADSHEETML_CHART_COLOR);
    }
}
