/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.vml.root.Xml;

public final class VMLPart extends JaxbXmlPart<Xml> {

    public VMLPart(PartName partName) {
        super(partName);
        init();
    }

    public VMLPart() throws InvalidFormatException {
        super(new PartName("/xl/drawings/vmlDrawing1.vml"));
        init();
    }

    public void init() {

        // Used if this Part is added to [Content_Types].xml
        setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
                org.docx4j.openpackaging.contenttype.ContentTypes.VML_DRAWING));

        // Used when this Part is added to a rels
        setRelationshipType(Namespaces.VML);
    }

    @Override
    public Xml getJaxbElement() {
        try {
            return super.getJaxbElement();
        }
        catch(Exception e) {
            // DOCS-4418, DOCS-4408, there exist broken documents not having unique "relid" attributes, for Excel documents we will create a new vml part, the root cause string is then
            // something like this: "TransformerException: Attribute "relid" bound to namespace "urn:schemas-microsoft-com:office:office" was already specified for element "v:fill"." 
            final String rootCause = ExceptionUtils.getRootCauseMessage(e);
            if(getPartName().getName().startsWith("/xl/") && rootCause.contains("TransformerException") && rootCause.contains("relid") && rootCause.contains(("v:fill"))) {
                final Xml xml = new Xml();
                this.setJaxbElement(xml);
                return xml;
            }
            throw e;
        }
    }
}
