/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts;

import jakarta.xml.bind.JAXBException;
import org.docx4j.XmlUtils;
import org.docx4j.docProps.extended.Properties;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

public class DocPropsExtendedPart extends JaxbXmlPart<Properties> {

	/*
	 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	 * <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
	 * xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
		 * <Template>Normal.dotm</Template>
		 * <TotalTime>0</TotalTime>
		 * <Pages>1</Pages><Words>3</Words><Characters>20</Characters>
		 * <Application>Microsoft Office Word</Application>
		 * <DocSecurity>0</DocSecurity>
		 * <Lines>1</Lines><Paragraphs>1</Paragraphs>
		 * <ScaleCrop>false</ScaleCrop>
		 * <Company>Plutext Pty Ltd</Company>
		 * <LinksUpToDate>false</LinksUpToDate><CharactersWithSpaces>22</CharactersWithSpaces>
		 * <SharedDoc>false</SharedDoc>
		 * <HyperlinksChanged>false</HyperlinksChanged>
		 * <AppVersion>12.0000</AppVersion>
	 * </Properties>
	 */

	 /**
	 */
	public DocPropsExtendedPart(PartName partName) {
		super(partName);
		init();
	}

	public DocPropsExtendedPart() throws InvalidFormatException {
		super(new PartName("/docProps/app.xml"));
		init();
	}

	public void init() {

		setJAXBContext(Context.getJcDocPropsExtended());

		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.OFFICEDOCUMENT_EXTENDEDPROPERTIES));

		// Used when this Part is added to a rels
		setRelationshipType(Namespaces.PROPERTIES_EXTENDED);
	}


    /**
     * Unmarshal XML data from the specified InputStream and return the
     * resulting content tree.  Validation event location information may
     * be incomplete when using this form of the unmarshal API.
     *
     * <p>
     * Implements <a href="#unmarshalGlobal">Unmarshal Global Root Element</a>.
     *
     * @param is the InputStream to unmarshal XML data from
     * @return the newly created root object of the java content tree
     *
     * @throws JAXBException
     *     If any unexpected errors occur while unmarshalling
     */
	@Override
    public Properties unmarshal( java.io.InputStream is ) {
		try {

		    setJAXBContext(org.docx4j.jaxb.Context.getJcDocPropsExtended());

			log.debug("unmarshalling " + this.getClass().getName() );

			jaxbElement = (Properties)XmlUtils.unmarshal(is, jc, new org.docx4j.jaxb.JaxbValidationEventHandler(getPackage()));

		} catch (Exception e ) {
			e.printStackTrace();
		}
		return jaxbElement;
    }
}
