
package org.docx4j.openpackaging.parts.WordprocessingML;

import java.util.HashSet;
import java.util.List;
import jakarta.xml.bind.Unmarshaller;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.Document;
import org.docx4j.wml.FldChar;
import com.openexchange.office.filter.core.IContentAccessor;

/**
 * @author jharrop
 *
 */
public class MainDocumentPart extends DocumentPart<org.docx4j.wml.Document> implements IContentAccessor<Object>  {

    @SuppressWarnings("serial")
    static HashSet<String> ignorables = new HashSet<String>(1) {{
        add("w:tblDescription");
	}};

	public MainDocumentPart(PartName partName) {
		super(partName);
		init();
	}

	public MainDocumentPart() throws InvalidFormatException {
		super(new PartName("/word/document.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
			org.docx4j.openpackaging.contenttype.ContentTypes.WORDPROCESSINGML_DOCUMENT));

		// Used when this Part is added to a rels
		setRelationshipType(Namespaces.DOCUMENT);
	}

    @Override
    public List<Object> getContent() {

    	if (this.getJaxbElement()==null) {
    		this.setJaxbElement(Context.getWmlObjectFactory().createDocument());
    	}
    	if (this.getJaxbElement().getBody()==null) {
    		this.getJaxbElement().setBody(
   				Context.getWmlObjectFactory().createBody());
    	}
    	return this.getJaxbElement().getContent();
    }

    private class UnmarshalListener extends Unmarshaller.Listener {

        private FldChar currentFldChar = null;

        public UnmarshalListener() {
            super();
        }

        @Override
        public void afterUnmarshal(Object target, Object parent) {
            super.afterUnmarshal(target, parent);

            if(target instanceof FldChar) {
                currentFldChar = ((FldChar)target).updateField(currentFldChar);
            }
        }

        @Override
        public void beforeUnmarshal(Object target, Object parent) {
            super.beforeUnmarshal(target, parent);
        }
    }

    @Override
    public Unmarshaller.Listener createUnmarshalListener() {
        return new UnmarshalListener();
    }

    @Override
    protected Document createDocument() {
        return new Document();
    }
}
