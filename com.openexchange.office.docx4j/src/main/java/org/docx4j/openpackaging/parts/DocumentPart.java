
package org.docx4j.openpackaging.parts;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Map;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Templates;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.IOUtils;
import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.FragmentPrefixMapper;
import org.docx4j.jaxb.JaxbValidationEventHandler;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io3.stores.PartStore;

public abstract class DocumentPart<E extends DocumentSerialization> extends SerializationPart<E> {

    public DocumentPart(PartName partName) {
        super(partName);
        setJAXBContext(Context.getJc());
    }

    public DocumentPart(PartName partName, JAXBContext jc) {
        super(partName);
        setJAXBContext(jc);
    }

    protected JAXBContext jc;

    public void setJAXBContext(JAXBContext jc) {
        this.jc = jc;
    }

    public JAXBContext getJAXBContext() {
        return jc;
    }

    public Unmarshaller.Listener createUnmarshalListener() {
        return null;
    }

    protected E document = null;

    @Override
    public boolean isUnmarshalled() {
        return document!=null;
    }

    protected abstract E createDocument();

    @Override
    public void setJaxbElement(E document) {
        this.document = document;
    }

    @Override
    public E getJaxbElement() {
        InputStream is = null;
        if (document==null) {
            final PartStore partStore = this.getPackage().getSourcePartStore();
            try {
                final String name = this.partName.getName();
                try {
                    if (partStore!=null) {
                        this.setContentLengthAsLoaded(partStore.getPartSize(name.substring(1)));
                    }
                } catch (UnsupportedOperationException uoe) {
                    //
                }
                is = partStore.loadPart(name.substring(1));
                if (is==null) {
                    log.info(name + " missing from part store");
                } else {
                    log.debug("Lazily unmarshalling " + name);
                    unmarshal(is);
                }
            } catch (Docx4JException e) {
                log.error(e.getMessage(), e);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
        return document;
    }

    @Override
    public E unmarshal(java.io.InputStream is) {
        try {
            final XMLInputFactory xif = XMLInputFactory.newFactory();
            xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
            xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
            try {
                final XMLStreamReader reader = xif.createXMLStreamReader(is);
                document = createDocument();
                document.readObject(reader, this);
            }
            catch (Exception ue) {

                Context.abortOnLowMemory(getPackage());
                is.reset();

                final XMLStreamReader reader = xif.createXMLStreamReader(new ByteArrayInputStream(XmlUtils.transformMcPreprocessorXslt(is)));
                document = createDocument();
                document.readObject(reader, this);

                Context.abortOnLowMemory(getPackage());
            }
            return document;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void marshal(java.io.OutputStream os) {
        try {
            getJaxbElement().writeObject(XMLOutputFactory.newFactory().createXMLStreamWriter(os, "UTF-8"), this);
        }
        catch (Exception ue) {
            throw new RuntimeException();
        }
    }

    private JaxbValidationEventHandler jaxbValidationEventHandler;

    public JaxbValidationEventHandler getJaxbValidationEventHandler() {
        if(jaxbValidationEventHandler==null) {
            jaxbValidationEventHandler = new JaxbValidationEventHandler(getPackage());
        }
        return jaxbValidationEventHandler;
    }

    public Unmarshaller getUnmarshaller() throws JAXBException {
        final Unmarshaller unmarshaller = Context.createUnmarshaller(getJAXBContext(), getJaxbValidationEventHandler());
        unmarshaller.setListener(createUnmarshalListener());
        return unmarshaller;
    }

    public Marshaller getFragmentMarshaller(Map<String, String> prefixToUri, Map<String, String> uriToPrefix) throws JAXBException {
        final FragmentPrefixMapper prefixMapper = prefixToUri!=null&&uriToPrefix!=null ? new FragmentPrefixMapper() : null;
        final Marshaller marshaller = Context.createMarshaller(getJAXBContext(), prefixMapper, getJaxbValidationEventHandler());
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        return marshaller;
    }
}
