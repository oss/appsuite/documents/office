
package org.docx4j.openpackaging.parts.DrawingML;

import org.docx4j.dml.chartStyle2012.CTChartStyle;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

public class ChartStyle extends JaxbXmlPart<CTChartStyle> {

    public ChartStyle(PartName partName) {
        super(partName);
        init();
    }

    public ChartStyle() throws InvalidFormatException {
        super(new PartName("/xl/charts/style1.xml")); // In a .xlsx could be "/xl/charts/chart1.xml"?
        init();
    }

    public void init() {
        // Used if this Part is added to [Content_Types].xml
        setContentType(new org.docx4j.openpackaging.contenttype.ContentType(
            org.docx4j.openpackaging.contenttype.ContentTypes.CHART_STYLE));

        // Used when this Part is added to a rels
        setRelationshipType(Namespaces.SPREADSHEETML_CHART_STYLE);
    }
}
