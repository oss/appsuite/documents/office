/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.util.JAXBResult;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Templates;
import org.apache.commons.io.IOUtils;
import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.JaxbValidationEventHandler;
import org.docx4j.jaxb.NamespacePrefixMapperUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io3.stores.PartStore;
import com.openexchange.office.filter.core.Tools;

/** OPC Parts are either XML, or binary (or text) documents.
 *
 *  Most are XML documents.
 *
 *  docx4j aims to represent XML parts using JAXB.
 *
 *  Any XML Part for which we have a JAXB representation (eg the main
 *  document part) should extend this Part.
 *
 *  This class provides only one of the methods for serializing (marshalling) the
 *  Java content tree back into XML data found in
 *  jakarta.xml.bind.Marshaller interface.  You can always use
 *  any of the others by getting the jaxbElement required by those
 *  methods.
 *
 *  Insofar as unmarshalling is concerned, at present it doesn't
 *  contain all the methods in jakarta.xml.bind.unmarshaller interface.
 *  This is because the content always comes from the same place
 *  (ie from a zip file or JCR via org.docx4j.io.*).
 *  TODO - what is the best thing to unmarshall from?
 *
 *  @param <E> type of the content tree object
 * */
public abstract class JaxbXmlPart<E> extends SerializationPart<E> {

    public JaxbXmlPart(PartName partName) {
        super(partName);
        setJAXBContext(Context.getJc());
    }

    public JaxbXmlPart(PartName partName, JAXBContext jc) {
        super(partName);
        setJAXBContext(jc);
    }

    protected JAXBContext jc;
    public void setJAXBContext(JAXBContext jc) {
        this.jc = jc;
    }
    /**
     * @since 2.7
     */
    public JAXBContext getJAXBContext() {
        return jc;
    }

    /** The content tree (ie JAXB representation of the Part) */
    protected E jaxbElement = null;

    @Override
    public E getJaxbElement() {

        // Lazy unmarshal
        InputStream is = null;
        if (jaxbElement==null) {
            PartStore partStore = this.getPackage().getSourcePartStore();
            try {
                String name = this.partName.getName();

                try {
                    if (partStore!=null) {
                        this.setContentLengthAsLoaded(
                                partStore.getPartSize( name.substring(1)));
                    }
                } catch (UnsupportedOperationException uoe) {
                    //
                }

                is = partStore.loadPart(name.substring(1));
                if (is==null) {
                    log.info(name + " missing from part store");
                } else {
                    log.debug("Lazily unmarshalling " + name);
                    // System.out.println("JaxbXmlPart.getJaxbElement() " + name + " " + this.getClass().getSimpleName());
                    unmarshal( is );
                }
            } catch (Docx4JException e) {
                log.error(e.getMessage(), e);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
        return jaxbElement;
    }

    @Override
    public void setJaxbElement(E jaxbElement) {
        this.jaxbElement = jaxbElement;
    }

    protected Unmarshaller.Listener createUnmarshalListener() {
        return null;
    }

    /**
     * See your content as XML.  An easy way to invoke XmlUtils.marshaltoString
     *
     * @return
     * @since 3.0.0
     */
    public String getXML() {
        return XmlUtils.marshaltoString(getPackage(), getJaxbElement(), true, true, jc );
    }

    @Override
    public boolean isUnmarshalled(){
        return jaxbElement!=null;
    }

    private JaxbValidationEventHandler jaxbValidationEventHandler;

    public JaxbValidationEventHandler getJaxbValidationEventHandler() {
        if(jaxbValidationEventHandler==null) {
            jaxbValidationEventHandler = new JaxbValidationEventHandler(getPackage());
        }
        return jaxbValidationEventHandler;
    }

    /**
     * Marshal the content tree rooted at <tt>jaxbElement</tt> into an output
     * stream, using org.docx4j.jaxb.NamespacePrefixMapper.
     *
     * @param os
     *            XML will be added to this stream.
     *
     * @throws JAXBException
     *             If any unexpected problem occurs during the marshalling.
     */
    @Override
    public void marshal(OutputStream os) throws JAXBException {

        marshal(os, NamespacePrefixMapperUtils.getPrefixMapper());
    }

    /**
     * Marshal the content tree rooted at <tt>jaxbElement</tt> into an output
     * stream
     *
     * @param os
     *            XML will be added to this stream.
     * @param namespacePrefixMapper
     *            namespacePrefixMapper
     *
     * @throws JAXBException
     *             If any unexpected problem occurs during the marshalling.
     */
    public void marshal(OutputStream os, org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper namespacePrefixMapper) throws JAXBException {

        try {
            final Marshaller marshaller = Context.createMarshaller(jc, namespacePrefixMapper, getJaxbValidationEventHandler());
            getJaxbElement();
            if (jaxbElement==null) {
                log.error("No JAXBElement has been created for this part, yet!");
                throw new JAXBException("No JAXBElement has been created for this part, yet!");
            }
            marshaller.marshal(jaxbElement, os);

        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Unmarshal XML data from the specified InputStream and return the
     * resulting content tree. Validation event location information may be
     * incomplete when using this form of the unmarshal API.
     *
     * <p>
     * Implements <a href="#unmarshalGlobal">Unmarshal Global Root Element</a>.
     *
     * @param is
     *            the InputStream to unmarshal XML data from
     * @return the newly created root object of the java content tree
     *
     * @throws JAXBException
     *             If any unexpected errors occur while unmarshalling
     * @throws XMLStreamException
     */

    @Override
    public E unmarshal(InputStream is) {
        try {
            if((Tools.junitTestParam&1)!=0) {
                return unmarshal2(is);
            }
            final JaxbValidationEventHandler eventHandler = getJaxbValidationEventHandler();
            eventHandler.setContinue(false);
            final XMLInputFactory xif = XMLInputFactory.newFactory();
            xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
            xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
            try {
                final XMLStreamReader reader = xif.createXMLStreamReader(is);
                final Unmarshaller unmarshaller = Context.createUnmarshaller(jc, eventHandler);
                unmarshaller.setListener(createUnmarshalListener());
                jaxbElement = (E)XmlUtils.unwrap(unmarshaller.unmarshal(reader));
            }
            catch (Exception ue) {

                Context.abortOnLowMemory(getPackage());
                eventHandler.setContinue(true);
                is.reset();

                if(ue instanceof UnmarshalException) {
                    final Throwable linkedException = ((UnmarshalException)ue).getLinkedException();
                    if(linkedException instanceof XMLStreamException) {
                        final String message = linkedException.getMessage();
                        if(message != null && message.contains("The element type \"br\" must be terminated by the matching end-tag")) {
                            final String result = IOUtils.toString(is, StandardCharsets.UTF_8);
                            is = new ByteArrayInputStream(result.replace("<br>", "<br/>").getBytes(StandardCharsets.UTF_8));
                        }
                    }
                }

                final XMLStreamReader reader = xif.createXMLStreamReader(new ByteArrayInputStream(XmlUtils.transformMcPreprocessorXslt(is)));
                jaxbElement = (E)XmlUtils.unwrap(Context.createUnmarshaller(jc, eventHandler).unmarshal(reader));

                Context.abortOnLowMemory(getPackage());
            }
            return jaxbElement;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public E unmarshal2(java.io.InputStream is) {
        try {
            JaxbValidationEventHandler eventHandler = getJaxbValidationEventHandler();
            eventHandler.setContinue(false);
            try {
                org.w3c.dom.Document doc = XmlUtils.getDocumentBuilderFactory().newDocumentBuilder().parse(is);
                final Unmarshaller unmarshaller = Context.createUnmarshaller(jc, eventHandler);
                unmarshaller.setListener(createUnmarshalListener());
                jaxbElement = (E) XmlUtils.unwrap(unmarshaller.unmarshal(doc));
            }
            catch (Exception ue) {

                Context.abortOnLowMemory(getPackage());
                // When reading from zip, we use a ByteArrayInputStream,
                // which does support this.
                log.info("encountered unexpected content; pre-processing");
                eventHandler.setContinue(true);

                try {
                    Templates mcPreprocessorXslt = JaxbValidationEventHandler.getMcPreprocessor();
                    is.reset();
                    JAXBResult result = XmlUtils.prepareJAXBResult(getPackage(), jc);
                    XmlUtils.transform(is,
                            mcPreprocessorXslt, null, result);
                    jaxbElement = (E) XmlUtils.unwrap(result.getResult());
                } catch (Exception e) {
                    throw new JAXBException("Preprocessing exception", e);
                }
            }
            return jaxbElement;

        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
