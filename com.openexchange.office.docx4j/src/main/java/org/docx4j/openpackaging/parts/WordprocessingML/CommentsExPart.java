
package org.docx4j.openpackaging.parts.WordprocessingML;

import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocumentPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.w15.CTCommentEx;
import org.docx4j.w15.CTCommentsEx;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;

public final class CommentsExPart extends DocumentPart<CTCommentsEx> implements INodeAccessor<CTCommentEx> {

	public CommentsExPart(PartName partName) {
		super(partName);
		init();
	}

	public CommentsExPart() throws InvalidFormatException {
		super(new PartName("/word/commentsExtended.xml"));
		init();
	}

	public void init() {		
		// Used if this Part is added to [Content_Types].xml 
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType( 
		    org.docx4j.openpackaging.contenttype.ContentTypes.WORDPROCESSINGML_COMMENTS_EX));

		// Used when this Part is added to a rels 
		setRelationshipType(Namespaces.COMMENTS_EX);
	}

	@Override
    public DLList<CTCommentEx> getContent() {

    	if (this.getJaxbElement()==null) {
    		this.setJaxbElement(new CTCommentsEx());
    	}
    	return this.getJaxbElement().getContent();
    }

    @Override
    protected CTCommentsEx createDocument() {
        return new CTCommentsEx();
    }
}
