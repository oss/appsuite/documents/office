
package org.docx4j.openpackaging.parts.WordprocessingML;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import org.docx4j.XmlUtils;
import org.docx4j.bibliography.CTSourceType;
import org.docx4j.bibliography.CTSources;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;

/**
 * @since 2.7
 */
public class BibliographyPart extends JaxbXmlPart<JAXBElement<org.docx4j.bibliography.CTSources>> {

	public BibliographyPart() throws InvalidFormatException {
		super(new PartName("/customXml/item1.xml"));
	}

	public BibliographyPart(PartName partName) {
		super(partName);
	}

	public BibliographyPart(PartName partName, JAXBContext jc) {
		super(partName, jc);
	}

	public void importSources(BibliographyPart otherPart) {

		org.docx4j.bibliography.CTSources ourSources = (CTSources)XmlUtils.unwrap(this.getJaxbElement());

		org.docx4j.bibliography.CTSources otherSourcesTmp = (CTSources)XmlUtils.unwrap(otherPart.getJaxbElement());		
		org.docx4j.bibliography.CTSources otherSourcesCloned = XmlUtils.deepCopy(otherSourcesTmp, getPackage());

		for (CTSourceType sourceType : otherSourcesCloned.getSource()) {
		
			// TODO duplicate detection.

			ourSources.getSource().add(sourceType);
		}
	}

}
