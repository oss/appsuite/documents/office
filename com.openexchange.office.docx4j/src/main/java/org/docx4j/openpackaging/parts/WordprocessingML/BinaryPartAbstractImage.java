/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *   
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License"); 
    you may not use this file except in compliance with the License. 

    You may obtain a copy of the License at 

        http://www.apache.org/licenses/LICENSE-2.0 

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
    See the License for the specific language governing permissions and 
    limitations under the License.

 */
package org.docx4j.openpackaging.parts.WordprocessingML;

import org.docx4j.openpackaging.Base;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.ExternalTarget;
import org.docx4j.openpackaging.parts.PartName;

public abstract class BinaryPartAbstractImage extends BinaryPart {

	final static String IMAGE_DIR_PREFIX = "/word/media/";
	final static String IMAGE_NAME_PREFIX = "image";

	public BinaryPartAbstractImage(PartName partName) {
		super(partName);
		
		// Can't setContentType or setRelationshipType, since 
		// these will differ depending on the nature of the data.
		// Common binary parts should extend this class to 
		// provide that information.
	
		this.getOwningRelationshipPart();
	}

	public BinaryPartAbstractImage(ExternalTarget externalTarget) {
		super(externalTarget);
	}

    /**
     * @param opcPackage
     * @param sourcePart
     * @param proposedRelId
     * @param ext extension eg png
     * @return
     */
    public static String createImageName(OpcPackage opcPackage, Base sourcePart, String proposedRelId, String ext) {
		
		if (opcPackage instanceof WordprocessingMLPackage) {		
			return PartName.generateUniqueName(sourcePart, proposedRelId, 
					IMAGE_DIR_PREFIX, IMAGE_NAME_PREFIX, ext);
		} else if (opcPackage instanceof PresentationMLPackage) {		
			return PartName.generateUniqueName(sourcePart, proposedRelId, 
					"/ppt/media/", IMAGE_NAME_PREFIX, ext);
		} else if (opcPackage instanceof SpreadsheetMLPackage) {		
			return PartName.generateUniqueName(sourcePart, proposedRelId, 
					"/xl/media/", IMAGE_NAME_PREFIX, ext);
		} else {
			// Shouldn't happen
			return PartName.generateUniqueName(sourcePart, proposedRelId, 
					IMAGE_DIR_PREFIX, IMAGE_NAME_PREFIX, ext);			
		}
	}
}
