
package org.docx4j.openpackaging.parts;

import jakarta.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

public interface IDocumentSerialization {

    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws JAXBException, XMLStreamException;

    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException;
}
