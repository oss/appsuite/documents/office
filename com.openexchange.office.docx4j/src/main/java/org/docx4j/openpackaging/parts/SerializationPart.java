
package org.docx4j.openpackaging.parts;

import jakarta.xml.bind.JAXBException;

public abstract class SerializationPart<E> extends Part {

    public SerializationPart(PartName partName) {
        super(partName);
    }

    public abstract boolean isUnmarshalled();

    public abstract void setJaxbElement(E jaxbElement);

    public abstract E getJaxbElement();

    public abstract E unmarshal(java.io.InputStream is);

    public abstract void marshal(java.io.OutputStream os) throws JAXBException;
}
