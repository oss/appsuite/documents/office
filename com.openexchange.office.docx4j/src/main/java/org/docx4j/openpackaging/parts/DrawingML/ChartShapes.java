/**
 * @author sven.jacobi@open-xchange.com
 */
package org.docx4j.openpackaging.parts.DrawingML;

import org.docx4j.dml.spreadsheetDrawing.CTDrawing;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

public class ChartShapes extends JaxbXmlPart<CTDrawing> {

	public ChartShapes(PartName partName) {
		super(partName);
		init();
	}

	public ChartShapes() throws InvalidFormatException {
		super(new PartName("/xl/drawings/drawing1.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.DRAWINGML_CHART_SHAPES));

		// Used when this Part is added to a rels
		setRelationshipType(Namespaces.SPREADSHEETML_CHART_SHAPES);
	}
}