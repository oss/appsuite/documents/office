/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts;

import org.docx4j.docProps.coverPageProps.CoverPageProperties;
import org.docx4j.jaxb.Context;

public class DocPropsCoverPagePart extends JaxbXmlPart<CoverPageProperties> {

	/*
		<CoverPageProperties xmlns="http://schemas.microsoft.com/office/2006/coverPageProps">
		  <PublishDate/>
		  <Abstract>This document is about ...</Abstract>
		  <CompanyAddress/>
		  <CompanyPhone/>
		  <CompanyFax/>
		  <CompanyEmail/>
		</CoverPageProperties>
	 */

	public DocPropsCoverPagePart(PartName partName) {
		super(partName);
		init();
	}


	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
			org.docx4j.openpackaging.contenttype.ContentTypes.OFFICEDOCUMENT_CUSTOMXML_DATASTORAGE));

		// Used when this Part is added to a rels
		setRelationshipType("http://schemas.microsoft.com/\u201a\u00c4\u00e5office/\u201a\u00c4\u00e52006/\u201a\u00c4\u00e5coverPageProps"); // hardcoding Namespaces.PROPERTIES_COVERPAGE for ease of integration in MergeDocx

		setJAXBContext(Context.getJc());
	}
}


