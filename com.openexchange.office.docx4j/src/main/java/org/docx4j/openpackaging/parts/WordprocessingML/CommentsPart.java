/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.parts.WordprocessingML;

import java.util.Iterator;
import java.util.List;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.JaxbXmlPart;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.w15.CTCommentEx;
import org.docx4j.w15.CTCommentsEx;
import org.docx4j.wml.Comments;
import org.docx4j.wml.Comments.Comment;
import com.openexchange.office.filter.core.IContentAccessor;

public final class CommentsPart extends JaxbXmlPart<Comments> implements IContentAccessor<Object> {

	public CommentsPart(PartName partName) {
		super(partName);
		init();
	}

	public CommentsPart() throws InvalidFormatException {
		super(new PartName("/word/comments.xml"));
		init();
	}

	public void init() {
		// Used if this Part is added to [Content_Types].xml
		setContentType(new  org.docx4j.openpackaging.contenttype.ContentType(
				org.docx4j.openpackaging.contenttype.ContentTypes.WORDPROCESSINGML_COMMENTS));

		// Used when this Part is added to a rels
		setRelationshipType(Namespaces.COMMENTS);
	}

    @Override
    public List<Object> getContent() {

    	if (this.getJaxbElement()==null) {
    		this.setJaxbElement(Context.getWmlObjectFactory().createComments());
    	}
    	return this.getJaxbElement().getContent();
    }

    public void mergeParaIdToComment() {
        final Comments comments = getJaxbElement();
        for(Comment comment:comments.getComments()) {
            final Integer paraId = comment.getParaIdFromParagraph();
            if(paraId!=null) {
                comment.setParaId(paraId);
            }
        }
    }

    public void mergeCommentsExPart(CommentsExPart commentsExPart) {
        final Comments comments = getJaxbElement();
        final CTCommentsEx commentsEx = commentsExPart.getJaxbElement();
        final Iterator<CTCommentEx> commentExIter = commentsEx.getContent().iterator();
        while(commentExIter.hasNext()) {
            final CTCommentEx commentEx = commentExIter.next();
            final Integer paraIdParent = commentEx.getParaIdParent();
            if(paraIdParent!=null) {
                final Comment parent = comments.getCommentByParaId(paraIdParent);
                final Comment child = comments.getCommentByParaId(commentEx.getParaId());
                if(parent!=null&&child!=null) {
                    child.setParentId(parent.getId());
                }
            }
        }
    }
}
