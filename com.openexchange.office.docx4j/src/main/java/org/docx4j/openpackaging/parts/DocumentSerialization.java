/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package org.docx4j.openpackaging.parts;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.docx4j.openpackaging.parts.relationships.Namespaces;

@XmlAccessorType(XmlAccessType.NONE)
public abstract class DocumentSerialization implements IDocumentSerialization {

	// namespaces loaded from existing document (documents created with new will have empty maps) 
    final protected Map<String, String> prefixToUri = new HashMap<String, String>();
    final protected Map<String, String> uriToPrefix = new HashMap<String, String>();

    final protected QName rootElement;
    protected String version = "1.0";

    private Boolean isStandalone = Boolean.TRUE;
    private String characterEncodingScheme = null;

    // namespaces we want to be part of the xmlns declaration in the header of the document,
    // this is initialized with Standard_DOCX_Namespaces normally.
    protected String[] namespaces;

    // prefix of namespaces that can be ignored, this is initialized with Standard_DOCX_Ignorables normally.
    final protected Set<String> ignorables = new HashSet<String>();

    public DocumentSerialization(QName rootElement, String[] namespaces, String _ignorables) {
        this.rootElement = rootElement;
        this.namespaces = namespaces;
        addStringArray(ignorables, _ignorables);
    }

    /* reads the root element of the document. Used prefixes and uris are saved and
     * will be written 1:1 when calling writeObject, this solves the problem we
     * had with prefixes used in the ignorable attribute and the corresponding uris. */

    @SuppressWarnings("unused")
    @Override
    public void readObject(XMLStreamReader reader, DocumentPart<?> documentPart) throws XMLStreamException, JAXBException {

        final String v = reader.getVersion();
        if(v!=null) {
            version = v;
        }
        if(reader.standaloneSet()) {
            isStandalone = Boolean.valueOf(reader.isStandalone());
        }
        characterEncodingScheme = reader.getCharacterEncodingScheme();

        int tag = reader.nextTag();
        int namespaceCount = reader.getNamespaceCount();
        for(int i=0; i<namespaceCount; i++) {
            String prefix = reader.getNamespacePrefix(i);
            if(prefix==null) {
                prefix = "xmlns";
            }
            prefixToUri.put(prefix, reader.getNamespaceURI(i));
            uriToPrefix.put(reader.getNamespaceURI(i), prefix);
        }
        addStringArray(ignorables, reader.getAttributeValue(Namespaces.MARKUP_COMPATIBILITY, "Ignorable"));
    }

    static final public String[] Standard_DOCX_Namespaces = {
        "r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
        "v", "urn:schemas-microsoft-com:vml",
        "w10", "urn:schemas-microsoft-com:office:word",
        "w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main",
        "sl", "http://schemas.openxmlformats.org/schemaLibrary/2006/main",
        "w15", "http://schemas.microsoft.com/office/word/2012/wordml",
        "w16cid", "http://schemas.microsoft.com/office/word/2016/wordml/cid",
        "w16se", "http://schemas.microsoft.com/office/word/2015/wordml/symex",
        "w14", "http://schemas.microsoft.com/office/word/2010/wordml",
        "wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing",
        "m", "http://schemas.openxmlformats.org/officeDocument/2006/math",
        "o", "urn:schemas-microsoft-com:office:office",
        "xsi", "http://www.w3.org/2001/XMLSchema-instance",
        "dsp", "http://schemas.microsoft.com/office/drawing/2008/diagram",
        "pic", "http://schemas.openxmlformats.org/drawingml/2006/picture",
        "dgm", "http://schemas.openxmlformats.org/drawingml/2006/diagram",
        "xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing",
        "c", "http://schemas.openxmlformats.org/drawingml/2006/chart",
        "a", "http://schemas.openxmlformats.org/drawingml/2006/main",
        "wne", "http://schemas.microsoft.com/office/word/2006/wordml",
        "wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing",
        "wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape",
        "cx1", "http://schemas.microsoft.com/office/drawing/2014/chartex",
        "cx2", "http://schemas.microsoft.com/office/drawing/2015/10/21/chartex",
        "cx4", "http://schemas.microsoft.com/office/drawing/2016/5/10/chartex",
        "wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas",
        "wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup",
        "a14", "http://schemas.microsoft.com/office/drawing/2010/main",
        "", "http://schemas.openxmlformats.org/drawingml/2006/lockedCanvas",
        "", "http://schemas.openxmlformats.org/drawingml/2006/compatibility",
        "", "http://schemas.openxmlformats.org/officeDocument/2006/bibliography",
        "", "http://schemas.microsoft.com/office/2006/coverPageProps",
        "", "http://schemas.microsoft.com/office/drawing/2012/chartStyle",
        "", "http://schemas.openxmlformats.org/drawingml/2006/chartDrawing",
        "", "urn:schemas-microsoft-com:office:excel",
        "", "urn:schemas-microsoft-com:office:powerpoint"
    };

    static final public String Standard_DOCX_Ignorables = "w14 w15 w16se w16cid wp14";

    @SuppressWarnings("unused")
    @Override
    public void writeObject(XMLStreamWriter writer, DocumentPart<?> documentPart)
        throws XMLStreamException, JAXBException {

        writer.writeStartDocument("UTF-8", version);
        writer.writeStartElement(rootElement.getPrefix(), rootElement.getLocalPart(), rootElement.getNamespaceURI());

        final Iterator<Entry<String, String>> namespaceIter = prefixToUri.entrySet().iterator();
        while(namespaceIter.hasNext()) {
            final Entry<String, String> namespace = namespaceIter.next();
            final String u = namespace.getValue();
            final String p = namespace.getKey();
            if(p.equals("xmlns")) {
                writer.writeDefaultNamespace(u);
            }
            else {
                writer.writeNamespace(p, u);
            }
        }
        if(!prefixToUri.containsKey("xmlns")) {
            writer.writeDefaultNamespace("");
        }

        // So far only namespaces and prefixes of "prefixToUri" and "uriToPrefix" were serialized (loaded
        // from existing documents). Also adding everything from "namespaces" now.
        int nsPrefixNumber = 1;
        for(int i=0;i<namespaces.length;i++) {
            final String prefix = namespaces[i++];
            final String namespace = namespaces[i];

            final String currentPrefix = uriToPrefix.get(namespace);
            if(currentPrefix == null) {
                if(!prefix.isEmpty()) {
                    writer.writeNamespace(prefix, namespace);
                }
                else {
                    while(true) {
                        final String nsPrefix = "ns" + Integer.valueOf(nsPrefixNumber++).toString();
                        if(!prefixToUri.containsKey(nsPrefix)) {
                            writer.writeNamespace(nsPrefix, namespace);
                            break;
                        }
                    }
                }
            }
            else if(!currentPrefix.equals(prefix)) {	// same namespace but different prefixes
            	if(currentPrefix.isEmpty()) {
                    writer.writeNamespace(prefix, namespace);
            	}
            	else if(ignorables.contains(prefix)) {
            		ignorables.remove(prefix);
            		ignorables.add(currentPrefix);
            	}
            }
        }
        if(writer.getPrefix(Namespaces.MARKUP_COMPATIBILITY)==null) {
            writer.writeNamespace("mc", Namespaces.MARKUP_COMPATIBILITY);
        }
        if(!ignorables.isEmpty()) {
            writer.writeAttribute(Namespaces.MARKUP_COMPATIBILITY, "Ignorable", toStringArray(ignorables));
        }
    }

    private void addStringArray(Set<String> set, String stringArray) {
        if(stringArray!=null) {
            for(String a : stringArray.split(" ", -1)) {
                set.add(a);
            }
        }
    }

    private String toStringArray(Set<String> set) {
        if(set.isEmpty()) {
            return null;
        }
        final StringBuilder builder = new StringBuilder(32);
        Iterator<String> iter = set.iterator();
        while(iter.hasNext()) {
            if(builder.length()!=0) {
                builder.append(' ');
            }
            builder.append(iter.next());
        }
        return builder.toString();
    }
}
