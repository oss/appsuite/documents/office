
package org.docx4j.openpackaging;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;

import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;

public class PackageRelsUtil {  
	
	public static String getNameOfMainPart(RelationshipsPart packageRels) throws Docx4JException  {
		
		// find rel of type officeDocument
		for (Relationship rel : packageRels.getRelationships().getRelationship() ) {
			
			if (rel.getType().equals(
					"http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument") ) {
				return rel.getTarget();
			}
			else if(rel.getType().equals("http://purl.oclc.org/ooxml/officeDocument/relationships/officeDocument")) {
				throw new FilterException("", ErrorCode.STRICT_OOXML_NOT_SUPPORTED);
			}
		}
		throw new Docx4JException("No relationship of type officeDocument");
	}
	
}
