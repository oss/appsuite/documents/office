/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
/*
 * Portions Copyright (c) 2006, Wygwam
 * With respect to those portions:
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Neither the name of Wygwam nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.docx4j.openpackaging.contenttype;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import org.docx4j.XmlUtils;
import org.docx4j.dml.chartex2014.ChartExPart;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.NamespacePrefixMapperUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.exceptions.PartUnrecognisedException;
import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.CustomXmlDataStoragePropertiesPart;
import org.docx4j.openpackaging.parts.DefaultXmlPart;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsCustomPart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.PeoplePart;
import org.docx4j.openpackaging.parts.ThemePart;
import org.docx4j.openpackaging.parts.VMLPart;
import org.docx4j.openpackaging.parts.DrawingML.Chart;
import org.docx4j.openpackaging.parts.DrawingML.ChartColorStyle;
import org.docx4j.openpackaging.parts.DrawingML.ChartShapes;
import org.docx4j.openpackaging.parts.DrawingML.ChartStyle;
import org.docx4j.openpackaging.parts.DrawingML.DiagramDrawingPart;
import org.docx4j.openpackaging.parts.DrawingML.Drawing;
import org.docx4j.openpackaging.parts.PresentationML.AuthorsPart;
import org.docx4j.openpackaging.parts.PresentationML.CommentAuthorsPart;
import org.docx4j.openpackaging.parts.PresentationML.CommentListPart;
import org.docx4j.openpackaging.parts.PresentationML.HandoutMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.ModernCommentsPart;
import org.docx4j.openpackaging.parts.PresentationML.NotesMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.NotesSlidePart;
import org.docx4j.openpackaging.parts.PresentationML.PresentationPropertiesPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideMasterPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.docx4j.openpackaging.parts.PresentationML.TableStylesPart;
import org.docx4j.openpackaging.parts.PresentationML.TagsPart;
import org.docx4j.openpackaging.parts.PresentationML.ViewPropertiesPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.CalcChain;
import org.docx4j.openpackaging.parts.SpreadsheetML.ChartsheetPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.ConnectionsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.ExternalLinkPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.MetadataPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.PersonsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.PivotCacheDefinition;
import org.docx4j.openpackaging.parts.SpreadsheetML.PivotCacheRecords;
import org.docx4j.openpackaging.parts.SpreadsheetML.PivotTable;
import org.docx4j.openpackaging.parts.SpreadsheetML.PrinterSettings;
import org.docx4j.openpackaging.parts.SpreadsheetML.QueryTablePart;
import org.docx4j.openpackaging.parts.SpreadsheetML.SharedStrings;
import org.docx4j.openpackaging.parts.SpreadsheetML.SpreadsheetCommentsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.Styles;
import org.docx4j.openpackaging.parts.SpreadsheetML.TablePart;
import org.docx4j.openpackaging.parts.SpreadsheetML.ThreadedCommentsPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorkbookPart;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.docx4j.openpackaging.parts.WordprocessingML.AlternativeFormatInputPart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsExPart;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsIdsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.CommentsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.DocumentSettingsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.EmbeddedPackagePart;
import org.docx4j.openpackaging.parts.WordprocessingML.EndnotesPart;
import org.docx4j.openpackaging.parts.WordprocessingML.FontTablePart;
import org.docx4j.openpackaging.parts.WordprocessingML.FooterPart;
import org.docx4j.openpackaging.parts.WordprocessingML.FootnotesPart;
import org.docx4j.openpackaging.parts.WordprocessingML.GlossaryDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MetafileEmfPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MetafileWmfPart;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.ObfuscatedFontPart;
import org.docx4j.openpackaging.parts.WordprocessingML.OleObjectBinaryPart;
import org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart;
import org.docx4j.openpackaging.parts.WordprocessingML.VbaDataPart;
import org.docx4j.openpackaging.parts.WordprocessingML.VbaProjectBinaryPart;
import org.docx4j.openpackaging.parts.WordprocessingML.WebSettingsPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.relationships.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manage package content types ([Content_Types].xml ) .
 *
 * @author Julien Chable
 * @version 1.0
 */
public class ContentTypeManager  {

	protected static Logger log = LoggerFactory.getLogger(ContentTypeManager.class);

	/**
	 * Content type part name.
	 */
	public static final String CONTENT_TYPES_PART_NAME = "[Content_Types].xml";

	/**
	 * Content type namespace
	 */
	public static final String TYPES_NAMESPACE_URI = "http://schemas.openxmlformats.org/package/2006/content-types";

	/**
	 * Default content type tree. <Extension, ContentType>
	 */
	private TreeMap<String, CTDefault> defaultContentType;

    private List<NonPersistContent> nonPersistContentList;

    public class NonPersistContent {

        final public String zipEntryName;
        final public byte[] contentData;

        NonPersistContent(String _zipEntryName, byte[] _contentData) {
            zipEntryName = _zipEntryName;
            contentData = _contentData;
        }
    }
    /**
	 * @return the defaultContentType
	 * @since 2.8.1
	 */
	public TreeMap<String, CTDefault> getDefaultContentType() {
		return defaultContentType;
	}

	/**
	 * Override content type tree.
	 */
	private TreeMap<URI, CTOverride> overrideContentType;

	/**
	 * @return the overrideContentType
	 * @since 2.8.1
	 */
	public TreeMap<URI, CTOverride> getOverrideContentType() {
		return overrideContentType;
	}

	private static ObjectFactory ctFactory = new ObjectFactory();

	public ContentTypeManager()  {
		init();
	}

	private void init() {
		defaultContentType = new TreeMap<String, CTDefault>();
		overrideContentType = new TreeMap<URI, CTOverride>();
        nonPersistContentList = new ArrayList<NonPersistContent>();
	}

//	/**
//	 * Build association extention-> content type (will be stored in
//	 * [Content_Types].xml) for example ContentType="image/png" Extension="png"
//	 *
//	 * @param partUri
//	 *            the uri that will be stored
//	 * @return <b>false</b> if an error occured.
//	 */
//	public void addContentType(PartName partName, String contentType) {
//		boolean defaultCTExists = false;
//		String extension = partName.getExtension();
//		if ((extension.length() == 0)
//				|| (this.defaultContentType.containsKey(extension)
//						&& !(defaultCTExists = this.defaultContentType.containsValue(contentType)))) {
//			this.addOverrideContentType(partName.getURI(), contentType);
//		} else if (!defaultCTExists) {
//			this.addDefaultContentType(extension, contentType);
//		}
//	}

	/**
	 * Add an override content type for a specific part.
	 *
	 * @param partUri
	 *            Uri of the part.
	 * @param contentType
	 *            Content type of the part.
	 */
	public void addOverrideContentType(URI partUri, CTOverride contentType) {
		log.debug("Registered " + partUri.toString() + " of type " + contentType.getContentType() );
		overrideContentType.put(partUri, contentType);
	}

	public void addOverrideContentType(URI partUri, String contentType) {

		CTOverride overrideCT = ctFactory.createCTOverride();
		overrideCT.setPartName( partUri.toASCIIString() );
		overrideCT.setContentType(contentType );

		overrideContentType.put(partUri, overrideCT);
	}

//	public String getOverrideContentType(URI partUri) {
//		return overrideContentType.get(partUri);
//	}


	/* Given a content type, return the Part Name URI is it
	 * overridden by.
	 */
	public URI getPartNameOverridenByContentType(String contentType) {

		// hmm, can there only be one instance of a given
		// content type?

		final Iterator<Entry<URI, CTOverride>> i = overrideContentType.entrySet().iterator();
		while (i.hasNext()) {
			final Entry<URI, CTOverride> e = i.next();
			if (e != null) {
				log.debug("Inspecting " + e.getValue());
				if (e.getValue().getContentType().equals(contentType)) {
					log.debug("Matched!");
					return e.getKey();
				}
			}
		}
		return null;
	}

	/* Return a part of the appropriate sub class */
	public  Part getPart(String partName, Relationship rel) throws URISyntaxException, InvalidFormatException {

		Part p;

		// look for an override
		CTOverride overrideCT = overrideContentType.get(new URI(partName));
		if (overrideCT!=null ) {
			String contentType = overrideCT.getContentType();
			log.debug("Found content type '" + contentType + "' for " + partName);
			 p = newPartForContentType(contentType, new PartName(partName), rel);
			 p.setContentType( new ContentType(contentType) );
			 return p;
		}

		// if there is no override, get use the file extension
		String ext = partName.substring(partName.indexOf(".") + 1).toLowerCase();
		log.debug("Looking at extension '" + ext);
		CTDefault defaultCT = defaultContentType.get(ext);
		if (defaultCT!=null ) {
			String contentType = defaultCT.getContentType();
			log.debug("Found content type '" + contentType + "' for "
							+ partName);
			p = newPartForContentType(contentType, new PartName(partName), rel);
			p.setContentType(new ContentType(contentType));
			return p;
		}

		// otherwise
		log.debug("No content type found for " + partName);
		return null;

	}

	// TODO: should create a switch for contenttye
	public Part newPartForContentType(String contentType, PartName partName, Relationship rel)
		throws InvalidFormatException {

		// TODO - a number of WordML parts aren't listed here!
		if (rel!=null && rel.getType().equals(Namespaces.AF) ) {
			// Could have just passed String relType
			// Null where used from BPAI, and a FlatOpcXmlImporter case.
			// Cases where rel is not available can prepare a suitable dummy

			AlternativeFormatInputPart afip =
				new AlternativeFormatInputPart(partName);
			afip.setContentType(new ContentType(contentType));
			return afip;

		} else if (rel!=null && rel.getType().equals(Namespaces.EMBEDDED_PKG) ) {

			EmbeddedPackagePart epp = new EmbeddedPackagePart(partName);
			epp.setContentType(new ContentType(contentType));
			return epp;

		} else if (rel!=null && rel.getType().equals(Namespaces.OLE_OBJECT) ) {

			OleObjectBinaryPart olePart = new OleObjectBinaryPart(partName);
			olePart.setContentType(new ContentType(contentType));
			return olePart;

		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_DOCUMENT)) {
			return CreateMainDocumentPartObject(partName);
			// how is the main document distinguished from the glossary document?
			// Answer:- Main Document is a Package level relationship target,
			// whereas the Glossary Document is a Part-level target (from the
			// Main Document part)
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_DOCUMENT_MACROENABLED)) {
			return CreateMainDocumentPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_TEMPLATE)) {
			return CreateMainDocumentPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_TEMPLATE_MACROENABLED)) {
			return CreateMainDocumentPartObject(partName);
		} else if (contentType.equals(ContentTypes.PACKAGE_COREPROPERTIES)) {
			return CreateDocPropsCorePartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_CUSTOMPROPERTIES)) {
			return CreateDocPropsCustomPartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_EXTENDEDPROPERTIES)) {
			return CreateDocPropsExtendedPartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_CUSTOMXML_DATASTORAGEPROPERTIES)) {
			return CreateCustomXmlDataStoragePropertiesPartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_FONT)) {
			return CreateObfuscatedFontPartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_OLE_OBJECT)
				|| contentType.equals(ContentTypes.OFFICEDOCUMENT_ACTIVEX_OBJECT)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.OleObjectBinaryPart(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_ACTIVEX_XML_OBJECT)) {
			return new org.docx4j.openpackaging.parts.ActiveXControlXmlPart(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_THEME)) {
			return CreateThemePartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_COMMENTS)) {
			return CreateCommentsPartObject(partName);
        } else if (contentType.equals(ContentTypes.WORDPROCESSINGML_COMMENTS_IDS)) {
            return CreateCommentsIdsPartObject(partName);
        } else if (contentType.equals(ContentTypes.WORDPROCESSINGML_COMMENTS_EX)) {
            return CreateCommentsExPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_ENDNOTES)) {
			return CreateEndnotesPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_FONTTABLE)) {
			return CreateFontTablePartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_FOOTER)) {
			return CreateFooterPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_FOOTNOTES)) {
			return CreateFootnotesPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_GLOSSARYDOCUMENT)) {
			return CreateGlossaryDocumentPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_HEADER)) {
			return CreateHeaderPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_NUMBERING)) {
			return CreateNumberingPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_SETTINGS)) {
			return CreateDocumentSettingsPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_STYLES)) {
			return CreateStyleDefinitionsPartObject( partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_WEBSETTINGS)) {
			return CreateWebSettingsPartObject(partName);
		} else if (contentType.equals(ContentTypes.WORDPROCESSINGML_PEOPLE)) {
			return CreatePeoplePartObject(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_VBA_DATA)) {
			return new VbaDataPart(partName);
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_VBA_PROJECT)) {
			return new VbaProjectBinaryPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_JPEG)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.ImageJpegPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_PNG)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.ImagePngPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_GIF)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.ImageGifPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_TIFF)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.ImageTiffPart(partName);
//		} else if (contentType.equals(ContentTypes.IMAGE_EPS)) {
//			return new org.docx4j.openpackaging.parts.WordprocessingML.ImageEpsPart(new PartName(partName));
		} else if (contentType.equals(ContentTypes.IMAGE_BMP)) {
			return new org.docx4j.openpackaging.parts.WordprocessingML.ImageBmpPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_EMF) || contentType.equals(ContentTypes.IMAGE_EMF2)) {
			return new MetafileEmfPart(partName);
		} else if (contentType.equals(ContentTypes.IMAGE_WMF)) {
			return new MetafileWmfPart(partName);
		} else if (contentType.equals(ContentTypes.VML_DRAWING)) {
			return new VMLPart(partName);
//			return new VMLBinaryPart(new PartName(partName));
		} else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_DRAWING)) {
			return new org.docx4j.openpackaging.parts.DrawingML.DiagramDrawingPart(partName);
		} else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.drawing")
				|| contentType.equals(ContentTypes.CHART_STYLE)
				|| contentType.equals(ContentTypes.CHART_COLOR_STYLE)) {
			try {
				return CreateDmlPartObject(contentType, partName);
			} catch (Exception e) {
				return new BinaryPart(partName);
			}
            } else if (contentType.equals(ContentTypes.DRAWINGML_CHART_EX)) {
            return new ChartExPart(partName);
		} else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.presentationml")
            || ContentTypes.PRESENTATIONML_MAIN_MACROENABLED.equals(contentType)
            || ContentTypes.PRESENTATIONML_TEMPLATE_MACROENABLED.equals(contentType)
            || ContentTypes.PRESENTATIONML_SLIDESHOW.equals(contentType)
            || ContentTypes.PRESENTATIONML_SLIDESHOW_MACROENABLED.equals(contentType)
            || ContentTypes.PRESENTATIONML_MODERN_COMMENTS.equals(contentType)
            || ContentTypes.PRESENTATIONML_AUTHORS.equals(contentType)) {

			try {
				return CreatePmlPartObject(contentType, partName);
			} catch (Exception e) {
                return new BinaryPart(partName);
			}
		} else if (contentType.equals(ContentTypes.SPREADSHEETML_WORKBOOK)
				|| contentType.equals(ContentTypes.SPREADSHEETML_WORKBOOK_MACROENABLED)
				|| contentType.equals(ContentTypes.SPREADSHEETML_TEMPLATE)
				|| contentType.equals(ContentTypes.SPREADSHEETML_TEMPLATE_MACROENABLED)) {
			try {
				return new WorkbookPart(partName);
			} catch (Exception e) {
				return new BinaryPart(partName);
			}

		} else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.spreadsheetml") || contentType.startsWith("application/vnd.ms-excel")) {
			try {
				return CreateSmlPartObject(contentType, partName);
			} catch (Exception e) {
				return new BinaryPart(partName);
			}
		} else if (contentType.equals(ContentTypes.OFFICEDOCUMENT_THEME_OVERRIDE)) {
			return new org.docx4j.openpackaging.parts.DrawingML.ThemeOverridePart(partName);
		} else if (contentType.equals(ContentTypes.DIGITAL_SIGNATURE_XML_SIGNATURE_PART)) {
		    return CreateDefaultXmlPartObject(partName); // return new org.docx4j.openpackaging.parts.digitalsignature.XmlSignaturePart(partName);
		} else if (contentType.equals(ContentTypes.APPLICATION_XML)
				|| "xml".equals(partName.getExtension())) {

            // Rarely (but sometimes) used, owing to OFFICEDOCUMENT_CUSTOMXML_DATASTORAGE above.

			// Simple minded detection of XML content.
			// If it turns out not to be XML, the zip loader
			// will catch the error and load it as a binary part instead.
			log.debug("DefaultPart used for part '" + partName.getName()
					+ "' of content type '" + contentType + "'");
			return CreateDefaultXmlPartObject(partName);
		} else {

			log.info("No subclass found for " + partName.getName() + "; defaulting to binary");
			//throw new PartUnrecognisedException("No subclass found for " + partName + " (content type '" + contentType + "')");
			return new BinaryPart(partName);
		}
	}

    public static Part CreatePmlPartObject(String contentType, PartName partName)
        throws InvalidFormatException, PartUnrecognisedException {

        if (contentType.equals(ContentTypes.PRESENTATIONML_MAIN)
            || contentType.equals(ContentTypes.PRESENTATIONML_TEMPLATE)
            || contentType.equals(ContentTypes.PRESENTATIONML_TEMPLATE_MACROENABLED)
            || contentType.equals(ContentTypes.PRESENTATIONML_MAIN_MACROENABLED)
            || contentType.equals(ContentTypes.PRESENTATIONML_SLIDESHOW)
            || contentType.equals(ContentTypes.PRESENTATIONML_SLIDESHOW_MACROENABLED)) {
            return new MainPresentationPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_SLIDE)) {
            return new SlidePart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_SLIDE_MASTER)) {
            return new SlideMasterPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_SLIDE_LAYOUT)) {
            return new SlideLayoutPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_TABLE_STYLES)) {
            return new TableStylesPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_PRES_PROPS)) {
            return new PresentationPropertiesPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_VIEW_PROPS)) {
            return new ViewPropertiesPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_TAGS)) {
            return new TagsPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_HANDOUT_MASTER)) {
            return new HandoutMasterPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_NOTES_MASTER)) {
            return new NotesMasterPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_NOTES_SLIDE)) {
            return new NotesSlidePart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_COMMENT_AUTHORS)) {
            return new CommentAuthorsPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_AUTHORS)) {
            return new AuthorsPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_COMMENTS)) {
            return new CommentListPart(partName);
        } else if (contentType.equals(ContentTypes.PRESENTATIONML_MODERN_COMMENTS)) {
            return new ModernCommentsPart(partName);
        } else {
            throw new PartUnrecognisedException("No subclass found for "
                    + partName.getName() + " (content type '" + contentType + "')");
        }
    }

    public static Part CreateSmlPartObject(String contentType, PartName partName)
        throws InvalidFormatException, PartUnrecognisedException {

        if (contentType.equals(ContentTypes.SPREADSHEETML_PRINTER_SETTINGS)) {
            return new PrinterSettings(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_STYLES)) {
            return new Styles(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_WORKSHEET)) {
            return new WorksheetPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_CHARTSHEET)) {
            return new ChartsheetPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_CALC_CHAIN)) {
            return new CalcChain(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_SHARED_STRINGS)) {
            return new SharedStrings(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_PIVOT_TABLE)) {
            return new PivotTable(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_PIVOT_CACHE_DEFINITION)) {
            return new PivotCacheDefinition(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_PIVOT_CACHE_RECORDS)) {
            return new PivotCacheRecords(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_COMMENTS)) {
            return new SpreadsheetCommentsPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_CONNECTIONS)) {
            return new ConnectionsPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_TABLE)) {
            return new TablePart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_QUERY_TABLE)) {
            return new QueryTablePart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_EXTERNAL_LINK)) {
            return new ExternalLinkPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_THREADED_COMMENTS)) {
            return new ThreadedCommentsPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_PERSONS)) {
            return new PersonsPart(partName);
        } else if (contentType.equals(ContentTypes.SPREADSHEETML_METADATA)) {
            return new MetadataPart(partName);
        } else {
            throw new PartUnrecognisedException("No subclass found for "
                    + partName.getName() + " (content type '" + contentType + "')");
        }
    }

    public static Part CreateDmlPartObject(String contentType, PartName partName)
        throws InvalidFormatException, PartUnrecognisedException {

        if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_COLORS)) {
            return new org.docx4j.openpackaging.parts.DrawingML.DiagramColorsPart(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_DATA)) {
            return new org.docx4j.openpackaging.parts.DrawingML.DiagramDataPart(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_LAYOUT)) {
            return new org.docx4j.openpackaging.parts.DrawingML.DiagramLayoutPart(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_STYLE)) {
            return new org.docx4j.openpackaging.parts.DrawingML.DiagramStylePart(partName);
//      } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_DRAWING)) {
//          return new org.docx4j.openpackaging.parts.DrawingML.DiagramDrawingPart(new PartName(partName));
        } else if (contentType.equals(ContentTypes.DRAWINGML_DRAWING)) {
            return new Drawing(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_CHART)) {
            return new Chart(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_CHART_SHAPES)) {
            return new ChartShapes(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_LAYOUT_HEADER)) {
            return new org.docx4j.openpackaging.parts.DrawingML.DiagramLayoutHeaderPart(partName);
        } else if (contentType.equals(ContentTypes.CHART_STYLE)) {
            return new ChartStyle(partName);
        } else if(contentType.equals(ContentTypes.CHART_COLOR_STYLE)){
            return new ChartColorStyle(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_CHART_EX)) {
            return new ChartExPart(partName);
        } else if (contentType.equals(ContentTypes.DRAWINGML_DIAGRAM_DRAWING)) {
            return new DiagramDrawingPart(partName);
        }
        else {
            throw new PartUnrecognisedException("No subclass found for "
                    + partName.getName() + " (content type '" + contentType + "')");
        }
    }

	public Part CreateDefaultXmlPartObject(PartName partName)
			throws InvalidFormatException {
		return new DefaultXmlPart(partName);
	}

	public Part CreateMainDocumentPartObject(PartName partName)
			throws InvalidFormatException {
		return new MainDocumentPart(partName);
	}

	public Part CreateStyleDefinitionsPartObject(PartName partName) throws InvalidFormatException {
		return new StyleDefinitionsPart(partName);
	}

	public Part CreateDocumentSettingsPartObject(PartName partName)
			throws InvalidFormatException {
		return new DocumentSettingsPart(partName);
	}

	public Part CreateWebSettingsPartObject(PartName partName)
			throws InvalidFormatException {
		return new WebSettingsPart(partName);
	}

	public Part CreatePeoplePartObject(PartName partName)
			throws InvalidFormatException {
		return new PeoplePart(partName);
	}

	public Part CreateFontTablePartObject(PartName partName)
			throws InvalidFormatException {
		return new FontTablePart(partName);
	}

	public Part CreateThemePartObject(PartName partName)
			throws InvalidFormatException {
		return new ThemePart(partName);
	}

	public Part CreateDocPropsCorePartObject(PartName partName)
			throws InvalidFormatException {
		return new DocPropsCorePart(partName);
	}

	public Part CreateDocPropsExtendedPartObject(PartName partName)
			throws InvalidFormatException {
		return new DocPropsExtendedPart(partName);
	}

	public Part CreateDocPropsCustomPartObject(PartName partName)
			throws InvalidFormatException {
		log.debug("Using DocPropsCustomPart ...");
		return new DocPropsCustomPart(partName);
	}

	public Part CreateCommentsPartObject(PartName partName)
			throws InvalidFormatException {
		return new CommentsPart(partName);
	}

    public Part CreateCommentsIdsPartObject(PartName partName)
            throws InvalidFormatException {
        return new CommentsIdsPart(partName);
    }

    public Part CreateCommentsExPartObject(PartName partName)
            throws InvalidFormatException {
        return new CommentsExPart(partName);
    }

    public Part CreateCustomXmlDataStoragePropertiesPartObject(PartName partName)
			throws InvalidFormatException {
		return new CustomXmlDataStoragePropertiesPart(partName);
	}

	public Part CreateEndnotesPartObject(PartName partName)
			throws InvalidFormatException {
		return new EndnotesPart(partName);
	}

	public Part CreateFooterPartObject(PartName partName)
			throws InvalidFormatException {
		return new FooterPart(partName);
	}

	public Part CreateFootnotesPartObject(PartName partName)
			throws InvalidFormatException {
		return new FootnotesPart(partName);
	}

	public Part CreateGlossaryDocumentPartObject(PartName partName)
			throws InvalidFormatException {
		return new GlossaryDocumentPart(partName);
	}

	public Part CreateHeaderPartObject(PartName partName)
			throws InvalidFormatException {
		return new HeaderPart(partName);
	}

	public Part CreateNumberingPartObject(PartName partName)
			throws InvalidFormatException {
		return new NumberingDefinitionsPart(partName);
	}

	public Part CreateObfuscatedFontPartObject(PartName partName)
			throws InvalidFormatException {
		return new ObfuscatedFontPart(partName);
	}

	/**
	 * Add a content type associated with the specified extension.
	 *
	 * @param extension
	 *            The part name extension to bind to a content type.
	 * @param contentType
	 *            The content type associated with the specified extension.
	 */
	public void addDefaultContentType(String extension, CTDefault contentType) {
		log.debug("Registered " + extension );
		defaultContentType.put(extension.toLowerCase(), contentType);
	}

	public void addDefaultContentType(String extension, String contentType) {

		CTDefault defaultCT = ctFactory.createCTDefault();
		defaultCT.setExtension(extension.toLowerCase());
		defaultCT.setContentType(contentType);

		log.debug("Registered " + extension );
		defaultContentType.put(extension.toLowerCase(), defaultCT);
	}


	/**
	 * Delete a content type based on the specified part name. If the specified
	 * part name is register with an override content type, then this content
	 * type is remove, else the content type is remove in the default content
	 * type list if it exists.
	 *
	 * @param partUri
	 *            The part URI associated with the override content type to
	 *            delete.
	 */
	public void removeContentType(PartName partName) {
		if (partName == null)
			throw new IllegalArgumentException("partName");

		// Override content type
		if (this.overrideContentType != null
				&& (this.overrideContentType.get(partName.getURI()) != null)) {
			this.overrideContentType.remove(partName.getURI());
			return;
		}
		// Default content type
		this.defaultContentType.remove(partName.getExtension().toLowerCase());
	}

	/**
	 * Get the content type for the specified part, if any.
	 *
	 * @param partUri
	 *            The URI part to check.
	 * @return The content type associated with the URI (in case of an override
	 *         content type) or the extension (in case of default content type),
	 *         else <code>null</code>.
	 */
	public String getContentType(PartName partName) {
		if (partName == null)
			throw new IllegalArgumentException("partName");

		if ((this.overrideContentType != null)
				&& this.overrideContentType.containsKey(partName.getURI()))
			return this.overrideContentType.get(partName.getURI()).getContentType();

		String extension = partName.getExtension().toLowerCase();
		if (this.defaultContentType.containsKey(extension))
			return this.defaultContentType.get(extension).getContentType();

		return null;
	}

	/**
	 * Clear all content types.
	 */
	public void clearAll() {
		this.defaultContentType.clear();
		if (this.overrideContentType != null)
			this.overrideContentType.clear();
	}

	/**
	 * Clear all override content types.
	 *
	 */
	public void clearOverrideContentTypes() {
		if (this.overrideContentType != null)
			this.overrideContentType.clear();
	}


	public void parseContentTypesFile(InputStream contentTypes)
		throws InvalidFormatException {

		CTTypes types;

		try {
			log.debug("unmarshalling " + this.getClass().getName() );

			Object res = XmlUtils.unwrap(XmlUtils.unmarshal(contentTypes, Context.getJcContentTypes(), new org.docx4j.jaxb.JaxbValidationEventHandler(null)));
			//types = (CTTypes)((JAXBElement)res).getValue();
			types = (CTTypes)res;
			//log.debug( types.getClass().getName() + " unmarshalled" );

			if (log.isDebugEnabled()) {
				XmlUtils.marshaltoString(null, res, true, true, Context.getJcContentTypes());
			}

			CTDefault defaultCT;
			CTOverride overrideCT;
			for(Object o : types.getDefaultOrOverride() ) {

				if (o instanceof CTDefault) {
					defaultCT = (CTDefault)o;
					addDefaultContentType( defaultCT.getExtension(), defaultCT  );
				}

				if (o instanceof CTOverride) {
					overrideCT = (CTOverride)o;
					URI uri = new URI(overrideCT.getPartName() );
					addOverrideContentType(uri, overrideCT );
				}
			}

		} catch (Exception e ) {
			throw new InvalidFormatException("Bad [Content_Types].xml", e);
		}


	}

	private CTTypes buildTypes() {

		// Build the JAXB object
		ObjectFactory factory = new ObjectFactory();
		CTTypes types = factory.createCTTypes();

		for (Entry<String, CTDefault> entry : defaultContentType.entrySet()) {
			types.getDefaultOrOverride().add(entry.getValue());
		}

		if (overrideContentType != null) {
			for (Entry<URI, CTOverride> entry : overrideContentType.entrySet()) {
				types.getDefaultOrOverride().add(entry.getValue());
			}
		}
		return types;
	}

//	public void listTypes() {
//
//
//		for (Entry<String, CTDefault> entry : defaultContentType.entrySet()) {
//
//			System.out.println("// " + entry.getValue().getExtension());
//			System.out.println("public final static String XX =" );
//			System.out.println(entry.getValue().getContentType());
//		}
//
//		if (overrideContentType != null) {
//			for (Entry<URI, CTOverride> entry : overrideContentType.entrySet()) {
//				System.out.println("// " + entry.getValue().getPartName());
//				System.out.println("public final static String XX =" );
//				System.out.println("\"" + entry.getValue().getContentType() + "\";");
//			}
//		}
//	}

    public void marshal(org.w3c.dom.Node node) throws JAXBException {

		try {
			Marshaller marshaller = Context.getJcContentTypes().createMarshaller();
			marshaller.setProperty("org.glassfish.jaxb.namespacePrefixMapper", NamespacePrefixMapperUtils.getPrefixMapper());

			log.debug("marshalling " + this.getClass().getName() + " ..." );

			marshaller.marshal(buildTypes(), node);

			log.debug("content types marshalled \n\n" );

		} catch (JAXBException e) {
			throw e;
		}
    }

    public void marshal(java.io.OutputStream os) throws JAXBException {

		try {
			Marshaller marshaller = Context.getJcContentTypes().createMarshaller();
            marshaller.setProperty("org.glassfish.jaxb.namespacePrefixMapper", NamespacePrefixMapperUtils.getPrefixMapper());
			log.debug("marshalling " + this.getClass().getName() + " ..." );
			marshaller.marshal(buildTypes(), os);
		} catch (JAXBException e) {
			throw e;
		}
	}



	/** Return a package of the appropriate type.  Used when loading an existing
	 *  Package, with an already populated [Content_Types].xml.  When
	 *  creating a new Package, start with the new WordprocessingMLPackage constructor. */
	public OpcPackage createPackage(String pkgContentType) throws InvalidFormatException {

        switch (pkgContentType) {
            case ContentTypes.WORDPROCESSINGML_DOCUMENT:
            case ContentTypes.WORDPROCESSINGML_DOCUMENT_MACROENABLED:
            case ContentTypes.WORDPROCESSINGML_TEMPLATE:
            case ContentTypes.WORDPROCESSINGML_TEMPLATE_MACROENABLED:

                log.debug("Detected WordProcessingML package ");
                return new WordprocessingMLPackage(this);

            case ContentTypes.PRESENTATIONML_MAIN:
            case ContentTypes.PRESENTATIONML_MAIN_MACROENABLED:
            case ContentTypes.PRESENTATIONML_TEMPLATE:
            case ContentTypes.PRESENTATIONML_TEMPLATE_MACROENABLED:
            case ContentTypes.PRESENTATIONML_SLIDESHOW:
            case ContentTypes.PRESENTATIONML_SLIDESHOW_MACROENABLED:

                log.info("Detected PresentationMLPackage package ");
                return new PresentationMLPackage(this);

            case ContentTypes.SPREADSHEETML_WORKBOOK:
            case ContentTypes.SPREADSHEETML_WORKBOOK_MACROENABLED:
            case ContentTypes.SPREADSHEETML_TEMPLATE:
            case ContentTypes.SPREADSHEETML_TEMPLATE_MACROENABLED:

                // "xlam", "xlsb" ?
                log.info("Detected SpreadhseetMLPackage package ");
                return new SpreadsheetMLPackage(this);

            case ContentTypes.DRAWINGML_DIAGRAM_LAYOUT:
                return new OpcPackage(this);

            default:
                // Nothing in overrides or defaults
                throw new InvalidFormatException("Couldn't identify package from " + pkgContentType);
        }
	}

	/*
	 * Gets the content type from an extension.

	public static String getContentTypeFromExtension(String extension) {
		if ((extension.equals(ContentTypes.EXTENSION_JPG_1))
				|| (extension.equals(ContentTypes.EXTENSION_JPG_2))) {
			return ContentTypes.IMAGE_JPEG;
		}
		if (extension.equals(ContentTypes.EXTENSION_PNG)) {
			return ContentTypes.IMAGE_PNG;
		}
		if (extension.equals(ContentTypes.EXTENSION_GIF)) {
			return ContentTypes.IMAGE_GIF;
		}
		if (extension.equals(ContentTypes.EXTENSION_TIFF)) {
			return ContentTypes.IMAGE_TIFF;
		}
		if (extension.equals(ContentTypes.EXTENSION_PICT)) {
			return ContentTypes.IMAGE_PICT;
		}
		return null;
	}
*/

    public boolean isContentEqual(ContentTypeManager other) throws Docx4JException {

    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
    	try {
        	marshal(baos);
			other.marshal(baos2);
		} catch (JAXBException e) {
			throw new Docx4JException("Error marshalling parts", e);
		}

    	return java.util.Arrays.equals(baos.toByteArray(), baos2.toByteArray());

    }

    @Override
    public String toString() {

    	CTTypes types = buildTypes();
    	return XmlUtils.marshaltoString(null, types, true, true, Context.getJcContentTypes());

    }

    /* is adding files to the zip package that are removed from the package after an additional load/save */
    public void addNonPersistContentFile(String zipEntryName, byte[] data, String extension, String contentType) {
        addDefaultContentType(extension, contentType);
        nonPersistContentList.add(new NonPersistContent(zipEntryName, data));
    }

    public List<NonPersistContent> getNonPersistContent() {
        return nonPersistContentList;
    }
}
