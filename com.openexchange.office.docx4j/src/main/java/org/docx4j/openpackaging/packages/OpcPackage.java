/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.packages;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.docx4j.openpackaging.Base;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.io3.Load3;
import org.docx4j.openpackaging.io3.Save;
import org.docx4j.openpackaging.io3.stores.PartStore;
import org.docx4j.openpackaging.io3.stores.ZipPartStore;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsCustomPart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.ExternalTarget;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.Parts;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.MarkupId;


/**
 * Represent a Package as defined in the Open Packaging Specification.
 *
 * @author Jason Harrop
 */
public class OpcPackage extends Base {

    private boolean lowMemoryAbort = false;

    public boolean isLowMemoryAbort() {
        return lowMemoryAbort;
    }

    public void setLowMemoryAbort() {
        lowMemoryAbort = true;
    }

    public MarkupId markupId = new MarkupId(4000000);
    public Integer getNextMarkupId() {
        return markupId.getNextMarkupId();
    }
    public void addMarkupId(Integer id) {
        markupId.addMarkupId(id);
    }

    public MarkupId paraId = new MarkupId(4000000);
    public Integer getNextParaId() {
        return paraId.getNextMarkupId();
    }
    public void addParaId(Integer id) {
        paraId.addMarkupId(id);
    }

    private int successfulAppliedOperations = 0;

    public int getSuccessfulAppliedOperations() {
        return successfulAppliedOperations;
    }

    public void setSuccessfulAppliedOperations(int operations) {
        successfulAppliedOperations = operations;
    }

    /**
	 * This HashMap is intended to prevent loops during the loading
	 * of this package. TODO This doesn't really tell us anything that
	 * the contents of Parts couldn't also tell us (except that
	 * that doesn't contain the rels parts), so consider removing.
	 * At least replace it with a method, so this implementation
	 * detail is hidden!
	 */
	public HashMap<String, String> handled = new HashMap<String, String>();

	/**
	 * Package parts collection.  This is a collection of _all_
	 * parts in the package (_except_ relationship parts),
	 * not just those referred to by the package-level relationships.
	 * It doesn't include external resources.
	 */
	protected Parts parts = new Parts();

	/**
	 * Retrieve the Parts object.
	 */
	public Parts getParts() {

		// Having a separate Parts object doesn't really buy
		// us much, but live with it...

		return parts;
	}

	protected HashMap<ExternalTarget, Part> externalResources
		= new HashMap<ExternalTarget, Part>();
	public HashMap<ExternalTarget, Part> getExternalResources() {
		return externalResources;
	}

	protected ContentTypeManager contentTypeManager;

	public ContentTypeManager getContentTypeManager() {
		return contentTypeManager;
	}

	public void setContentTypeManager(ContentTypeManager contentTypeManager) {
		this.contentTypeManager = contentTypeManager;
	}

	private PartStore sourcePartStore;

	/**
	 * @return the partStore
	 * @since 3.0.
	 */
	public PartStore getSourcePartStore() {
		return sourcePartStore;
	}

	/**
	 * @param partStore the partStore to set
	 * @since 3.0.
	 */
	public void setSourcePartStore(PartStore partStore) {
		this.sourcePartStore = partStore;
	}

	private PartStore targetPartStore;

	/**
	 * @return the partStore
	 * @since 3.0.
	 */
	public PartStore getTargetPartStore() {
		return targetPartStore;
	}

	/**
	 * @param partStore the partStore to set
	 * @since 3.0.
	 */
	public void setTargetPartStore(PartStore partStore) {
		this.targetPartStore = partStore;
	}

	/**
	 * Constructor.  Also creates a new content type manager
	 *
	 */
	public OpcPackage() {
		try {
			partName = new PartName("/", false);

			contentTypeManager = new ContentTypeManager();
		} catch (InvalidFormatException e) {
			// TODO: handle exception
		}
	}

	/**
	 * Constructor.
	 *
	 * @param contentTypeManager
	 *            The content type manager to use
	 */
	public OpcPackage(ContentTypeManager contentTypeManager) {
		try {
			partName = new PartName("/", false);

			this.contentTypeManager = contentTypeManager;
		} catch (InvalidFormatException e) {
			// TODO: handle exception
		}
	}

	@Override
    public OpcPackage getPackage() {
		return this;
	}


	protected DocPropsCorePart docPropsCorePart;

	protected DocPropsExtendedPart docPropsExtendedPart;

	protected DocPropsCustomPart docPropsCustomPart;

	/**
	 * Convenience method to create a WordprocessingMLPackage
	 * or PresentationMLPackage
	 * from an inputstream (.docx/.docxm, .ppxtx or Flat OPC .xml).
	 * It detects the convenient format inspecting two first bytes of stream (magic bytes).
	 * For office 2007 'x' formats, these two bytes are 'PK' (same as zip file)
     *
	 * @param inputStream
	 *            The docx file
	 *
	 * @Since 2.8.0
	 */
	public static OpcPackage load(final ByteArrayInputStream inputStream, boolean XMLReadOnly) throws Docx4JException {
		final byte[] firstTwobytes=new byte[2];
		int read=0;
		try {
			read = inputStream.read(firstTwobytes);
			inputStream.reset();
		} catch (final IOException e) {
			throw new Docx4JException("Error reading from the stream", e);
		}
		if (read!=2){
			throw new Docx4JException("Error reading from the stream (no bytes available)");
		}
		if (firstTwobytes[0]=='P' && firstTwobytes[1]=='K') { // 50 4B
            final ZipPartStore partLoader = new ZipPartStore(inputStream, XMLReadOnly);
            final Load3 loader = new Load3(partLoader);
            return loader.get();
		} else if  (firstTwobytes[0]==(byte)0xD0 && firstTwobytes[1]==(byte)0xCF) {
			// password protected docx is a compound file, with signature D0 CF 11 E0 A1 B1 1A E1
		    throw new FilterException("Problem reading compound file", ErrorCode.UNSUPPORTED_ENCRYPTION_USED);
		} else {
	        throw new Docx4JException("Couldn't load xml from stream ");
		}
	}


	/**
	 * Convenience method to save a WordprocessingMLPackage
	 * or PresentationMLPackage to a File.
     *
	 * @param file
	 *            The docx file
	 */
	public void save(java.io.File file) throws Docx4JException {
		save(file, null);
	}
	/**
	 * Convenience method to save a WordprocessingMLPackage
	 * or PresentationMLPackage to a File.
     *
	 * @param file
	 *            The docx file
	 */
	private void save(java.io.File file, String password) throws Docx4JException {

		if (password==null) {

//			SaveToZipFile saver = new SaveToZipFile(this);
//			saver.save(file);

			Save saver = new Save(this);
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(file);
				saver.save(fos);
			} catch (FileNotFoundException e) {
				throw new Docx4JException("Couldn't save " + file.getPath(), e);
			} finally {
				IOUtils.closeQuietly(fos);
			}

		} else {
			// Create the compound file
	        try {
	        	// Write the package to a stream

	        	// .. then encrypt

	        	// TODO.  See for example http://code.google.com/p/ooxmlcrypto/source/browse/trunk/OfficeCrypto/OfficeCrypto.cs

			} catch (Exception e) {
				throw new Docx4JException("Problem reading compound file", e);
			}

		}
	}




	@Override
	public boolean setPartShortcut(Part part, String relationshipType) {
		if (relationshipType.equals(Namespaces.PROPERTIES_CORE)) {
			docPropsCorePart = (DocPropsCorePart)part;
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_CUSTOM)) {
			docPropsCustomPart = (DocPropsCustomPart)part;
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_EXTENDED)) {
			docPropsExtendedPart = (DocPropsExtendedPart)part;
			return true;
		} else {
			return false;
		}
	}

    public DocPropsCorePart getDocPropsCorePart(boolean createIfMissing) {

        try {
            if(docPropsCorePart==null&&createIfMissing) {
                DocPropsCorePart core = new DocPropsCorePart();
                org.docx4j.docProps.core.ObjectFactory coreFactory = new org.docx4j.docProps.core.ObjectFactory();
                core.setJaxbElement(coreFactory.createCoreProperties() );
                addTargetPart(core);
            }
        }
        catch(InvalidFormatException e) {
            //
        }
        return docPropsCorePart;
    }

	public DocPropsExtendedPart getDocPropsExtendedPart() {
//		if (docPropsExtendedPart==null) {
//			try {
//				docPropsExtendedPart = new org.docx4j.openpackaging.parts.DocPropsExtendedPart();
//				this.addTargetPart(docPropsExtendedPart);
//
//				org.docx4j.docProps.extended.ObjectFactory factory =
//					new org.docx4j.docProps.extended.ObjectFactory();
//				org.docx4j.docProps.extended.Properties properties = factory.createProperties();
//				((org.docx4j.openpackaging.parts.JaxbXmlPart)docPropsExtendedPart).setJaxbElement((Object)properties);
//				((org.docx4j.openpackaging.parts.JaxbXmlPart)docPropsExtendedPart).setJAXBContext(Context.jcDocPropsExtended);
//			} catch (InvalidFormatException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		return docPropsExtendedPart;
	}

	/**
	 * Get DocPropsCustomPart, if any.
	 *
	 * @return
	 */
	public DocPropsCustomPart getDocPropsCustomPart() {

//		if (docPropsCustomPart==null) {
//			try {
//				docPropsCustomPart = new org.docx4j.openpackaging.parts.DocPropsCustomPart();
//				this.addTargetPart(docPropsCustomPart);
//
//				org.docx4j.docProps.custom.ObjectFactory factory =
//					new org.docx4j.docProps.custom.ObjectFactory();
//
//				org.docx4j.docProps.custom.Properties properties = factory.createProperties();
//				((org.docx4j.openpackaging.parts.JaxbXmlPart)docPropsCustomPart).setJaxbElement((Object)properties);
//
//				((org.docx4j.openpackaging.parts.JaxbXmlPart)docPropsCustomPart).setJAXBContext(Context.jcDocPropsCustom);
//
//			} catch (InvalidFormatException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}

		return docPropsCustomPart;
	}
}
