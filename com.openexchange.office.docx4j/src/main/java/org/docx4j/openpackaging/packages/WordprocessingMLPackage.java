/*
 *  Copyright 2007-2008, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.openpackaging.packages;

import java.util.Map;
import jakarta.xml.bind.JAXBException;
import javax.xml.transform.Templates;
import org.docx4j.Docx4jProperties;
import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.PageDimensions;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.contenttype.ContentTypeManager;
import org.docx4j.openpackaging.contenttype.ContentTypes;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.parts.DocPropsCorePart;
import org.docx4j.openpackaging.parts.DocPropsCustomPart;
import org.docx4j.openpackaging.parts.DocPropsExtendedPart;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.WordprocessingML.GlossaryDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.SectPr;

/**
 * @author jharrop
 *
 */
public class WordprocessingMLPackage extends OpcPackage {

	// What is a Word document these days?
	//
	// Well, a package is a logical entity which holds a collection of parts
	// And a word document is exactly a WordProcessingML package
	// Which has a Main Document Part, and optionally, a Glossary Document Part

	/* So its a Word doc if:
	 * 1. _rels/.rels tells you where to find an office document
	 * 2. [Content_Types].xml tells you that office document is
	 *    of content type application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml

	 * A minimal docx has:
	 *
	 * [Content_Types].xml containing:
	 * 1. <Default Extension="rels" ...
	 * 2. <Override PartName="/word/document.xml"...
	 *
	 * _rels/.rels with a target for word/document.xml
	 *
	 * word/document.xml
	 */

	// Main document
	protected MainDocumentPart mainDoc;

	// (optional) Glossary document
	protected GlossaryDocumentPart glossaryDoc;

	/**
	 * Constructor.  Also creates a new content type manager
	 *
	 */
	public WordprocessingMLPackage() {
		super();
		setContentType(new ContentType(ContentTypes.WORDPROCESSINGML_DOCUMENT));
	}
	/**
	 * Constructor.
	 *
	 * @param contentTypeManager
	 *            The content type manager to use
	 */
	public WordprocessingMLPackage(ContentTypeManager contentTypeManager) {
		super(contentTypeManager);
		setContentType(new ContentType(ContentTypes.WORDPROCESSINGML_DOCUMENT));
	}

	@Override
    public boolean setPartShortcut(Part part, String relationshipType) {

		if (relationshipType.equals(Namespaces.PROPERTIES_CORE)) {
			docPropsCorePart = (DocPropsCorePart)part;
			log.debug("Set shortcut for docPropsCorePart");
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_EXTENDED)) {
			docPropsExtendedPart = (DocPropsExtendedPart)part;
			log.debug("Set shortcut for docPropsExtendedPart");
			return true;
		} else if (relationshipType.equals(Namespaces.PROPERTIES_CUSTOM)) {
			docPropsCustomPart = (DocPropsCustomPart)part;
			log.debug("Set shortcut for docPropsCustomPart");
			return true;
		} else if (relationshipType.equals(Namespaces.DOCUMENT)) {
			mainDoc = (MainDocumentPart)part;
			log.debug("Set shortcut for mainDoc");
			return true;
		} else {
			return false;
		}
	}

	public MainDocumentPart getMainDocumentPart() {
		return mainDoc;
	}

    static Templates filterTemplate;

	/**
	 * Creates a WordprocessingMLPackage, using default page size and orientation.
	 * From 2.7.1, these are read from docx4j.properties, or if not found, default
	 * to A4 portrait.
	 */
	public static WordprocessingMLPackage createPackage() throws InvalidFormatException {

		String papersize= Docx4jProperties.getProperties().getProperty("docx4j.PageSize", "A4");
		log.info("Using paper size: " + papersize);

		String landscapeString = Docx4jProperties.getProperties().getProperty("docx4j.PageOrientationLandscape", "false");
		boolean landscape= Boolean.parseBoolean(landscapeString);
		log.info("Landscape orientation: " + landscape);

		return createPackage(
				PageSizePaper.valueOf(papersize), landscape);
	}

	public static WordprocessingMLPackage createPackage(PageSizePaper sz, boolean landscape ) throws InvalidFormatException {


		// Create a package
		WordprocessingMLPackage wmlPack = new WordprocessingMLPackage();

		// Create main document part
		MainDocumentPart wordDocumentPart = new MainDocumentPart();

		// Create main document part content
		org.docx4j.wml.ObjectFactory factory = Context.getWmlObjectFactory();
		org.docx4j.wml.Body  body = factory.createBody();
		org.docx4j.wml.Document wmlDocumentEl = factory.createDocument();

		wmlDocumentEl.setBody(body);

		// Create a basic sectPr using our Page model
		PageDimensions page = new PageDimensions();
		page.setPgSize(sz, landscape);

		SectPr sectPr = factory.createSectPr();
		body.setSectPr(sectPr);
		sectPr.setPgSz(  page.getPgSz() );
		sectPr.setPgMar( page.getPgMar() );

		// Put the content in the part
		wordDocumentPart.setJaxbElement(wmlDocumentEl);

		// Add the main document part to the package relationships
		// (creating it if necessary)
		wmlPack.addTargetPart(wordDocumentPart);

		// Create a styles part
		Part stylesPart = new org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart();
		((org.docx4j.openpackaging.parts.WordprocessingML.StyleDefinitionsPart)stylesPart).unmarshalDefaultStyles();
		wordDocumentPart.addTargetPart(stylesPart); // NB - add it to main doc part, not package!

		// Metadata: docx4j 2.7.1 can populate some of this from docx4j.properties
		// See SaveToZipFile
		DocPropsCorePart core = new DocPropsCorePart();
		org.docx4j.docProps.core.ObjectFactory coreFactory = new org.docx4j.docProps.core.ObjectFactory();
		core.setJaxbElement(coreFactory.createCoreProperties() );
		wmlPack.addTargetPart(core);


		DocPropsExtendedPart app = new DocPropsExtendedPart();
		org.docx4j.docProps.extended.ObjectFactory extFactory = new org.docx4j.docProps.extended.ObjectFactory();
		app.setJaxbElement(extFactory.createProperties() );
		wmlPack.addTargetPart(app);

		// Return the new package
		return wmlPack;

	}


	public static class FilterSettings {

		Boolean removeProofErrors = Boolean.FALSE;
		public void setRemoveProofErrors(boolean val) {
			removeProofErrors = Boolean.valueOf(val);
		}

		Boolean removeContentControls = Boolean.FALSE;
		public void setRemoveContentControls(boolean val) {
			removeContentControls = Boolean.valueOf(val);
		}

		Boolean removeRsids = Boolean.FALSE;
		public void setRemoveRsids(boolean val) {
			removeRsids = Boolean.valueOf(val);
		}

		Boolean tidyForDocx4all = Boolean.FALSE;
		public void setTidyForDocx4all(boolean val) {
			tidyForDocx4all = Boolean.valueOf(val);
		}

		public Map<String, Object> getSettings() {
			Map<String, Object> settings = new java.util.HashMap<String, Object>();

			settings.put("removeProofErrors", removeProofErrors);
			settings.put("removeContentControls", removeContentControls);
			settings.put("removeRsids", removeRsids);
			settings.put("tidyForDocx4all", tidyForDocx4all);

			return settings;
		}
	}
}
