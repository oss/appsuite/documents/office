
package org.docx4j.fonts;

/**
 * Certain fonts have CJK names in a docx.
 * However, in Win 7 (English locale at least), the font names are in English.
 * So this class translates the docx CJK font name to a physical font name.
 *  
 * @author jharrop
 *
 */
public class CJKToEnglish {
	
	/**
	 * Convert a CJK font name (as used in docx) to an English font name
	 * (as used on Win 7, English locale at least).
	 * 
	 * @param fontName
	 * @return
	 */
	public static String toEnglish(String fontName) {
		
		// Special cases; there are more; see http://en.wikipedia.org/wiki/List_of_CJK_fonts
		if (fontName.equals("\u00d4\u00ba\u2260\u00d4\u00ba\u2265 \u201e\u00c7\u00a5\u201e\u00c7\u2211\u201e\u00c9\u00c9\u201e\u00c7\u00d8")) {
//	        <a:font script="Jpan" typeface="\u00d4\u00ba\u2260\u00d4\u00ba\u2265 \u201e\u00c7\u00a5\u201e\u00c7\u2211\u201e\u00c9\u00c9\u201e\u00c7\u00d8"/>
			return "MS Gothic";
		} else if (fontName.equals("\u00d4\u00ba\u2260\u00d4\u00ba\u2265 \u00ca\u00f2\u00e9\u00ca\u00fa\u00f9")) {			
			return "MS Mincho";
		} else if (fontName.equals("\u00ce\u00df\u00eb\u00cf\u00f9\u00c4 \u00cd\u2265\u2020\u00ce\u00ee\u00ef")) {
//	        <a:font script="Hang" typeface="\u00ce\u00df\u00eb\u00cf\u00f9\u00c4 \u00cd\u2265\u2020\u00ce\u00ee\u00ef"/>
			return "Malgun Gothic";
		} else if (fontName.equals("\u00c2\u00c6\u00e3\u2030\u03a9\u00ec")) {
//	        <a:font script="Hans" typeface="\u00c2\u00c6\u00e3\u2030\u03a9\u00ec"/>
			return "SimSun"; 			
		} else if (fontName.equals("\u00ca\u00f1\u221e\u00c1\u00a5\u221e\u00ca\u00f2\u00e9\u00c8\u00b4\u00ee")) { 
			return "PMingLiU"; // according to http://zh.wikipedia.org/wiki/%E5%AE%8B%E4%BD%93
		} else {
			return null;
		}		
	}

}
