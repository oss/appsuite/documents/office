

/*
 *  Copyright 2009, Plutext Pty Ltd.
 *
 *  This file is part of docx4j.

    docx4j is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 */
package org.docx4j.model.structure;

import org.docx4j.Docx4jProperties;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.STPageOrientation;
import org.docx4j.wml.SectPr.PgMar;
import org.docx4j.wml.SectPr.PgSz;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wraps PgSz (Page size) and PgMar (margin settings).
 * - gives you an easy way to set these;
 * - performs a few useful calculations
 *
 * Used in SectionWrapper, to store the dimensions
 * of a page for a section; can also be used to
 * store a set of page dimensions which aren't
 * associated with any section.
 *
 * @author jharrop
 *
 */
public class PageDimensions {

	protected static Logger log = LoggerFactory.getLogger(PageDimensions.class);


	public PageDimensions() {

		pgSz = Context.getWmlObjectFactory().createSectPrPgSz();
		setPgSize();

		pgMar = Context.getWmlObjectFactory().createSectPrPgMar();
		setMargins();
	}

	private PgSz pgSz;
	/**
	 * @since 2.7
	 */
	public PgSz getPgSz() {
		return pgSz;
	}

	private PgMar pgMar;
	/**
	 * @since 2.7
	 */
	public PgMar getPgMar() {
		return pgMar;
	}

	/**
	 * set Margins from docx4j.properties
	 *
	 * @since 2.8
	 */
	public void setMargins() {

		String margin= Docx4jProperties.getProperties().getProperty("docx4j.PageMargins", "NORMAL");

		setMargins(MarginsWellKnown.valueOf(margin));

//		if (margin.equals("normal")) {
//			setMargins(MarginsWellKnown.NORMAL);
//		} else if (margin.equals("narrow")) {
//			setMargins(MarginsWellKnown.NARROW);
//		} else if (margin.equals("moderate")) {
//			setMargins(MarginsWellKnown.MODERATE);
//		} else if (margin.equals("wide")) {
//			setMargins(MarginsWellKnown.WIDE);
//		} else {
//			log.warn("Unknown margin setting " + margin + "in docx4j.properties");
//			setMargins(MarginsWellKnown.NORMAL);
//		}
	}

	/**
	 * @since 2.7
	 */
	public void setMargins(MarginsWellKnown m ) {

//	    NORMAL("normal"),     // <w:pgMar w:top="1440" w:right="1440" w:bottom="1440" w:left="1440" w:header="708" w:footer="708" w:gutter="0"/>
		if (m.equals(MarginsWellKnown.NORMAL)) {
			pgMar.setTop(Long.valueOf(1440));
			pgMar.setBottom(Long.valueOf(1440));
			pgMar.setLeft(Long.valueOf(1440));
			pgMar.setRight(Long.valueOf(1440));
			return;
		}

//	    NARROW("narrow"),     // <w:pgMar w:top="720"  w:right="720"  w:bottom="720"  w:left="720" w:header="708" w:footer="708" w:gutter="0"/>
		if (m.equals(MarginsWellKnown.NARROW)) {
			pgMar.setTop(Long.valueOf(720));
			pgMar.setBottom(Long.valueOf(720));
			pgMar.setLeft(Long.valueOf(720));
			pgMar.setRight(Long.valueOf(720));
			return;
		}

//	    MODERATE("moderate"), // <w:pgMar w:top="1440" w:right="1080" w:bottom="1440" w:left="1080" w:header="708" w:footer="708" w:gutter="0"/>
		if (m.equals(MarginsWellKnown.MODERATE)) {
			pgMar.setTop(Long.valueOf(1440));
			pgMar.setBottom(Long.valueOf(1440));
			pgMar.setLeft(Long.valueOf(1080));
			pgMar.setRight(Long.valueOf(1080));
			return;
		}

//	    WIDE("wide");         // <w:pgMar w:top="1440" w:right="2880" w:bottom="1440" w:left="2880" w:header="708" w:footer="708" w:gutter="0"/>
		if (m.equals(MarginsWellKnown.WIDE)) {
			pgMar.setTop(Long.valueOf(1440));
			pgMar.setBottom(Long.valueOf(1440));
			pgMar.setLeft(Long.valueOf(2880));
			pgMar.setRight(Long.valueOf(2880));
			return;
		}

	}

	/**
	 * set page size/orientation from docx4j.properties
	 * @since 2.8
	 */
	public void setPgSize() {

		String papersize= Docx4jProperties.getProperties().getProperty("docx4j.PageSize", "A4");
		log.info("Using paper size: " + papersize);

		String landscapeString = Docx4jProperties.getProperties().getProperty("docx4j.PageOrientationLandscape", "false");
		boolean landscape= Boolean.parseBoolean(landscapeString);
		log.info("Landscape orientation: " + landscape);

		setPgSize(PageSizePaper.valueOf(papersize), landscape);
	}
	/**
	 * @since 2.7
	 */
	public void setPgSize(PageSizePaper sz, boolean landscape ) {

		if (sz.equals(PageSizePaper.LETTER)) {
			pgSz.setCode(Long.valueOf(1));
			if (landscape) {
				pgSz.setOrient(STPageOrientation.LANDSCAPE);
				pgSz.setW(Long.valueOf(15840));
				pgSz.setH(Long.valueOf(12240));
			} else {
				pgSz.setW(Long.valueOf(12240));
				pgSz.setH(Long.valueOf(15840));
			}
		}

		else if (sz.equals(PageSizePaper.LEGAL)) {
			pgSz.setCode(Long.valueOf(5));
			if (landscape) {
				pgSz.setOrient(STPageOrientation.LANDSCAPE);
				pgSz.setW(Long.valueOf(20160));
				pgSz.setH(Long.valueOf(12240));
			} else {
				pgSz.setW(Long.valueOf(12240));
				pgSz.setH(Long.valueOf(20160));
			}
		}

		else if (sz.equals(PageSizePaper.A3)) {
			pgSz.setCode(Long.valueOf(8));
			if (landscape) {
				pgSz.setOrient(STPageOrientation.LANDSCAPE);
				pgSz.setW(Long.valueOf(23814));
				pgSz.setH(Long.valueOf(16839));
			} else {
				pgSz.setW(Long.valueOf(16839));
				pgSz.setH(Long.valueOf(23814));
			}
		}

		else if (sz.equals(PageSizePaper.A4)) {
			pgSz.setCode(Long.valueOf(9));
			if (landscape) {
				pgSz.setOrient(STPageOrientation.LANDSCAPE);
				pgSz.setW(Long.valueOf(16839));
				pgSz.setH(Long.valueOf(11907));
			} else {
				pgSz.setW(Long.valueOf(11907));
				pgSz.setH(Long.valueOf(16839));
			}
		}

		else if (sz.equals(PageSizePaper.B4JIS)) {
			pgSz.setCode(Long.valueOf(12));
			if (landscape) {
				pgSz.setOrient(STPageOrientation.LANDSCAPE);
				pgSz.setW(Long.valueOf(20639));
				pgSz.setH(Long.valueOf(14572));
			} else {
				pgSz.setW(Long.valueOf(14572));
				pgSz.setH(Long.valueOf(20639));
			}
		}
	}
}
