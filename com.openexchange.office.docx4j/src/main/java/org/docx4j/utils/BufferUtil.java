
package org.docx4j.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BufferUtil {
	
    private static final int BUFFER_SIZE = 1024;

    /**
     * Fully reads the given InputStream, returning its contents as a ByteBuffer
     *
     * @param in an <code>InputStream</code> value
     * @return a <code>ByteBuffer</code> value
     * @exception IOException if an error occurs
     */
    public static ByteBuffer readInputStream(InputStream in) throws IOException {
    	/**
    	 * Some useful methods for reading URLs or streams and returning the data as Direct Buffers
    	 *
    	 * @author Ivan Z. Ganza
    	 * @author Robert Schuster
    	 * @author Bart LEBOEUF
    	 * 
    	 * Originally from com.lilly.chorus.eSignatures.utils.BufferUtil
    	 */
    	
    	/*
		 * Converts the InputStream into a channels which allows us to read into
		 * a ByteBuffer.
		 */
		ReadableByteChannel ch = Channels.newChannel(in);

		// Creates a list that stores the intermediate buffers.
		List<ByteBuffer> list = new LinkedList<ByteBuffer>();

		// A variable storing the accumulated size of the data.
		int sum = 0, read = 0;

		/*
		 * Reads all the bytes from the channel and stores them in various
		 * buffer objects.
		 */
		do {
			ByteBuffer b = createByteBuffer(BUFFER_SIZE);
			read = ch.read(b);

			if (read > 0) {
				b.flip(); // make ready for reading later
				list.add(b);
				sum += read;
			}
		} while (read != -1);

		/*
		 * If there is only one buffer used we do not need to merge the data.
		 */
		if (list.size() == 1) {
			return list.get(0);
		}

		ByteBuffer bb = createByteBuffer(sum);

		/* Merges all buffers into the Buffer bb */
		Iterator<ByteBuffer> ite = list.iterator();
		while (ite.hasNext()) {
			bb.put(ite.next());
		}
		list.clear();

		return bb;
	}
    
    
    public static ByteBuffer createByteBuffer(int capacity) {
    	return ByteBuffer.allocateDirect(capacity).order(ByteOrder.nativeOrder());
    }
}
