/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.restore;

import com.openexchange.office.tools.common.error.ErrorCode;

/**
 *
 * @author carsten-driesner
 *
 */
public class RestoreData {
    public final static String KEY_RESTORE_FILENAME  = "restoredFileName";
    public final static String KEY_RESTORE_FILEID    = "restoredFileId";
    public final static String KEY_RESTORE_FOLDERID  = "restoredFolderId";
    public final static String KEY_RESTORE_ERRORCODE = "restoredErrorCode";

    private ErrorCode errorCode;
    private String restoredFileName;
    private String restoredFileId;
    private String restoredFolderId;

    public RestoreData(final ErrorCode errorCode, final String restoredFileName, final String fileId, final String folderId) {
        this.errorCode = errorCode;
        this.restoredFileName = restoredFileName;
        this.restoredFileId = fileId;
        this.restoredFolderId = folderId;
    }

    public ErrorCode getErrorCode() { return errorCode; }

    public String getRestoredFileName() { return restoredFileName; }

    public String getRestoredFileId() { return restoredFileId; }

    public String getRestoredFolderId() { return restoredFolderId; }

    public void setErrorCode(final ErrorCode errorCode) { this.errorCode = errorCode; }

    public void setRestoredFileName(final String restoredFileName) { this.restoredFileName = restoredFileName; }

    public void setRestoredFileId(final String restoredFileId) { this.restoredFileId = restoredFileId; }

    public void setRestoredFolderId(final String restoredFolderId) { this.restoredFolderId = restoredFolderId; }
}
