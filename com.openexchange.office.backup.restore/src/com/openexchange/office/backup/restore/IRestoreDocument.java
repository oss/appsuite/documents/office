/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.restore;

import org.json.JSONArray;

import com.openexchange.office.backup.restore.impl.RestoreDocument;
import com.openexchange.office.tools.annotation.ConcreteService;
import com.openexchange.tools.session.ServerSession;

@ConcreteService(clazz=RestoreDocument.class)
public interface IRestoreDocument {

    public RestoreData saveAsWithBackupDocumentAndOperations(final ServerSession session, final String id, final String restoreID, final String targetFolderId, final String initialFileName, final JSONArray operations) throws Exception;

    public JSONArray getRestoredOperations(int baseOSN, int currentOSN, final JSONArray storedOperations, final JSONArray clientOperations) throws RestoreException;

}
