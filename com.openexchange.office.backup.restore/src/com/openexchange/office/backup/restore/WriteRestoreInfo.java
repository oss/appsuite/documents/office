/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.restore;

import com.openexchange.file.storage.File;
import com.openexchange.office.tools.common.error.ErrorCode;

public class WriteRestoreInfo {

    private final ErrorCode errorCode;
    private final String restoredFileName;
    private final String restoredFileId;
    private final File   restoredMetaData;

    public WriteRestoreInfo(final ErrorCode errorCode, final String restoredFileName, final String restoredFileId, final File restoredMetaData) {
        this.errorCode = errorCode;
        this.restoredFileName = restoredFileName;
        this.restoredFileId = restoredFileId;
        this.restoredMetaData = restoredMetaData;
    }

    public final ErrorCode getErrorCode() {
        return errorCode;
    }

    public final String getRestoredFileName() {
        return restoredFileName;
    }

    public final String getRestoredFileId() {
        return restoredFileId;
    }

    public final File getRestoredMetaData() {
        return restoredMetaData;
    }
}
