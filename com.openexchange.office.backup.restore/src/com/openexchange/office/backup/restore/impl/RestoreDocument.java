/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.restore.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.documentconverter.Properties;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStoragePermission;
import com.openexchange.office.backup.manager.DocumentBackupController;
import com.openexchange.office.backup.manager.DocumentRestoreData;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.backup.restore.RestoreData;
import com.openexchange.office.backup.restore.RestoreException;
import com.openexchange.office.backup.restore.WriteRestoreInfo;
import com.openexchange.office.document.api.DocFileService;
import com.openexchange.office.document.api.DocFileServiceFactory;
import com.openexchange.office.document.api.ImExporterAccessFactory;
import com.openexchange.office.document.api.OXDocument;
import com.openexchange.office.document.api.OXDocumentFactory;
import com.openexchange.office.document.api.ResolvedStreamInfo;
import com.openexchange.office.document.api.WriteInfo;
import com.openexchange.office.document.tools.DocFileHelper;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.core.FilterExceptionToErrorCode;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.IResourceManagerFactory;
import com.openexchange.office.message.OperationHelper;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;
import com.openexchange.office.tools.common.files.FileHelper;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.directory.DocRestoreID;
import com.openexchange.office.tools.monitoring.RestoreDocEvent;
import com.openexchange.office.tools.monitoring.RestoreDocEventType;
import com.openexchange.office.tools.monitoring.Statistics;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.office.tools.service.files.FolderHelperService;
import com.openexchange.office.tools.service.files.FolderHelperServiceFactory;
import com.openexchange.office.tools.service.storage.StorageHelperService;
import com.openexchange.office.tools.service.storage.StorageHelperServiceFactory;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;

@Service
@RegisteredService(registeredClass=IRestoreDocument.class)
public class RestoreDocument implements IRestoreDocument {

    private static final Logger log = LoggerFactory.getLogger(RestoreDocument.class);

    private enum RestoreMessage {
        RESTORE_REQUESTED,
        RESTORE_SUCCESS,
        RESTORE_FAILED
    }

    @Autowired
    private DocumentBackupController backupController;

    @Autowired
	private FolderHelperServiceFactory folderHelperServiceFactory;

    @Autowired
    private FileHelperServiceFactory fileHelperServiceFactory;

    @Autowired
    private IResourceManagerFactory resourceManagerFactory;

    @Autowired
    private DocFileServiceFactory docFileServiceFactory;

    @Autowired
    private OXDocumentFactory oxDocFactory;

    @Autowired
    private ImExporterAccessFactory imExporterAccessFactory;

    @Autowired
    private StorageHelperServiceFactory storageHelperServiceFactory;

    @Autowired
    private Statistics statistics;

    /**
     * Tries to save a new document using a base document stream, stored actions and provided actions by a client.
     * The code tries to find out where the client derivates from the stored actions and combine it to a new valid
     * operations stream. This finally will be used to store the restored document.
     *
     * @param id
     * @param folderId
     * @param targetFolderId
     * @param initialFileName
     * @return
     * @throws Exception
     * @throws OXException
     */
    @Override
    public final RestoreData saveAsWithBackupDocumentAndOperations(final ServerSession session, final String id, final String restoreID, final String targetFolderId, final String initialFileName, final JSONArray operations) throws Exception {
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        RestoreData restoreData = new RestoreData(errorCode, null, null, null);

        // log request
        logRestoreMessage(RestoreMessage.RESTORE_REQUESTED, restoreID, targetFolderId, errorCode);

        // there are two different formats of the operations
        // 1. operations array:
        //    [{op1}, {op2}, {op3}, {op4}, ...]
        // 2. operations encapsulate in an array of operations
        //    [{operations: [{op1},{op2}, ...]}, {operations: [{op1}, {op2}, ....}]}
        // check if we have to convert the format -> we always want to provide type 1
        JSONArray actions = null;
        if ((null != operations) && (operations.length() > 0)) {
            try {
                final JSONObject obj = operations.getJSONObject(0);

                if (obj.optJSONArray("operations") != null) {
                    // we have format 2 must be converted
                    JSONArray realOpsArray = null;
                    actions = new JSONArray();

                    for (int i = 0; i < operations.length(); i++) {
                        final JSONObject opsObject = operations.getJSONObject(i);
                        realOpsArray = opsObject.getJSONArray("operations");
                        JSONHelper.appendArray(actions, realOpsArray);
                    }
                } else {
                    // easy way - no conversion
                    actions = operations;
                }
            } catch (JSONException e) {
                errorCode = ErrorCode.BACKUPDOCUMENT_CLIENTACTIONS_MALFORMED;
            }
        } else {
            // no client operations - make sure we always have an array
            // in this special case we have an empty one
            actions = new JSONArray();
        }

        if (errorCode.isNoError() && (null != backupController)) {
            // BE CAREFUL - we receive a plain id via string which is not encoded - make sure
            // to use the correct function to create the DocResourceID
            final DocRestoreID docRestoreID = DocRestoreID.create(restoreID);
            final DocumentRestoreData docRestoreData = backupController.getDocumentRestoreData(docRestoreID);

            if ((null != docRestoreData) && (docRestoreData.getErrorCode().isNoError())) {
                // in case of success we can try to restore the client document using the backup document stream
                // and all stored operations
                //final String baseVersion = docRestoreData.getBaseVersion();
                final int baseOSN = docRestoreData.getBaseOSN();
                final JSONArray docAppliedOps = docRestoreData.getOperations();
                final int currOSN = docRestoreData.getOSN();
                final String origFileName = docRestoreData.getFileName();
                final String toRestoreFileName = StringUtils.isEmpty(initialFileName) ? docRestoreData.getFileName() : initialFileName;
                final String mimeType = docRestoreData.getMimeType();
                final IResourceManager resManager = docRestoreData.getResourceManager();

                try {
                    if (null != resManager) {
                        resManager.lockResources(true);
                    }

                    // make sure that we always provide a JSONArray instance (will be empty, if we don't have store ops)
                    final JSONArray storedDocAppliedOps = (null == docAppliedOps) ? new JSONArray() : docAppliedOps;
                    // use restore document class/interface to merge the operations from document & client
                    final JSONArray operationsToBeStored = this.getRestoredOperations(baseOSN, currOSN, storedDocAppliedOps, actions);

                    final InputStream baseDocStream = docRestoreData.getInputStream();
                    restoreData = this.saveRestoredDocumentOperations(session, baseDocStream, resManager, origFileName, targetFolderId, toRestoreFileName, mimeType, operationsToBeStored);

                    // set error code from restore data
                    errorCode = restoreData.getErrorCode();
                } catch (RestoreException e) {
                    log.warn("RT connection: Restore exception caught trying to restore document with Hazelcast data", e);
                    errorCode = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                } catch (FilterException e) {
                    log.warn("RT connection: Filter exception caught trying to restore document with Hazelcast data", e);
                    errorCode = FilterExceptionToErrorCode.map(e, ErrorCode.SAVEDOCUMENT_FAILED_ERROR);
                } catch (Exception e) {
                    log.warn("RT connection: Exception caught trying to restore document with Hazelcast data", e);
                    errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                } finally {
                    if (null != resManager) {
                        resManager.lockResources(false);
                    }
                }
            } else {
                // We have no data found using our local DocumentBackupManager nor using the
                // global Hazelcast map. As a fall-back try to use the document file as the base.
            	final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);
                final IResourceManager resourceManager = resourceManagerFactory.createInstance(docRestoreID.toString());
                final String folderId = folderHelperService.getFolderId(id);
                final OXDocument oxDoc = oxDocFactory.createDocumentAccess(session, folderId, id, false, resourceManager, false, null);

                // check error state provided by OXDocuments - a non-existent file
                // would be detected here
                if (oxDoc.getLastError().isNoError()) {
                    try {
                        final IImporter importer = imExporterAccessFactory.getImporterAccess(docFileServiceFactory.createInstance().getDocumentFormat());
                        final JSONObject operationsObject = oxDoc.getOperations(importer, null, false);

                        if (null != operationsObject) {
                            final JSONArray docOperations = operationsObject.getJSONArray(OperationHelper.KEY_OPERATIONS);
                            final int baseOSN = (oxDoc.getUniqueDocumentId() != -1) ? oxDoc.getUniqueDocumentId() : operationsObject.getJSONArray(OperationHelper.KEY_OPERATIONS).length();
                            final int currOSN = OperationHelper.getFollowUpOSN(operationsObject);

                            // use restore document class/interface to merge the operations from document & client
                            // If we use a stored document, we cannot really merge but we have to hope that the
                            // client starts exactly with the next follow-up operation state number!!
                            final JSONArray operationsToBeStored = this.getRestoredOperations(baseOSN, currOSN, docOperations, actions);

                            final String mimeType = oxDoc.getMetaData().getFileMIMEType();
                            final IResourceManager resManager = resourceManagerFactory.createInstance(docRestoreID.toString());
                            final InputStream docStream = new ByteArrayInputStream(oxDoc.getDocumentBuffer());
                            restoreData = saveRestoredDocumentOperations(session, docStream, resManager, oxDoc.getMetaData().getFileName(), targetFolderId, initialFileName, mimeType, operationsToBeStored);
                            errorCode = restoreData.getErrorCode();
                        } else {
                            errorCode = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                        }
                    } catch (final FilterException e) {
                        log.warn("RT connection: Filter exception caught trying to restore document", e);
                        errorCode = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                    } catch (final JSONException e) {
                        log.warn("RT connection: JSON exception caught trying to restore document", e);
                        errorCode = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                    } catch (RestoreException e) {
                        log.warn("RT connection: Restore exception caught trying to restore document", e);
                        errorCode = ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                    }
                } else {
                    errorCode = oxDoc.getLastError();
                }
            }
        } else if (errorCode.isNoError()) {
            errorCode = ErrorCode.BACKUPDOCUMENT_SERVICE_NOT_AVAILABLE;
        }

        if (errorCode.isError()) {
            logRestoreMessage(RestoreMessage.RESTORE_FAILED, restoreID, targetFolderId, errorCode);
            statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.RESTORED_ERROR));
        } else {
            logRestoreMessage(RestoreMessage.RESTORE_SUCCESS, restoreID, targetFolderId, errorCode);
            statistics.handleRestoreDocEvent(new RestoreDocEvent(RestoreDocEventType.RESTORED_SUCCESS));
        }

        // set the latest error code
        restoreData.setErrorCode(errorCode);

        return restoreData;
    }

    /**
     * Creates the restore document file using the base document stream and the operations
     * for the restoring.
     *
     * @param session the session of the user requesting the restore of the document
     * @param baseDocStream the document stream of the existing base document
     * @param resManager the resource manager containing the needed image data
     * @param origFileName the original file name of the document file
     * @param targetFolderId the folder where the restore document should be written to
     * @param toRestoreFileName the file name of the restored document file
     * @param mimeType the mime type of the document
     * @param operationsToBeStored the operations to be applied for restoring the document content
     * @return an restore data instance with error code and other information
     */
    private RestoreData saveRestoredDocumentOperations(final ServerSession session, final InputStream baseDocStream, final IResourceManager resManager, final String origFileName, String targetFolderId, final String toRestoreFileName, final String mimeType, final JSONArray operationsToBeStored) {
        RestoreData result = new RestoreData(ErrorCode.NO_ERROR, null, null, null);
        String restoredFileName = null;
        String restoredFileId = null;
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        if (null != baseDocStream) {

            try {
            	final FolderHelperService folderHelperService = folderHelperServiceFactory.create(session);

                if (!folderHelperService.providesFolderPermissions(targetFolderId, false, true, true)) {
                    // Fallback if we are not allowed to create a new file within the original folder
                    // Use the documents folder of the user as 2nd target folder
                    targetFolderId = folderHelperService.getUserDocumentsFolderId(session);
                }

                int[] folderPermissions = folderHelperService.getFolderPermissions(targetFolderId);

                if (null != folderPermissions) {
                    boolean canCreate = folderPermissions[FolderHelperService.FOLDER_PERMISSIONS_FOLDER] >= FileStoragePermission.CREATE_OBJECTS_IN_FOLDER;
                    boolean canWrite = folderPermissions[FolderHelperService.FOLDER_PERMISSIONS_WRITE] > FileStoragePermission.NO_PERMISSIONS;

                    // check permissions and bail out with error code
                    if (!canCreate) { result = new RestoreData(ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR, restoredFileName, restoredFileId, targetFolderId); return result; }
                    if (!canWrite) { result = new RestoreData(ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR, restoredFileName, restoredFileId, targetFolderId); return result; }

                    final StorageHelperService storageHelper = storageHelperServiceFactory.create(session, targetFolderId);

                    final OXDocument oxDoc = oxDocFactory.createDocumentAccess(session, baseDocStream, origFileName, mimeType, resManager, storageHelper);
                    final IExporter exporter = imExporterAccessFactory.getExporterAccess(DocFileHelper.getDocumentFormat(origFileName));

                    final ResolvedStreamInfo resolvedStreamInfo = oxDoc.getResolvedDocumentStream(exporter, resManager, operationsToBeStored, FileStorageFileAccess.CURRENT_VERSION, true, false, false);
                    if ((null != resolvedStreamInfo) && (null != resolvedStreamInfo.resolvedStream)) {

                        final WriteRestoreInfo info = writeRestoreDocumentFile(session, resolvedStreamInfo.resolvedStream, origFileName, toRestoreFileName, targetFolderId, mimeType);
                        errorCode = info.getErrorCode();
                        restoredFileName = info.getRestoredFileName();
                        restoredFileId = info.getRestoredFileId();

                        if (errorCode.isNoError()) {
                            errorCode = resolvedStreamInfo.errorCode;
                            // map the partial save to a backup specific error code to enable
                            // better error messages on the client side
                            if (resolvedStreamInfo.errorCode.getCode() == ErrorCode.SAVEDOCUMENT_FAILED_FILTER_OPERATION_ERROR.getCode()) {
                                errorCode = ErrorCode.BACKUPDOCUMENT_RESTORE_DOCUMENT_PARTIAL_WRITTEN;
                            }

                            // check if we need to store a _ox file, too - only in case of version support
                            if ((null != resolvedStreamInfo.debugStream) && (storageHelper.supportsFileVersions())) {
                                final File restoredMetaData = info.getRestoredMetaData();
                                final String restoredVersion =  restoredMetaData.getVersion();

                                try {
                                	DocFileService docFileService = docFileServiceFactory.createInstance();
	                                final WriteInfo writeInfo = docFileService.writeDocumentStream(session, resolvedStreamInfo.debugStream, restoredFileId, targetFolderId, info.getRestoredMetaData(), storageHelper, Properties.OX_RESCUEDOCUMENT_EXTENSION_APPENDIX, true, false, false);
	                                if (writeInfo.errorCode.isNoError()) {
	                                	docFileService.setVersionOfFile(session, targetFolderId, restoredFileId, restoredVersion);
	                                }
                                } catch (Exception e) {
                                    log.debug("RT connection: Exception caught while trying to store _ox document version!", e);
                                }
                            }
                        }
                    } else {
                        errorCode = (null != resolvedStreamInfo) ? resolvedStreamInfo.errorCode : ErrorCode.BACKUPDOCUMENT_SYNC_NOT_POSSIBLE;
                    }
                } else {
                    errorCode = ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR;
                }
            } catch (OXException e) {
                log.warn("RT connection: Exception caught trying to store restored document", e);
                errorCode = ExceptionToErrorCode.map(e, ErrorCode.GENERAL_UNKNOWN_ERROR, false);
            }

            result = new RestoreData(errorCode, restoredFileName, restoredFileId, targetFolderId);
        }

        return result;
    }

    /**
     * Creates a JSONArray with operations retrieved from the storedOperations and clientOperations
     * at the point where clientOperations derived from the storedOperations.
     *
     * @param baseOSN the operation state number of the first stored operation
     * @param currOSN the operation state number following the last stored operation
     * @param
     */
    @Override
    public JSONArray getRestoredOperations(int baseOSN, int currOSN, final JSONArray storedOperations, final JSONArray clientOperations) throws RestoreException {
        Validate.notNull(storedOperations);
        Validate.notNull(clientOperations);

        JSONArray operationsToBeRestored = null;

        try {
            if (JSONHelper.isNullOrEmpty(clientOperations)) {
                // in case of no client operations we just store the latest
                // changed document using the stored base stream and the stored
                // applied operations
                operationsToBeRestored = storedOperations;
            } else {
                // in case we have client operations we try to find out at
                // what point we have to apply them
                final JSONObject firstClientOp = clientOperations.getJSONObject(0);
                if (!JSONHelper.isNullOrEmpty(storedOperations)) {
                    final JSONObject firstBackupOp = storedOperations.getJSONObject(0);
                    final int firstClientOSN = firstClientOp.getInt("osn");
                    final int firstBackupOSN = firstBackupOp.getInt("osn");

                    if ((firstClientOSN >= baseOSN) && (firstClientOSN <= currOSN)) {
                        if (firstClientOSN == currOSN) {
                            // append client operations to the backup document operations
                            operationsToBeRestored = JSONHelper.appendArray(storedOperations, clientOperations);
                        } else {
                            operationsToBeRestored = new JSONArray();

                            if (firstClientOSN > firstBackupOSN) {
                                // copy the backup document operation up-to the branch point &
                                // append the client operations
                                int osn = 0;
                                int opl = 0;
                                int index = 0;
                                while ((index < storedOperations.length()) && (osn < firstClientOSN)) {
                                    final JSONObject op = storedOperations.getJSONObject(index++);
                                    osn = op.getInt("osn");
                                    opl = op.getInt("opl");
                                    operationsToBeRestored.put(op);
                                    osn += opl;
                                }
                            }

                            operationsToBeRestored = JSONHelper.appendArray(operationsToBeRestored, clientOperations);
                        }
                    } else {
                        // logical problem - shouldn't happen (the base stream of the document
                        // is newer than the changed document or we have a hole between client
                        // operations and changed doc operations.
                        // BAIL OUT and provide an error message!
                        throw new RestoreException("Restore Document. Logical problem detected, base document newer than client operations!", RestoreException.ErrorCode.SYNC_NO_MERGE_POINT_ERROR);
                    }
                } else {
                    // no stored operations but client operations -> try to
                    // make a restore with these operations
                    operationsToBeRestored = clientOperations;
                }
            }
        } catch (final JSONException e) {
            throw new RestoreException("Restore Document - JSON exception caught!", RestoreException.ErrorCode.SYNC_NOT_POSSIBLE_ERROR);
        }

        return operationsToBeRestored;
    }

    /**
     * Writes the document stream to the storage.
     *
     * @param session the session of the client which requests restore document
     * @param docStream the final document stream to be restored
     * @param origFileName the original file name
     * @param toRestoreFileName the file name of the restored file
     * @param targetFolderId the folder id where the restored file should be written
     * @param mimeType the mime type of the document
     * @return
     * @throws OXException
     */
    private WriteRestoreInfo writeRestoreDocumentFile(final Session session, final InputStream docStream, final String origFileName, final String toRestoreFileName, final String targetFolderId, final String mimeType) throws OXException {
        ErrorCode errorCode = ErrorCode.NO_ERROR;

        final String orgExt = FileHelper.getExtension(origFileName);
        final String restoredExt = FileHelper.getExtension(toRestoreFileName);
        String restoreFileName = toRestoreFileName;

        // make sure that the new file name has an extension
        if (StringUtils.isEmpty(restoredExt)) {
            final StringBuffer strBuf = new StringBuffer(toRestoreFileName);
            strBuf.append(".");
            strBuf.append(orgExt);
            restoreFileName = strBuf.toString();
        }

        final File file = new DefaultFile();
        file.setFileName(restoreFileName);
        file.setFolderId(targetFolderId);
        file.setFileMIMEType(mimeType);
        file.setTitle(restoreFileName);

    	final FileHelperService fileHelperService = fileHelperServiceFactory.create(session);
        errorCode = fileHelperService.createFileAndWriteStream(file, docStream);

        // retrieve the file name and file id and set it
        final String restoredFileName = file.getFileName();
        final String restoredFileId = file.getId();

        return new WriteRestoreInfo(errorCode, restoredFileName, restoredFileId, file);
    }

    /**
     * Writes a message to the log using log-level INFO.
     *
     * @param msgType
     * @param restoreId
     * @param targetFolderId
     * @param errorCode
     */
    private void logRestoreMessage(final RestoreMessage msgType, final String restoreId, final String targetFolderId, final ErrorCode errorCode) {
        final String logRestoreId = (StringUtils.isEmpty(restoreId) ? "unknown" : restoreId);
        final String logTargetFolderId = (StringUtils.isEmpty(targetFolderId) ? "unknown" : targetFolderId);

        String logMessage = null;
        switch (msgType) {
            case RESTORE_REQUESTED:
                logMessage = "RestoreDocument: Restore document requested for: " + logRestoreId + ", target folder: " + logTargetFolderId;
                log.info(logMessage);
            break;

            case RESTORE_SUCCESS:
                logMessage = "RestoreDocument: Restore document was successful for: " + logRestoreId + ", target folder: " + logTargetFolderId;
                log.info(logMessage);
            break;

            case RESTORE_FAILED:
                logMessage = "RestoreDocument: Restore document failed for: " + logRestoreId + ", target folder: " + logTargetFolderId + ", with error: " + errorCode.toString();
                log.warn(logMessage);
            break;

            default:
                logMessage = "RestoreDocument: unknown message type for 'Restore Document', please check implementation!";
                log.error(logMessage);
                break;
        }
    }
}
