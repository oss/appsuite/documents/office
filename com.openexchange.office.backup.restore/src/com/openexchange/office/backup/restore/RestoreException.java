/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.backup.restore;


/**
 *
 * @author carsten-driesner
 *
 */
public class RestoreException extends RuntimeException {

    /**
    *
    */
   private static final long serialVersionUID = 1L;

   public enum ErrorCode {
       SYNC_NO_MERGE_POINT_ERROR,
       SYNC_NOT_POSSIBLE_ERROR
   };

   final private ErrorCode errorCode;

   public RestoreException(String message, ErrorCode eCode) {
       super(message);
       errorCode = eCode;
   }

   public RestoreException(String message, ErrorCode eCode, Exception e) {
       super(message, e);
       errorCode = eCode;
   }

   ErrorCode getErrorCode() {
       return this.errorCode;
   }

}
