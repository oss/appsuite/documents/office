/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.backup.restore;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.office.backup.restore.IRestoreDocument;
import com.openexchange.office.backup.restore.impl.RestoreDocument;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

public class OperationsRestoreTest {
    // restore test 1
    private final int sBaseOSN1 = 23;
    private final int sCurrOSN1 = 320;
    private final int cBaseOSN1 = 261; // split point!
    private final int cCurrOSN1 = 334;
    private final String sOps101 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[0],\"osn\":23,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"u\",\"" + OCKey.START.value() + "\":[0,0],\"osn\":24,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"reiuiowe uriiu wiourwu wreouwroiuwr oiuwroiu wioruiowr uiowruiow ruiourw iouwriouw ruwrouwriouiowr iowruiowru oiwurio wuiouwriowu riowruiouwriouwriouwrio\",\"" + OCKey.START.value() + "\":[0,1],\"osn\":25,\"opl\":120}]";
    private final String sOps102 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"uwr iouwroiu wriouwriouwr iouwriouwr\",\"" + OCKey.START.value() + "\":[0,154],\"osn\":145,\"opl\":29}]";
    private final String sOps103 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"2 28928928924892 89289248902489024 8902489024 890\",\"" + OCKey.START.value() + "\":[1,0],\"osn\":174,\"opl\":40},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[1,49],\"osn\":214,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"234 8 247 24724 9024890243890234890 234 890234890234890 \",\"" + OCKey.START.value() + "\":[2,0],\"osn\":215,\"opl\":39},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[2,56],\"osn\":254,\"opl\":1}]";
    private final String sOps104 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"SYNC:\",\"" + OCKey.START.value() + "\":[3,0],\"osn\":255,\"opl\":5}]";
    private final String sOps105 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[3,5],\"osn\":260,\"opl\":1}]";
    private final String sOps106 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"Hier kommt Kurt, ohne Hel\",\"" + OCKey.START.value() + "\":[4,0],\"osn\":261,\"opl\":25}]";
    private final String sOps107 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"m und ohne Gurt. Ri\",\"" + OCKey.START.value() + "\":[4,25],\"osn\":286,\"opl\":19},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[4,43],\"" + OCKey.END.value() + "\":[4,43],\"osn\":305,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[4,42],\"" + OCKey.END.value() + "\":[4,42],\"osn\":306,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"Ei\",\"" + OCKey.START.value() + "\":[4,42],\"osn\":307,\"opl\":2}]";
    private final String sOps108 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"nfach Kurt!\",\"" + OCKey.START.value() + "\":[4,44],\"osn\":309,\"opl\":11}]";
    private final String cOps101 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"029389028 24908 24908 423 9809028 3908 324 980\",\"" + OCKey.START.value() + "\":[4,0],\"osn\":261,\"opl\":23},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[4,46],\"osn\":284,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"23 234 8324 8342 732487 324879324879234 7893847\",\"" + OCKey.START.value() + "\":[5,0],\"osn\":285,\"opl\":24},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[5,47],\"osn\":309,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"324 9783 2487934 89734 87934879 4\",\"" + OCKey.START.value() + "\":[6,0],\"osn\":310,\"opl\":21}]";
    private final String cOps102 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"3 \",\"" + OCKey.START.value() + "\":[6,33],\"osn\":331,\"opl\":1}]";
    private final String cOps103 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"8\",\"" + OCKey.START.value() + "\":[6,35],\"osn\":332,\"opl\":1}]";
    private final String cOps104 = "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() + "\",\"text\":\"79\",\"" + OCKey.START.value() + "\":[6,36],\"osn\":333,\"opl\":1}]";
    // restore test 2
    private final int sBaseOSN2 = 23;
    private final int sCurrOSN2 = 200;
    private final int cBaseOSN2 = 36;
    private final int cCurrOSN2 = 219;
    private final String sOps201 = "[{\"" + OCKey.NAME.value() + "\":\"base01\",\"osn\":23,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"base02\",\"osn\":24,\"opl\":12},{\"" + OCKey.NAME.value() + "\":\"base03\",\"osn\":36,\"opl\":144},{\"" + OCKey.NAME.value() + "\":\"base04\",\"osn\":180,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"base05\",\"osn\":181,\"opl\":19}]";
    private final String cOps201 = "[{\"" + OCKey.NAME.value() + "\":\"client01\",\"osn\":36,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"client02\",\"osn\":37,\"opl\":17},{\"" + OCKey.NAME.value() + "\":\"client03\",\"osn\":54,\"opl\":106},{\"" + OCKey.NAME.value() + "\":\"client04\",\"osn\":160,\"opl\":19},{\"" + OCKey.NAME.value() + "\":\"client05\",\"osn\":179,\"opl\":40}]";
    // restore test 3
    private final int sBaseOSN3 = 23;
    private final int sCurrOSN3 = 156;
    private final int cBaseOSN3 = 23;
    private final int cCurrOSN3 = 178;
    private final String sOps301 = "[{\"" + OCKey.NAME.value() + "\":\"base01\",\"osn\":23,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"base02\",\"osn\":24,\"opl\":12},{\"" + OCKey.NAME.value() + "\":\"base03\",\"osn\":36,\"opl\":114},{\"" + OCKey.NAME.value() + "\":\"base04\",\"osn\":150,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"base05\",\"osn\":151,\"opl\":5}]";
    private final String cOps301 = "[{\"" + OCKey.NAME.value() + "\":\"client01\",\"osn\":23,\"opl\":1},{\"" + OCKey.NAME.value() + "\":\"client02\",\"osn\":24,\"opl\":17},{\"" + OCKey.NAME.value() + "\":\"client03\",\"osn\":41,\"opl\":106},{\"" + OCKey.NAME.value() + "\":\"client04\",\"osn\":147,\"opl\":19},{\"" + OCKey.NAME.value() + "\":\"client05\",\"osn\":166,\"opl\":12}]";


    public OperationsRestoreTest() {

    }

    @BeforeEach
    public void setUp()
        throws Exception
    {
    }

    @AfterEach
    public void tearDown()
        throws Exception
    {
    }

    @Test
    /**
     * Checks a normal case using the operations from a real-world test.
     */
    public void testOperationsRestore1() {
        final IRestoreDocument restoreDocument = new RestoreDocument();

        try {
            // create json array from JSON strings
            final JSONArray storedOperations = createJSONArrayFromJSONStrings(new String[] {sOps101, sOps102, sOps103, sOps104, sOps105, sOps106, sOps107, sOps108});
            final JSONArray clientOperations = createJSONArrayFromJSONStrings(new String[] {cOps101, cOps102, cOps103, cOps104});

            checkResultJSONArray(sBaseOSN1, sCurrOSN1, storedOperations, null);
            checkResultJSONArray(cBaseOSN1, cCurrOSN1, clientOperations, null);
            final JSONArray result = restoreDocument.getRestoredOperations(sBaseOSN1, sCurrOSN1, storedOperations, clientOperations);
            assertNotNull(result);

            checkResultJSONArray(sBaseOSN1, cCurrOSN1, result, null);

        } catch (JSONException e) {
            fail("JSON Exception caught: " + e.getMessage());
        }
    }

    @Test
    /**
     * Checks that the code correctly splits the doc operations and appends client operations
     */
    public void testOperationsRestore2() {
        final IRestoreDocument restoreDocument = new RestoreDocument();

        try {
            // create json array from JSON strings
            final JSONArray storedOperations = createJSONArrayFromJSONStrings(new String[] {sOps201});
            final JSONArray clientOperations = createJSONArrayFromJSONStrings(new String[] {cOps201});

            checkResultJSONArray(sBaseOSN2, sCurrOSN2, storedOperations, null);
            checkResultJSONArray(cBaseOSN2, cCurrOSN2, clientOperations, null);
            final JSONArray result = restoreDocument.getRestoredOperations(sBaseOSN2, sCurrOSN2, storedOperations, clientOperations);
            assertNotNull(result);

            checkResultJSONArray(sBaseOSN2, cCurrOSN2, result, new String[] {"base01", "base02", "client01", "client02", "client03", "client04", "client05"});

        } catch (JSONException e) {
            fail("JSON Exception caught: " + e.getMessage());
        }
    }

    @Test
    /**
     * Special case: Client can start with the same operations as the document. In this
     * case the client operations must be completely taken.
     */
    public void testOperationsRestore3() {
        final IRestoreDocument restoreDocument = new RestoreDocument();

        try {
            // create json array from JSON strings
            final JSONArray storedOperations = createJSONArrayFromJSONStrings(new String[] {sOps301});
            final JSONArray clientOperations = createJSONArrayFromJSONStrings(new String[] {cOps301});

            checkResultJSONArray(sBaseOSN3, sCurrOSN3, storedOperations, null);
            checkResultJSONArray(cBaseOSN3, cCurrOSN3, clientOperations, null);
            final JSONArray result = restoreDocument.getRestoredOperations(sBaseOSN3, sCurrOSN3, storedOperations, clientOperations);
            assertNotNull(result);

            checkResultJSONArray(sBaseOSN3, cCurrOSN3, result, new String[] {"client01", "client02", "client03", "client04", "client05"});

        } catch (JSONException e) {
            fail("JSON Exception caught: " + e.getMessage());
        }
    }

    private final JSONArray createJSONArrayFromJSONStrings(final String[] opsStrings) throws JSONException {
        final JSONArray result = new JSONArray();

        for (final String s : opsStrings) {
            appendJSONArray(result, new JSONArray(s));
        }

        return result;
    }

    private final void appendJSONArray(final JSONArray target, final JSONArray append) {
        Iterator<Object> entryIter = append.iterator();
        while (entryIter.hasNext()) {
            target.put(entryIter.next());
        }
    }

    private final void checkResultJSONArray(final int baseOSN, final int currOSN, final JSONArray result, final String[] opsOrder) throws JSONException {
        int osn = baseOSN;

        if (opsOrder != null) {
            assertEquals(opsOrder.length, result.length());
        }

        for (int i = 0; i < result.length(); i++) {
            final JSONObject op = result.getJSONObject(i);

            final String opName = op.getString(OCKey.NAME.value());
            int opOSN = op.getInt("osn");
            int opOPL = op.getInt("opl");

            if (opsOrder != null) {
                // check operation order via name property, too
                assertTrue(opsOrder[i].equals(opName));
            }

            assertEquals(osn, opOSN);
            int tmpOSN = opOSN + opOPL;

            assertTrue(tmpOSN > osn);
            osn = tmpOSN;
        }

        assertEquals(osn, currOSN);
    }
}
