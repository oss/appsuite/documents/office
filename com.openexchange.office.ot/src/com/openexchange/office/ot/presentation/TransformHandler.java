/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.presentation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.tools.ITransformHandler;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.TransformHandlerBasic;
import com.openexchange.office.ot.OTException;
import com.openexchange.office.ot.OTException.Type;
import com.openexchange.office.ot.tools.OpPair;
import com.openexchange.office.ot.tools.OTUtils.OTMoveMoveResult;
import com.openexchange.office.ot.tools.OTUtils.OTMoveResult;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.ot.tools.OTUtils;

public class TransformHandler implements ITransformHandlerMap {

    @Override
    public Map<OpPair, ITransformHandler> getTransformHandlerMap() {
        return opPairMap;
    }

    @Override
    public Set<OCValue> getSupportedOperations() {
        return supOpsSet;
    }

    @Override
    public Set<OCValue> getIgnoreOperations() {
        return null;
    }

    static final public ImmutableSet<OCValue> supOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.CHANGE_COMMENT)
        .add(OCValue.CHANGE_LAYOUT)
        .add(OCValue.CHANGE_MASTER)
        .add(OCValue.DELETE_COMMENT)
        .add(OCValue.DELETE_TARGET_SLIDE)
        .add(OCValue.GROUP)
        .add(OCValue.INSERT_COMMENT)
        .add(OCValue.INSERT_LAYOUT_SLIDE)
        .add(OCValue.INSERT_MASTER_SLIDE)
        .add(OCValue.INSERT_SLIDE)
        .add(OCValue.MOVE)
        .add(OCValue.MOVE_LAYOUT_SLIDE)
        .add(OCValue.MOVE_SLIDE)
        .add(OCValue.UNGROUP)
        .build();

    static final public ImmutableMap<OpPair, ITransformHandler> opPairMap;

    static {

        final Map<OpPair, ITransformHandler> map = new HashMap<OpPair, ITransformHandler>();
        final Set<OpPair> aliases = new HashSet<OpPair>();

        // ALIAS_IGNORABLE
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.CHANGE_LAYOUT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.CHANGE_MASTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.DELETE_TARGET_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.GROUP, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_MASTER_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.MOVE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.MOVE_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.MOVE_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.UNGROUP, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        // ALIAS_INSERT_CHAR
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.CHANGE_LAYOUT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.CHANGE_MASTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.DELETE_TARGET_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.GROUP, (options, localOp, externOp) -> handleGroupInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_MASTER_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.MOVE, (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.MOVE_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.MOVE_SLIDE, (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.UNGROUP, (options, localOp, externOp) -> handleUngroupInsertChar(localOp, externOp));
        // ALIAS_INSERT_COMP
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.CHANGE_LAYOUT, (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.CHANGE_MASTER, (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.DELETE_TARGET_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.GROUP, (options, localOp, externOp) -> handleGroupInsertComp(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_MASTER_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.MOVE, (options, localOp, externOp) -> handleMoveInsertComp(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.MOVE_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.MOVE_SLIDE, (options, localOp, externOp) -> handleMoveSlideInsertComp(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.UNGROUP, (options, localOp, externOp) -> handleUngroupInsertComp(localOp, externOp));
        // ALIAS_MERGE_COMP
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.CHANGE_LAYOUT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.CHANGE_MASTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.DELETE_TARGET_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.GROUP, (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_MASTER_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleMergeParaInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.MOVE, (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.MOVE_LAYOUT_SLIDE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.MOVE_SLIDE, (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.UNGROUP, (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp));
        // INSERT_CELLS
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertCellsDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertCellsDelete(localOp, externOp));

        opPairMap = new ImmutableMap.Builder<OpPair, ITransformHandler>()

        .putAll(map)

        // CHANGE_COMMENT
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleChangeCommentChangeComment(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleChangeCommentDeleteComment(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleChangeCommentInsertComment(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // CHANGE_LAYOUT
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> handleChangeLayoutChangeLayout(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleChangeLayoutDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_LAYOUT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // CHANGE_MASTER
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.CHANGE_MASTER), (options, localOp, externOp) ->  handleChangeLayoutChangeLayout(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleChangeLayoutDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_MASTER, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // DELETE_COMMENT
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleChangeCommentDeleteComment(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.CHANGE_MASTER), (options, localOp, externOp) ->  TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleDeleteCommentDeleteComment(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleDeleteCommentInsertComment(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.DELETE), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COMMENT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // DELETE_TARGET_SLIDE
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> handleChangeLayoutDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) ->  handleChangeLayoutDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTargetDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> handleInsertLayoutSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> handleInsertMasterSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> handleMoveLayoutSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_TARGET_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // GROUP
        .put(new OpPair(OCValue.GROUP, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGroup(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleGroupInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.MOVE), (options, localOp, externOp) -> handleGroupMove(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.GROUP, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.UNGROUP), (options, localOp, externOp) -> handleGroupUngroup(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.GROUP, OCValue.DELETE), (options, localOp, externOp) -> handleGroupDelete(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_CELLS), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> handleGroupInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.INSERT_ROWS), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> handleGroupSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.GROUP, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> handleGroupSetAttrs(localOp, externOp))
        // INSERT_COMMENT
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleChangeCommentInsertComment(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.CHANGE_MASTER), (options, localOp, externOp) ->  TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleDeleteCommentInsertComment(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleInsertCommentInsertComment(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // INSERT_LAYOUT_SLIDE
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleInsertLayoutSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> handleInsertLayoutSlideInsertLayoutSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> handleInsertLayoutSlideMoveLayoutSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LAYOUT_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // INSERT_MASTER_SLIDE
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleInsertMasterSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> handleInsertMasterSlideInsertMasterSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_MASTER_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // INSERT_SLIDE
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleInsertSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> handleGroupInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleInsertSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupInsertComp(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaDelete(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaSplitPara(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp))
        // MOVE
        .put(new OpPair(OCValue.MOVE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.GROUP), (options, localOp, externOp) -> handleGroupMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleMoveInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupMove(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE), (options, localOp, externOp) -> handleMoveDelete(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> handleMoveSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> handleMoveSetAttrs(localOp, externOp))
        // MOVE_LAYOUT_SLIDE
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> handleMoveLayoutSlideDeleteTargetSlide(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> handleInsertLayoutSlideMoveLayoutSlide(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> handleMoveLayoutSlideMoveLayoutSlide(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_LAYOUT_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // MOVE_SLIDE
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.GROUP), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleMoveSlideInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideMoveSlide(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.UNGROUP), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.DELETE), (options, localOp, externOp) -> handleMoveSlideDelete(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.MOVE_SLIDE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        // UNGROUP
        .put(new OpPair(OCValue.UNGROUP, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.GROUP), (options, localOp, externOp) -> handleGroupUngroup(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> handleUngroupInsertComp(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.MOVE), (options, localOp, externOp) -> handleUngroupMove(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UNGROUP, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupUngroup(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.UNGROUP, OCValue.DELETE), (options, localOp, externOp) -> handleUngroupDelete(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_CELLS), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> handleUngroupInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.INSERT_ROWS), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> handleUngroupSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UNGROUP, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> handleUngroupSetAttrs(localOp, externOp))

        //
        // basic operations vs presentation specific
        //
        // DELETE
        .put(new OpPair(OCValue.DELETE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.GROUP), (options, localOp, externOp) -> handleGroupDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleCommentDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupDelete(localOp, externOp))
        // DELETE COLUMNS
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // INSERT_CELLS
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // INSERT_COLUMN
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // INSERT_DRAWING
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.GROUP), (options, localOp, externOp) -> handleGroupInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupInsertChar(localOp, externOp))
        // INSERT_ROWS
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // SET_ATTRIBUTES
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.GROUP), (options, localOp, externOp) -> handleGroupSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.MOVE), (options, localOp, externOp) -> handleMoveSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupSetAttrs(localOp, externOp))
        // SPLIT_PARAGRAPH
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaSplitPara(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // SPLIT_TABLE
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.GROUP), (options, localOp, externOp) -> handleGroupGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupGeneric(localOp, externOp))
        // UPDATE_FIELD
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.CHANGE_LAYOUT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.CHANGE_MASTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.DELETE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.DELETE_TARGET_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.GROUP), (options, localOp, externOp) -> handleGroupSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_MASTER_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.MOVE), (options, localOp, externOp) -> handleMoveSetAttrs(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.MOVE_LAYOUT_SLIDE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.MOVE_SLIDE), (options, localOp, externOp) -> handleMoveSlideGeneric(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.UNGROUP), (options, localOp, externOp) -> handleUngroupSetAttrs(localOp, externOp))
        .build();
    }

    /**
     * Handling a moveSlide operation and an insertChar operation.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     * @throws JSONException
     */
    private static JSONObject handleMoveSlideGeneric(JSONObject localOp, JSONObject extOp) throws JSONException {

        final JSONObject moveSlideOp = OTUtils.getOperation(OCValue.MOVE_SLIDE, localOp, extOp);
        final JSONObject secondOp = moveSlideOp == localOp ? extOp : localOp;

        // the slide start position of the move slide operation
        final JSONArray moveSlideStartPos = OTUtils.getStartPosition(moveSlideOp);
        // the slide end position of the moveSlide operation
        final JSONArray moveSlideToPos = OTUtils.optEndPosition(moveSlideOp);
        // the slide start position of the moveSlide operation
        final int moveSlideStart0 = moveSlideStartPos.getInt(0);
        // the slide start position of the moveSlide operation
        final int moveSlideTo0 = moveSlideToPos.getInt(0);

        // the slide start position of the second operation
        final JSONArray secondStartPos = OTUtils.getStartPosition(secondOp);
        // the optional end position of the second operation
        final JSONArray secondEndPos = OTUtils.optEndPosition(secondOp);
        // the optional to position of the second operation
        final JSONArray secondToPos = OTUtils.optToPosition(secondOp);
        // the slide start position of the second operation
        final int secondStart0 = secondStartPos.getInt(0);

        if (moveSlideStart0 == secondStart0) {
            secondStartPos.put(0, moveSlideTo0);
            if(secondEndPos!=null) {
                secondEndPos.put(0, moveSlideTo0);
            }
            if(secondToPos!=null) {
                secondToPos.put(0, moveSlideTo0);
            }
        }
        else if (moveSlideStart0 > secondStart0 && moveSlideTo0 <= secondStart0) {
            secondStartPos.put(0, secondStart0 + 1);
            if(secondEndPos!=null) {
                secondEndPos.put(0, secondEndPos.getInt(0) + 1);
            }
            if(secondToPos!=null) {
                secondToPos.put(0, secondToPos.getInt(0) + 1);
            }
        } else if (moveSlideStart0 < secondStart0 && moveSlideTo0 >= secondStart0) {
            secondStartPos.put(0, secondStart0 - 1);
            if(secondEndPos!=null) {
                secondEndPos.put(0, secondEndPos.getInt(0) - 1);
            }
            if(secondToPos!=null) {
                secondToPos.put(0, secondToPos.getInt(0) - 1);
            }
        }
        return null;
    };

    private static JSONObject handleInsertLayoutSlideInsertLayoutSlide(JSONObject localOp, JSONObject extOp) throws JSONException {

        if(localOp.getString(OCKey.ID.value()).equals(extOp.getString(OCKey.ID.value()))) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertLayoutSlideInsertLayoutSlide: " + localOp.toString() + ", " + extOp.toString());
        }

        // Both operations influence its position, if they have the same master slide.
        if (!localOp.getString(OCKey.TARGET.value()).equals(extOp.getString(OCKey.TARGET.value()))) {
            return null;
        }
        // reading the integer start positions
        int localStart = localOp.optInt(OCKey.START.value(), 0);
        int extStart = extOp.optInt(OCKey.START.value(), 0);

        if (localStart < extStart) {
            extOp.put(OCKey.START.value(), Integer.valueOf(extStart + 1));
        } else {
            localOp.put(OCKey.START.value(), Integer.valueOf(localStart + 1));
        }
        return null;
    }

    private static JSONObject handleGroupDelete(JSONObject localOp, JSONObject extOp) throws JSONException {

        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the delete operation
        final JSONObject deleteOp = groupOp == extOp ? localOp : extOp;
        // the start position of the delete operation
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the optional end position of the delete operation
        final JSONArray deleteEndPos = OTUtils.optEndPosition(deleteOp);
        // the slide position of the delete operation
        final int deleteSlidePos = deleteStartPos.getInt(0);
        // the start position of the group operation
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // whether the content inside a drawing is removed (grouping is only possible for toplevel drawings)
        final boolean isDrawingContentDeletion = deleteStartPos.length() > groupStartPos.length(); // groupOp.start.length should always be 2

        // whether a complete slide is removed
        if(deleteStartPos.length() == 1) {
            if(deleteSlidePos < groupSlidePos) {
                groupStartPos.put(0, groupSlidePos - 1);
            }
            else if(deleteSlidePos == groupSlidePos) {
                OTUtils.setOperationRemoved(groupOp);
            }
        }
        else if(deleteSlidePos != groupSlidePos) {
            return null;    // fast exit, different slides
        }
        else {

            // the drawing position of the delete operation
            final int drawingPos = deleteStartPos.getInt(1);
            // the array with indices of grouped drawings
            final JSONArray groupedDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
            // is the drawing with deleted content one of the grouped drawings?
            int drawingPosIndex = OTUtils.indexOf(groupedDrawings, drawingPos);
            // the drawing 'start' position of the group operation
            final int groupPos = groupStartPos.getInt(1);

            if(isDrawingContentDeletion) {
                // the content inside a grouped drawing was deleted -> only the delete operation needs to be modified
                if(drawingPosIndex > -1) {
                    // calculating the new position inside the group
                    deleteStartPos.put(1, groupPos);
                    deleteStartPos.add(2, drawingPosIndex);
                    if(deleteEndPos!=null) {
                        deleteEndPos.put(1, groupPos);
                        deleteEndPos.add(2, drawingPosIndex);
                    }
                }
                else {  // not grouped, but the position needs to be decreased -> counting grouped drawings before insertParagraph-drawing
                    for(int i = 0; i < groupedDrawings.length(); i++) {
                        if(groupedDrawings.getInt(i) < drawingPos) {
                            drawingPosIndex++;
                        }
                    }
                    if(drawingPosIndex > 0) {
                        deleteStartPos.put(1, deleteStartPos.getInt(1) - drawingPosIndex);
                        if(deleteEndPos!=null) {
                            deleteEndPos.put(1, deleteEndPos.getInt(1) - drawingPosIndex);
                        }
                    }
                }
            }
            else {

                // a drawing was deleted -> if this drawing is grouped, only a reload can help
                if (drawingPosIndex > -1) {
                    throw new OTException(Type.RELOAD_REQUIRED, "handleGroupDelete: " + localOp.toString() + ", " + extOp.toString());
                }

                // not grouped, but the positions need to be adapted
                int counter = -1;   // decreasing by 1 again, because of the position of the grouped drawing itself
                for(int i = 0; i < groupedDrawings.length(); i++) {
                    int val = groupedDrawings.getInt(i);
                    if(val < drawingPos) {
                        counter++;
                    }
                    else {
                        groupedDrawings.put(i, val - 1);
                    }
                }
                groupStartPos.put(1, groupedDrawings.getInt(0));
                if(counter > 0) {
                    deleteStartPos.put(1, deleteStartPos.getInt(1) - counter);
                    if(deleteEndPos!=null) {
                        deleteEndPos.put(1, deleteEndPos.getInt(1) - counter);
                    }
                }
            }
        }
        return null;
    }

    private static JSONObject handleUngroupDelete(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the slide position of the delete operation
        final JSONObject deleteOp = ungroupOp == extOp ? localOp : extOp;
        // the start position of the delete operation
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the optional end position of the delete operation
        final JSONArray deleteEndPos = OTUtils.optEndPosition(deleteOp);
        // the slide position of the delete operation
        final int deleteSlidePos = deleteStartPos.getInt(0);
        // the start position of the group operation
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // the slide position of the group operation
        final int ungroupSlidePos = ungroupStartPos.getInt(0);

        // whether a complete slide is removed
        if (deleteStartPos.length()==1) { // handling removal of a complete slide
            if (deleteSlidePos < ungroupSlidePos) {
                ungroupStartPos.put(0, ungroupSlidePos-1);
            } else if (deleteSlidePos== ungroupSlidePos) {
                OTUtils.setOperationRemoved(ungroupOp); // ungroup operation can be ignored, because the slide is already removed
            }
            return null;
        }

        // the drawing position of the delete operation
        final int drawingPos = deleteStartPos.getInt(1);
        // the drawing 'start' position of the ungroup operation
        final int ungroupPos = ungroupStartPos.getInt(1);

        // isDeleteDrawingPos
        if (deleteStartPos.length() > ungroupStartPos.length()) {
            // only the delete operation needs to be modified
            if (deleteSlidePos != ungroupSlidePos) {
                return null;
            } // fast exit, different slides

            // the array with indices of ungrouped drawings
            final JSONArray ungroupedDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
            if (drawingPos < ungroupPos) {
                return null;
            } // fast exit, content inside a previous drawing removed

            // is the delete operation inside an ungrouped drawing?
            if (drawingPos == ungroupPos) {
                final int drawingPosInGroup = deleteStartPos.getInt(2);     // the new top level position is specified by the (old) position of the delete operation
                final int newDrawingTopLevelPos = ungroupedDrawings.getInt(drawingPosInGroup);
                deleteStartPos.put(1, newDrawingTopLevelPos);
                deleteStartPos.remove(2);   // index at [2] is no longer required
                if (deleteEndPos!=null) {
                    deleteEndPos.put(1, newDrawingTopLevelPos);
                    deleteEndPos.remove(2); // index at [2] is no longer required
                }
            }
            else {
                // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing
                int fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                // the position of the previous ungrouped drawing
                int previousUngroup = -1;
                // the size of the gap between two ungrouped drawings
                int gap = 0;
                // whether a gap for the inserted drawing or the drawing that got a char inserted was found
                boolean foundGap = false;
                for(int i = 0; i < ungroupedDrawings.length(); i++) {
                    final int value = ungroupedDrawings.getInt(i);
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                int shift = previousUngroup + fillDrawings;
                                deleteStartPos.put(1, shift);
                                if (deleteEndPos!=null) {
                                    deleteEndPos.put(1, shift);
                                }
                                foundGap = true;
                            }
                            else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                }
                if (!foundGap) {
                    int shift = previousUngroup + fillDrawings;
                    deleteStartPos.put(1, shift); // shifting drawing with second operation behind the last ungrouped drawing
                    if (deleteEndPos!=null) {
                        deleteEndPos.put(1, shift);
                    }
                }
            }
        }
        else {
            // a drawing was deleted
            if (deleteSlidePos != ungroupSlidePos) {
                return null;
            } // fast exit, different slides

            if (drawingPos == ungroupPos) { // the complete group was removed
                throw new OTException(Type.RELOAD_REQUIRED, "handleUngroupDelete: " + localOp.toString() + ", " + extOp.toString());
            }
            else if (drawingPos < ungroupPos) { // the deleted drawing is before the ungrouped drawing
                final JSONArray ungroupedDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
                for(int i=0; i < ungroupedDrawings.length(); i++) {
                    ungroupedDrawings.put(i, ungroupedDrawings.getInt(i) - 1);
                }
                ungroupStartPos.put(1, ungroupStartPos.getInt(1)-1);
            }
            else {

                // both operations influence each other
                final JSONArray ungroupedDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
                // the position of the previous ungrouped drawing
                int previousUngroup = -1;
                // the size of the gap between two ungrouped drawings
                int gap = 0;
                // whether a gap for the inserted drawing or the drawing that got a char inserted was found
                boolean foundGap = false;

                // check for a gap for the new inserted drawing in the ungrouped drawings -> modifying inserted drawing and following ungrouped drawings
                int fillDrawings = drawingPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                for(int i = 0; i < ungroupedDrawings.length(); i++) {
                    final int value = ungroupedDrawings.getInt(i);
                    if (previousUngroup >= 0 && !foundGap) {
                        gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                int shift =  previousUngroup + fillDrawings;
                                deleteStartPos.put(1, shift);
                                if (deleteEndPos!=null) {
                                    deleteEndPos.put(1, shift);
                                }
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                    if (value > drawingPos) {
                        ungroupedDrawings.put(i, ungroupedDrawings.getInt(i) - 1);
                    } // decreasing the index for all following ungrouped drawings
                }
                if (!foundGap) {
                    int shift =  previousUngroup + fillDrawings; // shifting the deleted drawing behind the last ungrouped drawing
                    deleteStartPos.put(1, shift);
                    if (deleteEndPos!=null) {
                        deleteEndPos.put(1, shift);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Handling two group operations.
     *
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing that will be grouped.
     *
     * All positions of the group operation are top level positions on the slide.
     *
     * The group operations influence each other, if they are on the same slide.
     * In this case only separated ranges of grouped drawings can be resolved. In
     * all other cases, one group operation must be reverted.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleGroupGroup(JSONObject localOp, JSONObject extOp) throws JSONException {

        // local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // external start pos
        final JSONArray externalStartPos = OTUtils.getStartPosition(extOp);
        // the slide position of the local operation
        final int localSlidePos = localStartPos.getInt(0);
        // the slide position of the external operation
        final int externalSlidePos = externalStartPos.getInt(0);

        if (localSlidePos != externalSlidePos) {
            return null;
        }

        // the drawing start position of the local operation
        final int localDrawingStartPos = localStartPos.getInt(1);
        // the drawing start position of the external operation
        final int externalDrawingStartPos = externalStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray localDrawings = localOp.getJSONArray(OCKey.DRAWINGS.value());
        // the external drawings that will be grouped
        final JSONArray externalDrawings = extOp.getJSONArray(OCKey.DRAWINGS.value());
        // the last local drawing that will be grouped
        final int localDrawingLastPos = localDrawings.getInt(localDrawings.length() - 1);
        // the last external drawing that will be grouped
        final int extDrawingLastPos = externalDrawings.getInt(externalDrawings.length() - 1);
        // whether the group operations overlap each other
        final boolean separatedRange = localDrawingStartPos > extDrawingLastPos || externalDrawingStartPos > localDrawingLastPos;

        if (localDrawingStartPos == externalDrawingStartPos && OTUtils.equalNumberArrays(localDrawings, externalDrawings, null)) {
            // if both operations are identical, both can be ignored
            OTUtils.setOperationRemoved(extOp);
            OTUtils.setOperationRemoved(localOp);
            return null;
        }

        if (!separatedRange) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleGroupGroup: " + localOp.toString() + ", " + extOp.toString());
        }

        // no overlap -> calculating new positions
        if (localDrawingStartPos > extDrawingLastPos) {
            final int groupCount = externalDrawings.length() - 1; // reducing by 1 because of grouped drawing itself
            // reducing all positions of local operation
            for(int i = 0; i < localDrawings.length(); i++) {
                localDrawings.put(i, localDrawings.getInt(i) - groupCount);
            }
            localStartPos.put(1, localDrawingStartPos - groupCount);
        } else {
            final int groupCount = localDrawings.length() - 1;
            // reducing all positions of external operation
            for(int i = 0; i < externalDrawings.length(); i++) {
                externalDrawings.put(i, externalDrawings.getInt(i) - groupCount);
            }
            externalStartPos.put(1, externalDrawingStartPos - groupCount);
        }
        return null;
    }

    /**
     * Handling two ungroup operations.
     *
     * All positions of the ungroup operation are top level positions on the slide.
     *
     * The ungroup operations influence each other, if they are on the same slide.
     * In this case only separated ranges of ungrouped drawings can be resolved. In
     * all other cases, one ungroup operation must be reverted.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleUngroupUngroup(JSONObject localOp, JSONObject extOp) throws JSONException {

        // local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // external start pos
        final JSONArray externalStartPos = OTUtils.getStartPosition(extOp);
        // the slide position of the local operation
        final int localSlidePos = localStartPos.getInt(0);
        // the slide position of the external operation
        final int externalSlidePos = externalStartPos.getInt(0);

        if (localSlidePos != externalSlidePos) {
            return null;
        }

        // the drawing start position of the local operation
        final int localDrawingStartPos = localStartPos.getInt(1);
        // the drawing start position of the external operation
        final int externalDrawingStartPos = externalStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray localDrawings = localOp.getJSONArray(OCKey.DRAWINGS.value());
        // the external drawings that will be grouped
        final JSONArray externalDrawings = extOp.getJSONArray(OCKey.DRAWINGS.value());

        if (localDrawingStartPos == externalDrawingStartPos && OTUtils.equalNumberArrays(localDrawings, externalDrawings, null)) {
            // if both operations are identical, both can be ignored
            OTUtils.setOperationRemoved(extOp);
            OTUtils.setOperationRemoved(localOp);
            return null;
        }

        // handling different drawings, if the first drawing does not have a gap
        if (localDrawingStartPos < externalDrawingStartPos && !OTUtils.hasGap(localDrawings)) {
            final int groupCount = localDrawings.length() - 1; // reducing by 1 because of grouped drawing itself
            // increasing all positions of external operation
            for(int i = 0; i < externalDrawings.length(); i++) {
                externalDrawings.put(i, externalDrawings.getInt(i) + groupCount);
            }
            externalStartPos.put(1, externalDrawingStartPos + groupCount);
            return null;
        }
        else if(externalDrawingStartPos < localDrawingStartPos && !OTUtils.hasGap(externalDrawings)) {
            final int groupCount = externalDrawings.length() - 1;
            // reducing all positions of external operation
            for(int i = 0; i < localDrawings.length(); i++) {
                localDrawings.put(i, localDrawings.getInt(i) + groupCount);
            }
            localStartPos.put(1, localDrawingStartPos + groupCount);
            return null;
        }
        else {
            // overlapping ranges
            throw new OTException(Type.RELOAD_REQUIRED, "handleUngroupUngroup: " + localOp.toString() + ", " + extOp.toString());
        }
    }

    /**
     * Handling a group and an ungroup operations.
     *
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing that will be grouped.
     *
     * All positions of the group and ungroup operations are top level positions on the slide.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: ungroup, start: [2, 5], drawings:[4, 5, 6, 7] }
     */
    private static JSONObject handleGroupUngroup(JSONObject localOp, JSONObject extOp) throws JSONException {

        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the ungroup operation
        final JSONObject ungroupOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // external start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // the slide position of the external operation
        final int ungroupSlidePos = ungroupStartPos.getInt(0);
        if (groupSlidePos != ungroupSlidePos) {
            return null;
        }
        // the drawing start position of the local operation
        final int groupDrawingPos = groupStartPos.getInt(1);
        // the drawing start position of the external operation
        final int ungroupDrawingPos = ungroupStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray groupDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
        // the external drawings that will be grouped
        final JSONArray ungroupDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
        // the last local drawing that will be grouped
        final int groupDrawingLastPos = groupDrawings.getInt(groupDrawings.length() - 1);

        if (ungroupDrawingPos > groupDrawingLastPos) {
            // no overlap -> calculating new positions
            int groupCount = groupDrawings.length() - 1; // reducing by 1 because of grouped drawing itself
            // reducing all positions of local operation
            for(int i = 0; i < ungroupDrawings.length(); i++) {
                ungroupDrawings.put(i, ungroupDrawings.getInt(i) - groupCount);
            }
            ungroupStartPos.put(1, ungroupDrawingPos - groupCount);
            return null;

        }
        else if (ungroupDrawingPos < groupDrawingPos && !OTUtils.hasGap(ungroupDrawings)) {
            int groupCount = ungroupDrawings.length() - 1; // reducing by 1 because of ungrouped drawing itself
            // increasing all positions of external operation
            for(int i = 0; i < groupDrawings.length(); i++) {
                groupDrawings.put(i, groupDrawings.getInt(i) + groupCount);
            }
            groupStartPos.put(1, groupDrawingPos + groupCount);
            return null;

        }
        else {
            // overlapping ranges
            throw new OTException(Type.RELOAD_REQUIRED, "handleGroupUngroup: " + localOp.toString() + ", " + extOp.toString());
        }
    }


    /**
     * Handling a group operation and an insertChar operation.
     *
     * Info: insertChar operation includes the 'insertDrawing'. All other insertChar
     *       operations are inside a drawing.
     * Info: Table drawings cannot be grouped.
     * Info: The new position of the grouped drawing is the position of the first
     *       drawing the will be grouped.
     *
     * All positions of the group operation are top level positions on the slide.
     *
     * The group operation influences the insertChar operation and the insertChar influences
     * the group operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleGroupInsertChar(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the ungroup operation
        final JSONObject insertOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // external start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // the slide position of the external operation
        final int insertSlidePos = insertStartPos.getInt(0);
        if (groupSlidePos != insertSlidePos) {
            return null;
        }
        // the drawing position of the insert operation
        final int insertPos = insertStartPos.getInt(1);
        // the drawing 'start' position of the group operation
        final int groupPos = groupStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray groupDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
        if(insertStartPos.length()==groupStartPos.length()) {
            // handling an insertDrawing operation
            if (insertPos <= groupPos) {
                groupStartPos.put(1, groupPos + 1);
                for(int i = 0; i < groupDrawings.length(); i++) {
                    groupDrawings.put(i, groupDrawings.getInt(i) + 1);
                }
            } else {
                int counter = 0;
                // the position needs to be decreased -> counting grouped drawings before insert-drawing
                for(int i = 0; i < groupDrawings.length(); i++) {
                    final int value = groupDrawings.getInt(i);
                    if(value < insertPos) {
                        counter++;
                    }
                    else {
                        groupDrawings.put(i, groupDrawings.getInt(i) + 1);
                    }
                }
                if(--counter > 0) {  // decreasing by 1 again, because of the position of the grouped drawing itself
                    insertStartPos.put(1, insertPos - counter);
                }
            }
        }
        else {
            if (insertPos < groupPos) {
                return null;
            } // fast exit, insert is not influenced by grouping
            final int insertPosIndex = OTUtils.indexOf(groupDrawings, insertPos);
            // is the inserted char inside a grouped drawing?
            if (insertPosIndex > -1) {
                // calculating the new position inside the group
                insertStartPos.put(1, groupPos);
                insertStartPos.add(2, insertPosIndex);
            } else {
                int counter = -1;
                for(int i = 0; i < groupDrawings.length(); i++) {
                    if(groupDrawings.getInt(i) < insertPos) {
                        counter++;
                    }
                }
                if(counter > 0) {
                    insertStartPos.put(1, insertPos - counter);
                }
            }
        }
        return null;
    }

    /**
     * Handling an ungroup operation and an insertChar operation.
     *
     * Info: insertChar operation includes the 'insertDrawing'. All other insertChar
     *       operations are inside a drawing.
     * Info: Table drawings cannot be ungrouped.
     * Info: The new position of the ungrouped drawing is the position of the first
     *       drawing the will be ungrouped.
     *
     * All positions of the ungroup operation are top level positions on the slide.
     *
     * The ungroup operation influences the insertChar operation and the insertChar influences
     * the ungroup operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleUngroupInsertChar(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the ungroup operation
        final JSONObject insertOp = ungroupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // external start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the group operation
        final int ungroupSlidePos = ungroupStartPos.getInt(0);
        // the slide position of the external operation
        final int insertSlidePos = insertStartPos.getInt(0);
        if (ungroupSlidePos != insertSlidePos) {
            return null;
        }
        // the drawing position of the insert operation
        final int insertPos = insertStartPos.getInt(1);
        // the drawing 'start' position of the group operation
        final int ungroupPos = ungroupStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray ungroupDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
        if(insertStartPos.length()==ungroupStartPos.length()) {
            // handling an insertDrawing operation
            if (insertPos <= ungroupPos) {
                ungroupStartPos.put(1, ungroupPos + 1);
                for(int i = 0; i < ungroupDrawings.length(); i++) {
                    ungroupDrawings.put(i, ungroupDrawings.getInt(i) + 1);
                }
            } else {
                // the number of drawings that need to be filled into gaps between ungrouped drawings
                int fillDrawings = insertPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                int previousUngroup = -1;
                boolean foundGap = false;

                // check for a gap for the new inserted drawing in the ungrouped drawings -> modifying inserted drawing and following ungrouped drawings
                for(int i=0; i < ungroupDrawings.length(); i++) {
                    final int value = ungroupDrawings.getInt(i);
                    if (previousUngroup >= 0 && !foundGap) {
                        int gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertStartPos.put(1, previousUngroup + fillDrawings);
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                    if (foundGap) {
                        ungroupDrawings.put(i, value + 1); // increasing the index for all following ungrouped drawings
                    }
                }
                if (!foundGap) {
                    insertStartPos.put(1, previousUngroup + fillDrawings); // shifting drawing with inserted char behind the last ungrouped drawing
                }
            }
        }
        else {
            if (insertPos < ungroupPos) {
                return null;
            } // fast exit, insert is not influenced by grouping

            // is the inserted char inside a grouped drawing?
            if (insertPos == ungroupPos) {
                int drawingPosInGroup = insertStartPos.getInt(2); // the new top level position is specified by the (old) position of the insertChar operation
                int newDrawingTopLevelPos = ungroupDrawings.getInt(drawingPosInGroup);
                insertStartPos.put(1, newDrawingTopLevelPos);
                insertStartPos.remove(2);
            } else if (ungroupPos < insertPos) {
                // not ungrouped, but the position for text insertion needs to be increased -> counting gaps in ungroup
                int fillDrawings = insertPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                int previousUngroup = -1;
                boolean foundGap = false;
                for(int i=0; i < ungroupDrawings.length(); i++) {
                    final int value = ungroupDrawings.getInt(i);
                    if (previousUngroup >= 0 && !foundGap) {
                        int gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertStartPos.put(1, previousUngroup + fillDrawings);
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                }
                if (!foundGap) {
                    insertStartPos.put(1, previousUngroup + fillDrawings); // shifting drawing with inserted char behind the last ungrouped drawing
                }
                // gaps exist only in undo scenario
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 5, 6, 7] (shifting [1, 4] to [1, 9]) -> not sufficient gaps
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 4, 5, 6] (shifting [1, 4] to [1, 9]) -> no gaps
                // Test: insertText [1, 4, 0, 0] and ungroup [1, 1] with drawings [1, 2, 3, 7, 8 ,9] (shifting [1, 4] to [1, 6]) -> sufficient gaps
            }
        }
        return null;
    }

    /**
     * Handling a group operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the group operation is affected.
     * In the case of an insertParagraph operation, the group operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleGroupInsertComp(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // external start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // the slide position of the external operation
        final int insertSlidePos = insertStartPos.getInt(0);
        if(insertStartPos.length()==1) {
            if(insertSlidePos <= groupSlidePos) {
                groupStartPos.put(0, groupSlidePos + 1);
            }
        }
        else {
            if(insertSlidePos != groupSlidePos) {
                return null;
            }
            // the drawing position of the insert operation
            final int insertPos = insertStartPos.getInt(1);
            // the drawing 'start' position of the group operation
            final int groupPos = groupStartPos.getInt(1);
            if(insertPos < groupPos) {
                return null;
            }
            // the local drawings that will be grouped
            final JSONArray groupDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
            int drawingIndex = OTUtils.indexOf(groupDrawings, insertPos);
            if(drawingIndex > -1) { // is the insertParagraph for a drawing or inside a drawing that will be grouped?
                insertStartPos.put(1, groupPos);
                insertStartPos.add(2, drawingIndex);
            }
            else {
                // not grouped, but the position needs to be decreased -> counting grouped drawings before insertParagraph-drawing
                int counter = -1;
                for(int i=0; i<groupDrawings.length(); i++) {
                    if(groupDrawings.getInt(i) < insertPos) {
                        counter++;
                    }
                }
                if(counter > 0) {
                    insertStartPos.put(1, insertPos - counter);
                }
            }
        }
        return null;
    }

    /**
     * Handling an ungroup operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the ungroup operation is affected.
     * In the case of an insertParagraph operation, the ungroup operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleUngroupInsertComp(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the ungroup operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = ungroupOp == extOp ? localOp : extOp;
        // ungroup start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // external start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the ungroup operation
        final int ungroupSlidePos = ungroupStartPos.getInt(0);
        // the slide position of the external operation
        final int insertSlidePos = insertStartPos.getInt(0);
        if(insertStartPos.length()==1) {
            if(insertSlidePos <= ungroupSlidePos) {
                ungroupStartPos.put(0, ungroupSlidePos + 1);
            }
        }
        else {
            if(insertSlidePos != ungroupSlidePos) {
                return null;
            }
            // the drawing position of the insert operation
            final int insertPos = insertStartPos.getInt(1);
            // the drawing 'start' position of the ungroup operation
            final int ungroupPos = ungroupStartPos.getInt(1);
            if(insertPos < ungroupPos) {
                return null;
            }
            // the local drawings that will be ungrouped
            final JSONArray ungroupDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
            if(insertPos==ungroupPos) {
                final int drawingPosInGroup = insertStartPos.getInt(2); // the new top level position is specified by the (old) position of the insertChar operation
                final int newDrawingTopLevelPos = ungroupDrawings.getInt(drawingPosInGroup);
                insertStartPos.put(1, newDrawingTopLevelPos);
                insertStartPos.remove(2); // index at [2] is no longer required
            }
            else {
                // not ungrouped, but the position for paragraph insertion needs to be increased -> counting gaps in ungroup
                int fillDrawings = insertPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                int previousUngroup = -1;
                boolean foundGap = false;
                for(int i=0; i < ungroupDrawings.length(); i++) {
                    final int value = ungroupDrawings.getInt(i);
                    if (previousUngroup >= 0 && !foundGap) {
                        int gap = value - previousUngroup - 1;
                        if (gap > 0) {
                            if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                insertStartPos.put(1, previousUngroup + fillDrawings);
                                foundGap = true;
                            } else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                }
                if (!foundGap) {
                    insertStartPos.put(1, previousUngroup + fillDrawings); // shifting drawing with inserted paragraph behind the last ungrouped drawing
                }
            }
        }
        return null;
    }

    /**
     * Handling a group operation and any operation that can only happen inside a drawing
     * (insertRows, deleteRow, ...).
     * All positions of the group operation are top level positions on the slide.
     * The group operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleGroupGeneric(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the second operation
        final JSONObject secondOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // external start pos
        final JSONArray secondStartPos = OTUtils.getStartPosition(secondOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // the slide position of the second operation
        final int secondSlidePos = secondStartPos.getInt(0);
        if(groupSlidePos!=secondSlidePos) {
            return null;
        }
        final int groupPos = groupStartPos.getInt(1);
        final int secondPos = secondStartPos.getInt(1);
        if(secondPos >= groupPos) {
            final JSONArray groupDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
            final int drawingIndex = OTUtils.indexOf(groupDrawings, secondPos);
            if(drawingIndex > -1) {
                secondStartPos.put(1, groupPos);
                secondStartPos.add(2, drawingIndex);
            }
            else {  // not grouped, but the position needs to be decreased -> counting grouped drawings before insert-drawing
                int counter = -1;
                for(int i=0; i < groupDrawings.length(); i++) {
                    final int value = groupDrawings.getInt(i);
                    if(value <secondPos) {
                        counter++;
                    }
                }
                if(counter > 0) {
                    secondStartPos.put(1, secondPos - counter);
                }
            }
        }
        return null;
    }

    /**
     * Handling a ungroup operation and any operation that can only happen inside a drawing
     * (insertRows, deleteRow, ...).
     * All positions of the ungroup operation are top level positions on the slide.
     * The ungroup operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */

    private static JSONObject handleUngroupGeneric(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the ungroup operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the second operation
        final JSONObject secondOp = ungroupOp == extOp ? localOp : extOp;
        // ungroup start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // external start pos
        final JSONArray secondStartPos = OTUtils.getStartPosition(secondOp);
        // the slide position of the ungroup operation
        final int ungroupSlidePos = ungroupStartPos.getInt(0);
        // the slide position of the external operation
        final int secondSlidePos = secondStartPos.getInt(0);
        if(secondSlidePos!=ungroupSlidePos) {
            return null;
        }
        // the drawing position of the second operation
        final int secondPos = secondStartPos.getInt(1);
        // the drawing 'start' position of the ungroup operation
        final int ungroupPos = ungroupStartPos.getInt(1);
        if(secondPos >= ungroupPos) {
            final JSONArray ungroupDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
            if(secondPos == ungroupPos) {
                final int newDrawingTopLevelPos = ungroupDrawings.getInt(secondStartPos.getInt(2));
                secondStartPos.put(1, newDrawingTopLevelPos);
                secondStartPos.remove(2);
            }
            else { // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing

                // the position of the previous ungrouped drawing
                int previousUngroup = -1;
                boolean foundGap = false;
                int fillDrawings = secondPos - ungroupPos; // the gaps will be filled with the preceeding drawings
                for(int i=0; i < ungroupDrawings.length(); i++) {
                    final int value = ungroupDrawings.getInt(i);
                    if(previousUngroup >= 0 && !foundGap) {
                        int gap = value - previousUngroup - 1;
                        if(gap > 0) {
                            if(gap >= fillDrawings) {
                                secondStartPos.put(1, previousUngroup + fillDrawings);
                                foundGap = true;
                            }
                            else {
                                fillDrawings -= gap;
                            }
                        }
                    }
                    previousUngroup = value;
                }
                if(!foundGap) {
                    secondStartPos.put(1, previousUngroup + fillDrawings); // shifting drawing with second operation behind the last ungrouped drawing
                }
            }
        }
        return null;
    }

    private static JSONArray undoMoveOperation(JSONObject moveOp) throws JSONException {
        final JSONObject newMoveOperation = OTUtils.cloneJSONObject(moveOp);
        newMoveOperation.put(OCKey.START.value(), OTUtils.cloneJSONArray(OTUtils.getToPosition(moveOp)));
        newMoveOperation.put(OCKey.TO.value(), OTUtils.cloneJSONArray(OTUtils.getStartPosition(moveOp)));
        if(newMoveOperation.has(OCKey.END.value())) {
            newMoveOperation.put(OCKey.END.value(), OTUtils.cloneJSONArray(newMoveOperation.getJSONArray(OCKey.START.value())));
        }
        OTUtils.setOperationRemoved(moveOp);
        final JSONArray opsBefore = new JSONArray(1);
        opsBefore.put(newMoveOperation);
        return opsBefore;
    }

    /**
     * Handling a group operation and a move operation.
     *
     * If a drawing is moved, the group operation can be affected.
     *
     * Important: If a drawing is moved, there is always one move operation for each drawing.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: move, start: [1, 1], to: [1, 4] }  // -> bringing drawing to front
     */
    private static JSONObject handleGroupMove(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the second operation
        final JSONObject moveOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // move to pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // the slide position of the group operation
        final int groupSlidePos = groupStartPos.getInt(0);
        // the slide position of the move operation
        final int moveSlidePos = moveStartPos.getInt(0);
        // the drawing start position of the move operation
        final int movePos = moveStartPos.getInt(1);
        // the drawing 'start' position of the ungroup operation
        final int moveDrawingToPos = moveToPos.getInt(1);
        // the drawing 'start' position of the group operation
        final int groupPos = groupStartPos.getInt(1);
        // the local drawings that will be grouped
        final JSONArray groupedDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
        // the index of the last drawing that will be grouped
        final int lastGroupedDrawing = groupedDrawings.getInt(groupedDrawings.length() - 1);
        if(moveSlidePos != groupSlidePos)  {
            return null;
        }
        if(movePos < groupPos) {
            if(moveDrawingToPos < groupPos) {
                return null;
            }
            else if(moveDrawingToPos > lastGroupedDrawing) {
                moveToPos.put(1, moveDrawingToPos - (groupedDrawings.length() - 1)); // moving drawing behind the last grouped drawing
                groupStartPos.put(1, groupPos - 1); // adapting also the start position of the group operation
                for(int i=0; i < groupedDrawings.length(); i++) {
                    groupedDrawings.put(i, groupedDrawings.getInt(i) -1 );
                }
            }
            else {
                int prevDrawingsCount = 0;
                for(int i=0; i<groupedDrawings.length(); i++) {
                    if(groupedDrawings.getInt(i) < moveDrawingToPos) {
                        prevDrawingsCount++;
                    }
                }
                if(moveDrawingToPos == groupPos) {
                    prevDrawingsCount = 1;
                }
                moveToPos.put(1, moveDrawingToPos - (prevDrawingsCount - 1));
                groupStartPos.put(1, groupPos -1);
                for(int i=0; i < groupedDrawings.length(); i++) {
                    final int value = groupedDrawings.getInt(i);
                    if(value <= moveDrawingToPos) {
                        groupedDrawings.put(i, value - 1);
                    }
                }
            }
        }
        else if(movePos > lastGroupedDrawing) {
            if(moveDrawingToPos <= groupPos) {
                moveStartPos.put(1, movePos - (groupedDrawings.length() - 1));
                groupStartPos.put(1, groupPos + 1);
                for(int i=0; i < groupedDrawings.length(); i++) {
                    groupedDrawings.put(i, groupedDrawings.getInt(i) + 1);
                }
            }
            else if(moveDrawingToPos > lastGroupedDrawing) {
                moveStartPos.put(1, movePos - (groupedDrawings.length() - 1));
                moveToPos.put(1, moveDrawingToPos - (groupedDrawings.length() - 1)); // moving drawing behind the last grouped drawing
            }
            else {

                int prevDrawingsCount = 0;
                for(int i=0; i<groupedDrawings.length(); i++) {
                    if(groupedDrawings.getInt(i) < moveDrawingToPos) {
                        prevDrawingsCount++;
                    }
                }
                moveStartPos.put(1, movePos - (groupedDrawings.length() -1));
                moveToPos.put(1, moveDrawingToPos - (prevDrawingsCount -1));
                for(int i=0; i < groupedDrawings.length(); i++) {
                    final int value = groupedDrawings.getInt(i);
                    if(value >= moveDrawingToPos) {
                        groupedDrawings.put(i, value + 1);
                    }
                }
            }
        }
        else {
            JSONArray externalOpsBefore = null;
            JSONArray localOpsBefore = null;

            // the drawing start position is in the range of the grouped drawings (but it is not sure, whether it is grouped)
            int drawingPosIndex = OTUtils.indexOf(groupedDrawings, movePos);
            if(drawingPosIndex > -1) {
                if (groupOp == extOp) {
                    externalOpsBefore = undoMoveOperation(moveOp);
                } else {
                    localOpsBefore = undoMoveOperation(moveOp);
                }
            }
            else {
                int prevDrawingsCount = 0;
                for(int i=0; i<groupedDrawings.length(); i++) {
                    if(groupedDrawings.getInt(i) < movePos) {
                        prevDrawingsCount++;
                    }
                }
                // the moved drawing is in the range of the grouped drawings, but it is not grouped
                if (moveDrawingToPos < groupPos) {
                    moveStartPos.put(1, movePos - (prevDrawingsCount - 1));
                    groupStartPos.put(1, groupPos + 1); // adapting also the start position of the group operation
                    for(int i=0; i < groupedDrawings.length(); i++) {
                        final int value = groupedDrawings.getInt(i);
                        if(value < movePos) {
                            groupedDrawings.put(i, value + 1);
                        }
                    }
                } else if (moveDrawingToPos > lastGroupedDrawing) {
                    moveStartPos.put(1, movePos - (prevDrawingsCount - 1));
                    moveToPos.put(1, moveDrawingToPos - (groupedDrawings.length() - 1));
                    for(int i=0; i < groupedDrawings.length(); i++) {
                        final int value = groupedDrawings.getInt(i);
                        if(value > movePos) {
                            groupedDrawings.put(i, value - 1);
                        }

                    }
                } else {
                    if (groupOp == extOp) {
                        externalOpsBefore = undoMoveOperation(moveOp);
                    } else {
                        localOpsBefore = undoMoveOperation(moveOp);
                    }
                }
            }
            return JSONHelper.getResultObject(externalOpsBefore, null, localOpsBefore, null);
        }
        return null;
    }

    /**
     * Handling an ungroup operation and a move operation.
     *
     * If a drawing is moved, the ungroup operation can be affected.
     * Also the ungrouping can influence the positions of the move operation.
     *
     * Important: If a drawing is moved, there is always one move operation for each drawing.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     * { name: move, start: [2, 1], to: [2, 4] }  // -> bringing drawing to front
     */
    private static JSONObject handleUngroupMove(JSONObject localOp, JSONObject extOp) throws JSONException {

        // the ungroup operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the second operation
        final JSONObject moveOp = ungroupOp == extOp ? localOp : extOp;
        // ungroup start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // the slide position of the ungroup operation
        final int ungroupStart0 = ungroupStartPos.getInt(0);
        // the slide position of the external operation
        final int moveStart0 = moveStartPos.getInt(0);

        JSONArray externalOpsBefore = null;
        JSONArray localOpsBefore = null;

        if(moveStart0 != ungroupStart0) {
            return null; // fast exit, different slides
        }

        // move to pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // the drawing 'to' position of the move operation
        final int moveTo1 =  moveToPos.getInt(1);


        final JSONArray ungroupDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
        final boolean isGapInArray = OTUtils.hasGap(ungroupDrawings);
        final int ungroupStart1 = ungroupStartPos.getInt(1);
        final int moveStart1 = moveStartPos.getInt(1);
        // the position of the previous ungrouped drawing
        int previousUngroup = -1;
        boolean foundGap = false;
        if(moveStart1 < ungroupStart1) {
            if(moveTo1 >= ungroupStart1) {
                if(isGapInArray) {
                    int fillDrawings = moveTo1 - ungroupStart1; // the gaps will be filled with the preceeding drawings
                    for(int i=0; i < ungroupDrawings.length(); i++) {
                        final int value = ungroupDrawings.getInt(i);
                        if (previousUngroup >= 0 && !foundGap) {
                            final int gap = value - previousUngroup - 1;
                            if (gap > 0) {
                                if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                    moveToPos.put(1, previousUngroup + fillDrawings);
                                    foundGap = true;
                                } else {
                                    fillDrawings -= gap;
                                }
                            }
                        }
                        previousUngroup = value;
                        if (!foundGap) {
                            ungroupDrawings.put(i, value - 1); // decreasing the index for all ungrouped drawings, as long as no gap for the moved drawing is found
                        }
                    }
                    if (!foundGap) {
                        moveToPos.put(1, previousUngroup + fillDrawings); // shifting drawing behind the last ungrouped drawing
                    }
                    ungroupStartPos.put(1, ungroupStart1 - 1); // adapting also the start position of the ungroup operation
                }
                else {
                    moveToPos.put(1, moveTo1 + (ungroupDrawings.length() - 1)); // shifting drawing behind the last ungrouped drawing
                    ungroupStartPos.put(1, ungroupStart1 - 1); // adapting also the start position of the ungroup operation
                    for(int i=0; i < ungroupDrawings.length(); i++) {
                        ungroupDrawings.put(i, ungroupDrawings.getInt(i) - 1);
                    }
                }
            }
        }
        else if(moveStart1 > ungroupStart1) {
            if(moveTo1 <= ungroupStart1) {
                if (isGapInArray) {
                    int fillDrawings = moveStart1 - ungroupStart1; // the gaps will be filled with the preceeding drawings
                    for(int i=0; i < ungroupDrawings.length(); i++) {
                        final int value = ungroupDrawings.getInt(i);
                        if (previousUngroup >= 0 && !foundGap) {
                            int gap = value - previousUngroup - 1;
                            if (gap > 0) {
                                if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                                    moveStartPos.put(1, previousUngroup + fillDrawings);
                                    foundGap = true;
                                } else {
                                    fillDrawings -= gap;
                                }
                            }
                        }
                        previousUngroup = value;
                        if (!foundGap) {
                            ungroupDrawings.put(i, value + 1); // increasing the index for all ungrouped drawings, as long as the new position is not found
                        }
                    }
                    if (!foundGap) {
                        moveStartPos.put(1, previousUngroup + fillDrawings); // shifting drawing start position behind the last ungrouped drawing
                    }
                    ungroupStartPos.put(1, ungroupStart1 + 1); // adapting also the start position of the ungroup operation
                } else {
                    moveStartPos.put(1, moveStart1 + (ungroupDrawings.length() - 1)); // shifting drawing behind the last ungrouped drawing
                    ungroupStartPos.put(1, ungroupStart1 + 1); // adapting also the start position of the ungroup operation
                    for(int i = 0 ; i < ungroupDrawings.length(); i++) {
                        ungroupDrawings.put(i, ungroupDrawings.getInt(i) + 1);
                    }
                }
            }
            else {
                final int lastUngroupPos = ungroupDrawings.getInt(ungroupDrawings.length() - 1);
                final boolean simpleMove = moveStart1 > lastUngroupPos && moveTo1 > lastUngroupPos; // move happened completely behind ungroup
                if (isGapInArray && !simpleMove) {
                    // reverting the move operation (this might be improved in the future)
                    // ungroup influences move, but move also ungroup (move [3, 5] to [3, 7] and ungroup [3,4] with positions [4, 5, 7])
                    if (ungroupOp == extOp) {
                        externalOpsBefore = undoMoveOperation(moveOp);
                    } else {
                        localOpsBefore = undoMoveOperation(moveOp);
                    }
                } else {
                    moveStartPos.put(1, moveStart1 + (ungroupDrawings.length() - 1));
                    moveToPos.put(1, moveTo1 + (ungroupDrawings.length() - 1));
                }
            }
        }
        else {
            // the same drawing was moved and ungrouped -> undo the move operation
            if (ungroupOp == extOp) {
                externalOpsBefore = undoMoveOperation(moveOp);
            } else {
                localOpsBefore = undoMoveOperation(moveOp);
            }
        }
        return JSONHelper.getResultObject(externalOpsBefore, null, localOpsBefore, null);
    }

    /**
     * Handling a group operation and a setAttributes operation.
     *
     * The group operation influnces the setAttributes position only, if the attribute position
     * is on the same slide.
     * The setAttributes position never influences the group operation.
     *
     * Example:
     * { name: group, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleGroupSetAttrs(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject groupOp = OTUtils.getOperation(OCValue.GROUP, localOp, extOp);
        // the second operation
        final JSONObject attrsOp = groupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray groupStartPos = OTUtils.getStartPosition(groupOp);
        // attrs start pos
        final JSONArray attrsStartPos = OTUtils.getStartPosition(attrsOp);
        // the slide position of the group operation
        final int groupStart0 = groupStartPos.getInt(0);
        // the slide position of the setAttributes operation
        final int attrsStart0 = attrsStartPos.getInt(0);

        if(attrsStart0 != groupStart0 || attrsStartPos.length() < groupStartPos.length()) {
            return null;
        }
        // the drawing 'start' position of the group operation
        final int groupStart1 = groupStartPos.getInt(1);
        // the drawing position of the setAttributes operation
        final int attrsStart1 = attrsStartPos.getInt(1);

        if(attrsStart1 < groupStart1) {
            return null;
        }
        // the array with indices of grouped drawings
        final JSONArray groupedDrawings = groupOp.getJSONArray(OCKey.DRAWINGS.value());
        final JSONArray attrsEndPos = OTUtils.optEndPosition(attrsOp);
        final int drawingIndex = OTUtils.indexOf(groupedDrawings, attrsStart1);

        // is the setAttributes for a drawing or inside a drawing that will be grouped?
        if (drawingIndex > -1) {

            final boolean isSetAttrsOp = attrsOp.getString(OCKey.NAME.value()).equals(OCValue.SET_ATTRIBUTES.value());

            if(isSetAttrsOp && attrsStartPos.length() == 2) {
                throw new OTException(Type.RELOAD_REQUIRED, "handleGroupSetAttrs: " + localOp.toString() + ", " + extOp.toString());
            }

            if(attrsStartPos.length() > 2 || (!isSetAttrsOp && attrsStartPos.length() == 2)) { // content inside a grouped drawing is attributed
                attrsStartPos.put(1, groupStart1);
                attrsStartPos.add(2, drawingIndex); // inserting the counter position behind the group position
                if(attrsEndPos!=null) { // end position must be for or inside the same drawing as the start position
                    attrsEndPos.put(1, groupStart1);
                    attrsEndPos.add(2, drawingIndex);
                }
            }
        }
        else {
            // not grouped, but the position needs to be decreased -> counting grouped drawings before setAttributes-drawing
            int counter = -1;
            for(int i=0; i < groupedDrawings.length(); i++) {
                if(groupedDrawings.getInt(i) < attrsStart1) {
                    counter++;
                }
            }
            if(counter > 0) {
                attrsStartPos.put(1, attrsStart1 - counter);
                if(attrsEndPos!=null) {
                    attrsEndPos.put(1, attrsEndPos.getInt(1) - counter);
                }
            }
        }
        return null;
    }

    /**
     * Handling an ungroup operation and a setAttributes operation.
     *
     * The ungroup operation influnces the setAttributes position only, if the attribute position
     * is on the same slide.
     * The setAttributes position never influences the ungroup operation.
     *
     * Example:
     * { name: ungroup, start: [2, 3], drawings:[3, 4, 7, 8] }
     */
    private static JSONObject handleUngroupSetAttrs(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the group operation
        final JSONObject ungroupOp = OTUtils.getOperation(OCValue.UNGROUP, localOp, extOp);
        // the second operation
        final JSONObject attrsOp = ungroupOp == extOp ? localOp : extOp;
        // group start pos
        final JSONArray ungroupStartPos = OTUtils.getStartPosition(ungroupOp);
        // attrs start pos
        final JSONArray attrsStartPos = OTUtils.getStartPosition(attrsOp);
        // the slide position of the group operation
        final int ungroupStart0 = ungroupStartPos.getInt(0);
        // the slide position of the setAttributes operation
        final int attrsStart0 = attrsStartPos.getInt(0);
        if(attrsStart0 != ungroupStart0 || attrsStartPos.length() < ungroupStartPos.length()) {
            return null;
        }
        // the drawing 'start' position of the group operation
        final int ungroupStart1 = ungroupStartPos.getInt(1);
        // the drawing position of the setAttributes operation
        final int attrsStart1 = attrsStartPos.getInt(1);
        if(attrsStart1 < ungroupStart1) {
            return null;
        }
        // the array with indices of ungrouped drawings
        final JSONArray ungroupedDrawings = ungroupOp.getJSONArray(OCKey.DRAWINGS.value());
        final JSONArray attrsEndPos = OTUtils.optEndPosition(attrsOp);
        // is the setAttributes for a drawing or inside a drawing that will be ungrouped?
        if(attrsStart1 == ungroupStart1) {
            final boolean isSetAttrsOp = attrsOp.getString(OCKey.NAME.value()).equals(OCValue.SET_ATTRIBUTES.value());

            if(isSetAttrsOp && attrsStartPos.length() == 2) {
                throw new OTException(Type.RELOAD_REQUIRED, "handleUngroupSetAttrs: " + localOp.toString() + ", " + extOp.toString());
            }

            if(attrsStartPos.length() > 2 || (!isSetAttrsOp && attrsStartPos.length() == 2)) {
                final int drawingPosInGroup = attrsStartPos.getInt(2); // the new top level position is specified by the (old) position of the insertChar operation
                final int newDrawingTopLevelPos = ungroupedDrawings.getInt(drawingPosInGroup);
                attrsStartPos.put(1, newDrawingTopLevelPos);
                attrsStartPos.remove(2);
                if(attrsEndPos!=null) {
                    attrsEndPos.put(1, newDrawingTopLevelPos);
                    attrsEndPos.remove(2);
                }
            }
        }
        else {
            // not ungrouped, but the position needs to be increased -> counting ungrouped drawings before insert-drawing
            int fillDrawings = attrsStart1 - ungroupStart1; // the gaps will be filled with the preceeding drawings
            int previousUngroup = -1;
            boolean foundGap = false;

            for(int i=0; i < ungroupedDrawings.length(); i++) {
                final int value = ungroupedDrawings.getInt(i);
                if (previousUngroup >= 0 && !foundGap) {
                    final int gap = value - previousUngroup - 1;
                    if (gap > 0) {
                        if (gap >= fillDrawings) { // the gap has a sufficient size for all drawings
                            int newDrawingPos = previousUngroup + fillDrawings;
                            attrsStartPos.put(1, newDrawingPos);
                            if(attrsEndPos!=null) {
                                attrsEndPos.put(1, newDrawingPos);
                            }
                            foundGap = true;
                        } else {
                            fillDrawings -= gap;
                        }
                    }
                }
                previousUngroup = value;
            }
            if (!foundGap) {
                int newDrawingPos = previousUngroup + fillDrawings;
                attrsStartPos.put(1, newDrawingPos); // shifting drawing with setAttributes operation behind the last ungrouped drawing
                if (attrsEndPos!=null) {
                    attrsEndPos.put(1, newDrawingPos);
                }
            }
        }
        return null;
    }

    /**
     * Handling an insertSlide operation and a second operation, that does not influence the
     * insertSlide operation.
     *
     * Info: This handler is also called for insertParagraph operation, but it can be ignored in this case.
     */
    private static JSONObject handleInsertSlideGeneric(JSONObject localOp, JSONObject extOp) throws JSONException {
        final String localName = OTUtils.getName(localOp);
        // the insertSlide / insertParagraph operation
        final JSONObject insertOp = localName.equals(OCValue.INSERT_SLIDE.value()) || localName.equals(OCValue.INSERT_PARAGRAPH.value()) || localName.equals(OCValue.INSERT_TABLE.value()) ? localOp : extOp;
        // the second operation
        final JSONObject secondOp = insertOp == extOp ? localOp : extOp;
        // insert start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        if(insertStartPos.length() > 1) {
            return null;    // handle only insertSlide operations
        }
        // second start pos
        final JSONArray secondStartPos = OTUtils.getStartPosition(secondOp);
        if(insertStartPos.getInt(0) <= secondStartPos.getInt(0)) {
            secondStartPos.put(0, secondStartPos.getInt(0) + 1);
        }
        return null;
    }

    /**
     * Handling of move operation with a delete operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], end: [2, 3] }
     */
    private static JSONObject handleMoveDelete(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE, localOp, extOp);
        // the delete operation
        final JSONObject deleteOp = moveOp == extOp ? localOp : extOp;
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // the optional end position of the move operation
        final JSONArray moveEndPos = OTUtils.optEndPosition(moveOp);
        // the optional end position of the move operation
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // delete start pos
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the optional end position of the delete operation
        final JSONArray deleteEndPos = OTUtils.optEndPosition(deleteOp);
        // the slide position of the move operation
        final int moveStart0 = moveStartPos.getInt(0);
        // the slide position of the delete operation
        final int deleteStart0 = deleteStartPos.getInt(0);
        if (deleteStartPos.length() == 1) {
            // a slide was deleted
            if (deleteStart0 == moveStart0) { // exactly the slide of the move operation was removed
                OTUtils.setOperationRemoved(moveOp); // setting marker at move operation!
            }
            else if (deleteStart0 < moveStart0) {
                moveStartPos.put(0, moveStart0 - 1);
                if (moveEndPos != null) {
                    moveEndPos.put(0, moveEndPos.getInt(0) - 1);
                }
                moveToPos.put(0, moveToPos.getInt(0) - 1);
            }
        }
        else if(deleteStartPos.length() == moveStartPos.length()) {
            if(deleteStart0 != moveStart0) {
                return null;
            }
            // a drawing on the same slide as the move operation was removed

            final int deleteStart1 = deleteStartPos.getInt(1);
            final int moveStart1 = moveStartPos.getInt(1);
            final int moveTo1 = moveToPos.getInt(1);

            if (deleteStart1 == moveStart1) {
                // the moved drawing was deleted
                OTUtils.setOperationRemoved(moveOp);; // moveOp cannot be executed anymore because the drawing is already deleted
                deleteOp.put(OCKey.START.value(), OTUtils.cloneJSONArray(moveToPos)); // but the drawing was deleted at a modified position
                deleteOp.put(OCKey.END.value(), OTUtils.cloneJSONArray(moveToPos));
            }
            else {
                // move of drawing and delete of drawing influence each other
                if (deleteStart1 < moveStart1) {
                    moveStartPos.put(1, moveStart1 - 1);
                    if (moveEndPos!=null) {
                        moveEndPos.put(1, moveEndPos.getInt(1) - 1);
                    }
                }
                if (deleteStart1 < moveTo1) {
                    moveToPos.put(1, moveTo1 - 1);
                }
                if (moveStart1 > deleteStart1 && moveTo1 <= deleteStart1) {
                    deleteStartPos.put(1, deleteStart1 + 1);
                    if (deleteEndPos != null) {
                        deleteEndPos.put(1, deleteEndPos.getInt(1) + 1);
                    }
                }
                else if (moveStart1 < deleteStart1 && moveTo1 > deleteStart1) { // TODO: Check, if >= is better (see moveSlideDelete handler)
                    deleteStartPos.put(1, deleteStart1 - 1);
                    if (deleteEndPos != null) {
                        deleteEndPos.put(1, deleteEndPos.getInt(1) - 1);
                    }
                }
            }
        }
        else {
            // the position of the delete operation is longer than the position of the move operation
            // -> the move operation influences the delete operation, not vice versa.

            if (deleteStart0 != moveStart0) {
                return null;
            }

            final int deleteStart1 = deleteStartPos.getInt(1);
            final int moveStart1 = moveStartPos.getInt(1);
            final int moveTo1 = moveToPos.getInt(1);

            if (moveStart1 == deleteStart1) {
                deleteStartPos.put(1, moveTo1); // the drawing was moved
                if (deleteEndPos!=null) {
                    deleteEndPos.put(1, moveTo1);
                }
            }
            else if (moveStart1 > deleteStart1 && moveTo1 <= deleteStart1) {
                deleteStartPos.put(1, deleteStart1 + 1);
                if (deleteEndPos != null) {
                    deleteEndPos.put(1, deleteEndPos.getInt(1) + 1);
                }
            }
            else if (moveStart1 < deleteStart1 && moveTo1 > deleteStart1) {
                deleteStartPos.put(1, deleteStart1 - 1);
                if (deleteEndPos != null) {
                    deleteEndPos.put(1, deleteEndPos.getInt(1) - 1);
                }
            }
        }
        return null;
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and any operation that can only happen inside a drawing (insertRows, deleteRow, ...).
     * A move 'start' position and a move 'to' position must be on the same slide.
     * Additionally all positions of the move operation are top level positions on the slide.
     * In a multi selection, one move operation is generated for every drawing.
     * The move operation influences the position of the second operation, but not vice versa.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    private static JSONObject handleMoveGeneric(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE, localOp, extOp);
        // the second operation
        final JSONObject secondOp = moveOp == extOp ? localOp : extOp;
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // second start pos
        final JSONArray secondStartPos = OTUtils.getStartPosition(secondOp);
        // the slide position of the move operation
        final int moveStart0 = moveStartPos.getInt(0);
        // the slide position of the second operation
        final int secondStart0 = secondStartPos.getInt(0);
        if(secondStart0 != moveStart0) {
            return null;
        }
        // the drawing 'start' position of the move operation
        final int moveStart1 = moveStartPos.getInt(1);
        // the drawing position of the second operation
        final int secondStart1 = secondStartPos.getInt(1);
        // move to start pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // the drawing 'to' position of the move operation
        final int moveTo1 = moveToPos.getInt(1);
        if(moveStart1 == secondStart1) {
            secondStartPos.put(1, moveTo1);
        }
        else if(moveStart1 > secondStart1 && moveTo1 <= secondStart1) {
            secondStartPos.put(1, secondStart1 + 1);
        }
        else if(moveStart1 < secondStart1 && moveTo1 > secondStart1) {
            secondStartPos.put(1, secondStart1 - 1);
        }
        return null;
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and an insertChar operation.
     * A move 'start' position and a move 'to' position must be on the same slide.
     * Additionally all positions of the move operation are top level positions on the slide.
     * In a multi selection, one move operation is generated for every drawing.
     * The move operation influences the insertChar operation and the insertChar influences
     * the move operation, if it is a drawing insertion on the slide.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    private static JSONObject handleMoveInsertChar(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = moveOp == extOp ? localOp : extOp;
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // second start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the move operation
        final int moveStart0 = moveStartPos.getInt(0);
        // the slide position of the second operation
        final int insertStart0 = insertStartPos.getInt(0);
        if(moveStart0 != insertStart0) {
            return null;
        }
        // the drawing position of the move operation
        final int moveStart1 = moveStartPos.getInt(1);
        // the drawing position of the second operation
        final int insertStart1 = insertStartPos.getInt(1);
        // move to start pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // the drawing 'to' position of the move operation
        final int moveTo1 = moveToPos.getInt(1);
        if(insertStartPos.length() == moveStartPos.length()) {
            if(insertStart1 < moveStart1) {
                moveStartPos.put(1, moveStart1 + 1);
                // move end pos
                final JSONArray moveEndPos = OTUtils.optEndPosition(moveOp);
                if(moveEndPos!=null) {
                    moveEndPos.put(1, moveEndPos.getInt(1) + 1);
                }
            }
            if(insertStart1 < moveTo1) {
                moveToPos.put(1, moveTo1 + 1);
            }
            if(moveStart1 > insertStart1 && moveTo1 <= insertStart1) {
                insertStartPos.put(1, insertStart1 + 1);
            }
            else if (moveStart1 < insertStart1 && moveTo1 > insertStart1) {
                insertStartPos.put(1, insertStart1 - 1);
            }
        }
        else {
            if (moveStart1 == insertStart1) {
                insertStartPos.put(1, moveTo1); // the drawing with the inserted text was moved
            }
            else if (moveStart1 > insertStart1 && moveTo1 <= insertStart1) {
                insertStartPos.put(1, insertStart1 + 1);
            }
            else if (moveStart1 < insertStart1 && moveTo1 > insertStart1) {
                insertStartPos.put(1, insertStart1 - 1);
            }
        }
        return null;
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and an insertComp operation (insertParagraph and insertSlide (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, only the move operation is affected.
     * In the case of an insertParagraph operation, the move operation affects the insertPargraph
     * operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    private static JSONObject handleMoveInsertComp(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = moveOp == extOp ? localOp : extOp;
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // second start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide position of the move operation
        final int moveStart0 = moveStartPos.getInt(0);
        // the slide position of the second operation
        final int insertStart0 = insertStartPos.getInt(0);
        // move to start pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        if(insertStartPos.length() == 1) {
            if(insertStart0 <= moveStart0) {
                moveStartPos.put(0, moveStart0 + 1);
                // move end pos
                final JSONArray moveEndPos = OTUtils.optEndPosition(moveOp);
                if(moveEndPos!=null) {
                    moveEndPos.put(0, moveEndPos.getInt(0) + 1);
                }
                moveToPos.put(0, moveToPos.getInt(0) + 1);
            }
        }
        else {
            // the drawing position of the move operation
            final int moveStart1 = moveStartPos.getInt(1);
            // the drawing position of the second operation
            final int insertStart1 = insertStartPos.getInt(1);
            // the drawing 'to' position of the move operation
            final int moveTo1 = moveToPos.getInt(1);
            if(moveStart1 == insertStart1) {
                insertStartPos.put(1, moveTo1);
            }
            else if(moveStart1 > insertStart1 && moveTo1 <= insertStart1) {
                insertStartPos.put(1, insertStart1 + 1);
            }
            else if(moveStart1 < insertStart1 && moveTo1 > insertStart1) {
                insertStartPos.put(1, insertStart1 - 1);
            }
        }
        return null;
    }

    /**
     * Handling two move operations (bringing drawings to front or sending them backwards).
     *
     * The move operations influence each other, if they are on the same slide.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    private static JSONObject handleMoveMove(JSONObject localOp, JSONObject extOp) throws JSONException {

        // this is the client side code, that is executed on server side.
        // The simplest solution is to exchange the operations.
        JSONObject tempObject = localOp;
        localOp = extOp;
        extOp = tempObject;

        // local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // ext start pos
        final JSONArray extStartPos = OTUtils.getStartPosition(extOp);
        // the slide position of the local move operation
        final int localStart0 = localStartPos.getInt(0);
        // the slide position of the ext move operation
        final int extStart0 = extStartPos.getInt(0);

        if(localStart0 != extStart0) {
            return null;
        }
        // the slide position of the move operation
        final int localStart1 = localStartPos.getInt(1);
        // the slide position of the second operation
        final int extStart1 = extStartPos.getInt(1);
        // local to pos
        final JSONArray localToPos = OTUtils.getToPosition(localOp);
        // ext to pos
        final JSONArray extToPos = OTUtils.getToPosition(extOp);
        // the slide position of the move operation
        final int localTo1 = localToPos.getInt(1);
        // the slide position of the second operation
        final int extTo1 = extToPos.getInt(1);

        final OTMoveMoveResult result = OTUtils.transformIndexMoveMove(localStart1, localTo1, extStart1, extTo1);
        final OTMoveResult localResult = result.getLclRes();
        final OTMoveResult extResult = result.getExtRes();

        if (localResult!=null) {
            localStartPos.put(1, localResult.getFromIdx());
            final JSONArray localEndPos = OTUtils.optEndPosition(localOp);
            if (localEndPos!=null) {
                localEndPos.put(1, localResult.getFromIdx());
            }
            localToPos.put(1, localResult.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(localOp);
        }

        if (extResult!=null) {
            extStartPos.put(1, extResult.getFromIdx());
            final JSONArray extEndPos = OTUtils.optEndPosition(extOp);
            if (extEndPos!=null) {
                extEndPos.put(1, extResult.getFromIdx());
            }
            extToPos.put(1, extResult.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }

    /**
     * Handling a move operation (bringing drawings to front or sending them backwards)
     * and a setAttributes operation.
     *
     * The move operation influnces the setAttributes position only, if the attribute position
     * is on the same slide and is a selected drawing or inside a drawing.
     * The setAttributes position never influences the move operation.
     *
     * Example:
     * { name: move, start: [2, 1], end: [2, 1], to: [2, 2] }
     */
    private static JSONObject handleMoveSetAttrs(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE, localOp, extOp);
        // the insert operation
        final JSONObject attrsOp = moveOp == extOp ? localOp : extOp;
        // move start pos
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // second start pos
        final JSONArray attrsStartPos = OTUtils.getStartPosition(attrsOp);
        // the slide position of the move operation
        final int moveStart0 = moveStartPos.getInt(0);
        // the slide position of the attrs operation
        final int attrsStart0 = attrsStartPos.getInt(0);
        if(moveStart0 != attrsStart0 || attrsStartPos.length() < moveStartPos.length()) {
            return null;
        }
        // the drawing position of the move operation
        final int moveStart1 = moveStartPos.getInt(1);
        // the drawing position of the attrs operation
        final int attrsStart1 = attrsStartPos.getInt(1);
        // move to start pos
        final JSONArray moveToPos = OTUtils.getToPosition(moveOp);
        // the drawing position of the move to operation
        final int moveTo1 = moveToPos.getInt(1);
        if(moveStart1 == attrsStart1 ) {
            attrsStartPos.put(1, moveTo1);
            final JSONArray attrsEndPos = OTUtils.optEndPosition(attrsOp);
            if(attrsEndPos!=null) {
                attrsEndPos.put(1, moveTo1);
            }
        }
        else if(moveStart1 > attrsStart1 && moveTo1 <= attrsStart1) {
            attrsStartPos.put(1, attrsStart1 + 1);
            final JSONArray attrsEndPos = OTUtils.optEndPosition(attrsOp);
            if(attrsEndPos!=null) {
                attrsEndPos.put(1, attrsEndPos.getInt(1) + 1);
            }
        }
        else if(moveStart1 < attrsStart1 && moveTo1 >= attrsStart1) {
            attrsStartPos.put(1, attrsStart1 - 1);
            final JSONArray attrsEndPos = OTUtils.optEndPosition(attrsOp);
            if(attrsEndPos!=null) {
                attrsEndPos.put(1, attrsEndPos.getInt(1) - 1);
            }
        }
        return null;
    }

    /**
     * Handling a moveSlide operation and a delete operation. The delete operation also
     * handles deleting slides. In this case the delete operation influences the moveSlide
     * operation. In all other cases (the delete happens on the slide) the moveSlide
     * operation influences the delete operation.
     *
     * Important for the delete operation is, that there is no range. Every slide is removed
     * with its own delete operation.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    private static JSONObject handleMoveSlideDelete(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveSlideOp = OTUtils.getOperation(OCValue.MOVE_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject deleteOp = moveSlideOp == extOp ? localOp : extOp;
        // moveSlide start pos
        final JSONArray moveSlideStartPos = OTUtils.getStartPosition(moveSlideOp);
        // moveSlide start pos
        final JSONArray moveSlideEndPos = OTUtils.optEndPosition(moveSlideOp);
        // delete start pos
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // optional delete end pos
        final JSONArray deleteEndPos = OTUtils.optEndPosition(deleteOp);
        // the slide position of the move operation
        final int moveSlideStart0 = moveSlideStartPos.getInt(0);
        // the slide end position of the move operation
        final int moveSlideEnd0 = moveSlideEndPos.getInt(0);
        // the slide position of the delete operation
        final int deleteStart0 = deleteStartPos.getInt(0);
        // whether this is a deletion of a slide
        final boolean isSlideDeletion = deleteStartPos.length() == 1;

        if (isSlideDeletion) {
            // -> a slide was deleted
            if (deleteStart0 == moveSlideStart0) { // the moved slide was deleted
                // -> the move operation must be marked as 'removed'
                OTUtils.setOperationRemoved(moveSlideOp); // move operation can be ignored, because the moved slide is already removed
                // -> the delete operation gets as target the destination of the move operation
                deleteOp.put(OCKey.START.value(), OTUtils.cloneJSONArray(moveSlideEndPos));
            }
            else {
                if (deleteStart0 < moveSlideStart0) {
                    moveSlideStartPos.put(0, moveSlideStart0 - 1);
                }
                if (deleteStart0 < moveSlideEnd0) {
                    moveSlideEndPos.put(0, moveSlideEnd0 - 1);
                }
                // move also influences delete
                if (moveSlideStart0 > deleteStart0 && moveSlideEnd0 <= deleteStart0) {
                    deleteStartPos.put(0, deleteStart0 + 1);
                    if (deleteEndPos!=null) {
                        deleteEndPos.put(0, deleteEndPos.getInt(0) + 1);
                    }
                }
                else if (moveSlideStart0 < deleteStart0 && moveSlideEnd0 >= deleteStart0) {
                    deleteStartPos.put(0, deleteStart0 - 1);
                    if (deleteEndPos!=null) {
                        deleteEndPos.put(0, deleteEndPos.getInt(0) - 1);
                    }
                }
            }
        }
        else {
            // -> deleting content on a slide -> the move operation influences the delete operation
            if (moveSlideStart0 == deleteStart0) {
                deleteStartPos.put(0, moveSlideEnd0);
                if (deleteEndPos!=null) {
                    deleteEndPos.put(0, moveSlideEnd0);
                }
            }
            else if (moveSlideStart0 > deleteStart0 && moveSlideEnd0 <= deleteStart0) {
                deleteStartPos.put(0, deleteStart0 + 1);
                if (deleteEndPos!=null) {
                    deleteEndPos.put(0, deleteEndPos.getInt(0) + 1);
                }
            }
            else if (moveSlideStart0 < deleteStart0 && moveSlideEnd0 >= deleteStart0) {
                deleteStartPos.put(0, deleteStart0 - 1);
                if (deleteEndPos!=null) {
                    deleteEndPos.put(0, deleteEndPos.getInt(0) - 1);
                }
            }
        }
        return null;
    }

    /**
     * Handling two moveSlide operations.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    private static JSONObject handleMoveSlideMoveSlide(JSONObject localOp, JSONObject extOp) throws JSONException {

        // this is the client side code, that is executed on server side.
        // The simplest solution is to exchange the operations.
        JSONObject tempObject = localOp;
        localOp = extOp;
        extOp = tempObject;

        // local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // local end pos
        final JSONArray localToPos = OTUtils.optEndPosition(localOp);
        // ext start pos
        final JSONArray extStartPos = OTUtils.getStartPosition(extOp);
        // ext end pos
        final JSONArray extToPos = OTUtils.optEndPosition(extOp);

        // the slide position of the local move operation
        final int localStart0 = localStartPos.getInt(0);
        // the slide position of the ext move operation
        final int extStart0 = extStartPos.getInt(0);
        // the destination slide position of the local move operation
        final int localTo0 = localToPos.getInt(0);
        // the destination slide position of the ext move operation
        final int extTo0 = extToPos.getInt(0);

        final OTMoveMoveResult result = OTUtils.transformIndexMoveMove(localStart0, localTo0, extStart0, extTo0);
        final OTMoveResult localResult = result.getLclRes();
        final OTMoveResult extResult = result.getExtRes();

        if (localResult!=null) {
            localStartPos.put(0, localResult.getFromIdx());
            localToPos.put(0, localResult.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(localOp);
        }

        if (extResult!=null) {
            extStartPos.put(0, extResult.getFromIdx());
            extToPos.put(0, extResult.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }

    /**
     * Handling of two changeLayoutSlide (pptx) operations or two changeMaster (odp) operations.
     *
     * This is only possible in pptx case.
     * Only document slides can change their layout.
     *
     * pptx: changeLayout: { start: [3], target: "new_layout_id" }
     * odp : changeMaster: { start: [3], target: "new_master_id" }
     */
    private static JSONObject handleChangeLayoutChangeLayout(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the local target
        final String localTarget = OTUtils.getTarget(localOp);
        // the external target
        final String extTarget = OTUtils.getTarget(extOp);
        // the local document slide
        final int localDocumentSlide = OTUtils.getStartPosition(localOp).getInt(0);
        // the ext document slide
        final int extDocumentSlide = OTUtils.getStartPosition(extOp).getInt(0);
        if(localDocumentSlide == extDocumentSlide) {
            if(localTarget.equals(extTarget)) {
                throw new OTException(Type.RELOAD_REQUIRED, "handleChangeLayoutChangeLayout: " + localOp.toString() + ", " + extOp.toString());
            }
        }
        return null;
     }

    /**
     * Handling an changeLayout (pptx) or changeMaster (odp) operation and a deleteTargetSlide operation.
     *
     * This might require a document reload, if a document slide gets a new layout or master assigned,
     * that was removed in the meantime.
     *
     * Problems that require a reload:
     *
     * 1. The target slide cannot be restored.
     *
     * 2. After the insertSlide operation follow several further operations like insertDrawing
     *    and insertParagraph. These operations do not know anything about the layout slide (or
     *    its ID). Therefore these operations cannot be marked as removed, like this is the case
     *    for the changeLayout/changeMaster operation.
     *
     * deleteTargetSlide: { id: "2147483672" }
     * pptx: changeLayout: { start: [1], target: "new_layout_id" }
     * odp:  changeMaster: { start: [1], target: "new_master_id" }
     */
    private static JSONObject handleChangeLayoutDeleteTargetSlide(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_TARGET_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject changeOp = deleteOp == extOp ? localOp : extOp;
        if(OTUtils.getIdProperty(deleteOp).equals(OTUtils.getTarget(changeOp))) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleChangeLayoutDeleteTargetSlide: " + localOp.toString() + ", " + extOp.toString());
        }
        return null;
    }

    /**
     * Handling two deleteComment operations.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleDeleteCommentDeleteComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // ext start pos
        final JSONArray extStartPos = OTUtils.getStartPosition(extOp);
        // the slide position of the local move operation
        final int localStart0 = localStartPos.getInt(0);
        // the slide position of the ext move operation
        final int extStart0 = extStartPos.getInt(0);
        if(localStart0 == extStart0) {
            // the slide position of the local move operation
            final int localStart1 = localStartPos.getInt(1);
            // the slide position of the ext move operation
            final int extStart1 = extStartPos.getInt(1);
            if(localStart1 < extStart1) {
                extStartPos.put(1, extStart1 - 1);
            }
            else if(localStart1 > extStart1) {
                localStartPos.put(1, localStart1 - 1);
            }
            else {
                OTUtils.setOperationRemoved(extOp);
                OTUtils.setOperationRemoved(localOp);
            }
        }
        return null;
    }

    /**
     * Handling an insertComment and a changeComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleChangeCommentInsertComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the insertComment operation
        final JSONObject insertCommentOp = OTUtils.getOperation(OCValue.INSERT_COMMENT, localOp, extOp);
        // the changeComment operation
        final JSONObject changeCommentOp = insertCommentOp == extOp ? localOp : extOp;
        // the insertComment start pos
        final JSONArray insertCommentStartPos = OTUtils.getStartPosition(insertCommentOp);
        // the changeComment start pos
        final JSONArray changeCommentStartPos = OTUtils.getStartPosition(changeCommentOp);
        if(insertCommentStartPos.getInt(0) == changeCommentStartPos.getInt(0) && insertCommentStartPos.getInt(1) <= changeCommentStartPos.getInt(1)) {
            changeCommentStartPos.put(1, changeCommentStartPos.getInt(1) + 1);
        }
        return null;
    }

    /**
     * Handling any comment operation with a delete operation.
     *
     * These operatons influence each other, if the delete operation deletes a complete slide.
     */
    private static JSONObject handleCommentDelete(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE, localOp, extOp);
        // the comment operation
        final JSONObject commentOp = deleteOp == extOp ? localOp : extOp;
        // the delete start pos
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the comment start pos
        final JSONArray commentStartPos = OTUtils.getStartPosition(commentOp);
        if(deleteStartPos.length() > 1) { // handle only delete operations for slides
            return null;
        }
        if(deleteStartPos.getInt(0) < commentStartPos.getInt(0)) {
            commentStartPos.put(0, commentStartPos.getInt(0) - 1);
        }
        else if(deleteStartPos.getInt(0) == commentStartPos.getInt(0)) {
            OTUtils.setOperationRemoved(commentOp);
        }
        return null;
    }

    /**
     * Handling two changeComment operations.
     *
     * These operatons influence each other, if they happen in the same comment.
     */
    private static JSONObject handleChangeCommentChangeComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the local start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        // the ext start pos
        final JSONArray extStartPos = OTUtils.getStartPosition(extOp);
        if(localStartPos.getInt(0) == extStartPos.getInt(0) && localStartPos.getInt(1) == extStartPos.getInt(1)) {
            OTUtils.setOperationRemoved(localOp);
        }
        return null;
    }

    /**
     * Handling a deleteComment and a changeComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleChangeCommentDeleteComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_COMMENT, localOp, extOp);
        // the comment operation
        final JSONObject changeOp = deleteOp == extOp ? localOp : extOp;
        // the delete start pos
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the comment start pos
        final JSONArray changeStartPos = OTUtils.getStartPosition(changeOp);
        if(deleteStartPos.getInt(0) == changeStartPos.getInt(0)) {
            if(deleteStartPos.getInt(1) < changeStartPos.getInt(1)) {
                changeStartPos.put(1, changeStartPos.getInt(1) - 1);
            }
            else if(deleteStartPos.getInt(1) == changeStartPos.getInt(1)) {
                OTUtils.setOperationRemoved(changeOp);
            }
        }
        return null;
    }

    /**
     * Handling an insertComment and a deleteComment operation.
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleDeleteCommentInsertComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_COMMENT, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = deleteOp == extOp ? localOp : extOp;
        // the delete start pos
        final JSONArray deleteStartPos = OTUtils.getStartPosition(deleteOp);
        // the insert start pos
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        if(deleteStartPos.getInt(0) == insertStartPos.getInt(0)) {
            if(deleteStartPos.getInt(1) < insertStartPos.getInt(1)) { // check '<=' on server side?
                insertStartPos.put(1, insertStartPos.getInt(1) - 1);
            }
            else if(deleteStartPos.getInt(1) >= insertStartPos.getInt(1)) {
                deleteStartPos.put(1, deleteStartPos.getInt(1) + 1);
            }
        }
        return null;
    }

    /**
     * Handling two insertComment operations.
     *
     * These operatons influence each other, if they happen on the same slide.
     *
     * If there are two identical comment positions and one comment is a child of a previous comment and
     * the other comment is not, then the position of the comment, that is not a child, must be increased.
     */
    private static JSONObject handleInsertCommentInsertComment(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete start pos
        final JSONArray extStartPos = OTUtils.getStartPosition(extOp);
        // the insert start pos
        final JSONArray localStartPos = OTUtils.getStartPosition(localOp);
        if(extStartPos.getInt(0) == localStartPos.getInt(0)) {
            if(extStartPos.getInt(1) < localStartPos.getInt(1)) {
                localStartPos.put(1, localStartPos.getInt(1) + 1);
            }
            else {
                final Object extParent = extOp.opt(OCKey.PARENT.value());
                final Object localParent = localOp.opt(OCKey.PARENT.value());
                if((extParent instanceof Integer) && !(localParent instanceof Integer)) {
                    localStartPos.put(1, localStartPos.getInt(1) + 1); // increasing localernal number, if ext comment is a child comment
                }
                else if(!(extParent instanceof Integer) && (localParent instanceof Integer)) {
                    extStartPos.put(1,  extStartPos.getInt(1) + 1);
                }
                else {
                    extStartPos.put(1, extStartPos.getInt(1) + 1); // increasing local number, if both positions are equal
                }
            }
        }
        return null;
    }

    /**
     * Handling an insertMasterSlide operation with a deleteTargetSlide operation.
     *
     * In pptx the insertMasterSlide operation is triggered by an undo of a removal of a master slide.
     * In odp the insertMasterSlide operation is triggered by duplicating a master (layout) slide.
     *
     * pptx:
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * insertMasterSlide: { id: "2147483672", start: 2, attrs: { slide: { type: "cust" } } }
     *
     * odp:
     * deleteTargetSlide: { id: "2147483650" }
     * insertMasterSlide: { id: "2147483672", attrs: { slide: { type: "cust" } } }
     *
     * In odp there is an arbitrary order of master (layout) slides. It is not important, if the order
     * is different on different clients.
     */
    private static JSONObject handleInsertMasterSlideDeleteTargetSlide(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_TARGET_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = deleteOp == extOp ? localOp : extOp;
        // is master slide removed
        final boolean isMasterSlideRemoved = !deleteOp.has(OCKey.PARENTTARGET.value());
        // optional start
        final Object insertStart = insertOp.opt(OCKey.START.value());
        // optional start
        final Object deleteIndex = deleteOp.opt(OCKey.INDEX.value());
        // only in pptx case, the position might be adapted, if a master slide is inserted and another one deleted.
        if(isMasterSlideRemoved && insertStart instanceof Integer && deleteIndex instanceof Integer) {
            if((Integer)deleteIndex < (Integer)insertStart) {
                insertOp.put(OCKey.START.value(), (Integer)insertStart - 1);
            }
            else {
                deleteOp.put(OCKey.INDEX.value(),  (Integer)deleteIndex + 1);
            }
        }
        return null;
    }

    /**
     * Handling of two insertMasterSlide operations.
     *
     * In odp this affects the 'layout' slides.
     * In pptx there is a start number to sort the master slides (but this is not important for the user).
     * In pptx the operation "insertMasterSlide" is only generated in undo.
     *
     * odp:  insertMasterSlide { id: "master_id", attrs: { .... } }
     * pptx: insertMasterSlide { start: 2, id: "master_id", attrs: { ... } }
     */
    private static JSONObject handleInsertMasterSlideInsertMasterSlide(JSONObject localOp, JSONObject extOp) throws JSONException {
        if(localOp.getString(OCKey.ID.value()).equals(extOp.getString(OCKey.ID.value()))) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertMasterSlideInsertMasterSlide: " + localOp.toString() + ", " + extOp.toString());
        }
        final Object localStart = localOp.opt(OCKey.START.value());
        final Object extStart = extOp.opt(OCKey.START.value());
        if(localStart instanceof Integer && extStart instanceof Integer) {
            if((Integer)localStart < (Integer)extStart) { // <= on client
                extOp.put(OCKey.START.value(), (Integer)extStart + 1);
            }
            else {
                localOp.put(OCKey.START.value(), (Integer)localStart + 1);
            }
        }
        return null;
    }

    /**
     * Handling an insertLayoutSlide operation with a deleteTargetSlide operation.
     * This is relevant, if a layout slide or a master slide is removed.
     *
     * insertLayoutSlide collides with deletion of layoutSlide. This is relevant for the start position value
     * below the master slide
     *
     * insertLayoutSlide collides with deletion of masterSlide. This is relevant, if the master slide for the
     * inserted layout slide was removed. In this case a reload is required.
     *
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * insertLayoutSlide: { id: "2147483672", target: "2147483648", attrs: { slide: { type: "cust" } }, start: 3 }
     */
    private static JSONObject handleInsertLayoutSlideDeleteTargetSlide(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_TARGET_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = deleteOp == extOp ? localOp : extOp;
        // insert target
        final String insertTarget = insertOp.getString(OCKey.TARGET.value());
        //  avoiding that insertLayoutSlide collides with deletion of masterSlide
        if(insertTarget.equals(deleteOp.getString(OCKey.ID.value()))) { //  'ERROR: OT conflict, master slide for new inserted layout slide removed.'); // Reload required
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertLayoutSlideDeleteTargetSlide: " + localOp.toString() + ", " + extOp.toString());
        }
        // If the master was removed, the insertLayoutSlide can be ignored completely.
        // But also all following operations after insertLayoutSlide must be ignored -> RELOAD REQUIRED
        if(insertTarget.equals(deleteOp.optString(OCKey.PARENTTARGET.value()))) {
            // Adapting the index of both operations, because another layout slide of the same master slide was removed.
            final int deleteIndex = deleteOp.getInt(OCKey.INDEX.value());
            final int insertStart = insertOp.getInt(OCKey.START.value());
            if (deleteIndex < insertStart) {
                insertOp.put(OCKey.START.value(), insertStart - 1);
            }
            else {
                deleteOp.put(OCKey.INDEX.value(), deleteIndex + 1);
            }
        }
        return null;
    }

    /**
     * Handling an insertLayoutSlide operation with a moveLayoutSlide operation.
     * This is relevant, if a layout slide or a master slide is removed.
     *
     * insertLayoutSlide: { start: 1, target: "2147483648", id: "2147483660", attrs: { slide: { type: "titleOnly" }}}
     * moveLayoutSlide: { id: "2147483660", target: "2147483648", oldtarget: "2147483648", oldindex: 1, start: 0 }
     * -> even the target (the masterSlide) might change
     */
    private static JSONObject handleInsertLayoutSlideMoveLayoutSlide(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE_LAYOUT_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = moveOp == extOp ? localOp : extOp;
        // the insert target
        final String insertTarget = insertOp.getString(OCKey.TARGET.value());
        // the move target
        final String moveTarget = moveOp.getString(OCKey.TARGET.value());
        // the old move target
        final String moveTargetOld = moveOp.getString(OCKey.OLDTARGET.value());
        // whether the layout slide was moved without changing the master
        final boolean sameMasterMove = moveTarget.equals(moveTargetOld);
        // the slide position of the insert operation
        final int insertCompSlidePos = insertOp.getInt(OCKey.START.value());
        // the slide start position of the move operation
        final int movePosStart = moveOp.getInt(OCKey.OLDINDEX.value());
        // the slide end position of the move operation
        final int movePosTo = moveOp.getInt(OCKey.START.value());
        if(!insertTarget.equals(moveTarget) && !insertTarget.equals(moveTargetOld)) {
            return null; // nothing to do
        }
        if (sameMasterMove) {
            // Changing the move operation (insert moves both positions, even if positions are equal)
            if (insertCompSlidePos <= movePosStart) {
                moveOp.put(OCKey.OLDINDEX.value(), moveOp.getInt(OCKey.OLDINDEX.value()) + 1);
            }
            if (insertCompSlidePos <= movePosTo) {
                moveOp.put(OCKey.START.value(), moveOp.getInt(OCKey.START.value()) + 1);
            }
            // Changing also the insertSlide operation
            if (movePosStart >= insertCompSlidePos && movePosTo < insertCompSlidePos) {
                insertOp.put(OCKey.START.value(), insertOp.getInt(OCKey.START.value()) + 1);
            }
            else if (movePosStart < insertCompSlidePos && movePosTo >= insertCompSlidePos) {
                insertOp.put(OCKey.START.value(), insertOp.getInt(OCKey.START.value()) - 1);
            }
        }
        else {
            // changing the master slide for the layout slide during move
            if (insertTarget.equals(moveTargetOld)) {
                if (insertCompSlidePos <= movePosStart) {
                    moveOp.put(OCKey.OLDINDEX.value(), moveOp.getInt(OCKey.OLDINDEX.value()) + 1);
                }
                if (movePosStart < insertCompSlidePos) {
                    insertOp.put(OCKey.START.value(), insertOp.getInt(OCKey.START.value()) - 1);
                }
            }
            else if  (insertTarget.equals(moveTarget)) {
                if (insertCompSlidePos <= movePosTo) {
                    moveOp.put(OCKey.START.value(), moveOp.getInt(OCKey.START.value()) + 1);
                }
                if (movePosTo < insertCompSlidePos) {
                    insertOp.put(OCKey.START.value(), insertOp.getInt(OCKey.START.value()) + 1);
                }
            }
        }
        return null;
    }

    /**
     * Handling a moveSlide operation and an insertComp operation (insertParagraph and insertSlide
     * (insertTable does not exist)).
     *
     * In the case of an insertSlide operation, both operations influence each other.
     * In the case of an insertParagraph operation, the moveSlide operation affects the insertPargraph
     * operation, but not vice versa.
     *
     * Example:
     * { name: moveSlide, start: [1], end: [3] }
     */
    private static JSONObject handleMoveSlideInsertComp(JSONObject localOp, JSONObject extOp) throws JSONException {
        // the move operation
        final JSONObject moveOp = OTUtils.getOperation(OCValue.MOVE_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = moveOp == extOp ? localOp : extOp;
        // the start position of the move operation
        final JSONArray moveStartPos = OTUtils.getStartPosition(moveOp);
        // move to pos
        final JSONArray moveToPos = OTUtils.optEndPosition(moveOp);
        // the ext document slide
        final JSONArray insertStartPos = OTUtils.getStartPosition(insertOp);
        // the slide start position of the move operation
        final int moveStart0  = moveStartPos.getInt(0);
        // the slide to position of the move operation
        final int moveTo0 = moveToPos.getInt(0);
        // the slide position of the insert operation
        final int insert0 = insertStartPos.getInt(0);

        if(insertStartPos.length()==1) {  // handling an insertSlide operation -> both operations influence each other
            // Changing the move operation (insert moves both positions, even if positions are equal)
            if (insert0 <= moveStart0) {
                moveStartPos.put(0, moveStart0 + 1);
            }
            if (insert0 <= moveTo0) {
                moveToPos.put(0, moveTo0 + 1);
            }
            // Changing also the insertSlide operation
            if (moveStart0 >= insert0 && moveTo0 < insert0) {
                insertStartPos.put(0, insert0 + 1);
            }
            else if (moveStart0 < insert0 && moveTo0 >= insert0) {
                insertStartPos.put(0, insert0 - 1);
            }
        }
        else {
            if (moveStart0 == insert0) {
                insertStartPos.put(0, moveTo0); // the slide with the change was moved
            }
            else if (moveStart0 > insert0 && moveTo0 <= insert0) {
                insertStartPos.put(0, insert0 + 1);
            }
            else if (moveStart0 < insert0 && moveTo0 >= insert0) {
                insertStartPos.put(0, insert0 - 1);
            }
        }
        return null;
    }

    /**
     * Handling a moveLayoutSlide operation with a deleteTargetSlide operation.
     *
     * moveLayoutSlide is only generated in PPTX files, not in ODP files. In the latter the master
     * (layout) slides are not sorted.
     *
     * The layout slide can be moved within one master slide or from one master slide to another
     * master slide. It must be checked, if the source master slide or the destination master slide
     * was removed.
     *
     * It can also happen, that the moved layout slide is removed by another client.
     *
     * deleteTargetSlide: { id: "2147483650", index: 2, parenttarget: "2147483756" }
     * moveLayoutSlide: { id: "2147483734", target: "2147483732", oldtarget: "2147483732", oldindex: 1, start: 0 }
     */
    private static JSONObject handleMoveLayoutSlideDeleteTargetSlide(JSONObject extOp, JSONObject localOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_TARGET_SLIDE, localOp, extOp);
        // the move operation
        final JSONObject moveOp = deleteOp == extOp ? localOp : extOp;
        // the move start
        final int moveStart = moveOp.getInt(OCKey.START.value());
        // the move start
        final int moveOldIndex = moveOp.getInt(OCKey.OLDINDEX.value());
        // the move oldtarget
        final String moveId = moveOp.getString(OCKey.ID.value());
        // the move oldtarget
        final String moveTarget = moveOp.getString(OCKey.TARGET.value());
        // the move oldtarget
        final String moveOldTarget = moveOp.getString(OCKey.OLDTARGET.value());
        // the delete id
        final String deleteId = deleteOp.getString(OCKey.ID.value());

        if (!deleteOp.has(OCKey.PARENTTARGET.value())) { // whether a master slide was removed
            // whether the move source target slide was deleted
            final boolean moveSourceDeleted = moveOldTarget.equals(deleteId);
            // whether the move destination target slide was deleted
            final boolean moveDestinationDeleted = moveTarget.equals(deleteId);
            if(moveSourceDeleted && moveDestinationDeleted) {
                // the master slide was removed
                OTUtils.setOperationRemoved(moveOp);
                // no need to change the delete operation
            }
            else if(moveSourceDeleted || moveDestinationDeleted) {
                JSONArray localOpsAfter = null;
                JSONArray externalOpsAfter = null;

                // the source or the destination master slide was removed
                OTUtils.setOperationRemoved(moveOp);

                // expanding the delete operation for the deleted layout slide
                final JSONObject newDeleteOperation = OTUtils.cloneJSONObject(deleteOp);
                OTUtils.setOperationName(newDeleteOperation, OCValue.DELETE.value());
                final JSONArray start = new JSONArray(1);
                start.put(0);
                newDeleteOperation.put(OCKey.START.value(), start);
                newDeleteOperation.put(OCKey.TARGET.value(), moveId);
                newDeleteOperation.remove(OCKey.ID.value());
                newDeleteOperation.remove(OCKey.INDEX.value());

                // expanding the delete operation for the deleted layout slide
                final JSONObject newDeleteTargetSlideOperation = OTUtils.cloneJSONObject(deleteOp);
                // adapting index and parenttarget
                newDeleteTargetSlideOperation.put(OCKey.ID.value(), moveId);
                newDeleteTargetSlideOperation.put(OCKey.INDEX.value(), moveSourceDeleted ? moveStart : moveOldIndex);
                newDeleteTargetSlideOperation.put(OCKey.PARENTTARGET.value(), moveSourceDeleted ? moveTarget : moveOldTarget);
                if((moveOp == localOp && moveSourceDeleted) || (moveOp == extOp && moveDestinationDeleted)) {
                    localOpsAfter = new JSONArray(2);
                    localOpsAfter.put(newDeleteOperation);
                    localOpsAfter.put(newDeleteTargetSlideOperation);
                }
                else if((moveOp == extOp && moveSourceDeleted) || (moveOp == localOp && moveDestinationDeleted)) {
                    externalOpsAfter = new JSONArray(2);
                    externalOpsAfter.put(newDeleteOperation);
                    externalOpsAfter.put(newDeleteTargetSlideOperation);
                }
                return JSONHelper.getResultObject(null, externalOpsAfter, null, localOpsAfter);
            }
        }
        else { // a layout slide was removed
            if(moveId.equals(deleteId)) {
                OTUtils.setOperationRemoved(moveOp); // move operation no longer required

                // the new position of the moved layout slide
                deleteOp.put(OCKey.INDEX.value(), moveStart);
                deleteOp.put(OCKey.PARENTTARGET.value(), moveTarget);
            }
            else {
                // whether the layout slide was moved without changing the master
                final boolean sameMasterMove = moveTarget.equals(moveOldTarget);
                // delete parent target
                final String deleteParentTarget = deleteOp.getString(OCKey.PARENTTARGET.value());
                // delete index
                final int deleteIndex = deleteOp.getInt(OCKey.INDEX.value());
                // the positions might influence each other
                if(sameMasterMove && deleteParentTarget.equals(moveTarget)) {
                    if (deleteIndex < moveOldIndex) {
                        moveOp.put(OCKey.OLDINDEX.value(), moveOldIndex - 1);
                    }
                    if (deleteIndex < moveStart) {
                        moveOp.put(OCKey.START.value(), moveStart - 1);
                    }
                    if (moveOldIndex > deleteIndex && moveStart <= deleteIndex) {
                        deleteOp.put(OCKey.INDEX.value(), deleteIndex + 1);
                    }
                    else if (moveOldIndex < deleteIndex && moveStart > deleteIndex) {
                        deleteOp.put(OCKey.INDEX.value(), deleteIndex - 1);
                    }
                }
                else if(!sameMasterMove) {
                    if (deleteParentTarget.equals(moveTarget)) {
                        // a layoutslide below the destination master slide was removed
                        if (deleteIndex < moveStart) {
                            moveOp.put(OCKey.START.value(),  moveStart - 1);
                        }
                        if (moveStart <= deleteIndex) {
                            deleteOp.put(OCKey.INDEX.value(), deleteIndex + 1);
                        }
                    }
                    else if (deleteParentTarget.equals(moveOldTarget)) {
                        // a layoutslide below the source master slide was removed
                        if (deleteIndex < moveOldIndex) {
                            moveOp.put(OCKey.OLDINDEX.value(), moveOldIndex - 1);
                        }
                        if (moveOldIndex < deleteIndex) {
                            deleteOp.put(OCKey.INDEX.value(), deleteIndex - 1);
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Handling of two moveLayoutSlide operations.
     *
     * This is only possible in pptx case.
     *
     * moveLayoutSlide: { id: "2147483660", start: 0, target: "2147483648", oldindex: 1, oldtarget: "2147483648" }
     */
    private static JSONObject handleMoveLayoutSlideMoveLayoutSlide(JSONObject extOp, JSONObject localOp) throws JSONException {
        // ignore both, if both are identical
        // do nothing, if all targets of both operations are different

        final String localFromTarget = localOp.getString(OCKey.OLDTARGET.value());
        final String extFromTarget = extOp.getString(OCKey.OLDTARGET.value());

        final String localToTarget = localOp.getString(OCKey.TARGET.value());
        final String extToTarget = extOp.getString(OCKey.TARGET.value());

        final int localFromPos = localOp.getInt(OCKey.OLDINDEX.value());
        final int extFromPos = extOp.getInt(OCKey.OLDINDEX.value());

        final int localToPos = localOp.getInt(OCKey.START.value());
        final int extToPos = extOp.getInt(OCKey.START.value());

        final boolean localSameMasterMove = localFromTarget.equals(localToTarget);
        final boolean extSameMasterMove = extFromTarget.equals(extToTarget);

        // var sameSlideMoved = localOp.id === extOp.id;
        final boolean sameSlideMoved = localFromTarget.equals(extFromTarget) && localFromPos == extFromPos;

        if (localSameMasterMove && extSameMasterMove && !localFromTarget.equals(extFromTarget)) {
            return null;
        } // nothing to do
        if (sameSlideMoved && localToTarget.equals(extToTarget) && localToPos == extToPos) {
            // identical move operations -> both can be ignored
            OTUtils.setOperationRemoved(localOp);
            OTUtils.setOperationRemoved(extOp);
        }
        else if (sameSlideMoved) {
            // different destination for the same layout slide -> setting the new source to local operation that will be sent to the server
            OTUtils.setOperationRemoved(extOp); // setting marker at external operation
            localOp.put(OCKey.OLDINDEX.value(), extToPos);
            localOp.put(OCKey.OLDTARGET.value(), extToTarget);
        }
        else if (localSameMasterMove && extSameMasterMove && localFromTarget.equals(extFromTarget)) {

            final OTMoveMoveResult result = OTUtils.transformIndexMoveMove(localFromPos, localToPos, extFromPos, extToPos);
            final OTMoveResult localResult = result.getLclRes();
            final OTMoveResult extResult = result.getExtRes();

            if (localResult!=null) {
                localOp.put(OCKey.OLDINDEX.value(), localResult.getFromIdx());
                localOp.put(OCKey.START.value(), localResult.getToIdx());
            }
            else {
                OTUtils.setOperationRemoved(localOp);
            }

            if (extResult!=null) {
                extOp.put(OCKey.OLDINDEX.value(), extResult.getFromIdx());
                extOp.put(OCKey.START.value(), extResult.getToIdx());
            }
            else {
                OTUtils.setOperationRemoved(extOp);
            }

        }
        else if (localSameMasterMove && (localFromTarget.equals(extFromTarget) || localFromTarget.equals(extToTarget))) {
            // different layout slides moved: local move below one master, external move source or destination below this master
            if (localFromTarget.equals(extFromTarget)) { // the external operation moves the slide away from the local move master -> extToPos can be ignored

                if (localFromPos > extFromPos && localToPos <= extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extFromPos + 1);
                }
                else if (localFromPos < extFromPos && localToPos >= extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extFromPos - 1);
                }
                if (extFromPos < localFromPos) {
                    localOp.put(OCKey.OLDINDEX.value(), localFromPos - 1);
                }
                if (extFromPos < localToPos) {
                    localOp.put(OCKey.START.value(), localToPos - 1);
                }
                if (extFromPos == localToPos && localFromPos < localToPos) {
                    localOp.put(OCKey.START.value(), localOp.getInt(OCKey.START.value()) - 1);
                }
            }
            else { // localFromTarget === extToTarget, the external operation moves the slide to the local move master -> extFromPos can be ignored
                if (localFromPos > extToPos && localToPos < extToPos) {
                    extOp.put(OCKey.START.value(), extToPos + 1);
                }
                else if (localFromPos < extToPos && localToPos > extToPos) {
                    extOp.put(OCKey.START.value(), extToPos - 1);
                }
                if (extToPos <= localFromPos) {
                    localOp.put(OCKey.OLDINDEX.value(), localFromPos + 1);
                }
                if (extToPos <= localToPos) {
                    localOp.put(OCKey.START.value(), localToPos + 1);
                }
            }
            if (localOp.getInt(OCKey.START.value()) == localOp.getInt(OCKey.OLDINDEX.value())) {
                OTUtils.setOperationRemoved(localOp);
            }
        }
        else if (extSameMasterMove && (extFromTarget.equals(localFromTarget) || extFromTarget.equals(localToTarget))) {
            // different layout slides moved: external move below one master, local move source or destination below this master
            if (extFromTarget.equals(localFromTarget)) { // the local operation moves the slide away from the external move master -> localToPos can be ignored
                if (extFromPos > localFromPos && extToPos <= localFromPos) {
                    localOp.put(OCKey.OLDINDEX.value(), localFromPos + 1);
                }
                else if (extFromPos < localFromPos && extToPos >= localFromPos) {
                    localOp.put(OCKey.OLDINDEX.value(), localFromPos - 1);
                }
                if (localFromPos < extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extFromPos - 1);
                }
                if (localFromPos < extToPos) {
                    extOp.put(OCKey.START.value(), extToPos - 1);
                }
                if (localFromPos == extToPos && extFromPos < extToPos) {
                    extOp.put(OCKey.START.value(),  extOp.getInt(OCKey.START.value()) - 1);
                }
            }
            else { // extFromTarget === localToTarget, the local operation moves the slide to the external move master -> localFromPos can be ignored

                if (extFromPos > localToPos && extToPos < localToPos) {
                    localOp.put(OCKey.START.value(), localToPos + 1);
                }
                else if (extFromPos < localToPos && extToPos > localToPos) {
                    localOp.put(OCKey.START.value(), localToPos - 1);
                }
                if (localToPos <= extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extFromPos + 1);
                }
                if (localToPos <= extToPos) {
                    extOp.put(OCKey.START.value(), extToPos + 1);
                }
            }
            if (extOp.getInt(OCKey.START.value()) == extOp.getInt(OCKey.OLDINDEX.value())) {
                OTUtils.setOperationRemoved(extOp);
            }
        }
        else if (localFromTarget.equals(extFromTarget) || localFromTarget.equals(extToTarget) || localToTarget.equals(extFromTarget) || localToTarget.equals(extToTarget)) {
            // different layout slides moved: both move operations change the master slide
            if (localFromTarget.equals(extFromTarget)) {
                if (localFromPos < extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extFromPos - 1);
                }
                else {
                    localOp.put(OCKey.OLDINDEX.value(), localFromPos - 1);
                }
            }
            if (localFromTarget.equals(extToTarget)) {
                if (localFromPos < extToPos) {
                    extOp.put(OCKey.START.value(), extOp.getInt(OCKey.START.value()) - 1);
                }
                else {
                    localOp.put(OCKey.OLDINDEX.value(), localOp.getInt(OCKey.OLDINDEX.value()) + 1);
                }
            }
            if (localToTarget.equals(extFromTarget)) {
                if (localToPos <= extFromPos) {
                    extOp.put(OCKey.OLDINDEX.value(), extOp.getInt(OCKey.OLDINDEX.value()) + 1);
                }
                else {
                    localOp.put(OCKey.START.value(), localOp.getInt(OCKey.START.value()) - 1);
                }
            }
            if (localToTarget.equals(extToTarget)) {
                if (localToPos <= extToPos) {
                    extOp.put(OCKey.START.value(), extOp.getInt(OCKey.START.value()) + 1);
                }
                else {
                    localOp.put(OCKey.START.value(), localOp.getInt(OCKey.START.value()) + 1);
                }
            }
        }
        return null;
    }

    /**
     * Handling an insertSlide and a deleteTargetSlide operation.
     *
     * This might require a document reload, if a new slide was inserted, whose layout slide
     * was removed in the meantime.
     *
     * Problems that require a reload:
     *
     * 1. The target slide cannot be restored.
     *
     * 2. After the insertSlide operation follow several further operations like insertDrawing
     *    and insertParagraph. These operations do not know anything about the layout slide (or
     *    its ID). Therefore these operations cannot be marked as removed, like this is the case
     *    for the insertSlide operation.
     *
     * deleteTargetSlide: { id: "2147483672" }
     * insertSlide: { start: [1], target: "2147483672" }
     */
    private static JSONObject handleInsertSlideDeleteTargetSlide(JSONObject extOp, JSONObject localOp) throws JSONException {
        // the delete operation
        final JSONObject deleteOp = OTUtils.getOperation(OCValue.DELETE_TARGET_SLIDE, localOp, extOp);
        // the insert operation
        final JSONObject insertOp = deleteOp == extOp ? localOp : extOp;
        if(deleteOp.getString(OCKey.ID.value()).equals(insertOp.getString(OCKey.TARGET.value()))) {
            // 'ERROR: OT conflict, layout slide for inserted document slide removed.');
            // this conflict cannot be resolved, because all following operations for drawings and paragraphs have no
            // information about the removed target.
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertSlideDeleteTargetSlide: " + extOp.toString() + ", " + localOp.toString());
        }
        return null;
    }

    /**
     * Handling an insertCells and a deleteColumns operation.
     *
     * This function is calling the baseHandler first and then deciding if this transformation is valid for presentation.
     * A reload required, when a cell cannot be inserted (DOCS-4422).
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleInsertCellsDeleteColumns(JSONObject extOp, JSONObject localOp) throws JSONException {
        final JSONObject ret = TransformHandlerBasic.handleInsertCellsDeleteColumns(extOp, localOp);
        final JSONObject insertCellsOp = OTUtils.isInsertCellsOperation(extOp) ? extOp : localOp;
        if (OTUtils.isOperationRemoved(insertCellsOp)) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertCellsDeleteColumns: " + extOp.toString() + ", " + localOp.toString());
        }
        return ret;
    }

    /**
     * Handling an insertCells and a delete operation.
     *
     * This function is calling the baseHandler first and then deciding if this transformation is valid for presentation.
     * A reload required, when a cell cannot be inserted (DOCS-4422).
     *
     * These operatons influence each other, if they happen on the same slide.
     */
    private static JSONObject handleInsertCellsDelete(JSONObject extOp, JSONObject localOp) throws JSONException {
        final JSONObject ret = TransformHandlerBasic.handleInsertRowsDelete(extOp, localOp);
        final JSONObject insertCellsOp = OTUtils.isInsertCellsOperation(extOp) ? extOp : localOp;
        if (OTUtils.isOperationRemoved(insertCellsOp)) {
            throw new OTException(Type.RELOAD_REQUIRED, "handleInsertCellsDelete: " + extOp.toString() + ", " + localOp.toString());
        }
        return ret;
    }
}
