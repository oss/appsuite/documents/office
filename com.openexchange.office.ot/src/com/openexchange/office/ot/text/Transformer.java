/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.text;

import org.json.JSONArray;
import org.json.JSONException;
import com.openexchange.office.ot.TransformHandlerBasic;
import com.openexchange.office.ot.TransformOptions;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.tools.OTUtils;

//=============================================================================

public class Transformer {

    /**
     * Transforming an array of external operations by using all pending operations
     * that need to be applied to the external operations.
     *
     * @param {JSONArray} pendingOps
     *  The collection of operations that are already applied to the document on
     *  server side, but were missing, when the client generated the new operations
     *  before sending them to the server.
     *
     * @param {JSONArray} newOps
     *  The array that contains all operations sent from the client. After this OT
     *  process these operations are applied to the document and sent to all other
     *  clients.
     *
     * @returns {Object[]}
     *  The transformed external operations. These operations are applied to the
     *  document and sent to all other clients.
     */

    public static JSONArray transformOperations(final TransformOptions options, final JSONArray pendingOps, final JSONArray newOps) throws JSONException {
        return OTUtils.transformOperations(options, opPairMaps, pendingOps, newOps);
    }

    public static ITransformHandlerMap[] getTransformHandlerMaps() {
        return opPairMaps;
    }

    static final private ITransformHandlerMap[] opPairMaps = {
        new TransformHandler(),
        new TransformHandlerBasic()
    };
}
