/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.text;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.TransformHandlerBasic;
import com.openexchange.office.ot.tools.ITransformHandler;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.tools.OpPair;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.ot.tools.OTUtils;

public class TransformHandler implements ITransformHandlerMap {

    @Override
    public Map<OpPair, ITransformHandler> getTransformHandlerMap() {
        return opPairMap;
    }

    @Override
    public Set<OCValue> getSupportedOperations() {
        return supOpsSet;
    }

    @Override
    public Set<OCValue> getIgnoreOperations() {
        return null;
    }

    static final public ImmutableSet<OCValue> supOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.CHANGE_COMMENT)
        .add(OCValue.DELETE_HEADER_FOOTER)
        .add(OCValue.DELETE_LIST_STYLE)
        .add(OCValue.INSERT_COMMENT)
        .add(OCValue.INSERT_COMPLEX_FIELD)
        .add(OCValue.INSERT_HEADER_FOOTER)
        .add(OCValue.INSERT_LIST_STYLE)
        .add(OCValue.MOVE)
        .add(OCValue.UPDATE_COMPLEX_FIELD)
        .build();

    static final public ImmutableMap<OpPair, ITransformHandler> opPairMap;

    static {

        final Map<OpPair, ITransformHandler> map = new HashMap<OpPair, ITransformHandler>();
        final Set<OpPair> aliases = new HashSet<OpPair>();

        // ALIAS_IGNORABLE
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.DELETE_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.DELETE_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.MOVE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.INSERT_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsIgnorable, OCValue.UPDATE_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        // ALIAS_INSERT_CHAR
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.DELETE_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.DELETE_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.MOVE, (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.UPDATE_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp));
        OTUtils.registerTransformation(map, aliases, TransformHandlerBasic.opsInsertChar, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp), false);
        OTUtils.registerTransformation(map, aliases, OCValue.INSERT_COMMENT, TransformHandlerBasic.opsInsertChar, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp), false);
        // ALIAS_INSERT_COMP
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.DELETE_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.DELETE_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.MOVE, (options, localOp, externOp) -> handleMoveInsertParagraph(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.INSERT_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleInsertParaInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsInsertComp, OCValue.UPDATE_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertPara(localOp, externOp));
        // ALIAS_MERGE_COMP
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaMerge(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.DELETE_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.DELETE_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> TransformHandlerBasic.handleParaMergeInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_HEADER_FOOTER, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_LIST_STYLE, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.MOVE, (options, localOp, externOp) -> handleParaMergeMove(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.INSERT_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleParaMergeInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsMergeComp, OCValue.UPDATE_COMPLEX_FIELD, (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaMerge(localOp, externOp));

        opPairMap = new ImmutableMap.Builder<OpPair, ITransformHandler>()

        .putAll(map)

        // CHANGE_COMMENT
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleChangeCommentChangeComment(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) ->  TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.MOVE), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDelete(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaSplit(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsSplitTable(localOp, externOp))
        .put(new OpPair(OCValue.CHANGE_COMMENT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // DELETE_HEADER_FOOTER
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTargetDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_HEADER_FOOTER, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        // DELETE_LIST_STYLE
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> handleDeleteListStyleDeleteListStyle(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> handleInsertListStyleDeleteListStyle(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_LIST_STYLE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // INSERT_COMMENT
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) ->  TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharDelete(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteColumnsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertColumnInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleParaSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMMENT, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        // INSERT_COMPLEX_FIELD
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharDelete(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteColumnsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertColumnInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleParaSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COMPLEX_FIELD, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        // INSERT_HEADER_FOOTER
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> handleInsertHeaderInsertHeader(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_HEADER_FOOTER, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // INSERT_LIST_STYLE
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> handleInsertListStyleDeleteListStyle(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> handleInsertListStyleInsertListStyle(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.MOVE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // ---
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_LIST_STYLE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // MOVE
        .put(new OpPair(OCValue.MOVE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.MOVE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE), (options, localOp, externOp) -> handleMoveDelete(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> handleMoveDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_CELLS), (options, localOp, externOp) -> handleMoveInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> handleMoveInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.INSERT_ROWS), (options, localOp, externOp) -> handleMoveInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> handleParaSplitMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> handleParaSplitMove(localOp, externOp))
        .put(new OpPair(OCValue.MOVE, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        // UPDATE_COMPLEX_FIELD
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.MOVE), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> handleUpdateComplexFieldUpdateComplexField(localOp, externOp))
        // ---
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.DELETE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDelete(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.DELETE_COLUMNS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_CELLS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_COLUMN), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_DRAWING), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.INSERT_ROWS), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.SET_ATTRIBUTES), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.SPLIT_PARAGRAPH), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaSplit(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.SPLIT_TABLE), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsSplitTable(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_COMPLEX_FIELD, OCValue.UPDATE_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())

        //
        // basic operations vs text specific
        //
        // DELETE
        .put(new OpPair(OCValue.DELETE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE, OCValue.MOVE), (options, localOp, externOp) -> handleMoveDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharDelete(localOp, externOp))
        .put(new OpPair(OCValue.DELETE, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDelete(localOp, externOp))
        // DELETE COLUMNS
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteColumnsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveDeleteColumns(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteColumnsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.DELETE_COLUMNS, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsDeleteColumns(localOp, externOp))
        // INSERT_CELLS
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_CELLS, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        // INSERT_COLUMN
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertColumnInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertColumn(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertColumnInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_COLUMN, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertColumn(localOp, externOp))
        // INSERT_DRAWING
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertCharInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_DRAWING, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        // INSERT_ROWS
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.MOVE), (options, localOp, externOp) -> handleMoveInsertRows(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleInsertRowsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.INSERT_ROWS, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertRows(localOp, externOp))
        // SET_ATTRIBUTES
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.MOVE), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SET_ATTRIBUTES, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        // SPLIT_PARAGRAPUH
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaSplit(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleParaSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.MOVE), (options, localOp, externOp) -> handleParaSplitMove(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleParaSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_PARAGRAPH, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsParaSplit(localOp, externOp))
        // SPLIT_TABLE
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsSplitTable(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.MOVE), (options, localOp, externOp) -> handleParaSplitMove(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleTableSplitInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.SPLIT_TABLE, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsSplitTable(localOp, externOp))
        // UPDATE_FIELD
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.CHANGE_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.DELETE_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleDeleteTarget(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.DELETE_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_COMMENT), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_HEADER_FOOTER), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_LIST_STYLE), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.MOVE), (options, localOp, externOp) -> handleSetAttrsMove(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.INSERT_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleSetAttrsInsertChar(localOp, externOp))
        .put(new OpPair(OCValue.UPDATE_FIELD, OCValue.UPDATE_COMPLEX_FIELD), (options, localOp, externOp) -> TransformHandlerBasic.handleNothing())
        .build();
    }

    private static JSONObject handleInsertCharExtPosition(final JSONObject insertOp, final JSONObject posOp) throws JSONException {
        JSONArray startInsert = OTUtils.getStartPosition(insertOp);
        JSONArray startPos = OTUtils.getStartPosition(posOp);
        int insertLength = startInsert.length();
        int posLength = startPos.length();
        int length = 1;
        int textIndex = insertLength - 1;

        if (posLength >= insertLength && JSONHelper.isEqual(startInsert, startPos, textIndex)) {
            if (startInsert.getInt(textIndex) <= startPos.getInt(textIndex)) {
                if (OTUtils.isInsertTextOperation(insertOp)) {
                    length = OTUtils.getText(insertOp).length();
                }
                startPos.put(textIndex, startPos.getInt(textIndex) + length);
            }
        }
        return null;
    }

    private static JSONObject handleDeleteExtPosition(final JSONObject deleteOp, final JSONObject posOp) throws JSONException {

        JSONArray startDelete = OTUtils.getStartPosition(deleteOp);
        JSONArray endDelete = OTUtils.optEndPosition(deleteOp);
        JSONArray startPos = OTUtils.getStartPosition(posOp);
        int deleteLength = startDelete.length();
        int posLength = startPos.length();
        int lastIndex = 0;
        boolean isRange = endDelete != null && !JSONHelper.isEqual(startDelete, endDelete);

        if (!isRange) {
            if (deleteLength <= posLength) {
                lastIndex = deleteLength - 1;
                if (lastIndex == 0 || JSONHelper.isEqual(startDelete, startPos, lastIndex)) {
                    if (startDelete.getInt(lastIndex) < startPos.getInt(lastIndex)) {
                        startPos.put(lastIndex, startPos.getInt(lastIndex) - 1);
                    } else {
                        startPos = null; // invalidating the position, not possible to determine new position
                    }
                }
            }
        } else {
            if (JSONHelper.compareNumberArrays(startPos, startDelete) < 0) {
                return null;
            } // no change of external position

            if (JSONHelper.ancestorRemoved(deleteOp, posOp) || (endDelete != null && JSONHelper.compareNumberArrays(startPos, endDelete) <= 0)) {
                startPos = null; // invalidating the external position
                return null;
            }

            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDelete, endDelete, startPos);
            for (int index = 0; index < posLength; index += 1) {
                startPos.put(index, startPos.getInt(index) - deleteArray.getInt(index));
            }
        }
        return null;
    }

    private static JSONObject handleInsertHeaderInsertHeader(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        String localType = OTUtils.getTypeProperty(localOp);
        String extType = OTUtils.getTypeProperty(extOp);

        String localId = OTUtils.getIdProperty(localOp);
        String extId = OTUtils.getIdProperty(extOp);

        if (localType.equals(extType)) {
            if (localId.equals(extId)) {
                // if the types and the IDs are the same, both operations can be ignored
                OTUtils.setOperationRemoved(extOp);
                OTUtils.setOperationRemoved(localOp);
            } else {
                JSONObject newOperation = OTUtils.cloneJSONObject(localOp);
                OTUtils.setOperationNameDeleteHeaderFooter(newOperation);
                OTUtils.removeType(newOperation);

                externalOpsBefore = new JSONArray();
                externalOpsBefore.put(newOperation);

                OTUtils.setOperationRemoved(localOp);
            }
        } else if (localId.equals(extId)) {
            JSONObject newOperation = OTUtils.cloneJSONObject(localOp);
            OTUtils.setOperationNameDeleteHeaderFooter(newOperation);
            OTUtils.removeType(newOperation);

            externalOpsBefore = new JSONArray();
            externalOpsBefore.put(newOperation);

            OTUtils.setOperationRemoved(localOp);
        }
        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleSetAttrsMove(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject moveOp = null;

        if (OTUtils.isSetAttributesOperation(localOp) || OTUtils.isAnyUpdateFieldOperation(localOp) || OTUtils.isChangeCommentOperation(localOp)) {
            attrsOp = localOp;
            moveOp = extOp;
        } else {
            attrsOp = extOp;
            moveOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startAttrs = OTUtils.getStartPosition(attrsOp);
        final JSONArray endAttrs = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrs, endAttrs };

        int moveStartLength = startMove.length();

        boolean movedAttributes = startAttrs.length() >= moveStartLength && JSONHelper.isEqual(startMove, startAttrs, moveStartLength);

        if (movedAttributes) {
            int index = 0;
            for (JSONArray attrsPos : allPos) {
                if (attrsPos != null) {
                    JSONArray newAttrsPos = JSONHelper.appendArray(JSONHelper.clonePosition(toMove), attrsPos, moveStartLength);
                    if (index == 0) {
                        OTUtils.setStartPosition(attrsOp, newAttrsPos);
                    } else if (index == 1) {
                        OTUtils.setEndPosition(attrsOp, newAttrsPos);
                    }
                }
                index++;
            }
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(startMove));
            TransformHandlerBasic.handleSetAttrsDelete(simpleDeleteOp, attrsOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(toMove));
            TransformHandlerBasic.handleSetAttrsInsertChar(simpleInsertOp, attrsOp);
        }
        return null;
    }

    private static JSONObject handleMoveInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            insertOp = extOp;
        } else {
            moveOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startInsert = OTUtils.getStartPosition(insertOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int insertLength = startInsert.length();

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        int insertTextLength = 1;
        int textIndex = 0;

        // insertChar influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (insertLength <= movePos.length() && JSONHelper.isEqual(startInsert, movePos, insertLength - 1)) {
                    textIndex = insertLength - 1;
                    if (startInsert.getInt(textIndex) <= movePos.getInt(textIndex)) { // insert shifts the move operation
                        if (OTUtils.isInsertTextOperation(insertOp)) {
                            insertTextLength = OTUtils.getText(insertOp).length();
                        }
                        movePos.put(textIndex, movePos.getInt(textIndex) + insertTextLength);
                    }
                }
            }
        }

        if (insertLength > moveStartLength && JSONHelper.isEqual(origStartMove, startInsert, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startInsert, moveStartLength); // modifies the JSONArray origStartMove
            OTUtils.setStartPosition(insertOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            TransformHandlerBasic.handleInsertCharDelete(insertOp, simpleDeleteOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            TransformHandlerBasic.handleInsertCharInsertChar(insertOp, simpleInsertOp);
            // -> Info: On server side the two operations for handleInsertCharInsertChar are in different order
            // This is relevant, if the 'to' position of the move operation is identical with the insert position of the insert operation
        }
        return null;
    }

    private static JSONObject handleMoveInsertParagraph(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            insertOp = extOp;
        } else {
            moveOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startInsert = OTUtils.getStartPosition(insertOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int insertLength = startInsert.length();
        int paraIndex = insertLength - 1;

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        // insertParagraph/insertTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startInsert, movePos) <= 0) {
                    if (movePos.length() >= insertLength && JSONHelper.isEqual(startInsert, movePos, insertLength - 1)) {
                        if (movePos.getInt(paraIndex) >= startInsert.getInt(paraIndex)) { // insert shifts the move operation
                            movePos.put(paraIndex, movePos.getInt(paraIndex) + 1);
                        }
                    }
                }
            }
        }

        if (insertLength > moveStartLength && JSONHelper.isEqual(origStartMove, startInsert, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startInsert, moveStartLength);
            OTUtils.setStartPosition(insertOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            TransformHandlerBasic.handleInsertParaDelete(simpleDeleteOp, insertOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            TransformHandlerBasic.handleInsertParaInsertChar(simpleInsertOp, insertOp);
        }
        return null;
    }

    private static JSONObject handleParaMergeMove(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject mergeOp = null;

        JSONObject result = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            mergeOp = extOp;
        } else {
            moveOp = extOp;
            mergeOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startMerge = OTUtils.getStartPosition(mergeOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int mergeLength = startMerge.length();

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        // mergeParagraph/mergeTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startMerge, movePos) <= 0) {
                    if (movePos.length() >= mergeLength && JSONHelper.isEqual(startMerge, movePos, mergeLength - 1)) {
                        int paraIndex = mergeLength - 1;
                        int textIndex = paraIndex + 1;
                        if (startMerge.getInt(paraIndex) < movePos.getInt(paraIndex) - 1) { // merge shifts the move operation
                            movePos.put(paraIndex, movePos.getInt(paraIndex) - 1);
                        } else if (startMerge.getInt(paraIndex) == movePos.getInt(paraIndex) - 1) {
                            String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);
                            movePos.put(paraIndex, movePos.getInt(paraIndex) - 1);
                            movePos.put(textIndex, movePos.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                        }
                    }
                }
            }
        }

        if (mergeLength > moveStartLength && JSONHelper.isEqual(origStartMove, startMerge, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startMerge, moveStartLength);
            OTUtils.setStartPosition(mergeOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            result = TransformHandlerBasic.handleParaMergeDelete(simpleDeleteOp, mergeOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            TransformHandlerBasic.handleParaMergeInsertChar(simpleInsertOp, mergeOp);
        }
        return result;
    }

    private static JSONObject handleParaSplitMove(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject splitOp = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            splitOp = extOp;
        } else {
            moveOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startSplit = OTUtils.getStartPosition(splitOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int splitLength = startSplit.length();

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        // mergeParagraph/mergeTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startSplit, movePos) <= 0) {
                    if (movePos.length() >= splitLength && JSONHelper.isEqual(startSplit, movePos, splitLength - 2)) {
                        int textIndex = splitLength - 1;
                        int paraIndex = textIndex - 1;
                        if (startSplit.getInt(paraIndex) < movePos.getInt(paraIndex)) { // merge shifts the move operation
                            movePos.put(paraIndex, movePos.getInt(paraIndex) + 1);
                        } else if (startSplit.getInt(paraIndex) == movePos.getInt(paraIndex)) {
                            if (startSplit.getInt(textIndex) <= movePos.getInt(textIndex)) {
                                movePos.put(paraIndex, movePos.getInt(paraIndex) + 1);
                                movePos.put(textIndex, movePos.getInt(textIndex) - startSplit.getInt(textIndex));
                            }
                        }
                    }
                }
            }
        }

        if (splitLength > moveStartLength && JSONHelper.isEqual(origStartMove, startSplit, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startSplit, moveStartLength);
            OTUtils.setStartPosition(splitOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            TransformHandlerBasic.handleParaSplitDelete(simpleDeleteOp, splitOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            if (OTUtils.isSplitParagraphOperation(splitOp)) {
                TransformHandlerBasic.handleParaSplitInsertChar(simpleInsertOp, splitOp);
            } else {
                TransformHandlerBasic.handleTableSplitInsertChar(simpleInsertOp, splitOp);
            }
        }
        return null;
    }

    private static JSONObject handleMoveMove(JSONObject localOp, JSONObject extOp) throws JSONException {

        // this is the client side code, that is executed on server side.
        // The simplest solution is to exchange the operations.
        JSONObject tempObject = localOp;
        localOp = extOp;
        extOp = tempObject;

        // Scenario:
        // The localOp (one of the pending Ops): move [5,0] to [3,0]
        // The external Op (from the client) is: move [5,0] to [4,0]
        // On server side the local Op must be removed and the external must be a move from [3,0] to [4,0].
        // On the client side (where the drawing was already moved to [4,0] the external operation
        // will be ignored (and if the move-Operation was not already sent to the server, the operation
        // move [5,0] to [4,0] will be modified to move [3,0] to [4,0] before sending it to the server).

        JSONObject origLocalOp = OTUtils.cloneJSONObject(localOp);

        final JSONArray localStart = OTUtils.getStartPosition(localOp);
        final JSONArray localEnd = OTUtils.optEndPosition(localOp);
        final JSONArray localTo = OTUtils.getToPosition(localOp);

        JSONArray[] localAllPos = { localStart, localEnd, localTo };

        final JSONArray extStart = OTUtils.getStartPosition(extOp);
        final JSONArray extEnd = OTUtils.optEndPosition(extOp);
        final JSONArray extTo = OTUtils.getToPosition(extOp);

        JSONArray[] extAllPos = { extStart, extEnd, extTo };

        if (JSONHelper.isEqual(localStart, extStart) && JSONHelper.isEqual(localEnd, extEnd)) {
            if (JSONHelper.isEqual(localTo, extTo)) {
                OTUtils.setOperationRemoved(extOp); // setting marker at external operation
                OTUtils.setOperationRemoved(localOp); // setting marker at local operation
            } else {
                // different destination for the same drawing
                OTUtils.setOperationRemoved(extOp); // setting marker at external operation
                OTUtils.setStartPosition(localOp, JSONHelper.clonePosition(extTo));
                OTUtils.setEndPosition(localOp, JSONHelper.clonePosition(extTo));
            }
        } else {
            for (JSONArray localMovePos : localAllPos) {
                if (localMovePos != null) {
                    JSONObject simpleLocalPosition = new JSONObject();
                    OTUtils.setStartPosition(simpleLocalPosition, localMovePos);

                    JSONObject simpleDeleteOp = new JSONObject();
                    OTUtils.setOperationNameDelete(simpleDeleteOp);
                    OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(extStart));
                    handleDeleteExtPosition(simpleDeleteOp, simpleLocalPosition);

                    JSONObject simpleInsertOp = new JSONObject();
                    OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
                    OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(extTo));
                    handleInsertCharExtPosition(simpleInsertOp, simpleLocalPosition);
                }
            }

            for (JSONArray extMovePos : extAllPos) {
                if (extMovePos != null) {
                    JSONObject simpleExtPosition = new JSONObject();
                    OTUtils.setStartPosition(simpleExtPosition, extMovePos);

                    JSONObject simpleDeleteOp = new JSONObject();
                    OTUtils.setOperationNameDelete(simpleDeleteOp);
                    OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(OTUtils.getStartPosition(origLocalOp)));
                    handleDeleteExtPosition(simpleDeleteOp, simpleExtPosition);

                    JSONObject simpleInsertOp = new JSONObject();
                    OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
                    OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(OTUtils.getToPosition(origLocalOp)));
                    handleInsertCharExtPosition(simpleInsertOp, simpleExtPosition);
                }
            }
        }
        return null;
    }

    private static JSONObject handleMoveInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            insertOp = extOp;
        } else {
            moveOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startInsert = OTUtils.getStartPosition(insertOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int insertLength = startInsert.length();
        int rowIndex = insertLength - 1;

        int rowCount = OTUtils.getCountProperty(insertOp, 1);

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        // insertParagraph/insertTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startInsert, movePos) <= 0) {
                    if (movePos.length() >= insertLength && JSONHelper.isEqual(startInsert, movePos, insertLength - 1)) {
                        if (movePos.getInt(rowIndex) >= startInsert.getInt(rowIndex)) { // insert shifts the move operation
                            movePos.put(rowIndex, movePos.getInt(rowIndex) + rowCount);
                        }
                    }
                }
            }
        }

        if (insertLength > moveStartLength && JSONHelper.isEqual(origStartMove, startInsert, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startInsert, moveStartLength);
            OTUtils.setStartPosition(insertOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            TransformHandlerBasic.handleInsertRowsDelete(simpleDeleteOp, insertOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            TransformHandlerBasic.handleInsertRowsInsertChar(simpleInsertOp, insertOp);
        }
        return null;
    }

    private static JSONObject handleMoveInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            insertOp = extOp;
        } else {
            moveOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startInsert = OTUtils.getStartPosition(insertOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int insertLength = startInsert.length();
        int tableIndex = 0;
        int columnIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertOp);

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);

        // insertParagraph/insertTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startInsert, movePos) <= 0) {
                    if (movePos.length() > insertLength && JSONHelper.isEqual(startInsert, movePos, insertLength)) {
                        tableIndex = insertLength - 1;
                        columnIndex = tableIndex + 2;
                        if ((movePos.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertOp)) || (OTUtils.isInsertModeBefore(mode) && movePos.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertOp))) { // insert shifts the move operation
                            movePos.put(columnIndex, movePos.getInt(columnIndex) + 1);
                        }
                    }
                }
            }
        }

        if (insertLength > moveStartLength && JSONHelper.isEqual(origStartMove, startInsert, moveStartLength)) {
            JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startInsert, moveStartLength);
            OTUtils.setStartPosition(insertOp, newStartInsert);
        } else {
            JSONObject simpleDeleteOp = new JSONObject();
            OTUtils.setOperationNameDelete(simpleDeleteOp);
            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
            TransformHandlerBasic.handleInsertColumnDelete(simpleDeleteOp, insertOp);

            JSONObject simpleInsertOp = new JSONObject();
            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
            TransformHandlerBasic.handleInsertColumnInsertChar(simpleInsertOp, insertOp);
        }
        return null;
    }

    private static JSONObject handleMoveDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject deleteOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            deleteOp = extOp;
        } else {
            moveOp = extOp;
            deleteOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startDelete = OTUtils.getStartPosition(deleteOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int deleteLength = startDelete.length();
        int tableIndex = 0;
        int columnIndex = 0;
        int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;

        boolean moveStartDeleted = false;
        boolean moveEndDeleted = false;

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);
        int index = 0;

        // insertParagraph/insertTable influences the move operation
        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                if (JSONHelper.compareNumberArrays(startDelete, movePos) <= 0) {
                    if (movePos.length() > deleteLength + 1 && JSONHelper.isEqual(startDelete, movePos, deleteLength)) {
                        tableIndex = deleteLength - 1;
                        columnIndex = tableIndex + 2;
                        if (movePos.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                            if (movePos.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                                movePos.put(columnIndex, movePos.getInt(columnIndex) - deleteColumns);
                            } else {
                                if (index == 2) {
                                    moveEndDeleted = true;
                                } else {
                                    moveStartDeleted = true;
                                }
                            }
                        }
                    }
                }
            }
            index++;
        }

        if (moveStartDeleted && moveEndDeleted) {
            OTUtils.setOperationRemoved(moveOp);
        } else if (moveStartDeleted || moveEndDeleted) {
            OTUtils.setOperationRemoved(moveOp); // moveOp cannot be executed anymore and will be replaced by a delete operation
            JSONObject newOperation = OTUtils.cloneJSONObject(deleteOp);
            OTUtils.setOperationNameDelete(newOperation);
            OTUtils.removeStartGridProperty(newOperation);
            OTUtils.removeEndGridProperty(newOperation);
            if (moveStartDeleted) {
                OTUtils.setStartPosition(newOperation, JSONHelper.clonePosition(toMove));
                OTUtils.setEndPosition(newOperation, JSONHelper.clonePosition(toMove));
            } else {
                OTUtils.setStartPosition(newOperation, JSONHelper.clonePosition(startMove));
                OTUtils.setEndPosition(newOperation, JSONHelper.clonePosition(startMove));
            }

            if ((OTUtils.isMoveOperation(extOp) && moveStartDeleted) || (OTUtils.isMoveOperation(localOp) && moveEndDeleted)) {
                localOpsAfter = new JSONArray();
                localOpsAfter.put(newOperation); // inserting behind the current local operation
            } else {
                externalOpsAfter = new JSONArray();
                externalOpsAfter.put(newOperation); // inserting behind the current external operation
            }

        }

        if (!OTUtils.isOperationRemoved(moveOp)) {
            if (deleteLength > moveStartLength && JSONHelper.isEqual(origStartMove, startDelete, moveStartLength)) {
                JSONArray newStartInsert = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startDelete, moveStartLength);
                OTUtils.setStartPosition(deleteOp, newStartInsert);
            } else {
                JSONObject simpleDeleteOp = new JSONObject();
                OTUtils.setOperationNameDelete(simpleDeleteOp);
                OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
                TransformHandlerBasic.handleDeleteColumnsDelete(simpleDeleteOp, deleteOp);

                JSONObject simpleInsertOp = new JSONObject();
                OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
                OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
                TransformHandlerBasic.handleDeleteColumnsInsertChar(simpleInsertOp, deleteOp);
            }
        }
        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleMoveDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject moveOp = null;
        JSONObject deleteOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isMoveOperation(localOp)) {
            moveOp = localOp;
            deleteOp = extOp;
        } else {
            moveOp = extOp;
            deleteOp = localOp;
        }

        final JSONArray startMove = OTUtils.getStartPosition(moveOp);
        JSONArray endMove = OTUtils.optEndPosition(moveOp);
        final JSONArray toMove = OTUtils.getToPosition(moveOp);
        final JSONArray startDelete = OTUtils.getStartPosition(deleteOp);
        JSONArray endDelete = OTUtils.optEndPosition(deleteOp);

        JSONArray[] allPos = { startMove, endMove, toMove };

        int moveStartLength = startMove.length();
        int deleteLength = startDelete.length();

        boolean moveStartDeleted = false;
        boolean moveEndDeleted = false;

        boolean deleteHasRange = endDelete != null && !JSONHelper.isEqual(startDelete, endDelete);
        boolean moveToPositionHandled = false;

        JSONObject origMoveOp = OTUtils.cloneJSONObject(moveOp);
        final JSONArray origStartMove = OTUtils.getStartPosition(origMoveOp);
        final JSONArray origToMove = OTUtils.getToPosition(origMoveOp);
        int idx = 0;

        for (JSONArray movePos : allPos) {
            if (movePos != null) {
                boolean deletedElement = idx < 2 && !deleteHasRange && JSONHelper.isEqual(startDelete, movePos);
                JSONObject moveOpHelper = new JSONObject();
                OTUtils.setStartPosition(moveOpHelper, JSONHelper.clonePosition(movePos));

                if (JSONHelper.compareNumberArrays(movePos, startDelete) < 0 || (JSONHelper.compareNumberArrays(movePos, startDelete) == 0 && !deletedElement)) {
                    if (idx != 1 && !moveToPositionHandled) {
                        if (deleteLength > moveStartLength && JSONHelper.isEqual(origStartMove, startDelete, moveStartLength)) {
                            JSONArray newStartDelete = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), startDelete, moveStartLength);
                            OTUtils.setStartPosition(deleteOp, newStartDelete);
                            if (endDelete != null) {
                                JSONArray newEndDelete = JSONHelper.appendArray(JSONHelper.clonePosition(origToMove), endDelete, moveStartLength);
                                OTUtils.setEndPosition(deleteOp, newEndDelete);
                            }
                            moveToPositionHandled = true;
                        } else {
                            JSONObject simpleDeleteOp = new JSONObject();
                            OTUtils.setOperationNameDelete(simpleDeleteOp);
                            OTUtils.setStartPosition(simpleDeleteOp, JSONHelper.clonePosition(origStartMove));
                            TransformHandlerBasic.handleDeleteDelete(simpleDeleteOp, deleteOp);

                            JSONObject simpleInsertOp = new JSONObject();
                            OTUtils.setOperationNameInsertDrawing(simpleInsertOp);
                            OTUtils.setStartPosition(simpleInsertOp, JSONHelper.clonePosition(origToMove));
                            TransformHandlerBasic.handleInsertCharDelete(simpleInsertOp, deleteOp);
                        }
                    }
                } else if (deletedElement || JSONHelper.ancestorRemoved(deleteOp, moveOpHelper) || (endDelete != null && JSONHelper.compareNumberArrays(movePos, endDelete) <= 0)) {
                    if (endDelete != null && movePos.length() == endDelete.length()) {
                        int endDeleteLength = endDelete.length();
                        if (idx == 2) {
                            endDelete.put(endDeleteLength - 1, endDelete.getInt(endDeleteLength - 1) + 1);
                        }
                        if (idx == 0) {
                            endDelete.put(endDeleteLength - 1, endDelete.getInt(endDeleteLength - 1) - 1);
                        }
                    }
                    OTUtils.setOperationRemoved(moveOp);
                    if (idx == 2) {
                        moveEndDeleted = true;
                    } else {
                        moveStartDeleted = true;
                    }
                } else {

                    JSONArray endDeletePosition = null;
                    if (endDelete != null) {
                        endDeletePosition = endDelete;
                    } else {
                        endDeletePosition = startDelete;
                    }

                    JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDelete, endDeletePosition, movePos);
                    for (int index = 0; index < movePos.length(); index += 1) {
                        movePos.put(index, movePos.getInt(index) - deleteArray.getInt(index));
                    }
                }
            }
            idx++;
        }

        if (moveStartDeleted && moveEndDeleted) {
            OTUtils.setOperationRemoved(moveOp);
        } else if (moveStartDeleted || moveEndDeleted) {
            OTUtils.setOperationRemoved(moveOp);
            JSONObject newOperation = OTUtils.cloneJSONObject(deleteOp);
            OTUtils.setOperationNameDelete(newOperation);
            if (moveStartDeleted) {
                OTUtils.setStartPosition(newOperation, JSONHelper.clonePosition(toMove));
                OTUtils.setEndPosition(newOperation, JSONHelper.clonePosition(toMove));
            } else {
                OTUtils.setStartPosition(newOperation, JSONHelper.clonePosition(startMove));
                OTUtils.setEndPosition(newOperation, JSONHelper.clonePosition(startMove));
            }

            // the deleteOp can be ignored, too, if only the moved drawing was deleted
            if (moveStartDeleted && !deleteHasRange && JSONHelper.isEqual(startDelete, startMove)) {
                OTUtils.setOperationRemoved(deleteOp);
            }

            if ((OTUtils.isMoveOperation(extOp) && moveStartDeleted) || (OTUtils.isMoveOperation(localOp) && moveEndDeleted)) {
                localOpsAfter = new JSONArray();
                localOpsAfter.put(newOperation); // inserting behind the current local operation
            } else {
                externalOpsAfter = new JSONArray();
                externalOpsAfter.put(newOperation); // inserting behind the current external operation
            }
        }
        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleChangeCommentChangeComment(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONArray startExtOp =OTUtils.getStartPosition(extOp);
        JSONArray startLocalOp =OTUtils.getStartPosition(localOp);

        if (JSONHelper.isEqual(startLocalOp, startExtOp)) {
            OTUtils.setOperationRemoved(localOp);
        }

        return null;
    }

    private static JSONObject handleUpdateComplexFieldUpdateComplexField(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONArray startExtOp =OTUtils.getStartPosition(extOp);
        JSONArray startLocalOp =OTUtils.getStartPosition(localOp);

        if (JSONHelper.isEqual(startLocalOp, startExtOp)) {

            String localInstruction = OTUtils.getInstructionProperty(localOp);
            String extInstruction = OTUtils.getInstructionProperty(extOp);

            if (localInstruction.equals(extInstruction)) {
                // if the instructions are the same, both operations can be ignored
                OTUtils.setOperationRemoved(extOp);
                OTUtils.setOperationRemoved(localOp);
            } else {
                // different instructions: always applying external operation
                // Info: on client side the external operation is ignored.
                OTUtils.setOperationRemoved(localOp);
            }
        }

        return null;
    }

    private static JSONObject handleInsertListStyleInsertListStyle(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.getListStyleIdProperty(localOp).equals(OTUtils.getListStyleIdProperty(extOp))) {
            OTUtils.setOperationRemoved(localOp); // on client side, the external operation is ignored
        }

        return null;
    }

    private static JSONObject handleInsertListStyleDeleteListStyle(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.getListStyleIdProperty(localOp).equals(OTUtils.getListStyleIdProperty(extOp))) {
            // removing the deleteListStyle operation to avoid superfluous problems, when
            // paragraphs get assigned a new listStyle in setAttributes operation after
            // the insertListStyle operation.
            if (OTUtils.isDeleteListStyleOperation(localOp)) {
                OTUtils.setOperationRemoved(localOp);
            } else {
                OTUtils.setOperationRemoved(extOp);
            }
        }

        return null;
    }

    private static JSONObject handleDeleteListStyleDeleteListStyle(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.getListStyleIdProperty(localOp).equals(OTUtils.getListStyleIdProperty(extOp))) {
            OTUtils.setOperationRemoved(localOp);
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }
}
