/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.tools.ITransformHandler;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.tools.OpPair;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.ot.tools.OTUtils;

public class TransformHandlerBasic implements ITransformHandlerMap {

    public static OCValue[] opsAutoStyle = {
        OCValue.INSERT_AUTO_STYLE,
        OCValue.DELETE_AUTO_STYLE,
        OCValue.CHANGE_AUTO_STYLE
    };

    public static OCValue[] opsChartSeries = {
        OCValue.INSERT_CHART_SERIES,
        OCValue.DELETE_CHART_SERIES,
        OCValue.CHANGE_CHART_SERIES
    };

    public static OCValue[] opsChartComp = {
        OCValue.CHANGE_CHART_AXIS,
        OCValue.CHANGE_CHART_GRID,
        OCValue.CHANGE_CHART_TITLE,
        OCValue.CHANGE_CHART_LEGEND
    };

    public static OCValue[][] opsChart = {
        opsChartSeries,
        { OCValue.DELETE_CHART_AXIS },  // TODO: operation deleteChartAxis is not implemented in filter
        opsChartComp
    };

    public static OCValue[] opsIgnorable = {
        OCValue.CHANGE_STYLE_SHEET,
        OCValue.DELETE_STYLE_SHEET,
        OCValue.INSERT_THEME,
        OCValue.INSERT_FONT_DESCRIPTION,
        OCValue.INSERT_STYLE_SHEET,
        OCValue.SET_DOCUMENT_ATTRIBUTES,
        OCValue.NO_OP
    };

    public static OCValue[] opsInsertChar = {
        OCValue.INSERT_BOOKMARK,
        OCValue.INSERT_FIELD,
        OCValue.INSERT_HARD_BREAK,
        OCValue.INSERT_RANGE,
        OCValue.INSERT_TAB,
        OCValue.INSERT_TEXT
    };

    public static OCValue[] opsInsertComp = {
        OCValue.INSERT_PARAGRAPH,
        OCValue.INSERT_TABLE
    };

    public static OCValue[] opsMergeComp = {
        OCValue.MERGE_PARAGRAPH,
        OCValue.MERGE_TABLE
    };

    public static OCValue[] opsSplitComp = {
        OCValue.SPLIT_PARAGRAPH,
        OCValue.SPLIT_TABLE
    };

    @Override
    public Map<OpPair, ITransformHandler> getTransformHandlerMap() {
        return opPairMap;
    }

    @Override
    public Set<OCValue> getSupportedOperations() {
        return supOpsSet;
    }

    @Override
    public Set<OCValue> getIgnoreOperations() {
        return ignoreOpsSet;
    }

    static final public ImmutableSet<OCValue> ignoreOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.NO_OP)
        .add(OCValue.SET_DOCUMENT_ATTRIBUTES)
        .add(OCValue.INSERT_FONT_DESCRIPTION)
        .add(OCValue.INSERT_THEME)
        .add(OCValue.INSERT_STYLE_SHEET)
        .add(OCValue.DELETE_STYLE_SHEET)
        .add(OCValue.CHANGE_STYLE_SHEET)
        //
        .add(OCValue.SET_ATTRIBUTES)
        .add(OCValue.DELETE_CHART_AXIS)
        .add(OCValue.CHANGE_CHART_AXIS)
        .add(OCValue.CHANGE_CHART_GRID)
        .add(OCValue.CHANGE_CHART_TITLE)
        .add(OCValue.CHANGE_CHART_LEGEND)
        .add(OCValue.CHANGE_CHART_SERIES)
        .add(OCValue.DELETE_CHART_AXIS)
        .add(OCValue.DELETE_CHART_SERIES)
        .add(OCValue.INSERT_CHART_SERIES)
        .add(OCValue.INSERT_AUTO_STYLE)
        .add(OCValue.DELETE_AUTO_STYLE)
        .add(OCValue.CHANGE_AUTO_STYLE)
        .build();

    static final public ImmutableSet<OCValue> supOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.CHANGE_AUTO_STYLE)
        .add(OCValue.CHANGE_CHART_AXIS)
        .add(OCValue.CHANGE_CHART_GRID)
        .add(OCValue.CHANGE_CHART_TITLE)
        .add(OCValue.CHANGE_CHART_LEGEND)
        .add(OCValue.CHANGE_CHART_SERIES)
        .add(OCValue.CHANGE_STYLE_SHEET)
        .add(OCValue.DELETE)
        .add(OCValue.DELETE_AUTO_STYLE)
        .add(OCValue.DELETE_CHART_AXIS)
        .add(OCValue.DELETE_CHART_SERIES)
        .add(OCValue.DELETE_COLUMNS)
        .add(OCValue.DELETE_STYLE_SHEET)
        .add(OCValue.INSERT_AUTO_STYLE)
        .add(OCValue.INSERT_BOOKMARK)
        .add(OCValue.INSERT_CELLS)
        .add(OCValue.INSERT_CHART_SERIES)
        .add(OCValue.INSERT_COLUMN)
        .add(OCValue.INSERT_DRAWING)
        .add(OCValue.INSERT_FIELD)
        .add(OCValue.INSERT_HARD_BREAK)
        .add(OCValue.INSERT_PARAGRAPH)
        .add(OCValue.INSERT_RANGE)
        .add(OCValue.INSERT_ROWS)
        .add(OCValue.INSERT_STYLE_SHEET)
        .add(OCValue.INSERT_TAB)
        .add(OCValue.INSERT_TABLE)
        .add(OCValue.INSERT_TEXT)
        .add(OCValue.INSERT_THEME)
        .add(OCValue.MERGE_PARAGRAPH)
        .add(OCValue.MERGE_TABLE)
        .add(OCValue.NO_OP)
        .add(OCValue.SET_ATTRIBUTES)
        .add(OCValue.SET_DOCUMENT_ATTRIBUTES)
        .add(OCValue.SPLIT_PARAGRAPH)
        .add(OCValue.SPLIT_TABLE)
        .add(OCValue.UPDATE_FIELD)
        .build();

    static final public ImmutableMap<OpPair, ITransformHandler> opPairMap;

    static {

        final Map<OpPair, ITransformHandler> map = new HashMap<OpPair, ITransformHandler>();
        final Set<OpPair> aliases = new HashSet<OpPair>();
        // changeConfig
        OTUtils.registerSelfTransformation(map, aliases, OCValue.SET_DOCUMENT_ATTRIBUTES, (options, localOp, externOp) -> transform_changeConfig_changeConfig(localOp, externOp));

        // insertStyleSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_STYLE_SHEET, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_STYLE_SHEET, OCValue.DELETE_STYLE_SHEET, (options, localOp, externOp) -> transform_insertStyleSheet_deleteStyleSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_STYLE_SHEET, OCValue.CHANGE_STYLE_SHEET, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());

        // deleteStyleSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_STYLE_SHEET, (options, localOp, externOp) -> transform_deleteStyleSheet_deleteStyleSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_STYLE_SHEET, OCValue.CHANGE_STYLE_SHEET, (options, localOp, externOp) -> transform_insertStyleSheet_deleteStyleSheet(localOp, externOp));

        // changeStyleSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_STYLE_SHEET, (options, localOp, externOp) -> transform_changeStyleSheet_changeStyleSheet(localOp, externOp));

        // auto-styles
        OTUtils.registerBidiTransformation(map, aliases, opsAutoStyle, opsChart, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());

        // insertAutoStyle
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, (options, localOp, externOp) -> transform_insertAutoStyle_insertAutoStyle(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.DELETE_AUTO_STYLE, (options, localOp, externOp) -> transform_insertAutoStyle_deleteAutoStyle(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.CHANGE_AUTO_STYLE, (options, localOp, externOp) -> transform_insertAutoStyle_changeAutoStyle(localOp, externOp));

        // deleteAutoStyle
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, (options, localOp, externOp) -> transform_deleteAutoStyle_deleteAutoStyle(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.CHANGE_AUTO_STYLE, (options, localOp, externOp) -> transform_deleteAutoStyle_changeAutoStyle(localOp, externOp));

        // changeAutoStyle
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_AUTO_STYLE, (options, localOp, externOp) -> transform_changeAutoStyle_changeAutoStyle(localOp, externOp));

        // chart series
        OTUtils.registerBidiTransformation(map, aliases, opsChartSeries, OCValue.DELETE_CHART_AXIS, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsChartSeries, opsChartComp, (options, localOp, externOp) -> TransformHandlerBasic.handleNothing());

        // insertChartSeries
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_CHART_SERIES, (options, localOp, externOp) -> transform_insertChartSeries_insertChartSeries(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CHART_SERIES, OCValue.DELETE_CHART_SERIES, (options, localOp, externOp) -> transform_insertChartSeries_deleteChartSeries(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CHART_SERIES, OCValue.CHANGE_CHART_SERIES, (options, localOp, externOp) -> transform_insertChartSeries_changeChartSeries(localOp, externOp));

        // deleteChartSeries
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_CHART_SERIES, (options, localOp, externOp) -> transform_deleteChartSeries_deleteChartSeries(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_CHART_SERIES, OCValue.CHANGE_CHART_SERIES, (options, localOp, externOp) -> transform_deleteChartSeries_changeChartSeries(localOp, externOp));

        // changeChartSeries
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_CHART_SERIES, (options, localOp, externOp) -> transform_changeChartSeries_changeChartSeries(localOp, externOp));

        // deleteChartAxis
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_CHART_AXIS, (options, localOp, externOp) -> transform_deleteChartAxis_deleteChartAxis(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_CHART_AXIS, opsChartComp, (options, localOp, externOp) -> transform_deleteChartAxis_changeChartComp(localOp, externOp));

        // changeChartComp
        OTUtils.registerSelfTransformation(map, aliases, opsChartComp, (options, localOp, externOp) -> transform_changeChartComp_changeChartComp(localOp, externOp));

        // ALIAS_IGNORABLE
        OTUtils.registerSelfTransformation(map, aliases, opsIgnorable, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, opsInsertChar, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, opsInsertComp, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, opsMergeComp, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.DELETE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsIgnorable, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleNothing());

        // ALIAS_INSERT_CHAR
        OTUtils.registerSelfTransformation(map, aliases, opsInsertChar, (options, localOp, externOp) -> handleInsertCharInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, opsInsertComp, (options, localOp, externOp) -> handleInsertParaInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, opsMergeComp, (options, localOp, externOp) -> handleParaMergeInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.DELETE, (options, localOp, externOp) -> handleInsertCharDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleDeleteColumnsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertRowsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertColumnInsertChar(localOp, externOp));
        OTUtils.registerTransformation(map, aliases, opsInsertChar, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertCharInsertChar(localOp, externOp), false);
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertRowsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleParaSplitInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleTableSplitInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertChar, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertChar(localOp, externOp));

        // ALIAS_INSERT_COMP
        OTUtils.registerSelfTransformation(map, aliases, opsInsertComp, (options, localOp, externOp) -> handleInsertParaInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, opsMergeComp, (options, localOp, externOp) -> handleMergeParaInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.DELETE, (options, localOp, externOp) -> handleInsertParaDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleInsertParaDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertParaInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertParaInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertParaInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertParaInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleInsertParaSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleTableSplitInsertPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsInsertComp, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertPara(localOp, externOp));
        // ALIAS_MERGE_COMP
        OTUtils.registerSelfTransformation(map, aliases, opsMergeComp, (options, localOp, externOp) -> handleParaMergeParaMerge(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.DELETE, (options, localOp, externOp) -> handleParaMergeDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleMergeParaDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleMergeParaInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleMergeParaInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleParaMergeInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleMergeParaInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsParaMerge(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleParaSplitParaMerge(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleParaSplitParaMerge(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsMergeComp, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsParaMerge(localOp, externOp));
        // DELETE
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE, (options, localOp, externOp) -> handleDeleteDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleDeleteColumnsDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertRowsDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertColumnDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertCharDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertRowsDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleParaSplitDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleParaSplitDelete(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsDelete(localOp, externOp));
        // DELETE COLUMNS
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> handleDeleteColumnsDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertCellsDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertColumnDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleDeleteColumnsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleDeleteColumnsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsDeleteColumns(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleDeleteColumnsSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleDeleteColumnsSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsDeleteColumns(localOp, externOp));
        // INSERT_CELLS
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_CELLS, (options, localOp, externOp) -> handleInsertRowsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertCellsInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertRowsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertCellsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleInsertRowsSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleInsertRowsSplitTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CELLS, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertRows(localOp, externOp));
        // INSERT_COLUMN
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_COLUMN, (options, localOp, externOp) -> handleInsertColumnInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertColumnInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertRowsInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertColumn(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleInsertColumnSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleInsertColumnSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMN, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertColumn(localOp, externOp));
        // INSERT_DRAWING
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> handleInsertCharInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertRowsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleParaSplitInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleTableSplitInsertChar(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertChar(localOp, externOp));
        OTUtils.registerTransformation(map, aliases, OCValue.INSERT_DRAWING, opsInsertChar, (options, localOp, externOp) -> handleInsertCharInsertChar(localOp, externOp), false);
        // INSERT_ROWS
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_ROWS, (options, localOp, externOp) -> handleInsertRowsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsInsertRows(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleInsertRowsSplitPara(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleInsertRowsSplitTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsInsertRows(localOp, externOp));
        // SET_ATTRIBUTES
        OTUtils.registerSelfTransformation(map, aliases, OCValue.SET_ATTRIBUTES, (options, localOp, externOp) -> handleSetAttrsSetAttrs(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SET_ATTRIBUTES, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleSetAttrsParaSplit(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SET_ATTRIBUTES, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleSetAttrsSplitTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SET_ATTRIBUTES, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleNothing());

        // SPLIT_PARAGRAPUH
        OTUtils.registerSelfTransformation(map, aliases, OCValue.SPLIT_PARAGRAPH, (options, localOp, externOp) -> handleParaSplitParaSplit(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SPLIT_PARAGRAPH, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleSplitParaSplitTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SPLIT_PARAGRAPH, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsParaSplit(localOp, externOp));
        // SPLIT_TABLE
        OTUtils.registerSelfTransformation(map, aliases, OCValue.SPLIT_TABLE, (options, localOp, externOp) -> handleSplitTableSplitTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.SPLIT_TABLE, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleSetAttrsSplitTable(localOp, externOp));
        // UPDATE_FIELD
        OTUtils.registerSelfTransformation(map, aliases, OCValue.UPDATE_FIELD, (options, localOp, externOp) -> handleUpdateFieldUpdateField(localOp, externOp));

        opPairMap = new ImmutableMap.Builder<OpPair, ITransformHandler>()
        .putAll(map)
        .build();
    }

    // changeConfig -----------------------------------------------------------

    private static JSONObject transform_changeConfig_changeConfig(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject reducedExtAttrs = OTUtils.getAttributes(extOp);
        JSONObject reducedLocalAttrs = OTUtils.getAttributes(localOp);

        JSONHelper.reduceAttributeSet(reducedExtAttrs, reducedLocalAttrs, true);

        // ignore the entire operation, if all changes have been discarded
        if (reducedExtAttrs.isEmpty()) { OTUtils.setOperationRemoved(extOp); }
        if (reducedLocalAttrs.isEmpty()) { OTUtils.setOperationRemoved(localOp); }

        return null;
    }

    // insertStyleSheet -------------------------------------------------------

    private static JSONObject transform_insertStyleSheet_deleteStyleSheet(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.isSameStyleSheet(localOp, extOp)) {
            if (OTUtils.isDeleteStyleSheetOperation(localOp)) {
                OTUtils.setOperationRemoved(extOp);
            } else {
                OTUtils.setOperationRemoved(localOp);
            }
        }
        return null;
    }

    private static JSONObject transform_deleteStyleSheet_deleteStyleSheet(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.isSameStyleSheet(localOp, extOp)) {
            OTUtils.setOperationRemoved(extOp);
            OTUtils.setOperationRemoved(localOp);
        }

        return null;
    }

    // changeStyleSheet --------------------------------------------------------

    private static JSONObject transform_changeStyleSheet_changeStyleSheet(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        if (OTUtils.isSameStyleSheet(localOp, extOp)) {

            JSONObject reducedExtAttrs = OTUtils.getAttributes(extOp);
            JSONObject reducedLocalAttrs = OTUtils.getAttributes(localOp);

            JSONHelper.reduceAttributeSet(reducedExtAttrs, reducedLocalAttrs, true);

            // ignore the entire operation, if all changes have been discarded
            if (reducedExtAttrs.isEmpty()) { OTUtils.setOperationRemoved(extOp); }
            if (reducedLocalAttrs.isEmpty()) { OTUtils.setOperationRemoved(localOp); }
        }

        return null;
    }

    // insertAutoStyle --------------------------------------------------------

    private static JSONObject transform_insertAutoStyle_insertAutoStyle(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different auto-style types are independent
        final String styleType = getSameAutoStyleType(lclOp, extOp);
        if (null == styleType) {
            return null;
        }

        Integer autostyleIndex = getAutoStyleIndex(lclOp);
        Integer autostyleIndex2 = getAutoStyleIndex(extOp);
        // in freestyle mode, an auto-style cannot be inserted twice
        if (null == autostyleIndex
            || null == autostyleIndex2
            ) {
            final String lclOpStyleId = lclOp.optString(OCKey.STYLE_ID.value());
            final String extOpStyleId = extOp.optString(OCKey.STYLE_ID.value());
            if (null != autostyleIndex || null != autostyleIndex2 || lclOpStyleId.equalsIgnoreCase(extOpStyleId)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "cannot insert auto-style twice"  + lclOp.toString() + "," + extOp.toString());
            }
            return null;
        }

        // transform the auto-style identifier of the local operation first
        transformAutoStyleProperty(lclOp, OCKey.STYLE_ID.value(), styleType, extOp, true);

        // transform the auto-style identifier of the external operation afterwards
        transformAutoStyleProperty(extOp, OCKey.STYLE_ID.value(), styleType, lclOp, true);

        return null;
    }

    private static JSONObject transform_insertAutoStyle_deleteAutoStyle(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // different auto-style types are independent
        final String styleType = getSameAutoStyleType(insertOp, deleteOp);
        if (null == styleType) {
            return null;
        }

        Integer autostyleIndex = getAutoStyleIndex(insertOp);
        Integer autostyleIndex2 = getAutoStyleIndex(deleteOp);
        // in freestyle mode, an auto-style cannot be inserted twice
        if (null == autostyleIndex
            || null == autostyleIndex2
            ) {
            final String lclOpStyleId = insertOp.optString(OCKey.STYLE_ID.value());
            final String extOpStyleId = deleteOp.optString(OCKey.STYLE_ID.value());
            if (null != autostyleIndex || null != autostyleIndex2 || lclOpStyleId.equalsIgnoreCase(extOpStyleId)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "OP ERROR: cannot insert and delete same auto-style"  + insertOp.toString() + "," + deleteOp.toString());
            }
            return null;
        }

        // transform the auto-style identifier of the delete operation first
        transformAutoStyleProperty(deleteOp, OCKey.STYLE_ID.value(), styleType, insertOp, true);

        // transform the auto-style identifier of the insert operation afterwards (will not be deleted)
        transformAutoStyleProperty(insertOp, OCKey.STYLE_ID.value(), styleType, deleteOp, false);
        return null;
    }

    private static JSONObject transform_insertAutoStyle_changeAutoStyle(JSONObject insertOp, JSONObject changeOp) throws JSONException {
     // different auto-style types are independent
        final String styleType = getSameAutoStyleType(insertOp, changeOp);
        if (null == styleType) {
            return null;
        }

        Integer autostyleIndex = getAutoStyleIndex(insertOp);
        Integer autostyleIndex2 = getAutoStyleIndex(changeOp);
        // in freestyle mode, an auto-style cannot be inserted twice
        if (null == autostyleIndex
            || null == autostyleIndex2
            ) {
            final String lclOpStyleId = insertOp.optString(OCKey.STYLE_ID.value());
            final String extOpStyleId = changeOp.optString(OCKey.STYLE_ID.value());
            if (null != autostyleIndex || null != autostyleIndex2 || lclOpStyleId.equalsIgnoreCase(extOpStyleId)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "OP ERROR: cannot insert existing auto-style"  + insertOp.toString() + "," + changeOp.toString());
            }
            return null;
        }

        // transform the auto-style identifier of the change operation
        transformAutoStyleProperty(changeOp, OCKey.STYLE_ID.value(), styleType, insertOp, true);
        return null;
    }

    // deleteAutoStyle --------------------------------------------------------

    private static JSONObject transform_deleteAutoStyle_deleteAutoStyle(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different auto-style types are independent
        final String styleType = getSameAutoStyleType(lclOp, extOp);
        if (null == styleType) {
            return null;
        }

        // ignore multiple delete operations on the same auto-style
        final String lclOpStyleId = lclOp.optString(OCKey.STYLE_ID.value());
        final String extOpStyleId = extOp.optString(OCKey.STYLE_ID.value());
        if (lclOpStyleId.equalsIgnoreCase(extOpStyleId)) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
            return null;
        }

        // transform the auto-style identifiers
        if (!transformAutoStyleProperty(lclOp, OCKey.STYLE_ID.value(), styleType, extOp, false)) {
            transformAutoStyleProperty(extOp, OCKey.STYLE_ID.value(), styleType, lclOp, false);
        }

        return null;
    }

    private static JSONObject transform_deleteAutoStyle_changeAutoStyle(JSONObject deleteOp, JSONObject changeOp) throws JSONException {
        // different auto-style types are independent
        final String styleType = getSameAutoStyleType(deleteOp, changeOp);
        if (null == styleType) {
            return null;
        }

        // ignore multiple delete operations on the same auto-style
        final String lclOpStyleId = deleteOp.optString(OCKey.STYLE_ID.value());
        final String extOpStyleId = changeOp.optString(OCKey.STYLE_ID.value());
        if (lclOpStyleId.equalsIgnoreCase(extOpStyleId)) {
            OTUtils.setOperationRemoved(changeOp);
            return null;
        }

        transformAutoStyleProperty(changeOp, OCKey.STYLE_ID.value(), styleType, deleteOp, false);

        // We still need to check for a valid auto-style identifier
        isValidAutoStyleIdentifier(deleteOp.optString(OCKey.STYLE_ID.value()));

        return null;
    }

    // changeAutoStyle --------------------------------------------------------

    private static JSONObject transform_changeAutoStyle_changeAutoStyle(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // reduce both attribute sets, set empty operations to "removed" state
        if (isSameAutoStyle(lclOp, extOp)) {
            OTUtils.reduceOperationAttributes(lclOp, extOp,
                new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true), new OTUtils.RemoveEmptyAttrsOptions(true)));
        }
        return null;
    }

    // insertChartSeries ------------------------------------------------------

    private static JSONObject transform_insertChartSeries_insertChartSeries(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different charts are independent
        if (OTUtils.equalNumberArrays(lclOp.getJSONArray(OCKey.START.value()), extOp.getJSONArray(OCKey.START.value()), null)) {
            // same series index in both operations: shift local series index away
            OTUtils.transformChartSeriesForInsert(lclOp, extOp.getInt(OCKey.SERIES.value()));
            // transform external operation with the new local insertion index
            OTUtils.transformChartSeriesForInsert(extOp, lclOp.getInt(OCKey.SERIES.value()));
        }
        return null;
    }

    private static JSONObject transform_insertChartSeries_deleteChartSeries(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // different charts are independent
        if (OTUtils.equalNumberArrays(insertOp.getJSONArray(OCKey.START.value()), deleteOp.getJSONArray(OCKey.START.value()), null)) {
            // "insert" (new series on either side) cannot collide with "delete" (existing series)
            OTUtils.transformChartSeriesForInsert(deleteOp, insertOp.getInt(OCKey.SERIES.value()));
            // transform insert operation with the new deletion index
            OTUtils.transformChartSeriesForDelete(insertOp, deleteOp.getInt(OCKey.SERIES.value()));
        }
        return null;
    }

    private static JSONObject transform_insertChartSeries_changeChartSeries(JSONObject insertOp, JSONObject changeOp) throws JSONException {
        // different charts are independent
        if (OTUtils.equalNumberArrays(insertOp.getJSONArray(OCKey.START.value()), changeOp.getJSONArray(OCKey.START.value()), null)) {
            OTUtils.transformChartSeriesForInsert(changeOp, insertOp.getInt(OCKey.SERIES.value()));
        }
        return null;
    }

    // deleteChartSeries ------------------------------------------------------

    private static JSONObject transform_deleteChartSeries_deleteChartSeries(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different charts are independent
        if (OTUtils.equalNumberArrays(lclOp.getJSONArray(OCKey.START.value()), extOp.getJSONArray(OCKey.START.value()), null)) {
            final int lclIdx = lclOp.getInt(OCKey.SERIES.value());
            final int extIdx = extOp.getInt(OCKey.SERIES.value());
            OTUtils.transformChartSeriesForDelete(extOp, lclIdx);
            OTUtils.transformChartSeriesForDelete(lclOp, extIdx);
        }
        return null;
    }

    // TODO: frontend is using index instead of series which is not spezified
    private static JSONObject transform_deleteChartSeries_changeChartSeries(JSONObject deleteOp, JSONObject changeOp) throws JSONException {
        // different charts are independent
        if (OTUtils.equalNumberArrays(deleteOp.getJSONArray(OCKey.START.value()), changeOp.getJSONArray(OCKey.START.value()), null)) {
            OTUtils.transformChartSeriesForDelete(changeOp, deleteOp.getInt(OCKey.SERIES.value()));
        }
        return null;
    }

    // changeChartSeries ------------------------------------------------------

    // TODO: frontend is using index instead of series which is not spezified
    private static JSONObject transform_changeChartSeries_changeChartSeries(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different chart series are independent
        if (OTUtils.equalNumberArrays(lclOp.getJSONArray(OCKey.START.value()), extOp.getJSONArray(OCKey.START.value()), null) && lclOp.getInt(OCKey.SERIES.value()) == extOp.getInt(OCKey.SERIES.value())) {
            OTUtils.reduceOperationAttributes(lclOp, extOp,
                new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true), new OTUtils.RemoveEmptyAttrsOptions(true)));
        }
        return null;
    }

    // deleteChartAxis --------------------------------------------------------

    private static JSONObject transform_deleteChartAxis_deleteChartAxis(JSONObject lclOp, JSONObject extOp) throws JSONException {
     // set both operations to "removed" state, if they delete the same axis (otherwise they are independent)
        if (OTUtils.isSameChartComp(lclOp, extOp)) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }
        return null;
    }

    private static JSONObject transform_deleteChartAxis_changeChartComp(JSONObject deleteOp,JSONObject changeOp) throws JSONException {
        // delete operation always wins over change operation
        if (OTUtils.isSameChartComp(deleteOp, changeOp)) {
            OTUtils.setOperationRemoved(changeOp);
        }
        return null;
    }

    // changeChartComp --------------------------------------------------------

    private static JSONObject transform_changeChartComp_changeChartComp(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // reduce both attribute sets, set empty operations to "removed" state
        if (OTUtils.isSameChartComp(lclOp, extOp)) {
            OTUtils.reduceOperationAttributes(lclOp, extOp,
                new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true), new OTUtils.RemoveEmptyAttrsOptions(true)));
        }
        return null;
    }

    // OT handler functions with same operations

    // helper function to modify the start/end position of the delete operation
    private static void modifyDeletePositionFromMergePara(JSONArray deletePos, final JSONObject mergeOp, final JSONObject deleteOp,  int index) throws JSONException {
        if (deletePos != null) {
            final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);
            int paraIndex = startMergeOp.length() - 1;
            int lastIndex = paraIndex + 1;

            String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

            if (paraIndex == 0 || (deletePos.optInt(paraIndex, -1) >= 0 && JSONHelper.isEqual(startMergeOp, deletePos, paraIndex))) {
                // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the delete op
                if (startMergeOp.getInt(paraIndex) == deletePos.getInt(paraIndex) - 1) {
                    deletePos.put(paraIndex, deletePos.getInt(paraIndex) - 1);
                    if (deletePos.optInt(lastIndex, -1) >= 0) { deletePos.put(lastIndex, deletePos.getInt(lastIndex) + mergeOp.optInt(lengthProperty, 0)); }
                } else if (startMergeOp.getInt(paraIndex) < deletePos.getInt(paraIndex) - 1) {
                    deletePos.put(paraIndex, deletePos.getInt(paraIndex) - 1); // decrease the paragraph position of the delete operation
                } else if (index == 1 && startMergeOp.length() == deletePos.length() && startMergeOp.getInt(paraIndex) == deletePos.getInt(paraIndex)) {
                    // the complete paragraph of the merge operation was removed (delete end position is [3] and merge also in [3])
                    deletePos.add(deletePos.length(), mergeOp.optInt(lengthProperty, 0) - 1);
                    OTUtils.setEndPosition(deleteOp, deletePos);
                }
            }
        }
    }

    public static JSONObject handleInsertCharInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal = OTUtils.getStartPosition(localOp);
        final JSONArray startExt = OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int textIndex = localLength - 1;
        int length = 1;

        if (localLength == extLength) {
            if (JSONHelper.isEqual(startLocal, startExt, localLength - 1)) {
                if (startLocal.getInt(textIndex) <= startExt.getInt(textIndex)) { // INFO: On client side this is implemented with '<'
                    if (OTUtils.isInsertTextOperation(localOp)) { length = OTUtils.getText(localOp).length(); }
                    startExt.put(textIndex, startExt.getInt(textIndex) + length);
                } else {
                    if (OTUtils.isInsertTextOperation(extOp)) { length =OTUtils.getText(extOp).length(); }
                    startLocal.put(textIndex, startLocal.getInt(textIndex) + length);
                }
            }
        } else {
            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort = OTUtils.getStartPosition(shortOp);
            final JSONArray startLong = OTUtils.getStartPosition(longOp);
            textIndex = startShort.length() - 1;

            if (JSONHelper.isEqual(startShort, startLong, textIndex)) {
                if (startShort.getInt(textIndex) <= startLong.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(shortOp)) { length = OTUtils.getText(shortOp).length(); }
                    startLong.put(textIndex, startLong.getInt(textIndex) + length);
                }
            }
        }

        return null;
    }

    private static JSONObject handleParaSplitParaSplit(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal = OTUtils.getStartPosition(localOp);
        final JSONArray startExt = OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int paraIndex = 0;
        int textIndex = 0;
        int length = 0;

        if (localLength == extLength) {
            length = localLength;
            if (length == 2 || JSONHelper.isEqual(startLocal, startExt, length - 2)) {
                paraIndex = length - 2;
                textIndex = paraIndex + 1;

                if (startLocal.getInt(paraIndex) < startExt.getInt(paraIndex)) {
                    startExt.put(paraIndex, startExt.getInt(paraIndex) + 1);
                } else if (startLocal.getInt(paraIndex) > startExt.getInt(paraIndex)) {
                    startLocal.put(paraIndex, startLocal.getInt(paraIndex) + 1);
                } else if (startLocal.getInt(paraIndex) == startExt.getInt(paraIndex)) { // splits in same paragraph
                    if (startLocal.getInt(textIndex) <= startExt.getInt(textIndex)) {
                        startExt.put(paraIndex, startExt.getInt(paraIndex) + 1);
                        startExt.put(textIndex, startExt.getInt(textIndex) - startLocal.getInt(textIndex));
                    } else if (startLocal.getInt(textIndex) > startExt.getInt(textIndex)) {
                        startLocal.put(paraIndex, startLocal.getInt(paraIndex) + 1);
                        startLocal.put(textIndex, startLocal.getInt(textIndex) - startExt.getInt(textIndex));
                    }
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort = OTUtils.getStartPosition(shortOp);
            final JSONArray startLong = OTUtils.getStartPosition(longOp);
            final int shortLength = startShort.length();

            paraIndex = shortLength - 2;
            textIndex = paraIndex + 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                if (startShort.getInt(paraIndex) < startLong.getInt(paraIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                } else if (startShort.getInt(paraIndex) == startLong.getInt(paraIndex) && startShort.getInt(textIndex) <= startLong.getInt(textIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                    startLong.put(textIndex, startLong.getInt(textIndex) - startShort.getInt(textIndex));
                }
            }
        }

        return null;
    }

    private static JSONObject handleSplitTableSplitTable(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int tableIndex = 0;
        int rowIndex = 0;
        int length = 0;

        if (localLength == extLength) {
            length = localLength;
            if (length == 2 || JSONHelper.isEqual(startLocal, startExt, length - 2)) {
                tableIndex = length - 2;
                rowIndex = tableIndex + 1;
                if (startLocal.getInt(tableIndex) < startExt.getInt(tableIndex)) {
                    startExt.put(tableIndex, startExt.getInt(tableIndex) + 1);
                } else if (startLocal.getInt(tableIndex) > startExt.getInt(tableIndex)) {
                    startLocal.put(tableIndex, startLocal.getInt(tableIndex) + 1);
                } else if (startLocal.getInt(tableIndex) == startExt.getInt(tableIndex)) {
                    // both splits happened in the same table -> did the local split happen before the external split
                    if (startLocal.getInt(rowIndex) < startExt.getInt(rowIndex)) {
                        startExt.put(tableIndex, startExt.getInt(tableIndex) + 1);
                        startExt.put(rowIndex, startExt.getInt(rowIndex) - startLocal.getInt(rowIndex));
                    } else if (startLocal.getInt(rowIndex) >= startExt.getInt(rowIndex)) {
                        startLocal.put(tableIndex, startLocal.getInt(tableIndex) + 1);
                        startLocal.put(rowIndex, startLocal.getInt(rowIndex) - startExt.getInt(rowIndex));
                    }
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);
            final int shortLength = startShort.length();

            tableIndex = shortLength - 2;
            rowIndex = tableIndex + 1;

            if (tableIndex == 0 || JSONHelper.isEqual(startShort, startLong, tableIndex)) {
                if (startShort.getInt(tableIndex) < startLong.getInt(tableIndex)) {
                    startLong.put(tableIndex, startLong.getInt(tableIndex) + 1);
                } else if (startShort.getInt(tableIndex) == startLong.getInt(tableIndex) && startShort.getInt(rowIndex) <= startLong.getInt(rowIndex)) {
                    startLong.put(tableIndex, startLong.getInt(tableIndex) + 1);
                    startLong.put(rowIndex, startLong.getInt(rowIndex) - startShort.getInt(rowIndex));
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertParaInsertPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int paraIndex = 0;
        int length = 0;

        if (localLength == extLength) {
            length = localLength;
            if (localLength == 1 || JSONHelper.isEqual(startLocal, startExt, length - 1)) {
                paraIndex = length - 1;
                if (startLocal.getInt(paraIndex) <= startExt.getInt(paraIndex)) { // on client side this is implemented with '<'
                    startExt.put(paraIndex, startExt.getInt(paraIndex) + 1);
                } else {
                    startLocal.put(paraIndex, startLocal.getInt(paraIndex) + 1);
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);
            final int shortLength = startShort.length();

            paraIndex = shortLength - 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                if (startShort.getInt(paraIndex) <= startLong.getInt(paraIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertRowsInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int rowIndex = 0;
        int rowCount = 1;

        if (localLength == extLength) {
            if (JSONHelper.isEqual(startLocal, startExt, localLength - 1)) {
                rowIndex = localLength - 1;
                if (startLocal.getInt(rowIndex) <= startExt.getInt(rowIndex)) { // this is implemented on client side with '<'
                    rowCount = OTUtils.getCountProperty(localOp, 1);
                    startExt.put(rowIndex, startExt.getInt(rowIndex) + rowCount);
                    final int referenceRow = extOp.optInt(OCKey.REFERENCE_ROW.value(), -1);
                    if (referenceRow >= startLocal.getInt(rowIndex)) {
                        extOp.put(OCKey.REFERENCE_ROW.value(), referenceRow + rowCount);
                    }
                } else {
                    rowCount = OTUtils.getCountProperty(extOp, 1);
                    startLocal.put(rowIndex, startLocal.getInt(rowIndex) + rowCount);
                    final int referenceRow = localOp.optInt(OCKey.REFERENCE_ROW.value(), -1);
                    if (referenceRow >= startExt.getInt(rowIndex)) {
                        localOp.put(OCKey.REFERENCE_ROW.value(), referenceRow + rowCount);
                    }
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            rowIndex = startShort.length() - 1;

            if (JSONHelper.isEqual(startShort, startLong, rowIndex)) {
                if (startShort.getInt(rowIndex) <= startLong.getInt(rowIndex)) {
                    rowCount = OTUtils.getCountProperty(shortOp, 1);
                    startLong.put(rowIndex, startLong.getInt(rowIndex) + rowCount);
                }
            }
        }

        return null;
    }

    private static JSONObject handleSetAttrsSetAttrs(JSONObject localOp, JSONObject extOp) throws JSONException {

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        int surrounding = 0;
        int posLength = 0;
        int lastIndex = 0;

        // this is the client side code, that is executed on server side.
        // On client side only the external operation is modified. A local operation is never modified.
        // Therefore on server side, only the local operation (the operation in the temporary pending OPs
        // container) must be modified. This is localOp. extOp must not be modified.
        // The simplest solution is to exchange the operations now.
        JSONObject tempObject = localOp;
        localOp = extOp;
        extOp = tempObject;

        if (JSONHelper.separateRanges(localOp, extOp)) { return null; }

        JSONObject reducedExtAttrs = OTUtils.cloneJSONObject(OTUtils.getAttributes(extOp));

        boolean extModified = JSONHelper.reduceAttributeSet(OTUtils.getAttributes(localOp), reducedExtAttrs, false);
        boolean emptyExtAttrs = false;
        if (extModified && reducedExtAttrs.isEmpty()) { emptyExtAttrs = true; }
        boolean insertBefore = false;

        if (extModified) {

            if (JSONHelper.identicalRanges(localOp, extOp)) {
                if (emptyExtAttrs) {
                    OTUtils.setOperationRemoved(extOp); // setting marker that the local operation can be ignored
                } else {
                    OTUtils.setAttributes(extOp, reducedExtAttrs);
                }
            } else {

                final JSONArray startExtOp =OTUtils.getStartPosition(extOp);
                final JSONArray startLocalOp =OTUtils.getStartPosition(localOp);

                final JSONArray endExtOp = OTUtils.optEndPosition(extOp);
                final JSONArray endLocalOp = OTUtils.optEndPosition(localOp);

                surrounding = JSONHelper.surroundingRanges(localOp, extOp);

                if (surrounding != 0) {
                    if (surrounding == 1) {
                        if (startExtOp.length() == startLocalOp.length()) {
                            if (emptyExtAttrs) {
                                OTUtils.setOperationRemoved(extOp); // setting marker that the external operation can be ignored
                            } else {
                                OTUtils.setAttributes(extOp, reducedExtAttrs);
                            }
                        }
                    } else {
                        posLength = startExtOp.length();
                        lastIndex = posLength - 1;

                        if (endExtOp != null && endLocalOp != null && endExtOp.length() == posLength && startLocalOp.length() == posLength && endLocalOp.length() == posLength) {
                            if (startExtOp.getInt(lastIndex) < startLocalOp.getInt(lastIndex) && endExtOp.getInt(lastIndex) > endLocalOp.getInt(lastIndex)) {
                                JSONObject newOperation = OTUtils.cloneJSONObject(extOp);
                                OTUtils.setStartPosition(newOperation, JSONHelper.increaseLastIndex(endLocalOp));
                                localOpsAfter = new JSONArray();
                                localOpsAfter.put(newOperation); // inserting behind the current external operation

                                OTUtils.setEndPosition(extOp, JSONHelper.increaseLastIndex(startLocalOp, -1)); // TODO: Not reliable
                            } else if (startExtOp.getInt(lastIndex) < startLocalOp.getInt(lastIndex)) {
                                OTUtils.setEndPosition(extOp, JSONHelper.increaseLastIndex(startLocalOp, -1));
                            } else if (endExtOp.getInt(lastIndex) > endLocalOp.getInt(lastIndex)) {
                                OTUtils.setStartPosition(extOp, JSONHelper.increaseLastIndex(endLocalOp));
                                insertBefore = true;
                            }

                            // a further new external operation is required for the range of the local operation, but with reduced attribute set
                            if (!emptyExtAttrs) {
                                JSONObject newReducedOperation = OTUtils.cloneJSONObject(localOp);
                                OTUtils.setAttributes(newReducedOperation ,reducedExtAttrs);

                                if (insertBefore) {
                                    localOpsBefore = new JSONArray();
                                    localOpsBefore.put(newReducedOperation); // inserting before the current external operation
                                } else {
                                    if (localOpsAfter == null) { localOpsAfter = new JSONArray(); }
                                    localOpsAfter.add(0, newReducedOperation); // inserting behind the current external operation
                                }
                            }
                        } else {
                            // If a text attribute is set at the paragraph [3], the more specific setting of a concurrent attribute
                            // from [3,2] to [3,6] will overwrite it -> no further operation required.
                        }

                    }
                } else {

                    if (JSONHelper.compareNumberArrays(startLocalOp, startExtOp) <= 0) {
                        if (endLocalOp != null && endLocalOp.length() == startExtOp.length()) {

                            JSONArray extOpStartOrig = JSONHelper.clonePosition(startExtOp);

                            OTUtils.setStartPosition(extOp, JSONHelper.increaseLastIndex(endLocalOp));

                            // a further new external operation is required for the common range of the operations, but with reduced attribute set
                            if (!emptyExtAttrs) {
                                JSONObject newReducedOperation = OTUtils.cloneJSONObject(localOp);
                                OTUtils.setAttributes(newReducedOperation, reducedExtAttrs);
                                OTUtils.setStartPosition(newReducedOperation, extOpStartOrig);
                                localOpsBefore = new JSONArray();
                                localOpsBefore.put(newReducedOperation); // inserting before the current external operation
                            }
                        }
                    } else {
                        if (endExtOp != null && startLocalOp.length() == endExtOp.length()) {

                            JSONArray extOpEndOrig = JSONHelper.clonePosition(endExtOp);

                            OTUtils.setEndPosition(extOp, JSONHelper.increaseLastIndex(startLocalOp, -1));

                            // a further new external operation is required for the common range of the operations, but with reduced attribute set
                            if (!emptyExtAttrs) {
                                JSONObject newReducedOperation = OTUtils.cloneJSONObject(localOp);
                                OTUtils.setAttributes(newReducedOperation, reducedExtAttrs);
                                OTUtils.setEndPosition(newReducedOperation, extOpEndOrig);
                                localOpsAfter = new JSONArray();
                                localOpsAfter.put(newReducedOperation); // inserting behind the current external operation
                            }
                        }
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleUpdateFieldUpdateField(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONArray startExtOp =OTUtils.getStartPosition(extOp);
        JSONArray startLocalOp =OTUtils.getStartPosition(localOp);

        if (JSONHelper.isEqual(startLocalOp, startExtOp)) {

            String localType = OTUtils.getTypeProperty(localOp);
            String extType = OTUtils.getTypeProperty(extOp);

            String localRepresentation = OTUtils.getRepresentationProperty(localOp);
            String extRepresentation = OTUtils.getRepresentationProperty(extOp);


            if (localType.equals(extType) && localRepresentation.equals(extRepresentation)) {
                // if the type and representation are the same, both operations can be ignored
                OTUtils.setOperationRemoved(extOp);
                OTUtils.setOperationRemoved(localOp);
            } else {
                // different type or representation: always applying external operation
                // Info: on client side the external operation is ignored.
                OTUtils.setOperationRemoved(localOp);
            }
        }

        return null;
    }

    public static JSONObject handleDeleteDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONArray deleteArray = null;;
        JSONArray endPosition = null; // an optionally existing end position of a delete operation
        int index = 0;
        int surrounding = 0; // whether the two delete ranges surround each other

        JSONArray startExtOp =OTUtils.getStartPosition(extOp);
        JSONArray startLocalOp =OTUtils.getStartPosition(localOp);

        final JSONArray endExtOp = OTUtils.optEndPosition(extOp);
        final JSONArray endLocalOp = OTUtils.optEndPosition(localOp);

        if (JSONHelper.separateRanges(localOp, extOp)) {

            if (JSONHelper.compareNumberArrays(startLocalOp, startExtOp) < 0) {
                if (endLocalOp != null) {
                    endPosition = endLocalOp;
                } else {
                    endPosition = startLocalOp;
                }

                deleteArray = JSONHelper.calculateDeleteArray(startLocalOp, endPosition, startExtOp);
                for (index = 0; index < startExtOp.length(); index += 1) {
                    startExtOp.put(index, startExtOp.getInt(index) - deleteArray.getInt(index));
                }

                if (endExtOp != null) {
                    deleteArray = JSONHelper.calculateDeleteArray(startLocalOp, endPosition, endExtOp);
                    for (index = 0; index < endExtOp.length(); index += 1) {
                        endExtOp.put(index, endExtOp.getInt(index) - deleteArray.getInt(index));
                    }
                }
            } else {
                if (endExtOp != null) {
                    endPosition = endExtOp;
                } else {
                    endPosition = startExtOp;
                }

                deleteArray = JSONHelper.calculateDeleteArray(startExtOp, endPosition, startLocalOp);
                for (index = 0; index < startLocalOp.length(); index += 1) {
                    startLocalOp.put(index, startLocalOp.getInt(index) - deleteArray.getInt(index));
                }

                if (endLocalOp != null) {
                    deleteArray = JSONHelper.calculateDeleteArray(startExtOp, endPosition, endLocalOp);
                    for (index = 0; index < endLocalOp.length(); index += 1) {
                        endLocalOp.put(index, endLocalOp.getInt(index) - deleteArray.getInt(index));
                    }
                }
            }

        } else if (JSONHelper.identicalRanges(localOp, extOp))  {

            // setting marker at both operations
            OTUtils.setOperationRemoved(localOp); // setting marker at local operation
            OTUtils.setOperationRemoved(extOp); // setting marker at external operation

        } else {

            surrounding = JSONHelper.surroundingRanges(localOp, extOp);

            if (surrounding != 0) {

                if (surrounding == 1) {
                    // local surrounds external operation -> shrinking the local operation range and ignoring the external operation
                    if (endLocalOp != null) {
                        if (endExtOp != null) {
                            endPosition = endExtOp;
                        } else {
                            endPosition = startExtOp;
                        }
                        deleteArray = JSONHelper.calculateDeleteArray(startExtOp, endPosition, endLocalOp);
                        for (index = 0; index < endLocalOp.length(); index += 1) {
                            endLocalOp.put(index, endLocalOp.getInt(index) - deleteArray.getInt(index));
                        }
                    }
                    OTUtils.setOperationRemoved(extOp); // setting marker at external operation
                } else {
                    // external surrounds local operation -> ignoring the local operation and shrinking the external operation range
                    if (endExtOp != null) {
                        if (endLocalOp != null) {
                            endPosition = endLocalOp;
                        } else {
                            endPosition = startLocalOp;
                        }
                        deleteArray = JSONHelper.calculateDeleteArray(startLocalOp, endPosition, endExtOp);
                        for (index = 0; index < endExtOp.length(); index += 1) {
                            endExtOp.put(index, endExtOp.getInt(index) - deleteArray.getInt(index));
                        }
                    }
                    OTUtils.setOperationRemoved(localOp); // setting marker at internal operation
                }

            } else {
                // overlapping ranges -> 3 new ranges:
                //     - part only covered by localOp
                //     - part only covered by extOp
                //     - part covered by both operations (content already deleted by localOp)

                JSONArray origLocalStart = JSONHelper.clonePosition(startLocalOp);
                JSONArray origLocalEnd = JSONHelper.clonePosition(endLocalOp);

                // -> both operations need to get shrinked ranges
                if (JSONHelper.compareNumberArrays(startLocalOp, startExtOp) <= 0) {

                    // shrinking the range of the local operation -> leaving only the part covered only by the local operation
                    // recalculating the range of the external operation, that can be applied to the local document
                    OTUtils.setEndPosition(localOp, JSONHelper.increaseLastIndex(startExtOp, -1)); // TODO: Not reliable (both must have same length and same parent, fails at position 0)
                    // modifying the external operation in that way, that only the missing part is removed from the local document
                    OTUtils.setStartPosition(extOp, JSONHelper.increaseLastIndex(origLocalEnd)); // TODO: Not reliable

                    startExtOp =OTUtils.getStartPosition(extOp); // refresh required

                    // shifting the two positions of the external operation to the front
                    deleteArray = JSONHelper.calculateDeleteArray(origLocalStart, origLocalEnd, startExtOp);
                    for (index = 0; index < startExtOp.length(); index += 1) {
                        startExtOp.put(index, startExtOp.getInt(index) - deleteArray.getInt(index));
                    }
                    deleteArray = JSONHelper.calculateDeleteArray(origLocalStart, origLocalEnd, endExtOp);
                    for (index = 0; index < endExtOp.length(); index += 1) {
                        endExtOp.put(index, endExtOp.getInt(index) - deleteArray.getInt(index));
                    }

                } else {

                    JSONArray origExtStart = JSONHelper.clonePosition(startExtOp);
                    JSONArray origExtEnd = JSONHelper.clonePosition(endExtOp);

                    OTUtils.setStartPosition(localOp, JSONHelper.increaseLastIndex(endExtOp)); // TODO the common part becomes part of the external operation
                    // modifying the external operation in that way, that only the missing part is removed from the local document
                    OTUtils.setEndPosition(extOp, JSONHelper.increaseLastIndex(origLocalStart, -1)); // TODO

                    startLocalOp =OTUtils.getStartPosition(localOp); // refresh required

                    // shifting the two positions of the local operation to the front
                    deleteArray = JSONHelper.calculateDeleteArray(origExtStart, origExtEnd, startLocalOp);
                    for (index = 0; index < startLocalOp.length(); index += 1) {
                        startLocalOp.put(index, startLocalOp.getInt(index) - deleteArray.getInt(index));
                    }
                    deleteArray = JSONHelper.calculateDeleteArray(origExtStart, origExtEnd, endLocalOp);
                    for (index = 0; index < endLocalOp.length(); index += 1) {
                        endLocalOp.put(index, endLocalOp.getInt(index) - deleteArray.getInt(index));
                    }

                }
            }
        }

        return null;
    }

    private static JSONObject handleDeleteColumnsDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int tableIndex = 0;
        int columnIndex = 0;
        int deleteColumns = 0;

        if (localLength == extLength) {
            if (JSONHelper.isEqual(startLocal, startExt, localLength)) {
                tableIndex = localLength - 1;
                columnIndex = tableIndex + 2;

                if (OTUtils.getStartGridProperty(localOp) == OTUtils.getStartGridProperty(extOp) && OTUtils.getEndGridProperty(localOp) == OTUtils.getEndGridProperty(extOp)) { // identical ranges
                    OTUtils.setOperationRemoved(localOp);
                    OTUtils.setOperationRemoved(extOp);
                } else if (OTUtils.getEndGridProperty(localOp) < OTUtils.getStartGridProperty(extOp)) { // local range before external range
                    deleteColumns = OTUtils.getEndGridProperty(localOp) - OTUtils.getStartGridProperty(localOp) + 1;
                    OTUtils.setStartGridProperty(extOp, OTUtils.getStartGridProperty(extOp) - deleteColumns);
                    OTUtils.setEndGridProperty(extOp, OTUtils.getEndGridProperty(extOp) - deleteColumns);
                } else if (OTUtils.getEndGridProperty(extOp) < OTUtils.getStartGridProperty(localOp)) { // external range before local range
                    deleteColumns = OTUtils.getEndGridProperty(extOp) - OTUtils.getStartGridProperty(extOp) + 1;
                    OTUtils.setStartGridProperty(localOp, OTUtils.getStartGridProperty(localOp) - deleteColumns);
                    OTUtils.setEndGridProperty(localOp, OTUtils.getEndGridProperty(localOp) - deleteColumns);
                } else if (OTUtils.getStartGridProperty(localOp) >= OTUtils.getStartGridProperty(extOp) && OTUtils.getEndGridProperty(localOp) <= OTUtils.getEndGridProperty(extOp)) { // local range inside external range
                    deleteColumns = OTUtils.getEndGridProperty(localOp) - OTUtils.getStartGridProperty(localOp) + 1;
                    OTUtils.setEndGridProperty(extOp, OTUtils.getEndGridProperty(extOp) - deleteColumns);
                    OTUtils.setOperationRemoved(localOp);
                } else if (OTUtils.getStartGridProperty(extOp) >= OTUtils.getStartGridProperty(localOp) && OTUtils.getEndGridProperty(extOp) <= OTUtils.getEndGridProperty(localOp)) { // external range inside local range
                    deleteColumns = OTUtils.getEndGridProperty(extOp) - OTUtils.getStartGridProperty(extOp) + 1;
                    OTUtils.setEndGridProperty(localOp, OTUtils.getEndGridProperty(localOp) - deleteColumns);
                    OTUtils.setOperationRemoved(extOp);
                } else if (OTUtils.getStartGridProperty(localOp) <= OTUtils.getStartGridProperty(extOp)) { // overlapping ranges, local range before external range
                    deleteColumns = OTUtils.getEndGridProperty(localOp) - OTUtils.getStartGridProperty(localOp) + 1;
                    OTUtils.setEndGridProperty(localOp, OTUtils.getStartGridProperty(extOp) - 1);
                    OTUtils.setStartGridProperty(extOp, OTUtils.getStartGridProperty(localOp));
                    OTUtils.setEndGridProperty(extOp, OTUtils.getEndGridProperty(extOp) - deleteColumns);
                } else if (OTUtils.getStartGridProperty(localOp) > OTUtils.getStartGridProperty(extOp)) { // overlapping ranges, local range after external range
                    deleteColumns = OTUtils.getEndGridProperty(extOp) - OTUtils.getStartGridProperty(extOp) + 1;
                    OTUtils.setEndGridProperty(extOp, OTUtils.getStartGridProperty(localOp) - 1);
                    OTUtils.setStartGridProperty(localOp, OTUtils.getStartGridProperty(extOp));
                    OTUtils.setEndGridProperty(localOp, OTUtils.getEndGridProperty(localOp) - deleteColumns);
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            tableIndex = startShort.length() - 1;
            columnIndex = tableIndex + 2;

            if (JSONHelper.isEqual(startShort, startLong, tableIndex + 1) && startLong.getInt(columnIndex) >= OTUtils.getStartGridProperty(shortOp)) {
                if (startLong.getInt(columnIndex) > OTUtils.getEndGridProperty(shortOp)) {
                    deleteColumns = OTUtils.getEndGridProperty(shortOp) - OTUtils.getStartGridProperty(shortOp) + 1;
                    startLong.put(columnIndex, startLong.getInt(columnIndex) - deleteColumns);
                } else {
                    OTUtils.setOperationRemoved(longOp); // setting marker at long operation, that it is no longer used
                }
            }

        }

        return null;
    }

    private static JSONObject handleInsertColumnInsertColumn(JSONObject localOp, JSONObject extOp) throws JSONException {

        // this is the client side code, that is executed on server side.
        // The simplest solution is to exchange the operations because of the grid position calculation.
        JSONObject tempObject = localOp;
        localOp = extOp;
        extOp = tempObject;

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int tableIndex = 0;
        int columnIndex = 0;

        if (localLength == extLength) {
            if (JSONHelper.isEqual(startLocal, startExt, localLength)) {
                tableIndex = localLength - 1;
                columnIndex = tableIndex + 2;

                if (OTUtils.getGridPositionProperty(localOp) > OTUtils.getGridPositionProperty(extOp)) {
                    OTUtils.setGridPositionProperty(localOp, OTUtils.getGridPositionProperty(localOp) + 1);
                } else if (OTUtils.getGridPositionProperty(localOp) < OTUtils.getGridPositionProperty(extOp)) {
                    OTUtils.setGridPositionProperty(extOp, OTUtils.getGridPositionProperty(extOp) + 1);
                } else {
                    // both grid positions are equal
                    String localMode = OTUtils.getInsertModeProperty(localOp);
                    String extMode = OTUtils.getInsertModeProperty(extOp);

                    if (localMode.equals(extMode)) {
                        OTUtils.setGridPositionProperty(localOp, OTUtils.getGridPositionProperty(localOp) + 1); // increasing the local position
                    } else if (OTUtils.isInsertModeBehind(OTUtils.getInsertModeProperty(localOp))) {
                        OTUtils.setGridPositionProperty(localOp, OTUtils.getGridPositionProperty(localOp) + 1); // increasing the local position
                    } else if (OTUtils.isInsertModeBehind(OTUtils.getInsertModeProperty(extOp))) {
                        OTUtils.setGridPositionProperty(extOp, OTUtils.getGridPositionProperty(extOp) + 1); // increasing the external position
                    }
                }

                // adapting the table grid
                int localInsertedIndex = OTUtils.getGridPositionProperty(localOp);
                JSONArray extTableGrid = JSONHelper.clonePosition(OTUtils.getTableGridProperty(extOp));
                int tableGridLength = extTableGrid.length();
                int insertIndex = localInsertedIndex;
                if (OTUtils.isInsertModeBehind(OTUtils.getInsertModeProperty(localOp))) { insertIndex = localInsertedIndex + 1; }

                int columnWidth = 0;
                if (tableGridLength > localInsertedIndex) {
                    columnWidth = extTableGrid.getInt(localInsertedIndex);
                } else {
                    columnWidth = extTableGrid.getInt(tableGridLength - 1);
                }

                // inserting the new column in the table grid
                extTableGrid.add(insertIndex, columnWidth); // inserting behind the current local operation

                OTUtils.setTableGridProperty(extOp, JSONHelper.clonePosition(extTableGrid));
                OTUtils.setTableGridProperty(localOp, JSONHelper.clonePosition(extTableGrid));
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            tableIndex = startShort.length() - 1;
            columnIndex = tableIndex + 2;

            if (JSONHelper.isEqual(startShort, startLong, startShort.length())) { // both positions must be inside the same table
                if (startLong.getInt(columnIndex) > OTUtils.getGridPositionProperty(shortOp) || (OTUtils.isInsertModeBefore(OTUtils.getInsertModeProperty(shortOp)) && startLong.getInt(columnIndex) == OTUtils.getGridPositionProperty(shortOp))) {
                    startLong.put(columnIndex, startLong.getInt(columnIndex) + 1);
                }
            }
        }

        return null;
    }

    private static JSONObject handleParaMergeParaMerge(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);
        final int localLength = startLocal.length();
        final int extLength = startExt.length();
        int paraIndex = 0;
        int textIndex = 0;
        int length = 0;

        if (localLength == extLength) {
            if (length == 1 || JSONHelper.isEqual(startLocal, startExt, localLength - 1)) {
                // both positions are top level (length === 1) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = localLength - 1; // the paragraph index is always the last index
                String lengthProperty = OTUtils.getMergeLengthProperty(localOp);
                if (startLocal.getInt(paraIndex) < startExt.getInt(paraIndex)) {
                    startExt.put(paraIndex, startExt.getInt(paraIndex) - 1); // decreasing external operation paragraph position
                    if (startExt.getInt(paraIndex) == startLocal.getInt(paraIndex)) { extOp.put(lengthProperty, extOp.optInt(lengthProperty, 0) + localOp.optInt(lengthProperty, 0)); }
                } else if (startLocal.getInt(paraIndex) > startExt.getInt(paraIndex)) {
                    startLocal.put(paraIndex, startLocal.getInt(paraIndex) - 1); // decreasing internal operation paragraph position
                    if (startExt.getInt(paraIndex) == startLocal.getInt(paraIndex)) { localOp.put(lengthProperty, localOp.optInt(lengthProperty, 0) + extOp.optInt(lengthProperty, 0)); }
                } else if (startLocal.getInt(paraIndex) == startExt.getInt(paraIndex)) {
                    // both merges happened in the same paragraph -> they are identical and can be ignored
                    OTUtils.setOperationRemoved(extOp); // setting marker at external operation
                    OTUtils.setOperationRemoved(localOp); // setting marker at local operation
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            paraIndex = startShort.length() - 1;
            textIndex = paraIndex + 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (startShort.getInt(paraIndex) == startLong.getInt(paraIndex) - 1) {
                    String shortLengthProperty = OTUtils.getMergeLengthProperty(shortOp);
                    startLong.put(paraIndex, startLong.getInt(paraIndex) - 1); // decrease the paragraph position of the operation with the longer position
                    startLong.put(textIndex, startLong.getInt(textIndex) + shortOp.optInt(shortLengthProperty, 0));
                } else if (startShort.getInt(paraIndex) < startLong.getInt(paraIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) - 1); // decrease the paragraph position of the operation with the longer position
                }
            }
        }

        return null;
    }

    // OT handler functions with different operations

    public static JSONObject handleParaSplitInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject splitOp = null;
        JSONObject insertOp = null;
        int paraIndex = 0;
        int textIndex = 0;
        int length = 1;

        if (OTUtils.isSplitParagraphOperation(localOp)) {
            splitOp = localOp;
            insertOp = extOp;
        } else {
            splitOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);
        final int lengthSplit = startSplit.length();
        final int lengthInsert = startInsert.length();

        if (lengthSplit <= lengthInsert) {
            if (lengthSplit == 2 || JSONHelper.isEqual(startSplit, startInsert, lengthSplit - 2)) {
                paraIndex = lengthSplit - 2;
                textIndex = paraIndex + 1;
                if (startSplit.getInt(paraIndex) < startInsert.getInt(paraIndex)) {
                    startInsert.put(paraIndex, startInsert.getInt(paraIndex) + 1); // increasing the paragraph position of the insert operation
                } else if (startSplit.getInt(paraIndex) == startInsert.getInt(paraIndex)) { // same paragraph
                    if (startSplit.getInt(textIndex) <= startInsert.getInt(textIndex)) {
                        startInsert.put(paraIndex, startInsert.getInt(paraIndex) + 1); // increasing the paragraph position of the insert operation
                        startInsert.put(textIndex, startInsert.getInt(textIndex) - startSplit.getInt(textIndex)); // decreasing the text position of the insert operation
                    } else {
                        if (OTUtils.isInsertTextOperation(insertOp)) { length =OTUtils.getText(insertOp).length(); }
                        startSplit.put(textIndex, startSplit.getInt(textIndex) + length); // increasing the text position of the split operation
                    }
                }
            }
        } else {
            textIndex = lengthInsert - 1;

            if (JSONHelper.isEqual(startSplit, startInsert, lengthInsert - 1)) {
                if (startInsert.getInt(textIndex) <= startSplit.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertOp)) { length =OTUtils.getText(insertOp).length(); }
                    startSplit.put(textIndex, startSplit.getInt(textIndex) + length); // increasing the paragraph position of the insert operation
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertParaInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertParaOp = null;
        JSONObject insertCharOp = null;

        if (OTUtils.isInsertParagraphOperation(localOp) || OTUtils.isInsertTableOperation(localOp)) {
            insertParaOp = localOp;
            insertCharOp = extOp;
        } else {
            insertParaOp = extOp;
            insertCharOp = localOp;
        }

        final JSONArray startInsertPara =OTUtils.getStartPosition(insertParaOp);
        final JSONArray startInsertChar =OTUtils.getStartPosition(insertCharOp);

        int paraIndex = 0;
        int insertParaLength = startInsertPara.length();

        if (JSONHelper.compareNumberArrays(startInsertPara, startInsertChar) <= 0) {
            paraIndex = insertParaLength - 1;
            if (paraIndex == 0 || (startInsertChar.length() > insertParaLength && JSONHelper.isEqual(startInsertChar, startInsertPara, insertParaLength - 1))) {
                startInsertChar.put(paraIndex, startInsertChar.getInt(paraIndex) + 1);
            }
        } else {
            int insertCharLength = startInsertChar.length();
            int textIndex = insertCharLength - 1;
            int insertLength = 1;
            if (insertCharLength < insertParaLength && JSONHelper.isEqual(startInsertChar, startInsertPara, insertCharLength - 1)) {
                if (startInsertChar.getInt(textIndex) <= startInsertPara.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertCharOp)) { insertLength =OTUtils.getText(insertCharOp).length(); }
                    startInsertPara.put(textIndex, startInsertPara.getInt(textIndex) + insertLength);
                }
            }

        }

        return null;
    }

    public static JSONObject handleInsertParaSplitPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertParaOp = null;
        JSONObject splitParaOp = null;
        int paraIndex = 0;

        if (OTUtils.isInsertParagraphOperation(localOp) || OTUtils.isInsertTableOperation(localOp) || OTUtils.isInsertSlideOperation(localOp)) {
            insertParaOp = localOp;
            splitParaOp = extOp;
        } else {
            insertParaOp = extOp;
            splitParaOp = localOp;
        }

        final JSONArray startInsertPara =OTUtils.getStartPosition(insertParaOp);
        final JSONArray startSplitPara =OTUtils.getStartPosition(splitParaOp);

        int insertParaLength = startInsertPara.length();
        int splitParaLength = startSplitPara.length();

        if (splitParaLength == insertParaLength + 1) {
            if (insertParaLength == 1 || JSONHelper.isEqual(startSplitPara, startInsertPara, insertParaLength - 1)) {
                paraIndex = insertParaLength - 1;
                if (startInsertPara.getInt(paraIndex) <= startSplitPara.getInt(paraIndex)) {
                    startSplitPara.put(paraIndex, startSplitPara.getInt(paraIndex) + 1);
                } else {
                    startInsertPara.put(paraIndex, startInsertPara.getInt(paraIndex) + 1);
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (insertParaLength < splitParaLength - 1) {
                shortOp = insertParaOp;
                longOp = splitParaOp;
            } else {
                shortOp = splitParaOp;
                longOp = insertParaOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            if (OTUtils.isInsertParagraphOperation(shortOp) || OTUtils.isInsertTableOperation(shortOp) || OTUtils.isInsertSlideOperation(shortOp)) {
                paraIndex = startShort.length() - 1;
            } else {
                paraIndex = startShort.length() - 2;
            }

            int textIndex = paraIndex + 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                if (startShort.getInt(paraIndex) < startLong.getInt(paraIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                } else if (startShort.getInt(paraIndex) == startLong.getInt(paraIndex)) {
                    if (shortOp == splitParaOp && startShort.getInt(textIndex) <= startLong.getInt(textIndex)) {
                        startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                        startLong.put(textIndex, startLong.getInt(textIndex) - startShort.getInt(textIndex));
                    } else if (shortOp == insertParaOp) {
                        startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                    }
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertRowsInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertRowsOp = null;
        JSONObject insertCharOp = null;
        int rowIndex = 0;

        if (OTUtils.isInsertRowsOperation(localOp) || OTUtils.isInsertCellsOperation(localOp)) {
            insertRowsOp = localOp;
            insertCharOp = extOp;
        } else {
            insertRowsOp = extOp;
            insertCharOp = localOp;
        }

        final JSONArray startInsertRows =OTUtils.getStartPosition(insertRowsOp);
        final JSONArray startInsertChar =OTUtils.getStartPosition(insertCharOp);

        int insertRowsLength = startInsertRows.length();
        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);

        if (JSONHelper.compareNumberArrays(startInsertRows, startInsertChar) <= 0) {
            rowIndex = insertRowsLength - 1;

            if ((startInsertChar.length() >= insertRowsLength && JSONHelper.isEqual(startInsertChar, startInsertRows, insertRowsLength - 1))) {
                if (startInsertChar.getInt(rowIndex) >= startInsertRows.getInt(rowIndex)) { // position inside a following row/cell
                    startInsertChar.put(rowIndex, startInsertChar.getInt(rowIndex) + rowCount);
                }
            }
        } else {
            int insertCharLength = startInsertChar.length();
            int textIndex = insertCharLength - 1;
            int insertLength = 1;
            if (insertCharLength < insertRowsLength && JSONHelper.isEqual(startInsertChar, startInsertRows, insertCharLength - 1)) {
                if (startInsertChar.getInt(textIndex) <= startInsertRows.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertCharOp)) { insertLength =OTUtils.getText(insertCharOp).length(); }
                    startInsertRows.put(textIndex, startInsertRows.getInt(textIndex) + insertLength);
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertRowsSplitPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertRowsOp = null;
        JSONObject splitParaOp = null;
        int rowIndex = 0;
        int paraIndex = 0;

        if (OTUtils.isInsertRowsOperation(localOp) || OTUtils.isInsertCellsOperation(localOp)) {
            insertRowsOp = localOp;
            splitParaOp = extOp;
        } else {
            insertRowsOp = extOp;
            splitParaOp = localOp;
        }

        final JSONArray startInsertRows =OTUtils.getStartPosition(insertRowsOp);
        final JSONArray startSplitPara =OTUtils.getStartPosition(splitParaOp);

        int insertRowsLength = startInsertRows.length();
        int splitParaLength = startSplitPara.length();
        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);

        if (JSONHelper.compareNumberArrays(startInsertRows, startSplitPara) <= 0) {
            rowIndex = insertRowsLength - 1;

            if ((splitParaLength >= insertRowsLength && JSONHelper.isEqual(startInsertRows, startSplitPara, insertRowsLength - 1))) {
                if (startSplitPara.getInt(rowIndex) >= startInsertRows.getInt(rowIndex)) {
                    startSplitPara.put(rowIndex, startSplitPara.getInt(rowIndex) + rowCount);
                }
            }
        } else {
            paraIndex = splitParaLength - 2;
            int textIndex = paraIndex + 1;

            if ((paraIndex == 0) || (insertRowsLength >= splitParaLength && JSONHelper.isEqual(startInsertRows, startSplitPara, paraIndex))) {
                if (startSplitPara.getInt(paraIndex) < startInsertRows.getInt(paraIndex)) {
                    startInsertRows.put(paraIndex, startInsertRows.getInt(paraIndex) + 1);
                } else if (startSplitPara.getInt(paraIndex) == startInsertRows.getInt(paraIndex) && startSplitPara.getInt(textIndex) <= startInsertRows.getInt(textIndex)) {
                    startInsertRows.put(paraIndex, startInsertRows.getInt(paraIndex) + 1);
                    startInsertRows.put(textIndex, startInsertRows.getInt(textIndex) - startSplitPara.getInt(textIndex));
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertParaInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertRowsOp = null;
        JSONObject insertParaOp = null;

        if (OTUtils.isInsertRowsOperation(localOp) || OTUtils.isInsertCellsOperation(localOp)) {
            insertRowsOp = localOp;
            insertParaOp = extOp;
        } else {
            insertRowsOp = extOp;
            insertParaOp = localOp;
        }

        final JSONArray startInsertRows =OTUtils.getStartPosition(insertRowsOp);
        final JSONArray startInsertPara =OTUtils.getStartPosition(insertParaOp);

        int paraIndex = 0;
        int insertParaLength = startInsertPara.length();
        int rowIndex = 0;
        int insertRowsLength = startInsertRows.length();
        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);

        if (JSONHelper.compareNumberArrays(startInsertRows, startInsertPara) <= 0) {
            rowIndex = insertRowsLength - 1;

            if ((insertParaLength > insertRowsLength && JSONHelper.isEqual(startInsertPara, startInsertRows, insertRowsLength - 1))) {
                if (startInsertPara.getInt(rowIndex) >= startInsertRows.getInt(rowIndex)) { // position inside a following row/cell
                    startInsertPara.put(rowIndex, startInsertPara.getInt(rowIndex) + rowCount);
                }
            }
        } else {

            paraIndex = insertParaLength - 1;

            if (paraIndex == 0 || (insertRowsLength > insertParaLength && JSONHelper.isEqual(startInsertRows, startInsertPara, insertParaLength - 1))) {
                startInsertRows.put(paraIndex, startInsertRows.getInt(paraIndex) + 1);
            }
        }

        return null;
    }

    public static JSONObject handleSetAttrsInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isSetAttributesOperation(localOp) || OTUtils.isAnyUpdateFieldOperation(localOp) || OTUtils.isChangeCommentOperation(localOp)) {
            attrsOp = localOp;
            insertOp = extOp;
        } else {
            attrsOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        final JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrsOp, endAttrsOp };

        int insertPosLength = startInsertOp.length();
        int insertLength = 1;
        int textIndex = 0;

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {
                if (insertPosLength <= attrsPos.length() && JSONHelper.isEqual(startInsertOp, attrsPos, insertPosLength - 1)) {
                    textIndex = insertPosLength - 1;
                    if (startInsertOp.getInt(textIndex) <= attrsPos.getInt(textIndex)) { // insert shifts the setAttributes operation
                        if (OTUtils.isInsertTextOperation(insertOp)) { insertLength =OTUtils.getText(insertOp).length(); }
                        attrsPos.put(textIndex, attrsPos.getInt(textIndex) + insertLength);
                    }
                }
            }
        }

        return null;
    }

    public static JSONObject handleSetAttrsParaSplit(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject splitOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isSetAttributesOperation(localOp) || OTUtils.isAnyUpdateFieldOperation(localOp) || OTUtils.isChangeCommentOperation(localOp)) {
            attrsOp = localOp;
            splitOp = extOp;
        } else {
            attrsOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startSplitOp =OTUtils.getStartPosition(splitOp);
        final JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrsOp, endAttrsOp };

        int posLength = startSplitOp.length();
        int paraIndex = posLength - 2;
        int textIndex = paraIndex + 1;
        int index = 0;

        boolean hasEndPosition = (endAttrsOp != null);

        // whether the split happens inside the (text) attribute range (requires a second operation)
        boolean splitInAttrs = false;
        if (endAttrsOp != null && startAttrsOp.length() == posLength && endAttrsOp.length() == posLength && startAttrsOp.getInt(textIndex) < startSplitOp.getInt(textIndex) && endAttrsOp.getInt(textIndex) >= startSplitOp.getInt(textIndex)) {
            splitInAttrs = true;
        }

        // whether the splitted paragraph gets attributes assigned (in this case the end position is not specified or the same as the start position)
        boolean isSplitInAttributedParagraph = false;
        if (startAttrsOp != null && (endAttrsOp == null || JSONHelper.isEqual(startAttrsOp, endAttrsOp, startAttrsOp.length())) && startAttrsOp.length() == posLength - 1 && JSONHelper.isEqual(startSplitOp, startAttrsOp, posLength - 1)) {
            isSplitInAttributedParagraph = true;
        }

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {

                if (posLength <= attrsPos.length() && (posLength == 2 || JSONHelper.isEqual(startSplitOp, attrsPos, posLength - 2))) {
                    if (startSplitOp.getInt(paraIndex) < attrsPos.getInt(paraIndex)) {
                        attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) + 1);
                    } else if (startSplitOp.getInt(paraIndex) == attrsPos.getInt(paraIndex)) {
                        if (splitInAttrs) {
                            // the split happens inside the range of setAttributes
                            if (index == 0) {
                                // setAttributes operation must stay inside one paragraph -> new operation for following paragraph required
                                JSONObject newOperation = OTUtils.cloneJSONObject(attrsOp);
                                if (endAttrsOp != null) { endAttrsOp.put(textIndex, startSplitOp.getInt(textIndex) - 1); }
                                final JSONArray startNewOp =OTUtils.getStartPosition(newOperation);
                                final JSONArray endNewOp = OTUtils.optEndPosition(newOperation);
                                startNewOp.put(paraIndex, startNewOp.getInt(paraIndex) + 1);
                                endNewOp.put(paraIndex, endNewOp.getInt(paraIndex) + 1);
                                startNewOp.put(textIndex, 0);
                                endNewOp.put(textIndex, endNewOp.getInt(textIndex) - startSplitOp.getInt(textIndex));

                                if (OTUtils.isSetAttributesOperation(localOp)) {
                                    localOpsAfter = new JSONArray();
                                    localOpsAfter.put(newOperation); // inserting behind the current local operation
                                } else {
                                    externalOpsAfter = new JSONArray();
                                    externalOpsAfter.put(newOperation); // inserting behind the current external operation
                                }
                            }
                        } else if (startSplitOp.getInt(textIndex) <= attrsPos.getInt(textIndex)) {
                            attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) + 1);
                            attrsPos.put(textIndex, attrsPos.getInt(textIndex) - startSplitOp.getInt(textIndex));
                        }
                    }
                } else {
                    if (paraIndex == 0 || (attrsPos.optInt(paraIndex, -1) >= 0 && JSONHelper.isEqual(startSplitOp, attrsPos, paraIndex))) {
                        if (isSplitInAttributedParagraph) {
                            if (index == 0) {
                                JSONObject newOperation = OTUtils.cloneJSONObject(attrsOp);
                                final JSONArray startNewOp =OTUtils.getStartPosition(newOperation);
                                final JSONArray endNewOp = OTUtils.optEndPosition(newOperation);
                                startNewOp.put(paraIndex, startNewOp.getInt(paraIndex) + 1);
                                if (endNewOp != null) { endNewOp.put(paraIndex, endNewOp.getInt(paraIndex) + 1); }

                                if (OTUtils.isSetAttributesOperation(localOp)) {
                                    localOpsAfter = new JSONArray();
                                    localOpsAfter.put(newOperation); // inserting behind the current local operation
                                } else {
                                    externalOpsAfter = new JSONArray();
                                    externalOpsAfter.put(newOperation); // inserting behind the current external operation
                                }
                            }
                        } else {
                            if (startSplitOp.getInt(paraIndex) == attrsPos.getInt(paraIndex)) {
                                if (index == 1 || !hasEndPosition) {
                                    if (!hasEndPosition) { OTUtils.setEndPosition(attrsOp, JSONHelper.clonePosition(startAttrsOp)); }
                                    endAttrsOp = OTUtils.optEndPosition(attrsOp);
                                    endAttrsOp.put(paraIndex, endAttrsOp.getInt(paraIndex) + 1);
                                }
                            } else if (startSplitOp.getInt(paraIndex) < attrsPos.getInt(paraIndex)) {
                                attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) + 1);
                            }
                        }
                    }
                }

            }
            index++;
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    public static JSONObject handleSetAttrsSplitTable(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject splitOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isSetAttributesOperation(localOp) || OTUtils.isAnyUpdateFieldOperation(localOp) || OTUtils.isChangeCommentOperation(localOp)) {
            attrsOp = localOp;
            splitOp = extOp;
        } else {
            attrsOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startAttrs =OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrs = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrs, endAttrs };

        int posLength = startSplit.length();
        int tableIndex = posLength - 2;
        int rowIndex = tableIndex + 1;
        int index = 0;

        boolean hasEndPosition = (endAttrs != null);

        // whether the splitted table gets attributes assigned (in this case the end position is not specified or the same as the start position)
        boolean isSplitInAttributedTable = false;
        if (startAttrs != null && (endAttrs == null || JSONHelper.isEqual(startAttrs, endAttrs)) && startAttrs.length() == posLength - 1 && JSONHelper.isEqual(startSplit, startAttrs, posLength - 1)) {
            isSplitInAttributedTable = true;
        }

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {

                if (posLength <= attrsPos.length() && (posLength == 2 || JSONHelper.isEqual(startSplit, attrsPos, posLength - 2))) {
                    if (startSplit.getInt(tableIndex) < attrsPos.getInt(tableIndex)) {
                        attrsPos.put(tableIndex, attrsPos.getInt(tableIndex) + 1);
                    } else if (startSplit.getInt(tableIndex) == attrsPos.getInt(tableIndex)) {
                        if (startSplit.getInt(rowIndex) <= attrsPos.getInt(rowIndex)) {
                            attrsPos.put(tableIndex, attrsPos.getInt(tableIndex) + 1);
                            attrsPos.put(rowIndex, attrsPos.getInt(rowIndex) - startSplit.getInt(rowIndex));
                        }
                    }
                } else {
                    if (tableIndex == 0 || (attrsPos.optInt(tableIndex, -1) >= 0 && JSONHelper.isEqual(startSplit, attrsPos, tableIndex))) {
                        if (isSplitInAttributedTable) {
                            if (index == 0) {
                                JSONObject newOperation = OTUtils.cloneJSONObject(attrsOp);
                                final JSONArray startNewOp =OTUtils.getStartPosition(newOperation);
                                final JSONArray endNewOp = OTUtils.optEndPosition(newOperation);
                                startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) + 1);
                                if (endNewOp != null) { endNewOp.put(tableIndex, endNewOp.getInt(tableIndex) + 1); }

                                if (OTUtils.isSetAttributesOperation(localOp)) {
                                    localOpsAfter = new JSONArray();
                                    localOpsAfter.put(newOperation); // inserting behind the current local operation
                                } else {
                                    externalOpsAfter = new JSONArray();
                                    externalOpsAfter.put(newOperation); // inserting behind the current external operation
                                }
                            }
                        } else {
                            if (startSplit.getInt(tableIndex) == attrsPos.getInt(tableIndex)) {
                                if (index == 1 || !hasEndPosition) {
                                    if (!hasEndPosition) { OTUtils.setEndPosition(attrsOp, JSONHelper.clonePosition(startAttrs)); }
                                    endAttrs = OTUtils.optEndPosition(attrsOp);
                                    endAttrs.put(tableIndex, endAttrs.getInt(tableIndex) + 1);
                                }
                            } else if (startSplit.getInt(tableIndex) < attrsPos.getInt(tableIndex)) {
                                attrsPos.put(tableIndex, attrsPos.getInt(tableIndex) + 1);
                            }
                        }
                    }
                }

            }
            index++;
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    public static JSONObject handleSetAttrsInsertPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isSetAttributesOperation(localOp) || OTUtils.isAnyUpdateFieldOperation(localOp) || OTUtils.isChangeCommentOperation(localOp)) {
            attrsOp = localOp;
            insertOp = extOp;
        } else {
            attrsOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrsOp, endAttrsOp };

        int posLength = startInsertOp.length();
        int paraIndex = posLength - 1;

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {
                if (paraIndex == 0 || (attrsPos.optInt(paraIndex, -1) >= 0 && JSONHelper.isEqual(startInsertOp, attrsPos, paraIndex))) {
                    if (startInsertOp.getInt(paraIndex) <= attrsPos.getInt(paraIndex)) {
                        attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) + 1);
                    }
                }
            }
        }

        return null;
    }

    public static JSONObject handleSetAttrsInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isInsertColumnOperation(localOp)) {
            attrsOp = extOp;
            insertOp = localOp;
        } else {
            attrsOp = localOp;
            insertOp = extOp;
        }

        final JSONArray startInsert = OTUtils.getStartPosition(insertOp);
        final JSONArray startAttrs = OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrs = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrs, endAttrs };

        int insertLength = startInsert.length();
        int tableIndex = insertLength - 1;
        int columnIndex = tableIndex + 2;

        String mode = OTUtils.getInsertModeProperty(insertOp);

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {
                if(JSONHelper.isEqual(startInsert, attrsPos, insertLength)) {
                    if(startAttrs.length()==insertLength) {
                        // check for tableGrid property
                        final JSONObject attrs = attrsOp.optJSONObject(OCKey.ATTRS.value());
                        if(attrs!=null) {
                            final JSONObject tableProperties = attrs.optJSONObject(OCKey.TABLE.value());
                            if(tableProperties!=null) {
                                final JSONArray tableGrid = tableProperties.optJSONArray(OCKey.TABLE_GRID.value());
                                if(tableGrid!=null) {
                                    tableProperties.remove(OCKey.TABLE_GRID.value());
                                    if(tableProperties.isEmpty()) {
                                        attrs.remove(OCKey.TABLE.value());
                                    }
                                }
                            }
                        }
                    }
                    else if (attrsPos.optInt(columnIndex, -1) >= 0) {
                        if ((attrsPos.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertOp)) || (OTUtils.isInsertModeBefore(mode) && attrsPos.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertOp))) {
                            attrsPos.put(columnIndex, attrsPos.getInt(columnIndex) + 1);
                        }
                    }
                }
            }
        }

        return null;
    }

    public static JSONObject handleSetAttrsDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject deleteOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            attrsOp = extOp;
            deleteOp = localOp;
        } else {
            attrsOp = localOp;
            deleteOp = extOp;
        }

        final JSONArray startDelete = OTUtils.getStartPosition(deleteOp);
        final JSONArray startAttrs = OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrs = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrs, endAttrs };

        int deleteLength = startDelete.length();
        int tableIndex = deleteLength - 1;
        int columnIndex = tableIndex + 2;

        final int startGridProperty = OTUtils.getStartGridProperty(deleteOp);
        final int endGridProperty = OTUtils.getEndGridProperty(deleteOp);
        final int deleteColumns = endGridProperty - startGridProperty + 1;

        for (JSONArray attrsPos: allPos) {
            if(attrsPos != null) {
                if(JSONHelper.isEqual(startDelete, attrsPos, deleteLength)) {
                    // check for tableGrid property
                    if(startAttrs.length()==deleteLength) {
                        final JSONObject attrs = attrsOp.optJSONObject(OCKey.ATTRS.value());
                        if(attrs!=null) {
                            final JSONObject tableProperties = attrs.optJSONObject(OCKey.TABLE.value());
                            if(tableProperties!=null) {
                                final JSONArray tableGrid = tableProperties.optJSONArray(OCKey.TABLE_GRID.value());
                                if(tableGrid!=null) {
                                    for(int i = 0; i < deleteColumns; i++) {
                                        tableGrid.remove(startGridProperty);
                                    }
                                }
                            }
                        }
                    }
                    else if (attrsPos.optInt(columnIndex, -1) >= 0) {
                        if (attrsPos.getInt(columnIndex) >= startGridProperty) {
                            if (attrsPos.getInt(columnIndex) > endGridProperty) {
                                attrsPos.put(columnIndex, attrsPos.getInt(columnIndex) - deleteColumns);
                            } else {
                                OTUtils.setOperationRemoved(attrsOp);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public static JSONObject handleSetAttrsInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject attrsOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isInsertCellsOperation(localOp) || OTUtils.isInsertRowsOperation(localOp)) {
            attrsOp = extOp;
            insertOp = localOp;
        } else {
            attrsOp = localOp;
            insertOp = extOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrsOp, endAttrsOp };

        int posLength = startInsertOp.length();
        int rowIndex = posLength - 1;
        int rowCount = OTUtils.getCountProperty(insertOp, 1);

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {
                if (attrsPos.length() >= startInsertOp.length() && JSONHelper.isEqual(startInsertOp, attrsPos, rowIndex)) {
                    if (startInsertOp.getInt(rowIndex) <= attrsPos.getInt(rowIndex)) {
                        attrsPos.put(rowIndex, attrsPos.getInt(rowIndex) + rowCount);
                    }
                }
            }
        }

        return null;
    }

    public static JSONObject handleParaMergeInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
            mergeOp = localOp;
            insertOp = extOp;
        } else {
            mergeOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);

        int paraIndex = 0;
        int textIndex = 0;
        int posLength = 0;
        int insertCharLength = 2;
        int insertLength = 1;

        boolean isParaMerge = true;
        if (OTUtils.isMergeTableOperation(mergeOp)) { isParaMerge = false; }

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

        if (startMergeOp.length() <= startInsertOp.length() - 1) {
            posLength = startMergeOp.length(); // both paragraph positions have the same length (insert contains additionally the text position)
            if (posLength == 1 || JSONHelper.isEqual(startMergeOp, startInsertOp, posLength - 1)) {
                paraIndex = startMergeOp.length() - 1;
                textIndex = paraIndex + 1;
                if (startMergeOp.getInt(paraIndex) < (startInsertOp.getInt(paraIndex) - 1)) { // handling mergeParagraph at [1] and insertText at [3,14], not [2,14]
                    startInsertOp.put(paraIndex, startInsertOp.getInt(paraIndex) - 1);
                } else if (startMergeOp.getInt(paraIndex) == (startInsertOp.getInt(paraIndex) - 1)) { // handling mergeParagraph at [1] and insertText at [2,14]
                    // the merge is always before the text insertion
                    startInsertOp.put(paraIndex, startInsertOp.getInt(paraIndex) - 1);
                    startInsertOp.put(textIndex, startInsertOp.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                } else if (startMergeOp.getInt(paraIndex) == startInsertOp.getInt(paraIndex)) {
                    if (isParaMerge && (startInsertOp.length() == startMergeOp.length() + 1) && startInsertOp.getInt(textIndex) <= mergeOp.optInt(lengthProperty, 0)) {
                        if (OTUtils.isInsertTextOperation(insertOp)) { insertLength =OTUtils.getText(insertOp).length(); }
                        mergeOp.put(lengthProperty, mergeOp.optInt(lengthProperty, 0) + insertLength);
                    } else {
                        // this should never happen
                    }
                }
            }
        } else {
            insertCharLength = startInsertOp.length(); // the merged paragraph might be inside a drawing
            textIndex = insertCharLength - 1;
            if (JSONHelper.isEqual(startInsertOp, startMergeOp, insertCharLength - 1)) {
                if (startInsertOp.getInt(textIndex) <= startMergeOp.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertOp)) { insertLength =OTUtils.getText(insertOp).length(); }
                    startMergeOp.put(textIndex, startMergeOp.getInt(textIndex) + insertLength);
                }
            }
        }

        return null;
    }

    public static JSONObject handleSetAttrsParaMerge(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject attrsOp = null;

        if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
            mergeOp = localOp;
            attrsOp = extOp;
        } else {
            mergeOp = extOp;
            attrsOp = localOp;
        }

        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);
        final JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);

        JSONArray[] allPos = { startAttrsOp, endAttrsOp };

        int paraIndex = 0;
        int textIndex = 0;
        int posLength = startMergeOp.length();

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

        for (JSONArray attrsPos: allPos) {
            if (attrsPos != null) {
                if ((posLength == attrsPos.length() - 1) && (posLength == 1 || JSONHelper.isEqual(startMergeOp, attrsPos, posLength - 1))) {
                    paraIndex = posLength - 1;
                    textIndex = paraIndex + 1;
                    if (startMergeOp.getInt(paraIndex) < attrsPos.getInt(paraIndex) - 1) {
                        attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) - 1); // decreasing position of setAttributes operation
                    } else if (startMergeOp.getInt(paraIndex) == attrsPos.getInt(paraIndex) - 1) {
                        // the split happens in the paragraph directly before the setAttributes position
                        attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) - 1);
                        attrsPos.put(textIndex, attrsPos.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                    }
                } else {
                    // both positions have or have NOT the same length for the common paragraph (setAttributes length is pretty arbitrary, must not be a text selection)
                    paraIndex = posLength - 1;
                    textIndex = paraIndex + 1;
                    if (paraIndex == 0 || (attrsPos.optInt(paraIndex, -1) >= 0 && JSONHelper.isEqual(startMergeOp, attrsPos, paraIndex))) {
                        // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the attributes op
                        if (startMergeOp.getInt(paraIndex) < attrsPos.getInt(paraIndex) - 1) {
                            attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) - 1); // decrease the paragraph position of the setAttributes operation
                        } else if (startMergeOp.getInt(paraIndex) == attrsPos.getInt(paraIndex) - 1) {
                            attrsPos.put(paraIndex, attrsPos.getInt(paraIndex) - 1);
                            if (attrsPos.optInt(textIndex, -1) > -1) { attrsPos.put(textIndex, attrsPos.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0)); }
                        } else if (startMergeOp.getInt(paraIndex) == attrsPos.getInt(paraIndex)) {
                            // What happens in setAttributes([6]) and mergeParagraph at (6,5), for example for background color of paragraph?
                            // -> this should not be a problem: First merge and following setting of background color has the same result as
                            //    first setting background color and then merge the modified paragraph with the following paragraph
                            // -> nothing to do
                        }
                    }
                }
            }
        }

        return null;
    }

    private static JSONObject handleMergeParaInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject insertRowsOp = null;

        if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
            mergeOp = localOp;
            insertRowsOp = extOp;
        } else {
            mergeOp = extOp;
            insertRowsOp = localOp;
        }

        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);
        final JSONArray startInsertRowsOp =OTUtils.getStartPosition(insertRowsOp);

        int paraIndex = 0;
        int textIndex = 0;
        int rowIndex = 0;
        int insertRowsLength = startInsertRowsOp.length();
        int mergeParaLength = startMergeOp.length();
        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);

        boolean isParaMerge = true;
        if (OTUtils.isMergeTableOperation(mergeOp)) { isParaMerge = false; }

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

        if (JSONHelper.compareNumberArrays(startInsertRowsOp, startMergeOp) <= 0) {
            rowIndex = insertRowsLength - 1;

            if ((mergeParaLength >= insertRowsLength && JSONHelper.isEqual(startInsertRowsOp, startMergeOp, insertRowsLength - 1))) {
                if (startMergeOp.getInt(rowIndex) >= startInsertRowsOp.getInt(rowIndex)) {
                    startMergeOp.put(rowIndex, startMergeOp.getInt(rowIndex) + rowCount);
                }
            }
        } else {
            paraIndex = mergeParaLength - 1;
            textIndex = paraIndex + 1;

            if ((paraIndex == 0) || (insertRowsLength >= mergeParaLength && JSONHelper.isEqual(startInsertRowsOp, startMergeOp, paraIndex))) {
                if (startMergeOp.getInt(paraIndex) < startInsertRowsOp.getInt(paraIndex)) {
                    startInsertRowsOp.put(paraIndex, startInsertRowsOp.getInt(paraIndex) - 1);
                    if (startMergeOp.getInt(paraIndex) == startInsertRowsOp.getInt(paraIndex)) { // paragraph position already decreased
                        startInsertRowsOp.put(textIndex, startInsertRowsOp.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                        if (!isParaMerge && insertRowsLength == (mergeParaLength + 1) && OTUtils.hasReferenceRowProperty(insertRowsOp) && mergeOp.optInt(lengthProperty, 0) > 0) { // adapting the reference row for insertRows operation
                            final int newReferenceRow = OTUtils.getReferenceRowProperty(insertRowsOp) + mergeOp.optInt(lengthProperty, 0);
                            if(newReferenceRow >= 0) {
                                OTUtils.setReferenceRowProperty(insertRowsOp, newReferenceRow);
                            }
                            else {
                                insertRowsOp.remove(OCKey.REFERENCE_ROW.value());
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public static JSONObject handleMergeParaInsertPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeParaOp = null;
        JSONObject insertParaOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
            mergeParaOp = localOp;
            insertParaOp = extOp;
        } else {
            mergeParaOp = extOp;
            insertParaOp = localOp;
        }

        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeParaOp);
        final JSONArray startInsertParaOp =OTUtils.getStartPosition(insertParaOp);

        int paraIndex = 0;
        int textIndex = 0;
        int insertParaLength = startInsertParaOp.length();
        int mergeParaLength = startMergeOp.length();

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeParaOp);

        if (mergeParaLength == insertParaLength) {
            if (insertParaLength == 1 || JSONHelper.isEqual(startMergeOp, startInsertParaOp, insertParaLength - 1)) {
                paraIndex = insertParaLength - 1;
                if (startInsertParaOp.getInt(paraIndex) <= startMergeOp.getInt(paraIndex)) {
                    startMergeOp.put(paraIndex, startMergeOp.getInt(paraIndex) + 1);
                } else if (startInsertParaOp.getInt(paraIndex) == startMergeOp.getInt(paraIndex) + 1) {
                    // local mergeParagraph [0] and remote insertTable [1]
                    // splitting paragraph again and then insert new paragraph/table
                    JSONObject newOperation = OTUtils.cloneJSONObject(mergeParaOp);
                    OTUtils.setOperationName(newOperation, OTUtils.getSplitNamePropertyAfterMerge(mergeParaOp));
                    JSONArray startNewOperation =OTUtils.getStartPosition(newOperation);
                    startNewOperation.add(startNewOperation.length(), newOperation.optInt(lengthProperty, 0));
                    newOperation.remove(lengthProperty);

                    if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
                        externalOpsBefore = new JSONArray();
                        externalOpsBefore.put(newOperation); // inserting before the current external operation
                    } else {
                        localOpsBefore = new JSONArray();
                        localOpsBefore.put(newOperation); // inserting before the current internal operation
                    }
                    OTUtils.setOperationRemoved(mergeParaOp); // ignoring the merge operation locally and externally
                } else {
                    startInsertParaOp.put(paraIndex, startInsertParaOp.getInt(paraIndex) - 1);
                }
            }
        } else {

            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (insertParaLength < mergeParaLength) {
                shortOp = insertParaOp;
                longOp = mergeParaOp;
            } else {
                shortOp = mergeParaOp;
                longOp = insertParaOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            paraIndex = startShort.length() - 1;
            textIndex = paraIndex + 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                if (startShort.getInt(paraIndex) <= startLong.getInt(paraIndex)) {
                    if (OTUtils.isMergeParagraphOperation(shortOp) || OTUtils.isMergeTableOperation(shortOp)) {
                        if (startShort.getInt(paraIndex) < startLong.getInt(paraIndex)) {
                            startLong.put(paraIndex, startLong.getInt(paraIndex) - 1);
                            if (startShort.getInt(paraIndex) == startLong.getInt(paraIndex)) { // insertParagraph in a text frame (already decreased yet)
                                startLong.put(textIndex, startLong.getInt(textIndex) + shortOp.optInt(lengthProperty, 0));
                            }
                        }
                    } else {
                        startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleParaSplitParaMerge(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject splitOp = null;

        if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
            mergeOp = localOp;
            splitOp = extOp;
        } else {
            mergeOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);
        final JSONArray startSplitOp =OTUtils.getStartPosition(splitOp);

        int paraIndex = 0;
        int textIndex = 0;
        int posLength = 0;

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

        if (startMergeOp.length() == startSplitOp.length() - 1) {
            posLength = startMergeOp.length(); // both paragraph positions have the same length, splitParagraph contains additionally the text position
            if (posLength == 1 || JSONHelper.isEqual(startMergeOp, startSplitOp, posLength - 1)) {
                paraIndex = posLength - 1;
                textIndex = posLength;
                if (startMergeOp.getInt(paraIndex) < (startSplitOp.getInt(paraIndex) - 1)) { // handling mergeParagraph at [1] and splitParagraph at [3,14], not [2,14]
                    startSplitOp.put(paraIndex, startSplitOp.getInt(paraIndex) - 1); // decreasing position of split operation
                } else if (startMergeOp.getInt(paraIndex) == (startSplitOp.getInt(paraIndex) - 1)) { // handling mergeParagraph at [1] and splitParagraph at [2,14]
                    // the merge is always before the split operation in the next paragraph
                    startSplitOp.put(paraIndex, startSplitOp.getInt(paraIndex) - 1);
                    startSplitOp.put(textIndex, startSplitOp.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                } else if (startMergeOp.getInt(paraIndex) == startSplitOp.getInt(paraIndex)) { // handling mergeParagraph at [1] (length 8) and splitParagraph at [1,4]
                    if (mergeOp.optInt(lengthProperty, 0) >= startSplitOp.getInt(textIndex)) {
                        startMergeOp.put(paraIndex, startMergeOp.getInt(paraIndex) + 1);
                        mergeOp.put(lengthProperty, mergeOp.optInt(lengthProperty, 0) - startSplitOp.getInt(textIndex));
                    } else {
                        // this is a case, that never should happen: mergeParagraph at [1] with length 4 and splitParagraph at [1,8]
                    }
                } else {
                    startMergeOp.put(paraIndex, startMergeOp.getInt(paraIndex) + 1); // in all other cases the index at the paragraph for the merge operation needs to be increased because of the split operation
                }
            }
        } else {
            // both paragraph positions have NOT the same level -> both operations can influence each other
            if (startMergeOp.length() < (startSplitOp.length() - 1)) {
                paraIndex = startMergeOp.length() - 1;
                textIndex = paraIndex + 1;

                if (paraIndex == 0 || JSONHelper.isEqual(startMergeOp, startSplitOp, paraIndex)) {
                    // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the split op
                    if (startMergeOp.getInt(paraIndex) < startSplitOp.getInt(paraIndex) - 1) {
                        startSplitOp.put(paraIndex, startSplitOp.getInt(paraIndex) - 1); // decrease the paragraph position of the split operation
                    } else if (startMergeOp.getInt(paraIndex) == startSplitOp.getInt(paraIndex) - 1) {
                        startSplitOp.put(paraIndex, startSplitOp.getInt(paraIndex) - 1);;
                        startSplitOp.put(textIndex, startSplitOp.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                    }
                }
            } else {
                // the split operation might influence a longer merge operation
                paraIndex = startSplitOp.length() - 2;
                textIndex = paraIndex + 1;

                if (paraIndex == 0 || JSONHelper.isEqual(startMergeOp, startSplitOp, paraIndex)) {
                    // the split paragraph is top level (index is 0) or the parent of the paragraph of the split operation is an ancestor of the merge op
                    if (startSplitOp.getInt(paraIndex) < startMergeOp.getInt(paraIndex)) {
                        startMergeOp.put(paraIndex, startMergeOp.getInt(paraIndex) + 1); // increase the paragraph position of the merge operation
                    } else if (startSplitOp.getInt(paraIndex) == startMergeOp.getInt(paraIndex) && startSplitOp.getInt(textIndex) <= startMergeOp.getInt(textIndex)) {
                        startMergeOp.put(paraIndex, startMergeOp.getInt(paraIndex) + 1);; // merge inside a text frame
                        startMergeOp.put(textIndex, startMergeOp.getInt(textIndex) - startSplitOp.getInt(textIndex));
                    }
                }
            }
        }

        return null;
    }

    // helper function to modify the start/end position of the delete operation for insertChar operations
    private static void modifyDeletePositionFromInsertChar(final JSONArray deletePos, final JSONObject insertOp) throws JSONException {
        if (deletePos != null) {
            JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
            int insertCharLength = startInsertOp.length();
            int insertLength = 1;
            int textIndex = 0;
            // is the position of the insert operation in the same paragraph as the start/end position of the delete operation?
            if ((insertCharLength <= deletePos.length()) && JSONHelper.isEqual(startInsertOp, deletePos, insertCharLength - 1)) {
                textIndex = insertCharLength - 1;
                if (startInsertOp.getInt(textIndex) <= deletePos.getInt(textIndex)) { // insert shifts delete, if the positions are equal
                    if (OTUtils.isInsertTextOperation(insertOp)) { insertLength =OTUtils.getText(insertOp).length(); }
                    deletePos.put(textIndex, deletePos.getInt(textIndex) + insertLength); // the start/end position of the delete operation needs to be increased
                }
            }
        }
    }

    public static JSONObject handleInsertCharDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        JSONArray[] allPos = { startDeleteOp, endDeleteOp };

        // Case 1: insert is before the start position (or equal to the start postion) -> insert shifts both delete positions.
        // Case 2: insert is between start and end position -> insert operation needs to be removed.
        // Case 3: insert is after the end position -> delete shifts the insert position.

        if (JSONHelper.compareNumberArrays(startInsertOp, startDeleteOp) <= 0) {
            // 1. the specified position is before the delete start position (or it is the delete start position)
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromInsertChar(deletePos, insertOp); };
        } else if (JSONHelper.ancestorRemoved(deleteOp, insertOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startInsertOp, endDeleteOp) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (endDeleteOp != null) { modifyDeletePositionFromInsertChar(endDeleteOp, insertOp); } // only modifying the end position
            OTUtils.setOperationRemoved(insertOp); // setting marker that this operation is not used.

        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startInsertOp);
            for (int index = 0; index < startInsertOp.length(); index += 1) { startInsertOp.put(index, startInsertOp.getInt(index) - deleteArray.getInt(index)); }
        }

        return null;
    }

    // helper function to modify the start/end position of the delete operation caused by insertParagraph/insertTable
    private static void modifyDeletePositionFromInsertPara(JSONArray deletePos, final JSONObject insertOp) throws JSONException {
        if (deletePos != null) {
            JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
            int insertPosLength = startInsertOp.length();
            int paraIndex = insertPosLength - 1;

            if (paraIndex == 0 || (deletePos.optInt(paraIndex, -1) > -1 && JSONHelper.isEqual(startInsertOp, deletePos, paraIndex))) {
                // the insertParagraph/insertTable is top level (index is 0) or the parent of the paragraph of the insert operation is an ancestor of the delete op
                if (startInsertOp.getInt(paraIndex) <= deletePos.getInt(paraIndex)) {
                    deletePos.put(paraIndex, deletePos.getInt(paraIndex) + 1);
                }
            }
        }
    }

    public static JSONObject handleInsertParaDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        // Case 1: insertPara/insertTable is before the start position (or equal to the start position) -> insertPara/insertTable shifts both delete positions.
        // Case 2: insertPara/insertTable is between start and end position -> insert operation needs to be removed
        // Case 3: insertPara/insertTable is after the end position -> delete shifts the insertPara/insertTable position.

        if (JSONHelper.compareNumberArrays(startInsertOp, startDeleteOp) <= 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)
            JSONArray[] allPos = { startDeleteOp, endDeleteOp };
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromInsertPara(deletePos, insertOp); };
        } else if (JSONHelper.ancestorRemoved(deleteOp, insertOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startInsertOp, endDeleteOp) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (endDeleteOp != null) { modifyDeletePositionFromInsertPara(endDeleteOp, insertOp); } // expanding the delete operation
            OTUtils.setOperationRemoved(insertOp); // setting marker that the operation is not applied locally

        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startInsertOp);
            for (int index = 0; index < startInsertOp.length(); index += 1) { startInsertOp.put(index, startInsertOp.getInt(index) - deleteArray.getInt(index)); }
        }

        return null;
    }

    // helper function to modify the start/end position of the delete operation
    private static void modifyDeletePositionFromInsertRows(JSONArray deletePos, final JSONObject insertOp) throws JSONException {
        if (deletePos != null) {
            JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
            int insertPosLength = startInsertOp.length();
            int rowIndex = insertPosLength - 1;

            if (deletePos.optInt(rowIndex, -1) > -1 && JSONHelper.isEqual(startInsertOp, deletePos, rowIndex)) {
                if (startInsertOp.getInt(rowIndex) <= deletePos.getInt(rowIndex)) {
                    deletePos.put(rowIndex, deletePos.getInt(rowIndex) + OTUtils.getCountProperty(insertOp, 1));
                }
            }
        }
    }

    public static JSONObject handleInsertRowsDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        // Case 1: insertRows is before the start position (or equal to the start position) -> insertRows shifts both delete positions.
        // Case 2: insertRows is between start and end position -> insert operation needs to be removed
        // Case 3: insertRows is after the end position -> delete shifts the insertRows position.

        if (JSONHelper.compareNumberArrays(startInsertOp, startDeleteOp) <= 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)
            JSONArray[] allPos = { startDeleteOp, endDeleteOp };
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromInsertRows(deletePos, insertOp); };
        } else if (JSONHelper.ancestorRemoved(deleteOp, insertOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startInsertOp, endDeleteOp) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (endDeleteOp != null) { modifyDeletePositionFromInsertRows(endDeleteOp, insertOp); } // expanding the delete operation
            OTUtils.setOperationRemoved(insertOp); // setting marker that the operation is not applied locally
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startInsertOp);
            for (int index = 0; index < startInsertOp.length(); index += 1) { startInsertOp.put(index, startInsertOp.getInt(index) - deleteArray.getInt(index)); }

            final int rowIndex = startInsertOp.length() - 1;
            final int referenceRow = insertOp.optInt(OCKey.REFERENCE_ROW.value(), -1);
            if (referenceRow > -1 && deleteArray.getInt(rowIndex) > 0) {
                if (endPosition.getInt(rowIndex) < referenceRow) {
                    insertOp.put(OCKey.REFERENCE_ROW.value(), referenceRow - deleteArray.getInt(rowIndex)); // reducing the value for the reference row
                } else if (startDeleteOp.getInt(rowIndex) <= referenceRow) {
                    insertOp.remove(OCKey.REFERENCE_ROW.value()); // the reference row was deleted
                }
            }
        }
        return null;
    }

    // helper function to modify the start/end position of the delete operation
    private static void modifyDeletePositionFromInsertColumn(JSONArray deletePos, final JSONObject insertOp) throws JSONException {
        if (deletePos != null) {
            JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
            int insertPosLength = startInsertOp.length();
            int tableIndex = insertPosLength - 1;
            int columnIndex = tableIndex + 2;
            String mode = OTUtils.getInsertModeProperty(insertOp);

            if (deletePos.optInt(columnIndex, -1) > -1 && JSONHelper.isEqual(startInsertOp, deletePos, tableIndex)) {
                if ((deletePos.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertOp)) || (OTUtils.isInsertModeBefore(mode) && deletePos.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertOp))) {
                    deletePos.put(columnIndex, deletePos.getInt(columnIndex) + 1);
                }
            }
        }
    }

    public static JSONObject handleInsertColumnDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startInsertOp =OTUtils.getStartPosition(insertOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        // Case 1: insertColumn is before the start position (or equal to the start position) -> insertColumn shifts both delete positions.
        // Case 2: insertColumn is between start and end position -> insert operation needs to be removed
        // Case 3: insertColumn is after the end position -> delete shifts the insertColumn position.

        if (JSONHelper.compareNumberArrays(startInsertOp, startDeleteOp) < 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)
            JSONArray[] allPos = { startDeleteOp, endDeleteOp };
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromInsertColumn(deletePos, insertOp); };
        } else if (JSONHelper.isEqual(startInsertOp, startDeleteOp) || JSONHelper.ancestorRemoved(deleteOp, insertOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startInsertOp, endDeleteOp) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (endDeleteOp != null) { modifyDeletePositionFromInsertColumn(endDeleteOp, insertOp); } // expanding the delete operation
            OTUtils.setOperationRemoved(insertOp); // setting marker that the operation is not applied locally
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startInsertOp);
            for (int index = 0; index < startInsertOp.length(); index += 1) { startInsertOp.put(index, startInsertOp.getInt(index) - deleteArray.getInt(index)); }
        }

        return null;
    }

    // helper function to modify the start/end position of the delete operation
    private static void modifyDeletePositionFromDeleteColumns(JSONArray deletePos, final JSONObject deleteOp, final JSONObject deleteColumnsOp) throws JSONException {
        if (deletePos != null) {
            JSONArray startDeleteColumns =OTUtils.getStartPosition(deleteColumnsOp);
            int startDeleteColumnsLength = startDeleteColumns.length();
            int tableIndex = startDeleteColumnsLength - 1;
            int columnIndex = tableIndex + 2;
            int deleteColumns = OTUtils.getEndGridProperty(deleteColumnsOp) - OTUtils.getStartGridProperty(deleteColumnsOp) + 1;

            if (deletePos.optInt(columnIndex, -1) > -1 && JSONHelper.isEqual(startDeleteColumns, deletePos, tableIndex)) {
                if (deletePos.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteColumnsOp)) {
                    if (deletePos.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteColumnsOp)) {
                        deletePos.put(columnIndex, deletePos.getInt(columnIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(deleteOp);
                    }
                }
            }
        }
    }

    public static JSONObject handleDeleteColumnsDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject deleteColumnsOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            deleteColumnsOp = extOp;
        } else {
            deleteOp = extOp;
            deleteColumnsOp = localOp;
        }

        final JSONArray startDeleteColumnsOp =OTUtils.getStartPosition(deleteColumnsOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        // Case 1: deleteColumns is before the start position (or equal to the start position) -> deleteColumns shifts both delete positions.
        // Case 2: deleteColumns is between start and end position -> deleteColumns operation needs to be removed
        // Case 3: deleteColumns is after the end position -> delete shifts the deleteColumns position.

        if (JSONHelper.compareNumberArrays(startDeleteColumnsOp, startDeleteOp) < 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)
            JSONArray[] allPos = { startDeleteOp, endDeleteOp };
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromDeleteColumns(deletePos, deleteOp, deleteColumnsOp); };
        } else if (JSONHelper.isEqual(startDeleteColumnsOp, startDeleteOp) || JSONHelper.ancestorRemoved(deleteOp, deleteColumnsOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startDeleteColumnsOp, endDeleteOp) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (endDeleteOp != null) { modifyDeletePositionFromDeleteColumns(endDeleteOp, deleteOp, deleteColumnsOp); } // expanding the delete operation
            OTUtils.setOperationRemoved(deleteColumnsOp); // setting marker that the operation is not applied locally
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startDeleteColumnsOp);
            for (int index = 0; index < startDeleteColumnsOp.length(); index += 1) { startDeleteColumnsOp.put(index, startDeleteColumnsOp.getInt(index) - deleteArray.getInt(index)); }
        }

        return null;
    }

    // helper function that checks if the merge is still required. This is the case for delete operations
    // like delete [0,4] to [1,2], because this operation does NOT remove a single paragraph, it only removes text.
    // Therefore an external merge operation cannot be ignored.
    private static boolean mergeRequired(JSONArray startDeleteOp, JSONArray endDeleteOp, JSONArray startMergeOp) throws JSONException {

        boolean mergeRequired = false;
        int deleteStartLength = startDeleteOp.length();
        int deleteEndLength = 0;
        if (endDeleteOp != null) { deleteEndLength = endDeleteOp.length(); }
        int mergeStartLength = startMergeOp.length();

        if (deleteStartLength == deleteEndLength && deleteStartLength == mergeStartLength + 1) {
            int paraIndex = mergeStartLength - 1;
            if (endDeleteOp != null && startDeleteOp.getInt(paraIndex) == endDeleteOp.getInt(paraIndex) - 1) { // the direct following paragraph
                mergeRequired = true;
            }
        }
        return mergeRequired;
    }

    public static JSONObject handleParaMergeDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject mergeOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            mergeOp = extOp;
        } else {
            deleteOp = extOp;
            mergeOp = localOp;
        }

        final JSONArray startMergeOp =OTUtils.getStartPosition(mergeOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        boolean isRemovalOfFollowingParagraph = false; // whether a remote merge operation can be ignored because the paragraph is removed locally

        String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);

        // Case 1: merge is before the start position (or equal to the start position) -> merge shifts both delete positions.
        // Case 2: merge is between start and end position -> split operation needs to be removed (also handling: delete removes the parent of split (delete[2] and mergeParagraph[2,5,6,7]))
        // Case 3: merge is after the end position -> delete shifts the merge position.

        JSONArray mergePos = JSONHelper.clonePosition(startMergeOp); // a merge position that includes the text position
        int paralength = mergeOp.optInt(lengthProperty, 0);
        mergePos.add(startMergeOp.length(), paralength); // -> adding the paragraph length, so that a text position is generated
        JSONObject mergeOpHelper = new JSONObject(); // a helper object containing this 'artificial' position as start position
        OTUtils.setStartPosition(mergeOpHelper, mergePos);

        if (JSONHelper.compareNumberArrays(mergePos, startDeleteOp) <= 0) {
            // 1. the merge position is before the delete start position (or it is exactly the delete start position)

            int lastIdx = startDeleteOp.length() - 1;
            if (startDeleteOp.length() == startMergeOp.length() && JSONHelper.isEqual(startDeleteOp, startMergeOp, lastIdx) && (startDeleteOp.getInt(lastIdx) - 1) == startMergeOp.getInt(lastIdx)) {
                isRemovalOfFollowingParagraph = true;
            }

            if (isRemovalOfFollowingParagraph) {
                OTUtils.setOperationRemoved(mergeOp); // setting marker for removal -> but the delete operation gets a previous split operation

                // adding a new split operation in front of the delete operation
                JSONObject newOperation = OTUtils.cloneJSONObject(deleteOp);
                OTUtils.setOperationName(newOperation, OTUtils.getSplitNamePropertyAfterMerge(mergeOp));
                JSONArray startNewOperation = JSONHelper.clonePosition(startMergeOp);
                startNewOperation.put(mergeOp.getInt(lengthProperty));
                OTUtils.setStartPosition(newOperation, startNewOperation);
                OTUtils.removeEndPosition(newOperation);

                if (OTUtils.isDeleteOperation(extOp)) {
                    externalOpsBefore = new JSONArray();
                    externalOpsBefore.put(newOperation); // inserting before the current external operation
                } else {
                    // the split operation must also be added to the local operation stack
                    localOpsBefore = new JSONArray();
                    localOpsBefore.put(newOperation); // inserting before the current local operation
                }

            } else {
                JSONArray[] allPos = { startDeleteOp, OTUtils.optEndPosition(deleteOp) };
                int index = 0;
                for (JSONArray deletePos: allPos) {
                    modifyDeletePositionFromMergePara(deletePos, mergeOp, deleteOp, index);
                    index++;
                };
            }

        } else if (JSONHelper.ancestorRemoved(deleteOp, mergeOpHelper) || (endDeleteOp != null && JSONHelper.compareNumberArrays(mergePos, endDeleteOp) <= 0)) {
            // 2. the merge position is inside the deletion range
            boolean isMergeRequired = mergeRequired(startDeleteOp, endDeleteOp, startMergeOp);
            if (endDeleteOp == null && startDeleteOp.length() == startMergeOp.length()) {
                // this might be the case for delete start [1] and mergeParagraph [1]
                OTUtils.setEndPosition(deleteOp, JSONHelper.clonePosition(startDeleteOp));
                startDeleteOp.add(startDeleteOp.length(), 0);  // always deleting from the beginning of the paragraph
            }
            if (endDeleteOp != null) { modifyDeletePositionFromMergePara(endDeleteOp, mergeOp, deleteOp, 1); } // shrinking the delete operation
            if (!isMergeRequired) { OTUtils.setOperationRemoved(mergeOp); } // setting marker for removal

        } else {
            // 3. the merge position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }


            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, mergePos);
            int lastIndex = startMergeOp.length() - 1;

            for (int index = 0; index < startMergeOp.length() + 1; index += 1) {
                boolean isLast = false;
                if (index > lastIndex) { isLast = true; }
                if (isLast) {
                    OTUtils.setParaLengthProperty(mergeOp, OTUtils.getParaLengthProperty(mergeOp) - deleteArray.getInt(index)); // modifying the paralength attribute, not the start position
                } else {
                    startMergeOp.put(index, startMergeOp.getInt(index) - deleteArray.getInt(index));
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    public static JSONObject handleSetAttrsDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject attrsOp = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            attrsOp = extOp;
        } else {
            deleteOp = extOp;
            attrsOp = localOp;
        }

        JSONArray startAttrsOp =OTUtils.getStartPosition(attrsOp);
        final JSONArray endAttrsOp = OTUtils.optEndPosition(attrsOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        final JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        JSONArray deleteArray;
        JSONArray endPosition;
        int surrounding = 0;
        int index = 0;

        if (JSONHelper.separateRanges(deleteOp, attrsOp)) {

            if (JSONHelper.compareNumberArrays(startDeleteOp, startAttrsOp) < 0) {
                if (endDeleteOp == null) {
                    endPosition = startDeleteOp;
                } else {
                    endPosition = endDeleteOp;
                }
                deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startAttrsOp);
                for (index = 0; index < startAttrsOp.length(); index += 1) {
                    startAttrsOp.put(index, startAttrsOp.getInt(index) - deleteArray.getInt(index));
                }

                if (endAttrsOp != null) {
                    deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, endAttrsOp);
                    for (index = 0; index < endAttrsOp.length(); index += 1) {
                        endAttrsOp.put(index, endAttrsOp.getInt(index) - deleteArray.getInt(index));
                    }
                }
            }

        } else if (JSONHelper.identicalRanges(deleteOp, attrsOp))  {
            // setting marker at setAttributes operation
            OTUtils.setOperationRemoved(attrsOp); // setting marker at internal operation!
        } else {

            surrounding = JSONHelper.surroundingRanges(deleteOp, attrsOp);

            if (surrounding != 0) {

                if (surrounding == 1) {
                    // delete surrounds setAttributes operation -> setAttributes can be ignored
                    OTUtils.setOperationRemoved(attrsOp); // setting marker at setAttributes operation
                } else {
                    // setAttributes surrounds the delete operation -> shrinking the setAttributes operation range
                    if (endAttrsOp != null) {
                        if (endDeleteOp == null) {
                            endPosition = startDeleteOp;
                        } else {
                            endPosition = endDeleteOp;
                        }
                        deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, endAttrsOp);
                        for (index = 0; index < endAttrsOp.length(); index += 1) {
                            endAttrsOp.put(index, endAttrsOp.getInt(index) - deleteArray.getInt(index));
                        }
                    }
                }

            } else {
                // overlapping ranges
                // -> 3 new ranges:
                //     - part only covered by attrsOp
                //     - part only covered by deleteOp
                //     - part covered by both operations

                // -> both operations need to get shrinked ranges
                if (JSONHelper.compareNumberArrays(startDeleteOp, startAttrsOp) <= 0) {
                    if (endDeleteOp == null) {
                        endPosition = startDeleteOp;
                    } else {
                        endPosition = endDeleteOp;
                    }
                    // shrinking the range of the setAttributes operation
                    OTUtils.setStartPosition(attrsOp, JSONHelper.increaseLastIndex(endPosition)); // TODO: Not reliable

                    startAttrsOp =OTUtils.getStartPosition(attrsOp); // refresh start position

                    // shifting the two positions of the setAttributes operation to the front
                    deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startAttrsOp);
                    for (index = 0; index < startAttrsOp.length(); index += 1) {
                        startAttrsOp.put(index, startAttrsOp.getInt(index) - deleteArray.getInt(index));
                    }
                    deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, endAttrsOp);
                    for (index = 0; index < endAttrsOp.length(); index += 1) {
                        endAttrsOp.put(index, endAttrsOp.getInt(index) - deleteArray.getInt(index));
                    }
                } else {
                    // modifying the setAttributes operation in that way, that only the missing part is removed from the local document
                    OTUtils.setEndPosition(attrsOp, JSONHelper.increaseLastIndex(startDeleteOp, -1)); // TODO: Not reliable
                }

            }
        }

        return null;
    }

    // helper function to modify the start/end position of the delete operation
    private static boolean modifyDeletePositionFromSplitParagraph(JSONArray deletePos, JSONObject splitOp) throws JSONException {
        boolean mergeRequired = false;
        if (deletePos != null) {
            final JSONArray startSplitOp =OTUtils.getStartPosition(splitOp);
            int paraIndex = startSplitOp.length() - 2;
            int textIndex = paraIndex + 1;

            if (paraIndex == 0 || (deletePos.optInt(paraIndex, -1) >= 0 && JSONHelper.isEqual(startSplitOp, deletePos, paraIndex))) {
                // the split paragraph is top level (index is 0) or the parent of the paragraph of the split operation is an ancestor of the delete op
                if (startSplitOp.getInt(paraIndex) == deletePos.getInt(paraIndex)) {
                    // the split position is before the delete.start position. The delete position might be longer
                    if (deletePos.optInt(textIndex, -1) >= 0) {
                        if (startSplitOp.getInt(textIndex) <= deletePos.getInt(textIndex)) {
                            deletePos.put(paraIndex, deletePos.getInt(paraIndex) + 1);
                            deletePos.put(textIndex, deletePos.getInt(textIndex) - startSplitOp.getInt(textIndex));
                            // this is a text position for the end of the delete operation -> following paragraph merge operation is required
                            // because a delete from [0,5] to [0,8] does only delete the text content and does not merge the paragraphs.
                            mergeRequired = true;
                        }
                    } else {
                        deletePos.put(paraIndex, deletePos.getInt(paraIndex) + 1); // a split in [1,3] leads to a delete of paragraph [2]
                    }
                } else if (startSplitOp.getInt(paraIndex) <= deletePos.getInt(paraIndex)) {
                    deletePos.put(paraIndex, deletePos.getInt(paraIndex) + 1); // increase the paragraph position of the delete operation
                }
            }
        }
        return mergeRequired;
    }

    public static JSONObject handleParaSplitDelete(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject splitOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isDeleteOperation(localOp)) {
            deleteOp = localOp;
            splitOp = extOp;
        } else {
            deleteOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startSplitOp =OTUtils.getStartPosition(splitOp);
        final JSONArray startDeleteOp =OTUtils.getStartPosition(deleteOp);
        JSONArray endDeleteOp = OTUtils.optEndPosition(deleteOp);

        int paraIndex = 0;
        int textIndex = 0;
        boolean mergeRequired = false; // whether a merge operation is required additionally to the modified delete operation

        // Case 1: split is before the start position (or equal to the start position) -> split shifts both delete positions.
        // Case 2: split is between start and end position -> split operation needs to be removed (also handling: delete removes the parent of split (delete[2] and splitParagraph[2,5]))
        // Case 3: split is after the end position -> delete shifts the split position.

        if (JSONHelper.compareNumberArrays(startSplitOp, startDeleteOp) <= 0) {
            // 1. the split position is before the delete start position (or it is exactly the delete start position)
            JSONArray[] allPos = { startDeleteOp, endDeleteOp };
            for (JSONArray deletePos: allPos) { modifyDeletePositionFromSplitParagraph(deletePos, splitOp); };
        } else if (JSONHelper.ancestorRemoved(deleteOp, splitOp) || (endDeleteOp != null && JSONHelper.compareNumberArrays(startSplitOp, endDeleteOp) <= 0)) {
            // 2. the split position is inside the deletion range
            if (endDeleteOp == null) {
                OTUtils.setEndPosition(deleteOp, JSONHelper.clonePosition(startDeleteOp));
                endDeleteOp = OTUtils.optEndPosition(deleteOp);

            } // example: 'delete start:[1]' can be handled like 'delete start: [1] end: [1]'
            JSONArray origDeleteEnd = JSONHelper.clonePosition(endDeleteOp);
            mergeRequired = modifyDeletePositionFromSplitParagraph(endDeleteOp, splitOp);
            // the merge is required, if a new paragraph is generated, but text positions are used inside one paragraph: [0,1] -> [0,8] goes to [0,1] -> [1,2]
            // the merge is not required, if a new paragraph is generated, but the text positions are in different paragraphs: [0,1] -> [3,1] goes to [0,1] -> [4,1]
            // the merge is not required, if a new paragraph is generated, but no text positions are used: [0,1] -> [3] goes to [0,1] -> [4]
            boolean isDeleteInsideParagraph = false;
            if (origDeleteEnd != null && startDeleteOp.length() == origDeleteEnd.length() && JSONHelper.isEqual(startDeleteOp, origDeleteEnd, startDeleteOp.length() - 1)) {
                isDeleteInsideParagraph = true;
            }
            if (mergeRequired && isDeleteInsideParagraph) {
                paraIndex = startSplitOp.length() - 2;
                textIndex = paraIndex + 1;

                if (startDeleteOp.optInt(paraIndex, -1) == -1) { return null; } // check, when this happens

                JSONObject mergeOperation = OTUtils.cloneJSONObject(splitOp);
                String lengthProperty = OTUtils.getMergeLengthPropertyAfterSplit(splitOp);
                OTUtils.setOperationName(mergeOperation, OTUtils.getMergeNamePropertyAfterSplit(splitOp));
                OTUtils.setStartPosition(mergeOperation, JSONHelper.clonePosition(startDeleteOp, paraIndex + 1));
                mergeOperation.put(lengthProperty, startDeleteOp.optInt(textIndex, 0));

                if (OTUtils.isDeleteOperation(localOp)) {
                    localOpsAfter = new JSONArray();
                    localOpsAfter.put(mergeOperation); // inserting the merge operation behind the current local delete operation
                } else {
                    externalOpsAfter = new JSONArray();
                    externalOpsAfter.put(mergeOperation); // inserting the merge operation behind the current external delete operation
                }
            } // sometimes adding a mergeParagraph operation, so that following external operations are counted correctly
            OTUtils.setOperationRemoved(splitOp); // setting marker that the operation is not applied locally
        } else {
            // 3. the split position is behind the delete end position -> its position must be modified.
            JSONArray endPosition = null;
            if (endDeleteOp == null) {
                endPosition = startDeleteOp;
            } else {
                endPosition = endDeleteOp;
            }
            JSONArray deleteArray = JSONHelper.calculateDeleteArray(startDeleteOp, endPosition, startSplitOp);
            for (int index = 0; index < startSplitOp.length(); index += 1) { startSplitOp.put(index, startSplitOp.getInt(index) - deleteArray.getInt(index)); }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleInsertColumnSplitPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertOp = null;
        JSONObject splitOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isInsertColumnOperation(localOp)) {
            insertOp = localOp;
            splitOp = extOp;
        } else {
            insertOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int splitLength = startSplit.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;
        int textIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertOp);

        boolean isParaSplit = OTUtils.isSplitParagraphOperation(splitOp);
        // checking whether the table that has the insertColumn also needs to be splitted
        boolean tableSplit = !isParaSplit && insertLength == splitLength - 1 && JSONHelper.isEqual(startSplit, startInsert, insertLength);

        if (tableSplit) {
            // additional deleteColumns operation required
            tableIndex = insertLength - 1;
            JSONObject newOperation = OTUtils.cloneJSONObject(insertOp);
            JSONArray startNewOp =OTUtils.getStartPosition(newOperation);
            startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) + 1);

            if (OTUtils.isSplitTableOperation(localOp)) {
                externalOpsAfter = new JSONArray();
                externalOpsAfter.put(newOperation); // inserting the insertColumn operation behind the current external insertColumn operation
            } else {
                localOpsAfter = new JSONArray();
                localOpsAfter.put(newOperation); // inserting the insertColumn operation behind the current local insertColumn operation
            }
        } else {
            if (JSONHelper.compareNumberArrays(startInsert, startSplit) <= 0) {
                if (splitLength > insertLength + 1 && JSONHelper.isEqual(startSplit, startInsert, insertLength)) {
                    tableIndex = insertLength - 1;
                    columnIndex = tableIndex + 2;
                    if ((startSplit.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertOp)) || (OTUtils.isInsertModeBefore(mode) && startSplit.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertOp))) {
                        startSplit.put(columnIndex, startSplit.getInt(columnIndex) + 1);
                    }
                }
            } else {
                paraIndex = splitLength - 2;
                textIndex = paraIndex + 1;

                if ((paraIndex == 0) || (insertLength >= splitLength - 1 && JSONHelper.isEqual(startSplit, startSplit, paraIndex))) {
                    if (startSplit.getInt(paraIndex) < startInsert.getInt(paraIndex)) {
                        startInsert.put(paraIndex, startInsert.getInt(paraIndex) + 1);
                    } else if (startSplit.getInt(paraIndex) == startInsert.getInt(paraIndex) && startSplit.getInt(textIndex) <= startInsert.getInt(textIndex)) {
                        startInsert.put(paraIndex, startInsert.getInt(paraIndex) + 1);
                        startInsert.put(textIndex, startInsert.getInt(textIndex) - startSplit.getInt(textIndex));
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleDeleteColumnsSplitPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject splitOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            splitOp = extOp;
        } else {
            deleteOp = extOp;
            splitOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);

        int splitLength = startSplit.length();
        int deleteLength = startDelete.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;
        int textIndex = 0;

        boolean isParaSplit = OTUtils.isSplitParagraphOperation(splitOp);
        // checking whether the table that has the deleteColumns also needs to be splitted
        boolean tableSplit = !isParaSplit && deleteLength == splitLength - 1 && JSONHelper.isEqual(startSplit, startDelete, deleteLength);

        if (tableSplit) {
            // additional deleteColumns operation required
            tableIndex = deleteLength - 1;
            JSONObject newOperation = OTUtils.cloneJSONObject(deleteOp);
            JSONArray startNewOp =OTUtils.getStartPosition(newOperation);
            startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) + 1);

            if (OTUtils.isSplitTableOperation(localOp)) {
                externalOpsAfter = new JSONArray();
                externalOpsAfter.put(newOperation); // inserting the deleteColumns operation behind the current external deleteColumns operation
            } else {
                localOpsAfter = new JSONArray();
                localOpsAfter.put(newOperation); // inserting the deleteColumns operation behind the current local deleteColumns operation
            }
        } else {
            if (JSONHelper.compareNumberArrays(startDelete, startSplit) <= 0) {
                if (splitLength > deleteLength + 1 && JSONHelper.isEqual(startSplit, startDelete, deleteLength)) {
                    tableIndex = deleteLength - 1;
                    columnIndex = tableIndex + 2;
                    if (startSplit.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                        if (startSplit.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                            int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                            startSplit.put(columnIndex, startSplit.getInt(columnIndex) - deleteColumns);
                        } else {
                            OTUtils.setOperationRemoved(splitOp);
                        }
                    }
                }
            } else {
                paraIndex = splitLength - 2;
                textIndex = paraIndex + 1;

                if ((paraIndex == 0) || (deleteLength >= splitLength - 1 && JSONHelper.isEqual(startDelete, startSplit, paraIndex))) {
                    if (startSplit.getInt(paraIndex) < startDelete.getInt(paraIndex)) {
                        startDelete.put(paraIndex, startDelete.getInt(paraIndex) + 1);
                    } else if (startSplit.getInt(paraIndex) == startDelete.getInt(paraIndex) && startSplit.getInt(textIndex) <= startDelete.getInt(textIndex)) {
                        startDelete.put(paraIndex, startDelete.getInt(paraIndex) + 1);
                        startDelete.put(textIndex, startDelete.getInt(textIndex) - startSplit.getInt(textIndex));
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleInsertCellsInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertCellsOp = null;
        JSONObject insertColumnOp = null;

        if (OTUtils.isInsertCellsOperation(localOp)) {
            insertCellsOp = localOp;
            insertColumnOp = extOp;
        } else {
            insertCellsOp = extOp;
            insertColumnOp = localOp;
        }

        final JSONArray startInsertCells =OTUtils.getStartPosition(insertCellsOp);
        final JSONArray startInsertColumn =OTUtils.getStartPosition(insertColumnOp);

        int insertCellsLength = startInsertCells.length();
        int insertColumnLength = startInsertColumn.length();

        int cellCount = OTUtils.getCountProperty(insertCellsOp, 1);
        int tableIndex = 0;
        int cellIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertColumnOp);

        if (insertColumnLength < insertCellsLength) {
            if (JSONHelper.isEqual(startInsertCells, startInsertColumn, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                cellIndex = tableIndex + 2;
                if ((startInsertCells.getInt(cellIndex) > OTUtils.getGridPositionProperty(insertColumnOp)) || (OTUtils.isInsertModeBefore(mode) && startInsertCells.getInt(cellIndex) == OTUtils.getGridPositionProperty(insertColumnOp))) {
                    startInsertCells.put(cellIndex, startInsertCells.getInt(cellIndex) + 1);
                }
            }

        } else {
            if (JSONHelper.isEqual(startInsertCells, startInsertColumn, insertCellsLength - 1)) {
                cellIndex = insertCellsLength - 1;
                if (startInsertCells.getInt(cellIndex) <= startInsertColumn.getInt(cellIndex)) {
                    startInsertColumn.put(cellIndex, startInsertColumn.getInt(cellIndex) + cellCount);
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertCellsInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertCellsOp = null;
        JSONObject insertRowsOp = null;

        if (OTUtils.isInsertCellsOperation(localOp)) {
            insertCellsOp = localOp;
            insertRowsOp = extOp;
        } else {
            insertCellsOp = extOp;
            insertRowsOp = localOp;
        }

        final JSONArray startInsertCells =OTUtils.getStartPosition(insertCellsOp);
        final JSONArray startInsertRows =OTUtils.getStartPosition(insertRowsOp);

        int insertCellsLength = startInsertCells.length();
        int insertRowsLength = startInsertRows.length();

        int cellCount = OTUtils.getCountProperty(insertCellsOp, 1);
        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);
        int rowIndex = 0;
        int cellIndex = 0;

        if (JSONHelper.compareNumberArrays(startInsertRows, startInsertCells) <= 0) {
            rowIndex = insertRowsLength - 1;
            if (insertCellsLength > insertRowsLength && JSONHelper.isEqual(startInsertCells,  startInsertRows, rowIndex)) {
                if (startInsertCells.getInt(rowIndex) >= startInsertRows.getInt(rowIndex)) {
                    startInsertCells.put(rowIndex, startInsertCells.getInt(rowIndex) + rowCount);
                }
            }

        } else {
            cellIndex = insertCellsLength - 1;
            if (insertRowsLength > insertCellsLength && JSONHelper.isEqual(startInsertRows,  startInsertCells,  cellIndex)) {
                startInsertRows.put(cellIndex, startInsertRows.getInt(cellIndex) + cellCount);
            }

        }

        return null;
    }

    public static JSONObject handleInsertColumnInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertColumnOp = null;
        JSONObject insertCharOp = null;

        if (OTUtils.isInsertColumnOperation(localOp)) {
            insertColumnOp = localOp;
            insertCharOp = extOp;
        } else {
            insertColumnOp = extOp;
            insertCharOp = localOp;
        }

        final JSONArray startInsertColumn =OTUtils.getStartPosition(insertColumnOp);
        final JSONArray startInsertChar =OTUtils.getStartPosition(insertCharOp);

        int insertColumnLength = startInsertColumn.length();
        int insertCharLength = startInsertChar.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int textIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertColumnOp);
        int insertLength = 1;

        if (JSONHelper.compareNumberArrays(startInsertColumn, startInsertChar) < 0) {
            if (insertCharLength > insertColumnLength + 1 && JSONHelper.isEqual(startInsertChar, startInsertColumn, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((startInsertChar.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertColumnOp)) || (OTUtils.isInsertModeBefore(mode) && startInsertChar.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertColumnOp))) {
                    startInsertChar.put(columnIndex, startInsertChar.getInt(columnIndex) + 1);
                }
            }
        } else {
            textIndex = insertCharLength - 1;
            if (insertCharLength < insertColumnLength && JSONHelper.isEqual(startInsertChar, startInsertColumn, insertCharLength - 1)) {
                if (startInsertChar.getInt(textIndex) <= startInsertColumn.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertCharOp)) { insertLength =OTUtils.getText(insertCharOp).length(); }
                    startInsertColumn.put(textIndex, startInsertColumn.getInt(textIndex) + insertLength);
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertParaInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertColumnOp = null;
        JSONObject insertParaOp = null;

        if (OTUtils.isInsertColumnOperation(localOp)) {
            insertColumnOp = localOp;
            insertParaOp = extOp;
        } else {
            insertColumnOp = extOp;
            insertParaOp = localOp;
        }

        final JSONArray startInsertColumn =OTUtils.getStartPosition(insertColumnOp);
        final JSONArray startInsertPara =OTUtils.getStartPosition(insertParaOp);

        int insertColumnLength = startInsertColumn.length();
        int insertParaLength = startInsertPara.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertColumnOp);

        if (JSONHelper.compareNumberArrays(startInsertColumn, startInsertPara) < 0) {
            if (insertParaLength > insertColumnLength + 1 && JSONHelper.isEqual(startInsertPara, startInsertColumn, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((startInsertPara.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertColumnOp)) || (OTUtils.isInsertModeBefore(mode) && startInsertPara.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertColumnOp))) {
                    startInsertPara.put(columnIndex, startInsertPara.getInt(columnIndex) + 1);
                }
            }
        } else {
            paraIndex = insertParaLength - 1;
            if (paraIndex == 0 || (insertParaLength < insertColumnLength && JSONHelper.isEqual(startInsertPara, startInsertColumn, insertParaLength - 1))) {
                startInsertColumn.put(paraIndex, startInsertColumn.getInt(paraIndex) + 1);
            }
        }

        return null;
    }

    public static JSONObject handleInsertParaDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int deleteLength = startDelete.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;

        if (JSONHelper.compareNumberArrays(startDelete, startInsert) < 0) {
            if (insertLength > deleteLength + 1 && JSONHelper.isEqual(startInsert, startDelete, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (startInsert.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                    if (startInsert.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                        int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                        startInsert.put(columnIndex, startInsert.getInt(columnIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                    }
                }
            }
        } else {
            paraIndex = insertLength - 1;
            if (paraIndex == 0 || (deleteLength < insertLength && JSONHelper.isEqual(startInsert, startDelete, insertLength - 1))) {
                startDelete.put(paraIndex, startDelete.getInt(paraIndex) + 1);
            }
        }

        return null;
    }

    public static JSONObject handleDeleteColumnsInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int deleteLength = startDelete.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int columnIndex = 0;
        int textIndex = 0;
        int insertTextLength = 1;

        if (JSONHelper.compareNumberArrays(startDelete, startInsert) < 0) {
            if (insertLength > deleteLength + 1 && JSONHelper.isEqual(startInsert, startDelete, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (startInsert.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                    if (startInsert.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                        int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                        startInsert.put(columnIndex, startInsert.getInt(columnIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                    }
                }
            }
        } else {
            textIndex = insertLength - 1;
            if (insertLength < deleteLength && JSONHelper.isEqual(startInsert, startDelete, insertLength - 1)) {
                if (startInsert.getInt(textIndex) <= startDelete.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertOp)) { insertTextLength =OTUtils.getText(insertOp).length(); }
                    startDelete.put(textIndex, startDelete.getInt(textIndex) + insertTextLength);
                }
            }
        }

        return null;
    }

    private static JSONObject handleDeleteColumnsInsertRows(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int deleteLength = startDelete.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int rowIndex = 0;
        int columnIndex = 0;
        int rowCount = OTUtils.getCountProperty(insertOp, 1);

        if (deleteLength + 1 == insertLength) { return null; } // nothing to do

        if (deleteLength < insertLength) {
            if (JSONHelper.isEqual(startInsert, startDelete, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (startInsert.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                    if (startInsert.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                        int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                        startInsert.put(columnIndex, startInsert.getInt(columnIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                    }
                }
            }
        } else {
            if (insertLength == 1 || JSONHelper.isEqual(startInsert, startDelete, insertLength - 1)) {
                rowIndex = insertLength - 1;
                if (startInsert.getInt(rowIndex) <= startDelete.getInt(rowIndex)) {
                    startDelete.put(rowIndex, startDelete.getInt(rowIndex) + rowCount);
                }
            }
        }

        return null;
    }

    public static JSONObject handleInsertCellsDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int deleteLength = startDelete.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int cellIndex = 0;
        int cellCount = OTUtils.getCountProperty(insertOp, 1);

        if (deleteLength < insertLength) {
            if (JSONHelper.isEqual(startInsert, startDelete, deleteLength)) {
                tableIndex = deleteLength - 1;
                cellIndex = tableIndex + 2;
                if (startInsert.getInt(cellIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                    if (startInsert.getInt(cellIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                        int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                        startInsert.put(cellIndex, startInsert.getInt(cellIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                    }
                }
            }
        } else {
            if (JSONHelper.isEqual(startInsert, startDelete, insertLength - 1)) {
                cellIndex = insertLength - 1;
                if (startInsert.getInt(cellIndex) <= startDelete.getInt(cellIndex)) {
                    startDelete.put(cellIndex, startDelete.getInt(cellIndex) + cellCount);
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertRowsInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject insertRowsOp = null;
        JSONObject insertColumnOp = null;

        if (OTUtils.isInsertRowsOperation(localOp)) {
            insertRowsOp = localOp;
            insertColumnOp = extOp;
        } else {
            insertRowsOp = extOp;
            insertColumnOp = localOp;
        }

        final JSONArray startInsertRows =OTUtils.getStartPosition(insertRowsOp);
        final JSONArray startInsertColumn =OTUtils.getStartPosition(insertColumnOp);

        int insertRowsLength = startInsertRows.length();
        int insertColumnLength = startInsertColumn.length();

        int tableIndex = 0;
        int rowIndex = 0;
        int columnIndex = 0;

        int rowCount = OTUtils.getCountProperty(insertRowsOp, 1);
        String mode = OTUtils.getInsertModeProperty(insertColumnOp);

        if (insertColumnLength + 1 == insertRowsLength) { return null; } // nothing to do

        if (insertColumnLength < insertRowsLength) {
            if (JSONHelper.isEqual(startInsertRows, startInsertColumn, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((startInsertRows.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertColumnOp)) || (OTUtils.isInsertModeBefore(mode) && startInsertRows.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertColumnOp))) {
                    startInsertRows.put(columnIndex, startInsertRows.getInt(columnIndex) + 1);
                }
            }
        } else {
            if (JSONHelper.isEqual(startInsertRows, startInsertColumn, insertRowsLength - 1)) {
                rowIndex = insertRowsLength - 1;
                if (startInsertRows.getInt(rowIndex) <= startInsertColumn.getInt(rowIndex)) {
                    startInsertColumn.put(rowIndex, startInsertColumn.getInt(rowIndex) + rowCount);
                }
            }
        }

        return null;
    }

    public static JSONObject handleTableSplitInsertChar(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject splitOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isSplitTableOperation(localOp)) {
            splitOp = localOp;
            insertOp = extOp;
        } else {
            splitOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int splitLength = startSplit.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int rowIndex = 0;
        int textIndex = 0;

        int insertTextLength = 1;

        if (splitLength <= insertLength) {
            rowIndex = splitLength - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex == 0 || JSONHelper.isEqual(startSplit, startInsert, tableIndex)) {
                if (startSplit.getInt(tableIndex) < startInsert.getInt(tableIndex)) {
                    startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                } else if (startSplit.getInt(tableIndex) == startInsert.getInt(tableIndex)) {
                    if (startSplit.getInt(rowIndex) <= startInsert.getInt(rowIndex)) {
                        startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                        startInsert.put(rowIndex, startInsert.getInt(rowIndex) - startSplit.getInt(rowIndex));
                    }
                }
            }
        } else {
            textIndex = insertLength - 1;
            if (JSONHelper.isEqual(startInsert, startSplit, insertLength - 1)) {
                if (startInsert.getInt(textIndex) <= startSplit.getInt(textIndex)) {
                    if (OTUtils.isInsertTextOperation(insertOp)) { insertTextLength =OTUtils.getText(insertOp).length(); }
                    startSplit.put(textIndex, startSplit.getInt(textIndex) + insertTextLength);
                }
            }
        }

        return null;
    }

    public static JSONObject handleTableSplitInsertPara(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject splitOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isSplitTableOperation(localOp)) {
            splitOp = localOp;
            insertOp = extOp;
        } else {
            splitOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int splitLength = startSplit.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int rowIndex = 0;
        int paraIndex = 0;

        if (splitLength <= insertLength) {
            rowIndex = splitLength - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex == 0 || JSONHelper.isEqual(startSplit, startInsert, tableIndex)) {
                if (startSplit.getInt(tableIndex) < startInsert.getInt(tableIndex)) {
                    startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                } else if (startSplit.getInt(tableIndex) == startInsert.getInt(tableIndex)) {
                    if (startSplit.getInt(rowIndex) <= startInsert.getInt(rowIndex)) {
                        startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                        startInsert.put(rowIndex, startInsert.getInt(rowIndex) - startSplit.getInt(rowIndex));
                    }
                }
            }
        } else {
            paraIndex = insertLength - 1;
            if (paraIndex == 0 || JSONHelper.isEqual(startInsert, startSplit, paraIndex)) {
                if (startInsert.getInt(paraIndex) <= startSplit.getInt(paraIndex)) {
                    startSplit.put(paraIndex, startSplit.getInt(paraIndex) + 1);
                } else {
                    if (splitLength == insertLength + 1) {
                        startInsert.put(paraIndex, startInsert.getInt(paraIndex) + 1);
                    }
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertRowsSplitTable(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject splitOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isSplitTableOperation(localOp)) {
            splitOp = localOp;
            insertOp = extOp;
        } else {
            splitOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startSplit =OTUtils.getStartPosition(splitOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int splitLength = startSplit.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int rowIndex = 0;

        int rowCount = OTUtils.getCountProperty(insertOp, 1);

        if (splitLength <= insertLength) {
            rowIndex = splitLength - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex == 0 || JSONHelper.isEqual(startSplit, startInsert, tableIndex)) {
                if (startSplit.getInt(tableIndex) < startInsert.getInt(tableIndex)) {
                    startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                } else if (startSplit.getInt(tableIndex) == startInsert.getInt(tableIndex)) {
                    if (startSplit.getInt(rowIndex) <= startInsert.getInt(rowIndex)) {
                        startInsert.put(tableIndex, startInsert.getInt(tableIndex) + 1);
                        startInsert.put(rowIndex, startInsert.getInt(rowIndex) - startSplit.getInt(rowIndex));
                        if (OTUtils.hasReferenceRowProperty(insertOp) && splitLength == insertLength) {
                            OTUtils.setReferenceRowProperty(insertOp, OTUtils.getReferenceRowProperty(insertOp) - startSplit.getInt(rowIndex));
                            if (OTUtils.getReferenceRowProperty(insertOp) < 0) {
                                insertOp.remove(OCKey.REFERENCE_ROW.value());
                            }
                        }
                    } else {
                        if(splitLength == insertLength) {
                            final int referenceRow = insertOp.optInt(OCKey.REFERENCE_ROW.value(), -1);
                            if(referenceRow >= startSplit.getInt(rowIndex)) {
                                insertOp.remove(OCKey.REFERENCE_ROW.value());
                            }
                            if (OTUtils.isInsertRowsOperation(insertOp)) {
                                startSplit.put(rowIndex, startSplit.getInt(rowIndex) + rowCount);
                            }
                        }
                    }
                }
            }
        } else {
            rowIndex = insertLength - 1;
            if (JSONHelper.isEqual(startInsert, startSplit, rowIndex)) {
                if (startInsert.getInt(rowIndex) <= startSplit.getInt(rowIndex)) {
                    startSplit.put(rowIndex, startSplit.getInt(rowIndex) + rowCount);
                }
            }
        }

        return null;
    }

    private static JSONObject handleInsertColumnDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteOp = null;
        JSONObject insertOp = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            insertOp = extOp;
        } else {
            deleteOp = extOp;
            insertOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);

        int deleteLength = startDelete.length();
        int insertLength = startInsert.length();

        int tableIndex = 0;
        int columnIndex = 0;

        int deleteColumns = 1;

        String mode = OTUtils.getInsertModeProperty(insertOp);

        if (deleteLength == insertLength) {
            if (JSONHelper.isEqual(startInsert, startDelete, deleteLength)) { // same table
                if (OTUtils.getGridPositionProperty(insertOp) <= OTUtils.getStartGridProperty(deleteOp)) {
                    OTUtils.setStartGridProperty(deleteOp, OTUtils.getStartGridProperty(deleteOp) + 1);
                    OTUtils.setEndGridProperty(deleteOp, OTUtils.getEndGridProperty(deleteOp) + 1);
                } else if (OTUtils.getGridPositionProperty(insertOp) <= OTUtils.getEndGridProperty(deleteOp)) {
                    OTUtils.setEndGridProperty(deleteOp, OTUtils.getEndGridProperty(deleteOp) + 1);
                    OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                } else {
                    deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                    OTUtils.setGridPositionProperty(insertOp, OTUtils.getGridPositionProperty(insertOp) - deleteColumns);
                }
            }
        } else if (deleteLength < insertLength) {
            if (JSONHelper.isEqual(startInsert, startDelete, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (startInsert.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                    if (startInsert.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                        deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                        startInsert.put(columnIndex, startInsert.getInt(columnIndex) - deleteColumns);
                    } else {
                        OTUtils.setOperationRemoved(insertOp); // insert operation can be ignored
                    }
                }
            }
        } else { // insertLength is smaller than deleteLength
            if (JSONHelper.isEqual(startInsert, startDelete, insertLength)) {
                tableIndex = insertLength - 1;
                columnIndex = tableIndex + 2;
                if (startDelete.optInt(columnIndex - 1, -1) >= 0 && (OTUtils.getGridPositionProperty(insertOp) < startDelete.getInt(columnIndex)) || (OTUtils.isInsertModeBefore(mode) && OTUtils.getGridPositionProperty(insertOp) == startDelete.getInt(columnIndex))) {
                    startDelete.put(columnIndex, startDelete.getInt(columnIndex) + 1);
                }
            }
        }

        return null;
    }

    private static JSONObject handleSplitParaSplitTable(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        final JSONArray startLocal =OTUtils.getStartPosition(localOp);
        final JSONArray startExt =OTUtils.getStartPosition(extOp);

        int localLength = startLocal.length();
        int extLength = startExt.length();

        int textRowIndex = 0;
        int paraIndex = 0;

        if (localLength == extLength) {
            if (localLength == 2 || JSONHelper.isEqual(startLocal, startExt, localLength - 2)) {
                paraIndex = localLength - 2;
                if (startLocal.getInt(paraIndex) < startExt.getInt(paraIndex)) {
                    startExt.put(paraIndex, startExt.getInt(paraIndex) + 1);
                } else if (startLocal.getInt(paraIndex) > startExt.getInt(paraIndex)) {
                    startLocal.put(paraIndex, startLocal.getInt(paraIndex) + 1);
                }
            }
        } else { // both positions have not the same length
            JSONObject shortOp = null;
            JSONObject longOp = null;

            if (localLength < extLength) {
                shortOp = localOp;
                longOp = extOp;
            } else {
                shortOp = extOp;
                longOp = localOp;
            }

            final JSONArray startShort =OTUtils.getStartPosition(shortOp);
            final JSONArray startLong =OTUtils.getStartPosition(longOp);

            paraIndex = startShort.length() - 2;
            textRowIndex = paraIndex + 1;

            if (paraIndex == 0 || JSONHelper.isEqual(startShort, startLong, paraIndex)) {
                if (startShort.getInt(paraIndex) < startLong.getInt(paraIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                } else if (startShort.getInt(paraIndex) == startLong.getInt(paraIndex) && startShort.getInt(textRowIndex) <= startLong.getInt(textRowIndex)) {
                    startLong.put(paraIndex, startLong.getInt(paraIndex) + 1);
                    startLong.put(textRowIndex, startLong.getInt(textRowIndex) - startShort.getInt(textRowIndex));
                }
            }
        }

        return null;
    }

    private static JSONObject handleMergeParaInsertColumn(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject insertOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isInsertColumnOperation(localOp)) {
            insertOp = localOp;
            mergeOp = extOp;
        } else {
            insertOp = extOp;
            mergeOp = localOp;
        }

        final JSONArray startInsert =OTUtils.getStartPosition(insertOp);
        final JSONArray startMerge =OTUtils.getStartPosition(mergeOp);

        int insertLength = startInsert.length();
        int mergeLength = startMerge.length();

        int lastInsertPos = startInsert.getInt(insertLength - 1);
        int lastMergePos = startMerge.getInt(mergeLength - 1);

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;
        int textIndex = 0;

        String mode = OTUtils.getInsertModeProperty(insertOp);

        boolean isParaMerge = OTUtils.isMergeParagraphOperation(mergeOp);
        boolean sameTable = false;
        // whether the table that has the insertColumn operation also needs to be merged
        boolean tableMerge = !isParaMerge && insertLength == mergeLength && (insertLength == 1 || JSONHelper.isEqual(startMerge, startInsert, insertLength - 1)) && (lastMergePos == lastInsertPos || lastMergePos == lastInsertPos - 1);

        if (tableMerge) {
            tableIndex = insertLength - 1;
            sameTable = (lastMergePos == lastInsertPos);

            JSONObject newOperation = null;
            JSONArray startNewOp = null;

            if (sameTable) {

                newOperation = OTUtils.cloneJSONObject(insertOp);
                startNewOp =OTUtils.getStartPosition(newOperation);
                startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) + 1);

                if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
                    localOpsBefore = new JSONArray();
                    localOpsBefore.put(newOperation);
                } else {
                    externalOpsBefore = new JSONArray();
                    externalOpsBefore.put(newOperation);
                }

            } else {

                newOperation = OTUtils.cloneJSONObject(insertOp);
                startInsert.put(tableIndex, startInsert.getInt(tableIndex) - 1);

                startNewOp =OTUtils.getStartPosition(newOperation);
                startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) - 1);

                if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
                    localOpsBefore = new JSONArray();
                    localOpsBefore.put(newOperation);
                } else {
                    externalOpsBefore = new JSONArray();
                    externalOpsBefore.put(newOperation);
                }
            }

        } else { // different table(s) for merge and insertColumn (or mergeParagraph operation)

            if (JSONHelper.compareNumberArrays(startInsert, startMerge) <= 0) {
                if (mergeLength > insertLength + 1 && JSONHelper.isEqual(startMerge, startInsert, insertLength)) {
                    tableIndex = insertLength - 1;
                    columnIndex = tableIndex + 2;
                    if ((startMerge.getInt(columnIndex) > OTUtils.getGridPositionProperty(insertOp)) || (OTUtils.isInsertModeBefore(mode) && startMerge.getInt(columnIndex) == OTUtils.getGridPositionProperty(insertOp))) {
                        startMerge.put(columnIndex, startMerge.getInt(columnIndex) + 1);
                    }
                }
            } else {
                paraIndex = mergeLength - 1;
                textIndex = paraIndex + 1;

                if ((paraIndex == 0) || (insertLength >= mergeLength && JSONHelper.isEqual(startInsert, startMerge, paraIndex))) {
                    if (startMerge.getInt(paraIndex) < startInsert.getInt(paraIndex)) {
                        startInsert.put(paraIndex, startInsert.getInt(paraIndex) - 1);
                        if (startMerge.getInt(paraIndex) == startInsert.getInt(paraIndex)) {
                            String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);
                            startInsert.put(textIndex, startInsert.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                        }
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    private static JSONObject handleMergeParaDeleteColumns(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject mergeOp = null;
        JSONObject deleteOp = null;

        JSONArray externalOpsBefore = null;
        JSONArray externalOpsAfter = null;
        JSONArray localOpsBefore = null;
        JSONArray localOpsAfter = null;

        if (OTUtils.isDeleteColumnsOperation(localOp)) {
            deleteOp = localOp;
            mergeOp = extOp;
        } else {
            deleteOp = extOp;
            mergeOp = localOp;
        }

        final JSONArray startDelete =OTUtils.getStartPosition(deleteOp);
        final JSONArray startMerge =OTUtils.getStartPosition(mergeOp);

        int deleteLength = startDelete.length();
        int mergeLength = startMerge.length();

        int lastDeletePos = startDelete.getInt(deleteLength - 1);
        int lastMergePos = startMerge.getInt(mergeLength - 1);

        int tableIndex = 0;
        int columnIndex = 0;
        int paraIndex = 0;
        int textIndex = 0;

        boolean isParaMerge = OTUtils.isMergeParagraphOperation(mergeOp);
        boolean sameTable = false;
        // whether the table that has the insertColumn operation also needs to be merged
        boolean tableMerge = !isParaMerge && deleteLength == mergeLength && (deleteLength == 1 || JSONHelper.isEqual(startMerge, startDelete, deleteLength - 1)) && (lastMergePos == lastDeletePos || lastMergePos == lastDeletePos - 1);

        if (tableMerge) {
            tableIndex = deleteLength - 1;
            sameTable = (lastMergePos == lastDeletePos);

            JSONObject newOperation = null;
            JSONArray startNewOp = null;

            if (sameTable) {

                newOperation = OTUtils.cloneJSONObject(deleteOp);
                startNewOp =OTUtils.getStartPosition(newOperation);
                startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) + 1);

                if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
                    localOpsBefore = new JSONArray();
                    localOpsBefore.put(newOperation); // inserting before the current local operation
                } else {
                    externalOpsBefore = new JSONArray();
                    externalOpsBefore.put(newOperation); // inserting before the current external operation
                }

            } else {

                newOperation = OTUtils.cloneJSONObject(deleteOp);
                startDelete.put(tableIndex, startDelete.getInt(tableIndex) - 1);

                startNewOp =OTUtils.getStartPosition(newOperation);
                startNewOp.put(tableIndex, startNewOp.getInt(tableIndex) - 1);

                if (OTUtils.isMergeParagraphOperation(localOp) || OTUtils.isMergeTableOperation(localOp)) {
                    localOpsBefore = new JSONArray();
                    localOpsBefore.put(newOperation); // inserting before the current local operation
                } else {
                    externalOpsBefore = new JSONArray();
                    externalOpsBefore.put(newOperation); // inserting before the current external operation
                }
            }

        } else { // different table(s) for merge and insertColumn (or mergeParagraph operation)

            if (JSONHelper.compareNumberArrays(startDelete, startMerge) <= 0) {
                if (mergeLength > deleteLength + 1 && JSONHelper.isEqual(startMerge, startDelete, deleteLength)) {
                    tableIndex = deleteLength - 1;
                    columnIndex = tableIndex + 2;
                    if (startMerge.getInt(columnIndex) >= OTUtils.getStartGridProperty(deleteOp)) {
                        if (startMerge.getInt(columnIndex) > OTUtils.getEndGridProperty(deleteOp)) {
                            int deleteColumns = OTUtils.getEndGridProperty(deleteOp) - OTUtils.getStartGridProperty(deleteOp) + 1;
                            startMerge.put(columnIndex, startMerge.getInt(columnIndex) - deleteColumns);
                        } else {
                            OTUtils.setOperationRemoved(mergeOp); // merge operation can be ignored
                        }
                    }
                }
            } else {
                paraIndex = mergeLength - 1;
                textIndex = paraIndex + 1;

                if ((paraIndex == 0) || (deleteLength >= mergeLength && JSONHelper.isEqual(startDelete, startMerge, paraIndex))) {
                    if (startMerge.getInt(paraIndex) < startDelete.getInt(paraIndex)) {
                        startDelete.put(paraIndex, startDelete.getInt(paraIndex) - 1);
                        if (startMerge.getInt(paraIndex) == startDelete.getInt(paraIndex)) {
                            String lengthProperty = OTUtils.getMergeLengthProperty(mergeOp);
                            startDelete.put(textIndex, startDelete.getInt(textIndex) + mergeOp.optInt(lengthProperty, 0));
                        }
                    }
                }
            }
        }

        return JSONHelper.getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    public static JSONObject handleDeleteTarget(final JSONObject localOp, final JSONObject extOp) throws JSONException {

        JSONObject deleteTargetOp = null;
        JSONObject otherOp = null;

        if (OTUtils.isDeleteTargetOperation(localOp)) {
            deleteTargetOp = localOp;
            otherOp = extOp;
        } else {
            deleteTargetOp = extOp;
            otherOp = localOp;
        }

        String target = OTUtils.getTarget(otherOp);

        if (!target.isEmpty() && target.equals(OTUtils.getIdProperty(deleteTargetOp))) {
            OTUtils.setOperationRemoved(otherOp);
        }

        return null;
    }

    public static JSONObject handleDeleteTargetDeleteTarget(final JSONObject localOp, final JSONObject extOp) throws JSONException {
        if (OTUtils.getName(localOp).equals(OTUtils.getName(extOp)) && OTUtils.getIdProperty(localOp).equals(OTUtils.getIdProperty(extOp))) {
            OTUtils.setOperationRemoved(localOp);
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }

    public static JSONObject handleNothing() {
        return null;
        //
    }

    /**
     * Resolves the type identifier of the passed auto-style.
     *
     * @param styleOp
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  The type identifier of the passed auto-style. If the operation does not
     *  contain an explicit type identifier, the default type identifier passed
     *  to the constructor of this instance will be returned.
     */
    private static String getAutoStyleType(JSONObject autoStyleOperation) {
        return autoStyleOperation.optString(OCKey.TYPE.value(), "cell");    // defaultAutoStyleType
    }

    /**
     * Resolves the type identifier of the passed auto-styles. Returns the type
     * identifier only if both auto-styles have the same type.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  The type identifier of the passed auto-styles; or null if the
     *  auto-styles do not have the same type.
     */
    private static String getSameAutoStyleType(JSONObject autoStyleOp1, JSONObject autoStyleOp2) {
        final String type1 = getAutoStyleType(autoStyleOp1);
        final String type2 = getAutoStyleType(autoStyleOp2);
        return type1.equals(type2) ? type1 : null;
    }


    private static final Map<String, Set<String>> autoStyleTypes = new ImmutableMap.Builder<String, Set<String>>()
        .put("cell", new ImmutableSet.Builder<String>()
            .add("a")
            .build())
        .build();

    private static Integer getAutoStyleIndex(String id, String prefix) {
        if(!id.startsWith(prefix)) {
            return null;
        }
        try {
            return Integer.parseInt(id.substring(prefix.length()));
        }
        catch(Exception e) {
            return null;
        }
    }

    /**
     * if possible it returns the auto-style index (or null)
     *
     * @param styleOp
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  the index of the auto-style or null
     */
    private static Integer getAutoStyleIndex(JSONObject styleOp) {
        final String styleId = styleOp.optString(OCKey.STYLE_ID.value(), null);
        if(styleId==null) {
            return null;
        }

        final String autoStyleType = getAutoStyleType(styleOp);
        if (null != autoStyleType) {
            Set<String> prefixes = autoStyleTypes.get(autoStyleType);
            if (null == prefixes) {
                // We found an unknown auto-style type. We expect that it
                // the index is using our default which is a.
                prefixes = new ImmutableSet.Builder<String>()
                    .add("a")
                    .build();
            }
            //if(autoStyleTypes.containsKey(getAutoStyleType(styleOp))) {
            //if(null != prefixes) {
                final Iterator<String> iter = prefixes.iterator();
                while(iter.hasNext()) {
                    final Integer index = getAutoStyleIndex(styleId, iter.next());
                    if(index!=null) {
                        return index;
                    }
                }
            //}
        }
        // We found no auto-style type or an unknown auto-style is not using
        // our default prefix a.
        return null;
    }

    /**
     * Returns whether both auto-style operations refer to the same type.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  Whether both auto-style operations refer to the same type.
     */
    private static boolean isSameAutoStyleType(JSONObject styleOp1, JSONObject styleOp2) {
        return getAutoStyleType(styleOp1).equals(getAutoStyleType(styleOp2));
    }

    /**
     * Returns whether both auto-style operations refer to the same auto-style.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  Whether both operations refer to the same auto-style (type and
     *  identifier).
     */
    private static boolean isSameAutoStyle(JSONObject styleOp1, JSONObject styleOp2) {
        return isSameAutoStyleType(styleOp1, styleOp2) && (styleOp1.optString(OCKey.STYLE_ID.value(), "").equals(styleOp2.optString(OCKey.STYLE_ID.value(), "")));
    }

    /**
     * Transforms an indexed auto-style identifier in the passed record.
     *
     * @param record
     *  The record containing an auto-style identifier property.
     *
     * @param key
     *  The name of the auto-style identifier property in the record.
     *
     * @param type
     *  The type of the auto-styles referered by the record.
     *
     * @param styleOp
     *  The auto-style operation causing to transform the auto-style property
     *  in the record.
     *
     * @param insert
     *  Whether the passed auto-style operation is "insertAutoStyle" (`true`),
     *  or "deleteAutoStyle" (`false`).
     *
     * @returns
     *  Whether the property in the record has changed.
     */
    public static boolean transformAutoStyleProperty(JSONObject record, String key, String type, JSONObject styleOp, boolean insert) throws JSONException {

        // check the type of the passed auto-style operation
        if (!(getAutoStyleType(styleOp).equals(type))) {
            return false;
        }

        // check if the record contains an auto-style identifier
        final String styleId = record.optString(key, null);
        if(styleId==null) {
            return false;
        }

        final String recordStylePrefix = "a";
        Integer xfIndex = getAutoStyleIndex(styleId, recordStylePrefix);
        if(xfIndex==null) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, String.format("invalid style identifiers %s in indexed mode", styleId));
        }

        // try to get the parser RE for indexed auto-style identifiers
        final Integer styleIndex = getAutoStyleIndex(styleOp);
        if(styleIndex==null) {
            return false;
        }

        // transform the index of the record's auto-style identifier
        if (insert) {
            if (xfIndex < styleIndex) {
                return false;
            }
            xfIndex += 1;
        }
        else {
            if (xfIndex == styleIndex) {
                record.remove(key);
// TODO:
                styleOp.put("otIndexShift", true);
                return true;
            }
            if (xfIndex <= styleIndex) {
                return false;
            }
            xfIndex -= 1;
        }

        // write the new style identifier back into the record
        record.put(key, recordStylePrefix + xfIndex.toString());
// TODO:
        styleOp.put("otIndexShift", true);
        return true;
    }

    public static void isValidAutoStyleIdentifier(String styleId) {
        String recordStylePrefix = "a";
        Integer xfIndex = getAutoStyleIndex(styleId, recordStylePrefix);
        if(xfIndex==null) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, String.format("invalid style identifiers %s in indexed mode", styleId));
        }
    }
}
