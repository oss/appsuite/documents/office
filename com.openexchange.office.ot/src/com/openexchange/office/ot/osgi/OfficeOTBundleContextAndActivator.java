/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.osgi;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.monitoring.Statistics;

//=============================================================================
public class OfficeOTBundleContextAndActivator extends OsgiBundleContextAndActivator {

    private static OfficeOTBundleContextAndActivator INSTANCE;

    private static AtomicReference<Statistics> REF_STATISTICS = new AtomicReference<>();

    public OfficeOTBundleContextAndActivator() {
        super(new OfficeOTBundleContextIdentificator(), true);
        INSTANCE = this;
    }

    @Override
    protected Set<Class<?>> getAdditionalNeededServices() {
        Set<Class<?>> res = new HashSet<>();
        res.add(Statistics.class);
        return res;
    }

    public static Statistics getStatistics() {
        if (INSTANCE == null) {
            return null;
        }

        Statistics statistics = REF_STATISTICS.get();
        if (statistics == null) {
            if (REF_STATISTICS.compareAndSet(null, INSTANCE.getOptionalService(Statistics.class))) {
                statistics = REF_STATISTICS.get();
            }
        }
        return statistics;
    }

    @Override
    protected boolean logEntry(String entry) {
        return false;
    }
}
