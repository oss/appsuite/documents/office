/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.tools;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONValue;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.OTException;
import com.openexchange.office.ot.TransformOptions;
import com.openexchange.office.ot.osgi.OfficeOTBundleContextAndActivator;
import com.openexchange.office.tools.common.json.JSONHelper;
import com.openexchange.office.tools.monitoring.OTEvent;
import com.openexchange.office.tools.monitoring.OTEventType;
import com.openexchange.office.tools.monitoring.Statistics;

public class OTUtils {
    // -------------------------------------------------------------------------
    // Helper functions

    /**
     * Transforming an array of external operations by using all pending operations
     * that need to be applied to the external operations.
     *
     * @param {JSONArray} pendingOps
     *  The collection of operations that are already applied to the document on
     *  server side, but were missing, when the client generated the new operations
     *  before sending them to the server.
     *
     * @param {JSONArray} newOps
     *  The array that contains all operations sent from the client. After this OT
     *  process these operations are applied to the document and sent to all other
     *  clients.
     *
     * @returns {Object[]}
     *  The transformed external operations. These operations are applied to the
     *  document and sent to all other clients.
     */
    public static JSONArray transformOperations(TransformOptions options, ITransformHandlerMap[] transformHandlerMaps, final JSONArray pendingOps, final JSONArray newOps) throws JSONException {

        final JSONArray transformedOperations = new JSONArray();
        for (int i = 0; i < newOps.length(); i++) {

            final JSONObject oneExternOp = newOps.getJSONObject(i);
            JSONArray allExtOps = new JSONArray(); // the collector for all external operations
            allExtOps.put(oneExternOp);
            JSONObject result = null;

            for (int j = 0; j < pendingOps.length(); j++) {

                JSONArray newLclOpsBefore = null; // collect new local operations to be added before the current local operation
                JSONArray newLclOpsAfter = null; // collect new local operations to be added after the current local operation

                final JSONObject localOp = pendingOps.getJSONObject(j);

                for (int k = 0; k < allExtOps.length(); k++) {
                    final JSONObject externOp = allExtOps.getJSONObject(k);

                    result = applyLocalOpToExternalOp(options, transformHandlerMaps, localOp, externOp);

                    if (result != null) {

                        int afterLength = 0;
                        int beforeLength = 0;

                        final JSONArray externalOpsAfter = result.optJSONArray("externalOpsAfter");
                        if (externalOpsAfter!=null) {
                            afterLength = externalOpsAfter.length();
                            for (int m = 0; m < afterLength; m++) {
                                allExtOps.add(k + m + 1, externalOpsAfter.get(m));
                            }
                        }
                        final JSONArray externalOpsBefore = result.optJSONArray("externalOpsBefore");
                        if (externalOpsBefore!=null) {
                            beforeLength = externalOpsBefore.length();
                            for (int m = 0; m < beforeLength; m++) {
                                allExtOps.add(k + m, externalOpsBefore.get(m));
                            }
                        }
                        k = k + afterLength + beforeLength; // increasing the counter for the array allExtOps

                        // also collecting the new local operations
                        final JSONArray localOpsBefore = result.optJSONArray("localOpsBefore");
                        if (localOpsBefore!=null) {
                            if (newLclOpsBefore == null) {
                                newLclOpsBefore = new JSONArray();
                            }
                            for (int m = 0; m < localOpsBefore.length(); m++) {
                                newLclOpsBefore.put(localOpsBefore.get(m));
                            }
                        }
                        final JSONArray localOpsAfter = result.optJSONArray("localOpsAfter");
                        if (localOpsAfter!=null) {
                            if (newLclOpsAfter == null) {
                                newLclOpsAfter = new JSONArray();
                            }
                            for (int m = 0; m < localOpsAfter.length(); m++) {
                                newLclOpsAfter.put(localOpsAfter.get(m));
                            }
                        }
                    }
                };

                int afterLen = 0;
                int beforeLen = 0;

                if (newLclOpsAfter != null) {
                    afterLen = newLclOpsAfter.length();
                    for (int m = 0; m < afterLen; m++) {
                        pendingOps.add(j + m + 1, newLclOpsAfter.get(m));
                    }
                }
                if (newLclOpsBefore != null) {
                    beforeLen = newLclOpsBefore.length();
                    for (int m = 0; m < beforeLen; m++) {
                        pendingOps.add(j + m, newLclOpsBefore.get(m));
                    }
                }
                j = j + afterLen + beforeLen; // increasing the counter for the pending OPs
            };

            if (allExtOps.length() > 0) {
                for (int m = 0; m < allExtOps.length() ; m++) {
                    final JSONObject oneExtOp = allExtOps.getJSONObject(m);
                    if (!OTUtils.isOperationRemoved(oneExtOp)) {
                        transformedOperations.put(oneExtOp);
                    }
                }
            }
        };
        return transformedOperations;
    }


    // -------------------------------------------------------------------------
    /**
     * The operation dispatcher for the Operational Transformations.
     *
     * @param {JSONObject} localOp
     *  The local operation.
     *
     * @param {JSONObject} extOp
     *  The external operation.
     *
     * @param {JSONArray} allExtOps
     *  The container for the external operation(s).
     *
     * @param {int} extIdx
     *  The index of the external operation inside its operation container.
     *
     * @param {JSONArray} localOps
     *  The container with the local operations.
     *
     * @param {int} localIdx
     *  The index of the local operation inside its operation container of the action.
     *
     * @throws JSONException
     */
    public static JSONObject applyLocalOpToExternalOp(final TransformOptions options, ITransformHandlerMap[] transformHandlerMaps, final JSONObject localOp, final JSONObject externOp) throws JSONException {

        // check, if the local operation is removed in the meantime
        if (OTUtils.isOperationRemoved(localOp) || OTUtils.isOperationRemoved(externOp)) {
            return null;
        }

        final boolean isLocalIgnoreTarget = OTUtils.isIgnoreTargetOperation(localOp);
        final boolean isExternIgnoreTarget = OTUtils.isIgnoreTargetOperation(externOp);
        final boolean isLocalDeleteOp = OTUtils.isDeleteTargetOperation(localOp);
        final boolean isExternDeleteOp = OTUtils.isDeleteTargetOperation(externOp);
        final String localTarget = isLocalIgnoreTarget ? "" : OTUtils.getTarget(localOp);
        final String externTarget = isExternIgnoreTarget ? "" : OTUtils.getTarget(externOp);
        boolean forceUseHandler = (isLocalDeleteOp || isExternDeleteOp) &&  (isLocalIgnoreTarget || isExternIgnoreTarget);

        if(!forceUseHandler) {
            // check the target of the operations
            boolean hasLocalTarget = !isLocalIgnoreTarget && OTUtils.hasTarget(localOp);
            boolean hasExternTarget = !isExternIgnoreTarget && OTUtils.hasTarget(externOp);
            if (hasLocalTarget || hasExternTarget) {
                boolean isTargetRemove = isLocalDeleteOp ||isExternDeleteOp;
                if(!isTargetRemove) {
                    if ((hasLocalTarget && !hasExternTarget) || (!hasLocalTarget && hasExternTarget)) {
                        return null;
                    }
                    if (!localTarget.equals(externTarget)) {
                        return null;
                    }
                }
            }
        }
        final OCValue opA = OCValue.fromValue(localOp.getString(OCKey.NAME.value()));
        final OCValue opB = OCValue.fromValue(externOp.getString(OCKey.NAME.value()));
        final OpPair k = new OpPair(opA, opB);

        boolean bidi = false;
        ITransformHandler handler = null;
        for(ITransformHandlerMap transformHandlerMap : transformHandlerMaps) {
            if(transformHandlerMap.getTransformHandlerMap().containsKey(k)) {
                handler = transformHandlerMap.getTransformHandlerMap().get(k);
                if(handler == bidiHandler) { // if it is the static bidi handler then whe have to take the bidi version of this handler by swapping the arguments
                    handler = transformHandlerMap.getTransformHandlerMap().get(new OpPair(opB, opA));
                    bidi = true;
                }
                break;
            }
        }
        if (null != options.getDocumentType()) {
        	Statistics statistics = OfficeOTBundleContextAndActivator.getStatistics();
        	if (statistics != null) {
        		statistics.handleTransformation(new OTEvent(OTEventType.TRANSFORMATION, options.getDocumentType()));
        	}
        }
        if(handler==null) {
            for(ITransformHandlerMap transformHandlerMap : transformHandlerMaps) {
                final Set<OCValue> ignoreOperations = transformHandlerMap.getIgnoreOperations();
                if(ignoreOperations!=null && (ignoreOperations.contains(opA) || ignoreOperations.contains(opB))) {
                    return null;    // no handler is required if op is part of the ignore op list
                }
            }
            throw new OTException(OTException.Type.HANDLER_MISSING_ERROR, "applyLocalOpToExternalOp" + opA.toString() + "," + opB.toString());
        }

        final JSONObject result;
        if(bidi) {
            final JSONObject tmp = handler.handle(options, externOp, localOp);
            // the result needs to be swapped for bidi handler
            result = tmp!=null ? JSONHelper.getResultObject(tmp.optJSONArray("localOpsBefore"), tmp.optJSONArray("localOpsAfter"), tmp.optJSONArray("externalOpsBefore"), tmp.optJSONArray("externalOpsAfter")) : null;
        }
        else {
            result = handler.handle(options, localOp,  externOp);
        }
        return result;
    }

    public static JSONObject getOperation(OCValue op, JSONObject op1, JSONObject op2) throws JSONException {
        return op1.getString(OCKey.NAME.value()).equals(op.value()) ? op1 : op2;
    }

    public static JSONArray getStartPosition(final JSONObject operation) throws JSONException {
        return operation.getJSONArray(OCKey.START.value());
    }

    public static void setStartPosition(final JSONObject operation, JSONArray pos) throws JSONException {
        operation.put(OCKey.START.value(), pos);
    }

    public static JSONArray optEndPosition(final JSONObject operation) {
        return operation.optJSONArray(OCKey.END.value());
    }

    public static void setEndPosition(final JSONObject operation, JSONArray pos) throws JSONException {
        operation.put(OCKey.END.value(), pos);
    }

    public static void removeEndPosition(final JSONObject operation) {
        operation.remove(OCKey.END.value());
    }

    public static void removeType(final JSONObject operation) {
        operation.remove(OCKey.TYPE.value());
    }

    public static JSONArray getToPosition(final JSONObject operation) throws JSONException {
        return operation.getJSONArray(OCKey.TO.value());
    }

    public static JSONArray optToPosition(final JSONObject operation) {
        return operation.optJSONArray(OCKey.TO.value());
    }

    public static String getName(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value());
    }

    public static String getTarget(final JSONObject operation) {
        return operation.optString(OCKey.TARGET.value());
    }

    public static boolean hasTarget(final JSONObject operation) {
        return operation.has(OCKey.TARGET.value());
    }

    public static JSONObject getAttributes(final JSONObject operation) throws JSONException {
        return operation.getJSONObject(OCKey.ATTRS.value());
    }

    public static void setAttributes(final JSONObject operation, JSONObject attrs) throws JSONException {
        operation.put(OCKey.ATTRS.value(), attrs);
    }

    public static void setOperationName(final JSONObject operation, String value) throws JSONException {
        operation.put(OCKey.NAME.value(), value);
    }

    public static void setOperationNameDelete(final JSONObject operation) throws JSONException {
        operation.put(OCKey.NAME.value(), OCValue.DELETE.value());
    }

    public static void setOperationNameDeleteHeaderFooter(final JSONObject operation) throws JSONException {
        operation.put(OCKey.NAME.value(), OCValue.DELETE_HEADER_FOOTER.value());
    }

    public static void setOperationNameInsertDrawing(final JSONObject operation) throws JSONException {
        operation.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
    }

    public static String getText(final JSONObject operation) {
        return operation.optString(OCKey.TEXT.value());
    }

    public static boolean isInsertTextOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_TEXT.value());
    }

    public static boolean isDeleteOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE.value());
    }

    public static boolean isSetAttributesOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.SET_ATTRIBUTES.value());
    }

    public static boolean isUpdateComplexFieldOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.UPDATE_COMPLEX_FIELD.value());
    }

    public static boolean isUpdateFieldOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.UPDATE_FIELD.value());
    }

    public static boolean isAnyUpdateFieldOperation(final JSONObject operation) throws JSONException {
        return isUpdateFieldOperation(operation) || isUpdateComplexFieldOperation(operation);
    }

    public static boolean isChangeCommentOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.CHANGE_COMMENT.value());
    }

    public static boolean isInsertParagraphOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_PARAGRAPH.value());
    }

    public static boolean isInsertTableOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_TABLE.value());
    }

    public static boolean isInsertSlideOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_SLIDE.value());
    }

    public static boolean isSplitParagraphOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.SPLIT_PARAGRAPH.value());
    }

    public static boolean isMergeParagraphOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.MERGE_PARAGRAPH.value());
    }

    public static boolean isMergeTableOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.MERGE_TABLE.value());
    }

    public static boolean isSplitTableOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.SPLIT_TABLE.value());
    }

    public static boolean isInsertRowsOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_ROWS.value());
    }

    public static boolean isInsertCellsOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_CELLS.value());
    }

    public static boolean isInsertColumnOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.INSERT_COLUMN.value());
    }

    public static boolean isDeleteColumnsOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE_COLUMNS.value());
    }

    public static boolean isDeleteListStyleOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE_LIST_STYLE.value());
    }

    public static boolean isMoveOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.MOVE.value());
    }

    public static boolean isDeleteHeaderFooterOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE_HEADER_FOOTER.value());
    }

    public static boolean isDeleteTargetSlideOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE_TARGET_SLIDE.value());
    }

    public static boolean isDeleteStyleSheetOperation(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.NAME.value()).equals(OCValue.DELETE_STYLE_SHEET.value());
    }

    public static boolean isDeleteTargetOperation(final JSONObject operation) throws JSONException {
        return isDeleteHeaderFooterOperation(operation) || isDeleteTargetSlideOperation(operation);
    }

    public static boolean isIgnoreTargetOperation(final JSONObject operation) throws JSONException {
        switch(OCValue.fromValue(operation.getString(OCKey.NAME.value()))) {
            case INSERT_SLIDE :
            // PASSTHROUGH INTENDED
            case INSERT_LAYOUT_SLIDE :
            // PASSTHROUGH INTENDED
            case INSERT_MASTER_SLIDE :
            // PASSTHROUGH INTENDED
            case MOVE_LAYOUT_SLIDE :
            // PASSTHROUGH INTENDED
            case CHANGE_LAYOUT :
            // PASSTHROUGH INTENDED
            case CHANGE_MASTER :
                return true;
            default:
                return false;
        }
    }

    public static String getIdProperty(final JSONObject operation) throws JSONException {
        return operation.getString(OCKey.ID.value());
    }

    public static String getStyleIdProperty(final JSONObject operation) {
        return operation.optString(OCKey.STYLE_ID.value());
    }

    public static int getCountProperty(final JSONObject operation, int def) {
        return operation.optInt(OCKey.COUNT.value(), def);
    }

    public static int getStartGridProperty(final JSONObject operation) throws JSONException {
        return operation.getInt(OCKey.START_GRID.value());
    }

    public static void setStartGridProperty(final JSONObject operation, int value) throws JSONException {
        operation.put(OCKey.START_GRID.value(), value);
    }

    public static void removeStartGridProperty(final JSONObject operation) {
        operation.remove(OCKey.START_GRID.value());
    }

    public static int getEndGridProperty(final JSONObject operation) throws JSONException {
        return operation.getInt(OCKey.END_GRID.value());
    }

    public static void setEndGridProperty(final JSONObject operation, int value) throws JSONException {
        operation.put(OCKey.END_GRID.value(), value);
    }

    public static void removeEndGridProperty(final JSONObject operation) {
        operation.remove(OCKey.END_GRID.value());
    }

    public static int getGridPositionProperty(final JSONObject operation) throws JSONException {
        return operation.getInt(OCKey.GRID_POSITION.value());
    }

    public static void setGridPositionProperty(final JSONObject operation, int value) throws JSONException {
        operation.put(OCKey.GRID_POSITION.value(), value);
    }

    public static String getInsertModeProperty(final JSONObject operation) {
        return operation.optString(OCKey.INSERT_MODE.value(), "behind");
    }

    public static boolean isInsertModeBehind(final String mode) {
        return mode.equals("behind");
    }

    public static boolean isInsertModeBefore(final String mode) {
        return mode.equals("before");
    }

    public static String getMergeLengthProperty(final JSONObject operation) throws JSONException {
        return isMergeTableOperation(operation) ? "rowcount" : "paralength";
    }

    public static String getMergeNamePropertyAfterSplit(final JSONObject operation) throws JSONException {
        return isSplitTableOperation(operation) ? OCValue.MERGE_TABLE.value() : OCValue.MERGE_PARAGRAPH.value();
    }

    public static String getSplitNamePropertyAfterMerge(final JSONObject operation) throws JSONException {
        return isMergeTableOperation(operation) ? OCValue.SPLIT_TABLE.value() : OCValue.SPLIT_PARAGRAPH.value();
    }

    public static String getMergeLengthPropertyAfterSplit(final JSONObject operation) throws JSONException {
        return isSplitTableOperation(operation) ? "rowcount" : "paralength";
    }

    public static int getParaLengthProperty(final JSONObject operation) throws JSONException {
        String key = isMergeTableOperation(operation) ? "rowcount" : "paralength";
        return operation.optInt(key, 0);
    }

    public static void setParaLengthProperty(final JSONObject operation, int value) throws JSONException {
        String key = isMergeTableOperation(operation) ? "rowcount" : "paralength";
        operation.put(key, value);
    }

    public static JSONArray getTableGridProperty(final JSONObject operation) throws JSONException {
        return operation.getJSONArray(OCKey.TABLE_GRID.value());
    }

    public static void setTableGridProperty(final JSONObject operation, JSONArray value) throws JSONException {
        operation.put(OCKey.TABLE_GRID.value(), value);
    }

    public static int getReferenceRowProperty(final JSONObject operation) throws JSONException {
        return operation.getInt(OCKey.REFERENCE_ROW.value());
    }

    public static boolean hasReferenceRowProperty(final JSONObject operation) {
        return operation.has(OCKey.REFERENCE_ROW.value());
    }

    public static void setReferenceRowProperty(final JSONObject operation, int value) throws JSONException {
        operation.put(OCKey.REFERENCE_ROW.value(), value);
    }

    public static boolean checkChangeIntervalsNoOp(final JSONObject intervalOp) throws JSONException {
        final boolean removed = !intervalOp.has(OCKey.ATTRS.value()) && !intervalOp.has(OCKey.S.value());
        if(removed) {
            setOperationRemoved(intervalOp);
        }
        return removed;
    }

    public static void checkChangeTableNoOp(JSONObject changeOp) throws JSONException {
        if (!changeOp.has(OCKey.ATTRS.value()) && !changeOp.has(OCKey.NEW_NAME.value()) && !changeOp.has(OCKey.RANGE.value()) && !changeOp.has(OCKey.HEADERS.value())) {
            setOperationRemoved(changeOp);
        }
    }

    public static void checkChangeCellsNoOp(final JSONObject changeCellOp) throws JSONException {
        if(changeCellOp.getJSONObject(OCKey.CONTENTS.value()).isEmpty()) {
            setOperationRemoved(changeCellOp);
        }
    }

    public static boolean hasSameSheetName(JSONObject op1, JSONObject op2) {
        final String s1 = op1.optString(OCKey.SHEET_NAME.value(), null);
        final String s2 = op2.optString(OCKey.SHEET_NAME.value(), null);
        return (s1!=null && s2!=null && s1.equals(s2));
    }

    public static void setOperationRemoved(final JSONObject operation) throws JSONException {
        operation.put("_REMOVED_OPERATION_", true);
    }

    public static boolean isOperationRemoved(final JSONObject operation) {
        return operation.has("_REMOVED_OPERATION_");
    }

    public static void setConflictReloadRequired(final JSONObject operation) throws JSONException {
        operation.put("_CONFLICT_RELOAD_REQUIRED_", true);
    }

    public static boolean isConflictReloadRequired(final JSONObject op1, final JSONObject op2) {
        return op1.has("_CONFLICT_RELOAD_REQUIRED_") || (op2 != null && op2.has("_CONFLICT_RELOAD_REQUIRED_"));
    }

    public static String getInstructionProperty(final JSONObject operation) {
        return operation.optString(OCKey.INSTRUCTION.value(), "");
    }

    public static String getTypeProperty(final JSONObject operation) {
        return operation.optString(OCKey.TYPE.value(), "");
    }

    public static String getRepresentationProperty(final JSONObject operation) {
        return operation.optString(OCKey.REPRESENTATION.value(), "");
    }

    public static String getListStyleIdProperty(final JSONObject operation) {
        return operation.optString(OCKey.LIST_STYLE_ID.value(), "");
    }

    public static Boolean isSameStyleSheet(final JSONObject op1, final JSONObject op2) {
        return OTUtils.getStyleIdProperty(op1).equals(OTUtils.getStyleIdProperty(op2)) && OTUtils.getTypeProperty(op1).equals(OTUtils.getTypeProperty(op2));
    }

    // returns true if a is contained in as
    public static boolean contains(String a, String[] as) {
        for(String v:as) {
            if(a.equals(v)) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------

    public static int indexOf(JSONArray array, int val) throws JSONException {
        for(int i=0; i < array.length(); i++) {
            if(array.getInt(i)==val) {
                return i;
            }
        }
        return -1;
    }

    // -------------------------------------------------------------------------

    public static JSONArray cloneJSONArray(JSONArray source) throws JSONException {
        final JSONArray dest = new JSONArray(source.length());
        for(int i=0; i < source.length(); i++) {
            final Object s = source.get(i);
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(i, d);
        }
        return dest;
    }

    public static JSONObject cloneJSONObject(JSONObject source) throws JSONException {
        final JSONObject dest = new JSONObject(source.length());
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            final Object s = sourceEntry.getValue();
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(sourceEntry.getKey(), d);
        }
        return dest;
    }

    /**
     * Check, whether a specified jsonArray of int numbers contains only directly following numbers or if
     * there is a gap between two following numbers. Furthermore it is expected, that the numbers
     * in the array are sorted.
     *
     * This function is required for evaluating group and ungroup operations
     *
     * @param {JSONArray} intArray
     *  A (sorted) number jsonArray of int values
     *
     * @returns {Boolean}
     *  Whether the specified array of numbers contains a gap between two directly following numbers.
     */
    public static boolean hasGap(JSONArray intArray) throws JSONException {
        if(intArray.length()>1) {
            int last = intArray.getInt(0);
            for(int i=1; i<intArray.length(); i++) {
                if(intArray.getInt(i)!=++last) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void registerSelfTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue value, ITransformHandler handler) {
        registerTransformation(map, aliases, value, value, handler, false);
    }

    public static void registerSelfTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] value, ITransformHandler handler) {
        registerTransformation(map, aliases, value, value, handler, false);
    }

    public static void registerSelfTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] value, ITransformHandler handler) {
        registerTransformation(map, aliases, value, value, handler, false);
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue[][] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue[][] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue[] a1 : valueA) {
            for(OCValue a : a1) {
                for(OCValue[] b1 : valueB) {
                    for(OCValue b : b1) {
                        putTransformationHandler(map, aliases, a, true, b, true, handler, bidi);
                    }
                }
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue[] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue[] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue[] a1 : valueA) {
            for(OCValue a : a1) {
                for(OCValue b : valueB) {
                    putTransformationHandler(map, aliases, a, true, b, true, handler, bidi);
                }
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[][] valueA, OCValue valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue[] a1 : valueA) {
            for(OCValue a : a1) {
                putTransformationHandler(map, aliases, a, true, valueB, false, handler, bidi);
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue[][] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue[][] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue a : valueA) {
            for(OCValue[] b1 : valueB) {
                for(OCValue b : b1) {
                    putTransformationHandler(map, aliases, a, true, b, true, handler, bidi);
                }
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue[] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue[] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue a : valueA) {
            for(OCValue b : valueB) {
                putTransformationHandler(map, aliases, a, true, b, true, handler, bidi);
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue[] valueA, OCValue valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue a : valueA) {
            putTransformationHandler(map, aliases, a, true, valueB, false, handler, bidi);
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue[][] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue[][] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue[] b1 : valueB) {
            for(OCValue b : b1) {
                putTransformationHandler(map, aliases, valueA, false, b, true, handler, bidi);
            }
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue[] valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue[] valueB, ITransformHandler handler, boolean bidi) {
        for(OCValue b : valueB) {
            putTransformationHandler(map, aliases, valueA, false, b, true, handler, bidi);
        }
    }

    public static void registerBidiTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue valueB, ITransformHandler handler) {
        registerTransformation(map, aliases, valueA, valueB, handler, true);
    }

    public static void registerTransformation(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, OCValue valueB, ITransformHandler handler, boolean bidi) {
        putTransformationHandler(map, aliases, valueA, false, valueB, false, handler, bidi);
    }

    public static void putTransformationHandler(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, boolean isAliasA, OCValue valueB, boolean isAliasB, ITransformHandler handler, boolean bidi) {
        putTransformationHandler(map, aliases, valueA, isAliasA, valueB, isAliasB, bidi, handler);
    }

    public static JSONObject bidiHandler() {
        return null;
    }

    final static ITransformHandler bidiHandler = (options, localOp, externOp) -> bidiHandler();

    public static void putTransformationHandler(Map<OpPair, ITransformHandler> map, Set<OpPair> aliases, OCValue valueA, boolean isAliasA, OCValue valueB, boolean isAliasB, boolean bidi, ITransformHandler handler) {
        final OpPair k1 = new OpPair(valueA, valueB);
        final OpPair bidiPair = bidi ? new OpPair(valueB, valueA) : null;
        if(!isAliasA && !isAliasB) {
            map.put(k1, handler); // no alias is used, we have to add this key...
            if(bidiPair!=null) {
                map.put(bidiPair, bidiHandler);
            }
            aliases.remove(k1);
        }
        else { // we have somehow an alias
            final ITransformHandler h = map.get(k1);
            if(h==null) {
                map.put(k1, handler); // there was no handler
                if(bidiPair!=null) {
                    map.put(bidiPair, bidiHandler);
                }
                aliases.add(k1);
            }
            else if(isAliasA || isAliasB) {
                return; // there is already a handler
            }
            else {
                map.put(k1, handler);
                if(bidiPair!=null) {
                    map.put(bidiPair, bidiHandler);
                }
                aliases.add(k1);
            }
        }
    }

    /**
     * Reduces a specific property in local and external ditionaries for operation
     * transformation. This generic function can be used for operation properties,
     * for formatting attribute sets, etc.
     *
     * Removes the property from the external dictionary if it is contained in the
     * local dictionary, and the property values are different (local operations
     * always win by definition).
     *
     * Optionally, properties with (deeply) equal values will be removed from both
     * dictionaries.
     *
     * @param lclMap
     *  The dictionary from the local operation (already applied) that overrules
     *  the external operation. This dictionary will be reduced in-place!
     *
     * @param extMap
     *  The dictionary from the external operation (to be applied locally). This
     *  dictionary will be reduced in-place!
     *
     * @param keys
     *  The names of all properties (or a single property name) to be reduced in
     *  both dictionaries.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  Whether the property has been removed from the external dictionary.
     */
    public static boolean reduceProperties(JSONObject lclMap, JSONObject extMap, ReducePropertyOptions options, String...keys) {

        // whether the property has been deleted
        boolean reduced = false;

        // process single string or string array
        for(String key:keys) {

            final Object lclVal = lclMap.opt(key);
            final Object extVal = extMap.opt(key);

            // nothing to do, if either property is missing
            if(lclVal==null || extVal==null) {
                continue;
            }
            final boolean isEqual;
            if(lclVal instanceof JSONValue) {
                if(extVal instanceof JSONValue) {
                    isEqual = ((JSONValue)lclVal).isEqualTo((JSONValue)extVal);
                }
                else {
                    continue;
                }
            }
            else {
                isEqual = lclVal.equals(extVal);
            }

            // delete from local and external, if equal to external (only in optional delete-equal mode)
            if (options.isDeleteEqual()) {
                if(isEqual) {
                    lclMap.remove(key);
                    extMap.remove(key);
                    reduced = true;
                }
                else {
                    // delete from external, if different to local (or always in delete-equal mode)
                    extMap.remove(key);
                    reduced = true;
                }
            }
        };
        return reduced;
    }

    /**
     * Invokes a callback function for the value of an existing attribute in the
     * passed attributed document operation, and inserts the return value of the
     * callback function into the attribute set.
     *
     * @param attrOp
     *  The JSON document operation with an (optional) attribute set.
     *
     * @param family
     *  The style family of the attribute to be transformed.
     *
     * @param key
     *  The key of the attribute to be transformed.
     *
     * @param callback
     *  The callback function that will be invoked, if the specified attribute
     *  exists in the attribute set of the operation. Receives the attribute value,
     *  and the attribute key. Returns the new value of the attribute. Can return
     *  `undefined` to keep the attribute unchanged.
     */
/*
    export function transformAttribute(attrOp: AnyAttrOperation, family: string, key: string, callback: (value: unknown, key: string) => unknown): void {
        const attrMap = getDict(attrOp.attrs, family);
        if (attrMap && (key in attrMap)) {
            const value = callback(attrMap[key], key);
            if (!isUndefined(value)) { attrMap[key] = value; }
        }
    }
*/

    /**
    * Reduces the local and external attribute set for operation transformation.
    *
    * Removes the attributes from the external attribute set that are contained in
    * the local attribute set, and have different values, so that the external
    * attribute set can be applied to an object afterwards, but behaves as if this
    * happened before applying the local attribute set.
    *
    * Optionally, removes attributes with equal values from the local and external
    * attribute set. These attributes do not need to be applied again locally and
    * externally (optionally, because sometimes it may be better to keep these
    * attributes to prevent duplication of operations for partially overlapping
    * format ranges).
    *
    * @param lclAttrSet
    *  The local attribute set already applied at an object that overrules the
    *  external attributes. This object will be reduced in-place!
    *
    * @param extAttrSet
    *  The external attribute set to be applied at an object without changing the
    *  local attributes. This object will be reduced in-place!
    *
    * @param [options]
    *  Optional parameters.
    *
    * @returns
    *  Whether at least one attribute has been removed from the attribute sets.
    */
    public static boolean reduceAttributeSets(JSONObject lclAttrSet, JSONObject extAttrSet, ReducePropertyOptions options) {

       // whether an attribute has been deleted in the attribute sets
       boolean reduced = false;

       // process all attribute maps in the external attribute set
       final Iterator<Entry<String, Object>> extAttrSetIter = extAttrSet.entrySet().iterator();
       while(extAttrSetIter.hasNext()) {
           final Entry<String, Object> extAttrSetEntry = extAttrSetIter.next();

           if(extAttrSetEntry.getKey().equals(OCKey.STYLE.value())) {
               reduced = reduceProperties(lclAttrSet, extAttrSet, options, OCKey.STYLE.value()) || reduced;
               continue;
           }

           final Object lclValue = lclAttrSet.opt(extAttrSetEntry.getKey());
           final Object extValue = extAttrSetEntry.getValue();

           if(lclValue instanceof JSONObject && extValue instanceof JSONObject) {

               final Object[] keys = ((JSONObject)extValue).keySet().toArray();
               for(Object key:keys) {
                   reduced = reduceProperties((JSONObject)lclValue, (JSONObject)extValue, options, (String)key) || reduced;
               }
               if(((JSONObject)lclValue).isEmpty()) {
                   lclAttrSet.remove(extAttrSetEntry.getKey());
               }
               if(((JSONObject)extValue).isEmpty()) {
                   extAttrSet.remove(extAttrSetEntry.getKey());
               }
           }
       }
       return reduced;
   }

   /**
    * Removes the property `attrs` from the passed operation, if it is empty; or
    * optionally sets the entire operation to "removed" state in this case.
    *
    * @param attrOp
    *  The operation with an optional `attrs` property.
    *
    * @param options
    *  Optional parameters.
    * @throws JSONException
    */
    public static void removeEmptyAttributes(JSONObject attrsOp, RemoveEmptyAttrsOptions options) throws JSONException {
        final JSONObject attrs = attrsOp.optJSONObject(OCKey.ATTRS.value());
        if(attrs!=null&&attrs.isEmpty()) {
            if(options.isRemoveEmptyOp()) {
               OTUtils.setOperationRemoved(attrsOp);
            }
            else {
                attrsOp.remove(OCKey.ATTRS.value());
            }
        }
    }

    /**
     * Reduces the attribute sets in the local and external operation for operation
     * transformation, using the function `reduceAttributeSets()`.
     *
     * @param lclOp
     *  The local operation with an attribute set already applied at an object that
     *  overrules the external attributes. The attribute set will be reduced
     *  in-place, and will even be deleted from the operation if drained.
     *
     * @param extOp
     *  The external operation with an attribute set to be applied at an object
     *  without changing the local attributes. The attribute set will be reduced
     *  in-place, and will even be deleted from the operation if drained.
     *
     * @param options
     *  parameters.
     * @throws JSONException
     *
     * @returns
     *  Whether at least one attribute has been removed from the attribute sets.
     */
    public static boolean reduceOperationAttributes(JSONObject lclOp, JSONObject extOp) throws JSONException {
        return reduceOperationAttributes(lclOp, extOp, new ReduceOperationAttrsOptions());
    }

    public static boolean reduceOperationAttributes(JSONObject lclOp, JSONObject extOp, ReduceOperationAttrsOptions options) throws JSONException {
        final JSONObject lclAttrs = lclOp.optJSONObject(OCKey.ATTRS.value());
        final JSONObject extAttrs = extOp.optJSONObject(OCKey.ATTRS.value());
        final boolean reduced = lclAttrs!=null && extAttrs!=null && reduceAttributeSets(lclAttrs, extAttrs, options.getReducePropertyOptions());
        removeEmptyAttributes(lclOp, options.getRemoveEmptyAttrsOptions());
        removeEmptyAttributes(extOp, options.getRemoveEmptyAttrsOptions());
        return reduced;
    }

    /**
     * Transforms an array index against an "insert" operation for multiple
     * elements in the same array.
     *
     * @param xfIdx
     *  The array index to be transformed according to the "insert" oeration.
     *
     * @param insIdx
     *  The index in the array where the first new element will be inserted.
     *
     * @param insSize
     *  The number of inserted array elements.
     *
     * @returns
     *  The transformed array index. If the new array elements will be inserted
     *  before the passed index, it will be increased by the number of inserted
     *  elements.
     */
    public static int transformIndexInsert(int xfIdx, int insIdx, int insSize) {
        return (xfIdx < insIdx) ? xfIdx : (xfIdx + insSize);
    }

    /**
     * Transforms an array index against a "delete" operation for multiple elements
     * in the same array.
     *
     * @param xfIdx
     *  The array index to be transformed according to the "delete" oeration.
     *
     * @param delIdx
     *  The index of the first array element to be deleted.
     *
     * @param delSize
     *  The number of deleted array elements.
     *
     * @param [keepDel=false]
     *  If set to `true`, the array index will not be "deleted" (as indicated by
     *  returning `undefined`) but will be moved to the index of the first deleted
     *  array element.
     *
     * @returns
     *  The transformed array index. If the array elements will be deleted before
     *  the passed index, it will be decreased by the number of deleted elements.
     *  If the element itself will be deleted, `undefined` will be returned.
     */
    public static Integer transformIndexDelete(int xfIdx, int delIdx, int delSize, Boolean keepDel) {
        if(xfIdx < delIdx) {
            return xfIdx;
        }
        else if(xfIdx < delIdx + delSize) {
            return keepDel!=null && keepDel.booleanValue() ? delIdx : null;
        }
        else {
            return (xfIdx - delSize);
        }
    }

    /**
     * Transforms array indexes for an "insert" operation concurring with a "copy"
     * operation in the same array.
     *
     * @param insIdx
     *  The array index of the new array element.
     *
     * @param fromIdx
     *  The array index of the copied array element.
     *
     * @param toIdx
     *  The index in the array where the copied element will be inserted.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTMoveShiftResult transformIndexInsertCopy(int insIdx, int fromIdx, int toIdx) {

        // transform the index of the copied element according to the insert index
        fromIdx = transformIndexInsert(fromIdx, insIdx, 1);
        // "insert" wins over "copy" per definition (shift cloned element away)
        if (insIdx <= toIdx) {
            toIdx += 1;
        }
        else {
            insIdx += 1;
        }
        return new OTMoveShiftResult(insIdx, new OTMoveResult(fromIdx, toIdx));
    }

   /*
    * Transforms an array index against a "move" operation for multiple elements
    * in the same array.
    *
    * @param xfIdx
    *  The array index to be transformed according to the "move" operation.
    *
    * @param fromIdx
    *  The index of the first array element that will be moved to another index.
    *
    * @param fromSize
    *  The number of moved array elements.
    *
    * @param toIdx
    *  The index in the array where the elements will be moved to.
    *
    * @returns
    *  The transformed array index.
    */
   public static int transformIndexMove(int xfIdx, int fromIdx, int fromSize, int toIdx) {
       // element moved by itself
       if ((fromIdx <= xfIdx) && (xfIdx < fromIdx + fromSize)) {
           return toIdx + xfIdx - fromIdx;
       }
       // element shifted backwards
       if ((fromIdx <= xfIdx) && (xfIdx < toIdx + fromSize)) {
           return xfIdx - fromSize;
       }
       // element shifted forwards
       if ((toIdx <= xfIdx) && (xfIdx < fromIdx)) {
           return xfIdx + fromSize;
       }
       // element not moved at all
       return xfIdx;
   }

   /**
    * Transforms an array index against a "sort" operation in the same array.
    *
    * @param xfIdx
    *  The array index to be transformed according to the "sort" operation.
    *
    * @param sortVec
    *  The sort vector for the array elements. See description of `OTSortVector`
    *  for details.
    *
    * @returns
    *  The transformed array index.
    */
   public static int transformIndexSort(int xfIdx, OTSortVector sortVec) {
       final int newIdx = ArrayUtils.indexOf(sortVec.getVector(), xfIdx);
       return (newIdx >= 0) ? newIdx : xfIdx;
   }

   /**
     * Transforms an "insert" operation and a "sort" operation in the same array.
     *
     * @param insIdx
     *  The array index of the new element inserted into the array.
     *
     * @param sortVec
     *  The sort vector for the array elements.
     *
     * @returns
     *  The transformed index and sort vector for both operations.
     */
    public static OTShiftSortResult transformIndexInsertSort(int insIdx, OTSortVector sortVec) {
        final int[] transformedSort = new int[sortVec.vec.length];
        for(int i=0; i<sortVec.vec.length; i++) {
            transformedSort[i] = transformIndexInsert(sortVec.vec[i], insIdx, 1);
        }
        sortVec = new OTSortVector(transformedSort);
        sortVec = sortVec.splice(insIdx, 0, Integer.valueOf(insIdx));
        // the position of the inserted element will not change
        return new OTShiftSortResult(insIdx, checkSortVec(sortVec));
    }

    /**
     * Transforms array indexes for an "insert" operation concurring with a "move"
     * operation in the same array.
     *
     * @param insIdx
     *  The array index of the new array element.
     *
     * @param fromIdx
     *  The array index of the moved array element.
     *
     * @param toIdx
     *  The index in the array where the element will be moved to.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTShiftMoveResult transformIndexInsertMove(int insIdx, int fromIdx, int toIdx) {

        // transform the move indexes according to the insert operation
        fromIdx = transformIndexInsert(fromIdx, insIdx, 1);
        toIdx = transformIndexInsert(toIdx, insIdx, 1);
        // transform the insert index according to the *new* move indexes
        insIdx = transformIndexMove(insIdx, fromIdx, 1, toIdx);

        return new OTShiftMoveResult(insIdx, fromIdx!= toIdx ? new OTMoveResult(fromIdx, toIdx) : null);
    }

    /**
     * Transforms array indexes for a "delete" operation concurring with a "move"
     * operation in the same array.
     *
     * @param delIdx
     *  The array index of the deleted array element.
     *
     * @param fromIdx
     *  The array index of the moved array element.
     *
     * @param toIdx
     *  The index in the array where the element will be moved to.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTShiftMoveResult transformIndexDeleteMove(int delIdx, int fromIdx, int toIdx) {

        // moved element will be deleted too: delete operation wins
        if (delIdx == fromIdx) {
            return new OTShiftMoveResult(toIdx, null);
        }

        // transform the delete index according to the move operation
        final int shiftIdx = transformIndexMove(delIdx, fromIdx, 1, toIdx);
        // compare source index with *old* deletion index (equality already checked above)
        fromIdx = transformIndexDelete(fromIdx, delIdx, 1, true);
        // compare target index with *new* deletion index (equality already checked above)
        toIdx = transformIndexDelete(toIdx, shiftIdx, 1, true);
        return new OTShiftMoveResult(shiftIdx, moveResult(fromIdx, toIdx));
    }

    /**
     * Transforms array indexes for a "delete" operation concurring with a "copy"
     * operation in the same array.
     *
     * @param delIdx
     *  The array index of the deleted array element.
     *
     * @param fromIdx
     *  The array index of the copied array element.
     *
     * @param toIdx
     *  The index in the array where the copied element will be inserted.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTDeleteCopyResult transformIndexDeleteCopy(int delIdx, int fromIdx, int toIdx) {

        // move source index of copied element according to deleted element
        final Integer newFromIdx = transformIndexDelete(fromIdx, delIdx, 1, false);

        // transform delete index according to index of cloned element
        final int shiftIdx = transformIndexInsert(delIdx, toIdx, 1);

        // keep target index if it would be deleted
        toIdx = transformIndexDelete(toIdx, delIdx, 1, true);

        final OTMoveResult moveRes = newFromIdx!=null ? new OTMoveResult(newFromIdx, toIdx) : null;
        // create the result object (omit copy indexes if element was deleted)
        return new OTDeleteCopyResult(shiftIdx, moveRes, moveRes!=null ? null : toIdx);
    }

    /**
     * Transforms a "delete" operation and a "sort" operation in the same array.
     *
     * @param delIdx
     *  The array index of the deleted array element.
     *
     * @param sortVec
     *  The sort vector for the array elements.
     *
     * @returns
     *  The transformed index and sort vector for both operations.
     */
    public static OTShiftSortResult transformIndexDeleteSort(int delIdx, OTSortVector sortVec) {
        final int newDelIdx = transformIndexSort(delIdx, sortVec);
        sortVec = new OTSortVector(sortVec.getVector());
        sortVec = sortVec.splice(newDelIdx, 1, null);

        final int[] transformedSort = new int[sortVec.vec.length];
        for(int i=0; i<sortVec.vec.length; i++) {
            transformedSort[i] = transformIndexDelete(sortVec.getVector()[i], delIdx, 1, true);
        }
        return new OTShiftSortResult(newDelIdx, checkSortVec(new OTSortVector(transformedSort)));
    }

    /**
     * Transforms array indexes for concurring "move" operations in the same array.
     *
     * @param lclFrom
     *  The array index of the locally moved element.
     *
     * @param lclTo
     *  The index in the array where the local element will be moved to.
     *
     * @param extFrom
     *  The array index of the externally moved element.
     *
     * @param extTo
     *  The index in the array where the external element will be moved to.
     *
     * @returns
     *  The transformed array indexes for both "move" operations.
     */
    public static OTMoveMoveResult transformIndexMoveMove(int lclFrom, int lclTo, int extFrom, int extTo) {

        // transform both "from" positions according to the opposite move operation
        final int newLclFrom = transformIndexMove(lclFrom, extFrom, 1, extTo);
        final int newExtFrom = transformIndexMove(extFrom, lclFrom, 1, lclTo);

        // special handling needed if both clients move the same element, or if the external move is a no-op:
        // local operation wins, external operation must be ignored
        if ((lclFrom == extFrom) || (extFrom == extTo)) {
            extTo = newExtFrom;
        }
        else {
            // transform local target index for server according to external operation (already applied there)
            lclTo = transformIndexMove(lclTo, newExtFrom, 1, extTo);
            // transform external target index according to local operation already applied
            extTo = transformIndexMove(extTo, newLclFrom, 1, lclTo);
        }
        return new OTMoveMoveResult(moveResult(newLclFrom, lclTo), moveResult(newExtFrom, extTo));
    }

    /**
     * Transforms array indexes for a "move" operation concurring with a "copy"
     * operation in the same array.
     *
     * @param moveFrom
     *  The array index of the moved element.
     *
     * @param moveTo
     *  The index in the array where the element will be moved to.
     *
     * @param copyFrom
     *  The array index of the copied element.
     *
     * @param copyTo
     *  The index in the array where the copied element will be inserted.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTMoveCopyResult transformIndexMoveCopy(int moveFrom, int moveTo, int copyFrom, int copyTo) {

        final int oldMoveFrom = moveFrom,
                  oldMoveTo = moveTo;

        // transform move indexes
        moveFrom = transformIndexInsert(moveFrom, copyTo, 1);
        moveTo = transformIndexInsert(moveTo, copyTo, 1);
        // transform copy indexes
        copyFrom = transformIndexMove(copyFrom, oldMoveFrom, 1, oldMoveTo);
        copyTo = transformIndexMove(copyTo, moveFrom, 1, moveTo);

        return new OTMoveCopyResult(moveResult(moveFrom, moveTo), new OTMoveResult(copyFrom, copyTo));
    }


    /**
     * Transforms array indexes for a "move" operation concurring with a "sort"
     * operation in the same array.
     *
     * @param fromIdx
     *  The array index of the moved element.
     *
     * @param toIdx
     *  The index in the array where the element will be moved to.
     *
     * @param sortVec
     *  The sort vector for the array elements.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTMoveSortResult transformIndexMoveSort(int fromIdx, int toIdx, OTSortVector sortVec) {
        // adjust the sort vector to neutralize the move operation
        final int[] transformedSort = new int[sortVec.vec.length];
        for(int i=0; i<sortVec.vec.length; i++) {
            transformedSort[i] = transformIndexMove(sortVec.vec[i], fromIdx, 1, toIdx);
        }
        sortVec = new OTSortVector(transformedSort);
        // always ignore the move operation (per definition)
        return new OTMoveSortResult(null, checkSortVec(sortVec));
    }

    /**
     * Transforms array indexes for concurring "copy" operations in the same array.
     *
     * @param lclFrom
     *  The array index of the locally copied element.
     *
     * @param lclTo
     *  The index in the array where the new local element will be inserted.
     *
     * @param extFrom
     *  The array index of the externally copied element.
     *
     * @param extTo
     *  The index in the array where the new external element will be inserted.
     *
     * @returns
     *  The transformed array indexes for both "copy" operations.
     */
    public static OTCopyCopyResult transformIndexCopyCopy(int lclFrom, int lclTo, int extFrom, int extTo) {
        final int oldLclTo = lclTo;
        lclFrom = transformIndexInsert(lclFrom, extTo, 1);
        lclTo = transformIndexInsert(oldLclTo, extTo, 1);
        extFrom = transformIndexInsert(extFrom, oldLclTo, 1);
        extTo = transformIndexInsert(extTo, lclTo, 1);
        return new OTCopyCopyResult(new OTMoveResult(lclFrom, lclTo), new OTMoveResult(extFrom, extTo));
    }

    /**
     * Transforms array indexes for a "copy" operation concurring with a "sort"
     * operation in the same array.
     *
     * @param fromIdx
     *  The array index of the copied element.
     *
     * @param toIdx
     *  The index in the array where the new element will be inserted.
     *
     * @param sortVec
     *  The sort vector for the array elements.
     *
     * @returns
     *  The transformed array indexes for both operations.
     */
    public static OTMoveSortResult transformIndexCopySort(int fromIdx, int toIdx, OTSortVector sortVec) {
        fromIdx = transformIndexSort(fromIdx, sortVec);

        final int[] transformedSort = new int[sortVec.vec.length];
        for(int i=0; i<sortVec.vec.length; i++) {
            transformedSort[i] = transformIndexInsert(sortVec.vec[i], toIdx, 1);
        }
        sortVec = new OTSortVector(transformedSort);
        // the insertion index of the copied element will not be changed
        sortVec = sortVec.splice(toIdx, 0, toIdx);
        return new OTMoveSortResult(new OTMoveResult(fromIdx, toIdx), checkSortVec(sortVec));
    }

    /**
     * Transforms array indexes for concurring "sort" operations in the same array.
     *
     * @param lclSortVec
     *  The sort vector of the local "sort" operation.
     *
     * @param extSortVec
     *  The sort vector of the external "sort" operation.
     *
     * @returns
     *  The transformed sort vectors for both "sort" operations.
     */
    public static OTSortSortResult transformIndexSortSort(OTSortVector lclSortVec, OTSortVector extSortVec) {
        // transform local sort vector according to external sorting
        final int[] transformedSort = new int[lclSortVec.vec.length];
        for(int i=0; i<lclSortVec.vec.length; i++) {
            transformedSort[i] = transformIndexSort(lclSortVec.vec[i], extSortVec);
        }
        lclSortVec = new OTSortVector(transformedSort);
        // always ignore the external sort operation (local array order wins)
        return new OTSortSortResult(checkSortVec(lclSortVec), null);
    }

    // position transformations ---------------------------------------------------

    /**
     * Transforms a document position in-place against an "insert" operation.
     *
     * @param xfPos
     *  The document position to be transformed in-place against the "insert"
     *  operation.
     *
     * @param insPos
     *  The start position of the inserted component.
     *
     * @param insSize
     *  The size of the inserted component.
     * @throws JSONException
     *
     * @returns
     *  The transformed document position (a reference to the same array passed in
     *  `xfPos` which has been transformed in-place).
     */
    public static JSONArray transformPositionInsert(JSONArray xfPos, JSONArray insPos, int insSize) throws JSONException {
        final int ai = insPos.length() - 1;
        if ((ai < xfPos.length()) && equalNumberArrays(xfPos, insPos, ai)) {
            xfPos.put(ai, transformIndexInsert(xfPos.getInt(ai), insPos.getInt(ai), insSize));
        }
        return xfPos;
    }

    /**
     * Transforms a document position in-place against a "delete" operation.
     *
     * @param xfPos
     *  The document position to be transformed in-place against the "delete"
     *  operation.
     *
     * @param delPos
     *  The start position of the deleted component.
     *
     * @param delSize
     *  The size of the deleted component.
     * @throws JSONException
     *
     * @returns
     *  The transformed document position (a reference to the same array passed in
     *  `xfPos` which has been transformed in-place); or `undefined`, if the
     *  transformed document position was deleted implicitly by the "delete"
     *  operation.
     */
    public static JSONArray transformPositionDelete(JSONArray xfPos, JSONArray delPos, int delSize) throws JSONException {
        final int ai = delPos.length() - 1;
        if ((ai < xfPos.length()) && equalNumberArrays(xfPos, delPos, ai)) {
            final Integer xfIdx = transformIndexDelete(xfPos.getInt(ai), delPos.getInt(ai), delSize, false);
            if(xfIdx==null) {
                return null;
            }
            xfPos.put(ai, xfIdx);
        }
        return xfPos;
    }

    /**
     * Transforms a document position in-place against a "move" operation in the
     * same parent component.
     *
     * @param xfPos
     *  The document position to be transformed in-place against the "move"
     *  operation.
     *
     * @param fromPos
     *  The start position of the moved component.
     *
     * @param fromSize
     *  The size of the moved component.
     *
     * @param toIdx
     *  The target index where the component will be moved to (inside its parent
     *  component, i.e. by changing the last element of `fromPos`).
     * @throws JSONException
     *
     * @returns
     *  The transformed document position (a reference to the same array passed in
     *  `xfPos` which has been transformed in-place).
     */
    public static JSONArray transformPositionMove(JSONArray xfPos, JSONArray fromPos, int fromSize, int toIdx) throws JSONException {
        final int ai = fromPos.length() - 1;
        if ((ai < xfPos.length()) && equalNumberArrays(xfPos, fromPos, ai)) {
            xfPos.put(ai, transformIndexMove(xfPos.getInt(ai), fromPos.getInt(ai), fromSize, toIdx));
        }
        return xfPos;
    }

    /**
     * Optional parameters for helper functions that reduce properties in arbitrary
     * dictionaries in operation transformations.
     */
    public static class ReducePropertyOptions {

        /**
         * If set to `true`, properties that have a (deeply) equal value in the
         * local and external dictionary will be deleted from both dictionaries.
         *
         * If set to `false`, properties with equal values will be kept in both
         * dictionaries (this implies that the local dictionary will never change,
         * because properties with different values will only be deleted from the
         * external dictionary).
         *
         * Default value is `false`.
         */
        final boolean deleteEqual;

        public ReducePropertyOptions() {
            this.deleteEqual = false;
        }

        public ReducePropertyOptions(boolean deleteEqual) {
            this.deleteEqual = deleteEqual;
        }

        public boolean isDeleteEqual() {
            return deleteEqual;
        }
    }

    /**
     * Optional parameters for helper functions that remove empty attribute sets
     * from document operations.
     */
    public static class RemoveEmptyAttrsOptions {

        /**
         * If set to `true`, an operation without any attributes left (the property
         * `attrs` is missing or is an empty dictionary) will be set to "removed"
         * state (transformation reduces attributes only).
         *
         * If set to `false`, an empty `attrs` property will be deleted from the
         * operation only (reducing attributes is part of a more complex
         * transformation).
         *
         * Default value is `false`.
         */
        final boolean removeEmptyOp;

        public RemoveEmptyAttrsOptions() {
            this.removeEmptyOp = false;
        }

        public RemoveEmptyAttrsOptions(boolean removeEmptyOp) {
            this.removeEmptyOp = removeEmptyOp;
        }

        public boolean isRemoveEmptyOp() {
            return removeEmptyOp;
        }
    }

    /**
     * Optional parameters for helper functions that reduce entire attribute sets
     * (operation property `attrs`) in operation transformations.
     */
    public static class ReduceOperationAttrsOptions {

        final ReducePropertyOptions reducePropertyOptions;
        final RemoveEmptyAttrsOptions removeEmptyAttrsOptions;

        public ReduceOperationAttrsOptions() {
            this(null, null);
        }

        public ReduceOperationAttrsOptions(ReducePropertyOptions reducePropertyOptions) {
            this(reducePropertyOptions, null);
        }

        public ReduceOperationAttrsOptions(RemoveEmptyAttrsOptions removeEmptyAttrsOptions) {
            this(null, removeEmptyAttrsOptions);
        }

        public ReduceOperationAttrsOptions(ReducePropertyOptions reducePropertyOptions, RemoveEmptyAttrsOptions removeEmptyAttrsOptions) {
            this.reducePropertyOptions = reducePropertyOptions != null ? reducePropertyOptions : new ReducePropertyOptions();
            this.removeEmptyAttrsOptions = removeEmptyAttrsOptions != null ? removeEmptyAttrsOptions : new RemoveEmptyAttrsOptions();
        }

        public ReducePropertyOptions getReducePropertyOptions() {
            return reducePropertyOptions;
        }

        public RemoveEmptyAttrsOptions getRemoveEmptyAttrsOptions() {
            return removeEmptyAttrsOptions;
        }
    }

    public static class OTMoveResult {
        /**
         * The array index of the moved or copied element.
         */
        final int fromIdx;

        /**
         * The index in the array where the element will be moved or copied to.
         */
        final int toIdx;

        public OTMoveResult(int fromIdx, int toIdx) {
            this.fromIdx = fromIdx;
            this.toIdx = toIdx;
        }

        public int getFromIdx() {
            return fromIdx;
        }

        public int getToIdx() {
            return toIdx;
        }
    }

    public static class OTShiftMoveResult {

        final int shiftIdx;
        final OTMoveResult moveRes;

        public OTShiftMoveResult(int shiftIdx, OTMoveResult moveRes) {
            this.shiftIdx = shiftIdx;
            this.moveRes = moveRes;
        }

        public int getShiftIdx() {
            return shiftIdx;
        }

        public OTMoveResult getMoveResult() {
            return moveRes;
        }
    }

    public static class OTMoveShiftResult {

        final int shiftIdx;
        final OTMoveResult moveRes;

        public OTMoveShiftResult(int shiftIdx, OTMoveResult moveRes) {
            this.shiftIdx = shiftIdx;
            this.moveRes = moveRes;
        }

        public int getShiftIdx() {
            return shiftIdx;
        }

        public OTMoveResult getMoveResult() {
            return moveRes;
        }
    }

    /**
     * The resulting array indexes of an "insert" or "delete" operation concurring
     * with a "sort" operation in the same array.
     */
    public static class OTShiftSortResult {

        final int shiftIdx;
        final OTSortVector sortVec;

        public OTShiftSortResult(int shiftIdx, OTSortVector sortVec) {
            this.shiftIdx = shiftIdx;
            this.sortVec = sortVec;
        }

        public int getShiftIdx() {
            return shiftIdx;
        }

        public OTSortVector getSortVector() {
            return sortVec;
        }
    }

    /**
     * The resulting array indexes of a "delete" operation concurring with a "copy"
     * operation in the same array.
     */
    public static class OTDeleteCopyResult  extends OTShiftMoveResult {

        /**
         * The array index of the copied element that needs to be deleted because
         * the delete operation has deleted the original copied element.
         */
        final Integer delToIdx;

        public OTDeleteCopyResult(int shiftIdx, OTMoveResult moveRes, Integer delToIdx) {
            super(shiftIdx, moveRes);
            this.delToIdx = delToIdx;
        }

        public Integer optDelToIdx() {
            return delToIdx;
        }
    }

    /**
     * The resulting array indexes of two concurring "move" operations in the same
     * array.
     */
    public static class OTMoveMoveResult {

        /**
         * The array indexes of the transformed local "move" operation; or
         * `undefined`, if the local "move" operation becomes a no-op.
         */
        final OTMoveResult lclRes;

        /**
         * The array indexes of the transformed external "move" operation; or
         * `undefined`, if the local "move" operation becomes a no-op.
         */
        final OTMoveResult extRes;

        public OTMoveMoveResult(OTMoveResult lclRes, OTMoveResult extRes) {
            this.lclRes = lclRes;
            this.extRes = extRes;
        }

        public OTMoveResult getLclRes() {
            return lclRes;
        }

        public OTMoveResult getExtRes() {
            return extRes;
        }
    }

    /**
     * The resulting array indexes of a "move" operation concurring with a "copy"
     * operation in the same array.
     */
    public static class OTMoveCopyResult {

        /**
         * The array indexes of the transformed "move" operation; or `undefined`,
         * if the "move" operation becomes a no-op.
         */
        final OTMoveResult optMoveRes;

        /**
         * The array indexes of the transformed "copy" operation.
         */
        final OTMoveResult copyRes;

        public OTMoveCopyResult(OTMoveResult optMoveRes, OTMoveResult copyRes) {
            this.optMoveRes = optMoveRes;
            this.copyRes = copyRes;
        }

        public OTMoveResult optMoveResult() {
            return optMoveRes;
        }

        public OTMoveResult getCopyResult() {
            return copyRes;
        }
    }

    /**
     * The resulting array indexes of a "move" operation concurring with a "sort"
     * operation in the same array.
     */
    public static class OTMoveSortResult {

        /**
         * The array indexes of the transformed "move" operation; or `undefined`,
         * if the "move" operation becomes a no-op.
         */
        final OTMoveResult optMoveRes;

        /**
         * The transformed sort vector for the "sort" operation; or `undefined`, if
         * the "sort" operation becomes a no-op.
         */
        final OTSortVector optSortVec;

        public OTMoveSortResult(OTMoveResult optMoveRes, OTSortVector optSortVec) {
            this.optMoveRes = optMoveRes;
            this.optSortVec = optSortVec;
        }

        public OTMoveResult optMoveResult() {
            return optMoveRes;
        }

        public OTSortVector optSortVector() {
            return optSortVec;
        }
    }

    /**
     * The resulting array indexes of two concurring "copy" operations in the same
     * array.
     */
    public static class OTCopyCopyResult {

        /**
         * The array indexes of the transformed local "copy" operation.
         */
        final OTMoveResult lclRes;

        /**
         * The array indexes of the transformed external "copy" operation.
         */
        final OTMoveResult extRes;

        public OTCopyCopyResult(OTMoveResult lclRes, OTMoveResult extRes) {
            this.lclRes = lclRes;
            this.extRes = extRes;
        }

        public OTMoveResult getLclResult() {
            return lclRes;
        }

        public OTMoveResult getExtResult() {
            return extRes;
        }
    }

    /**
     * The resulting sort vectors of two concurring "sort" operations in the same
     * array.
     */
    public static class OTSortSortResult {

        /**
         * The transformed sort vector for the local "sort" operation; or
         * `undefined`, if the local "sort" operation becomes a no-op.
         */
        final OTSortVector optLclSortVec;

        /**
         * The transformed sort vector for the external "sort" operation; or
         * `undefined`, if the external "sort" operation becomes a no-op.
         */
        final OTSortVector optExtSortVec;

        public OTSortSortResult(OTSortVector optLclSortVec, OTSortVector optExtSortVec) {
            this.optLclSortVec = optLclSortVec;
            this.optExtSortVec = optExtSortVec;
        }

        public OTSortVector optLclSortVector() {
            return optLclSortVec;
        }

        public OTSortVector optExtSortVector() {
            return optExtSortVec;
        }
    }

    public static class OTSortVector {

        final public int[] vec;

        public OTSortVector() {
            vec = null;
        }

        public OTSortVector(int[] a) {
            vec = a.clone();
        }

        public OTSortVector(JSONArray a) throws JSONException {
            vec = new int[a.length()];
            for(int i=0; i<a.length(); i++) {
                vec[i] = a.getInt(i);
            }
        }

        public OTSortVector(String a) {
            final String[] s = a.split(" ", -1);
            vec = new int[s.length];
            for(int i = 0; i < s.length; i++) {
                vec[i] = Integer.parseInt(s[i]);
            }
        }

        public int[] getVector() {
            return vec;
        }

        public JSONArray createJSONArray() {
            if(vec == null) {
                return null;
            }
            final JSONArray array = new JSONArray(vec.length);
            for(int i=0; i < vec.length; i++) {
                array.put(vec[i]);
            }
            return array;
        }

        public OTSortVector splice(int start, int deleteCount, int insert) {
            final int[] v = new int[1];
            v[0] = insert;
            return splice(start, deleteCount, v);
        }

        public OTSortVector splice(int start, int deleteCount, int[] insert) {
            final int insertSize = insert!=null ? insert.length : 0;
            final int newSize = (vec.length - deleteCount) + insertSize;
            final int[] v = new int[newSize];
            int i=0;

            for(; i < start; i++) {
                v[i] = vec[i];
            }
            if(insert!=null) {
                for(int j=0; j<insert.length; j++) {
                    v[i++] = insert[j];
                }
            }
            for(int j = start + deleteCount; i < newSize; i++, j++) {
                v[i] = vec[j];
            }
            return new OTSortVector(v);
        }
    }

    public static OTMoveResult moveResult(int fromIdx, int toIdx) {
        return (fromIdx == toIdx) ? null : new OTMoveResult(fromIdx, toIdx);
    }

    public static OTSortVector checkSortVec(OTSortVector sortVec) {
        for(int i=0; i<sortVec.vec.length; i++) {
            if(i!=sortVec.vec[i]) {
                return sortVec;
            }
        }
        return null;
    }

    /**
     * Compares the elements of the two passed numeric arrays.
     *
     * @param array1
     *  The first array that will be compared to the second array.
     *
     * @param array2
     *  The second array that will be compared to the first array.
     *
     * @param [maxLength]
     *  If specified, compares the specified number of leading elements in the
     *  passed arrays. Otherwise, the entire arrays will be compared.
     * @throws JSONException
     *
     * @returns
     *  Whether all array elements (or the specified number of array elements) are
     *  equal.
     */
    public static boolean equalNumberArrays(JSONArray array1, JSONArray array2, Integer maxLength) throws JSONException {

        // references to the same array
        if (array1 == array2) {
            return true;
        }

        // number of elements in the arrays to be compared
        final int length1 = maxLength!=null ? Math.min(array1.length(), maxLength.intValue()) : array1.length();
        final int length2 = maxLength!=null ? Math.min(array2.length(), maxLength.intValue()) : array2.length();

        // minimum length of both arrays
        if (length1 != length2) {
            return false;
        }
        final int length = Math.min(length1, length2);

        // compare all array elements
        for (int index = 0; index < length; index += 1) {
            if (array1.getInt(index) != array2.getInt(index)) {
                return false;
            }
        }

        // all compared elements are equal
        return true;
    }

    public static boolean isSameAnchor(JSONObject lclOp, JSONObject extOp) {
        String lclAnchor = lclOp.optString(OCKey.ANCHOR.value());
        String extAnchor = extOp.optString(OCKey.ANCHOR.value());

        if (null != lclAnchor && null != extAnchor) {
            return lclAnchor.equalsIgnoreCase(extAnchor);
        }

        return false;
    }

    /**
     * Transforms the series index in a chart data series operation according to an
     * inserted data series in the same chart.
     * @throws JSONException
     */
    public static void transformChartSeriesForInsert(JSONObject seriesOp, int insIdx) throws JSONException {
        seriesOp.put(OCKey.SERIES.value(), transformIndexInsert(seriesOp.getInt(OCKey.SERIES.value()), insIdx, 1));
    }

    /**
     * Transforms the series index in a chart data series operation according to a
     * deleted data series in the same chart.
     * @throws JSONException
     */
    public static void transformChartSeriesForDelete(JSONObject seriesOp, int delIdx) throws JSONException {
        final Integer xfIdx = OTUtils.transformIndexDelete(seriesOp.getInt(OCKey.SERIES.value()), delIdx, 1, false);
        if (xfIdx==null) {
            setOperationRemoved(seriesOp);
        }
        else {
            seriesOp.put(OCKey.SERIES.value(), xfIdx);
        }
    }

    /**
     * Returns whether both operations are referring to the same component in the
     * same chart object.
     * @throws JSONException
     */
    public static boolean isSameChartComp(JSONObject op1, JSONObject op2) throws JSONException {
        return equalNumberArrays(op1.getJSONArray(OCKey.START.value()), op2.getJSONArray(OCKey.START.value()), null) && (op1.optInt(OCKey.AXIS.value(), -1) == op2.optInt(OCKey.AXIS.value(), -1));
    }

    public static boolean isSameSheet(JSONObject op1, JSONObject op2) throws JSONException {
        // Use -1 to prevent null == 0.
        return op1.optInt(OCKey.SHEET.value(), -1) == op2.optInt(OCKey.SHEET.value(), -1);
    }

    /**
     * Returns whether both name operations refer to the same defined name.
     * @throws JSONException
     */
    public static boolean isSameName(JSONObject op1, JSONObject op2) throws JSONException {
        return isSameSheet(op1, op2) && op1.getString(OCKey.LABEL.value()).equalsIgnoreCase(op2.getString(OCKey.LABEL.value()));
    }

    /**
     * Returns whether both table operations refer to the same table range (ignores
     * sheet index except for auto-filters which can appear on every sheet).
     * @throws JSONException
     */
    public static boolean isSameTable(JSONObject op1, JSONObject op2) throws JSONException {
        final String table1 = op1.optString(OCKey.TABLE.value(), null);
        final String table2 = op2.optString(OCKey.TABLE.value(), null);
        return (table1 != null && table2 != null && table1.equalsIgnoreCase(table2)) || (table1 == null && table2 == null && (op1.getInt(OCKey.SHEET.value()) == op2.getInt(OCKey.SHEET.value())));
    }

    /**
     * Returns whether both operations refer to the same conditional formatting
     * rule.
     * @throws JSONException
     */
    public static boolean isSameCFRule(JSONObject op1, JSONObject op2) throws JSONException {
        return (op1.getInt(OCKey.SHEET.value()) == op2.getInt(OCKey.SHEET.value())) && (op1.getString(OCKey.ID.value()).equalsIgnoreCase(op2.getString(OCKey.ID.value())));
    }

    public static void checkChangeNameNoOp(JSONObject changeOp) throws JSONException {
        // remove properties that depend on "formula"
        if (!changeOp.has(OCKey.FORMULA.value())) {
            changeOp.remove(OCKey.REF.value());
            changeOp.remove(OCKey.IS_EXPR.value());
        }
        if (!changeOp.has(OCKey.NEW_LABEL.value()) && !(changeOp.has(OCKey.FORMULA.value()))) {
            OTUtils.setOperationRemoved(changeOp);
        }
    }

    /**
     * Copies specific existing properties from the source dictionary to the target
     * dictionary. If the property does not exist in the source dictionary, it will
     * be deleted in the target dictionary.
     *
     * @param targetMap
     *  The dictionary to write properties to.
     *
     * @param sourceMap
     *  The dictionary to read properties from.
     *
     * @param keys
     *  The keys of all properties to be copied.
     * @throws JSONException
     */
    public static void copyProperties(JSONObject targetMap, JSONObject sourceMap, String...keys) throws JSONException {
        for(String key:keys) {
            if(sourceMap.has(key)) {
                targetMap.put(key, sourceMap.get(key));
            }
            else {
                targetMap.remove(key);
            }
        }
    }
}
