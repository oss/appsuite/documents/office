/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.tools;

public class OpPair {

    public final Enum<?> a;
    public final Enum<?> b;

    public OpPair(final Enum<?> a, final Enum<?> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof OpPair)) {
            return false;
        }
        if (((OpPair)o).a != a) {
            return false;
        }
        return (((OpPair)o).b == b);
    }

    @Override
    public int hashCode() {
        return ((a.getClass().getSimpleName().hashCode() + a.ordinal()) << 16) | ((b.getClass().getSimpleName().hashCode() + b.ordinal()) & 0xffff);
    }

    @Override
    public String toString() {
        return a.toString() + ":" + b.toString();
    }
}
