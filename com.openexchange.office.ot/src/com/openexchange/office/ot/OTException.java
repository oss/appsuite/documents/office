/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot;

public class OTException extends RuntimeException {

	private static final long serialVersionUID = 4630313113918592195L;

	public static enum Type {
        /**
         * None
         */
        NONE,

        /**
         * A conflicting operation a document reload is required
         */
        RELOAD_REQUIRED,

        /**
         * operations should be transformed for which a ot handler is not implemented yet
         */
        RELOAD_REQUIRED_HANDLER_NOT_IMPLEMENTED_YET,

        /**
         * operations should be transformed for which a ot handler could not be found
         */
        HANDLER_MISSING_ERROR
	}

    final private Type type;

    public OTException(Type type) {
        this.type = type;
    }

    public OTException(Type type, String message) {
        super(message);
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
