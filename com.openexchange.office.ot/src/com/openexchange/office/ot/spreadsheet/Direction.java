/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

import com.openexchange.office.filter.api.OCValue;

public enum Direction {
    LEFT(OCValue.LEFT),
    RIGHT(OCValue.RIGHT),
    UP(OCValue.UP),
    DOWN(OCValue.DOWN);

    private OCValue value;

    Direction(OCValue value) {
        this.value = value;
    }

    /**
     * Returns whether the passed direction value is orientated vertically.
     *
     * @param direction
     *  The direction to be checked.
     *
     * @returns
     *  Whether the direction value is orientated vertically (`true` for the
     *  directions `UP` and `DOWN`, `false` for the directions `LEFT` and `RIGHT`).
     */
    public boolean isVerticalDir() {
        return this == Direction.UP || this == Direction.DOWN;
    }

    /**
     * Returns whether the passed direction value is orientated to the leading
     * border of a bounding box.
     *
     * @param direction
     *  The direction to be checked.
     *
     * @returns
     *  Whether the direction value is orientated to the leading border (`true` for
     *  the directions `LEFT` and `UP`, `false` for the directions `RIGHT` and
     *  `DOWN`).
     */
    public boolean isLeadingDir() {
        return this == Direction.LEFT || this == Direction.UP;
    }

    public OCValue value() {
        return value;
    }

    public static Direction fromValue(String val) {
        final OCValue value = OCValue.fromValue(val);
        if(value == OCValue.LEFT) {
            return Direction.LEFT;
        }
        if(value == OCValue.RIGHT) {
            return Direction.RIGHT;
        }
        if(value == OCValue.UP) {
            return Direction.UP;
        }
        if(value == OCValue.DOWN) {
            return Direction.DOWN;
        }
        return null;
    }
}
