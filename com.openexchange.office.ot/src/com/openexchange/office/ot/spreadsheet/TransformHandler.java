/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefArray;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.CellRefRangeArray;
import com.openexchange.office.filter.core.spreadsheet.IntervalArray;
import com.openexchange.office.ot.OTException;
import com.openexchange.office.ot.TransformHandlerBasic;
import com.openexchange.office.ot.TransformOptions;
import com.openexchange.office.ot.spreadsheet.AddressTransformer.OTAddressTransformer;
import com.openexchange.office.ot.spreadsheet.AddressTransformer.TransformAddressOptions;
import com.openexchange.office.ot.spreadsheet.AddressTransformer.TransformIntervalOptions;
import com.openexchange.office.ot.spreadsheet.AddressTransformer.TransformMode;
import com.openexchange.office.ot.spreadsheet.AddressTransformer.TransformRangeOptions;
import com.openexchange.office.ot.spreadsheet.FormulaUpdateTask.DeleteSheetUpdateTask;
import com.openexchange.office.ot.spreadsheet.FormulaUpdateTask.MoveCellsUpdateTask;
import com.openexchange.office.ot.spreadsheet.FormulaUpdateTask.RelabelNameUpdateTask;
import com.openexchange.office.ot.spreadsheet.FormulaUpdateTask.RenameSheetUpdateTask;
import com.openexchange.office.ot.spreadsheet.FormulaUpdateTask.RenameTableUpdateTask;
import com.openexchange.office.ot.tools.ITransformHandler;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.tools.OTUtils;
import com.openexchange.office.ot.tools.OTUtils.OTCopyCopyResult;
import com.openexchange.office.ot.tools.OTUtils.OTDeleteCopyResult;
import com.openexchange.office.ot.tools.OTUtils.OTMoveCopyResult;
import com.openexchange.office.ot.tools.OTUtils.OTMoveMoveResult;
import com.openexchange.office.ot.tools.OTUtils.OTMoveShiftResult;
import com.openexchange.office.ot.tools.OTUtils.OTMoveSortResult;
import com.openexchange.office.ot.tools.OTUtils.OTShiftMoveResult;
import com.openexchange.office.ot.tools.OTUtils.OTShiftSortResult;
import com.openexchange.office.ot.tools.OTUtils.OTSortSortResult;
import com.openexchange.office.ot.tools.OTUtils.OTSortVector;
import com.openexchange.office.ot.tools.OpPair;
import com.openexchange.office.tools.common.json.JSONHelper;

public class TransformHandler implements ITransformHandlerMap {

    public static OCValue[] opsNumberFormat = {
        OCValue.INSERT_NUMBER_FORMAT,
        OCValue.DELETE_NUMBER_FORMAT,
    };

    public static OCValue[] opsSheets = {
        OCValue.INSERT_SHEET,
        OCValue.DELETE_SHEET,
        OCValue.MOVE_SHEET,
        OCValue.COPY_SHEET,
        OCValue.MOVE_SHEETS
    };

    public static OCValue[] opsSheetCols = {
        OCValue.INSERT_COLUMNS,
        OCValue.DELETE_COLUMNS,
        OCValue.CHANGE_COLUMNS
    };

    public static OCValue[] opsSheetRows = {
        OCValue.INSERT_ROWS,
        OCValue.DELETE_ROWS,
        OCValue.CHANGE_ROWS
    };

    public static OCValue[] opsSheetCells = {
        OCValue.CHANGE_CELLS,
        OCValue.MOVE_CELLS,
        OCValue.MERGE_CELLS
    };

    public static OCValue[] opsSheetLinks = {
        OCValue.INSERT_HYPERLINK,
        OCValue.DELETE_HYPERLINK
    };

    public static OCValue[] opsSheetNames = {
        OCValue.INSERT_NAME,
        OCValue.DELETE_NAME,
        OCValue.CHANGE_NAME
    };

    public static OCValue[] opsSheetTables = {
        OCValue.INSERT_TABLE,
        OCValue.DELETE_TABLE,
        OCValue.CHANGE_TABLE,
        OCValue.CHANGE_TABLE_COLUMN
    };

    public static OCValue[] opsSheetDVRules = {
        OCValue.INSERT_DV_RULE,
        OCValue.DELETE_DV_RULE,
        OCValue.CHANGE_DV_RULE
    };

    public static OCValue[] opsSheetCFRules = {
        OCValue.INSERT_CF_RULE,
        OCValue.DELETE_CF_RULE,
        OCValue.CHANGE_CF_RULE
    };

    public static OCValue[] opsSheetNotes = {
        OCValue.INSERT_NOTE,
        OCValue.DELETE_NOTE,
        OCValue.CHANGE_NOTE,
        OCValue.MOVE_NOTES
    };

    public static OCValue[] opsSheetComments = {
        OCValue.INSERT_COMMENT,
        OCValue.DELETE_COMMENT,
        OCValue.CHANGE_COMMENT,
        OCValue.MOVE_COMMENTS
    };

    public static OCValue[][] opsSheetIndex = {
        { OCValue.CHANGE_SHEET },
        opsSheetRows,
        opsSheetCols,
        opsSheetCells,
        opsSheetLinks,
        opsSheetNames,
        opsSheetTables,
        opsSheetDVRules,
        opsSheetCFRules,
        opsSheetNotes,
        opsSheetComments
    };

    public static OCValue[] opsSheetRanges =  {
        OCValue.INSERT_HYPERLINK,
        OCValue.DELETE_HYPERLINK,
        OCValue.INSERT_DV_RULE,
        OCValue.CHANGE_DV_RULE,
        OCValue.INSERT_CF_RULE,
        OCValue.CHANGE_CF_RULE
    };

    public static OCValue[][] opsDrawing = {
        { OCValue.INSERT_DRAWING },
        { OCValue.DELETE_DRAWING },
        { OCValue.CHANGE_DRAWING },
        { OCValue.MOVE_DRAWING },
        TransformHandlerBasic.opsChartSeries,   // TransformHandlerBasic.opsChart
        { OCValue.DELETE_CHART_AXIS },
        TransformHandlerBasic.opsChartComp
    };

    public static OCValue[][] opsDrawingText = {
        TransformHandlerBasic.opsInsertChar,
        TransformHandlerBasic.opsInsertComp,
        TransformHandlerBasic.opsMergeComp,
        TransformHandlerBasic.opsSplitComp,
        { OCValue.SET_ATTRIBUTES },
        { OCValue.DELETE }
    };

/*
    public static OCValue[][][] OpsDrawingPos = {
        OpsDrawing,
        OpsDrawingText
    };
*/

    public static OCValue[] opsChartSourcelink = {
        OCValue.INSERT_CHART_SERIES,
        OCValue.CHANGE_CHART_SERIES,
        OCValue.CHANGE_CHART_TITLE
    };

    public static OCValue[] opsCellAnchor = {
        OCValue.INSERT_NOTE,
        OCValue.DELETE_NOTE,
        OCValue.CHANGE_NOTE,
        OCValue.INSERT_COMMENT,
        OCValue.DELETE_COMMENT,
        OCValue.CHANGE_COMMENT
    };

    @Override
    public Map<OpPair, ITransformHandler> getTransformHandlerMap() {
        return opPairMap;
    }

    @Override
    public Set<OCValue> getSupportedOperations() {
        return supOpsSet;
    }

    @Override
    public Set<OCValue> getIgnoreOperations() {
        return ignoreOpsSet;
    }

    static final public ImmutableSet<OCValue> ignoreOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.MOVE_CELLS)
        .add(OCValue.MOVE_SHEETS)
        .build();

    static final public ImmutableSet<OCValue> supOpsSet = new ImmutableSet.Builder<OCValue>()
        .add(OCValue.CHANGE_AUTO_STYLE)
        .add(OCValue.CHANGE_CELLS)
        .add(OCValue.CHANGE_CF_RULE)
        .add(OCValue.CHANGE_CHART_SERIES)
        .add(OCValue.CHANGE_CHART_TITLE)
        .add(OCValue.CHANGE_COLUMNS)
        .add(OCValue.CHANGE_COMMENT)
        .add(OCValue.CHANGE_DRAWING)
        .add(OCValue.CHANGE_NAME)
        .add(OCValue.CHANGE_NOTE)
        .add(OCValue.CHANGE_ROWS)
        .add(OCValue.CHANGE_SHEET)
        .add(OCValue.CHANGE_TABLE)
        .add(OCValue.CHANGE_TABLE_COLUMN)
        .add(OCValue.CHANGE_DV_RULE)
        .add(OCValue.COPY_SHEET)
        .add(OCValue.DELETE_AUTO_STYLE)
        .add(OCValue.DELETE_CF_RULE)
        .add(OCValue.DELETE_COLUMNS)
        .add(OCValue.DELETE_COMMENT)
        .add(OCValue.DELETE_DRAWING)
        .add(OCValue.DELETE_HYPERLINK)
        .add(OCValue.DELETE_NAME)
        .add(OCValue.DELETE_NOTE)
        .add(OCValue.DELETE_NUMBER_FORMAT)
        .add(OCValue.DELETE_ROWS)
        .add(OCValue.DELETE_SHEET)
        .add(OCValue.DELETE_TABLE)
        .add(OCValue.DELETE_DV_RULE)
        .add(OCValue.MERGE_CELLS)
        .add(OCValue.MOVE_CELLS)
        .add(OCValue.MOVE_COMMENTS)
        .add(OCValue.MOVE_NOTES)
        .add(OCValue.MOVE_SHEET)
        .add(OCValue.MOVE_SHEETS)
        .add(OCValue.INSERT_AUTO_STYLE)
        .add(OCValue.INSERT_CF_RULE)
        .add(OCValue.INSERT_COLUMNS)
        .add(OCValue.INSERT_COMMENT)
        .add(OCValue.INSERT_CHART_SERIES)
        .add(OCValue.INSERT_HYPERLINK)
        .add(OCValue.INSERT_NAME)
        .add(OCValue.INSERT_NOTE)
        .add(OCValue.INSERT_NUMBER_FORMAT)
        .add(OCValue.INSERT_ROWS)
        .add(OCValue.INSERT_SHEET)
        .add(OCValue.INSERT_TABLE)
        .add(OCValue.INSERT_DV_RULE)
        .build();

    // TODO: MOVE_CELLS ?

    static final ImmutableMap<OCValue, IFormulaTransformFn> formulaTransformMap = new ImmutableMap.Builder<OCValue, IFormulaTransformFn>()
        .put(OCValue.CHANGE_CELLS, (operation, updateTask) -> transformChangeCellsFormulas(operation, updateTask))
        .put(OCValue.INSERT_NAME, (operation, updateTask) -> transformDefinedNameFormulas(operation, updateTask))
        .put(OCValue.CHANGE_NAME, (operation, updateTask) -> transformDefinedNameFormulas(operation, updateTask))
        .put(OCValue.INSERT_DV_RULE, (operation, updateTask) -> transformDVRuleFormulas(operation, updateTask))
        .put(OCValue.CHANGE_DV_RULE, (operation, updateTask) -> transformDVRuleFormulas(operation, updateTask))
        .put(OCValue.INSERT_CF_RULE, (operation, updateTask) -> transformCFRuleFormulas(operation, updateTask))
        .put(OCValue.CHANGE_CF_RULE, (operation, updateTask) -> transformCFRuleFormulas(operation, updateTask))
        .put(OCValue.INSERT_DRAWING, (operation, updateTask) -> transformTextLinkFormula(operation, updateTask))
        .put(OCValue.CHANGE_DRAWING, (operation, updateTask) -> transformTextLinkFormula(operation, updateTask))
        .put(OCValue.INSERT_CHART_SERIES, (operation, updateTask) -> transformChartSeriesFormula(operation, updateTask))
        .put(OCValue.CHANGE_CHART_SERIES, (operation, updateTask) -> transformChartSeriesFormula(operation, updateTask))
        .put(OCValue.CHANGE_CHART_TITLE, (operation, updateTask) -> transformTextLinkFormula(operation, updateTask))
        .build();

    static final public ImmutableMap<OpPair, ITransformHandler> opPairMap;

    static {

        final Map<OpPair, ITransformHandler> map = new HashMap<OpPair, ITransformHandler>();
        final Set<OpPair> aliases = new HashSet<OpPair>();

        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsAutoStyle, opsNumberFormat, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsAutoStyle, opsSheets, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsAutoStyle, opsSheetIndex, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsAutoStyle, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsAutoStyle, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertAutoStyle
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.INSERT_COLUMNS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, true, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.CHANGE_COLUMNS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, true, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.INSERT_ROWS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, true, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.CHANGE_ROWS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, true, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_AUTO_STYLE, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveAutoStyle_changeCells(localOp, externOp, true));

        // deleteAutoStyle
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.INSERT_COLUMNS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, false, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.CHANGE_COLUMNS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, false, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.INSERT_ROWS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, false, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.CHANGE_ROWS, (options, localOp, externOp) -> transform_moveAutoStyle_changeIntervals(localOp, externOp, false, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_AUTO_STYLE, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveAutoStyle_changeCells(localOp, externOp, false));

        // skip chart operations with text operations (but check for shapes embedded in charts)
        OTUtils.registerBidiTransformation(map, aliases, TransformHandlerBasic.opsChart, opsDrawingText, (options, localOp, externOp) -> transform_anyChart_drawingText(localOp, externOp));

        // skip number format operations with other unrelated operations
        OTUtils.registerBidiTransformation(map, aliases, opsNumberFormat, opsSheets, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsNumberFormat, opsSheetIndex, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsNumberFormat, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsNumberFormat, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertNumberFormat
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_NUMBER_FORMAT, (options, localOp, externOp) -> transform_insertNumFmt_insertNumFmt(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NUMBER_FORMAT, OCValue.DELETE_NUMBER_FORMAT, (options, localOp, externOp) -> transform_insertNumFmt_deleteNumFmt(localOp, externOp));

        // deleteNumberFormat
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_NUMBER_FORMAT, (options, localOp, externOp) -> transform_deleteNumFmt_deleteNumFmt(localOp, externOp));

        // insertSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_SHEET, (options, localOp, externOp) -> transform_insertSheet_insertSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, OCValue.DELETE_SHEET, (options, localOp, externOp) -> transform_insertSheet_deleteSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, OCValue.MOVE_SHEET, (options, localOp, externOp) -> transform_insertSheet_moveSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, OCValue.COPY_SHEET, (options, localOp, externOp) -> transform_insertSheet_copySheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, OCValue.MOVE_SHEETS, (options, localOp, externOp) -> transform_insertSheet_moveSheets(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, OCValue.CHANGE_SHEET, (options, localOp, externOp) -> transform_insertSheet_changeSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, opsSheetIndex, (options, localOp, externOp) -> transform_insertSheet_sheetIndex(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, opsDrawing, (options, localOp, externOp) -> transform_insertSheet_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_SHEET, opsDrawingText, (options, localOp, externOp) -> transform_insertSheet_drawingPos(localOp, externOp));

        // deleteSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_SHEET, (options, localOp, externOp) -> transform_deleteSheet_deleteSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, OCValue.MOVE_SHEET,  (options, localOp, externOp) -> transform_deleteSheet_moveSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, OCValue.COPY_SHEET, (options, localOp, externOp) -> transform_deleteSheet_copySheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, OCValue.MOVE_SHEETS, (options, localOp, externOp) -> transform_deleteSheet_moveSheets(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, opsSheetIndex, (options, localOp, externOp) -> transform_deleteSheet_sheetIndex(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, opsDrawing, (options, localOp, externOp) -> transform_deleteSheet_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_SHEET, opsDrawingText, (options, localOp, externOp) -> transform_deleteSheet_drawingPos(localOp, externOp));

        // moveSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MOVE_SHEET, (options, localOp, externOp) -> transform_moveSheet_moveSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEET, OCValue.COPY_SHEET, (options, localOp, externOp) -> transform_moveSheet_copySheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEET, OCValue.MOVE_SHEETS, (options, localOp, externOp) -> transform_moveSheet_moveSheets(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEET, opsSheetIndex, (options, localOp, externOp) -> transform_moveSheet_sheetIndex(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEET, opsDrawing, (options, localOp, externOp) -> transform_moveSheet_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEET, opsDrawingText, (options, localOp, externOp) -> transform_moveSheet_drawingPos(localOp, externOp));

        // copySheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.COPY_SHEET, (options, localOp, externOp) -> transform_copySheet_copySheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.COPY_SHEET, OCValue.MOVE_SHEETS, (options, localOp, externOp) -> transform_copySheet_moveSheets(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.COPY_SHEET, OCValue.CHANGE_SHEET, (options, localOp, externOp) -> transform_copySheet_changeSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.COPY_SHEET, opsSheetIndex, (options, localOp, externOp) -> transform_copySheet_sheetIndex(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.COPY_SHEET, opsDrawing, (options, localOp, externOp) -> transform_copySheet_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.COPY_SHEET, opsDrawingText, (options, localOp, externOp) -> transform_copySheet_drawingPos(localOp, externOp));

        // moveSheets
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MOVE_SHEETS, (options, localOp, externOp) -> transform_moveSheets_moveSheets(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEETS, opsSheetIndex, (options, localOp, externOp) -> transform_moveSheets_sheetIndex(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEETS, opsDrawing, (options, localOp, externOp) -> transform_moveSheets_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_SHEETS, opsDrawingText, (options, localOp, externOp) -> transform_moveSheets_drawingPos(localOp, externOp));

        // changeSheet
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_SHEET, (options, localOp, externOp) -> transform_changeSheet_changeSheet(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, opsSheetIndex,(options, localOp, externOp) -> transform_changeSheet_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, OCValue.INSERT_NOTE, (options, localOp, externOp) -> transform_changeSheet_drawingAnchor(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_changeSheet_drawingAnchor(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, opsDrawing, (options, localOp, externOp) -> transform_changeSheet_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, opsDrawingText, (options, localOp, externOp) -> transform_changeSheet_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_changeSheet_drawingAnchor(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_SHEET, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_changeSheet_drawingAnchor(localOp, externOp));

        // column collection
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, opsSheetRows, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, OCValue.DELETE_NAME, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, OCValue.DELETE_TABLE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, OCValue.DELETE_DV_RULE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, OCValue.DELETE_CF_RULE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCols, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertColumns
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_COLUMNS, (options, localOp, externOp) -> transform_insertIntervals_insertIntervals(options, localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> transform_insertIntervals_deleteIntervals(options, localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_COLUMNS, (options, localOp, externOp) -> transform_moveIntervals_changeIntervals(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_changeCells(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_mergeCells(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_moveIntervals_changeTableCol(localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, opsSheetRanges, (options, localOp, externOp) -> transform_moveIntervals_rangeList(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, opsCellAnchor, (options, localOp, externOp) -> transform_moveIntervals_cellAnchor(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_moveIntervals_moveNotes(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_moveIntervals_moveComments(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.RIGHT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COLUMNS, opsChartSourcelink, (options, localOp, externOp) -> transform_moveIntervals_sourceLinks(options, localOp, externOp, Direction.RIGHT));

        // deleteColumns
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_COLUMNS, (options, localOp, externOp) -> transform_deleteIntervals_deleteIntervals(options, localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_COLUMNS, (options, localOp, externOp) -> transform_moveIntervals_changeIntervals(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_changeCells(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_mergeCells(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_moveIntervals_changeTableCol(localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, opsSheetRanges, (options, localOp, externOp) -> transform_moveIntervals_rangeList(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, opsCellAnchor, (options, localOp, externOp) -> transform_moveIntervals_cellAnchor(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_moveIntervals_moveNotes(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_moveIntervals_moveComments(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.LEFT));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COLUMNS, opsChartSourcelink, (options, localOp, externOp) -> transform_moveIntervals_sourceLinks(options, localOp, externOp, Direction.LEFT));

        // changeColumns
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_COLUMNS, (options, localOp, externOp) -> transform_changeIntervals_changeIntervals(localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetCells, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetLinks, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetNames, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetTables, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, OCValue.INSERT_NOTE, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, true));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COLUMNS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, true));

        // row collection
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, OCValue.DELETE_NAME, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, OCValue.DELETE_TABLE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, OCValue.DELETE_DV_RULE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, OCValue.DELETE_CF_RULE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetRows, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertRows
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_ROWS, (options, localOp, externOp) -> transform_insertIntervals_insertIntervals(options, localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.DELETE_ROWS, (options, localOp, externOp) -> transform_insertIntervals_deleteIntervals(options, localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_ROWS, (options, localOp, externOp) -> transform_moveIntervals_changeIntervals(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_changeCells(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_mergeCells(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_moveIntervals_changeTableCol(localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, opsSheetRanges, (options, localOp, externOp) -> transform_moveIntervals_rangeList(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, opsCellAnchor, (options, localOp, externOp) -> transform_moveIntervals_cellAnchor(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_moveIntervals_moveNotes(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_moveIntervals_moveComments(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.DOWN));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_ROWS, opsChartSourcelink, (options, localOp, externOp) -> transform_moveIntervals_sourceLinks(options, localOp, externOp, Direction.DOWN));

        // deleteRows
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_ROWS, (options, localOp, externOp) -> transform_deleteIntervals_deleteIntervals(options, localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_ROWS, (options, localOp, externOp) -> transform_moveIntervals_changeIntervals(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_changeCells(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_moveIntervals_mergeCells(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_moveIntervals_definedName(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_moveIntervals_tableRange(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_moveIntervals_changeTableCol(localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, opsSheetRanges, (options, localOp, externOp) -> transform_moveIntervals_rangeList(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, opsCellAnchor, (options, localOp, externOp) -> transform_moveIntervals_cellAnchor(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_moveIntervals_moveNotes(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_moveIntervals_moveComments(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_moveIntervals_drawingAnchor(options, localOp, externOp, Direction.UP));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_ROWS, opsChartSourcelink, (options, localOp, externOp) -> transform_moveIntervals_sourceLinks(options, localOp, externOp, Direction.UP));

        // changeRows
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_ROWS, (options, localOp, externOp) -> transform_changeIntervals_changeIntervals(localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetCells, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetLinks, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetNames, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetTables, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, OCValue.INSERT_NOTE, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, false));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_ROWS, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_changeIntervals_drawingAnchor(localOp, externOp, false));

        // cells
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetLinks, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetNames, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, OCValue.DELETE_TABLE, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsDrawingText, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCells, opsSheetComments, (options, localOp, externOp) -> handleNothing());

        // changeCells
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_CELLS, (options, localOp, externOp) -> transform_changeCells_changeCells(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_CELLS, OCValue.MOVE_CELLS, (options, localOp, externOp) -> transform_changeCells_moveCells(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_CELLS, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_changeCells_mergeCells(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_CELLS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_changeCells_changeName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_CELLS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_changeCells_tableRange(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_CELLS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_changeCells_tableRange(localOp, externOp));

        // moveCells
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.INSERT_DV_RULE, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.INSERT_CF_RULE, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, opsDrawing, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_CELLS, opsDrawingText, (options, localOp, externOp) -> transform_moveCells_formulaExpressions(options, localOp, externOp));

        // mergeCells
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MERGE_CELLS, (options, localOp, externOp) -> transform_mergeCells_mergeCells(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MERGE_CELLS, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_mergeCells_tableRange(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MERGE_CELLS, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_mergeCells_tableRange(localOp, externOp));

        // hyperlinks
        OTUtils.registerSelfTransformation(map, aliases, opsSheetLinks, (options, localOp, externOp) -> transform_changeHyperlink_changeHyperlink(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetNames, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetTables, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetLinks, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // defined names
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsSheetTables, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNames, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertName
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_NAME, (options, localOp, externOp) -> transform_insertName_insertName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NAME, OCValue.DELETE_NAME, (options, localOp, externOp) -> transform_insertName_deleteName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NAME, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_insertName_changeName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NAME, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_insertName_insertTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NAME, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_insertName_changeTable(localOp, externOp));

        // deleteName
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_NAME, (options, localOp, externOp) -> transform_deleteName_deleteName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_NAME, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_deleteName_changeName(localOp, externOp));

        // changeName
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_NAME, (options, localOp, externOp) -> transform_changeName_changeName(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_changeName_insertTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_changeName_changeTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.INSERT_DV_RULE, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.INSERT_CF_RULE, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, opsDrawing, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NAME, opsDrawingText, (options, localOp, externOp) -> transform_changeName_formulaExpressions(localOp, externOp));

        // table ranges
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsSheetDVRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetTables, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertTable
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_TABLE, (options, localOp, externOp) -> transform_insertTable_insertTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_TABLE, OCValue.DELETE_TABLE, (options, localOp, externOp) -> transform_insertTable_deleteTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_TABLE, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_insertTable_changeTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_TABLE, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_insertTable_changeTableCol(localOp, externOp));

        // deleteTable
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_TABLE, (options, localOp, externOp) -> transform_deleteTable_deleteTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_TABLE, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_deleteTable_changeTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_TABLE, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_deleteTable_changeTableCol(localOp, externOp));

        // changeTable
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_TABLE, (options, localOp, externOp) -> transform_changeTable_changeTable(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_changeTable_changeTableCol(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, OCValue.INSERT_DV_RULE, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, OCValue.INSERT_CF_RULE, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));;
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, opsDrawing, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_TABLE, opsDrawingText, (options, localOp, externOp) -> transform_changeTable_formulaExpressions(localOp, externOp));

        // changeTableColumn
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_TABLE_COLUMN, (options, localOp, externOp) -> transform_changeTableCol_changeTableCol(localOp, externOp));

        // data validation
        OTUtils.registerBidiTransformation(map, aliases, opsSheetDVRules, opsSheetCFRules, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetDVRules, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetDVRules, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetDVRules, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetDVRules, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertDVRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_DV_RULE, (options, localOp, externOp) -> transform_insertDVRule_insertDVRule(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DV_RULE, OCValue.DELETE_DV_RULE, (options, localOp, externOp) -> transform_insertDVRule_deleteDVRule(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DV_RULE, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_insertDVRule_changeDVRule(options, localOp, externOp));

        // deleteDVRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_DV_RULE, (options, localOp, externOp) -> transform_deleteDVRule_deleteDVRule(options, localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_DV_RULE, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_deleteDVRule_changeDVRule(options, localOp, externOp));

        // changeDVRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_DV_RULE, (options, localOp, externOp) -> transform_changeDVRule_changeDVRule(options, localOp, externOp));

        // conditional formatting
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCFRules, opsSheetNotes, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCFRules, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCFRules, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetCFRules, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertCFRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_CF_RULE, (options, localOp, externOp) -> transform_insertCFRule_insertCFRule(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CF_RULE, OCValue.DELETE_CF_RULE, (options, localOp, externOp) -> transform_insertCFRule_deleteCFRule(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_CF_RULE, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_insertCFRule_changeCFRule(localOp, externOp));

        // deleteCFRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_CF_RULE, (options, localOp, externOp) -> transform_deleteCFRule_deleteCFRule(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_CF_RULE, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_deleteCFRule_changeCFRule(localOp, externOp));

        // changeCFRule
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_CF_RULE, (options, localOp, externOp) -> transform_changeCFRule_changeCFRule(localOp, externOp));

        // cell notes
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNotes, opsSheetComments, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNotes, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetNotes, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertNote
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_NOTE, (options, localOp, externOp) -> transform_insertNote_insertNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NOTE, OCValue.DELETE_NOTE, (options, localOp, externOp) -> transform_insertNote_deleteNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NOTE, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_insertNote_changeNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NOTE, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_insertNote_moveNotes(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NOTE, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> transform_insertNote_insertComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_NOTE, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_insertNote_moveComments(localOp, externOp));

        // deleteNote
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_NOTE, (options, localOp, externOp) -> transform_deleteNote_deleteNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_NOTE, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_deleteNote_changeNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_NOTE, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_deleteNote_moveNotes(localOp, externOp));

        // changeNote
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_NOTE, (options, localOp, externOp) -> transform_changeNote_changeNote(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_NOTE, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_changeNote_moveNotes(localOp, externOp));

        // moveNotes
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MOVE_NOTES, (options, localOp, externOp) -> transform_moveNotes_moveNotes(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_NOTES, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> transform_moveNotes_insertComment(localOp, externOp));

        // comment threads
        OTUtils.registerBidiTransformation(map, aliases, opsSheetComments, opsDrawing, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, opsSheetComments, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // insertComment
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_COMMENT, (options, localOp, externOp) -> transform_insertComment_insertComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COMMENT, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> transform_insertComment_deleteComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COMMENT, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> transform_insertComment_changeComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_COMMENT, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_changeNote_moveNotes(localOp, externOp));

        // deleteComment
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_COMMENT, (options, localOp, externOp) -> transform_deleteComment_deleteComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COMMENT, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> transform_deleteComment_changeComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_COMMENT, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_deleteComment_moveComments(localOp, externOp));

        // changeComment
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_COMMENT, (options, localOp, externOp) -> transform_changeComment_changeComment(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_COMMENT, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_changeNote_moveNotes(localOp, externOp));

        // moveComments
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MOVE_COMMENTS, (options, localOp, externOp) -> transform_moveNotes_moveNotes(localOp, externOp));

        // insertDrawing
        OTUtils.registerSelfTransformation(map, aliases, OCValue.INSERT_DRAWING, (options, localOp, externOp) -> transform_insertDrawing_insertDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.DELETE_DRAWING, (options, localOp, externOp) -> transform_insertDrawing_deleteDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, OCValue.MOVE_DRAWING, (options, localOp, externOp) -> transform_insertDrawing_moveDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, opsDrawing, (options, localOp, externOp) -> transform_insertDrawing_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.INSERT_DRAWING, opsDrawingText, (options, localOp, externOp) -> transform_insertDrawing_drawingPos(localOp, externOp));

        // deleteDrawing
        OTUtils.registerSelfTransformation(map, aliases, OCValue.DELETE_DRAWING, (options, localOp, externOp) -> transform_deleteDrawing_deleteDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_DRAWING, OCValue.MOVE_DRAWING, (options, localOp, externOp) -> transform_deleteDrawing_moveDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_DRAWING, opsDrawing, (options, localOp, externOp) -> transform_deleteDrawing_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.DELETE_DRAWING, opsDrawingText, (options, localOp, externOp) -> transform_deleteDrawing_drawingPos(localOp, externOp));

        // changeDrawing
        OTUtils.registerSelfTransformation(map, aliases, OCValue.CHANGE_DRAWING, (options, localOp, externOp) -> transform_changeDrawing_changeDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_DRAWING, TransformHandlerBasic.opsChart, (options, localOp, externOp) -> handleNothing());
        OTUtils.registerBidiTransformation(map, aliases, OCValue.CHANGE_DRAWING, opsDrawingText, (options, localOp, externOp) -> handleNothing());

        // moveDrawing
        OTUtils.registerSelfTransformation(map, aliases, OCValue.MOVE_DRAWING, (options, localOp, externOp) -> transform_moveDrawing_moveDrawing(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_DRAWING, opsDrawing, (options, localOp, externOp) -> transform_moveDrawing_drawingPos(localOp, externOp));
        OTUtils.registerBidiTransformation(map, aliases, OCValue.MOVE_DRAWING, opsDrawingText, (options, localOp, externOp) -> transform_moveDrawing_drawingPos(localOp, externOp));

        opPairMap = new ImmutableMap.Builder<OpPair, ITransformHandler>()
        .putAll(map)
        .build();
    }

    private static JSONObject handleNothing() {
        return null;
    }

    // private methods --------------------------------------------------------

    /**
     * Creates an address transformer for the passed index intervals operation.
     * @throws JSONException
     */
    private static OTAddressTransformer createIntervalTransformer(AddressFactory addressFactory, JSONObject intervalOp, Direction dir) throws JSONException {
        return OTAddressTransformer.fromIntervals(addressFactory, IntervalArray.createIntervals(!dir.isVerticalDir(), intervalOp.getString(OCKey.INTERVALS.value())), dir, intervalOp.getInt(OCKey.SHEET.value()));
    }

    /**
     * Creates an address transformer for the passed "moveCells" "operation.
     * @throws JSONException
     */
    private static OTAddressTransformer createRangeTransformer(AddressFactory addressFactory, JSONObject moveOp) throws JSONException {
        final CellRefRange range = CellRefRange.createCellRefRange(moveOp.getString(OCKey.RANGE.value()));
        return OTAddressTransformer.fromRange(addressFactory, range, Direction.fromValue(moveOp.getString(OCKey.DIR.value())), moveOp.getInt(OCKey.SHEET.value()));
    }

    /**
     * Invokes a callback function for the value of an existing attribute in the
     * passed attributed document operation, and inserts the return value of the
     * callback function into the attribute set.
     *
     * @param attrOp
     *  The JSON document operation with an (optional) attribute set.
     *
     * @param family
     *  The style family of the attribute to be transformed.
     *
     * @param key
     *  The key of the attribute to be transformed.
     *
     * @param callback
     *  The callback function that will be invoked, if the specified attribute
     *  exists in the attribute set of the operation. Receives the attribute value,
     *  and the attribute key. Returns the new value of the attribute. Can return
     *  `undefined` to keep the attribute unchanged.
     * @throws JSONException
     */
    private static void transformAttribute(JSONObject attrsOp, String family, String key, TransformKeyValueFn transformFn) throws JSONException {
        final JSONObject attrs = attrsOp.optJSONObject(family);
        if (attrs != null && attrs.has(key)) {
            final Object value = transformFn.transform(key, attrs.opt(key));
            if (value!=null) {
                attrs.put(key, value);
            }
        }
    }

    /**
     * Transforms the drawing anchor property in the passed operation with
     * drawing attributes (drawing objects, notes, comments) according to the
     * settings in the address transformer.
     * @throws JSONException
     */
    private static boolean transformDrawingAnchor(JSONObject drawingOp, AddressTransformer transformer, TransformAddressOptions options) throws JSONException {
        boolean deleted = false;
        transformAttribute(drawingOp, "drawing", "anchor", (key, value) -> {
            if(!(value instanceof String)) {
                return null;
            }
/* TODO: parse drawing cell anchor
            cellAnchor = this.addressFactory.parseCellAnchor(anchorStr);
            const newCellAnchor = transformer.transformCellAnchor(cellAnchor, options);
            this.warnIf(newCellAnchor, "NOT IMPLEMENTED: cannot transform absolute drawing anchor (moved cells)");
            deleted = !newCellAnchor;
            return newCellAnchor?.toOpStr();
*/
            return null;
        });
        return deleted;
    }

    private interface TransformKeyValueFn {

        Object transform(String key, Object value);
    }

    // formula expressions ----------------------------------------------------

    private static String transformFormula(Object formula, @SuppressWarnings("unused") FormulaUpdateTask _updateTask, String failMsg) {
        if((formula instanceof String) && !((String)formula).isEmpty()) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "NOT IMPLEMENTED: formula transformation for " + failMsg);
        }
        return null;
    }

    private static void transformFmlaAttributes(JSONObject attrOp, FormulaUpdateTask updateTask, String failMsg, String family, String...keys) throws JSONException {
        for(String key:keys) {
            final JSONObject attrs = attrOp.optJSONObject(OCKey.ATTRS.value());
            if(attrs!=null) {
                final JSONObject familyAttrs = attrs.optJSONObject(family);
                if(familyAttrs!=null && familyAttrs.has(key)) {
                    final String formula = transformFormula(familyAttrs.get(key), updateTask, failMsg + " (family=" + family + " key=" + key + ")");
                    if(formula!=null) {
                        familyAttrs.put(key, formula);
                    }
                }
            }
        }
    }

    private static void transformFmlaProperties(Object data, FormulaUpdateTask updateTask, String failMsg, String...keys) throws JSONException {
        if(data instanceof JSONObject) {
            for(String key:keys) {
                final Object value = ((JSONObject)data).opt(key);
                if(value!=null) {
                    final String formula = transformFormula(value, updateTask, failMsg + " (key=" + key + ")");
                    if(formula!=null) {
                        ((JSONObject)data).put(key, formula);
                    }
                }
            }
        }
    }

    private static void transformChangeCellsFormulas(JSONObject changeOp, FormulaUpdateTask updateTask) throws JSONException {

        final JSONObject contents = changeOp.optJSONObject(OCKey.CONTENTS.value());
        if (null == contents) {
            return;
        }

        final Iterator<Entry<String, Object>> iter = contents.entrySet().iterator();
        while(iter.hasNext()) {
            final Entry<String, Object> entry = iter.next();
            transformFmlaProperties(entry.getValue(), updateTask, "cell formula", "f");
        }
    }

    private static void transformDefinedNameFormulas(JSONObject nameOp, FormulaUpdateTask updateTask) throws JSONException {
        transformFmlaProperties(nameOp, updateTask, "defined name", "formula", "ref");
    }

    private static void transformDVRuleFormulas(JSONObject ruleOp, FormulaUpdateTask updateTask) throws JSONException {
        transformFmlaProperties(ruleOp, updateTask, "data validation", "value1", "value2", "ref");
    }

    private static void transformCFRuleFormulas(JSONObject ruleOp, FormulaUpdateTask updateTask) throws JSONException {
        transformFmlaProperties(ruleOp, updateTask, "conditional formatting", "value1", "value2", "ref");
        final JSONArray colorScale = ruleOp.optJSONArray(OCKey.COLOR_SCALE.value());
        if(colorScale!=null) {
            for(Object colorStep:colorScale) {
                transformFmlaProperties(colorStep, updateTask, "conditional formatting (color step)", "v");
            }
        }
        final JSONObject dataBar = ruleOp.optJSONObject(OCKey.DATA_BAR.value());
        if(dataBar!=null) {
            transformFmlaProperties(dataBar.optJSONObject(OCKey.R1.value()), updateTask, "conditional formatting (data bar)", "v");
            transformFmlaProperties(dataBar.optJSONObject(OCKey.R2.value()), updateTask, "conditional formatting (data bar)", "v");
        }
    }

    private static void transformTextLinkFormula(JSONObject drawingOp, FormulaUpdateTask updateTask) throws JSONException {
        transformFmlaAttributes(drawingOp, updateTask, "drawing source link", "text", "link");
    }

    private static void transformChartSeriesFormula(JSONObject chartOp, FormulaUpdateTask updateTask) throws JSONException {
        transformFmlaAttributes(chartOp, updateTask, "chart data source link", "series", "title", "values", "names", "bubbles");
    }

    private static void transformFormulaExpressions(JSONObject fmlaOp, FormulaUpdateTask updateTask) throws JSONException {
        final OCValue valueKey = OCValue.fromValue(fmlaOp.getString(OCKey.NAME.value()));
        if(formulaTransformMap.containsKey(valueKey)) {
            formulaTransformMap.get(valueKey).call(fmlaOp, updateTask);
        }
    }

    // auto-styles ------------------------------------------------------------

    private static JSONObject transform_moveAutoStyle_changeIntervals(JSONObject styleOp, JSONObject changeOp, boolean insert, boolean checkNoOp) throws JSONException {
        if (TransformHandlerBasic.transformAutoStyleProperty(changeOp, "s", "cell", styleOp, insert) && !changeOp.has(OCKey.S.value()) && checkNoOp) {
            OTUtils.checkChangeIntervalsNoOp(changeOp);
        }
        return null;
    }

    private static JSONObject transform_moveAutoStyle_changeCells(JSONObject styleOp, JSONObject changeOp, boolean insert) throws JSONException {
        final JSONObject contents = changeOp.getJSONObject(OCKey.CONTENTS.value());
        final Iterator<Entry<String, Object>> iter = contents.entrySet().iterator();
        while(iter.hasNext()) {
            final Entry<String, Object> entry = iter.next();
            final Object o = entry.getValue();
            if(o instanceof JSONObject) {
                if (TransformHandlerBasic.transformAutoStyleProperty((JSONObject)o, "s", "cell", styleOp, insert) && ((JSONObject)o).isEmpty()) {
                    iter.remove();
                }
            }
            OTUtils.checkChangeCellsNoOp(changeOp);
        }
        return null;
    }

    // charts -----------------------------------------------------------------

    private static JSONObject transform_anyChart_drawingText(JSONObject chartOp, JSONObject textOp) throws JSONException {
        final JSONArray chartPos = OTUtils.getStartPosition(chartOp);
        final JSONArray textPos = OTUtils.getStartPosition(textOp);
        if(textPos.length()>=chartPos.length()) {
            if(JSONHelper.isEqual(chartPos, textPos, chartPos.length())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_anyChart_drawingText" + chartOp.toString() + "," + textOp.toString());
            }
        }
        return null;
    }

    // insertNumberFormat -----------------------------------------------------

    private static JSONObject transform_insertNumFmt_insertNumFmt(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // both operations have inserted the same number format: do not repeat that on either side
        if ((lclOp.getInt(OCKey.ID.value()) == extOp.getInt(OCKey.ID.value())) && (lclOp.optString(OCKey.CODE.value(), "").equals(extOp.optString(OCKey.CODE.value(), "")))) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
            return null;
        }
        // inserting number formats is very rare, shifting identifiers through style sheet operations not implemented
        throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertNumFmt_insertNumFmt" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot transform different new number formats");
    }

    private static JSONObject transform_insertNumFmt_deleteNumFmt(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // remove delete operation if a number format will be inserted with the same identifier
        if (insertOp.getInt(OCKey.ID.value()) == deleteOp.getInt(OCKey.ID.value())) {
            OTUtils.setOperationRemoved(deleteOp);
        }
        return null;
    }

    // deleteNumberFormat -----------------------------------------------------

    private static JSONObject transform_deleteNumFmt_deleteNumFmt(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // both operations have deleted the same number format: do not repeat that on either side
        if (lclOp.getInt(OCKey.ID.value()) == extOp.getInt(OCKey.ID.value())) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }
        return null;
    }

    // insertSheet ------------------------------------------------------------

    private static JSONObject transform_insertSheet_insertSheet(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if(OTUtils.hasSameSheetName(lclOp, extOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertSheet_insertSheet" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
        // first, shift local sheet index away (external sheet position wins)
        lclOp.put(OCKey.SHEET.value(), OTUtils.transformIndexInsert(lclOp.getInt(OCKey.SHEET.value()), extOp.getInt(OCKey.SHEET.value()), 1));
        // transform external operation with the new local insertion index
        extOp.put(OCKey.SHEET.value(), OTUtils.transformIndexInsert(extOp.getInt(OCKey.SHEET.value()), lclOp.getInt(OCKey.SHEET.value()), 1));
        return null;
    }

    private static JSONObject transform_insertSheet_deleteSheet(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // first, shift away delete position according to insert position (new sheet cannot collide with existing sheet)
        deleteOp.put(OCKey.SHEET.value(), OTUtils.transformIndexInsert(deleteOp.getInt(OCKey.SHEET.value()), insertOp.getInt(OCKey.SHEET.value()), 1));
        // transform insert operation with the new deletion index (will not collide, see above)
        insertOp.put(OCKey.SHEET.value(), OTUtils.transformIndexDelete(insertOp.getInt(OCKey.SHEET.value()), deleteOp.getInt(OCKey.SHEET.value()), 1, true));
        return null;
    }

    private static JSONObject transform_insertSheet_moveSheet(JSONObject insertOp, JSONObject moveOp) throws JSONException {
        final OTShiftMoveResult result = OTUtils.transformIndexInsertMove(insertOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.TO.value()));
        insertOp.put(OCKey.SHEET.value(), result.getShiftIdx());
        assignMoveSheet(moveOp, result.getMoveResult());
        return null;
    }

    private static JSONObject transform_insertSheet_copySheet(JSONObject insertOp, JSONObject copyOp) throws JSONException {
        if(OTUtils.hasSameSheetName(insertOp, copyOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertSheet_insertSheet" + insertOp.toString() + "," + copyOp.toString() + "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
        final OTMoveShiftResult res = OTUtils.transformIndexInsertCopy(insertOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.TO.value()));
        insertOp.put(OCKey.SHEET.value(), res.getShiftIdx());
        assignMoveSheet(copyOp, res.getMoveResult());
        return null;
    }

    private static JSONObject transform_insertSheet_moveSheets(JSONObject insertOp, JSONObject sortOp) throws JSONException {
        final OTShiftSortResult result = OTUtils.transformIndexInsertSort(insertOp.getInt(OCKey.SHEET.value()), new OTUtils.OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value())));
        insertOp.put(OCKey.SHEET.value(), result.getShiftIdx());
        assignMoveSheets(sortOp, result.getSortVector());
        return null;
    }

    private static JSONObject transform_insertSheet_changeSheet(JSONObject insertOp, JSONObject changeOp) throws JSONException {
        if(OTUtils.hasSameSheetName(insertOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertSheet_changeSheet" + insertOp.toString() + "," + changeOp.toString() + "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
        return transform_insertSheet_sheetIndex(insertOp, changeOp);
    }

    private static JSONObject transform_insertSheet_sheetIndex(JSONObject insertOp, JSONObject sheetOp) throws JSONException {
        final Object s = sheetOp.opt(OCKey.SHEET.value());
        if (s instanceof Number) {
            sheetOp.put(OCKey.SHEET.value(), OTUtils.transformIndexInsert(((Integer)s).intValue(), insertOp.getInt(OCKey.SHEET.value()), 1));
        }
        return null;
    }

    private static JSONObject transform_insertSheet_drawingPos(JSONObject insertOp, JSONObject drawingOp) throws JSONException {
        transformSheetInDrawingPositions(drawingOp, xfSheet -> OTUtils.transformIndexInsert(xfSheet, insertOp.getInt(OCKey.SHEET.value()), 1));
        return null;
    }

    // deleteSheet ------------------------------------------------------------

    private static JSONObject transform_deleteSheet_deleteSheet(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // set both operations to "removed" state, if they delete the same sheet
        final int lclSheet = lclOp.getInt(OCKey.SHEET.value());
        final int extSheet = extOp.getInt(OCKey.SHEET.value());
        if (lclSheet == extSheet) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }
        else {
            // transform both sheet indexes with the original (untransformed) indexes
            extOp.put(OCKey.SHEET.value(), OTUtils.transformIndexDelete(extOp.getInt(OCKey.SHEET.value()), lclSheet, 1, true));
            lclOp.put(OCKey.SHEET.value(), OTUtils.transformIndexDelete(lclOp.getInt(OCKey.SHEET.value()), extSheet, 1, true));
        }
        return null;
    }

    private static JSONObject transform_deleteSheet_moveSheet(JSONObject deleteOp, JSONObject moveOp) throws JSONException {
        final OTShiftMoveResult result = OTUtils.transformIndexDeleteMove(deleteOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.TO.value()));
        deleteOp.put(OCKey.SHEET.value(), result.getShiftIdx());
        assignMoveSheet(moveOp, result.getMoveResult());
        return null;
    }

    private static JSONObject transform_deleteSheet_copySheet(JSONObject deleteOp, JSONObject copyOp) throws JSONException {
        final OTDeleteCopyResult result = OTUtils.transformIndexDeleteCopy(deleteOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.TO.value()));
        deleteOp.put(OCKey.SHEET.value(), result.getShiftIdx());
        assignMoveSheet(copyOp, result.getMoveResult());
        // delete the cloned sheet, if source sheet has been deleted
        return JSONHelper.getResultObject(null, null, null, result.optDelToIdx()!=null ? new JSONObject(2).put(OCKey.NAME.value(), OCValue.DELETE_SHEET.value()).put(OCKey.SHEET.value(), result.optDelToIdx()) : null);
    }

    private static JSONObject transform_deleteSheet_moveSheets(JSONObject deleteOp, JSONObject sortOp) throws JSONException {
        final OTShiftSortResult result = OTUtils.transformIndexDeleteSort(deleteOp.getInt(OCKey.SHEET.value()), new OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value())));
        deleteOp.put(OCKey.SHEET.value(), result.getShiftIdx());
        assignMoveSheets(sortOp, result.getSortVector());
        return null;
    }

    private static void transform_deleteSheet_formulaExpressions(JSONObject deleteOp, JSONObject fmlaOp) throws JSONException {
        if (!OTUtils.isOperationRemoved(fmlaOp)) {
            final DeleteSheetUpdateTask updateTask = new FormulaUpdateTask.DeleteSheetUpdateTask(deleteOp.getInt(OCKey.SHEET.value()));
            transformFormulaExpressions(fmlaOp, updateTask);
        }
    }

    private static JSONObject transform_deleteSheet_sheetIndex(JSONObject deleteOp, JSONObject sheetOp) throws JSONException {
        Object sheet = sheetOp.opt(OCKey.SHEET.value());
        if(sheet instanceof Integer) {
            final Integer newIdx = OTUtils.transformIndexDelete((Integer)sheet, deleteOp.getInt(OCKey.SHEET.value()), 1, false);
            if(newIdx!=null) {
                sheetOp.put(OCKey.SHEET.value(), newIdx);
            }
            else {
                OTUtils.setOperationRemoved(sheetOp);
            }

        }
        transform_deleteSheet_formulaExpressions(deleteOp, sheetOp);
        return null;
    }

    private static JSONObject transform_deleteSheet_drawingPos(JSONObject deleteOp, JSONObject drawingOp) throws JSONException {
        transformSheetInDrawingPositions(drawingOp, xfSheet -> OTUtils.transformIndexDelete(xfSheet, deleteOp.getInt(OCKey.SHEET.value()), 1, false));
        transform_deleteSheet_formulaExpressions(deleteOp, drawingOp);
        return null;
    }

    // moveSheet --------------------------------------------------------------

    private static JSONObject transform_moveSheet_moveSheet(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // transform and reassign all sheet indexes (either operation may become a no-op)
        final OTMoveMoveResult result = OTUtils.transformIndexMoveMove(lclOp.getInt(OCKey.SHEET.value()), lclOp.getInt(OCKey.TO.value()), extOp.getInt(OCKey.SHEET.value()), extOp.getInt(OCKey.TO.value()));
        assignMoveSheet(lclOp, result.getLclRes());
        assignMoveSheet(extOp, result.getExtRes());
        return null;
    }

    private static JSONObject transform_moveSheet_copySheet(JSONObject moveOp, JSONObject  copyOp) throws JSONException {
        // transform and reassign all sheet indexes (move operation may become a no-op)
        final OTMoveCopyResult result = OTUtils.transformIndexMoveCopy(moveOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.TO.value()), copyOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.TO.value()));
        assignMoveSheet(moveOp, result.optMoveResult());
        assignMoveSheet(copyOp, result.getCopyResult());
        return null;
    }

    private static JSONObject transform_moveSheet_moveSheets(JSONObject moveOp, JSONObject sortOp) throws JSONException {
        // transform and reassign all sheet indexes (move operation may become a no-op)
        final OTMoveSortResult result = OTUtils.transformIndexMoveSort(moveOp.getInt(OCKey.SHEET.value()), moveOp.getInt(OCKey.TO.value()), new OTUtils.OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value())));
        assignMoveSheet(moveOp, result.optMoveResult());
        assignMoveSheets(sortOp, result.optSortVector());
        return null;
    }

    private static JSONObject transform_moveSheet_sheetIndex(JSONObject moveOp, JSONObject sheetOp) throws JSONException {
        final Object sheet = sheetOp.opt(OCKey.SHEET.value());
        if(sheet instanceof Number) {
            sheetOp.put(OCKey.SHEET.value(), OTUtils.transformIndexMove(((Number)sheet).intValue(), moveOp.getInt(OCKey.SHEET.value()), 1, moveOp.getInt(OCKey.TO.value())));
        }
        return null;
    }

    private static JSONObject transform_moveSheet_drawingPos(JSONObject moveOp, JSONObject drawingOp) throws JSONException {
         transformSheetInDrawingPositions(drawingOp, sheet -> OTUtils.transformIndexMove(sheet, moveOp.getInt(OCKey.SHEET.value()), 1, moveOp.getInt(OCKey.TO.value())));
         return null;
    }

    // copySheet --------------------------------------------------------------

    private static JSONObject transform_copySheet_copySheet(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if(OTUtils.hasSameSheetName(lclOp, extOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_copySheet_copySheet" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
        final OTCopyCopyResult result = OTUtils.transformIndexCopyCopy(lclOp.getInt(OCKey.SHEET.value()), lclOp.getInt(OCKey.TO.value()), extOp.getInt(OCKey.SHEET.value()), extOp.getInt(OCKey.TO.value()));
        assignMoveSheet(lclOp, result.getLclResult());
        assignMoveSheet(extOp, result.getExtResult());
        return null;
    }

    private static JSONObject transform_copySheet_moveSheets(JSONObject copyOp, JSONObject sortOp) throws JSONException {
        final OTMoveSortResult result = OTUtils.transformIndexCopySort(copyOp.getInt(OCKey.SHEET.value()), copyOp.getInt(OCKey.TO.value()), new OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value())));
        assignMoveSheet(copyOp, result.optMoveResult());
        assignMoveSheets(sortOp, result.optSortVector());
        return null;
    }

    private static JSONObject transform_copySheet_changeSheet(JSONObject copyOp, JSONObject changeOp) throws JSONException {
        if(OTUtils.hasSameSheetName(copyOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_copySheet_changeSheet" + copyOp.toString() + "," + changeOp.toString() + "NOT IMPLEMENTED: different sheets cannot receive the same name");
        }
        return transform_copySheet_sheetIndex(copyOp, changeOp);
    }

    private static JSONObject transform_copySheet_formulaExpressions(JSONObject copyOp, JSONObject fmlaOp) throws JSONException {
        if (!OTUtils.isOperationRemoved(fmlaOp)) {
            transformFormulaExpressions(fmlaOp, new FormulaUpdateTask.RenameSheetUpdateTask(copyOp.getInt(OCKey.TO.value()), copyOp.getString(OCKey.SHEET_NAME.value())));
        }
        return null;
    }

    private static String[] OT_SHEET_TABLES_ALIAS = {OCValue.INSERT_TABLE.value(), OCValue.DELETE_TABLE.value(), OCValue.CHANGE_TABLE.value(), OCValue.CHANGE_TABLE_COLUMN.value()};


    private static JSONObject transform_copySheet_sheetIndex(JSONObject copyOp, JSONObject sheetOp) throws JSONException {
        // local "copySheet": replicate external operation locally in the new sheet
        // external "copySheet": replicate local operation externally in the new sheet (for simplicity,
        // otherwise it would be needed to generate matching reversed operations for each local operation)
        JSONObject externalOpsAfter = null;
        if (copyOp.getInt(OCKey.SHEET.value()) == sheetOp.optInt(OCKey.SHEET.value(), -1)) {

            // create a deep clone of the sheet index operation
            final JSONObject cloneOp = OTUtils.cloneJSONObject(sheetOp);
            cloneOp.put(OCKey.SHEET.value(), copyOp.getInt(OCKey.TO.value()));

            // special behavior for "changeSheet": do not clone a new sheet name (to prevent name collision)
            if (cloneOp.getString(OCKey.NAME.value()).equals(OCValue.CHANGE_SHEET.value())) {
                cloneOp.remove(OCKey.SHEET_NAME.value());
                checkChangeSheetNoOp(cloneOp);
            }

            // cloned table operations need new unused table name
            if(OTUtils.contains(cloneOp.getString(OCKey.NAME.value()), OT_SHEET_TABLES_ALIAS)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_copySheet_sheetIndex + NOT IMPLEMENTED: cloned table operations need unused table name");
            }

            // transform formula expressions in the cloned operation
            transform_copySheet_formulaExpressions(copyOp, cloneOp);

            // add cloned operation to the result object
            if (!OTUtils.isOperationRemoved(cloneOp)) {
                externalOpsAfter = cloneOp;
            }
        }

        // transform sheet index according to position of the clone
        final Object sheet = sheetOp.opt(OCKey.SHEET.value());
        if (sheet instanceof Integer) {
            sheetOp.put(OCKey.SHEET.value(), OTUtils.transformIndexInsert(((Integer)sheet).intValue(), copyOp.getInt(OCKey.TO.value()), 1));
        }
        return JSONHelper.getResultObject(null, externalOpsAfter, null, null);
    }

    private static JSONObject transform_copySheet_drawingPos(JSONObject copyOp, JSONObject drawingOp) throws JSONException {
        // local "copySheet": replicate external operation locally in the new sheet
        // external "copySheet": replicate local operation externally in the new sheet (for simplicity,
        // otherwise it would be needed to generate matching reversed operations for each local operation)
        JSONObject externalOpsAfter = null;
        if (copyOp.getInt(OCKey.SHEET.value()) == drawingOp.getJSONArray(OCKey.START.value()).getInt(0)) {
            // create a deep clone of the drawing operation
            final JSONObject cloneOp = OTUtils.cloneJSONObject(drawingOp);
            assignDrawingSheetIndex(cloneOp, copyOp.getInt(OCKey.TO.value()));

            // transform formula expressions in the cloned operation
            transform_copySheet_formulaExpressions(copyOp, cloneOp);

            // add cloned operation to the result object
            if (!OTUtils.isOperationRemoved(cloneOp)) {
                externalOpsAfter = cloneOp;
            }
        }
        // transform sheet index according to position of the clone
        transformSheetInDrawingPositions(drawingOp, xfSheet -> OTUtils.transformIndexInsert(xfSheet, copyOp.getInt(OCKey.TO.value()), 1));
        return JSONHelper.getResultObject(null, externalOpsAfter, null, null);
    }

    // moveSheets -------------------------------------------------------------

    private static JSONObject transform_moveSheets_moveSheets(JSONObject lclOp, JSONObject extOp) throws JSONException {
        final JSONArray lclSheets = lclOp.getJSONArray(OCKey.SHEETS.value());
        final JSONArray extSheets = extOp.getJSONArray(OCKey.SHEETS.value());
        if(lclSheets.length()!=extSheets.length()) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveSheets_moveSheets, sort vectors of 'moveSheets' operations have different length" + lclOp.toString() + "," + extOp.toString());
        }

        // transform and reassign sort vectors (either operation may become a no-op)
        final OTSortSortResult result = OTUtils.transformIndexSortSort(new OTSortVector(lclSheets), new OTSortVector(extSheets));
        assignMoveSheets(lclOp, result.optLclSortVector());
        assignMoveSheets(extOp, result.optExtSortVector());
        return null;
    }

    private static JSONObject transform_moveSheets_sheetIndex(JSONObject sortOp, JSONObject sheetOp) throws JSONException {
        Object sheet = sheetOp.opt(OCKey.SHEET.value());
        if (sheet instanceof Integer) {
            sheetOp.put(OCKey.SHEET.value(), OTUtils.transformIndexSort(((Integer)sheet).intValue(), new OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value()))));
        }
        return null;
    }

    private static JSONObject transform_moveSheets_drawingPos(JSONObject sortOp, JSONObject drawingOp) throws JSONException {
        transformSheetInDrawingPositions(drawingOp, xfsheet -> OTUtils.transformIndexSort(xfsheet, new OTSortVector(sortOp.getJSONArray(OCKey.SHEETS.value()))));
        return null;
    }

    // changeSheet ------------------------------------------------------------

    private static JSONObject transform_changeSheet_changeSheet(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if(lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            if(OTUtils.hasSameSheetName(lclOp, extOp)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeSheet_changeSheet" + lclOp.toString() + "," + extOp.toString() + "same sheet name");
            }
            return null;
        }

        // process the formatting attributes
        OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true)));

        // ignore externally renamed sheet, if it has been renamed locally
        OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), OCKey.SHEET_NAME.value());

        // ignore the entire operation, if all changes have been discarded
        checkChangeSheetNoOp(lclOp);
        checkChangeSheetNoOp(extOp);
        return null;
    }

    private static JSONObject transform_changeSheet_formulaExpressions(JSONObject changeOp, JSONObject fmlaOp) throws JSONException {
        if(changeOp.has(OCKey.SHEET_NAME.value())) {
            final RenameSheetUpdateTask updateTask = new FormulaUpdateTask.RenameSheetUpdateTask(changeOp.getInt(OCKey.SHEET.value()), changeOp.getString(OCKey.SHEET_NAME.value()));
            transformFormulaExpressions(fmlaOp, updateTask);
        }
        return null;
    }

    private static JSONObject transform_changeSheet_drawingAnchor(JSONObject changeOp, JSONObject drawingOp) throws JSONException {
/*  TODO: warnings...

        // transform absolute position of the drawing object according to new column/row default size
        const drawingAttrs = getDict(drawingOp.attrs, "drawing");
        if (drawingAttrs && isString(drawingAttrs.anchor)) {

            const colAttrs = getDict(changeOp.attrs, "column");
            this.warnIf(colAttrs && this.colResizeAttrs.some(name => name in colAttrs), "cannot transform absolute drawing anchor (changed column size)");

            const rowAttrs = getDict(changeOp.attrs, "row");
            this.warnIf(rowAttrs && this.rowResizeAttrs.some(name => name in rowAttrs), "cannot transform absolute drawing anchor (changed row size)");
        }

*/
        // transform formula expression linking the shape to a cell
        return transform_changeSheet_formulaExpressions(changeOp, drawingOp);
    }

    // addressTansform --------------------------------------------------------

    private static void transform_addressTransform_formulaExpressions(OTAddressTransformer transformer, JSONObject fmlaOp) throws JSONException {
        if (!OTUtils.isOperationRemoved(fmlaOp)) {
            final MoveCellsUpdateTask updateTask = new FormulaUpdateTask.MoveCellsUpdateTask(transformer.getSheetIndex(), transformer);
            transformFormulaExpressions(fmlaOp, updateTask);
        }
    }

    // moveIntervals ----------------------------------------------------------

    private static JSONObject transform_insertIntervals_insertIntervals(TransformOptions options, JSONObject lclOp, JSONObject extOp, boolean columns) throws JSONException {
        if(lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // read the interval lists to be transformed from the operations
        final IntervalArray lclIntervals = IntervalArray.createIntervals(columns, lclOp.getString(OCKey.INTERVALS.value()));
        final IntervalArray extIntervals = IntervalArray.createIntervals(columns, extOp.getString(OCKey.INTERVALS.value()));

        // transform external operation first
        final Direction dir = columns ? Direction.RIGHT : Direction.DOWN;
        final AddressTransformer lclTransformer = AddressTransformer.fromIntervals(addressFactory, lclIntervals, dir);

        final TransformIntervalOptions transformIntervalOptions = new TransformIntervalOptions(TransformMode.FIXED);
        final IntervalArray newExtIntervals = lclTransformer.transformIntervals(extIntervals, transformIntervalOptions);
        assignIntervalList(extOp, newExtIntervals);

        // transform local operation with the transformed external operation
        if(!newExtIntervals.isEmpty()) {
            final AddressTransformer extTransformer = AddressTransformer.fromIntervals(addressFactory, newExtIntervals, dir);
            final IntervalArray newLclIntervals = extTransformer.transformIntervals(lclIntervals, transformIntervalOptions);
            assignIntervalList(lclOp, newLclIntervals);
        }
        return null;
    }

    private static JSONObject transform_insertIntervals_deleteIntervals(TransformOptions options, JSONObject insertOp, JSONObject deleteOp, boolean columns) throws JSONException {
        // operations for different sheets are independent
        if(insertOp.getInt(OCKey.SHEET.value()) != deleteOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // read the interval lists to be transformed from the operations
        final IntervalArray insertIntervals = IntervalArray.createIntervals(columns, insertOp.getString(OCKey.INTERVALS.value()));
        final IntervalArray deleteIntervals = IntervalArray.createIntervals(columns, deleteOp.getString(OCKey.INTERVALS.value()));

        // transform the insert operation according to deleted intervals
        final AddressTransformer deleteTransformer = AddressTransformer.fromIntervals(addressFactory, deleteIntervals, columns ? Direction.LEFT : Direction.UP);
        final IntervalArray newInsertIntervals = deleteTransformer.transformIntervals(insertIntervals, new TransformIntervalOptions(TransformMode.FIXED)); // never empty
        assignIntervalList(insertOp, newInsertIntervals);

        // transform the delete operation according to inserted intervals
        final AddressTransformer insertTransformer = AddressTransformer.fromIntervals(addressFactory, insertIntervals, columns ? Direction.RIGHT : Direction.DOWN);
        final IntervalArray newDeleteIntervals = insertTransformer.transformIntervals(deleteIntervals, new TransformIntervalOptions(TransformMode.SPLIT));
        assignIntervalList(deleteOp, newDeleteIntervals);
        return null;
    }

    private static JSONObject transform_deleteIntervals_deleteIntervals(TransformOptions options, JSONObject lclOp, JSONObject extOp, boolean columns) throws JSONException {

        // operations for different sheets are independent
        if (!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // read the interval lists to be transformed from the operations
        final IntervalArray lclIntervals = IntervalArray.createIntervals(columns, lclOp.getString(OCKey.INTERVALS.value()));
        final IntervalArray extIntervals = IntervalArray.createIntervals(columns, extOp.getString(OCKey.INTERVALS.value()));

        // transform local operation according to externally deleted intervals
        final Direction dir = columns ? Direction.LEFT : Direction.UP;

        final AddressTransformer extTransformer = AddressTransformer.fromIntervals(addressFactory, extIntervals, dir);
        assignIntervalList(lclOp, extTransformer.transformIntervals(lclIntervals, null).merge());

        // transform external operation according to locally deleted intervals
        final AddressTransformer lclTransformer = AddressTransformer.fromIntervals(addressFactory, lclIntervals, dir);
        assignIntervalList(extOp, lclTransformer.transformIntervals(extIntervals, null).merge());
        return null;
    }

    private static JSONObject transform_moveIntervals_changeIntervals(TransformOptions options, JSONObject intervalsOp, JSONObject changeOp, Direction dir) throws JSONException {
        // operations for different sheets are independent
        if (intervalsOp.getInt(OCKey.SHEET.value()) != changeOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // create the address transformer
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        final boolean columns = transformer.columns;

        // read the interval list to be transformed from the operation
        final IntervalArray intervals = IntervalArray.createIntervals(columns, changeOp.getString(OCKey.INTERVALS.value()));

        // transform the index intervals (ignore the operation, if the intervals
        // will be deleted or shifted outside the sheet)
        final IntervalArray newIntervals = transformer.transformIntervals(intervals, new TransformIntervalOptions(TransformMode.SPLIT));
        assignIntervalList(changeOp, newIntervals);
        return null;
    }

    private static JSONObject transform_moveIntervals_changeCells(TransformOptions options, JSONObject intervalsOp, JSONObject changeOp, Direction dir) throws JSONException {
        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        return transform_changeCells_addressTransform(changeOp, transformer);
    }

    private static JSONObject transform_moveIntervals_mergeCells(TransformOptions options, JSONObject intervalsOp, JSONObject mergeOp, Direction dir) throws JSONException {
        // operations for different sheets are independent
        if (intervalsOp.getInt(OCKey.SHEET.value()) != mergeOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // read the range list to be transformed from the operation
        final CellRefRangeArray ranges = CellRefRangeArray.createRanges(mergeOp.getString(OCKey.RANGES.value()));

        // transform the cell range addresses (split when inserting between single stripes)
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);

        final MergeMode mergeOpType = MergeMode.fromValue(mergeOp.getString(OCKey.TYPE.value()));
        final boolean stripes;
        if(transformer.columns) {
            stripes = mergeOpType == MergeMode.VERTICAL;
        }
        else {
            stripes =  mergeOpType == MergeMode.HORIZONTAL;
        }

        final CellRefRangeArray newRanges = transformer.transformRanges(ranges, new TransformRangeOptions(stripes ? TransformMode.SPLIT : TransformMode.RESIZE));

        if(mergeOpType!=MergeMode.UNMERGE) {    // always allow to unmerge on single cells
            for(int i=newRanges.size() - 1; i >= 0; i--) {
                final CellRefRange range = newRanges.get(i);

                boolean remove = false;
                switch(mergeOpType) {
                    case HORIZONTAL: {
                        remove = range.singleCol();
                        break;
                    }
                    case VERTICAL: {
                        remove = range.singleRow();
                        break;
                    }
                    default: {
                        remove = range.single();
                    }
                }
                if(remove) {
                    newRanges.remove(i);
                }
            }
        }
        // ignore the operation, if the ranges will be deleted or shifted outside the sheet
        assignRangeList(mergeOp, newRanges);
        return null;
    }

    private static JSONObject transform_moveIntervals_definedName(TransformOptions options, JSONObject intervalsOp, JSONObject nameOp, Direction dir) throws JSONException {

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // transform formula expressions in all operations (regardless of sheet index)
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        transform_addressTransform_formulaExpressions(transformer, nameOp);
        return null;
    }

    private static JSONObject transform_moveIntervals_tableRange(TransformOptions options, JSONObject intervalsOp, JSONObject tableOp, Direction dir) throws JSONException {

        // operations for different sheets are independent, ignore missing table range
        if ((intervalsOp.getInt(OCKey.SHEET.value()) != tableOp.getInt(OCKey.SHEET.value())) || tableOp.optString(OCKey.RANGE.value(), null) == null) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // transform the table range
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        final CellRefRange tableRange = CellRefRange.createCellRefRange(tableOp.getString(OCKey.RANGE.value()));
        final CellRefRange newRange = transformer.transformRanges(new CellRefRangeArray(tableRange), new TransformRangeOptions(TransformMode.STRICT)).first();

        // create "deleteTable" operation for implicit deletion of the table
        if (newRange==null) {
            OTUtils.setOperationRemoved(tableOp);
            final JSONArray localOpsBefore = new JSONArray(1);
            final JSONObject op = new JSONObject();
            op.put(OCKey.NAME.value(), OCValue.DELETE_TABLE.value());
            op.put(OCKey.SHEET.value(), transformer.getSheetIndex());
            op.put(OCKey.TABLE.value(), tableOp.getString(OCKey.TABLE.value()));
            localOpsBefore.put(op);
            return JSONHelper.getResultObject(null, null, localOpsBefore, null);
        }

        // do not allow to expand/shrink columns (TODO: needs update of `headers` property)
        if (!dir.isVerticalDir() && (tableRange.getColumns() != newRange.getColumns())) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveIntervals_tableRange" + intervalsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: cannot insert/delete columns in table range");
        }

        // deleting header row or footer row of the table range is not supported
        if (dir == Direction.UP) {
            if(transformer.transformRanges(tableRange.headerRow(), new TransformRangeOptions()).isEmpty()) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveIntervals_tableRange" + intervalsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: cannot delete table header row");
            }
            if(transformer.transformRanges(tableRange.footerRow(), new TransformRangeOptions()).isEmpty()) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveIntervals_tableRange" + intervalsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: cannot delete table footer row");
            }
        }
        // update the table range in the table operation
        tableOp.put(OCKey.RANGE.value(), CellRefRange.getCellRefRange(newRange));
        return null;
    }

    private static JSONObject transform_moveIntervals_changeTableCol(JSONObject  intervalsOp, JSONObject tableOp, Direction dir) throws JSONException {

        // operations for different sheets are independent, ignore missing table range
        if (intervalsOp.getInt(OCKey.SHEET.value()) == tableOp.getInt(OCKey.SHEET.value())) {
            if(!dir.isVerticalDir()) {
                // table column index (relative to table range) cannot be transformed with column operations
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveIntervals_changeTableCol" + intervalsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: cannot transform table column index when inserting/deleting columns");
            }
        }
        return null;
    }

    private static JSONObject createDeleteDVRuleOp(JSONObject sourceOp, int sheet, boolean isOdf) throws JSONException {
        final JSONObject deleteOp = new JSONObject(3);
        deleteOp.put(OCKey.NAME.value(), OCValue.DELETE_DV_RULE.value());
        if(isOdf) {
            deleteOp.put(OCKey.RANGES.value(), sourceOp.getString(OCKey.RANGES.value()));
        }
        else {
            deleteOp.put(OCKey.INDEX.value(), sourceOp.getInt(OCKey.INDEX.value()));
        }
        return deleteOp.put(OCKey.SHEET.value(), sheet);
    }

    private static JSONObject transform_moveIntervals_rangeList(TransformOptions options, JSONObject intervalsOp, JSONObject rangesOp, Direction dir) throws JSONException {

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // transform formula expressions in all operations (regardless of sheet index)
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        transform_addressTransform_formulaExpressions(transformer, rangesOp);

        // operations for different sheets are independent
        if (!rangesOp.has(OCKey.RANGES.value()) || (intervalsOp.getInt(OCKey.SHEET.value()) != rangesOp.getInt(OCKey.SHEET.value()))) {
            return null;
        }

        final OCValue rangesOpName = OCValue.fromValue(rangesOp.getString(OCKey.NAME.value()));

        boolean isDVRule = false;
        boolean isCFRule = false;

        final TransformMode transformMode;
        switch(rangesOpName) {
            case INSERT_HYPERLINK : {
                transformMode = TransformMode.RESIZE;
                break;
            }
            case DELETE_HYPERLINK : {
                transformMode = TransformMode.RESIZE;
                break;
            }
            case INSERT_DV_RULE : {
                transformMode = TransformMode.EXPAND;
                isDVRule = true;
                break;
            }
            case CHANGE_DV_RULE : {
                transformMode = TransformMode.EXPAND;
                isDVRule = true;
                break;
            }
            case INSERT_CF_RULE : {
                transformMode = TransformMode.EXPAND;
                isCFRule = true;
                break;
            }
            case CHANGE_CF_RULE : {
                transformMode = TransformMode.EXPAND;
                isCFRule = true;
                break;
            }
            default: {
                throw new OTException(OTException.Type.HANDLER_MISSING_ERROR, "transform_moveIntervals_rangeList" + intervalsOp.toString() + "," + rangesOp.toString() + "INTERNAL ERROR: missing configuration for ranges transformation of operation");
            }
        }
        // read the range list to be transformed from the operation
        final CellRefRangeArray ranges = CellRefRangeArray.createRanges(rangesOp.getString(OCKey.RANGES.value()));

        // transform the cell range addresses
        final CellRefRangeArray newRanges = transformer.transformRanges(ranges, new TransformRangeOptions(transformMode));

        // set the operation to "removed", if the ranges will be deleted or shifted outside the sheet
        assignRangeList(rangesOp, newRanges);
        if (!newRanges.isEmpty()) {
            return null;
        }
        if(isDVRule || isCFRule) {
            final JSONArray localOpsBefore = new JSONArray(1);
            if(isDVRule) {
                localOpsBefore.put(createDeleteDVRuleOp(rangesOp, transformer.getSheetIndex(), options.isOdf()));
            }
            else {
                final JSONObject deleteOp = new JSONObject(2);
                deleteOp.put(OCKey.NAME.value(), OCValue.DELETE_CF_RULE.value());
                deleteOp.put(OCKey.ID.value(), rangesOp.getString(OCKey.ID.value()));
                deleteOp.put(OCKey.SHEET.value(), transformer.getSheetIndex());
                localOpsBefore.put(deleteOp);
            }
            return JSONHelper.getResultObject(null, null, localOpsBefore, null);
        }
        return null;
    }

    private static JSONObject transform_moveIntervals_sourceLinks(TransformOptions options, JSONObject intervalsOp, JSONObject drawingOp, Direction dir) throws JSONException {

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // transform formula expressions in all operations (regardless of sheet index)
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        transform_addressTransform_formulaExpressions(transformer, drawingOp);
        return null;
    }

    private static JSONObject transform_moveIntervals_cellAnchor(TransformOptions options, JSONObject intervalsOp, JSONObject anchorOp, Direction dir) throws JSONException {

        // operations for different sheets are independent
        if (intervalsOp.getInt(OCKey.SHEET.value()) != anchorOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final OCValue anchorOpName = OCValue.fromValue(anchorOp.getString(OCKey.NAME.value()));

        boolean isDeleteNote = false;
        boolean isDeleteComment = false;

        switch(anchorOpName) {
            case INSERT_NOTE : {
                isDeleteNote = true;
                break;
            }
            case DELETE_NOTE : {
                break;
            }
            case CHANGE_NOTE : {
                isDeleteNote = true;
                break;
            }
            case INSERT_COMMENT : {
                isDeleteComment = true;
                break;
            }
            case DELETE_COMMENT : {
                break;
            }
            case CHANGE_COMMENT : {
                isDeleteComment = true;
                break;
            }
            default: {
                throw new OTException(OTException.Type.HANDLER_MISSING_ERROR, "transform_moveIntervals_cellAnchor" + intervalsOp.toString() + "," + anchorOp.toString() + "INTERNAL ERROR: missing configuration for anchor transformation of operation");
            }
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // parse the addresses from the operations
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        final CellRef anchor = CellRef.createCellRef(anchorOp.getString(OCKey.ANCHOR.value()));

        // transform the anchor address of the comment (ignore the comment operation,
        // if the comment will be deleted or shifted outside the sheet)
        final CellRef newAnchor = transformer.transformAddress(anchor, new AddressTransformer.TransformAddressOptions());
        if (newAnchor!=null) {
            anchorOp.put(OCKey.ANCHOR.value(), CellRef.getCellRef(newAnchor));
            // transform the anchor drawing attribute (position of the text frame); never delete the text frame
            transformDrawingAnchor(anchorOp, transformer, new AddressTransformer.TransformAddressOptions(true));
            return null;
        }

        // generate a "delete" operation, if the anchor cell will be deleted or shifted outside the sheet
        OTUtils.setOperationRemoved(anchorOp);
        final JSONObject deleteOp = new JSONObject(3);
        if(isDeleteNote) {
            deleteOp.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
        }
        else if(isDeleteComment) {
            final int index = anchorOp.getInt(OCKey.INDEX.value());
            if(index!=0) {
                return null;
            }
            deleteOp.put(OCKey.NAME.value(), OCValue.DELETE_COMMENT.value());
            deleteOp.put(OCKey.INDEX.value(), 0);
        }
        if(deleteOp.isEmpty()) {
            return null;
        }
        final JSONArray localOpsBefore = new JSONArray(1);
        deleteOp.put(OCKey.SHEET.value(), transformer.getSheetIndex());
        deleteOp.put(OCKey.ANCHOR.value(), anchorOp.get(OCKey.ANCHOR.value()));
        localOpsBefore.put(deleteOp);
        return JSONHelper.getResultObject(null, null, localOpsBefore, null);
    }

    private static JSONObject transform_moveIntervals_moveAnchors(TransformOptions options, JSONObject intervalsOp, JSONObject moveOp, Direction dir, DeleteOpFn deleteOp) throws JSONException {

        // operations for different sheets are independent
        if (intervalsOp.getInt(OCKey.SHEET.value()) != moveOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // parse source and target addresses
        final CellRefArray fromAnchors = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        final CellRefArray toAnchors = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        // transformed anchor addresses
        final CellRefArray newFromAnchors = new CellRefArray();
        final CellRefArray newToAnchors = new CellRefArray();

        // additional operations for notes/comments to be deleted
        final JSONArray lclDelOps = new JSONArray();
        final JSONArray extDelOps = new JSONArray();

        // transform the anchor addresses
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        for(int i = 0; i < fromAnchors.size(); i++) {
            final CellRef fromAnchor = fromAnchors.get(i);
            // transform the anchor addresses
            final CellRef toAnchor = toAnchors.get(i);
            final CellRef newFromAnchor = transformer.transformAddress(fromAnchor, new AddressTransformer.TransformAddressOptions());
            final CellRef newToAnchor = transformer.transformAddress(toAnchor, new AddressTransformer.TransformAddressOptions());

            // push addresses, if the note/comment will not be deleted
            if (newFromAnchor!=null && newToAnchor!=null) {
                newFromAnchors.add(newFromAnchor);
                newToAnchors.add(newToAnchor);
                continue;
            }

            // if note/comment will be deleted at source or target address, create a local "delete" operation
            // with original target address to be applied remotely before the "moveIntervals" operation
            lclDelOps.put(deleteOp.generate(moveOp.getInt(OCKey.SHEET.value()), toAnchor));

            // if note/comment will be deleted at target address only, create an external "delete" operation
            // with tarnsformed source address to be applied locally before the "move" operation
            if (newFromAnchor!=null) {
                extDelOps.put(deleteOp.generate(moveOp.getInt(OCKey.SHEET.value()), newFromAnchor));
            }
        }
        // delete the "move" operation, if all notes/comments have been deleted
        assignMoveAnchors(moveOp, newFromAnchors, newToAnchors);
        // return the additional delete operations for deleted notes/comments
        return JSONHelper.getResultObject(extDelOps, null, lclDelOps, null);
    }

    private static JSONObject transform_moveIntervals_moveNotes(TransformOptions options, JSONObject intervalsOp, JSONObject moveOp, Direction dir) throws JSONException {

        final DeleteOpFn deleteOp = (sheet, anchor) -> {
            final JSONObject op = new JSONObject(3);
            op.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
            op.put(OCKey.SHEET.value(), sheet);
            op.put(OCKey.ANCHOR.value(), CellRef.getCellRef(anchor));
            return op;
        };
        return transform_moveIntervals_moveAnchors(options, intervalsOp, moveOp, dir, deleteOp);
    }

    private static JSONObject transform_moveIntervals_moveComments(TransformOptions options, JSONObject intervalsOp, JSONObject moveOp, Direction dir) throws JSONException {

        final DeleteOpFn deleteOp = (sheet, anchor) -> {
            final JSONObject op = new JSONObject(3);
            op.put(OCKey.NAME.value(), OCValue.DELETE_COMMENT.value());
            op.put(OCKey.SHEET.value(), sheet);
            op.put(OCKey.ANCHOR.value(), CellRef.getCellRef(anchor));
            op.put(OCKey.INDEX.value(), 0);
            return op;
        };
        return transform_moveIntervals_moveAnchors(options, intervalsOp, moveOp, dir, deleteOp);
    }

    interface DeleteOpFn {

        JSONObject generate(int sheet, CellRef address) throws JSONException;
    }

    private static JSONObject transform_moveIntervals_drawingAnchor(TransformOptions options, JSONObject intervalsOp, JSONObject drawingOp, Direction dir) throws JSONException {

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        // transform formula expressions in all operations (regardless of sheet index)
        final OTAddressTransformer transformer = createIntervalTransformer(addressFactory, intervalsOp, dir);
        transform_addressTransform_formulaExpressions(transformer, drawingOp);

        // operations for different sheets are independent
        if (intervalsOp.getInt(OCKey.SHEET.value()) != drawingOp.getJSONArray(OCKey.START.value()).getInt(0)) {
            return null;
        }

        // transform the anchor drawing attribute (position of the drawing frame)
        final boolean deleted = transformDrawingAnchor(drawingOp, transformer, new AddressTransformer.TransformAddressOptions(true));
        if (!deleted) {
            return null;
        }

        // generate a "deleteDrawing" operation, if the drawing objects will be deleted
        OTUtils.setOperationRemoved(drawingOp);
        final JSONArray localOpsBefore = new JSONArray(1);
        final JSONObject deleteDrawingOp = new JSONObject(2);
        deleteDrawingOp.put(OCKey.NAME.value(), OCValue.DELETE_DRAWING.value());
        deleteDrawingOp.put(OCKey.START.value(), drawingOp.getJSONArray(OCKey.START.value()));
        localOpsBefore.put(deleteDrawingOp);
        return JSONHelper.getResultObject(null, null, localOpsBefore, null);
    }

    // changeIntervals --------------------------------------------------------

    private static JSONObject transform_changeIntervals_changeIntervals(JSONObject lclOp, JSONObject extOp, boolean columns) throws JSONException {

        // operations for different sheets are independent
        if(!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        // read the column/row intervals from both operations
        IntervalArray lclIntervals = IntervalArray.createIntervals(columns, lclOp.getString(OCKey.INTERVALS.value()));
        IntervalArray extIntervals = IntervalArray.createIntervals(columns, extOp.getString(OCKey.INTERVALS.value()));

        // nothing to do if the intervals do not overlap
        final IntervalArray overlapIntervals = lclIntervals.intersect(extIntervals);
        if (overlapIntervals.isEmpty()) {
            return null;
        }

        // create the reduced local and external operations for the overlapping parts
        final JSONObject lclOverlapOp = JSONHelper.cloneJSONObject(lclOp);
        final JSONObject extOverlapOp = JSONHelper.cloneJSONObject(extOp);

        final boolean reducedStyle = OTUtils.reduceProperties(lclOverlapOp, extOverlapOp, new OTUtils.ReducePropertyOptions(true), "s");
        final boolean reducedAttrs = OTUtils.reduceOperationAttributes(lclOverlapOp, extOverlapOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true)));

        // if the operations do not influence each other, just use them as they are
        if (!reducedStyle && !reducedAttrs) {
            return null;
        }

        // set the "removed" flag in the new overlap operations, if all changes have been reduced
        final boolean lclRemoved = OTUtils.checkChangeIntervalsNoOp(lclOverlapOp);
        final boolean extRemoved = OTUtils.checkChangeIntervalsNoOp(extOverlapOp);

        // reduce the intervals to the distinct parts
        lclIntervals = lclIntervals.difference(overlapIntervals);
        extIntervals = extIntervals.difference(overlapIntervals);

        // process the local operation
        if (lclRemoved && lclIntervals.isEmpty()) {
            // remove the local operation, if it has been covered by the external operation, and does not add any change
            OTUtils.setOperationRemoved(lclOp);
        }
        else if (lclRemoved && !lclIntervals.isEmpty()) {
            // local operation does not change external formatting: reduce to uncovered intervals
            assignIntervalList(lclOp, lclIntervals);
        }
        else if (!lclRemoved && lclIntervals.isEmpty()) {
            // local operation covered by external operation: use reduced attributes only
            OTUtils.copyProperties(lclOp, lclOverlapOp, OCKey.ATTRS.value(), OCKey.S.value());
        }

        // process the external operation
        JSONArray externalOpsAfter = null;
        if (extRemoved && extIntervals.isEmpty()) {
            // remove the external operation, if it has been covered by the local operation, and does not add any change
            OTUtils.setOperationRemoved(extOp);
        }
        else if (extRemoved && !extIntervals.isEmpty()) {
            // external operation does not change local formatting: reduce to uncovered intervals
            assignIntervalList(extOp, extIntervals);
        }
        else if (!extRemoved && extIntervals.isEmpty()) {
            // external operation covered by local operation: use reduced attributes only
            OTUtils.copyProperties(extOp, extOverlapOp, OCKey.ATTRS.value(), OCKey.S.value());
        }
        else {
            // split external operation: all attributes in uncovered intervals, reduced attributes in covered intervals
            assignIntervalList(extOp, extIntervals);
            assignIntervalList(extOverlapOp, overlapIntervals);
            externalOpsAfter = new JSONArray(1);
            externalOpsAfter.put(extOverlapOp);
        }
        return externalOpsAfter == null ? null : JSONHelper.getResultObject(null, externalOpsAfter, null, null);
    }

    private static JSONObject transform_changeIntervals_drawingAnchor(JSONObject intervalsOp, JSONObject drawingOp, boolean columns) {

/* nothing is transformed and we do not have warnings in the backend transformation
        const intervalAttrs = getDict(intervalsOp.attrs, columns ? "column" : "row");
        if (!intervalAttrs) { return; }

        const drawingAttrs = getDict(drawingOp.attrs, "drawing");
        if (!drawingAttrs || !isString(drawingAttrs.anchor)) { return; }

        const resizeAttrNames = columns ? this.colResizeAttrs : this.rowResizeAttrs;
        this.warnIf(resizeAttrNames.some(name => name in intervalAttrs), "cannot transform absolute drawing anchor (changed column/row size)");
*/
        return null;
    }

    // changeCells ------------------------------------------------------------

    private static JSONObject transform_changeCells_addressTransform(JSONObject changeOp, OTAddressTransformer transformer) throws JSONException {

        // transform formula expressions in all operations (regardless of sheet index)
        transform_addressTransform_formulaExpressions(transformer, changeOp);

        // operations for different sheets are independent
        if (transformer.getSheetIndex() != changeOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        // transform the range addresses (keys of the contents dictionary)
        // TODO: handle shared formulas
        final JSONObject contents = new JSONObject();
        final JSONObject changeContents = changeOp.optJSONObject(OCKey.CONTENTS.value());
        if(changeContents!=null) {
            final Iterator<Entry<String, Object>> changeContentIter = changeContents.entrySet().iterator();
            while(changeContentIter.hasNext()) {
                final Entry<String, Object> changeContentEntry = changeContentIter.next();
                final Object data = changeContentEntry.getValue();
                if((data instanceof JSONObject) && (((JSONObject)data).hasAndNotNull(OCKey.SI.value()) || ((JSONObject)data).hasAndNotNull(OCKey.SR.value()))) {
                    // immediately fail to touch shared formulas
                    throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_addressTransform" + changeOp.toString() + "NOT IMPLEMENTED: shared formulas cannot be modified");
                }

                // transform the range address: insert gaps when inserting columns/rows in between
                final CellRefRange oldRange = CellRefRange.createCellRefRange(changeContentEntry.getKey());
                final CellRefRangeArray newRanges = transformer.transformRanges(oldRange, new TransformRangeOptions(TransformMode.SPLIT));
                if(!newRanges.isEmpty()) {
                    // create a new entry for all transformed ranges
                    for(CellRefRange cellRefRange:newRanges) {
                        contents.put(CellRefRange.getCellRefRange(cellRefRange), changeContentEntry.getValue());
                    }

                    // transform matrix range (its size must not change, but complete deletion is allowed)
                    if ((data instanceof JSONObject) && ((JSONObject)data).has(OCKey.MR.value())) {
                        final CellRefRange oldMatRange = CellRefRange.createCellRefRange(((JSONObject)data).getString(OCKey.MR.value()));
                        final CellRefRangeArray newMatRanges = transformer.transformRanges(oldMatRange, new TransformRangeOptions());
                        if(newMatRanges.size()!=1 || !oldMatRange.equalSize(newMatRanges.first())) {
                            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_addressTransform" + changeOp.toString() + "NOT IMPLEMENTED: matrix formula modified partially");
                        }
                        ((JSONObject)data).put(OCKey.MR.value(), CellRefRange.getCellRefRange(newMatRanges.first()));
                    }
                }
            }
        }
        // insert the new contents dictionary into the operation, delete operation on demand
        changeOp.put(OCKey.CONTENTS.value(), contents);
        OTUtils.checkChangeCellsNoOp(changeOp);
        return null;
    }

    // helper interface to store parsed range addresses and cell data objects in arrays
    private static class RangeAndCellData {

        final private CellRefRange range;
        final private JSONObject data;

        public RangeAndCellData(CellRefRange range, JSONObject data) {
            this.range = range;
            this.data = data;
        }

        public CellRefRange getRange() {
            return range;
        }

        public JSONObject getData() {
            return data;
        }
    }

    private static class ContentsContainer {
        List<RangeAndCellData> cellContents = new ArrayList<RangeAndCellData>();
        CellRefRangeArray matrixRanges = new CellRefRangeArray();

        public ContentsContainer(JSONObject op) throws JSONException {
            final Iterator<Entry<String, Object>> iter = op.getJSONObject(OCKey.CONTENTS.value()).entrySet().iterator();
            while(iter.hasNext()) {
                final Entry<String, Object> entry = iter.next();
                final CellRefRange range = CellRefRange.createCellRefRange(entry.getKey());
                final Object value = entry.getValue();
                if(value instanceof JSONObject) {
                    final JSONObject jsonObject = (JSONObject)value;
                    cellContents.add(new RangeAndCellData(range, jsonObject));
                    if(jsonObject.has("mr")) {
                        final String mr = jsonObject.optString("mr", "");
                        matrixRanges.add(mr.isEmpty() ? range : CellRefRange.createCellRefRange(mr));
                    }
                }
                else {
                    final JSONObject data = new JSONObject(1);
                    data.put(OCKey.V.value(), value);
                    cellContents.add(new RangeAndCellData(range, data));
                }
            }
        }

        // returns true if a collision between formula and a matrix of the matrixContainer was detected
        public boolean checkMatrixCollusion(ContentsContainer matrixContainer) {
            if(matrixContainer.matrixRanges.isEmpty()) {
                return false;
            }
            for(RangeAndCellData rangeAndCellData:cellContents) {
                final JSONObject data = rangeAndCellData.getData();
                if(data.has("f") ||data.has("si") || data.has("sr") || data.has("mr")) {
                    if(matrixContainer.matrixRanges.overlaps(rangeAndCellData.getRange())) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    // inserts an entry into a contents dictionary
    private static void insertCellData(JSONObject contents, CellRefRange range, Object cellData) throws JSONException {
        if(cellData instanceof JSONObject) {
            final int length = ((JSONObject) cellData).length();
            if(length == 0) {
                return;
            }
            else if(length == 1 && ((JSONObject)cellData).has(OCKey.V.value())) {
                cellData = ((JSONObject)cellData).get(OCKey.V.value());
            }
        }
        else if(cellData instanceof String && ((String) cellData).isEmpty()) {
            return;
        }
        if(cellData!=null) {
            contents.put(CellRefRange.getCellRefRange(range), cellData);
        }
    }

    private static OCKey findFirst(OCKey[] propKeys, JSONObject data) {
        for(OCKey key:propKeys) {
            if(data.has(key.value())) {
                return key;
            }
        }
        return null;
    }

    // helper function to process a single property in all cell data objects
    private static void processProperty(JSONObject lclData, JSONObject lclData2, JSONObject extData, JSONObject extData2, OCKey...propKeys) throws JSONException {

        // whether the property exists in the original cell data objects
        final OCKey lclKey = findFirst(propKeys, lclData);
        final OCKey extKey = findFirst(propKeys, extData);

        // send local property to server (write into `newLclData`), if it exists, and:
        // - it differs from the external property (or no external property is set), or
        // - the local operation deletes the cell (property "u")
        if (lclKey != null && (lclData.has(OCKey.U.value()) || (lclKey != extKey) || !(lclData.get(lclKey.value()).toString().equals(extData.opt(lclKey.value()).toString())))) {
            lclData2.put(lclKey.value(), lclData.get(lclKey.value()));
        }

        // re-apply local property locally (write into `newExtData`), if external operation
        // will delete the cell, but local operation has not deleted the cell
        if (lclKey != null && !lclData.has(OCKey.U.value()) && extData.has(OCKey.U.value())) {
            extData2.put(lclKey.value(), lclData.get(lclKey.value()));
        }

        // apply external property locally (write into `newExtData`), if it exists, and if the
        // local operation neither defines the property by itself nor deletes the cell entirely
        if (extKey != null && lclKey == null && !lclData.has(OCKey.U.value())) {
            extData2.put(extKey.value(), extData.get(extKey.value()));
        }

        // force to delete the cell externally, if an external property has been set but
        // locally the property was not modified
        if (lclData.has(OCKey.U.value()) && lclKey == null && extKey != null) {
            lclData2.put(OCKey.U.value(), true);
        }
    }

    private static void appendMapped(List<RangeAndCellData> contentsArray, CellRefRange range1, CellRefRange range2, JSONObject data) {
        final CellRefRangeArray ranges = new CellRefRangeArray(range1);
        final CellRefRangeArray  diffResultArray = ranges.difference(range2);
        if(diffResultArray!=null) {
            for(CellRefRange diffResult:diffResultArray) {
                contentsArray.add(new RangeAndCellData(diffResult, data));
            }
        }
    }

    private static JSONObject transform_changeCells_changeCells(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // operations for different sheets are independent
        if (lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        final ContentsContainer lclContainer = new ContentsContainer(lclOp);
        final ContentsContainer extContainer = new ContentsContainer(extOp);


        // matrix formulas must not collide with any other formulas (without transformation for simplicity; this should rarely happen)
        if(lclContainer.checkMatrixCollusion(extContainer) || extContainer.checkMatrixCollusion(lclContainer)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_changeCells" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: matrix formula overlaps with other formula");
        }

        // shortcuts to container properties
        final List<RangeAndCellData> lclContentsArr = lclContainer.cellContents;
        final List<RangeAndCellData> extContentsArr = extContainer.cellContents;

        // clear the contents dictionaries in the operations (will be re-filled in the loops below)
        final JSONObject lclContents = new JSONObject();
        lclOp.put(OCKey.CONTENTS.value(), lclContents);
        final JSONObject extContents = new JSONObject();
        extOp.put(OCKey.CONTENTS.value(), extContents);

        // process all cell data entries in the local operation (pop from front, push new to end)
        while(!lclContentsArr.isEmpty()) {
            // decompose the array element into range address and cell data
            final RangeAndCellData lclEntry = lclContentsArr.remove(0);
            final CellRefRange lclRange = lclEntry.getRange();
            final JSONObject lclData = lclEntry.getData();

            // find an overlapping entry in the external operation
            CellRefRange isectRange = null;
            int extIdx;
            for(extIdx = 0; extIdx < extContentsArr.size(); extIdx++) {
                final RangeAndCellData extEntry = extContentsArr.get(extIdx);
                isectRange = lclRange.intersect(extEntry.getRange());
                if(isectRange!=null) {
                    break;
                }
            }
            if(isectRange==null) {
                insertCellData(lclContents, lclRange, lclData);
                continue;
            }

            // remove found entry from array of external cell data entries;
            // decompose the array element into range address and cell data
            final RangeAndCellData extEntry = extContentsArr.remove(extIdx);
            final CellRefRange extRange = extEntry.getRange();
            final JSONObject extData = extEntry.getData();

            // immediately fail to transform different shared formulas (for simplicity; this should rarely happen)
            if((lclData.has(OCKey.SR.value()) && extData.has(OCKey.F.value())) || (extData.has(OCKey.SI.value()) || extData.has(OCKey.SR.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_changeCells" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: shared formula overlaps with other formula");
            }
            if(((extData.has(OCKey.SR.value()) && ((lclData.has(OCKey.F.value()) || lclData.has(OCKey.SI.value())))))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_changeCells" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: shared formula overlaps with other formula");
            }

            // create new cell data objects to be filled with the effective cell properties
            final JSONObject lclData2 = new JSONObject();
            final JSONObject extData2 = new JSONObject();

            // copy the delete property if it exists on one side only
            if(lclData.has(OCKey.U.value()) && !extData.has(OCKey.U.value())) {
                lclData2.put(OCKey.U.value(), true);
            }
            if(!lclData.has(OCKey.U.value()) && extData.has(OCKey.U.value())) {
                extData2.put(OCKey.U.value(), true);
            }

            // process all properties of the cell data objects ("sr" and "mr" will not collide, see above)
            processProperty(lclData, lclData2, extData, extData2, OCKey.V, OCKey.E);
            processProperty(lclData, lclData2, extData, extData2, OCKey.F);
            processProperty(lclData, lclData2, extData, extData2, OCKey.SI);
            processProperty(lclData, lclData2, extData, extData2, OCKey.S);

            // insert the filled cell data into the contents dictionaries
            insertCellData(lclContents, isectRange, lclData2);
            insertCellData(extContents, isectRange, extData2);

            // append remaining uncovered parts of the ranges to the contents arrays
            // (either of them may overlap with other unprocessed cell ranges)
            if (!lclRange.equals(extRange)) {
                appendMapped(lclContentsArr, lclRange, extRange, lclData);
                appendMapped(extContentsArr, extRange, lclRange, extData);
            }
        }
        for(RangeAndCellData extEntry:extContentsArr) {
            // remaining entries in `extContentsArr` are not covered by local contents
            insertCellData(extContents, extEntry.getRange(), extEntry.getData());
        }

        // mark empty operations as "removed"
        OTUtils.checkChangeCellsNoOp(lclOp);
        OTUtils.checkChangeCellsNoOp(extOp);
        return null;
    }

    private static JSONObject transform_changeCells_moveCells(TransformOptions options, JSONObject changeOp, JSONObject moveOp) throws JSONException {

        final AddressFactory addressFactory = new AddressFactory(options.getDocumentFormat());
        final OTAddressTransformer transformer = createRangeTransformer(addressFactory, moveOp);
        return transform_changeCells_addressTransform(changeOp, transformer);
    }

    private static JSONObject transform_changeCells_mergeCells(JSONObject changeOp, JSONObject mergeOp) throws JSONException {

        // operations for different sheets are independent, unmerging is always allowed
        if (changeOp.getInt(OCKey.SHEET.value()) != mergeOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        // compare merged ranges against matrix ranges
        final CellRefRangeArray mergedRanges = CellRefRangeArray.createRanges(mergeOp.getString(OCKey.RANGES.value()));
        final JSONObject contents = changeOp.getJSONObject(OCKey.CONTENTS.value());
        final Iterator<Entry<String, Object>> contentIter = contents.entrySet().iterator();
        while(contentIter.hasNext()) {
            final Entry<String, Object> entry = contentIter.next();
            final Object value = entry.getValue();
            if(value instanceof JSONObject && ((JSONObject)value).has(OCKey.MR.value())) {
                final String matRange = ((JSONObject)value).getString(OCKey.MR.value());
                if(mergedRanges.overlaps(CellRefRange.createCellRefRange(matRange))) {
                    throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_mergeCells" + changeOp.toString() + "," + mergeOp.toString() + "NOT IMPLEMENTED: merged range overlaps with matrix formula");
                }
            }
        }
        return null;
    }

    private static JSONObject transform_changeCells_changeName(JSONObject cellsOp, JSONObject nameOp) throws JSONException {
        return transform_changeName_formulaExpressions(nameOp, cellsOp);
    }

    private static JSONObject transform_changeCells_tableRange(JSONObject cellsOp, JSONObject tableOp) throws JSONException {

        // update new table name in cell formulas
        transform_changeTable_formulaExpressions(tableOp, cellsOp);

        // operations for different sheets are independent, ignore missing table range
        if ((cellsOp.getInt(OCKey.SHEET.value()) != tableOp.getInt(OCKey.SHEET.value())) || !tableOp.has(OCKey.RANGE.value())) {
            return null;
        }

        // parse the table range
        final CellRefRange tableRange = CellRefRange.createCellRefRange(tableOp.getString(OCKey.RANGE.value()));

        // check that the table header will not be changed, and matrix formulas do not overlap with the table
        final JSONObject contents = cellsOp.getJSONObject(OCKey.CONTENTS.value());
        final Iterator<Entry<String, Object>> contentIter = contents.entrySet().iterator();
        while(contentIter.hasNext()) {
            final Entry<String, Object> contentEntry = contentIter.next();
            final Object data = contentEntry.getValue();
            final boolean isJSONObject = data instanceof JSONObject;

            if(!isJSONObject || ((JSONObject)data).has(OCKey.V.value()) || ((JSONObject)data).has(OCKey.F.value()) || ((JSONObject)data).has(OCKey.SI.value()) || ((JSONObject)data).has(OCKey.SR.value())) {
                final CellRefRange range = CellRefRange.createCellRefRange(contentEntry.getKey());
                if(range.overlaps(tableRange.headerRow())) {
                    throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_tableRange" + cellsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: header row of table range cannot be changed");
                }
            }

            if(isJSONObject && ((JSONObject)data).has(OCKey.MR.value())) {
                final CellRefRange matrixRange = CellRefRange.createCellRefRange(((JSONObject)data).getString(OCKey.MR.value()));
                if(matrixRange.overlaps(tableRange)) {
                    throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeCells_tableRange" + cellsOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: matrix range cannot overlap with table range");
                }
            }
        }
        return null;
    }

    // moveCells --------------------------------------------------------------

    private static JSONObject transform_moveCells_formulaExpressions(TransformOptions options,JSONObject moveOp, JSONObject fmlaOp) throws JSONException {
        final OTAddressTransformer transformer = createRangeTransformer(new AddressFactory(options.getDocumentFormat()), moveOp);
        transform_addressTransform_formulaExpressions(transformer, fmlaOp);
        return null;
    }

    // mergeCells -------------------------------------------------------------

    private static JSONObject transform_mergeCells_mergeCells(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // operations for different sheets are independent
        if ((lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value()))) {
            return null;
        }

        // parse local and external merged ranges
        final CellRefRangeArray lclRanges = CellRefRangeArray.createRanges(lclOp.getString(OCKey.RANGES.value()));
        final CellRefRangeArray extRanges = CellRefRangeArray.createRanges(extOp.getString(OCKey.RANGES.value()));

        // reduce all external ranges that overlap with local ranges (local ranges win over external ranges)
        final CellRefRangeArray newExtRanges = reduceMergedRanges(lclRanges, extRanges, MergeMode.fromValue(extOp.optString(OCKey.TYPE.value(), "merge")));

        // assign transformed range lists to operations, set to "removed" state on demand
        assignRangeList(extOp, newExtRanges);
        return null;
    }

    private static JSONObject transform_mergeCells_tableRange(JSONObject mergeOp, JSONObject tableOp) throws JSONException {

        // operations for different sheets are independent, ignore unmerge, ignore missing table range
        final MergeMode mergeMode = MergeMode.fromValue(mergeOp.optString(OCKey.TYPE.value(), "merge"));
        if ((mergeOp.getInt(OCKey.SHEET.value()) != tableOp.getInt(OCKey.SHEET.value())) || mergeMode == MergeMode.UNMERGE || !tableOp.has(OCKey.RANGE.value())) {
            return null;
        }

        // merged ranges must not overlap with table range
        final CellRefRangeArray mergedRanges = CellRefRangeArray.createRanges(mergeOp.getString(OCKey.RANGES.value()));
        final CellRefRange tableRange = CellRefRange.createCellRefRange(tableOp.getString(OCKey.RANGE.value()));

        // reduce merged ranges that overlap with table range
        final CellRefRangeArray newMergedRanges = reduceMergedRanges(new CellRefRangeArray(tableRange), mergedRanges, mergeMode);
        assignRangeList(mergeOp, newMergedRanges);

        // add an unmerge operation if the merged ranges have been reduced
        if(mergedRanges.equals(newMergedRanges)) {
            return null;
        }
        final JSONArray externalOpsBefore = new JSONArray(1);
        final JSONObject op = new JSONObject(4);
        op.put(OCKey.NAME.value(), OCValue.MERGE_CELLS.value());
        op.put(OCKey.SHEET.value(), tableOp.getInt(OCKey.SHEET.value()));
        op.put(OCKey.RANGES.value(), CellRefRange.getCellRefRange(tableRange));
        op.put(OCKey.TYPE.value(), "unmerge");
        externalOpsBefore.put(op);
        return JSONHelper.getResultObject(externalOpsBefore, null, null, null);
    }

    private static CellRefRangeArray reduceMergedRanges(CellRefRangeArray lclRanges, CellRefRangeArray extRanges, MergeMode mergeMode) {

        // reduce all external ranges that overlap with local ranges (local ranges win over external ranges)
        switch (mergeMode) {
            case HORIZONTAL:
            case VERTICAL: {
                final CellRefRangeArray result = new CellRefRangeArray();
                final boolean columns = mergeMode == MergeMode.VERTICAL;
                for(CellRefRange extRange:extRanges) {
                    final CellRefRangeArray resRanges = new CellRefRangeArray(extRange);
                    for(CellRefRange lclRange:lclRanges) {
                        if(!extRange.overlaps(lclRange)) {
                            continue;
                        }
                        final CellRefRangeArray srcRanges = new CellRefRangeArray(resRanges);
                        resRanges.clear();
                        for(CellRefRange srcRange:srcRanges) {
                            if (srcRange.getStart().get(columns) < lclRange.getStart().get(columns)) {
                                final CellRefRange resRange = srcRange.clone();
                                resRange.getEnd().set(Math.min(srcRange.getEnd().get(columns), lclRange.getStart().get(columns) - 1), columns);
                                resRanges.add(resRange);
                            }
                            if (lclRange.getEnd().get(columns) < srcRange.getEnd().get(columns)) {
                                final CellRefRange resRange = srcRange.clone();
                                resRange.getStart().set(Math.max(srcRange.getStart().get(columns), lclRange.getEnd().get(columns) + 1), columns);
                                resRanges.add(resRange);
                            }
                        }
                    }
                    result.addAll(resRanges);
                }
                return result;
            }

            case UNMERGE:
                return extRanges.difference(lclRanges);

            default: {
                final CellRefRangeArray result = new CellRefRangeArray();
                for(CellRefRange extRange:extRanges) {
                    if(!lclRanges.overlaps(extRange)) {
                        result.add(extRange.clone());
                    }
                }

                return result;
            }
        }
    }

    // hyperlinks -------------------------------------------------------------

    private static JSONObject transform_changeHyperlink_changeHyperlink(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // operations for different sheets are independent
        if (lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        // parse local and external hyperlink ranges
        // parse local and external merged ranges
        final CellRefRangeArray lclRanges = CellRefRangeArray.createRanges(lclOp.getString(OCKey.RANGES.value()));
        final CellRefRangeArray extRanges = CellRefRangeArray.createRanges(extOp.getString(OCKey.RANGES.value()));

        // reduce extenal ranges so that they do not overwrite local hyperlinks
        assignRangeList(extOp, extRanges.difference(lclRanges));
        return null;
    }

    // insertName -------------------------------------------------------------

    private static JSONObject transform_insertName_insertName(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // cannot insert an existing name again
        if(OTUtils.isSameName(lclOp, extOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_insertName" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot insert existing name again");
        }
        return null;
    }

    private static JSONObject transform_insertName_deleteName(JSONObject insertOp, JSONObject deleteOp) throws JSONException {

        if(OTUtils.isSameName(insertOp, deleteOp)) {
            OTUtils.setOperationRemoved(insertOp);
        }
        return null;
    }

    private static JSONObject transform_insertName_changeName(JSONObject insertOp, JSONObject changeOp) throws JSONException {

        // cannot insert an existing name again
        if(OTUtils.isSameName(insertOp, changeOp)) {
            // cannot insert an existing name again
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_changeName" + insertOp.toString() + "," + changeOp.toString() + "OP ERROR: label of inserted name already used");
        }

        // cannot insert name and change the label of another name to the same label
        if (changeOp.has(OCKey.NEW_LABEL.value()) && OTUtils.isSameSheet(insertOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_changeName" + insertOp.toString() + "," + changeOp.toString() + "OP ERROR: label of inserted name already used");
        }

        // update new label in all formula expressions
        return transform_changeName_formulaExpressions(changeOp, insertOp);
    }

    private static JSONObject transform_insertName_insertTable(JSONObject nameOp, JSONObject tableOp) throws JSONException {
        if (tableOp.has(OCKey.TABLE.value())) {
            if(nameOp.getString(OCKey.LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_insertTable" + nameOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: defined name and table cannot have same label");
            }
        }
        return null;
    }

    private static JSONObject transform_insertName_changeTable(JSONObject nameOp, JSONObject tableOp) throws JSONException {

        // cannot insert a name for an existing table
        if (tableOp.has(OCKey.TABLE.value())) {
            if(nameOp.getString(OCKey.LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_changeTable" + nameOp.toString() + "," + tableOp.toString() + "OP ERROR: label of inserted name already used");
            }
        }

        if(tableOp.has(OCKey.NEW_NAME.value())) {
            if(nameOp.getString(OCKey.LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.NEW_NAME.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertName_changeTable" + nameOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: defined name and table cannot have same label");
            }
        }

        // update new table name in all formula expressions of the defined name
        return transform_changeTable_formulaExpressions(tableOp, nameOp);
    }

    // deleteName -------------------------------------------------------------

    private static JSONObject transform_deleteName_deleteName(JSONObject lclOp, JSONObject extOp) throws JSONException {

        if (OTUtils.isSameName(lclOp, extOp)) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }
        return null;
    }

    private static JSONObject transform_deleteName_changeName(JSONObject deleteOp, JSONObject changeOp) throws JSONException {

        // different names are independent
        if (!OTUtils.isSameName(deleteOp, changeOp)) {
            return null;
        }

        // delete operation wins over change operation
        OTUtils.setOperationRemoved(changeOp);

        // update label in delete operation
        if (changeOp.has(OCKey.NEW_LABEL.value())) {
            deleteOp.put(OCKey.LABEL.value(), changeOp.getString(OCKey.NEW_LABEL.value()));
        }
        return null;
    }

    // changeName -------------------------------------------------------------

    private static JSONObject transform_changeName_formulaExpressions(JSONObject nameOp, JSONObject fmlaOp) throws JSONException {

        final String newLabel = nameOp.optString(OCKey.NEW_LABEL.value(), "");
        if (!newLabel.isEmpty()) {
            final int sheet = nameOp.optInt(OCKey.SHEET.value(), -1);
            final RelabelNameUpdateTask updateTask = new FormulaUpdateTask.RelabelNameUpdateTask(sheet == -1 ? null : sheet, nameOp.getString(OCKey.LABEL.value()), nameOp.getString(OCKey.NEW_LABEL.value()));
            transformFormulaExpressions(fmlaOp, updateTask);
        }
        return null;
    }

    private static JSONObject transform_changeName_changeName(JSONObject lclOp, JSONObject extOp) throws JSONException {

        if (OTUtils.isSameName(lclOp, extOp)) {

            // update new label in operations (before reducing the property)
            if (lclOp.has(OCKey.NEW_LABEL.value())) {
                extOp.put(OCKey.LABEL.value(), lclOp.getString(OCKey.NEW_LABEL.value()));
            }
            if (extOp.has(OCKey.NEW_LABEL.value())) {
                lclOp.put(OCKey.LABEL.value(), extOp.getString(OCKey.NEW_LABEL.value()));
            }

            // transform the operation properties
            OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), OCKey.NEW_LABEL.value(), OCKey.FORMULA.value());

            // set no-ops to "removed" state
            OTUtils.checkChangeNameNoOp(lclOp);
            OTUtils.checkChangeNameNoOp(extOp);
        }

        // cannot change different names in same parent to the same label
        if (lclOp.has(OCKey.NEW_LABEL.value()) && extOp.has(OCKey.NEW_LABEL.value()) && (lclOp.optInt(OCKey.SHEET.value(), -1) == extOp.optInt(OCKey.SHEET.value(), -1))) {
            if(lclOp.getString(OCKey.NEW_LABEL.value()).equalsIgnoreCase(extOp.getString(OCKey.NEW_LABEL.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_changeName" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot set same label to different names");
            }
        }

        // update new label in formula expressions
        transform_changeName_formulaExpressions(lclOp, extOp);
        transform_changeName_formulaExpressions(extOp, lclOp);
        return null;
    }

    private static JSONObject transform_changeName_insertTable(JSONObject nameOp, JSONObject tableOp) throws JSONException {

        // cannot insert a table for an existing name
        if (tableOp.has(OCKey.TABLE.value())) {
            if(nameOp.getString(OCKey.LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_insertTable" + nameOp.toString() + "," + tableOp.toString() + "OP ERROR: name of inserted table already used");
            }
        }

        // cannot insert a table and change a name to the same label
        if (nameOp.has(OCKey.NEW_LABEL.value()) && tableOp.has(OCKey.TABLE.value())) {
            if(nameOp.getString(OCKey.NEW_LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_insertTable" + nameOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: table and defined name cannot have same label");
            }
        }
        return null;
    }

    private static JSONObject transform_changeName_changeTable(JSONObject nameOp, JSONObject tableOp) throws JSONException {

        // cannot change name label to existing table
        if (nameOp.has(OCKey.NEW_LABEL.value()) && tableOp.has(OCKey.TABLE.value())) {
            if(nameOp.getString(OCKey.NEW_LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_changeTable" + nameOp.toString() + "," + tableOp.toString() + "OP ERROR: name label already used by table");
            }
        }

        // cannot change table name to existing defined name
        if (tableOp.has(OCKey.NEW_NAME.value())) {
            if(nameOp.getString(OCKey.LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.NEW_NAME.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_changeTable" + nameOp.toString() + "," + tableOp.toString() + "OP ERROR: table name already used by defined name");
            }
        }

        // cannot change both labels to the same string
        if (nameOp.has(OCKey.NEW_LABEL.value()) && tableOp.has(OCKey.NEW_NAME.value())) {
            if(nameOp.getString(OCKey.NEW_LABEL.value()).equalsIgnoreCase(tableOp.getString(OCKey.NEW_NAME.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeName_changeTable" + nameOp.toString() + "," + tableOp.toString() + "NOT IMPLEMENTED: table and defined name cannot have same label");
            }
        }

        // update new table name in all formula expressions of the defined name
        return transform_changeTable_formulaExpressions(tableOp, nameOp);
    }

    // insertTable ------------------------------------------------------------

    private static JSONObject transform_insertTable_insertTable(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // tables must not have same name (regardless of sheet)
        if(OTUtils.isSameTable(lclOp, extOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_insertTable" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot insert table with same name twice");
        }

        // tables in the same sheet must not overlap
        if (lclOp.getInt(OCKey.SHEET.value()) == extOp.getInt(OCKey.SHEET.value())) {
            final CellRefRange lclRange = CellRefRange.createCellRefRange(lclOp.getString(OCKey.RANGE.value()));
            final CellRefRange extRange = CellRefRange.createCellRefRange(extOp.getString(OCKey.RANGE.value()));
            if(lclRange.overlaps(extRange)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_insertTable" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: table ranges cannot overlap");
            }
        }
        return null;
    }

    private static JSONObject transform_insertTable_deleteTable(JSONObject insertOp, JSONObject deleteOp) throws JSONException {

        // cannot insert an existing table again (regardless of sheet)
        if(OTUtils.isSameTable(insertOp, deleteOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_insertTable" + insertOp.toString() + "," + deleteOp.toString() + "OP ERROR: cannot insert existing table");
        }
        return null;
    }

    private static JSONObject transform_insertTable_changeTable(JSONObject insertOp, JSONObject changeOp) throws JSONException {

        // cannot insert an existing table again (regardless of sheet)
        if(OTUtils.isSameTable(insertOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_changeTable" + insertOp.toString() + "," + changeOp.toString() + "OP ERROR: cannot insert existing table");
        }

        // cannot insert table and change another table to the same name
        if (insertOp.has(OCKey.TABLE.value()) && changeOp.has(OCKey.NEW_NAME.value())) {
            if(insertOp.getString(OCKey.TABLE.value()).equalsIgnoreCase(changeOp.getString(OCKey.NEW_NAME.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_changeTable" + insertOp.toString() + "," + changeOp.toString() + "NOT IMPLEMENTED: label of inserted name already used");
            }
        }
        return null;
    }

    private static JSONObject transform_insertTable_changeTableCol(JSONObject insertOp, JSONObject changeOp) throws JSONException {

        // cannot insert an existing table again (regardless of sheet)
        if(OTUtils.isSameTable(insertOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertTable_changeTableCol" + insertOp.toString() + "," + changeOp.toString() + "OP ERROR: cannot insert existing table");
        }
        return null;
    }

    // deleteTable ------------------------------------------------------------

    private static JSONObject transform_deleteTable_deleteTable(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // different tables are independent
        if(!OTUtils.isSameTable(lclOp, extOp)) {
            return null;
        }

        if(lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_deleteTable_deleteTable" + lclOp.toString() + "," + extOp.toString() + "OP ERROR: same table name in different sheets");
        }

        // both operations become no-ops
        OTUtils.setOperationRemoved(lclOp);
        OTUtils.setOperationRemoved(extOp);
        return null;
    }

    private static JSONObject transform_deleteTable_changeTable(JSONObject deleteOp, JSONObject changeOp) throws JSONException {

        // different tables are independent
        if (!OTUtils.isSameTable(deleteOp, changeOp)) {
            return null;
        }

        if(deleteOp.getInt(OCKey.SHEET.value()) != changeOp.getInt(OCKey.SHEET.value())) {
            // operations must target the same sheet
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_deleteTable_changeTable" + deleteOp.toString() + "," + changeOp.toString() + "OP ERROR: same table name in different sheets");
        }

        // delete operation wins over change operation
        OTUtils.setOperationRemoved(changeOp);

        // update table name in delete operation
        if(changeOp.has(OCKey.NEW_NAME.value())) {
            deleteOp.put(OCKey.TABLE.value(), changeOp.getString(OCKey.NEW_NAME.value()));
        }
        return null;
    }

    private static JSONObject transform_deleteTable_changeTableCol(JSONObject deleteOp, JSONObject changeOp) throws JSONException {
        // different tables are independent
        if (!OTUtils.isSameTable(deleteOp, changeOp)) {
            return null;
        }

        if(deleteOp.getInt(OCKey.SHEET.value()) != changeOp.getInt(OCKey.SHEET.value())) {
            // operations must target the same sheet
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_deleteTable_changeTableCol" + deleteOp.toString() + "," + changeOp.toString() + "OP ERROR: same table name in different sheets");
        }

        // delete operation wins over change operation
        OTUtils.setOperationRemoved(changeOp);
        return null;
    }

    // changeTable ------------------------------------------------------------

    private static JSONObject transform_changeTable_formulaExpressions(JSONObject tableOp, JSONObject fmlaOp) throws JSONException {

        if (tableOp.has(OCKey.TABLE.value()) && tableOp.has(OCKey.NEW_NAME.value())) {
            final RenameTableUpdateTask updateTask = new FormulaUpdateTask.RenameTableUpdateTask(tableOp.getString(OCKey.TABLE.value()), tableOp.getString(OCKey.NEW_NAME.value()));
            transformFormulaExpressions(fmlaOp, updateTask);
        }
        return null;
    }

    private static JSONObject transform_changeTable_changeTable(JSONObject lclOp, JSONObject extOp) throws JSONException {

        if (OTUtils.isSameTable(lclOp, extOp)) {

            // operations must target the same sheet
            if(!OTUtils.isSameSheet(lclOp, extOp)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTable_changeTable" + lclOp.toString() + "," + extOp.toString() + "OP ERROR: same table name in different sheets");
            }

            // update table name in operations (before reducing the property)
            if (lclOp.has(OCKey.NEW_NAME.value())) {
                extOp.put(OCKey.TABLE.value(), lclOp.getString(OCKey.NEW_NAME.value()));
            }
            if (extOp.has(OCKey.NEW_NAME.value())) {
                lclOp.put(OCKey.TABLE.value(), extOp.getString(OCKey.NEW_NAME.value()));
            }

            // transform the formatting attributes and other properties
            OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true)));
            OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), OCKey.NEW_NAME.value(), OCKey.RANGE.value(), OCKey.HEADERS.value());

            // set no-ops to "removed" state
            OTUtils.checkChangeTableNoOp(lclOp);
            OTUtils.checkChangeTableNoOp(extOp);
        }
        // cannot change different tables to the same name
        if (lclOp.has(OCKey.NEW_NAME.value()) && extOp.has(OCKey.NEW_NAME.value())) {
            if(lclOp.getString(OCKey.NEW_NAME.value()).equalsIgnoreCase(extOp.getString(OCKey.NEW_NAME.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTable_changeTable" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot set same name to different tables");
            }
        }
        return null;
    }

    private static JSONObject transform_changeTable_changeTableCol(JSONObject changeOp, JSONObject colOp) throws JSONException {

        if (OTUtils.isSameTable(changeOp, colOp)) {

            // operations must target the same sheet
            if(!OTUtils.isSameSheet(changeOp, colOp)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTable_changeTableCol" + changeOp.toString() + "," + colOp.toString() + "OP ERROR: same table name in different sheets");
            }

            // cannot change table range (TODO: needs transformation or validity checking of table column index)
            if(changeOp.has(OCKey.RANGE.value())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTable_changeTableCol" + changeOp.toString() + "," + colOp.toString() + "NOT IMPLEMENTED: table range cannot be changed when changing columns");
            }

            // update table name in column operation
            if (changeOp.has(OCKey.NEW_NAME.value())) {
                colOp.put(OCKey.TABLE.value(), changeOp.getString(OCKey.NEW_NAME.value()));
            }

        }
        else if (changeOp.has(OCKey.NEW_NAME.value()) && colOp.has(OCKey.TABLE.value())) {
            // cannot change table name to a used name
            if(changeOp.getString(OCKey.NEW_NAME.value()).equalsIgnoreCase(colOp.getString(OCKey.TABLE.value()))) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTable_changeTableCol" + changeOp.toString() + "," + colOp.toString() + "OP ERROR: table name already used");
            }
        }
        return null;
    }

    // changeTableCol ---------------------------------------------------------

    private static JSONObject transform_changeTableCol_changeTableCol(JSONObject lclOp, JSONObject extOp) throws JSONException {

        if (OTUtils.isSameTable(lclOp, extOp) && (lclOp.getInt(OCKey.COL.value()) == extOp.getInt(OCKey.COL.value()))) {
            // operations must target the same sheet
            if(!OTUtils.isSameSheet(lclOp, extOp)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTableCol_changeTableCol" + lclOp.toString() + "," + extOp.toString() + "OP ERROR: table name already used");
            }

            // do not interfere with multi-column sort operation
            if(lclOp.optBoolean(OCKey.SORT.value(), false) || extOp.optBoolean(OCKey.SORT.value(), false)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeTableCol_changeTableCol" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: multi-column sort operation in table");
            }

            // transform the formatting attributes and other properties
            OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true), new OTUtils.RemoveEmptyAttrsOptions(true)));
        }
        return null;
    }

    // insertDVRule -----------------------------------------------------------

    private static JSONObject transform_insertDVRule_insertDVRule(TransformOptions options, JSONObject lclOp, JSONObject extOp) throws JSONException {

        // rules in different sheets are independent
        if(!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(lclOp, extOp);
            if(ranges.getLeft().overlaps(ranges.getRight())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertDVRule_insertDVRule" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
        else {

            // rule ranges must not overlap
            ensureUniqueTargetRanges(lclOp, extOp);

            // first, shift local index away (external rule index wins)
            lclOp.put(OCKey.INDEX.value(), OTUtils.transformIndexInsert(lclOp.getInt(OCKey.INDEX.value()), extOp.getInt(OCKey.INDEX.value()), 1));
            // transform external operation with the new local insertion index
            extOp.put(OCKey.INDEX.value(), OTUtils.transformIndexInsert(extOp.getInt(OCKey.INDEX.value()), lclOp.getInt(OCKey.INDEX.value()), 1));
        }
        return null;
    }

    private static JSONObject transform_insertDVRule_deleteDVRule(TransformOptions options, JSONObject insertOp, JSONObject deleteOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(insertOp, deleteOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(insertOp, deleteOp);
            if(ranges.getLeft().overlaps(ranges.getRight())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertDVRule_insertDVRule" + insertOp.toString() + "," + deleteOp.toString() + "NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
        else {
            // first, shift away delete position according to insert position (new rule cannot collide with existing rule)
            deleteOp.put(OCKey.INDEX.value(), OTUtils.transformIndexInsert(deleteOp.getInt(OCKey.INDEX.value()), insertOp.getInt(OCKey.INDEX.value()), 1));
            // transform insert operation with the new deletion index (will not collide, see above)
            insertOp.put(OCKey.INDEX.value(), OTUtils.transformIndexDelete(insertOp.getInt(OCKey.INDEX.value()), deleteOp.getInt(OCKey.INDEX.value()), 1, true));
        }
        return null;
    }

    private static JSONObject transform_insertDVRule_changeDVRule(TransformOptions options, JSONObject insertOp, JSONObject changeOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(insertOp, changeOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(insertOp, changeOp);
            if(ranges.getLeft().overlaps(ranges.getRight())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertDVRule_insertDVRule" + insertOp.toString() + "," + changeOp.toString() + "NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
        else {
            // rule ranges must not overlap
            ensureUniqueTargetRanges(insertOp, changeOp);

            // transform change operation with insert index
            changeOp.put(OCKey.INDEX.value(), OTUtils.transformIndexInsert(changeOp.getInt(OCKey.INDEX.value()), insertOp.getInt(OCKey.INDEX.value()), 1));
        }
        return null;
    }

    // deleteDVRule -----------------------------------------------------------

    private static JSONObject transform_deleteDVRule_deleteDVRule(TransformOptions options, JSONObject lclOp, JSONObject extOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(lclOp, extOp);
            if(ranges.getLeft().equals(ranges.getRight())) {
                OTUtils.setOperationRemoved(lclOp);
                OTUtils.setOperationRemoved(extOp);
            }
        }
        else {
            // set both operations to "removed" state, if they delete the same rule
            final int lclIdx = lclOp.getInt(OCKey.INDEX.value());
            final int extIdx = extOp.getInt(OCKey.INDEX.value());
            if (lclIdx == extIdx) {
                OTUtils.setOperationRemoved(lclOp);
                OTUtils.setOperationRemoved(extOp);
                return null;
            }

            // transform both indexes with the original (untransformed) indexes
            extOp.put(OCKey.INDEX.value(), OTUtils.transformIndexDelete(extOp.getInt(OCKey.INDEX.value()), lclOp.getInt(OCKey.INDEX.value()), 1, true));
            lclOp.put(OCKey.INDEX.value(), OTUtils.transformIndexDelete(lclOp.getInt(OCKey.INDEX.value()), extOp.getInt(OCKey.INDEX.value()), 1, true));
        }
        return null;
    }

    private static JSONObject transform_deleteDVRule_changeDVRule(TransformOptions options, JSONObject deleteOp, JSONObject changeOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(deleteOp, changeOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(deleteOp, changeOp);
            if(ranges.getLeft().equals(ranges.getRight())) {
                OTUtils.setOperationRemoved(changeOp);
            }
            else if(ranges.getLeft().overlaps(ranges.getRight())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_deleteDVRule_changeDVRule" + deleteOp.toString() + "," + changeOp.toString() + "NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
        else {
            // transform the index of the change operation
            final Integer index = OTUtils.transformIndexDelete(changeOp.getInt(OCKey.INDEX.value()), deleteOp.getInt(OCKey.INDEX.value()), 1, false);
            if(index!=null) {
                changeOp.put(OCKey.INDEX.value(), index);
            }
            else {
                OTUtils.setOperationRemoved(changeOp);
            }
        }
        return null;
    }

    // changeDVRule -----------------------------------------------------------

    final static String[] DVRULE_VAL_PROPS_KEYS = { OCKey.TYPE.value(), OCKey.COMPARE.value(), OCKey.VALUE1.value(), OCKey.VALUE2.value() };

    final static String[] DVRULE_PROPS_KEYS = { OCKey.RANGES.value(), OCKey.TYPE.value(), OCKey.COMPARE.value(), OCKey.VALUE1.value(), OCKey.VALUE2.value(), OCKey.SHOW_INFO.value(), OCKey.INFO_TITLE.value(), OCKey.INFO_TEXT.value(), OCKey.SHOW_ERROR.value(), OCKey.ERROR_TITLE.value(), OCKey.ERROR_TEXT.value(), OCKey.ERROR_TYPE.value(), OCKey.SHOW_DROP_DOWN.value(), OCKey.IGNORE_EMPTY.value() };

    private static JSONObject transform_changeDVRule_changeDVRule(TransformOptions options, JSONObject lclOp, JSONObject extOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        if(options.isOdf()) {
            final Pair<CellRefRangeArray, CellRefRangeArray> ranges = ensureRanges(lclOp, extOp);
            if(ranges.getLeft().overlaps(ranges.getRight())) {
                return null;
            }
            if(!ranges.getLeft().equals(ranges.getRight())) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeDVRule_changeDVRule" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: partially overlapping DV rules");
            }
        }
        else {
            // ranges of different rules must not overlap
            if (lclOp.getInt(OCKey.INDEX.value()) != extOp.getInt(OCKey.INDEX.value())) {
                ensureUniqueTargetRanges(lclOp, extOp);
                return null;
            }
        }

        // reduce operation properties
        OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), DVRULE_PROPS_KEYS);

        // TODO: change rule properties to single opaque "rule" property, handle "ref" property
        boolean hasLclVals = hasKey(lclOp, DVRULE_VAL_PROPS_KEYS);
        boolean hasExtVals = hasKey(extOp, DVRULE_VAL_PROPS_KEYS);
        if(hasLclVals || hasExtVals) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_changeDVRule_changeDVRule" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot partially change type/value settings of data validation rule");
        }
        // set no-ops to "removed" state
        checkChangeRuleNoOp(lclOp, DVRULE_PROPS_KEYS);
        checkChangeRuleNoOp(extOp, DVRULE_PROPS_KEYS);
        return null;
    }

    private static boolean hasKey(JSONObject o, String[] props) {
        for(String prop:props) {
            if(o.has(prop)) {
                return true;
            }
        }
        return false;
    }

    private static void checkChangeRuleNoOp(JSONObject changeOp, String[] props) throws JSONException {
        if(!changeOp.has(OCKey.REF.value())) {
            for(String key:props) {
                if(changeOp.has(key)) {
                    return;
                }
            }
            OTUtils.setOperationRemoved(changeOp);
        }
    }

    // insertCFRule -----------------------------------------------------------

    private static JSONObject transform_insertCFRule_insertCFRule(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // rules in different sheets are independent
        if (lclOp.getInt(OCKey.SHEET.value()) != extOp.getInt(OCKey.SHEET.value())) {
            return null;
        }

        // rule ranges must not overlap
        if (!lclOp.getString(OCKey.ID.value()).equals(extOp.getString(OCKey.ID.value()))) {
            return null;
        }

        // same rule identifier: convert operations to "changeCFRule" and reduce properties
        lclOp.put(OCKey.NAME.value(), OCValue.CHANGE_CF_RULE.value());
        extOp.put(OCKey.NAME.value(), OCValue.CHANGE_CF_RULE.value());

        return transform_changeCFRule_changeCFRule(lclOp, extOp);
    }

    private static JSONObject transform_insertCFRule_deleteCFRule(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // should not happen in real-life: preceding insert collision was handled in "insertCFRule" self transformation
        if(OTUtils.isSameCFRule(insertOp, deleteOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertCFRule_deleteCFRule" + insertOp.toString() + "," + deleteOp.toString() + "OP ERROR: cannot insert an existing CF rule");
        }
        return null;
    }

    private static JSONObject transform_insertCFRule_changeCFRule(JSONObject insertOp, JSONObject changeOp) throws JSONException {

        // should not happen in real-life: preceding insert collision was handled in "insertCFRule" self transformation
        if(OTUtils.isSameCFRule(insertOp, changeOp)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertCFRule_changeCFRule" + insertOp.toString() + "," + changeOp.toString() + "OP ERROR: cannot insert an existing CF rule");
        }

        return null;
    }

    // deleteCFRule -----------------------------------------------------------

    private static JSONObject transform_deleteCFRule_deleteCFRule(JSONObject lclOp, JSONObject extOp) throws JSONException {

        if (OTUtils.isSameCFRule(lclOp, extOp)) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }
        return null;
    }

    private static JSONObject transform_deleteCFRule_changeCFRule(JSONObject deleteOp, JSONObject changeOp) throws JSONException {

        if (OTUtils.isSameCFRule(deleteOp, changeOp)) {
            OTUtils.setOperationRemoved(changeOp);
        }
        return null;
    }

    // changeCFRule -----------------------------------------------------------

//    final static String[] CFRULE_VAL_PROPS = { "type", "value1", "value2", "colorScale", "dataBar", "iconSet" };
//
//    final static String[] CFRULE_PROPS = { "ranges", "type", "value1", "value2", "colorScale", "dataBar", "iconSet", "priority", "stop", "attrs" };

    final static String[] CFRULE_VAL_PROPS_KEYS = { OCKey.TYPE.value(), OCKey.VALUE1.value(), OCKey.VALUE2.value(), OCKey.COLOR_SCALE.value(), OCKey.DATA_BAR.value(), OCKey.ICON_SET.value() };

    final static String[] CFRULE_PROPS_KEYS = { OCKey.RANGES.value(), OCKey.TYPE.value(), OCKey.VALUE1.value(), OCKey.VALUE2.value(), OCKey.COLOR_SCALE.value(), OCKey.DATA_BAR.value(), OCKey.ICON_SET.value(), OCKey.PRIORITY.value(), OCKey.STOP.value(), OCKey.ATTRS.value() };

    private static JSONObject transform_changeCFRule_changeCFRule(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // rules in different sheets are independent
        if (!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        // ranges of different rules must not overlap
        if (!lclOp.getString(OCKey.ID.value()).equals(extOp.getString(OCKey.ID.value()))) {
            return null;
        }

        // reduce operation properties
        OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), CFRULE_PROPS_KEYS);

        // TODO: change rule properties to single opaque "rule" property, handle "ref" property
        final boolean hasLclVals = hasKey(lclOp, CFRULE_VAL_PROPS_KEYS);
        final boolean hasExtVals = hasKey(extOp, CFRULE_VAL_PROPS_KEYS);
        if(hasLclVals || hasExtVals) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertCFRule_deleteCFRule" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot partially change type/value settings of conditional formatting rule");
        }

        // set no-ops to "removed" state
        checkChangeRuleNoOp(lclOp, CFRULE_PROPS_KEYS);
        checkChangeRuleNoOp(extOp, CFRULE_PROPS_KEYS);
        return null;
    }

    // insertNote -------------------------------------------------------------

    private static JSONObject transform_insertNote_insertNote(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if(!(OTUtils.isSameAnchor(lclOp, extOp) && OTUtils.isSameSheet(lclOp, extOp))) {
            return null;
        }

        lclOp.put(OCKey.NAME.value(), OCValue.CHANGE_NOTE.value());
        extOp.put(OCKey.NAME.value(), OCValue.CHANGE_NOTE.value());

        transform_changeNote_changeNote(lclOp, extOp);

        return null;
    }

    private static JSONObject transform_insertNote_deleteNote(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        if(OTUtils.isSameAnchor(insertOp, deleteOp) && OTUtils.isSameSheet(insertOp, deleteOp)) {
            // OP ERROR: cannot insert a note twice into the same cell
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertNote_deleteNote" + insertOp.toString() + "," + deleteOp.toString());
        }

        return null;
    }

    private static JSONObject transform_insertNote_changeNote(JSONObject insertOp, JSONObject changeOp) throws JSONException {
        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        if(OTUtils.isSameAnchor(insertOp, changeOp) && OTUtils.isSameSheet(insertOp, changeOp)) {
            // OP ERROR: cannot insert a note twice into the same cell
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertNote_changeNote" + insertOp.toString() + "," + changeOp.toString());
        }

        return null;
    }

    private static JSONObject transform_insertNote_moveNotes(JSONObject insertOp, JSONObject moveOp) throws JSONException {
        // notes in different sheets are independent
        if (!OTUtils.isSameSheet(insertOp, moveOp)) {
            return null;
        }

        CellRef anchor = CellRef.createCellRef(insertOp.getString(OCKey.ANCHOR.value()));
        CellRefArray anchorFrom = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        CellRefArray anchorTo = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        // should not happen in real-life: preceding insert collision was handled in "insertNote" self transformation
        if (anchorFrom.contains(anchor)) {
            // OP ERROR: cannot insert a note twice into the same cell
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertNote_moveNotes" + insertOp.toString() + "," + moveOp.toString());
        }

        // note moved into the cell of the new note: delete external note before sending locally inserted note, ignore external move locally
        final int index = anchorTo.indexOf(anchor);
        if (index >= 0) {
            anchorFrom.remove(index);
            anchorTo.remove(index);
            assignMoveAnchors(moveOp, anchorFrom, anchorTo);
            final JSONArray localOpsBefore = new JSONArray(1);
            final JSONObject deleteNote = new JSONObject(3);
            deleteNote.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
            deleteNote.put(OCKey.SHEET.value(), insertOp.get(OCKey.SHEET.value()));
            deleteNote.put(OCKey.ANCHOR.value(), insertOp.get(OCKey.ANCHOR.value()));
            localOpsBefore.put(deleteNote);
            return JSONHelper.getResultObject(null, null, localOpsBefore, null);
        }

        return null;
    }

    private static JSONObject transform_insertNote_insertComment(JSONObject noteOp, JSONObject commentOp) throws JSONException {
        // different comments are independent
        if(!(OTUtils.isSameAnchor(noteOp, commentOp) && OTUtils.isSameSheet(noteOp, commentOp))) {
            return null;
        }

        OTUtils.setOperationRemoved(noteOp);

        final JSONArray externalOpsBefore = new JSONArray(1);
        final JSONObject deleteNote = new JSONObject(3);
        deleteNote.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
        deleteNote.put(OCKey.SHEET.value(), noteOp.get(OCKey.SHEET.value()));
        deleteNote.put(OCKey.ANCHOR.value(), noteOp.get(OCKey.ANCHOR.value()));
        externalOpsBefore.put(deleteNote);

        return JSONHelper.getResultObject(externalOpsBefore, null, null, null);
    }

    private static JSONObject transform_insertNote_moveComments(JSONObject insertOp, JSONObject moveOp) throws JSONException {
        // different comments are independent
        if(!OTUtils.isSameSheet(insertOp, moveOp)) {
            return null;
        }

        CellRef anchor = CellRef.createCellRef(insertOp.getString(OCKey.ANCHOR.value()));
        CellRefArray anchorFrom = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        CellRefArray anchorTo = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        // should not happen in real-life: preceding insert collision was handled in "insertComment" transformation
        if (anchorFrom.contains(anchor)) {
            // OP ERROR: cannot insert a note into a comment cell
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_insertNote_moveComments" + insertOp.toString() + "," + moveOp.toString());
        }

        final int index = anchorTo.indexOf(anchor);
        if (index >= 0) {
            OTUtils.setOperationRemoved(insertOp);
            final JSONArray externalOpsBefore = new JSONArray(1);
            final JSONObject deleteNote = new JSONObject(3);
            deleteNote.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
            deleteNote.put(OCKey.SHEET.value(), insertOp.get(OCKey.SHEET.value()));
            deleteNote.put(OCKey.ANCHOR.value(), insertOp.get(OCKey.ANCHOR.value()));
            externalOpsBefore.put(deleteNote);
            return JSONHelper.getResultObject(externalOpsBefore, null, null, null);
        }

        return null;
    }

    // deleteNote -------------------------------------------------------------

    private static JSONObject transform_deleteNote_deleteNote(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if(OTUtils.isSameAnchor(lclOp, extOp) && OTUtils.isSameSheet(lclOp, extOp)) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }

    private static JSONObject transform_deleteNote_changeNote(JSONObject deleteOp, JSONObject changeOp) throws JSONException {
        if(OTUtils.isSameAnchor(deleteOp, changeOp) && OTUtils.isSameSheet(deleteOp, changeOp)) {
            OTUtils.setOperationRemoved(changeOp);
        }

        return null;
    }

    private static JSONObject transform_deleteNote_moveNotes(JSONObject deleteOp, JSONObject moveOp) throws JSONException {
        // notes/comments in different sheets are independent
        String sheetChange = deleteOp.optString(OCKey.SHEET.value());
        String sheetMove = moveOp.optString(OCKey.SHEET.value());
        if (!sheetChange.equals(sheetMove)) {
            return null;
        }

        CellRef anchorDelete = CellRef.createCellRef(deleteOp.getString(OCKey.ANCHOR.value()));
        CellRefArray anchorFrom = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        CellRefArray anchorTo = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        int index = anchorFrom.indexOf(anchorDelete);
        if (index >= 0) {
            CellRef newRef = anchorTo.get(index);
            deleteOp.put(OCKey.ANCHOR.value(), CellRef.getCellRef(newRef));
            anchorFrom.remove(index);
            anchorTo.remove(index);
            assignMoveAnchors(moveOp, anchorFrom, anchorTo);
            return null;
        }

        // fail when trying to move a note over another note (with transformed `anchor`)
        if (anchorTo.contains(anchorDelete)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_deleteNote_moveNotes" + deleteOp.toString() + "," + moveOp.toString());
        }

        return null;
    }

    // changeNote -------------------------------------------------------------

    private static JSONObject transform_changeNote_changeNote(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different notes are independent
        if(!(OTUtils.isSameAnchor(lclOp, extOp) && OTUtils.isSameSheet(lclOp, extOp))) {
            return null;
        }

        // transform the formatting attributes and note text
        OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true)));
        // Short names from OCKeys mentions and done is null.
        OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), "w");

        //if (!changeOp.attrs && !("text" in changeOp)) { setOperationRemoved(changeOp); }
        if (!lclOp.has(OCKey.ATTRS.value()) && !lclOp.has("w")) {
            OTUtils.setOperationRemoved(lclOp);
        }

        if (!extOp.has(OCKey.ATTRS.value()) && !extOp.has("w")) {
            OTUtils.setOperationRemoved(extOp);
        }

        return null;
    }

    private static JSONObject transform_changeNote_moveNotes(JSONObject changeOp, JSONObject moveOp) throws JSONException {
        // notes/comments in different sheets are independent
        String sheetChange = changeOp.optString(OCKey.SHEET.value());
        String sheetMove = moveOp.optString(OCKey.SHEET.value());
        if (!sheetChange.equals(sheetMove)) {
            return null;
        }

        CellRef anchorChange = CellRef.createCellRef(changeOp.getString(OCKey.ANCHOR.value()));
        CellRefArray anchorMoveFrom = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        CellRefArray anchorMoveTo = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        int index = anchorMoveFrom.indexOf(anchorChange);
        if (index >= 0) {
            CellRef newRef = anchorMoveTo.get(index);
            changeOp.put(OCKey.ANCHOR.value(), CellRef.getCellRef(newRef));
        }

        return null;
    }

    // moveNotes --------------------------------------------------------------

    private static JSONObject transform_moveNotes_moveNotes(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different comments are independent
        if(!OTUtils.isSameSheet(lclOp, extOp)) {
            return null;
        }

        CellRefArray lclFromAnchors = CellRefArray.createCellRefArray(lclOp.getString(OCKey.FROM.value()));
        CellRefArray lclToAnchors = CellRefArray.createCellRefArray(lclOp.getString(OCKey.TO.value()));
        CellRefArray extFromAnchors = CellRefArray.createCellRefArray(extOp.getString(OCKey.FROM.value()));
        CellRefArray extToAnchors = CellRefArray.createCellRefArray(extOp.getString(OCKey.TO.value()));

        // process all local note moves
        int length = lclFromAnchors.size()-1;
        for (int i=length; i>=0; --i) {
            CellRef lclFrom = lclFromAnchors.get(i);
            CellRef lclTo = lclToAnchors.get(i);

            // handle moving same note in both oerations (ignore both if it is the same target)
            int extIdx = extFromAnchors.indexOf(lclFrom);
            if (extIdx >= 0) {
                CellRef extTo = extToAnchors.get(extIdx);
                if (lclTo.equals(extTo)) {
                    lclFromAnchors.remove(i);
                    lclToAnchors.remove(i);
                } else {
                    lclFromAnchors.set(i, extTo);
                }
                extFromAnchors.remove(extIdx);
                extToAnchors.remove(extIdx);

                break;
            }

            // handle moving two notes to the same target address (local move wins, needs to overrule external move)
            extIdx = extToAnchors.indexOf(lclTo);
            if (extIdx >= 0) {
                lclFromAnchors.add(i + 1, extToAnchors.get(extIdx));
                lclToAnchors.add(i + 1, extFromAnchors.get(extIdx));
                extFromAnchors.remove(extIdx);
                extToAnchors.remove(extIdx);
            }
        }

        assignMoveAnchors(lclOp, lclFromAnchors, lclToAnchors);
        assignMoveAnchors(extOp, extFromAnchors, extToAnchors);

        return null;
    }

    private static JSONObject transform_moveNotes_insertComment(JSONObject moveOp, JSONObject insertOp) throws JSONException {
        // different comments are independent
        if(!OTUtils.isSameSheet(moveOp, insertOp)) {
            return null;
        }

        // parse cell anchors of all note operations
        CellRef anchor = CellRef.createCellRef(insertOp.getString(OCKey.ANCHOR.value()));
        CellRefArray anchorFrom = CellRefArray.createCellRefArray(moveOp.getString(OCKey.FROM.value()));
        CellRefArray anchorTo = CellRefArray.createCellRefArray(moveOp.getString(OCKey.TO.value()));

        // should not happen in real-life: preceding insert collision was handled in "insertNote" transformation
        if (anchorFrom.contains(anchor)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "transform_moveNotes_insertComment" + insertOp.toString() + "," + moveOp.toString());
        }

        // note moved into the cell of the new comment: delete note before creating the comment
        int index = anchorTo.indexOf(anchor);
        if (index >= 0) {
            anchorFrom.remove(index);
            anchorTo.remove(index);
            assignMoveAnchors(moveOp, anchorFrom, anchorTo);
            final JSONArray externalOpsBefore = new JSONArray(1);
            final JSONObject deleteNote = new JSONObject(3);
            deleteNote.put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value());
            deleteNote.put(OCKey.SHEET.value(), insertOp.get(OCKey.SHEET.value()));
            deleteNote.put(OCKey.ANCHOR.value(), insertOp.get(OCKey.ANCHOR.value()));
            externalOpsBefore.put(deleteNote);

            return JSONHelper.getResultObject(externalOpsBefore, null, null, null);
        }

        return null;
    }

    // insertComment ----------------------------------------------------------

    private static JSONObject transform_insertComment_insertComment(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different comments are independent
        if(!(OTUtils.isSameAnchor(lclOp, extOp) && OTUtils.isSameSheet(lclOp, extOp))) {
            return null;
        }

        // first, shift local index away (external comment index wins)
        lclOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexInsert(
                lclOp.optInt(OCKey.INDEX.value()),
                extOp.optInt(OCKey.INDEX.value()), 1));

        // transform external operation with the new local insertion index
        extOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexInsert(
                extOp.optInt(OCKey.INDEX.value()),
                lclOp.optInt(OCKey.INDEX.value()), 1));

        return null;
    }

    private static JSONObject transform_insertComment_deleteComment(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // different comments are independent
        if(!(OTUtils.isSameAnchor(insertOp, deleteOp) && OTUtils.isSameSheet(insertOp, deleteOp))) {
            return null;
        }

        // ignore insert operations, if the entire thread will be deleted
        if (0 == deleteOp.optInt(OCKey.INDEX.value())) {
            OTUtils.setOperationRemoved(insertOp);
            return null;
        }

        // first, shift away delete position according to insert position (new comment cannot collide with existing comment)
        deleteOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexInsert(
                deleteOp.optInt(OCKey.INDEX.value()),
                insertOp.optInt(OCKey.INDEX.value()), 1));

        insertOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexDelete(
                insertOp.optInt(OCKey.INDEX.value()),
                deleteOp.optInt(OCKey.INDEX.value()), 1, true));

        return null;
    }

    private static JSONObject transform_insertComment_changeComment(JSONObject insertOp, JSONObject changeOp) throws JSONException {
        // different comments are independent
        if(!(OTUtils.isSameAnchor(insertOp, changeOp) && OTUtils.isSameSheet(insertOp, changeOp))) {
            return null;
        }

        // transform change operation with insert index
        //  changeOp.index = transformIndexInsert(changeOp.index, insertOp.index, 1);
        changeOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexInsert(
                changeOp.optInt(OCKey.INDEX.value()),
                insertOp.optInt(OCKey.INDEX.value()), 1));

        return null;
    }

    // deleteComment ----------------------------------------------------------

    private static JSONObject transform_deleteComment_deleteComment(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different comments are independent
        if(!(OTUtils.isSameAnchor(lclOp, extOp) && OTUtils.isSameSheet(lclOp, extOp))) {
            return null;
        }

        // set both operations to "removed" state, if they delete the same comment
        final int lclIndex = lclOp.optInt(OCKey.INDEX.value());
        final int extIndex = extOp.optInt(OCKey.INDEX.value());
        if (lclIndex == extIndex) {
            OTUtils.setOperationRemoved(lclOp);
            OTUtils.setOperationRemoved(extOp);
            return null;
        }

        // if one operation deletes the entire thread, set the other operation to "removed" state
        if (0 == lclIndex) {
            OTUtils.setOperationRemoved(lclOp);
            return null;
        }
        if (0 == extIndex) {
            OTUtils.setOperationRemoved(extOp);
            return null;
        }

        // transform both indexes with the original (untransformed) indexes
        extOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexDelete(
                extOp.optInt(OCKey.INDEX.value()),
                lclOp.optInt(OCKey.INDEX.value()), 1, true));

        // transform both indexes with the original (untransformed) indexes
        lclOp.put(OCKey.INDEX.value(),
            OTUtils.transformIndexDelete(
                lclOp.optInt(OCKey.INDEX.value()),
                extOp.optInt(OCKey.INDEX.value()), 1, true));

        return null;
    }

    private static JSONObject transform_deleteComment_changeComment(JSONObject deleteOp, JSONObject changeOp) throws JSONException {
        if(!(OTUtils.isSameAnchor(deleteOp, changeOp) && OTUtils.isSameSheet(deleteOp, changeOp))) {
            return null;
        }

        if (0 == deleteOp.optInt(OCKey.INDEX.value())) {
            OTUtils.setOperationRemoved(changeOp);
            return null;
        }

        final Integer index = OTUtils.transformIndexDelete(changeOp.optInt(OCKey.INDEX.value()), deleteOp.optInt(OCKey.INDEX.value()), 1, false);
        if (null != index) {
            changeOp.put(OCKey.INDEX.value(), index);
        } else {
            OTUtils.setOperationRemoved(changeOp);
        }

        return null;
    }

    private static JSONObject transform_deleteComment_moveComments(JSONObject deleteOp, JSONObject moveOp) throws JSONException {

        // deleting an entire thread works like delete+move notes (delete the move operation);
        // deleting a single reply works like change+move notes (retain the move operation)
        if (0 == deleteOp.getInt(OCKey.INDEX.value())) {
            transform_deleteNote_moveNotes(deleteOp, moveOp);
        } else {
            transform_changeNote_moveNotes(deleteOp, moveOp);
        }

        return null;
    }

    // changeComment ----------------------------------------------------------

    private static JSONObject transform_changeComment_changeComment(JSONObject lclOp, JSONObject extOp) throws JSONException {
        // different comments or replies are independent
        if (!OTUtils.isSameAnchor(lclOp, extOp) || (lclOp.getInt(OCKey.INDEX.value()) != lclOp.getInt(OCKey.INDEX.value()))) {
            return null;
        }

        // transform the formatting attributes and note text
        OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true)));
        // String[] keys = {"text", "mentions", "done"};
        // Short names from OCKeys mentions and done is null.
        OTUtils.reduceProperties(lclOp, extOp, new OTUtils.ReducePropertyOptions(true), "w", "mentions", "done");

        // ignore the entire operation, if all changes have been discarded
        checkChangeCommentNoOp(lclOp);
        checkChangeCommentNoOp(extOp);

        return null;
    }

    private static void checkChangeCommentNoOp(JSONObject changeOp) throws JSONException {
        if (!changeOp.has(OCKey.ATTRS.value())
            && !changeOp.has(OCKey.TEXT.value())
            && !changeOp.has(OCKey.MENTIONS.value())) {
            OTUtils.setOperationRemoved(changeOp);
        }
    }

    // insertDrawing ----------------------------------------------------------

    private static JSONObject transform_insertDrawing_insertDrawing(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // first, shift local position away (external operation wins)
        transformInsertDrawingPositions(lclOp, extOp.getJSONArray(OCKey.START.value()));
        transformInsertDrawingPositions(extOp, lclOp.getJSONArray(OCKey.START.value()));

        return null;
    }

    private static JSONObject transform_insertDrawing_deleteDrawing(JSONObject insertOp, JSONObject deleteOp) throws JSONException {
        // first, shift away delete position according to insert position (new drawing cannot collide with existing drawing)
        transformInsertDrawingPositions(deleteOp, insertOp.getJSONArray(OCKey.START.value()));
        // transform insert operation with the new deletion index
        transformDeleteDrawingPositions(insertOp, deleteOp.getJSONArray(OCKey.START.value()));

        return null;
    }

    private static JSONObject transform_insertDrawing_moveDrawing(JSONObject insertOp, JSONObject moveOp) throws JSONException {

        // check validity of "moveDrawing" operation, and extract position parts
        final MoveDrawingParts moveParts = parseMoveDrawing(moveOp);
        // array index of the position of the inserted shape
        final int arrIdx = insertOp.getJSONArray(OCKey.START.value()).length() - 1;

        if (arrIdx < moveParts.arrIdx) { // parent shape inserted: transform indexes of move operation
            transformInsertDrawingPositions(moveOp, insertOp.getJSONArray(OCKey.START.value()));
        }
        else if (arrIdx > moveParts.arrIdx) { // embedded shape inserted: transform indexes of delete operation
            transformDrawingPositionsForMove(insertOp, moveParts);
        }
        else if (OTUtils.equalNumberArrays(insertOp.getJSONArray(OCKey.START.value()), moveParts.toPos, arrIdx)) { // shapes on same level: transform indexes if shapes are inside the same parent component
            final OTShiftMoveResult result = OTUtils.transformIndexInsertMove(insertOp.getJSONArray(OCKey.START.value()).getInt(arrIdx), moveParts.getFromIdx(), moveParts.getToIdx());
            insertOp.getJSONArray(OCKey.START.value()).put(arrIdx, result.getShiftIdx());
            assignMoveDrawing(moveOp, arrIdx, result.getMoveResult());
        }
        return null;
    }

    private static JSONObject transform_insertDrawing_drawingPos(JSONObject insertOp, JSONObject drawingOp) throws JSONException {
        transformInsertDrawingPositions(drawingOp, insertOp.getJSONArray(OCKey.START.value()));
        return null;
    }

    // deleteDrawing ----------------------------------------------------------

    private static JSONObject transform_deleteDrawing_deleteDrawing(JSONObject lclOp, JSONObject extOp) throws JSONException {
        final JSONArray origExtPos = OTUtils.cloneJSONArray(extOp.getJSONArray(OCKey.START.value()));
        // transform both positions with the original (untransformed) positions
        transformDeleteDrawingPositions(extOp, lclOp.getJSONArray(OCKey.START.value()));
        transformDeleteDrawingPositions(lclOp, origExtPos);
        return null;
    }

    private static JSONObject transform_deleteDrawing_moveDrawing(JSONObject deleteOp, JSONObject moveOp) throws JSONException {

        // check validity of "moveDrawing" operation, and extract position parts
        final MoveDrawingParts moveParts = parseMoveDrawing(moveOp);
        // array index of the position of the deleted shape
        final int arrIdx = deleteOp.getJSONArray(OCKey.START.value()).length() - 1;

        // parent shape deleted: transform indexes of move operation
        if (arrIdx < moveParts.arrIdx) {
            transformDeleteDrawingPositions(moveOp, deleteOp.getJSONArray(OCKey.START.value()));
        }
        else if (arrIdx > moveParts.arrIdx) { // embedded shape deleted: transform indexes of delete operation
            transformDrawingPositionsForMove(deleteOp, moveParts);
        }
        else if (OTUtils.equalNumberArrays(deleteOp.getJSONArray(OCKey.START.value()), moveParts.toPos, arrIdx)) { // shapes on same level: transform indexes if shapes are inside the same parent component
            final OTShiftMoveResult result = OTUtils.transformIndexDeleteMove(deleteOp.getJSONArray(OCKey.START.value()).getInt(arrIdx), moveParts.getFromIdx(), moveParts.getToIdx());
            deleteOp.getJSONArray(OCKey.START.value()).put(arrIdx, result.getShiftIdx());
            assignMoveDrawing(moveOp, arrIdx, result.getMoveResult());
        }
        return null;
    }

    private static JSONObject transform_deleteDrawing_drawingPos(JSONObject deleteOp, JSONObject drawingOp) throws JSONException {
        transformDeleteDrawingPositions(drawingOp, deleteOp.getJSONArray(OCKey.START.value()));
        return null;
    }

    // changeDrawing ----------------------------------------------------------

    private static JSONObject transform_changeDrawing_changeDrawing(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // reduce both attribute sets, set empty operations to "removed" state
        if (OTUtils.equalNumberArrays(lclOp.getJSONArray(OCKey.START.value()), extOp.getJSONArray(OCKey.START.value()), null)) {
            OTUtils.reduceOperationAttributes(lclOp, extOp, new OTUtils.ReduceOperationAttrsOptions(new OTUtils.ReducePropertyOptions(true), new OTUtils.RemoveEmptyAttrsOptions(true)));
        }
        return null;
    }

    // moveDrawing ------------------------------------------------------------

    private static JSONObject transform_moveDrawing_moveDrawing(JSONObject lclOp, JSONObject extOp) throws JSONException {

        // check validity of "moveDrawing" operations, and extract position parts
        final MoveDrawingParts lclParts = parseMoveDrawing(lclOp);
        final MoveDrawingParts extParts = parseMoveDrawing(extOp);

        // different position level: transform move operation in embedded shape only
        final int arrIdx = lclParts.arrIdx;
        if (arrIdx < extParts.arrIdx) {
            transformDrawingPositionsForMove(extOp, lclParts);
        } else if (arrIdx > extParts.arrIdx) {
            transformDrawingPositionsForMove(lclOp, extParts);
        } else if (OTUtils.equalNumberArrays(lclParts.toPos, extParts.toPos, arrIdx)) {
            // transform and reassign all indexes (either move operation may become a no-op)
            final OTMoveMoveResult result = OTUtils.transformIndexMoveMove(lclParts.getFromIdx(), lclParts.getToIdx(), extParts.getFromIdx(), extParts.getToIdx());
            assignMoveDrawing(lclOp, arrIdx, result.getLclRes());
            assignMoveDrawing(extOp, arrIdx, result.getExtRes());
        }
        return null;
    }

    private static JSONObject transform_moveDrawing_drawingPos(JSONObject moveOp, JSONObject drawingOp) throws JSONException {
        // check validity of "moveDrawing" operation, and extract position parts
        final MoveDrawingParts moveParts = parseMoveDrawing(moveOp);
        // transform all position properties of the drawing operation
        transformDrawingPositionsForMove(drawingOp, moveParts);
        return null;
    }

    /**
     * Assigns the indexes of an `OTMoveResult` structure to a "moveSheet" or a
     * "copySheet" operation.
     * @throws JSONException
     */
    private static void assignMoveSheet(JSONObject sheetOp, OTUtils.OTMoveResult moveRes) throws JSONException {
        if (moveRes!=null) {
            sheetOp.put(OCKey.SHEET.value(), moveRes.getFromIdx());
            sheetOp.put(OCKey.TO.value(), moveRes.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(sheetOp);
        }
    }

    /**
     * Assigns the passed sort vector to a "moveSheets" operation, and checks that
     * the sort vector is not a no-op.
     * @throws JSONException
     */
    private static void assignMoveSheets(JSONObject sortOp, OTSortVector sortVec) throws JSONException {
        if (sortVec!=null) {
            sortOp.put(OCKey.SHEETS.value(), sortVec.createJSONArray());
        }
        else {
            OTUtils.setOperationRemoved(sortOp);
        }
    }

    /**
     * Assigns an interval list to an operation, and checks whether the operation
     * becomes a no-op.
     * @throws JSONException
     */
    private static void assignIntervalList(JSONObject intervalsOp, IntervalArray intervals) throws JSONException {
        if (intervals.isEmpty()) {
            OTUtils.setOperationRemoved(intervalsOp);
        }
        else {
            intervalsOp.put(OCKey.INTERVALS.value(), intervals.toString());
        }
    }

    /**
     * Assigns a cell range list to an operation, and checks whether the operation
     * becomes a no-op.
     * @throws JSONException
     */
    public static void assignRangeList(JSONObject rangesOp, CellRefRangeArray ranges) throws JSONException {
        if (ranges.isEmpty()) {
            OTUtils.setOperationRemoved(rangesOp);
        }
        else {
            rangesOp.put(OCKey.RANGES.value(), ranges.toString());
        }
    }

    /**
     * Assigns the passed address lists to the properties of a "moveNotes" or
     * "moveComments" operation.
     * @throws JSONException
     */
    private static void assignMoveAnchors(JSONObject moveOp, List<CellRef> fromAnchors, List<CellRef> toAnchors) throws JSONException {
        if (fromAnchors.isEmpty() || toAnchors.isEmpty()) {
            OTUtils.setOperationRemoved(moveOp);
        }
        else {
            moveOp.put(OCKey.FROM.value(), CellRef.toString(fromAnchors));
            moveOp.put(OCKey.TO.value(), CellRef.toString(toAnchors));
        }
    }

    /**
     * Inserts a sheet index into all position properties of a drawing operation.
     * @throws JSONException
     */
    private static void assignDrawingSheetIndex(JSONObject drawingOp, int sheet) throws JSONException {
        for(String key: DRAWING_POS_PROPS) {
            final JSONArray pos = drawingOp.optJSONArray(key);
            if (pos!=null) {
                pos.put(0, sheet);
            }
        }
    }

    /**
     * Assigns the indexes of an `OTMoveResult` structure to a specific element in
     * the positions of a "moveDrawing" operation.
     * @throws JSONException
     */
    private static void assignMoveDrawing(JSONObject drawingOp, int arrIdx, OTUtils.OTMoveResult moveRes) throws JSONException {
        if (moveRes!=null) {
            drawingOp.getJSONArray(OCKey.START.value()).put(arrIdx, moveRes.getFromIdx());
            drawingOp.getJSONArray(OCKey.TO.value()).put(arrIdx, moveRes.getToIdx());
        }
        else {
            OTUtils.setOperationRemoved(drawingOp);
        }
    }

    private static String[] DRAWING_POS_PROPS = {OCKey.START.value(), OCKey.END.value(), OCKey.TO.value()};

    private static void transformSheetInDrawingPositions(JSONObject drawingOp, IDrawingPos drawingPos) throws JSONException {
        for(String key: DRAWING_POS_PROPS) {
            final JSONArray pos = drawingOp.optJSONArray(key);
            if (pos!=null) {
                final Integer sheet = drawingPos.transformFn(pos.getInt(0));
                if(sheet!=null) {
                    pos.put(0, sheet);
                }
                else {
                    OTUtils.setOperationRemoved(drawingOp);
                }
            }
        }
    }

    private static void transformInsertDrawingPositions(JSONObject drawingOp, JSONArray insPos) throws JSONException {
        for(String key: DRAWING_POS_PROPS) {
            final JSONArray pos = drawingOp.optJSONArray(key);
            if (pos!=null) {
                OTUtils.transformPositionInsert(pos, insPos, 1);
            }
        }
    }

    private static void transformDeleteDrawingPositions(JSONObject drawingOp, JSONArray delPos) throws JSONException {
        for(String key: DRAWING_POS_PROPS) {
            final JSONArray pos = drawingOp.optJSONArray(key);
            if ((pos!=null) && OTUtils.transformPositionDelete(pos, delPos, 1)==null) {
                OTUtils.setOperationRemoved(drawingOp);
            }
        }
    }

    private static void transformDrawingPositionsForMove(JSONObject drawingOp, MoveDrawingParts moveParts) throws JSONException {
        for(String key: DRAWING_POS_PROPS) {
            final JSONArray pos = drawingOp.optJSONArray(key);
            if (pos!=null) {
                OTUtils.transformPositionMove(pos, moveParts.getFromPos(), 1, moveParts.getToIdx());
            }
        }
    }

    /**
     * Throws an `OTException`, if both operations contain overlapping cell range
     * addresses.
     * @throws OTException
     */
    private static void ensureUniqueTargetRanges(JSONObject lclOp, JSONObject extOp) throws JSONException {
        if (lclOp.has(OCKey.RANGES.value()) && extOp.has(OCKey.RANGES.value())) {
            final CellRefRangeArray lclRanges = CellRefRangeArray.createRanges(lclOp.getString(OCKey.RANGES.value()));
            final CellRefRangeArray extRanges = CellRefRangeArray.createRanges(extOp.getString(OCKey.RANGES.value()));
            if(lclRanges.overlaps(extRanges)) {
                throw new OTException(OTException.Type.RELOAD_REQUIRED, "ensureUniqueTargetRanges" + lclOp.toString() + "," + extOp.toString() + "NOT IMPLEMENTED: cannot insert objects with overlapping target ranges");
            }
        }
    }

    private static void checkChangeSheetNoOp(JSONObject changeOp) throws JSONException {
        if (!changeOp.has(OCKey.ATTRS.value()) && !changeOp.has(OCKey.SHEET_NAME.value())) {
            OTUtils.setOperationRemoved(changeOp);
        }
    }

    /**
     * Parses the target range addresses of both DV rule operations.
     */
    private static Pair<CellRefRangeArray, CellRefRangeArray> ensureRanges(JSONObject lclOp, JSONObject extOp) {
        final String lclRanges = lclOp.optString(OCKey.RANGES.value(), "");
        final String extRanges = extOp.optString(OCKey.RANGES.value(), "");

        if(lclRanges.isEmpty() || extRanges.isEmpty()) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "ensureRangesThatAreNotOverlapping" + lclOp.toString() + "," + extOp.toString() + "OP ERROR: missing DV target rule ranges");
        }
        return Pair.of(CellRefRangeArray.createRanges(lclRanges), CellRefRangeArray.createRanges(extRanges));
    }

    /**
     * Extracts different parts of a "moveDrawing" operation.
     * @throws JSONException
     */
    private static MoveDrawingParts parseMoveDrawing(JSONObject moveOp) throws JSONException {

        // check validity of the operation
        final JSONArray fromPos = moveOp.getJSONArray(OCKey.START.value());
        final JSONArray toPos = moveOp.getJSONArray(OCKey.TO.value());

        final int arrIdx = fromPos.length() - 1;
        if(arrIdx < 1) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "parseMoveDrawing" + moveOp.toString() + "OP ERROR: start position in \"moveDrawing\" operation too short");
        }
        if((arrIdx != toPos.length() - 1) || !OTUtils.equalNumberArrays(fromPos, toPos, arrIdx)) {
            throw new OTException(OTException.Type.RELOAD_REQUIRED, "parseMoveDrawing" + moveOp.toString() + "OP ERROR: \"moveDrawing\" target position needs same parent");
        }

        // return the position parts
        return new MoveDrawingParts(fromPos.getInt(arrIdx), toPos.getInt(arrIdx), fromPos.getInt(0), fromPos, toPos, arrIdx);
    }

    /**
     * Parsed properties of a valid "moveDrawing" operation.
     */
    public static class MoveDrawingParts extends OTUtils.OTMoveResult {
        final int sheetIdx;
        final JSONArray fromPos;
        final JSONArray toPos;
        final int arrIdx;

        public MoveDrawingParts(int fromIdx, int toIdx, int sheetIdx, JSONArray fromPos, JSONArray toPos, int arrIdx) {
            super(fromIdx, toIdx);

            this.sheetIdx = sheetIdx;
            this.fromPos = fromPos;
            this.toPos = toPos;
            this.arrIdx = arrIdx;
        }

        public int getSheetIdx() {
            return sheetIdx;
        }

        public JSONArray getFromPos() {
            return fromPos;
        }

        public JSONArray getToPos() {
            return toPos;
        }

        public int getArrIdx() {
            return arrIdx;
        }
    }
}

