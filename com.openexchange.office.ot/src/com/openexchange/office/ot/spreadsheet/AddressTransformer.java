/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefArray;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.CellRefRangeArray;
import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.filter.core.spreadsheet.IntervalArray;

public class AddressTransformer {

    // properties -------------------------------------------------------------

    /**
     * The index interval in the crossing direction that restricts the moved
     * interval, that can be used to convert the target intervals to cell range
     * addresses.
     */
    final public Interval bandInterval;

    /**
     * The sorted, shifted and shortened (insertion mode), and merged (deletion
     * mode) move intervals, that represent the new inserted index intervals,
     * or the still existing index intervals to be deleted.
     */
    final public IntervalArray targetIntervals;

    /**
     * The move intervals needed to exactly reverse the operation. In insertion
     * mode, this property equals `targetIntervals` needed to delete the
     * (already shifted) insertion intervals. In deletion mode, this property
     * contains the pack-pulled target intervals (to compensate that the target
     * intervals have been shifted forwards).
     */
    final public IntervalArray reverseIntervals;

    /**
     * The index intervals containing the source position of the cells before
     * they will be moved towards the end (insertion), or to the beginning
     * (deletion) of the sheet.
     */
    public IntervalArray moveFromIntervals = new IntervalArray();

    /**
     * The index intervals containing the final position of the cells after
     * they have been moved towards the end (insertion), or to the beginning
     * (deletion) of the sheet. The intervals in this array will have the same
     * size as the corresponding original intervals from `moveFromIntervals`.
     */
    public IntervalArray moveToIntervals = new IntervalArray();

    /**
     * The index intervals containing the position of all entries that will be
     * deleted, either trailing entries that will be shifted outside the sheet
     * on insertion, or the target intervals themselved on deletion.
     */
    final public IntervalArray deleteIntervals;

    /**
     * The address of the cell range that will be modified by the move
     * operation represented by this transformer. Includes all cells that will
     * be moved, inserted, or deleted.
     */
    final public CellRefRange dirtyRange;

    /**
     * The maximum column/row index allowed in the specified move direction.
     */
    final public int maxIndex;

    final public int maxCol;
    final public int maxRow;

    /**
     * If `true`, the cells will be moved through columns (i.e. to the left or
     * right), otherwise through rows (i.e. up or down).
     */
    final public boolean columns;

    /**
     * If set to `true`, the cells will be moved to the end of the sheet (new
     * blank cells will be inserted), otherwise to the beginning of the sheet
     * (existing cells will be deleted).
     */
    final public boolean insert;


    public AddressTransformer(AddressFactory addressFactory, Interval bandInterval, IntervalArray moveIntervals, Direction direction) {

        this.bandInterval = bandInterval;

        columns = !direction.isVerticalDir();
        insert = !direction.isLeadingDir();
        maxIndex = addressFactory.getMaxIndex(columns);
        maxCol = addressFactory.getMaxCol();
        maxRow = addressFactory.getMaxRow();

        // preparations for insertion or deletion mode
        if (insert) {
            Interval boundInterval = Interval.createInterval(false, 0, maxIndex);// new Interval(0, maxIndex);
            // shift the following intervals to the end, according to the size of the preceding insertion intervals,
            // shorten or delete the trailing intervals that will be shifted outside the maximum column/row index
            final IntervalArray source = moveIntervals.deepClone().sort();

            int insertOffset = 0;
            targetIntervals = new IntervalArray();
            for(Interval interval:source) {
                final Interval targetInterval = interval.deepClone().move(insertOffset).intersect(boundInterval);
                insertOffset += interval.size();
                if(targetInterval!=null) {
                    targetIntervals.add(targetInterval);
                }
            }
            reverseIntervals = targetIntervals;
            // the trailing indexes at the end of the sheet will be shifted outside (a single index interval)
            deleteIntervals = new IntervalArray(Interval.createInterval(columns, maxIndex - targetIntervals.size() + 1, maxIndex));
        }
        else {
            // deletion mode: merge the intervals (the result array will be sorted)
            deleteIntervals = targetIntervals = moveIntervals.deepClone().merge();

            // the reverse intervals (for insertion) need to be pulled back
            int deleteOffset = 0;
            reverseIntervals = new IntervalArray();
            for(Interval interval:targetIntervals) {
                final Interval undoInterval = interval.deepClone().move(deleteOffset);
                deleteOffset -= interval.size();
                reverseIntervals.add(undoInterval);
            }
        }
        // calculate the move intervals between the resulting target intervals
        final IntervalArray shiftedIntervals = insert ? moveToIntervals : moveFromIntervals;
        final IntervalArray originalIntervals = insert ? moveFromIntervals : moveToIntervals;

        int moveOffset = 0;
        for(int i=0; i < targetIntervals.size(); i++) {
            final Interval targetInterval = targetIntervals.get(i);
            // exit if the outer border of the sheet has been reached
            if(targetInterval.getMax().getValue() >= maxIndex) {
                break;
            }
            // the interval between the current and next target interval
            final Interval nextInterval = (i + 1 < targetIntervals.size()) ? targetIntervals.get(i + 1) : null;
            final Interval moveInterval = Interval.createInterval(columns,targetInterval.getMax().getValue() + 1, nextInterval!=null ? nextInterval.getMin().getValue() - 1 : maxIndex);
            // the interval is the position after moving (insertion mode); or before moving (deletion mode)
            shiftedIntervals.add(moveInterval);

            // move the interval back by the total size of preceding target intervals
            moveOffset -= targetInterval.size();
            originalIntervals.add(moveInterval.deepClone().move(moveOffset));
        }
        // calculate the dirty range covered by this transformer
        final Interval dirtyInterval = Interval.createInterval(columns, 0, maxIndex);
        if(!targetIntervals.isEmpty()) {
            dirtyInterval.getMin().setValue(targetIntervals.get(0).getMin().getValue());
        }
        dirtyRange = createBandRange(dirtyInterval);
    }

    /**
     * Returns whether the passed cell address is contained in the cell range
     * covered by the band interval of this address transformer.
     *
     * @param address
     *  The cell address to be checked.
     *
     * @returns
     *  Whether the passed cell address is contained in the cell range covered
     *  by the band interval of this address transformer.
     */
    public boolean bandContainsAddress(CellRef address) {
        return bandInterval.containsIndex(address.get(!columns));
    }

    /**
     * Creates an address transformer for one or more index intervals (entire
     * columns or rows in the sheet, suitable e.g. for the document operations
     * "insertColumns", "deleteRows", etc.).
     *
     * @param addressFactory
     *  The address factory used to resolve the maximum column/row index in the
     *  document.
     *
     * @param moveIntervals
     *  The index intervals of the columns or rows to be inserted or deleted in
     *  the sheet.
     *
     * @param direction
     *  The direction to move existing columns/rows to.
     */
    public static AddressTransformer fromIntervals(AddressFactory addressFactory, IntervalArray moveIntervals, Direction direction) {
        final boolean columns = !direction.isVerticalDir();
        return new AddressTransformer(addressFactory, addressFactory.getFullInterval(!columns), moveIntervals, direction);
    }

    /**
     * Creates a cell range address located in the band interval of this
     * address transformer, and covering the specified index interval in that
     * band.
     *
     * Example: If the address transformer represents the row band `1:3` (move
     * cells to the left or right inside these rows), this method will return
     * the cell range address `E1:F3` for the column interval `E:F` passed to
     * it.
     *
     * @param interval
     *  The interval inside the band covered by this address transformer.
     *
     * @returns
     *  The cell range address located in the band interval of this address
     *  transformer, and covering the specified index interval.
     */
    public CellRefRange createBandRange(Interval interval) {
        return columns ? new CellRefRange(interval, bandInterval) : new CellRefRange(bandInterval, interval);
    }

    /**
     * Transforms the passed cell address according to the settings of this
     * address transformer.
     *
     * @param address
     *  The cell address to be transformed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell address. An untransformed cell address will be
     *  cloned. If the cell would be deleted completely while deleting columns
     *  or rows, or would be shifted outside the sheet while inserting columns
     *  or rows, `undefined` will be returned instead (except if `move` mode is
     *  passed in the options).
     */
    // implementation
    public CellRef transformAddress(CellRef address, TransformAddressOptions options) {

        // nothing to do, if the address is not covered by this transformer
        if (!bandContainsAddress(address)) {
            return address.clone();
        }
        final boolean _reverse = options!=null && options.reverse!=null && options.reverse.booleanValue();
        // the effective move operation
        final boolean _insert =  _reverse ? !insert : insert;
        // the effective move intervals
        final IntervalArray moveIntervals = _reverse ? reverseIntervals : targetIntervals;
        // the private method that implements transformation of an address index
        final boolean moveMode = options != null && options.move != null ? options.move.booleanValue() : false;
        final Integer index;
        if(_insert) {
            index = transformIndexInsert(moveIntervals, address.get(columns), maxIndex, moveMode);
        }
        else {
            index = transformIndexDelete(moveIntervals, address.get(columns), moveMode);
        }
        // create a new address with adjusted column/row index, or return undefined to indicate deleted cell
        return index == null ? null : address.clone().set(index, columns);
    }

    /**
     * Transforms the passed cell addresses according to the settings of this
     * address transformer.
     *
     * @param addresses
     *  The cell addresses to be transformed, as array of cell addresses, or as
     *  single cell address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell addresses. Untransformed cell addresses will be
     *  cloned. Cells that would be deleted completely while deleting columns
     *  or rows, or would be shifted outside the sheet while inserting columns
     *  or rows, will not be included into the result.
     */
    public CellRefArray transformAddresses(CellRefArray addresses, TransformAddressOptions options) {
        // automatically filters `undefined` returned from `transformAddress()`
        final CellRefArray result = new CellRefArray(addresses.size());
        for(CellRef address:addresses) {
            final CellRef r = transformAddress(address, options);
            if(r!=null) {
                result.add(r);
            }
        }
        return result;
    }

    public boolean isFullInterval(Interval interval) {
        if(interval.getMin().getValue()!=0) {
            return false;
        }
        return interval.isColumnInterval() ? interval.getMax().getValue() == maxCol : interval.getMax().getValue() == maxRow;
    }

    /**
     * Transforms the passed index intervals according to the settings of this
     * address transformer, but ignores its band interval.
     *
     * @param intervals
     *  The index intervals to be transformed, as array of index intervals, or
     *  as single index interval.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed index intervals. Untransformed index intervals will be
     *  cloned. Index intervals that would be deleted completely while deleting
     *  columns or rows, or would be shifted outside the sheet while inserting
     *  columns or rows, will not be included into the result. With the
     *  insertion mode `SPLIT`, the result array may contain multiple index
     *  intervals for a single source interval.
     */

    public IntervalArray transformIntervals(IntervalArray intervals, TransformIntervalOptions options) {

        final boolean _reverse = options!=null && options.reverse!=null && options.reverse.booleanValue();
        // the effective move operation
        final boolean _insert =  _reverse ? !insert : insert;

        // the effective move intervals
        final IntervalArray _moveIntervals = _reverse ? reverseIntervals : targetIntervals;
        // how to insert/delete intervals inside intervals
        final TransformMode _xfMode = options!=null && options.transformMode!=null ? options.transformMode : TransformMode.RESIZE;
        // the transformed result intervals
        final IntervalArray _resultIntervals = new IntervalArray();
        // transform all intervals in the passed data source
        final ResultFn appendFn = (first, last) -> {
            _resultIntervals.add(Interval.createInterval(columns, first, last));
        };

        for(Interval interval:intervals) {
            if(_insert) {
                AddressTransformer.transformIntervalInsert(_moveIntervals, interval, _xfMode, maxIndex, appendFn);
            }
            else {
                AddressTransformer.transformIntervalDelete(_moveIntervals, interval, _xfMode, maxIndex, appendFn);
            }
        }
        return _resultIntervals;
    }

    interface ResultFn {

        void apply(int first, int last);
    }

    /**
     * Transforms the passed cell range addresses according to the settings of
     * this address transformer.
     *
     * @param ranges
     *  The cell range addresses to be transformed, as array of cell range
     *  addresses, or as single cell range address.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The transformed cell range addresses. Untransformed range addresses
     *  will be cloned. Cell ranges that would be deleted completely while
     *  deleting columns or rows, or would be shifted outside the sheet while
     *  inserting columns or rows, will not be included into the result. With
     *  the insertion mode `SPLIT` or the option `splitPartial`, the result
     *  array may contain multiple cell ranges for a single source range.
     */
    public CellRefRangeArray transformRanges(CellRefRange ranges, TransformRangeOptions options) {
        return transformRanges(new CellRefRangeArray(ranges), options);
    }

    public CellRefRangeArray transformRanges(CellRefRangeArray ranges, TransformRangeOptions options) {

        final boolean _reverse = options!=null && options.reverse!=null && options.reverse.booleanValue();
        // the effective move operation
        final boolean _insert =  _reverse ? !insert : insert;

        // the effective move intervals
        final IntervalArray _moveIntervals = _reverse ? reverseIntervals : targetIntervals;
        // how to insert/delete intervals inside ranges
        final TransformMode _xfMode = options!=null && options.transformMode!=null ? options.transformMode : TransformMode.RESIZE;
        // whether to split partially covered ranges
        final boolean splitPartial = options!=null && options.splitPartial!=null ? options.splitPartial.booleanValue() : false;
        // the transformed result ranges
        final CellRefRangeArray _resultRanges = new CellRefRangeArray();
        // the minimum column/row index that will be transformed
        final int minIndex = (columns ? dirtyRange.getStart().getColumn() : dirtyRange.getStart().getRow()) - ((_insert && (_xfMode == TransformMode.EXPAND)) ? 1 : 0);
        // returns whether the passed move interval covers the entire sheet

        // transform all ranges in the passed data source
        ranges.forEach(range -> {

            // the column/row interval of the range in the band interval
            final Interval _bandInterval = range.createInterval(!columns);

            // the column/row interval of the range to be transformed
            final Interval moveInterval = range.createInterval(columns);
            // whether the range overlaps and/or is contained in the band interval
            final boolean overlaps = bandInterval.overlaps(_bandInterval);
            final boolean contains = overlaps && bandInterval.contains(_bandInterval);

            // nothing to do, if the range is not covered by this transformer;
            // do not transform full column/row intervals
            if (!overlaps || (!contains && !splitPartial) || (moveInterval.getMax().getValue() < minIndex) || isFullInterval(moveInterval)) {
                _resultRanges.add(range.clone());
                return;
            }

            // split partially covered ranges (insert the outer parts of the range's band interval into the result)
            if (!contains) {
                final IntervalArray outerIntervals = new IntervalArray(_bandInterval).difference(this.bandInterval);
                outerIntervals.forEach(bandInt -> {
                    _resultRanges.add(CellRefRange.createFromInterval(bandInt, moveInterval.getMin().getValue(), moveInterval.getMax().getValue()));
                });
                _bandInterval.setValue(_bandInterval.intersect(bandInterval));
            }
            // transform the correct interval of the passed range, according to move direction
            final ResultFn appendFn = (first, last) -> {
                _resultRanges.add(CellRefRange.createFromInterval(_bandInterval, first, last));
            };

            if(_insert) {
                AddressTransformer.transformIntervalInsert(_moveIntervals, moveInterval, _xfMode, maxIndex, appendFn);
            }
            else {
                AddressTransformer.transformIntervalDelete(_moveIntervals, moveInterval, _xfMode, maxIndex, appendFn);
            }
        });

        return _resultRanges;
    }

    /**
     * Transforms a column/row index according to the move intervals in insertion
     * mode.
     *
     * @param moveIntervals
     *  The move intervals used to transform the index.
     *
     * @param xfIndex
     *  The column/row index to be transformed.
     *
     * @param [moveMode]
     *  Whether to keep the index valid (at the maximum index) when it would be
     *  moved off.
     *
     * @returns
     *  The transformed column/row index; or `undefined`, if the index has been
     *  moved beyond the maximum column/row index in the sheet (unless the
     *  `moveMode` flag has been passed).
     */
    public Integer transformIndexInsert(IntervalArray moveIntervals, int xfIndex, int _maxIndex, boolean moveMode) {

        // transform the column/row index as long as it remains valid
        for (int ai = 0, l = moveIntervals.size(); ai < l; ai += 1) {

            // early exit, if the target intervals are located behind the index (array is sorted)
            final Interval moveInterval = moveIntervals.get(ai);
            if (xfIndex < moveInterval.getMin().getValue()) {
                break;
            }
            // shift index ahead
            xfIndex += moveInterval.size();

            // early exit if the index becomes invalid (moved beyond the maximum index)
            if (xfIndex > _maxIndex) {
                return moveMode ? _maxIndex : null;
            }
        }
        return xfIndex;
    }

    /**
     * Transforms a column/row index according to the move intervals in deletion
     * mode.
     *
     * @param moveIntervals
     *  The move intervals used to transform the index.
     *
     * @param xfIndex
     *  The column/row index to be transformed.
     *
     * @param [moveMode]
     *  Whether to keep the index valid (at the interval start index) when it
     *  would be deleted by an interval.
     *
     * @returns
     *  The transformed column/row index; or `undefined`, if the index was part
     *  of a target interval and has therefore been deleted (unless the
     *  `moveMode` flag has been passed).
     */
    public Integer transformIndexDelete(IntervalArray moveIntervals, int xfIndex, boolean moveMode) {

        // transform the column/row index as long as it remains valid (in reversed order!)
        for (int ai = moveIntervals.size() - 1; ai >= 0; ai -= 1) {

            // nothing to do, if the target intervals are located behind the index
            final Interval moveInterval = moveIntervals.get(ai);
            if (xfIndex < moveInterval.getMin().getValue()) {
                continue;
            }
            // transform the index (special handling if it is contained in the target interval)
            if (xfIndex <= moveInterval.getMax().getValue()) {
                if (!moveMode) {
                    return null;
                }
                xfIndex = moveInterval.getMin().getValue();
            }
            else {
                xfIndex -= moveInterval.size();
            }
        }
        return xfIndex;
    }

    /**
     * Transforms an index interval according to the move intervals in insertion
     * mode.
     *
     * @param moveIntervals
     *  The move intervals used to transform the interval.
     *
     * @param xfInterval
     *  The index interval to be transformed.
     *
     * @param xfMode
     *  The resolved transformation mode.
     *
     * @param resultFn
     *  A callback function that will be invoked for each transformed index
     *  interval. May be invoked multiple times, e.g. for `SPLIT` mode.
     */
    public static void transformIntervalInsert(IntervalArray moveIntervals, Interval xfInterval, TransformMode xfMode, int maxIndex, ResultFn resultFn) {

        // the interval indexes to be transformed
        int first = xfInterval.getMin().getValue();
        int last = xfInterval.getMax().getValue();

        // special behavior for specific transformation modes
        final boolean strictLimit = xfMode == TransformMode.STRICT;
        final boolean splitMode = xfMode == TransformMode.SPLIT;

        // predicate function returning whether the interval will be transformed by a target interval
        // - `FIXED` mode: interval will be shifted if target interval starts before
        // - `EXPAND` mode: interval will be expanded if target interval starts directly after
        // - otherwise: interval will be expanded if target interval starts inside
        final IsAffectedFn isAffected;
        if(xfMode == TransformMode.FIXED) {
            isAffected = (interval, f, l) -> {
                return interval.getMin().getValue() <= f;
            };
        }
        else if(xfMode == TransformMode.EXPAND) {
            isAffected = (interval, f, l) -> { return interval.getMin().getValue() <= l + 1; };
        }
        else {
            isAffected = (interval, f, l) -> { return interval.getMin().getValue() <= l; };
        }

        // transform the interval as long as it remains valid
        for (int ai = 0, al = moveIntervals.size(); ai < al; ai += 1) {

            // early exit, if the remaining (sorted) target intervals will not transform the interval anymore
            final Interval moveInterval = moveIntervals.get(ai);
            if (!isAffected.test(moveInterval, first, last)) {
                break;
            }

            // split leading part from interval
            if (splitMode && (first < moveInterval.getMin().getValue())) {
                resultFn.apply(first, moveInterval.getMin().getValue() - 1);
                first = moveInterval.getMin().getValue();
            }

            // shift start position ahead, if the interval starts inside or behind the target interval;
            // exit the loop early if the start index becomes invalid (moved beyond the maximum index)
            final int moveSize = moveInterval.size();
            if (moveInterval.getMin().getValue() <= first) {
                first += moveSize;
                if (first > maxIndex) {
                    return;
                }
            }

            // shift end position ahead (in `EXPAND` mode: also, if it ends exactly before the target
            // interval), keep the end position inside the limits of the sheet (interval may shrink;
            // but in `STRICT` mode, the interval will be dropped instead)
            last += moveSize;
            if (last > maxIndex) {
                if (strictLimit) {
                    return;
                }
                last = maxIndex;
            }
        }

        // invoke the callback function with the new interval
        resultFn.apply(first, last);
    }

    interface IsAffectedFn {

        boolean test(Interval interval, int first, int last);
    }

    /**
     * Transforms an index interval according to the move intervals in deletion
     * mode.
     *
     * @param moveIntervals
     *  The move intervals used to transform the interval.
     *
     * @param xfInterval
     *  The index interval to be transformed.
     *
     * @param xfMode
     *  The resolved transformation mode.
     *
     * @param resultFn
     *  A callback function that will be invoked for each transformed index
     *  interval. Will be invoked exactly once, if the interval has been
     *  transformed; or never, if the interval was covered by a target interval
     *  and has therefore been deleted completely.
     */
    public static void transformIntervalDelete(IntervalArray moveIntervals, Interval xfInterval, TransformMode xfMode, int _maxIndex, ResultFn resultFn) {

        // the interval indexes to be transformed
        int first = xfInterval.getMin().getValue();
        int last = xfInterval.getMax().getValue();

        // special behavior for specific transformation modes
        final boolean fixedMode = xfMode == TransformMode.FIXED;

        // predicate function returning whether the interval will be transformed by a target interval
        // - `FIXED` mode: interval will be shifted if target interval starts before
        // - otherwise: interval will be transformed if target interval starts inside
        final IsAffectedFn isAffected;
        if(fixedMode) {
            isAffected = (interval, f, l) -> { return interval.getMin().getValue() < f; };
        }
        else {
            isAffected = (interval, f, l) -> { return interval.getMin().getValue() <= l; };
        }

        // transform the interval index as long as it remains valid (in reversed order!)
        for (int ai = moveIntervals.size() - 1; ai >= 0; ai -= 1) {

            // nothing to do, if the target intervals are located behind the interval
            final Interval moveInterval = moveIntervals.get(ai);
            if (!isAffected.test(moveInterval, first, last)) {
                continue;
            }

            // shift start position of the interval back to start of target interval
            final int moveSize = moveInterval.size();
            if (moveInterval.getMin().getValue() < first) {
                first = Math.max(moveInterval.getMin().getValue(), first - moveSize);
            }

            // shift end position of the interval back to start of target interval;
            // exit the loop early if the interval becomes invalid
            if (!fixedMode) {
                last = Math.max(last - moveSize, moveInterval.getMin().getValue() - 1);
                if (last < first) {
                    return;
                }
            }
        }
        // invoke the callback function with the new interval
        resultFn.apply(first, fixedMode ? (first + xfInterval.size() - 1) : last);
    }

    /**
     * Generic options for transforming index intervals, cell addresses, and cell
     * range addresses.
     */
    public static class TransformReverseOptions {

        /**
         * If set to `true`, the passed interval, cell address, or cell range will
         * be transformed with the reversed operation. If the address transformer
         * represents an insert operation, the interval, address, or range will be
         * transformed according to a delete operation, and vice versa. Default
         * value is `false`.
         */
        Boolean reverse;
    }

    /**
     * Special options on top of `TransformReverseOptions` for transforming cell
     * addresses.
     */
    public static class TransformAddressOptions extends TransformReverseOptions {

        TransformAddressOptions() {
            //
        }

        TransformAddressOptions(Boolean move) {
            this.move = move;
        }
        /**
         * If set to `true`, a cell address will never be deleted but will be moved
         * according to the transformation. Insert operations will shift the
         * address to (but not off) the sheet limits, delete operations will move
         * the (deleted) address to the left border of the deleted interval.
         */
        Boolean move;
    }

    /**
     * Specifies how to transform index intervals and cell range addresses when
     * inserting or deleting intervals inside them.
     */
    public static enum TransformMode {

        /**
         * The size of the index intervals and cell ranges will be increased when
         * inserting an interval inside, and decreased when deleting intervals
         * inside, but will NOT increased when inserting intervals directly after.
         *
         * Example: Inserting columns `C:E` will transform interval `A:D` to `A:G`.
         * Deleting columns `C:E` will transform interval `A:D` to `A:B`.
         */
        RESIZE,

        /**
         * Similar to `RESIZE`, but the end position of the index interval or cell
         * range will be expanded, if a new interval will be inserted directly
         * after the interval or range. Behaves equally to `RESIZE` when deleting
         * intervals.
         *
         * Example: Inserting columns `C:E` will transform interval `A:D` to `A:G`,
         * and interval `A:B` to `A:E` (directly after). Deleting columns `C:E`
         * will transform interval `A:D` to `A:B`.
         */
        EXPAND,

        /**
         * Similar to `RESIZE`, but an index interval or cell range will be deleted
         * (instead of being shortened), if its end position would be shifted
         * beyond the maximum index. Behaves equally to `RESIZE` when deleting
         * intervals.
         *
         * Example: With sheet limit at column Z, inserting single column `C` will
         * transform interval `X:Y` to `Y:Z`, but will delete interval `Y:Z`
         * (instead of transforming it to interval `Z`) because it would be shifted
         * outside the sheet partially.
         */
        STRICT,

        /**
         * Index intervals and cell ranges will be split into parts when inserting
         * intervals inside. Behaves equally to `RESIZE` when deleting intervals.
         *
         * Example: Inserting columns `C:E` will transform interval `A:D` to the
         * interval list `A:B F:G`. Deleting columns `C:E` will transform interval
         * `A:D` to `A:B`.
         */
        SPLIT,

        /**
         * Index intervals and cell ranges will not be modified when inserting or
         * deleting intervals inside (fixed size). Only insertions and deletions
         * before an interval or cell range will cause to shift it.
         *
         * Example: Inserting columns `A:C` will transform interval `A:D` to `D:G`,
         * but inserting columns `C:E` will not transform interval `A:D`. Deleting
         * columns `B:C` will transform intervals `B:C`, `C:D`, and `D:E` to `B:C`
         * (intervals will be pulled to the beginning of the deleted interval, but
         * will not be shortened).
         */
        FIXED
    }

    /**
     * Special options on top of `TransformReverseOptions` for transforming index
     * intervals and cell range addresses.
     */
    public static class TransformIntervalOptions extends TransformReverseOptions {

        public TransformIntervalOptions() {
            //
        }

        public TransformIntervalOptions(TransformMode transformMode) {
            super();

            this.transformMode = transformMode;
        }

        /**
         * Specifies how to transform index intervals or cell range addresses when
         * inserting or deleting intervals inside. Default value is `RESIZE`.
         */
        TransformMode transformMode;
    }

    /**
     * Special options on top of `TransformIntervalOptions` for transforming cell
     * range addresses.
     */
    public static class TransformRangeOptions extends TransformIntervalOptions {

        /**
         * If set to `true`, cell range addresses partially covered by the band
         * interval of the address transformer will be split, the covered part will
         * be moved and transformed, while the uncovered part will be kept
         * unmodified. By default, partially covered cell range addresses will not
         * be transformed at all. Default value is `false`.
         */
        Boolean splitPartial;

        public TransformRangeOptions() {
            //
        }

        public TransformRangeOptions(TransformMode transformMode) {
            super(transformMode);
        }
    }

    public static class OTAddressTransformer extends AddressTransformer {

        final int sheetIndex;

        public OTAddressTransformer(AddressFactory addressFactory, Interval bandInterval, IntervalArray moveIntervals, Direction direction, int sheetIndex) {
            super(addressFactory, bandInterval, moveIntervals, direction);
            this.sheetIndex = sheetIndex;
        }
        /**
         * Creates an address transformer for one or more index intervals (entire
         * columns or rows in the sheet, suitable e.g. for the document operations
         * "insertColumns", "deleteRows", etc.).
         *
         * @param addressFactory
         *  The address factory used to resolve the maximum column/row index in the
         *  document.
         *
         * @param moveIntervals
         *  The index intervals of the columns or rows to be inserted or deleted in
         *  the sheet.
         *
         * @param direction
         *  The direction to move existing columns/rows to.
         */
        public static OTAddressTransformer fromIntervals(AddressFactory addressFactory, IntervalArray moveIntervals, Direction direction, int sheet) {
            final boolean columns = !direction.isVerticalDir();
            return new OTAddressTransformer(addressFactory, addressFactory.getFullInterval(!columns), moveIntervals, direction, sheet);
        }

        /**
         * Creates an address transformer for a single cell range (suitable e.g.
         * for the document operation "moveCells").
         *
         * @param addressFactory
         *  The address factory used to resolve the maximum column/row index in the
         *  document.
         *
         * @param moveRange
         *  The address of the cell range to be inserted or deleted in the sheet.
         *  In insertion mode, this cell range will become empty, and the contents
         *  starting from its leading border will be shifted towards the end of the
         *  sheet. In deletion mode, this cell range will be deleted, and the
         *  contents starting outside its trailing border will be shifted towards
         *  its leading border.
         *
         * @param direction
         *  The direction to move existing cells to.
         */
        public static OTAddressTransformer fromRange(AddressFactory addressFactory, CellRefRange moveRange, Direction direction, int sheet) {
            final boolean columns = !direction.isVerticalDir();
            return new OTAddressTransformer(addressFactory, moveRange.interval(!columns), new IntervalArray(moveRange.interval(columns)), direction, sheet);
        }

        public int getSheetIndex() {
            return sheetIndex;
        }
    }
}
