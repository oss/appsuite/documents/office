/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.tools.doc.DocumentFormat;

/**
 * An instance of this class contains the maximum possible column index and row
 * index of cell addresses in a spreadsheet document, and provides methods to
 * deal with index intervals (columns and rows), cell addresses, and cell range
 * addresses.
 *
 * @param colCount
 *  The total number of columns contained in a sheet.
 *
 * @param rowCount
 *  The total number of rows contained in a sheet.
 */
public class AddressFactory {

    final int maxColumns;

    final int maxRows;

    public AddressFactory(DocumentFormat format) {
        if(format == DocumentFormat.ODS) {
            this.maxColumns = 1024;
            this.maxRows = 65536;
        }
        else {
            this.maxColumns = 16384;
            this.maxRows = 1048576;
        }
    }

    public AddressFactory(int maxColumns, int maxRows) {
        this.maxColumns = maxColumns;
        this.maxRows = maxRows;
    }

    public final int getColCount() {
        return maxColumns;
    }

    public final int getRowCount() {
        return maxRows;
    }

    public int getMaxCol() {
        return maxColumns - 1;
    }

    public int getMaxRow() {
        return maxRows - 1;
    }

    /**
     * Returns the maximum column or row index in a sheet.
     *
     * @param columns
     *  Whether to return the maximum column index (`true`), or the maximum row
     *  index (`false`).
     *
     * @returns
     *  The maximum column or row index in a sheet.
     */
    public int getMaxIndex(boolean columns) {
        return columns ? getMaxCol() : getMaxRow();
    }

    /**
     * Returns the index interval covering all columns or rows in a sheet.
     *
     * @param columns
     *  Whether to return the column interval (`true`), or the row interval
     *  (`false`).
     *
     * @returns
     *  The full column or row interval in a sheet. Each invocation of this
     *  method creates a new interval object that can be modified further.
     */
    public Interval getFullInterval(boolean columns) {
        return Interval.createInterval(columns, 0, getMaxIndex(columns));
    }
}
