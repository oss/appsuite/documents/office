/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

import com.openexchange.office.filter.core.spreadsheet.CellRef;

public abstract class FormulaUpdateTask {

    public FormulaUpdateTask() {

    }

 // class DeleteSheetUpdateTask ================================================

    /**
     * Formula update task for a deleted sheet in the spreadsheet document. All
     * references in all formula expressions containing the deleted sheet become
     * invalid and will be replaced with `#REF!` errors.
     *
     * @param sheet
     *  The index of the deleted sheet.
     */
    static public class DeleteSheetUpdateTask extends FormulaUpdateTask {

        final int sheet;

        public DeleteSheetUpdateTask(int sheet) {
            this.sheet = sheet;
        }

/*  TODO:
        protected boolean prepareTokens(tokenArray: TokenArray): boolean {
            return tokenArray.hasAnySheetTokens() || tokenArray.hasToken(TokenType.TABLE);
        }

        protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
            return token.resolveDeletedSheet(grammar, this.sheet, refSheet);
        }
*/
    }

    static public class RenameSheetUpdateTask extends FormulaUpdateTask {

        final int sheet;
        final String sheetName;

        public RenameSheetUpdateTask(int sheet, String sheetName) {
            this.sheet = sheet;
            this.sheetName = sheetName;
        }
    }

    /**
     * Formula update task for moving a range of cells in a sheet. This includes
     * inserting or deleting some columns or rows. All affected references in all
     * formula expressions will be updated.
     *
     * @param sheet
     *  The index of the sheet containing the moved cells.
     *
     * @param transformer
     *  An address transformer specifying how to transform the cell references
     *  after cells have been moved in the sheet.
     */
    static public class MoveCellsUpdateTask extends FormulaUpdateTask {

        final int sheet;
        final AddressTransformer transformer;
        final CellRef refAddress;
        final CellRef targetAddress;

        public MoveCellsUpdateTask(int sheet, AddressTransformer transformer) {
            this(sheet, transformer, new CellRef(0, 0), new CellRef(0, 0));
        }

        public MoveCellsUpdateTask(int sheet, AddressTransformer transformer, CellRef refAddress) {
            this(sheet, transformer, refAddress, new CellRef(0, 0));
        }

        public MoveCellsUpdateTask(int sheet, AddressTransformer transformer, CellRef refAddress, CellRef targetAddress) {
            this.sheet = sheet;
            this.transformer = transformer;
            this.refAddress = refAddress;
            this.targetAddress = targetAddress;
        }
    }

    /**
     * Formula update task for changing the label of a defined name. All formula
     * expressions referring to the defined name will be updated to use the new
     * label.
     *
     * @param sheet
     *  The index of the sheet containing the relabeled defined name; or `null` for
     *  a globally defined name.
     *
     * @param oldLabel
     *  The old label of the relabeled defined name.
     *
     * @param newLabel
     *  The new label of the relabeled defined name.
     */
    static public class RelabelNameUpdateTask extends FormulaUpdateTask {

        final Integer sheet;
        final String oldLabel;
        final String newLabel;

        public RelabelNameUpdateTask(Integer sheet, String oldLabel, String newLabel) {
            this.sheet = sheet;
            this.oldLabel = oldLabel;
            this.newLabel = newLabel;
        }

        // protected methods ------------------------------------------------------

/* TODO:
        protected prepareTokens(okenArray: TokenArray): boolean {
            return tokenArray.hasToken(TokenType.NAME);
        }

        protected resolveToken(token: BaseToken, grammar: FormulaGrammar, refSheet: OptSheet): string | null {
            return token.resolveRelabeledName(grammar, this.sheet, this.oldLabel, this.newLabel, refSheet);
        }
    }
*/
    }

    /**
     * Formula update task for changing the name of a table range. All formula
     * expressions referring to the table range will be updated to use the new
     * name.
     *
     * @param sheet
     *  The index of the sheet containing the renamed table.
     *
     * @param oldName
     *  The old name of the renamed table range.
     *
     * @param newName
     *  The new name of the renamed table range.
     */
    static public class RenameTableUpdateTask extends FormulaUpdateTask {

        final String oldName;
        final String newName;

        public RenameTableUpdateTask(String oldName, String newName) {
            this.oldName = oldName;
            this.newName = newName;
        }

        // protected methods ------------------------------------------------------
/*
        protected prepareTokens(tokenArray: TokenArray): boolean {
            return tokenArray.hasToken(TokenType.TABLE);
        }

        protected resolveToken(token: BaseToken, grammar: FormulaGrammar): string | null {
            return token.resolveRenamedTable(grammar, this.oldName, this.newName);
        }
*/
    }

}
