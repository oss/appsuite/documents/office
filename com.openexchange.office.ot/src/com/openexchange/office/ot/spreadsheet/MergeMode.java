/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.ot.spreadsheet;

public enum MergeMode {

    /**
     * Merges an entire cell range (multiple columns and rows).
     */
    MERGE("merge"),

    /**
     * Merges the individual rows in a cell range.
     */
    HORIZONTAL("horizontal"),

    /**
     * Merges the individual columns in a cell range.
     */
    VERTICAL("vertical"),

    /**
     * Converts the affected merged ranges to single cells.
     */
    UNMERGE("unmerge");

    private String value;

    private MergeMode(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static MergeMode fromValue(String val) {
        for(MergeMode entry:MergeMode.values()) {
            if(entry.value.equals(val)) {
                return entry;
            }
        }
        return null;
    }
}
